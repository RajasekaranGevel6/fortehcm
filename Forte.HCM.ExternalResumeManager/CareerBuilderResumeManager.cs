﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CareerBuilderResumeManager.cs
// File that represents the business layer for the External resume module.
// This will talk to the data layer to perform the operations associated
// with the module.

#endregion

#region Directives                                                             
using System;
using System.IO;
using System.Xml;
using System.Configuration;
using System.Collections.Generic;
using Forte.HCM.BL;
using System.Threading;
using Forte.HCM.DataObjects;
using Forte.HCM.ResumeParser.Parser;
using System.Diagnostics;
using Forte.HCM.ResumeParser.Trace;
using System.Xml.Serialization;
using System.Text;
#endregion Directives

namespace Forte.HCM.ExternalResumeManager
{
    /// <summary>                                                  
    /// Class that represents the business layer for the External resume module.
    /// This includes functionalities for retrieving the sessionId,resume from 
    /// CareerBuilder Resume database and stored in local database.
    /// This will talk to the data layer for performing these operations.
    /// </summary>
    public class CareerBuilderResumeManager
    {
        #region Public Methods                                                 

        /// <summary>
        /// Gets the career builder resume results.
        /// </summary>
        /// <param name="packet">The packet.</param>
        /// <param name="sessionID">The session ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <returns></returns>
        public string GetCareerBuilderResumeResults(string packet, string sessionID, 
            int tenantID, int userID, out string errorMessage)
        {
            string candidateIds = "";
            errorMessage = "";
            try
            {
                string existingCandidateID = "";
                string newCandidateID = "";
                Resumes resume = new Resumes();
                string searchResuletXMLString = resume.V2_AdvancedResumeSearch(packet);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(searchResuletXMLString);
                XmlNode xmlNodeTopLevelErrorCodeNode = xmlDoc.SelectSingleNode("/Packet/Error");

                if (!Support.Utility.IsNullOrEmpty(xmlNodeTopLevelErrorCodeNode))
                {
                    errorMessage = xmlNodeTopLevelErrorCodeNode.InnerText;
                    return candidateIds;
                }

                XmlNode xmlNodeErrorCodeNode = xmlDoc.SelectSingleNode("/Packet/Errors");
                XmlNode xmlNodeHitsNode = xmlDoc.SelectSingleNode("/Packet/Hits");
                errorMessage = xmlNodeErrorCodeNode.InnerText;
                if (xmlNodeHitsNode.InnerText == "0")
                {
                    errorMessage = "No data found";
                    LogWrite(string.Format("Message: {0} ", errorMessage), "");
                    return candidateIds;
                }
                XmlNode xmlNode = xmlDoc.SelectSingleNode("/Packet/Results");
                if (Support.Utility.IsNullOrEmpty(xmlNode))
                {
                    errorMessage = "No data found";
                    LogWrite(string.Format("Message: {0} ", errorMessage), "");
                    return candidateIds;
                }
                LogWrite(string.Format("Total Records Searched From CareerBuiler: {0} ", xmlNodeHitsNode.InnerText), "");

                int candidateID = 0;
                string resumeID = "";
                string fileName = "";
                Dictionary<int, string> fileList = null;
                for (int i = 0; i < xmlNode.ChildNodes.Count; i++)
                {
                    resumeID = xmlNode.ChildNodes[i].ChildNodes[9].InnerText;
                    candidateID = new ExternalResumeServiceBLManager().GetExistingCandidateID(resumeID, null);

                    if (candidateID > 0)
                    {
                        new ExternalResumeServiceBLManager().InsertTenanatCandidate(tenantID, candidateID, userID);
                        existingCandidateID = existingCandidateID + "," + candidateID.ToString();
                    }
                    else
                    {
                        candidateID = GetCareerBuilderResume(xmlNode.ChildNodes[i].ChildNodes[9].InnerText, sessionID, tenantID, userID, out fileName);
                        if (File.Exists(fileName))
                        {
                            if (Support.Utility.IsNullOrEmpty(fileList))
                                fileList = new Dictionary<int, string>();
                            fileList.Add(candidateID, fileName);
                        }
                        newCandidateID = newCandidateID + "," + candidateID.ToString();
                    }
                    if (candidateIds.Length > 0)
                        candidateIds = candidateIds + "," + candidateID.ToString();
                    else
                        candidateIds = candidateID.ToString();
                }
                if (!Support.Utility.IsNullOrEmpty(fileList))
                    ParseMultiple(fileList);
                LogWrite(string.Format("New Canidates ID: {0} <br/> Existing Canidates ID: {1}", newCandidateID.TrimEnd(','), existingCandidateID.TrimEnd(',')), "Candidates id");
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
            return candidateIds;
        }

        /// <summary>
        /// Checks the job board login credentials.
        /// </summary>
        /// <param name="loginName">Name of the login.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public string CheckJobBoardLoginCredentials(string loginName, string password)
        {
            SessionInfo sessionInfo = new SessionInfo();
            try
            {
                Resumes resume = new Resumes();
                sessionInfo = resume.CB_BeginSession(loginName, password);
                if (sessionInfo.Errors.Length > 0)
                    return null;
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }

            return sessionInfo.SessionToken;
        }

        #endregion

        #region Private methods                                                
        /// <summary>
        /// Gets the career builder resume.
        /// </summary>
        /// <param name="resumeID">The resume ID.</param>
        /// <param name="sessionID">The session ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="filePath">The file path.</param>
        /// <returns></returns>
        private int GetCareerBuilderResume(string resumeID, string sessionID,
            int tenantID, int userID, out string filePath)
        {
            int candidateID = 0;
            filePath = "";
            try
            {
                Resumes resume = new Resumes();
                string packet = "<Packet><SessionToken>" + sessionID + "</SessionToken><ResumeID>" + resumeID +
                    "</ResumeID><GetWordDocIfAvailable>True</GetWordDocIfAvailable></Packet>";
                string resumeXMLString = resume.V2_GetResume(packet);
                CandidateInformation candidateInformation = new CandidateInformation();
                ExternalResume externalResume = new ExternalResume();
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(resumeXMLString);
                XmlNode xmlNode = xmlDoc.SelectSingleNode("/Packet");
                byte[] resumeContent = null;
                string resumeType = "txt";
                string resumeText = "";

                string candidateFirstName = "";
                externalResume.ResumeID = resumeID;

                externalResume.JobBoardID = "JB_CR_BLDR";

                //externalResume.CandidateAddress = resumeResultItem.HomeLocation;
                //externalResume.CandidateName = resumeResultItem.ContactName;
                //externalResume.RecentPay = resumeResultItem.RecentPay;
                //externalResume.LastModified = resumeResultItem.LastUpdate;
                //externalResume.CandidateEmail = resumeResultItem.ContactEmail;

                for (int i = 0; i < xmlNode.ChildNodes.Count; i++)
                {
                    switch (xmlNode.ChildNodes[i].Name)
                    {
                        case "ContactName":
                            candidateFirstName = xmlNode.ChildNodes[i].InnerText;
                            string[] contactName = candidateFirstName.Split(' ');
                            int count = 1;
                            if (contactName.Length > 1)
                                count = 2;
                            contactName = candidateFirstName.Split(new Char[] {' '}, count);
                            candidateInformation.caiFirstName = contactName[0];
                            if (contactName.Length>1)
                                candidateInformation.caiLastName = contactName[1];
                            else
                                candidateInformation.caiLastName = string.Empty;
                            break;
                        case "ContactEmail":
                            externalResume.CandidateEmail = xmlNode.ChildNodes[i].InnerText;
                            candidateInformation.caiEmail = xmlNode.ChildNodes[i].InnerText;
                            break;
                        case "ContactPhone":
                            externalResume.CandidatePhone = xmlNode.ChildNodes[i].InnerText;
                            candidateInformation.caiCell = xmlNode.ChildNodes[i].InnerText;
                            break;
                        case "HomeLocation":
                            candidateInformation.caiZip = xmlNode.ChildNodes[i].ChildNodes[3].InnerText;
                            externalResume.ZipCode = xmlNode.ChildNodes[i].ChildNodes[3].InnerText;
                            candidateInformation.caiLocation = xmlNode.ChildNodes[i].ChildNodes[0].InnerText;
                            break;
                        case "OriginalWordDoc":
                            if (xmlNode.ChildNodes[i].ChildNodes[0].InnerText.Length > 0)
                            {
                                string[] resumeTypeArray = xmlNode.ChildNodes[i].ChildNodes[0].InnerText.Split('.');
                                resumeType = resumeTypeArray[resumeTypeArray.Length - 1];
                                resumeContent = Convert.FromBase64String(xmlNode.ChildNodes[i].ChildNodes[1].InnerText);
                                xmlDoc.DocumentElement.RemoveChild(xmlNode.ChildNodes[i]);
                            }
                            externalResume.ResumeType = resumeType;
                            externalResume.ResumeContent = resumeContent;
                            break;
                        case "ResumeTitle":
                            externalResume.ResumeTitle = xmlNode.ChildNodes[i].InnerText;
                            break;
                        case "ResumeText":
                            resumeText = xmlNode.ChildNodes[i].InnerText;
                            externalResume.ResumeText = resumeText;
                            xmlDoc.DocumentElement.RemoveChild(xmlNode.ChildNodes[i]);
                            break;
                    }

                }
                externalResume.ExperienceMonths = "1";
                externalResume.ResumeDetails = xmlDoc.InnerXml;
                candidateInformation.caiType = "CT_CR_BLDR";
                candidateInformation.caiEmployee = 'N';
                candidateInformation.caiLimitedAccess = true;
                candidateID = new ExternalResumeServiceBLManager().InsertCareerBuiderResume(externalResume, candidateInformation, userID, tenantID);
                resumeType = externalResume.ResumeType;
                // string filePath = GetUploadedResumePath();

                string fileName = candidateID + "_" + candidateInformation.caiFirstName;
                string sourceFile = ReadyToParserPath;
                filePath = sourceFile + fileName;
                fileName = candidateID + "_" + candidateInformation.caiFirstName + "." + resumeType;
                sourceFile = sourceFile + fileName;
                if (!new FileInfo(sourceFile).Exists)
                {
                    if (resumeType == "txt")
                    {
                        sourceFile = SourceFile + fileName;
                        StreamWriter SW;
                        SW = File.CreateText(sourceFile);
                        SW.WriteLine(externalResume.ResumeText);
                        SW.Close();
                    }
                    else
                    {
                        FileStream fileStream = new FileStream(sourceFile, FileMode.CreateNew, FileAccess.ReadWrite);
                        fileStream.Write(externalResume.ResumeContent, 0, externalResume.ResumeContent.Length);
                        fileStream.Close();
                    }
                }

                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                UserID = userID;
                bool documentConverterFlag = GetIParserInstance.ConvertDocument(sourceFile, candidateID);

                if (documentConverterFlag)
                {
                    LogWrite(string.Format("{0} Conversion successfully ", fileName), "Conversion ");
                }
                else
                {
                    LogWrite(string.Format("{0} Conversion Failed ", fileName), "Conversion ");
                }

                new ResumeRepositoryBLManager().InsertDownloadloadResumeLog(candidateID, candidateInformation.caiFirstName, fileName, userID);
                filePath = filePath + ".txt";

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
            return candidateID;
        }

        /// <summary>
        /// Parses the multiple.
        /// </summary>
        /// <param name="fileList">The file list.</param>
        private void ParseMultiple(Dictionary<int, string> fileList)
        {
            try
            {
                int MAX = fileList.Count;
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                List<CallBackData> callBackDatas = new List<CallBackData>();
                AutoResetEvent[] autoResetEvents = null;
                autoResetEvents = new AutoResetEvent[MAX];

                // Initialize auto reset event.
                int item = 0;
                string filename = "";
                foreach (var pair in fileList)
                {
                    filename = pair.Value;
                    autoResetEvents[item] = new AutoResetEvent(false);
                    callBackDatas.Add(new CallBackData(filename, pair.Key, 1, autoResetEvents[item]));
                    item++;
                }


                // Queue the files to process.
                for (int file = 0; file < MAX; file++)
                {
                    ThreadPool.QueueUserWorkItem(
                        new WaitCallback(ParseFile), callBackDatas[file]);
                }

                // Wait for all of the files are to be parsed.
                WaitHandle.WaitAll(autoResetEvents);

                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds / 10);
                LogWrite(elapsedTime, "Parsing time per resume");
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
        }

        /// <summary>
        /// Parses the file.
        /// </summary>
        /// <param name="data">The data.</param>
        private void ParseFile(object data)
        {
            try
            {
                CallBackData callBackData = data as CallBackData;
                bool result = GetIParserInstance.Parse(callBackData.SourceFile, callBackData.CandidateID);

                if (result)
                    LogWrite(string.Format("{0} Resume parsed successfully ", callBackData.SourceFile), "Parsing ");
                else
                    LogWrite(string.Format("{0} Resume parsed failed", callBackData.SourceFile), "Parsing ");

                callBackData.AutoResetEvent.Set();
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        private class CallBackData
        {
            public string SourceFile { set; get; }
            public int CandidateID { set; get; }
            public int UserID { set; get; }
            public AutoResetEvent AutoResetEvent { set; get; }

            public CallBackData(string sourceFile, int candidateID, int userID,
                AutoResetEvent autoResetEvent)
            {
                this.SourceFile = sourceFile;
                this.CandidateID = candidateID;
                this.UserID = userID;
                this.AutoResetEvent = autoResetEvent;
            }
        }

        /// <summary>
        /// Gets the source file.
        /// </summary>
        private string SourceFile
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_UPLOADED_RESUMES_FOLDER"].ToString();
            }
        }

        /// <summary>
        /// Gets the converted resumes path.
        /// </summary>
        private string ConvertedResumesPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_CONVERSION_SUCCEEDED_FOLDER"].ToString();
            }
        }

        /// <summary>
        /// Gets the conversion failed path.
        /// </summary>
        private string ConversionFailedPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_CONVERSION_FAILED_FOLDER"].ToString();
            }
        }

        /// <summary>
        /// Gets the ready to parser path.
        /// </summary>
        private string ReadyToParserPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_READY_TO_PARSE_FOLDER"].ToString();
            }
        }
        //parsing

        /// <summary>
        /// Gets the parsing process path.
        /// </summary>
        private string ParsingProcessPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_PARSING_ON_PROCESS_FOLDER"].ToString();
            }
        }

        /// <summary>
        /// Gets the parsing succeeded path.
        /// </summary>
        private string ParsingSucceededPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_PARSING_SUCCEEDED_FOLDER"].ToString();
            }
        }

        /// <summary>
        /// Gets the parsing failed path.
        /// </summary>
        private string ParsingFailedPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_PARSING_FAILED_FOLDER"].ToString();
            }
        }

        /// <summary>
        /// Gets the HR XML path.
        /// </summary>
        private string HR_XmlPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_HRXML_FOLDER"].ToString();
            }
        }

        /// <summary>
        /// Gets the get I parser instance.
        /// </summary>
        private IParser GetIParserInstance
        {
            get
            {
                ConverterSettings converterSettings = new ConverterSettings();
                converterSettings.ConversionFailedPath = ConversionFailedPath;
                converterSettings.ConvertedResumesPath = ConvertedResumesPath;
                converterSettings.ReadyToParsePath = ReadyToParserPath;
                converterSettings.UserID = _userID;

                Forte.HCM.ResumeParser.Parser.ParserSettings parserSettings = new Forte.HCM.ResumeParser.Parser.ParserSettings();
                parserSettings.FailedPath = ParsingFailedPath;
                parserSettings.HRXMLPath = HR_XmlPath;
                parserSettings.ProcessPath = ParsingProcessPath;
                parserSettings.SucceededPath = ParsingSucceededPath;
                parserSettings.UserID = _userID;
                IParser parser = ParserFactory.GetInstance(converterSettings, parserSettings);
                return parser;
            }
        }

        int _userID;
        /// <summary>
        /// Sets the user ID.
        /// </summary>
        /// <value>
        /// The user ID.
        /// </value>
        private int UserID
        {
            set
            {
                _userID = value;
            }
        }

        private void LogWrite(string logMessage, string appTitle)
        {
            Logger.EventLog(logMessage, appTitle);
        }
        #endregion
    }
}
