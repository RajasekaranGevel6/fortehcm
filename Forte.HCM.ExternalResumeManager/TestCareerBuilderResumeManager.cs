﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CareerBuilderResumeManager.cs
// File that represents the business layer for the External resume module.
// This will talk to the data layer to perform the operations associated
// with the module.

#endregion

#region Directives
using System;
using System.IO;
using System.Xml;
using System.Configuration;
using System.Collections.Generic;
using Forte.HCM.BL;
using System.Threading;
using Forte.HCM.DataObjects;
using Forte.HCM.ResumeParser.Parser;
using System.Diagnostics;
using Forte.HCM.ResumeParser.Trace;
using System.Xml.Serialization;
using System.Text;
#endregion Directives

namespace Forte.HCM.ExternalResumeManager
{
    /// <summary>                                                  
    /// Class that represents the business layer for the External resume module.
    /// This includes functionalities for retrieving the sessionId,resume from 
    /// CareerBuilder Resume database and stored in local database.
    /// This will talk to the data layer for performing these operations.
    /// </summary>
    public class TestCareerBuilderResumeManager
    {
        #region Public Methods

        public string test()
        {
            MemoryStream stream = null;
            TextWriter writer = null;
            try
            {
                Category category = new Category();
                category.CategoryID = 1;
                category.CategoryName = "Category";
                stream = new MemoryStream(); // read xml in memory

                writer = new StreamWriter(stream, Encoding.Unicode);
                // get serialise object
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                XmlSerializer serializer = new XmlSerializer(typeof(Category));
                serializer.Serialize(writer, category, ns); // read object

                int count = (int)stream.Length; // saves object in memory stream

                byte[] arr = new byte[count];
                stream.Seek(0, SeekOrigin.Begin);
                // copy stream contents in byte array

                stream.Read(arr, 0, count);
                UnicodeEncoding utf = new UnicodeEncoding(); // convert byte array to string
                string XML = utf.GetString(arr).Trim();
                XML.Replace("<?xml version=\"1.0\" encoding=\"utf-16\" ?>", "").Replace("Category", "Packet");
                return XML;
            }
            catch
            {
                return string.Empty;
            }
            finally
            {
                if (stream != null) stream.Close();
                if (writer != null) writer.Close();
            }
        }

        /// <summary>
        /// Gets the career builder resume results.
        /// </summary>
        /// <param name="packet">The packet.</param>
        /// <param name="sessionID">The session ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <returns></returns>
        public string GetCareerBuilderResumeResults(string packet, string sessionID, int tenantID, int userID,int pageNo,int pageSize, out string errorMessage)
        {
            string candidateIds = "";
            errorMessage = "";
            try
            {
                int candidateID = 0;
                string resumeID = "";
                string fileName = "";
                Dictionary<int, string> fileList = null;

                List<string> resumesID = new ExternalResumeServiceBLManager().GetResumesID(1,2);
                if (resumesID == null)
                    return candidateIds;
                for (int i = 0; i < resumesID.Count; i++)
                {
                    resumeID = RandomString(19);
                    candidateID = GetCareerBuilderResume(resumeID, resumesID[i], tenantID, userID, out fileName);
                    if (File.Exists(fileName))
                    {
                        if (Support.Utility.IsNullOrEmpty(fileList))
                            fileList = new Dictionary<int, string>();
                        fileList.Add(candidateID, fileName);
                    }
                    if (candidateIds.Length > 0)
                        candidateIds = candidateIds + "," + candidateID.ToString();
                    else
                        candidateIds = candidateID.ToString();
                }
                if (!Support.Utility.IsNullOrEmpty(fileList))
                    ParseMultiple(fileList);
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
            return candidateIds;
        }


        /// <summary>
        /// Checks the job board login credentials.
        /// </summary>
        /// <param name="loginName">Name of the login.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public string CheckJobBoardLoginCredentials(string loginName, string password)
        {
            SessionInfo sessionInfo = new SessionInfo();
            try
            {
                Resumes resume = new Resumes();
                sessionInfo = resume.CB_BeginSession(loginName, password);
                if (sessionInfo.Errors.Length > 0)
                    return "";
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }

            return sessionInfo.SessionToken;
        }

        #endregion

        #region Private methods

        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }



        /// <summary>
        /// Gets the career builder resume.
        /// </summary>
        /// <param name="resumeID">The resume ID.</param>
        /// <param name="sessionID">The session ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="filePath">The file path.</param>
        /// <returns></returns>
        private int GetCareerBuilderResume(string resumeID, string sessionID, int tenantID, int userID, out string filePath)
        {
            string Test = " TEST";
            int candidateID = 0;
            filePath = "";
            try
            {
                //Resumes resume = new Resumes();
                //string packet = "<Packet><SessionToken>" + sessionID + "</SessionToken><ResumeID>" + resumeID +
                //    "</ResumeID><GetWordDocIfAvailable>True</GetWordDocIfAvailable></Packet>";
                ExternalResume externalResume1 = new ExternalResumeServiceBLManager().GetExternalResume(sessionID);
                string resumeXMLString = externalResume1.ResumeDetails;
                CandidateInformation candidateInformation = new CandidateInformation();
                ExternalResume externalResume = new ExternalResume();
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(resumeXMLString);
                XmlNode xmlNode = xmlDoc.SelectSingleNode("/Packet");
                byte[] resumeContent = null;
                string resumeType = "txt";
                string resumeText = "";

                string candidateFirstName = "";
                externalResume.ResumeID = resumeID;

                externalResume.JobBoardID = "JB_CR_BLDR";

                //externalResume.CandidateAddress = resumeResultItem.HomeLocation;
                //externalResume.CandidateName = resumeResultItem.ContactName;
                //externalResume.RecentPay = resumeResultItem.RecentPay;
                //externalResume.LastModified = resumeResultItem.LastUpdate;
                //externalResume.CandidateEmail = resumeResultItem.ContactEmail;

                for (int i = 0; i < xmlNode.ChildNodes.Count; i++)
                {
                    switch (xmlNode.ChildNodes[i].Name)
                    {
                        case "ContactName":
                            candidateFirstName = xmlNode.ChildNodes[i].InnerText + Test;
                            candidateInformation.caiFirstName = candidateFirstName + Test;
                            break;
                        case "ContactEmail":
                            externalResume.CandidateEmail = xmlNode.ChildNodes[i].InnerText + Test;
                            candidateInformation.caiEmail = xmlNode.ChildNodes[i].InnerText + Test;
                            break;
                        case "ContactPhone":
                            externalResume.CandidatePhone = xmlNode.ChildNodes[i].InnerText;
                            candidateInformation.caiCell = xmlNode.ChildNodes[i].InnerText;
                            break;
                        case "HomeLocation":
                            candidateInformation.caiZip = xmlNode.ChildNodes[i].ChildNodes[3].InnerText;
                            externalResume.ZipCode = xmlNode.ChildNodes[i].ChildNodes[3].InnerText;
                            candidateInformation.caiLocation = xmlNode.ChildNodes[i].ChildNodes[0].InnerText;
                            break;
                        case "OriginalWordDoc":
                            if (xmlNode.ChildNodes[i].ChildNodes[0].InnerText.Length > 0)
                            {
                                string[] resumeTypeArray = xmlNode.ChildNodes[i].ChildNodes[0].InnerText.Split('.');
                                resumeType = resumeTypeArray[resumeTypeArray.Length - 1];
                                resumeContent = Convert.FromBase64String(xmlNode.ChildNodes[i].ChildNodes[1].InnerText);
                                xmlDoc.DocumentElement.RemoveChild(xmlNode.ChildNodes[i]);
                            }
                            externalResume.ResumeType = resumeType;
                            externalResume.ResumeContent = resumeContent;
                            break;
                        case "ResumeTitle":
                            externalResume.ResumeTitle = xmlNode.ChildNodes[i].InnerText + Test;
                            break;
                        case "ResumeText":
                            resumeText = xmlNode.ChildNodes[i].InnerText;
                            externalResume.ResumeText = resumeText;
                            xmlDoc.DocumentElement.RemoveChild(xmlNode.ChildNodes[i]);
                            break;
                    }

                }
                externalResume.ExperienceMonths = "1";
                externalResume.ResumeDetails = xmlDoc.InnerXml;
                candidateID = new ExternalResumeServiceBLManager().InsertCareerBuiderResume(externalResume, candidateInformation, userID, tenantID);
                resumeType = externalResume.ResumeType;
                // string filePath = GetUploadedResumePath();

                string fileName = candidateID + "_" + candidateInformation.caiFirstName;
                string sourceFile = ReadyToParserPath;
                filePath = sourceFile + fileName;
                fileName = candidateID + "_" + candidateInformation.caiFirstName + "." + resumeType;
                sourceFile = sourceFile + fileName;
                if (!new FileInfo(sourceFile).Exists)
                {
                    if (resumeType == "txt")
                    {
                        StreamWriter SW;
                        SW = File.CreateText(sourceFile);
                        SW.WriteLine(externalResume.ResumeText);
                        SW.Close();
                    }
                    else
                    {
                        FileStream fileStream = new FileStream(sourceFile, FileMode.CreateNew, FileAccess.ReadWrite);
                        fileStream.Write(externalResume.ResumeContent, 0, externalResume.ResumeContent.Length);
                        fileStream.Close();
                    }
                }

                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                UserID = userID;
                bool documentConverterFlag = GetIParserInstance.ConvertDocument(sourceFile, candidateID);

                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds / 10);
                Console.WriteLine();
                Console.WriteLine(elapsedTime);

                new ResumeRepositoryBLManager().InsertDownloadloadResumeLog(candidateID, candidateInformation.caiFirstName, fileName, userID);
                filePath = filePath + ".txt";

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
            return candidateID;
        }

        /// <summary>
        /// Parses the multiple.
        /// </summary>
        /// <param name="fileList">The file list.</param>
        private void ParseMultiple(Dictionary<int, string> fileList)
        {
            try
            {
                int MAX = fileList.Count;
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                List<CallBackData> callBackDatas = new List<CallBackData>();
                AutoResetEvent[] autoResetEvents = null;
                autoResetEvents = new AutoResetEvent[MAX];

                // Initialize auto reset event.
                int item = 0;
                string filename = "";
                foreach (var pair in fileList)
                {
                    filename = pair.Value;
                    autoResetEvents[item] = new AutoResetEvent(false);
                    callBackDatas.Add(new CallBackData(filename, pair.Key, 1, autoResetEvents[item]));
                    item++;
                }


                // Queue the files to process.
                for (int file = 0; file < MAX; file++)
                {
                    ThreadPool.QueueUserWorkItem(
                        new WaitCallback(ParseFile), callBackDatas[file]);
                }

                // Wait for all of the files are to be parsed.
                WaitHandle.WaitAll(autoResetEvents);

                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds / 10);
                Console.WriteLine();
                Console.WriteLine(elapsedTime);

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
        }

        /// <summary>
        /// Parses the file.
        /// </summary>
        /// <param name="data">The data.</param>
        private void ParseFile(object data)
        {
            try
            {
                CallBackData callBackData = data as CallBackData;

                Console.WriteLine(string.Format("Started parsing '{0}'", callBackData.SourceFile));

                bool result = GetIParserInstance.Parse(callBackData.SourceFile, callBackData.CandidateID);

                if (result)
                    Console.WriteLine("Resume parsed successfully");
                else
                    Console.WriteLine("Failed in parsing resume");

                callBackData.AutoResetEvent.Set();
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        private class CallBackData
        {
            public string SourceFile { set; get; }
            public int CandidateID { set; get; }
            public int UserID { set; get; }
            public AutoResetEvent AutoResetEvent { set; get; }

            public CallBackData(string sourceFile, int candidateID, int userID,
                AutoResetEvent autoResetEvent)
            {
                this.SourceFile = sourceFile;
                this.CandidateID = candidateID;
                this.UserID = userID;
                this.AutoResetEvent = autoResetEvent;
            }
        }

        /// <summary>
        /// Gets the source file.
        /// </summary>
        private string SourceFile
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_UPLOADED_RESUMES_FOLDER"].ToString();
            }
        }

        /// <summary>
        /// Gets the converted resumes path.
        /// </summary>
        private string ConvertedResumesPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_CONVERSION_SUCCEEDED_FOLDER"].ToString();
            }
        }

        /// <summary>
        /// Gets the conversion failed path.
        /// </summary>
        private string ConversionFailedPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_CONVERSION_FAILED_FOLDER"].ToString();
            }
        }

        /// <summary>
        /// Gets the ready to parser path.
        /// </summary>
        private string ReadyToParserPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_READY_TO_PARSE_FOLDER"].ToString();
            }
        }
        //parsing

        /// <summary>
        /// Gets the parsing process path.
        /// </summary>
        private string ParsingProcessPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_PARSING_ON_PROCESS_FOLDER"].ToString();
            }
        }

        /// <summary>
        /// Gets the parsing succeeded path.
        /// </summary>
        private string ParsingSucceededPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_PARSING_SUCCEEDED_FOLDER"].ToString();
            }
        }

        /// <summary>
        /// Gets the parsing failed path.
        /// </summary>
        private string ParsingFailedPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_PARSING_FAILED_FOLDER"].ToString();
            }
        }

        /// <summary>
        /// Gets the HR XML path.
        /// </summary>
        private string HR_XmlPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CAREER_BUILDER_HRXML_FOLDER"].ToString();
            }
        }

        /// <summary>
        /// Gets the get I parser instance.
        /// </summary>
        private IParser GetIParserInstance
        {
            get
            {
                ConverterSettings converterSettings = new ConverterSettings();
                converterSettings.ConversionFailedPath = ConversionFailedPath;
                converterSettings.ConvertedResumesPath = ConvertedResumesPath;
                converterSettings.ReadyToParsePath = ReadyToParserPath;
                converterSettings.UserID = _userID;

                Forte.HCM.ResumeParser.Parser.ParserSettings parserSettings = new Forte.HCM.ResumeParser.Parser.ParserSettings();
                parserSettings.FailedPath = ParsingFailedPath;
                parserSettings.HRXMLPath = HR_XmlPath;
                parserSettings.ProcessPath = ParsingProcessPath;
                parserSettings.SucceededPath = ParsingSucceededPath;
                parserSettings.UserID = _userID;
                IParser parser = ParserFactory.GetInstance(converterSettings, parserSettings);
                return parser;
            }
        }

        int _userID;
        /// <summary>
        /// Sets the user ID.
        /// </summary>
        /// <value>
        /// The user ID.
        /// </value>
        private int UserID
        {
            set
            {
                _userID = value;
            }
        }
        #endregion
    }
}
