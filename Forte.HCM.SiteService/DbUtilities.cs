﻿using System;
using System.Configuration;

namespace Forte.HCM.SiteService
{
    /// <summary>
    /// Represents the class that provides helper methods that are used while 
    /// performing database operations.
    /// </summary>
    public class DbUtility
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="System.String"/> that holds the key in the 
        /// configuration file to get the command time out value.
        /// </summary>
        private const string _commandTimeOut = "CommandTimeOut";

        /// <summary>
        /// A <see cref="System.Int32"/> that represents the default command 
        /// time out value.
        /// </summary>
        private const int _defaultCommandTimeOut = 60;

        /// <summary>
        /// A <see cref="System.String"/> that represents the name of the 
        /// connection string in the application configuration file that is 
        /// used to connect to the application database.
        /// </summary>
        private const string _connectionStringName = "HCM_SQL";

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a <see cref="System.String"/> that represents the name of the 
        /// connection string in the application configuration file that is 
        /// used to connect to the application database.
        /// </summary>
        public static string ConnectionStringName
        {
            get
            {
                return _connectionStringName;
            }
        }

        /// <summary>
        /// Gets a <see cref="System.Int32"/> that represents the time-out 
        /// value for a database command.
        /// </summary>
        public static int CommandTimeout
        {
            get
            {
            
                //Read the value from the configuration file.
                string value = ConfigurationManager.AppSettings[_commandTimeOut];

                //If the value is not present in the configuration file.
                if (String.IsNullOrEmpty(value))
                    return _defaultCommandTimeOut;

                //Value is there in the configuration. Try and parse it.
                int parsedValue = 0;

                if (int.TryParse(value, out parsedValue))
                {
                    //Safety check - 0 means infinite. Never let it infinite
                    if (parsedValue <= 0)
                        return _defaultCommandTimeOut;
                }

                //Return the non zero parsed value from the configuration.
                return parsedValue;
            }
        }

        #endregion
    }
}
