﻿using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Web.Services;
using System.Configuration;
using System.Web.Configuration;
using System.Text.RegularExpressions;

using Forte.HCM.BL;

using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;
using Forte.HCM.Trace;

namespace Forte.HCM.SiteService
{
   

    /// <summary>
    /// Summary description for FHCMSignupService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FHCMSignupService : WebService
    {

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="UserDetail">
        /// A <see cref="userDetail"/> that holds the user object details.
        /// </param>
        private delegate void AsyncTaskDelegate(UserDetail userDetail);

        /// <summary>
        /// Method that validates the user and returns the sesion key and user
        /// type.
        /// </summary>
        /// <param name="userName">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="password">
        /// A <see cref="string"/> that holds the password.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the User details
        /// </returns>
        [WebMethod]
        public string GetUserDetail(string userName, string password)
        {
            return new AuthenticationBLManager().GetSiteUserDetail(userName, password);
        }

        /// <summary>
        /// Method that validates the username
        /// </summary>
        /// <param name="userName">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the User details
        /// </returns>
        [WebMethod]
        public bool GetUserPassword(string userName)
        {
           bool retVal = false;

           UserDetail userDetail = new UserRegistrationBLManager().GetPassword(userName);
           if (!Utility.IsNullOrEmpty(userDetail))
           {
               // Decrypt the password.
               userDetail.Password = new EncryptAndDecrypt().DecryptString(userDetail.Password);

               // Send the password through mail.
               try
               {
                   AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendPasswordToUser);
                   IAsyncResult result = taskDelegate.BeginInvoke(userDetail,
                       new AsyncCallback(SendPasswordToUserCallBack), taskDelegate);
               }
               catch (Exception exp)
               {
                   Logger.ExceptionLog(exp);
               }
               retVal = true;
           }
           return retVal;
        }

        /// <summary>
        /// Method to insert user log details
        /// </summary>
        /// <param name="userName">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="ipAddress">
        /// A <see cref="string"/> that holds the ip address.
        /// </param>
        [WebMethod]
        public void InsertUserLog(string userName, string ipAddress)
        {
            try
            {
                //Insert user log
                new DatabaseConnectionManager().InsertUserLog(userName,
                    ipAddress);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that creates an account.
        /// </summary>
        /// <param name="firstName">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="lastName">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="email">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="password">
        /// A <see cref="string"/> that holds the password.
        /// </param>
        /// <param name="phone">
        /// A <see cref="string"/> that holds the phone.
        /// </param>
        /// <param name="company">
        /// A <see cref="string"/> that holds the company.
        /// </param>
        /// <
        /// <param name="subscrptionID">
        /// A <see cref="int"/> that holds the subscription ID.
        /// 1: Professional (Free)
        /// 2: Business (Standard)
        /// 3: Enterprise (Corporate)
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the status.
        /// 1: Succeeded
        /// 2: Account already exist
        /// 3: Mail failed
        /// 4: Error occured
        /// </returns>
        [WebMethod]
        public string CreateAccount(string firstName, string lastName, 
            string email, string password, string phone, int subscrptionID)
        {
            string returnstring = "2";
            if (CheckAccountAlreadyExist(email))
                return returnstring;

            UserRegistrationInfo userRegistrationInfo = null;
            RegistrationConfirmation registrationConfirmation = null;
            string Confirmation_Code = string.Empty;
            IDbTransaction transaction = null;
            try
            {
                userRegistrationInfo = new UserRegistrationInfo();
                userRegistrationInfo.BussinessTypesIds = "";
                userRegistrationInfo.BussinessTypeOther = "FHCM Website";
                userRegistrationInfo.UserEmail = email.Trim();
                userRegistrationInfo.Password = new EncryptAndDecrypt().EncryptString(password);

                userRegistrationInfo.FirstName = firstName.Trim();
                userRegistrationInfo.LastName = lastName.Trim();
                userRegistrationInfo.Phone = phone.Trim();
                userRegistrationInfo.Company = string.Empty;
                userRegistrationInfo.Title = "";
                userRegistrationInfo.NumberOfUsers = 1;
                userRegistrationInfo.SubscriptionId = subscrptionID;
                if (subscrptionID == 1)
                {
                     userRegistrationInfo.SubscriptionRole = "SR_FRE";
                }
                else if (subscrptionID == 2)
                {
                    userRegistrationInfo.SubscriptionRole = "SR_STA";
                }               
                else if (subscrptionID == 3)
                {
                    userRegistrationInfo.SubscriptionRole = "SR_COR_AMN";
                }
                else 
                    userRegistrationInfo.SubscriptionRole = "SR_FRE";

                userRegistrationInfo.IsTrial = 0;
                userRegistrationInfo.TrialDays = 30;

                registrationConfirmation = new RegistrationConfirmation();
                Confirmation_Code = new EncryptAndDecrypt().EncryptString(RandomString(15));
                registrationConfirmation.ConfirmationCode = Confirmation_Code;
                transaction = new TransactionManager().Transaction;

                // Insert user.
                new UserRegistrationBLManager().InsertUser(null, userRegistrationInfo, 
                    registrationConfirmation, transaction as DbTransaction);

                // Check if the user is non-corporate. For non-corporate 
                // subscription types, send the confirmation mail. For 
                // corporate send a mail to the admin, indicating that a
                // corporate user has signed up.
                try
                {
                    if (userRegistrationInfo.SubscriptionId != 3)
                        SendConfirmationCodeEmail(firstName, lastName, email.Trim(), Confirmation_Code);
                    else
                        SendAdminIndicationMail(firstName, lastName, email.Trim(), Confirmation_Code);
                }
                catch
                {
                    returnstring = "3";
                }

                transaction.Commit();
                returnstring = "1";
            }
            catch
            {
                if (!Utility.IsNullOrEmpty(transaction))
                    transaction.Rollback();
                returnstring = "4";
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(transaction)) transaction = null;
                if (userRegistrationInfo != null) userRegistrationInfo = null;
                if (registrationConfirmation != null) registrationConfirmation = null;
                try
                {
                    GC.SuppressFinalize(userRegistrationInfo);
                    GC.SuppressFinalize(registrationConfirmation);
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
                
            }
            return returnstring;
        }

        [WebMethod]
        public bool CheckAccountAlreadyExist(string emailID)
        {
            try
            {
                if (string.IsNullOrEmpty(emailID.Trim()))
                {
                    return true;
                }

                Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$", RegexOptions.Compiled);


                if (!regex.IsMatch(emailID.Trim()))
                {
                    return true;
                }

                return !(new UserRegistrationBLManager().CheckUserEmailExists(emailID));
            }
            catch
            {
                return true;
            }
            
        }

        /// <summary>
        /// Method that sends the corporate account request mail to the admin.
        /// </summary>
        /// <param name="firstName">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="lastName">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="email">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="phone">
        /// A <see cref="string"/> that holds the phone.
        /// </param>
        /// <param name="company">
        /// A <see cref="string"/> that holds the company.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the send request status. True 
        /// represents success and false failure.
        /// </returns>
        [WebMethod]
        public bool SendCorporateAccountRequest(string firstName, 
            string lastName, string email, string phone, string company, string title, string additionalQuestions)
        {
            try
            {
                string subject = string.Empty;
                string message = string.Empty;

                // Compose mail.
                ComposeCorporateAccountRequestMail(firstName, lastName, email, 
                    phone, company, title, additionalQuestions, out subject, out message);

                // Send email.
                new EmailHandler().SendMail
                    (ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"],
                    ConfigurationManager.AppSettings["CORPORATE_REQUEST_EMAIL_ADDRESS"],
                    string.Empty, subject, message);

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Method that sends the contact request mail to the admin.
        /// </summary>
        /// <param name="firstName">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="lastName">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="organization">
        /// A <see cref="string"/> that holds the organization.
        /// </param>
        /// <param name="reason">
        /// A <see cref="string"/> that holds the reason.
        /// </param>
        /// <param name="email">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="phone">
        /// A <see cref="string"/> that holds the phone.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the send request status. True 
        /// represents success and false failure.
        /// </returns>
        [WebMethod]
        public bool SendContactRequest(string firstName,
            string lastName, string organization, string reason, string email, string phone)
        {
            try
            {
                string subject = string.Empty;
                string message = string.Empty;

                // Compose mail.
                ComposeContactRequestMail(firstName, lastName, organization, 
                    reason, email, phone, out subject, out message);

                // Send email.
                new EmailHandler().SendMail
                    (ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"],
                    ConfigurationManager.AppSettings["CONTACT_EMAIL_ADDRESS"],
                    string.Empty, subject, message);

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Method that sends the contact request mail to the admin.
        /// </summary>
        /// <param name="firstName">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="lastName">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="email">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="phone">
        /// A <see cref="string"/> that holds the phone.
        /// </param>
        /// <param name="organization">
        /// A <see cref="string"/> that holds the organization.
        /// </param>
        /// <param name="title">
        /// A <see cref="string"/> that holds the title.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the send request status. True
        /// represents success and false failure.
        /// </returns>
        [WebMethod]
        public bool SendWhitepaperDownloadRequest(string firstName,
            string lastName, string email, string phone, string organization,
            string title)
        {
            try
            {
                string subject = string.Empty;
                string message = string.Empty;

                // Compose mail.
                ComposeWhitepaperDownloadRequestMail(firstName, lastName, email, phone, 
                    organization, title, out subject, out message);

                // Send email.
                new EmailHandler().SendMail
                    (ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"],
                    ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"],
                    string.Empty, subject, message);

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }

        /// <summary>
        /// A Method that sends the activation code as email
        /// </summary>
        /// <param name="userEmail">
        /// A <see cref="String"/> that holds the user email
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="String"/> that holds the encrypted 
        /// confirmation code to activate his/her account
        /// </param>
        private void SendConfirmationCodeEmail(string fname,string lname,string userEmail, string confirmationCode)
        {
            // Send mail.
            new EmailHandler().SendMail
                (userEmail, ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"],
                "Activate your ForteHCM account", GetMailBody(fname, lname, userEmail, confirmationCode));
        }
            
         /// <summary>
        /// Method that sends the indication mail to the admin, when a 
        /// corporate user signed up.
        /// </summary>
        /// <param name="userRegistrationInfo">
        /// A <see cref="UserRegistrationInfo"/> that holds the user 
        /// registration info.
        /// </param>
        private void SendAdminIndicationMail(string fname, string lname, string userEmail, string confirmationCode)
        {
            new EmailHandler().SendMail(
                ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"],
                "Corporate subscription account activation request",
                GetMailBody( fname,lname,userEmail, confirmationCode));
        }

        /// <summary>
        /// Method that constructs the body content of the email.
        /// </summary>
        /// A <see cref="String"/> that holds the user email.
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="String"/> that holds the encrypted 
        /// confirmation code to activate his/her account.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that contains the mail body.
        /// </returns>
        private string GetMailBody(string fname,string lname,string userEmail, string confirmationCode)
        {
            StringBuilder sbBody = new StringBuilder();

            try
            {
                sbBody.Append("Dear ");
                sbBody.Append(fname.Trim());
                sbBody.Append(",");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Thank you for signing up. You have 60 days to try all the features of our business edition. Please click on the following link to activate your account.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                string strUri =
                    string.Format(WebConfigurationManager.AppSettings["SIGNUP_ACTIVATIONURL"] + "?" +
                    WebConfigurationManager.AppSettings["SIGNUP_USERNAME"] + "={0}&" +
                    WebConfigurationManager.AppSettings["SIGNUP_CONFIRMATION"] + "={1}",
                    new EncryptAndDecrypt().EncryptString(userEmail), confirmationCode);
                sbBody.Append("<a href='" + strUri + "'");
                sbBody.Append(" >" + strUri + "</a>");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(string.Format("Please contact {0} for any support questions", ConfigurationManager.AppSettings["SUPPORT_EMAIL_ADDRESS"]));
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Thank you,");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("ForteHCM team");

                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        /// <summary>
        /// Method that compose the mail for given parameters.
        /// </summary>
        /// <param name="firstName">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="lastName">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="email">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="phone">
        /// A <see cref="string"/> that holds the phone.
        /// </param>
        /// <param name="company">
        /// A <see cref="string"/> that holds the company.
        /// </param>
        /// <param name="title">
        /// A <see cref="string"/> that holds the title.
        /// </param>
        /// <param name="additionalQuestions">
        /// A <see cref="string"/> that holds the additional questions.
        /// </param>
        /// <param name="subject">
        /// A <see cref="string"/> that holds the subject as an output 
        /// parameter.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message as an output 
        /// parameter.
        /// </param>
        private void ComposeCorporateAccountRequestMail(string firstName, 
            string lastName, string email, string phone, string company, 
            string title, string additionalQuestions, 
            out string subject, out string message)
        {
            // Initiate the output parameters.
            subject = string.Empty;
            message = string.Empty;

            StringBuilder body = new StringBuilder();
            // Compose subject.
            subject = string.Format("New corporate account request");

            // Compose message.
            body.Append("Dear Admin,");
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append("A new corporate account request was submitted");
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append("The details are given below:");
            body.Append(Environment.NewLine);
            body.Append(string.Format("First Name: {0}", firstName));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Last Name: {0}", lastName));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Email: {0}", email));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Phone: {0}", phone));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Company: {0}", company));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Title: {0}", title));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Additional Questions: {0}", additionalQuestions));
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append("Review the details and process the request.");
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);

            // Closure message.
            body.Append(string.Format("This is a system generated message"));
            body.Append(Environment.NewLine);
            body.Append(string.Format("No need to reply to this message"));

            // Get disclaimer message.
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append("============================== Disclaimer ==============================");
            body.Append(Environment.NewLine);
            body.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
            body.Append(Environment.NewLine);
            body.Append("otherwise private information. If you have received it in error, please notify the sender");
            body.Append(Environment.NewLine);
            body.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
            body.Append(Environment.NewLine);
            body.Append("=========================== End Of Disclaimer ============================");

            // Assign the message.
            message = body.ToString();
        }

        /// <summary>
        /// Method that compose the contact request mail for the given 
        /// parameters.
        /// </summary>
        /// <param name="firstName">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="lastName">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="organization">
        /// A <see cref="string"/> that holds the organization.
        /// </param>
        /// <param name="reason">
        /// A <see cref="string"/> that holds the reason.
        /// </param>
        /// <param name="email">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="phone">
        /// A <see cref="string"/> that holds the phone.
        /// </param>
        /// <param name="subject">
        /// A <see cref="string"/> that holds the subject as an output 
        /// parameter.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message as an output 
        /// parameter.
        /// </param>
        private void ComposeContactRequestMail(string firstName,
            string lastName, string organization, string reason, string email, 
            string phone, out string subject, out string message)
        {
            // Initiate the output parameters.
            subject = string.Empty;
            message = string.Empty;

            StringBuilder body = new StringBuilder();
            // Compose subject.
            subject = string.Format("New contact request");

            // Compose message.
            body.Append("Dear Admin,");
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append("A new contact request was submitted");
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append("The details are given below:");
            body.Append(Environment.NewLine);
            body.Append(string.Format("First Name: {0}", firstName));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Last Name: {0}", lastName));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Organization: {0}", organization));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Reason: {0}", reason));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Email: {0}", email));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Phone: {0}", phone));
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);

            // Closure message.
            body.Append(string.Format("This is a system generated message"));
            body.Append(Environment.NewLine);
            body.Append(string.Format("No need to reply to this message"));

            // Get disclaimer message.
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append("============================== Disclaimer ==============================");
            body.Append(Environment.NewLine);
            body.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
            body.Append(Environment.NewLine);
            body.Append("otherwise private information. If you have received it in error, please notify the sender");
            body.Append(Environment.NewLine);
            body.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
            body.Append(Environment.NewLine);
            body.Append("=========================== End Of Disclaimer ============================");

            // Assign the message.
            message = body.ToString();
        }

        /// <summary>
        /// Method that compose the white paper download request mail for the
        /// given parameters.
        /// </summary>
        /// <param name="firstName">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="lastName">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="email">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="phone">
        /// A <see cref="string"/> that holds the phone.
        /// </param>
        /// <param name="organization">
        /// A <see cref="string"/> that holds the organization.
        /// </param>
        /// <param name="title">
        /// A <see cref="string"/> that holds the title.
        /// </param>
        /// <param name="subject">
        /// A <see cref="string"/> that holds the subject as an output 
        /// parameter.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message as an output 
        /// parameter.
        /// </param>
        private void ComposeWhitepaperDownloadRequestMail(string firstName,
            string lastName, string email, string phone, string organization, 
            string title, out string subject, out string message)
        {
            // Initiate the output parameters.
            subject = string.Empty;
            message = string.Empty;

            StringBuilder body = new StringBuilder();
            // Compose subject.
            subject = string.Format("White paper request");

            // Compose message.
            body.Append("Dear Admin,");
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append("A white paper request was submitted");
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append("The details are given below:");
            body.Append(Environment.NewLine);
            body.Append(string.Format("First Name: {0}", firstName));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Last Name: {0}", lastName));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Email: {0}", email));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Phone: {0}", phone));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Organization: {0}", organization));
            body.Append(Environment.NewLine);
            body.Append(string.Format("Title: {0}", title));
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);

            // Closure message.
            body.Append(string.Format("This is a system generated message"));
            body.Append(Environment.NewLine);
            body.Append(string.Format("No need to reply to this message"));

            // Get disclaimer message.
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append("============================== Disclaimer ==============================");
            body.Append(Environment.NewLine);
            body.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
            body.Append(Environment.NewLine);
            body.Append("otherwise private information. If you have received it in error, please notify the sender");
            body.Append(Environment.NewLine);
            body.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
            body.Append(Environment.NewLine);
            body.Append("=========================== End Of Disclaimer ============================");

            // Assign the message.
            message = body.ToString();
        }

        /// <summary>
        /// Method that sends the password to requested user
        /// </summary>
        /// <param name="UserDetail">
        /// A <see cref="userDetail"/> that holds the user deail object
        /// detail.
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendPasswordToUser(UserDetail userDetail)
        {
            try
            {
                new EmailHandler().SendMail(EntityType.ForgotPassword, userDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #region Asynchronous Method Handlers

        /// <summary>
        /// Handler method that acts as the callback method for send mail.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendPasswordToUserCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Asynchronous Method Handlers
    }
}