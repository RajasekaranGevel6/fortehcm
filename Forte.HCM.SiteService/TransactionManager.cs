﻿using System;
using System.Data;
using System.Data.Common;

namespace Forte.HCM.SiteService
{
    public class TransactionManager : DatabaseConnectionManager
    {
        private IDbTransaction sqlTransaction;
        private DbConnection dbConnection;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionManager"/> class.
        /// </summary>
        public TransactionManager()
        {
            try
            {
                dbConnection = HCMDatabase.CreateConnection();

                try
                {
                    dbConnection.Open();
                }
                catch
                {
                    throw new Exception("Unable to open connection.");
                }

                sqlTransaction = dbConnection.BeginTransaction();
            }
            catch
            {
                try
                {
                    if (dbConnection.State != ConnectionState.Closed)
                        dbConnection.Close();
                }
                catch
                {
                    throw;
                }

                throw;
            }
        }

        /// <summary>
        /// Gets the transaction.
        /// </summary>
        /// <value>The transaction.</value>
        public IDbTransaction Transaction
        {
            get
            {
                return sqlTransaction;
            }
        }

        /// <summary>
        /// Method to Commit a Transaction
        /// </summary>
        public void Commit()
        {
            try
            {
                sqlTransaction.Commit();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (sqlTransaction.Connection != null)
                {
                    if (sqlTransaction.Connection.State == ConnectionState.Open)
                        sqlTransaction.Connection.Close();

                    sqlTransaction.Connection.Dispose();
                }
                sqlTransaction.Dispose();
            }
        }

        /// <summary>
        /// Rollbacks this instance.
        /// </summary>
        public void Rollback()
        {
            try
            {
                sqlTransaction.Rollback();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (sqlTransaction.Connection != null)
                {
                    if (sqlTransaction.Connection.State == ConnectionState.Open)
                        sqlTransaction.Connection.Close();

                    sqlTransaction.Connection.Dispose();
                }
                sqlTransaction.Dispose();
            }
        }
    }
}
