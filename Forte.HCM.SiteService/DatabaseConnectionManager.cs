﻿using System;
using System.Data;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;


namespace Forte.HCM.SiteService
{
    /// <summary>
    /// Represents the class that assists in creating database
    /// connection.
    /// </summary>
    public class DatabaseConnectionManager
    {
        /// <summary>
        /// A <see cref="Database"/> that holds the database object.
        /// </summary>
        private static Database hcmDatabase;

        /// <summary>
        /// Property that sets or gets the <see cref="Database"/> object.
        /// </summary>
        /// <value>
        /// A <see cref="Database"/> that holds the database object.
        /// </value>
        public static Database HCMDatabase
        {
            get
            {
                if (hcmDatabase == null)
                {
                    CreateConnection();
                }
                return hcmDatabase;
            }
        }

        /// <summary>
        /// Method that creates the connection to the database.
        /// </summary>
        private static void CreateConnection()
        {
            try
            {
                hcmDatabase = DatabaseFactory.CreateDatabase
                  (DbUtility.ConnectionStringName);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to obtain a connection", exception);
            }
        }

        /// <summary>
        /// Method to insert the user log details
        /// </summary>
        /// <param name="userName">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="ipAddress">
        /// A <see cref="string"/> that holds the ip address.
        /// </param>
        public void InsertUserLog(string userName, string ipAddress)
        {
            // Create a stored procedure command object
            DbCommand inserUserLogCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_USER_LOG");

            // Add input parameters.
            HCMDatabase.AddInParameter(inserUserLogCommand,
                "@USER_EMAIL", DbType.String, userName);

            HCMDatabase.AddInParameter(inserUserLogCommand,
                "@IP_ADDRESS", DbType.String, ipAddress);

            // Execute the query.
            HCMDatabase.ExecuteNonQuery(inserUserLogCommand);
        }
    }
}
