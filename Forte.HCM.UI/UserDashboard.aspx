﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserDashboard.aspx.cs" MasterPageFile="~/MasterPages/ModuleHomeMaster.Master"
    Inherits="Forte.HCM.UI.UserDashboard" Title="ForteHCM Applications" %>
    <%@ Register Src="~/CommonControls/TestScheduleControl.ascx" TagName="TestScheduleControl"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/ModuleHomeMaster.Master" %>
<asp:Content ID="UserDashboard_bodyContent" ContentPlaceHolderID="ModuleHomeMaster_body"
    runat="server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="td_pading_logo_10">
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="UserDashboard_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="UserDashboard_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="dash_body_bg" style="height: 100%">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" style="height: 100%;
                    table-layout: fixed">
                    <tr>
                        <td style="width: 45%">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="width: 114px" class="db_left_bg_blue" align="center">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="UserDashboard_PPLeftIcon" runat="server" SkinID="sknbtnPPLeftIcon"
                                                        OnCommand="UserDashboard_icon_Command" CommandName="CreatePositionProfile" ToolTip="Create Position Profile" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label runat="server" SkinID="sknLabelHeaderDashboard" Text="Position Profile"
                                                        ID="UserDashboard_PPLeftLabel" ToolTip="Create Position Profile"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="dash_center_bg" valign="top">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td class="db_left_bg" style="width: 12px" align="right">
                                                            </td>
                                                            <td class="dash_icon_bg_center" align="right">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="UserDashboard_createPPIconImageButton" SkinID="sknCreatePPImageButton"
                                                                                runat="server" OnCommand="UserDashboard_icon_Command" CommandName="CreatePositionProfile"
                                                                                ToolTip="Create Position Profile" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label runat="server" SkinID="sknLabelFieldDashboard" Text="Create" ID="UserDashboard_createPPLabel"
                                                                                ToolTip="Create Position Profile"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="db_right_bg" style="width: 12px" align="left">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 20px" width="100%">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td width="100%">
                                                                <div class="db_content_div">
                                                                    <asp:GridView ID="UserDashboard_positionProfileGridView" runat="server" AllowSorting="True"
                                                                        AutoGenerateColumns="False" Width="100%" SkinID="sknWrapHeaderGrid" ShowHeader="false"
                                                                        OnRowCommand="UserDashboard_gridView_RowCommand">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="UserDashboard_positionProfileGridView_viewPositionProfileImageButton"
                                                                                        runat="server" SkinID="sknEditFormImageButton" ToolTip="Edit Position Profile"
                                                                                        CommandName="EditPositionProfile" CommandArgument='<% # Eval("PositionProfileID") %>' />
                                                                                    <asp:ImageButton ID="UserDashboard_positionProfileGridView_editPositionProfileImageButton"
                                                                                        runat="server" SkinID="sknViewPositionProfile" ToolTip="View Position Profile"
                                                                                        CommandName="ViewPositionProfile" CommandArgument='<% # Eval("PositionProfileID") %>' />
                                                                                    <asp:ImageButton ID="UserDashboard_positionProfileGridView_createImageButton" runat="server"
                                                                                        SkinID="sknCreatePositionProfileImageButton" ToolTip="Create Position Profile"
                                                                                        CommandName="EditPositionProfile" CommandArgument='<% # Eval("PositionProfileID") %>' />
                                                                                    <asp:ImageButton ID="UserDashboard_positionProfileGridView_talentSearchPositionProfileImageButton"
                                                                                        runat="server" SkinID="sknTalentSearchImageButton" ToolTip="intelliSPOT Search" CommandName="TalentSearch"
                                                                                        CommandArgument='<% # Eval("PositionProfileID") %>' />
                                                                                    <asp:ImageButton ID="UserDashboard_positionProfileGridView_authorTestsearchExistingTestImageButton"
                                                                                        runat="server" SkinID="sknSearchExistingTestImageButton" ToolTip="Search Existing Test"
                                                                                        CommandName="SearchTest" CommandArgument='<% # Eval("PositionProfileID") %>' />
                                                                                </ItemTemplate>
                                                                                <ItemStyle Wrap="false" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Position Name">
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" Text='<%# TrimContent(Eval("PositionProfileName").ToString(),20) %>'
                                                                                        ToolTip='<%# Eval("PositionProfileName") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Client Name">
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" Text='<%# TrimContent(Eval("ClientName").ToString(),20) %>'
                                                                                        ToolTip='<%# Eval("ClientName") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:LinkButton ID="UserDashboard_positionProfileMoreLinkButton" SkinID="sknDashBoardMoreLinkButton"
                                                        runat="server" ToolTip="More Position Profile " CommandName="SearchPositionProfile" OnCommand="UserDashboard_moreLinkButton_Command">
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="db_right_bg_blue" style="width: 25px" valign="top">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 10%">
                        </td>
                        <td style="width: 45%">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="width: 114px" class="db_left_bg_green" align="center">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="UserDashboard_testLeftIconImageButton" runat="server" SkinID="sknbtnTestLeftIcon"
                                                        OnCommand="UserDashboard_icon_Command" CommandName="CreateManualTest" ToolTip="Create Test" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label runat="server" SkinID="sknLabelHeaderDashboard" Text="Test" ID="UserDashboard_testLabel"
                                                        ToolTip="Create Test"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 68%" class="dash_center_bg" valign="top">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td class="db_left_bg" style="width: 12px" align="right">
                                                            </td>
                                                            <td class="dash_icon_bg_center" align="right">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="UserDashboard_createAutomatedTestImageButton" runat="server"
                                                                                SkinID="sknCreateAutomatedTestImageButton" OnCommand="UserDashboard_icon_Command"
                                                                                CommandName="CreateAutomatedTest" ToolTip="Create Automated Test" />
                                                                        </td>
                                                                        <td style="padding-right: 5px">
                                                                            <asp:Label ID="UserDashboard_createAutomatedTestLabel" ToolTip="Create Automated Test"
                                                                                runat="server" SkinID="sknLabelFieldDashboard" Text="Automated"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="UserDashboard_createManualTestImageButton" SkinID="sknCreateManualTestImageButton"
                                                                                runat="server" ToolTip="Create Manual Test" OnCommand="UserDashboard_icon_Command"
                                                                                CommandName="CreateManualTest" />
                                                                        </td>
                                                                        <td style="padding-right: 5px">
                                                                            <asp:Label runat="server" SkinID="sknLabelFieldDashboard" Text="Manual" ID="UserDashboard_createManualTestLabel"
                                                                                ToolTip="Create Manual Test"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="UserDashboard_searchTestImageButton" SkinID="sknSearchTestImageButton"
                                                                                runat="server" ToolTip="Search Test" OnCommand="UserDashboard_icon_Command" CommandName="SearchTest" />
                                                                        </td>
                                                                        <td style="padding-right: 5px">
                                                                            <asp:Label runat="server" SkinID="sknLabelFieldDashboard" Text="Search" ID="UserDashboard_searchTestLabel"
                                                                                ToolTip="Search Test"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="db_right_bg" style="width: 12px" align="right">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 20px">
                                                    <div class="db_content_div">
                                                        <asp:GridView ID="UserDashboard_testGridView" runat="server" AllowSorting="True"
                                                            AutoGenerateColumns="False" Width="100%" SkinID="sknWrapHeaderGrid" ShowHeader="false"
                                                            OnRowCommand="UserDashboard_gridView_RowCommand">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton runat="server" SkinID="sknDashBoardEditTestImageButton" ToolTip="Edit Test"
                                                                            CommandName="EditTest" CommandArgument='<% # Eval("TestKey") %>' />
                                                                        <asp:ImageButton runat="server" SkinID="sknDashBoardCreateSessiomImageButton" ToolTip="Create Test Session"
                                                                            CommandName="CreateTestSession" CommandArgument='<% # Eval("TestKey") %>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle Wrap="false" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Test Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" Text='<%# TrimContent(Eval("TestName").ToString(),30) %>'
                                                                            ToolTip='<%# Eval("TestName") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:LinkButton ID="UserDashboard_testMoreLinkButton" SkinID="sknDashBoardMoreLinkButton"
                                                        CommandName="SearchTest" OnCommand="UserDashboard_moreLinkButton_Command" runat="server"
                                                        ToolTip="More Tests"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 25px" valign="top" class="db_right_bg_green">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="width: 114px" class="dash_left_bg_pink" align="center">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="center">
                                                    <asp:ImageButton ID="UserDashboard_RRleftIconImageButton" runat="server" SkinID="sknbtnRRLeftIcon"
                                                        OnCommand="UserDashboard_icon_Command" CommandName="ResumeRepository" ToolTip="Resume Repository" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label runat="server" SkinID="sknLabelHeaderDashboard" Text="Resume Repository"
                                                        ID="UserDashboard_resumeRespositoryLeftLabel" ToolTip="Resume Repository"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 68%" class="dash_center_bg" valign="top">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td class="db_left_bg" style="width: 12px" align="right">
                                                            </td>
                                                            <td class="dash_icon_bg_center" align="right">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="UserDashboard_uploadResumeImageButton" runat="server" 
                                                                            OnCommand="UserDashboard_icon_Command" CommandName="UploadResume" ToolTip="Upload Resume"
                                                                            SkinID="sknDashBoardUploadResumeImageButton" />
                                                                        </td>
                                                                        <td style="padding-right: 5PX">
                                                                            <asp:Label runat="server" SkinID="sknLabelFieldDashboard" Text="Upload Resume" ID="UserDashboard_uploadResumelabel"
                                                                             ToolTip="Search Upload Resume"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="UserDashboard_viewResumeRepositoryImageButton" ToolTip="View Resume Repository" 
                                                                            OnCommand="UserDashboard_icon_Command" CommandName="ViewResumeRepository"
                                                                            runat="server" SkinID="sknDashBoardViewResumeRepositoryImageButton" />
                                                                        </td>
                                                                        <td style="padding-right: 5PX">
                                                                            <asp:Label runat="server" SkinID="sknLabelFieldDashboard" Text="View Repository" ToolTip="View Resume Repository" 
                                                                                ID="UserDashboard_viewResumeRepositoryLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="db_right_bg" style="width: 12px" align="right">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 20px">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                Uploaded resume
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                Downloaded resume
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="db_resume_upload_content_div">
                                                                    <asp:GridView ID="UserDashboard_uploadedResumeGridView" runat="server" AllowSorting="True"
                                                                        AutoGenerateColumns="False" Width="100%" SkinID="sknWrapHeaderGrid" ShowHeader="false"
                                                                        OnRowCommand="UserDashboard_gridView_RowCommand">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="UserDashboard_testGridView_viewPositionProfileImageButton" runat="server"
                                                                                        SkinID="sknCandidateDetailImageButton" CommandArgument='<%# Eval("CandidateID") %>' ToolTip="View Candidate" CommandName="ViewCandidate"
                                                                                        />
                                                                                </ItemTemplate>
                                                                                <ItemStyle Wrap="false" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="First Name">
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" Text='<%# TrimContent(Eval("FirstName").ToString(),15) %>'
                                                                                        ToolTip='<%# Eval("FirstName") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Last Name">
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" Text='<%# TrimContent(Eval("LastName").ToString(),15) %>'
                                                                                        ToolTip='<%# Eval("LastName") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <div class="db_resume_download_content_div">
                                                                    <asp:GridView ID="UserDashboard_downloadedResumeGridView" runat="server" AllowSorting="True"
                                                                        AutoGenerateColumns="False" Width="100%" SkinID="sknWrapHeaderGrid" ShowHeader="false"
                                                                        OnRowCommand="UserDashboard_gridView_RowCommand">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="UserDashboard_testGridView_viewPositionProfileImageButton" runat="server"
                                                                                        SkinID="sknCandidateDetailImageButton" CommandArgument='<%# Eval("CandidateID") %>' ToolTip="View Candidate" CommandName="ViewCandidate" />
                                                                                </ItemTemplate>
                                                                                <ItemStyle Wrap="false" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="First Name">
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" Text='<%# TrimContent(Eval("FirstName").ToString(),15) %>'
                                                                                        ToolTip='<%# Eval("FirstName") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Last Name">
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" Text='<%# TrimContent(Eval("LastName").ToString(),15) %>'
                                                                                        ToolTip='<%# Eval("LastName") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:LinkButton ID="UserDashboard_resumeMoreLinkButton" SkinID="sknDashBoardMoreLinkButton"
                                                        runat="server" ToolTip="More Candidate " CommandName="SearchCandidate" OnCommand="UserDashboard_moreLinkButton_Command">
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 25px" valign="top" class="db_right_bg_pink">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="width: 114px" class="db_left_bg_green" align="center">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="center">
                                                    <asp:ImageButton ID="UserDashboard_SCleftIconImageButon" runat="server" SkinID="sknbtnSCLeftIcon"
                                                        OnCommand="UserDashboard_icon_Command" CommandName="TestScheduler" ToolTip="Test Scheduler" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label runat="server" SkinID="sknLabelHeaderDashboard" Text="Schedule Candidate"
                                                        ID="UserDashboard_scheduleCandidateLeftLabel"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 68%" class="dash_center_bg" valign="top">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td class="db_left_bg" style="width: 12px" align="right">
                                                            </td>
                                                            <td class="dash_icon_bg_center" align="right">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="UserDashboard_createTestSessionIconImageButon" runat="server"
                                                                                SkinID="sknDashBoardSearchTestSessionImageButton" CommandName="SearchTestSession"
                                                                                OnCommand="UserDashboard_icon_Command" ToolTip="Search Test Session" />
                                                                        </td>
                                                                        <td style="padding-right: 5PX">
                                                                            <asp:Label runat="server" SkinID="sknLabelFieldDashboard" Text="Test Session" ID="UserDashboard_testSessionLabel"
                                                                                ToolTip="Search Test Session"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="db_right_bg" style="width: 12px" align="right">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 20px">
                                                    <div class="db_content_div">
                                                        <asp:GridView ID="UserDashboard_testSessionGridView" runat="server" AllowSorting="True"
                                                            AutoGenerateColumns="False" Width="100%" SkinID="sknWrapHeaderGrid" ShowHeader="false"
                                                            OnRowCommand="UserDashboard_gridView_RowCommand">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="UserDashboard_testGridView_viewPositionProfileImageButton" runat="server"
                                                                            SkinID="sknDashBoardCandidateSessionImageButton" ToolTip="ViewScheldule" CommandName="ViewScheldule"
                                                                            CommandArgument='<% # Eval("CandidateDetails") %>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle Wrap="false" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Candidate Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" Text='<%# TrimContent(Eval("CandidateName").ToString(),15) %>'
                                                                            ToolTip='<%# Eval("CandidateName") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Test Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" Text='<%# TrimContent(Eval("TestName").ToString(),15) %>'
                                                                            ToolTip='<%# Eval("TestName") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Session Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" Text='<%# TrimContent(Eval("SessionName").ToString(),15) %>'
                                                                            ToolTip='<%# Eval("SessionName") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:LinkButton ID="UserDashboard_candidateSessionMoreLinkButton" SkinID="sknDashBoardMoreLinkButton"
                                                        runat="server" CommandName="SearchTestSession" OnCommand="UserDashboard_icon_Command"
                                                        ToolTip="More Test Session"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 25px" valign="top" class="db_right_bg_green">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="width: 114px" class="db_left_bg_orange" align="center">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="UserDashboard_TSLeftIconImageButton" SkinID="sknbtnTSLeftIcon"
                                                        runat="server" OnCommand="UserDashboard_icon_Command" CommandName="TalentSearch" ToolTip="intelliSPOT Search" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label runat="server" SkinID="sknLabelHeaderDashboard" Text="Talent Scout" 
                                                    ID="UserDashboard_talentScoutLeftLabel" ToolTip="intelliSPOT Search"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 68%" class="dash_center_bg" valign="top">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td class="db_left_bg" style="width: 12px" align="right">
                                                            </td>
                                                            <td class="dash_icon_bg_center" align="right">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="UserDashboard_talentSearchIconImageButton" runat="server" SkinID="sknDashBoardTalentSearchImageButton"
                                                                                OnCommand="UserDashboard_icon_Command" CommandName="TalentSearch" ToolTip="intelliSPOT Search" />
                                                                        </td>
                                                                        <td style="padding-right: 5px">
                                                                            <asp:Label runat="server" SkinID="sknLabelFieldDashboard" Text="intelliSPOT Search" ID="UserDashboard_talentSearchLabel"
                                                                                ToolTip="intelliSPOT Search"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="db_right_bg" style="width: 12px" align="right">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 20px">
                                                    <div class="db_content_div">
                                                        <asp:GridView ID="UserDashboard_talentSearchGridView" runat="server" AllowSorting="True"
                                                            AutoGenerateColumns="False" Width="100%" SkinID="sknWrapHeaderGrid" ShowHeader="false"
                                                            OnRowCommand="UserDashboard_gridView_RowCommand">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="UserDashboard_testGridView_viewPositionProfileImageButton" runat="server"
                                                                            SkinID="sknDashBoardTalentSearchKeyImageButton" ToolTip="intelliSPOT Search " CommandName="SearchTerm"
                                                                            CommandArgument='<% # Eval("SearchTearms") %>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle Wrap="false" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Candidate Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" Text='<%# TrimContent(Eval("SearchTearms").ToString(),15) %>'
                                                                            ToolTip='<%# Eval("SearchTearms") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Position Profile Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" Text='<%# TrimContent(Eval("PositionProfileName").ToString(),15) %>'
                                                                            ToolTip='<%# Eval("PositionProfileName") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:LinkButton ID="UserDashboard_talentSearchMoreLinkButton" SkinID="sknDashBoardMoreLinkButton"
                                                        runat="server" ToolTip="More intelliSPOT Search " CommandName="TalentSearch" OnCommand="UserDashboard_moreLinkButton_Command"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 25px" valign="top" class="db_right_bg_orange">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="width: 114px" class="db_left_bg_green" align="center">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="UserDashboard_reportLeftIconImageButton" runat="server" SkinID="sknbtnReportLeftIcon"
                                                        OnCommand="UserDashboard_icon_Command" CommandName="TestReport" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label runat="server" SkinID="sknLabelHeaderDashboard" Text="Reports" ID="UserDashboard_reportsLeftLabel"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 68%" class="dash_center_bg" valign="top">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td class="db_left_bg" style="width: 12px" align="right">
                                                            </td>
                                                            <td class="dash_icon_bg_center" align="right">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="UserDashboard_testReportIconImageButon" runat="server" SkinID="sknDashBoardTestReportImageButton"
                                                                                OnCommand="UserDashboard_icon_Command" CommandName="TestReport" ToolTip="Test Report" />
                                                                        </td>
                                                                        <td style="padding-right: 5px">
                                                                            <asp:Label runat="server" SkinID="sknLabelFieldDashboard" Text="Test Report" ID="UserDashboard_testReportLabel"
                                                                                ToolTip="Test Report"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="UserDashboard_testCandidateReportIconImageButon" OnCommand="UserDashboard_icon_Command"
                                                                                CommandName="CandidateReport" SkinID="sknDashBoardTestCandidateReportImageButton"
                                                                                runat="server" ToolTip="Test Candidate Report" />
                                                                        </td>
                                                                        <td style="padding-right: 5px">
                                                                            <asp:Label runat="server" SkinID="sknLabelFieldDashboard" Text="Candidate Report"
                                                                                ToolTip="Test Candidate Report" ID="UserDashboard_testCandidateReportIconLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="db_right_bg" style="width: 12px" align="right">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 20px">
                                                    <div class="db_content_div">
                                                        <asp:GridView ID="UserDashboard_testReportGridView" runat="server" AllowSorting="True"
                                                            AutoGenerateColumns="False" Width="100%" SkinID="sknWrapHeaderGrid" ShowHeader="false"
                                                            OnRowCommand="UserDashboard_gridView_RowCommand">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="UserDashboard_testGridView_viewPositionProfileImageButton" runat="server"
                                                                            SkinID="sknDashBoardViewReportImageButton" ToolTip="View Test Report" CommandName="ReportStatistics"
                                                                            CommandArgument='<% # Eval("CandidateDetails") %>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle Wrap="false" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Candidate Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label1" runat="server" Text='<%# TrimContent(Eval("CandidateName").ToString(),15) %>'
                                                                            ToolTip='<%# Eval("CandidateName") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Test Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label2" runat="server" Text='<%# TrimContent(Eval("TestName").ToString(),15) %>'
                                                                            ToolTip='<%# Eval("TestName") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:LinkButton ID="UserDashboard_testReportMoreLinkButton" SkinID="sknDashBoardMoreLinkButton"
                                                        runat="server" ToolTip="More Test Report" CommandName="TestReport" OnCommand="UserDashboard_moreLinkButton_Command"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 25px" valign="top" class="db_right_bg_green">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UserDashboard_previewProfileUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="UserDashboard_PreviewPopupPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_segment_Controls_preview">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align: center">
                    <tr>
                        <td class="td_height_20">
                            <div style="display: none">
                                <asp:Button ID="UserDashboard_hiddenPreviewModalButton" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <table width="95%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 85%" class="popup_header_text" valign="middle" align="left">
                                        <asp:Label ID="UserDashboard_placeHolderLiteral" runat="server" Text="View Position Profile "></asp:Label>
                                    </td>
                                    <td style="width: 15%" align="right">
                                        <asp:ImageButton ID="UserDashboard_placeHolderCloseImageButton" runat="server" SkinID="sknCloseImageButton" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_20" colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_panel_inner_bg">
                                        <div id="content" style="height: 450px; overflow: auto;">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" style="width: 100%">
                                                        <asp:Panel ID="UserDashboard_previewPopupInnerPanel" runat="server">
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">
                                        <asp:LinkButton ID="UserDashboard_controlsPlaceHolderCancelButton" runat="server"
                                            Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="UserDashboard_previewPopupExtender" runat="server"
                PopupControlID="UserDashboard_PreviewPopupPanel" TargetControlID="UserDashboard_hiddenPreviewModalButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
              <asp:Panel ID="UserDashboard_hiddenViewSchedulePanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_question_detail">
                    
                    <uc1:TestScheduleControl ID="UserDashboard_viewTestSchedule" runat="server" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="UserDashboard_viewScheduleModalPopupExtender"
                    runat="server" PopupControlID="UserDashboard_hiddenViewSchedulePanel" TargetControlID="UserDashboard_hiddenPreviewModalButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
