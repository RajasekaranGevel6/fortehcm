﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master"
CodeBehind="ResumeRepositoryHome.aspx.cs" Inherits="Forte.HCM.UI.ResumeRepositoryHome" %>

<%@ MasterType VirtualPath="~/MasterPages/ResumeRepositoryMaster.Master" %>
<asp:Content ID="ResumeRepositoryHome_bodyContent" runat="server" ContentPlaceHolderID="ResumeRepositoryMaster_body">
    <table width="972px" border="0" cellspacing="0" cellpadding="0" class="table_bg"
        align="center">
        <tr>
            <td align="center" class="title_home_icon">
                <h2>
                    Resume Repository Home
                </h2>
            </td>
        </tr>
        <tr>
            <td align="center">
                <img src="App_Themes/DefaultTheme/Images/Resume_Repository_Tool.png" alt="Resume Repository"
                    height="226px" width="234px" />
            </td>
        </tr>
    </table>
</asp:Content>