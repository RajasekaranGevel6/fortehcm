﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/HomeMaster.Master"
    CodeBehind="AboutUs.aspx.cs" Inherits="Forte.HCM.UI.General.AboutUs" %>

<%@ MasterType VirtualPath="~/MasterPages/HomeMaster.Master" %>
<asp:Content ID="AboutUs_bodyContent" ContentPlaceHolderID="HomeMaster_body" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0" class="td_pading_logo_10">
        <tr>
            <td class="header_bg">
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td width="90%" class="header_text_bold">
                            <asp:Literal ID="AboutUs_headerLiteral" runat="server" Text="About Us"></asp:Literal>
                        </td>
                        <td width="10%" align="right">
                            <table cellpadding="3" cellspacing="4" width="100%">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="AboutUs_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="AboutUs_cancelLinkButton_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="AboutUs_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="AboutUs_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" style="color: #81868A; font-size: small">
                            <b>Explore Geval6! </b>
                            <br />
                            Geval6, the 'IT Asset Performance Services' organization, provides Professional
                            Consulting, Custom Projects, and Professional Resource Augmentation services to
                            clients in the Mid-West and beyond. The strategic characteristics of the organization
                            are Standardization, Specialization, and Collaboration. 
                            <br />
                            <br />
                            We have Specialization at the technology level and the service level. The core technology specialization areas
                            are Data Asset Performance and Enterprise Application Asset Performance; Software
                            Architecture and Enterprise Content Management are additional areas of focus. At
                            the service level, our Professional Resources Augmentation Service (PRAS) is a unique
                            specialized combination of systems, technology/ tools, and collaboration network
                            deployed to bring one of the best resource provision services in the industry. Read
                            more about this in the Services section. 
                            <br />
                            <br />
                            Collaboration is a key ingredient of our delivery strategy as it increases our reach to qualified resources and service options
                            exponentially. Consistent and formal efforts help us build and expand the network
                            of specialist resources that we can call upon. 
                            <br />
                            <br />
                            Geval6 sees itself as an entity destined to deliver increasingly substantial value to all those it serves; we work with the
                            intent of being those who make very positive difference in the sphere we operate
                            in. 
                            <br />
                            <br />
                            <b>Our History & Name: </b>
                            <br/>
                            Formed in 2002 as a Data Management Services entity we have
                            developed in to a service organization with a richer set of services & solutions
                            and a growing client and resource base. We are headquartered in Hoffman Estates,
                            IL with additional presence in Somerset, NJ. Our offshore back office facility is
                            in Chennai, India. 
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
