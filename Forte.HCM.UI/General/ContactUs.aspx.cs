﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Forte.HCM.Trace;
using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.General
{
    public partial class ContactUs : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Contact Us");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ContactUs_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the ContactUs_cancelLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ContactUs_cancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["USER_DETAIL"] == null)
                    Response.Redirect("~/Default.aspx", false);

                UserDetail userDetails = Session["USER_DETAIL"] as UserDetail;

                Response.Redirect("~/Default.aspx", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ContactUs_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the method that shows the user message.
        /// </summary>
        /// <param name="topLabel">
        /// A <see cref="Label"/> that holds the top label control.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        private void ShowMessage(Label label, string message)
        {
            // Set the message to the label.
            if (label.Text.Length == 0)
            {
                label.Text = message;
            }
            else
            {
                label.Text += "<br>" + message;
            }
        }
    }
}