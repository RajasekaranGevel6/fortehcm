﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HomeMaster.Master"
    AutoEventWireup="true" CodeBehind="Feedback.aspx.cs" Inherits="Forte.HCM.UI.General.Feedback" %>

<%@ MasterType VirtualPath="~/MasterPages/HomeMaster.Master" %>
<asp:Content ID="Feedback_bodyContent" ContentPlaceHolderID="HomeMaster_body" runat="server">
    <script type="text/javascript" language="javascript">

        // Method that will show/hide the captcha image help panel.
        function ShowWhatsThis(show) {
            if (show == 'Y') {
                $find('Feedback_captchaHelpTextModalPopUpExtender').show();
                //document.getElementById('SignUp_whatsThisDiv').style.display = "block";
            }

            else {
                //document.getElementById('SignUp_whatsThisDiv').style.display = "none";
                $find('Feedback_captchaHelpTextModalPopUpExtender').hide();
            }
            return false;
        }
        
    </script>
    <table width="100%" cellpadding="0" cellspacing="0" class="td_pading_logo_10">
        <tr>
            <td class="header_bg">
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td width="90%" class="header_text_bold">
                            <asp:Literal ID="Feedback_headerLiteral" runat="server" Text="Feedback"></asp:Literal>
                        </td>
                        <td width="10%" align="right">
                            <table cellpadding="3" cellspacing="4" width="100%">
                                <tr>
                                    <td align="right">
                                        <asp:LinkButton ID="Feedback_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Reset" OnClick="Feedback_resetLinkButton_Click" />
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="Feedback_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="Feedback_cancelLinkButton_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="Feedback_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="Feedback_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 60%">
                            <table width="100%" cellpadding="2" cellspacing="2" border="0">
                                <tr>
                                    <td align="left" colspan="3" class="feedback_instructions_title">
                                        Instructions:
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="3" class="feedback_instructions">
                                        1. Items marked as <span class="mandatory">*</span> are mandatory
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="3" class="feedback_instructions">
                                        2. Security image ensures that the form is being submitted by a human being
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Feedback_nameLabel" runat="server" Text="First Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 25px;" align="right">
                                        <span class="mandatory">*</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Feedback_nameTextBox" runat="server" Width="60%" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Feedback_lastNameLabel" runat="server" Text="Last Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 25px;" align="right">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Feedback_lastNameTextBox" runat="server" Width="60%" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Feedback_emailLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 25px;" align="right">
                                        <span class="mandatory">*</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Feedback_emailTextBox" runat="server" Width="60%" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 1%">
                                        <asp:Label ID="Feedback_phoneNumberLabel" runat="server" Text="Phone Number" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 25px;" align="right">
                                        <span class="mandatory">*</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Feedback_phoneNumberTextBox" runat="server" Width="30%" MaxLength="15"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Feedback_subjectLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 25px;" align="right">
                                        <span class="mandatory">*</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Feedback_subjectDropDownList" runat="server" Width="32%">
                                            <asp:ListItem Text="--Select--" Value="0">
                                            </asp:ListItem>
                                            <asp:ListItem Text="General Comment" Value="1">
                                            </asp:ListItem>
                                            <asp:ListItem Text="Feed back on a feature" Value="2">
                                            </asp:ListItem>
                                            <asp:ListItem Text="Suggest a feature" Value="3">
                                            </asp:ListItem>
                                            <asp:ListItem Text="Report a bug" Value="4">
                                            </asp:ListItem>
                                            <asp:ListItem Text="Others" Value="5">
                                            </asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Label ID="Feedback_commentLabel" runat="server" Text="Comments" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 25px;" align="right" valign="top">
                                        <span class="mandatory">*</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Feedback_commentsTextBox" runat="server" Columns="65" TextMode="MultiLine"
                                            Height="100" MaxLength="1000" onkeyup="CommentsCount(1000,this)" onchange="CommentsCount(1000,this)"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left">
                                        <asp:UpdatePanel ID="Feedback_updatePanel" runat="server">
                                            <ContentTemplate>
                                                <input type="hidden" runat="server" id="Feedback_countHidden" value="0" />
                                                <img id="Feedback_capchaImage" src="" runat="server" alt="Captcha Image" />
                                                &nbsp;
                                                <asp:LinkButton ID="Feedback_regenerateCaptchaImageLinkButton" runat="server" Text="Regenerate Key"
                                                    CssClass="link_button_hcm" OnClick="Feedback_regenerateCaptchaImageLinkButton_Click"></asp:LinkButton>
                                                &nbsp;&nbsp;<asp:LinkButton ID="Feedback_whatsThisLinkButton" runat="server" Text="What's This"
                                                    SkinID="sknActionLinkButton" OnClientClick="javascript:return ShowWhatsThis('Y')" />
                                                <ajaxToolKit:ModalPopupExtender ID="Feedback_captchaHelpTextModalPopUpExtender" runat="server"
                                                    BehaviorID="Feedback_captchaHelpTextModalPopUpExtender" TargetControlID="Feedback_whatsThisLinkButton"
                                                    PopupControlID="Feedback_whatsThisDiv" BackgroundCssClass="modalBackground">
                                                </ajaxToolKit:ModalPopupExtender>
                                                <div id="Feedback_whatsThisDiv" style="display: none; height: 220px; width: 270px;
                                                    left: 390px; top: 340px; z-index: 0; position: absolute" class="popupcontrol_confirm">
                                                    <table width="100%" cellpadding="10" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 95%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Literal ID="Feedback_whatsThisDiv_titleLiteral" runat="server" Text="What's This"></asp:Literal>
                                                            </td>
                                                            <td style="width: 5%" valign="top" align="right">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="Feedback_whatsThisDiv_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                                                                OnClientClick="javascript:return ShowWhatsThis('N')" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table cellpadding="10" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                                            <table width="100%" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td align="center" class="label_field_text">
                                                                                        <asp:Literal ID="Feedback_whatsThisDiv_messageLiteral" runat="server" Text="The image you are looking at is known as a captcha. This is a security test to determine whether or not the user is human. In order to submit the form, you must enter the text seen in the image into the text box. The letters are case sensitive. If you can't make out the text, click the Regenerate Key link seen below the image">
                                                                                        </asp:Literal>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_height_20">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Feedback_captchaImageHeaderLabel" runat="server" Text="Security Code"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <span class="mandatory">*</span>
                                    </td>
                                    <td align="left">
                                        <asp:UpdatePanel ID="Feedback_captchaImageTextBoxUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="Feedback_captchaImageTextBox" runat="server" AutoCompleteType="None"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:Button ID="Feedback_submitButton" SkinID="sknButtonId" runat="server" Text="Submit"
                                            Width="60px" OnClick="Feedback_submitButton_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 40%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="center" valign="top" style="width: 100%" class="feedback">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="bottom" align="left" style="width: 100%; color: #81868A; font-size: small">
                                        <b>Or you can contact us by mail or phone at:</b><br />
                                        Geval6 Integration Inc
                                        <br />
                                        2500 W. Higgins Rd, #870,
                                        <br />
                                        Hoffman Estates, IL- 60169
                                        <br />
                                        Fixedline: 847.882.0585
                                        <br />
                                        Fax: 847.841.3796
                                        <br />
                                        EMail: info@Geval6.com
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="feedback_link_message" align="left" valign="bottom">
                                        Click here to go to
                                        <asp:LinkButton ID="Feedback_contactUsLinkButton" runat="server" Text="Contact Us"
                                            CssClass="feedback_link_btn" PostBackUrl="~/General/ContactUs.aspx" ToolTip="Click here to go to contact us page"> </asp:LinkButton>
                                        page
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td width="90%">
                        </td>
                        <td width="10%" align="right">
                            <table cellpadding="3" cellspacing="4" width="100%">
                                <tr>
                                    <td align="right">
                                        <asp:LinkButton ID="Feedback_bottomResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Reset" OnClick="Feedback_resetLinkButton_Click" />
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="Feedback_bottomCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="Feedback_cancelLinkButton_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
