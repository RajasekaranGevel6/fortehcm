﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/HomeMaster.Master"
    CodeBehind="ContactUs.aspx.cs" Inherits="Forte.HCM.UI.General.ContactUs" %>

<%@ MasterType VirtualPath="~/MasterPages/HomeMaster.Master" %>
<asp:Content ID="ContactUs_bodyContent" ContentPlaceHolderID="HomeMaster_body" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0" class="td_pading_logo_10">
        <tr>
            <td class="header_bg">
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td width="90%" class="header_text_bold">
                            <asp:Literal ID="ContactUs_headerLiteral" runat="server" Text="Contact Us"></asp:Literal>
                        </td>
                        <td width="10%" align="right">
                            <table cellpadding="3" cellspacing="4" width="100%">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="ContactUs_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="ContactUs_cancelLinkButton_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="ContactUs_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="ContactUs_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" style="color: #81868A; font-size: small">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top" style="width: 33%">
                            <b>Our Corporate Office:</b><br />
                            Geval6 Integration Inc <br />
                            2500 W. Higgins Rd, #870, <br />
                            Hoffman Estates, IL- 60169 <br />
                            Fixedline: 847.882.0585 <br />
                            Fax: 847.841.3796 <br />
                            EMail: info@Geval6.com
                        </td>
                        <td align="left" valign="top" style="width: 33%">
                            <b>Our Sales Office:</b> <br />
                            39 Julie Court, <br />
                            Somerset, NJ- 08873 <br />
                            Cell: 732 476 9811<br />
                            Fixedline: 609.423.4182 <br />
                            Fax: 877.240.0445
                        </td>
                        <td align="left" valign="top" style="width: 33%">
                            <b>Our India office: </b><br />
                            Geval6 Soft Systems P Ltd. <br />
                            New No. 47, Old No. 17, I Floor,<br />
                            Oliver Road, Mylapore, <br />
                            Chennai 600 004, India <br />
                            Fixedline: +91 44 4211 6885 / 6565 1235 (direct)<br />
                            Cell: +91 98408 42002 <br />
                            Fixedline: 847.882.0585 <br />
                            Fax: 847.841.3796
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left" valign="top">
                            <b>For career enquiries, please address your mail to: </b><br />
                            HR <br />
                            2500 W. Higgins Rd,<br />
                            #870, Hoffman Estates, IL- 60169.<br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
