﻿#region Directives

using System;
using System.Text;

using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

using Resources;


using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

#endregion  Directives

namespace Forte.HCM.UI.General
{
    public partial class Feedback : PageBase
    {
        /// <summary>
        /// A <see cref="System.String"/> that holds the string value
        /// used to store the capcha value in viewstate.
        /// </summary>
        private const string CAPTCHAVALUEVIEWSTATE = "CAPTCHA_TEXT";

        #region Event Handlers
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Feedback");

                Feedback_topSuccessMessageLabel.Text = string.Empty;
                Feedback_topErrorMessageLabel.Text = string.Empty;

                if (!IsPostBack)
                {

                    LoadLoggedInUserdDetails();
                    GenerateCaptchaImage();

                    if (Request.QueryString["from"] == "success")
                    {
                        base.ShowMessage(Feedback_topSuccessMessageLabel,
                            Resources.HCMResource.Feedback_FeedbackMailSentSuccessfully);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Feedback_topErrorMessageLabel,
                       exception.Message);
            }
        }


        /// <summary>
        /// A Method that validates whether the user inputed username
        /// is valid email or not using regular expression
        /// </summary>
        /// <param name="strUserEmailId">
        /// A <see cref="System.String"/> that holds the email to validate
        /// </param>
        /// <returns>
        /// A <see cref="System.Boolean"/> that holds the validity status
        /// </returns>
        private bool IsValidEmailAddress(string strUserEmailId)
        {
            //Regex regex = new Regex(".+@.+\\.[a-zA-Z]+", RegexOptions.Compiled);
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$", RegexOptions.Compiled);
            return regex.IsMatch(strUserEmailId);
        }

        /// <summary>
        /// Handles the Click event of the Feedback_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Feedback_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/General/Feedback.aspx", false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Feedback_topErrorMessageLabel,
                       exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the Feedback_cancelLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Feedback_cancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["USER_DETAIL"] == null)
                    Response.Redirect("~/Default.aspx", false);

                UserDetail userDetails = Session["USER_DETAIL"] as UserDetail;

                Response.Redirect("~/Default.aspx", false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Feedback_topErrorMessageLabel,
                       exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the Feedback_submitButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Feedback_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValidData())
                {
                    if (!IsValidEmailAddress(Feedback_emailTextBox.Text.Trim()))
                    {
                        base.ShowMessage(Feedback_topErrorMessageLabel,
                            HCMResource.UserRegistration_EnterValidEmailID);
                        return;
                    }

                    SendMail();

                    Response.Redirect("~/General/Feedback.aspx?&from=success", false);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Feedback_topErrorMessageLabel,
                       Resources.HCMResource.EmailAttachment_MailCouldNotSend);
            }
        }

        /// <summary>
        /// Handles the Click event of the Feedback_regenerateCaptchaImageLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Feedback_regenerateCaptchaImageLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                GenerateCaptchaImage();
                Feedback_captchaImageTextBox.Text = "";
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Feedback_topErrorMessageLabel,
                       exception.Message);
            }

        }

        #endregion Event Handlers

        #region Private Methods
        private void LoadLoggedInUserdDetails()
        {
            if (Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                return;
            UserDetail userDetail = (Session["USER_DETAIL"] as UserDetail);
            Feedback_nameTextBox.Text = userDetail.FirstName.Length == 0 ? string.Empty : userDetail.FirstName;
            Feedback_lastNameTextBox.Text = userDetail.LastName.Length == 0 ? string.Empty : userDetail.LastName.Trim();
            Feedback_emailTextBox.Text = userDetail.Email.Length == 0 ? string.Empty : userDetail.Email.Trim(); ;
            Feedback_phoneNumberTextBox.Text = userDetail.Phone == null ? string.Empty : userDetail.Phone.Trim();
        }
        /// <summary>
        /// 
        /// </summary>
        private void GenerateCaptchaImage()
        {
            CaptchaImage ci = null;
            try
            {
                Feedback_capchaImage.Src = "";
                ci = new CaptchaImage(
                   RandomString(5), 100, 60, "Century Schoolbook");
                Session["CAPTCHA_IMAGE"] = ci.Image;
                ViewState[CAPTCHAVALUEVIEWSTATE] = ci.Text;
                Feedback_countHidden.Value = (Convert.ToInt32(Feedback_countHidden.Value) + 1).ToString();
                Feedback_capchaImage.Src = "~/Common/CaptchaImageHandler.ashx?id=" +
                     Feedback_countHidden.Value;
            }
            finally
            {
                if (ci != null) ci = null;
            }
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }


        private void SendMail()
        {
            //Collect the details from the UI
            //Feedback feedbackDetail = new Feedback();
            DataObjects.FeedbackDetail feedbackDetail = new DataObjects.FeedbackDetail();

            feedbackDetail.FirstName = Feedback_nameTextBox.Text.Trim();
            feedbackDetail.LastName = Feedback_lastNameTextBox.Text.Trim();
            feedbackDetail.Email = Feedback_emailTextBox.Text.Trim();
            feedbackDetail.PhoneNumber = Feedback_phoneNumberTextBox.Text.Trim();
            feedbackDetail.Subject = Feedback_subjectDropDownList.SelectedItem.Text;
            feedbackDetail.Comments = Feedback_commentsTextBox.Text.Trim();

            //// Send email
            new EmailHandler().SendMail(EntityType.Feedback, feedbackDetail);
        }
        #endregion Private Methods

        #region Protected Overridden Methods
        protected override bool IsValidData()
        {

            MakeDefaultStyle();

            bool value = true;

            if (Utility.IsNullOrEmpty(Feedback_nameTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(Feedback_emailTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(Feedback_phoneNumberTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(Feedback_commentsTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(Feedback_captchaImageTextBox.Text.Trim()))
            {
                base.ShowMessage(Feedback_topErrorMessageLabel, HCMResource.UserRegistration_MandatoryFieldsMissing);
                value = false;
            }

            if (Utility.IsNullOrEmpty(Feedback_nameTextBox.Text.Trim()))
            {
                Feedback_nameTextBox.CssClass = "mandatory_text_box";
            }
            if (Utility.IsNullOrEmpty(Feedback_emailTextBox.Text.Trim()))
            {
                Feedback_emailTextBox.CssClass = "mandatory_text_box";
            }
            if (Utility.IsNullOrEmpty(Feedback_phoneNumberTextBox.Text.Trim()))
            {
                Feedback_phoneNumberTextBox.CssClass = "mandatory_text_box";
            }
            if (Utility.IsNullOrEmpty(Feedback_commentsTextBox.Text.Trim()))
            {
                Feedback_commentsTextBox.CssClass = "mandatory_text_box";
            }
            if (Utility.IsNullOrEmpty(Feedback_captchaImageTextBox.Text.Trim()))
            {
                Feedback_captchaImageTextBox.CssClass = "mandatory_text_box";
            }

            if (Feedback_subjectDropDownList.SelectedIndex == 0)
            {
                Feedback_subjectDropDownList.CssClass = "mandatory_dropdown";
                //base.ShowMessage(Feedback_topErrorMessageLabel, HCMResource.UserRegistration_PleaseSelectTheSubject);
                value = false;
            }

            if (Feedback_captchaImageTextBox.Text.Trim().Length > 0 &&
                    Feedback_captchaImageTextBox.Text.Trim() != ViewState[CAPTCHAVALUEVIEWSTATE].ToString())
            {
                base.ShowMessage(Feedback_topErrorMessageLabel, HCMResource.UserRegistration_CaptchaValueMisMatch);
                value = false;
            }
            return value;
        }

        private void MakeDefaultStyle()
        {
            Feedback_nameTextBox.CssClass = "text_box";

            Feedback_emailTextBox.CssClass = "text_box";

            Feedback_phoneNumberTextBox.CssClass = "text_box";

            Feedback_commentsTextBox.CssClass = "text_box";

            Feedback_captchaImageTextBox.CssClass = "text_box";

            Feedback_subjectDropDownList.CssClass = "dropdown";
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Overridden Methods
    }
}