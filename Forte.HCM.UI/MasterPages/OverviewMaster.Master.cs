﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Forte.HCM.UI.MasterPages
{
    public partial class OverviewMaster : MasterPage
    {
        /// <summary>
        /// A <see cref="string"/> that holds the page caption.
        /// </summary>
        protected string pageCaption;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void SetPageCaption(string pageTitle)
        {
            if (pageTitle != null)
            {
                pageCaption = pageTitle.Trim();
            }
        }

        public void ControlVisibility(bool flag)
        {
            LinkButton Candcel_LinkButton = (LinkButton)OverviewMaster_headerControl.FindControl("OverviewHeaderControl_cancelLinkButton");
            Candcel_LinkButton.Visible = flag;
        }
    }
}