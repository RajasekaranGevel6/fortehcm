﻿using System;
using System.Web.UI;
using System.Collections.Generic;

namespace Forte.HCM.UI.MasterPages
{
    public partial class InterviewMaster : MasterPage
    {
        List<string> breadCrumbText;
        protected string pageCaption;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InterviewMaster_headerControl.BreadCrumbText = breadCrumbText;
            }
        }

        public void SetPageCaption(string pageTitle)
        {
            if (pageTitle != null)
            {
                pageCaption = pageTitle.Trim();
            }
        }

        public List<string> BreadCrumbText
        {
            set
            {
                breadCrumbText = value;
            }
        }

        /// <summary>
        /// Method that sets hides the menu and sign up button.
        /// </summary>
        public void HideMenu()
        {
            InterviewMaster_menuControl.Visible = false;
            InterviewMaster_headerControl.HideSignUpButton();
        }
    }
}
