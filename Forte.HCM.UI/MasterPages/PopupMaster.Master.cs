﻿using System;
using System.Web.UI;

namespace Forte.HCM.UI.MasterPages
{
    public partial class PopupMaster : MasterPage
    {
        protected string pageCaption = "HCM";

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void SetPageCaption(string pageTitle)
        {
            if (pageTitle != null)
            {
                pageCaption = pageTitle.Trim();
            }
        }
    }
}
