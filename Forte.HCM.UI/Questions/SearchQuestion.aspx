﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OTMMaster.Master"
    Inherits="Forte.HCM.UI.Questions.SearchQuestion" CodeBehind="SearchQuestion.aspx.cs" %>

<%@ Register Src="~/CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ Register Src="../CommonControls/QuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc4" %>
    <%@ Register Src="../CommonControls/QuestionDetailPreviewControlOpenText.ascx" TagName="QuestionDetailPreviewControlOpenText"
    TagPrefix="uc5" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="SearchQuestion_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">
    <script type="text/javascript" language="javascript">
        //Expand and Collapse All Question's 
        //Option Details panel show and Hide.
        function ExpORCollapseRows(targetControl) {
            var ctrl = document.getElementById('<%=SearchQuestion_stateExpandHiddenField.ClientID %>');
            targetValue = ctrl.value;
            if (targetValue == "0") {
                targetControl.innerHTML = "Collapse All";
                ExpandAllRows(targetControl);
                ctrl.value = 1;
            }
            else {
                targetControl.innerHTML = "Expand All";
                CollapseAllRows(targetControl);
                ctrl.value = 0;
            }

            return false;
        }

        //Expand all the question's Option panel
        function ExpandAllRows(targetControl) {
            var gridCtrl=null;
            var questionType = document.getElementById("<%= SearchQuestion_questionTypeDropDownList.ClientID %>");
            var selectedValue = questionType.value;
            if(selectedValue=="1")
                 gridCtrl = document.getElementById("<%= SearchQuestion_questionDiv.ClientID %>");
            else
                gridCtrl = document.getElementById("<%= SearchQuestion_questionOpenTextDiv.ClientID %>");
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (selectedValue == "1") {
                        if (rowItems[indexRow].id.indexOf("SearchQuestion_detailsDiv") != "-1") {
                            rowItems[indexRow].style.display = "block";
                        }
                    }
                    else {
                        if (rowItems[indexRow].id.indexOf("SearchQuestion_openText_detailsDiv") != "-1") {
                            rowItems[indexRow].style.display = "block";
                        }
                    }
                }
            }
            return false;
        }
        //Hide all the question's Option panel
        function CollapseAllRows(targetControl) {
            
        var gridCtrl=null;
            var questionType = document.getElementById("<%= SearchQuestion_questionTypeDropDownList.ClientID %>");
            var selectedValue = questionType.value;
            if(selectedValue=="1")
                 gridCtrl = document.getElementById("<%= SearchQuestion_questionDiv.ClientID %>");
            else
                gridCtrl = document.getElementById("<%= SearchQuestion_questionOpenTextDiv.ClientID %>");
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (selectedValue == "1") {
                        if (rowItems[indexRow].id.indexOf("SearchQuestion_detailsDiv") != "-1") {
                            rowItems[indexRow].style.display = "none";
                        }
                    }
                    else {
                        if (rowItems[indexRow].id.indexOf("SearchQuestion_openText_detailsDiv") != "-1") {
                            rowItems[indexRow].style.display = "none";
                        }
                    }
                }
            }
            var linkcontrol = document.getElementById
            ("<%=SearchQuestion_testDraftExpandLinkButton.ClientID %>");           
            if (linkcontrol.innerHTML = "Collapse All") {
                linkcontrol.innerHTML = "Expand All";
                var ctrl = document.getElementById('<%=SearchQuestion_stateExpandHiddenField.ClientID %>');
                ctrl.value = 0;

            }
            return false;
        }

        function GetMouseClickedPos(ctrlID) {
            if (document.getElementById(ctrlID) != null) {
                document.getElementById(ctrlID).focus();
            }
            return true;
        }
    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="SearchQuestion_headerLiteral" runat="server" Text="Search Question"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="SearchQuestion_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Reset" OnClick="SearchQuestion_resetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="SearchQuestion_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="SearchQuestion_cancelLinkButton_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="SearchQuestion_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="SearchQuestion_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchQuestion_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                            ID="SearchQuestion_stateHiddenField" runat="server" Value="0" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div id="SearchQuestion_searchCriteriasDiv" runat="server" style="display: block;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="right" class="td_padding_bottom_5">
                                            <asp:UpdatePanel runat="server" ID="SearchQuestion_simpleLinkUpdatePanel">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="SearchQuestion_simpleLinkButton" runat="server" Text="Advanced"
                                                        SkinID="sknActionLinkButton" OnClick="SearchQuestion_simpleLinkButton_Click" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tab_body_bg">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="SearchQuestion_questionTypeHeadLabel" runat="server" Text="Question Type"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        <asp:DropDownList ID="SearchQuestion_questionTypeDropDownList" SkinID="sknQuestionTypeDropDown"
                                                            runat="server" AutoPostBack="false" EnableViewState="true">
                                                            <asp:ListItem Text="Multiple Choice" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Open Text" Value="2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="panel_bg">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="100%">
                                                        <asp:UpdatePanel runat="server" ID="SearchQuestion_SearchDivUpdatePanel">
                                                            <ContentTemplate>
                                                                <div id="SearchQuestion_simpleSearchDiv" runat="server">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="SearchQuestion_categoryHeadLabel" runat="server" Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="SearchQuestion_categoryTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="SearchQuestion_subjectHeadLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="SearchQuestion_subjectTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="SearchQuestion_keywordsHeadLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <div style="float: left; padding-right: 5px;">
                                                                                    <asp:TextBox ID="SearchQuestion_keywordsTextBox" runat="server" MaxLength="100"></asp:TextBox></div>
                                                                                <div style="float: left;">
                                                                                    <asp:ImageButton ID="SearchQuestion_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div id="SearchQuestion_advanceSearchDiv" runat="server" style="width: 100%" visible="false">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <uc1:CategorySubjectControl ID="SearchQuestion_searchCategorySubjectControl" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_8">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="panel_inner_body_bg">
                                                                                <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="SearchQuestion_testAreaHeadLabel" runat="server" Text="Test Area"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="7" class="checkbox_list_bg" align="left" style="width: 90%">
                                                                                            <asp:CheckBoxList ID="SearchQuestion_testAreaCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                            </asp:CheckBoxList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="SearchQuestion_complexityLabel" runat="server" Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="3" align="left" valign="middle">
                                                                                            <div style="float: left; padding-right: 5px; width: 90%; vertical-align: middle;"
                                                                                                class="checkbox_list_bg">
                                                                                                <asp:CheckBoxList ID="SearchQuestion_complexityCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                    RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                </asp:CheckBoxList>
                                                                                            </div>
                                                                                            <div style="float: left; vertical-align: middle; padding-left: 5px; padding-top: 5px;">
                                                                                                <asp:ImageButton ID="SearchQuestion_complexityImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, ComplexityHelp %>" />
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchQuestion_keywordHeadLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="3">
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="SearchQuestion_keywordTextBox" runat="server" Columns="50" MaxLength="100"></asp:TextBox></div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="SearchQuestion_keywordImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" />
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchQuestion_questionLabel" runat="server" Text="Question ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="SearchQuestion_questionIDTextBox" runat="server" MaxLength="15"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchQuestion_creditHeadLabel" runat="server" Text="Credit Earned (in $)"
                                                                                                SkinID="sknLabelFieldHeaderText" Visible="false"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="SearchQuestion_creditTextBox" runat="server" Visible="false"></asp:TextBox>
                                                                                            <ajaxToolKit:MaskedEditExtender ID="Options_simpleMaskedEditExtender" runat="server"
                                                                                                TargetControlID="SearchQuestion_creditTextBox" Mask="99.99" MessageValidatorTip="true"
                                                                                                InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                                MaskType="Number" AcceptNegative="None" ErrorTooltipEnabled="True" />
                                                                                        </td>
                                                                                        <td style="width: 8%">
                                                                                            <asp:Label ID="SearchQuestion_statusHeadLabel" runat="server" Text="Status" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:DropDownList ID="SearchQuestion_statusDropDownList" runat="server" Width="132px">
                                                                                                    <asp:ListItem Text="All" Value=""></asp:ListItem>
                                                                                                    <asp:ListItem Text="Active" Value="Y"></asp:ListItem>
                                                                                                    <asp:ListItem Text="Inactive" Value="N"></asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="SearchQuestion_statusImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, StatusHelp %>" /></div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchQuestion_authorHeadLabel" runat="server" Text="Author" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="SearchQuestion_authorTextBox" runat="server"></asp:TextBox>
                                                                                                <asp:HiddenField ID="SearchQuestion_dummyAuthorID" runat="server" />
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="SearchQuestion_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the question author" /></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="td_padding_top_5">
                                                        <asp:Button ID="SearchQuestion_topSearchButton" runat="server" Text="Search" SkinID="sknButtonId"
                                                            OnClick="SearchQuestion_topSearchButton_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr id="SearchQuestion_searchQuestionResultsTR" runat="server">
                        <td class="header_bg">
                            <asp:UpdatePanel ID="SearchQuestion_expandAllUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 50%" align="left" class="header_text_bold">
                                                <asp:Literal ID="SearchQuestion_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                    ID="SearchQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                            </td>
                                            <td style="width: 48%" align="left">
                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="SearchQuestion_testDraftExpandLinkButton" runat="server" Text="Expand All"
                                                                SkinID="sknActionLinkButton" OnClientClick="javascript:return ExpORCollapseRows(this);"
                                                                Visible="false"></asp:LinkButton>
                                                            <asp:HiddenField ID="SearchQuestion_stateExpandHiddenField" runat="server" Value="0" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 2%" align="right">
                                                <span id="SearchQuestion_searchResultsUpSpan" runat="server" style="display: none;">
                                                    <asp:Image ID="SearchQuestion_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                        id="SearchQuestion_searchResultsDownSpan" runat="server" style="display: block;"><asp:Image
                                                            ID="SearchQuestion_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                <asp:HiddenField ID="SearchQuestion_restoreHiddenField" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <asp:UpdatePanel runat="server" ID="SearchQuestion_questionGridViewUpdatePanel">
                                <ContentTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" >
                                        <tr>
                                            <td align="left">
                                                <div style="height: 300px; overflow: auto;" runat="server" id="SearchQuestion_questionOpenTextDiv"
                                                    visible="false">
                                                    <asp:GridView ID="SearchQuestion_questionOpenTextGridView" runat="server" OnRowCommand="SearchQuestion_questionOpenTextGridView_RowCommand"
                                                        OnSorting="SearchQuestion_questionOpenTextGridView_Sorting" OnRowDataBound="SearchQuestion_questionOpenTextGridView_RowDataBound"
                                                        OnRowCreated="SearchQuestion_questionOpenTextGridView_RowCreated">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="12%">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="SearchQuestion_openText_editImageButton" runat="server" SkinID="sknEditImageButton"
                                                                        ToolTip="Edit Question" CommandArgument='<%# Eval("QuestionKey") + "," + Eval("QuestionType") %>'
                                                                        CommandName="editquestion" />
                                                                    <asp:ImageButton ID="SearchQuestion_openText_activeImageButton" runat="server" SkinID="sknInactiveImageButton"
                                                                        Visible="false" ToolTip="Deactivate Question" CommandName="inactive" CommandArgument='<%# Eval("QuestionKey") %>' />
                                                                    <asp:ImageButton ID="SearchQuestion_openText_inactiveImageButton" runat="server" SkinID="sknActiveImageButton"
                                                                        ToolTip="Activate Question" Visible="false" CommandName="active" CommandArgument='<%# Eval("QuestionKey") %>' />
                                                                    <asp:ImageButton ID="SearchQuestion_openText_questionUsageSummaryImageButton" runat="server"
                                                                        SkinID="sknDashboardImageButton" ToolTip="Question Summary" OnClientClick=<%# "OpenUsageSummary('" + Eval("QuestionKey") +"');return false;"%> />
                                                                    <asp:ImageButton ID="SearchQuestion_openText_questionInfoImageButton" runat="server" ToolTip="Question Info"
                                                                        SkinID="sknPreviewImageButton" CommandName="view" CommandArgument='<%# Eval("QuestionKey") %>' />
                                                                    <asp:ImageButton ID="SearchQuestion_openText_deleteImageButton" runat="server" SkinID="sknDeleteImageButton"
                                                                        ToolTip="Delete Question" CommandName="deletes" CommandArgument='<%# Eval("QuestionKey") %>' /><asp:HiddenField
                                                                            ID="SearchQuestion_openText_statusHiddenField" runat="server" Value='<%# Eval("Status") %>' />
                                                                    <asp:HiddenField ID="SearchQuestion_openText_TestIncludedHiddenField" runat="server" Value='<%# Eval("TestIncluded") %>' />
                                                                    <asp:HiddenField ID="SearchQuestion_openText_DataAccessRightsHiddenField" runat="server" Value='<%# Eval("DataAccessRights") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Question ID" SortExpression="QUESTIONKEY" HeaderStyle-Width="80px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="SearchQuestion_openText_questionIdLinkButton" runat="server" Text='<%# Eval("QuestionKey") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Question Type" SortExpression="QUESTIONTYPE" HeaderStyle-Width="60px" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="SearchQuestion_openText_questiontyQuestionTypeLiteral" runat="server" Text='<%# Eval("QuestionType") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Question" SortExpression="QUESTIONDESC" HeaderStyle-Width="340px">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="SearchQuestion_openText_questionGridLiteral" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),55) %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Complexity" DataField="Complexity" HeaderStyle-Width="83px"
                                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" SortExpression="COMPLEXITYNAME">
                                                            </asp:BoundField>
                                                            <asp:BoundField HeaderText="Credits (in&nbsp;$)" DataField="CreditsEarned" SortExpression="CREDITEARNED"
                                                                ItemStyle-CssClass="td_padding_right_20" HeaderStyle-Width="87px" HeaderStyle-HorizontalAlign="Center"
                                                                HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="right" Visible="false">
                                                            </asp:BoundField>
                                                            <asp:BoundField HeaderText="Status" HeaderStyle-Width="70px" HeaderStyle-HorizontalAlign="Center"
                                                                HeaderStyle-VerticalAlign="Middle" DataField="Status" SortExpression="STATUS" />
                                                        
                                                            <asp:TemplateField HeaderText="Modified Date" SortExpression="MODIFIEDDATE DESC">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchQuestion_openText_modifiedDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.QuestionDetail)Container.DataItem).ModifiedDate) %>'
                                                                        Width="125px"></asp:Label>
                                                                    <tr>
                                                                        <td class="grid_padding_right" colspan="7">
                                                                            <div id="SearchQuestion_openText_detailsDiv" runat="server" style="display: none;" class="table_outline_bg">
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="popup_question_icon" style="width: 35%" colspan="4">
                                                                                            <asp:Label ID="SearchQuestion_openText_questionLabel" runat="server" Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'
                                                                                                Width="810px"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4">
                                                                                            <div style="height: 60px; overflow: auto; width: 100%">
                                                                                                <div class="label_multi_field_text" style="height: 40px; overflow: auto; word-wrap: break-word; white-space: normal;">
                                                                                                                            <asp:Label ID="SearchQuestion_questionOpenTextGridView_AnswerLabel" runat="server" SkinID="sknLabelFieldHeaderText" Text="Answer:"></asp:Label>&nbsp;&nbsp;&nbsp; 
                                                                                                                                <asp:Literal ID="SearchQuestion_questionOpenTextGridView_Answer" runat="server" SkinID="sknMultiLineText"
                                                                                                                                    Text="Screening candidates for Yes Banking project. The candidates must have minimum 3 years experience in the field of ASP.Net UI design, Java Script, C# Programming & OOPS."></asp:Literal>
                                                                                                                            </div>
                                                                                                <asp:HiddenField ID="SearchQuestion_openText_CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="SearchQuestion_openText_testAreaHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                Text="Test Area"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 40%">
                                                                                            <asp:Label ID="SearchQuestion_openText_testAreaTextBox" runat="server" ReadOnly="true" SkinID="sknLabelFieldText"
                                                                                                Text='<%# Eval("TestAreaName") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="SearchQuestion_openText_complexityHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                Text="Complexity"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchQuestion_openText_complexityTextBox" runat="server" ReadOnly="true" SkinID="sknLabelFieldText"
                                                                                                Text='<%# Eval("Complexity") %>'></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                            <%-- popup DIV --%>
                                                                            <a href="#SearchQuestion_openText_focusmeLink" id="SearchQuestion_openText_focusDownLink" runat="server">
                                                                            </a><a href="#" id="SearchQuestion_openText_focusmeLink" runat="server"></a>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>                                         
                                        <tr>
                                            <td>
                                                <uc2:PageNavigator ID="SearchQuestion_questionOpenText_bottomPagingNavigator" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" >
                                        <tr>
                                            <td align="left">
                                                <div style="height: 300px; overflow: auto;" runat="server" id="SearchQuestion_questionDiv"
                                                    visible="false">
                                                    <asp:GridView ID="SearchQuestion_questionGridView" runat="server" OnRowCommand="SearchQuestion_questionGridView_RowCommand"
                                                        OnSorting="SearchQuestion_questionGridView_Sorting" OnRowDataBound="SearchQuestion_questionGridView_RowDataBound"
                                                        OnRowCreated="SearchQuestion_questionGridView_RowCreated">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="12%">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="SearchQuestion_editImageButton" runat="server" SkinID="sknEditImageButton"
                                                                        ToolTip="Edit Question" CommandArgument='<%# Eval("QuestionKey") + "," + Eval("QuestionType") %>'
                                                                        CommandName="editquestion" />
                                                                    <asp:ImageButton ID="SearchQuestion_activeImageButton" runat="server" SkinID="sknInactiveImageButton"
                                                                        Visible="false" ToolTip="Deactivate Question" CommandName="inactive" CommandArgument='<%# Eval("QuestionKey") %>' />
                                                                    <asp:ImageButton ID="SearchQuestion_inactiveImageButton" runat="server" SkinID="sknActiveImageButton"
                                                                        ToolTip="Activate Question" Visible="false" CommandName="active" CommandArgument='<%# Eval("QuestionKey") %>' />
                                                                    <asp:ImageButton ID="SearchQuestion_questionUsageSummaryImageButton" runat="server"
                                                                        SkinID="sknDashboardImageButton" ToolTip="Question Summary" OnClientClick=<%# "OpenUsageSummary('" + Eval("QuestionKey") +"');return false;"%> />
                                                                    <asp:ImageButton ID="SearchQuestion_questionInfoImageButton" runat="server" ToolTip="Question Info"
                                                                        SkinID="sknPreviewImageButton" CommandName="view" CommandArgument='<%# Eval("QuestionKey") %>' />
                                                                    <asp:ImageButton ID="SearchQuestion_deleteImageButton" runat="server" SkinID="sknDeleteImageButton"
                                                                        ToolTip="Delete Question" CommandName="deletes" CommandArgument='<%# Eval("QuestionKey") %>' /><asp:HiddenField
                                                                            ID="SearchQuestion_statusHiddenField" runat="server" Value='<%# Eval("Status") %>' />
                                                                    <asp:HiddenField ID="TestIncludedHiddenField" runat="server" Value='<%# Eval("TestIncluded") %>' />
                                                                    <asp:HiddenField ID="DataAccessRightsHiddenField" runat="server" Value='<%# Eval("DataAccessRights") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Question ID" SortExpression="QUESTIONKEY" HeaderStyle-Width="80px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="SearchQuestion_questionIdLinkButton" runat="server" Text='<%# Eval("QuestionKey") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Question Type" SortExpression="QUESTIONTYPE" HeaderStyle-Width="60px" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="SearchQuestion_questiontyQuestionTypeLiteral" runat="server" Text='<%# Eval("QuestionType") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Question" SortExpression="QUESTIONDESC" HeaderStyle-Width="340px">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="SearchQuestion_questionGridLiteral" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),55) %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Complexity" DataField="Complexity" HeaderStyle-Width="83px"
                                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" SortExpression="COMPLEXITYNAME">
                                                            </asp:BoundField>
                                                            <asp:BoundField HeaderText="Credits (in&nbsp;$)" DataField="CreditsEarned" SortExpression="CREDITEARNED"
                                                                ItemStyle-CssClass="td_padding_right_20" HeaderStyle-Width="87px" HeaderStyle-HorizontalAlign="Center"
                                                                HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="right" Visible="false">
                                                            </asp:BoundField>
                                                            <asp:BoundField HeaderText="Status" HeaderStyle-Width="70px" HeaderStyle-HorizontalAlign="Center"
                                                                HeaderStyle-VerticalAlign="Middle" DataField="Status" SortExpression="STATUS" />
                                                                 <asp:BoundField HeaderText="Category" HeaderStyle-Width="70px" HeaderStyle-HorizontalAlign="Center"
                                                                HeaderStyle-VerticalAlign="Middle" DataField="CategoryName" SortExpression="CATEGORY" />
                                                             <asp:BoundField HeaderText="Subject" HeaderStyle-Width="70px" HeaderStyle-HorizontalAlign="Center"
                                                                HeaderStyle-VerticalAlign="Middle" DataField="SubjectName" SortExpression="SUBJECT" />
                                                            <asp:TemplateField HeaderText="Modified Date" SortExpression="MODIFIEDDATE DESC">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchQuestion_modifiedDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.QuestionDetail)Container.DataItem).ModifiedDate) %>'
                                                                        Width="125px"></asp:Label>
                                                                    <tr>
                                                                        <td class="grid_padding_right" colspan="9">
                                                                            <div id="SearchQuestion_detailsDiv" runat="server" style="display: none;" class="table_outline_bg">
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="popup_question_icon" style="width: 35%" colspan="4">
                                                                                            <asp:Label ID="SearchQuestion_questionLabel" runat="server" Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'
                                                                                                Width="810px"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4">
                                                                                            <div style="height: 60px; overflow: auto; width: 100%">
                                                                                                <asp:PlaceHolder ID="SearchQuestion_answerChoicesPlaceHolder" runat="server"></asp:PlaceHolder>
                                                                                                <asp:HiddenField ID="CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="SearchQuestion_testAreaHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                Text="Test Area"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 40%">
                                                                                            <asp:Label ID="SearchQuestion_testAreaTextBox" runat="server" ReadOnly="true" SkinID="sknLabelFieldText"
                                                                                                Text='<%# Eval("TestAreaName") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="SearchQuestion_complexityHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                Text="Complexity"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchQuestion_complexityTextBox" runat="server" ReadOnly="true" SkinID="sknLabelFieldText"
                                                                                                Text='<%# Eval("Complexity") %>'></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                            <%-- popup DIV --%>
                                                                            <a href="#SearchQuestion_focusmeLink" id="SearchQuestion_focusDownLink" runat="server">
                                                                            </a><a href="#" id="SearchQuestion_focusmeLink" runat="server"></a>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <uc2:PageNavigator ID="SearchQuestion_question_bottomPagingNavigator" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                    
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="SearchQuestion_topSearchButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="SearchQuestion_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="SearchQuestion_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchQuestion_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%">
                            &nbsp;
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="SearchQuestion_bottomResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Reset" OnClick="SearchQuestion_resetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="SearchQuestion_bottomCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="SearchQuestion_cancelLinkButton_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="SearchQuestion_modelPopupUpdatePanel">
                    <ContentTemplate>
                        <asp:Panel ID="SearchQuestion_questionPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_question_detail">
                            <div style="display: none">
                                <asp:Button ID="SearchQuestion_hiddenButton" runat="server" Text="Hidden" /></div>
                            <div id="SearchQuestion_QuestionDetailPreviewControlDiv" runat="server" style="display:none">
                                <uc4:QuestionDetailPreviewControl ID="SearchQuestion_QuestionDetailPreviewControl"
                                    runat="server" OnCancelClick="SearchQuestion_cancelClick" />
                            </div>
                            <div id="SearchQuestion_QuestionDetailPreviewControlOpenTextDiv" runat="server" style="display:none">
                                <uc5:QuestionDetailPreviewControlOpenText ID="SearchQuestion_QuestionDetailPreviewControlOpenText"
                                    runat="server" OnCancelClick="SearchQuestion_cancelClick" />
                            </div>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="SearchQuestion_questionModalPopupExtender" runat="server"
                            PopupControlID="SearchQuestion_questionPanel" TargetControlID="SearchQuestion_hiddenButton"
                            BackgroundCssClass="modalBackground" DynamicServicePath="" Enabled="True">
                        </ajaxToolKit:ModalPopupExtender>
                        <asp:Panel ID="SearchQuestion_ConfirmPopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc3:ConfirmMsgControl ID="SearchQuestiont_confirmPopupExtenderControl" runat="server"
                                OnOkClick="SearchQuestiont_okClick" OnCancelClick="SearchQuestion_cancelClick" />
                        </asp:Panel>
                        <div id="SearchQuestion_hiddenDIV" runat="server" style="display: none">
                            <asp:Button ID="SearchQuestion_hiddenPopupModalButton" runat="server" />
                        </div>
                        <ajaxToolKit:ModalPopupExtender ID="SearchQuestion_ConfirmPopupExtender" runat="server"
                            PopupControlID="SearchQuestion_ConfirmPopupPanel" TargetControlID="SearchQuestion_hiddenPopupModalButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                        <asp:HiddenField ID="SearchQuestion_QuestionKey" runat="server" />
                        <asp:HiddenField ID="SearchQuestion_Action" runat="server" />
                        <asp:HiddenField ID="SearchQuestion_authorIdHiddenField" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
