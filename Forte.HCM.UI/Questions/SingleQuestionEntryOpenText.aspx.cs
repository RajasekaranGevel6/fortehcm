﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SingleQuestionEntry.cs
// File that represents the user interface for question entry page.
// This will helps to add a single question to the question repository.

#endregion Header

#region Directives

using System;
using System.Web;
using System.Text;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using Resources;
using System.IO;


#endregion Directives

namespace Forte.HCM.UI.Questions
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities for 
    /// the question entry page. This page helps add a single question to the
    /// question repository.
    /// </summary>
    /// <remarks>
    /// This class subclasses the <see cref="PageBase"/> object.
    /// </remarks>
    public partial class SingleQuestionEntryOpenText : PageBase
    {
        #region Events Handlers

        /// <summary>
        /// This method checks to see if the querystring is found or not. If the 
        /// querystring contains the questionkey, the page will be considered as 
        /// edit question entry page. Otherwise, it will be SingleQuestionEntry page.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.DefaultButton = SingleQuestionEntry_topSaveButton.UniqueID;
                Page.SetFocus(SingleQuestionEntry_topSaveButton.ClientID);
                ClearAllLabelMessage();

                SingleQuestionEntry_questionType.QuestionTypeChanged += new QuestionTypeDelegate(SingleQuestionEntry_questionType_QuestionTypeChanged);

                if (!IsPostBack)
                {
                    SingleQuestionEntry_questionType.QuestionType = QuestionType.OpenText;

                    Session["POSTED_QUESTION_IMAGE"] = null;

                    LoadValues();

                    Page.Form.DefaultFocus = SingleQuestionEntry_questionTextBox.UniqueID;
                    SingleQuestionEntry_questionTextBox.Focus();

                    // Check if the querystring is empty (i.e questionkey). If the condition meets
                    // TRUE, the page will show 4 answer hoices by default. if the page is opened 
                    // in EDIT MODE. Note: The query string will hold questionkey only.
                    if (Utility.IsNullOrEmpty(Request.QueryString["questionkey"]))
                    {
                        // Set visibility false to the CREDITS EARNED field. No need to show these
                        // for adding new question.
                        SingleQuestionEntry_creditTextBox.Visible = false;
                        SingleQuestionEntry_creditHeadLabel.Visible = false;

                        // Set page title
                        SingleQuestionEntry_headerLiteral.Text =
                            Resources.HCMResource.SingleQuestionEntry_Title;
                        Master.SetPageCaption(Resources.HCMResource.SingleQuestionEntry_Title);

                        // Show currently logged in user name in the Author textbox
                        SingleQuestionEntry_authorTextBox.Text
                            = new CommonBLManager().GetUserDetail(base.userID).FirstName;
                        SingleQuestionEntry_authorIdHiddenField.Value = base.userID.ToString();
                    }
                    else
                    {
                        // Show the edit mode table row that displays the 
                        // question ID and last modified date.
                        SingleQuestionEntry_editModeTR.Visible = true;

                        ViewState["IS_EDIT_MODE"] = true;

                        // Change the page title and header label
                        SingleQuestionEntry_headerLiteral.Text =
                            Resources.HCMResource.SingleQuestionEntry_Edit_Title;
                        Master.SetPageCaption(Resources.HCMResource.SingleQuestionEntry_Edit_Title);

                        // Set the visibility status for CREDITS EARNED field
                        SingleQuestionEntry_creditHeadLabel.Visible = true;
                        SingleQuestionEntry_creditTextBox.Visible = true;

                        LoadQuestionDetails(Request.QueryString["questionkey"].ToString());

                        // Check if the page is redirected from single question after saving a
                        // question. If so, show the success message accordingly.
                        if (!Utility.IsNullOrEmpty(Request.QueryString["fromnew"]) &&
                            Request.QueryString["fromnew"].ToUpper() == "Y")
                        {
                            base.ShowMessage(SingleQuestionEntry_topSuccessMessageLabel,
                                SingleQuestionEntry_bottomSuccessMessageLabel,
                                string.Format(Resources.HCMResource.SingleQuestionEntry_AddedSuccessfully,
                                Request.QueryString["questionkey"]));
                        }
                        else if (!Utility.IsNullOrEmpty(Request.QueryString["fromedit"]) &&
                            Request.QueryString["fromedit"].ToUpper() == "Y")
                        {
                            base.ShowMessage(SingleQuestionEntry_topSuccessMessageLabel,
                                SingleQuestionEntry_bottomSuccessMessageLabel,
                                string.Format(Resources.HCMResource.SingleQuestionEntry_UpdatedSuccessfully,
                                Request.QueryString["questionkey"]));
                        }
                    }
                }

                // Change the EDIT page title
                if (Convert.ToBoolean(ViewState["IS_EDIT_MODE"]))
                {
                    SingleQuestionEntry_headerLiteral.Text =
                        Resources.HCMResource.SingleQuestionEntry_Edit_Title;
                    Master.SetPageCaption(Resources.HCMResource.SingleQuestionEntry_Edit_Title);
                    SingleQuestionEntry_questionType.Enabled = false;
                }
                else
                {
                    // Set SINGLE question entry page title
                    SingleQuestionEntry_headerLiteral.Text =
                        Resources.HCMResource.SingleQuestionEntry_Title;
                    Master.SetPageCaption(Resources.HCMResource.SingleQuestionEntry_Title);
                    SingleQuestionEntry_questionType.Enabled = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                    SingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }

            // Subscribing event to prevent existing category.
            SingleQuestionEntry_categorySubjectControl.ControlMessageThrown +=
                new CategorySubjectControl.ControlMessageThrownDelegate
                    (SingleQuestionEntry_categorySubjectControl_ControlMessageThrown);

            // Open Search User popup by calling javascript funciton
            SingleQuestionEntry_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                + SingleQuestionEntry_dummyAuthorId.ClientID + "','"
                + SingleQuestionEntry_authorIdHiddenField.ClientID + "','"
                + SingleQuestionEntry_authorTextBox.ClientID + "','QA')");

            SingleQuestionEntry_addImageLinkButton.Attributes.Add("onclick", "return ShowAddImagePanel('" + this.SingleQuestionEntry_AddQuestionImageTR.ClientID + "','" + SingleQuestionEntry_addImageLinkButton.ClientID + "')");
        }

        private void SingleQuestionEntry_questionType_QuestionTypeChanged(object sender, QuestionTypeEventArgs e)
        {
            if (e.QuestionType == QuestionType.MultipleChoice)
            {
                Response.Redirect("~/Questions/SingleQuestionEntryOpenText.aspx?m=0&s=1");

            }
            else if (e.QuestionType == QuestionType.OpenText)
            {
                Response.Redirect("~/Questions/SingleQuestionEntryOpenText.aspx?m=0&s=1");
            }
        }

        /// <summary>
        /// Handler that shows error/success messages.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/> that contains the event data.
        /// </param>
        private void SingleQuestionEntry_categorySubjectControl_ControlMessageThrown
            (object sender, ControlMessageEventArgs c)
        {
            if (c.MessageType == MessageType.Error)
            {
                SingleQuestionEntry_topErrorMessageLabel.Text = c.Message.ToString();
                SingleQuestionEntry_bottomErrorMessageLabel.Text = c.Message.ToString();
            }
            else if (c.MessageType == MessageType.Success)
            {
                SingleQuestionEntry_topSuccessMessageLabel.Text = c.Message.ToString();
                SingleQuestionEntry_bottomSuccessMessageLabel.Text = c.Message.ToString();
            }
        }

        /// <summary>
        /// This method will be called when the user clicks the Accept button in 
        /// the disclaimer popup. It performs a task based on the page mode. For 
        /// example, If the page mode is EDIT and the question is not included in
        /// any test, the question will be saved as a new question. Otherwise, the
        /// question will get updated. Also, in this event handler, concurrent 
        /// modification is handled by using the MODIFIED_DATE.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void DisclaimerControl_acceptButton_Click(object sender, EventArgs e)
        {
            ClearAllLabelMessage();

            bool redirectToEditFromNew = false;
            bool redirectToEditFromEdit = false;
            QuestionDetail questionDetail = null;
            string questionKey = null;

            try
            {
                questionDetail = ConstructQuestionDetails();

                if (Convert.ToBoolean(ViewState["IS_EDIT_MODE"]) == true)
                {
                    questionKey = Request.QueryString["questionkey"].ToString();

                    if (Convert.ToBoolean(ViewState["CAN_EDIT"]) == false)
                    {
                        new QuestionBLManager().SaveQuestion(questionDetail);
                        redirectToEditFromNew = true;
                    }
                    else
                    {
                        // Check if the question is modified by someone else. 
                        // This will help to prevent concurrent updation
                        if (new CommonBLManager().IsRecordModified("QUESTION", questionKey,
                            (DateTime)ViewState["MODIFIED_DATE"]))
                        {
                            ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                                SingleQuestionEntry_bottomErrorMessageLabel,
                                    Resources.HCMResource.SingleQuestionEntry_ConcurrentEntryIsNotPossible);
                            return;
                        }

                        new QuestionBLManager().UpdateQuestion(questionDetail, questionKey,
                            "", base.userID);

                        redirectToEditFromEdit = true;
                    }
                }
                else
                {
                    new QuestionBLManager().SaveQuestion(questionDetail);
                    redirectToEditFromNew = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                    SingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }

            string redirectURL = "SingleQuestionEntryOpenText.aspx?m=0";

            if (redirectToEditFromNew == true)
            {
                // This will check whether the navigation came from Search Question page.
                // If the question is associated with the any tests, user tries to edit,
                // This will show the appropriate success message after its been saved.
                if (Request.QueryString["parentpage"] == Constants.ParentPage.SEARCH_QUESTION)
                {
                    redirectURL = redirectURL + "&s=2&questionKey=" + questionDetail.QuestionKey
                        + "&fromnew=y&parentpage=S_QSN&reseturl=s";
                    Response.Redirect(redirectURL, false);
                }
                else
                {
                    // Open newly saved question in edit mode.
                    Response.Redirect("SingleQuestionEntryOpenText.aspx?m=0&s=1&questionKey="
                        + questionDetail.QuestionKey
                        + "&fromnew=y&reseturl=s", false);
                }
            }
            else if (redirectToEditFromEdit == true)
            {
                // If the control comes from the search question page, highlight the parent page
                // menu (i.e. search question). Otherwise, highlight the SingleQuestionEntry menu.
                if (Request.QueryString["parentpage"] == Constants.ParentPage.SEARCH_QUESTION)
                    redirectURL = redirectURL + "&s=2&questionKey=" + questionKey
                        + "&fromedit=y&parentpage=S_QSN";
                else
                    redirectURL = redirectURL + "&s=1&questionKey=" + questionKey
                        + "&fromedit=y&parentpage=SNG_QUS";

                Response.Redirect(redirectURL, false);
            }
        }

        /// <summary>
        /// Handler that will check all the mandatory fields are filled. Then it allows the user
        /// to save a question.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void SingleQuestionEntry_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                bool isEmptySubject = false;

                ClearAllLabelMessage();

                SingleQuestionEntry_authorTextBox.Text =
                    Request[SingleQuestionEntry_authorTextBox.UniqueID].Trim();

                if (!IsValidData())
                    return;

                QuestionDetail questionDetail = new QuestionDetail();

                // Get selected subject list from the category subject control
                questionDetail.Subjects = SingleQuestionEntry_categorySubjectControl.SubjectDataSource;

                // Get added category list
                List<Subject> categories = SingleQuestionEntry_categorySubjectControl.CategoryDataSource;

                foreach (Subject subject in categories)
                {
                    // Get selected subject count
                    int SubjectCount = (from sub in questionDetail.Subjects
                                        where sub.IsSelected == true &&
                                        subject.CategoryID == sub.CategoryID
                                        select sub).Count();
                    if (SubjectCount != 0)
                        continue;

                    base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                            SingleQuestionEntry_bottomErrorMessageLabel,
                            string.Format("No subjects selected for '" + subject.CategoryName + "'"));
                    isEmptySubject = true;
                }

                // Terminate the further execution if no subject is selected against the category.
                if (isEmptySubject)
                    return;

                //

                SingleQuestionEntry_disclaimerPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                    SingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that helps to preview the question before it is saved.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void SingleQuestionEntry_previewButton_Click(object sender, EventArgs e)
        {
            try
            {
                SingleQuestionEntry_authorTextBox.Text
                    = Request[SingleQuestionEntry_authorTextBox.UniqueID].Trim();

                QuestionDetail questionDetails = ConstructQuestionDetails();

                SingleQuestionEntry_questionPreviewControl.QuestionDatasource = questionDetails;
                SingleQuestionEntry_questionPreviewControl.Title = "Preview";
                SingleQuestionEntry_questionPreviewModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                    SingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This will clear if the page mode is new question entry. Otherwise, it will reload
        /// the data against given question key.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void SingleQuestionEntry_resetLinkButton_Click(object sender, EventArgs e)
        {
            // Redirect to edit question entry page when the mode is edit
            // Otherwise, redirect to single question entry page
            if (Convert.ToBoolean(ViewState["IS_EDIT_MODE"]) == true)
            {
                if (Request.QueryString["reseturl"] != null)
                {
                    Response.Redirect("~/Questions/SingleQuestionEntryOpenText.aspx?m=0&s=1", false);
                }
                else
                {
                    Response.Redirect("~/Questions/SingleQuestionEntryOpenText.aspx?m=0&s=2&parentpage=S_QSN&questionkey="
                        + Request.QueryString["questionkey"], false);
                }
            }
            else
            {
                Response.Redirect("~/Questions/SingleQuestionEntryOpenText.aspx?m=0&s=1", false);
            }
        }

        
        /// <summary>
        /// Handler that helps to save the posted file in session
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void SingleQuestion_questionImageButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (SingleQuestionEntry_fileUpload.HasFile)
                {
                    if (SingleQuestionEntry_fileUpload.PostedFile.ContentLength > 102400) // check if image size exceeds 100kb
                    {
                        base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                    SingleQuestionEntry_bottomErrorMessageLabel, HCMResource.BatchQuestionEntry_imageSizeExceeded);
                        return;
                    }

                    string extension = Path.GetExtension(SingleQuestionEntry_fileUpload.FileName);
                    extension = extension.ToLower();
                    if (extension != ".gif" && extension != ".png" && extension != ".jpg" && extension != ".jpeg")
                    {
                        base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                            SingleQuestionEntry_bottomErrorMessageLabel,
                            "Only gif/png/jpg/jpeg files are allowed");
                        return;
                    }

                    // Read the bytes from the posted file.
                    HttpPostedFile postedQstImage = SingleQuestionEntry_fileUpload.PostedFile;
                    int nFileLen = postedQstImage.ContentLength;
                    byte[] imgData = new byte[nFileLen];

                    postedQstImage.InputStream.Read(imgData, 0, nFileLen);

                    Session["POSTED_QUESTION_IMAGE"] = imgData;
                    SingleQuestionEntry_questionImage.ImageUrl= @"~/Common/ImageHandler.ashx?source=QUESTION_IMAGE";
                    SingleQuestionEntry_AddQuestionImageTR.Style.Add("display", "none");
                    SingleQuestionEntry_DisplayQuestionImageTR.Style.Add("display", "");
                    SingleQuestionEntry_addImageLinkButton.Style.Add("display", "none");
                }
                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                    SingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler that helps to delete the posted file.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void SingleQuestion_deleteQstLnkButtonClick(object sender, EventArgs e)
        {
            try
            {
                Session["POSTED_QUESTION_IMAGE"] = null;
                SingleQuestionEntry_questionImage.ImageUrl = string.Empty;
                SingleQuestionEntry_AddQuestionImageTR.Style.Add("display", "none");
                SingleQuestionEntry_DisplayQuestionImageTR.Style.Add("display", "none");
                SingleQuestionEntry_addImageLinkButton.Style.Add("display", "");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                    SingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }
        }
        
        /// <summary>
        /// Handler that helps to close the popup.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CloseQuestionImagePopUpClick(object sender, EventArgs e)
        {
            try
            {
                Session["POSTED_QUESTION_IMAGE"] = null;

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                    SingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }
        }
        
        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method that will construct the question detail.
        /// </summary>
        /// <remarks>This method can be used by Save and Preview buttons. </remarks>
        /// <returns>
        /// A <see cref="QuestionDetail"/> that contains the question detail object.
        /// </returns>
        private QuestionDetail ConstructQuestionDetails()
        {
            // Construct question detail object.
            QuestionDetail questionDetail = new QuestionDetail();

            questionDetail.Question = SingleQuestionEntry_questionTextBox.Text.Trim();

            if (SingleQuestionEntry_complexityDropDownList.SelectedItem.Value != null &&
                SingleQuestionEntry_complexityDropDownList.SelectedItem.Value.Trim().ToUpper() != "--SELECT--")
            {
                questionDetail.Complexity = SingleQuestionEntry_complexityDropDownList.SelectedItem.Value;
                questionDetail.ComplexityName = SingleQuestionEntry_complexityDropDownList.SelectedItem.Text;
            }

            questionDetail.Tag = SingleQuestionEntry_tagsTextBox.Text.Trim();

            if (Convert.ToBoolean(ViewState["IS_EDIT_MODE"]) == true)
            {
                // Check if the CreditsEarned textbox is not empty
                if (SingleQuestionEntry_creditTextBox.Text.Length > 0)
                {
                    // Set modified credits to object field.
                    questionDetail.CreditsEarned
                        = Convert.ToDecimal(SingleQuestionEntry_creditTextBox.Text);
                    questionDetail.DeletedCategories
                        = SingleQuestionEntry_categorySubjectControl.DeletedCategories;
                }
            }
            else
            {
                // Set 0.00 if the page is single question entry
                questionDetail.CreditsEarned = 0.00M;
            }

            questionDetail.NoOfChoices = 0;
            questionDetail.QuestionType = QuestionType.OpenText;
            questionDetail.QuestionAttribute = new QuestionAttribute();
            questionDetail.QuestionAttribute.Marks = Convert.ToInt32(SingleQuestionEntry_marksTextBox.Text);
            questionDetail.QuestionAttribute.MaxLength = Convert.ToInt32(SingleQuestionEntry_maxLengthTextBox.Text);
            questionDetail.QuestionAttribute.AnswerReference = SingleQuestionEntry_answerReferenceTextBox.Text;

            // Construct the answer choice.
            List<AnswerChoice> answerChoices = new List<AnswerChoice>();
            questionDetail.AnswerChoices = null; ;

            if (!Utility.IsNullOrEmpty(SingleQuestionEntry_authorIdHiddenField.Value.ToString()))
                questionDetail.Author = Convert.ToInt32(SingleQuestionEntry_authorIdHiddenField.Value);

            // Additional settings (Test AreaId).
            if (!Utility.IsNullOrEmpty(SingleQuestionEntry_testAreaRadioButtonList.SelectedValue))
            {
                questionDetail.TestAreaID = SingleQuestionEntry_testAreaRadioButtonList.SelectedValue;
                questionDetail.TestAreaName = SingleQuestionEntry_testAreaRadioButtonList.SelectedItem.Text;
            }

            // Check if the page is edit mode. If so, retain the existing Created by
            // and CreateDate value as it is in the database.
            if (Convert.ToBoolean(ViewState["IS_EDIT_MODE"]) == true)
            {
                questionDetail.CreatedBy =
                    Convert.ToInt32(SingleQuestionEntry_createdByHiddenField.Value);
                questionDetail.CreatedDate =
                    Convert.ToDateTime(SingleQuestionEntry_createdDateHiddenField.Value);
            }
            else
            {
                questionDetail.CreatedBy = Convert.ToInt32(base.userID);
                questionDetail.CreatedDate = DateTime.Now;
            }

            questionDetail.ModifiedBy = Convert.ToInt32(base.userID);
            questionDetail.Subjects = SingleQuestionEntry_categorySubjectControl.SubjectDataSource;

            // Get selected subject ids only. This will be used in QuestionBLManager:InsertQuestionRelation
            // method to insert multiple subjects simultanesouly.
            questionDetail.SelectedSubjectIDs = SingleQuestionEntry_categorySubjectControl.SelectedSubjectIDList;
            if (Session["POSTED_QUESTION_IMAGE"] != null)
            {
                questionDetail.QuestionImage = Session["POSTED_QUESTION_IMAGE"] as byte[];
                questionDetail.HasImage = true;
                Session["POSTED_QUESTION_IMAGE"] = null;
            }
            else
                questionDetail.HasImage = false;
           
            return questionDetail;
        }

        /// <summary>
        /// Method that will load the question details for the given question key.
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that contains the question key.
        /// </param>
        private void LoadQuestionDetails(string questionKey)
        {
            // Construct the question details object
            QuestionDetail questionDetail = new QuestionBLManager().GetQuestion(questionKey);

            // Keep modified date in view state.
            ViewState["MODIFIED_DATE"] = questionDetail.ModifiedDate;

            // Call the method which returns true if the question key is already exists.
            bool canEdit = new QuestionBLManager().IsQuestionExistsInTest(questionKey);

            ViewState["CAN_EDIT"] = !canEdit;

            if (Convert.ToBoolean(ViewState["CAN_EDIT"]) == false)
            {
                base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                SingleQuestionEntry_bottomErrorMessageLabel,
                string.Format(Resources.HCMResource.SingleQuestionEntry_QuestionKeyAlreadyExists,
                questionKey.ToString().Trim()));
            }

            SingleQuestionEntry_questionIDValueLabel.Text = questionDetail.QuestionKey.Trim();
            SingleQuestionEntry_lastModifiedDateValueLabel.Text =
                questionDetail.ModifiedDate.ToString("MMM-dd-yyyy");

            SingleQuestionEntry_questionTextBox.Text = questionDetail.Question.ToString().Trim();
            SingleQuestionEntry_tagsTextBox.Text = questionDetail.Tag.ToString().Trim();
            SingleQuestionEntry_creditTextBox.Text = questionDetail.CreditsEarned.ToString();

            // Get user name/email by passing user id
            SingleQuestionEntry_authorTextBox.Text =
                new CommonBLManager().GetUserDetail(questionDetail.Author).FirstName;
            SingleQuestionEntry_authorIdHiddenField.Value = questionDetail.Author.ToString();
            SingleQuestionEntry_createdByHiddenField.Value = questionDetail.CreatedBy.ToString().Trim();
            SingleQuestionEntry_createdDateHiddenField.Value = questionDetail.CreatedDate.ToString();

            // Fetch unique categories from the list.
            var distinctCategories = questionDetail.Subjects.GroupBy(x => x.CategoryID)
                .Select(x => x.First());

            // Get selected subjects
            SingleQuestionEntry_categorySubjectControl.SubjectsToBeSelected = questionDetail.Subjects;

            SingleQuestionEntry_categorySubjectControl.QuestionKey = questionKey.ToString().Trim();
            SingleQuestionEntry_categorySubjectControl.CategoryDataSource = distinctCategories.ToList<Subject>();
            SingleQuestionEntry_categorySubjectControl.SubjectDataSource = distinctCategories.ToList<Subject>();

            SingleQuestionEntry_testAreaRadioButtonList.SelectedValue = questionDetail.TestAreaID;
            SingleQuestionEntry_complexityDropDownList.SelectedValue = questionDetail.Complexity;

            // Assign answer choices to viewstate. This will be used for adding new choice or delete choice.
            ViewState["ANSWER_CHOICES"] = questionDetail.AnswerChoices;

            if (questionDetail.QuestionAttribute != null)
            {
                SingleQuestionEntry_marksTextBox.Text = questionDetail.QuestionAttribute.Marks.ToString();
                SingleQuestionEntry_maxLengthTextBox.Text = questionDetail.QuestionAttribute.MaxLength.ToString();
                SingleQuestionEntry_answerReferenceTextBox.Text = questionDetail.QuestionAttribute.AnswerReference;
            }

            if (questionDetail.HasImage)
            {
                Session["POSTED_QUESTION_IMAGE"] = questionDetail.QuestionImage;
                SingleQuestionEntry_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=QUESTION_IMAGE&question_key=" + questionDetail.QuestionKey;
                SingleQuestionEntry_AddQuestionImageTR.Style.Add("display", "none");
                SingleQuestionEntry_DisplayQuestionImageTR.Style.Add("display", "");
                SingleQuestionEntry_addImageLinkButton.Style.Add("display", "none");
            }
        }

        /// <summary>
        /// Method taht helps to clear success/error label text.
        /// </summary>
        private void ClearAllLabelMessage()
        {
            SingleQuestionEntry_topErrorMessageLabel.Text = string.Empty;
            SingleQuestionEntry_bottomErrorMessageLabel.Text = string.Empty;
            SingleQuestionEntry_topSuccessMessageLabel.Text = string.Empty;
            SingleQuestionEntry_bottomSuccessMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// It checks the all the mandatory fields are filled without fail.
        /// </summary>
        /// <returns>
        /// It returns either TRUE or FALSE based on the validation.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            StringBuilder errorMessage = new StringBuilder();

            SingleQuestionEntry_topErrorMessageLabel.Text = string.Empty;
            SingleQuestionEntry_bottomErrorMessageLabel.Text = string.Empty;

            // Validate question description
            if (SingleQuestionEntry_questionTextBox.Text.Trim().Length == 0)
            {
                errorMessage.Append("Question, ");
                //base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                //    SingleQuestionEntry_bottomErrorMessageLabel,
                //    Resources.HCMResource.SingleQuestionEntry_QuestionCannotBeEmpty);
                //isValidData = false;
            }

            if (SingleQuestionEntry_marksTextBox.Text.Trim().Length == 0)
            {
                errorMessage.Append("Marks, ");
            }
            else
            {
                int marks;
                int.TryParse(SingleQuestionEntry_marksTextBox.Text, out marks);
                if (marks <= 0)
                {
                    errorMessage.Append("Marks, ");
                }
            }

            if (SingleQuestionEntry_maxLengthTextBox.Text.Trim().Length == 0)
            {
                errorMessage.Append("Max Length, ");
            }
            else
            {
                int marks;
                int.TryParse(SingleQuestionEntry_maxLengthTextBox.Text, out marks);
                if (marks <= 0)
                {
                    errorMessage.Append("Max Length, ");
                }
            }

            if (SingleQuestionEntry_answerReferenceTextBox.Text.Trim().Length == 0)
            {
                errorMessage.Append("Answer Reference, ");
            }

            // Validate category
            List<Subject> subjects = new List<Subject>();
            subjects = SingleQuestionEntry_categorySubjectControl.CategoryDataSource;
            bool subjectFlag = true;
            if (subjects == null || subjects.Count == 0)
            {
                errorMessage.Append("Category, ");
                //base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                //    SingleQuestionEntry_bottomErrorMessageLabel,
                //    Resources.HCMResource.SingleQuestionEntry_CategoryNotSelected);
                subjectFlag = false;
               // isValidData = false;
            }

            // Validate subjects
            string selectedSubjects = SingleQuestionEntry_categorySubjectControl.SelectedSubjectIDList;

            List<Subject> SubjectCategory = new List<Subject>();
            SubjectCategory = SingleQuestionEntry_categorySubjectControl.SubjectDataSource;
            if (!Utility.IsNullOrEmpty(SubjectCategory))
            {
                if (SubjectCategory.Exists(p => p.IsSelected == true))
                {
                    subjectFlag = true;
                }
                else
                {
                    subjectFlag = false;
                }
            }
            else
            {
                subjectFlag = false;
            }

            if (!subjectFlag)
            {
                errorMessage.Append("Subject, ");
                //base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                //       SingleQuestionEntry_bottomErrorMessageLabel,
                //       string.Format(Resources.HCMResource.SingleQuestionEntry_SubjectsNotSelected));
                //isValidData = false;
            }

            // Validate Testarea
            ListItem item = SingleQuestionEntry_testAreaRadioButtonList.SelectedItem;
            if (Utility.IsNullOrEmpty(item))
            {
                errorMessage.Append("Test Area, ");
                //base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                //    SingleQuestionEntry_bottomErrorMessageLabel,
                //    Resources.HCMResource.SingleQuestionEntry_TestAreaNotSelected);
                //isValidData = false;
            }

            // Validate Complexity
            if (SingleQuestionEntry_complexityDropDownList.SelectedIndex == 0)
            {
                errorMessage.Append("Complexity, ");
                //base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                //    SingleQuestionEntry_bottomErrorMessageLabel,
                //    Resources.HCMResource.SingleQuestionEntry_ComplexityNotSelected);
                //isValidData = false;
            }
            if (errorMessage.Length > 0)
            {
                base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                    SingleQuestionEntry_bottomErrorMessageLabel,
                    Resources.HCMResource.Common_Mandatory_Error_Message +" "+ errorMessage.ToString().Trim().TrimEnd(','));
                isValidData = false;
            }

            if (errorMessage.Length == 0)
            {
                if (Convert.ToInt32(SingleQuestionEntry_maxLengthTextBox.Text.Trim()) > 5000)
                {
                    base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
                       SingleQuestionEntry_bottomErrorMessageLabel,
                       "Max 5000 can be set for answer max limit");
                    isValidData = false;
                }
            }

            return isValidData;
        }

        /// <summary>
        /// It loads the default data to the respective controls.
        /// </summary>
        protected override void LoadValues()
        {
            Master.SetPageCaption(Resources.HCMResource.SingleQuestionEntry_Title);

            //Bind test area based on TEST_AREA attribute type
            SingleQuestionEntry_testAreaRadioButtonList.DataSource =
                new AttributeBLManager().GetTestAreas(base.tenantID, "A");
            SingleQuestionEntry_testAreaRadioButtonList.DataTextField = "AttributeName";
            SingleQuestionEntry_testAreaRadioButtonList.DataValueField = "AttributeID";

            SingleQuestionEntry_testAreaRadioButtonList.DataBind();

            //Bind complexities/attributes based on the attribute type
            SingleQuestionEntry_complexityDropDownList.DataSource =
                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
            SingleQuestionEntry_complexityDropDownList.DataTextField = "AttributeName";
            SingleQuestionEntry_complexityDropDownList.DataValueField = "AttributeID";
            SingleQuestionEntry_complexityDropDownList.DataBind();

            SingleQuestionEntry_complexityDropDownList.Items.Insert(0, "--Select--");
            SingleQuestionEntry_complexityDropDownList.SelectedIndex = 0;

            // Get disclaimer message from the database
            SingleQuestionEntry_disclaimerControl.Message =
                new CommonBLManager().GetDisclaimerMessage
                (Constants.DisclaimerMessageConstants.SAVE_QUESTION).ToString();
        }

        #endregion Protected Overridden Methods
    }
}