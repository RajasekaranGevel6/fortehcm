﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.

// SerachQuestion.cs
// File that represents the SearchQuestion class that defines the user interface
// layout and functionalities for the SearchQuestion page. This page helps in 
// searching for users by providing search criteria for Category,subject,Keyword,
// TestArea,Complexity,etc.. This class inherits Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives

using System;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Questions
{
    /// <summary>                                                                         
    /// Class that represents the user interface layout and functionalities
    /// for the SearchQuestion page. This page helps in searching for users by
    /// providing search criteria for Category,subject,Keyword,TestArea,Complexity,
    /// etc.. This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class SearchQuestion : PageBase
    {
        #region Private Members

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Members

        #region Events Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Method will call for every postback for retaining the 
                // maximized hidden field value.
                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    if (Support.Utility.IsNullOrEmpty(SearchQuestion_authorIdHiddenField.Value))
                        SearchQuestion_authorIdHiddenField.Value = base.userID.ToString();
                    //bind the all default values like TestArea,complexity
                    LoadValues();

                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU))
                    {
                        base.ClearSearchCriteriaSession();
                        Session["CATEGORY_SUBJECTS"] = null;
                    }

                    // Check if page is redirected from any child page. If the page
                    // if redirected from any child page, fill the search criteria
                    // and apply the search.
                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_QUESTION] != null)
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_QUESTION]
                            as QuestionDetailSearchCriteria);

                    // Reload the maximize hidden value.
                    CheckAndSetExpandorRestore();
                }

                // Set default button.
                Page.Form.DefaultButton = SearchQuestion_topSearchButton.UniqueID;

                // Clear the success & error messages in Top and Bottom
                SearchQuestion_topSuccessMessageLabel.Text = string.Empty;
                SearchQuestion_bottomSuccessMessageLabel.Text = string.Empty;
                SearchQuestion_topErrorMessageLabel.Text = string.Empty;
                SearchQuestion_bottomErrorMessageLabel.Text = string.Empty;

                // Paging Event 
                SearchQuestion_question_bottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                    (SearchQuestion_pagingNavigator_PageNumberClick);

                SearchQuestion_questionOpenText_bottomPagingNavigator.PageNumberClick += new
                   PageNavigator.PageNumberClickEventHandler
                   (SearchQuestion_pagingNavigator_PageNumberClick);

                // Category subject Control Message Event
                SearchQuestion_searchCategorySubjectControl.ControlMessageThrown += new
                    CategorySubjectControl.ControlMessageThrownDelegate
                    (SearchQuestion_searchCategorySubjectControl_ControlMessageThrown);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// It shows error/success messages whenever the user calls this method
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        private void SearchQuestion_searchCategorySubjectControl_ControlMessageThrown
            (object sender, ControlMessageEventArgs c)
        {
            try
            {
                // Show message on the label.
                if (c.MessageType == MessageType.Error)
                {
                    base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                        SearchQuestion_bottomErrorMessageLabel, c.Message);
                }
                else if (c.MessageType == MessageType.Success)
                {
                    base.ShowMessage(SearchQuestion_topSuccessMessageLabel,
                        SearchQuestion_bottomSuccessMessageLabel, c.Message);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchQuestion_pagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ViewState["PAGENUMBER"] = e.PageNumber;

                //Bind the Search Result based on the Pageno.
                LoadQuestion(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void SearchQuestion_questionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                        SearchQuestion_QuestionDetailPreviewControlDiv.Style["display"] = "block";
                        SearchQuestion_QuestionDetailPreviewControlOpenTextDiv.Style["display"] = "none";
                        SearchQuestion_QuestionDetailPreviewControl.LoadQuestionDetails
                            (e.CommandArgument.ToString(), 0);
                        SearchQuestion_QuestionDetailPreviewControl.Title = "Question Detail";
                        SearchQuestion_QuestionDetailPreviewControl.SetFocus();
                        SearchQuestion_questionModalPopupExtender.Show();
                   
                }
                else if (e.CommandName == "active")
                {
                    SearchQuestiont_confirmPopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchQuestion_Activate_Confirm_Message,
                        e.CommandArgument.ToString());
                    SearchQuestiont_confirmPopupExtenderControl.Title = "Activate Question";
                    SearchQuestiont_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchQuestiont_confirmPopupExtenderControl.DataBind();
                    SearchQuestion_ConfirmPopupExtender.Show();
                }
                else if (e.CommandName == "inactive")
                {
                    SearchQuestiont_confirmPopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchQuestion_DeActivate_Confirm_Message,
                        e.CommandArgument.ToString());
                    SearchQuestiont_confirmPopupExtenderControl.Title = "Deactivate Question";
                    SearchQuestiont_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchQuestion_ConfirmPopupExtender.Show();
                }
                else if (e.CommandName == "deletes")
                {
                    SearchQuestiont_confirmPopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchQuestion_Delete_Confirm_Message,
                        e.CommandArgument.ToString());
                    SearchQuestiont_confirmPopupExtenderControl.Title = "Delete Question";
                    SearchQuestiont_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchQuestion_ConfirmPopupExtender.Show();
                }
                else if (e.CommandName == "editquestion")
                {
                    string []split = e.CommandArgument.ToString().Split(new char [] {','});
                    QuestionType qType = (QuestionType) Enum.Parse(typeof(QuestionType), split[1]);

                    if (qType == QuestionType.MultipleChoice)
                    {
                        Response.Redirect("SingleQuestionEntry.aspx?m=0&s=2&parentpage=" +
                            Constants.ParentPage.SEARCH_QUESTION + "&questionKey=" + split[0], false);
                    }
                    else
                    {
                        Response.Redirect("SingleQuestionEntryOpenText.aspx?m=0&s=2&parentpage=" +
                            Constants.ParentPage.SEARCH_QUESTION + "&questionKey=" + split[0], false);
                    }
                }
                SearchQuestion_QuestionKey.Value = e.CommandArgument.ToString();
                SearchQuestion_Action.Value = e.CommandName.ToString();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void SearchQuestion_questionOpenTextGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {                    
                        SearchQuestion_QuestionDetailPreviewControlOpenTextDiv.Style["display"] = "block";
                        SearchQuestion_QuestionDetailPreviewControlDiv.Style["display"] = "none";
                        SearchQuestion_QuestionDetailPreviewControlOpenText.LoadQuestionDetails
                            (e.CommandArgument.ToString(), 0);
                        SearchQuestion_QuestionDetailPreviewControlOpenText.Title = "Question Detail";
                        SearchQuestion_QuestionDetailPreviewControlOpenText.SetFocus();
                        SearchQuestion_questionModalPopupExtender.Show();                   
                }
                else if (e.CommandName == "active")
                {
                    SearchQuestiont_confirmPopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchQuestion_Activate_Confirm_Message,
                        e.CommandArgument.ToString());
                    SearchQuestiont_confirmPopupExtenderControl.Title = "Activate Question";
                    SearchQuestiont_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchQuestiont_confirmPopupExtenderControl.DataBind();
                    SearchQuestion_ConfirmPopupExtender.Show();
                }
                else if (e.CommandName == "inactive")
                {
                    SearchQuestiont_confirmPopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchQuestion_DeActivate_Confirm_Message,
                        e.CommandArgument.ToString());
                    SearchQuestiont_confirmPopupExtenderControl.Title = "Deactivate Question";
                    SearchQuestiont_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchQuestion_ConfirmPopupExtender.Show();
                }
                else if (e.CommandName == "deletes")
                {
                    SearchQuestiont_confirmPopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchQuestion_Delete_Confirm_Message,
                        e.CommandArgument.ToString());
                    SearchQuestiont_confirmPopupExtenderControl.Title = "Delete Question";
                    SearchQuestiont_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchQuestion_ConfirmPopupExtender.Show();
                }
                else if (e.CommandName == "editquestion")
                {
                    string[] split = e.CommandArgument.ToString().Split(new char[] { ',' });
                    QuestionType qType = (QuestionType)Enum.Parse(typeof(QuestionType), split[1]);

                    if (qType == QuestionType.MultipleChoice)
                    {
                        Response.Redirect("SingleQuestionEntry.aspx?m=0&s=2&parentpage=" +
                            Constants.ParentPage.SEARCH_QUESTION + "&questionKey=" + split[0], false);
                    }
                    else
                    {
                        Response.Redirect("SingleQuestionEntryOpenText.aspx?m=0&s=2&parentpage=" +
                            Constants.ParentPage.SEARCH_QUESTION + "&questionKey=" + split[0], false);
                    }
                }
                SearchQuestion_QuestionKey.Value = e.CommandArgument.ToString();
                SearchQuestion_Action.Value = e.CommandName.ToString();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchQuestion_questionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                SearchQuestion_stateExpandHiddenField.Value = "0";
                SearchQuestion_testDraftExpandLinkButton.Text = "Expand All";
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                //SearchQuestion_questionOpenText_bottomPagingNavigator.Reset();
                SearchQuestion_question_bottomPagingNavigator.Reset();
                LoadQuestion(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void SearchQuestion_questionOpenTextGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                SearchQuestion_stateExpandHiddenField.Value = "0";
                SearchQuestion_testDraftExpandLinkButton.Text = "Expand All";
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                SearchQuestion_questionOpenText_bottomPagingNavigator.Reset();
                //SearchQuestion_question_bottomPagingNavigator.Reset();
                LoadQuestion(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void SearchQuestion_resetLinkButton_Click(object sender, EventArgs e)
        {
            #region Not used now


            //try
            //{
            //    SearchQuestion_questionGridView.DataSource = null;
            //    SearchQuestion_questionGridView.DataBind();
            //    SearchQuestion_bottomPagingNavigator.TotalRecords = 0;
            //    ResetFormControlValues(this);
            //    if (!Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
            //        ViewState["SORT_ORDER"] = "A";

            //    if (!Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
            //        ViewState["SORT_FIELD"] = "QUESTIONKEY";

            //}
            //catch (Exception exp)
            //{
            //    Logger.ExceptionLog(exp);
            //    SearchQuestion_topErrorMessageLabel.Text = exp.Message;
            //    SearchQuestion_bottomErrorMessageLabel.Text = exp.Message;
            //}
            //finally
            //{
            //    SearchQuestion_questionDiv.Visible = false;
            //}
            #endregion

            base.ClearSearchCriteriaSession();
            Session["CATEGORY_SUBJECTS"] = null;
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchQuestion_questionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                ImageButton SearchQuestion_activeImageButton = (ImageButton)e.Row.FindControl(
                    "SearchQuestion_activeImageButton");
                ImageButton SearchQuestion_deleteImageButton = (ImageButton)e.Row.FindControl(
                    "SearchQuestion_deleteImageButton");
                ImageButton SearchQuestion_inactiveImageButton = (ImageButton)e.Row.FindControl(
                    "SearchQuestion_inactiveImageButton");
                ImageButton SearchQuestion_editImageButton = (ImageButton)e.Row.FindControl(
                    "SearchQuestion_editImageButton");
                HiddenField SearchQuestion_statusHiddenField = (HiddenField)e.Row.FindControl(
                    "SearchQuestion_statusHiddenField");
                HiddenField CorrectAnswerHiddenField = (HiddenField)e.Row.FindControl(
                    "CorrectAnswerHiddenField");
                HiddenField TestIncludedHiddenField = (HiddenField)e.Row.FindControl(
                    "TestIncludedHiddenField");
                HiddenField DataAccessRightsHiddenField = (HiddenField)e.Row.FindControl(
                    "DataAccessRightsHiddenField");


                if (Convert.ToInt32(TestIncludedHiddenField.Value) > 0)
                {
                    SearchQuestion_deleteImageButton.Visible = false;
                }

                if (SearchQuestion_statusHiddenField.Value == "Active")
                {
                    SearchQuestion_activeImageButton.Visible = true;
                }
                else
                {
                    SearchQuestion_editImageButton.Visible = false;
                    SearchQuestion_inactiveImageButton.Visible = true;
                }

                if ((DataAccessRightsHiddenField.Value == "V"))
                {
                    SearchQuestion_editImageButton.Visible = false;
                    SearchQuestion_inactiveImageButton.Visible = false;
                    SearchQuestion_deleteImageButton.Visible = false;
                    SearchQuestion_activeImageButton.Visible = false;
                }

                LinkButton SearchQuestion_questionIdLinkButton =
                    (LinkButton)e.Row.FindControl("SearchQuestion_questionIdLinkButton");
                HtmlContainerControl SearchQuestion_detailsDiv =
                    (HtmlContainerControl)e.Row.FindControl("SearchQuestion_detailsDiv");
                HtmlAnchor SearchQuestion_focusDownLink =
                    (HtmlAnchor)e.Row.FindControl("SearchQuestion_focusDownLink");

                SearchQuestion_questionIdLinkButton.Attributes.Add("onclick", "return TestHideDetails('" +
                SearchQuestion_detailsDiv.ClientID + "','" + SearchQuestion_focusDownLink.ClientID + "')");

                Label SearchQuestion_questionLabel =
                    (Label)e.Row.FindControl("SearchQuestion_questionLabel");

                PlaceHolder SearchQuestion_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl(
                    "SearchQuestion_answerChoicesPlaceHolder");
                SearchQuestion_answerChoicesPlaceHolder.Controls.Add
                    (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions
                    (SearchQuestion_questionIdLinkButton.Text.ToString()),false));

                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void SearchQuestion_questionOpenTextGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                ImageButton SearchQuestion_activeImageButton = (ImageButton)e.Row.FindControl(
                    "SearchQuestion_openText_activeImageButton");
                ImageButton SearchQuestion_deleteImageButton = (ImageButton)e.Row.FindControl(
                    "SearchQuestion_openText_deleteImageButton");
                ImageButton SearchQuestion_inactiveImageButton = (ImageButton)e.Row.FindControl(
                    "SearchQuestion_openText_inactiveImageButton");
                ImageButton SearchQuestion_editImageButton = (ImageButton)e.Row.FindControl(
                    "SearchQuestion_openText_activeImageButton");
                HiddenField SearchQuestion_statusHiddenField = (HiddenField)e.Row.FindControl(
                    "SearchQuestion_openText_statusHiddenField");
                //HiddenField CorrectAnswerHiddenField = (HiddenField)e.Row.FindControl(
                //    "CorrectAnswerHiddenField");
                HiddenField TestIncludedHiddenField = (HiddenField)e.Row.FindControl(
                    "SearchQuestion_openText_TestIncludedHiddenField");
                HiddenField DataAccessRightsHiddenField = (HiddenField)e.Row.FindControl(
                    "SearchQuestion_openText_DataAccessRightsHiddenField");


                if (Convert.ToInt32(TestIncludedHiddenField.Value) > 0)
                {
                    SearchQuestion_deleteImageButton.Visible = false;
                }

                if (SearchQuestion_statusHiddenField.Value == "Active")
                {
                    SearchQuestion_activeImageButton.Visible = true;
                }
                else
                {
                    SearchQuestion_editImageButton.Visible = false;
                    SearchQuestion_inactiveImageButton.Visible = true;
                }

                if ((DataAccessRightsHiddenField.Value == "V"))
                {
                    SearchQuestion_editImageButton.Visible = false;
                    SearchQuestion_inactiveImageButton.Visible = false;
                    SearchQuestion_deleteImageButton.Visible = false;
                    SearchQuestion_activeImageButton.Visible = false;
                }

                LinkButton SearchQuestion_questionIdLinkButton =
                    (LinkButton)e.Row.FindControl("SearchQuestion_openText_questionIdLinkButton");
                HtmlContainerControl SearchQuestion_detailsDiv =
                    (HtmlContainerControl)e.Row.FindControl("SearchQuestion_openText_detailsDiv");
                HtmlAnchor SearchQuestion_focusDownLink =
                    (HtmlAnchor)e.Row.FindControl("SearchQuestion_openText_focusDownLink");

                SearchQuestion_questionIdLinkButton.Attributes.Add("onclick", "return TestHideDetails('" +
                SearchQuestion_detailsDiv.ClientID + "','" + SearchQuestion_focusDownLink.ClientID + "')");

                Label SearchQuestion_questionLabel =
                    (Label)e.Row.FindControl("SearchQuestion_openText_questionLabel");

                Literal SearchQuestion_questionOpenTextGridView_Answer =
                    (Literal)e.Row.FindControl("SearchQuestion_questionOpenTextGridView_Answer");

                //PlaceHolder SearchQuestion_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl(
                //    "SearchQuestion_answerChoicesPlaceHolder");
                //QuestionDetail questionDetail = (QuestionDetail)Session["QuestionDetail"];
              QuestionDetail questionDetail=  new BL.QuestionBLManager().GetQuestion
                    (SearchQuestion_questionIdLinkButton.Text.ToString());
                SearchQuestion_questionOpenTextGridView_Answer.Text = questionDetail.AnswerReference;
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchQuestion_questionGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                int sortColumnIndex = GetSortColumnIndex
                    (SearchQuestion_questionGridView,
                    (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void SearchQuestion_questionOpenTextGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                int sortColumnIndex = GetSortColumnIndex
                    (SearchQuestion_questionGridView,
                    (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Interchange Advanced and Simple Search Link 
        /// and Search Criteria fields 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchQuestion_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (SearchQuestion_simpleLinkButton.Text.ToLower() == "simple")
                {
                    SearchQuestion_simpleLinkButton.Text = "Advanced";
                    SearchQuestion_simpleSearchDiv.Visible = true;
                    SearchQuestion_advanceSearchDiv.Visible = false;
                }
                else
                {
                    SearchQuestion_simpleLinkButton.Text = "Simple";
                    SearchQuestion_simpleSearchDiv.Visible = false;
                    SearchQuestion_advanceSearchDiv.Visible = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchQuestion_topSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchQuestion_questionOpenText_bottomPagingNavigator.Reset();
                SearchQuestion_question_bottomPagingNavigator.Reset();
                ViewState["PAGENUMBER"] = "1";
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = "QUESTIONKEY";

                LoadQuestion(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and 
        /// OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void SearchQuestiont_okClick(object sender, EventArgs e)
        {
            try
            {
                QuestionBLManager questionBLManager = new QuestionBLManager();
                string searchQuestion_Action = SearchQuestion_Action.Value.ToString();
                string searchQuestionKey = SearchQuestion_QuestionKey.Value.Trim().ToString();
                if (searchQuestion_Action == "active")
                {
                    questionBLManager.ActivateQuestion(searchQuestionKey, base.userID);
                    base.ShowMessage(SearchQuestion_topSuccessMessageLabel,
                    SearchQuestion_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.SearchQuestion_Activate, searchQuestionKey));
                }
                else if (searchQuestion_Action == "inactive")
                {
                    questionBLManager.DeactivateQuestion(searchQuestionKey, base.userID);
                    base.ShowMessage(SearchQuestion_topSuccessMessageLabel,
                     SearchQuestion_bottomSuccessMessageLabel,
                     string.Format(Resources.HCMResource.SearchQuestion_InActivate, searchQuestionKey));
                }
                else if (searchQuestion_Action == "deletes")
                {
                    questionBLManager.DeleteQuestion(searchQuestionKey, base.userID);
                    base.ShowMessage(SearchQuestion_topSuccessMessageLabel,
                    SearchQuestion_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.SearchQuestion_Delete, searchQuestionKey));
                }
                LoadQuestion(Convert.ToInt32(ViewState["PAGENUMBER"].ToString()));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        ///  Event Handling for the common ConfirmMessage Popup Extenter Cancel and 
        ///  Close Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel/Close button Event will be raised.
        /// </remarks>
        protected void SearchQuestion_cancelClick(object sender, EventArgs e)
        {
            try
            {
                LoadQuestion(Convert.ToInt32(ViewState["PAGENUMBER"].ToString()));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the cancel link button is clicked.
        /// It will clear all the session values.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchQuestion_cancelLinkButton_Click(object sender, EventArgs e)
        {
            base.ClearSearchCriteriaSession();
            Session["CATEGORY_SUBJECTS"] = null;
            Response.Redirect("~/OTMHome.aspx", false);
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadQuestion(int pageNumber)
        {
            int totalRecords = 0;
            StringBuilder complexityID = GetSelectedComplexityID();
            StringBuilder testAreaID = GetTestAreaID();
            List<Subject> categorySubjects = null;

            QuestionDetailSearchCriteria questionDetailSearchCriteria = new QuestionDetailSearchCriteria();

            if (SearchQuestion_simpleLinkButton.Text == "Advanced")
            {
                questionDetailSearchCriteria.CategoryName =
                                    SearchQuestion_categoryTextBox.Text.Trim() == "" ?
                                    null : SearchQuestion_categoryTextBox.Text.Trim();
                questionDetailSearchCriteria.SubjectName =
                                    SearchQuestion_subjectTextBox.Text.Trim() == "" ?
                                    null : SearchQuestion_subjectTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    SearchQuestion_keywordsTextBox.Text.Trim() == "" ?
                                    null : SearchQuestion_keywordsTextBox.Text.Trim();
                questionDetailSearchCriteria.IsSimple = true;

                questionDetailSearchCriteria.CurrentPage = pageNumber;
                questionDetailSearchCriteria.IsMaximized =
                    SearchQuestion_restoreHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

                questionDetailSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                questionDetailSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();
                questionDetailSearchCriteria.Author = base.userID;
            }
            else
            {
                questionDetailSearchCriteria.IsAdvanced = true;
                string SelectedSubjectIDs = "";

                if (!Utility.IsNullOrEmpty(SearchQuestion_searchCategorySubjectControl.SubjectDataSource))
                {
                    List<Category> categoryList = new List<Category>();

                    foreach (Subject subject in SearchQuestion_searchCategorySubjectControl.SubjectDataSource)
                    {
                        if (categorySubjects == null)
                            categorySubjects = new List<Subject>();

                        Subject subjectCat = new Subject();

                        if (subject.IsSelected)
                        {
                            Category category = new Category();
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                            category.CategoryID = subject.CategoryID;
                            categoryList.Add(category);
                        }

                        // Get all the subjects and categories and then assign to the list.
                        subjectCat.CategoryID = subject.CategoryID;
                        subjectCat.CategoryName = subject.CategoryName;
                        subjectCat.SubjectID = subject.SubjectID;
                        subjectCat.SubjectName = subject.SubjectName;
                        subjectCat.IsSelected = subject.IsSelected;
                        categorySubjects.Add(subjectCat);
                    }

                    //Get all Subject ids in  Un selected Subject List.
                    Category categoryID = new Category();
                    foreach (Subject subject in SearchQuestion_searchCategorySubjectControl.SubjectDataSource)
                    {
                        categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                        if (categoryID == null)
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                    }
                }

                // Keep the categories and subjects in Session
                Session["CATEGORY_SUBJECTS"] = categorySubjects;

                questionDetailSearchCriteria.CategorySubjectsKey =
                                    SelectedSubjectIDs == "" ?
                                    null : SelectedSubjectIDs.TrimEnd(',');
                questionDetailSearchCriteria.TestAreaID =
                                    testAreaID.ToString() == "" ?
                                    null : testAreaID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.Complexities =
                                    complexityID.ToString() == "" ?
                                    null : complexityID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.QuestionKey =
                                    SearchQuestion_questionIDTextBox.Text.Trim() == "" ?
                                    null : SearchQuestion_questionIDTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    SearchQuestion_keywordTextBox.Text.Trim() == "" ?
                                    null : SearchQuestion_keywordTextBox.Text.Trim();
                questionDetailSearchCriteria.ActiveFlag =
                                    SearchQuestion_statusDropDownList.SelectedValue.ToString() == "" ?
                                    null : SearchQuestion_statusDropDownList.SelectedValue.ToString();
                questionDetailSearchCriteria.CreditsEarned =
                                    SearchQuestion_creditTextBox.Text.Trim() == "" ?
                                    0 : Convert.ToDecimal(SearchQuestion_creditTextBox.Text.Trim());
                questionDetailSearchCriteria.AuthorName = SearchQuestion_authorTextBox.Text.Trim() == "" ?
                                    null : SearchQuestion_authorIdHiddenField.Value;

                questionDetailSearchCriteria.Author = base.userID;

                questionDetailSearchCriteria.CurrentPage = pageNumber;
                questionDetailSearchCriteria.IsMaximized =
                    SearchQuestion_restoreHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

                questionDetailSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                questionDetailSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();
            }

            QuestionType questionType = (QuestionType)Convert.ToInt32(SearchQuestion_questionTypeDropDownList.SelectedValue.ToString());
            //questionDetailSearchCriteria.QuestionType = questionType;

            // Keep the search criteria in Session if the page is launched from 
            // somewhere else.
            Session[Constants.SearchCriteriaSessionKey.SEARCH_QUESTION] = questionDetailSearchCriteria;

            List<QuestionDetail> questionDetail =
                new BL.QuestionBLManager().GetQuestions(questionDetailSearchCriteria,questionType,
                    base.GridPageSize, pageNumber, questionDetailSearchCriteria.SortExpression,
                    questionDetailSearchCriteria.SortDirection, out totalRecords);

            if (questionDetail == null || questionDetail.Count == 0)
            {
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                if (questionType == QuestionType.MultipleChoice)
                {
                    SearchQuestion_questionGridView.DataSource = null;
                    SearchQuestion_questionGridView.DataBind();
                    SearchQuestion_question_bottomPagingNavigator.TotalRecords = 0;
                    SearchQuestion_questionDiv.Visible = false;
                    SearchQuestion_questionOpenTextDiv.Visible = false;
                    SearchQuestion_testDraftExpandLinkButton.Visible = false;
                }
                else
                {
                    SearchQuestion_questionOpenTextGridView.DataSource = null;
                    SearchQuestion_questionOpenTextGridView.DataBind();
                    SearchQuestion_questionOpenText_bottomPagingNavigator.TotalRecords = 0;
                    SearchQuestion_questionDiv.Visible = false;
                    SearchQuestion_questionOpenTextDiv.Visible = false;
                    SearchQuestion_testDraftExpandLinkButton.Visible = false;
                }
            }
            else
            {
                if (questionType == QuestionType.MultipleChoice)
                {
                    SearchQuestion_questionGridView.DataSource = questionDetail;
                    SearchQuestion_questionGridView.DataBind();
                    //SearchQuestion_question_bottomPagingNavigator.Visible = true;
                    SearchQuestion_questionOpenText_bottomPagingNavigator.TotalRecords = 0;
                    SearchQuestion_question_bottomPagingNavigator.PageSize = base.GridPageSize;
                    SearchQuestion_question_bottomPagingNavigator.TotalRecords = totalRecords;
                    SearchQuestion_questionDiv.Visible = true;
                    SearchQuestion_questionOpenTextDiv.Visible = false;
                    SearchQuestion_testDraftExpandLinkButton.Visible = true;
                }
                else
                {
                    SearchQuestion_questionOpenTextGridView.DataSource = questionDetail;
                    SearchQuestion_questionOpenTextGridView.DataBind();                     
                    SearchQuestion_question_bottomPagingNavigator.TotalRecords = 0;
                    SearchQuestion_questionOpenText_bottomPagingNavigator.PageSize = base.GridPageSize;
                    SearchQuestion_questionOpenText_bottomPagingNavigator.TotalRecords = totalRecords;
                    SearchQuestion_questionDiv.Visible = false;
                    SearchQuestion_questionOpenTextDiv.Visible = true;
                    SearchQuestion_testDraftExpandLinkButton.Visible = true;
                }
            }
        }

        /// <summary>
        /// Javascript Event Handler Declared here.
        /// </summary>
        private void JavaScriptHandler()
        {
            GridRestore();

            SearchQuestion_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                + SearchQuestion_dummyAuthorID.ClientID + "','"
                + SearchQuestion_authorIdHiddenField.ClientID + "','"
                + SearchQuestion_authorTextBox.ClientID + "','QA')");
        }

        /// <summary>
        ///  Bind complexities/attributes based on the attribute type
        /// </summary>
        private void BindComplexities()
        {
            try
            {
                SearchQuestion_complexityCheckBoxList.DataSource =
                    new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY,
                    Constants.SortTypeConstants.ASCENDING);
                SearchQuestion_complexityCheckBoxList.DataTextField = "AttributeName";
                SearchQuestion_complexityCheckBoxList.DataValueField = "AttributeID";
                SearchQuestion_complexityCheckBoxList.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Bind test area based on TEST_AREA attribute type
        /// </summary>
        private void BindTestArea()
        {
            try
            {
                SearchQuestion_testAreaCheckBoxList.DataSource = new AttributeBLManager().GetTestAreas(base.tenantID);
                   //new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA,
                   //Constants.SortTypeConstants.ASCENDING);
                SearchQuestion_testAreaCheckBoxList.DataTextField = "AttributeName";
                SearchQuestion_testAreaCheckBoxList.DataValueField = "AttributeID";
                SearchQuestion_testAreaCheckBoxList.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchQuestion_topErrorMessageLabel,
                SearchQuestion_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Get Selected Test Area IDs and its used in the Search.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetTestAreaID()
        {
            StringBuilder testAreaID = new StringBuilder();
            for (int i = 0; i < SearchQuestion_testAreaCheckBoxList.Items.Count; i++)
            {
                if (SearchQuestion_testAreaCheckBoxList.Items[i].Selected)
                {
                    testAreaID.Append(SearchQuestion_testAreaCheckBoxList.Items[i].Value.ToString() + ",");
                }
            }
            return testAreaID;
        }

        /// <summary>
        /// Get Selected Complexity IDs and its used in the Search.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetSelectedComplexityID()
        {
            StringBuilder complexityID = new StringBuilder();
            for (int i = 0; i < SearchQuestion_complexityCheckBoxList.Items.Count; i++)
            {
                if (SearchQuestion_complexityCheckBoxList.Items[i].Selected)
                {
                    complexityID.Append(SearchQuestion_complexityCheckBoxList.Items[i].Value.ToString() + ",");
                }
            }
            return complexityID;
        }

        /// <summary>
        /// Expand & Restore the Search Result grid, while clicking the Grid right corner Image.
        /// </summary>
        private void GridRestore()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchQuestion_restoreHiddenField.Value) &&
             SearchQuestion_restoreHiddenField.Value == "Y")
            {
                SearchQuestion_searchCriteriasDiv.Style["display"] = "none";
                SearchQuestion_searchResultsUpSpan.Style["display"] = "block";
                SearchQuestion_searchResultsDownSpan.Style["display"] = "none";
                SearchQuestion_questionDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                SearchQuestion_searchCriteriasDiv.Style["display"] = "block";
                SearchQuestion_searchResultsUpSpan.Style["display"] = "none";
                SearchQuestion_searchResultsDownSpan.Style["display"] = "block";
                SearchQuestion_questionDiv.Style["height"] = RESTORED_HEIGHT;
            }
            SearchQuestion_searchResultsUpSpan.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                SearchQuestion_questionDiv.ClientID + "','" +
                SearchQuestion_searchCriteriasDiv.ClientID + "','" +
                SearchQuestion_searchResultsUpSpan.ClientID + "','" +
                SearchQuestion_searchResultsDownSpan.ClientID + "','" +
                SearchQuestion_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
            SearchQuestion_searchResultsDownSpan.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                SearchQuestion_questionDiv.ClientID + "','" +
                SearchQuestion_searchCriteriasDiv.ClientID + "','" +
                SearchQuestion_searchResultsUpSpan.ClientID + "','" +
                SearchQuestion_searchResultsDownSpan.ClientID + "','" +
                SearchQuestion_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
        }

        /// <summary>
        /// Method that will fill the search criteria to the appropriate fields.
        /// </summary>
        /// <param name="questionDetailSearchCriteria">
        /// A <see cref="QuestionDetailSearchCriteria"/> that contains the search
        /// criteria fields.
        /// </param>
        public void FillSearchCriteria(QuestionDetailSearchCriteria questionDetailSearchCriteria)
        {
            string[] testAreaIDs = null;
            string[] complexities = null;

            // Check if the simple link button is clicked.
            if (questionDetailSearchCriteria.IsSimple)
            {
                SearchQuestion_simpleLinkButton.Text = "Advanced";
                SearchQuestion_simpleSearchDiv.Visible = true;
                SearchQuestion_advanceSearchDiv.Visible = false;

                if (questionDetailSearchCriteria.CategoryName != null)
                    SearchQuestion_categoryTextBox.Text = questionDetailSearchCriteria.CategoryName;

                if (questionDetailSearchCriteria.SubjectName != null)
                    SearchQuestion_subjectTextBox.Text = questionDetailSearchCriteria.SubjectName;

                if (questionDetailSearchCriteria.Tag != null)
                    SearchQuestion_keywordsTextBox.Text = questionDetailSearchCriteria.Tag;

                SearchQuestion_restoreHiddenField.Value =
                    questionDetailSearchCriteria.IsMaximized == true ? "Y" : "N";

                ViewState["SORT_FIELD"] = questionDetailSearchCriteria.SortExpression;
                ViewState["SORT_ORDER"] = questionDetailSearchCriteria.SortDirection;

                // Apply search
                LoadQuestion(questionDetailSearchCriteria.CurrentPage);
                //QuestionType quest
                if (SearchQuestion_questionTypeDropDownList.SelectedValue.ToString() == "1")
                {
                    SearchQuestion_question_bottomPagingNavigator.MoveToPage
                        (questionDetailSearchCriteria.CurrentPage);
                }
                else
                {
                    SearchQuestion_questionOpenText_bottomPagingNavigator.MoveToPage
                        (questionDetailSearchCriteria.CurrentPage);
                }
            }
            else
            {
                SearchQuestion_simpleLinkButton.Text = "Simple";
                SearchQuestion_simpleSearchDiv.Visible = false;
                SearchQuestion_advanceSearchDiv.Visible = true;

                if (!Utility.IsNullOrEmpty(questionDetailSearchCriteria.TestAreaID))
                {
                    testAreaIDs = questionDetailSearchCriteria.TestAreaID.Split(new char[] { ',' });

                    // Make selection test area.
                    for (int idx = 0; idx < testAreaIDs.Length; idx++)
                        SearchQuestion_testAreaCheckBoxList.Items.FindByValue(testAreaIDs[idx]).Selected = true;
                }

                if (!Utility.IsNullOrEmpty(questionDetailSearchCriteria.Complexities))
                {
                    complexities = questionDetailSearchCriteria.Complexities.Split(new char[] { ',' });

                    // Make selection of complexities.
                    for (int idx = 0; idx < complexities.Length; idx++)
                        SearchQuestion_complexityCheckBoxList.Items.FindByValue(complexities[idx]).Selected = true;
                }

                if (questionDetailSearchCriteria.QuestionKey != null)
                    SearchQuestion_questionIDTextBox.Text = questionDetailSearchCriteria.QuestionKey;

                if (questionDetailSearchCriteria.Tag != null)
                    SearchQuestion_keywordTextBox.Text = questionDetailSearchCriteria.Tag;

                if (questionDetailSearchCriteria.ActiveFlag != null)
                    SearchQuestion_statusDropDownList.SelectedValue = questionDetailSearchCriteria.ActiveFlag;

                if (questionDetailSearchCriteria.CreditsEarned.ToString() != null)
                    SearchQuestion_creditTextBox.Text = questionDetailSearchCriteria.CreditsEarned.ToString();

                if (questionDetailSearchCriteria.AuthorName != null)
                    SearchQuestion_authorTextBox.Text = questionDetailSearchCriteria.AuthorName;

                if (Session["CATEGORY_SUBJECTS"] != null)
                {
                    QuestionDetail questionDetail = new QuestionDetail();
                    questionDetail.Subjects = Session["CATEGORY_SUBJECTS"] as List<Subject>;

                    // Fetch unique categories from the list.
                    var distinctCategories = (Session["CATEGORY_SUBJECTS"] as List<Subject>).GroupBy
                        (x => x.CategoryID).Select(x => x.First());

                    // This datasource is used to make the selection of the subjects
                    // which are already selected by the user to search.
                    SearchQuestion_searchCategorySubjectControl.SubjectsToBeSelected =
                        Session["CATEGORY_SUBJECTS"] as List<Subject>;

                    // Bind distinct categories in the category datalist
                    SearchQuestion_searchCategorySubjectControl.CategoryDataSource =
                        distinctCategories.ToList<Subject>();

                    // Bind subjects for the categories which are bind previously.
                    SearchQuestion_searchCategorySubjectControl.SubjectDataSource =
                        distinctCategories.ToList<Subject>();
                }

                SearchQuestion_restoreHiddenField.Value =
                    questionDetailSearchCriteria.IsMaximized == true ? "Y" : "N";
                ViewState["SORT_FIELD"] = questionDetailSearchCriteria.SortExpression;
                ViewState["SORT_ORDER"] = questionDetailSearchCriteria.SortDirection;

                // Apply search
                LoadQuestion(questionDetailSearchCriteria.CurrentPage);
                if (SearchQuestion_questionTypeDropDownList.SelectedValue.ToString() == "1")
                {
                    SearchQuestion_question_bottomPagingNavigator.MoveToPage
                        (questionDetailSearchCriteria.CurrentPage);
                }
                else
                {
                    SearchQuestion_questionOpenText_bottomPagingNavigator.MoveToPage(questionDetailSearchCriteria.CurrentPage);
                }
            }
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Utility.IsNullOrEmpty(SearchQuestion_restoreHiddenField.Value) &&
                         SearchQuestion_restoreHiddenField.Value == "Y")
            {
                SearchQuestion_searchCriteriasDiv.Style["display"] = "none";
                SearchQuestion_searchResultsUpSpan.Style["display"] = "block";
                SearchQuestion_searchResultsDownSpan.Style["display"] = "none";
                SearchQuestion_questionDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                SearchQuestion_searchCriteriasDiv.Style["display"] = "block";
                SearchQuestion_searchResultsUpSpan.Style["display"] = "none";
                SearchQuestion_searchResultsDownSpan.Style["display"] = "block";
                SearchQuestion_questionDiv.Style["height"] = RESTORED_HEIGHT;
            }

            if (!Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_QUESTION]))
                if (!Utility.IsNullOrEmpty(SearchQuestion_restoreHiddenField.Value))
                    ((QuestionDetailSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_QUESTION]).IsMaximized =
                        SearchQuestion_restoreHiddenField.Value == "Y" ? true : false;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindTestArea();
            BindComplexities();
            JavaScriptHandler();

            // Set default focus field.
            Page.Form.DefaultFocus = SearchQuestion_categoryTextBox.UniqueID;
            SearchQuestion_categoryTextBox.Focus();
            SearchQuestion_simpleSearchDiv.Visible = true;
            SearchQuestion_advanceSearchDiv.Visible = false;

            if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                ViewState["SORT_ORDER"] = SortType.Ascending;
            if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                ViewState["SORT_FIELD"] = "QUESTIONKEY";
            if (Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                ViewState["PAGENUMBER"] = "1";

            Master.SetPageCaption(Resources.HCMResource.SearchQuestion_Title);
        }

        #endregion Protected Overridden Methods
    }
}