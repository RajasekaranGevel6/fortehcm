﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewContributorSummary.cs
// File that represents the user interface for View contributor summary page.
// This will helps to view the question contributor information.

#endregion Header

#region Directives                                                             
using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Questions
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities for
    /// the View contributor summary page. This page helps to view the 
    /// question contributor summary details
    /// </summary>
    public partial class ViewContributorSummary : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                
                ClearMessageLabels();
                CheckAndSetExpandOrRestore();
                ViewContributorSummary_questionDetailBottomPageNavigator.PageNumberClick
                    += new PageNavigator.PageNumberClickEventHandler
                        (ViewContributorSummary_questionDetailBottomPageNavigator_PageNumberClick);
                //Set the master page caption
                Master.SetPageCaption("View Contributor Summary");
                if (!IsPostBack)
                {
                    SubscribtClientSideHandlers();
                    //Assign the user id to the author idhidden field
                    ViewContributorSummary_authorIDHiddenField.Value = base.userID.ToString();
                    //save the sort order in view state
                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    //Save the sort field in view state
                    ViewState["SORT_FIELD"] = "QUESTIONKEY";
                    //Load the values for the user id 
                    LoadValues();
                    if (base.isAdmin)
                        ViewContributorSummary_searchImage.Visible = true;
                }
                // This validation is done for focusing the linkbutton of the input
                // fields such as category, subject, test area.
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewContributorSummary_browserHiddenField.Value))
                {
                    ValidateEnterKey(ViewContributorSummary_browserHiddenField.Value.Trim());
                    ViewContributorSummary_browserHiddenField.Value = string.Empty;
                }
                //Create the chart the question author 
                //CreateQuestionAuthorChart();
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                ShowMessage(ViewContributorSummary_bottomErrorMessageLabel,
                    ViewContributorSummary_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/> that holds the event data.
        /// </param>
        void ViewContributorSummary_questionDetailBottomPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                //Set the question details based on the author id 
                // hidden field and the selected pagenumber 
                SetQuestionDetails(e.PageNumber);
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                ShowMessage(ViewContributorSummary_topErrorMessageLabel,
                    ViewContributorSummary_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called on the row data bound of the grid view 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void ViewContributorSummary_questionDetailsGridView_OnRowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //Find the test included link button 
                    LinkButton ViewContributorSummary_testIncludedLinkButton = (LinkButton)
                    e.Row.FindControl("ViewContributorSummary_testIncludedLinkButton");

                    //Find the questio id link button 
                    LinkButton ViewContributorSummary_questionLinkButton = (LinkButton)
                        e.Row.FindControl("ViewContributorSummary_questionLinkButton");

                    //Find the test included link button 
                    Label ViewContributorSummary_testIncludedLabel = (Label)
                        e.Row.FindControl("ViewContributorSummary_testIncludedLabel");

                    //Get the number of test included 
                    string testIncluded = ViewContributorSummary_testIncludedLabel.Text.Trim();

                    // Testincluded > 0, hyperlink will be provided.
                    // Otherwise, it will be shown in a label
                    if (testIncluded != "0")
                    {
                        ViewContributorSummary_testIncludedLinkButton.Text = testIncluded.ToString();
                        ViewContributorSummary_testIncludedLinkButton.Attributes.Add("onclick",
                            "javascript:ShowTestInclusion('"
                            + ViewContributorSummary_questionLinkButton.Text.Trim()
                            + "'); return false;");
                        ViewContributorSummary_testIncludedLinkButton.Visible = true;
                        ViewContributorSummary_testIncludedLabel.Visible = false;
                    }
                    else
                    {
                        ViewContributorSummary_testIncludedLinkButton.Visible = false;
                        ViewContributorSummary_testIncludedLabel.Visible = true;
                    }
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                ShowMessage(ViewContributorSummary_bottomErrorMessageLabel,
                    ViewContributorSummary_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the cancel link is clicked 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ViewContributorSummary_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Redirect the page to its raw url
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                ShowMessage(ViewContributorSummary_bottomErrorMessageLabel,
                    ViewContributorSummary_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called on the sorting of the data grid view
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        protected void ViewContributorSummary_questionDetailsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                //check whether the sort field in view state is same as 
                //the sort expression 
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    //Swap the sort order in the view state
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                //Store the sort field in the view state
                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                ViewContributorSummary_questionDetailBottomPageNavigator.Reset();

                //Set question details based on the values
                SetQuestionDetails(1);
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                ShowMessage(ViewContributorSummary_bottomErrorMessageLabel,
                    ViewContributorSummary_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called on the row command of the data grid view
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        protected void ViewContributorSummary_questionDetailsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                //Check whether the command name is view 
                if (e.CommandName == "view")
                {
                    //Load the question details for the corresponding question id 
                    ViewContributorSummary_questionDetailPreviewControl.LoadQuestionDetails
                        (e.CommandArgument.ToString(), 0);
                    //Assign the title 
                    ViewContributorSummary_questionDetailPreviewControl.Title = "Question Detail";
                    //show the modal popup
                    ViewContributorSummary_questionDetailPreviewControl.SetFocus();
                    ViewContributorSummary_questionModalPopupExtender.Show();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(ViewContributorSummary_bottomErrorMessageLabel,
                    ViewContributorSummary_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the  row is created of the data grid view
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        protected void ViewContributorSummary_questionDetailsGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    //Get the column index for the sorting 
                    int sortColumnIndex = GetSortColumnIndex
                        (ViewContributorSummary_questionDetailsGridView,
                        ViewState["SORT_FIELD"].ToString());

                    if (sortColumnIndex != -1)
                    {
                        //Add sort image for the corresponding column
                        AddSortImage(sortColumnIndex, e.Row,
                            (SortType)ViewState["SORT_ORDER"]);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(ViewContributorSummary_bottomErrorMessageLabel,
                    ViewContributorSummary_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called on load details button  click
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ViewContributorSummary_loadDetailsButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                //Reset the paging control
                ViewContributorSummary_questionDetailBottomPageNavigator.Reset();

                //save the sort order in view state
                ViewState["SORT_ORDER"] = SortType.Ascending;

                //Save the sort field in view state
                ViewState["SORT_FIELD"] = "QUESTIONKEY";

                //Load values based on the viewstate values 
                LoadValues();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                ShowMessage(ViewContributorSummary_bottomErrorMessageLabel,
                    ViewContributorSummary_topErrorMessageLabel, exception.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method used to create the charts 
        /// </summary>
        private void CreateQuestionAuthorChart()
        {
            //Get the data for the first total questions authored chart
            ContributorSummary contributorSummary = new QuestionBLManager().
                GetContributorChartDetails(int.Parse
                (ViewContributorSummary_authorIDHiddenField.Value));

            if (contributorSummary.QuestionsAuthoredData.ChartData.Count == 0)
            {
                ViewContributorSummary_totalQuestionAuthored_noDataLabel.Text = string.Empty;

                //display error message if no chart data 
                ShowMessage(ViewContributorSummary_totalQuestionAuthored_noDataLabel,
                    "No chart data to display");

                ViewContributorSummary_totalQuestionsAuthoredChart.Visible = false;
            }
            else
            {
                //Assign the values for the question authored chart control

                ViewContributorSummary_totalQuestionsAuthoredChart.Visible = true;

                contributorSummary.QuestionsAuthoredData.ChartLength = 200;

                contributorSummary.QuestionsAuthoredData.ChartWidth = 200;

                contributorSummary.QuestionsAuthoredData.ChartType = SeriesChartType.Column;

                contributorSummary.QuestionsAuthoredData.IsDisplayChartTitle = true;

                contributorSummary.QuestionsAuthoredData.ChartTitle =
                    "Total Questions Authored";

                contributorSummary.QuestionsAuthoredData.IsDisplayAxisTitle = true;

                contributorSummary.QuestionsAuthoredData.XAxisTitle = "Months";

                contributorSummary.QuestionsAuthoredData.YAxisTitle = "Questions Authored";

                contributorSummary.QuestionsAuthoredData.IsDisplayLegend = false;

                //Assign the data source for the chart control 
                ViewContributorSummary_totalQuestionsAuthoredChart.DataSource =
                    contributorSummary.QuestionsAuthoredData;
            }

            if (contributorSummary.QuestionUsageCountData.ChartData.Count == 0)
            {
                ViewContributorSummary_questionUsageSummary_noDataLabel.Text = string.Empty;
                //display error message if no chart data 
                ShowMessage(ViewContributorSummary_questionUsageSummary_noDataLabel,
                    "No chart data to display");
                ViewContributorSummary_questionUsageSummaryChart.Visible = false;
            }
            else
            {
                //Assign the values for the question usage summary chart control
                ViewContributorSummary_questionUsageSummaryChart.Visible = true;

                //Assign the data for the question usage count chart
                contributorSummary.QuestionUsageCountData.ChartLength = 200;

                contributorSummary.QuestionUsageCountData.ChartWidth = 200;

                contributorSummary.QuestionUsageCountData.ChartType = SeriesChartType.Column;

                contributorSummary.QuestionUsageCountData.IsDisplayChartTitle = true;

                contributorSummary.QuestionUsageCountData.ChartTitle = "Question Usage Count";

                contributorSummary.QuestionUsageCountData.IsDisplayAxisTitle = true;

                contributorSummary.QuestionUsageCountData.XAxisTitle = "Months";

                contributorSummary.QuestionUsageCountData.YAxisTitle = "Question Usage";

                contributorSummary.QuestionUsageCountData.IsDisplayLegend = false;

                contributorSummary.QuestionUsageCountData.IsChangeSeriesColor = true;

                contributorSummary.QuestionUsageCountData.PaletteName = ChartColorPalette.Berry;

                ViewContributorSummary_questionUsageSummaryChart.DataSource =
                    contributorSummary.QuestionUsageCountData;
            }

            if (contributorSummary.CreditsEarnedData.ChartData.Count == 0)
            {
                ViewContributorSummary_creditsEarned_noDataLabel.Text = string.Empty;

                ShowMessage(ViewContributorSummary_creditsEarned_noDataLabel,
                    "No chart data to display");

                ViewContributorSummary_creditsEarnedChart.Visible = false;
            }
            else
            {
                ViewContributorSummary_creditsEarnedChart.Visible = true;

                //Assign the data for the credits earned chart
                contributorSummary.CreditsEarnedData.ChartLength = 200;

                contributorSummary.CreditsEarnedData.ChartWidth = 200;

                contributorSummary.CreditsEarnedData.ChartType = SeriesChartType.Column;

                contributorSummary.CreditsEarnedData.IsDisplayChartTitle = true;

                contributorSummary.CreditsEarnedData.ChartTitle = "Credits Earned";

                contributorSummary.CreditsEarnedData.IsDisplayAxisTitle = true;

                contributorSummary.CreditsEarnedData.XAxisTitle = "Months";

                contributorSummary.CreditsEarnedData.YAxisTitle = "Credits Earned (in $)";

                contributorSummary.CreditsEarnedData.IsDisplayLegend = false;

                contributorSummary.CreditsEarnedData.IsChangeSeriesColor = true;

                contributorSummary.CreditsEarnedData.PaletteName = ChartColorPalette.BrightPastel;

                ViewContributorSummary_creditsEarnedChart.DataSource = contributorSummary.CreditsEarnedData;
            }
        }

        /// <summary>
        /// Method used to set the author name and first name for the first time
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user id 
        /// </param>
        private void SetAuthorDetails()
        {
            //Get the author id from the hidden field
            int userID = int.Parse(ViewContributorSummary_authorIDHiddenField.Value);

            //Get the user details from DB and assign it
            UserDetail userDetail = new QuestionBLManager().GetAuthorIDAndName(userID);

            if (userDetail == null)
                return;

            ViewContributorSummary_questionAuthorIDTextBox.Text = userDetail.FirstName;
        }

        /// <summary>
        /// Method used to set the question summary details for the author
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user ID 
        /// </param>
        private void SetQuestionSummaryDetails()
        {
            //Get the author id from the hidden field
            int userID = int.Parse(ViewContributorSummary_authorIDHiddenField.Value);

            //Get the contributor summary details from the databse
            ContributorSummary contributorSummary = new QuestionBLManager()
                .GetContributorSummary(userID);

            //return if contributor summary is null
            if (contributorSummary == null)
            {
                ViewContributorSummary_contributorSinceValueLabel.Text = "";
                ViewContributorSummary_noOfQuestionsAuthoredValueLabel.Text = " 0";
                ViewContributorSummary_creditsEarnedValueLabel.Text = "0.00";
                ViewContributorSummary_noOfCategoriesContributedValueLabel.Visible = true;
                ViewContributorSummary_noOfCategoriesContributedValueLabel.Text = "0";
                ViewContributorSummary_noOfSubjectsContributedValueLabel.Visible = true;
                ViewContributorSummary_noOfSubjectsContributedValueLabel.Text = "0";
                ViewContributorSummary_questionUsageValueLabel.Text = "0";
                ViewContributorSummary_noOfCategoriesContibutedHyperLink.Visible = false;
                ViewContributorSummary_noOfSubjectsContributedHyperLink.Visible = false;
                return;
            }

            //if date is default date. display blank space
            ViewContributorSummary_contributorSinceValueLabel.Text =
                contributorSummary.ContributorSince.ToString("MM/dd/yyyy")
                == "01/01/0001" ? "" : contributorSummary.ContributorSince.ToString("MM/dd/yyyy");

            //Assign the values for the fields
            ViewContributorSummary_noOfQuestionsAuthoredValueLabel.Text =
                contributorSummary.QuestionsAuthored.ToString();

            ViewContributorSummary_creditsEarnedValueLabel.Text =
                contributorSummary.CreditsEarned.ToString().Trim() == "0.00" ? "0" :
            contributorSummary.CreditsEarned.ToString();

            //if categories contributes is 0 display label 
            if (contributorSummary.CategoriesContributed == 0)
            {
                ViewContributorSummary_noOfCategoriesContributedValueLabel.Visible = true;
                ViewContributorSummary_noOfCategoriesContibutedHyperLink.Visible = false;
                ViewContributorSummary_noOfCategoriesContributedValueLabel.Text =
                    contributorSummary.CategoriesContributed.ToString();
            }
            //else display link button 
            else
            {
                ViewContributorSummary_noOfCategoriesContibutedHyperLink.Visible = true;
                ViewContributorSummary_noOfCategoriesContibutedHyperLink.Text =
                    contributorSummary.CategoriesContributed.ToString();
                ViewContributorSummary_noOfCategoriesContributedValueLabel.Visible = false;
            }
            //if subjects contributes is 0 display label 
            if (contributorSummary.SubjectsContributed == 0)
            {
                ViewContributorSummary_noOfSubjectsContributedValueLabel.Visible = true;
                ViewContributorSummary_noOfSubjectsContributedHyperLink.Visible = false;
                ViewContributorSummary_noOfSubjectsContributedValueLabel.Text =
                    contributorSummary.CategoriesContributed.ToString();
            }
            //else display link button 
            else
            {
                ViewContributorSummary_noOfSubjectsContributedHyperLink.Visible = true;
                ViewContributorSummary_noOfSubjectsContributedHyperLink.Text =
                    contributorSummary.SubjectsContributed.ToString();
                ViewContributorSummary_noOfSubjectsContributedValueLabel.Visible = false;
            }

            ViewContributorSummary_noOfSubjectsContributedHyperLink.Text =
                contributorSummary.SubjectsContributed.ToString();

            ViewContributorSummary_questionUsageValueLabel.Text =
                contributorSummary.QuestionUsageCount.ToString();

            ViewContributorSummary_noOfCategoriesContibutedHyperLink.NavigateUrl
                = "javascript:OpenCategorySummary('" + userID + "');";

            ViewContributorSummary_noOfSubjectsContributedHyperLink.NavigateUrl
                = "javascript:OpenSubjectSummary('" + userID + "');";
        }

        /// <summary>
        /// Method used to get the question details from the database and 
        /// display to the user
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user id 
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the order by string 
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/>that holds the sorting order direction
        /// </param>
        private void SetQuestionDetails(int pageNumber)
        {
            int userID = int.Parse(ViewContributorSummary_authorIDHiddenField.Value);

            string orderBy = ViewState["SORT_FIELD"].ToString();
            SortType orderByDirection = ((SortType)ViewState["SORT_ORDER"]);

            int totalRecords = 0;

            //Get the question details from the database
            List<QuestionDetail> questionDetails = new QuestionBLManager().
                GetContributorSummaryQuestionDetails(userID,
             pageNumber, GridPageSize, orderBy, orderByDirection, out totalRecords);

            //Assign the datasource
            ViewContributorSummary_questionDetailsGridView.DataSource = questionDetails;

            ViewContributorSummary_questionDetailsGridView.DataBind();

            //Assign the total records for the paging control 
            ViewContributorSummary_questionDetailBottomPageNavigator.TotalRecords = totalRecords;

            //Assign the grid page size
            ViewContributorSummary_questionDetailBottomPageNavigator.PageSize = GridPageSize;
        }

        /// <summary>
        /// Method that will call when the page gets loaded. This will perform
        /// to call the event handlers based parameter.
        /// </summary>
        /// <param name="stringValue">
        /// A <see cref="string"/> that contains the string.
        /// </param>
        private void ValidateEnterKey(string stringValue)
        {
            switch (stringValue.Trim())
            {
                case "NAME":
                    this.ViewContributorSummary_loadDetailsButton_Click(ViewContributorSummary_loadDetailsButton, null);
                    break;
                case "SName":
                    if (IsValidUserName())
                        this.ViewContributorSummary_loadDetailsButton_Click(ViewContributorSummary_loadDetailsButton, null);
                    else
                        ShowMessage(ViewContributorSummary_bottomErrorMessageLabel, ViewContributorSummary_topErrorMessageLabel,
                                       string.Format("'{0}' and '{1}' are mismatched", ViewContributorSummary_questionAuthorIDTextBox.Text,
                                       ViewContributorSummary_authorIDHiddenField.Value));
                    break;
            }
        }

        /// <summary>
        /// Method that checks whether the user firstname,lastname and userid are
        /// valid.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that represents whether the user is valid or not.
        /// </returns>
        private bool IsValidUserName()
        {
            return new CommonBLManager().IsValidUserNameWithId(
                ViewContributorSummary_questionAuthorIDTextBox.Text.Trim(),
                Convert.ToInt32(ViewContributorSummary_authorIDHiddenField.Value));
        }

        /// <summary>
        /// Represents the method that is used to get the correct amount
        /// </summary>
        /// <param name="date">
        /// A<see cref="DateTime"/>that holds the date
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the correct formatted date
        /// </returns>
        protected string GetCorrectAmount(Decimal amount)
        {
            return amount == 0.00M ? "0" : amount.ToString();
        }

        /// <summary>
        /// Clears all message labels
        /// </summary>
        private void ClearMessageLabels()
        {
            //Clear the contents in the top, botton, success and error message labels
            ViewContributorSummary_topSuccessMessageLabel.Text = string.Empty;
            ViewContributorSummary_bottomSuccessMessageLabel.Text = string.Empty;
            ViewContributorSummary_topErrorMessageLabel.Text = string.Empty;
            ViewContributorSummary_bottomErrorMessageLabel.Text = string.Empty;
            ViewContributorSummary_creditsEarned_noDataLabel.Text = string.Empty;
            ViewContributorSummary_questionUsageSummary_noDataLabel.Text = string.Empty;
            ViewContributorSummary_totalQuestionAuthored_noDataLabel.Text = string.Empty;
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandOrRestore()
        {
            //If the is miaximized hidden field is y. Change the height to the expanded height
            if (!Utility.IsNullOrEmpty(ViewContributorSummary_isMaximizedHiddenField.Value) &&
                 ViewContributorSummary_isMaximizedHiddenField.Value == "Y")
            {
                ViewContributorSummary_questionSummaryDIV.Style["display"] = "none";
                ViewContributorSummary_questionDetailsUparrowSpan.Style["display"] = "block";
                ViewContributorSummary_questionDetailsDownarrowSpan.Style["display"] = "none";
                ViewContributorSummary_questionDetailsDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                ViewContributorSummary_questionSummaryDIV.Style["display"] = "block";
                ViewContributorSummary_questionDetailsUparrowSpan.Style["display"] = "none";
                ViewContributorSummary_questionDetailsDownarrowSpan.Style["display"] = "block";
                ViewContributorSummary_questionDetailsDIV.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Load all javascript tags
        /// </summary>
        private void SubscribtClientSideHandlers()
        {
            ViewContributorSummary_questionAuthorIDTextBox.Attributes.Add("onKeyPress", "return focusME(event,'"
                    + ViewContributorSummary_questionAuthorIDTextBox.ClientID + "','NAME','"
                    + ViewContributorSummary_browserHiddenField.ClientID + "');");
            //Assign the on click attribute for the search image
            ViewContributorSummary_searchImage.Attributes.Add("onclick",
                 "javascript:return LoadAdminName('"
                + ViewContributorSummary_questionAuthorIDTextBox.ClientID + "','"
                + ViewContributorSummary_authorIDHiddenField.ClientID + "','"
                + ViewContributorSummary_questionAuthorIDTextBox.ClientID + "','QA')");
            //Assign the on click attributes for the expand or restore button 
            ViewContributorSummary_questionDetailsTR.Attributes.Add("onclick",
              "ExpandOrRestore('" +
              ViewContributorSummary_questionDetailsDIV.ClientID + "','" +
              ViewContributorSummary_questionSummaryDIV.ClientID + "','" +
              ViewContributorSummary_questionDetailsUparrowSpan.ClientID + "','" +
              ViewContributorSummary_questionDetailsDownarrowSpan.ClientID + "','" +
              ViewContributorSummary_isMaximizedHiddenField.ClientID + "','" +
              RESTORED_HEIGHT + "','" +
              EXPANDED_HEIGHT + "')");
            //Adding client side onchange attribute the the authorid textbox.
            ViewContributorSummary_questionAuthorIDTextBox.Attributes.Add("onchange", "return TrimUserId('"
                + ViewContributorSummary_questionAuthorIDTextBox.ClientID + "','" +
                ViewContributorSummary_authorIDHiddenField.ClientID + "','" +
                ViewContributorSummary_browserHiddenField.ClientID + "');");
        }

        #endregion Private Methods

        #region Overridden Methods                                              

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
            //Set the author details
            SetAuthorDetails();

            //Set question summary details
            SetQuestionSummaryDetails();

            //Set the question details in grid 
            SetQuestionDetails(1);

            //Get the chart values and assign 
            CreateQuestionAuthorChart();
        }

        #endregion Overridden Methods
    }
}
