﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI
{
    public partial class ResumeRepositoryHome
    {
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.ResumeRepositoryMaster Master
        {
            get
            {
                return ((Forte.HCM.UI.MasterPages.ResumeRepositoryMaster)(base.Master));
            }
        }
       
    }
}
