﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WorkflowMaster.Master"
    AutoEventWireup="true" CodeBehind="InterviewWorkflow.aspx.cs" Inherits="Forte.HCM.UI.InterviewWorkflow" %>

<%@ Register Src="CommonControls/WorkFlowSideLinkButtonControl.ascx" TagName="WorkflowSideLinkButtonControl"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/WorkflowMaster.Master" %>
<asp:Content ID="InterviewWorkflow_content" ContentPlaceHolderID="WorkflowMaster_body"
    runat="server">
    <br />
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:Label ID="InterviewWorkflow_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="InterviewWorkflow_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="InterviewWorkflow_mainUpdatePanel" runat="server">
                    <ContentTemplate>
                        <div id="InterviewWorkflow_mainDIV" runat="server">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td height="77">
                                                    <img src="App_Themes/DefaultTheme/Images/wf_top_left_strip.jpg" alt="" width="9"
                                                        height="77" />
                                                </td>
                                                <td valign="middle" class="wf_top_center_strip">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="94%" align="left" class="wf_sub_title">
                                                                <asp:Label ID="InterviewWorkflow_headerLabel" runat="server" Text="Interview"></asp:Label>
                                                            </td>
                                                            <td width="6%" align="left" class="wf_back_icon">
                                                                <asp:LinkButton ID="InterviewWorkflow_backLinkButton" runat="server" Text="Back"
                                                                    CssClass="wf_back" PostBackUrl="~/Workflow.aspx"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td >
                                                    <img src="App_Themes/DefaultTheme/Images/wf_top_right_strip.jpg" alt="" width="9"
                                                        height="77" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="wf_body">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 30%" valign="top">
                                                    <uc1:WorkflowSideLinkButtonControl ID="InterviewWorkflow_workflowSideLinkButtonControl"
                                                        runat="server" />
                                                </td>
                                                <td width="70%" valign="middle" align="center">
                                                    <img src="App_Themes/DefaultTheme/Images/interview.jpg" alt="" width="767" height="609"
                                                        border="0" usemap="#Map" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <map name="Map" id="Map">
        <area shape="rect" coords="81,54,221,104" href="InterviewQuestions/InterviewSingleQuestionEntry.aspx?m=0&s=1&parentpage=WF_INTERVIEW" title="Single Question Entry" alt=""/>
        <area shape="rect" coords="79,106,220,151" href="InterviewQuestions/InterviewBatchQuestionEntry.aspx?m=0&s=0&parentpage=WF_INTERVIEW" title="Batch Upload" alt=""/>
        <area shape="rect" coords="435,67,512,146" href="InterviewTestMaker/CreateManualInterviewTest.aspx?m=1&s=1&parentpage=WF_INTERVIEW" title="Manual Composition" alt=""/>
        <area shape="rect" coords="545,61,614,147" href="InterviewTestMaker/CreateAutomaticInterviewTest.aspx?m=1&s=0&parentpage=WF_INTERVIEW" title="Automated Test" alt=""/>
        <area shape="rect" coords="466,177,579,258" href="InterviewTestMaker/CreateInterviewTestWithAdaptive.aspx?m=1&s=1&parentpage=WF_INTERVIEW" title="Adaptive Recommendations" alt=""/>
        <area shape="rect" coords="405,446,508,564" href="InterviewScheduler/InterviewTestScheduler.aspx?m=2&s=0&parentpage=WF_INTERVIEW" title="Test Scheduler" alt=""/>
        <area shape="rect" coords="559,441,680,563" href="InterviewTestMaker/InterviewTestSession.aspx?m=1&s=3&parentpage=WF_INTERVIEW" title="Test Session" alt=""/>
    </map>
</asp:Content>
