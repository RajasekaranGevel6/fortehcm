﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Forte.HCM.UI
{
    public partial class SiteAdminHome : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.SetPageCaption(Resources.HCMResource.SiteAdminHome_PageCaption);
        }
    }
}