﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Workflow.aspx.cs
// File that represents the user interface layout and functionalities 
// for the workflow page. This page helps in viewing and navigating through
// the workflow. This class inherits Forte.HCM.UI.Common.PageBase class.

#endregion Header 

#region Directives                                                             

using System;

using Forte.HCM.Trace;
using Forte.HCM.UI.Common;

#endregion Directives

namespace Forte.HCM.UI
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities for
    /// the workflow page. This page helps in viewing and navigatinga through
    /// the workflow. This class inherits Forte.HCM.UI.Common.PageBase class
    /// </summary>
    public partial class Workflow : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Forte HCM - Workflow");
            }
            catch (Exception excpetion)
            {
                base.ShowMessage(
                     Workflow_topErrorMessageLabel, excpetion.Message);

                Logger.ExceptionLog(excpetion);
            }
        }

        #endregion Event Handlers

        #region Public Methods                                                 

        /// <summary>
        /// Navigates to specific page. This method is invoked from the user control.
        /// </summary>
        /// <param name="pageName">
        /// A <see cref="string"/> that holds the page name.
        /// </param>
        public void MoveToPage(string pageName)
        {
            switch (pageName)
            {
                case "Client Management":
                    Response.Redirect("~/Landing.aspx?index=0&parentpage=WF_MAJOR", false);
                    break;

                case "Position Profile":
                    Response.Redirect("~/Landing.aspx?index=1&parentpage=WF_MAJOR", false);
                    break;

                case "Assessment":
                    Response.Redirect("~/Landing.aspx?index=3&parentpage=WF_MAJOR", false);
                    break;

                case "Interview":
                    Response.Redirect("~/Landing.aspx?index=4&parentpage=WF_MAJOR", false);
                    break;
                case "OnlineInterview":
                    Response.Redirect("~/Landing.aspx?index=5&parentpage=WF_MAJOR", false);
                    break;
                case "Reports":
                    Response.Redirect("~/Landing.aspx?index=6&parentpage=WF_MAJOR", false);
                    break;
            }
        }

        #endregion Public Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Overridden Methods
    }
}