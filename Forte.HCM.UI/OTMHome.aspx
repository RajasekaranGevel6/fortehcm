﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OTMMaster.Master"
    CodeBehind="OTMHome.aspx.cs" Inherits="Forte.HCM.UI.OTMHome" %>

<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="OTMHome_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">
    <table width="972px" border="0" cellspacing="0" cellpadding="0" class="table_bg"
        align="center">
        <tr>
            <td align="center" class="title_home_icon">
                <h2>
                    Online Testing Module Home
                </h2>
            </td>
        </tr>
        <tr>
            <td align="center">
                <img src="App_Themes/DefaultTheme/Images/OTM_Tool.png" alt="Online Testing Module"
                    height="226px" width="234px" />
            </td>
        </tr>
    </table>
</asp:Content>
