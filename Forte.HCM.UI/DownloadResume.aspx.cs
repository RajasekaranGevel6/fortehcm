﻿
#region Namespace                                                              

using System;
using System.Web.UI;

using Forte.HCM.BL;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Namespace

namespace Forte.HCM.UI
{
    public partial class DownloadResume : Page
    {

        #region Events                                                         

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack)
                    return;
                if (!Request.QueryString.HasKeys() || string.IsNullOrEmpty(Request.QueryString["resumeid"]))
                    return;
                try
                {
                    ResumeDownload(Convert.ToInt32(Request.QueryString["resumeid"]));
                }
                catch (InvalidCastException)
                {
                }
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp.Message);
            }
        }

        #endregion Events

        #region Private Methods                                                

        private void ResumeDownload(int candidateID)
        {
            CandidateResumeDetails candidateResumeDetails = null;
            try
            {
                candidateResumeDetails = new ResumeRepositoryBLManager().GetResumeSource(candidateID);
                if (candidateResumeDetails == null)
                    return;
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + candidateResumeDetails.FileName);
                Response.ContentType = GetContentType(candidateResumeDetails.MimeType.ToLower());
                Response.BinaryWrite(candidateResumeDetails.ResumeSource);
                Response.End();
            }
            finally
            {
                if (candidateResumeDetails != null) candidateResumeDetails = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        private string GetContentType(string FileMimeType)
        {
            switch (FileMimeType)
            {
                case "doc":
                    return "application/msword";
                case "xls":
                    return "application/vnd.ms-excel";
                case "pdf":
                    return "application/pdf";
                case "docx":
                    return "application/vnd.openxmlformats";
                case "txt":
                    return "text/plain";
                default:
                    return "application/octet-stream";
            }
        }

        private void ShowErrorMessage(string Message)
        {
            if (string.IsNullOrEmpty(DownloadResume_topErrorMessage.Text))
                DownloadResume_topErrorMessage.Text = Message;
            else
                DownloadResume_topErrorMessage.Text += "<br />" + Message;
        }

        #endregion Private Methods

    }
}