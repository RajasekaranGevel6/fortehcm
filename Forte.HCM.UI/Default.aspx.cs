﻿#region Namespace

using System;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Threading;
using System.Web.Security;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

using Resources;

#endregion

namespace Forte.HCM.UI
{
    public partial class Default : System.Web.UI.Page
    {
        #region Variables

        public string homeUrl = string.Empty;
        public string landingUrl = string.Empty;
        public string userOptionsUrl = string.Empty;
        public string helpUrl = string.Empty;

        #endregion Variables

        #region Declaration

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="UserDetail">
        /// A <see cref="userDetail"/> that holds the user object details.
        /// </param>
        private delegate void AsyncTaskDelegate(UserDetail userDetail);

        #endregion Declaration

        #region Events Handlers
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {  
                //HeaderControl_logoutButton.UseSubmitBehavior = false;
                string[] uri = Request.Url.AbsoluteUri.Split('/');
                if (uri.Length > 3)
                {
                    // Compose home url.
                    homeUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Default.aspx";

                    // Compose dashboard url.
                    landingUrl = uri[0] + "/" + uri[1] + "/" + uri[2] +  "/Landing.aspx";

                    // Compose user options url.
                    userOptionsUrl = uri[0] + "/" + uri[1] + "/" + uri[2] +  "/Settings/UserOptions.aspx";

                    // Compose help url.
                    helpUrl = uri[0] + "/" + uri[1] + "/" + uri[2] +  "/Help.aspx";
                }

                //Page.Form.DefaultButton = HomePage_goImageButton.UniqueID;
                //Page.Form.DefaultFocus = HomePage_userNametextbox.UniqueID;
                string PageName = Request.Url.AbsoluteUri.Split('/')[Request.Url.AbsoluteUri.Split('/').Length - 1];
                if (PageName.Contains("?"))
                    PageName = PageName.Substring(0, PageName.IndexOf('?'));
                
                /*if (PageName == "Home.aspx")
                    SubscriptionMaster_menu.Visible = true;
                else
                    SubscriptionMaster_menu.Visible = false;*/

                // TODO - This needs to reviewed.
               /* if (!Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                {
                    //HeaderControl_changePasswordLinkButton.Visible = true;
                }
                else
                {
                    // Hide the options menu.
                     if (SubscriptionMaster_menu.Items.Count > 5)
                        SubscriptionMaster_menu.Items.RemoveAt(5);
                    HeaderControl_changePasswordLinkButton.Visible = false; 
                }*/
               
                // Check if login is requested from web site.
                if (!Utility.IsNullOrEmpty(Request.QueryString["sitesessionkey"]))
                    AuthenticateSessionKey();

                // Retrieve cookies.
                RetrieveCookies();

                if (IsPostBack)
                    return;
                if (!Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                {
                    //HeaderControl_loggedInTable.Visible = true;
                    //HeaderControl_loginTable.Visible = false;
                    //Index_loginButton.Visible = false;
                    //HeaderControl_logoLoginTD.Visible = false;
                    if (Page.Request.IsAuthenticated)
                    {
                        Response.Redirect("~/Landing.aspx", false);
                    }
                    //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //HeaderControl_loginUserNameLabel.Text = ((UserDetail)Session["USER_DETAIL"]).FirstName;
                    //HeaderControl_lastLoginDateTimeLabel.Text = ((UserDetail)Session["USER_DETAIL"]).LastLogin.GetDateTimeFormats()[16];
                }
                else
                {
                    //HeaderControl_loggedInTable.Visible = false;
                    //HeaderControl_loginTable.Visible = true;
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    // Check if page is accessed from mail.
                    if (Request.QueryString["parentpage"] != null &&
                        Request.QueryString["parentpage"] == "mail")
                    {
                        Index_loginButton.Visible = false;
                    }
                    else
                        Index_loginButton.Visible = true;

                    //HeaderControl_logoLoginTD.Visible = true;
                    //6FormsAuthentication.RedirectToLoginPage();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void Index_loginButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Try login.
                TryLogin();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the send button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will valid the user name and send to the password to the 
        /// email.
        /// </remarks>
        protected void ForgotPassword_sendPasswordButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Get user detail along with password.
                UserDetail userDetail = new UserDetail();
                userDetail = new UserRegistrationBLManager().GetPassword
                    (ForgotPassword_userNameTextBox.Text.Trim());

                if (userDetail == null)
                {
                    ShowErrorMessage("No such user name exist !");
                    ForgotPassword_userNameTextBox.Focus();
                    return;
                }

                // Decrypt the password.
                userDetail.Password = new EncryptAndDecrypt().DecryptString(userDetail.Password);

                // Send the password through mail.
                try
                {
                    AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendPasswordToUser);
                    IAsyncResult result = taskDelegate.BeginInvoke(userDetail,
                        new AsyncCallback(SendPasswordToUserCallBack), taskDelegate);
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }

                // Show a message to the user.
                ShowErrorMessage("Your password has been sent to your email");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowErrorMessage(exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods

        /// <summary>
        /// Method that authenticates the user based on the session key.
        /// </summary>
        private void AuthenticateSessionKey()
        {
            // Check if not post back.
            if (IsPostBack)
                return;

            UserDetail userDetail = null;
            try
            {
                // Get the user detail based on session key.
                userDetail = new AuthenticationBLManager().GetAuthenticateUser
                    (Request.QueryString["sitesessionkey"]);
            }
            catch (Exception exp)
            {
                // An exception will be raised when the guid is in invalid format.
                Logger.ExceptionLog(exp);

                ShowErrorMessage(HCMResource.Login_InvalidUserLogin);
                return;
            }

            // Check if user is valid or not.
            if (userDetail == null || userDetail.UserID == 0)
            {
                ShowErrorMessage(HCMResource.Login_InvalidUserLogin);
                return;
            }

            // Check if the user is a candidate.
            if (userDetail.Roles != null && userDetail.Roles.Exists(item => item == UserRole.Candidate))
            {
                // Do not allow non-candidate users to access the application.
                ShowErrorMessage(HCMResource.Login_InvalidUserLogin);

                return;
            }

            // Keep the user detail in session.
            Session["USER_DETAIL"] = userDetail;

            if (Request.Params["ReturnUrl"] != null)
            {
                FormsAuthentication.RedirectFromLoginPage(Index_userNameTextBox.Text.Trim(), false);
            }
            else
            {

                FormsAuthentication.SetAuthCookie(Index_userNameTextBox.Text.Trim(), false);
                Response.Redirect("~/Landing.aspx", false);
            }
        }

        /// <summary>
        /// Method that retreives the user name and password form cookie and 
        /// tries to login using that.
        /// </summary>
        private void RetrieveCookies()
        {
            try
            {
                // Check if not post back and user already logged in.
                if (IsPostBack || !Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                    return;

                // Try to get cookie information.
                HttpCookie cookie = Request.Cookies["FORTEHCM_USER_DETAILS"];

                if (cookie != null)
                {
                    string userName = cookie["FHCM_USER_NAME"];
                    string password = cookie["FHCM_PASSWORD"];

                    if (!Utility.IsNullOrEmpty(userName) && !Utility.IsNullOrEmpty(password))
                    {
                        // Assign user name and password.
                        Index_userNameTextBox.Text = userName;
                        Index_passwordTextBox.Text = password;

                        // Try to login.
                        TryLogin();
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that try login to the system using the given user name and 
        /// password.
        /// </summary>
        private void TryLogin()
        {
            UserDetail userDetails = null;
            if (this.IsValidData())
            {
                userDetails = AuthenticateUserDetails(Index_userNameTextBox.Text.Trim(),
                    Index_passwordTextBox.Text.Trim());

                //if (Utility.IsNullOrEmpty(userDetails.Roles))
                if (Utility.IsNullOrEmpty(userDetails) || userDetails.UserID == 0)
                {
                    ShowErrorMessage(HCMResource.Login_InvalidUserLogin);
                    return;
                }

                //Do not allow candidate to login through ForteHCM application
                if (userDetails.IsCandidate)
                {
                    ShowErrorMessage(HCMResource.Login_InvalidUserLogin);
                    return;
                }

                if (!Utility.IsNullOrEmpty(Request.QueryString["c"]))
                {
                    //Checking candidate have online interview for current date & time
                    OnlineCandidateSessionDetail onlineInterviewDetail =
                        new CandidateBLManager().GetOnlineInterviewDetail(0, userDetails.UserID, Request.QueryString["c"].ToString());

                    if (onlineInterviewDetail != null)
                    {
                        if (new Utility().GetOnlineInterviewAvailable(onlineInterviewDetail.InterviewDate,
                            onlineInterviewDetail.TimeSlotFrom, onlineInterviewDetail.TimeSlotTo))
                        { 
                            /*Response.Redirect(ConfigurationManager.AppSettings["ONLINE_INTERVIEW_URL"].ToString() + "?candidateid=" + new EncryptAndDecrypt().EncryptString(onlineInterviewDetail.EmailId.Trim())
                                + "&userrole=A" + "&userid=" + new EncryptAndDecrypt().EncryptString(userDetails.UserName.Trim()) + "&interviewkey=" + onlineInterviewDetail.CandidateSessionID
                                + "&chatroom=" + onlineInterviewDetail.ChatRoomName, false);*/
                            Response.Redirect(ConfigurationManager.AppSettings["ONLINE_INTERVIEW_URL"].ToString() + "?candidateid=" +   onlineInterviewDetail.CandidateID.ToString()
                                + "&userrole=A" + "&userid=" +  userDetails.UserID.ToString().Trim() + "&interviewkey=" + onlineInterviewDetail.CandidateSessionID
                                + "&chatroom=" + onlineInterviewDetail.ChatRoomName, false);
                            return;
                        }
                    }
                }

                Session["USER_DETAIL"] = userDetails;

                // Try to add the cookie information.
                if (HomePage_rememberMeCheckBox.Checked)
                {
                    try
                    {
                        HttpCookie cookie = Request.Cookies["FORTEHCM_USER_DETAILS"];
                        if (cookie == null)
                        {
                            cookie = new HttpCookie("FORTEHCM_USER_DETAILS");
                        }

                        cookie["FHCM_USER_NAME"] = Index_userNameTextBox.Text;
                        cookie["FHCM_PASSWORD"] = Index_passwordTextBox.Text;

                        cookie.Expires = DateTime.Now.AddMonths
                            (Convert.ToInt32(ConfigurationManager.AppSettings["REMEMBER_ME_MONTHS"]));
                        Response.AppendCookie(cookie);
                    }
                    catch (Exception exp)
                    {
                        Logger.ExceptionLog(exp);
                    }
                }

                if (Request.Params["ReturnUrl"] != null)
                {
                    FormsAuthentication.RedirectFromLoginPage(Index_userNameTextBox.Text.Trim(), false);
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(Index_userNameTextBox.Text.Trim(), false);
                    Response.Redirect("~/Landing.aspx", false);
                }
            }
        }

        /// <summary>
        /// A Method that shows the error message to the user
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the message to be 
        /// show to the user.
        /// </param>
        private void ShowErrorMessage(string Message)
        {
            Index_loginErrorLabel.Text = string.Empty;
            Index_loginErrorLabel.Text = Message;
            //if (string.IsNullOrEmpty(Index_loginErrorLabel.Text))
            //    Index_loginErrorLabel.Text = Message;
            //else
            //    Index_loginErrorLabel.Text += "<br />" + Message;
        }

        /// <summary>
        /// Shows the error message to the user
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception object.
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            Logger.ExceptionLog(exp);
            ShowErrorMessage(exp.Message);
        }

        /// <summary>
        /// Method that will return a userdetail object for the 
        /// given username and password.
        /// </summary>
        /// <param name="UserName">
        /// A <see cref="string"/> that contains the username.
        /// </param>
        /// <param name="Password">
        /// A <see cref="string"/> that contains the password.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> object that contains the 
        /// information for the given username and password.
        /// </returns>
        private UserDetail AuthenticateUserDetails(string UserName, string Password)
        {
            AuthenticationBLManager authenticationBLManager = new AuthenticationBLManager();
            Authenticate authenticate = new Authenticate();
            authenticate.UserName = UserName;
            authenticate.Password = new EncryptAndDecrypt().EncryptString(Password.Trim());
            UserDetail userDetails = new UserDetail();
            userDetails = authenticationBLManager.GetAuthenticateUser(authenticate);
            return userDetails;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool IsValidData()
        {
            bool isValidData = true;
            if (Index_userNameTextBox.Text.Trim() == "")
            {
                ShowErrorMessage(HCMResource.Login_UserNameTextBoxEmpty);
                isValidData = false;
            }
            if (Index_passwordTextBox.Text.Trim() == "")
            {
                ShowErrorMessage(HCMResource.Login_PasswordTextBoxEmpty);
                isValidData = false;
            }
            return isValidData;
        }

        public void ShowChangePasswordModelPopup()
        {
            //HomePage_changePassword_ModalpPopupExtender.Show();
        }

        /// <summary>
        /// Method that sends the password to requested user
        /// </summary>
        /// <param name="UserDetail">
        /// A <see cref="userDetail"/> that holds the user deail object
        /// detail.
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendPasswordToUser(UserDetail userDetail)
        {
            try
            {
                new EmailHandler().SendMail(EntityType.ForgotPassword, userDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Private Methods

        #region Asynchronous Method Handlers

        /// <summary>
        /// Handler method that acts as the callback method for send mail.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendPasswordToUserCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Asynchronous Method Handlers

        #region Public Methods

        public List<string> BreadCrumbText
        {
            set
            {
                List<string> breadCrumbList = (List<string>)value;

                if (breadCrumbList != null)
                {
                    foreach (string breadCrumbText in breadCrumbList)
                    {
                    }
                }
            }
        }

        /// <summary>
        /// Method that sets hides the sign up button.
        /// </summary>
        public void HideSignUpButton()
        {
            Index_loginButton.Visible = false;
        }

        #endregion Public Methods
    }
}