﻿using System;
using System.Web;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using System.Web.Optimization;


namespace Forte.HCM.UI
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
           
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            try
            {
                // Keep page size in session.
                Application[Constants.SessionConstants.GRID_PAGE_SIZE] = new
                    AdminBLManager().GetAdminSettings().GridPageSize;

                // Keep the tenant ID in session.
                //Session[Constants.SessionConstants.TENANT_ID] = ConfigurationManager.
                //    AppSettings["TENANT_ID"];
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                Session["UNHANDLED_ERROR"] = exp.Message;
                
                Response.Redirect("~/Common/UnhandledError.aspx", true);
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            Uri ReqUrl = HttpContext.Current.Request.Url;
            string[] requestedSegment = ReqUrl.Segments;
            string requestedPage = requestedSegment[requestedSegment.Length - 1].ToLower();

            if (requestedPage == "testdownload.aspx" ||
                requestedPage == "testresultprint.aspx" ||
                requestedPage == "imagehandler.ashx" ||
                requestedPage == "candidatetestdetails.aspx" ||
                requestedPage == "subscriptiontype.aspx" ||
                requestedPage == "signup.aspx" ||
                requestedPage == "aboutus.aspx" || 
                requestedPage == "usersignup.aspx" ||
                requestedPage == "featureslist.aspx" ||
                requestedPage == "licenseagreement.aspx" ||
                requestedPage == "captchaviewer.aspx" ||
                requestedPage == "captchaimagehandler.ashx" ||
                requestedPage == "candidateimagehandler.ashx" ||
                requestedPage == "subscriptionstatus.aspx" ||
                requestedPage == "activatesubscription.aspx" ||
                requestedPage == "comingsoon.aspx" ||
                requestedPage == "accessdenied.aspx" ||
                requestedPage == "featuredenied.aspx" ||
                requestedPage == "candidateaccessdenied.aspx" ||
                requestedPage == "unhandlederror.aspx" ||
                requestedPage == "candidateunhandlederror.aspx" ||
                requestedPage == "pageunderconstruction.aspx" ||
                requestedPage == "candidatepageunderconstruction.aspx" ||
                requestedPage == "feedback.aspx" ||
                requestedPage == "default.aspx" ||
                requestedPage == "workflow.aspx" ||
                requestedPage == "landiong.aspx" ||
                requestedPage == "assessmentworkflow.aspx" ||
                requestedPage == "interviewworkflow.aspx" ||
                requestedPage == "userdashboard.aspx" ||
                requestedPage == "help.aspx" ||
                requestedPage == "quicktour.aspx" ||
                requestedPage == "contactsales.aspx" ||
                requestedPage == "otmoverview.aspx" ||
                requestedPage == "positionprofileoverview.aspx" ||
                requestedPage == "resumerepositoryoverview.aspx" ||
                requestedPage == "talentscoutoverview.aspx" ||
                requestedPage == "fhcmsignupservice.asmx" ||
                requestedPage == "publishassessorrating.aspx" ||
                requestedPage == "publishcandidateinterviewresponse.aspx" ||
                requestedPage == "externalassessor.aspx" ||
                requestedPage == "externalassessorrating.aspx" ||
                requestedPage == "externalinterviewresponse.aspx")
            {
                HttpContext.Current.SkipAuthorization = true;
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception currentException = Server.GetLastError().GetBaseException();
            Exception ex = Server.GetLastError();
            Logger.ExceptionLog(currentException);

            if (currentException.Message.ToUpper().Contains(".ASPX' DOES NOT EXIST."))
            {
                if (currentException.Message.ToUpper().Contains("/POPUP/"))
                    Response.Redirect("~/Popup/PopupUnderConstruction.aspx", true);
                else
                    Response.Redirect("~/Common/PageUnderConstruction.aspx", true);
            }
            else if (currentException.StackTrace != null &&
                currentException.StackTrace.ToUpper().Contains("FORTE.HCM.UI.POPUP"))
            {
                Session["UNHANDLED_ERROR"] = currentException.Message;
                Response.Redirect("~/Popup/PopupUnhandledError.aspx", true);
            }
            else
            {

                Session["UNHANDLED_ERROR"] = currentException.Message;
                
                Response.Redirect("~/Common/UnhandledError.aspx", true);
            }
        }
      
        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}