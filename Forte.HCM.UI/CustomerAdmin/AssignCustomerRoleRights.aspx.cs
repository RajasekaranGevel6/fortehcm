﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AssignCustomerRoleRights.aspx.cs
// File that represents the UserRoleMatrix class that defines the user 
// interface layout and functionalities for the FeatureUsage page. This 
// page helps in assigning roles to the user. This class inherits 
// Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives                                                             
using System;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Data;
#endregion Directives

namespace Forte.HCM.UI.CustomerAdmin
{
    public partial class AssignCustomerRoleRights : PageBase
    {

        #region Private Members                                                

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "300px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "325px";

        #endregion Private Members

        #region Event Handlers                                                 
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance
        /// containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                AssignCustomerRoleRights_topSuccessMessageLabel.Text = string.Empty;
                AssignCustomerRoleRights_topErrorMessageLabel.Text = string.Empty;
                AssignCustomerRoleRights_bottomErrorMessageLabel.Text = string.Empty;
                AssignCustomerRoleRights_bottomSuccessMessageLabel.Text = string.Empty;

                Master.SetPageCaption("Assign Role Rights");
                Page.Form.DefaultButton = AssignCustomerRoleRights_showDetailsButton.UniqueID;
                //Page.SetFocus(AssignCustomerRoleRights_moduleDropDownList.ClientID);

                if (!IsPostBack)
                {
                    AssignCustomerRoleRights_roleDropDownList.DataSource =
                        new AuthenticationBLManager().GetAssignedCorporateRole(base.tenantID);
                    AssignCustomerRoleRights_roleDropDownList.DataBind();
                    AssignCustomerRoleRights_roleDropDownList.Items.Insert(0,
                  new ListItem("--Select--", "0"));
                }

                AssignCustomerRoleRights_assignRightsUpSpan.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                   AssignCustomerRoleRights_matrixRightDIV.ClientID + "','" +
                   AssignCustomerRoleRights_searchHeaderdiv.ClientID + "','" +
                   AssignCustomerRoleRights_assignRightsUpSpan.ClientID + "','" +
                   AssignCustomerRoleRights_assignRightsDownSpan.ClientID + "','" +
                   AssignCustomerRoleRights_restoreHiddenField.ClientID + "','" +
                   RESTORED_HEIGHT + "','" +
                   EXPANDED_HEIGHT + "')");

                AssignCustomerRoleRights_assignRightsDownSpan.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    AssignCustomerRoleRights_matrixRightDIV.ClientID + "','" +
                    AssignCustomerRoleRights_searchHeaderdiv.ClientID + "','" +
                    AssignCustomerRoleRights_assignRightsUpSpan.ClientID + "','" +
                    AssignCustomerRoleRights_assignRightsDownSpan.ClientID + "','" +
                    AssignCustomerRoleRights_restoreHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exception)
            {
                ShowMessage(AssignCustomerRoleRights_topErrorMessageLabel,
                    AssignCustomerRoleRights_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }

        }


        /// <summary>
        /// Handles the Click event of the AssignCustomerRoleRights_saveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void AssignCustomerRoleRights_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (AssignCustomerRoleRights_matrixGridView.Rows.Count == 0)
                {
                    ShowMessage(AssignCustomerRoleRights_bottomErrorMessageLabel,
                        AssignCustomerRoleRights_topErrorMessageLabel,
                        "Please select role");
                    return;
                }

                SaveFeatureRights();

                AssignCustomerRoleRights_matrixGridView.DataSource = new AuthenticationBLManager().GetCorporateFeatureRightsDetails
               (int.Parse(AssignCustomerRoleRights_roleDropDownList.SelectedValue),base.tenantID);

                //assign the column name as the role drop down selected text 
                AssignCustomerRoleRights_matrixGridView.Columns[3].HeaderText =
                    AssignCustomerRoleRights_roleDropDownList.SelectedItem.Text;
                AssignCustomerRoleRights_matrixGridView.DataBind();
            }
            catch (Exception exception)
            {
                ShowMessage(AssignCustomerRoleRights_topErrorMessageLabel,
                    AssignCustomerRoleRights_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }


        }

        /// <summary>
        /// Handles the Click event of the AssignCustomerRoleRights_showDetailsButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void AssignCustomerRoleRights_showDetailsButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadValues();
            }
            catch (Exception exception)
            {
                ShowMessage(AssignCustomerRoleRights_topErrorMessageLabel,
                    AssignCustomerRoleRights_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }

        }

        /// <summary>
        /// Handles the Click event of the AssignCustomerRoleRights_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void AssignCustomerRoleRights_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);

            }
            catch (Exception exception)
            {
                ShowMessage(AssignCustomerRoleRights_topErrorMessageLabel,
                    AssignCustomerRoleRights_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }
        }
        #endregion Event Handlers

        #region Private Method                                                 

        /// <summary>
        /// Saves the feature rights.
        /// </summary>
        private void SaveFeatureRights()
        {
            List<RoleRightMatrixDetail> fearueRightMatrix = new List<RoleRightMatrixDetail>();

            HiddenField modIDHiddenField = null;

            HiddenField submodIDHiddenField = null;

            HiddenField ischeckedidHiddenfied = null;
            HiddenField feaIDHiddenField = null;

            HiddenField roleRightIDHiddenField = null;

            CheckBox isApplicableCheckBox = null;

            RoleRightMatrixDetail roleRightDeatail = null;

            foreach (GridViewRow row in AssignCustomerRoleRights_matrixGridView.Rows)
            {

                modIDHiddenField = (HiddenField)row.FindControl(
                    "AssignCustomerRoleRights_matrixGridView_modIDHiddenField");

                submodIDHiddenField = (HiddenField)row.FindControl(
                   "AssignCustomerRoleRights_matrixGridView_submodIDHiddenField");

                feaIDHiddenField = (HiddenField)row.FindControl(
                   "AssignCustomerRoleRights_matrixGridView_featureIDHiddenField");

                roleRightIDHiddenField = (HiddenField)row.FindControl(
                   "AssignCustomerRoleRights_matrixGridView_roleRightsIDHiddenField");

                isApplicableCheckBox = (CheckBox)row.FindControl(
                    "AssignCustomerRoleRights_matrixGridView_checkedCheckBox");


                ischeckedidHiddenfied = (HiddenField)row.FindControl(
                    "AssignCustomerRoleRights_matrixGridView_roleRightsIscheckedHiddenField");

                if (modIDHiddenField == null ||
                    submodIDHiddenField == null ||
                    feaIDHiddenField == null ||
                    roleRightIDHiddenField == null)
                {

                    continue;
                }

                roleRightDeatail = new RoleRightMatrixDetail();

                if (!Utility.IsNullOrEmpty(modIDHiddenField))
                {
                    roleRightDeatail.ModuleID = int.Parse(modIDHiddenField.Value);
                }
                if (!Utility.IsNullOrEmpty(submodIDHiddenField))
                {
                    roleRightDeatail.SubModuleID = int.Parse(submodIDHiddenField.Value);
                }
                if (!Utility.IsNullOrEmpty(feaIDHiddenField))
                {
                    roleRightDeatail.FeatureID = int.Parse(feaIDHiddenField.Value);
                }
                if (!Utility.IsNullOrEmpty(roleRightIDHiddenField))
                {
                    roleRightDeatail.RoleRightID = int.Parse(roleRightIDHiddenField.Value);
                }
                if (!Utility.IsNullOrEmpty(isApplicableCheckBox))
                {
                    roleRightDeatail.IsApplicable = isApplicableCheckBox.Checked;
                }
                if (!Utility.IsNullOrEmpty(ischeckedidHiddenfied))
                {
                    roleRightDeatail.IsChecked = bool.Parse(ischeckedidHiddenfied.Value);
                }

                roleRightDeatail.RoleID = int.Parse(AssignCustomerRoleRights_roleDropDownList.SelectedValue);

                roleRightDeatail.CreatedBy = base.userID;

                roleRightDeatail.TenantID = base.tenantID;

                fearueRightMatrix.Add(roleRightDeatail);
            }

            new AuthenticationBLManager().SaveCorporateFeatureRightsDetails(fearueRightMatrix,base.tenantID);

            ShowMessage(AssignCustomerRoleRights_bottomSuccessMessageLabel, AssignCustomerRoleRights_topSuccessMessageLabel,
                "Role rights updated successfully");
        }   
        #endregion Private Method

        #region Protected Override Methods                                     
        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
           
            AssignCustomerRoleRights_matrixGridView.DataSource = null;
            AssignCustomerRoleRights_matrixGridView.DataBind();

            AssignCustomerRoleRights_matrixGridView.DataSource = new AuthenticationBLManager().GetCorporateFeatureRightsDetails
                (int.Parse(AssignCustomerRoleRights_roleDropDownList.SelectedValue),base.tenantID);

            //assign the column name as the role drop down selected text 
            AssignCustomerRoleRights_matrixGridView.Columns[3].HeaderText =
                AssignCustomerRoleRights_roleDropDownList.SelectedItem.Text;
            AssignCustomerRoleRights_matrixGridView.DataBind();
        }
        #endregion Protected Override Methods

    }
   
}