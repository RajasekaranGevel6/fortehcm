﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Forte.HCM.UI.Common;
using Forte.HCM.BL;
using Forte.HCM.DataObjects;
using Forte.HCM.Trace;
using Forte.HCM.EventSupport;
using Forte.HCM.Support;

namespace Forte.HCM.UI.CustomerAdmin
{
    public partial class AssignCustomerUserRights : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "250px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "350px";

        #endregion Private Constants

        #region Event Handler
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Assign Customer Rights");              

                Page.Form.DefaultButton = AssignCustomerUserRights_showDetailButton.UniqueID;

                Page.SetFocus(AssignCustomerUserRights_showDetailButton.ClientID);

                if (!IsPostBack)
                {
                    LoadCustomers();
                   


                    //Add on click attribute for the check box list
                    //AssignCustomerUserRights_modulesCheckBoxList.Attributes.Add("onclick", "javascript:return selectedModuleId();");
                    if (Request.QueryString["userID"] != null)
                    {
                        UserDetail userDetail = new CommonBLManager().GetUserDetail(int.Parse(Request.QueryString["userID"]));
                      
                    }

                }


                AssignCustomerUserRights_assignRights_expandTD.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                AssignCustomerUserRights_assignRightsDiv.ClientID + "','" +
                AssignCustomerUserRights_searchDIV.ClientID + "','" +
                AssignCustomerUserRights_assignRolesUpSpan.ClientID + "','" +
                AssignCustomerUserRights_assignRolesDownSpan.ClientID + "','" +
                AssignCustomerUserRights_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");

                AssignCustomerUserRights_topErrorMessageLabel.Text = string.Empty;
                AssignCustomerUserRights_topSuccessMessageLabel.Text = string.Empty;

                AssignCustomerUserRights_bottomErrorMessageLabel.Text = string.Empty;
                AssignCustomerUserRights_bottomSuccessMessageLabel.Text = string.Empty;

                //Assign on click event handler for the page navigator
                AssignCustomerUserRights_pageNavigator.PageNumberClick += new
                    CommonControls.PageNavigator.PageNumberClickEventHandler
                    (AssignCustomerUserRights_pageNavigator_PageNumberClick);

                CheckAndSetExpandOrRestore();

            }
            catch (Exception exception)
            {
                base.ShowMessage(AssignCustomerUserRights_topErrorMessageLabel,
                    AssignCustomerUserRights_bottomErrorMessageLabel, exception.Message);
               Logger.ExceptionLog(exception);
            }
        }
        private void LoadCustomers()
        {
            List<UserDetail> customerList = new AdminBLManager().GetCustomerUser(base.tenantID);

            AssignCustomerUserRights_customerNameDropDownList.DataSource = customerList;
            AssignCustomerUserRights_customerNameDropDownList.DataTextField = "UserName";
            AssignCustomerUserRights_customerNameDropDownList.DataValueField = "UserID";
            AssignCustomerUserRights_customerNameDropDownList.DataBind();
            AssignCustomerUserRights_customerNameDropDownList.Items.Insert(0,
            new ListItem("--Select--", "0"));
        }

        /// <summary>
        /// Handles the PageNumberClick event of the AssignCustomerUserRights_pageNavigator control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Forte.HCM.EventSupport.PageNumberEventArgs"/>
        /// instance containing the event data.</param>
        void AssignCustomerUserRights_pageNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadUserRights(e.PageNumber);
            }
            catch (Exception exception)
            {
                base.ShowMessage(AssignCustomerUserRights_topErrorMessageLabel,
                    AssignCustomerUserRights_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);

            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the AssignCustomerUserRights_modulesCheckBoxList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void AssignCustomerUserRights_modulesCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetSelectedRoleIDs();
            }
            catch (Exception exception)
            {
                base.ShowMessage(AssignCustomerUserRights_topErrorMessageLabel,
                    AssignCustomerUserRights_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Handles the Click event of the AssignCustomerUserRights_showDetailButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void AssignCustomerUserRights_showDetailButton_Click(object sender, EventArgs e)
        {
            try
            {
                    AssignCustomerUserRights_assignRolesGridView.DataSource = null;
                    AssignCustomerUserRights_assignRolesGridView.DataBind();

                    AssignCustomerUserRights_assignRolesGridView.AllowSorting = true;
                    AssignCustomerUserRights_pageNavigator.TotalRecords = 0;
                    AssignCustomerUserRights_pageNavigator.PageSize = base.GridPageSize;


                //Assign the defaul values for the sort field and 
                // sort order 
                ViewState["SORT_FIELD"] = "ROLE_NAME";

                ViewState["SORT_ORDER"] = SortType.Ascending;

                AssignCustomerUserRights_pageNavigator.Reset();
                AssignCustomerUserRights_pageNavigator.TotalRecords = 0;

                //Load the deatils for the first page
                LoadUserRights(1);
            }
            catch (Exception exception)
            {
                base.ShowMessage(AssignCustomerUserRights_topErrorMessageLabel,
                    AssignCustomerUserRights_bottomErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Handles the Sorting event of the AssignCustomerUserRights_assignRolesGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/>
        /// instance containing the event data.</param>
        protected void AssignCustomerUserRights_assignRolesGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //check whether the sort field in view state is same as 
                //the sort expression 
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    //Swap the sort order in the view state
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                //Store the sort field in the view state
                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                AssignCustomerUserRights_pageNavigator.Reset();

                //Load the user rights from the first page
                LoadUserRights(1);
            }
            catch (Exception exception)
            {
                ShowMessage(AssignCustomerUserRights_bottomErrorMessageLabel,
                   AssignCustomerUserRights_topErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Handles the RowCreated event of the AssignCustomerUserRights_assignRolesGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/>
        /// instance containing the event data.</param>
        protected void AssignCustomerUserRights_assignRolesGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //if (e.Row.RowType == DataControlRowType.Header)
                //{
                //    //Get the column index for the sorting 
                //    int sortColumnIndex = GetSortColumnIndex
                //        (AssignCustomerUserRights_assignRolesGridView,
                //        ViewState["SORT_FIELD"].ToString());

                //    if (sortColumnIndex != -1)
                //    {
                //        //Add sort image for the corresponding column
                //        AddSortImage(sortColumnIndex, e.Row,
                //            (SortType)ViewState["SORT_ORDER"]);
                //    }
                //}
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerUserRights_bottomErrorMessageLabel,
                    AssignCustomerUserRights_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the AssignCustomerUserRights_saveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void AssignCustomerUserRights_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (AssignCustomerUserRights_assignRolesGridView.Rows.Count == 0)
                {
                    ShowMessage(AssignCustomerUserRights_bottomErrorMessageLabel,
                AssignCustomerUserRights_topErrorMessageLabel, "Please select user right");
                    return;
                }

                SaveUerRights();

                ViewState["SORT_ORDER"] = SortType.Ascending;

                //Store the sort field in the view state
                ViewState["SORT_FIELD"] = "ROLE_NAME";

                // Reset and show records for first page.
                AssignCustomerUserRights_pageNavigator.Reset();

                //Load the user rights from the first page
                LoadUserRights(1);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerUserRights_bottomErrorMessageLabel,
                    AssignCustomerUserRights_topErrorMessageLabel, exception.Message);
            }

        }

        /// <summary>
        /// Handles the Click event of the AssignCustomerUserRights_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void AssignCustomerUserRights_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerUserRights_bottomErrorMessageLabel,
                    AssignCustomerUserRights_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileFormEntry_selectAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void PositionProfileFormEntry_selectAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                
                GetSelectedRoleIDs();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerUserRights_bottomErrorMessageLabel,
                    AssignCustomerUserRights_topErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handles the Click event of the PositionProfileFormEntry_unSelectAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void PositionProfileFormEntry_unSelectAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
              
                GetSelectedRoleIDs();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerUserRights_bottomErrorMessageLabel,
                    AssignCustomerUserRights_topErrorMessageLabel, exception.Message);
            }
        }

        protected void AssignCustomerUserRights_assignRolesGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                {
                    return;
                }



            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerUserRights_bottomErrorMessageLabel,
                    AssignCustomerUserRights_topErrorMessageLabel, exception.Message);
            }
        }

        protected void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                GridViewRow gridViewRow = (GridViewRow)((CheckBox)(sender)).Parent.Parent;

                if (gridViewRow != null)
                {
                    HiddenField userRightsIDhiddenField = (HiddenField)gridViewRow.FindControl("AssignCustomerUserRights_assignRolesGridView_userIDHiddenField");
                    CheckBox checkBox = (CheckBox)gridViewRow.FindControl("AssignCustomerUserRights_assignRolesGridView_allowCheckBox");
                    HiddenField modIdHiddenField = (HiddenField)gridViewRow.FindControl("AssignCustomerUserRights_assignRolesGridView_modIDHiddenField");
                    HiddenField sModIDHiddenField = (HiddenField)gridViewRow.FindControl("AssignCustomerUserRights_assignRolesGridView_subModuleIDHiddenField");
                    HiddenField feaIDHiddenField = (HiddenField)gridViewRow.FindControl("AssignCustomerUserRights_assignRolesGridView_featureIDHiddenField");
                    HiddenField userIDHiddenField = (HiddenField)gridViewRow.FindControl("AssignCustomerUserRights_assignRolesGridView_usrIDHiddenField");
                    HiddenField isActiveHiddenField = (HiddenField)gridViewRow.FindControl("AssignCustomerUserRights_assignRolesGridView_isActiveHiddenField");


                    DataObjects.UserRightsMatrix userRight = new DataObjects.UserRightsMatrix();

                    if (userRightsIDhiddenField.Value != "")
                    {
                        userRight.UserRightsID = int.Parse(userRightsIDhiddenField.Value);
                    }
                    if (checkBox != null)
                    {
                        userRight.IsApplicable = checkBox.Checked;
                    }
                    if (!Utility.IsNullOrEmpty(modIdHiddenField) && modIdHiddenField.Value != "")
                    {
                        userRight.ModuleID = int.Parse(modIdHiddenField.Value);
                    }
                    if (!Utility.IsNullOrEmpty(sModIDHiddenField) && sModIDHiddenField.Value != "")
                    {
                        userRight.SubModuleID = int.Parse(sModIDHiddenField.Value);
                    }
                    if (!Utility.IsNullOrEmpty(feaIDHiddenField) && feaIDHiddenField.Value != "")
                    {
                        userRight.FeatureID = int.Parse(feaIDHiddenField.Value);
                    }

                    if (!Utility.IsNullOrEmpty(isActiveHiddenField) && isActiveHiddenField.Value != "")
                    {
                        userRight.IsActive = Convert.ToBoolean(isActiveHiddenField.Value);
                    }


                    userRight.UserID = int.Parse(AssignCustomerUserRights_customerNameDropDownList.SelectedValue);

                    userRight.ModifiedBy = base.userID;
                    userRight.CreatedBy = base.userID;

                    new AuthenticationBLManager().UpdateCorporateUserRightMatrix(userRight);

                    LoadUserRights(1);

                    base.ShowMessage(AssignCustomerUserRights_bottomSuccessMessageLabel, AssignCustomerUserRights_topSuccessMessageLabel,
    "User rights updated successfully");
                }

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerUserRights_bottomErrorMessageLabel,
                    AssignCustomerUserRights_topErrorMessageLabel, exception.Message);
            }

        }

        protected void AssignCustomerUserRights_hiddenButton_Click(object sender, EventArgs e)
        {
            try
            {
                /*AssignCustomerUserRights_individualRoleNameLabel.Text =
                new AuthenticationBLManager().GetRolesForUser(int.Parse(AssignCustomerUserRights_customerNameDropDownList.SelectedValue));*/
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerUserRights_bottomErrorMessageLabel,
                    AssignCustomerUserRights_topErrorMessageLabel, exception.Message);
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Gets the selected role I ds.
        /// </summary>
        private void GetSelectedRoleIDs()
        {
          /*  AssignCustomerUserRights_roleHiddenField.Value = string.Empty;

            string selectedModuleName = "";

            for (int i = 0; i < AssignCustomerUserRights_modulesCheckBoxList.Items.Count; i++)
            {
                if (AssignCustomerUserRights_modulesCheckBoxList.Items[i].Selected)
                {
                    AssignCustomerUserRights_roleHiddenField.Value += ", " + AssignCustomerUserRights_modulesCheckBoxList.Items[i].Value;

                    selectedModuleName += ", " + AssignCustomerUserRights_modulesCheckBoxList.Items[i].Text;
                }
            }

            if (selectedModuleName != null || selectedModuleName.Length != 0)
            {
                selectedModuleName = selectedModuleName.TrimStart(',', ' ');
                AssignCustomerUserRights_roleHiddenField.Value = AssignCustomerUserRights_roleHiddenField.Value.TrimStart(',', ' ');
            }

            AssignCustomerUserRights_modulesTextBox.Text = selectedModuleName;*/
        }

        /// <summary>
        /// Saves the uer rights.
        /// </summary>
        private void SaveUerRights()
        {
            List<UserRightsMatrix> userMatrixDeatils = new List<UserRightsMatrix>();

            UserRightsMatrix userRight = null;

            foreach (GridViewRow row in AssignCustomerUserRights_assignRolesGridView.Rows)
            {
                userRight = new UserRightsMatrix();

                HiddenField userRightsIDhiddenField = (HiddenField)row.FindControl("AssignCustomerUserRights_assignRolesGridView_userIDHiddenField");
                CheckBox checkBox = (CheckBox)row.FindControl("AssignCustomerUserRights_assignRolesGridView_allowCheckBox");
                HiddenField modIdHiddenField = (HiddenField)row.FindControl("AssignCustomerUserRights_assignRolesGridView_modIDHiddenField");
                HiddenField sModIDHiddenField = (HiddenField)row.FindControl("AssignCustomerUserRights_assignRolesGridView_subModuleIDHiddenField");
                HiddenField feaIDHiddenField = (HiddenField)row.FindControl("AssignCustomerUserRights_assignRolesGridView_featureIDHiddenField");
                // HiddenField rolIDHiddenField = (HiddenField)row.FindControl("AssignCustomerUserRights_assignRolesGridView_rolIDHiddenField");


                if (userRightsIDhiddenField.Value != "")
                {
                    userRight.UserRightsID = int.Parse(userRightsIDhiddenField.Value);
                }
                if (checkBox != null)
                {
                    userRight.IsApplicable = checkBox.Checked;
                }
                if (!Utility.IsNullOrEmpty(modIdHiddenField) && modIdHiddenField.Value != "")
                {
                    userRight.ModuleID = int.Parse(modIdHiddenField.Value);
                }
                if (!Utility.IsNullOrEmpty(sModIDHiddenField) && sModIDHiddenField.Value != "")
                {
                    userRight.SubModuleID = int.Parse(sModIDHiddenField.Value);
                }
                if (!Utility.IsNullOrEmpty(feaIDHiddenField) && feaIDHiddenField.Value != "")
                {
                    userRight.FeatureID = int.Parse(feaIDHiddenField.Value);
                }
               
                userRight.IsEditable = checkBox.Enabled;

                userRight.UserID = int.Parse(AssignCustomerUserRights_customerNameDropDownList.SelectedValue);

                userRight.CreatedBy = base.userID;

                userRight.ModifiedBy = base.userID;

                userMatrixDeatils.Add(userRight);
            }

            new AuthenticationBLManager().UpdateUserRights(userMatrixDeatils);

            base.ShowMessage(AssignCustomerUserRights_bottomSuccessMessageLabel, AssignCustomerUserRights_topSuccessMessageLabel,
                "User rights updated successfully");
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandOrRestore()
        {
            //If the is miaximized hidden field is y. Change the height to the expanded height
            if (!Utility.IsNullOrEmpty(AssignCustomerUserRights_restoreHiddenField.Value) &&
                 AssignCustomerUserRights_restoreHiddenField.Value == "Y")
            {
                AssignCustomerUserRights_searchDIV.Style["display"] = "none";
                AssignCustomerUserRights_assignRolesUpSpan.Style["display"] = "block";
                AssignCustomerUserRights_assignRolesDownSpan.Style["display"] = "none";
                AssignCustomerUserRights_assignRightsDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                AssignCustomerUserRights_searchDIV.Style["display"] = "block";
                AssignCustomerUserRights_assignRolesUpSpan.Style["display"] = "none";
                AssignCustomerUserRights_assignRolesDownSpan.Style["display"] = "block";
                AssignCustomerUserRights_assignRightsDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Loads the user rights.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        private void LoadUserRights(int pageNumber)
        {
            string orderBy = ViewState["SORT_FIELD"].ToString();
            SortType orderByDirection = ((SortType)ViewState["SORT_ORDER"]);

            int totalRecordCount = 0;

            List<UserRightsMatrix> customerRightDetails = new List<UserRightsMatrix>();

            int showAllFeature = AssignCustomerUserRights_showApplicableFeaturesRadioButton.Checked ? 0 : 1;

            customerRightDetails = new AdminBLManager().GetCustomerRightsDetails
                (AssignCustomerUserRights_customerNameDropDownList.SelectedValue,
                showAllFeature, base.tenantID, out totalRecordCount);

            if (customerRightDetails.Count == 0)
            {
                base.ShowMessage(AssignCustomerUserRights_topErrorMessageLabel,
                    AssignCustomerUserRights_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);

                AssignCustomerUserRights_assignRolesGridView.DataSource = customerRightDetails;
                AssignCustomerUserRights_assignRolesGridView.DataBind();

                AssignCustomerUserRights_pageNavigator.TotalRecords = 0;

                return;
            }

            AssignCustomerUserRights_assignRolesGridView.DataSource = customerRightDetails;
            AssignCustomerUserRights_assignRolesGridView.DataBind();

            AssignCustomerUserRights_assignRolesGridView.AllowSorting = true;
            AssignCustomerUserRights_pageNavigator.TotalRecords = totalRecordCount;
            AssignCustomerUserRights_pageNavigator.PageSize = base.GridPageSize;
        }
        #endregion Private Methods

        #region Protected Override Methods
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion Protected Override Methods

    }
}