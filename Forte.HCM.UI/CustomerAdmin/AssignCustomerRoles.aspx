﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssignCustomerRoles.aspx.cs"
    MasterPageFile="~/MasterPages/CustomerAdminMaster.Master" Inherits="Forte.HCM.UI.CustomerAdmin.AssignCustomerRoles" %>
    <%@ MasterType VirtualPath="~/MasterPages/CustomerAdminMaster.Master" %>

<asp:Content ID="AssignCustomerRoles_Content" ContentPlaceHolderID="CustomerAdminMaster_body"
    runat="server">
    <script type="text/javascript" language="javascript">

        var xGridPos, yGridPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xGridPos = $get('<%= AssignCustomerRoles_userDetailsDIV.ClientID %>').scrollLeft;
            yGridPos = $get('<%= AssignCustomerRoles_userDetailsDIV.ClientID %>').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('<%= AssignCustomerRoles_userDetailsDIV.ClientID %>').scrollLeft = xGridPos;
            $get('<%= AssignCustomerRoles_userDetailsDIV.ClientID %>').scrollTop = yGridPos;
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width: 72%" class="header_text_bold">
                                    <asp:Label ID="test" runat="server"></asp:Label>
                                    <asp:Literal ID="AssignCustomerRoles_headerLiteral" runat="server" Text="Assign User Roles"></asp:Literal>
                                </td>
                                <td width="28%" align="right">
                                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                        <tr>
                                            <td style="width: 52%">
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Button ID="AssignCustomerRoles_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                    OnClick="AssignCustomerRoles_topSaveButton_Click" Visible="false" />
                                            </td>
                                            <td style="width: 16%" align="right">
                                                <asp:LinkButton ID="AssignCustomerRoles_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="AssignCustomerRoles_topResetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td width="18%" align="left">
                                                <asp:LinkButton ID="AssignCustomerRoles_topCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="AssignCustomerRoles_topMessageUpdatePanel" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="AssignCustomerRoles_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                <asp:Label ID="AssignCustomerRoles_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
                <tr>
                    <td class="tab_body_bg">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td class="panel_bg">
                                                <div id="ViewContributorSummary_questionSummaryDIV" runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <asp:UpdatePanel ID="AssignCustomerRoles_selectRoleUpdatePanel" runat="server">
                                                                        <ContentTemplate>
                                                                            <table width="100%" cellpadding="0" cellspacing="3" border="0" class="panel_inner_body_bg">
                                                                                <tr>
                                                                                    <td style="vertical-align: middle">
                                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                            <tr>
                                                                                                <td style="width: 5%">
                                                                                                    <asp:Label ID="AssignCustomerRoles_selectUser" runat="server" Text="Customer" SkinID="sknLabelFieldHeaderText">
                                                                                                    </asp:Label>
                                                                                                    <span class='mandatory'>*</span>
                                                                                                </td>                                                                                                
                                                                                                <td  style="width:17%">
                                                                                                    <asp:DropDownList ID="AssignCustomerRoles_customerNameDropDownList" runat="server" Width="220px">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                                <td style="width: 2%">
                                                                                                    <asp:ImageButton ID="AssignCustomerRoles_helpImageButton" SkinID="sknHelpImageButton"
                                                                                                        runat="server" OnClientClick="javascript:return false;" ToolTip="Please select the Tenant here" />
                                                                                                </td>
                                                                                                <td style="width: 40%">
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:RadioButton ID="AssignCustomerRoles_applcableRoleRadioButton" runat="server"
                                                                                                                    Checked="true" GroupName="a" Text="Show Assigned Roles Only" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:RadioButton ID="AssignCustomerRoles_allRolesRadioButton" runat="server" GroupName="a"
                                                                                                                    Text="Show All Roles" />
                                                                                                            </td>
                                                                                                            <td colspan="2">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Button ID="AssignCustomerRoles_showButton" runat="server" OnClick="AssignCustomerRoles_showButton_Click"
                                                                    SkinID="sknButtonId" Text="Show Details" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 98%" align="left">
                                                            <asp:Label ID="AssignCustomerRoles_assignRolesLabel" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="width: 2%" align="right">
                                                            <asp:HiddenField ID="AssignCustomerRoles_restoreHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="AssignCustomerRoles_userDetailsTR" runat="server">
                                            <td class="header_bg" align="center">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="AssignCustomerRoles_userDetailHeaderLiteral" runat="server" Text="Assign Roles"></asp:Literal>
                                                            <asp:Label ID="AssignCustomerRoles_userDetailHeaderHelpLabel" runat="server" SkinID="sknLabelText"
                                                                Text=" - Click column headers to sort">                                                
                                                            </asp:Label>
                                                        </td>
                                                        <td style="width: 50%" align="right">
                                                            <span id="AssignCustomerRoles_userDetailsUparrowSpan" style="display: block;" runat="server">
                                                                <asp:Image ID="AssignCustomerRoles_userDetailsUpArrow" runat="server" SkinID="sknMinimizeImage" />
                                                            </span><span id="AssignCustomerRoles_userDetailsDownarrowSpan" style="display: none;"
                                                                runat="server">
                                                                <asp:Image ID="AssignCustomerRoles_userDetailsDownArrow" runat="server" SkinID="sknMaximizeImage" />
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <div id="AssignCustomerRoles_userDetailsDIV" runat="server" style="height: 525px;
                                                    overflow: auto;">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="AssignCustomerRoles_rolesUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView ID="AssignCustomerRoles_rolesGridView" runat="server" AllowSorting="True"
                                                                            OnSorting="AssignCustomerRoles_userRolesGridView_Sorting" OnRowCreated="AssignCustomerRoles_userRoles_GridView_RowCreated"
                                                                            AutoGenerateColumns="False">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Role" HeaderStyle-Width="25%" HeaderStyle-HorizontalAlign="Left"
                                                                                    ItemStyle-Width="25%" SortExpression="ROLE">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="AssignCustomerRoles_rolesGridView_rolecategotycodeLabel" runat="server"
                                                                                            Text='<%# Eval("RoleName") %>'>
                                                                                        </asp:Label>
                                                                                        <asp:HiddenField ID="AssignCustomerRoles_rolenAMEGridViewIDHiddenField" runat="server"
                                                                                            Value='<%# Eval("RoleID") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Job Description" HeaderStyle-Width="70%" HeaderStyle-HorizontalAlign="Left"
                                                                                    ItemStyle-Width="36%" SortExpression="JOBDISCRIPTION">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="AssignCustomerRoles_rolesGridView_rolecategotynameLabel" runat="server"
                                                                                            Text='<%# Eval("RoleJobDescription") %>'>
                                                                                        </asp:Label>
                                                                                        <asp:HiddenField ID="AssignCustomerRoles_rolesjdGridViewIDHiddenField" runat="server"
                                                                                            Value='<%# Eval("RoleID") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>                                                                                
                                                                                <asp:TemplateField HeaderText="Allow" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left"
                                                                                    ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="AssignCustomerRoles_rolesGridView_CheckBox" runat="server" AutoPostBack="true"
                                                                                            Checked="false" OnCheckedChanged="checkBox_CheckedChanged" />
                                                                                        <ajaxToolKit:ToggleButtonExtender ID="AssignCustomerRoles_assignRolesGridView_allowCheckBoxToggleButton"
                                                                                            runat="server" TargetControlID="AssignCustomerRoles_rolesGridView_CheckBox" ImageHeight="25"
                                                                                            ImageWidth="25" CheckedImageUrl="~/App_Themes/DefaultTheme/Images/correct_answer.png"
                                                                                            UncheckedImageUrl="~/App_Themes/DefaultTheme/Images/wrong_answer.png" CheckedImageOverAlternateText="Assign Rights"
                                                                                            UncheckedImageOverAlternateText="Unassign Roles">
                                                                                        </ajaxToolKit:ToggleButtonExtender>
                                                                                        <asp:HiddenField ID="AssignCustomerRoles_rolesGridViewIDHiddenField" runat="server"
                                                                                            Value='<%# Eval("RoleID") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                        <asp:HiddenField ID="AssignCustomerRoles_checkedStatusHiddenField" runat="server" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <caption>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                </tr>
            </table>
            </div> </caption> </td> </tr>
            <tr>
                <td class="td_height_5">
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="AssignCustomerRoles_bottomMessageUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="AssignCustomerRoles_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            <asp:Label ID="AssignCustomerRoles_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="td_height_5">
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                </td>
            </tr>
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="width: 72%" class="header_text_bold">
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td style="width: 52%">
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Button ID="AssignCustomerRoles_bottomSaveButton" runat="server" Text="Save"
                                                SkinID="sknButtonId" OnClick="AssignCustomerRoles_topSaveButton_Click" Visible="false" />
                                        </td>
                                        <td style="width: 16%" align="right">
                                            <asp:LinkButton ID="AssignCustomerRoles_bottomResetLinkButton" runat="server" Text="Reset"
                                                SkinID="sknActionLinkButton" OnClick="AssignCustomerRoles_topResetLinkButton_Click"></asp:LinkButton>
                                        </td>
                                        <td width="4%" align="center" class="link_button">
                                            |
                                        </td>
                                        <td width="18%" align="left">
                                            <asp:LinkButton ID="AssignCustomerRoles_bottomCancelLinkButton" runat="server" Text="Cancel"
                                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:HiddenField ID="AssignCustomerRoles_isMaximizedHiddenField" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
