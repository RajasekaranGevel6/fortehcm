﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// UserRoleMatrix.aspx.cs
// File that represents the AssignCustomerRoles class that defines the user 
// interface layout and functionalities for the FeatureUsage page. This 
// page helps in assigning roles to the user. This class inherits 
// Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives                                                             
using System;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
#endregion Directives

namespace Forte.HCM.UI.CustomerAdmin
{
    public partial class AssignCustomerRoles : PageBase
    {
        #region Private Constants                                              
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "525px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "510px";
        #endregion Private Constants

        #region Event Handlers                                                 
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set the page caption in the page
                Master.SetPageCaption("Assign Tenant Roles");
               // Master.HideUpdateProgress(AssignCustomerRoles_selectRoleUpdatePanel.UniqueID);
                AssignCustomerRoles_topSuccessMessageLabel.Text = string.Empty;
                AssignCustomerRoles_bottomSuccessMessageLabel.Text = string.Empty;
                AssignCustomerRoles_topErrorMessageLabel.Text = string.Empty;
                AssignCustomerRoles_bottomErrorMessageLabel.Text = string.Empty;
                CheckAndSetExpandOrRestore();
                Page.Form.DefaultButton = AssignCustomerRoles_showButton.UniqueID;
               

                if (!IsPostBack)
                {
                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "ROLE";

                    SubscribtClientSideHandlers();

                    LoadValues();
                    LoadCustomers();


                    //Add on click attribute for the check box list
                    //UserRightsMatrix_modulesCheckBoxList.Attributes.Add("onclick", "javascript:return selectedModuleId();");
                    if (Request.QueryString["userID"] != null)
                    {
                        UserDetail userDetail = new CommonBLManager().GetUserDetail
                            (int.Parse(Request.QueryString["userID"]));
                      

                        LoadRolesGridView();
                    }

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(AssignCustomerRoles_topErrorMessageLabel,
                    AssignCustomerRoles_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row creation
        /// action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void AssignCustomerRoles_rolesGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex
                    (AssignCustomerRoles_rolesGridView,
                    (string)ViewState["SORT_FIELD"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerRoles_topErrorMessageLabel,
                    AssignCustomerRoles_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save is clicked
        /// after assigning the rights to the user.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void AssignCustomerRoles_topSaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (AssignCustomerRoles_rolesGridView.Rows.Count == 0)
                {
                    base.ShowMessage(AssignCustomerRoles_topErrorMessageLabel,
                        AssignCustomerRoles_bottomErrorMessageLabel,
                        "User role list not available");
                    return;
                }

                int userID = int.Parse(AssignCustomerRoles_customerNameDropDownList.SelectedValue);

                string sortOrder = ViewState["SORT_ORDER"].ToString();

                string sortExpression = ViewState["SORT_FIELD"].ToString();

                List<UsersRoleMatrix> rolesList = new AuthenticationBLManager().
                    GetAppliacableUserRoleRights(sortExpression, sortOrder, userID);

                for (int i = 0; i < AssignCustomerRoles_rolesGridView.Rows.Count; i++)
                {

                    HiddenField userRights_HiddenField = (HiddenField)AssignCustomerRoles_rolesGridView.Rows[i].FindControl
                                ("AssignCustomerRoles_rolenAMEGridViewIDHiddenField");

                    CheckBox userRights_CheckBox = (CheckBox)AssignCustomerRoles_rolesGridView.Rows[i].FindControl
                                 ("AssignCustomerRoles_rolesGridView_CheckBox");

                    if (userRights_CheckBox.Checked == true)
                    {
                        new AuthenticationBLManager().InsertNewUserRoles(userID, Convert.ToInt32(userRights_HiddenField.Value),
                            base.userID);
                    }

                    for (int j = 0; j < rolesList.Count; j++)
                    {
                        if ((Convert.ToInt32(userRights_HiddenField.Value) == Convert.ToInt32(rolesList[j].RoleID)) &&
                            (userRights_CheckBox.Checked == false))
                        {
                            new AuthenticationBLManager().DeleteUserRoles(Convert.ToInt32(rolesList[j].URLID),
                                userID);
                        }
                    }

                }

                LoadRolesGridView();
                AssignCustomerRoles_topErrorMessageLabel.Text = string.Empty;
                AssignCustomerRoles_bottomErrorMessageLabel.Text = string.Empty;

                base.ShowMessage(AssignCustomerRoles_topSuccessMessageLabel,
                   AssignCustomerRoles_bottomSuccessMessageLabel, "Roles updated successfully");
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerRoles_topErrorMessageLabel,
                    AssignCustomerRoles_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the reset is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void AssignCustomerRoles_topResetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
             //   AssignCustomerRoles_userIDTextBox.Text = string.Empty;
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerRoles_topErrorMessageLabel,
                    AssignCustomerRoles_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when column sorting action
        /// takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void AssignCustomerRoles_userRolesGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                LoadRolesGridView();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerRoles_topErrorMessageLabel,
                    AssignCustomerRoles_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when show is clicked
        /// after assigning the rights to the user.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void AssignCustomerRoles_showButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (AssignCustomerRoles_customerNameDropDownList.SelectedIndex==0)
                {
                    base.ShowMessage(AssignCustomerRoles_topErrorMessageLabel,
                       AssignCustomerRoles_bottomErrorMessageLabel,
                      "Select any user");
                    return;
                }
               
                LoadRolesGridView();


            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerRoles_topErrorMessageLabel,
                    AssignCustomerRoles_bottomErrorMessageLabel, exception.Message);
            }
        }

        private void LoadCustomers()
        {
            List<UserDetail> customerList = new AdminBLManager().GetCustomerUser(base.tenantID);

            AssignCustomerRoles_customerNameDropDownList.DataSource = customerList;
            AssignCustomerRoles_customerNameDropDownList.DataTextField = "UserName";
            AssignCustomerRoles_customerNameDropDownList.DataValueField = "UserID";
            AssignCustomerRoles_customerNameDropDownList.DataBind();
            AssignCustomerRoles_customerNameDropDownList.Items.Insert(0,
             new ListItem("--Select--", "0"));
        }

        /// <summary>
        /// Handler method that will be called when the row creation
        /// action takes place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void AssignCustomerRoles_userRoles_GridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex
                    (AssignCustomerRoles_rolesGridView,
                    (string)ViewState["SORT_FIELD"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerRoles_topErrorMessageLabel,
                    AssignCustomerRoles_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                GridViewRow gridViewRow = (GridViewRow)((CheckBox)(sender)).Parent.Parent;

                if (gridViewRow != null)
                {
                    int userID = int.Parse(AssignCustomerRoles_customerNameDropDownList.SelectedValue);

                    string sortOrder = ViewState["SORT_ORDER"].ToString();

                    string sortExpression = ViewState["SORT_FIELD"].ToString();

                    List<UsersRoleMatrix> rolesList = new AuthenticationBLManager().
                        GetAppliacableUserRoleRights(sortExpression, sortOrder, userID);

                    HiddenField userRights_HiddenField = (HiddenField)gridViewRow.FindControl
                                ("AssignCustomerRoles_rolenAMEGridViewIDHiddenField");

                    CheckBox userRights_CheckBox = (CheckBox)gridViewRow.FindControl
                                 ("AssignCustomerRoles_rolesGridView_CheckBox");

                    if (userRights_CheckBox.Checked == true)
                    {
                        new AuthenticationBLManager().InsertCorporateNewUserRoles(userID,
                            Convert.ToInt32(userRights_HiddenField.Value), base.userID);
                    }
                    else
                        new AuthenticationBLManager().DeleteUserRoles(Convert.ToInt32(userRights_HiddenField.Value),
                             userID);

                    LoadRolesGridView();

                    AssignCustomerRoles_topErrorMessageLabel.Text = string.Empty;
                    AssignCustomerRoles_bottomErrorMessageLabel.Text = string.Empty;

                    base.ShowMessage(AssignCustomerRoles_topSuccessMessageLabel,
                       AssignCustomerRoles_bottomSuccessMessageLabel, "Roles updated successfully");
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(AssignCustomerRoles_topErrorMessageLabel,
                    AssignCustomerRoles_bottomErrorMessageLabel, exception.Message);
            }

        }
        #endregion Event Handlers

        #region private Methods                                                
        /// <summary>
        /// Method to check for the Expand or Restore images
        /// </summary>
        private void CheckAndSetExpandOrRestore()
        {
            //If the is miaximized hidden field is y. Change the height to the expanded height
            if (!Utility.IsNullOrEmpty(AssignCustomerRoles_isMaximizedHiddenField.Value) &&
                 AssignCustomerRoles_isMaximizedHiddenField.Value == "Y")
            {
                ViewContributorSummary_questionSummaryDIV.Style["display"] = "none";
                AssignCustomerRoles_userDetailsUparrowSpan.Style["display"] = "block";
                AssignCustomerRoles_userDetailsDownarrowSpan.Style["display"] = "none";
                AssignCustomerRoles_userDetailsDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                ViewContributorSummary_questionSummaryDIV.Style["display"] = "block";
                AssignCustomerRoles_userDetailsUparrowSpan.Style["display"] = "none";
                AssignCustomerRoles_userDetailsDownarrowSpan.Style["display"] = "block";
                AssignCustomerRoles_userDetailsDIV.Style["height"] = RESTORED_HEIGHT;
            }
        }
        /// <summary>
        /// Method to load the roles
        /// </summary>
        private void LoadRolesGridView()
        {
            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD"].ToString();

            int userID = int.Parse(AssignCustomerRoles_customerNameDropDownList.SelectedValue);

            List<UsersRoleMatrix> rolesList = new AuthenticationBLManager().GetAppliacableUserRoleRights(
                sortExpression, sortOrder, userID);

            List<UsersRoleMatrix> rolesAllList = new AuthenticationBLManager().GetAllCorporateUserRoleRights(
                sortExpression, sortOrder,base.tenantID);

            CheckBox checkRoles = null;

            if (AssignCustomerRoles_applcableRoleRadioButton.Checked == true)
            {

                AssignCustomerRoles_rolesGridView.DataSource = rolesList;

                AssignCustomerRoles_rolesGridView.DataBind();

                for (int i = 0; i < AssignCustomerRoles_rolesGridView.Rows.Count; i++)
                {
                    checkRoles = (CheckBox)AssignCustomerRoles_rolesGridView.Rows[i].
                        FindControl("AssignCustomerRoles_rolesGridView_CheckBox");

                    checkRoles.Checked = true;
                }

                if (rolesList == null || rolesList.Count == 0)
                {
                    ShowMessage(AssignCustomerRoles_topErrorMessageLabel, AssignCustomerRoles_bottomErrorMessageLabel,
                        "No data found to display");
                }
            }
            else
            {
                AssignCustomerRoles_rolesGridView.DataSource = rolesAllList;

                AssignCustomerRoles_rolesGridView.DataBind();

                for (int i = 0; i < AssignCustomerRoles_rolesGridView.Rows.Count; i++)
                {

                    HiddenField userRights_HiddenField = (HiddenField)AssignCustomerRoles_rolesGridView.Rows[i].FindControl
                                ("AssignCustomerRoles_rolenAMEGridViewIDHiddenField");


                    for (int j = 0; j < rolesList.Count; j++)
                    {
                        if ((userID == Convert.ToInt32(rolesList[j].UserID)) &&
                            (Convert.ToInt32(userRights_HiddenField.Value) == Convert.ToInt32(rolesList[j].RoleID)))
                        {
                            CheckBox userRights_CheckBox = (CheckBox)AssignCustomerRoles_rolesGridView.Rows[i].FindControl
                                  ("AssignCustomerRoles_rolesGridView_CheckBox");

                            userRights_CheckBox.Checked = true;
                        }
                    }

                }
            }
        }
        /// <summary>
        /// Method to Subscribe the click event for the
        /// Expand or restore.
        /// </summary>
        private void SubscribtClientSideHandlers()
        {
            //AssignCustomerRoles_userIDTextBox.Attributes.Add("onKeyPress", "return focusME(event,'"
            //       + AssignCustomerRoles_userIDTextBox.ClientID + "','NAME','"
            //       + AssignCustomerRoles_browserHiddenField.ClientID + "');");
            ////Assign the on click attribute for the search image
            //AssignCustomerRoles_searchImage.Attributes.Add("onclick",
            //     "javascript:return LoadUserForUserRoleRights('"
            //    + AssignCustomerRoles_userIDTextBox.ClientID + "','"
            //    + AssignCustomerRoles_userIDHiddenField.ClientID + "','"
            //    + AssignCustomerRoles_userIDTextBox.ClientID + "','QA')");


            ////Assign the on click attributes for the expand or restore button 
            //AssignCustomerRoles_userDetailsTR.Attributes.Add("onclick",
            // "ExpandOrRestore('" +
            // AssignCustomerRoles_userDetailsDIV.ClientID + "','" +
            // ViewContributorSummary_questionSummaryDIV.ClientID + "','" +
            // AssignCustomerRoles_userDetailsUparrowSpan.ClientID + "','" +
            // AssignCustomerRoles_userDetailsDownarrowSpan.ClientID + "','" +
            // AssignCustomerRoles_isMaximizedHiddenField.ClientID + "','" +
            // RESTORED_HEIGHT + "','" +
            // EXPANDED_HEIGHT + "')");
            //Adding client side onchange attribute the the authorid textbox.
            /* ViewContributorSummary_questionAuthorIDTextBox.Attributes.Add("onchange", "return TrimUserId('"
                 + ViewContributorSummary_questionAuthorIDTextBox.ClientID + "','" +
                 ViewContributorSummary_authorIDHiddenField.ClientID + "','" +
                 ViewContributorSummary_browserHiddenField.ClientID + "');");*/
        }
        #endregion private Methods

        #region Protected Overridden Methods                                   
        protected override void LoadValues()
        {
           
        }
        protected override bool IsValidData()
        {
            bool value = true;

            return value;
        }
        #endregion Protected Overridden Methods

    }
}