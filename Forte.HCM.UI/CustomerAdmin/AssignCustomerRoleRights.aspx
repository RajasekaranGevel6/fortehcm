﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssignCustomerRoleRights.aspx.cs"
    MasterPageFile="~/MasterPages/CustomerAdminMaster.Master" Inherits="Forte.HCM.UI.CustomerAdmin.AssignCustomerRoleRights" %>
    
<%@ MasterType VirtualPath="~/MasterPages/CustomerAdminMaster.Master" %>
<asp:Content ID="AssignCustomerRoleRights_Content" ContentPlaceHolderID="CustomerAdminMaster_body"
    runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <asp:UpdatePanel ID="AssignCustomerRoleRights_headerUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width: 72%" class="header_text_bold">
                                    <asp:Literal ID="AssignCustomerRoleRights_headerLiteral" runat="server" Text="Assign Role Rights"></asp:Literal>
                                </td>
                                <td width="28%" align="right">
                                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                        <tr>
                                            <td style="width: 52%">
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Button ID="AssignCustomerRoleRights_topSaveButton" runat="server" Text="Save"
                                                    SkinID="sknButtonId" OnClick="AssignCustomerRoleRights_saveButton_Click" />
                                            </td>
                                            <td style="width: 16%" align="right">
                                                <asp:LinkButton ID="AssignCustomerRoleRights_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="AssignCustomerRoleRights_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td width="18%" align="left">
                                                <asp:LinkButton ID="AssignCustomerRoleRights_topCancelLinkButton" runat="server"
                                                    Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="AssignCustomerRoleRights_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="AssignCustomerRoleRights_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <asp:Label ID="AssignCustomerRoleRights_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="AssignCustomerRoleRights_searchHeaderdiv" runat="server">
                                <asp:UpdatePanel ID="AssignCustomerRoleRights_searchHeaderUpdatePanel" runat="server">
                                    <ContentTemplate>
                                        <table width="100%" cellpadding="0" cellspacing="0" class="panel_bg">
                                            <tr>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0" class="panel_inner_body_bg">
                                                        <tr>                                                         
                                                            <td style="width: 2%">
                                                                <asp:Label ID="AssignCustomerRoleRights_roleLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Role"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:DropDownList ID="AssignCustomerRoleRights_roleDropDownList" runat="server" Width="25%"
                                                                    DataTextField="RoleName" DataValueField="RoleID" SkinID="sknSubjectDropDown">
                                                                </asp:DropDownList>
                                                                <asp:ImageButton ID="AssignCustomerRoleRights_roleHelpImageButton1" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the role here" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Button ID="AssignCustomerRoleRights_showDetailsButton" runat="server" Text="Show Details"
                                                        SkinID="sknButtonId" OnClick="AssignCustomerRoleRights_showDetailsButton_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="header_text_bold">
                                                    <asp:Literal ID="AssignCustomerRoleRights_assignRightsLiteral" runat="server" Text="Assign Rights"></asp:Literal>
                                                </td>
                                                <td style="width: 2%" align="right">
                                                    <span id="AssignCustomerRoleRights_assignRightsUpSpan" runat="server" style="display: none;">
                                                        <asp:Image ID="AssignCustomerRoleRights_assignRightsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                            id="AssignCustomerRoleRights_assignRightsDownSpan" runat="server" style="display: block;"><asp:Image
                                                                ID="AssignCustomerRoleRights_assignRightsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                    <asp:HiddenField ID="AssignCustomerRoleRights_restoreHiddenField" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <div id="AssignCustomerRoleRights_matrixRightDIV" runat="server" style="height: 300px;
                                                        overflow: auto;">
                                                        <asp:UpdatePanel ID="AssignCustomerRoleRights_matrixUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView ID="AssignCustomerRoleRights_matrixGridView" runat="server" AutoGenerateColumns="false">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Role Name" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="AssignCustomerRoleRights_assignRolesGridView_RoleNameLabel" runat="server"
                                                                                    Text='<%# Eval("RoleName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Module Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="AssignCustomerRoleRights_matrixGridView_moduleNameLabel" runat="server"
                                                                                    Text='<%# Eval("ModuleName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Sub Module Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="AssignCustomerRoleRights_matrixGridView_subModuleNameLabel" runat="server"
                                                                                    Text='<%# Eval("SubModuleName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Feature Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="AssignCustomerRoleRights_matrixGridView_featureNameLabel" runat="server"
                                                                                    Text='<%# Eval("FeatureName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Role Name">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="AssignCustomerRoleRights_matrixGridView_checkedCheckBox" runat="server"
                                                                                    Checked='<%# Eval("IsApplicable") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="AssignCustomerRoleRights_matrixGridView_modIDHiddenField" runat="server"
                                                                                    Value='<%# Eval("ModuleID") %>' />
                                                                                <asp:HiddenField ID="AssignCustomerRoleRights_matrixGridView_submodIDHiddenField"
                                                                                    runat="server" Value='<%# Eval("SubModuleID") %>' />
                                                                                <asp:HiddenField ID="AssignCustomerRoleRights_matrixGridView_featureIDHiddenField"
                                                                                    runat="server" Value='<%# Eval("FeatureID") %>' />
                                                                                <asp:HiddenField ID="AssignCustomerRoleRights_matrixGridView_roleRightsIDHiddenField"
                                                                                    runat="server" Value='<%# Eval("RoleRightID") %>' />
                                                                                    <asp:HiddenField ID="AssignCustomerRoleRights_matrixGridView_roleRightsIscheckedHiddenField"
                                                                                    runat="server" Value='<%# Eval("IsChecked") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="AssignCustomerRoleRights_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="AssignCustomerRoleRights_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <asp:Label ID="AssignCustomerRoleRights_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 72%" class="header_text_bold">
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 52%">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:Button ID="AssignCustomerRoleRights_bottomSaveButton" runat="server" Text="Save"
                                            SkinID="sknButtonId" OnClick="AssignCustomerRoleRights_saveButton_Click" />
                                    </td>
                                    <td style="width: 16%" align="right">
                                        <asp:LinkButton ID="AssignCustomerRoleRights_bottomResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="AssignCustomerRoleRights_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="AssignCustomerRoleRights_bottomCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
