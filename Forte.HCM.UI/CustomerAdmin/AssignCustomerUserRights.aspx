﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssignCustomerUserRights.aspx.cs"
    MasterPageFile="~/MasterPages/CustomerAdminMaster.Master" Inherits="Forte.HCM.UI.CustomerAdmin.AssignCustomerUserRights" %>
        <%@ MasterType VirtualPath="~/MasterPages/CustomerAdminMaster.Master" %>

<%@ Register TagPrefix="uc1" Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" %>
<asp:Content ID="AssignCustomerUserRights_Content" ContentPlaceHolderID="CustomerAdminMaster_body"
    runat="server">
    <script type="text/javascript" language="javascript">
       
            //find the Browser details
        function detectBrowser() {
            var browser = navigator.appName;
            var b_version = navigator.appVersion;
            var version = parseFloat(b_version);
            if (browser == "Netscape") {
                return true;
            }
            else {  
                return false; ;
            }
        }        

        var xGridPos, yGridPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xGridPos = $get('<%= AssignCustomerUserRights_assignRightsDiv.ClientID %>').scrollLeft;
            yGridPos = $get('<%= AssignCustomerUserRights_assignRightsDiv.ClientID %>').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('<%= AssignCustomerUserRights_assignRightsDiv.ClientID %>').scrollLeft = xGridPos;
            $get('<%= AssignCustomerUserRights_assignRightsDiv.ClientID %>').scrollTop = yGridPos;
        }
        
    </script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <asp:UpdatePanel ID="AssignCustomerUserRights_headerUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width: 72%" class="header_text_bold">
                                    <asp:Literal ID="AssignCustomerUserRights_headerLiteral" runat="server" Text="Assign User Rights"></asp:Literal>
                                </td>
                                <td width="28%" align="right">
                                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                        <tr>
                                            <td style="width: 52%">
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Button ID="AssignCustomerUserRights_topSaveButton" runat="server" Text="Save"
                                                    SkinID="sknButtonId" OnClick="AssignCustomerUserRights_saveButton_Click" Visible="false" />
                                            </td>
                                            <td style="width: 16%" align="right">
                                                <asp:LinkButton ID="AssignCustomerUserRights_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="AssignCustomerUserRights_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td width="18%" align="left">
                                                <asp:LinkButton ID="AssignCustomerUserRights_topCancelLinkButton" runat="server"
                                                    Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="AssignCustomerUserRights_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="AssignCustomerUserRights_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <asp:Label ID="AssignCustomerUserRights_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="AssignCustomerUserRights_searchDIV" runat="server">
                                <asp:UpdatePanel ID="AssignCustomerUserRights_topSelectUpdatePanel" runat="server">
                                    <ContentTemplate>
                                        <table width="100%" cellpadding="0" cellspacing="0" class="panel_bg">
                                            <tr>
                                                <td class="panel_inner_body_bg ">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td colspan="5">
                                                            </td>
                                                          
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:8%">
                                                                <asp:Label ID="AssignCustomerUserRights_userLabel" runat="server" Text="Customer"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                           
                                                            <td style="width: 17%">
                                                                <asp:DropDownList ID="AssignCustomerUserRights_customerNameDropDownList" runat="server"
                                                                    Width="220px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 3%;" align="center">
                                                                <asp:ImageButton ID="AssignCustomerUserRights_userHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the customer here" />
                                                            </td>
                                                           
                                                            <td style="width: 3%;">
                                                            </td>
                                                            <td>
                                                                <div class="alignTop">
                                                                    <asp:RadioButton ID="AssignCustomerUserRights_showApplicableFeaturesRadioButton"
                                                                        runat="server" Text="Show Assigned Features Only" GroupName="a" Checked="true" />
                                                                </div>
                                                                <div class="alignBottom">
                                                                    <asp:RadioButton ID="AssignCustomerUserRights_showAllFeaturesRadioButton" runat="server"
                                                                        Text="Show All Features" GroupName="a" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="8" class="td_height_8">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Button ID="AssignCustomerUserRights_showDetailButton" runat="server" Text="Show Details"
                                                        SkinID="sknButtonId" OnClick="AssignCustomerUserRights_showDetailButton_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_10">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="AssignCustomerUserRights_assignRolesGridViewDIV" runat="server">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="header_bg" id="AssignCustomerUserRights_assignRights_expandTD" runat="server">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="header_text_bold">
                                                        <asp:Label ID="AssignCustomerUserRights_assignRights_Label" runat="server" Text="Assign Rights"></asp:Label>
                                                        &nbsp;<asp:Label ID="AssignCustomerUserRights_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                            Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 2%" align="right">
                                                        <span id="AssignCustomerUserRights_assignRolesUpSpan" runat="server" style="display: none;">
                                                            <asp:Image ID="AssignCustomerUserRights_assignRolesUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                                id="AssignCustomerUserRights_assignRolesDownSpan" runat="server" style="display: block;"><asp:Image
                                                                    ID="AssignCustomerUserRights_assignRolesDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                        <asp:HiddenField ID="AssignCustomerUserRights_restoreHiddenField" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="grid_body_bg">
                                            <asp:UpdatePanel ID="AssignCustomerUserRights_assignRolesUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <div id="AssignCustomerUserRights_assignRightsDiv" runat="server" style="height: 250px;
                                                        overflow: auto;">
                                                        <asp:GridView ID="AssignCustomerUserRights_assignRolesGridView" runat="server" SkinID="sknNewNonSortingGridView"
                                                            AutoGenerateColumns="false" OnRowCreated="AssignCustomerUserRights_assignRolesGridView_RowCreated"
                                                            OnSorting="AssignCustomerUserRights_assignRolesGridView_Sorting" OnRowDataBound="AssignCustomerUserRights_assignRolesGridView_RowDataBound">
                                                            <Columns>
                                                              
                                                                <asp:TemplateField HeaderText="Sub Module Name" ItemStyle-Width="40%" HeaderStyle-Width="45%"
                                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="AssignCustomerUserRights_assignRolesGridView_subModuleNameLabel" runat="server"
                                                                            Text='<%# Eval("SubModuleName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Feature Name" ItemStyle-Width="40%" HeaderStyle-Width="45%"
                                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="AssignCustomerUserRights_assignRolesGridView_featureNameLabel" runat="server"
                                                                            Text='<%# Eval("FeatureName") %>'></asp:Label>
                                                                        <asp:HiddenField ID="AssignCustomerUserRights_assignRolesGridView_modIDHiddenField"
                                                                            runat="server" Value='<%# Eval("ModuleID") %>' />
                                                                        <asp:HiddenField ID="AssignCustomerUserRights_assignRolesGridView_featureIDHiddenField"
                                                                            runat="server" Value='<%# Eval("FeatureID") %>' />
                                                                        <asp:HiddenField ID="AssignCustomerUserRights_assignRolesGridView_subModuleIDHiddenField"
                                                                            runat="server" Value='<%# Eval("SubModuleID") %>' />
                                                                        <asp:HiddenField ID="AssignCustomerUserRights_assignRolesGridView_userIDHiddenField"
                                                                            runat="server" Value='<%# Eval("UserRightsID") %>' />
                                                                        <%--                <asp:HiddenField ID="AssignCustomerUserRights_assignRolesGridView_rolIDHiddenField" runat="server"
                                                                            Value='<%# Eval("RoleID") %>' />--%>
                                                                        <asp:HiddenField ID="AssignCustomerUserRights_assignRolesGridView_isActiveHiddenField"
                                                                            runat="server" Value='<%# Eval("IsActive") %>' />
                                                                        <asp:HiddenField ID="AssignCustomerUserRights_assignRolesGridView_usrIDHiddenField"
                                                                            runat="server" Value='<%# Eval("UserID") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Allow" HeaderStyle-Width="10%" ControlStyle-Width="20%"
                                                                    HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="AssignCustomerUserRights_assignRolesGridView_allowCheckBox" runat="server"
                                                                            Checked='<%# Eval("IsActive") %>' AutoPostBack="true" OnCheckedChanged="checkBox_CheckedChanged" />
                                                                        <ajaxToolKit:ToggleButtonExtender ID="AssignCustomerUserRights_assignRolesGridView_allowCheckBoxToggleButton"
                                                                            runat="server" TargetControlID="AssignCustomerUserRights_assignRolesGridView_allowCheckBox"
                                                                            ImageHeight="25" ImageWidth="25" CheckedImageUrl="~/App_Themes/DefaultTheme/Images/correct_answer.png"
                                                                            UncheckedImageUrl="~/App_Themes/DefaultTheme/Images/wrong_answer.png" CheckedImageOverAlternateText="Assign Rights"
                                                                            UncheckedImageOverAlternateText="Unassign Rights">
                                                                        </ajaxToolKit:ToggleButtonExtender>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="AssignCustomerUserRights_pageNavigatorUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <uc1:PageNavigator ID="AssignCustomerUserRights_pageNavigator" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_2">
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="AssignCustomerUserRights_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="AssignCustomerUserRights_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <asp:Label ID="AssignCustomerUserRights_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 72%" class="header_text_bold">
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 52%">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:Button ID="AssignCustomerUserRights_bottomSaveButton" runat="server" Text="Save"
                                            SkinID="sknButtonId" OnClick="AssignCustomerUserRights_saveButton_Click" Visible="false" />
                                    </td>
                                    <td style="width: 16%" align="right">
                                        <asp:LinkButton ID="AssignCustomerUserRights_bottomResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="AssignCustomerUserRights_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="AssignCustomerUserRights_bottomCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
