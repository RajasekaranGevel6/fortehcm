﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Landing.aspx.cs
// File that represents the user interface layout and functionalities 
// for the landing page. This page helps in choosing the workflow.
// This class inherits Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives                                                             

using System;

using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities for
    /// the landing page.  This page helps in choosing the workflow.
    /// This class inherits Forte.HCM.UI.Common.PageBase class
    /// </summary>
    public partial class Landing : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Forte HCM - Workflow");

                if (!IsPostBack)
                {
                    if (!Utility.IsNullOrEmpty(Request.QueryString["index"]))
                    {
                        int value = int.Parse(Request.QueryString["index"]);

                        Landing_accordion.SelectedIndex = value;

                        // Scroll the page to show the specific expanded portion.
                        if (value == 3)
                            Page.SetFocus(Landing_scheduleTestImageButton.ClientID);
                        else if (value == 4)
                            Page.SetFocus(Landing_interviewScheduleTestImageButton.ClientID);
                        else if (value == 5)
                            Page.SetFocus(Landing_viewReportsInterviewReportsImageButton.ClientID);
                    }
                }
                if (!Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                {
                    UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;
                    isSiteAdmin = (userDetail.UserType == UserType.SiteAdmin ? true : false);
                    if (!Utility.IsNullOrEmpty(userDetail.SubscriptionRole) &&userDetail.SubscriptionRole== "SR_COR_AMN") btnCorUserMng.Visible = true;
                    
                   
                }
               
                if (isSiteAdmin)
                {
                    Landing_tenantManagementAccordionPane.Visible = true;
                  
                }
            }
                   
            catch (Exception exception)
            {
                ShowMessage(Landing_topErrorMessageLabel, exception.Message);
                Logger.ExceptionLog(exception);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Overridden Methods
    }
}