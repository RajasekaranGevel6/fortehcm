﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Forte.HCM.Trace;

namespace Forte.HCM.UI
{
    public partial class Help : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Help");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(Help_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the Help_cancelLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Help_cancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Default.aspx", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(Help_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the method that shows the user message.
        /// </summary>
        /// <param name="topLabel">
        /// A <see cref="Label"/> that holds the top label control.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        private void ShowMessage(Label label, string message)
        {
            // Set the message to the label.
            if (label.Text.Length == 0)
            {
                label.Text = message;
            }
            else
            {
                label.Text += "<br>" + message;
            }
        }
    }
}