﻿#region Directives                                                   

using System;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Settings
{
    /// <summary>
    /// Represents the class to change the user options
    /// </summary>
    public partial class UserOptions : PageBase
    {
        #region Event Handlers                                       

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("User Options");

                Page.Form.DefaultButton = UserOptions_topSaveButton.UniqueID;
                Page.Form.DefaultFocus = UserOptions_topSaveButton.UniqueID;
                Page.SetFocus(UserOptions_topSaveButton.ClientID);

                if (!IsPostBack)
                {
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(UserOptions_topErrorMessageLabel,
                    UserOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will save the user options to the database.
        /// </remarks>
        protected void UserOptions_saveButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ClearMessages();
                
                // Construct the user option object.
                UserOptionDetail userOption = new UserOptionDetail();
                userOption.UserID = base.userID;
                userOption.SendMailOnNotesAdded = UserOptions_mailSettingsNotesCheckBox.Checked;

                // Save the changes.
                new CommonBLManager().UpdateUserOptions(userOption);

                // Show a success message.
                base.ShowMessage(UserOptions_topSuccessMessageLabel,
                    UserOptions_bottomSuccessMessageLabel, "User options saved successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(UserOptions_topErrorMessageLabel,
                    UserOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset link button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page.
        /// </remarks>
        protected void UserOptions_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(UserOptions_topErrorMessageLabel,
                    UserOptions_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method the clears the messages.
        /// </summary>
        private void ClearMessages()
        {
            UserOptions_topErrorMessageLabel.Text = string.Empty;
            UserOptions_topSuccessMessageLabel.Text = string.Empty;
            UserOptions_bottomErrorMessageLabel.Text = string.Empty;
            UserOptions_bottomSuccessMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Get user option.
            UserOptionDetail optionDetail =  new CommonBLManager().
                GetUserOptions(base.userID);

            // Set the status.
            UserOptions_mailSettingsNotesCheckBox.Checked = optionDetail.SendMailOnNotesAdded;
        }

        #endregion Protected Overridden Methods
    }
}