﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserOptions.aspx.cs" MasterPageFile="~/MasterPages/SettingsMaster.Master"
    Inherits="Forte.HCM.UI.Settings.UserOptions" %>

<%@ MasterType VirtualPath="~/MasterPages/SettingsMaster.Master" %>
<asp:Content ID="UserOptions_content" ContentPlaceHolderID="SettingsMaster_body"
    runat="server">
    <script type="text/javascript" language="javascript">
        function LoadGetForm(dropDownLisID)
        {
            var dropdownlist = document.getElementById(dropDownLisID);
            var value = dropdownlist.options[dropdownlist.selectedIndex].value;
            if (value == 0)
            {
                return;
            }
            else
            {
                LoadPreviewForm('', value);
            }

        }
    </script>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="UserOptions_headerLiteral" runat="server" Text="User Options"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="UserOptions_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="UserOptions_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="UserOptions_topResetLinkButton" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                            OnClick="UserOptions_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="UserOptions_messageUpdatePanel" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="UserOptions_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="UserOptions_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="UserOptions_mailSettingsLiteral" runat="server" Text="Mail Settings"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td >
                                        <asp:UpdatePanel ID="UserOptions_mailSettingsUpdatePanel" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left" style="width: 90%">
                                                            <asp:Label ID="UserOptions_mailSettingsNotesLabel" runat="server" Text="Send mail when notes added against candidate"></asp:Label>
                                                        </td>
                                                        <td align="left" >
                                                            <asp:CheckBox ID="UserOptions_mailSettingsNotesCheckBox" runat="server" Text="" Checked="true" />
                                                        </td>
                                                        <td align="left">
                                                            <asp:ImageButton ID="UserOptions_mailSettingsNotesHelpImageButton" SkinID="sknHelpImageButton"
                                                                runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                ToolTip="Enable on or off for email notification when a note is added to candidate" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="UserOptions_bottomMessageUpdatePanel" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="UserOptions_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="UserOptions_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="4" align="right">
                    <tr>
                        <td>
                            <asp:Button ID="UserOptions_bottomSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                OnClick="UserOptions_saveButton_Click" />
                        </td>
                        <td>
                            <asp:LinkButton ID="UserOptions_bottomResetLinkButton" runat="server" Text="Reset"
                                SkinID="sknActionLinkButton" OnClick="UserOptions_resetLinkButton_Click"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
