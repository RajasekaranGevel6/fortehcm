﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Forte.HCM.UI.MasterPages
{
    public partial class SampleDashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void UpgradeLinkButton_Click(object sender, EventArgs e)
        {
            QuestionImageButton.Enabled = true;

            ScheduleCandidateButton.Enabled = true;

            OnlineTestImageButton.Enabled = true;

            OnlineInterviewImageButton.Enabled = true;

            UserDashboard_reportLeftIconImageButton.Enabled = true;


            assessment_right_bgTD.Attributes.Add("class", "db_sample_right_bg_blue");
            //assessment_right_bgTD.Style["class"] = "";

            assessment_left_bgTD.Attributes.Add("class", "db_right_bg_blue");

            report_right_bg_db.Attributes.Add("class", "db_sample_right_bg_blue");

            report_left_bg_db.Attributes.Add("class", "db_right_bg_blue");

            //report_left_bg_db.Style["class"] = "db_right_bg_blue";

            //assessment_left_bgTD.Style["class"] = "db_right_bg_blue";

            //report_right_bg_db.Style["class"] = "db_sample_right_bg_blue";

            //report_left_bg_db.Style["class"] = "db_right_bg_blue";
        }
    }
}