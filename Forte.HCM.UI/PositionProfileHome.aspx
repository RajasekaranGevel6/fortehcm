﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PositionProfileMaster.Master"
    CodeBehind="PositionProfileHome.aspx.cs" Inherits="Forte.HCM.UI.PositionProfileHome" %>

<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content ID="PositionProfileHome_bodyContent" runat="server" ContentPlaceHolderID="PositionProfileMaster_body">
    <table width="972px" border="0" cellspacing="0" cellpadding="0" class="table_bg"
        align="center">
        <tr style="height: 140px">
            <td align="center">
                <h1 style="color: Gray">
                    Position Profile Home
                </h1>
            </td>
        </tr>
        <tr style="height: 140px">
            <td align="center">
                <img src="App_Themes/DefaultTheme/Images/PositionProfile_Tool.jpg" alt="Position Profile" />
            </td>
        </tr>
    </table>
</asp:Content>
