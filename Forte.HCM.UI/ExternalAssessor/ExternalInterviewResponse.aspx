﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExternalInterviewResponse.aspx.cs"
    Inherits="Forte.HCM.UI.ExternalAssessor.ExternalInterviewResponse" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Candidate Interview Response</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--  BEGIN Browser History required section -->
    <link rel="stylesheet" type="text/css" href="../VideoStreaming/history/history.css" />
    <!--  END Browser History required section -->
    <script type="text/javascript" src="../VideoStreaming/AC_OETags.js" language="javascript"></script>
    <!--  BEGIN Browser History required section -->
    <script type="text/javascript" src="../VideoStreaming/history/history.js" language="javascript"></script>
    <!--  END Browser History required section -->
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 10px;
            overflow: auto;
        }
    </style>
    <script language="JavaScript" type="text/javascript">

        var requiredMajorVersion = 9;
        var requiredMinorVersion = 0;
        var requiredRevision = 28;

        // Handler for onchange and onkeyup to check for max characters.
        function CommentsCount(characterLength, clientId) {
            var maxlength = new Number(characterLength);
            if (clientId.value.length > maxlength) {
                clientId.value = clientId.value.substring(0, maxlength);
            }
        }

        // Handler method that will be called when the maximize or restore
        // button is clicked. This will maximize or restore the panel to
        // show or hide full view.
        function ExpandOrRestore(ctrlExpand, ctrlHide, expandImage, compressImage,
            isMaximizedHidden, restoredHeight, expandedHeight) {
            if (document.getElementById(ctrlExpand) != null) {
                document.getElementById(expandImage).style.display = "none";
                document.getElementById(compressImage).style.display = "none";
                if (document.getElementById(ctrlExpand).style.height.toString() == restoredHeight) {
                    document.getElementById(ctrlExpand).style.height = expandedHeight;
                    document.getElementById(ctrlHide).style.display = "none";
                    document.getElementById(expandImage).style.display = "block";
                    document.getElementById(isMaximizedHidden).value = "Y";
                }
                else {
                    document.getElementById(ctrlExpand).style.height = restoredHeight;
                    document.getElementById(ctrlHide).style.display = "block";
                    document.getElementById(compressImage).style.display = "block";
                    document.getElementById(isMaximizedHidden).value = "N";
                }
            }
            return false;
        }
    </script>
    <script type="text/javascript">

        // Function that downloads the candidate resume.
        function DownloadVideo(folderName,fileName) {
            var url = '../Common/DownloadVideo.aspx?filename=' + fileName + '&foldername=' + folderName + '&type=VIDEOFILE';
            window.open(url, '', 'toolbar=0,resizable=0,width=1,height=1', '');
            return false;
        }
    </script>
</head>
<body>
    <form id="CandidateInterviewResponse" runat="server">
    <ajaxToolKit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolKit:ToolkitScriptManager>
    <div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="popup_td_padding_2" colspan="2">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="height: 8px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%" class="assessor_header_text_grey">
                                <asp:Literal ID="ExternalInterviewResponse_headerLiteral" runat="server" Text="Candidate Interview Response"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="popup_td_padding_2" valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="assessor_inner_bg">
                        <tr>
                            <td class="popup_td_padding_10" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td class="msg_align">
                                            <asp:UpdatePanel ID="ExternalInterviewResponse_messageUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="ExternalInterviewResponse_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                    <asp:Label ID="ExternalInterviewResponse_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ExternalInterviewResponse_saveButton" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <table width="100%" cellpadding="0" border="0" cellspacing="0">
                                                <tr valign="top">
                                                    <td style="width: 30px; height: 20px" valign="top">
                                                        <asp:Image ID="ExternalInterviewResponse_questionHeaderImage" ImageUrl="~/App_Themes/DefaultTheme/Images/question_icon_popup.gif"
                                                            runat="server" ToolTip="Question" />
                                                    </td>
                                                    <td valign="top" class="assessor_question_text_background">
                                                        <div style="width: 570px; word-wrap: break-word;white-space:normal;">
                                                            <div>
                                                                <asp:Label ID="ExternalInterviewResponse_questionValueLabel" runat="server" SkinID="sknLabelQuestionText"></asp:Label>
                                                            </div>
                                                            <div style="text-align: center;" id="ExternalInterviewResponse_questionImageDiv"
                                                                runat="server">
                                                                <asp:Image ID="ExternalInterviewResponse_questionImage" runat="server" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_10">
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td style="height: 30px" valign="top">
                                                        <asp:Image ID="ExternalInterviewResponse_answerImage" ImageUrl="~/App_Themes/DefaultTheme/Images/question_answer.gif"
                                                            runat="server" ToolTip="Question Answer" />
                                                    </td>
                                                    <td valign="top" class="assessor_answer_text_background">
                                                        <div style="width: 570px; word-wrap: break-word;white-space:normal;">
                                                            <asp:Label ID="CandidateResponse_questionAnswerLabel" runat="server" SkinID="sknLabelAnswerText"></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_10">
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td style="height: 30px" valign="top">
                                                        <asp:Image ID="ExternalInterviewResponse_interviewQuestionImage" ImageUrl="~/App_Themes/DefaultTheme/Images/question_comments.gif"
                                                            runat="server" ToolTip="Interview Question Comments" />
                                                    </td>
                                                    <td valign="top" class="assessor_comments_text_background">
                                                        <div style="word-wrap: break-word;white-space:normal;">
                                                        <asp:Label ID="InterviewCandidateTestDetails_commentValueLabel" SkinID="sknLabelCommentsText"
                                                            runat="server"></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_10">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_3">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="assessor_comments_text_background">
                                            <table width="100%" cellpadding="0" border="0" cellspacing="0">
                                                <tr>
                                                    <td style="width: 20%">
                                                        <asp:Label ID="ExternalInterviewResponse_categoryLabel" runat="server" Text="Category"
                                                            SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:Label ID="ExternalInterviewResponse_subjectLabel" runat="server" Text="Subject"
                                                            SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label ID="ExternalInterviewResponse_weightageLabel" runat="server" Text="Weightage"
                                                            SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label ID="ExternalInterviewResponse_skippedLabel" runat="server" Text="Skipped"
                                                            SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label ID="ExternalInterviewResponse_complexityLabel" runat="server" Text="Complexity"
                                                            SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label ID="ExternalInterviewResponse_testAreaLabel" runat="server" Text="Interview Area"
                                                            SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 3px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20%">
                                                        <asp:Label ID="ExternalInterviewResponse_categoryValueLabel" runat="server" SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:Label ID="ExternalInterviewResponse_subjectValueLabel" runat="server" Text=""
                                                            SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label ID="ExternalInterviewResponse_weightageValueLabel" runat="server" Text=""
                                                            SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label ID="ExternalInterviewResponse_skippedValueLabel" runat="server" Text=""
                                                            SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label ID="ExternalInterviewResponse_complexityValueLabel" runat="server" Text=""
                                                            SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label ID="ExternalInterviewResponse_testAreaValueLabel" runat="server" Text=""
                                                            SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_10">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td valign="top" class="video_table">
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td class="video_header_bg">
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td valign="top" align="left">
                                                                                <asp:Literal ID="ExternalInterviewResponse_videoHeaderLabel" runat="server" Text="Video and Text Response"
                                                                                    SkinID="sknLabelText"></asp:Literal>
                                                                            </td>
                                                                            <td valign="top" align="right">
                                                                                <asp:LinkButton ID="ExternalInterviewResponse_downloadVideoLinkButton" runat="server"
                                                                                    Text="Download Video" SkinID="sknActionLinkButton" ToolTip="Click here to download the video"
                                                                                    Visible="false"></asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="video_table_inner_padding" valign="top">
                                                                    <table cellpadding="0" cellspacing="0" border="0"  width="100%">
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <table cellpadding="0" border="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div style="width: 186px; overflow: auto; height: 215px; background-color: Gray;">
                                                                                                <%--<cc1:WVC ID="ExternalInterviewResponse_video" runat="server" BackColor="Black" BorderStyle="Ridge"
                                                                    Height="210px" ShowControls="False" ShowPositionControls="False" ShowStatusBar="False"
                                                                    ShowTracker="False" Width="210px" />--%>
                                                                                                <script language="JavaScript" type="text/javascript">
                                                                <!--
                                                                                                    // Version check for the Flash Player that has the ability to start Player Product Install (6.0r65)
                                                                                                    var hasProductInstall = DetectFlashVer(6, 0, 65);

                                                                                                    // Version check based upon the values defined in globals
                                                                                                    var hasRequestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);

                                                                                                    if (hasProductInstall && !hasRequestedVersion) {
                                                                                                        // DO NOT MODIFY THE FOLLOWING FOUR LINES
                                                                                                        // Location visited after installation is complete if installation is required
                                                                                                        var MMPlayerType = (isIE == true) ? "ActiveX" : "PlugIn";
                                                                                                        var MMredirectURL = window.location;
                                                                                                        document.title = document.title.slice(0, 47) + " - Flash Player Installation";
                                                                                                        var MMdoctitle = document.title;

                                                                                                        AC_FL_RunContent(
		                                                                "src", "playerProductInstall",
		                                                                "FlashVars", "URL=<%= videoURL %>&MMredirectURL=" + MMredirectURL + '&MMplayerType=' + MMPlayerType + '&MMdoctitle=' + MMdoctitle + "",
		                                                                "width", "186",
		                                                                "height", "215",
		                                                                "align", "middle",
		                                                                "id", "OFIStream",
		                                                                "quality", "high",
		                                                                "bgcolor", "#ffffff",
		                                                                "name", "OFIStream",
		                                                                "allowScriptAccess", "sameDomain",
		                                                                "type", "application/x-shockwave-flash",
		                                                                "pluginspage", "http://www.adobe.com/go/getflashplayer"
	                                                                );
                                                                                                    } else if (hasRequestedVersion) {
                                                                                                        // if we've detected an acceptable version
                                                                                                        // embed the Flash Content SWF when all tests are passed
                                                                                                        AC_FL_RunContent(
			                                                                "src", "../VideoStreaming/OFIStream",
			                                                                "FlashVars", "URL=<%= videoURL %>&QuestionID=<%= questionID %>",
			                                                                "width", "186",
			                                                                "height", "215",
			                                                                "align", "left",
			                                                                "id", "OFIStream",
			                                                                "quality", "high",
			                                                                "bgcolor", "#ffffff",
			                                                                "name", "OFIStream",
			                                                                "allowScriptAccess", "sameDomain",
			                                                                "type", "application/x-shockwave-flash",
			                                                                "pluginspage", "http://www.adobe.com/go/getflashplayer"
	                                                                );
                                                                                                    } else {  // flash is too old or we can't detect the plugin
                                                                                                        var alternateContent = 'Alternate HTML content should be placed here. '
  	                                                                + 'This content requires the Adobe Flash Player. '
   	                                                                + '<a href=http://www.adobe.com/go/getflash/>Get Flash</a>';
                                                                                                        document.write(alternateContent);  // insert non-flash content
                                                                                                    }
                                                                // -->
                                                                                                </script>
                                                                                                <noscript>
                                                                                                    <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="OFIStream" width="186px"
                                                                                                        height="215px" codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">
                                                                                                        <param name="movie" value="OFIStream.swf" />
                                                                                                        <param name="quality" value="high" />
                                                                                                        <param name="bgcolor" value="#ffffff" />
                                                                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                                                                        <embed src="../VideoStreaming/OFIStream.swf" quality="high" bgcolor="#ffffff" width="186px"
                                                                                                            height="215px" name="OFIStream" align="middle" play="true" loop="false" quality="high"
                                                                                                            allowscriptaccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer">
			                                                             </embed>
                                                                                                    </object>
                                                                                                </noscript>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table> 
                                                                            </td>
                                                                            <td style="width: 20px">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" border="0" align="left">
                                                                                    <tr>
                                                                                        <td valign="top" style="width: 100%">
                                                                                            <table cellpadding="0" border="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td valign="top" align="left">
                                                                                                        <asp:Image ID="ExternalInterviewResponse_candidateCommentsImage" ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_comments.gif"
                                                                                                            runat="server" ToolTip="Candidate Comments" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td valign="top" align="left">
                                                                                                        <div style="width: 350px; overflow: auto;word-wrap: break-word;white-space:normal;">
                                                                                                            <asp:Label ID="ExternalInterviewResponse_candidateCommentsValueLabel" SkinID="sknLabelCommentsText"
                                                                                                                runat="server"></asp:Label>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <asp:UpdatePanel ID="ExternalInterviewResponse_assessorRatingUpdatePanel" runat="server">
                                                                                                <ContentTemplate>
                                                                                                    <div id="CandiateInterviewResponse_ratingDiv" runat="server">
                                                                                                        <table style="width: 100%;" cellpadding="0" border="0" cellspacing="0">
                                                                                                            <tr>
                                                                                                                <td align="left" class="Question_Rating_title">
                                                                                                                    <asp:Literal ID="ExternalInterviewResponse_assessorRating" runat="server" Text="Question Rating"
                                                                                                                        SkinID="sknLabelRatingText"></asp:Literal>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="height: 5px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td align="left">
                                                                                                                    <table cellpadding="0" cellspacing="0" width="98%" border="0" align="left">
                                                                                                                        <tr>
                                                                                                                            <td align="left">
                                                                                                                                <ajaxToolKit:Rating ID="ExternalInterviewResponse_rating" runat="server" MaxRating="10"
                                                                                                                                    CurrentRating="0" CssClass="rating_star" StarCssClass="rating_item" WaitingStarCssClass="rating_saved"
                                                                                                                                    FilledStarCssClass="rating_filled" EmptyStarCssClass="rating_empty" AutoPostBack="false" />
                                                                                                                            </td>
                                                                                                                            <td align="right">
                                                                                                                                <asp:Label ID="InterviewCandidateTestDetails_userCommentsLabel" runat="server" Text="Comments"
                                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="height: 5px">
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td align="left" colspan="2">
                                                                                                                                <asp:TextBox ID="InterviewCandidateTestDetails_userCommentsTextBox" runat="server"
                                                                                                                                    MaxLength="500" Width="400px" Height="60px" TextMode="MultiLine" onchange="CommentsCount(500,this)"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="height: 8px">
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td valign="middle" align="right" colspan="2">
                                                                                                                               
                                                                                                                                <table style="width: 98%" cellpadding="0" cellspacing="3">
                                                                                                                                    <tr>
                                                                                                                                        <td width="3%">
                                                                                                                                            <asp:HyperLink ID="ExternalInterviewResponse_downloadVideoHyperLink" runat="server"></asp:HyperLink>
                                                                                                                                            <asp:ImageButton ID="ExternalInterviewResponse_downloadVideoImageButton"
                                                                                                                                                runat="server" Text="Download" ImageUrl="~/App_Themes/DefaultTheme/Images/offline_dowload.png"   ToolTip="Click here to download the video"
                                                                                                                                                AlternateText="Download" CommandName="Download" 
                                                                                                                                                onclick="ExternalInterviewResponse_downloadVideoImageButton_Click"  />
                                                                                                                                        </td>
                                                                                                                                        <td valign="middle" align="right">
                                                                                                                                            <asp:Button ID="ExternalInterviewResponse_saveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                                                                                                                OnClick="ExternalInterviewResponse_saveButton_Click" />
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </ContentTemplate>
                                                                                            </asp:UpdatePanel>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_10" colspan="3">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3" valign="top">
                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                                                <tr id="CandiateInterviewResponse_otherCommentsTR" runat="server">
                                                                                                    <td align="left" class="Question_Rating_title">
                                                                                                        <asp:Literal ID="ExternalInterviewResponse_otherAssessorLiteral" runat="server" Text="Other Assessor Question Rating & Comments"
                                                                                                            SkinID="sknLabelRatingText"></asp:Literal>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <span id="CandiateInterviewResponse_otherCommentsUpSpan" runat="server" style="display: none;">
                                                                                                            <asp:Image ID="CandiateInterviewResponse_otherCommentsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                                                                                id="CandiateInterviewResponse_otherCommentsDownSpan" runat="server" style="display: block;"><asp:Image
                                                                                                                    ID="CandiateInterviewResponse_otherCommentsDownImage" runat="server" SkinID="sknMaximizeImage" /></span><asp:HiddenField
                                                                                                                        ID="CandiateInterviewResponse_restoreHiddenField" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_border_height_5">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td valign="top">
                                                                                                        <div id="ExternalInterviewResponse_otherCommentsDiv" runat="server" style="display: block;">
                                                                                                            <asp:UpdatePanel ID="ExternalInterviewResponse_otherAssessorUpdatePanel" runat="server">
                                                                                                                <ContentTemplate>
                                                                                                                    <div>
                                                                                                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                                                                            <asp:GridView ID="ExternalInterviewResponse_otherAssessorRatingCommentsGridView"
                                                                                                                                runat="server" AutoGenerateColumns="False" SkinID="sknNewGridView" 
                                                                                                                                ShowHeader="false" 
                                                                                                                                OnRowDataBound="ExternalInterviewResponse_otherAssessorRatingCommentsGridView_RowDataBound">
                                                                                                                                <Columns>
                                                                                                                                    <asp:TemplateField>
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                                                                                                <tr>
                                                                                                                                                    <td>
                                                                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                                            <tr>
                                                                                                                                                                <td align="left">
                                                                                                                                                                    <asp:Label ID="ExternalInterviewResponse_assessorNameLabel" runat="server" Text='<%# Eval("assessorName") %>'
                                                                                                                                                                        SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                                                                                </td>
                                                                                                                                                                <td align="right">
                                                                                                                                                                    <asp:Label ID="ExternalInterviewResponse_ratingPercentageLabel" runat="server" Text='<%# Eval("RatingPercent")%>'
                                                                                                                                                                        SkinID="sknLabelRatingFieldText"></asp:Label>
                                                                                                                                                                    <asp:HiddenField ID="ExternalInterviewResponse_ratingPercentageHiddenField" runat="server" Value='<%# Eval("RatingPercent")%>' />
                                                                                                                                                                </td>
                                                                                                                                                            </tr>
                                                                                                                                                        </table>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                                <tr>
                                                                                                                                                    <td>
                                                                                                                                                        <div>
                                                                                                                                                            <asp:Label ID="ExternalInterviewResponse_otherAssessorCommentsLabel" SkinID="sknLabelRatingCommentsFieldText"
                                                                                                                                                                runat="server" Text='<%# Eval("userComments") %>'></asp:Label>
                                                                                                                                                        </div>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                            </table>
                                                                                                                                        </ItemTemplate>
                                                                                                                                    </asp:TemplateField>
                                                                                                                                </Columns>
                                                                                                                                <HeaderStyle CssClass="grid_header" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                                                <AlternatingRowStyle CssClass="grid_001" BorderColor="#CBDAEA" BorderStyle="Solid"
                                                                                                                                    BorderWidth="1" />
                                                                                                                                <RowStyle CssClass="grid" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                                            </asp:GridView>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </ContentTemplate>
                                                                                                            </asp:UpdatePanel>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 4px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
