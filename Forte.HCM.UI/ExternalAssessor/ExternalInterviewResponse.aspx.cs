﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExternalInterviewResponse.aspx.cs
// File that represents the user interface layout and functionalities for
// the ExternalInterviewResponse page. 
// This will helps to view the response of interview questions


#endregion Header

#region Directives

using System;
using System.IO;
using System.Net;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.ExternalAssessor
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ExternalInterviewResponse page. This will helps to view the response of interview questions. 
    /// This class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ExternalInterviewResponse : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Protected Variables
        protected string videoURL;
        protected string questionID;
        #endregion Protected Variables

        #region Events Handlers
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ExternalInterviewResponse_successMessageLabel.Text = string.Empty;
                ExternalInterviewResponse_errorMessageLabel.Text = string.Empty;

                // Load the skill values.
                if (!IsPostBack)
                {
                    LoadValues();

                    
                    string serverURL = ConfigurationManager.AppSettings["VIDEO_STREAMING_SERVER_URL"].ToString();
                    videoURL = string.Concat(serverURL, Request.QueryString["candidatesessionid"], "_" + Request.QueryString["attemptid"]);
                    questionID = Request.QueryString["testquestionid"].ToString();

                    string folderName =Request.QueryString["candidatesessionid"] + "_" + Request.QueryString["attemptid"];
                    string fileName = Request.QueryString["testquestionid"].ToString();

                    /*ExternalInterviewResponse_downloadVideoImageButton.Attributes.Add("onclick",
                        "javascript:return DownloadVideo('" + folderName +"','" + fileName + "');");*/
                }
                ExpandRestore();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ExternalInterviewResponse_errorMessageLabel, exp.Message);
            }
        }
          /// <summary>
        /// Handler method that will be called when the save button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void ExternalInterviewResponse_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                SaveRatingComments();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalInterviewResponse_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler to rating values to percentage
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/>that holds the grid view row event data.
        /// </param>
        protected void ExternalInterviewResponse_otherAssessorRatingCommentsGridView_RowDataBound(object sender, 
            GridViewRowEventArgs e)
        {
            try
            {
                decimal ratingPercentage = 0;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    HiddenField ExternalInterviewResponse_ratingPercentageHiddenField = (HiddenField)e.Row.FindControl(
                        "ExternalInterviewResponse_ratingPercentageHiddenField");

                    Label ExternalInterviewResponse_ratingPercentageLabel = (Label)e.Row.FindControl(
                        "ExternalInterviewResponse_ratingPercentageLabel");

                    if (!string.IsNullOrEmpty(Convert.ToString(ExternalInterviewResponse_ratingPercentageHiddenField.Value)))
                        ratingPercentage = Convert.ToDecimal(ExternalInterviewResponse_ratingPercentageHiddenField.Value);


                    if (ratingPercentage <= 0)
                        ExternalInterviewResponse_ratingPercentageLabel.Visible = false;
                    else
                    {
                        ExternalInterviewResponse_ratingPercentageLabel.Visible = true;
                        ExternalInterviewResponse_ratingPercentageLabel.Text = string.Concat("Rating(", string.Format("{0}%", string.Format("{0:0.00}", ratingPercentage)), ")");
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalInterviewResponse_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will be called when download video image button is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/>that holds the event data.
        /// </param>
        protected void ExternalInterviewResponse_downloadVideoImageButton_Click(object sender,
            ImageClickEventArgs e)
        {
            try
            {
                string filePath = ConfigurationManager.AppSettings["OFFLINE_INTERVIEW_VIDEO_FILE"].ToString();
                string folderName = Request.QueryString["candidatesessionid"] + "_" + Request.QueryString["attemptid"];
                string fileName = Request.QueryString["testquestionid"].ToString() + ".flv";

                filePath += folderName + "//" + fileName;

                if (File.Exists(Server.MapPath(filePath)))
                {
                    ExternalInterviewResponse_downloadVideoHyperLink.NavigateUrl = "../Common/CandidateImageHandler.ashx?filepath=" + filePath + "&filetype=FLV";
                    string downloadScript = "<script>window.location.href = document.getElementById('ExternalInterviewResponse_downloadVideoHyperLink').href;</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "videodownload", downloadScript, false);
                }
                else
                {
                    string script = string.Format("alert('{0}');", "File not found");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "key_name", script, true);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalInterviewResponse_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden protected method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
            string candidateSessionID = null;
            string questionKey = string.Empty;
            string interviewKey = string.Empty;
            int assessorID = 0;

            // Check if candidate session ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]))
            {
                ShowMessage(ExternalInterviewResponse_errorMessageLabel,
                    "Candidate session ID is not present");
                return;
            }
            candidateSessionID = Request.QueryString["candidatesessionid"].Trim();

            // Check if attempt ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
            {
                ShowMessage(ExternalInterviewResponse_errorMessageLabel,
                    "Attempt ID is not present");
                return;
            }

            // Check if attempt ID is valid.
            int attemptID = 0;
            if (int.TryParse(Request.QueryString["attemptid"].Trim(), out attemptID) == false)
            {
                ShowMessage(ExternalInterviewResponse_errorMessageLabel,
                    "Attempt ID is invalid");
                return;
            }

            // Check if test question ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["testquestionid"]))
            {
                ShowMessage(ExternalInterviewResponse_errorMessageLabel,
                    "Test question ID is not present");
                return;
            }

            // Check if test question ID is valid.
            int testQuestionID = 0;
            if (int.TryParse(Request.QueryString["testquestionid"].Trim(), out testQuestionID) == false)
            {
                ShowMessage(ExternalInterviewResponse_errorMessageLabel,
                    "Test question ID is invalid");
                return;
            }

            // Check if interview key is given 
            if (Utility.IsNullOrEmpty(Request.QueryString["interviewKey"]))
            {
                ShowMessage(ExternalInterviewResponse_errorMessageLabel,
                    "Interview Key is not present");
                return;
            }
            interviewKey = Request.QueryString["interviewKey"].ToString();

            // Check if question key is given 
            if (Utility.IsNullOrEmpty(Request.QueryString["questionKey"]))
            {
                ShowMessage(ExternalInterviewResponse_errorMessageLabel,
                    "Question Key is not present");
                return;
            }
            questionKey = Request.QueryString["questionKey"].ToString();

            // Check if assessor ID is valid.
            if (int.TryParse(Request.QueryString["assessorid"].Trim(), out assessorID) == false)
            {
                ShowMessage(ExternalInterviewResponse_errorMessageLabel,
                    "Assessor ID is invalid");
                return;
            }

            ExternalInterviewResponse_saveButton.Enabled = true;
            

            // Get interview question attributes
            QuestionDetail questionAttributes = new InterviewReportBLManager().
                GetInterviewTestQuestionAttributesByKeys(interviewKey, questionKey);
                        
            if (questionAttributes != null)
            {
                //Set maximum rating star
                if (Convert.ToInt32(questionAttributes.Rating) > 0)
                    ExternalInterviewResponse_rating.MaxRating = Convert.ToInt32(questionAttributes.Rating);
                else
                    ExternalInterviewResponse_rating.MaxRating = 10;
                //Set question comments
                InterviewCandidateTestDetails_commentValueLabel.Text = questionAttributes.Comments == null ? questionAttributes.Comments : 
                    questionAttributes.Comments.ToString().ToString().Replace(Environment.NewLine, "<br />");
            }

            // Get interview response
            InterviewResponseDetail logDetail = new InterviewReportBLManager().
                GetInterviewResponseDetail(candidateSessionID, attemptID, testQuestionID, assessorID);

            if (logDetail == null || logDetail.Question==null)
            {
                ShowMessage(ExternalInterviewResponse_errorMessageLabel,
                    "No data found to display");

                return;
            }
            // Assign details.
            ExternalInterviewResponse_questionValueLabel.Text = logDetail.Question == null ? logDetail.Question :
                logDetail.Question.ToString().ToString().Replace(Environment.NewLine, "<br />");
            if (logDetail.HasImage)
            {
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = logDetail.QuestionImage;
                ExternalInterviewResponse_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=INTERVIEW_QUESTION_IMAGE&questionKey=" + logDetail.QuestionKey;
                ExternalInterviewResponse_questionImageDiv.Style["display"] = "block";
            }
            else
            {
                ExternalInterviewResponse_questionImageDiv.Style["display"] = "none";
            }
            
            if(!Utility.IsNullOrEmpty(logDetail.ChoiceDesc))
            {
                string choiceDesc = logDetail.ChoiceDesc == null ? logDetail.ChoiceDesc : logDetail.ChoiceDesc.ToString().ToString().Replace("&#x0D;", "<br />");
                CandidateResponse_questionAnswerLabel.Text = choiceDesc == null ? choiceDesc : choiceDesc.ToString().ToString().Replace(Environment.NewLine, "<br />");
            }

            if (!Utility.IsNullOrEmpty(logDetail.CandidateComments))
            {
                string candidateComments =  logDetail.CandidateComments == null ? logDetail.CandidateComments : logDetail.CandidateComments.ToString().ToString().Replace(Environment.NewLine, "<br />");
                ExternalInterviewResponse_candidateCommentsValueLabel.Text = candidateComments;
            }
            ExternalInterviewResponse_weightageValueLabel.Text = logDetail.Weightage.ToString();
            ExternalInterviewResponse_categoryValueLabel.Text = logDetail.CategoryName;
            ExternalInterviewResponse_subjectValueLabel.Text = logDetail.SubjectName;
            ExternalInterviewResponse_skippedValueLabel.Text = logDetail.Skipped ? "Yes" : "No";
            ExternalInterviewResponse_complexityValueLabel.Text = logDetail.Complexity;
            ExternalInterviewResponse_testAreaValueLabel.Text = logDetail.TestArea;

            
            if (logDetail.RatingComments == null)
            {
                ShowMessage(ExternalInterviewResponse_errorMessageLabel, "");
                return;
            }
            for (int i = 0; i < logDetail.RatingComments.Count; i++)
            {
                if (logDetail.RatingComments[i].assessorID == assessorID)
                {
                    if (logDetail.RatingComments[i].CandidateSessionId == candidateSessionID)
                        ExternalInterviewResponse_rating.CurrentRating = Convert.ToInt32(logDetail.RatingComments[i].Rating);

                    InterviewCandidateTestDetails_userCommentsTextBox.Text = logDetail.RatingComments[i].userComments;
                    logDetail.RatingComments.RemoveAt(i);
                }
            }
            
            ExternalInterviewResponse_otherAssessorRatingCommentsGridView.DataSource = logDetail.RatingComments;
            ExternalInterviewResponse_otherAssessorRatingCommentsGridView.DataBind();
        }

        #endregion Protected Overridden Methods

        #region Private Methods

        /// <summary>
        /// Method that checks if the given remote file exists or not.
        /// </summary>
        /// <param name="url">
        /// A <see cref="string"/> that holds the remote file URL.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the exist status. True if exists
        /// and false otherwise.
        /// </returns>
        private bool IsRemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";

                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                //Returns TURE if the Status code == 200
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                // Any exception will returns false.
                return false;
            }
        }

        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>
        private void ExpandRestore()
        {
            if (CandiateInterviewResponse_restoreHiddenField.Value == "Y")
            {
                ExternalInterviewResponse_otherCommentsDiv.Style["display"] = "block";
                CandiateInterviewResponse_otherCommentsUpSpan.Style["display"] = "none";
                CandiateInterviewResponse_otherCommentsDownSpan.Style["display"] = "block";
                CandiateInterviewResponse_ratingDiv.Style["height"] = RESTORED_HEIGHT;
            }
            else
            {
                ExternalInterviewResponse_otherCommentsDiv.Style["display"] = "none";
                CandiateInterviewResponse_otherCommentsUpSpan.Style["display"] = "block";
                CandiateInterviewResponse_otherCommentsDownSpan.Style["display"] = "none";
                CandiateInterviewResponse_ratingDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            CandiateInterviewResponse_otherCommentsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                CandiateInterviewResponse_ratingDiv.ClientID + "','" +
                ExternalInterviewResponse_otherCommentsDiv.ClientID + "','" +
                CandiateInterviewResponse_otherCommentsUpSpan.ClientID + "','" +
                CandiateInterviewResponse_otherCommentsDownSpan.ClientID + "','" +
                CandiateInterviewResponse_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
        }

        /// <summary>
        /// Method to save the rating comments
        /// </summary>
        private void SaveRatingComments()
        {
            QuestionDetail questionDetails = new QuestionDetail();

            questionDetails.Comments = InterviewCandidateTestDetails_userCommentsTextBox.Text.Trim();
            questionDetails.Rating = Convert.ToDecimal(ExternalInterviewResponse_rating.CurrentRating.ToString());
            questionDetails.QuestionKey = Request.QueryString.Get("questionKey").ToString();

            new InterviewReportBLManager().SaveQuestionDetailsWithRatingComments(questionDetails, Request.QueryString.Get("candidatesessionid").ToString(),
                Convert.ToInt32(Request.QueryString.Get("attemptid").ToString()),
                Convert.ToInt32(Request.QueryString.Get("assessorid").ToString()), Convert.ToInt32(Request.QueryString.Get("assessorid").ToString()));

            base.ShowMessage(ExternalInterviewResponse_successMessageLabel, "Comments and rating saved successfully");
        }
        #endregion Private Methods

    }
}