﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OverviewMaster.Master"
    CodeBehind="ExternalAssessor.aspx.cs" Inherits="Forte.HCM.UI.ExternalAssessor.ExternalAssessor" %>

<%@ MasterType VirtualPath="~/MasterPages/OverviewMaster.Master" %>
<asp:Content ID="ExternalAssessor_content" ContentPlaceHolderID="OverviewMaster_body"
    runat="server">
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="http://dev.jquery.com/view/trunk/plugins/validate/jquery.validate.js"></script>
    <style>
    .right {
        float: right;
    }
    .scrolldown { padding-left:20px; color:#EDECE8; font-size:40px; font-weight:bold; vertical-align:middle;
	    text-shadow: #6374AB 2px 2px 2px;
     }
     .contentblock { margin: 0px 20px; }
     .results { border: 1px solid blue; padding:20px; margin-top:20px; min-height:50px; }
     .blue-bold { font-size:18px; font-weight:bold; color:blue; }

    .external_form {
        background: url(images/pp_workflow_panel_bg.png);
        repeat-x scroll left bottom #e9ecee;
        border: 1px solid #DFDCDC;
        border-radius: 15px 15px 15px 15px;
        display: inline-block;
        margin-bottom: 30px;
        margin-top: 10px;
        margin-left:20px;
        padding: 25px 50px;
    }
</style>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="td_height_10">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="ExternalAssessor_topMessageUpdatePanel" runat="server">
                    <contenttemplate>
                        <asp:Label ID="ExternalAssessor_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage">
                        </asp:Label>
                        <asp:Label ID="ExternalAssessor_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </contenttemplate>
                    <triggers>
                        <asp:AsyncPostBackTrigger ControlID="ExternalAssessor_continueButton" />
                    </triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="ExternalAssessor_registrationUpdatePanel" runat="server">
                    <contenttemplate>
                        <div>
                            <div class="external_form" style="float:left">
                                <div style="float:left">
                                <div style="float: left">
                                    <asp:Label ID="ExternalAssessor_titleLabel" class="external_assessor_title" runat="server"
                                        Text="Your information here !"></asp:Label>
                                </div>
                                <div style="clear:both;float:left">
                                    <hr style="height:1px;width:343px; border-left: 1px solid #ffffff !important;border-right: 1px solid #ffffff !important;border-bottom: 1px solid #ffffff !important;" />
                                </div>
                                <div style="height: 50px">
                                </div>
                                <div>
                                    <div style="float: left; width: 100px;">
                                        <div style="text-align: left">
                                            <asp:Label ID="ExternalAssessor_emailLabel" runat="server" Text="Email" SkinID="sknLabelText"></asp:Label>&nbsp;<span
                                                class="mandatory">*</span></div>
                                    </div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="ExternalAssessor_emailTextBox" runat="server" MaxLength="50" 
                                            AutoCompleteType="None" SkinID="sknExternalAssessorTextBox"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="height: 50px">
                                </div>
                                <div>
                                    <div style="float: left; width: 100px;">
                                        <div style="text-align: left">
                                            <asp:Label ID="ExternalAssessor_contactNumberLabel" runat="server" Text="Contact Number"
                                                SkinID="sknLabelText"></asp:Label>&nbsp;<span class="mandatory">*</span></div>
                                    </div>
                                    <div style="float: left">
                                        <asp:TextBox ID="ExternalAssessor_contactNumberTextBox" runat="server" MaxLength="15"
                                            SkinID="sknExternalAssessorTextBox"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="height: 50px">
                                </div>
                                <div>
                                    <div style="float: left; width: 100px;">
                                        <div style="text-align: left">
                                            <asp:Label ID="ExternalAssessor_firstNameLabel" runat="server" Text="First Name"
                                                SkinID="sknLabelText"></asp:Label>&nbsp;<span class="mandatory">*</span></div>
                                    </div>
                                    <div style="float: left">
                                        <asp:TextBox ID="ExternalAssessor_firstNameTextBox" runat="server" MaxLength="50"
                                            SkinID="sknExternalAssessorTextBox"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="height: 50px">
                                </div>
                                <div>
                                    <div style="float: left; width: 100px;">
                                        <div style="text-align: left">
                                            <asp:Label ID="ExternalAssessor_lastNameLabel" runat="server" Text="Last Name" SkinID="sknLabelText"></asp:Label>&nbsp;<span
                                                class="mandatory">*</span></div>
                                    </div>
                                    <div style="float: left">
                                        <asp:TextBox ID="ExternalAssessor_lastNameTextBox" runat="server" MaxLength="50"
                                            SkinID="sknExternalAssessorTextBox"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="height: 50px">
                                </div>
                                <div>
                                    <div style="float: left;"></div>
                                    <div style="float: left;padding-left:100px">
                                        <asp:HiddenField ID="ExternalAssessor_candidateInterviewSessionKeyHiddenField" runat="server" />
                                        <asp:HiddenField ID="ExternalAssessor_attempIDHiddenField" runat="server" />
                                        <asp:HiddenField ID="ExternalAssessor_externalAssessorIDHiddenField" runat="server" />
                                        <asp:Button ID="ExternalAssessor_continueButton" runat="server" Text="Continue" SkinID="sknButtonId"
                                            OnClick="ExternalAssessor_continueButton_Click" />
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="external_form" style="float:left">
                                <div style="float:left">
                                <div style="float: left">
                                    <asp:Label ID="ExternalAssessor_loginLabel" class="external_assessor_title" runat="server"
                                        Text="Please sign in here, if you have an account with us"></asp:Label>
                                </div>
                                <div style="clear:both;float:left">
                                    <hr style="height:1px;width:343px; border-left: 1px solid #ffffff !important;border-right: 1px solid #ffffff !important;border-bottom: 1px solid #ffffff !important;" />
                                </div>
                                <div style="height: 50px">
                                </div>
                                <div>
                                    <div style="float: left; width: 100px;">
                                        <div style="text-align: left">
                                            <asp:Label ID="ExternalAssessor_userNameLabel" runat="server" Text="User Name" SkinID="sknLabelText"></asp:Label>&nbsp;<span
                                                class="mandatory">*</span></div>
                                    </div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="ExternalAssessor_userNameTextBox" runat="server" MaxLength="50" 
                                            AutoCompleteType="None" SkinID="sknExternalAssessorTextBox"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="height: 50px">
                                </div>
                                <div>
                                    <div style="float: left; width: 100px;">
                                        <div style="text-align: left">
                                            <asp:Label ID="ExternalAssessor_passwordLabel" runat="server" Text="Password"
                                                SkinID="sknLabelText"></asp:Label>&nbsp;<span class="mandatory">*</span></div>
                                    </div>
                                    <div style="float: left">
                                        <asp:TextBox ID="ExternalAssessor_passwordTextBox" runat="server" MaxLength="50"
                                            SkinID="sknExternalAssessorTextBox" TextMode="Password"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="height: 50px">
                                </div>
                                
                                <div>
                                    <div style="float: left;"></div>
                                    <div style="float: left;padding-left:100px">
                                        <asp:Button ID="ExternalAssessor_loginButton" runat="server" Text="Login" 
                                            SkinID="sknButtonId" onclick="ExternalAssessor_loginButton_Click" />
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </contenttemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
