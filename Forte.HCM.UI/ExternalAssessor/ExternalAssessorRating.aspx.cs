#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExternalAssessorRating.aspx.cs
// File that represents the user interface layout and functionalities
// for the PreviewAssessmentDetails page. This page helps in viewing 
// the candidate interview videos and provide rating. Also helps in 
// viewing other assessor rating and consolidated rating. 

#endregion Header

#region Directives

using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.ExternalAssessor
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the ExternalAssessorRating.aspx page. This page helps in viewing 
    /// the candidate interview videos and provide rating. Also helps in 
    /// viewing other assessor rating and consolidated rating. This class
    /// inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class ExternalAssessorRating : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "130px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "105px";

        /// <summary>
        /// /// A <see cref="string"/> constant that holds the restored height of otehr assessor score all score
        /// </summary>
        public const string OVERALLSCORE_RESTORED_HEIGHT = "130px";

        /// <summary>
        /// /// /// A <see cref="string"/> constant that holds the expanded height of otehr assessor score all score
        /// </summary>
        public const string OVERALLSCORE_EXPANDED_HEIGHT = "105px";

        #endregion Private Constants

        #region Protected Variables

        /// <summary>
        /// Holding video url
        /// </summary>
        protected string videoURL;
        /// <summary>
        /// Holding question id
        /// </summary>
        protected string questionID;
        
        #endregion Protected Variables

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("External Assessment Rating");
                ExternalAssessorRating_topErrorMessageLabel.Text = string.Empty;
                ExternalAssessorRating_topSuccessMessageLabel.Text = string.Empty;

                if (!IsPostBack)
                {
                    string externalKey = Request.QueryString["a"];
                    if (Utility.IsNullOrEmpty(externalKey))
                    {
                        ExternalAssessorRating_candidatePhotoImage.ImageUrl = "~/CandidatePhotos/photo_not_available.jpg";
                        ExternalAssessorRating_topErrorMessageLabel.Text = "No data found to display";
                        return;
                    }

                    ViewState["INTERVIEW_KEY"] = null;
                    ViewState["CANDIDATE_SESSION_KEY"] = null;
                    ViewState["ATTEMPT_ID"] = null;
                    ViewState["ASSESSOR_ID"] = null;
                    ViewState["SESSION_KEY"] = null;

                    ExternalAssessorDetail externalAssesorSessionDetail = null;
                    externalAssesorSessionDetail = new InterviewSessionBLManager().
                        GetExternalAssessorDetailByKey(externalKey);

                    if (externalAssesorSessionDetail == null)
                    {
                        ExternalAssessorRating_candidatePhotoImage.ImageUrl = "~/CandidatePhotos/photo_not_available.jpg";
                        ExternalAssessorRating_topErrorMessageLabel.Text = "Key is invalid";
                        return;                        
                    }

                    ViewState["INTERVIEW_KEY"] =externalAssesorSessionDetail.InterviewKey;
                    ViewState["CANDIDATE_SESSION_KEY"] = externalAssesorSessionDetail.CandidateInterviewSessionkey;
                    ViewState["ATTEMPT_ID"] = externalAssesorSessionDetail.AttemptID;
                    ViewState["ASSESSOR_ID"] = externalAssesorSessionDetail.ExternalAssessorID;
                    ViewState["SESSION_KEY"] = externalAssesorSessionDetail.InterviewSessionKey;
                    
                    LoadValues();
                }
                ExpandRestoreOtherAssessorSubjectScore();
                ExpandRestoreOtherOverallScore();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the question list.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void ExternalAssessorRating_questionAreaDataList_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                // Find the row type is DataRow
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    object dr = e.Row.DataItem;

                    decimal _rating = ((Forte.HCM.DataObjects.QuestionDetail)(dr)).Rating;
                    int _questionID = ((Forte.HCM.DataObjects.QuestionDetail)(dr)).QuestionID;

                    if (((Forte.HCM.DataObjects.QuestionDetail)(dr)).RatingEdit == "Y")
                    {
                        e.Row.CssClass = "RatingEditable";
                        if (_rating > 0)
                            e.Row.CssClass = "RatingFinished";
                    }
                    else
                    {
                        e.Row.CssClass = "RatingNotAllowed";
                    }
                    
                    // Find a label from the current row.
                    Label ExternalAssessorRating_rowNoLabel = (Label)e.Row.FindControl("ExternalAssessorRating_rowNoLabel");

                    // Assign the row no to the label.
                    ExternalAssessorRating_rowNoLabel.Text = (e.Row.RowIndex + 1).ToString();

                    LinkButton ExternalAssessorRating_questionAreaQuestionsLabel =
                        (LinkButton)e.Row.FindControl("ExternalAssessorRating_questionAreaQuestionsLabel");

                    HiddenField ExternalAssessorRating_questionAreaQuestionIDHiddenField =
                        (HiddenField)e.Row.FindControl("ExternalAssessorRating_questionAreaQuestionIDHiddenField");

                    HiddenField ExternalAssessorRating_questionAreaQuestionKeyHiddenField =
                        (HiddenField)e.Row.FindControl("ExternalAssessorRating_questionAreaQuestionKeyHiddenField");


                    if (((Forte.HCM.DataObjects.QuestionDetail)(dr)).RatingEdit != "Y")
                    {
                        ExternalAssessorRating_questionAreaQuestionsLabel.Style.Add("cursor", "default");
                        ExternalAssessorRating_questionAreaQuestionsLabel.Enabled = false;
                    }
                    
                    Label ExternalAssessorRating_questionAreaRatingLabel = (Label)e.Row.FindControl("ExternalAssessorRating_questionAreaRatingLabel");

                    if (!Utility.IsNullOrEmpty(ExternalAssessorRating_questionAreaRatingLabel.Text))
                    {
                        int questRating = 0;
                        questRating = (int)Math.Ceiling(Convert.ToDouble(ExternalAssessorRating_questionAreaRatingLabel.Text.ToString()));
                        if(questRating < 0)
                            ExternalAssessorRating_questionAreaRatingLabel.Text = "N/R";    
                        else
                            ExternalAssessorRating_questionAreaRatingLabel.Text = questRating.ToString();
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will be called when row data is bound at grid
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void ExternalAssessorRating_othersOverAllScore_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                decimal _overAllWeightageScore = 0;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField ExternalAssessorRating_overallAssessorWeightedGridviewHiddenField = (HiddenField)e.Row.FindControl(
                        "ExternalAssessorRating_overallAssessorWeightedGridviewHiddenField");

                    Label ExternalAssessorRating_overallAssessorWeightedGridviewLabel = (Label)e.Row.FindControl(
                        "ExternalAssessorRating_overallAssessorWeightedGridviewLabel");


                    if (!string.IsNullOrEmpty(Convert.ToString(ExternalAssessorRating_overallAssessorWeightedGridviewHiddenField.Value)))
                        _overAllWeightageScore =
                            Convert.ToDecimal(ExternalAssessorRating_overallAssessorWeightedGridviewHiddenField.Value);

                    if (_overAllWeightageScore < 0)
                        ExternalAssessorRating_overallAssessorWeightedGridviewLabel.Visible = false;
                    else
                    {
                        ExternalAssessorRating_overallAssessorWeightedGridviewLabel.Visible = true;
                        ExternalAssessorRating_overallAssessorWeightedGridviewLabel.Text =
                            string.Format("{0}%", String.Format("{0:0.00}", _overAllWeightageScore));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Hanlder that will be called assessor subject row data bound
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void ExternalAssessorRating_assessorSubjectRatingGridview_RowDataBound(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                decimal _assessorWeightageScore = 0;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField ExternalAssessorRating_assessorWeightageScoreGridviewHiddenField = (HiddenField)e.Row.FindControl(
                        "ExternalAssessorRating_assessorWeightageScoreGridviewHiddenField");

                    Label ExternalAssessorRating_assessorWeightageScoreGridviewLabel = (Label)e.Row.FindControl(
                        "ExternalAssessorRating_assessorWeightageScoreGridviewLabel");

                    if (!string.IsNullOrEmpty(Convert.ToString(ExternalAssessorRating_assessorWeightageScoreGridviewHiddenField.Value)))
                        _assessorWeightageScore =
                            Convert.ToDecimal(ExternalAssessorRating_assessorWeightageScoreGridviewHiddenField.Value);

                    if (_assessorWeightageScore < 0)
                        ExternalAssessorRating_assessorWeightageScoreGridviewLabel.Visible = false;
                    else
                    {
                        ExternalAssessorRating_assessorWeightageScoreGridviewLabel.Visible = true;
                        ExternalAssessorRating_assessorWeightageScoreGridviewLabel.Text = 
                            string.Format("{0}%", String.Format("{0:0.00}", _assessorWeightageScore));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler to format the other assessor subject rating
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void ExternalAssessorRating_otherAssessorSubjectRatingGridview_RowDataBound(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                decimal _otherAssessorWeightageScore = 0;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    HiddenField ExternalAssessorRating_otherAssessorWeightageScoreGridviewHiddenField = (HiddenField)e.Row.FindControl(
                        "ExternalAssessorRating_otherAssessorWeightageScoreGridviewHiddenField");

                    Label ExternalAssessorRating_otherAssessorWeightageScoreGridviewLabel = (Label)e.Row.FindControl(
                        "ExternalAssessorRating_otherAssessorWeightageScoreGridviewLabel");

                    if (!string.IsNullOrEmpty(Convert.ToString(ExternalAssessorRating_otherAssessorWeightageScoreGridviewHiddenField.Value)))
                        _otherAssessorWeightageScore =
                            Convert.ToDecimal(ExternalAssessorRating_otherAssessorWeightageScoreGridviewHiddenField.Value);

                    if (_otherAssessorWeightageScore < 0)
                        ExternalAssessorRating_otherAssessorWeightageScoreGridviewLabel.Visible = false;
                    else
                    {
                        ExternalAssessorRating_otherAssessorWeightageScoreGridviewLabel.Visible = true;
                        ExternalAssessorRating_otherAssessorWeightageScoreGridviewLabel.Text = 
                            string.Format("{0}%", String.Format("{0:0.00}", _otherAssessorWeightageScore));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the question link button is 
        /// clicked in the question list.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ExternalAssessorRating_questionAreaQuestionsLabel_Click
            (object sender, EventArgs e)
        {
            try
            {
                LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the save button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ExternalAssessorRating_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                AssessmentSummary assessmentSummary = new AssessmentSummary();
                assessmentSummary.Comments = ExternalAssessorRating_commentsTextbox.Text;
                assessmentSummary.CreatedBy = Convert.ToInt32(ViewState["ASSESSOR_ID"].ToString());
                assessmentSummary.AssessmentStatus = "ASS_INPRG";
                new AssessmentSummaryBLManager().SaveCandidateAssessmentRatngComments(assessmentSummary,
                    Convert.ToInt32(ViewState["ASSESSOR_ID"].ToString()), ViewState["CANDIDATE_SESSION_KEY"].ToString(),
                    Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString()));

                base.ShowMessage(ExternalAssessorRating_topSuccessMessageLabel, "Candidate status saved successfully");
                LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the submit button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ExternalAssessorRating_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                AssessmentSummary assessmentSummary = new AssessmentSummary();
                assessmentSummary.Comments = ExternalAssessorRating_commentsTextbox.Text;
                assessmentSummary.CreatedBy = Convert.ToInt32(ViewState["ASSESSOR_ID"].ToString());
                assessmentSummary.AssessmentStatus = "ASS_COMP";
                new AssessmentSummaryBLManager().SaveCandidateAssessmentRatngComments(assessmentSummary,
                    Convert.ToInt32(ViewState["ASSESSOR_ID"].ToString()),
                    ViewState["CANDIDATE_SESSION_KEY"].ToString(),
                    Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString()));

                //Update Candidate Interview Score
                DataSet overallCandidateRatingDataset = new AssessmentSummaryBLManager().
                GetRatingSummary(ViewState["CANDIDATE_SESSION_KEY"].ToString(), Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString()));

                decimal overAllSubjectScore = 0;
                decimal overAllSubjectWeightageScore = 0;

                if (overallCandidateRatingDataset.Tables[5].Rows.Count != 0)
                    if (!string.IsNullOrEmpty(Convert.ToString(overallCandidateRatingDataset.Tables[5].Rows[0]["OVERALL_SCORE"])))
                        overAllSubjectScore = Convert.ToDecimal(overallCandidateRatingDataset.Tables[5].Rows[0]["OVERALL_SCORE"].ToString());

                if (overallCandidateRatingDataset.Tables[6].Rows.Count != 0)
                    if (!string.IsNullOrEmpty(Convert.ToString(overallCandidateRatingDataset.Tables[6].Rows[0]["OVERALL_WEIGHTAGE_SCORE"])))
                        overAllSubjectWeightageScore =
                            Convert.ToDecimal(overallCandidateRatingDataset.Tables[6].Rows[0]["OVERALL_WEIGHTAGE_SCORE"].ToString());

                new AssessmentSummaryBLManager().UpdateCanidateInterviewScore(ViewState["INTERVIEW_KEY"].ToString(),
                    ViewState["CANDIDATE_SESSION_KEY"].ToString(), Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString()),
                    overAllSubjectScore, overAllSubjectWeightageScore, Convert.ToInt32(ViewState["ASSESSOR_ID"].ToString()));

                LoadValues();
                base.ShowMessage(ExternalAssessorRating_topSuccessMessageLabel, "Candidate status completed successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when question group by drop down list
        /// item is seleted.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ExternalAssessorRating_questiongroupByDropDownList_SelectedIndexChanged
            (object sender, EventArgs e)
        {
            try
            {
                if (ExternalAssessorRating_questiongroupByDropDownList.SelectedValue != "--Select--")
                {
                    BindAssessmentSummary(ViewState["CANDIDATE_SESSION_KEY"].ToString(),
                        Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString()),
                        Convert.ToInt32(ViewState["ASSESSOR_ID"].ToString()),
                        Convert.ToChar(ExternalAssessorRating_questiongroupByDropDownList.SelectedValue));
                }
                else
                {
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the item data bound event is 
        /// fired in the assessor details list.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="DataListItemEventArgs"/> that holds the event data.
        /// </param>
        protected void ExternalAssessorRating_assessorDetailsDatalist_ItemDataBound
            (object sender, DataListItemEventArgs e)
        {
            try
            {
                HiddenField ExternalAssessorRating_assessorDetailsRatingHiddenField = (HiddenField)e.Item.FindControl
                    ("ExternalAssessorRating_assessorDetailsRatingHiddenField");

                Image ExternalAssessorRating_assessorDetailsRatingImage = (Image)e.Item.FindControl
                    ("ExternalAssessorRating_assessorDetailsRatingImage");

                Label ExternalAssessorRating_assessorDetailsRatingLabel = (Label)e.Item.FindControl
                    ("ExternalAssessorRating_assessorDetailsRatingLabel");

                if (ExternalAssessorRating_assessorDetailsRatingHiddenField.Value == "0")
                {
                    ExternalAssessorRating_assessorDetailsRatingImage.Visible = false;
                    ExternalAssessorRating_assessorDetailsRatingLabel.Text = "N/R";
                }
                else
                {
                    if (string.IsNullOrEmpty(base.GetSummaryRatingImage(
                        Convert.ToDecimal(ExternalAssessorRating_assessorDetailsRatingHiddenField.Value))))
                        ExternalAssessorRating_assessorDetailsRatingImage.Visible = false;
                    else
                    {
                        ExternalAssessorRating_assessorDetailsRatingImage.Visible = true;
                        ExternalAssessorRating_assessorDetailsRatingImage.ImageUrl = base.GetSummaryRatingImage(
                            Convert.ToDecimal(ExternalAssessorRating_assessorDetailsRatingHiddenField.Value));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event to handle row command events
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        protected void ExternalAssessorRating_questionAreaDataList_RowCommand(object sender, 
            GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "QuestionDetail")
                {
                    LinkButton lnk = (LinkButton)e.CommandSource;
                    HiddenField ExternalAssessorRating_questionAreaQuestionIDHiddenField =
                        (HiddenField)lnk.Parent.FindControl("ExternalAssessorRating_questionAreaQuestionIDHiddenField");
                    int testquestionID = Convert.ToInt32(ExternalAssessorRating_questionAreaQuestionIDHiddenField.Value);

                    HiddenField ExternalAssessorRating_questionAreaQuestionKeyHiddenField = 
                        (HiddenField)lnk.Parent.FindControl("ExternalAssessorRating_questionAreaQuestionKeyHiddenField");
                    string questionKey = ExternalAssessorRating_questionAreaQuestionKeyHiddenField.Value;

                    HiddenField ExternalAssessorRating_questionRatingEditHiddenField = 
                        (HiddenField)lnk.Parent.FindControl("ExternalAssessorRating_questionRatingEditHiddenField");
                    string ratingEdit = ExternalAssessorRating_questionRatingEditHiddenField.Value;

                    LoadCandidateResponse(testquestionID, questionKey, ratingEdit);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Refresh the page details
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ExternalAssessorRating_refreshButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method to show the publish url popup
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        protected void ExternalAssessorRating_publishInterviewImageButton_Click(object sender,
            ImageClickEventArgs e)
        {
            try
            {
                InterviewScoreParamDetail interviewScoreParamDetail = null;
                if (interviewScoreParamDetail == null)
                    interviewScoreParamDetail = new InterviewScoreParamDetail();

                int attemptID = 0;
                if (!Utility.IsNullOrEmpty(ViewState["ATTEMPT_ID"].ToString()))
                    attemptID = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());

                interviewScoreParamDetail.CandidateInterviewSessionKey = ViewState["CANDIDATE_SESSION_KEY"].ToString();
                interviewScoreParamDetail.AttemptID = attemptID;
                interviewScoreParamDetail.CandidateName = ExternalAssessorRating_candidateNameLabelValue.Text.ToString();
                ExternalAssessorRating_emailConfirmation_ConfirmMsgControl.InterviewParams = interviewScoreParamDetail;
                ExternalAssessorRating_emailConfirmation_ModalPopupExtender.Show();
                ExternalAssessorRating_emailConfirmation_ConfirmMsgControl.ShowScoreUrl();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }
        #endregion Event Handlers

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindAssessmentSummary(ViewState["CANDIDATE_SESSION_KEY"].ToString(), Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString()),
                Convert.ToInt32(ViewState["ASSESSOR_ID"].ToString()), 'Q');
        }

        #endregion Protected Overridden Methods

        #region Private Methods

        /// <summary>
        /// Method that binds the assessment summary.
        /// </summary>
        /// <param name="candidateInterviewKey">
        /// A <see cref="string"/> that holds the candidate interview key.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="char"/> that holds the sort order.
        /// </param>
        private void BindAssessmentSummary(string candidateInterviewKey,
            int attemptID, int assessorID, char orderBy)
        {
            AssessmentSummary assessmentSummary = new AssessmentSummary();
            assessmentSummary =
                new AssessmentSummaryBLManager().GetAssessorSummary(candidateInterviewKey, attemptID, assessorID, orderBy);

            LoadCandidateDetails(assessmentSummary);

            DataSet overallRatingDataset = new AssessmentSummaryBLManager().
               GetRatingSummary(ViewState["CANDIDATE_SESSION_KEY"].ToString(),
               Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString()));

            if (overallRatingDataset.Tables[0].Rows.Count == 0)
            {
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, "");
            }
            else
            {
                decimal assessorWeightedScore = 0;

                DataView dvOverallWeightedFilterByAssessor = new DataView(overallRatingDataset.Tables[0]);
                dvOverallWeightedFilterByAssessor.RowFilter = "ASSESSOR_ID = " + assessorID;
                DataTable dtOverallWeightedAssessorScore = dvOverallWeightedFilterByAssessor.ToTable();
                dtOverallWeightedAssessorScore.AcceptChanges();

                if (dtOverallWeightedAssessorScore.Rows.Count > 0)
                {
                    assessorWeightedScore = Convert.ToDecimal(dtOverallWeightedAssessorScore.Rows[0]["OVERALL_ASSESSOR_WEIGHTED"].ToString());
                    ExternalAssessorRating_assessorOverallWeightageScoreValueLabel.Text = 
                        string.Format("{0}%", String.Format("{0:0.00}", assessorWeightedScore));
                }
            }

            decimal _overAllScore = 0;
            decimal _overWeightageScore = 0;

            if (overallRatingDataset.Tables[5].Rows.Count != 0)
                if (!string.IsNullOrEmpty(Convert.ToString(overallRatingDataset.Tables[5].Rows[0]["OVERALL_SCORE"])))
                    _overAllScore = Convert.ToDecimal(overallRatingDataset.Tables[5].Rows[0]["OVERALL_SCORE"].ToString());


            if (overallRatingDataset.Tables[6].Rows.Count != 0)
                if (!string.IsNullOrEmpty(Convert.ToString(overallRatingDataset.Tables[6].Rows[0]["OVERALL_WEIGHTAGE_SCORE"])))
                    _overWeightageScore =
                        Convert.ToDecimal(overallRatingDataset.Tables[6].Rows[0]["OVERALL_WEIGHTAGE_SCORE"].ToString());

            ExternalAssessorRating_overallWeightageScoreLabelValue.Text = string.Format("{0}%", String.Format("{0:0.00}", _overWeightageScore));

           
            DataTable dtOtherAssessorSubjectScore = new DataTable();
            //Assign subjec score
            if (overallRatingDataset.Tables[1].Rows.Count == 0)
            {
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, "");
            }
            else
            {
                //Set current assessor subject score                
                DataView dvFilterByAssessor = new DataView(overallRatingDataset.Tables[1]);
                dvFilterByAssessor.RowFilter = "ASSESSOR = " + assessorID;
                DataTable dtAssessorSubjectScore = dvFilterByAssessor.ToTable();
                dtAssessorSubjectScore.AcceptChanges();

                ExternalAssessorRating_assessorSubjectRatingGridview.DataSource = dtAssessorSubjectScore;
                ExternalAssessorRating_assessorSubjectRatingGridview.DataBind();

                DataView dvFilterByOtherAssessor = new DataView(overallRatingDataset.Tables[1]);
                dvFilterByOtherAssessor.RowFilter = "ASSESSOR <> " + assessorID;
                dtOtherAssessorSubjectScore = dvFilterByOtherAssessor.ToTable();
                dtOtherAssessorSubjectScore.AcceptChanges();

                dtOtherAssessorSubjectScore.DefaultView.Sort = "CAT_SUB_ID ASC";

                //Set other assessor subject score 
                ExternalAssessorRating_otherAssessorSubjectRatingGridview.DataSource = dtOtherAssessorSubjectScore.DefaultView.ToTable();
                ExternalAssessorRating_otherAssessorSubjectRatingGridview.DataBind();
            }

            //Set other assesor overall score and comment
            if (overallRatingDataset.Tables[0].Rows.Count == 0)
            {
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, "");
            }
            else
            {
                DataView dvOtherAssessor = new DataView(overallRatingDataset.Tables[0]);
                dvOtherAssessor.RowFilter = "ASSESSOR_ID <> " + assessorID;
                DataTable dtOtherAssessor = dvOtherAssessor.ToTable();
                dtOtherAssessor.AcceptChanges();

                ExternalAssessorRating_othersOverAllScore.DataSource = dtOtherAssessor;
                ExternalAssessorRating_othersOverAllScore.DataBind();
            }

            if (assessmentSummary.QuestionDetails == null)
            {
                base.ShowMessage(ExternalAssessorRating_topErrorMessageLabel, "");
            }
            else
            {
                ExternalAssessorRating_questionAreaDataList.DataSource = assessmentSummary.QuestionDetails;
                ExternalAssessorRating_questionAreaDataList.DataBind();
            }
        }
        
        /// <summary>
        /// Method that loads the candidate details.
        /// </summary>
        /// <param name="assessmentSummary">
        /// A <see cref="AssessmentSummary"/> that holds the assessment summary.
        /// </param>
        private void LoadCandidateDetails(AssessmentSummary assessmentSummary)
        {
            //Get test completed date
            DateTime testCompletedDate = DateTime.Now;
            testCompletedDate = new ReportBLManager().GetInterviewTestCompletedDateByCandidate(ViewState["CANDIDATE_SESSION_KEY"].ToString(),
                Convert.ToInt32(ViewState["ASSESSOR_ID"].ToString()));

            string interviewName = new InterviewReportBLManager().
                GetInterviewTestName(ViewState["INTERVIEW_KEY"].ToString());
            ExternalAssessorRating_interviewNameLabel.Text = interviewName;

            if (!Utility.IsNullOrEmpty(testCompletedDate))
            {
                ExternalAssessorRating_testCompletedDateLabel.Text =
                  "Interview Completed on " + testCompletedDate.ToString("MMMM dd, yyyy");
            }
            ExternalAssessorRating_candidateNameLabelValue.Text = assessmentSummary.CanidateName;
            PublishAssessorRating_clientNameLabelValue.Text = assessmentSummary.ClientName;
            ExternalAssessorRating_positionProfileLabelValue.Text = assessmentSummary.PositionProfileName;

            if ((assessmentSummary.CompletedOn == Convert.ToDateTime("01/01/1901")) ||
                (assessmentSummary.CompletedOn == Convert.ToDateTime("1/1/0001 12:00:00 AM")) ||
                (Convert.ToString(assessmentSummary.CompletedOn) == string.Empty))
            {
                ////ExternalAssessorRating_completedDateTextBox.Text = string.Empty;
            }
            

            ExternalAssessorRating_commentsTextbox.Text = assessmentSummary.Comments;

            // Assign candidate image handler.
            ExternalAssessorRating_candidatePhotoImage.ImageUrl = "~/Common/CandidateImageHandler.ashx?source=VIEW_CAND&candidateid=" +
                assessmentSummary.CandidateInfoID;
        }

        /// <summary>
        /// Method to load question detail when question is loaded
        /// </summary>
        /// <param name="testQuestionID">
        /// A <see cref="int"/> that holds the interview question ID.
        /// </param>
        /// <param name="questionKey">
        /// A <see cref="string"/> that holds the interview question key.
        /// </param>
        /// <param name="ratingEdit">
        /// A <see cref="string"/> that holds the rading edit options.
        /// </param>
        private void LoadCandidateResponse(int testQuestionID, 
            string questionKey, string ratingEdit)
        {
            string candidateSessionID = null;
            string interviewKey = string.Empty;
            string iFrameSrc = string.Empty;
            string baseURL = ConfigurationManager.AppSettings["DEFAULT_URL"];

            // Check if candidate session ID is given.
            if (Utility.IsNullOrEmpty(ViewState["CANDIDATE_SESSION_KEY"].ToString()))
            {
                ShowMessage(ExternalAssessorRating_topErrorMessageLabel,
                    "Candidate session ID is not present");
                return;
            }
            candidateSessionID = ViewState["CANDIDATE_SESSION_KEY"].ToString().Trim();

            // Check if attempt ID is given.
            if (Utility.IsNullOrEmpty(ViewState["ATTEMPT_ID"].ToString()))
            {
                ShowMessage(ExternalAssessorRating_topErrorMessageLabel,
                    "Attempt ID is not present");
                return;
            }

            // Check if attempt ID is valid.
            int attemptID = 0;
            if (int.TryParse(ViewState["ATTEMPT_ID"].ToString().Trim(), out attemptID) == false)
            {
                ShowMessage(ExternalAssessorRating_topErrorMessageLabel,
                    "Attempt ID is invalid");
                return;
            }

            // Check if interview key is given 
            if (Utility.IsNullOrEmpty(ViewState["INTERVIEW_KEY"]))
            {
                ShowMessage(ExternalAssessorRating_topErrorMessageLabel,
                    "Interview Key is not present");
                return;
            }
            interviewKey = ViewState["INTERVIEW_KEY"].ToString();

            ExternalAssessorRating_saveButton.Enabled = true;
            AsssessorRating_candidateResponseIframe.Visible = true;

            if (ratingEdit.ToString().Trim().ToUpper() == "Y")
            {
                iFrameSrc = baseURL + "ExternalAssessor/ExternalInterviewResponse.aspx";
                iFrameSrc += "?interviewKey=" + interviewKey;
                iFrameSrc += "&candidatesessionid=" + candidateSessionID;
                iFrameSrc += "&attemptid=" + attemptID;
                iFrameSrc += "&testquestionid=" + testQuestionID;
                iFrameSrc += "&questionKey=" + questionKey;
                iFrameSrc += "&assessorid=" + ViewState["ASSESSOR_ID"].ToString();
                iFrameSrc += "&sessionKey=" + ViewState["SESSION_KEY"].ToString();

                AsssessorRating_candidateResponseIframe.Attributes["src"] = iFrameSrc;
            }
            else
                AsssessorRating_candidateResponseIframe.Attributes["src"] = null;
        }

        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>
        private void ExpandRestoreOtherAssessorSubjectScore()
        {
            if (ExternalAssessorRating_otherAssessorSubjectRatingRestoreHiddenField.Value == "Y")
            {
                ExternalAssessorRating_OtherAssessorSubjectScoreDiv.Style["display"] = "block";
                ExternalAssessorRating_otherAssessorSubjectRatingUpSpan.Style["display"] = "none";
                ExternalAssessorRating_otherAssessorSubjectRatingDownSpan.Style["display"] = "block";
                ExternalAssessorRating_AssessorSubjectScoreDiv.Style["height"] = RESTORED_HEIGHT;
            }
            else
            {
                ExternalAssessorRating_OtherAssessorSubjectScoreDiv.Style["display"] = "none";
                ExternalAssessorRating_otherAssessorSubjectRatingUpSpan.Style["display"] = "block";
                ExternalAssessorRating_otherAssessorSubjectRatingDownSpan.Style["display"] = "none";
                ExternalAssessorRating_AssessorSubjectScoreDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            ExternalAssessorRating_assessorSubjectScoreTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                ExternalAssessorRating_AssessorSubjectScoreDiv.ClientID + "','" +
                ExternalAssessorRating_OtherAssessorSubjectScoreDiv.ClientID + "','" +
                ExternalAssessorRating_otherAssessorSubjectRatingUpSpan.ClientID + "','" +
                ExternalAssessorRating_otherAssessorSubjectRatingDownSpan.ClientID + "','" +
                ExternalAssessorRating_otherAssessorSubjectRatingRestoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
        }

        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>
        private void ExpandRestoreOtherOverallScore()
        {
            if (ExternalAssessorRating_overallScoreRestoreHiddenField.Value == "Y")
            {
                ExternalAssessorRating_othersOverAllScoreDiv.Style["display"] = "block";
                ExternalAssessorRating_overallScoreUpSpan.Style["display"] = "none";
                ExternalAssessorRating_overallScoreDownSpan.Style["display"] = "block";
                ExternalAssessorRating_overAllScoreDiv.Style["height"] = OVERALLSCORE_RESTORED_HEIGHT;
            }
            else
            {
                ExternalAssessorRating_othersOverAllScoreDiv.Style["display"] = "none";
                ExternalAssessorRating_overallScoreUpSpan.Style["display"] = "block";
                ExternalAssessorRating_overallScoreDownSpan.Style["display"] = "none";
                ExternalAssessorRating_overAllScoreDiv.Style["height"] = OVERALLSCORE_EXPANDED_HEIGHT;
            }
            ExternalAssessorRating_otherOverallAssessorScoreTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                ExternalAssessorRating_overAllScoreDiv.ClientID + "','" +
                ExternalAssessorRating_othersOverAllScoreDiv.ClientID + "','" +
                ExternalAssessorRating_overallScoreUpSpan.ClientID + "','" +
                ExternalAssessorRating_overallScoreDownSpan.ClientID + "','" +
                ExternalAssessorRating_overallScoreRestoreHiddenField.ClientID + "','" +
                OVERALLSCORE_RESTORED_HEIGHT + "','" +
                OVERALLSCORE_EXPANDED_HEIGHT + "')");
        }

        #endregion Private Methods
    }
}