<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExternalAssessorRating.aspx.cs"
    Inherits="Forte.HCM.UI.ExternalAssessor.ExternalAssessorRating" MasterPageFile="~/MasterPages/OverviewMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/OverviewMaster.Master" %>
<%@ Register Src="~/CommonControls/ConfirmInterviewScoreMsgControl.ascx" TagName="ConfirmInterviewScoreMsgControl"
    TagPrefix="uc1" %>
<asp:Content ID="ExternalAssessorRating_bodyContent" runat="server" ContentPlaceHolderID="OverviewMaster_body">
    <script language="JavaScript" type="text/javascript">

        var requiredMajorVersion = 9;
        var requiredMinorVersion = 0;
        var requiredRevision = 28;

        // Handler for onchange and onkeyup to check for max characters.
        function CommentsCount(characterLength, clientId) {
            var maxlength = new Number(characterLength);
            if (clientId.value.length > maxlength) {
                clientId.value = clientId.value.substring(0, maxlength);
            }
        }

        // Handler method that will be called when the maximize or restore
        // button is clicked. This will maximize or restore the panel to
        // show or hide full view.
        function ExpandOrRestore(ctrlExpand, ctrlHide, expandImage, compressImage,
            isMaximizedHidden, restoredHeight, expandedHeight) {
            if (document.getElementById(ctrlExpand) != null) {
                document.getElementById(expandImage).style.display = "none";
                document.getElementById(compressImage).style.display = "none";
                if (document.getElementById(ctrlExpand).style.height.toString() == restoredHeight) {
                    document.getElementById(ctrlExpand).style.height = expandedHeight;
                    document.getElementById(ctrlHide).style.display = "none";
                    document.getElementById(expandImage).style.display = "block";
                    document.getElementById(isMaximizedHidden).value = "Y";
                }
                else {
                    document.getElementById(ctrlExpand).style.height = restoredHeight;
                    document.getElementById(ctrlHide).style.display = "block";
                    document.getElementById(compressImage).style.display = "block";
                    document.getElementById(isMaximizedHidden).value = "N";
                }
            }
            return false;
        }
    </script>
    <asp:UpdatePanel ID="tset" runat="server">
        <ContentTemplate>
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td class="header_bg">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 30%" class="header_text_bold">
                                    <asp:Literal ID="ExternalAssessorRating_headerLiteral" runat="server" Text="Assessment Rating"></asp:Literal>
                                </td>
                                <td style="width: 30%">
                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="ExternalAssessorRating_topPublishInterviewImageButton" runat="server"
                                                    ImageUrl="~/App_Themes/DefaultTheme/Images/publish.png" ToolTip="Publish Candidate Interview Response"
                                                    Visible="true" Width="18px" Height="18px" OnClick="ExternalAssessorRating_publishInterviewImageButton_Click" />
                                                <asp:UpdatePanel ID="ExternalAssessorRating_showPublishUrlUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <div style="float: left;">
                                                            <asp:Panel ID="ExternalAssessorRating_emailConfirmationPanel" runat="server" Style="display: none;
                                                                height: 254px;" CssClass="popupcontrol_confirm_publish_candidate_interview_response">
                                                                <div id="ExternalAssessorRating_emailConfirmationDiv" style="display: none">
                                                                    <asp:Button ID="ExternalAssessorRating_emailConfirmation_hiddenButton" runat="server" />
                                                                </div>
                                                                <uc1:ConfirmInterviewScoreMsgControl ID="ExternalAssessorRating_emailConfirmation_ConfirmMsgControl"
                                                                    runat="server" Title="Publish Candidate Interview Response" Type="InterviewScoreConfirmType" />
                                                            </asp:Panel>
                                                            <ajaxToolKit:ModalPopupExtender ID="ExternalAssessorRating_emailConfirmation_ModalPopupExtender"
                                                                BehaviorID="ExternalAssessorRating_emailConfirmation_ModalPopupExtender" runat="server"
                                                                PopupControlID="ExternalAssessorRating_emailConfirmationPanel" TargetControlID="ExternalAssessorRating_emailConfirmation_hiddenButton"
                                                                BackgroundCssClass="modalBackground">
                                                            </ajaxToolKit:ModalPopupExtender>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                                &nbsp;
                                                <asp:Button ID="ExternalAssessorRating_topRefreshButton" runat="server" SkinID="sknButtonId"
                                                    Text="Refresh" OnClick="ExternalAssessorRating_refreshButton_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="ExternalAssessorRating_messagesUpdatePanel" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="ExternalAssessorRating_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                <asp:Label ID="ExternalAssessorRating_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="tab_body_bg_assessor">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top" colspan="3">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="2" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 60px">
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Image ID="ExternalAssessorRating_candidatePhotoImage" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 225px; padding-top: 10px" valign="top" class="border_right">
                                                            <table cellpadding="4" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <asp:Label ID="ExternalAssessorRating_candidateNameLabelValue" CssClass="assessor_name_label_text"
                                                                            runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <asp:Label ID="ExternalAssessorRating_interviewNameLabel" runat="server" SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <asp:Label ID="ExternalAssessorRating_testCompletedDateLabel" runat="server" SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <div runat="server" id="PublishExternalAssessorRating_displayOverallScoreDiv">
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td style="width: 130px">
                                                                                        <asp:Label ID="ExternalAssessorRating_overallWeightageScoreLabel" runat="server"
                                                                                            Text="Total Weighted Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="ExternalAssessorRating_overallWeightageScoreLabelValue" SkinID="sknLabelRatingScoreFieldText"
                                                                                            runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 10px;">
                                                        </td>
                                                        <td valign="top" style="width: 350px; padding-top: 10px">
                                                            <table cellpadding="2" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top">
                                                                        <div style="width: 450px; word-wrap: break-word; white-space: normal">
                                                                            <asp:Label ID="ExternalAssessorRating_positionProfileLabelValue" runat="server" CssClass="assessor_name_label_text"
                                                                                Style="word-wrap: break-word; white-space: normal"></asp:Label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <asp:Label ID="PublishAssessorRating_clientNameLabelValue" runat="server" CssClass="client_name_label_text"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_5">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td style="width: 410px" valign="top">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="header_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:Literal ID="ExternalAssessorRating_subjectScoreLiteral" runat="server" Text="Assessor Subject Score"></asp:Literal>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <table cellpadding="2" cellspacing="0">
                                                                                        <tr id="ExternalAssessorRating_assessorSubjectScoreTR" runat="server">
                                                                                            <td>
                                                                                                <div style="float: left">
                                                                                                    <asp:Label ID="ExternalAssessorRating_otherAssessorTitleLable" runat="server" SkinID="sknLabelRatingText"
                                                                                                        Text="Other Assessor(s)"></asp:Label>
                                                                                                </div>
                                                                                                <div style="float: left">
                                                                                                    &nbsp;</div>
                                                                                                <div style="float: left">
                                                                                                    <span id="ExternalAssessorRating_otherAssessorSubjectRatingUpSpan" runat="server"
                                                                                                        style="display: none;">
                                                                                                        <asp:Image ID="ExternalAssessorRating_otherAssessorSubjectRatingUpImage" runat="server"
                                                                                                            ToolTip="Click here to view other assessor(s) subject score" SkinID="sknMinimizeImage" />
                                                                                                    </span><span id="ExternalAssessorRating_otherAssessorSubjectRatingDownSpan" runat="server"
                                                                                                        style="display: block;">
                                                                                                        <asp:Image ID="ExternalAssessorRating_otherAssessorSubjectRatingDownImage" runat="server"
                                                                                                            SkinID="sknMaximizeImage" />
                                                                                                    </span>
                                                                                                    <asp:HiddenField ID="ExternalAssessorRating_otherAssessorSubjectRatingRestoreHiddenField"
                                                                                                        runat="server" />
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="assessor_panel_body_bg_small" valign="top">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="width: 100%; overflow: auto; height: 100px;" runat="server" id="ExternalAssessorRating_AssessorSubjectScoreDiv">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td valign="top" align="left" style="width: 100%">
                                                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                                                        <tr>
                                                                                                            <td style="width: 210px">
                                                                                                                <asp:Label ID="ExternalAssessorRating_assesorSkillNameLabel" runat="server" Text="Skill"
                                                                                                                    SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 150px">
                                                                                                                <asp:Label ID="ExternalAssessorRating_assesorNameLabel" runat="server" Text="Name"
                                                                                                                    SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 150px; text-align: right">
                                                                                                                <asp:Label ID="ExternalAssessorRating_assesorSubjectWeightedScoreLabel" runat="server"
                                                                                                                    Text="Subject Weighted Score" SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:UpdatePanel ID="ExternalAssessorRating_assessorSubjectRatingUpdatePanel" runat="server">
                                                                                                        <ContentTemplate>
                                                                                                            <asp:GridView ID="ExternalAssessorRating_assessorSubjectRatingGridview" runat="server"
                                                                                                                AutoGenerateColumns="False" SkinID="sknNewGridView" ShowHeader="false" OnRowDataBound="ExternalAssessorRating_assessorSubjectRatingGridview_RowDataBound">
                                                                                                                <Columns>
                                                                                                                    <asp:TemplateField HeaderText="Subject" ItemStyle-Width="31%">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="ExternalAssessorRating_subjectNameLabel" runat="server" SkinID="sknLabelAssessorFieldNameText"
                                                                                                                                Text='<%# Eval("SUBJECT_NAME") %>'></asp:Label>
                                                                                                                            <asp:HiddenField ID="ExternalAssessorRating_overallSubjectGridViewHiddenField" Value='<%# Eval("CAT_SUB_ID") %>'
                                                                                                                                runat="server" />
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField HeaderText="Assessor Name" ItemStyle-Width="25%">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="ExternalAssessorRating_AssessorNameLabel" runat="server" SkinID="sknLabelAssessorFieldNameText"
                                                                                                                                Text='<%# Eval("ASSESSOR_NAME") %>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField HeaderText="OVERALL WEIGHTED SCORE" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Right">
                                                                                                                        <ItemTemplate>
                                                                                                                            &nbsp;
                                                                                                                            <asp:Label ID="ExternalAssessorRating_assessorWeightageScoreGridviewLabel" runat="server"
                                                                                                                                SkinID="sknLabelAssessorFieldscoreText" Text='<%# Eval("SUBJECT_WEIGHTAGE_RATING") %>'></asp:Label>
                                                                                                                            <asp:HiddenField ID="ExternalAssessorRating_assessorWeightageScoreGridviewHiddenField"
                                                                                                                                Value='<%# Eval("SUBJECT_WEIGHTAGE_RATING") %>' runat="server" />
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                </Columns>
                                                                                                                <HeaderStyle CssClass="grid_header" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                                <AlternatingRowStyle CssClass="grid_001" BorderColor="#CBDAEA" BorderStyle="Solid"
                                                                                                                    BorderWidth="1" />
                                                                                                                <RowStyle CssClass="grid" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                            </asp:GridView>
                                                                                                        </ContentTemplate>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <asp:UpdatePanel ID="ExternalAssessorRating_otherAssessorSubjectRatingUpdatePanel"
                                                                                        runat="server">
                                                                                        <ContentTemplate>
                                                                                            <div style="width: 100%; overflow: auto; height: 120px; display: block;" runat="server"
                                                                                                id="ExternalAssessorRating_OtherAssessorSubjectScoreDiv">
                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="ExternalAssessorRating_otherAssessorTitle" runat="server" SkinID="sknLabelRatingText"
                                                                                                                Text="Other Assessor(s)"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td style="width: 210px">
                                                                                                                        <asp:Label ID="ExternalAssessorRating_otherAssesorSkillNameLabel" runat="server"
                                                                                                                            Text="Skill" SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 150px">
                                                                                                                        <asp:Label ID="ExternalAssessorRating_otherAssesorNameLabel" runat="server" Text="Name"
                                                                                                                            SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 150px; text-align: right">
                                                                                                                        <asp:Label ID="ExternalAssessorRating_otherAssesorWeightedScoreLabel" runat="server"
                                                                                                                            Text="Subject Weighted Score" SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:GridView ID="ExternalAssessorRating_otherAssessorSubjectRatingGridview" runat="server"
                                                                                                                AutoGenerateColumns="False" SkinID="sknNewGridView" ShowHeader="false" OnRowDataBound="ExternalAssessorRating_otherAssessorSubjectRatingGridview_RowDataBound">
                                                                                                                <Columns>
                                                                                                                    <asp:TemplateField HeaderText="Subject" ItemStyle-Width="31%" ItemStyle-Wrap="true">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="ExternalAssessorRating_otherAssessorSubjectNameLabel" runat="server"
                                                                                                                                SkinID="sknLabelAssessorFieldNameText" Text='<%# Eval("SUBJECT_NAME") %>'></asp:Label>
                                                                                                                            <asp:HiddenField ID="ExternalAssessorRating_otherAssessorOverallSubjectGridViewHiddenField"
                                                                                                                                Value='<%# Eval("CAT_SUB_ID") %>' runat="server" />
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField HeaderText="Assessor Name" ItemStyle-Width="25%">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="ExternalAssessorRating_otherAssessorNameLabel" runat="server" SkinID="sknLabelAssessorFieldNameText"
                                                                                                                                Text='<%# Eval("ASSESSOR_NAME") %>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField HeaderText="OVERALL WEIGHTED SCORE" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Right">
                                                                                                                        <ItemTemplate>
                                                                                                                            &nbsp;
                                                                                                                            <asp:Label ID="ExternalAssessorRating_otherAssessorWeightageScoreGridviewLabel" runat="server"
                                                                                                                                SkinID="sknLabelAssessorFieldscoreText" Text='<%# Eval("SUBJECT_WEIGHTAGE_RATING") %>'></asp:Label>
                                                                                                                            <asp:HiddenField ID="ExternalAssessorRating_otherAssessorWeightageScoreGridviewHiddenField"
                                                                                                                                Value='<%# Eval("SUBJECT_WEIGHTAGE_RATING") %>' runat="server" />
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                </Columns>
                                                                                                                <HeaderStyle CssClass="grid_header" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                                <AlternatingRowStyle CssClass="grid_001" BorderColor="#CBDAEA" BorderStyle="Solid"
                                                                                                                    BorderWidth="1" />
                                                                                                                <RowStyle CssClass="grid" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                            </asp:GridView>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 2px">
                                                        </td>
                                                        <td valign="top" style="width: 410px">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="header_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:Literal ID="ExternalAssessorRating_overallScoreLiteral" runat="server" Text="Assessor Score"></asp:Literal>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <table cellpadding="2" cellspacing="0">
                                                                                        <tr id="ExternalAssessorRating_otherOverallAssessorScoreTR" runat="server">
                                                                                            <td>
                                                                                                <div style="float: left">
                                                                                                    <asp:Label ID="ExternalAssessorRating_otherOverAllScoreTitleLable" runat="server"
                                                                                                        SkinID="sknLabelRatingText" Text="Other Assessor(s)"></asp:Label>
                                                                                                </div>
                                                                                                <div style="float: left">
                                                                                                    &nbsp;</div>
                                                                                                <div style="float: left">
                                                                                                    <span id="ExternalAssessorRating_overallScoreUpSpan" runat="server" style="display: none;">
                                                                                                        <asp:Image ID="ExternalAssessorRating_overallScoreUpImage" runat="server" ToolTip="Click here to view other assessor(s) score"
                                                                                                            SkinID="sknMinimizeImage" />
                                                                                                    </span><span id="ExternalAssessorRating_overallScoreDownSpan" runat="server" style="display: block;">
                                                                                                        <asp:Image ID="ExternalAssessorRating_overallScoreDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                                                    </span>
                                                                                                    <asp:HiddenField ID="ExternalAssessorRating_overallScoreRestoreHiddenField" runat="server" />
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="assessor_panel_body_bg_small" valign="top">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="width: 100%; overflow: auto; height: 100px;" runat="server" id="ExternalAssessorRating_overAllScoreDiv">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="4" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="ExternalAssessorRating_assessorOverallWeightageScoreLabel" runat="server"
                                                                                                                    Text="Weighted Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="ExternalAssessorRating_assessorOverallWeightageScoreValueLabel" SkinID="sknLabelRatingScoreFieldText"
                                                                                                                    runat="server"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td align="right">
                                                                                                                <asp:Label ID="ExternalAssessorRating_commentsLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                    Text="Comment"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="ExternalAssessorRating_commentsTextbox" runat="server" Height="60px"
                                                                                                                    TextMode="MultiLine" Width="300px" Wrap="true" MaxLength="500" onchange="CommentsCount(500,this)"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="td_height_5">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">
                                                                                                                <asp:UpdatePanel ID="ExternalAssessorRating_SaveSubmitUpdatePanel" runat="server">
                                                                                                                    <ContentTemplate>
                                                                                                                        <asp:Button ID="ExternalAssessorRating_saveButton" runat="server" OnClick="ExternalAssessorRating_saveButton_Click"
                                                                                                                            SkinID="sknButtonId" Text="Save" />
                                                                                                                        <asp:Button ID="ExternalAssessorRating_submitButton" runat="server" OnClick="ExternalAssessorRating_submitButton_Click"
                                                                                                                            SkinID="sknButtonId" Text="Submit" />
                                                                                                                    </ContentTemplate>
                                                                                                                </asp:UpdatePanel>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <asp:UpdatePanel ID="ExternalAssessorRating_overallAssesorScoreUpdtePanel" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <div style="width: 100%; overflow: auto; height: 120px;" runat="server" id="ExternalAssessorRating_othersOverAllScoreDiv">
                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="ExternalAssessorRating_otherAssessorOverallScoreTitleLabel" runat="server"
                                                                                                                SkinID="sknLabelRatingText" Text="Other Assessor(s)"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:GridView ID="ExternalAssessorRating_othersOverAllScore" runat="server" AutoGenerateColumns="False"
                                                                                                                SkinID="sknNewGridView" ShowHeader="false" OnRowDataBound="ExternalAssessorRating_othersOverAllScore_RowDataBound">
                                                                                                                <Columns>
                                                                                                                    <asp:TemplateField>
                                                                                                                        <ItemTemplate>
                                                                                                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <asp:Label ID="ExternalAssessorRating_assessorNameLabel" runat="server" Text='<%# Eval("ASSESSOR_NAME") %>'
                                                                                                                                            SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td align="left" style="width: 100px">
                                                                                                                                                    <asp:Label ID="ExternalAssessorRating_otherAssessorWeightageScoreLabel" Text="Weighted Score"
                                                                                                                                                        runat="server"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                                <td align="left">
                                                                                                                                                    <asp:Label ID="ExternalAssessorRating_overallAssessorWeightedGridviewLabel" runat="server"
                                                                                                                                                        Text='<%# Eval("OVERALL_ASSESSOR_WEIGHTED") %>' SkinID="sknLabelAssessorFieldscoreText"></asp:Label>
                                                                                                                                                    <asp:HiddenField ID="ExternalAssessorRating_overallAssessorWeightedGridviewHiddenField"
                                                                                                                                                        Value='<%# Eval("OVERALL_ASSESSOR_WEIGHTED") %>' runat="server" />
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td colspan="2">
                                                                                                                                        <div class="label_multi_field_text">
                                                                                                                                            <asp:Label ID="ExternalAssessorRating_otherAssessorCommentsLabel" SkinID="sknLabelRatingCommentsFieldText"
                                                                                                                                                runat="server" Text='<%# Eval("COMMENTS") %>'></asp:Label>
                                                                                                                                        </div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                </Columns>
                                                                                                                <HeaderStyle CssClass="grid_header" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                                <AlternatingRowStyle CssClass="grid_001" BorderColor="#CBDAEA" BorderStyle="Solid"
                                                                                                                    BorderWidth="1" />
                                                                                                                <RowStyle CssClass="grid" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                            </asp:GridView>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5" colspan="3">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 270px" valign="top">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td valign="top">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Literal ID="ExternalAssessorRating_assessorDetailsQuestionsLiteral" runat="server"
                                                                            Text="Questions"></asp:Literal>
                                                                    </td>
                                                                    <td align="right" style="padding-left: 80px">
                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:Label ID="ExternalAssessorRating_questiongroupByLabel" runat="server" Text="Group By"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <asp:UpdatePanel ID="ExternalAssessorRating_questiongroupByDropDownListUpdatePanel"
                                                                                        runat="server">
                                                                                        <ContentTemplate>
                                                                                            <asp:DropDownList ID="ExternalAssessorRating_questiongroupByDropDownList" runat="server"
                                                                                                AutoPostBack="true" OnSelectedIndexChanged="ExternalAssessorRating_questiongroupByDropDownList_SelectedIndexChanged">
                                                                                                <asp:ListItem Text="--Select--"></asp:ListItem>
                                                                                                <asp:ListItem Text="Question" Value="Q"></asp:ListItem>
                                                                                                <asp:ListItem Text="Subject" Value="S"></asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="assessor_panel_body_bg" valign="top">
                                                            <table cellpadding="3" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellpadding="3" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:UpdatePanel ID="ExternalAssessorRating_questiongroupByUpdatePanel" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <div style="vertical-align: top">
                                                                                                <table cellpadding="0" cellspacing="3" border="0">
                                                                                                    <tr>
                                                                                                        <td width="70" class="RatingEditable">
                                                                                                            Rating Required
                                                                                                        </td>
                                                                                                        <td width="70" class="RatingFinished">
                                                                                                            Rating Completed
                                                                                                        </td>
                                                                                                        <td width="70" class="RatingNotAllowed">
                                                                                                            Rating Not Required
                                                                                                        </td>
                                                                                                </table>
                                                                                            </div>
                                                                                            <div>
                                                                                                &nbsp;
                                                                                            </div>
                                                                                            <div style="width: 100%; overflow: auto; height: 400px;" runat="server" id="ExternalAssessorRating_questionAreaDiv">
                                                                                                <asp:GridView ID="ExternalAssessorRating_questionAreaDataList" runat="server" AutoGenerateColumns="False"
                                                                                                    SkinID="sknNewGridView" ShowHeader="false" OnRowDataBound="ExternalAssessorRating_questionAreaDataList_RowDataBound"
                                                                                                    OnRowCommand="ExternalAssessorRating_questionAreaDataList_RowCommand">
                                                                                                    <Columns>
                                                                                                        <asp:TemplateField>
                                                                                                            <ItemTemplate>
                                                                                                                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                                                                    <tr>
                                                                                                                        <td valign="middle" style="width: 5%">
                                                                                                                            <%-- <asp:Image ID="ExternalAssessorRating_questionImage" runat="server" SkinID="sknQuestionImage"
                                                                                                                                ToolTip="Question" />--%>
                                                                                                                            <asp:Label ID="ExternalAssessorRating_rowNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                                                                                runat="server">
                                                                                                                            </asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="width: 60%" align="left">
                                                                                                                            <div class="label_multi_field_text">
                                                                                                                                <asp:LinkButton ID="ExternalAssessorRating_questionAreaQuestionsLabel" runat="server"
                                                                                                                                    Text='<%# TrimContent(Eval("Question").ToString(),25) %>' CommandName="QuestionDetail">
                                                                                                                                </asp:LinkButton>
                                                                                                                                <asp:HiddenField ID="ExternalAssessorRating_questionAreaQuestionIDHiddenField" runat="server"
                                                                                                                                    Value='<%# Eval("TestQuestionID") %>' />
                                                                                                                                <asp:HiddenField ID="ExternalAssessorRating_questionAreaQuestionKeyHiddenField" runat="server"
                                                                                                                                    Value='<%# Eval("QuestionKey") %>' />
                                                                                                                                <asp:HiddenField ID="ExternalAssessorRating_questionRatingEditHiddenField" runat="server"
                                                                                                                                    Value='<%# Eval("RatingEdit") %>' />
                                                                                                                            </div>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" align="left">
                                                                                                                            <asp:Label ID="ExternalAssessorRating_questionAreaSubjectLabel" runat="server" Text='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                                                            <asp:HiddenField ID="ExternalAssessorRating_questionAreaSubjectHidddenField" runat="server"
                                                                                                                                Value='<%# Eval("SubjectID") %>' />
                                                                                                                        </td>
                                                                                                                        <td style="width: 10%" align="left">
                                                                                                                            <asp:Label ID="ExternalAssessorRating_questionAreaRatingLabel" runat="server" Text='<%# Eval("Rating") %>'>
                                                                                                                            </asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>
                                                                                                    </Columns>
                                                                                                    <AlternatingRowStyle CssClass="grid_001" BorderColor="#CBDAEA" BorderStyle="Solid"
                                                                                                        BorderWidth="1" />
                                                                                                    <RowStyle CssClass="grid" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                </asp:GridView>
                                                                                            </div>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_20">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 4px">
                                </td>
                                <td style="width: 726px" valign="top">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="candidateResponseUpdatePanel" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <iframe id="AsssessorRating_candidateResponseIframe" runat="server" height="635"
                                                            style="border-style: none" width="100%"></iframe>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ExternalAssessorRating_questionAreaDataList"
                                                            EventName="RowCommand" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="header_bg">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 50%">
                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="ExternalAssessorRating_bottomPublishUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <asp:ImageButton ID="ExternalAssessorRating_bottomPublishInterviewImageButton" runat="server"
                                                            ImageUrl="~/App_Themes/DefaultTheme/Images/publish.png" ToolTip="Publish Candidate Interview Response"
                                                            Visible="true" Width="18px" Height="18px" OnClick="ExternalAssessorRating_publishInterviewImageButton_Click" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                                &nbsp;
                                                <asp:Button ID="ExternalAssessorRating_bottomRefreshButton" runat="server" SkinID="sknButtonId"
                                                    Text="Refresh" OnClick="ExternalAssessorRating_refreshButton_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
