﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExternalAssessor.aspx.cs
// File that represents the external assessor class that defines the user 
// interface layout and functionalities for the external assessor page. This 
// page helps in managing the external assessor user details enrollment page. 

#endregion Header

#region Directives

using System;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.Common;
using System.Configuration;
using System.Web.Configuration;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Resources;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;
using System.Web.Security;

#endregion Directives

namespace Forte.HCM.UI.ExternalAssessor
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ExternalAssessor page. This will helps to manage the external assessor user details
    /// This class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ExternalAssessor : PageBase
    {
        #region Event Handler

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set the page caption in the page
                Master.SetPageCaption("External Assessor");

                Page.SetFocus(ExternalAssessor_continueButton.ClientID);

                ClearControls();

                if (!IsPostBack)
                {
                    // Call and assign query string values.
                    if (RetrieveKey() == false)
                        return;

                    // Get external assessor details
                    GetExternalAssessorDetail();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the continue button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will update external assessor details and continue to assess the interview.
        /// </remarks>
        protected void ExternalAssessor_continueButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;
              
                //Get external assessor details
                GetExternalAssessorDetail();

                //Get query string values
                string externalKey = Request.QueryString["a"].ToString().Trim();

                //Construct external assessor detail
                ExternalAssessorDetail externalAssessorDetail = ExternalAssessorDetail();

                //Update external assessor
                new InterviewSessionBLManager().UpdateExternalAssessorDetail(externalAssessorDetail);

                string redirecURL = "~/ExternalAssessor/ExternalAssessorRating.aspx?a=" + externalKey;

                Response.Redirect(redirecURL, false);
                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handler

        #region Private Methods

        /// <summary>
        /// Method that retrieves the query string values and assign it to 
        /// hidden fields.
        /// </summary>
        private bool RetrieveKey()
        {
            // Check if correct key is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["a"]))
            {
                base.ShowMessage(ExternalAssessor_topSuccessMessageLabel, "External key is not passed");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Method to get the external assessor details
        /// </summary>
        private void GetExternalAssessorDetail()
        {
            string externalKey = Request.QueryString["a"].ToString().Trim();

            //Get the external assessor details
            ExternalAssessorDetail externalAssesorDetail = null;
            externalAssesorDetail = new InterviewSessionBLManager().GetExternalAssessorDetailByKey(externalKey);

            if (externalAssesorDetail == null)
            {
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel, "External key is invalid");
                return;
            }

            if (!Utility.IsNullOrEmpty(externalAssesorDetail.AssessmentStatus) && 
                externalAssesorDetail.AssessmentStatus.ToString().Trim().ToUpper() == "ASS_COMP")
            {
                //base.ShowMessage(ExternalAssessor_topSuccessMessageLabel, "Assessment completed already");
                //return;
            }

            //Assign values to hidden field values
            ExternalAssessor_externalAssessorIDHiddenField.Value = 
                externalAssesorDetail.ExternalAssessorID.ToString();

            ExternalAssessor_emailTextBox.Text = externalAssesorDetail.Email;
            ExternalAssessor_contactNumberTextBox.Text = externalAssesorDetail.ContactNumber;
            ExternalAssessor_firstNameTextBox.Text = externalAssesorDetail.FirstName;
            ExternalAssessor_lastNameTextBox.Text = externalAssesorDetail.LastName;
        }

        /// <summary>
        /// Method to clear display message controls
        /// </summary>
        private void ClearControls()
        {
            ExternalAssessor_topSuccessMessageLabel.Text = string.Empty;
            ExternalAssessor_topErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Method that constructs external assessor details
        /// </summary>
        /// <returns>That returns external assessor detail</returns>
        private ExternalAssessorDetail ExternalAssessorDetail()
        {
            ExternalAssessorDetail externalAssessorDetail = new ExternalAssessorDetail();

            externalAssessorDetail.ExternalAssessorID = 
                Convert.ToInt32(ExternalAssessor_externalAssessorIDHiddenField.Value);
            externalAssessorDetail.Email = ExternalAssessor_emailTextBox.Text;
            externalAssessorDetail.ContactNumber = ExternalAssessor_contactNumberTextBox.Text;
            externalAssessorDetail.FirstName = ExternalAssessor_firstNameTextBox.Text;
            externalAssessorDetail.LastName = ExternalAssessor_lastNameTextBox.Text;
            
            return externalAssessorDetail;
        }

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        private bool IsValidLoginData()
        {
            bool isValid = true;

            //Get query string values
            if (Utility.IsNullOrEmpty(Request.QueryString["a"]))
            {
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel, "External key is invalid");
                isValid = false;
            }

            if (Utility.IsNullOrEmpty(ExternalAssessor_userNameTextBox.Text))
            {
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel, HCMResource.Login_UserNameTextBoxEmpty);
                isValid = false;
            }

            if (!Utility.IsNullOrEmpty(ExternalAssessor_userNameTextBox.Text) &&
                !Utility.IsValidEmailAddress(ExternalAssessor_userNameTextBox.Text.Trim()))
            {
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel,
                    HCMResource.UserRegistration_InvalidEmailAddress);
                isValid = false;
            }

            if (Utility.IsNullOrEmpty(ExternalAssessor_passwordTextBox.Text))
            {
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel, HCMResource.Login_PasswordTextBoxEmpty);
                isValid = false;
            }

           
            return isValid;
        }

        /// <summary>
        /// Method that try login to the system using the given user name and 
        /// password.
        /// </summary>
        private void TryLogin()
        {
            IDbTransaction transaction = null;
            try
            {
                UserDetail userDetails = null;

                userDetails = AuthenticateUserDetails(ExternalAssessor_userNameTextBox.Text.Trim(),
                    ExternalAssessor_passwordTextBox.Text.Trim());

                if (Utility.IsNullOrEmpty(userDetails) || userDetails.UserID == 0)
                {
                    base.ShowMessage(ExternalAssessor_topErrorMessageLabel, HCMResource.Login_InvalidUserLogin);
                    return;
                }

                //Do not allow candidate to login through ForteHCM application
                if (userDetails.IsCandidate)
                {
                    base.ShowMessage(ExternalAssessor_topErrorMessageLabel, HCMResource.Login_InvalidUserLogin);
                    return;
                }

                Session["USER_DETAIL"] = userDetails;

                UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                int userID = userDetail.UserID;

                //Construct assessor rating url and redirect the user
                string externalKey = Request.QueryString["a"].ToString().Trim();

                //Get the external assessor details
                ExternalAssessorDetail externalAssesorDetail = null;
                externalAssesorDetail = new InterviewSessionBLManager().GetExternalAssessorDetailByKey(externalKey);

                bool assessorStatus;

                assessorStatus = new AssessorBLManager().IsAssessorInvolvedAgainstThisCandidateSession(
                    externalAssesorDetail.CandidateInterviewSessionkey, externalAssesorDetail.AttemptID, userID);

                if (assessorStatus)
                {
                    base.ShowMessage(ExternalAssessor_topErrorMessageLabel, 
                        "Logged user is already involved in the assessment against this candidate session.");
                    return;
                }


                transaction = new TransactionManager().Transaction;

                //Transfer the external assessor assessed details to registered user
                new AssessorBLManager().UpdateExternalAssessorToRegisteredUser(externalAssesorDetail.ExternalAssessorID, 
                    userID, externalAssesorDetail.Email,transaction);

                transaction.Commit();

                //if (!Utility.IsNullOrEmpty(transaction))
                 //   transaction.Rollback();

                if (externalAssesorDetail != null)
                {
                    int externalAssessorID = externalAssesorDetail.ExternalAssessorID;

                    string ratingURL = string.Empty;

                    ratingURL = "~/Assessments/AssessorRating.aspx";
                    ratingURL += "?CISK=" + externalAssesorDetail.CandidateInterviewSessionkey;
                    ratingURL += "&ISK=" + externalAssesorDetail.InterviewKey;
                    ratingURL += "&sessionkey=" + externalAssesorDetail.InterviewSessionKey;
                    ratingURL += "&assessorId=" + userID;
                    ratingURL += "&ATMPID=" + externalAssesorDetail.AttemptID;
                    ratingURL += "&parentpage=MY_ASMT";

                    FormsAuthentication.RedirectFromLoginPage(ExternalAssessor_userNameTextBox.Text.Trim(), false);
                    Response.Redirect(ratingURL, false);
                }
            }
            catch (Exception exp)
            {
                if (!Utility.IsNullOrEmpty(transaction))
                    transaction.Rollback();

                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will return a userdetail object for the 
        /// given username and password.
        /// </summary>
        /// <param name="UserName">
        /// A <see cref="string"/> that contains the username.
        /// </param>
        /// <param name="Password">
        /// A <see cref="string"/> that contains the password.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> object that contains the 
        /// information for the given username and password.
        /// </returns>
        private UserDetail AuthenticateUserDetails(string UserName, string Password)
        {
            AuthenticationBLManager authenticationBLManager = new AuthenticationBLManager();
            Authenticate authenticate = new Authenticate();
            authenticate.UserName = UserName;
            authenticate.Password = new EncryptAndDecrypt().EncryptString(Password.Trim());
            UserDetail userDetails = new UserDetail();
            userDetails = authenticationBLManager.GetAuthenticateUser(authenticate);
            return userDetails;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            bool isValid = true;

            //Get query string values
            if (Utility.IsNullOrEmpty(Request.QueryString["a"]))
            {
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel, "External key is invalid");
                isValid = false;
            }
            
            if (Utility.IsNullOrEmpty(ExternalAssessor_emailTextBox.Text))
            {
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel, "Please enter email");
                isValid = false;
            }

            if (!Utility.IsNullOrEmpty(ExternalAssessor_emailTextBox.Text) && 
                !Utility.IsValidEmailAddress(ExternalAssessor_emailTextBox.Text.Trim()))
            {
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel,
                    HCMResource.UserRegistration_InvalidEmailAddress);
                isValid = false;
            }

            if (Utility.IsNullOrEmpty(ExternalAssessor_contactNumberTextBox.Text))
            {
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel, "Please enter contact number");
                isValid = false;
            }

            if (Utility.IsNullOrEmpty(ExternalAssessor_firstNameTextBox.Text))
            {
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel, "Please enter first name");
                isValid = false;
            }

            if (Utility.IsNullOrEmpty(ExternalAssessor_lastNameTextBox.Text))
            {
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel, "Please enter last name");
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Overridden Methods

        /// <summary>
        /// Handler method that will be called when the login button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will update external assessor details and continue to assess the interview.
        /// </remarks>
        protected void ExternalAssessor_loginButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidLoginData())
                    return;

                TryLogin();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ExternalAssessor_topErrorMessageLabel, exp.Message);
            }
        }
}
}