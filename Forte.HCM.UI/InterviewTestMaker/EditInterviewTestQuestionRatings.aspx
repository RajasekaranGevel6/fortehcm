<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditInterviewTestQuestionRatings.aspx.cs"
    Inherits="Forte.HCM.UI.InterviewTestMaker.EditInterviewTestQuestionRatings" MasterPageFile="~/MasterPages/InterviewMaster.Master" %>

<%@ Register Src="../CommonControls/ViewInterviewTestDetailsControl.ascx" TagName="ViewTestDetailsControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ContentPlaceHolderID="InterviewMaster_body" ID="EditInterviewTest_bodyContent"
    runat="server">
  <script type="text/javascript">
      function numeric_textBox() {
          if (event.keyCode > 47 && event.keyCode < 58 || event.keyCode == 8) {
              return true;
          }
          if (event.keyCode == 17) {
              return false;
          }
          else {
              event.keyCode = 0;
              return false;
          }
      }
 </script>
    <asp:UpdatePanel ID="EditInterviewTestQuestionRatings_testDraftGridviewUpdatePanel"
        runat="server">
        <ContentTemplate>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="header_bg">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 50%" class="header_text_bold">
                                    <asp:Literal ID="EditInterviewTestQuestionRatings_headerLiteral" runat="server" Text="Review Interview"></asp:Literal>
                                </td>
                                <td style="width: 50%">
                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                        <tr>
                                            <td>
                                                <asp:Button ID="EditInterviewTestQuestionRatings_topCreateSessionButton" runat="server"
                                                    SkinID="sknButtonId" Text="Create Interview Session" OnClick="EditInterviewTestQuestionRatings_createSessionButton_Click" />
                                                <asp:Button ID="EditInterviewTestQuestionRatings_topSaveButton" runat="server" SkinID="sknButtonId"
                                                    Text="Save" OnClick="EditInterviewTestQuestionRatings_saveButton_Click" />
                                            </td>
                                            <td>
                                                &nbsp; &nbsp;<asp:LinkButton ID="EditInterviewTestQuestionRatings_topCancelLinkButton"
                                                    SkinID="sknActionLinkButton" runat="server" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:Label ID="EditInterviewTestQuestionRatings_topSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="EditInterviewTestQuestionRatings_topErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_8">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="width: 100%">
                                    <uc1:ViewTestDetailsControl ID="EditInterviewTestQuestionRatings_testDetailsUserControl"
                                        runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td colspan="5" class="header_bg" valign="middle">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="left">
                                                <asp:Literal ID="TestDetailsControl_testDetailsMessageLiteral" runat="server" Text="Interview Questions"></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="tab_body_bg" width="100%" valign="middle">
                                    <table>
                                        <tr>
                                            <td style="width: 10%">
                                                <asp:Label ID="EditInterviewTestQuestionRatings_questionTimetakenApplyToAllLabel"
                                                    runat="server" SkinID="sknLabelFieldHeaderText" Text="Time"></asp:Label>
                                                    <asp:Label ID="EditInterviewTestQuestionRatings_questionTimetakenApplyToAllHelpLabel" SkinID="sknLabelFieldText" Text="(Max 05:00)" runat="server"/>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:TextBox ID="EditInterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox"
                                                    runat="server" Width="30px" Text="5:00"></asp:TextBox>
                                                <asp:LinkButton ID="EditInterviewTestQuestionRatings_questionTimetakenLinkButton"
                                                    runat="server" Text="Apply To All" SkinID="sknActionLinkButton" OnClick="EditInterviewTestQuestionRatings_questionTimetakenApplyToAllLinkButton_Click"></asp:LinkButton>
                                                <ajaxToolKit:MaskedEditExtender ID="EditInterviewTestQuestionRatings_questionTimetakenApplyToAllTextboxMaskedEditExtender"
                                                    runat="server" Enabled="True" ErrorTooltipEnabled="True" UserTimeFormat="None"
                                                    Mask="99:99" MaskType="Time" TargetControlID="EditInterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox"
                                                    AutoComplete="true" AutoCompleteValue="00:00">
                                                </ajaxToolKit:MaskedEditExtender>
                                            </td>
                                            <td style="width: 2%">
                                                <asp:Label ID="EditInterviewTestQuestionRatings_questionRatingsApplyToAllLabel" runat="server"
                                                    SkinID="sknLabelFieldHeaderText" Text="Scale Max"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:DropDownList ID="EditInterviewTestQuestionRatings_questionRatingsApplyToAllDropDownList"
                                                    runat="server" AutoPostBack="false">
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10" Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:LinkButton ID="EditInterviewTestQuestionRatings_questionRatingsApplyToAllLinkButton"
                                                    runat="server" Text="Apply To All" SkinID="sknActionLinkButton" OnClick="EditInterviewTestQuestionRatings_questionRatingsApplyToAllLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td style="width: 2%">
                                                <asp:Label ID="EditInterviewTestQuestionRatings_questionCommentsApplyToAllLabel"
                                                    runat="server" SkinID="sknLabelFieldHeaderText" Text="Comments"></asp:Label>
                                            </td>
                                            <td style="width: 40%">
                                                <asp:TextBox ID="EditInterviewTestQuestionRatings_questionCommentsApplyToAllTextbox"
                                                    TextMode="MultiLine" runat="server" Width="100%"></asp:TextBox>
                                            </td>
                                            <td style="padding-left: 8px">
                                                <asp:LinkButton ID="EditInterviewTestQuestionRatings_questionCommentsApplyToAllLinkButton"
                                                    runat="server" Text="Apply To All" SkinID="sknActionLinkButton" OnClick="EditInterviewTestQuestionRatings_questionCommentsApplyToAllLinkButton_Click"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="tab_body_bg" width="100%" valign="top">
                                    <asp:GridView ID="EditInterviewTestQuestionRatings_testDrftGridView" runat="server"
                                        AutoGenerateColumns="False" SkinID="sknNewGridView" ShowHeader="false" OnRowDataBound="EditInterviewTestQuestionRatings_testDrftGridView_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                        <tr>
                                                            <td valign="middle" style="width: 5%">
                                                                <asp:Image ID="EditInterviewTestQuestionRatings_questionImage" runat="server" SkinID="sknQuestionImage"
                                                                    ToolTip="Question" />
                                                                <asp:Label ID="EditInterviewTestQuestionRatings_rowNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                    runat="server">
                                                                </asp:Label>
                                                            </td>
                                                            <td colspan="8" style="width: 92%">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="label_multi_field_text" style="word-wrap: break-word;white-space:normal;">
                                                                                <asp:Literal ID="EditInterviewTestQuestionRatings_questionLiteral" runat="server"
                                                                                    Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'>
                                                                                </asp:Literal>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Image runat="server" ID="EditInterviewTestQuestionRatings_questionImageDisplay" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 5%" valign="top">
                                                                <asp:Image ID="EditInterviewTestQuestionRatings_answerImage" ImageUrl="~/App_Themes/DefaultTheme/Images/question_answer.gif"
                                                                                runat="server" ToolTip="Answer"/>
                                                            </td>
                                                            <td colspan="8">
                                                                <div class="label_multi_field_text" style="word-wrap: break-word; white-space: normal;">
                                                                    <asp:Label ID="EditInterviewTestQuestionRatings_answerQuestionTextBox" SkinID="sknLabelFieldText"
                                                                        runat="server" Text='<%# Eval("Choice_Desc")==null ? Eval("Choice_Desc") : Eval("Choice_Desc").ToString().Replace("\n", "<br />") %>'>
                                                                    </asp:Label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td style="width: 8%">
                                                                <asp:Label ID="EditInterviewTestQuestionRatings_categoryQuestionLabel" runat="server"
                                                                    Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 15%">
                                                                <asp:Label ID="EditInterviewTestQuestionRatings_categoryQuestionTextBox" SkinID="sknLabelFieldText"
                                                                    runat="server" Text='<%# Eval("CategoryName") %>'></asp:Label>
                                                            </td>
                                                            <td style="width: 6%">
                                                                <asp:Label ID="EditInterviewTestQuestionRatings_subjectQuestionLabel" runat="server"
                                                                    Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:Label ID="EditInterviewTestQuestionRatings_subjectQuestionTextBox" runat="server"
                                                                    Text='<%# Eval("SubjectName") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                            <td style="width: 8%">
                                                                <asp:Label ID="EditInterviewTestQuestionRatings_testAreaQuestionHeadLabel" runat="server"
                                                                    Text="Interview Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 15%">
                                                                <asp:Label ID="EditInterviewTestQuestionRatings_testAreaQuestionLabel" SkinID="sknLabelFieldText"
                                                                    runat="server" Text='<%# Eval("TestAreaName") %>' />
                                                            </td>
                                                            <td style="width: 8%">
                                                                <asp:Label ID="EditInterviewTestQuestionRatings_complexityQuestionHeadLabel" runat="server"
                                                                    Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="EditInterviewTestQuestionRatings_complexityQuestionLabel" SkinID="sknLabelFieldText"
                                                                    runat="server" Text='<%# Eval("ComplexityName") %>' />
                                                                <asp:HiddenField ID="EditInterviewTestQuestionRatings_interviewQuestionKeyHiddenField"
                                                                    runat="server" Value='<%# Eval("QuestionKey") %>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td style="width: 12%">
                                                                <asp:Label ID="EditInterviewTestQuestionRatings_questionTimetakenGridViewLabel"
                                                                    runat="server" SkinID="sknLabelFieldHeaderText" Text="Time"></asp:Label>&nbsp;<asp:Label ID="EditInterviewTestQuestionRatings_questionTimetakenHelpGridViewLabel" SkinID="sknLabelFieldText" Text="(Max 05:00)" runat="server"></asp:Label><span class="mandatory">*</span>
                                                            </td>
                                                            <td style="width: 15%">
                                                                <asp:TextBox ID="EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox"
                                                                    runat="server" Width="30px"></asp:TextBox>
                                                                <ajaxToolKit:MaskedEditExtender ID="EditInterviewTestQuestionRatings_questionTimetakenGridViewTextboxMaskedEditExtender"
                                                                    runat="server" Enabled="True" ErrorTooltipEnabled="True" UserTimeFormat="None"
                                                                    Mask="99:99" MaskType="Time" TargetControlID="EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox"
                                                                    AutoComplete="true" AutoCompleteValue="00:00">
                                                                </ajaxToolKit:MaskedEditExtender>
                                                            </td>
                                                            <td style="width: 8%" colspan="2">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            </span><asp:Label ID="EditInterviewTestQuestionRatings_questionRatingGridViewLabel"
                                                                                runat="server" SkinID="sknLabelFieldHeaderText" Text="Scale Max"></asp:Label><span class="mandatory">*
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="EditInterviewTestQuestionRatings_questionRatingsGridViewDropDownList"
                                                                                runat="server" AutoPostBack="false">
                                                                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                                                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                                                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                                                <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="width: 8%">
                                                                            <asp:Label ID="EditInterviewTestQuestionRatings_questionWeightageGridViewLabel" runat="server"
                                                                                SkinID="sknLabelFieldHeaderText" Text="Weightage"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="EditInterviewTestQuestionRatings_questionnWeightageGridViewTextbox"
                                                                                runat="server" Width="30px" MaxLength="3" onkeypress="javascript:numeric_textBox();" onkeyup="javascript:numeric_textBox();"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="width: 8%">
                                                                <asp:Label ID="EditInterviewTestQuestionRatings_questionCommentsGridViewLabel" runat="server"
                                                                    SkinID="sknLabelFieldHeaderText" Text="Comments"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%" colspan="3">
                                                                <asp:TextBox ID="EditInterviewTestQuestionRatings_questionCommentsGridViewTextbox"
                                                                    TextMode="MultiLine" runat="server" Width="97%"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="grid_header" />
                                        <AlternatingRowStyle CssClass="grid_001" />
                                        <RowStyle CssClass="grid" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="tr_height_25" align="center">
                        <asp:Label ID="EditInterviewTestQuestionRatings_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="EditInterviewTestQuestionRatings_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="header_bg" style="width: 100%;" align="right">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 50%;" class="header_text_bold">
                                </td>
                                <td style="width: 50%">
                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                        <tr>
                                            <td align="right">
                                                <asp:HiddenField ID="EditInterviewTestQuestionRatings_interviewTestKeyHiddenField"
                                                    runat="server" />
                                                <asp:Button ID="EditInterviewTestQuestionRatings_bottomCreateSessionButton" runat="server"
                                                    SkinID="sknButtonId" Text="Create Interview Session" OnClick="EditInterviewTestQuestionRatings_createSessionButton_Click" />
                                                <asp:Button ID="EditInterviewTestQuestionRatings_bottomSaveButton" runat="server"
                                                    SkinID="sknButtonId" Text="Save" OnClick="EditInterviewTestQuestionRatings_saveButton_Click" />
                                            </td>
                                            <td>
                                                &nbsp; &nbsp;<asp:LinkButton ID="EditInterviewTestQuestionRatings_bottomCancelLinkButton"
                                                    runat="server" SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
