﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    CodeBehind="ViewInterviewTest.aspx.cs" Inherits="Forte.HCM.UI.InterviewTestMaker.ViewInterviewTest" %>

<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<%@ Register Src="../CommonControls/ViewInterviewTestDetailsControl.ascx" TagName="ViewTestDetailsControl"
    TagPrefix="uc1" %>
<asp:Content ID="ViewInterviewTest_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="ViewInterviewTest_headerLiteral" runat="server" Text="View Interview"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="ViewInterviewTest_topEditTestButton" runat="server" SkinID="sknButtonId"
                                            Text="Edit Interview" OnClick="ViewInterviewTest_Edit_Button_Click" CommandName="EDIT_TEST" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:Button ID="ViewInterviewTest_topDeleteTestButton" runat="server" SkinID="sknButtonId"
                                            Text="Delete Interview" OnClick="ViewInterviewTest_Delete_Button_Click" CommandName="DELETE_TEST" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:Button ID="ViewInterviewTest_topCreateTestSessionButton" runat="server"
                                            Text="Create Interview Session" SkinID="sknButtonId" OnClick="ViewInterviewTest_createSessionButton_Click"
                                            CommandName="CREATE_SESSION" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:LinkButton ID="ViewInterviewTest_topCancelLinkButton" SkinID="sknActionLinkButton"
                                            runat="server" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="ViewInterviewTest_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="ViewInterviewTest_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td>
                <ajaxToolKit:TabContainer ID="ViewInterviewTest_mainTabContainer" runat="server"
                    ActiveTabIndex="0">
                    <ajaxToolKit:TabPanel ID="ViewInterviewTest_testdetailsTabPanel" runat="server">
                        <HeaderTemplate>
                            Interview Details
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="width: 100%">
                                        <uc1:ViewTestDetailsControl ID="ViewInterviewTest_testDetailsUserControl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="ViewInterviewTest_QuestionsTabPanel" HeaderText="questions"
                        runat="server">
                        <HeaderTemplate>
                            Questions
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="ViewInterviewTest_testDraftGridviewUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="width: 100%" align="right">
                                                <asp:Label ID="ViewInterviewTest_groupByLabel" runat="server" Text="Group&nbsp;By"
                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>&nbsp;
                                                <asp:DropDownList ID="ViewInterviewTest_groupByDDL" runat="server" OnSelectedIndexChanged="ViewInterviewTest_groupByDDL_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                    <asp:ListItem Text="Question" Value="QUESTION"></asp:ListItem>
                                                    <asp:ListItem Text="Category" Value="CATEGORY"></asp:ListItem>
                                                    <asp:ListItem Text="Subject" Value="SUBJECT"></asp:ListItem>
                                                    <asp:ListItem Text="Interview Area" Value="TESTAREA"></asp:ListItem>
                                                    <asp:ListItem Text="Complexity" Value="COMPLEXITY"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" valign="top">
                                                <asp:GridView ID="ViewInterviewTest_testDrftGridView" runat="server" AutoGenerateColumns="False"
                                                    SkinID="sknNewGridView" OnRowDataBound="ViewInterviewTest_testDrftGridView_RowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                                <tr>
                                                                                    <td valign="top" style="width: 5%">
                                                                                        <asp:Image ID="ViewInterviewTest_questionImage" runat="server" SkinID="sknQuestionImage"
                                                                                            ToolTip="Question" />
                                                                                        <asp:Label ID="ViewInterviewTest_rowNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                                            runat="server">
                                                                                        </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div style="word-wrap: break-word;">
                                                                                            <div>
                                                                                                <asp:Literal ID="ViewInterviewTest_questionLiteral" runat="server" Text='<%# Eval("Question") %>'></asp:Literal>
                                                                                            </div>
                                                                                            <div style="text-align: center;" id="ViewInterviewTest_questionImageDiv"
                                                                                                runat="server">
                                                                                                <asp:Image runat="server" ID="ViewInterviewTest_questionImageDisplay" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 5%">
                                                                                        <asp:Image ID="ViewInterviewTest_answerImage" ImageUrl="~/App_Themes/DefaultTheme/Images/question_answer.gif"
                                                                                            runat="server" ToolTip="Answer" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <div class="label_multi_field_text">
                                                                                            <asp:Label ID="ViewInterviewTest_answerQuestionTextBox" SkinID="sknLabelFieldText"
                                                                                                runat="server" Text='<%# Eval("Choice_Desc") %>'></asp:Label>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 5%">
                                                                                        <asp:Image ID="ViewInterviewTest_interviewQuestionImage" 
                                                                                            ImageUrl="~/App_Themes/DefaultTheme/Images/question_comments.gif"
                                                                                            runat="server" ToolTip="Comments" Visible='<%# Eval("Comments") != null &&  Eval("Comments") != string.Empty %>' />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="ViewInterviewTest_questionCommentLabel"
                                                                                            runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding-left:50px">
                                                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                                <tr>
                                                                                    <td style="width: 8%">
                                                                                        <asp:Label ID="ViewInterviewTest_categoryQuestionLabel" runat="server" Text="Category"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 6%">
                                                                                        <asp:Label ID="ViewInterviewTest_subjectQuestionLabel" runat="server" Text="Subject"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 6%">
                                                                                        <asp:Label ID="ViewInterviewTest_subjectWeightageLabel" runat="server" Text="Weightage"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 8%">
                                                                                        <asp:Label ID="ViewInterviewTest_testAreaQuestionHeadLabel" runat="server" Text="Interview Area"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 8%">
                                                                                        <asp:Label ID="ViewInterviewTest_complexityQuestionHeadLabel" runat="server" Text="Complexity"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 10%">
                                                                                        <asp:Label ID="ViewInterviewTest_categoryQuestionTextBox" SkinID="sknLabelFieldText"
                                                                                            runat="server" Text='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 10%">
                                                                                        <asp:Label ID="ViewInterviewTest_subjectQuestionTextBox" runat="server" Text='<%# Eval("SubjectName") %>'
                                                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 8%">
                                                                                        <asp:Label ID="ViewInterviewTest_subjectWeightageLabelValue" runat="server" Text='<%# Eval("Weightage") %>'
                                                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 10%">
                                                                                        <asp:Label ID="ViewInterviewTest_testAreaQuestionLabel" SkinID="sknLabelFieldText"
                                                                                            runat="server" Text='<%# Eval("TestAreaName") %>' />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="ViewInterviewTest_complexityQuestionLabel" SkinID="sknLabelFieldText"
                                                                                            runat="server" Text='<%# Eval("Complexity") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="grid_header" />
                                                    <AlternatingRowStyle CssClass="grid_001" />
                                                    <RowStyle CssClass="grid" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                </ajaxToolKit:TabContainer>
            </td>
        </tr>
        <tr>
            <td class="tr_height_25" align="center">
                <asp:Label ID="ViewInterviewTest_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="ViewInterviewTest_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 50%">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%" class="header_bg">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="ViewInterviewTest_bottomEditTestButton" runat="server" SkinID="sknButtonId"
                                            Text="Edit Interview" OnClick="ViewInterviewTest_Edit_Button_Click" CommandName="EDIT_TEST" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:Button ID="ViewInterviewTest_bottomDeleteTestButton" SkinID="sknButtonId"
                                            runat="server" Text="Delete Interview" OnClick="ViewInterviewTest_Delete_Button_Click"
                                            CommandName="DELETE_TEST" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:Button ID="ViewInterviewTest_bottomInactiveTestButton" SkinID="sknButtonId"
                                            runat="server" OnClick="ViewInterviewTest_Active_Button_Click" CommandName="DEACTIVATE_TEST"
                                            Text="Deactivate Interview" Visible="false" />
                                        <asp:Button ID="ViewInterviewTest_bottomActiveTestButton" SkinID="sknButtonId" runat="server"
                                            Text="Activate Interview" OnClick="ViewInterviewTest_Active_Button_Click" CommandName="ACTIVATE_TEST"
                                            Visible="false" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:Button ID="ViewInterviewTest_bottomCreateTestSessionButton" SkinID="sknButtonId"
                                            runat="server" Text="Create Interview Session" OnClick="ViewInterviewTest_createSessionButton_Click"
                                            CommandName="CREATE_SESSION" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:LinkButton ID="ViewInterviewTest_bottomCancelLinkButton" SkinID="sknActionLinkButton"
                                            runat="server" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="ViewInterviewTest_inactivepopupPanel" runat="server" Style="display: none"
                                CssClass="popupcontrol_confirm_remove">
                                <div style="display: none" id="ViewInterviewTest_hiddenButtonDIV">
                                    <asp:Button ID="ViewInterviewTest_hiddenButton" runat="server" />
                                </div>
                            </asp:Panel>
                            <ajaxToolKit:ModalPopupExtender ID="ViewInterviewTest_activeInActivepopupExtender"
                                runat="server" PopupControlID="ViewInterviewTest_inactivepopupPanel" TargetControlID="ViewInterviewTest_hiddenButton"
                                BackgroundCssClass="modalBackground">
                            </ajaxToolKit:ModalPopupExtender>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
