#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreateManualTestWithoutAdaptive.cs
// File that represents the user Serach the Question by various Key filed. 
// and Create the test Details and questions.
// This will helps Create a Test and have to implemeted the Certification details..

#endregion

#region Directives
using System;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Data;
using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.ExternalService;
using Forte.HCM.UI.CommonControls;
using System.IO;
using System.Web;
#endregion

namespace Forte.HCM.UI.InterviewTestMaker
{
    public partial class CreateManualInterviewTest : PageBase
    {
        #region Declaration
        string onmouseoverStyle = "className='grid_normal_row'";
        string onmouseoutStyle = "className='grid_alternate_row'";
        string sQryString = "";
        string questionID = "";
        string parentPage = string.Empty;
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        List<QuestionDetail> questionDetailSearchResultList;
        List<QuestionDetail> questionDetailTestDraftResultList;
        List<QuestionDetail> interviewQuestionDraftList;

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (CreateManualInterviewTest_mainTabContainer.ActiveTabIndex == 0)
                    Page.Form.DefaultButton = CreateManualInterviewTest_topSearchButton.UniqueID;
                else if (CreateManualInterviewTest_mainTabContainer.ActiveTabIndex == 1)
                    Page.Form.DefaultButton = CreateManualInterviewTest_bottomSaveButton.UniqueID;

               
                if (!IsPostBack)
                {
                    LoadValues();
                }

                CreateManualInterviewTest_ConfirmPopupPanel.Style.Add("height", "220px");
                Master.SetPageCaption("Create Manual Interview");
                MessageLabel_Reset();
                questionID = hiddenValue.Value;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionID))
                    MoveToTestDraft("Add");

               
                ExpandRestore();
                CreateManualInterviewTest_bottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (CreateManualInterviewTest_pagingNavigator_PageNumberClick);

                // Create events for paging control
                CreateManualInterviewTest_adaptivebottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (CreateManualInterviewTest_adaptivepagingNavigator_PageNumberClick);

                CreateManualInterviewTest_categorySubjectControl.ControlMessageThrown += 
                    new CategorySubjectControl.ControlMessageThrownDelegate(
                        CreateManualInterviewTest_categorySubjectControl_ControlMessageThrown);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// It shows error/success messages whenever the user calls this method
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        void CreateManualInterviewTest_categorySubjectControl_ControlMessageThrown(object sender, ControlMessageEventArgs c)
        {
            // Show message on the label.
            if (c.MessageType == MessageType.Error)
            {
                CreateManualInterviewTest_topErrorMessageLabel.Text = c.Message.ToString();
                CreateManualInterviewTest_bottomErrorMessageLabel.Text = c.Message.ToString();
            }
            else if (c.MessageType == MessageType.Success)
            {
                CreateManualInterviewTest_topSuccessMessageLabel.Text = c.Message.ToString();
                CreateManualInterviewTest_bottomSuccessMessageLabel.Text = c.Message.ToString();
            }
            MoveToTestDraft("");
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void CreateManualInterviewTest_okClick(object sender, EventArgs e)
        {
            try
            {
                MoveToTestDraft("Delete");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                    CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }

        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void CreateManualInterviewTest_searchQuestionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    string[] questionCollection = e.CommandArgument.ToString().Trim().Split(':');
                    CreateManualInterviewTest_previewQuestionAddHiddenField.Value = e.CommandArgument.ToString();
                    CreateManualInterviewTest_questionDetailSummaryControl.ShowAddButton = true;
                    CreateManualInterviewTest_questionDetailSummaryControl.LoadQuestionDetails
                        (questionCollection[0], int.Parse(questionCollection[1]));
                    CreateManualInterviewTest_questionDetailSummaryControl.Visible = true;
                    CreateManualInterviewTest_questionModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }
        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Add  Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel the Event will be raised.
        /// </remarks>
        protected void CreateManualInterviewTest_questionDetailPreviewControl_AddClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = CreateManualInterviewTest_previewQuestionAddHiddenField.Value;
                CreateManualInterviewTest_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("Add");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Cancel Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel the Event will be raised.
        /// </remarks>
        protected void CreateManualInterviewTest_cancelClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = "";
                CreateManualInterviewTest_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("");
                //BindAdaptiveGridStateMaintain();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CreateManualInterviewTest_searchQuestionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField CreateManualInterviewTest_hidddenValue = (HiddenField)e.Row.FindControl("CreateManualInterviewTest_hidddenValue");
                    string questionID = CreateManualInterviewTest_hidddenValue.Value.Split(':')[0].ToString();

                    ImageButton alertImageButton = (ImageButton)e.Row.FindControl("CreateManualInterviewTest_searchQuestionAlertImageButton");
                    alertImageButton.Attributes.Add("OnClick", "javascript:return LoadFlagQuestion('" + questionID + "');");

                    Image img = (Image)e.Row.FindControl("CreateManualInterviewTest_searchQuestionSelectImage");
                    img.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + CreateManualInterviewTest_searchQuestionGridView.ClientID + "');");
                    img.Attributes.Add("OnClick", "javascript:return singleqnsclick('" + CreateManualInterviewTest_hidddenValue.Value + "','" + CreateManualInterviewTest_selectMultipleImage.ClientID + "');");

                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);
                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }


        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CreateManualInterviewTest_testDrftGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ImageButton dowmImageButton = (ImageButton)e.Row.FindControl
                        ("CreateManualInterviewTest_byQuestion_moveDownQuestionImageButton");

                    ImageButton upImageButton = (ImageButton)e.Row.FindControl
                        ("CreateManualInterviewTest_byQuestion_moveUpQuestionImageButton");

                    HiddenField displayOrderHiddenField = (HiddenField)e.Row.FindControl
                        ("CreateManualInterviewTest_displayOrderHiddenField");

                    if (e.Row.RowIndex == 0)
                    {
                        upImageButton.Visible = false;
                    }
                    Image img = (Image)e.Row.FindControl("CreateManualInterviewTest_testDrftRemoveImage");

                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);
                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void CreateManualInterviewTest_testDrftGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    CreateManualInterviewTest_questionModalPopupExtender.Show();
                }
                else if (e.CommandName == "Select")
                {
                    hiddenDeleteValue.Value = e.CommandArgument.ToString();
                    CreateManualInterviewTest_ConfirmPopupExtenderControl.Message = string.Format(Resources.HCMResource.SearchQuestion_Delete_Confirm_Message, e.CommandArgument.ToString().Split(':')[0]);
                    CreateManualInterviewTest_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    CreateManualInterviewTest_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    CreateManualInterviewTest_ConfirmPopupExtender.Show();
                }
                else if ((e.CommandName == "MoveDown") || (e.CommandName == "MoveUp"))
                {

                    interviewQuestionDraftList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                    
                    int order = int.Parse(e.CommandArgument.ToString());
                    //if the command name is move down , move the record one line down 
                    if (e.CommandName == "MoveDown")
                    {
                        foreach (QuestionDetail question in interviewQuestionDraftList)
                        {
                            //Change the display order of the draft list
                            if (question.DisplayOrder == order)
                            {
                                question.DisplayOrder = order + 1;
                                continue;
                            }
                            if (question.DisplayOrder == order + 1)
                            {
                                question.DisplayOrder = order;
                                continue;
                            }
                        }
                    }

                    //if the command name is moveup, move the record one line up
                    if (e.CommandName == "MoveUp")
                    {
                        foreach (QuestionDetail question in interviewQuestionDraftList)
                        {
                            if (question.DisplayOrder == order - 1)
                            {
                                question.DisplayOrder = order;
                                continue;
                            }
                            if (question.DisplayOrder == order)
                            {
                                question.DisplayOrder = order - 1;
                                continue;
                            }
                        }
                    }
                    CreateManualInterviewTest_testDrftGridView.DataSource = interviewQuestionDraftList.OrderBy(p => p.DisplayOrder);
                    CreateManualInterviewTest_testDrftGridView.DataBind();
                    ViewState["TESTDRAFTGRID"] = interviewQuestionDraftList.OrderBy(p => p.DisplayOrder).ToList();
                }
                HideLastRecord();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }


        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void CreateManualInterviewTest_searchQuestionButton_Click(object sender, EventArgs e)
        {
            try
            {
                MoveToTestDraft("");
                CreateManualInterviewTest_bottomPagingNavigator.Reset();
                LoadQuestion(1, ViewState["SORTDIRECTIONKEY"].ToString(),
                     ViewState["SORTEXPRESSION"].ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the Move to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the move the Selected Question to Test Draft
        /// grid.
        /// </remarks>
        protected void CreateManualInterviewTest_selectMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                AddSearchQuestionToTestDraft();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the Remove to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the delete the Selected Question from Test Draft
        /// grid.
        /// </remarks>
        protected void CreateManualInterviewTest_removeMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool questionSelected = false;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
                {
                    foreach (GridViewRow row in CreateManualInterviewTest_testDrftGridView.Rows)
                    {
                        CheckBox CreateManualInterviewTest_testDrftCheckbox = (CheckBox)row.FindControl("CreateManualInterviewTest_testDrftCheckbox");
                        HiddenField CreateManualInterviewTest_questionKeyHiddenField = (HiddenField)row.FindControl("CreateManualInterviewTest_questionKeyHiddenField");
                        if (CreateManualInterviewTest_testDrftCheckbox.Checked)
                        {
                            questionID = questionID + CreateManualInterviewTest_questionKeyHiddenField.Value + ",";
                            questionSelected = true;
                        }
                    }
                }
                if (questionSelected)
                {
                    hiddenDeleteValue.Value = questionID.TrimEnd(',');
                    CreateManualInterviewTest_ConfirmPopupExtenderControl.Message = Resources.HCMResource.SearchQuestion_Delete_Multiple_Confirm_Message;
                    CreateManualInterviewTest_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    CreateManualInterviewTest_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    CreateManualInterviewTest_ConfirmPopupExtender.Show();
                }
                else
                {
                    MoveToTestDraft("");
                    base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                                       CreateManualInterviewTest_bottomErrorMessageLabel, Resources.HCMResource.CreateManualInterviewTest_Question_From_InterviewTestDraft_Result);

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Interchange Advanced and Simple Search Link 
        /// and Search Criteria fields 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateManualInterviewTest_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (CreateManualInterviewTest_simpleLinkButton.Text.ToLower() == "simple")
                {
                    CreateManualInterviewTest_simpleLinkButton.Text = "Advanced";
                    CreateManualInterviewTest_simpleSearchDiv.Visible = true;
                    CreateManualInterviewTest_advanceSearchDiv.Visible = false;
                }
                else
                {
                    CreateManualInterviewTest_simpleLinkButton.Text = "Simple";
                    CreateManualInterviewTest_simpleSearchDiv.Visible = false;
                    CreateManualInterviewTest_advanceSearchDiv.Visible = true;
                }
                MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateManualInterviewTest_pagingNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                MoveToTestDraft("");
                ViewState["PAGENUMBER"] = e.PageNumber;
                LoadQuestion(e.PageNumber, ViewState["SORTDIRECTIONKEY"].ToString(),
                    ViewState["SORTEXPRESSION"].ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Create Test and Test Question 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CreateManualInterviewTest_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if feature usage limit exceeds for creating test.
                if (new AdminBLManager().IsFeatureUsageLimitExceeds(base.tenantID, Constants.FeatureConstants.CREATE_TEST))
                {
                    base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                        CreateManualInterviewTest_bottomErrorMessageLabel, 
                        "You have reached the maximum limit based your subscription plan. Cannot create more interviews");
                    return;
                }

                if (!IsValidData())
                    return;

                ShowSaveConfirmPopUp();
            } 
        catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
                    questionDetailTestDraftResultList = null;
            }
        }


        


        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void CreateManualInterviewTest_resetLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Handler method that will be called when the With Adaptive button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void CreateManualInterviewTest_adptiveTestLinkButton_Click(object sender, EventArgs e)
        {
            if (!Support.Utility.IsNullOrEmpty(Request.QueryString.Get("parentpage")))
                parentPage = "&parentpage=" + Request.QueryString.Get("parentpage");
            Response.Redirect(Constants.TestMakerConstants.CREATE_INTERVIEW_TEST_WA + "?m=1&s=1" + parentPage, false);
        }


        /// <summary>
        /// Handler that helps to save the posted file in session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CreateManualInterviewTest_questionImageOnUploadComplete(object sender,
            AsyncFileUploadEventArgs e)
        {
            System.Threading.Thread.Sleep(500);
            if (CreateManualInterviewTest_questionImageUpload.PostedFile != null)
            {
                if (CreateManualInterviewTest_questionImageUpload.PostedFile.ContentLength > 102400) // check if image size exceeds 100kb
                {
                    base.ShowMessage(CreateManualInterviewTest_addQuestionErrorLabel,
                        Resources.HCMResource.BatchQuestionEntry_imageSizeExceeded);
                    CreateManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }

                // Check if the photo is of the desired extension.
                string extension = Path.GetExtension(CreateManualInterviewTest_questionImageUpload.FileName);
                extension = extension.ToLower();

                if (extension != ".gif" && extension != ".png" && extension != ".jpg" && extension != ".jpeg")
                {
                    base.ShowMessage(CreateManualInterviewTest_addQuestionErrorLabel, "Only gif/png/jpg/jpeg files are allowed");
                    CreateManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }

                // Read the bytes from the posted file.
                HttpPostedFile postedQstImage = CreateManualInterviewTest_questionImageUpload.PostedFile;
                int nFileLen = postedQstImage.ContentLength;
                byte[] imgData = new byte[nFileLen];

                postedQstImage.InputStream.Read(imgData, 0, nFileLen);
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = imgData;

                CreateManualInterviewTest_addQuestioModalPopupExtender.Show();
                return;
            }
            else
            {
                base.ShowMessage(CreateManualInterviewTest_addQuestionErrorLabel, "Please select valid image to upload");
                CreateManualInterviewTest_addQuestioModalPopupExtender.Show();
                return;
            }
        }

        /// <summary>
        /// Handler that helps to show the preview of the saved image
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>   
        protected void CreateManualInterviewTest_questionImageButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["POSTED_INTERVIEW_QUESTION_IMAGE"] != null)
                {
                    CreateManualInterviewTest_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=INTERVIEW_QUESTION_IMAGE";
                    CreateManualInterviewTest_AddQuestionImageTR.Style.Add("display", "none");
                    CreateManualInterviewTest_DisplayQuestionImageTR.Style.Add("display", "");
                    CreateManualInterviewTest_addImageLinkButton.Style.Add("display", "none");
                    CreateManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler that helps to delete the posted file.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateManualInterviewTest_deleteLinkButtonClick(object sender, EventArgs e)
        {
            try
            {
                ResetQuestionImage();
                CreateManualInterviewTest_addQuestioModalPopupExtender.Show();
                return;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                    exp.Message);
            }
        }

        #endregion

        #region Private  Methods

        /// <summary>
        /// Method to reset the question image
        /// </summary>
        private void ResetQuestionImage()
        {
            Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            CreateManualInterviewTest_questionImage.ImageUrl = string.Empty;
            CreateManualInterviewTest_AddQuestionImageTR.Style.Add("display", "none");
            CreateManualInterviewTest_DisplayQuestionImageTR.Style.Add("display", "none");
            CreateManualInterviewTest_addImageLinkButton.Style.Add("display", "");
        }

        /// <summary>
        /// Method to call the review rating page
        /// </summary>
        private void SaveQuestions()
        {
            try
            {
                questionDetailTestDraftResultList = new List<QuestionDetail>();
                questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

                List<decimal> creditEarned = new List<decimal>();
                List<decimal> timeTaken = new List<decimal>();
                string testKey = "";
                int questionCount = questionDetailTestDraftResultList.Count;

                TestDetail testDetail = new TestDetail();
                testDetail = CreateManualInterviewTest_testDetailsUserControl.TestDetailDataSource;

                testDetail.Questions = questionDetailTestDraftResultList;
                testDetail.IsActive = true;
                testDetail.CertificationId = 0;
                testDetail.CreatedBy = userID;
                testDetail.IsDeleted = false;
                testDetail.ModifiedBy = userID;
                testDetail.NoOfQuestions = questionCount;
                testDetail.TestAuthorID = userID;
                testDetail.TestCreationDate = DateTime.Now;
                testDetail.TestStatus = TestStatus.Active;
                testDetail.TestType = TestType.Genenal;
                testDetail.TestMode = "MANUAL";
                testDetail.TestKey = testKey;

                InterviewDetail InterviewDetail = new InterviewDetail();
                InterviewDetail.InterviewTestKey = testDetail.TestKey;
                InterviewDetail.InterviewName = testDetail.Name;
                InterviewDetail.InterviewDescription = testDetail.Description;
                InterviewDetail.PositionProfileId = Convert.ToInt32(testDetail.PositionProfileID);
                InterviewDetail.PositionProfileName = testDetail.PositionProfileName;
                InterviewDetail.TestAuthor = testDetail.TestAuthorName;
                InterviewDetail.SessionIncludedActive = testDetail.SessionIncludedActive;
                InterviewDetail.TestAuthorID = testDetail.TestAuthorID; ;
                InterviewDetail.SessionIncluded = testDetail.SessionIncluded;

                Session["interviewDetail"] = (InterviewDetail)InterviewDetail;
                Session["controlTestDetail"] = (TestDetail)testDetail;
                Session["questionDetailTestDraftResultList"] = questionDetailTestDraftResultList as List<QuestionDetail>;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
                    questionDetailTestDraftResultList = null;
            }
        }

        private void SaveInteviewQuestionsWithDefaultSettings()
        {
            try
            {
                questionDetailTestDraftResultList = new List<QuestionDetail>();
                
                //Assign Weightage for default settings
                AssignDefault((ViewState["TESTDRAFTGRID"] as List<QuestionDetail>).Count);
                //End Weightage

                questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

                List<decimal> creditEarned = new List<decimal>();
                List<decimal> timeTaken = new List<decimal>();
                string testKey = "";
                int questionCount = questionDetailTestDraftResultList.Count;

                TestDetail testDetail = new TestDetail();
                testDetail = CreateManualInterviewTest_testDetailsUserControl.TestDetailDataSource;

                testDetail.Questions = questionDetailTestDraftResultList;
                testDetail.IsActive = true;
                testDetail.CertificationId = 0;
                testDetail.CreatedBy = userID;
                testDetail.IsDeleted = false;
                testDetail.ModifiedBy = userID;
                testDetail.NoOfQuestions = questionCount;
                testDetail.TestAuthorID = userID;
                testDetail.TestCreationDate = DateTime.Now;
                testDetail.TestStatus = TestStatus.Active;
                testDetail.TestType = TestType.Genenal;
                testDetail.TestMode = "MANUAL";
                testDetail.TestKey = testKey;

                testKey = new InterviewBLManager().SaveInterviewTest(testDetail, userID,
                    new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY,
                    questionDetailTestDraftResultList).AttributeID.ToString());

                CreateManualInterviewTest_interviewTestKeyHiddenField.Value = testKey;

              
               // Show a success message.
                base.ShowMessage(CreateManualInterviewTest_topSuccessMessageLabel,
                    CreateManualInterviewTest_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.CreateAutomaticInterviewTest_TestSuccessfullMessage,
                     testDetail.TestKey));

                CreateManualInterviewTest_testDetailsUserControl.TestDetailDataSource = testDetail;

                // Send a mail to the position profile creator.
                try
                {
                    if (testDetail.PositionProfileID != 0 && testDetail.PositionProfileID!=null)
                    {
                        new EmailHandler().SendMail(EntityType.InterviewCreatedForPositionProfile,
                            testDetail.TestKey);
                    }
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }

                Response.Redirect(Constants.TestMakerConstants.EDIT_INTERVIEW_TEST + "?m=1&s=1&mode=new&parentpage=INT_T_MANU_QUST" +
                "&testkey=" + testKey, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
                    questionDetailTestDraftResultList = null;
            }
        }

        /// <summary>
        /// This method allocates default weightages,limit and ratings to the questions
        /// and ensures that the total weightage is 100% or not.
        /// </summary>
        /// <param name="NoofQuestions">No. of question</param>
        private void AssignDefault(int NoofQuestions)
        {
            DataRow[] drWeightage = null;
            int WeightagePercentage = 0;
            int ToAddExtraWeightage = 0;
            int index = 0;
            try
            {
                interviewQuestionDraftList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                WeightagePercentage = 100 / NoofQuestions;
                ToAddExtraWeightage = 100 - (WeightagePercentage * NoofQuestions);

                interviewQuestionDraftList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                foreach (QuestionDetail question in interviewQuestionDraftList)
                {
                    question.Weightage = WeightagePercentage;
                    question.InterviewTestRatings = 10;
                    question.TimeLimit = 300;
                    if ((ToAddExtraWeightage > 0) && (WeightagePercentage != 0)
                        && (index == NoofQuestions - 1))
                        question.Weightage = question.Weightage + ToAddExtraWeightage;
                    index++;
                }
                ViewState["TESTDRAFTGRID"] = interviewQuestionDraftList; 
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drWeightage)) drWeightage = null;
            }
        }

        /// <summary>
        /// Add & remove Question from the Test Draft.
        /// </summary>
        private void AddSearchQuestionToTestDraft()
        {
            bool questionSelected = false;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
            {
                foreach (GridViewRow row in CreateManualInterviewTest_searchQuestionGridView.Rows)
                {
                    CheckBox CreateManualInterviewTest_searchQuestionCheckbox = (CheckBox)row.FindControl("CreateManualInterviewTest_searchQuestionCheckbox");
                    HiddenField CreateManualInterviewTest_questionKeyHiddenField = (HiddenField)row.FindControl("CreateManualInterviewTest_questionKeyHiddenField");
                    if (CreateManualInterviewTest_searchQuestionCheckbox.Checked)
                    {
                        questionID = questionID + CreateManualInterviewTest_questionKeyHiddenField.Value + ",";
                        questionSelected = true;
                    }
                }
                hiddenValue.Value = questionID.TrimEnd(',');

            }
            if (questionSelected)
            {
                MoveToTestDraft("Add");
            }
            else
            {
                MoveToTestDraft("");
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                                   CreateManualInterviewTest_bottomErrorMessageLabel, "Select atleast one question from search results");
            }
        }

        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>                                         
        private void ExpandRestore()
        {
            if (CreateManualInterviewTest_restoreHiddenField.Value == "Y")
            {
                CreateManualInterviewTest_searchCriteriasDiv.Style["display"] = "none";
                CreateManualInterviewTest_searchResultsUpSpan.Style["display"] = "block";
                CreateManualInterviewTest_searchResultsDownSpan.Style["display"] = "none";
                CreateManualInterviewTest_questionDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                CreateManualInterviewTest_searchCriteriasDiv.Style["display"] = "block";
                CreateManualInterviewTest_searchResultsUpSpan.Style["display"] = "none";
                CreateManualInterviewTest_searchResultsDownSpan.Style["display"] = "block";
                CreateManualInterviewTest_questionDiv.Style["height"] = RESTORED_HEIGHT;
            }
            CreateManualInterviewTest_searchResultsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                CreateManualInterviewTest_questionDiv.ClientID + "','" +
                CreateManualInterviewTest_searchCriteriasDiv.ClientID + "','" +
                CreateManualInterviewTest_searchResultsUpSpan.ClientID + "','" +
                CreateManualInterviewTest_searchResultsDownSpan.ClientID + "','" +
                CreateManualInterviewTest_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
        }

        /// <summary>
        /// Reset the Messeags.
        /// </summary>
        private void MessageLabel_Reset()
        {
            CreateManualInterviewTest_topErrorMessageLabel.Text = string.Empty;
            CreateManualInterviewTest_bottomErrorMessageLabel.Text = string.Empty;
            CreateManualInterviewTest_topSuccessMessageLabel.Text = string.Empty;
            CreateManualInterviewTest_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Get Search Results with Paging and Sorting. 
        /// </summary>
        /// <param name="pageNumber">Current Page number </param>
        /// <param name="orderBy">QuestionKey/ModifiedDate/Complexity/Question Description/Credids Earned</param>
        /// <param name="sortDirection">Descending/Assending</param>
        private void LoadQuestion(int pageNumber, string orderBy, string sortDirection)
        {
            hiddenValue.Value = null;
            int totalRecords = 0;
            StringBuilder complexityID = GetSelectedComplexityID();
            StringBuilder testAreaID = GetTestAreaID();

            QuestionDetailSearchCriteria questionDetailSearchCriteria = new QuestionDetailSearchCriteria();

            if (CreateManualInterviewTest_simpleLinkButton.Text == "Advanced")
            {
                questionDetailSearchCriteria.CategoryName =
                                    CreateManualInterviewTest_categoryTextBox.Text.Trim() == "" ?
                                    null : CreateManualInterviewTest_categoryTextBox.Text.Trim();
                questionDetailSearchCriteria.SubjectName =
                                    CreateManualInterviewTest_subjectTextBox.Text.Trim() == "" ?
                                    null : CreateManualInterviewTest_subjectTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    CreateManualInterviewTest_keywordsTextBox.Text.Trim() == "" ?
                                    null : CreateManualInterviewTest_keywordsTextBox.Text.Trim();
                questionDetailSearchCriteria.Author = base.userID;
            }
            else
            {
                string SelectedSubjectIDs = "";
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(CreateManualInterviewTest_categorySubjectControl.SubjectDataSource))
                {
                    List<Category> categoryList = new List<Category>();
                    foreach (Subject subject in CreateManualInterviewTest_categorySubjectControl.SubjectDataSource)
                    {

                        if (subject.IsSelected == true)
                        {
                            Category category = new Category();
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                            category.CategoryID = subject.CategoryID;
                            categoryList.Add(category);
                        }

                    }
                    ///Get all Subject ids in  Un selected Subject List.
                    Category categoryID = new Category();
                    foreach (Subject subject in CreateManualInterviewTest_categorySubjectControl.SubjectDataSource)
                    {
                        categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                        if (categoryID == null)
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                    }
                }
                questionDetailSearchCriteria.CategorySubjectsKey =
                                    SelectedSubjectIDs == "" ?
                                    null : SelectedSubjectIDs.TrimEnd(',');
                questionDetailSearchCriteria.TestAreaID =
                                    testAreaID.ToString() == "" ?
                                    null : testAreaID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.Complexities =
                                    complexityID.ToString() == "" ?
                                    null : complexityID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.QuestionKey =
                                    CreateManualInterviewTest_questionIDTextBox.Text.Trim() == "" ?
                                    null : CreateManualInterviewTest_questionIDTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    CreateManualInterviewTest_keywordTextBox.Text.Trim() == "" ?
                                    null : CreateManualInterviewTest_keywordTextBox.Text.Trim();
                questionDetailSearchCriteria.AuthorName =
                                    CreateManualInterviewTest_authorTextBox.Text.Trim() == "" ?
                                    null : CreateManualInterviewTest_authorIdHiddenField.Value;

                questionDetailSearchCriteria.ActiveFlag = null;

                questionDetailSearchCriteria.ClientRequest = CreateManualInterviewTest_positionProfileKeywordTextBox.Text.Trim() == "" ?
                   null : CreateManualInterviewTest_positionProfileKeywordTextBox.Text.Trim();

                questionDetailSearchCriteria.Author = base.userID;
            }

            List<QuestionDetail> questionDetail = new List<QuestionDetail>();

            questionDetail = new InterviewBLManager().GetInterviewSearchQuestions(questionDetailSearchCriteria, 
                base.GridPageSize, pageNumber, orderBy, sortDirection, out totalRecords);

            if (questionDetail == null || questionDetail.Count == 0)
            {
                CreateManualInterviewTest_searchQuestionGridView.DataSource = null;
                CreateManualInterviewTest_searchQuestionGridView.DataBind();
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                 CreateManualInterviewTest_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                CreateManualInterviewTest_bottomPagingNavigator.TotalRecords = 0;
                CreateManualInterviewTest_questionDiv.Visible = false;
            }
            else
            {
                CreateManualInterviewTest_searchQuestionGridView.DataSource = questionDetail;
                CreateManualInterviewTest_searchQuestionGridView.DataBind();
                CreateManualInterviewTest_bottomPagingNavigator.PageSize = base.GridPageSize;
                CreateManualInterviewTest_bottomPagingNavigator.TotalRecords = totalRecords;
                CreateManualInterviewTest_questionDiv.Visible = true;
            }
            ViewState["SEARCHRESULTGRID"] = questionDetail;
            // return questionDetail;
        }
        /// <summary>
        ///  Bind complexities/attributes based on the attribute type
        /// </summary>
        private void BindComplexities()
        {
            CreateManualInterviewTest_complexityCheckBoxList.DataSource =
                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
            CreateManualInterviewTest_complexityCheckBoxList.DataTextField = "AttributeName";
            CreateManualInterviewTest_complexityCheckBoxList.DataValueField = "AttributeID";
            CreateManualInterviewTest_complexityCheckBoxList.DataBind();
        }
        /// <summary>
        /// Bind test area based on TEST_AREA attribute type
        /// </summary>
        private void BindTestArea()
        {

            CreateManualInterviewTest_testAreaCheckBoxList.DataSource =
               new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
            CreateManualInterviewTest_testAreaCheckBoxList.DataTextField = "AttributeName";
            CreateManualInterviewTest_testAreaCheckBoxList.DataValueField = "AttributeID";
            CreateManualInterviewTest_testAreaCheckBoxList.DataBind();
        }

        /// <summary>
        /// Get Selected Test Area IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetTestAreaID()
        {
            StringBuilder testAreaID = new StringBuilder();
            for (int i = 0; i < CreateManualInterviewTest_testAreaCheckBoxList.Items.Count; i++)
            {

                if (CreateManualInterviewTest_testAreaCheckBoxList.Items[i].Selected)
                {

                    testAreaID.Append(CreateManualInterviewTest_testAreaCheckBoxList.Items[i].Value.ToString());
                    testAreaID.Append(",");
                }

            }
            return testAreaID;
        }
        /// <summary>
        /// Get Selected Complexity IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetSelectedComplexityID()
        {
            StringBuilder complexityID = new StringBuilder();
            for (int i = 0; i < CreateManualInterviewTest_complexityCheckBoxList.Items.Count; i++)
            {
                if (CreateManualInterviewTest_complexityCheckBoxList.Items[i].Selected)
                {

                    complexityID.Append(CreateManualInterviewTest_complexityCheckBoxList.Items[i].Value.ToString());
                    complexityID.Append(",");
                }
            }
            return complexityID;
        }
        /// <summary>
        /// Uncheck the checkbox 
        /// </summary>
        private void UncheckCheckBox()
        {
            foreach (GridViewRow row in CreateManualInterviewTest_searchQuestionGridView.Rows)
            {
                if (((CheckBox)row.FindControl("CreateManualInterviewTest_searchQuestionCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("CreateManualInterviewTest_searchQuestionCheckbox")).Checked = false;
                }
            }
            foreach (GridViewRow row in CreateManualInterviewTest_adaptiveGridView.Rows)
            {
                if (((CheckBox)row.FindControl("CreateManualInterviewTest_adaptiveCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("CreateManualInterviewTest_adaptiveCheckbox")).Checked = false;
                }
            }

        }
        /// <summary>
        /// add and remove the Test Draft 
        /// </summary>
        /// <param name="action"></param>
        private void MoveToTestDraft(string action)
        {
            questionDetailTestDraftResultList = new List<QuestionDetail>();
            QuestionDetail questionDetailSearchResult = new QuestionDetail();

            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenDeleteValue.Value))
            {
                hiddenValue.Value = hiddenDeleteValue.Value;
                hiddenDeleteValue.Value = null;
            }
            string addedQuestionID = "";
            string notAddedQuestion = "";
            bool alertFlag = false;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
            {
                questionDetailSearchResultList = new List<QuestionDetail>();
                questionDetailSearchResultList = ViewState["SEARCHRESULTGRID"] as List<QuestionDetail>;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
            {

                string[] questionIds = hiddenValue.Value.Split(',');

                foreach (string ids in questionIds)
                {
                    if (ids != "")
                    {
                        string[] idList = ids.Split(':');
                        string id = idList[0];
                        string relationId = idList[1];

                        if (ViewState["TESTDRAFTGRID"] != null)
                        {
                            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                            if (action == "Add")
                            {
                                if (!questionDetailTestDraftResultList.Exists(p => p.QuestionKey.Trim() == id.Trim()))
                                {
                                    MessageLabel_Reset();
                                    questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                    questionDetailSearchResult.DisplayOrder = questionDetailTestDraftResultList.Count + 1;
                                    List<AnswerChoice> questionAnswerList = new BL.QuestionBLManager().GetInterviewQuestionOptions(id);
                                    if(questionAnswerList !=null)
                                        questionDetailSearchResult.Choice_Desc += questionAnswerList[0].Choice.ToString();
                                    questionDetailSearchResult.ComplexityName = questionDetailSearchResult.Complexity;
                                    questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                                    ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                                    addedQuestionID = addedQuestionID + id + ", ";
                                    base.ShowMessage(CreateManualInterviewTest_topSuccessMessageLabel,
                                        CreateManualInterviewTest_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToInterviewDraft);
                                }
                                else
                                {
                                    notAddedQuestion = notAddedQuestion + id + ", ";
                                    alertFlag = true;
                                }
                            }
                            else if (action == "Delete")
                            {
                                MessageLabel_Reset();
                                questionDetailSearchResult = questionDetailTestDraftResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                questionDetailTestDraftResultList.Remove(questionDetailSearchResult);
                                base.ShowMessage(CreateManualInterviewTest_topSuccessMessageLabel,
                                    CreateManualInterviewTest_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualInterviewTest_RemoveToTestDraft);
                                ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                            }
                        }
                        else
                        {
                            questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                            questionDetailSearchResult.DisplayOrder = 1;
                            List<AnswerChoice> questionAnswerList = new BL.QuestionBLManager().GetInterviewQuestionOptions(id);
                            if (questionAnswerList != null)
                                questionDetailSearchResult.Choice_Desc += questionAnswerList[0].Choice.ToString();
                            questionDetailSearchResult.ComplexityName = questionDetailSearchResult.Complexity;
                            questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                            base.ShowMessage(CreateManualInterviewTest_topSuccessMessageLabel,
                                       CreateManualInterviewTest_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToInterviewDraft);
                            ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                        }
                    }
                }
            }
            
            if (alertFlag)
            {
                string alertMessage = string.Format(Resources.HCMResource.CreateManualInterviewTest_QuestionExist, notAddedQuestion.Trim().TrimEnd(','));
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(addedQuestionID))
                {
                    alertMessage = alertMessage + "<br/>" + string.Format(Resources.HCMResource.CreateManualTest_QuestionAddedToTestDraft, addedQuestionID.Trim().TrimEnd(','));
                }
                CreateManualInterviewTest_ConfirmPopupPanel.Style.Add("height", "270px");
                CreateManualInterviewTest_ConfirmPopupExtenderControl.Type = MessageBoxType.Ok;
                CreateManualInterviewTest_ConfirmPopupExtenderControl.Title = "Warning";
                CreateManualInterviewTest_ConfirmPopupExtenderControl.Message = alertMessage;
                CreateManualInterviewTest_ConfirmPopupExtender.Show();
            }
            hiddenValue.Value = null;

            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            CreateManualInterviewTest_testDrftGridView.DataSource = questionDetailTestDraftResultList;
            CreateManualInterviewTest_testDrftGridView.DataBind();
            if (questionDetailSearchResultList != null)
            {
                CreateManualInterviewTest_searchQuestionGridView.DataSource = questionDetailSearchResultList;
                CreateManualInterviewTest_questionDiv.Visible = true;
            }
            else
            {
                CreateManualInterviewTest_searchQuestionGridView.DataSource = null;
                CreateManualInterviewTest_questionDiv.Visible = false;
            }

            CreateManualInterviewTest_searchQuestionGridView.DataBind();
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
            {
                if (questionDetailTestDraftResultList.Count != 0)
                {
                    if (action != "")
                    {
                        questionDetailTestDraftResultList = new List<QuestionDetail>();
                        questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                    }
                    TestDraftSummary testDraftSummary = new TestDraftSummary();
                    testDraftSummary.QuestionDetail = questionDetailTestDraftResultList;
                    testDraftSummary.AttributeDetail = new ControlUtility().
                        GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetailTestDraftResultList);
                    CreateManualInterviewTest_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;
                }
            }
            UncheckCheckBox();
            HideLastRecord();
        }
        /// <summary>
        /// Hide move down image of the last record
        /// </summary>
        private void HideLastRecord()
        {
            foreach (GridViewRow row in CreateManualInterviewTest_testDrftGridView.Rows)
            {
                if (row.RowIndex == CreateManualInterviewTest_testDrftGridView.Rows.Count - 1)
                {
                    ImageButton dowmImageButton = (ImageButton)row.FindControl
                        ("CreateManualInterviewTest_byQuestion_moveDownQuestionImageButton");
                    dowmImageButton.Visible = false;
                }
            }
        }

        /// <summary>
        /// Saves the new question.
        /// </summary>
        private void SaveNewQuestion()
        {
            List<QuestionDetail> questions = null;
            QuestionDetail questionDetail = null;
            QuestionBLManager questionBLManager = new QuestionBLManager();

            questionDetail = new QuestionDetail();

            if (ViewState["TESTDRAFTGRID"] == null)
                questions = new List<QuestionDetail>();
            else
                questions = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

            questionDetail = ConstructQuestionDetails();
            new QuestionBLManager().SaveInterviewQuestion(questionDetail);
            questionDetail.QuestionRelationId = questionBLManager.GetQuestionRelationID(questionDetail.QuestionKey);
            questionDetail.CategoryName = questionBLManager.GetCategory(questionDetail.QuestionKey);
            questionDetail.SubjectName = questionBLManager.GetSubject(questionDetail.QuestionKey);
            questionDetail.Complexity = questionDetail.ComplexityName;
            if(questions!=null)
                questionDetail.DisplayOrder = questions.Count + 1;
            else
                questionDetail.DisplayOrder = 1;
            List<AnswerChoice> questionAnswerList = new BL.QuestionBLManager().
                GetInterviewQuestionOptions(questionDetail.QuestionKey);
            if (questionAnswerList != null)
                questionDetail.Choice_Desc += questionAnswerList[0].Choice.ToString();

            questions.Add(questionDetail);
            ViewState["TESTDRAFTGRID"] = questions.ToList();

            CreateManualInterviewTest_testDrftGridView.DataSource = questions;
            CreateManualInterviewTest_testDrftGridView.DataBind();

            TestDraftSummary testDraftSummary = new TestDraftSummary();
            testDraftSummary.QuestionDetail = questions;
            testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questions);
            CreateManualInterviewTest_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;
            HideLastRecord();
            ResetQuestionImage();
        }

        /// <summary>
        /// Constructs the question details.
        /// </summary>
        /// <returns></returns>
        private QuestionDetail ConstructQuestionDetails()
        {
            // Construct question detail object.
            QuestionDetail questionDetail = new QuestionDetail();

            QuestionBLManager questionBLManager = new QuestionBLManager();

            questionDetail.Question = CreateManualInterviewTest_addQuestionQuestTextBox.Text.Trim();

            if (CreateManualInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Value != null &&
                CreateManualInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Value.Trim().ToUpper() != "--SELECT--")
            {
                questionDetail.Complexity = CreateManualInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Value;
                questionDetail.ComplexityName = CreateManualInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Text;
            }

            //Set question type

            if (CreateManualInterviewTest_isQuestionRepository.Checked)
                questionDetail.IsActiveFlag = "Y";
            else
                questionDetail.IsActiveFlag = "N";

            // Construct the answer choice.
            List<AnswerChoice> answerChoices = new List<AnswerChoice>();
            AnswerChoice answerChoice = null;


            answerChoice = new AnswerChoice();
            answerChoice.ChoiceID = 1;
            answerChoice.Choice = CreateManualInterviewTest_addQuestionAnswerTextBox.Text.Trim();
            answerChoices.Add(answerChoice);

            questionDetail.AnswerChoices = answerChoices;
            questionDetail.Author = Convert.ToInt32(base.userID);

            // Additional settings (Test AreaId).

            questionDetail.TestAreaID = CreateManualInterviewTest_addQuestionRadioButtonList.SelectedValue;
            questionDetail.TestAreaName = CreateManualInterviewTest_addQuestionRadioButtonList.SelectedItem.Text;

            questionDetail.Tag = CreateManualInterviewTest_tagsTextBox.Text;

            questionDetail.CreatedBy = Convert.ToInt32(base.userID);
            questionDetail.CreatedDate = DateTime.Now;

            questionDetail.ModifiedBy = Convert.ToInt32(base.userID);
            
            // Get selected subject ids only. This will be used in QuestionBLManager:InsertQuestionRelation
            // method to insert multiple subjects simultanesouly.
            questionDetail.SelectedSubjectIDs = CreateManualInterviewAddQuestion_byQuestion_categoryHiddenField.Value.ToString();

            if (Session["POSTED_INTERVIEW_QUESTION_IMAGE"] != null)
            {
                questionDetail.QuestionImage = Session["POSTED_INTERVIEW_QUESTION_IMAGE"] as byte[];
                questionDetail.HasImage = true;
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            }
            else
                questionDetail.HasImage = false;

             return questionDetail;
        }

        /// <summary>
        /// This method Invokes Confirm pop up when user tries to save
        /// the less number of questions in the test draft panel with the
        /// number of questions provided.
        /// </summary>
        /// 
        private void ShowSaveConfirmPopUp()
        {
            CreateManualInterviewTestSave_confirmPopupExtenderControl.Title = "Save Options";
            CreateManualInterviewTestSave_confirmPopupExtenderControl.Type = MessageBoxType.OkCancel;
            CreateManualInterviewTestSave_confirmPopupExtenderControl.DataBind();
            CreateManualInterviewTest_saveTestModalPopupExtender.Show();
        }
        #endregion

        #region Protected Overridden Methods


        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and 
        /// OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void CreateManualInterviewTest_okPopUpClick(object sender, EventArgs e)
        {
            try
            {
                Button YesorNo = (Button)sender;
                if (YesorNo.Text.ToString() == "Ok")
                {
                    if (CreateManualInterviewTestSave_confirmPopupExtenderControl.ReviewAndSave)
                    {
                        SaveQuestions();
                        Response.Redirect("~/InterviewTestMaker/AddInterviewTestQuestionRatings.aspx?m=1&s=1&parentpage=INT_T_MANU_QUST", false);
                    }
                    else if (CreateManualInterviewTestSave_confirmPopupExtenderControl.SaveWithDefaultValue)
                    {
                        SaveInteviewQuestionsWithDefaultSettings();
                    }
                }
                else
                {
                    ShowSaveConfirmPopUp();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                    CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        ///  Event Handling for the common ConfirmMessage Popup Extenter Cancel and 
        ///  Close Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel/Close button Event will be raised.
        /// </remarks>
        protected void CreateManualInterviewTest_cancelPopUpClick(object sender, EventArgs e)
        {
            DataTable dtTestSearchCriteria = null;
            try
            {
                //CreateManualInterviewTest_testDetailsUserControl.TestDetailDataSource = (TestDetail)Session["controlTestDetail"];
                //CreateManualInterviewTest_testDrftGridView.DataSource = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>; ;
                //CreateManualInterviewTest_testDrftGridView.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                    CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtTestSearchCriteria)) dtTestSearchCriteria = null;
            }
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool isQuestionTabActive = false;

           
            TestDetail testDetail = new TestDetail();
            testDetail = CreateManualInterviewTest_testDetailsUserControl.TestDetailDataSource;
            CreateManualInterviewTest_topErrorMessageLabel.Text = string.Empty;
            CreateManualInterviewTest_bottomErrorMessageLabel.Text = string.Empty;

            questionDetailTestDraftResultList = new List<QuestionDetail>();
            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
            {
                isQuestionTabActive = true;
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                    CreateManualInterviewTest_bottomErrorMessageLabel,
                    "Please add question to interview draft");
                isValidData = false;

            }
            else if (questionDetailTestDraftResultList.Count == 0)
            {
                isQuestionTabActive = true;
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                    CreateManualInterviewTest_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualTest_QuestionCount);
                isValidData = false;
            }

            if (testDetail.Name.Trim().Length == 0)
            {
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel,
                   "Please enter interview name");
                isValidData = false;
            }

            if (testDetail.Description.Trim().Length == 0)
            {
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel,
                   "Please enter interview description");
                isValidData = false;
            }

            if (isQuestionTabActive)
                CreateManualInterviewTest_mainTabContainer.ActiveTab = CreateManualInterviewTest_questionsTabPanel;
            else
                CreateManualInterviewTest_mainTabContainer.ActiveTab = CreateManualInterviewTest_testdetailsTabPanel;

            CreateManualInterviewTest_testDetailsUserControl.TestDetailDataSource = testDetail;
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindTestArea();
            BindComplexities();
            CreateManualInterviewTest_manualImageButton.Visible = false;
            CreateManualInterviewTest_adaptiveImageButton.Visible = false;
            CreateManualInterviewTest_bothImageButton.Visible = false;
            CreateManualInterviewTest_simpleSearchDiv.Visible = true;
            CreateManualInterviewTest_advanceSearchDiv.Visible = false;
            CreateManualInterviewTest_questionDiv.Visible = false;

            CreateManualInterviewTest_testSummaryControl.AverageTimeTakenByCandidatesLabel = false;
            CreateManualInterviewTest_testSummaryControl.AverageTimeTakenByCandidatesText = false;

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTDIRECTIONKEY"]))
                ViewState["SORTDIRECTIONKEY"] = "A";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTEXPRESSION"]))
                ViewState["SORTEXPRESSION"] = "QUESTIONKEY";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                ViewState["PAGENUMBER"] = "1";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
                ViewState["SEARCHRESULTGRID"] = null;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["mode"]))
                sQryString = Request.QueryString["mode"].ToUpper();

            CreateManualInterviewTest_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                + CreateManualInterviewTest_dummyAuthorID.ClientID + "','"
                + CreateManualInterviewTest_authorIdHiddenField.ClientID + "','"
                + CreateManualInterviewTest_authorTextBox.ClientID + "','QA')");

            CreateManualInterviewTest_positionProfileImageButton.Attributes.Add("onclick",
                    "return ShowClientRequestForManual('" + CreateManualInterviewTest_positionProfileTextBox.ClientID + "','" +
                    CreateManualInterviewTest_positionProfileKeywordTextBox.ClientID + "','" +
                     CreateManualInterviewTest_positionProfileHiddenField.ClientID + "')");

            
            CreateManualInterviewTest_testDrftGridView.DataSource = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            CreateManualInterviewTest_testDrftGridView.DataBind();
         }

        /// <summary>
        /// Handles the Clicked event of the CreateManualInterviewTest_addQuestioSaveButton_Clicked control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CreateManualInterviewTest_addQuestioSaveButton_Clicked
                (object sender, EventArgs e)
        {
            try
            {
                Session["NEWSEARCH_QUESTIONLIST"] = null;
                if (CreateManualInterviewTest_addQuestionQuestTextBox.Text.Length == 0)
                {
                    CreateManualInterviewTest_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateManualInterviewTest_addQuestionErrorLabel, "Enter the question");
                    CreateManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (CreateManualInterviewTest_addQuestionAnswerTextBox.Text.Length == 0)
                {
                    CreateManualInterviewTest_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateManualInterviewTest_addQuestionErrorLabel, "Enter the answer");
                    CreateManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (CreateManualInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Text == "--Select--")
                {
                    CreateManualInterviewTest_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateManualInterviewTest_addQuestionErrorLabel, "Select the complexity");

                    CreateManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }

                if (CreateManualInterviewAddQuestion_byQuestion_categoryTextBox.Text.Length == 0)
                {
                    CreateManualInterviewTest_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateManualInterviewTest_addQuestionErrorLabel, "Select the category and subject");
                    CreateManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (CreateManualInterviewAddQuestion_byQuestion_subjectTextBox.Text.Length == 0)
                {
                    CreateManualInterviewTest_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateManualInterviewTest_addQuestionErrorLabel, "Select the category and subject");
                    CreateManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }

                SaveNewQuestion();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                    CreateManualInterviewTest_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the CreateManualInterviewTest_addQuestionLinkButton_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CreateManualInterviewTest_addQuestionLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                CreateManualInterviewTest_addQuestionErrorLabel.Text = string.Empty;
                CreateManualInterviewTest_addQuestionQuestTextBox.Text = string.Empty;
                CreateManualInterviewTest_questionImage.ImageUrl = string.Empty;
                CreateManualInterviewTest_addQuestionAnswerTextBox.Text = string.Empty;
                CreateManualInterviewTest_isQuestionRepository.Checked = false;
                CreateManualInterviewAddQuestion_byQuestion_categoryTextBox.Text = string.Empty;
                CreateManualInterviewAddQuestion_byQuestion_subjectTextBox.Text = string.Empty;
                CreateManualInterviewTest_tagsTextBox.Text = string.Empty;
                
                // Binding test area to radiobutton list.
                CreateManualInterviewTest_addQuestionRadioButtonList.DataSource =
               new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
                CreateManualInterviewTest_addQuestionRadioButtonList.DataTextField = "AttributeName";
                CreateManualInterviewTest_addQuestionRadioButtonList.DataValueField = "AttributeID";
                CreateManualInterviewTest_addQuestionRadioButtonList.DataBind();
                CreateManualInterviewTest_addQuestionRadioButtonList.Items[0].Selected = true;

                //Bind complexities/attributes based on the attribute type
                CreateManualInterviewTest_addQuestioncomplexityDropDownList.DataSource =
                    new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
                CreateManualInterviewTest_addQuestioncomplexityDropDownList.DataTextField = "AttributeName";
                CreateManualInterviewTest_addQuestioncomplexityDropDownList.DataValueField = "AttributeID";
                CreateManualInterviewTest_addQuestioncomplexityDropDownList.DataBind();

                CreateManualInterviewTest_addQuestioncomplexityDropDownList.Items.Insert(0, "--Select--");
                CreateManualInterviewTest_addQuestioncomplexityDropDownList.SelectedIndex = 0;

                CreateManualInterviewAddQuestion_byQuestion_categoryImageButton.Attributes.Add("onclick",
                        "return LoadCategorySubjectLookUpforReadOnly('" + CreateManualInterviewAddQuestion_byQuestion_categoryTextBox.ClientID +
                            "','" + CreateManualInterviewAddQuestion_byQuestion_subjectTextBox.ClientID + "','" +
                            CreateManualInterviewAddQuestion_byQuestion_categoryHiddenField.ClientID + "','" +
                            CreateManualInterviewAddQuestion_byQuestion_categoryNameHiddenField.ClientID + "','" +
                            CreateManualInterviewAddQuestion_byQuestion_subjectNameHiddenField.ClientID + "');");

                CreateManualInterviewTest_addQuestioModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                    CreateManualInterviewTest_bottomErrorMessageLabel, exception.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the create interview session button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateManualInterviewTest_createSessionButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("CreateInterviewTestSession.aspx?m=1&s=2&parentpage=" +
                Constants.ParentPage.SEARCH_INTERVIEW +
                "&interviewtestkey=" + CreateManualInterviewTest_interviewTestKeyHiddenField.Value, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                    CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void CreateManualInterviewTest_addQuestionTopCancelImageButton_Click(object sender, ImageClickEventArgs e)
        {
            ResetQuestionImage();
        }
        protected void CreateManualInterviewTest_addQuestioCloseLinkButton_Click(object sender, EventArgs e)
        {
            ResetQuestionImage();
        }
        #endregion Protected Overridden Methods

        #region Not Use Now (Adaptive Question)

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateManualInterviewTest_adaptivepagingNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {

        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void CreateManualInterviewTest_searchQuestionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            // may be used in future.
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void CreateManualInterviewTest_testDrftGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            // may be used in future.
        }
        protected void CreateManualInterviewTest_adaptiveGridView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }
        protected void CreateManualInterviewTest_adaptiveGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "view")
            {
                CreateManualInterviewTest_questionModalPopupExtender.Show();
            }
            if (e.CommandName == "AdaptiveQuestion")
            {
                CreateManualInterviewTest_adaptiveQuestionPreviewModalPopupExtender.Show();

                ViewState["RowIndex"] = (e.CommandArgument.ToString());
            }
        }
        protected void CreateManualInterviewTest_adaptiveGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Image alertImage = (Image)e.Row.FindControl("CreateManualInterviewTest_adaptiveAlertImageButton");
                    alertImage.Attributes.Add("onClick", "javascript:return LoadFlagQuestion();");

                    Image img = (Image)e.Row.FindControl("CreateManualInterviewTest_adaptiveSelectImage");
                    img.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + CreateManualInterviewTest_adaptiveGridView.ClientID + "');");
                    //(Image)e.Row.FindControl("CreateManualInterviewTest_adaptiveSelectImage");

                    //img.Attributes.Add("onClick", "javascript:return AddRow(" + e.Row.RowIndex.ToString() + "," + 1 + ");");
                    // if (dtSearchQuestions.Rows[e.Row.RowIndex][0].ToString().Equals(""))
                    {
                        img.Visible = false;
                        ImageButton img1 = (ImageButton)e.Row.FindControl("CreateManualInterviewTest_adaptiveAlertImageButton");
                        img1.Visible = false;
                        //Image img2 = (Image)e.Row.FindControl("CreateManualInterviewTest_adaptiveViewImage");
                        //img2.Visible = false;
                        CheckBox chk = (CheckBox)e.Row.FindControl("CreateManualInterviewTest_adaptiveCheckbox");
                        chk.Visible = false;
                    }

                    ImageButton imageButton = (ImageButton)e.Row.FindControl("CreateManualInterviewTest_adaptiveQuestionDetailsImage");

                    imageButton.CommandArgument = e.Row.RowIndex.ToString();
                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
                    Label CreateManualInterviewTest_questionLabel =
                         (Label)e.Row.FindControl("CreateManualInterviewTest_questionLabel");

                    HoverMenuExtender CreateManualInterviewTest_hoverMenuExtender = (HoverMenuExtender)
                        e.Row.FindControl("CreateManualInterviewTest_hoverMenuExtender1");
                    CreateManualInterviewTest_hoverMenuExtender.TargetControlID = CreateManualInterviewTest_questionLabel.ID;

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateManualInterviewTest_topErrorMessageLabel,
                   CreateManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }
        protected void CreateManulTest_CloseButton_Click(object sender, EventArgs e)
        {
            MoveToTestDraft("");
            CreateManualInterviewTest_questionModalPopupExtender.Show();
        }
        protected void CreateManualInterviewTest_questionDetailTopAddButton_Click(object sender, EventArgs e)
        {
            //CreateManualInterviewTest_adaptiveQuestionPreviewModalPopupExtender.Hide();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "AddRow",
            //    "<script> AddRow(" + ViewState["RowIndex"] + "," + 1 + "); </script>", false);

            //ViewState["RowIndex"] = 0;

        }
        protected void QuestionDetail_topCancelButton_Click(object sender, EventArgs e)
        {
            CreateManualInterviewTest_adaptiveQuestionPreviewModalPopupExtender.Hide();
        }
        #endregion


        
}
}
