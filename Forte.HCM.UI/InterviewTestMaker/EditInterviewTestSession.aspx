﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditInterviewTestSession.aspx.cs" MasterPageFile="~/MasterPages/InterviewMaster.Master" Inherits="Forte.HCM.UI.InterviewTestMaker.EditInterviewTestSession" %>

<%@ Register Src="~/CommonControls/CandidateInterviewDetailControl.ascx" TagName="CandidateDetail"
    TagPrefix="uc5" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/InterviewTestSessionPreviewControl.ascx" TagName="InterviewTestSessionPreview" TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="EditInterviewTestSession_bodyContent" ContentPlaceHolderID="InterviewMaster_body"
    runat="server">

    <script type="text/javascript">

        // Method that will show/hide the captcha image help panel.
        function ShowWhyDisabled(show)
        {
            if (show == 'Y')
                document.getElementById('EditInterviewTestSession_cyberProctorWhyDisabledDiv').style.display = "block";
            else
                document.getElementById('EditInterviewTestSession_cyberProctorWhyDisabledDiv').style.display = "none";
            return false;
        }

        // Function calls when user clicks on the cancel test session image button
        function CandidateDetailModalPopup() 
        {
            $find("<%= EditInterviewTestSession_candidateDetailModalPopupExtender.ClientID %>").show();
            return false;
        }
    </script>

    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="50%" class="header_text_bold">
                            <asp:Literal ID="EditInterviewTestSession_headerLiteral" runat="server" Text="Edit Interview Session"></asp:Literal>
                        </td>
                        <td width="50%" align="right">
                            <asp:UpdatePanel ID="EditInterviewTestSession_topButtonsUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="4">
                                        <tr>
                                            <td>
                                                <asp:Button ID="EditInterviewTestSession_topScheduleCandidateButton" runat="server" Text="Schedule Candidate"
                                                    SkinID="sknButtonId" OnClick="EditInterviewTestSession_scheduleCandidateButton_Click" />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="EditInterviewTestSession_emailImageButton" runat="server"
                                                    ToolTip="Click here to email the interview sessions" SkinID="sknMailImageButton"
                                                    Visible="false" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="EditInterviewTestSession_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                    Text="Reset" OnClick="EditInterviewTestSession_resetButton_Click" />
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="EditInterviewTestSession_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                    Text="Cancel" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="EditInterviewTestSession_topSuccessErrorMsgUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="EditInterviewTestSession_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="EditInterviewTestSession_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="non_tab_body_bg">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="panel_bg">
                            <asp:UpdatePanel ID="EditInterviewTestSession_sessionDetailsUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <div id="EditInterviewTestSession_sessionDetailsDIV" runat="server">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="panel_inner_body_bg">
                                                                    <tr>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="EditInterviewTestSession_testKeyHeadLabel" runat="server" Text="Interview ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="EditInterviewTestSession_testKeyValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="EditInterviewTestSession_testNameHeadLabel" runat="server" Text="Interview Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%;">
                                                                            <asp:Label ID="EditInterviewTestSession_testNameValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="EditInterviewTestSession_sessionKeyHeadLabel" runat="server" Text="Interview Session ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="EditInterviewTestSession_sessionKeyValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                Width="60%"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%;">
                                                                            <asp:Label ID="EditInterviewTestSession_creditHeadLabel" runat="server" Text="Total Credit (in $)"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="EditInterviewTestSession_creditValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                Text="0"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="7">
                                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                <tr>
                                                                                    <td style="width:18%">
                                                                                        <asp:Label ID="EditInterviewTestSession_positionProfileHeadLabel" runat="server" Text="Position Profile"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td >
                                                                                        <asp:Label ID="EditInterviewTestSession_positionProfileValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                            Text=""></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="header_bg_testsession" style="width: 50%;">
                                                                <asp:Label ID="EditInterviewTestSession_sessionDescHeadLabel" runat="server" Text="Session Description"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td class="header_bg_testsession" style="width: 50%;">
                                                                <asp:Label ID="EditInterviewTestSession_instructionsHeadLabel" runat="server" Text="Interview Instructions"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%;">
                                                                <asp:TextBox ID="EditInterviewTestSession_sessionDescTextBox" runat="server" TextMode="MultiLine"
                                                                    SkinID="sknMultiLineTextBox" MaxLength="8000" Width="466px" Height="80px" onkeyup="CommentsCount(8000,this)"
                                                                    onchange="CommentsCount(8000,this)" TabIndex="1"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 50%;">
                                                                <asp:TextBox ID="EditInterviewTestSession_instructionsTextBox" runat="server" TextMode="MultiLine"
                                                                    MaxLength="8000" Width="466px" Height="80px" onkeyup="CommentsCount(8000,this)"
                                                                    onchange="CommentsCount(8000,this)" TabIndex="2"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_bg_testsession">
                                                    Settings
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="non_tab_body_bg">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="2" cellspacing="3" width="100%" height="100px">
                                                                    <tr>
                                                                        <td style="width: 40%;">
                                                                            <asp:Label ID="EditInterviewTestSession_sessionNoHeadLabel" runat="server" Text="Number of Sessions"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">&nbsp;*</span>
                                                                        </td>
                                                                        <td>
                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="EditInterviewTestSession_sessionNoTextBox" MaxLength="2" runat="server" Width="30px"
                                                                                            TabIndex="3"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:ImageButton ID="EditInterviewTestSession_upImageButton" runat="server" ImageAlign="AbsBottom" SkinID="sknNumericUpArrowImage" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:ImageButton ID="EditInterviewTestSession_downImageButton" runat="server" ImageAlign="Top" SkinID="sknNumericDownArrowImage" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <ajaxToolKit:NumericUpDownExtender ID="EditInterviewTestSession_NumericUpDownExtender" Width="60"
                                                                                runat="server" Minimum="0" Maximum="30" Step="1" TargetControlID="EditInterviewTestSession_sessionNoTextBox"
                                                                                TargetButtonUpID="EditInterviewTestSession_upImageButton" TargetButtonDownID="EditInterviewTestSession_downImageButton">
                                                                            </ajaxToolKit:NumericUpDownExtender>
                                                                        </td>
                                                                    </tr>
                                                                    <%--<tr>
                                                                        <td>
                                                                            <asp:Label ID="EditInterviewTestSession_timeLimitHeadLabel" runat="server" Text="Time Limit"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">&nbsp;*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="EditInterviewTestSession_timeLimitTextBox" runat="server" Columns="8" MaxLength="8"
                                                                                TabIndex="4"></asp:TextBox>
                                                                            <ajaxToolKit:MaskedEditExtender ID="EditInterviewTestSession_timeLimitMaskedEditExtender"
                                                                                runat="server" TargetControlID="EditInterviewTestSession_timeLimitTextBox" Mask="99:99:99"
                                                                                MessageValidatorTip="true" MaskType="Time" AcceptAMPM="false" AcceptNegative="None"
                                                                                InputDirection="LeftToRight" ErrorTooltipEnabled="True" />
                                                                        </td>
                                                                    </tr>--%>
                                                                    <%--<tr>
                                                                        <td>
                                                                            <asp:Label ID="EditInterviewTestSession_recommendedTimeHeadLabel" runat="server" Text="Recommended Time Limit"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="EditInterviewTestSession_recommendedTimeValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>--%>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="EditInterviewTestSession_expiryDateHeadLabel" runat="server" Text="Expiry Date"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">&nbsp;*</span>
                                                                        </td>
                                                                        <td>
                                                                            <table width="97.5%" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td style="width: 40%">
                                                                                        <asp:TextBox ID="EditInterviewTestSession_expiryDateTextBox" runat="server" MaxLength="10"
                                                                                            AutoCompleteType="None" Columns="15" TabIndex="5"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:ImageButton ID="EditInterviewTestSession_calendarImageButton" SkinID="sknCalendarImageButton"
                                                                                            runat="server" ImageAlign="Middle" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <ajaxToolKit:MaskedEditExtender ID="EditInterviewTestSession_MaskedEditExtender" runat="server"
                                                                                TargetControlID="EditInterviewTestSession_expiryDateTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                                                                OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Date"
                                                                                DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="EditInterviewTestSession_MaskedEditValidator" runat="server"
                                                                                ControlExtender="EditInterviewTestSession_MaskedEditExtender" ControlToValidate="EditInterviewTestSession_expiryDateTextBox"
                                                                                EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                                                TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                                ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="EditInterviewTestSession_customCalendarExtender" runat="server"
                                                                                TargetControlID="EditInterviewTestSession_expiryDateTextBox" CssClass="MyCalendar" Format="MM/dd/yyyy"
                                                                                PopupPosition="BottomLeft" PopupButtonID="EditInterviewTestSession_calendarImageButton" />
                                                                        </td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CreateInterviewTestSession_allowPauseInterviewLabel" Text="Allow Interview To Be Paused"
                                                                                runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="EditInterviewTestSession_allowPauseInterviewCheckbox" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="td_v_line">
                                                            </td>
                                                            <td id="Td2" style="width: 50%">
                                                                <table width="100%" cellpadding="2" cellspacing="3" border="0" height="100px">
                                                                    <tr>
                                                                        <td align="right" valign="top" style="height: 23px">
                                                                            <asp:LinkButton ID="EditInterviewTestSession_addAssessorLinkButton" runat="server"
                                                                                SkinID="sknAddLinkButton" Text="Search and Add Assessors" ToolTip="Search and Add Assessors"
                                                                                OnClick="EditInterviewTestSession_addAssessorLinkButton_Click">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="CreateInterviewTestSession_assessorDetailsTr" style="width: 100%" runat="server">
                                                                        <td id="Td2" class="tab_body_bg" runat="server">
                                                                            <asp:UpdatePanel ID="EditInterviewTestSession_assessorDetailsupdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <div style="width: 100%; overflow: auto; height: 80px;" runat="server" id="EditInterviewTestSession_assessorDetailsDiv"
                                                                                        visible="False">
                                                                                        <asp:GridView ID="EditInterviewTestSession_assessorDetailsGridView" runat="server"
                                                                                            AutoGenerateColumns="True" GridLines="Horizontal" BorderColor="white" BorderWidth="1px"
                                                                                            Width="100%">
                                                                                            <RowStyle CssClass="grid_alternate_row" />
                                                                                            <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                                            <HeaderStyle CssClass="grid_header_row" />
                                                                                            <Columns>
                                                                                                <asp:TemplateField HeaderText="Name">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateInterviewTestSession_assessorDetailsFirstNameLabel" runat="server"
                                                                                                            Text='<%# Eval("FirstName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Email">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateInterviewTestSession_assessorDetailsEmailLabel" runat="server"
                                                                                                            Text='<%# Eval("UserEmail") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div id="EditInterviewTestSession_assessorOptionDiv" runat="server" style="display:block">
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="EditInterviewTestSession_assignToScheduledCandidatesRadioButton"
                                                                                            GroupName="AssignAssessor" Text="Assign the new assessor(s) to all Scheduled Candidates"
                                                                                            runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="EditInterviewTestSession_assignToScheduledCompletedCandidatesRadioButton"
                                                                                            GroupName="AssignAssessor" Text="Assign the new assessor(s) to all Scheduled and Completed Candidates"
                                                                                            runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="td_padding_top_5">
                                                    <asp:Button ID="EditInterviewTestSession_saveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                        OnClick="EditInterviewTestSession_saveButton_Click" TabIndex="9" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header_bg" align="center">
                                        <asp:UpdatePanel ID="EditInterviewTestSession_maxMinButtonUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="EditInterviewTestSession_searchSessionResultsLiteral" runat="server" Text="Interview Sessions"></asp:Literal>
                                                            <asp:Label ID="EditInterviewTestSession_searchSessionResultsHelpLabel" runat="server" SkinID="sknLabelText"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%" align="right" id="EditInterviewTestSession_searchTestSessionResultsTR"
                                                            runat="server">
                                                            <span id="EditInterviewTestSession_searchSessionResultsUpSpan" runat="server" style="display: none;">
                                                                <asp:Image ID="EditInterviewTestSession_searchSessionResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                                    id="EditInterviewTestSession_searchSessionResultsDownSpan" runat="server" style="display: none;"><asp:Image
                                                                        ID="EditInterviewTestSession_searchSessionResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tab_body_bg">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:HiddenField ID="EditInterviewTestSession_restoreHiddenField" runat="server" />
                                                    <asp:UpdatePanel ID="EditInterviewTestSession_testSessionUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr id="EditInterviewTestSession_testSessionGridviewTR" runat="server" visible="false">
                                                                    <td align="left">
                                                                        <div style="height: 200px; overflow: auto;" runat="server" id="EditInterviewTestSession_testSessionDiv">
                                                                            <asp:GridView ID="EditInterviewTestSession_testSessionGridView" runat="server" AllowSorting="True"
                                                                                AutoGenerateColumns="False" GridLines="None" Width="100%" OnSorting="EditInterviewTestSession_testSessionGridView_Sorting"
                                                                                OnRowCommand="EditInterviewTestSession_testSessionGridView_RowCommand" OnRowDataBound="EditInterviewTestSession_testSessionGridView_RowDataBound"
                                                                                OnRowCreated="EditInterviewTestSession_testSessionGridView_RowCreated" SkinID="sknWrapHeaderGrid">
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="EditInterviewTestSession_candidateDetailImageButton" runat="server" SkinID="sknCandidateDetailImageButton"
                                                                                                ToolTip="Candidate Detail" CommandName="CandidateDetail" />
                                                                                            <asp:ImageButton ID="EditInterviewTestSession_viewTestSessionsImageButton" runat="server"
                                                                                                SkinID="sknViewSessionImageButton" ToolTip="View Interview Session" CommandName="ViewInterviewTestSession"
                                                                                                CommandArgument='<%# Eval("TestSessionID") %>' />
                                                                                            <asp:ImageButton ID="EditInterviewTestSession_cancelTestSessionImageButton" runat="server"
                                                                                                SkinID="sknCancelImageButton" ToolTip="Cancel Interview Session" CommandName="CancelTestSession"
                                                                                                CommandArgument='<%# Eval("CandidateTestSessionID") %>' />
                                                                                            <asp:HiddenField runat="server" ID="EditInterviewTestSession_attemptIdHiddenField" Value='<%# Eval("AttemptID") %>' />
                                                                                            <asp:HiddenField runat="server" ID="EditInterviewTestSession_statusHiddenField" Value='<%# Eval("Status") %>' />
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="10%" HorizontalAlign="Left" />
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="Interview&nbsp;Session&nbsp;ID" DataField="TestSessionID"
                                                                                        SortExpression="TestSessionID">
                                                                                        <ItemStyle Width="10%" />
                                                                                    </asp:BoundField>
                                                                                    <asp:TemplateField HeaderText="Interview&nbsp;Key" Visible="false">
                                                                                        <ItemStyle Width="10%" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="EditInterviewTestSession_byTestSession_testKeyLabel" runat="server" Text='<%# Eval("TestID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Candidate Session&nbsp;ID" SortExpression="CandidateTestSessionID">
                                                                                        <ItemStyle Width="10%" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="EditInterviewTestSession_byTestSession_candidateSessionIdLabel" runat="server"
                                                                                                Text='<%# Eval("CandidateTestSessionID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="Position Profile" DataField="PositionProfileName" SortExpression="PositionProfile">
                                                                                        <ItemStyle Width="12%" />
                                                                                    </asp:BoundField>
                                                                                    <asp:TemplateField ItemStyle-Width="12%" HeaderText="Date of Purchase" SortExpression="CreatedDate">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="EditInterviewTestSession_purchaseDateLabel" runat="server"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="Credits (in&nbsp;$)" DataField="TotalCredit" SortExpression="TotalCredit"
                                                                                        ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="td_padding_right_20" />
                                                                                    <asp:TemplateField HeaderText="Administered By" SortExpression="TestSessionAuthor">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="EditInterviewTestSession_testSessionAuthorFullnameLabel" runat="server" Text='<%# Eval("TestSessionAuthor") %>'
                                                                                                ToolTip='<%# Eval("TestSessionAuthorFullName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Candidate Name" SortExpression="CandidateName">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="EditInterviewTestSession_candidateFullnameLabel" runat="server" Text='<%# Eval("CandidateName") %>'
                                                                                                ToolTip='<%# Eval("CandidateFullName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Date of Interview" SortExpression="ScheduledDate">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="EditInterviewTestSession_scheduledDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateTestSessionDetail)Container.DataItem).ScheduledDate) %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="EditInterviewTestSession_pageNavigatorUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <uc1:PageNavigator ID="EditInterviewTestSession_bottomSessionPagingNavigator" runat="server" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:UpdatePanel ID="EditInterviewTestSession_cancelTestPopupUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="EditInterviewTestSession_cancelTestPanel" runat="server" Style="display: none"
                                                    CssClass="popupcontrol_cancel_session">
                                                    <div style="display: none">
                                                        <asp:Button ID="EditInterviewTestSession_hiddenButton" runat="server" Text="Hidden" />
                                                    </div>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="popup_td_padding_10">
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                                                            <asp:Label ID="EditInterviewTestSession_questionResultLiteral" runat="server" Text="Cancel Interview Session"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%" align="right">
                                                                            <asp:ImageButton ID="EditInterviewTestSession_cancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popup_td_padding_10">
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                                    <tr>
                                                                        <td class="popup_td_padding_10">
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td colspan="2" style="width: 100%" align="center">
                                                                                        <asp:Label ID="EditInterviewTestSession_cancelErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%">
                                                                                        <asp:Label ID="EditInterviewTestSession_cancelReasonLabel" runat="server" Text="Reason" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="EditInterviewTestSession_cancelTestReasonTextBox" runat="server" MaxLength="100"
                                                                                            SkinID="sknMultiLineTextBox" Columns="90" TextMode="MultiLine" Height="70" onkeyup="CommentsCount(100,this)"
                                                                                            onchange="CommentsCount(100,this)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popup_td_padding_10">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Button ID="EditInterviewTestSession_saveCancellationButton" runat="server" Text="Save"
                                                                                SkinID="sknButtonId" OnClick="EditInterviewTestSession_saveCancellationButton_Click" />
                                                                            &nbsp;
                                                                            <asp:LinkButton ID="EditInterviewTestSession_closeCancellationButton" runat="server" Text="Cancel"
                                                                                SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="EditInterviewTestSession_cancelSessionModalPopupExtender"
                                                    runat="server" PopupControlID="EditInterviewTestSession_cancelTestPanel" TargetControlID="EditInterviewTestSession_hiddenButton"
                                                    BackgroundCssClass="modalBackground" CancelControlID="EditInterviewTestSession_closeCancellationButton">
                                                </ajaxToolKit:ModalPopupExtender>
                                                <asp:Panel ID="EditInterviewTestSession_canidateDetailPanel" runat="server" Style="display: none;
                                                    height: auto;" CssClass="popupcontrol_question_detail">
                                                    <div style="display: none">
                                                        <asp:Button ID="EditInterviewTestSession_canidateDetailHiddenButton" runat="server" />
                                                    </div>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <uc5:CandidateDetail ID="EditInterviewTestSession_candidateDetailControl" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="EditInterviewTestSession_candidateDetailModalPopupExtender"
                                                    runat="server" TargetControlID="EditInterviewTestSession_canidateDetailHiddenButton" PopupControlID="EditInterviewTestSession_canidateDetailPanel"
                                                    BackgroundCssClass="modalBackground">
                                                </ajaxToolKit:ModalPopupExtender>
                                                <asp:Panel ID="EditInterviewTestSession_viewTestSessionSavePanel" runat="server" Style="display: none"
                                                    CssClass="preview_interview_session">
                                                    <div style="display: none">
                                                        <asp:Button ID="EditInterviewTestSession_viewTestSessionSaveButton" runat="server" />
                                                    </div>
                                                    <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <uc2:InterviewTestSessionPreview ID="EditInterviewTestSession_viewTestSessionSave_UserControl" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <table border="0" cellspacing="3" cellpadding="3" align="left">
                                                                    <tr>
                                                                        <td class="td_padding_top_5" style="padding-left:7px">
                                                                            <asp:Button ID="EditInterviewTestSession_previewTestSessionControl_createButton" runat="server"
                                                                                Text="Update" OnClick="EditInterviewTestSession_previewTestSessionControl_updateButton_Click"
                                                                                SkinID="sknButtonId" />
                                                                        </td>
                                                                        <td class="td_padding_top_5">
                                                                            <asp:LinkButton ID="EditInterviewTestSession_previewTestSessionControl_cancelButton" runat="server"
                                                                                Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                                        </td>
                                                                        <td style="height: 25px">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="EditInterviewTestSession_viewTestSessionSave_modalpPopupExtender"
                                                    runat="server" TargetControlID="EditInterviewTestSession_viewTestSessionSaveButton" PopupControlID="EditInterviewTestSession_viewTestSessionSavePanel"
                                                    BackgroundCssClass="modalBackground">
                                                </ajaxToolKit:ModalPopupExtender>
                                                <asp:Panel ID="EditInterviewTestSession_previewTestSessionPanel" runat="server" CssClass="preview_interview_session"
                                                    Style="display: none">
                                                    <div style="display: none">
                                                        <asp:Button ID="EditInterviewTestSession_previewTestSessionControl_Button" runat="server" />
                                                    </div>
                                                    <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <uc2:InterviewTestSessionPreview ID="EditInterviewTestSession_previewTestSessionControl_userControl1"
                                                                    runat="server" Mode="view" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_padding_top_5" style="padding-left: 10px">
                                                                <asp:LinkButton ID="CreateTestSession_previewTestSessionControl_cancelButton2" runat="server"
                                                                    Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="EditInterviewTestSession_previewTestSessionControl_modalpPopupExtender1"
                                                    runat="server" TargetControlID="EditInterviewTestSession_previewTestSessionControl_Button"
                                                    PopupControlID="EditInterviewTestSession_previewTestSessionPanel" BackgroundCssClass="modalBackground">
                                                </ajaxToolKit:ModalPopupExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="EditInterviewTestSession_bottomSuccessErrorMsgUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="EditInterviewTestSession_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="EditInterviewTestSession_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="EditInterviewTestSession_testKeyHiddenField" runat="server" />
    <asp:HiddenField ID="EditInterviewTestSession_positionProfileIDHiddenField" runat="server" />
    <asp:HiddenField ID="EditInterviewTestSession_candidateSessionIDsHiddenField" runat="server" />
    <asp:HiddenField ID="EditInterviewTestSession_testSessionIDHiddenField" runat="server" />
</asp:Content>
