﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EditInterviewTestSession.cs
// File that represents the user interface for editing an existing test
// session details. This will interact with the TestBLManager to update 
// the test session.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.InterviewTestMaker
{
    /// <summary>
    /// Class that represents the user interface and layout for 
    /// EditInterviewTestSession page. This provides to modify the existing
    /// information against the test session id. Also this page 
    /// provides some set of links in the test session grid to 
    /// view the candidate detail, test session and cancel the 
    /// the candidate test session. This class is inherited from 
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class EditInterviewTestSession : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Variables                                           

        #region Event Handlers                                                 

        /// <summary>
        /// Handler that will fire when the page is loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.DefaultButton = EditInterviewTestSession_saveButton.UniqueID;
                Master.SetPageCaption("Edit Interview Session");
                
                if (!IsPostBack)
                {
                    // Set default focus field
                    Page.Form.DefaultFocus = EditInterviewTestSession_sessionDescTextBox.UniqueID;
                    EditInterviewTestSession_sessionDescTextBox.Focus();

                    if (Utility.IsNullOrEmpty(ViewState["SORT_DIRECTION_KEY"]))
                        ViewState["SORT_DIRECTION_KEY"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_EXPRESSION"]))
                        ViewState["SORT_EXPRESSION"] = "TestSessionID";

                    if (Utility.IsNullOrEmpty(ViewState["PAGE_NUMBER"]))
                        ViewState["PAGE_NUMBER"] = "1";

                    if (!Utility.IsNullOrEmpty(Request.QueryString["testsessionid"]))
                    {
                        EditInterviewTestSession_testSessionIDHiddenField.Value =
                            Request.QueryString["testsessionid"].ToString();

                        // Method that will bind the test session details in the grid 
                        // against the test key.

                        //To get list of subjectids
                        Session["SESSION_SUBJECTIDS"] = null;
                        LoadTestSessionDetail(EditInterviewTestSession_testSessionIDHiddenField.Value);

                        // Enable/disable cyber proctoring based on feature
                        // applicability.
                       
                        //bool isCyberProctorApplicable = new
                        //    AdminBLManager().IsFeatureApplicable(base.tenantID,
                        //    Constants.FeatureConstants.CYBER_PROCTOR);

                        //EditInterviewTestSession_cyberProctorateCheckBox.Enabled = isCyberProctorApplicable;
                        //EditInterviewTestSession_cyberProctorWhyDisabledLinkButton.Visible = !isCyberProctorApplicable;
                    }

                    if (!Utility.IsNullOrEmpty(Request.QueryString["edit"]) &&
                        Request.QueryString["edit"].ToUpper() == "Y")
                    {
                        base.ShowMessage(EditInterviewTestSession_topSuccessMessageLabel,
                            EditInterviewTestSession_bottomSuccessMessageLabel,
                            string.Format(Resources.HCMResource.EditInterviewTestSession_TestSessionsUpdatedSuccessfully,
                            Request.QueryString["testsessionid"].ToString().Trim(),
                            Request.QueryString["candidatesessionids"].ToString().Trim()));
                        EditInterviewTestSession_emailImageButton.Visible = true;
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["edit"]) &&
                        Request.QueryString["edit"].ToUpper() == "N")
                    {
                        base.ShowMessage(EditInterviewTestSession_topSuccessMessageLabel,
                            EditInterviewTestSession_bottomSuccessMessageLabel,
                            string.Format(Resources.HCMResource.EditInterviewTestSession_CandidateSessionsCreatedSuccessfully,
                            Request.QueryString["testsessionid"].ToString().Trim(),
                            Request.QueryString["candidatesessionids"].ToString().Trim()));
                        EditInterviewTestSession_emailImageButton.Visible = true;
                    }
                }

                // Call pagination event handler
                EditInterviewTestSession_bottomSessionPagingNavigator.PageNumberClick +=
                    new PageNavigator.PageNumberClickEventHandler
                        (EditInterviewTestSession_bottomSessionPagingNavigator_PageNumberClick);

                EditInterviewTestSession_addAssessorLinkButton.Attributes.Add("onclick",
               "return SearchInterviewRecommendAssessor('SESSIONKEY','" + EditInterviewTestSession_testSessionIDHiddenField.Value.Trim() + "',1,null,null);");

                // Add javascript function for expanding and collapsing gridview.
                EditInterviewTestSession_searchTestSessionResultsTR.Attributes.Add("onclick", "ExpandorCompress('"
                  + EditInterviewTestSession_testSessionDiv.ClientID + "','"
                  + EditInterviewTestSession_sessionDetailsDIV.ClientID
                  + "','EditInterviewTestSession_searchSessionResultsUpSpan',"
                  + "'EditInterviewTestSession_searchSessionResultsDownSpan')");

                // Add handler for email button.
                EditInterviewTestSession_emailImageButton.Attributes.Add
                    ("onclick", "javascript:return ShowEmailCandidateSessions('EMAIL_INTERVIEW_SESSION')");

                // Validation for expand all and collapse all image button
                if (!Utility.IsNullOrEmpty(EditInterviewTestSession_restoreHiddenField.Value) &&
                   EditInterviewTestSession_restoreHiddenField.Value == "Y")
                {
                    EditInterviewTestSession_sessionDetailsDIV.Style["display"] = "none";
                    EditInterviewTestSession_searchSessionResultsUpSpan.Style["display"] = "block";
                    EditInterviewTestSession_searchSessionResultsDownSpan.Style["display"] = "none";
                    EditInterviewTestSession_testSessionDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    EditInterviewTestSession_sessionDetailsDIV.Style["display"] = "block";
                    EditInterviewTestSession_searchSessionResultsUpSpan.Style["display"] = "none";
                    EditInterviewTestSession_searchSessionResultsDownSpan.Style["display"] = "block";
                    EditInterviewTestSession_testSessionDiv.Style["height"] = RESTORED_HEIGHT;
                }

                EditInterviewTestSession_searchTestSessionResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    EditInterviewTestSession_testSessionDiv.ClientID + "','" +
                    EditInterviewTestSession_sessionDetailsDIV.ClientID + "','" +
                    EditInterviewTestSession_searchSessionResultsUpSpan.ClientID + "','" +
                    EditInterviewTestSession_searchSessionResultsDownSpan.ClientID + "','" +
                    EditInterviewTestSession_restoreHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                    EditInterviewTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will be called when a page number is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/> that contains the event data.
        /// </param>
        void EditInterviewTestSession_bottomSessionPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            LoadCandidateSessions(e.PageNumber);
            ViewState["PAGE_NUMBER"] = e.PageNumber;
        }

        /// <summary>
        /// Handler that will validate the input fields before they saved.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EditInterviewTestSession_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();

                // Validate the input fields
                if (!IsValidData())
                    return;

                InterviewTestSessionDetail testSessionDetail = PreviewTestSession();

                // Set the TestSessionPreview Mode. If the mode is VIEW, checkboxes
                // won't be displayed for Randomize question, cyberproctoring, and
                // display results to candidate. Instead, respective label control
                // will be shown as YES.
                EditInterviewTestSession_viewTestSessionSave_UserControl.Mode = "view";
                EditInterviewTestSession_viewTestSessionSave_UserControl.DataSource = testSessionDetail;
                EditInterviewTestSession_viewTestSessionSave_modalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                    EditInterviewTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that shows the modified information before its getting updated
        /// in the database.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> contains the event data.
        /// </param>
        protected void EditInterviewTestSession_previewTestSessionControl_updateButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();

                InterviewTestSessionDetail interviewSessionDetail = ConstructTestSessionDetails();

                string interviewSessionID = EditInterviewTestSession_testSessionIDHiddenField.Value.Trim();
                string candidateSessionIDs = null;

                // Check if the test session is modified by someone else. 
                // This will help to prevent concurrent updation
                // The MODIFIED_DATE should be retrieved as formatted date.
                // i.e Convert(varchar(25),date,121)
                if (new CommonBLManager().IsRecordModified("INTERVIEW_TEST_SESS",
                    interviewSessionDetail.TestSessionID,
                    (DateTime)ViewState["MODIFIED_DATE"]))
                {
                    base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                    EditInterviewTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.EditInterviewTestSession_ConcurrencyIsNotPossible);
                    return;
                }

                bool isMailSent = false;

                //Set candidate interview sessions

                string candidateInterviewStatus = string.Empty;

                if (EditInterviewTestSession_assignToScheduledCandidatesRadioButton.Checked)
                    candidateInterviewStatus = Constants.CandidateSessionStatus.SCHEDULED;

                if (EditInterviewTestSession_assignToScheduledCompletedCandidatesRadioButton.Checked)
                    candidateInterviewStatus = string.Format("{0},{1}", Constants.CandidateSessionStatus.COMPLETED,
                    Constants.CandidateSessionStatus.SCHEDULED);

                //Assessor list and selected skill id
                List<AssessorDetail> assessorDetails = null;

                if (Session["ASSESSOR"] == null)
                    assessorDetails = new List<AssessorDetail>();
                else
                    assessorDetails = Session["ASSESSOR"] as List<AssessorDetail>;

                new InterviewSessionBLManager().UpdateInterviewTestSession(interviewSessionDetail, assessorDetails,base.userID,
                    interviewSessionID, candidateInterviewStatus, out candidateSessionIDs,
                    (interviewSessionDetail.NumberOfCandidateSessions -
                    Convert.ToInt32(ViewState["NO_OF_SESSIONS"])), out isMailSent);


                // If the number of session is increased, 
                // then the page redirect with EDIT=N parameter.
                if (interviewSessionDetail.NumberOfCandidateSessions -
                    Convert.ToInt32(ViewState["NO_OF_SESSIONS"]) > 0)
                {
                    Response.Redirect("EditInterviewTestSession.aspx?m=1&s=3" +
                        "&testsessionid=" + interviewSessionID +
                        "&parentpage=S_ITSN&edit=n" +
                        "&candidatesessionids=" + candidateSessionIDs, false);
                }
                else
                {
                    Response.Redirect("EditInterviewTestSession.aspx?m=1&s=3" +
                        "&testsessionid=" + interviewSessionID +
                        "&parentpage=S_ITSN&edit=y" +
                        "&candidatesessionids=" + candidateSessionIDs, false);
                }

                // Show the email session button.
                EditInterviewTestSession_emailImageButton.Visible = true;

                // If the mail sent failed, then show the error message.
                if (isMailSent == false)
                {
                    base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                        EditInterviewTestSession_bottomErrorMessageLabel,
                         "Mail cannot be sent to the session author. Contact your administrator");
                }

               
                // Keep the created test session details in session, so that it can
                // be used in the email popup.
                Session["EMAIL_INTERVIEW_SESSION_SUBJECT"] = string.Format
                    ("Interview session '{0}' updated", interviewSessionID);
                Session["EMAIL_INTERVIEW_SESSION_MESSAGE"] = string.Format
                    ("Dear Viewer,\n\nSub: Interview session '{0}' updated.\n\nThe following candidate sessions are associated:\n{1}",
                    interviewSessionID, candidateSessionIDs);

                // Refresh test session details gridview i.e. Once a candidate 
                // session is created, it'll be updated in the gridview.
                LoadCandidateSessions(1); ;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                    EditInterviewTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that shows the CandidateDetail/View Test Session/Cancel Test Session
        /// image according to the passed the command name and its command argument.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that contains the event data.
        /// </param>
        protected void EditInterviewTestSession_testSessionGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "CandidateDetail")
                {
                    CandidateTestDetail candidateDetail = new CandidateTestDetail();
                    candidateDetail.CandidateSessionID = ((Label)((ImageButton)e.CommandSource).
                        FindControl("EditInterviewTestSession_byTestSession_candidateSessionIdLabel")).Text;
                    candidateDetail.AttemptID = int.Parse(((HiddenField)((ImageButton)e.CommandSource).
                        FindControl("EditInterviewTestSession_attemptIdHiddenField")).Value);
                    candidateDetail.TestID = ((Label)((ImageButton)e.CommandSource).
                        FindControl("EditInterviewTestSession_byTestSession_testKeyLabel")).Text;
                     candidateDetail.CandidateName = ((Label)((ImageButton)e.CommandSource).
                        FindControl("EditInterviewTestSession_candidateFullnameLabel")).Text;
                     candidateDetail.ShowTestScore = new InterviewSessionBLManager().GetInterviewShowTestScore(candidateDetail.CandidateSessionID);
                    EditInterviewTestSession_candidateDetailControl.Datasource = candidateDetail;
                    EditInterviewTestSession_candidateDetailModalPopupExtender.CancelControlID =
                        ((ImageButton)EditInterviewTestSession_candidateDetailControl.
                        FindControl("CandidateInterviewDetailControl_topCancelImageButton")).ClientID;
                    EditInterviewTestSession_candidateDetailModalPopupExtender.Show();
                }
                else if (e.CommandName == "ViewInterviewTestSession")
                {
                    // Instead of dummy, if we pass empty string, it returns all 
                    // candidate sessions available in the session.
                    InterviewTestSessionDetail testSession =
                        new InterviewSchedulerBLManager().GetInterviewTestSessionDetail
                        (e.CommandArgument.ToString(), "dummy");

                    // Assign test session object to the preview test session user control's datasource.
                    // This will show the test session detail before create.
                    EditInterviewTestSession_previewTestSessionControl_userControl1.DataSource = testSession;

                    EditInterviewTestSession_previewTestSessionControl_modalpPopupExtender1.Show();
                }
                else if (e.CommandName == "CancelTestSession")
                {
                    EditInterviewTestSession_cancelTestReasonTextBox.Text = string.Empty;

                    HiddenField attemptIdHiddenField = (e.CommandSource as ImageButton).Parent.
                        FindControl("EditInterviewTestSession_attemptIdHiddenField") as HiddenField;

                    ViewState["CANDIDATE_SESSION_ID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;

                    EditInterviewTestSession_cancelErrorMessageLabel.Text = string.Empty;
                    EditInterviewTestSession_cancelSessionModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                    EditInterviewTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call whenever a row gets the information. 
        /// It implements the mouse over and out style and showing icons
        /// based on the validation.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that contains the event data.
        /// </param>
        protected void EditInterviewTestSession_testSessionGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                ImageButton EditInterviewTestSession_candidateDetailImageButton = (ImageButton)
                    e.Row.FindControl("EditInterviewTestSession_candidateDetailImageButton");

                ImageButton EditInterviewTestSession_cancelTestSessionImageButton = (ImageButton)
                    e.Row.FindControl("EditInterviewTestSession_cancelTestSessionImageButton");

                HiddenField EditInterviewTestSession_statusHiddenField = (HiddenField)
                    e.Row.FindControl("EditInterviewTestSession_statusHiddenField");

                // Set visibility based on the attempt status
                if (EditInterviewTestSession_statusHiddenField.Value.Trim()
                    == Constants.CandidateAttemptStatus.SCHEDULED)
                {
                    EditInterviewTestSession_candidateDetailImageButton.Visible = true;
                    EditInterviewTestSession_cancelTestSessionImageButton.Visible = true;
                }

                if (EditInterviewTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.NOT_SCHEDULED
                    || EditInterviewTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.CANCELLED)
                {
                    EditInterviewTestSession_candidateDetailImageButton.Visible = false;
                    EditInterviewTestSession_cancelTestSessionImageButton.Visible = false;
                }
                else if (EditInterviewTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.IN_PROGRESS
                    || EditInterviewTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.CYBER_PROC_INIT
                    || EditInterviewTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.COMPLETED
                    || EditInterviewTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.ELAPSED
                    || EditInterviewTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.QUIT)
                {
                    EditInterviewTestSession_candidateDetailImageButton.Visible = true;
                    EditInterviewTestSession_cancelTestSessionImageButton.Visible = false;
                }
            }
        }

        /// <summary>
        /// Handler that is used to show an icon on the header column
        /// which is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that contains the event data.
        /// </param>
        protected void EditInterviewTestSession_testSessionGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    // Get last clicked column index
                    int sortColumnIndex = GetSortColumnIndex
                        (EditInterviewTestSession_testSessionGridView,
                        (string)ViewState["SORT_EXPRESSION"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_DIRECTION_KEY"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                    EditInterviewTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will sort the grid view data.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the send of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        protected void EditInterviewTestSession_testSessionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                // Assign sort expression column to the viewstate
                if (ViewState["SORT_EXPRESSION"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_DIRECTION_KEY"] =
                        ((SortType)ViewState["SORT_DIRECTION_KEY"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                {
                    ViewState["SORT_DIRECTION_KEY"] = SortType.Ascending;
                }

                ViewState["SORT_EXPRESSION"] = e.SortExpression;

                // Reload the pagination and gridview controls
                EditInterviewTestSession_bottomSessionPagingNavigator.Reset();
                LoadCandidateSessions(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                    EditInterviewTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will fire when clicking on the save button. 
        /// This cancels the candidate test session by updating the reason.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EditInterviewTestSession_saveCancellationButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();

                if (ViewState["ATTEMPT_ID"] == null || ViewState["CANDIDATE_SESSION_ID"] == null)
                    return;

                // Check if the reason text box is empty. If it is, show the error message.
                // Otherwise, update the session status as CANCEL.
                if (EditInterviewTestSession_cancelTestReasonTextBox.Text.Trim().Length == 0)
                {
                    EditInterviewTestSession_cancelErrorMessageLabel.Text = "Reason cannot be empty";
                    EditInterviewTestSession_cancelSessionModalPopupExtender.Show();
                }
                else
                {
                    int attemptId = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());
                    string candidateSessionId = ViewState["CANDIDATE_SESSION_ID"].ToString();

                    bool isMailSent = true;

                    new InterviewSchedulerBLManager().UpdateInterviewSessionStatus(candidateSessionId,
                       attemptId, Constants.CandidateAttemptStatus.CANCELLED,
                       Constants.CandidateSessionStatus.CANCELLED, base.userID, out isMailSent);

                    CandidateTestSessionDetail candidateTestSession = new CandidateTestSessionDetail();

                    candidateTestSession.AttemptID = attemptId;
                    candidateTestSession.CandidateTestSessionID = candidateSessionId;
                    candidateTestSession.CancelReason = EditInterviewTestSession_cancelTestReasonTextBox.Text.Trim();
                    candidateTestSession.ModifiedBy = base.userID;

                    new InterviewBLManager().CancelInterviewTestSession(candidateTestSession);

                    base.ShowMessage(EditInterviewTestSession_topSuccessMessageLabel,
                        EditInterviewTestSession_bottomSuccessMessageLabel,
                        string.Format(Resources.HCMResource.
                        EditTestSession_CandidateSessionIsCancelled, candidateSessionId));

                    LoadCandidateSessions(Convert.ToInt32(ViewState["PAGE_NUMBER"]));
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                    EditInterviewTestSession_bottomErrorMessageLabel, ex.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the reset linkbutton is clicked.
        /// During that time, it will load values before modified.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EditInterviewTestSession_resetButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Handler that will redirect to the schedule candidate page.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EditInterviewTestSession_scheduleCandidateButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/InterviewScheduler/InterviewScheduleCandidate.aspx?m=2&s=1&parentpage=" + Constants.ParentPage.EDIT_INTERVIEW_SESSION
                + "&interviewsessionid="
                + EditInterviewTestSession_testSessionIDHiddenField.Value.Trim(), false);
        }

        /// <summary>
        /// Method to bind the assessor list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditInterviewTestSession_addAssessorLinkButton_Click(object sender, EventArgs e)
        {
            ClearLabelMessage();
            if (Session["ASSESSOR"] == null)
                EditInterviewTestSession_addAssessorLinkButton.Attributes.Add("onclick", "javascript:return SearchInterviewRecommendAssessor('SESSIONKEY','" + EditInterviewTestSession_testSessionIDHiddenField.Value.Trim() + "',1,null,null);");
            else
            {
                List<AssessorDetail> slots = Session["ASSESSOR"] as List<AssessorDetail>;
                EditInterviewTestSession_assessorDetailsGridView.DataSource = Session["ASSESSOR"] as List<AssessorDetail>;
                EditInterviewTestSession_assessorDetailsGridView.DataBind();
                EditInterviewTestSession_assessorDetailsGridView.Visible = true;
            }
        }

        #endregion Event Handlers                                              

        #region Public Methods                                                 

        /// <summary>
        /// Method that will show pop up extender when download button clicked on user control.
        /// </summary>
        public void ShowModalPopup()
        {
            EditInterviewTestSession_candidateDetailModalPopupExtender.Show();
        }

        #endregion Public Methods                                              

        #region Private Methods                                                

        /// <summary>
        /// This method gets the all the subject ids related to this interview test
        /// </summary>
        /// <param name="interviewTestKey"></param>
        private void LoadSubjectIds(string interviewTestKey)
        {
            string subjectIds = new InterviewSessionBLManager().GetInterviewTestSubjectIdsByKey(interviewTestKey);
            Session["SESSION_SUBJECTIDS"] = subjectIds;
        }

        /// <summary>
        /// Method that will populate the test session and candidate
        /// session details against the test session id.
        /// </summary>
        /// <param name="testSessionID">
        /// A <see cref="string"/> that contains the test session id.
        /// </param>
        private void LoadTestSessionDetail(string testSessionID)
        {
            // Instead of dummy, if we pass empty string, it returns all 
            // candidate sessions available in the session.
            InterviewTestSessionDetail testSession =
                new InterviewSchedulerBLManager().GetInterviewTestSessionDetail
               (Request.QueryString["testsessionid"], "");

            if (testSession == null)
                return;

            //To get the subject ids bind to this interview question
            LoadSubjectIds(testSession.TestID);

            EditInterviewTestSession_testKeyValueLabel.Text = testSession.TestID;
            EditInterviewTestSession_testNameValueLabel.Text = testSession.TestName.Trim();
            EditInterviewTestSession_sessionKeyValueLabel.Text = testSessionID;
            EditInterviewTestSession_sessionNoTextBox.Text = testSession.NumberOfCandidateSessions.ToString();
            EditInterviewTestSession_creditValueLabel.Text = testSession.TotalCredit.ToString();
            EditInterviewTestSession_positionProfileValueLabel.Text = testSession.PositionProfileName;
            EditInterviewTestSession_positionProfileIDHiddenField.Value = testSession.PositionProfileID.ToString();
            EditInterviewTestSession_expiryDateTextBox.Text = testSession.ExpiryDate.ToString();

                        
            EditInterviewTestSession_sessionDescTextBox.Text = testSession.TestSessionDesc.Trim();
            EditInterviewTestSession_instructionsTextBox.Text = testSession.Instructions.Trim();

            ViewState["NO_OF_SESSIONS"] = testSession.NumberOfCandidateSessions.ToString();
            EditInterviewTestSession_testKeyHiddenField.Value = testSession.TestID;
            ViewState["CREATED_BY"] = testSession.CreatedBy;
            ViewState["MODIFIED_DATE"] = testSession.ModifiedDate;

            // Call load test session method by passing test key and page number
            LoadCandidateSessions(1);
            LoadTestAssessor();

            if (testSession.AllowPauseInterview == 'Y')
                EditInterviewTestSession_allowPauseInterviewCheckbox.Checked = true;
            else
                EditInterviewTestSession_allowPauseInterviewCheckbox.Checked = false;   

        }

         /// <summary>
        /// Method that will populate the test session and candidate
        /// session details against the test session id.
        /// </summary>
        /// <param name="testSessionID">
        /// A <see cref="string"/> that contains the test session id.
        /// </param>
        private void LoadTestAssessor()
        {
            List<AssessorDetail> assessorDetails = null;
            assessorDetails = new InterviewBLManager().GetInterviewTestAssessorDetail(Request.QueryString["testsessionid"], base.userID);

            Session["ASSESSOR"] = null;

            Session["ASSESSOR"] = assessorDetails;            
            if (Session["ASSESSOR"] == null)
                return;
            else
            {
                EditInterviewTestSession_assessorDetailsGridView.DataSource = Session["ASSESSOR"] as List<AssessorDetail>;
                EditInterviewTestSession_assessorDetailsGridView.DataBind();
                EditInterviewTestSession_assessorDetailsDiv.Visible = true;
            }
        }

        /// <summary>
        /// Method that is used to preview test session details once the user clicked 
        /// on the save button.
        /// </summary>
        /// <returns>Returns the <see cref="TestSessionDetail"/> object</returns>
        private InterviewTestSessionDetail PreviewTestSession()
        {
            // Initialize the test session detail object
            InterviewTestSessionDetail testSessionDetail = new InterviewTestSessionDetail();

            // Get the information from the controls and assign to the object members
            testSessionDetail.TestID = EditInterviewTestSession_testKeyValueLabel.Text.Trim();
            testSessionDetail.TestName = EditInterviewTestSession_testNameValueLabel.Text;
            testSessionDetail.TestSessionID = Request.QueryString["testsessionid"].Trim();

            testSessionDetail.NumberOfCandidateSessions
                = Convert.ToInt32(EditInterviewTestSession_sessionNoTextBox.Text);

            testSessionDetail.TotalCredit = Convert.ToDecimal(EditInterviewTestSession_creditValueLabel.Text);

            //Request[ScheduleCandidate_Popup_candidateNameTextBox.UniqueID].Trim();
            testSessionDetail.PositionProfileName = EditInterviewTestSession_positionProfileValueLabel.Text;

            //testSessionDetail.TimeLimit =
            //    Utility.ConvertHoursMinutesSecondsToSeconds(EditInterviewTestSession_timeLimitTextBox.Text);

            //testSessionDetail.RecommendedTimeLimit =
            //    Utility.ConvertHoursMinutesSecondsToSeconds(EditInterviewTestSession_recommendedTimeValueLabel.Text);

            // Set the time to maximum in expiry date.
            testSessionDetail.ExpiryDate = (Convert.ToDateTime(EditInterviewTestSession_expiryDateTextBox.Text)).Add(new TimeSpan(23, 59, 59));

            //testSessionDetail.IsRandomizeQuestionsOrdering = EditInterviewTestSession_randomSelectionCheckBox.Checked;
            //testSessionDetail.IsDisplayResultsToCandidate = EditInterviewTestSession_displayResultsCheckBox.Checked;
            //testSessionDetail.IsCyberProctoringEnabled = EditInterviewTestSession_cyberProctorateCheckBox.Checked;
            testSessionDetail.TestSessionDesc = EditInterviewTestSession_sessionDescTextBox.Text;
            testSessionDetail.Instructions = EditInterviewTestSession_instructionsTextBox.Text;

            testSessionDetail.CreatedBy = base.userID;
            testSessionDetail.ModifiedBy = base.userID;

            return testSessionDetail;
        }

        /// <summary>
        /// Method that gets the seconds values and updated the time format as hh:mm:yyyy
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public string GetTimeFormat(int seconds)
        {
            return Utility.ConvertSecondsToHoursMinutesSeconds(seconds);
        }

        /// <summary>
        /// Method that will load the candidate sessions.
        /// </summary>
        /// <param name="testID">
        /// A <see cref="string"/> that contains the test key.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that contains the page number.
        /// </param>
        /// <param name="sortBy">
        /// A <see cref="string"/> that contains the column to be sorted.
        /// </param>
        /// <param name="sortDirection">
        /// A <see cref="string"/> that contains the sort direction either ASC/DESC.
        /// </param>
        private void LoadCandidateSessions(int pageNumber)
        {
            int totalRecords = 0;
            List<CandidateTestSessionDetail> candidateTestSessions =
                new InterviewSessionBLManager().GetCandidateTestSessions
                (EditInterviewTestSession_testKeyHiddenField.Value, pageNumber,
                base.GridPageSize, ViewState["SORT_EXPRESSION"].ToString(),
                (SortType)ViewState["SORT_DIRECTION_KEY"],base.userID, out totalRecords);

            // Check if the candidateTestSession object is null, 
            // Then hide the gridview. Otherwise, show the loaded gridview 
            if (candidateTestSessions == null)
                EditInterviewTestSession_testSessionGridviewTR.Visible = false;
            else
            {
                EditInterviewTestSession_testSessionGridviewTR.Visible = true;
                EditInterviewTestSession_testSessionGridView.DataSource = candidateTestSessions;
                EditInterviewTestSession_testSessionGridView.DataBind();
            }

            // Update page size and total records
            EditInterviewTestSession_bottomSessionPagingNavigator.PageSize = base.GridPageSize;
            EditInterviewTestSession_bottomSessionPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that returns the TestSessionDetails object.
        /// </summary>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that contains the test session details.
        /// </returns>
        private InterviewTestSessionDetail ConstructTestSessionDetails()
        {
            // Initialize test session detail object
            InterviewTestSessionDetail testSessionDetail = new InterviewTestSessionDetail();

            testSessionDetail.TestID = EditInterviewTestSession_testKeyValueLabel.Text.Trim();
            testSessionDetail.TestSessionID = EditInterviewTestSession_sessionKeyValueLabel.Text.Trim();
            testSessionDetail.TestName = EditInterviewTestSession_testNameValueLabel.Text.Trim();

            testSessionDetail.NumberOfCandidateSessions =
                Convert.ToInt32(EditInterviewTestSession_sessionNoTextBox.Text);
            testSessionDetail.TotalCredit =
                Convert.ToDecimal(EditInterviewTestSession_creditValueLabel.Text);

            if (!Utility.IsNullOrEmpty(EditInterviewTestSession_positionProfileIDHiddenField.Value))
                testSessionDetail.PositionProfileID = Convert.ToInt32(EditInterviewTestSession_positionProfileIDHiddenField.Value);
            
            testSessionDetail.ExpiryDate = Convert.ToDateTime(EditInterviewTestSession_expiryDateTextBox.Text);
            testSessionDetail.AssessorIDs = string.Empty;

            if (EditInterviewTestSession_allowPauseInterviewCheckbox.Checked == true)
                testSessionDetail.AllowPauseInterview = 'Y';
            else
                testSessionDetail.AllowPauseInterview = 'N';

            testSessionDetail.CreatedBy = base.userID;
            testSessionDetail.ModifiedBy = base.userID;

            // Set test instructions
            testSessionDetail.Instructions =
                EditInterviewTestSession_instructionsTextBox.Text.Trim();

            // Set session descriptions
            testSessionDetail.TestSessionDesc =
                EditInterviewTestSession_sessionDescTextBox.Text.Trim();

            return testSessionDetail;
        }

        /// <summary>
        /// Method that helps to clear success/error label messages.
        /// </summary>
        private void ClearLabelMessage()
        {
            EditInterviewTestSession_topSuccessMessageLabel.Text = string.Empty;
            EditInterviewTestSession_bottomSuccessMessageLabel.Text = string.Empty;
            EditInterviewTestSession_topErrorMessageLabel.Text = string.Empty;
            EditInterviewTestSession_bottomErrorMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods                                             

        #region Protected Methods                                              

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            
            // Validate number of session field
            if (Convert.ToInt32(EditInterviewTestSession_sessionNoTextBox.Text) <= 0 ||
                (Convert.ToInt32(EditInterviewTestSession_sessionNoTextBox.Text) > 30))
            {
                isValidData = false;
                base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                    EditInterviewTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.EditTestSession_EnteredNumberOfSessionIsInvalid);
            }
            else
            {
                if (Convert.ToInt32(EditInterviewTestSession_sessionNoTextBox.Text) <
                    Convert.ToInt32(ViewState["NO_OF_SESSIONS"]))
                {
                    isValidData = false;
                    base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                    EditInterviewTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.EditTestSession_NumberOfSessionCannotBeDecreased);
                }
            }

            if (!EditInterviewTestSession_assignToScheduledCandidatesRadioButton.Checked &&
                !EditInterviewTestSession_assignToScheduledCompletedCandidatesRadioButton.Checked)
            {
                isValidData = false;
                base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                    EditInterviewTestSession_bottomErrorMessageLabel,
                    "Please select assessor update option");
            }
            
            EditInterviewTestSession_MaskedEditValidator.Validate();

            if (EditInterviewTestSession_expiryDateTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                    EditInterviewTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.EditTestSession_ExpiryDateCannotBeEmpty);
            }
            else
            {
                // This will check if the date is valid date. i.e. 99/99/9999
                if (!EditInterviewTestSession_MaskedEditValidator.IsValid)
                {
                    isValidData = false;
                    base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                    EditInterviewTestSession_bottomErrorMessageLabel,
                    EditInterviewTestSession_MaskedEditValidator.ErrorMessage);
                }
                else
                {
                    // Check if the expiry date is less than current date
                    if (DateTime.Parse(EditInterviewTestSession_expiryDateTextBox.Text) < DateTime.Today)
                    {
                        isValidData = false;
                        base.ShowMessage(EditInterviewTestSession_topErrorMessageLabel,
                        EditInterviewTestSession_bottomErrorMessageLabel,
                        Resources.HCMResource.EditTestSession_ExpiryDateCannotBeLessThanCurrentDate);
                    }
                }
            }
            return isValidData;
        }

        
        

        #endregion Protected Methods                                           
    }
}