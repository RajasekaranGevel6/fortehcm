﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchInterviewTest.cs
// File that represents the user Search the SearchInterviewTest by various Key filed.
// This will helps Search a Test From the Test repository.
//

#endregion Header

#region Directives

using System;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.InterviewTestMaker
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the Search Test page. This page helps in searching tests based on
    /// search criteria are, category,subject,keyword, testarea and so forth.
    /// This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class SearchInterviewTest : PageBase
    {
        #region Declaration

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Declaration

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button.
                Page.Form.DefaultButton = SearchInterviewTest_topSearchButton.UniqueID;
                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    LoadValues();

                    // Check if the page is launched from menu. If so, clear the existing
                    // session variables.
                    if ((!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU))
                    {
                        base.ClearSearchCriteriaSession();
                        Session["CATEGORY_SUBJECTS"] = null;
                    }

                    // Check if page is redirected from any child page. If the page
                    // if redirected from any child page, fill the search criteria
                    // and apply the search.
                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_INTERVIEW_TEST] != null)
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_INTERVIEW_TEST]
                            as TestSearchCriteria);
                    CheckAndSetExpandorRestore();
                }

                SearchInterviewTest_topSuccessMessageLabel.Text = string.Empty;
                SearchInterviewTest_bottomSuccessMessageLabel.Text = string.Empty;
                SearchInterviewTest_topErrorMessageLabel.Text = string.Empty;
                SearchInterviewTest_bottomErrorMessageLabel.Text = string.Empty;

                SearchInterviewTest_bottomPagingNavigator.PageNumberClick += new
                PageNavigator.PageNumberClickEventHandler
                         (SearchInterviewTest_pagingNavigator_PageNumberClick);
                SearchInterviewTest_categorySubjectControl.ControlMessageThrown += new
                    CategorySubjectControl.ControlMessageThrownDelegate(
                         SearchInterviewTest_categorySubjectControl_ControlMessageThrown);

                foreach (MultiHandleSliderTarget target in
                    SearchInterviewTest_noOfQuestionsMultiHandleSliderExtender.MultiHandleSliderTargets)
                    target.ControlID = SearchInterviewTest_noOfQuestionsMultiHandleSliderExtender.Parent.FindControl(target.ControlID).ClientID;

                

                if (!IsPostBack)
                {
                    // Load position profile details. This is applicable when navigating
                    // from the 'search position profile' page.
                    LoadPositionProfileDetail();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTest_topErrorMessageLabel,
                SearchInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// It shows error/success messages whenever the user calls this method
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/> that holds the event data.
        /// </param>
        private void SearchInterviewTest_categorySubjectControl_ControlMessageThrown(object sender,
            ControlMessageEventArgs c)
        {
            try
            {
                // Show message on the label.
                if (c.MessageType == MessageType.Error)
                {
                    base.ShowMessage(SearchInterviewTest_topErrorMessageLabel,
                        SearchInterviewTest_bottomErrorMessageLabel, c.Message);
                }
                else if (c.MessageType == MessageType.Success)
                {
                    base.ShowMessage(SearchInterviewTest_topSuccessMessageLabel,
                        SearchInterviewTest_bottomSuccessMessageLabel, c.Message);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTest_topErrorMessageLabel,
                SearchInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void SearchInterviewTest_OkClick(object sender, EventArgs e)
        {
            try
            {
                InterviewBLManager interviewTestBLManager = new InterviewBLManager();

                string SearchInterviewTestAction = SearchInterviewTest_Action.Value;
                string interviewTestKey = SearchInterviewTest_TestKey.Value.Trim();
                if (SearchInterviewTestAction == "active")
                {
                    interviewTestBLManager.ActivateInterviewTest(interviewTestKey, base.userID);
                    base.ShowMessage(SearchInterviewTest_topSuccessMessageLabel,
                    SearchInterviewTest_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.SearchInterviewTest_Activate, interviewTestKey));
                }
                else if (SearchInterviewTestAction == "inactive")
                {
                    interviewTestBLManager.DeactivateInterviewTest(interviewTestKey, base.userID);
                    base.ShowMessage(SearchInterviewTest_topSuccessMessageLabel,
                     SearchInterviewTest_bottomSuccessMessageLabel,
                     string.Format(Resources.HCMResource.SearchInterviewTest_InActivate, interviewTestKey));
                }
                else if (SearchInterviewTestAction == "deletes")
                {
                    interviewTestBLManager.DeleteInterviewTest(interviewTestKey, base.userID);
                    base.ShowMessage(SearchInterviewTest_topSuccessMessageLabel,
                    SearchInterviewTest_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.SearchInterviewTest_Delete, interviewTestKey));
                }
                LoadTests(Convert.ToInt32(ViewState["PAGENUMBER"].ToString()));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTest_topErrorMessageLabel,
                SearchInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Interchange Advanced and Simple Search Link and search criteria fields.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ControlMessageEventArgs"/> that holds the event data.
        /// </param>
        protected void SearchInterviewTest_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (SearchInterviewTest_simpleLinkButton.Text.ToLower() == "simple")
                {
                    SearchInterviewTest_simpleLinkButton.Text = "Advanced";
                    SearchInterviewTest_simpleSearchDiv.Visible = true;
                    SearchInterviewTest_advanceSearchDiv.Visible = false;
                }
                else
                {
                    SearchInterviewTest_simpleLinkButton.Text = "Simple";
                    SearchInterviewTest_simpleSearchDiv.Visible = false;
                    SearchInterviewTest_advanceSearchDiv.Visible = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTest_topErrorMessageLabel,
                SearchInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchInterviewTest_topSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchInterviewTest_bottomPagingNavigator.Reset();
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "TESTNAME";
                LoadTests(1);
                SearchInterviewTest_noOfQuestionsMultiHandleSliderExtender.ClientState = "0";
                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTest_topErrorMessageLabel,
                SearchInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchInterviewTest_pagingNavigator_PageNumberClick(object sender,
                     PageNumberEventArgs e)
        {
            try
            {
                ViewState["PAGENUMBER"] = e.PageNumber;

                //Bind the Search Result based on the Pageno, Sort Expression and Sort Direction
                LoadTests(e.PageNumber);
                SearchInterviewTest_noOfQuestionsMultiHandleSliderExtender.ClientState = "0";
                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTest_topErrorMessageLabel,
                SearchInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchInterviewTest_testGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                SearchInterviewTest_bottomPagingNavigator.Reset();
                LoadTests(1);
                SearchInterviewTest_noOfQuestionsMultiHandleSliderExtender.ClientState = "0";
                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTest_topErrorMessageLabel,
                SearchInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchInterviewTest_testGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ImageButton SearchInterviewTest_viewImage = (ImageButton)e.Row.FindControl("SearchInterviewTest_viewImage");

                    if (SearchInterviewTest_viewImage != null && !string.IsNullOrEmpty(SearchInterviewTest_viewImage.CommandArgument))
                    {
                        SearchInterviewTest_viewImage.Visible = true;
                    }

                    ImageButton SearchInterviewTest_additionalDetailsImageButton = (ImageButton)e.Row.FindControl(
                        "SearchInterviewTest_additionalDetailsImageButton");

                    ImageButton SearchInterviewTest_activeImageButton = (ImageButton)e.Row.FindControl(
                        "SearchInterviewTest_activeImageButton");
                    ImageButton SearchInterviewTest_inactiveImageButton = (ImageButton)e.Row.FindControl(
                        "SearchInterviewTest_inactiveImageButton");
                    ImageButton SearchInterviewTest_createNewImageButton = (ImageButton)e.Row.FindControl(
                        "SearchInterviewTest_createNewImageButton");
                    ImageButton SearchInterviewTest_deleteImageButton = (ImageButton)e.Row.FindControl(
                        "SearchInterviewTest_deleteImageButton");
                    ImageButton SearchInterviewTest_editImageButton = (ImageButton)e.Row.FindControl(
                        "SearchInterviewTest_editImageButton");
                    HiddenField SearchInterviewTest_statusHiddenField = (HiddenField)e.Row.FindControl(
                        "SearchInterviewTest_statusHiddenField");
                    HiddenField SearchInterviewTest_testKeyHiddenField = (HiddenField)e.Row.FindControl(
                        "SearchInterviewTest_testKeyHiddenField");
                    HiddenField SearchInterviewTest_activeInactivestatusHiddenField = (HiddenField)e.Row.FindControl(
                        "SearchInterviewTest_activeInactivestatusHiddenField");
                    HiddenField SearchInterviewTest_testAutherIdHiddenField = (HiddenField)e.Row.FindControl(
                        "SearchInterviewTest_testAutherIdHiddenField");
                    HiddenField SearchInterviewTest_dataAccessRightsHiddenField = (HiddenField)e.Row.FindControl(
                   "SearchInterviewTest_dataAccessRightsHiddenField");

                    ImageButton SearchInterviewTest_copyImageButton = (ImageButton)e.Row.FindControl(
                       "SearchInterviewTest_copyImageButton");

                    ImageButton SearchInterviewTest_associateToPPImageButton = (ImageButton)e.Row.FindControl(
                       "SearchInterviewTest_associateToPPImageButton");
                    HiddenField SearchInterviewTest_positionProfileIDHiddenField = (HiddenField)e.Row.FindControl(
                   "SearchInterviewTest_positionProfileIDHiddenField");
                    HiddenField SearchInterviewTest_tenantIDHiddenField = (HiddenField)e.Row.FindControl(
                   "SearchInterviewTest_tenantIDHiddenField");

                    SearchInterviewTest_additionalDetailsImageButton.Attributes.Add("onclick", "OpenAdditionalInterviewTestDetail('" +
                        SearchInterviewTest_testKeyHiddenField.Value + "')");
                    bool loginAuther = SearchInterviewTest_testAutherIdHiddenField.Value ==
                        base.userID.ToString() ? true : false;

                    if (SearchInterviewTest_statusHiddenField.Value == "True")
                    {
                        if (loginAuther)
                        {
                            if (SearchInterviewTest_activeInactivestatusHiddenField.Value == "True")
                            {
                                SearchInterviewTest_activeImageButton.Visible = true;
                                SearchInterviewTest_editImageButton.Visible = true;
                            }
                        }
                        else
                        {
                            SearchInterviewTest_deleteImageButton.Visible = false;
                            SearchInterviewTest_editImageButton.Visible = false;
                        }
                    }
                    else
                    {
                        SearchInterviewTest_activeImageButton.Visible = false;
                        SearchInterviewTest_editImageButton.Visible = false;
                        SearchInterviewTest_createNewImageButton.Visible = false;
                        if (loginAuther)
                        {
                            if (SearchInterviewTest_activeInactivestatusHiddenField.Value == "True")
                            {
                                SearchInterviewTest_inactiveImageButton.Visible = true;
                            }
                        }
                        else
                        {
                            SearchInterviewTest_deleteImageButton.Visible = false;
                        }
                    }

                    if ((SearchInterviewTest_dataAccessRightsHiddenField.Value == "V"))
                    {
                        SearchInterviewTest_editImageButton.Visible = false;
                        SearchInterviewTest_inactiveImageButton.Visible = false;
                        SearchInterviewTest_deleteImageButton.Visible = false;
                        SearchInterviewTest_activeImageButton.Visible = false;
                        SearchInterviewTest_copyImageButton.Visible = true;
                        SearchInterviewTest_createNewImageButton.Visible = false;
                        ImageButton SearchInterviewTest_viewImageButton = (ImageButton)e.Row.FindControl(
                        "SearchInterviewTest_viewImageButton");
                        SearchInterviewTest_viewImageButton.CommandName = "copyviewtest";
                    }

                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchInterviewTest_tenantIDHiddenField.Value)
                        && SearchInterviewTest_tenantIDHiddenField.Value == base.tenantID.ToString())
                    {
                        SearchInterviewTest_createNewImageButton.Visible = true;
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(SearchInterviewTest_positionProfileIDHiddenField.Value))
                        {
                            SearchInterviewTest_associateToPPImageButton.Visible = true;
                            SearchInterviewTest_copyImageButton.Visible = false;
                        }
                        else
                            SearchInterviewTest_copyImageButton.Visible = true;

                    }
                    int positionProfileID = 0;

                    if (!int.TryParse(Request.QueryString
               [Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING], out positionProfileID))
                    {
                        SearchInterviewTest_associateToPPImageButton.Visible = false;
                        SearchInterviewTest_copyImageButton.Visible = true;

                    }

                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTest_topErrorMessageLabel,
                SearchInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchInterviewTest_testGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex(
                        SearchInterviewTest_testGridView,
                        (string)ViewState["SORT_FIELD"]);
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTest_topErrorMessageLabel,
                SearchInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void SearchInterviewTest_resetLinkButton_Click(object sender, EventArgs e)
        {
            base.ClearSearchCriteriaSession();
            Session["CATEGORY_SUBJECTS"] = null;
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void SearchInterviewTest_testGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "active")
                {
                    SearchInterviewTest_inactivePopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchInterviewTest_Activate_Confirm_Message, e.CommandArgument.ToString());
                    SearchInterviewTest_inactivePopupExtenderControl.Title = "Activate Interview";
                    SearchInterviewTest_inactivePopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchInterviewTest_inactivepopupExtender.Show();
                }
                else if (e.CommandName == "inactive")
                {
                    SearchInterviewTest_inactivePopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchInterviewTest_DeActivate_Confirm_Message, e.CommandArgument.ToString());
                    SearchInterviewTest_inactivePopupExtenderControl.Title = "Deactivate Interview";
                    SearchInterviewTest_inactivePopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchInterviewTest_inactivepopupExtender.Show();
                }
                else if (e.CommandName == "deletes")
                {
                    SearchInterviewTest_inactivePopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.SearchInterviewTest_Delete_Confirm_Message, e.CommandArgument.ToString());
                    SearchInterviewTest_inactivePopupExtenderControl.Title = "Delete Interview";
                    SearchInterviewTest_inactivePopupExtenderControl.Type = MessageBoxType.YesNo;
                    SearchInterviewTest_inactivepopupExtender.Show();
                }
                else if (e.CommandName == "view")
                {
                    Response.Redirect("ViewInterviewTest.aspx?m=1&s=2&parentpage=" +
                        Constants.ParentPage.SEARCH_INTERVIEW + "&testkey=" + e.CommandArgument, false);
                }
                else if (e.CommandName == "edittest")
                {
                    Response.Redirect("EditManualInterviewTest.aspx?m=1&s=2&parentpage=" +
                        Constants.ParentPage.SEARCH_INTERVIEW + "&testkey=" + e.CommandArgument, false);
                }
                else if (e.CommandName == "create")
                {
                    Response.Redirect("CreateInterviewTestSession.aspx?m=1&s=2&parentpage=" +
                        Constants.ParentPage.SEARCH_INTERVIEW + "&interviewtestkey=" + e.CommandArgument, false);
                }
                else if (e.CommandName == "copytest")
                {
                    Response.Redirect(Constants.TestMakerConstants.EDIT_INTERVIEW_TEST + "?m=1&s=2&type=copy&parentpage=" +
                        Constants.ParentPage.SEARCH_INTERVIEW + "&testkey=" + e.CommandArgument, false);
                }
                else if (e.CommandName == "copyviewtest")
                {
                    Response.Redirect("ViewTest.aspx?m=1&s=2&type=copy&parentpage=" +
                        Constants.ParentPage.SEARCH_INTERVIEW + "&testkey=" + e.CommandArgument, false);
                }
                else if (e.CommandName == "associatePP")
                {
                    int positionProfileID = 0;

                    if (!int.TryParse(Request.QueryString
                        [Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING], out positionProfileID))
                    {
                        return;
                    }
                    new TestBLManager().UpdateTest(e.CommandArgument.ToString(), positionProfileID, userID);
                    LoadTests(int.Parse(ViewState["PAGENUMBER"].ToString()));
                    Response.Redirect("CreateTestSession.aspx?m=1&s=2&parentpage=" +
                        Constants.ParentPage.SEARCH_INTERVIEW + "&testkey=" + e.CommandArgument, false);
               //     base.ShowMessage(SearchInterviewTest_topSuccessMessageLabel,
               //SearchInterviewTest_bottomSuccessMessageLabel, "Test associated with position profile");
                }
                SearchInterviewTest_Action.Value = e.CommandName.ToString();
                SearchInterviewTest_TestKey.Value = e.CommandArgument.ToString();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTest_topErrorMessageLabel,
                SearchInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the cancel link button is clicked.
        /// During that time, it will clear all the search criteria session,
        /// category subject session values.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void SearchInterviewTest_cancelLinkButton_Click(object sender, EventArgs e)
        {
            base.ClearSearchCriteriaSession();
            Session["CATEGORY_SUBJECTS"] = null;
            Response.Redirect("~/OTMHome.aspx", false);
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadTests(int pageNumber)
        {
            int totalRecords = 0;
            StringBuilder testAreaID = GetTestAreaID();
            TestSearchCriteria testSearchCriteria = new TestSearchCriteria();
            testSearchCriteria.IsCertification = null;
            List<Subject> categorySubjects = null;

            //Here in the UI caption of the link button will be Advanced (for simple)
            if (SearchInterviewTest_simpleLinkButton.Text == "Advanced")
            {
                testSearchCriteria.Category = SearchInterviewTest_categoryTextBox.Text.Trim() == "" ?
                                    null : SearchInterviewTest_categoryTextBox.Text.Trim();
                testSearchCriteria.Subject = SearchInterviewTest_subjectTextBox.Text.Trim() == "" ?
                                    null : SearchInterviewTest_subjectTextBox.Text.Trim();
                testSearchCriteria.Keyword = SearchInterviewTest_keywordSimpleTextBox.Text.Trim() == "" ?
                                    null : SearchInterviewTest_keywordSimpleTextBox.Text.Trim();

                testSearchCriteria.SearchType = SearchType.Simple;
                testSearchCriteria.CurrentPage = pageNumber;
                testSearchCriteria.IsMaximized =
                    SearchInterviewTest_restoreHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

                testSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                testSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();
                testSearchCriteria.TestAuthorID = base.userID;
                testSearchCriteria.ShowCopiedTests = true;
                testSearchCriteria.SearchTenantID = base.tenantID;
            }
            else
            {
                testSearchCriteria.SearchType = SearchType.Advanced;
                testSearchCriteria.TenantID = base.tenantID;

                string SelectedSubjectIDs = "";
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchInterviewTest_categorySubjectControl.SubjectDataSource))
                {
                    List<Category> categoryList = new List<Category>();
                    foreach (Subject subject in SearchInterviewTest_categorySubjectControl.SubjectDataSource)
                    {
                        if (categorySubjects == null)
                            categorySubjects = new List<Subject>();

                        Subject subjectCat = new Subject();

                        if (subject.IsSelected == true)
                        {
                            Category category = new Category();
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                            category.CategoryID = subject.CategoryID;
                            categoryList.Add(category);
                        }

                        // Get all the subjects and categories and then assign to the list.
                        subjectCat.CategoryID = subject.CategoryID;
                        subjectCat.CategoryName = subject.CategoryName;
                        subjectCat.SubjectID = subject.SubjectID;
                        subjectCat.SubjectName = subject.SubjectName;
                        subjectCat.IsSelected = subject.IsSelected;
                        categorySubjects.Add(subjectCat);
                    }

                    //Get all Subject ids in  Un selected Subject List.
                    Category categoryID = new Category();
                    foreach (Subject subject in SearchInterviewTest_categorySubjectControl.SubjectDataSource)
                    {
                        categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                        if (categoryID == null)
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                    }
                }

                // Keep the categories and subjects in Session
                Session["CATEGORY_SUBJECTS"] = categorySubjects;

                if (SearchInterviewTest_certificateTestDropDownList.SelectedValue != "S")
                    testSearchCriteria.IsCertification = (SearchInterviewTest_certificateTestDropDownList.SelectedValue
                        == "Y") ? true : false;

                testSearchCriteria.CategoriesID = SelectedSubjectIDs == "" ?
                    null : SelectedSubjectIDs.TrimEnd(',');
                testSearchCriteria.TestAreasID = testAreaID.ToString() == "" ?
                    null : testAreaID.ToString().TrimEnd(',');
                testSearchCriteria.TestKey = SearchInterviewTest_testIdTextBox.Text.Trim() == "" ?
                    null : SearchInterviewTest_testIdTextBox.Text.Trim();
                testSearchCriteria.Name = SearchInterviewTest_testNameTextBox.Text.Trim() == "" ?
                    null : SearchInterviewTest_testNameTextBox.Text.Trim();

                if (SearchInterviewTest_positionProfileHiddenField.Value != null &&
                    SearchInterviewTest_positionProfileHiddenField.Value.Trim().Length > 0)
                {
                    testSearchCriteria.PositionProfileID = Convert.ToInt32(SearchInterviewTest_positionProfileHiddenField.Value);
                }
                testSearchCriteria.PositionProfileName = SearchInterviewTest_positionProfileTextBox.Text;

                testSearchCriteria.Keyword = SearchInterviewTest_keywordAdvanceTextBox.Text.Trim() == "" ?
                    null : SearchInterviewTest_keywordAdvanceTextBox.Text.Trim();

                testSearchCriteria.TestAuthorName = SearchInterviewTest_authorTextBox.Text.Trim() == "" ?
                    null : SearchInterviewTest_authorIdHiddenField.Value;

                

                testSearchCriteria.TotalQuestionStart = (SearchInterviewTest_noOfQuestionsMinValueTextBox.Text.Trim() == "") ?
                    0 : Convert.ToInt32(SearchInterviewTest_noOfQuestionsMinValueTextBox.Text.Trim());

                testSearchCriteria.TotalQuestionEnd = (SearchInterviewTest_noOfQuestionsMaxValueTextBox.Text.Trim() == "") ?
                    0 : int.Parse(SearchInterviewTest_noOfQuestionsMaxValueTextBox.Text.Trim());

                testSearchCriteria.CurrentPage = pageNumber;
                testSearchCriteria.IsMaximized =
                    SearchInterviewTest_restoreHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

                testSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                testSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();

                testSearchCriteria.TestAuthorID = base.userID;
                testSearchCriteria.ShowCopiedTests = SearchInterviewTest_showCopiedTestsCheckBox.Checked;
            }

            // Keep the search criteria in Session if the page is launched from 
            // somewhere else.
            Session[Constants.SearchCriteriaSessionKey.SEARCH_INTERVIEW_TEST] = testSearchCriteria;

            List<TestDetail> testDetail = new List<TestDetail>();
            testDetail = new BL.InterviewBLManager().GetInterviewTests(testSearchCriteria, base.GridPageSize, pageNumber,
                testSearchCriteria.SortExpression, testSearchCriteria.SortDirection, out totalRecords);

            if (testDetail == null || testDetail.Count == 0)
            {
                base.ShowMessage(SearchInterviewTest_topErrorMessageLabel,
                SearchInterviewTest_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                SearchInterviewTest_testGridView.DataSource = null;
                SearchInterviewTest_testGridView.DataBind();
                SearchInterviewTest_bottomPagingNavigator.TotalRecords = 0;
                SearchInterviewTest_testDiv.Visible = false;
            }
            else
            {
                SearchInterviewTest_testGridView.DataSource = testDetail;
                SearchInterviewTest_testGridView.DataBind();
                SearchInterviewTest_bottomPagingNavigator.PageSize = base.GridPageSize;
                SearchInterviewTest_bottomPagingNavigator.TotalRecords = totalRecords;
                SearchInterviewTest_testDiv.Visible = true;
            }
        }

        /// <summary>
        /// Method that will fill the search criteria to the appropriate fields.
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A <see cref="TestSearchCriteria"/> that contains the test search
        /// criteria fields.
        /// </param>
        public void FillSearchCriteria(TestSearchCriteria testSearchCriteria)
        {
            string[] testAreaIDs = null;

            // Check if the simple link button is clicked.
            if (testSearchCriteria.SearchType == SearchType.Simple)
            {
                SearchInterviewTest_simpleLinkButton.Text = "Advanced";
                SearchInterviewTest_simpleSearchDiv.Visible = true;
                SearchInterviewTest_advanceSearchDiv.Visible = false;

                if (testSearchCriteria.Category != null)
                    SearchInterviewTest_categoryTextBox.Text = testSearchCriteria.Category;

                if (testSearchCriteria.Subject != null)
                    SearchInterviewTest_subjectTextBox.Text = testSearchCriteria.Subject;

                if (testSearchCriteria.Keyword != null)
                    SearchInterviewTest_keywordSimpleTextBox.Text = testSearchCriteria.Keyword;

                SearchInterviewTest_restoreHiddenField.Value = testSearchCriteria.IsMaximized == true ? "Y" : "N";
                ViewState["SORT_FIELD"] = testSearchCriteria.SortExpression;
                ViewState["SORT_ORDER"] = testSearchCriteria.SortDirection;

                // Apply search
                LoadTests(testSearchCriteria.CurrentPage);

                // Highlight the last page number which is stored in session
                // when the page is launched from somewhere else.
                SearchInterviewTest_bottomPagingNavigator.MoveToPage(testSearchCriteria.CurrentPage);
            }
            else
            {
                SearchInterviewTest_simpleLinkButton.Text = "Simple";
                SearchInterviewTest_simpleSearchDiv.Visible = false;
                SearchInterviewTest_advanceSearchDiv.Visible = true;

                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria.TestAreasID))
                {
                    testAreaIDs = testSearchCriteria.TestAreasID.Split(new char[] { ',' });

                    // Make selection test area.
                    for (int idx = 0; idx < testAreaIDs.Length; idx++)
                        SearchInterviewTest_testAreaCheckBoxList.Items.FindByValue(testAreaIDs[idx]).Selected = true;
                }

                if (testSearchCriteria.TestKey != null)
                    SearchInterviewTest_testIdTextBox.Text = testSearchCriteria.TestKey.Trim();

                if (testSearchCriteria.Name != null)
                    SearchInterviewTest_testNameTextBox.Text = testSearchCriteria.Name.Trim();

                if (testSearchCriteria.TestAuthorName != null)
                    SearchInterviewTest_authorTextBox.Text = testSearchCriteria.TestAuthorName.Trim();

                if (testSearchCriteria.IsCertification != null)
                    SearchInterviewTest_certificateTestDropDownList.SelectedValue =
                        (testSearchCriteria.IsCertification == true) ? "Y" : "N";

                SearchInterviewTest_positionProfileTextBox.Text = testSearchCriteria.PositionProfileName.Trim();
                SearchInterviewTest_positionProfileHiddenField.Value = testSearchCriteria.PositionProfileID.ToString();

                if (testSearchCriteria.Keyword != null)
                    SearchInterviewTest_keywordAdvanceTextBox.Text = testSearchCriteria.Keyword.Trim();

                
                if (testSearchCriteria.TotalQuestionStart != 0)
                    SearchInterviewTest_noOfQuestionsMinValueTextBox.Text = testSearchCriteria.TotalQuestionStart.ToString();

                if (testSearchCriteria.TotalQuestionEnd != 0)
                    SearchInterviewTest_noOfQuestionsMaxValueTextBox.Text = testSearchCriteria.TotalQuestionEnd.ToString();

                if (Session["CATEGORY_SUBJECTS"] != null)
                {
                    // Fetch unique categories from the list.
                    var distinctCategories = (Session["CATEGORY_SUBJECTS"] as List<Subject>).GroupBy
                        (x => x.CategoryID).Select(x => x.First());

                    // This datasource is used to make the selection of the subjects
                    // which are already selected by the user to search.
                    SearchInterviewTest_categorySubjectControl.SubjectsToBeSelected =
                        Session["CATEGORY_SUBJECTS"] as List<Subject>;

                    // Bind distinct categories in the category datalist
                    SearchInterviewTest_categorySubjectControl.CategoryDataSource =
                        distinctCategories.ToList<Subject>();

                    // Bind subjects for the categories which are bind previously.
                    SearchInterviewTest_categorySubjectControl.SubjectDataSource =
                        distinctCategories.ToList<Subject>();
                }
                SearchInterviewTest_showCopiedTestsCheckBox.Checked = testSearchCriteria.ShowCopiedTests;
                SearchInterviewTest_restoreHiddenField.Value = testSearchCriteria.IsMaximized == true ? "Y" : "N";
                ViewState["SORT_FIELD"] = testSearchCriteria.SortExpression;
                ViewState["SORT_ORDER"] = testSearchCriteria.SortDirection;

                // Apply search
                LoadTests(testSearchCriteria.CurrentPage);

                // Highlight the last page number which is stored in session
                // when the page is launched from somewhere else.
                SearchInterviewTest_bottomPagingNavigator.MoveToPage(testSearchCriteria.CurrentPage);
            }
        }

        /// <summary>
        /// Bind test area based on TEST_AREA attribute type
        /// </summary>
        private void BindTestArea()
        {
            try
            {
                SearchInterviewTest_testAreaCheckBoxList.DataSource =
                   new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA,
                   Constants.SortTypeConstants.ASCENDING);
                SearchInterviewTest_testAreaCheckBoxList.DataTextField = "AttributeName";
                SearchInterviewTest_testAreaCheckBoxList.DataValueField = "AttributeID";
                SearchInterviewTest_testAreaCheckBoxList.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTest_topErrorMessageLabel,
                SearchInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Get Selected Test Area IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetTestAreaID()
        {
            StringBuilder testAreaID = new StringBuilder();
            for (int i = 0; i < SearchInterviewTest_testAreaCheckBoxList.Items.Count; i++)
            {
                if (SearchInterviewTest_testAreaCheckBoxList.Items[i].Selected)
                {
                    testAreaID.Append(SearchInterviewTest_testAreaCheckBoxList.Items[i].Value.ToString() + ",");
                }
            }
            return testAreaID;
        }

        /// <summary>
        /// Method that adds the click handler for expand or restore buttons.
        /// </summary>
        private void AssignHandlers()
        {
            SearchInterviewTest_SearchInterviewTestResultsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                SearchInterviewTest_testDiv.ClientID + "','" +
                SearchInterviewTest_searchByTestDiv.ClientID + "','" +
                SearchInterviewTest_searchResultsUpSpan.ClientID + "','" +
                SearchInterviewTest_searchResultsDownSpan.ClientID + "','" +
                SearchInterviewTest_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");

            // Add click hander for position profile image icon.
            SearchInterviewTest_positionProfileImageButton.Attributes.Add("onclick",
                "return LoadPositionProfileName('" + SearchInterviewTest_positionProfileTextBox.ClientID + "','" +
                SearchInterviewTest_positionProfileHiddenField.ClientID + "')");

            // Add click hander for author image icon.
            SearchInterviewTest_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                + SearchInterviewTest_dummyAuthorID.ClientID + "','"
                + SearchInterviewTest_authorIdHiddenField.ClientID + "','"
                + SearchInterviewTest_authorTextBox.ClientID + "','TA')");
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchInterviewTest_restoreHiddenField.Value) &&
                SearchInterviewTest_restoreHiddenField.Value == "Y")
            {
                SearchInterviewTest_searchByTestDiv.Style["display"] = "none";
                SearchInterviewTest_searchResultsUpSpan.Style["display"] = "block";
                SearchInterviewTest_searchResultsDownSpan.Style["display"] = "none";
                SearchInterviewTest_testDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                SearchInterviewTest_searchByTestDiv.Style["display"] = "block";
                SearchInterviewTest_searchResultsUpSpan.Style["display"] = "none";
                SearchInterviewTest_searchResultsDownSpan.Style["display"] = "block";
                SearchInterviewTest_testDiv.Style["height"] = RESTORED_HEIGHT;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST]))
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchInterviewTest_restoreHiddenField.Value))
                    ((TestSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST]).IsMaximized =
                        SearchInterviewTest_restoreHiddenField.Value == "Y" ? true : false;
        }

        /// <summary>
        /// A Method that gets the position profile keywords by removing there
        /// weightages
        /// </summary>
        /// <param name="KeyWordWithWeightages">
        /// A <see cref="System.String"/> that holds the position profile keywords
        /// among with their weightages
        /// </param>
        /// <returns>
        /// A <see cref="System.String"/> that holds the position profile keywords
        /// by removing their weightages
        /// </returns>
        private string GetPositionProfileKeys(string KeyWordWithWeightages)
        {
            StringBuilder sbKeywords = null;
            string[] strSplitKeywords = null;
            try
            {
                strSplitKeywords = KeyWordWithWeightages.Split(',');
                sbKeywords = new StringBuilder();
                for (int i = 0; i < strSplitKeywords.Length; i++)
                    if (strSplitKeywords[i].IndexOf('[') != -1)
                        sbKeywords.Append(strSplitKeywords[i].Substring(0, strSplitKeywords[i].IndexOf('[')) + ",");
                    else
                        sbKeywords.Append(strSplitKeywords[i] + ",");
                return sbKeywords.ToString().TrimEnd(',');
            }
            finally
            {
                if (strSplitKeywords != null) strSplitKeywords = null;
                if (sbKeywords != null) sbKeywords = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        private void LoadPositionProfileDetail()
        {
            // Check if position profile ID is present.
            if (Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING] == null ||
                Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING].Trim().Length == 0)
            {
                return;
            }

            int positionProfileID = 0;

            // Check if the query string contains a valid position profile ID.
            if (!int.TryParse(Request.QueryString
                [Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING], out positionProfileID))
            {
                return;
            }

            PositionProfileDetail positionProfileDetail = null;
            positionProfileDetail = new PositionProfileBLManager().GetPositionProfileKeyWord(positionProfileID);
            if (Support.Utility.IsNullOrEmpty(positionProfileDetail))
                return;

            // Assign keywords alone.
            SearchInterviewTest_keywordAdvanceTextBox.Text = GetPositionProfileKeys(positionProfileDetail.Keys);

            // Expand the advanced search panel.
            SearchInterviewTest_simpleLinkButton.Text = "Simple";
            SearchInterviewTest_simpleSearchDiv.Visible = false;
            SearchInterviewTest_advanceSearchDiv.Visible = true;

            // Un-check the 'Show Copied Tests' checkbox.
            SearchInterviewTest_showCopiedTestsCheckBox.Checked = false;

            // Apply default search.
            SearchInterviewTest_bottomPagingNavigator.Reset();
            ViewState["SORT_ORDER"] = SortType.Ascending;
            ViewState["SORT_FIELD"] = "TESTNAME";
            LoadTests(1);
            SearchInterviewTest_noOfQuestionsMultiHandleSliderExtender.ClientState = "0";
            
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Page title
            Master.SetPageCaption(Resources.HCMResource.SearchInterviewTest_Title);

            // Bind test Area.
            BindTestArea();

            // Set default focus field.
            Page.Form.DefaultFocus = SearchInterviewTest_categoryTextBox.UniqueID;
            SearchInterviewTest_categoryTextBox.Focus();
            SearchInterviewTest_testDiv.Visible = false;

            AssignHandlers();

            // Default settings
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                ViewState["SORT_ORDER"] = SortType.Ascending;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                ViewState["SORT_FIELD"] = "TESTNAME";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                ViewState["PAGENUMBER"] = "1";


        }

        #endregion Protected Overridden Methods

    }
}