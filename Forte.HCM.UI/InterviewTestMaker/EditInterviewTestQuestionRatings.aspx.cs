
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EditInterviewTestQuestionRatings.aspx.cs
// File that represents the user interface for displaying test detail.
// This will also used to show the question detail against the test key.

#endregion Header

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Web.UI;
using Forte.HCM.UI.CommonControls;
using Forte.HCM.ExternalService;
using System.Data;
using System.Linq;

#endregion Directives

namespace Forte.HCM.UI.InterviewTestMaker
{
    public partial class EditInterviewTestQuestionRatings : PageBase
    {
        List<QuestionDetail> questionAttributes = new List<QuestionDetail>();
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set page title
           Master.SetPageCaption("Edit Interview Question Rating");
           if(!IsPostBack)
            LoadValues();
        }

        /// <summary>
        /// This event handler helps to find a question number label 
        /// to bind the row number as question serial no(Eg: 1,2...)
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that contains the sender of the event
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> object which holds the event data.
        /// </param>
        protected void EditInterviewTestQuestionRatings_testDrftGridView_RowDataBound(object sender, 
            GridViewRowEventArgs e)
        {
            try
            {
                // Find the row type is DataRow
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    questionAttributes = ViewState["INTERVIEWTESTQUESTIONATTRIBUTES"] as List<QuestionDetail>;

                    // Find a label from the current row.
                    HiddenField EditInterviewTestQuestionRatings_interviewQuestionKeyHiddenField =
                        (HiddenField)e.Row.FindControl("EditInterviewTestQuestionRatings_interviewQuestionKeyHiddenField");

                    if (!string.IsNullOrEmpty(EditInterviewTestQuestionRatings_interviewQuestionKeyHiddenField.Value))
                    {
                        // Find a comment text box from the current row.
                        TextBox EditInterviewTestQuestionRatings_questionCommentsGridViewTextbox =
                            (TextBox)e.Row.FindControl("EditInterviewTestQuestionRatings_questionCommentsGridViewTextbox");

                        // Find a Drop Down Box from the current row.
                        DropDownList EditInterviewTestQuestionRatings_questionRatingsGridViewDropDownList =
                            (DropDownList)e.Row.FindControl("EditInterviewTestQuestionRatings_questionRatingsGridViewDropDownList");

                        // Find a time text box from the current row.
                        TextBox EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox =
                            (TextBox)e.Row.FindControl("EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox");

                        // Find a time text box from the current row.
                        TextBox EditInterviewTestQuestionRatings_questionnWeightageGridViewTextbox =
                            (TextBox)e.Row.FindControl("EditInterviewTestQuestionRatings_questionnWeightageGridViewTextbox");


                        var existingValues = from p in questionAttributes
                                             where p.QuestionKey == EditInterviewTestQuestionRatings_interviewQuestionKeyHiddenField.Value
                                             select new { p.TimeLimit, p.Rating, p.Weightage, p.Comments };

                        foreach (var id in existingValues)
                        {
                            EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox.Text =
                                ConvertSecondsToMinutesSeconds(id.TimeLimit);
                            EditInterviewTestQuestionRatings_questionRatingsGridViewDropDownList.SelectedIndex =
                                Convert.ToInt32(id.Rating - 1);
                            EditInterviewTestQuestionRatings_questionnWeightageGridViewTextbox.Text =
                                Convert.ToString(id.Weightage);
                            EditInterviewTestQuestionRatings_questionCommentsGridViewTextbox.Text = id.Comments;
                        }
                    }

                    // Find a label from the current row.
                    Label EditInterviewTestQuestionRatings_rowNoLabel =
                        (Label)e.Row.FindControl("EditInterviewTestQuestionRatings_rowNoLabel");

                    // Assign the row no to the label.
                    EditInterviewTestQuestionRatings_rowNoLabel.Text = (e.Row.RowIndex + 1).ToString();
                    // Display the image if any
                    QuestionDetail questionDetail = ((QuestionDetail)e.Row.DataItem);

                    Image EditInterviewTestQuestionRatings_questionImageDisplay =
                        (Image)e.Row.FindControl("EditInterviewTestQuestionRatings_questionImageDisplay");
                    if (((QuestionDetail)e.Row.DataItem).HasImage)
                    {
                        EditInterviewTestQuestionRatings_questionImageDisplay.Visible = true;
                        EditInterviewTestQuestionRatings_questionImageDisplay.ImageUrl
                            = @"~/Common/ImageHandler.ashx?questionKey="
                                + questionDetail.QuestionKey
                                + "&isThumb=0&source=VIEW_INTERVIEW_QUESTION_IMAGE";
                    }
                    else
                    {
                        EditInterviewTestQuestionRatings_questionImageDisplay.Visible = false;
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                    EditInterviewTestQuestionRatings_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Convert seconds to hh:mm:ss format 
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        private static string ConvertSecondsToMinutesSeconds(int seconds)
        {
            TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);

            string minutesSeconds = string.Format("{0:D2}:{1:D2}",
                                    timeSpan.Minutes,
                                    timeSpan.Seconds);

            return minutesSeconds;
        }
        /// <summary>
        /// Handles the Click event of the 
        /// EditInterviewTestQuestionRatings_questionTimetakenApplyToAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// 
        protected void EditInterviewTestQuestionRatings_questionTimetakenApplyToAllLinkButton_Click(
            object sender, EventArgs e)
        {
            try
            {
                int timeLimitValidation = 0;

                EditInterviewTestQuestionRatings_topErrorMessageLabel.Text = string.Empty;
                EditInterviewTestQuestionRatings_bottomErrorMessageLabel.Text = string.Empty;

                if (EditInterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox.Text != "")
                {
                    if (!Forte.HCM.Support.Utility.VerifyMinutesSeconds(
                           EditInterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox.Text.Trim()))
                    {
                        base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                          EditInterviewTestQuestionRatings_bottomErrorMessageLabel,
                          "Verify the time limit" );
                        return;
                    }

                    timeLimitValidation = Convert.ToInt32(Forte.HCM.Support.Utility.ConvertMinutesSecondsToSeconds(
                        EditInterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox.Text));

                    if (timeLimitValidation <= 0)
                    {                        
                        base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                          EditInterviewTestQuestionRatings_bottomErrorMessageLabel, 
                          "Time limit should be greater than 0");
                        return;
                    }
                    else if (timeLimitValidation > 300)
                    {
                        base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                              EditInterviewTestQuestionRatings_bottomErrorMessageLabel, 
                              "Time limit should be less than 5 minutes");
                        return;
                    }
                }
                else {
                    base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                              EditInterviewTestQuestionRatings_bottomErrorMessageLabel, 
                              "Please enter time limit for the questions");
                  return;
               
                }

                for (int i = 0; i < EditInterviewTestQuestionRatings_testDrftGridView.Rows.Count; i++)
                {
                    if (EditInterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox.Text.Length == 0)
                    {
                        base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                        EditInterviewTestQuestionRatings_bottomErrorMessageLabel,
                        "Please enter time limit for the questions");                    
                    }
                    else
                    {
                        TextBox EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox =
                            (TextBox)EditInterviewTestQuestionRatings_testDrftGridView.Rows[i].
                            FindControl("EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox");
                        EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox.Text = 
                            EditInterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox.Text;
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                    EditInterviewTestQuestionRatings_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the EditInterviewTestQuestionRatings_questionTimetakenApplyToAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void EditInterviewTestQuestionRatings_questionRatingsApplyToAllLinkButton_Click(object sender, 
            EventArgs e)
        {
            try
            {
                EditInterviewTestQuestionRatings_topErrorMessageLabel.Text = string.Empty;
                EditInterviewTestQuestionRatings_bottomErrorMessageLabel.Text = string.Empty;

                    if (EditInterviewTestQuestionRatings_questionRatingsApplyToAllDropDownList.SelectedItem.Text == "0")
                    {
                        base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                        EditInterviewTestQuestionRatings_bottomErrorMessageLabel, "Please select ratings for the questions ");
                        return;
                    }

                for (int i = 0; i < EditInterviewTestQuestionRatings_testDrftGridView.Rows.Count; i++)
                {
                   DropDownList EditInterviewTestQuestionRatings_questionRatingsGridViewDropDownList =
                          (DropDownList)EditInterviewTestQuestionRatings_testDrftGridView.Rows[i].FindControl("EditInterviewTestQuestionRatings_questionRatingsGridViewDropDownList");
                   EditInterviewTestQuestionRatings_questionRatingsGridViewDropDownList.SelectedValue = EditInterviewTestQuestionRatings_questionRatingsApplyToAllDropDownList.SelectedValue;
                   
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                    EditInterviewTestQuestionRatings_bottomErrorMessageLabel, exception.Message);

            }
        }

        /// <summary>
        /// Handles the Click event of the EditInterviewTestQuestionRatings_questionTimetakenApplyToAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void EditInterviewTestQuestionRatings_questionCommentsApplyToAllLinkButton_Click(object sender,
            EventArgs e)
        {
            try
            {
                for (int i = 0; i < EditInterviewTestQuestionRatings_testDrftGridView.Rows.Count; i++)
                {
                    if (EditInterviewTestQuestionRatings_questionCommentsApplyToAllTextbox.Text.Length == 0)
                    {
                        base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                        EditInterviewTestQuestionRatings_bottomErrorMessageLabel, "Please enter time limit for the questions");
                    }
                    else
                    {
                        TextBox EditInterviewTestQuestionRatings_questionCommentsGridViewTextbox =
                            (TextBox)EditInterviewTestQuestionRatings_testDrftGridView.Rows[i].FindControl("EditInterviewTestQuestionRatings_questionCommentsGridViewTextbox");
                        EditInterviewTestQuestionRatings_questionCommentsGridViewTextbox.Text = EditInterviewTestQuestionRatings_questionCommentsApplyToAllTextbox.Text;
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                    EditInterviewTestQuestionRatings_bottomErrorMessageLabel, exception.Message);

            }
        }

        private bool GetWeightageValue()
        {
            int _numWeightPercentage = 0;
            for (int i = 0; i < EditInterviewTestQuestionRatings_testDrftGridView.Rows.Count; i++)
            {

                TextBox WeightAge_Textbox =
                    (TextBox)EditInterviewTestQuestionRatings_testDrftGridView.Rows[i].FindControl("EditInterviewTestQuestionRatings_questionnWeightageGridViewTextbox");

                if (WeightAge_Textbox != null)
                {
                    if (Utility.IsNullOrEmpty(WeightAge_Textbox.Text))
                    {
                        base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                            EditInterviewTestQuestionRatings_bottomErrorMessageLabel,
                            string.Format("Weightage cannot be empty for  question {0}", i + 1));

                        return false;
                    }
                    _numWeightPercentage += Convert.ToInt32(WeightAge_Textbox.Text);
                }
            }
            if (_numWeightPercentage < 100 || _numWeightPercentage > 100)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Event handles the save question and its attributes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditInterviewTestQuestionRatings_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                InterviewDetail testDetail = null;
                testDetail = new InterviewDetail();

                if (Session["interviewDetail"] != null)
                {
                    testDetail = Session["interviewDetail"] as InterviewDetail;
                    EditInterviewTestQuestionRatings_testDetailsUserControl.TestDetailDataSource = testDetail;
                }

                if (string.IsNullOrEmpty(testDetail.InterviewTestKey))
                    return;

                if (EditInterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox.Text.Trim() != "")
                {
                    if (!Forte.HCM.Support.Utility.VerifyMinutesSeconds(
                                EditInterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox.Text.Trim()))
                    {
                        base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                          EditInterviewTestQuestionRatings_bottomErrorMessageLabel,
                          "Verify the time limit");
                        return;
                    }
                }

                //Verifying the weightage
                if (!GetWeightageValue())
                {
                    EditInterviewTestQuestionRatings_topErrorMessageLabel.Text = "";
                    EditInterviewTestQuestionRatings_bottomErrorMessageLabel.Text = "";

                    base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                        EditInterviewTestQuestionRatings_bottomErrorMessageLabel,
                        "Questions weightage should be equal to 100");
                    /*EditInterviewTestQuestionRatings_topErrorMessageLabel.Text = "";
                    EditInterviewTestQuestionRatings_topErrorMessageLabel.Text = "Questions weightage should be equal to 100";*/
                    return;
                }

                #region Validation
                for (int i = 0; i < EditInterviewTestQuestionRatings_testDrftGridView.Rows.Count; i++)
                {
                    TextBox EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox =
                        (TextBox)EditInterviewTestQuestionRatings_testDrftGridView.Rows[i].
                        FindControl("EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox");

                    if (EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox != null)
                    {
                        if (EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox.Text == "")
                        {
                            EditInterviewTestQuestionRatings_topErrorMessageLabel.Text = string.Empty;
                            EditInterviewTestQuestionRatings_bottomErrorMessageLabel.Text = string.Empty;

                            base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                                EditInterviewTestQuestionRatings_bottomErrorMessageLabel,
                                string.Concat("Please enter time limit for question ", (i + 1)));
                            return;
                        }
                        else
                        {
                            if (!Forte.HCM.Support.Utility.VerifyMinutesSeconds(
                               EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox.Text.Trim()))
                            {
                                base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                                  EditInterviewTestQuestionRatings_bottomErrorMessageLabel,
                                  string.Concat("Verify the time limit for question ", (i + 1)));
                                return;
                            }

                            int timeLimitValidation = 0;
                            timeLimitValidation =
                                Convert.ToInt32(Forte.HCM.Support.Utility.ConvertMinutesSecondsToSeconds(
                                EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox.Text));

                            EditInterviewTestQuestionRatings_topErrorMessageLabel.Text = string.Empty;
                            EditInterviewTestQuestionRatings_bottomErrorMessageLabel.Text = string.Empty;

                            if (timeLimitValidation <= 0)
                            {
                                base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                                  EditInterviewTestQuestionRatings_bottomErrorMessageLabel,
                                  string.Concat("Time limit should be greater than 0 for question ", (i + 1)));
                                return;
                            }
                            else if (timeLimitValidation > 300)
                            {
                                base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                                      EditInterviewTestQuestionRatings_bottomErrorMessageLabel,
                                       string.Concat("Time limit should be less than 5 minutes for question ", (i + 1)));
                                return;
                            }
                        }
                    }

                    DropDownList EditInterviewTestQuestionRatings_questionRatingsGridViewDropDownList =
                        (DropDownList)EditInterviewTestQuestionRatings_testDrftGridView.Rows[i].
                        FindControl("EditInterviewTestQuestionRatings_questionRatingsGridViewDropDownList");

                    if (EditInterviewTestQuestionRatings_questionRatingsGridViewDropDownList != null)
                    {
                        if (EditInterviewTestQuestionRatings_questionRatingsGridViewDropDownList.SelectedValue == "0")
                        {
                            EditInterviewTestQuestionRatings_topErrorMessageLabel.Text = "";
                            EditInterviewTestQuestionRatings_bottomErrorMessageLabel.Text = "";

                            base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                                EditInterviewTestQuestionRatings_bottomErrorMessageLabel,
                                string.Concat("Please select ratings for question ", (i + 1)));
                            return;
                        }
                    }
                }

                #endregion

                //Here is the logic for calculate weight
                DataTable dtTestSearchCriteria = null;
                dtTestSearchCriteria = BuildTableFromGridView();
                int TotalWeightage = 0;
                LoadWeightages(ref dtTestSearchCriteria, dtTestSearchCriteria.Rows.Count, out TotalWeightage);
                int index = 0;
                foreach (GridViewRow gridViewRow in EditInterviewTestQuestionRatings_testDrftGridView.Rows)
                {
                    TextBox Weightage = (TextBox)gridViewRow.
                        FindControl("EditInterviewTestQuestionRatings_questionnWeightageGridViewTextbox");
                    Weightage.Text = dtTestSearchCriteria.Rows[index]["Weightage"].ToString();
                    index++;
                }

                List<QuestionDetail> questionDetails = new List<QuestionDetail>();
                TestDetail interviewTestDetail = null;
                interviewTestDetail = new TestDetail();

                if (Session["controlTestDetail"] != null)
                    interviewTestDetail = (TestDetail)Session["controlTestDetail"];

                questionDetails = ConstructQuestionInformation();

                interviewTestDetail.Questions = questionDetails;


                if (Session["interviewTestNew"] != null && Convert.ToString(Session["interviewTestNew"]) == "Y")
                {
                    new InterviewBLManager().SaveInterviewTest(interviewTestDetail, userID,
                        new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY,
                        questionDetails).AttributeID.ToString());
                }
                else
                {
                    // Update Interview Test
                    new InterviewBLManager().UpdateInterviewTest(interviewTestDetail, userID,
                        new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY,
                            questionDetails).AttributeID.ToString(), true);
                }
                // Show a success message.
                EditInterviewTestQuestionRatings_topErrorMessageLabel.Text = string.Empty;
                EditInterviewTestQuestionRatings_bottomErrorMessageLabel.Text = string.Empty;
                EditInterviewTestQuestionRatings_topSuccessMessageLabel.Text = string.Empty;
                EditInterviewTestQuestionRatings_bottomSuccessMessageLabel.Text = string.Empty;

                base.ShowMessage(EditInterviewTestQuestionRatings_topSuccessMessageLabel,
                    EditInterviewTestQuestionRatings_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.UpdateAutomaticInterviewTest_TestSuccessfullMessage,
                    interviewTestDetail.TestKey));

                testDetail.InterviewTestKey = interviewTestDetail.TestKey;
                Session["interviewTestNew"] = string.Empty;
                EditInterviewTestQuestionRatings_topCreateSessionButton.Visible = true;
                EditInterviewTestQuestionRatings_bottomCreateSessionButton.Visible = true;

                EditInterviewTestQuestionRatings_topSaveButton.Visible = false;
                EditInterviewTestQuestionRatings_bottomSaveButton.Visible = false;

                EditInterviewTestQuestionRatings_interviewTestKeyHiddenField.Value = interviewTestDetail.TestKey;
                EditInterviewTestQuestionRatings_testDetailsUserControl.TestDetailDataSource = testDetail;

                // Send a mail to the position profile creator.
                try
                {
                    if (interviewTestDetail.PositionProfileID != 0)
                        new EmailHandler().SendMail(EntityType.InterviewCreatedForPositionProfile,
                            interviewTestDetail.TestKey);
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                    EditInterviewTestQuestionRatings_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the create interview session button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void EditInterviewTestQuestionRatings_createSessionButton_Click(object sender, 
            EventArgs e)
        {
            try
            {
                Response.Redirect("CreateInterviewTestSession.aspx?m=1&s=2&parentpage=" +
                Constants.ParentPage.SEARCH_INTERVIEW + 
                "&interviewtestkey=" + EditInterviewTestQuestionRatings_interviewTestKeyHiddenField.Value, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                    EditInterviewTestQuestionRatings_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This method allocates weightages to the segements
        /// and ensures that the total weightage is 100% or not.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        /// <param name="TotalWeightage">Total weightage allocated by this method 
        /// (Note:- it should be out parametarized)</param>
        private void LoadWeightages(ref DataTable dtSearchCriteria, 
            int NoofQuestions, out int TotalWeightage)
        {
            DataRow[] drWeightage = null;
            int GivenWeightage = 0;
            int SearchSegements = 0;
            int WeightagePercentage = 0;
            int LoopI = 0;
            int ToAddExtraWeightage = 0;
            try
            {
                SearchSegements = dtSearchCriteria.Rows.Count;
                drWeightage = dtSearchCriteria.Select("Weightage > 0");
                for (LoopI = 1; LoopI <= drWeightage.Length; LoopI++)
                    GivenWeightage += Convert.ToInt32(drWeightage[LoopI - 1]["Weightage"]);
                if ((GivenWeightage > 100) || ((GivenWeightage == 100) && (SearchSegements >= LoopI)))
                {
                    TotalWeightage = 101;
                    base.ShowMessage(EditInterviewTestQuestionRatings_topErrorMessageLabel,
                         EditInterviewTestQuestionRatings_bottomErrorMessageLabel,
                         Resources.HCMResource.CreateAutomaticInterviewTest_WeightageExceed);
                    return;
                }
                if (GivenWeightage == 0)
                    WeightagePercentage = 100 / SearchSegements;
                else if (GivenWeightage < 100)
                {
                    if (SearchSegements == drWeightage.Length)
                        dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"] = 100 -
                            (GivenWeightage - Convert.ToInt32(dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"]));
                    else
                        WeightagePercentage = (100 - GivenWeightage) / (SearchSegements - drWeightage.Length);
                }
                ToAddExtraWeightage = 100 - ((WeightagePercentage * (SearchSegements - (LoopI - 1))) + GivenWeightage);
                for (LoopI = 0; LoopI < SearchSegements; LoopI++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[LoopI]["Weightage"]))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] = WeightagePercentage;
                    if ((ToAddExtraWeightage > 0) && (WeightagePercentage != 0) && (LoopI == SearchSegements - 1))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["Weightage"]) + ToAddExtraWeightage;
                }
                TotalWeightage = 100;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drWeightage)) drWeightage = null;
            }
        }

        /// <summary>
        /// Builds data table from test segment grid view
        /// </summary>
        /// <returns>Data table that contains user given input for test segment grid</returns>
        private DataTable BuildTableFromGridView()
        {
            DataTable dttestDrft = null;
            try
            {
                dttestDrft = new DataTable();
                AddTestSegmentColumns(ref dttestDrft);

                DataRow drNewRow = null;
                int Weightage = 0;
                int SNo = 1;
                foreach (GridViewRow gridViewRow in EditInterviewTestQuestionRatings_testDrftGridView.Rows)
                {
                    drNewRow = dttestDrft.NewRow();
                    drNewRow["SNO"] = SNo++.ToString();

                    int.TryParse(((TextBox)gridViewRow.
                        FindControl("EditInterviewTestQuestionRatings_questionnWeightageGridViewTextbox")).Text, out Weightage);
                    if (Weightage > 0)
                        drNewRow["Weightage"] = Weightage;

                    dttestDrft.Rows.Add(drNewRow);
                    drNewRow = null;
                }
                return dttestDrft;
            }
            finally
            {
                if (dttestDrft != null) dttestDrft = null;
            }
        }

        /// <summary>
        /// Creates a Table structure for serach criteria
        /// </summary>
        /// <param name="dtTestSegmentGridColumns">Data table object to add columns (use ref: key word)</param>
        private void AddTestSegmentColumns(ref DataTable dtTestSegmentGridColumns)
        {
            dtTestSegmentGridColumns = new DataTable();
            dtTestSegmentGridColumns.Columns.Add("SNO", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Weightage", typeof(int));          
        }

        /// <summary>
        /// Constructs the question information.
        /// </summary>
        /// <returns></returns>
        private List<QuestionDetail> ConstructQuestionInformation()
        {
            // Construct question detail object.     
            List<QuestionDetail> qd = new List<QuestionDetail>();
            List<QuestionDetail> questionDetails = new List<QuestionDetail>();

            if (Session["questionDetailTestDraftResultList"] != null)
                questionDetails = Session["questionDetailTestDraftResultList"] as List<QuestionDetail>;

            for (int i = 0; i < EditInterviewTestQuestionRatings_testDrftGridView.Rows.Count; i++)
            {
                QuestionDetail questionDetail = new QuestionDetail();
                questionDetail = questionDetails[i];

                TextBox EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox = 
                    (TextBox)EditInterviewTestQuestionRatings_testDrftGridView.Rows[i].
                    FindControl("EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox");

                if(EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox!=null)
                    questionDetail.TimeLimit =  
                        Convert.ToInt32(Forte.HCM.Support.Utility.ConvertMinutesSecondsToSeconds(
                        EditInterviewTestQuestionRatings_questionTimetakenGridViewTextbox.Text));

                DropDownList EditInterviewTestQuestionRatings_questionRatingsGridViewDropDownList = 
                    (DropDownList)EditInterviewTestQuestionRatings_testDrftGridView.Rows[i].
                    FindControl("EditInterviewTestQuestionRatings_questionRatingsGridViewDropDownList");
                questionDetail.InterviewTestRatings = 
                    Convert.ToInt32(EditInterviewTestQuestionRatings_questionRatingsGridViewDropDownList.SelectedValue);

                TextBox EditInterviewTestQuestionRatings_questionnWeightageGridViewTextbox =
                   (TextBox)EditInterviewTestQuestionRatings_testDrftGridView.Rows[i].
                   FindControl("EditInterviewTestQuestionRatings_questionnWeightageGridViewTextbox");

                if (EditInterviewTestQuestionRatings_questionnWeightageGridViewTextbox != null)
                    questionDetail.Weightage =
                        Convert.ToInt32(EditInterviewTestQuestionRatings_questionnWeightageGridViewTextbox.Text);
                
                TextBox EditInterviewTestQuestionRatings_questionCommentsGridViewTextbox =
                    (TextBox)EditInterviewTestQuestionRatings_testDrftGridView.Rows[i].
                    FindControl("EditInterviewTestQuestionRatings_questionCommentsGridViewTextbox");

                if (EditInterviewTestQuestionRatings_questionCommentsGridViewTextbox != null)
                    questionDetail.InterviewTestQuestionComments = 
                        EditInterviewTestQuestionRatings_questionCommentsGridViewTextbox.Text; 
                qd.Add(questionDetail);
            }
            return qd;
        }

        #region Protected Overidden Methods

        /// <summary>
        /// Method which loads the default values and settings.
        /// </summary>
        protected override void LoadValues()
        {
            // Set default button
            Page.Form.DefaultButton = EditInterviewTestQuestionRatings_topCancelLinkButton.UniqueID;

            EditInterviewTestQuestionRatings_topCreateSessionButton.Visible = false;
            EditInterviewTestQuestionRatings_bottomCreateSessionButton.Visible = false;

            // Clear label messages
            EditInterviewTestQuestionRatings_topSuccessMessageLabel.Text = string.Empty;
            EditInterviewTestQuestionRatings_bottomSuccessMessageLabel.Text = string.Empty;

            InterviewDetail testDetail = null;
            testDetail = new InterviewDetail();          
       
            if (Session["interviewDetail"] != null)
            {
                testDetail = Session["interviewDetail"] as InterviewDetail;
                EditInterviewTestQuestionRatings_testDetailsUserControl.TestDetailDataSource = testDetail;
            }

            List<QuestionDetail> questionDetails = new List<QuestionDetail>();

            
            QuestionDetail questionDetailSearchResult = new QuestionDetail();

            questionAttributes = new InterviewBLManager().
                GetInterviewTestQuestionAttributes(testDetail.InterviewTestKey);

            if (Session["questionDetailTestDraftResultList"] != null)
                questionDetails = Session["questionDetailTestDraftResultList"] as List<QuestionDetail>;

            ViewState["INTERVIEWTESTQUESTIONATTRIBUTES"] = questionAttributes;

            //questionDetailSearchResult = questionAttributes.Find(p => p.QuestionKey.Trim() == "IQN-00000029");
            //questionDetailSearchResult.Rating = questionDetailSearchResult.Rating;
            //questionDetailSearchResult.Comments = questionDetailSearchResult.Comments;
            //questionDetails.Add(questionDetailSearchResult);


            // Bind gridview
            EditInterviewTestQuestionRatings_testDrftGridView.DataSource = questionDetails;
            EditInterviewTestQuestionRatings_testDrftGridView.DataBind();
        }

        /// <summary>
        /// Helps to validate the input data if it is provided.
        /// </summary>
        /// <returns></returns>
        protected override bool IsValidData()
        { return false; }

        #endregion Protected Overidden Methods

    }
}