#region Header                                                                                        

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EditManualTestWithoutAdaptive.cs
// File that represents the user Serach the Question by various Key filed. 
// and Modified the Exists test Details and questions if Test included the Some of Session the new test will be created.
// This will helps edit & create a Test and have to implemeted the Certification details..

#endregion

#region Directives                                                                                    
using System;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Data;
using System.Web;
using System.IO;
#endregion
namespace Forte.HCM.UI.InterviewTestMaker
{
    public partial class EditManualInterviewTest : PageBase
    {
        #region Declaration                                                                           
        string onmouseoverStyle = "className='grid_normal_row'";
        string onmouseoutStyle = "className='grid_alternate_row'";
        string parentPage = "";
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";
        string testKeyQuestyString = "";
        string questionID = "";
        List<QuestionDetail> questionDetailSearchResultList;
        List<QuestionDetail> interviewQuestionDraftList;
        #endregion

        #region Event Handlers                                                                        

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                parentPage = Request.QueryString.Get("parentpage");

                if (EditManualInterviewTest_mainTabContainer.ActiveTabIndex == 0)
                    Page.Form.DefaultButton = EditManualInterviewTest_topSearchButton.UniqueID;
                else if (EditManualInterviewTest_mainTabContainer.ActiveTabIndex == 1)
                    Page.Form.DefaultButton = EditManualInterviewTest_bottomSaveButton.UniqueID;
                
                MessageLabel_Reset();
                Master.SetPageCaption("Edit Interview");
                EditManualInterviewTest_ConfirmPopupPanel.Style.Add("height", "220px");


                if (!IsPostBack)
                {
                    LoadValues();
                    EditManualInterviewTest_manualImageButton.Visible = false;
                    EditManualInterviewTest_adaptiveImageButton.Visible = false;
                    EditManualInterviewTest_bothImageButton.Visible = false;
                    EditManualInterviewTest_simpleSearchDiv.Visible = true;
                    EditManualInterviewTest_advanceSearchDiv.Visible = false;
                    EditManualInterviewTest_questionDiv.Visible = false;

                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTDIRECTIONKEY"]))
                        ViewState["SORTDIRECTIONKEY"] = "A";
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTEXPRESSION"]))
                        ViewState["SORTEXPRESSION"] = "QUESTIONKEY";
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                        ViewState["PAGENUMBER"] = "1";
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
                        ViewState["SEARCHRESULTGRID"] = null;
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
                        testKeyQuestyString = Request.QueryString["testkey"].ToUpper();

                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["type"]))
                    {
                        EditManualInterviewTest_topCreateSessionButton.Visible = false;
                        EditManualInterviewTest_bottomCreateSessionButton.Visible = false;
                    }
                    
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["mode"]))
                    {
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["showmessage"]))
                        {
                            if (Request.QueryString["mode"].ToLower() == "new")
                            {
                                base.ShowMessage(EditManualInterviewTest_topSuccessMessageLabel,
                                        EditManualInterviewTest_bottomSuccessMessageLabel, string.Format(Resources.HCMResource.EditManualInterviewTest_AddedSuccessfully, testKeyQuestyString));
                            }
                            else if (Request.QueryString["mode"].ToLower() == "edit")
                            {
                                base.ShowMessage(EditManualInterviewTest_topSuccessMessageLabel,
                                       EditManualInterviewTest_bottomSuccessMessageLabel, string.Concat("Interview ", testKeyQuestyString, " updated successfully"));
                            }
                            else if (Request.QueryString["mode"].ToLower() == "copy")
                            {
                                base.ShowMessage(EditManualInterviewTest_topSuccessMessageLabel,
                                       EditManualInterviewTest_bottomSuccessMessageLabel, string.Format(Resources.HCMResource.EditManualInterviewTest_CopySuccessfully, testKeyQuestyString));
                                EditManualInterviewTest_topCreateSessionButton.Visible = true;
                                EditManualInterviewTest_bottomCreateSessionButton.Visible = true;
                            }
                        }
                    }

                    EditManualInterviewTest_bottomCreateSessionButton.PostBackUrl = 
                        "CreateInterviewTestSession.aspx?m=1&s=2&parentpage=" +
                        Constants.ParentPage.EDIT_INTERVIEW_TEST + "&interviewTestkey=" + testKeyQuestyString;
                    EditManualInterviewTest_topCreateSessionButton.PostBackUrl = 
                        "CreateInterviewTestSession.aspx?m=1&s=2&parentpage=" +
                        Constants.ParentPage.EDIT_INTERVIEW_TEST + "&interviewTestkey=" + testKeyQuestyString;

                    EditManualInterviewTest_questionKeyHiddenField.Value = testKeyQuestyString;

                    BindInterviewTestQuestions(testKeyQuestyString);
                    BindTestDetails(testKeyQuestyString);

                    EditManualInterviewTest_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('" 
                        + EditManualInterviewTest_dummyAuthorID.ClientID + "','" 
                        + EditManualInterviewTest_authorIdHiddenField.ClientID + "','"
                        + EditManualInterviewTest_authorTextBox.ClientID + "','QA')");

                    EditManualInterviewTest_positionProfileImageButton.Attributes.Add("onclick",
                            "return ShowClientRequestForManual('" + EditManualInterviewTest_positionProfileTextBox.ClientID + "','" +
                            EditManualInterviewTest_positionProfileKeywordTextBox.ClientID + "','" +
                             EditManualInterviewTest_positionProfileHiddenField.ClientID + "')");

                    EditManualInterviewTest_testDrftGridView.DataSource = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                    EditManualInterviewTest_testDrftGridView.DataBind();
                }
                else
                {
                    //  EditManualInterviewTest_questionDiv.Visible = true;
                }
                questionID = hiddenValue.Value;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionID))
                    MoveToTestDraft("Add");
                ExpandRestore();
                EditManualInterviewTest_bottomPagingNavigator.PageNumberClick += new
                   PageNavigator.PageNumberClickEventHandler
                      (EditManualInterviewTest_pagingNavigator_PageNumberClick);

                // Create events for paging control
                EditManualInterviewTest_adaptivebottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (EditManualInterviewTest_adaptivepagingNavigator_PageNumberClick);

                EditManualInterviewTest_categorySubjectControl.ControlMessageThrown += new CategorySubjectControl.ControlMessageThrownDelegate(EditManualInterviewTest_categorySubjectControl_ControlMessageThrown);
                HideLastRecord();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// It shows error/success messages whenever the user calls this method
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        void EditManualInterviewTest_categorySubjectControl_ControlMessageThrown(object sender, ControlMessageEventArgs c)
        {
            // Show message on the label.
            if (c.MessageType == MessageType.Error)
            {
                EditManualInterviewTest_topErrorMessageLabel.Text = c.Message.ToString();
                EditManualInterviewTest_bottomErrorMessageLabel.Text = c.Message.ToString();
            }
            else if (c.MessageType == MessageType.Success)
            {
                EditManualInterviewTest_topSuccessMessageLabel.Text = c.Message.ToString();
                EditManualInterviewTest_bottomSuccessMessageLabel.Text = c.Message.ToString();
            }
        }


        /// <summary>
        /// Handles the Click event of the EditManualInterviewTest_addQuestionLinkButton_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void EditManualInterviewTest_addQuestionLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                EditManualInterviewTest_addQuestionErrorLabel.Text = string.Empty;
                EditManualInterviewTest_addQuestionQuestTextBox.Text = string.Empty;
                EditManualInterviewTest_questionImage.ImageUrl = string.Empty;
                EditManualInterviewTest_addQuestionAnswerTextBox.Text = string.Empty;
                EditManualInterviewTest_isQuestionRepository.Checked = false;
                EditManualInterviewAddQuestion_byQuestion_categoryTextBox.Text = string.Empty;
                EditManualInterviewAddQuestion_byQuestion_subjectTextBox.Text = string.Empty;
                EditManualInterviewTest_tagsTextBox.Text = string.Empty;
                

                // Binding test area to radiobutton list.
                EditManualInterviewTest_addQuestionRadioButtonList.DataSource =
               new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
                EditManualInterviewTest_addQuestionRadioButtonList.DataTextField = "AttributeName";
                EditManualInterviewTest_addQuestionRadioButtonList.DataValueField = "AttributeID";
                EditManualInterviewTest_addQuestionRadioButtonList.DataBind();
                EditManualInterviewTest_addQuestionRadioButtonList.Items[0].Selected = true;

                //Bind complexities/attributes based on the attribute type
                EditManualInterviewTest_addQuestioncomplexityDropDownList.DataSource =
                    new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
                EditManualInterviewTest_addQuestioncomplexityDropDownList.DataTextField = "AttributeName";
                EditManualInterviewTest_addQuestioncomplexityDropDownList.DataValueField = "AttributeID";
                EditManualInterviewTest_addQuestioncomplexityDropDownList.DataBind();

                EditManualInterviewTest_addQuestioncomplexityDropDownList.Items.Insert(0, "--Select--");
                EditManualInterviewTest_addQuestioncomplexityDropDownList.SelectedIndex = 0;

                
                EditManualInterviewAddQuestion_byQuestion_categoryImageButton.Attributes.Add("onclick",
                        "return LoadCategorySubjectLookUpforReadOnly('" + EditManualInterviewAddQuestion_byQuestion_categoryTextBox.ClientID +
                            "','" + EditManualInterviewAddQuestion_byQuestion_subjectTextBox.ClientID + "','" +
                            EditManualInterviewAddQuestion_byQuestion_categoryHiddenField.ClientID + "','" +
                            EditManualInterviewAddQuestion_byQuestion_categoryNameHiddenField.ClientID + "','" +
                            EditManualInterviewAddQuestion_byQuestion_subjectNameHiddenField.ClientID + "');");
                EditManualInterviewTest_addQuestioModalPopupExtender.Show();

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                    EditManualInterviewTest_bottomErrorMessageLabel, exception.Message);

            }
        }


        /// <summary>
        /// Handles the Clicked event of the EditManualInterviewTest_addQuestioSaveButton_Clicked control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void EditManualInterviewTest_addQuestioSaveButton_Clicked
                (object sender, EventArgs e)
        {
            try
            {
                
                if (EditManualInterviewTest_addQuestionQuestTextBox.Text.Length == 0)
                {
                    EditManualInterviewTest_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(EditManualInterviewTest_addQuestionErrorLabel, "Enter the question");
                    EditManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (EditManualInterviewTest_addQuestionAnswerTextBox.Text.Length == 0)
                {
                    EditManualInterviewTest_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(EditManualInterviewTest_addQuestionErrorLabel, "Enter the answer");
                    EditManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (EditManualInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Text == "--Select--")
                {
                    EditManualInterviewTest_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(EditManualInterviewTest_addQuestionErrorLabel, "Select the complexity");

                    EditManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }

                if (EditManualInterviewAddQuestion_byQuestion_categoryTextBox.Text.Length == 0)
                {
                    EditManualInterviewTest_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(EditManualInterviewTest_addQuestionErrorLabel, "Select the category and subject");
                    EditManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (EditManualInterviewAddQuestion_byQuestion_subjectTextBox.Text.Length == 0)
                {
                    EditManualInterviewTest_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(EditManualInterviewTest_addQuestionErrorLabel, "Select the category and subject");
                    EditManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }

                SaveNewQuestion();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                    EditManualInterviewTest_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void EditManualInterviewTest_okClick(object sender, EventArgs e)
        {
            
            try
            {
                MoveToTestDraft("Delete");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel, 
                    EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
            
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void EditManualInterviewTest_searchQuestionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    string[] questionCollection = e.CommandArgument.ToString().Trim().Split(':');
                    EditManualInterviewTest_previewQuestionAddHiddenField.Value = e.CommandArgument.ToString();
                    EditManualInterviewTest_questionDetailSummaryControl.ShowAddButton = true;
                    
                    EditManualInterviewTest_questionDetailSummaryControl.LoadQuestionDetails
                        (questionCollection[0], int.Parse(questionCollection[1]));
                    EditManualInterviewTest_questionDetailSummaryControl.Visible = true;

                    EditManualInterviewTest_questionModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void EditManualInterviewTest_searchQuestionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField EditManualInterviewTest_hidddenValue = (HiddenField)e.Row.FindControl("EditManualInterviewTest_hidddenValue");
                    string questionID = EditManualInterviewTest_hidddenValue.Value.Split(':')[0].ToString();
                    ImageButton alertImageButton = (ImageButton)e.Row.FindControl("EditManualInterviewTest_searchQuestionAlertImageButton");
                    alertImageButton.Attributes.Add("OnClick", "javascript:return LoadFlagQuestion('" + questionID + "');");

                    Image img = (Image)e.Row.FindControl("EditManualInterviewTest_searchQuestionSelectImage");
                    img.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + EditManualInterviewTest_searchQuestionGridView.ClientID + "');");
                    img.Attributes.Add("OnClick", "javascript:return singleqnsclick('" + EditManualInterviewTest_hidddenValue.Value + "','" + EditManualInterviewTest_selectMultipleImage.ClientID + "');");

                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);
                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }
      
        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void EditManualInterviewTest_testDrftGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Image img = (Image)e.Row.FindControl("EditManualInterviewTest_testDrftRemoveImage");
                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);
                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);

                    ImageButton dowmImageButton = (ImageButton)e.Row.FindControl
                        ("EditManualInterviewTest_byQuestion_moveDownQuestionImageButton");

                    ImageButton upImageButton = (ImageButton)e.Row.FindControl
                        ("EditManualInterviewTest_byQuestion_moveUpQuestionImageButton");

                    HiddenField displayOrderHiddenField = (HiddenField)e.Row.FindControl
                        ("CreateManualInterviewTest_displayOrderHiddenField");

                    if (e.Row.RowIndex == 0) 
                        upImageButton.Visible = false;

                  
                    //HiddenField EditManualInterviewTest_testDraftQuestionKeyHiddenField = (HiddenField)e.Row.FindControl(
                    //          "EditManualInterviewTest_testDraftQuestionKeyHiddenField");

                    //HiddenField EditManualInterviewTest_correctAnswerHiddenField = (HiddenField)e.Row.FindControl(
                    //         "EditManualInterviewTest_correctAnswerHiddenField");

                    //string questionID = EditManualInterviewTest_testDraftQuestionKeyHiddenField.Value.Split(':')[0].ToString().Trim();
                    //PlaceHolder EditManualInterviewTest_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl("EditManualInterviewTest_answerChoicesPlaceHolder");
                    //EditManualInterviewTest_answerChoicesPlaceHolder.Controls.Add
                    //                    (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetQuestionOptions(questionID), true));

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void EditManualInterviewTest_testDrftGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    EditManualInterviewTest_questionModalPopupExtender.Show();
                }
                else if (e.CommandName == "Select")
                {
                    hiddenDeleteValue.Value = e.CommandArgument.ToString();
                    EditManualInterviewTest_ConfirmPopupExtenderControl.Message =string.Format(Resources.HCMResource.SearchQuestion_Delete_Confirm_Message,e.CommandArgument.ToString().Split(':')[0]);
                    EditManualInterviewTest_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    EditManualInterviewTest_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    EditManualInterviewTest_ConfirmPopupExtender.Show();
                }
                else if ((e.CommandName == "MoveDown") || (e.CommandName == "MoveUp"))
                {
                    interviewQuestionDraftList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

                    int order = int.Parse(e.CommandArgument.ToString());
                    //if the command name is move down , move the record one line down 
                    if (e.CommandName == "MoveDown")
                    {
                        foreach (QuestionDetail question in interviewQuestionDraftList)
                        {
                            //Change the display order of the draft list
                            if (question.DisplayOrder == order)
                            {
                                question.DisplayOrder = order + 1;
                                continue;
                            }
                            if (question.DisplayOrder == order + 1)
                            {
                                question.DisplayOrder = order;
                                continue;
                            }
                        }
                    }

                    //if the command name is moveup, move the record one line up
                    if (e.CommandName == "MoveUp")
                    {
                        foreach (QuestionDetail question in interviewQuestionDraftList)
                        {
                            if (question.DisplayOrder == order - 1)
                            {
                                question.DisplayOrder = order;
                                continue;
                            }
                            if (question.DisplayOrder == order)
                            {
                                question.DisplayOrder = order - 1;
                                continue;
                            }
                        }
                    }
                    EditManualInterviewTest_testDrftGridView.DataSource = 
                        interviewQuestionDraftList.OrderBy(p => p.DisplayOrder);
                    EditManualInterviewTest_testDrftGridView.DataBind();
                    ViewState["TESTDRAFTGRID"] = interviewQuestionDraftList.OrderBy(p => p.DisplayOrder).ToList();
                }
                HideLastRecord();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }
      
        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void EditManualInterviewTest_searchQuestionButton_Click(object sender, EventArgs e)
        {
            try
            {
                EditManualInterviewTest_bottomPagingNavigator.Reset();
                LoadQuestion(1, ViewState["SORTDIRECTIONKEY"].ToString(),
                     ViewState["SORTEXPRESSION"].ToString());
                MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the Move to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the move the Selected Question to Test Draft
        /// grid.
        /// </remarks>
        protected void EditManualInterviewTest_selectMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                AddSearchQuestionToTestDraft();
               
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }


        /// <summary>
        /// Handler method that will be called when the Remove to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the delete the Selected Question from Test Draft
        /// grid.
        /// </remarks>
        protected void EditManualInterviewTest_removeMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool questionSelected = false;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
                {
                    foreach (GridViewRow row in EditManualInterviewTest_testDrftGridView.Rows)
                    {
                        CheckBox EditManualInterviewTest_testDrftCheckbox = (CheckBox)row.FindControl("EditManualInterviewTest_testDrftCheckbox");
                        HiddenField EditManualInterviewTest_testDraftQuestionKeyHiddenField = (HiddenField)row.FindControl("EditManualInterviewTest_testDraftQuestionKeyHiddenField");
                        if (EditManualInterviewTest_testDrftCheckbox.Checked)
                        {
                            questionID = questionID + EditManualInterviewTest_testDraftQuestionKeyHiddenField.Value + ",";
                            questionSelected = true;
                        }
                    }
                  //  hiddenValue.Value = questionID.TrimEnd(',');
                   
                }
                if (questionSelected)
                {
                    hiddenDeleteValue.Value = questionID.TrimEnd(',');
                    EditManualInterviewTest_ConfirmPopupExtenderControl.Message = Resources.HCMResource.SearchQuestion_Delete_Multiple_Confirm_Message;
                    EditManualInterviewTest_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    EditManualInterviewTest_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    EditManualInterviewTest_ConfirmPopupExtender.Show();  
                    //MoveToTestDraft("Delete");
                }
                else
                {
                    MoveToTestDraft("");
                    base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                                       EditManualInterviewTest_bottomErrorMessageLabel, Resources.HCMResource.CreateManualInterviewTest_Question_From_InterviewTestDraft_Result);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Interchange Advanced and Simple Search Link 
        /// and Search Criteria fields 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        protected void EditManualInterviewTest_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (EditManualInterviewTest_simpleLinkButton.Text.ToLower() == "simple")
                {
                    EditManualInterviewTest_simpleLinkButton.Text = "Advanced";
                    EditManualInterviewTest_simpleSearchDiv.Visible = true;
                    EditManualInterviewTest_advanceSearchDiv.Visible = false;
                }
                else
                {
                    EditManualInterviewTest_simpleLinkButton.Text = "Simple";
                    EditManualInterviewTest_simpleSearchDiv.Visible = false;
                    EditManualInterviewTest_advanceSearchDiv.Visible = true;
                }
                MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void EditManualInterviewTest_pagingNavigator_PageNumberClick(object sender, 
            PageNumberEventArgs e)
        {
            MoveToTestDraft("");
            ViewState["PAGENUMBER"] = e.PageNumber;
            LoadQuestion(e.PageNumber, ViewState["SORTDIRECTIONKEY"].ToString(),
                ViewState["SORTEXPRESSION"].ToString());
        }

        /// <summary>
        /// Create Test and Test Question 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditManualInterviewTest_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValidData())
                {
                    //ShowSaveConfirmPopUp();
                    EditInterviewTest(true);
                }
                else
                {
                    MoveToTestDraft("");
                    if( EditManualInterviewTest_questionDiv.Visible == true)
                        LoadQuestion(Convert.ToInt32(ViewState["PAGENUMBER"].ToString()), ViewState["SORTDIRECTIONKEY"].ToString(),
                            ViewState["SORTEXPRESSION"].ToString());
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void EditManualInterviewTest_resetLinkButton_Click(object sender, EventArgs e)
        {

            Response.Redirect(Request.RawUrl +"&showmessage=n" , false);
        }

        /// <summary>
        /// Handler method that will be called when the With Adaptive button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void EditManualInterviewTest_adptiveTestLinkButton_Click(object sender, EventArgs e)
        {
            string type = "";
            if (Request.QueryString.Get("type") == "copy")
            {
                type = "&type=copy";
            }
            Response.Redirect(Constants.TestMakerConstants.EDIT_INTERIVIEW_TEST_WA
                + "?m=1&s=1&testkey=" + Request.QueryString["testkey"] + "&parentpage=" + parentPage + type, false);
        }
        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Add  Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel the Event will be raised.
        /// </remarks>
        protected void EditManualInterviewTest_questionDetailPreviewControl_AddClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = EditManualInterviewTest_previewQuestionAddHiddenField.Value;
                EditManualInterviewTest_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("Add");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Cancel Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel the Event will be raised.
        /// </remarks>
        protected void EditManualInterviewTest_cancelClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = "";
                EditManualInterviewTest_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("");
                //BindAdaptiveGridStateMaintain();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and 
        /// OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void EditManualInterviewTest_okPopUpClick(object sender, EventArgs e)
        {
            try
            {
                Button YesorNo = (Button)sender;
                if (YesorNo.Text.ToString() == "Ok")
                {
                    if (EditInterviewTestSave_confirmPopupExtenderControl.ReviewAndSave)
                        EditInterviewTest(true);
                    else
                        EditInterviewTest(false);
                }
                else
                    ShowSaveConfirmPopUp();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                    EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        ///  Event Handling for the common ConfirmMessage Popup Extenter Cancel and 
        ///  Close Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel/Close button Event will be raised.
        /// </remarks>
        protected void EditManualInterviewTest_cancelPopUpClick(object sender, EventArgs e)
        {
            DataTable dtTestSearchCriteria = null;
            try
            {
                //CreateManualInterviewTest_testDetailsUserControl.TestDetailDataSource = (TestDetail)Session["controlTestDetail"];
                //CreateManualInterviewTest_testDrftGridView.DataSource = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>; ;
                //CreateManualInterviewTest_testDrftGridView.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                    EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtTestSearchCriteria)) dtTestSearchCriteria = null;
            }
        }

        /// <summary>
        /// Handler that helps to save the posted file in session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditManualInterviewTest_questionImageOnUploadComplete(object sender,
            AsyncFileUploadEventArgs e)
        {
            System.Threading.Thread.Sleep(500);
            if (EditManualInterviewTest_questionImageUpload.PostedFile != null)
            {
                if (EditManualInterviewTest_questionImageUpload.PostedFile.ContentLength > 102400) // check if image size exceeds 100kb
                {
                    base.ShowMessage(EditManualInterviewTest_addQuestionErrorLabel,
                        Resources.HCMResource.BatchQuestionEntry_imageSizeExceeded);
                    EditManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }

                // Check if the photo is of the desired extension.
                string extension = Path.GetExtension(EditManualInterviewTest_questionImageUpload.FileName);
                extension = extension.ToLower();

                if (extension != ".gif" && extension != ".png" && extension != ".jpg" && extension != ".jpeg")
                {
                    base.ShowMessage(EditManualInterviewTest_addQuestionErrorLabel, "Only gif/png/jpg/jpeg files are allowed");
                    EditManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }

                // Read the bytes from the posted file.
                HttpPostedFile postedQstImage = EditManualInterviewTest_questionImageUpload.PostedFile;
                int nFileLen = postedQstImage.ContentLength;
                byte[] imgData = new byte[nFileLen];

                postedQstImage.InputStream.Read(imgData, 0, nFileLen);
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = imgData;

                EditManualInterviewTest_addQuestioModalPopupExtender.Show();
                return;
            }
            else
            {
                base.ShowMessage(EditManualInterviewTest_addQuestionErrorLabel, "Please select valid image to upload");
                EditManualInterviewTest_addQuestioModalPopupExtender.Show();
                return;
            }
        }

        /// <summary>
        /// Handler that helps to show the preview of the saved image
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>   
        protected void EditManualInterviewTest_questionImageButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["POSTED_INTERVIEW_QUESTION_IMAGE"] != null)
                {
                    EditManualInterviewTest_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=INTERVIEW_QUESTION_IMAGE";
                    EditManualInterviewTest_AddQuestionImageTR.Style.Add("display", "none");
                    EditManualInterviewTest_DisplayQuestionImageTR.Style.Add("display", "");
                    EditManualInterviewTest_addImageLinkButton.Style.Add("display", "none");
                    EditManualInterviewTest_addQuestioModalPopupExtender.Show();
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler that helps to delete the posted file.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EditManualInterviewTest_deleteLinkButtonClick(object sender, EventArgs e)
        {
            try
            {
                ResetQuestionImage();
                EditManualInterviewTest_addQuestioModalPopupExtender.Show();
                return;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                    exp.Message);
            }
        }
        #endregion

        #region Private  Methods                                                                      
       
        /// <summary>
        /// Method to reset the question image
        /// </summary>
        private void ResetQuestionImage()
        {
            Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            EditManualInterviewTest_questionImage.ImageUrl = string.Empty;
            EditManualInterviewTest_AddQuestionImageTR.Style.Add("display", "none");
            EditManualInterviewTest_DisplayQuestionImageTR.Style.Add("display", "none");
            EditManualInterviewTest_addImageLinkButton.Style.Add("display", "");
        }

        /// <summary>
        /// This method Invokes Confirm pop up when user tries to save
        /// the less number of questions in the test draft panel with the
        /// number of questions provided.
        /// </summary>
        /// 
        private void ShowSaveConfirmPopUp()
        {
            EditInterviewTestSave_confirmPopupExtenderControl.Title = "Save Options";
            EditInterviewTestSave_confirmPopupExtenderControl.Type = MessageBoxType.OkCancel;
            EditInterviewTestSave_confirmPopupExtenderControl.DataBind();
            EditInterviewTest_saveTestModalPopupExtender.Show();
        }

        /// <summary>
        /// Saves the new question.
        /// </summary>
        private void SaveNewQuestion()
        {
            List<QuestionDetail> questions = null;
            QuestionDetail questionDetail = null;
            QuestionBLManager questionBLManager = new QuestionBLManager();

            questionDetail = new QuestionDetail();
            questions = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            questionDetail = ConstructQuestionDetails();
            new QuestionBLManager().SaveInterviewQuestion(questionDetail);
            questionDetail.QuestionRelationId = questionBLManager.GetQuestionRelationID(questionDetail.QuestionKey);
            questionDetail.CategoryName = questionBLManager.GetCategory(questionDetail.QuestionKey);
            questionDetail.SubjectName = questionBLManager.GetSubject(questionDetail.QuestionKey);
            questionDetail.Complexity = questionDetail.ComplexityName;
            questionDetail.DisplayOrder = questions.Count + 1;
            List<AnswerChoice> questionAnswerList = new BL.QuestionBLManager().
            GetInterviewQuestionOptions(questionDetail.QuestionKey);
            if (questionAnswerList != null)
                questionDetail.Choice_Desc += questionAnswerList[0].Choice.ToString();

            questions.Add(questionDetail);
            ViewState["TESTDRAFTGRID"] = questions.ToList();

            EditManualInterviewTest_testDrftGridView.DataSource = questions;
            EditManualInterviewTest_testDrftGridView.DataBind();

            TestDraftSummary testDraftSummary = new TestDraftSummary();
            testDraftSummary.QuestionDetail = questions;
            testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questions);
            EditManualInterviewTest_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;
            HideLastRecord();
            ResetQuestionImage();
        }


        /// <summary>
        /// Constructs the question details.
        /// </summary>
        /// <returns></returns>
        private QuestionDetail ConstructQuestionDetails()
        {
            // Construct question detail object.
            QuestionDetail questionDetail = new QuestionDetail();

            QuestionBLManager questionBLManager = new QuestionBLManager();

            questionDetail.Question = EditManualInterviewTest_addQuestionQuestTextBox.Text.Trim();

            if (EditManualInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Value != null &&
                EditManualInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Value.Trim().ToUpper() != "--SELECT--")
            {
                questionDetail.Complexity = EditManualInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Value;
                questionDetail.ComplexityName = EditManualInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Text;
            }

            //Set question type

            if (EditManualInterviewTest_isQuestionRepository.Checked)
                questionDetail.IsActiveFlag = "Y";
            else
                questionDetail.IsActiveFlag = "N";

            // Construct the answer choice.
            List<AnswerChoice> answerChoices = new List<AnswerChoice>();
            AnswerChoice answerChoice = null;


            answerChoice = new AnswerChoice();
            answerChoice.ChoiceID = 1;
            answerChoice.Choice = EditManualInterviewTest_addQuestionAnswerTextBox.Text.Trim();
            answerChoices.Add(answerChoice);

            questionDetail.AnswerChoices = answerChoices;
            questionDetail.Author = Convert.ToInt32(base.userID);

            // Additional settings (Test AreaId).

            questionDetail.TestAreaID = EditManualInterviewTest_addQuestionRadioButtonList.SelectedValue;
            questionDetail.TestAreaName = EditManualInterviewTest_addQuestionRadioButtonList.SelectedItem.Text;

            questionDetail.Tag = EditManualInterviewTest_tagsTextBox.Text;

            questionDetail.CreatedBy = Convert.ToInt32(base.userID);
            questionDetail.CreatedDate = DateTime.Now;

            questionDetail.ModifiedBy = Convert.ToInt32(base.userID);

            // Get selected subject ids only. This will be used in QuestionBLManager:InsertQuestionRelation
            // method to insert multiple subjects simultanesouly.
            questionDetail.SelectedSubjectIDs = EditManualInterviewAddQuestion_byQuestion_categoryHiddenField.Value.ToString();

            if (Session["POSTED_INTERVIEW_QUESTION_IMAGE"] != null)
            {
                questionDetail.QuestionImage = Session["POSTED_INTERVIEW_QUESTION_IMAGE"] as byte[];
                questionDetail.HasImage = true;
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            }
            else
                questionDetail.HasImage = false;

            return questionDetail;
        }

        /// <summary>
        /// Hide move down image of the last record
        /// </summary>
        private void HideLastRecord()
        {
            foreach (GridViewRow row in EditManualInterviewTest_testDrftGridView.Rows)
            {
                if (row.RowIndex == EditManualInterviewTest_testDrftGridView.Rows.Count - 1)
                {
                    ImageButton dowmImageButton = (ImageButton)row.FindControl
                        ("EditManualInterviewTest_byQuestion_moveDownQuestionImageButton");
                    dowmImageButton.Visible = false;
                }
            }
        }

        /// <summary>
        /// Method handles Edit Interview Test Process.
        /// </summary>
        private void EditInterviewTest(bool isReviewQuestion)
        {
            questionDetailSearchResultList = new List<QuestionDetail>();
            questionDetailSearchResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            string testKey = "";
            
            Session["interviewDetail"] = null;
            Session["controlTestDetail"] = null;
            Session["interviewTestNew"] = null;

            string complexity= new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetailSearchResultList).AttributeID.ToString();
            int questionCount = Convert.ToInt32(questionDetailSearchResultList.Count.ToString());
            
            TestDetail testDetail = new TestDetail();
            testDetail = EditManualInterviewTest_testDetailsUserControl.TestDetailDataSource;
            testDetail.Questions = questionDetailSearchResultList;
            testDetail.IsActive = true;
            testDetail.CertificationId = 0;
            testDetail.CreatedBy = userID;
            testDetail.IsDeleted = false;
            testDetail.ModifiedBy = userID;
            testDetail.NoOfQuestions = questionCount;
            testDetail.TestAuthorID = userID;
            testDetail.TestCreationDate = DateTime.Now;
            testDetail.TestStatus = TestStatus.Active;
            testDetail.TestType = TestType.Genenal;
            testDetail.TestMode = "MANUAL";

            if (Request.QueryString.Get("type") == "copy")
            {
                testDetail.TestKey = testKey;

                InterviewDetail interviewDetail = new InterviewDetail();
                interviewDetail.InterviewTestKey = testDetail.TestKey;
                interviewDetail.InterviewName = testDetail.Name;
                interviewDetail.InterviewDescription = testDetail.Description;
                interviewDetail.PositionProfileId = Convert.ToInt32(testDetail.PositionProfileID);
                interviewDetail.PositionProfileName = testDetail.PositionProfileName;
                interviewDetail.TestAuthor = testDetail.TestAuthorName;
                interviewDetail.SessionIncludedActive = testDetail.SessionIncludedActive;
                interviewDetail.TestAuthorID = testDetail.TestAuthorID; ;
                interviewDetail.SessionIncluded = testDetail.SessionIncluded;
                interviewDetail.ParentInterviewKey = Request.QueryString["testkey"];

                Session["interviewDetail"] = (InterviewDetail)interviewDetail;
                Session["controlTestDetail"] = (TestDetail)testDetail;
                Session["questionDetailTestDraftResultList"] = questionDetailSearchResultList as List<QuestionDetail>;
                Session["isCopyInterview"] = "Y";
                EditManualInterviewTest_sessionInclucedKeyHiddenField.Value = "";
                Response.Redirect("~/InterviewTestMaker/AddInterviewTestQuestionRatings.aspx?m=1&s=2&parentpage=" + parentPage, false);
            }
            else
            {
                if (EditManualInterviewTest_sessionInclucedKeyHiddenField.Value.ToLower() == "true")
                {
                    // Check if feature usage limit exceeds for creating test.
                    if (new AdminBLManager().IsFeatureUsageLimitExceeds(base.tenantID, Constants.FeatureConstants.CREATE_TEST))
                    {
                        base.ShowMessage(EditManualInterviewTest_topSuccessMessageLabel,
                            EditManualInterviewTest_bottomSuccessMessageLabel, 
                            "You have reached the maximum limit based your subscription plan. Cannot create more tests");
                        return;
                    }
                    //testKey = new InterviewTestBLManager().SaveInterviewTest(testDetail, userID, complexity);
                    EditManualInterviewTest_sessionInclucedKeyHiddenField.Value = "";

                    InterviewDetail interviewDetail = new InterviewDetail();
                    interviewDetail.InterviewTestKey = EditManualInterviewTest_questionKeyHiddenField.Value;
                    interviewDetail.InterviewName = testDetail.Name;
                    interviewDetail.InterviewDescription = testDetail.Description;
                    interviewDetail.PositionProfileId = Convert.ToInt32(testDetail.PositionProfileID);
                    interviewDetail.PositionProfileName = testDetail.PositionProfileName;
                    interviewDetail.TestAuthor = testDetail.TestAuthorName;
                    interviewDetail.SessionIncludedActive = testDetail.SessionIncludedActive;
                    interviewDetail.TestAuthorID = testDetail.TestAuthorID; ;
                    interviewDetail.SessionIncluded = testDetail.SessionIncluded;

                    Session["interviewDetail"] = (InterviewDetail)interviewDetail;
                    Session["controlTestDetail"] = (TestDetail)testDetail;
                    Session["interviewTestNew"] = "Y";
                    Session["questionDetailTestDraftResultList"] = questionDetailSearchResultList as List<QuestionDetail>;
                    Response.Redirect("~/InterviewTestMaker/EditInterviewTestQuestionRatings.aspx?m=1&s=2&parentpage=" + parentPage, false);

                    //Response.Redirect("EditManualInterviewTest.aspx?m=1&s=2&mode=new&parentpage=" + parentPage + "&testkey=" + testKey, false);
                }
                else
                {
                    testDetail.TestKey = EditManualInterviewTest_questionKeyHiddenField.Value;
                    // Check if the question is modified by some body.
                    if (new CommonBLManager().IsRecordModified("INTERVIEW_TEST", testDetail.TestKey,
                        (DateTime)ViewState["MODIFIED_DATE"]))
                    {
                        ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                            EditManualInterviewTest_bottomErrorMessageLabel,
                                Resources.HCMResource.Edit_InterviewTest_Modifed_Another_User);

                        return;
                    }

                    if (isReviewQuestion)
                    {
                        InterviewDetail interviewDetail = new InterviewDetail();
                        interviewDetail.InterviewTestKey = testDetail.TestKey;
                        interviewDetail.InterviewName = testDetail.Name;
                        interviewDetail.InterviewDescription = testDetail.Description;
                        interviewDetail.PositionProfileId = Convert.ToInt32(testDetail.PositionProfileID);
                        interviewDetail.PositionProfileName = testDetail.PositionProfileName;
                        interviewDetail.TestAuthor = testDetail.TestAuthorName;
                        interviewDetail.SessionIncludedActive = testDetail.SessionIncludedActive;
                        interviewDetail.TestAuthorID = testDetail.TestAuthorID; ;
                        interviewDetail.SessionIncluded = testDetail.SessionIncluded;

                        Session["interviewDetail"] = (InterviewDetail)interviewDetail;
                        Session["controlTestDetail"] = (TestDetail)testDetail;
                        Session["questionDetailTestDraftResultList"] = questionDetailSearchResultList as List<QuestionDetail>;
                        Response.Redirect("~/InterviewTestMaker/EditInterviewTestQuestionRatings.aspx?m=1&s=2&parentpage=" + parentPage, false);
                    }
                    else
                    {
                        new InterviewBLManager().UpdateInterviewTest(testDetail, userID,
                            complexity, isReviewQuestion);
                        EditManualInterviewTest_sessionInclucedKeyHiddenField.Value = "";
                        Response.Redirect("EditManualInterviewTest.aspx?m=1&s=" + Request.QueryString["s"] +
                            "&mode=edit&parentpage=" + parentPage + "&testkey=" + testDetail.TestKey, false);
                    }
                }
            }

            TestDraftSummary testDraftSummary = new TestDraftSummary();
            testDraftSummary.QuestionDetail = questionDetailSearchResultList;
            testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, 
                questionDetailSearchResultList);
            EditManualInterviewTest_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;

            base.ShowMessage(EditManualInterviewTest_topSuccessMessageLabel,
                EditManualInterviewTest_bottomSuccessMessageLabel,
                string.Format(Resources.HCMResource.CreateManualInterviewTest_AddedSuccessfully, testKey));
        }

        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>
        private void ExpandRestore()
        {
            if (EditManualInterviewTest_restoreHiddenField.Value == "Y")
            {
                EditManualInterviewTest_searchCriteriasDiv.Style["display"] = "none";
                EditManualInterviewTest_searchResultsUpSpan.Style["display"] = "block";
                EditManualInterviewTest_searchResultsDownSpan.Style["display"] = "none";
                EditManualInterviewTest_questionDiv.Style["height"] =EXPANDED_HEIGHT;
            }
            else
            {
                EditManualInterviewTest_searchCriteriasDiv.Style["display"] = "block";
                EditManualInterviewTest_searchResultsUpSpan.Style["display"] = "none";
                EditManualInterviewTest_searchResultsDownSpan.Style["display"] = "block";
                EditManualInterviewTest_questionDiv.Style["height"] = RESTORED_HEIGHT;
            }
            EditManualInterviewTest_searchResultsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                EditManualInterviewTest_questionDiv.ClientID + "','" +
                EditManualInterviewTest_searchCriteriasDiv.ClientID + "','" +
                EditManualInterviewTest_searchResultsUpSpan.ClientID + "','" +
                EditManualInterviewTest_searchResultsDownSpan.ClientID + "','" +
                EditManualInterviewTest_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
        }

        /// <summary>
        /// Reset the Messeags.
        /// </summary>
        private void MessageLabel_Reset()
        {
            EditManualInterviewTest_topErrorMessageLabel.Text = string.Empty;
            EditManualInterviewTest_bottomErrorMessageLabel.Text = string.Empty;
            EditManualInterviewTest_topSuccessMessageLabel.Text = string.Empty;
            EditManualInterviewTest_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Add & remove Question from the Test Draft.
        /// </summary>
        private void AddSearchQuestionToTestDraft()
        {
            bool questionSelected = false;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
            {
                foreach (GridViewRow row in EditManualInterviewTest_searchQuestionGridView.Rows)
                {
                    CheckBox EditManualInterviewTest_searchQuestionCheckbox = (CheckBox)row.FindControl("EditManualInterviewTest_searchQuestionCheckbox");
                    HiddenField EditManualInterviewTest_searchQuestionKeyHiddenField = (HiddenField)row.FindControl("EditManualInterviewTest_searchQuestionKeyHiddenField");
                    if (EditManualInterviewTest_searchQuestionCheckbox.Checked)
                    {
                        questionID = questionID + EditManualInterviewTest_searchQuestionKeyHiddenField.Value + ",";
                        questionSelected = true;
                    }
                }
                hiddenValue.Value = questionID.TrimEnd(',');

            }
            if (questionSelected)
            {
                MoveToTestDraft("Add");
            }
            else
            {
                MoveToTestDraft("");
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                                   EditManualInterviewTest_bottomErrorMessageLabel, Resources.HCMResource.CreateManualTest_Question_From_Search_Result);
            }
        }

        /// <summary>
        /// Get Search Results with Paging and Sorting. 
        /// </summary>
        /// <param name="pageNumber">Current Page number </param>
        /// <param name="orderBy">QuestionKey/ModifiedDate/Complexity/Question Description/Credids Earned</param>
        /// <param name="sortDirection">Descending/Assending</param>
        private void LoadQuestion(int pageNumber, string orderBy, string sortDirection)
        {
            hiddenValue.Value = null;
            int totalRecords = 0;
            StringBuilder complexityID = GetSelectedComplexityID();
            StringBuilder testAreaID = GetTestAreaID();

            QuestionDetailSearchCriteria questionDetailSearchCriteria = new QuestionDetailSearchCriteria();

            if (EditManualInterviewTest_simpleLinkButton.Text == "Advanced")
            {
                questionDetailSearchCriteria.CategoryName =
                                    EditManualInterviewTest_categoryTextBox.Text.Trim() == "" ?
                                    null : EditManualInterviewTest_categoryTextBox.Text.Trim();
                questionDetailSearchCriteria.SubjectName =
                                    EditManualInterviewTest_subjectTextBox.Text.Trim() == "" ?
                                    null : EditManualInterviewTest_subjectTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    EditManualInterviewTest_keywordsTextBox.Text.Trim() == "" ?
                                    null : EditManualInterviewTest_keywordsTextBox.Text.Trim();
            }
            else
            {
                string SelectedSubjectIDs = "";
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(EditManualInterviewTest_categorySubjectControl.SubjectDataSource))
                {
                    List<Category> categoryList = new List<Category>();
                    foreach (Subject subject in EditManualInterviewTest_categorySubjectControl.SubjectDataSource)
                    {

                        if (subject.IsSelected == true)
                        {
                            Category category = new Category();
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                            category.CategoryID = subject.CategoryID;
                            categoryList.Add(category);
                        }

                    }
                    ///Get all Subject ids in  Un selected Subject List.
                    Category categoryID = new Category();
                    foreach (Subject subject in EditManualInterviewTest_categorySubjectControl.SubjectDataSource)
                    {
                        categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                        if (categoryID == null)
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                    }
                }
                questionDetailSearchCriteria.CategorySubjectsKey =
                                    SelectedSubjectIDs == "" ?
                                    null : SelectedSubjectIDs.TrimEnd(',');
                questionDetailSearchCriteria.TestAreaID =
                                    testAreaID.ToString() == "" ?
                                    null : testAreaID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.Complexities =
                                    complexityID.ToString() == "" ?
                                    null : complexityID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.QuestionKey =
                                    EditManualInterviewTest_questionIDTextBox.Text.Trim() == "" ?
                                    null : EditManualInterviewTest_questionIDTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    EditManualInterviewTest_keywordTextBox.Text.Trim() == "" ?
                                    null : EditManualInterviewTest_keywordTextBox.Text.Trim();
                questionDetailSearchCriteria.AuthorName =
                                    EditManualInterviewTest_authorTextBox.Text.Trim() == "" ?
                                    null : EditManualInterviewTest_authorTextBox.Text.Trim();
                questionDetailSearchCriteria.ActiveFlag = null;
                questionDetailSearchCriteria.CreditsEarned =
                                    EditManualInterviewTest_creditTextBox.Text.Trim() == "" ?
                                    0 : Convert.ToDecimal(EditManualInterviewTest_creditTextBox.Text.Trim());
                questionDetailSearchCriteria.ClientRequest = EditManualInterviewTest_positionProfileKeywordTextBox.Text.Trim() == "" ?
                  null : EditManualInterviewTest_positionProfileKeywordTextBox.Text.Trim();
            }


            List<QuestionDetail> questionDetail = new List<QuestionDetail>();
            questionDetail = new BL.InterviewBLManager().GetInterviewSearchQuestions(
                questionDetailSearchCriteria, base.GridPageSize, pageNumber, orderBy, sortDirection, out totalRecords);

            if (questionDetail == null || questionDetail.Count == 0)
            {
                EditManualInterviewTest_searchQuestionGridView.DataSource = null;
                EditManualInterviewTest_searchQuestionGridView.DataBind();
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                 EditManualInterviewTest_bottomErrorMessageLabel,Resources.HCMResource.Common_Empty_Grid);
                EditManualInterviewTest_bottomPagingNavigator.TotalRecords = 0;
                EditManualInterviewTest_questionDiv.Visible = false;
            }
            else
            {
                EditManualInterviewTest_searchQuestionGridView.DataSource = questionDetail;
                EditManualInterviewTest_searchQuestionGridView.DataBind();
                EditManualInterviewTest_bottomPagingNavigator.PageSize = base.GridPageSize;
                EditManualInterviewTest_bottomPagingNavigator.TotalRecords = totalRecords;
                EditManualInterviewTest_questionDiv.Visible = true;
            }
            ViewState["SEARCHRESULTGRID"] = questionDetail;
        }
       
        /// <summary>
        ///  Bind complexities/attributes based on the attribute type
        /// </summary>
        private void BindComplexities()
        {
            EditManualInterviewTest_complexityCheckBoxList.DataSource =
                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
            EditManualInterviewTest_complexityCheckBoxList.DataTextField = "AttributeName";
            EditManualInterviewTest_complexityCheckBoxList.DataValueField = "AttributeID";
            EditManualInterviewTest_complexityCheckBoxList.DataBind();
        }
      
        /// <summary>
        /// Bind test area based on TEST_AREA attribute type
        /// </summary>
        private void BindTestArea()
        {

            EditManualInterviewTest_testAreaCheckBoxList.DataSource =
               new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
            EditManualInterviewTest_testAreaCheckBoxList.DataTextField = "AttributeName";
            EditManualInterviewTest_testAreaCheckBoxList.DataValueField = "AttributeID";
            EditManualInterviewTest_testAreaCheckBoxList.DataBind();
        }

        /// <summary>
        /// Get Selected Test Area IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetTestAreaID()
        {
            StringBuilder testAreaID = new StringBuilder();
            for (int i = 0; i < EditManualInterviewTest_testAreaCheckBoxList.Items.Count; i++)
            {

                if (EditManualInterviewTest_testAreaCheckBoxList.Items[i].Selected)
                {

                    testAreaID.Append(EditManualInterviewTest_testAreaCheckBoxList.Items[i].Value.ToString());
                    testAreaID.Append(",");
                }

            }
            return testAreaID;
        }
     
        /// <summary>
        /// Get Selected Complexity IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetSelectedComplexityID()
        {
            StringBuilder complexityID = new StringBuilder();
            for (int i = 0; i < EditManualInterviewTest_complexityCheckBoxList.Items.Count; i++)
            {
                if (EditManualInterviewTest_complexityCheckBoxList.Items[i].Selected)
                {

                    complexityID.Append(EditManualInterviewTest_complexityCheckBoxList.Items[i].Value.ToString());
                    complexityID.Append(",");
                }
            }
            return complexityID;
        }
     
        /// <summary>
        /// Uncheck the checkbox 
        /// </summary>
        private void UncheckCheckBox()
        {
            foreach (GridViewRow row in EditManualInterviewTest_searchQuestionGridView.Rows)
            {
                if (((CheckBox)row.FindControl("EditManualInterviewTest_searchQuestionCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("EditManualInterviewTest_searchQuestionCheckbox")).Checked = false;
                }
            }
            foreach (GridViewRow row in EditManualInterviewTest_adaptiveGridView.Rows)
            {
                if (((CheckBox)row.FindControl("EditManualInterviewTest_adaptiveCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("EditManualInterviewTest_adaptiveCheckbox")).Checked = false;
                }
            }

        }
     
        /// <summary>
        /// add and remove the Test Draft 
        /// </summary>
        /// <param name="action"></param>
        private void MoveToTestDraft(string action)
        {
            questionDetailSearchResultList = new List<QuestionDetail>();
            List<QuestionDetail> questionDetailTestDraftResultList = new List<QuestionDetail>();
            QuestionDetail questionDetailSearchResult = new QuestionDetail();

            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenDeleteValue.Value))
            {
                hiddenValue.Value = hiddenDeleteValue.Value;
                hiddenDeleteValue.Value = null;
            }
            string addedQuestionID = "";
            string notAddedQuestion = "";
            bool alertFlag = false;
            questionDetailSearchResultList = ViewState["SEARCHRESULTGRID"] as List<QuestionDetail>;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
            {

                string[] questionIds = hiddenValue.Value.Split(',');

                foreach (string ids in questionIds)
                {
                    if (ids != "")
                    {
                        string[] idList = ids.Split(':');
                        string id = idList[0];
                        string relationId = idList[1];
                        if (ViewState["TESTDRAFTGRID"] != null)
                        {
                            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                            if (action == "Add")
                            {
                                if (!questionDetailTestDraftResultList.Exists(p => p.QuestionKey.Trim() == id.Trim()))
                                {
                                    MessageLabel_Reset();
                                    questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                    questionDetailSearchResult.DisplayOrder = questionDetailTestDraftResultList.Count + 1;
                                    List<AnswerChoice> questionAnswerList = new BL.QuestionBLManager().GetInterviewQuestionOptions(id);
                                    if (questionAnswerList != null)
                                        questionDetailSearchResult.Choice_Desc += questionAnswerList[0].Choice.ToString();
                                    questionDetailSearchResult.ComplexityName = questionDetailSearchResult.Complexity;
                                    questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                                    ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                                    addedQuestionID = addedQuestionID + id + ", ";
                                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(EditManualInterviewTest_insertedQuestionHiddenField.Value))
                                    {
                                        EditManualInterviewTest_insertedQuestionHiddenField.Value = questionDetailSearchResult.QuestionKey + ",";
                                    }
                                    else
                                    {
                                        EditManualInterviewTest_insertedQuestionHiddenField.Value = EditManualInterviewTest_insertedQuestionHiddenField.Value + questionDetailSearchResult.QuestionKey + ",";
                                    }
                                    base.ShowMessage(EditManualInterviewTest_topSuccessMessageLabel,
                                        EditManualInterviewTest_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToInterviewDraft);
                                }
                                else
                                {
                                    notAddedQuestion = notAddedQuestion + id + ", ";
                                    alertFlag = true;
                                }
                            }
                            else if (action == "Delete")
                            {
                                MessageLabel_Reset();
                                questionDetailSearchResult = questionDetailTestDraftResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                questionDetailTestDraftResultList.Remove(questionDetailSearchResult);
                                base.ShowMessage(EditManualInterviewTest_topSuccessMessageLabel,
                                    EditManualInterviewTest_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualInterviewTest_RemoveToTestDraft);
                                ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                                if (Forte.HCM.Support.Utility.IsNullOrEmpty(EditManualInterviewTest_deletedQuestionHiddenField.Value))
                                {
                                    EditManualInterviewTest_deletedQuestionHiddenField.Value = questionDetailSearchResult.QuestionKey + ",";
                                }
                                else
                                {
                                    EditManualInterviewTest_deletedQuestionHiddenField.Value = EditManualInterviewTest_deletedQuestionHiddenField.Value + questionDetailSearchResult.QuestionKey + ",";
                                }
                            }
                        }
                        else
                        {
                            questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                            questionDetailSearchResult.DisplayOrder = 1;
                            List<AnswerChoice> questionAnswerList = new BL.QuestionBLManager().GetInterviewQuestionOptions(id);
                            if (questionAnswerList != null)
                                questionDetailSearchResult.Choice_Desc += questionAnswerList[0].Choice.ToString();
                            questionDetailSearchResult.ComplexityName = questionDetailSearchResult.Complexity;
                            questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                            ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                            base.ShowMessage(EditManualInterviewTest_topSuccessMessageLabel,
                                        EditManualInterviewTest_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToInterviewDraft);
                        }

                    }
                }
            }
                if (alertFlag)
                {
                    string alertMessage = string.Format(Resources.HCMResource.CreateManualInterviewTest_QuestionExist, notAddedQuestion.Trim().TrimEnd(','));
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(addedQuestionID))
                    {
                        alertMessage = alertMessage + "<br/>" + string.Format(Resources.HCMResource.CreateManualTest_QuestionAddedToTestDraft, addedQuestionID.Trim().TrimEnd(','));
                    }
                    EditManualInterviewTest_ConfirmPopupPanel.Style.Add("height", "270px");
                    EditManualInterviewTest_ConfirmPopupExtenderControl.Type = MessageBoxType.Ok;
                    EditManualInterviewTest_ConfirmPopupExtenderControl.Title = "Warning";
                    EditManualInterviewTest_ConfirmPopupExtenderControl.Message = alertMessage;
                    EditManualInterviewTest_ConfirmPopupExtender.Show();

                }
                hiddenValue.Value = null;

                questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                EditManualInterviewTest_testDrftGridView.DataSource = questionDetailTestDraftResultList;
                EditManualInterviewTest_testDrftGridView.DataBind();
                if (questionDetailSearchResultList != null)
                {
                    EditManualInterviewTest_searchQuestionGridView.DataSource = questionDetailSearchResultList;
                    EditManualInterviewTest_questionDiv.Visible = true;
                }
                else
                {
                    EditManualInterviewTest_searchQuestionGridView.DataSource = null;
                    EditManualInterviewTest_questionDiv.Visible = false;
                }

                EditManualInterviewTest_searchQuestionGridView.DataBind();

                if (action != "")
                {
                    questionDetailSearchResultList = new List<QuestionDetail>();
                    questionDetailSearchResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

                    List<decimal> timeTaken = new List<decimal>();

                    for (int i = 0; i < questionDetailSearchResultList.Count; i++)
                    {
                        timeTaken.Add(questionDetailSearchResultList[i].AverageTimeTaken);
                    }

                    if (questionDetailTestDraftResultList.Count > 0)
                    {
                        EditManualInterviewTest_testDetailsUserControl.SysRecommendedTime = Convert.ToInt32(timeTaken.Average()) * questionDetailTestDraftResultList.Count;
                    }
                    else
                    {
                        EditManualInterviewTest_testDetailsUserControl.SysRecommendedTime = 0;
                    }

                    TestDraftSummary testDraftSummary = new TestDraftSummary();
                    testDraftSummary.QuestionDetail = questionDetailTestDraftResultList;
                    testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetailTestDraftResultList);
                    EditManualInterviewTest_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;

                }
            
            UncheckCheckBox();
            HideLastRecord();
        }

        /// <summary>
        /// Helps to bind test question details against the testkey
        /// </summary>
        /// <param name="testKey"></param>
        private void BindInterviewTestQuestions(string testKey)
        {
            int displayOrder = 0;
            List<QuestionDetail> questionDetail = new List<QuestionDetail>();
            questionDetail =
                new InterviewBLManager().GetInterviewTestQuestionDetail(testKey, "");
            
             TestDraftSummary testDraftSummary = new TestDraftSummary();
                testDraftSummary.QuestionDetail = questionDetail;
                testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetail);
                EditManualInterviewTest_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;
            //EditManualInterviewTest_testSummaryControl.LoadQuestionDetailsSummary(questionDetail, GetComplexity());
            foreach (QuestionDetail question in questionDetail)
            {
                question.DisplayOrder = displayOrder + 1;
                displayOrder++;
            }

            ViewState["TESTDRAFTGRID"] = questionDetail;
        }

        /// <summary>
        /// Helps to bind test details against the testkey
        /// </summary>
        /// <param name="testKey"></param>
        private void BindTestDetails(string testKey)
        {
            TestDetail testDetail = new TestDetail();
            testDetail = new InterviewBLManager().GetInterviewTestDetailView(testKey);
            EditManualInterviewTest_testDetailsUserControl.TestDetailDataSource = testDetail;
            EditManualInterviewTest_sessionInclucedKeyHiddenField.Value = testDetail.SessionIncluded.ToString();
            ViewState["MODIFIED_DATE"] = testDetail.ModifiedDate;

            if (Request.QueryString.Get("type") == "copy")
            {
                EditManualInterviewTest_topSaveButton.Text = "Copy Interview";
                EditManualInterviewTest_bottomSaveButton.Text = "Copy Interview";

                // Clear position profile details.
                EditManualInterviewTest_testDetailsUserControl.PositionProfileID = 0;
                EditManualInterviewTest_testDetailsUserControl.PositionProfileName = string.Empty;

                return;
            }
            if (EditManualInterviewTest_sessionInclucedKeyHiddenField.Value.ToLower() == "true")
            {
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                  EditManualInterviewTest_bottomErrorMessageLabel, string.Format(Resources.HCMResource.EditManualInterviewTest_TestAlreadyExists, testKey));
            }
        }
        protected void EditManualInterviewTest_addQuestionTopCancelImageButton_Click(object sender, ImageClickEventArgs e)
        {
            ResetQuestionImage();
        }
        protected void EditManualInterviewTest_addQuestioCloseLinkButton_Click(object sender, EventArgs e)
        {
            ResetQuestionImage();
        }
        #endregion

        #region Protected Overridden Methods                                                          

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool isQuestionTabActive = false;
            

            EditManualInterviewTest_topErrorMessageLabel.Text = string.Empty;
            EditManualInterviewTest_bottomErrorMessageLabel.Text = string.Empty;
            TestDetail testDetail = new TestDetail();
            testDetail = EditManualInterviewTest_testDetailsUserControl.TestDetailDataSource;
            

            questionDetailSearchResultList = new List<QuestionDetail>();
            questionDetailSearchResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailSearchResultList))
            {
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                    EditManualInterviewTest_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualInterviewTest_QuestionCount);
                isValidData = false;
                isQuestionTabActive = true;
            }
            else if (questionDetailSearchResultList.Count == 0)
            {
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                    EditManualInterviewTest_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualInterviewTest_QuestionCount);
                isValidData = false;
                isQuestionTabActive = true;
            }

            if (testDetail.Name.Trim().Length == 0)
            {
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateAutomaticInterviewTest_TestNameEmpty);
                isValidData = false;
            }
            if (testDetail.Description.Trim().Length == 0)
            {
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel,
                   Resources.HCMResource.CreateManualInterviewTest_Test_Description_Empty);
                isValidData = false;
            }
           
            if (isQuestionTabActive)
                EditManualInterviewTest_mainTabContainer.ActiveTab = EditManualInterviewTest_questionsTabPanel;
            else
                EditManualInterviewTest_mainTabContainer.ActiveTab = EditManualInterviewTest_testdetailsTabPanel;

            EditManualInterviewTest_testDetailsUserControl.TestDetailDataSource = testDetail;
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindTestArea();
            BindComplexities();
            EditManualInterviewTest_testSummaryControl.AverageTimeTakenByCandidatesLabel = false;
            EditManualInterviewTest_testSummaryControl.AverageTimeTakenByCandidatesText = false;
        }

        #endregion Protected Overridden Methods

        #region Not Used Now (Adaptive Question & Sorting)                                            

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void EditManualInterviewTest_searchQuestionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            //may be used in future.
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void EditManualInterviewTest_testDrftGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            //may be used in future    
        }

        protected void EditManualInterviewTest_adaptiveGridView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }
        protected void EditManualInterviewTest_adaptiveGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "view")
            {
                EditManualInterviewTest_questionModalPopupExtender.Show();
            }
            if (e.CommandName == "AdaptiveQuestion")
            {
                EditManualInterviewTest_adaptiveQuestionPreviewModalPopupExtender.Show();

                ViewState["RowIndex"] = (e.CommandArgument.ToString());
            }
        }
        protected void EditManualInterviewTest_adaptiveGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Image alertImage = (Image)e.Row.FindControl("EditManualInterviewTest_adaptiveAlertImageButton");
                    alertImage.Attributes.Add("onClick", "javascript:return LoadFlagQuestion();");

                    Image img = (Image)e.Row.FindControl("EditManualInterviewTest_adaptiveSelectImage");
                    img.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + EditManualInterviewTest_adaptiveGridView.ClientID + "');");
                    //(Image)e.Row.FindControl("EditManualInterviewTest_adaptiveSelectImage");

                    //img.Attributes.Add("onClick", "javascript:return AddRow(" + e.Row.RowIndex.ToString() + "," + 1 + ");");
                    // if (dtSearchQuestions.Rows[e.Row.RowIndex][0].ToString().Equals(""))
                    {
                        img.Visible = false;
                        ImageButton img1 = (ImageButton)e.Row.FindControl("EditManualInterviewTest_adaptiveAlertImageButton");
                        img1.Visible = false;
                        //Image img2 = (Image)e.Row.FindControl("EditManualInterviewTest_adaptiveViewImage");
                        //img2.Visible = false;
                        CheckBox chk = (CheckBox)e.Row.FindControl("EditManualInterviewTest_adaptiveCheckbox");
                        chk.Visible = false;
                    }

                    ImageButton imageButton = (ImageButton)e.Row.FindControl("EditManualInterviewTest_adaptiveQuestionDetailsImage");

                    imageButton.CommandArgument = e.Row.RowIndex.ToString();
                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
                    Label EditManualInterviewTest_questionLabel =
                         (Label)e.Row.FindControl("EditManualInterviewTest_questionLabel");

                    HoverMenuExtender EditManualInterviewTest_hoverMenuExtender = (HoverMenuExtender)
                        e.Row.FindControl("EditManualInterviewTest_hoverMenuExtender1");
                    EditManualInterviewTest_hoverMenuExtender.TargetControlID = EditManualInterviewTest_questionLabel.ID;

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditManualInterviewTest_topErrorMessageLabel,
                   EditManualInterviewTest_bottomErrorMessageLabel, exp.Message);

            }
        }
        protected void CreateManulTest_CloseButton_Click(object sender, EventArgs e)
        {
            MoveToTestDraft("");
            EditManualInterviewTest_questionModalPopupExtender.Show();
        }

        protected void EditManualInterviewTest_questionDetail_TopAddButton_Click(object sender, EventArgs e)
        {
            //EditManualInterviewTest_adaptiveQuestionPreviewModalPopupExtender.Hide();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "AddRow",
            //    "<script> AddRow(" + ViewState["RowIndex"] + "," + 1 + "); </script>", false);

            //ViewState["RowIndex"] = 0;

        }

        protected void QuestionDetail_topCancelButton_Click(object sender, EventArgs e)
        {
            EditManualInterviewTest_adaptiveQuestionPreviewModalPopupExtender.Hide();
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void EditManualInterviewTest_adaptivepagingNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {

        }

        #endregion


}
}
