﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchInterviewTest.aspx.cs" Inherits="Forte.HCM.UI.InterviewTestMaker.SearchInterviewTest"  MasterPageFile="~/MasterPages/InterviewMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl" TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl" TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>

<asp:Content ID="SearchInterviewTest_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="SearchInterviewTest_headerLiteral" runat="server" Text="Search Interview"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 42%">
                                        &nbsp;
                                    </td>
                                    <td style="width: 20%">
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="SearchInterviewTest_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="SearchInterviewTest_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="SearchInterviewTest_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="SearchInterviewTest_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="SearchInterviewTest_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchInterviewTest_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div id="SearchInterviewTest_searchByTestDiv" runat="server" style="display: block;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="right" class="td_padding_bottom_5">
                                            <asp:UpdatePanel runat="server" ID="SearchInterviewTest_simpleLinkUpdatePanel">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="SearchInterviewTest_simpleLinkButton" runat="server" Text="Advanced"
                                                        SkinID="sknActionLinkButton" OnClick="SearchInterviewTest_simpleLinkButton_Click" />
                                                    <asp:HiddenField ID="SearchInterviewTest_stateHiddenField" runat="server" Value="0" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="panel_bg">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="100%">
                                                        <asp:UpdatePanel runat="server" ID="SearchInterviewTest_SearchDivUpdatePanel">
                                                            <ContentTemplate>
                                                                <div id="SearchInterviewTest_simpleSearchDiv" runat="server" style="display: block;">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="SearchInterviewTest_categoryHeadLabel" runat="server" Text="Category"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="SearchInterviewTest_categoryTextBox" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="SearchInterviewTest_subjectHeadLabel" runat="server" Text="Subject"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="SearchInterviewTest_subjectTextBox" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="SearchInterviewTest_keywordsHeadLabel" runat="server" Text="Keyword"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <div style="float: left; padding-right: 5px;">
                                                                                    <asp:TextBox ID="SearchInterviewTest_keywordSimpleTextBox" runat="server"></asp:TextBox>
                                                                                </div>
                                                                                <div style="float: left;">
                                                                                    <asp:ImageButton ID="SearchInterviewTest_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, Search_Interview_KeywordHelp %>" />
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div id="SearchInterviewTest_advanceSearchDiv" runat="server" style="width: 100%"
                                                                    visible="false">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <uc3:CategorySubjectControl ID="SearchInterviewTest_categorySubjectControl" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_8">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="panel_inner_body_bg">
                                                                                <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="SearchInterviewTest_testAreaHeadLabel" runat="server" Text="Interview Area"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td class="checkbox_list_bg" style="width: 100%;" colspan="5">
                                                                                            <asp:CheckBoxList ID="SearchInterviewTest_testAreaCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                            </asp:CheckBoxList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 10%;">
                                                                                            <asp:Label ID="SearchInterviewTest_testIdLabel" runat="server" Text="Interview ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%;">
                                                                                            <asp:TextBox ID="SearchInterviewTest_testIdTextBox" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td style="width: 16%">
                                                                                            <asp:Label ID="SearchInterviewTest_testNameLabel" runat="server" Text="Interview Name"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:TextBox ID="SearchInterviewTest_testNameTextBox" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td style="width: 10%;">
                                                                                            <asp:Label ID="SearchInterviewTest_authorHeadLabel" runat="server" Text="Author"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 19%;">
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="SearchInterviewTest_authorTextBox" runat="server"></asp:TextBox>
                                                                                                <asp:HiddenField ID="SearchInterviewTest_dummyAuthorID" runat="server" />
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="SearchInterviewTest_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the interview author" /></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchInterviewTest_certificateTestHeadLabel" runat="server" Text="Certificate Interview"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:DropDownList ID="SearchInterviewTest_certificateTestDropDownList" runat="server"
                                                                                                Width="133px">
                                                                                                <asp:ListItem Text="--Select--" Value="S"></asp:ListItem>
                                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchInterviewTest_positionProfileLabel" runat="server" Text="Position Profile"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="SearchInterviewTest_positionProfileTextBox" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="SearchInterviewTest_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the position profile" />
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchInterviewTest_keywordHeadLabel" runat="server" Text="Keyword"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="SearchInterviewTest_keywordAdvanceTextBox" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                &nbsp;<asp:ImageButton ID="SearchInterviewTest_keywordImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, Search_Interview_KeywordHelp %>" />
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchInterviewTest_noOfQuestionsLabel" runat="server" Text="Number of Questions"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table width="40%" border="0" cellpadding="1" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="SearchInterviewTest_noOfQuestionsMinValueTextBox" runat="server"
                                                                                                            MaxLength="5" Width="25"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td style="width: 60%">
                                                                                                        <asp:TextBox ID="SearchInterviewTest_noOfQuestionsTextBox" runat="server" MaxLength="225"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="SearchInterviewTest_noOfQuestionsMaxValueTextBox" runat="server"
                                                                                                            MaxLength="5" Width="25"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <ajaxToolKit:MultiHandleSliderExtender ID="SearchInterviewTest_noOfQuestionsMultiHandleSliderExtender"
                                                                                                runat="server" Enabled="True" HandleAnimationDuration="0.1" Maximum="100" Minimum="0"
                                                                                                TargetControlID="SearchInterviewTest_noOfQuestionsTextBox" RailCssClass="slider_rail"
                                                                                                HandleCssClass="slider_handler" ShowHandleDragStyle="true" ShowHandleHoverStyle="true">
                                                                                                <MultiHandleSliderTargets>
                                                                                                    <ajaxToolKit:MultiHandleSliderTarget ControlID="SearchInterviewTest_noOfQuestionsMinValueTextBox"
                                                                                                        HandleCssClass="MultiHandleMinSliderCss" />
                                                                                                    <ajaxToolKit:MultiHandleSliderTarget ControlID="SearchInterviewTest_noOfQuestionsMaxValueTextBox"
                                                                                                        HandleCssClass="MultiHandleMaxSliderCss" />
                                                                                                </MultiHandleSliderTargets>
                                                                                            </ajaxToolKit:MultiHandleSliderExtender>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchInterviewTest_showCopiedTestsLabel" runat="server" Text="Include Copied Interviews"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:CheckBox ID="SearchInterviewTest_showCopiedTestsCheckBox" runat="server"></asp:CheckBox>
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                &nbsp;<asp:ImageButton ID="SearchInterviewTest_showCopiedTestsImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Check this to search for copied interviews too" />
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="td_padding_top_5">
                                                        <asp:Button ID="SearchInterviewTest_topSearchButton" runat="server" Text="Search"
                                                            OnClick="SearchInterviewTest_topSearchButton_Click" SkinID="sknButtonId" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr id="SearchInterviewTest_SearchInterviewTestResultsTR" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="SearchInterviewTest_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                            ID="SearchInterviewTest_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="SearchInterviewTest_searchResultsUpSpan" style="display: none;" runat="server">
                                            <asp:Image ID="SearchInterviewTest_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                        </span><span id="SearchInterviewTest_searchResultsDownSpan" style="display: block;"
                                            runat="server">
                                            <asp:Image ID="SearchInterviewTest_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                        </span>
                                        <asp:HiddenField ID="SearchInterviewTest_restoreHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel runat="server" ID="SearchInterviewTest_testGridViewUpdatePanel">
                                            <ContentTemplate>
                                                <div style="height: 200px; overflow: auto;" runat="server" id="SearchInterviewTest_testDiv">
                                                    <asp:GridView ID="SearchInterviewTest_testGridView" runat="server" OnSorting="SearchInterviewTest_testGridView_Sorting"
                                                        OnRowDataBound="SearchInterviewTest_testGridView_RowDataBound" OnRowCommand="SearchInterviewTest_testGridView_RowCommand"
                                                        OnRowCreated="SearchInterviewTest_testGridView_RowCreated">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="140px">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="SearchInterviewTest_additionalDetailsImageButton" runat="server"
                                                                        SkinID="sknAdditionalDetailImageButton" ToolTip="Interview Statistics" />
                                                                    <asp:HiddenField ID="SearchInterviewTest_testKeyHiddenField" runat="server" Value='<%# Eval("TestKey") %>' />
                                                                    <asp:ImageButton ID="SearchInterviewTest_viewImageButton" runat="server" SkinID="sknViewImageButton"
                                                                        ToolTip="View Interview" CommandName="view" CommandArgument='<%# Eval("TestKey") %>' />
                                                                    <asp:ImageButton ID="SearchInterviewTest_editImageButton" runat="server" SkinID="sknEditImageButton"
                                                                        ToolTip="Edit Interview" CommandName="edittest" CommandArgument='<%# Eval("TestKey") %>' />
                                                                    <asp:ImageButton ID="SearchInterviewTest_createNewImageButton" runat="server" SkinID="sknCreateNewSessionImageButton"
                                                                        ToolTip="Create/View Interview Sessions" CommandName="create" CommandArgument='<%# Eval("TestKey") %>' />
                                                                    <asp:ImageButton ID="SearchInterviewTest_activeImageButton" runat="server" SkinID="sknInactiveImageButton"
                                                                        Visible="false" ToolTip="Deactivate Interview" CommandName="inactive" CommandArgument='<%# Eval("TestKey") %>' /><asp:ImageButton
                                                                            ID="SearchInterviewTest_inactiveImageButton" runat="server" SkinID="sknActiveImageButton"
                                                                            ToolTip="Activate Interview" Visible="false" CommandName="active" CommandArgument='<%# Eval("TestKey") %>' />
                                                                    <asp:ImageButton ID="SearchInterviewTest_deleteImageButton" runat="server" SkinID="sknDeleteImageButton"
                                                                        ToolTip="Delete Interview" CommandName="deletes" Visible='<%# Eval("SessionIncluded") %>'
                                                                        CommandArgument='<%# Eval("TestKey") %>' />
                                                                    <asp:ImageButton ID="SearchInterviewTest_copyImageButton" runat="server" SkinID="sknCopyImageButton"
                                                                        ToolTip="Copy Interview" CommandName="copytest" CommandArgument='<%# Eval("TestKey") %>'
                                                                        Visible="false" />
                                                                    <asp:ImageButton ID="SearchInterviewTest_associateToPPImageButton" runat="server"
                                                                        SkinID="sknPPassociateCandidateImageButton" ToolTip="Associate to position profile"
                                                                        CommandName="associatePP" CommandArgument='<%# Eval("TestKey") %>' Visible="false" />
                                                                    <asp:HiddenField ID="SearchInterviewTest_statusHiddenField" runat="server" Value='<%# Eval("IsActive") %>' />
                                                                    <asp:HiddenField ID="SearchInterviewTest_activeInactivestatusHiddenField" runat="server"
                                                                        Value='<%# Eval("SessionIncludedActive") %>' />
                                                                    <asp:HiddenField ID="SearchInterviewTest_testAutherIdHiddenField" runat="server"
                                                                        Value='<%# Eval("TestAuthorID") %>' />
                                                                    <asp:HiddenField ID="SearchInterviewTest_dataAccessRightsHiddenField" runat="server"
                                                                        Value='<%# Eval("DataAccessRights") %>' />
                                                                    <asp:HiddenField ID="SearchInterviewTest_positionProfileIDHiddenField" runat="server"
                                                                        Value='<%# Eval("PositionProfileID") %>' />
                                                                    <asp:HiddenField ID="SearchInterviewTest_tenantIDHiddenField" runat="server" Value='<%# Eval("TenantID") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle Wrap="false" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Interview ID" DataField="TestKey" SortExpression="TESTKEY">
                                                                <ItemStyle Width="95px" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Interview Name" SortExpression="TESTNAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TestReport_testNameLabel" runat="server" Text='<%# TrimContent(Eval("Name").ToString(),20) %>'
                                                                        ToolTip='<%# Eval("Name") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="150px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Author" SortExpression="USRNAME" ItemStyle-Width="135px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TestReport_testAuthorNameLabel" runat="server" Text='<%# Eval("TestAuthorName") %>'
                                                                        ToolTip='<%# Eval("TestAuthorFullName") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="No of Questions" DataField="NoOfQuestions" SortExpression="TOTALQUESTION"
                                                                ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="110px" ItemStyle-HorizontalAlign="right"
                                                                HeaderStyle-CssClass="td_padding_right_20"></asp:BoundField>
                                                            <asp:TemplateField HeaderText="Created Date" SortExpression="CREATEDDATE DESC" ItemStyle-Width="120px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TestReport_testCreatedDateLabel" runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("CreatedDate"))) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Cost (in&nbsp;$)" DataField="TestCost" SortExpression="TESTCOST"
                                                                ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="75px" ItemStyle-HorizontalAlign="right"
                                                                HeaderStyle-CssClass="td_padding_right_20"></asp:BoundField>
                                                            <asp:TemplateField HeaderText="Certification Interview" SortExpression="CERTIFICATION DESC">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TestReport_testCertificationLabel" runat="server" Text='<%# GetExpansion(Convert.ToBoolean(Eval("IsCertification")))%>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="110px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchInterviewTest_topSearchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel runat="server" ID="SearchInterviewTest_bottomPagingNavigatorUpdatePanel">
                                <ContentTemplate>
                                    <uc1:PageNavigator ID="SearchInterviewTest_bottomPagingNavigator" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="SearchInterviewTest_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="SearchInterviewTest_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchInterviewTest_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text">
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 32%">
                                        &nbsp;
                                    </td>
                                    <td style="width: 30%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="SearchInterviewTest_bottomReset" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="SearchInterviewTest_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="SearchInterviewTest_bottomCancel" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="SearchInterviewTest_modelPopupUpdatePanel">
                    <ContentTemplate>
                        <div id="SearchInterviewTest_hiddenDIV" style="display: none">
                            <asp:Button ID="SearchInterviewTest_hiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="SearchInterviewTest_inactivepopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc2:ConfirmMsgControl ID="SearchInterviewTest_inactivePopupExtenderControl" runat="server"
                                OnOkClick="SearchInterviewTest_OkClick" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="SearchInterviewTest_inactivepopupExtender" runat="server"
                            PopupControlID="SearchInterviewTest_inactivepopupPanel" TargetControlID="SearchInterviewTest_hiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                        <asp:HiddenField ID="SearchInterviewTest_authorIdHiddenField" runat="server" />
                        <asp:HiddenField ID="SearchInterviewTest_positionProfileHiddenField" runat="server" />
                        <asp:HiddenField ID="SearchInterviewTest_TestKey" runat="server" />
                        <asp:HiddenField ID="SearchInterviewTest_Action" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
