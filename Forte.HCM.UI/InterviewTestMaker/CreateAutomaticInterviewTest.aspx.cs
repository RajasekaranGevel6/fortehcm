﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreateAutomaticTest.cs
// File that represents the user interface for Generating a test with
// system generated questions according to the user input search 
// criteria

#endregion Header

#region Directives                                                             

using System;
using System.Text;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;
using Forte.HCM.UI.CommonControls;
using System.IO;
using System.Web;

#endregion

namespace Forte.HCM.UI.InterviewTestMaker
{
    public partial class CreateAutomaticInterviewTest : PageBase
    {
        #region Declarations                                                   

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        /// <summary>
        /// A<see cref="string"/> constans that holds the key for the viewstate
        /// which refer to the questions for the test in test draft gridview.
        /// </summary>
        private const string CreateAutomaticInterviewTest_QUESTIONVIEWSTATE = "CreateAutomaticInterviewTest_QUESTIONSGRID";

        #endregion Declarations

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loading.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (CreateAutomaticInterviewTest_mainTabContainer.ActiveTabIndex == 0)
                    Page.Form.DefaultButton = CreateAutomaticInterviewTest_byQuestion_generateTestButton.UniqueID;
                else if (CreateAutomaticInterviewTest_mainTabContainer.ActiveTabIndex == 1)
                    Page.Form.DefaultButton = CreateAutomaticInterviewTest_bottomSaveButton.UniqueID;
                //Page title
                Master.SetPageCaption(Resources.HCMResource.CreateAutomaticInterviewTest_Title);
                ClearAllLabelMessages();
                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    Session["QuestionOrder"] = null;

                    LoadValues();
                    if (Session["NEWSEARCH_QUESTIONLIST"] != null && Session["QuestionOrder"] !=null)
                        SaveNewQuestion();
                }
                CreateAutomaticInterviewTest_searchQuestionLinkButton.Attributes.Add("onclick",
                    "return SearchInterviewQuestions();");

                Session["interviewDetail"] = null;


            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler event that will reset the page when user cliks on the reset link button
        /// </summary>
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticInterviewTest_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the generate button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticInterviewTest_byQuestion_generateTestButton_Click(object sender, EventArgs e)
        {
            try
            {

                CreateAutomaticInterviewTest_byQuestion_testDrftDiv.Visible = false;
                CreateAutomaticInterviewTest_byQuestion_automatedTestUserControl.Visible = false;
                ViewState[CreateAutomaticInterviewTest_QUESTIONVIEWSTATE] = null;
                CreateAutomaticInterviewTest_byQuestion_positionProfileTextBox.Text =
                    Request[CreateAutomaticInterviewTest_byQuestion_positionProfileTextBox.UniqueID].Trim();
                Session["QuestionOrder"] = null;
                LoadAutomatedQuestions();

                if (CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows.Count > 0)
                    CreateAutomaticInterviewTest_isQuestionGenerated.Value = "Y";
                else
                    CreateAutomaticInterviewTest_isQuestionGenerated.Value = "N";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the test draft grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void CreateAutomaticInterviewTest_byQuestion_testDraftGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the test segment grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticInterviewTest_byQuestion_testSegmantGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                ImageButton CreateAutomaticInterviewTest_categoryImageButton = ((ImageButton)e.Row.FindControl
                        ("CreateAutomaticInterviewTest_byQuestion_categoryImageButton"));
                TextBox CreateAutomaticInterviewTest_categoryTextBox = ((TextBox)e.Row.FindControl
                        ("CreateAutomaticInterviewTest_byQuestion_categoryTextBox"));
                TextBox CreateAutomaticInterviewTest_subjectTextBox = ((TextBox)e.Row.FindControl
                        ("CreateAutomaticInterviewTest_byQuestion_subjectTextBox"));
                HiddenField CreateAutomaticInterviewTest_categoryHiddenField = (HiddenField)e.Row.FindControl
                        ("CreateAutomaticInterviewTest_byQuestion_categoryHiddenField");
                HiddenField CreateAutomaticInterviewTest_categoryNameHiddenField = (HiddenField)e.Row.FindControl
                        ("CreateAutomaticInterviewTest_byQuestion_categoryNameHiddenField");
                HiddenField CreateAutomaticInterviewTest_subjectNameHiddenField = (HiddenField)e.Row.FindControl
                        ("CreateAutomaticInterviewTest_byQuestion_subjectNameHiddenField");
                Label CreateAutomaticInterviewTest_byQuestion_expecetdQuestions =
                        (Label)e.Row.FindControl("CreateAutomaticInterviewTest_byQuestion_expecetdQuestions");
                //
                string[] SelectedTestAreaID = ((HiddenField)e.Row.
                    FindControl("CreateAutomaticInterviewTest_byQuestion_testAreaHiddenField")).Value.Split(',');
                string[] SelectedComplexityID = ((HiddenField)e.Row.
                    FindControl("CreateAutomaticText_byQuestion_complexityHiddenField")).Value.Split(',');
                //
                //Select Subject and Category
                CreateAutomaticInterviewTest_categoryImageButton.Attributes.Add("onclick",
                        "return LoadCategorySubjectLookUpforReadOnly('" + CreateAutomaticInterviewTest_categoryTextBox.ClientID +
                            "','" + CreateAutomaticInterviewTest_subjectTextBox.ClientID + "','" +
                            CreateAutomaticInterviewTest_categoryHiddenField.ClientID + "','" +
                            CreateAutomaticInterviewTest_categoryNameHiddenField.ClientID + "','" +
                            CreateAutomaticInterviewTest_subjectNameHiddenField.ClientID + "');");

                CheckBoxList CreateAutomaticInterviewTest_testAreaCheckBoxList = ((CheckBoxList)e.Row.FindControl
                        ("CreateAutomaticInterviewTest_byQuestion_testAreaCheckBoxList"));
                CheckBoxList CreateAutomaticInterviewTest_complexityCheckBoxList = ((CheckBoxList)e.Row.FindControl
                        ("CreateAutomaticInterviewTest_byQuestion_complexityCheckBoxList"));
                // Binds test aread check box list
                CreateAutomaticInterviewTest_testAreaCheckBoxList.DataSource = GetTestAreaList();
                CreateAutomaticInterviewTest_testAreaCheckBoxList.DataTextField = "AttributeName";
                CreateAutomaticInterviewTest_testAreaCheckBoxList.DataValueField = "AttributeID";
                CreateAutomaticInterviewTest_testAreaCheckBoxList.DataBind();
                for (int i = 0; i < SelectedTestAreaID.Length; i++)
                    if (SelectedTestAreaID[i] != "")
                        CreateAutomaticInterviewTest_testAreaCheckBoxList.Items.FindByValue(SelectedTestAreaID[i]).Selected = true;
                // Bind Complexity Check box list
                CreateAutomaticInterviewTest_complexityCheckBoxList.DataSource = GetComplexityList();
                CreateAutomaticInterviewTest_complexityCheckBoxList.DataTextField = "AttributeName";
                CreateAutomaticInterviewTest_complexityCheckBoxList.DataValueField = "AttributeID";
                CreateAutomaticInterviewTest_complexityCheckBoxList.DataBind();
                for (int i = 0; i < SelectedComplexityID.Length; i++)
                    if (SelectedComplexityID[i] != "")
                        CreateAutomaticInterviewTest_complexityCheckBoxList.Items.FindByValue(SelectedComplexityID[i]).Selected = true;
                if (CreateAutomaticInterviewTest_byQuestion_expecetdQuestions.Text != "")
                    ((HtmlGenericControl)e.Row.FindControl("CreateAutomaticInterviewTest_byQuestion_expectedQuestionsDiv")).Visible = true;
                EnableOrDisableTestSegmentGridControls(e.Row);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticInterviewTest_byQuestion_testDraftGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ViewInterviewQuestionDetails")
                {
                    CreateAutomaticInterviewTest_byQuestion_QuestionDetailPreviewControl.LoadInterviewQuestionDetails
                        (e.CommandArgument.ToString(), 0);
                    CreateAutomaticInterviewTest_byQuestion_QuestionDetailPreviewControl.Title = "Interview Question Detail";
                    CreateAutomaticInterviewTest_byQuestion_QuestionDetailPreviewControl.SetFocus();
                    CreateAutomaticInterviewTest_byQuestion_bottomQuestionModalPopupExtender.Show();
                }

                List<QuestionDetail> Questions = Session["QuestionOrder"] as List<QuestionDetail>;

                if ((e.CommandName == "MoveDown") || (e.CommandName == "MoveUp"))
                {
                    int order = int.Parse(e.CommandArgument.ToString());

                    //if the command name is move down , move the record one line down 
                    if (e.CommandName == "MoveDown")
                    {
                        foreach (QuestionDetail question in Questions)
                        {
                            //Change the display order of the segment
                            if (question.DisplayOrder == order)
                            {
                                question.DisplayOrder = order + 1;
                                continue;
                            }
                            if (question.DisplayOrder == order + 1)
                            {
                                question.DisplayOrder = order;
                                continue;
                            }
                        }
                    }

                    //if the command name is moveup, move the record one line up
                    if (e.CommandName == "MoveUp")
                    {
                        foreach (QuestionDetail question in Questions)
                        {
                            if (question.DisplayOrder == order - 1)
                            {
                                question.DisplayOrder = order;
                                continue;
                            }
                            if (question.DisplayOrder == order)
                            {
                                question.DisplayOrder = order - 1;
                                continue;
                            }
                        }
                    }
                    CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataSource = (Session["QuestionOrder"] as List<QuestionDetail>).OrderBy(p => p.DisplayOrder);
                    CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataBind();

                    // Hide the last down arrow button in the grid
                    HideLastRecord();
                }

                if (e.CommandName == "DeleteQuestion")
                {
                    CreateAutomaticInterviewTest_deleteHiddenField.Value = e.CommandArgument.ToString();
                    CreateAutomaticInterviewTest_deleteInterviewQuestionsConfirmMsgControl.Message =
                        "Are you sure to delete the question - " + CreateAutomaticInterviewTest_deleteHiddenField.Value + "?";
                    CreateAutomaticInterviewTest_deleteInterviewQuestionsConfirmMsgControl.Type = MessageBoxType.YesNo;
                    CreateAutomaticInterviewTest_deleteInterviewQuestionsConfirmMsgControl.Title = "Warning";
                    CreateAutomaticInterviewTest_deleteInterviewQuestionsFormModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the OK button is clicked
        /// in the deleting the role category.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateAutomaticInterviewTest_deleteInterviewQuestionsConfirmMsgControl_okClick
          (object sender, EventArgs e)
        {
            try
            {
                string questionKey = CreateAutomaticInterviewTest_deleteHiddenField.Value;
                List<QuestionDetail> questionDetails = Session["QuestionOrder"] as List<QuestionDetail>;
                for (int i = 0; i < questionDetails.Count; i++)
                {
                    if (questionDetails[i].QuestionKey == questionKey)
                    {
                        questionDetails.RemoveAt(i);
                        Session["QuestionOrder"] = questionDetails;
                        if (questionDetails != null && questionDetails.Count > 0)
                        {
                            CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataSource = questionDetails;
                            CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataBind();
                            CreateAutomaticInterviewTest_byQuestion_testDrftDiv.Visible = true;

                            TestStatistics testStatistics = AutomatedSummaryList(questionDetails);
                            LoadTestStatisticsObject(ref questionDetails, ref testStatistics);
                            CreateAutomaticInterviewTest_byQuestion_automatedTestUserControl.DataSource = testStatistics;
                            CreateAutomaticInterviewTest_byQuestion_automatedTestUserControl.Visible = true;
                        }
                        else
                        {
                            CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataSource=null;
                            CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataBind();
                            CreateAutomaticInterviewTest_byQuestion_testDrftDiv.Visible = false;
                            CreateAutomaticInterviewTest_byQuestion_automatedTestUserControl.Visible = false;
                        }

                        #region Commented by VSJ - To fix question removed by the system
                        ////Check for interview segments
                        //if (CreateAutomaticInterviewTest_isQuestionGenerated.Value== "Y")
                        //{
                        //    LoadAutomatedQuestions();
                        //}
                        //else
                        //{
                        //    CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataSource = questionDetails;
                        //    CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataBind();
                        //}
                        #endregion Commented by VSJ - To fix question removed by the system
                        base.ShowMessage(CreateAutomaticInterviewTest_topSuccessMessageLabel,
                            CreateAutomaticInterviewTest_bottomSuccessMessageLabel, "Question deleted successfully");
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the Cancel button is clicked
        /// in the deleting the role.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateAutomaticInterviewTest_deleteInterviewQuestionsConfirmMsgControl_cancelClick
          (object sender, EventArgs e)
        {
            try
            {
                //Remove the business ype id from the hidden field
                CreateAutomaticInterviewTest_deleteHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the Cancel button is clicked
        /// in the deleting the role.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateAutomaticInterviewTest_saveInterviewQuestionsConfirmMsgControl_cancelClick
          (object sender, EventArgs e)
        {
            try
            {
                //Remove the business ype id from the hidden field
               // CreateAutomaticInterviewTest_saveHiddenField.Value = string.Empty;

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Clicked event of the CreateAutomaticText_addQuestioSaveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CreateAutomaticText_addQuestioSaveButton_Clicked
                (object sender, EventArgs e)
        {
            try
            {
                Session["NEWSEARCH_QUESTIONLIST"] = null;
                if (CreateAutomaticInterviewTest_addQuestionQuestTextBox.Text.Length == 0)
                {
                    CreateAutomaticText_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateAutomaticText_addQuestionErrorLabel, "Enter the question");

                    CreateAutomaticText_addQuestioModalPopupExtender.Show();

                    return;
                }
                if (CreateAutomaticInterviewTest_addQuestionAnswerTextBox.Text.Length == 0)
                {
                    CreateAutomaticText_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateAutomaticText_addQuestionErrorLabel, "Enter the answer");

                    CreateAutomaticText_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (CreateAutomaticInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Text == "--Select--")
                {
                    CreateAutomaticText_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateAutomaticText_addQuestionErrorLabel, "Select the complexity");

                    CreateAutomaticText_addQuestioModalPopupExtender.Show();
                    return;
                }

                if (CreateAutomaticInterviewAddQuestion_byQuestion_categoryTextBox.Text.Length == 0)
                {
                    CreateAutomaticText_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateAutomaticText_addQuestionErrorLabel, "Select the category and subject");

                    CreateAutomaticText_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (CreateAutomaticInterviewAddQuestion_byQuestion_subjectTextBox.Text.Length == 0)
                {
                    CreateAutomaticText_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateAutomaticText_addQuestionErrorLabel, "Select the category and subject");

                    CreateAutomaticText_addQuestioModalPopupExtender.Show();
                    return;
                }

                SaveNewQuestion();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the CreateAutomaticInterviewTest_addQuestionLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CreateAutomaticInterviewTest_addQuestionLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                CreateAutomaticText_addQuestionErrorLabel.Text = string.Empty;
                CreateAutomaticInterviewTest_addQuestionQuestTextBox.Text = string.Empty;
                CreateAutomaticInterviewTest_questionImage.ImageUrl = string.Empty;
                CreateAutomaticInterviewTest_addQuestionAnswerTextBox.Text = string.Empty;
                CreateAutomaticInterviewTest_isQuestionRepository.Checked = false;
                CreateAutomaticInterviewAddQuestion_byQuestion_categoryTextBox.Text = string.Empty;
                CreateAutomaticInterviewAddQuestion_byQuestion_subjectTextBox.Text = string.Empty;
                CreateAutomaticInterviewTest_tagsTextBox.Text = string.Empty;
                
                // Binding test area to radiobutton list.
                CreateAutomaticInterviewTest_addQuestionRadioButtonList.DataSource =
               new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
                CreateAutomaticInterviewTest_addQuestionRadioButtonList.DataTextField = "AttributeName";
                CreateAutomaticInterviewTest_addQuestionRadioButtonList.DataValueField = "AttributeID";
                CreateAutomaticInterviewTest_addQuestionRadioButtonList.DataBind();
                CreateAutomaticInterviewTest_addQuestionRadioButtonList.Items[0].Selected = true;

                //Bind complexities/attributes based on the attribute type
                CreateAutomaticInterviewTest_addQuestioncomplexityDropDownList.DataSource =
                    new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
                CreateAutomaticInterviewTest_addQuestioncomplexityDropDownList.DataTextField = "AttributeName";
                CreateAutomaticInterviewTest_addQuestioncomplexityDropDownList.DataValueField = "AttributeID";
                CreateAutomaticInterviewTest_addQuestioncomplexityDropDownList.DataBind();

                CreateAutomaticInterviewTest_addQuestioncomplexityDropDownList.Items.Insert(0, "--Select--");
                CreateAutomaticInterviewTest_addQuestioncomplexityDropDownList.SelectedIndex = 0;

                CreateAutomaticInterviewAddQuestion_byQuestion_categoryImageButton.Attributes.Add("onclick",
                "return LoadCategorySubjectLookUpforReadOnly('" + CreateAutomaticInterviewAddQuestion_byQuestion_categoryTextBox.ClientID +
                "','" + CreateAutomaticInterviewAddQuestion_byQuestion_subjectTextBox.ClientID + "','" +
                CreateAutomaticInterviewAddQuestion_byQuestion_categoryHiddenField.ClientID + "','" +
                CreateAutomaticInterviewAddQuestion_byQuestion_categoryNameHiddenField.ClientID + "','" +
                CreateAutomaticInterviewAddQuestion_byQuestion_subjectNameHiddenField.ClientID + "');");

                CreateAutomaticText_addQuestioModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exception.Message);

            }
        }

        /// <summary>
        /// Handles the Click event of the CreateAutomaticInterviewTest_searchQuestionLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CreateAutomaticInterviewTest_searchQuestionLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["NEWSEARCH_QUESTIONLIST"] == null)
                    return;
                SaveNewQuestion();

                List<QuestionDetail> questions = Session["QuestionOrder"] as List<QuestionDetail>;

                if (questions != null)
                {
                    TestStatistics testStatistics = AutomatedSummaryList(questions);
                    LoadTestStatisticsObject(ref questions, ref testStatistics);
                    CreateAutomaticInterviewTest_byQuestion_automatedTestUserControl.DataSource = testStatistics;
                    CreateAutomaticInterviewTest_byQuestion_automatedTestUserControl.Visible = true;
                }

                CreateAutomaticInterviewTest_byQuestion_testDrftDiv.Visible = true;
                CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataSource = Session["QuestionOrder"] as List<QuestionDetail>;
                CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataBind();

                HideLastRecord();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exception.Message);

            }
        }
        protected void CreateAutomaticText_addQuestioCloseLinkButton_Clicked
           (object sender, EventArgs e)
        {


        }
        /// <summary>
        /// Handles the cancelClick event of the CreateAutomaticInterviewTest control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CreateAutomaticInterviewTest_cancelClick
           (object sender, EventArgs e)
        {
            try
            {
                CreateAutomaticInterviewTest_byQuestion_bottomQuestionModalPopupExtender.Hide();
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exception.Message);
            }
        }

       /// <summary>
        /// Handler that helps to save the posted file in session
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        protected void CreateAutomaticInterviewTest_questionImageOnUploadComplete(object sender,
            AsyncFileUploadEventArgs e)
        {
            System.Threading.Thread.Sleep(500);
            if (CreateAutomaticInterviewTest_questionImageUpload.PostedFile!=null)
            {
                if (CreateAutomaticInterviewTest_questionImageUpload.PostedFile.ContentLength > 102400) // check if image size exceeds 100kb
                {
                    base.ShowMessage(CreateAutomaticText_addQuestionErrorLabel,
                        Resources.HCMResource.BatchQuestionEntry_imageSizeExceeded);
                    CreateAutomaticText_addQuestioModalPopupExtender.Show();
                    return;
                }

                // Check if the photo is of the desired extension.
                string extension = Path.GetExtension(CreateAutomaticInterviewTest_questionImageUpload.FileName);
                extension = extension.ToLower();

                if (extension != ".gif" && extension != ".png" && extension != ".jpg" && extension != ".jpeg")
                {
                    base.ShowMessage(CreateAutomaticText_addQuestionErrorLabel, "Only gif/png/jpg/jpeg files are allowed");
                    CreateAutomaticText_addQuestioModalPopupExtender.Show();
                    return;
                }

                // Read the bytes from the posted file.
                HttpPostedFile postedQstImage = CreateAutomaticInterviewTest_questionImageUpload.PostedFile;
                int nFileLen = postedQstImage.ContentLength;
                byte[] imgData = new byte[nFileLen];

                postedQstImage.InputStream.Read(imgData, 0, nFileLen);
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = imgData;

                CreateAutomaticText_addQuestioModalPopupExtender.Show();
                return;
            }
            else
            {
                base.ShowMessage(CreateAutomaticText_addQuestionErrorLabel,"Please select valid image to upload");
                CreateAutomaticText_addQuestioModalPopupExtender.Show();
                return;
            }
        }

        /// <summary>
        /// Handler that helps to show the preview of the saved image
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>   
        protected void CreateAutomaticInterviewTest_questionImageButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["POSTED_INTERVIEW_QUESTION_IMAGE"]!=null)
                {
                    CreateAutomaticInterviewTest_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=INTERVIEW_QUESTION_IMAGE";
                    CreateAutomaticInterviewTest_AddQuestionImageTR.Style.Add("display", "none");
                    CreateAutomaticInterviewTest_DisplayQuestionImageTR.Style.Add("display", "");
                    CreateAutomaticInterviewTest_addImageLinkButton.Style.Add("display", "none");
                    CreateAutomaticText_addQuestioModalPopupExtender.Show();
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler that helps to delete the posted file.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateAutomaticInterviewTest_deleteLinkButtonClick(object sender, EventArgs e)
        {
            try
            {
                ResetQuestionImage();
                CreateAutomaticText_addQuestioModalPopupExtender.Show();
                return;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Saves the new question.
        /// </summary>
        public void SaveNewQuestion()
        {

            List<QuestionDetail> questions = null;
            List<QuestionDetail> quest = null;
            QuestionDetail questionDetail = null;

            QuestionBLManager questionBLManager = new QuestionBLManager();

            if (Session["NEWSEARCH_QUESTIONLIST"] != null)
            {
                if (Session["NEWSEARCH_QUESTIONLIST"] == null)
                    quest = new List<QuestionDetail>();
                else
                    quest = Session["NEWSEARCH_QUESTIONLIST"] as List<QuestionDetail>;

                if (Session["QuestionOrder"] == null)
                    questions = new List<QuestionDetail>();
                else
                    questions = Session["QuestionOrder"] as List<QuestionDetail>;

                for (int i = 0; i < quest.Count; i++)
                {
                    if (!questions.Exists(p => p.QuestionKey.Trim() == quest[i].QuestionKey.Trim()))
                    {
                        questionDetail = new QuestionDetail();
                        questionDetail.QuestionKey = quest[i].QuestionKey;
                        questionDetail.Question = quest[i].Question;
                        questionDetail.ComplexityName = quest[i].Complexity;
                        questionDetail.Complexity = quest[i].ComplexityID;
                        // Construct the answer choice.
                        List<AnswerChoice> answerChoices = new List<AnswerChoice>();
                        AnswerChoice answerChoice = null;
                        answerChoice = new AnswerChoice();
                        answerChoice.ChoiceID = 1;
                        answerChoice.Choice = "";
                        answerChoices.Add(answerChoice);
                        questionDetail.AnswerChoices = answerChoices;
                        questionDetail.Author = quest[i].Author;
                        questionDetail.TestAreaID = quest[i].TestAreaID;
                        questionDetail.TestAreaName = quest[i].TestAreaName;
                        questionDetail.Tag = "";
                        questionDetail.CreatedBy = quest[i].CreatedBy;
                        questionDetail.CreatedDate = DateTime.Now;
                        questionDetail.ModifiedBy = quest[i].ModifiedBy;
                        questionDetail.QuestionRelationId = questionBLManager.GetQuestionRelationID(questionDetail.QuestionKey);
                        questionDetail.CategoryName = questionBLManager.GetCategory(questionDetail.QuestionKey);
                        questionDetail.SubjectName = questionBLManager.GetSubject(questionDetail.QuestionKey);
                        if (questions != null)
                            questionDetail.DisplayOrder = questions.Count + 1;
                        else
                            questionDetail.DisplayOrder = 1;
                        questions.Add(questionDetail);
                        Session["QuestionOrder"] = questions.ToList();
                    }
                }
            }
            else
            {
                questionDetail = new QuestionDetail();

                if (Session["QuestionOrder"] == null)
                    questions = new List<QuestionDetail>();
                else
                    questions = Session["QuestionOrder"] as List<QuestionDetail>;

                questionDetail = ConstructQuestionDetails();
                new QuestionBLManager().SaveInterviewQuestion(questionDetail);
                questionDetail.QuestionRelationId = questionBLManager.GetQuestionRelationID(questionDetail.QuestionKey);
                questionDetail.CategoryName = questionBLManager.GetCategory(questionDetail.QuestionKey);
                questionDetail.SubjectName = questionBLManager.GetSubject(questionDetail.QuestionKey);
                if (questions!= null)
                    questionDetail.DisplayOrder = questions.Count + 1;
                else
                    questionDetail.DisplayOrder = 1;
                questions.Add(questionDetail);
                Session["QuestionOrder"] = questions.ToList();
                TestStatistics testStatistics = AutomatedSummaryList(questions);
                LoadTestStatisticsObject(ref questions, ref testStatistics);
                CreateAutomaticInterviewTest_byQuestion_automatedTestUserControl.DataSource = testStatistics;
                CreateAutomaticInterviewTest_byQuestion_automatedTestUserControl.Visible = true;
                CreateAutomaticInterviewTest_byQuestion_testDrftDiv.Visible = true;
                CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataSource = Session["QuestionOrder"] as List<QuestionDetail>;
                CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataBind();
                HideLastRecord();
                ResetQuestionImage();
            }
        }

        /// <summary>
        /// Constructs the question details.
        /// </summary>
        /// <returns></returns>
        private QuestionDetail ConstructQuestionDetails()
        {
            // Construct question detail object.
            QuestionDetail questionDetail = new QuestionDetail();

            QuestionBLManager questionBLManager = new QuestionBLManager();

            questionDetail.Question = CreateAutomaticInterviewTest_addQuestionQuestTextBox.Text.Trim();

            if (CreateAutomaticInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Value != null &&
                CreateAutomaticInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Value.Trim().ToUpper() != "--SELECT--")
            {
                questionDetail.Complexity = CreateAutomaticInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Value;
                questionDetail.ComplexityName = CreateAutomaticInterviewTest_addQuestioncomplexityDropDownList.SelectedItem.Text;
            }

            //Set question type

            if (CreateAutomaticInterviewTest_isQuestionRepository.Checked)
                questionDetail.IsActiveFlag = "Y";
            else
                questionDetail.IsActiveFlag = "N";

            // Construct the answer choice.
            List<AnswerChoice> answerChoices = new List<AnswerChoice>();
            AnswerChoice answerChoice = null;


            answerChoice = new AnswerChoice();
            answerChoice.ChoiceID = 1;
            answerChoice.Choice = CreateAutomaticInterviewTest_addQuestionAnswerTextBox.Text.Trim();
            answerChoices.Add(answerChoice);

            questionDetail.AnswerChoices = answerChoices;
            questionDetail.Author = Convert.ToInt32(base.userID);

            // Additional settings (Test AreaId).
            questionDetail.TestAreaID = CreateAutomaticInterviewTest_addQuestionRadioButtonList.SelectedValue;
            questionDetail.TestAreaName = CreateAutomaticInterviewTest_addQuestionRadioButtonList.SelectedItem.Text;

            questionDetail.Tag = CreateAutomaticInterviewTest_tagsTextBox.Text;

            questionDetail.CreatedBy = Convert.ToInt32(base.userID);
            questionDetail.CreatedDate = DateTime.Now;

            questionDetail.ModifiedBy = Convert.ToInt32(base.userID);
            //   questionDetail.Subjects = SingleQuestionEntry_categorySubjectControl.SubjectDataSource;

            // Get selected subject ids only. This will be used in QuestionBLManager:InsertQuestionRelation
            // method to insert multiple subjects simultanesouly.
            questionDetail.SelectedSubjectIDs = CreateAutomaticInterviewAddQuestion_byQuestion_categoryHiddenField.Value.ToString();


            if (Session["POSTED_INTERVIEW_QUESTION_IMAGE"] != null)
            {
                questionDetail.QuestionImage = Session["POSTED_INTERVIEW_QUESTION_IMAGE"] as byte[];
                questionDetail.HasImage = true;
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            }
            else
                questionDetail.HasImage = false;

            return questionDetail;
        }

        /// <summary>
        /// Constructs the question information.
        /// </summary>
        /// <returns></returns>
        private List<QuestionDetail> ConstructQuestionInformation()
        {
            // Construct question detail object.     
            List<QuestionDetail> qd = new List<QuestionDetail>();

            for (int i = 0; i < CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows.Count; i++)
            {

                QuestionDetail questionDetail = new QuestionDetail();

                questionDetail.Question = ((HiddenField)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                            FindControl("SearchQuestion_byQuestion_questionGridHiddenfiels")).Value;
                questionDetail.Complexity = ((HiddenField)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                            FindControl("CreateAutomaticInterviewTest_byQuestion_complexityIDHiddenField")).Value;

                questionDetail.ComplexityName = ((Label)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                            FindControl("CreateAutomaticInterviewTest_byQuestion_complexityLabel")).Text;

                questionDetail.QuestionKey = ((LinkButton)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                           FindControl("QuestionDetailPreviewControl_byQuestion_questionLinkButton")).Text;

                //questionDetail.TimeLimit =120;
                //questionDetail.Rating = 10;
                //questionDetail.Comments = string.Empty;

                questionDetail.QuestionRelationId = Convert.ToInt32(
                     ((HiddenField)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                            FindControl("CreateAutomaticInterviewTest_byQuestion_questionrelationIDHiddenfield")).Value);

                //questionDetail.CategoryID = Convert.ToInt32(((HiddenField)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                //      FindControl("CreateAutomaticInterviewTest_byQuestion_categoryHiddenField")).Value);

                questionDetail.SubjectName = CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].Cells[3].Text;
                            //FindControl("CreateAutomaticInterviewTest_byQuestion_subjectNameHiddenField")).Value;

                questionDetail.CategoryName = CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].Cells[2].Text;
                            //FindControl("CreateAutomaticInterviewTest_byQuestion_categoryNameHiddenField")).Value;

                 List<AnswerChoice> questionDetail_Answer = new BL.QuestionBLManager().GetInterviewQuestionOptions(questionDetail.QuestionKey);
                
                questionDetail.Choice_Desc = "";

                 for (int AnswerCount = 0; AnswerCount < questionDetail_Answer.Count; AnswerCount++)
                     questionDetail.Choice_Desc += questionDetail_Answer[AnswerCount].Choice.ToString() + "<br>";

                // Construct the answer choice.
                List<AnswerChoice> answerChoices = new List<AnswerChoice>();
                AnswerChoice answerChoice = null;
                answerChoice = new AnswerChoice();
                answerChoice.ChoiceID = 1;
                answerChoice.Choice = "";
                answerChoices.Add(answerChoice);
                questionDetail.AnswerChoices = answerChoices;                
                questionDetail.Author = Convert.ToInt32(base.userID);

                // Additional settings (Test AreaId).
                questionDetail.TestAreaID = ((HiddenField)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                            FindControl("CreateAutomaticInterviewTest_byQuestion_testAreaIDHiddenField")).Value;
                questionDetail.TestAreaName = ((Label)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                            FindControl("CreateAutomaticInterviewTest_byQuestion_testAreaTextBox")).Text;
                questionDetail.Tag = "";
                questionDetail.CreatedBy = Convert.ToInt32(base.userID);
                questionDetail.CreatedDate = DateTime.Now;
                questionDetail.ModifiedBy = Convert.ToInt32(base.userID);
                // questionDetail.Subjects = SingleQuestionEntry_categorySubjectControl.SubjectDataSource;
                // Get selected subject ids only. This will be used in QuestionBLManager:InsertQuestionRelation
                // method to insert multiple subjects simultanesouly.
                questionDetail.SelectedSubjectIDs = CreateAutomaticInterviewAddQuestion_byQuestion_categoryHiddenField.Value.ToString();
                qd.Add(questionDetail);
            }

            return qd;
        }      

        

             /// <summary>
        /// Constructs the question information.
        /// </summary>
        /// <returns></returns>
        private List<QuestionDetail> ConstructQuestionInformationDefaultSettings()
        {
            // Construct question detail object.     
            List<QuestionDetail> qd = new List<QuestionDetail>();
            int WeightagePercentage = 0;
            int ToAddExtraWeightage = 0;

            WeightagePercentage = 100 / CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows.Count;
            ToAddExtraWeightage = 100 - (WeightagePercentage * CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows.Count);


            for (int i = 0; i < CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows.Count; i++)
            {

                QuestionDetail questionDetail = new QuestionDetail();

                questionDetail.Question = ((HiddenField)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                            FindControl("SearchQuestion_byQuestion_questionGridHiddenfiels")).Value;
                questionDetail.Complexity = ((HiddenField)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                            FindControl("CreateAutomaticInterviewTest_byQuestion_complexityIDHiddenField")).Value;

                questionDetail.ComplexityName = ((Label)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                            FindControl("CreateAutomaticInterviewTest_byQuestion_complexityLabel")).Text;

                questionDetail.QuestionKey = ((LinkButton)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                           FindControl("QuestionDetailPreviewControl_byQuestion_questionLinkButton")).Text;

                questionDetail.TimeLimit =300;              
                questionDetail.InterviewTestRatings = 10;
                questionDetail.InterviewTestQuestionComments = string.Empty;

                //Assign Weightage while save default settings
                questionDetail.Weightage = WeightagePercentage;
                if ((ToAddExtraWeightage > 0) && (WeightagePercentage != 0)
                    && (i == CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows.Count - 1))
                    questionDetail.Weightage = questionDetail.Weightage + ToAddExtraWeightage;
                //End Weightage assign

                questionDetail.QuestionRelationId = Convert.ToInt32(
                     ((HiddenField)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                            FindControl("CreateAutomaticInterviewTest_byQuestion_questionrelationIDHiddenfield")).Value);

                //questionDetail.CategoryID = Convert.ToInt32(((HiddenField)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                //      FindControl("CreateAutomaticInterviewTest_byQuestion_categoryHiddenField")).Value);

                questionDetail.SubjectName = CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].Cells[3].Text;
                            //FindControl("CreateAutomaticInterviewTest_byQuestion_subjectNameHiddenField")).Value;

                questionDetail.CategoryName = CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].Cells[2].Text;
                            //FindControl("CreateAutomaticInterviewTest_byQuestion_categoryNameHiddenField")).Value;

                 List<AnswerChoice> questionDetail_Answer = new BL.QuestionBLManager().GetInterviewQuestionOptions(questionDetail.QuestionKey);
                
                questionDetail.Choice_Desc = "";

                 for (int AnswerCount = 0; AnswerCount < questionDetail_Answer.Count; AnswerCount++)
                     questionDetail.Choice_Desc += questionDetail_Answer[AnswerCount].Choice.ToString() + "<br>";

                // Construct the answer choice.
                List<AnswerChoice> answerChoices = new List<AnswerChoice>();
                AnswerChoice answerChoice = null;
                answerChoice = new AnswerChoice();
                answerChoice.ChoiceID = 1;
                answerChoice.Choice = "";
                answerChoices.Add(answerChoice);
                questionDetail.AnswerChoices = answerChoices;                
                questionDetail.Author = Convert.ToInt32(base.userID);

                // Additional settings (Test AreaId).
                questionDetail.TestAreaID = ((HiddenField)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                            FindControl("CreateAutomaticInterviewTest_byQuestion_testAreaIDHiddenField")).Value;
                questionDetail.TestAreaName = ((Label)CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows[i].
                            FindControl("CreateAutomaticInterviewTest_byQuestion_testAreaTextBox")).Text;
                questionDetail.Tag = "";
                questionDetail.CreatedBy = Convert.ToInt32(base.userID);
                questionDetail.CreatedDate = DateTime.Now;
                questionDetail.ModifiedBy = Convert.ToInt32(base.userID);
                // questionDetail.Subjects = SingleQuestionEntry_categorySubjectControl.SubjectDataSource;
                // Get selected subject ids only. This will be used in QuestionBLManager:InsertQuestionRelation
                // method to insert multiple subjects simultanesouly.
                questionDetail.SelectedSubjectIDs = CreateAutomaticInterviewAddQuestion_byQuestion_categoryHiddenField.Value.ToString();
                qd.Add(questionDetail);
            }

            return qd;
        }      

        private void HideLastRecord()
        {
            foreach (GridViewRow row in CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows)
            {
                if (row.RowIndex == CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows.Count - 1)
                {
                    ImageButton dowmImageButton = (ImageButton)row.FindControl
                        ("CreateAutomaticTest_byQuestion_GridView_moveDownQuestionImageButton");

                    dowmImageButton.Visible = false;
                }
            }
        }

        /// <summary>
        /// Method to reset the question image
        /// </summary>
        private void ResetQuestionImage()
        {
            Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            CreateAutomaticInterviewTest_questionImage.ImageUrl = string.Empty;
            CreateAutomaticInterviewTest_AddQuestionImageTR.Style.Add("display", "none");
            CreateAutomaticInterviewTest_DisplayQuestionImageTR.Style.Add("display", "none");
            CreateAutomaticInterviewTest_addImageLinkButton.Style.Add("display", "");
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the test draft grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void CreateAutomaticInterviewTest_byQuestion_testDraftGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                HiddenField hiddenField = (HiddenField)e.Row.FindControl
                 ("QuestionDetailPreviewControl_selectedQuestionGridView_qestIDHiddenField");

                ImageButton dowmImageButton = (ImageButton)e.Row.FindControl
                    ("CreateAutomaticTest_byQuestion_GridView_moveDownQuestionImageButton");

                ImageButton upImageButton = (ImageButton)e.Row.FindControl
                    ("CreateAutomaticTest_byQuestion_GridView_moveUpQuestionImageButton");

                HiddenField displayOrderHiddenField = (HiddenField)e.Row.FindControl
                    ("QuestionDetailPreviewControl_selectedQuestionGridView_displayOrderHiddenField");

                if (e.Row.RowIndex == 0)
                {
                    upImageButton.Visible = false;
                }

                if (hiddenField.Value == "0")
                {
                    dowmImageButton.Visible = false;

                    upImageButton.Visible = false;

                }
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                ImageButton alertImageButton = (ImageButton)e.Row.FindControl("CreateAutomaticTes_byQuestion_testDrftRemoveImage");
                LinkButton CreateAutomaticInterviewTest_questionIdLinkButton =
                                    (LinkButton)e.Row.FindControl("QuestionDetailPreviewControl_byQuestion_questionLinkButton");
                alertImageButton.Attributes.Add("OnClick", "javascript:return LoadFlagQuestion('" +
                    CreateAutomaticInterviewTest_questionIdLinkButton.Text + "' );");
                HtmlContainerControl SearchQuestion_detailsDiv =
                    (HtmlContainerControl)e.Row.FindControl("CreateAutomaticInterviewTest_byQuestion_detailsDiv");

                PlaceHolder CreateAutomaticInterviewTest_answerChoicesPlaceHolder = (PlaceHolder)e.Row.FindControl(
                    "CreateAutomaticInterviewTest_byQuestion_answerChoicesPlaceHolder");
                CreateAutomaticInterviewTest_answerChoicesPlaceHolder.Controls.Add
                    (new ControlUtility().GetAnswerChoices(new BL.QuestionBLManager().GetInterviewQuestionOptions
                    (CreateAutomaticInterviewTest_questionIdLinkButton.Text.ToString()), false));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the delete segment image button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticInterviewTest_byQuestion_testSegmantGridView_deleteImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            try
            {
                DeleteTestSegmentRow(((GridViewRow)((ImageButton)sender).Parent.Parent).RowIndex);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the add segment button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticInterviewTest_byQuestion_addSegmantLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                AddTestSegmentRow(false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the add position profile
        /// button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref=
        protected void CreateAutomaticInterviewTest_byQuestion_addPositionProfileLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(CreateAutomaticInterviewTest_byQuestion_positionProfileTagsHiddenField.Value))
                {
                    base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, "Please select the position profile");
                    return;
                }
                DataTable dtTestSearchCriteria = BuildTableFromGridView();

                CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataSource = FilledSegmants(ref dtTestSearchCriteria);
                CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataBind();

                AddTestSegmentRow(true);
                CreateAutomaticInterviewTest_byQuestion_positionProfileTextBox.Text = "";
                CreateAutomaticInterviewTest_byQuestion_positionProfileTagsHiddenField.Value = "";
                CreateAutomaticInterviewTest_byQuestion_positionProfileIDHiddenField.Value = "";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticInterviewTest_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;

                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(CreateAutomaticInterviewTest_byQuestion_totalNumberTextBox.Text) && 
                    ViewState[CreateAutomaticInterviewTest_QUESTIONVIEWSTATE] != null)
                {
                    if (Convert.ToInt32(CreateAutomaticInterviewTest_byQuestion_totalNumberTextBox.Text) > 
                        ((List<QuestionDetail>)ViewState[CreateAutomaticInterviewTest_QUESTIONVIEWSTATE]).Count)
                        ShowConfirmPopUp();
                    else
                        ShowSaveConfirmPopUp();
                }
                else
                    ShowSaveConfirmPopUp();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel, CreateAutomaticInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and 
        /// OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void CreateAutomaticText_okPopUpClick(object sender, EventArgs e)
        {
            try
            {
                Button YesorNo = (Button)sender;
                if (YesorNo.Text.ToString() == "Ok")
                {
                    if (CreateAutomaticText_confirmPopupExtenderControl.ReviewAndSave)
                    {
                        SaveQuestions();
                        Response.Redirect("~/InterviewTestMaker/AddInterviewTestQuestionRatings.aspx?m=1&s=0&parentpage=INT_T_AOTO_QUST", false);
                    }
                    else if (CreateAutomaticText_confirmPopupExtenderControl.SaveWithDefaultValue)
                    {
                        SaveQuestionsWithDefaultSettings();
                    }
                }
                else
                {
                    ShowSaveConfirmPopUp();
                }

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        ///  Event Handling for the common ConfirmMessage Popup Extenter Cancel and 
        ///  Close Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel/Close button Event will be raised.
        /// </remarks>
        protected void CreateAutomaticText_cancelPopUpClick(object sender, EventArgs e)
        {
            DataTable dtTestSearchCriteria = null;
            try
            {
                dtTestSearchCriteria = BuildTableFromGridView();
                CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataSource = dtTestSearchCriteria;
                CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtTestSearchCriteria)) dtTestSearchCriteria = null;
            }
        }

        /// <summary>
        /// Handler method that will be called when the create session button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAutomaticInterviewTest_CreateSessionButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/InterviewTestMaker/CreateInterviewTestSession.aspx?m=1&s=2&parentpage=S_TSN&interviewtestkey=" +
                    CreaetAutomaticTest_byQuestion_testKeyHiddenField.Value, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, exp.Message);
            }
        }
        protected void CreateAutomaticText_addQuestioCloseLinkButton_Click(object sender, EventArgs e)
        {
            ResetQuestionImage();
        }
        protected void CreateAutomaticInterviewTest_addQuestionTopCancelImageButton_Click(object sender, ImageClickEventArgs e)
        {
            ResetQuestionImage();
        }
        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// This method Invokes Confirm pop up when user tries to save
        /// the less number of questions in the test draft panel with the
        /// number of questions provided.
        /// </summary>
        private void ShowConfirmPopUp()
        {
            CreateAutomaticText_confirmPopupExtenderControl.Message =  Resources.HCMResource.CreateAutomaticInterviewTest_SaveTestLessQuestionsWarningMessage;
            CreateAutomaticText_confirmPopupExtenderControl.Title = "Warning";
            CreateAutomaticText_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
            //CreateAutomaticText_confirmPopupExtenderControl.Type = MessageBoxType.OkSmall;
            CreateAutomaticText_confirmPopupExtenderControl.DataBind();
            CreateAutomaticText_saveTestModalPopupExtender.Show();
        }



        /// <summary>
        /// This method Invokes Confirm pop up when user tries to save
        /// the less number of questions in the test draft panel with the
        /// number of questions provided.
        /// </summary>
        /// 
        private void ShowSaveConfirmPopUp()
        {
            CreateAutomaticText_confirmPopupExtenderControl.Title = "Save Options";           
            CreateAutomaticText_confirmPopupExtenderControl.Type = MessageBoxType.OkCancel;
            CreateAutomaticText_confirmPopupExtenderControl.DataBind();
            CreateAutomaticText_saveTestModalPopupExtender.Show();
        }

        /// <summary>
        /// Binds the empty data source to the test detail user control.
        /// </summary>
        private void BindEmptyTestDetailDataSource1()
        {
            TestDetail testDetail = null;
            try
            {
                CreaetAutomaticTest_byQuestion_testKeyHiddenField.Value = "";
                testDetail = new TestDetail();
                testDetail.TestKey = "";
                testDetail.Name = "";
                testDetail.Description = "";
                testDetail.SystemRecommendedTime = 0;
                testDetail.RecommendedCompletionTime = 0;
                CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl.TestDetailDataSource = testDetail;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail)) testDetail = null;
            }
        }


        /// <summary>
        /// Method that saves the question to the DB and generates a test key
        /// which will be shown to the user in the control.
        /// </summary>
        private void SaveQuestions()
        {
            List<QuestionDetail> questionDetailTestDraftResultList = null;
            TestDetail testDetail = null;

            QuestionDetail qd = new QuestionDetail();
            try
            {
                questionDetailTestDraftResultList = new List<QuestionDetail>();

                // questionDetailTestDraftResultList = Session["QuestionOrder"] as List<QuestionDetail>;

                questionDetailTestDraftResultList = ConstructQuestionInformation();

                List<decimal> creditEarned = new List<decimal>();
                List<decimal> timeTaken = new List<decimal>();
                for (int i = 0; i < questionDetailTestDraftResultList.Count; i++)
                {
                    creditEarned.Add(questionDetailTestDraftResultList[i].CreditsEarned);
                    timeTaken.Add(questionDetailTestDraftResultList[i].AverageTimeTaken);
                }
                //int questionCount = Convert.ToInt32(questionDetailTestDraftResultList.Count);
                testDetail = new TestDetail();
                testDetail = CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl.TestDetailDataSource;
                testDetail.Questions = questionDetailTestDraftResultList;
                testDetail.IsActive = true;
                //testDetail.IsCertification = false;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail.CertificationDetail))
                    //  testDetail.CertificationDetail = new CertificationDetail();
                    testDetail.CertificationId = 0;
                else
                    testDetail.CertificationId = testDetail.CertificationDetail.CertificateID;
               
                testDetail.CreatedBy = userID;
                testDetail.IsDeleted = false;
                testDetail.ModifiedBy = userID;
                testDetail.NoOfQuestions = questionDetailTestDraftResultList.Count;
                testDetail.SystemRecommendedTime = Convert.ToInt32(timeTaken.Average()) *
                    questionDetailTestDraftResultList.Count;
                testDetail.TestAuthorID = userID;
                testDetail.TestCost = Convert.ToDecimal(creditEarned.Average());
                testDetail.TestCreationDate = DateTime.Now;
                testDetail.TestStatus = TestStatus.Active;
                testDetail.TestType = TestType.Genenal;
                testDetail.TestMode = "AUTO";
                testDetail.TestKey = "";


                InterviewDetail InterviewDetail = new InterviewDetail();
                InterviewDetail.InterviewTestKey = testDetail.TestKey;
                InterviewDetail.InterviewName = testDetail.Name;
                InterviewDetail.InterviewDescription = testDetail.Description;
                InterviewDetail.PositionProfileId = Convert.ToInt32(testDetail.PositionProfileID);
                InterviewDetail.PositionProfileName = testDetail.PositionProfileName;
                InterviewDetail.TestAuthor = testDetail.TestAuthorName;
                InterviewDetail.SessionIncludedActive = testDetail.SessionIncludedActive;
                InterviewDetail.TestAuthorID = testDetail.TestAuthorID; ;
                InterviewDetail.SessionIncluded = testDetail.SessionIncluded;

                Session["controlTestDetail"] = (TestDetail)testDetail;
                //if(Session["InterviewDetail"] ==null)              
                Session["interviewDetail"] = (InterviewDetail)InterviewDetail;
                
               // if (Session["questionDetailTestDraftResultList"] == null)
                Session["questionDetailTestDraftResultList"] = questionDetailTestDraftResultList as List<QuestionDetail>;
            }
            catch (Exception exp)
            {
                    Logger.ExceptionLog(exp);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
                    questionDetailTestDraftResultList = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail)) testDetail = null;
            }
        }


        /// <summary>
        /// Method that saves the question to the DB and generates a test key
        /// which will be shown to the user in the control.
        /// </summary>
        private void SaveQuestionsWithDefaultSettings()
        {
            List<QuestionDetail> questionDetailTestDraftResultList = null;
            TestDetail testDetail = null;

            QuestionDetail qd = new QuestionDetail();
            try
            {
                questionDetailTestDraftResultList = new List<QuestionDetail>();
                // questionDetailTestDraftResultList = Session["QuestionOrder"] as List<QuestionDetail>;
                questionDetailTestDraftResultList = ConstructQuestionInformationDefaultSettings();

                List<decimal> creditEarned = new List<decimal>();
                List<decimal> timeTaken = new List<decimal>();
                for (int i = 0; i < questionDetailTestDraftResultList.Count; i++)
                {
                    creditEarned.Add(questionDetailTestDraftResultList[i].CreditsEarned);
                    timeTaken.Add(questionDetailTestDraftResultList[i].AverageTimeTaken);
                }
                //int questionCount = Convert.ToInt32(questionDetailTestDraftResultList.Count);
                testDetail = new TestDetail();
                testDetail = CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl.TestDetailDataSource;
                testDetail.Questions = questionDetailTestDraftResultList;
                testDetail.IsActive = true;
                //testDetail.IsCertification = false;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail.CertificationDetail))
                    //  testDetail.CertificationDetail = new CertificationDetail();
                    testDetail.CertificationId = 0;
                else
                    testDetail.CertificationId = testDetail.CertificationDetail.CertificateID;

                testDetail.CreatedBy = userID;
                testDetail.IsDeleted = false;
                testDetail.ModifiedBy = userID;
                testDetail.NoOfQuestions = questionDetailTestDraftResultList.Count;
                testDetail.SystemRecommendedTime = Convert.ToInt32(timeTaken.Average()) *
                    questionDetailTestDraftResultList.Count;
                testDetail.TestAuthorID = userID;
                testDetail.TestCost = Convert.ToDecimal(creditEarned.Average());
                testDetail.TestCreationDate = DateTime.Now;
                testDetail.TestStatus = TestStatus.Active;
                testDetail.TestType = TestType.Genenal;
                testDetail.TestMode = "AUTO";
                testDetail.TestKey = "";


                InterviewDetail InterviewDetail = new InterviewDetail();
                InterviewDetail.InterviewTestKey = testDetail.TestKey;
                InterviewDetail.InterviewName = testDetail.Name;
                InterviewDetail.InterviewDescription = testDetail.Description;
                InterviewDetail.PositionProfileId = Convert.ToInt32(testDetail.PositionProfileID);
                InterviewDetail.PositionProfileName = testDetail.PositionProfileName;
                InterviewDetail.TestAuthor = testDetail.TestAuthorName;
                InterviewDetail.SessionIncludedActive = testDetail.SessionIncludedActive;
                InterviewDetail.TestAuthorID = testDetail.TestAuthorID; ;
                InterviewDetail.SessionIncluded = testDetail.SessionIncluded;

                // Save the test.
                testDetail.TestKey = new InterviewBLManager().SaveInterviewTestWithRatings(testDetail, userID,
                     new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY,
                     questionDetailTestDraftResultList).AttributeID.ToString(), questionDetailTestDraftResultList);

                 CreaetAutomaticTest_byQuestion_testKeyHiddenField.Value = testDetail.TestKey;
                 CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl.TestDetailDataSource = testDetail;

                 // Show a success message.
                 base.ShowMessage(CreateAutomaticInterviewTest_topSuccessMessageLabel,
                     CreateAutomaticInterviewTest_bottomSuccessMessageLabel,
                     string.Format(Resources.HCMResource.CreateAutomaticInterviewTest_TestSuccessfullMessage, testDetail.TestKey));
                 CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl.TestDetailDataSource = testDetail;

                // Enable create interview session
                 SetVisibleStatusForButton(true);

                 // Send a mail to the position profile creator.
                 try
                 {
                     if (testDetail.PositionProfileID != 0)
                     {
                         new EmailHandler().SendMail(EntityType.InterviewCreatedForPositionProfile, testDetail.TestKey);
                     }
                 }
                 catch (Exception exp)
                 {
                     Logger.ExceptionLog(exp);
                 }                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
                    questionDetailTestDraftResultList = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail)) testDetail = null;
            }
        }

        /// <summary>
        /// Clear all top and bottom message labels
        /// </summary>
        private void ClearAllLabelMessages()
        {
            CreateAutomaticInterviewTest_topErrorMessageLabel.Text = string.Empty;
            CreateAutomaticInterviewTest_topSuccessMessageLabel.Text = string.Empty;
            CreateAutomaticInterviewTest_bottomErrorMessageLabel.Text = string.Empty;
            CreateAutomaticInterviewTest_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Binds emtpty test segment table grid
        /// </summary>
        private void BindEmptyTestSegmentGrid()
        {
            DataTable dtTestSegment = new DataTable();
            AddTestSegmentColumns(ref dtTestSegment);
            DataRow drNewRow = dtTestSegment.NewRow();
            dtTestSegment.Rows.Add(drNewRow);
            CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataSource = dtTestSegment;
            CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataBind();
        }

        /// <summary>
        /// Creates a Table structure for serach criteria
        /// </summary>
        /// <param name="dtTestSegmentGridColumns">Data table object to add columns (use ref: key word)</param>
        private void AddTestSegmentColumns(ref DataTable dtTestSegmentGridColumns)
        {
            dtTestSegmentGridColumns = new DataTable();
            dtTestSegmentGridColumns.Columns.Add("SNO", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Cat_sub_id", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Complexity", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("TestArea", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Category", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Subject", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("Weightage", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Keyword", typeof(string));
            dtTestSegmentGridColumns.Columns.Add("ExpectedQuestions", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("PickedQuestions", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("TotalRecordsinDB", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("QuestionsDifference", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Remarks", typeof(string));
        }

        /// <summary>
        /// Builds data table from test segment grid view
        /// </summary>
        /// <returns>Data table that contains user given input for test segment grid</returns>
        private DataTable BuildTableFromGridView()
        {
            DataTable dtTestSegment = null;
            try
            {
                dtTestSegment = new DataTable();
                AddTestSegmentColumns(ref dtTestSegment);
                DataRow drNewRow = null;
                int Weightage = 0;
                int SNo = 1;
                foreach (GridViewRow gridViewRow in CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.Rows)
                {
                    drNewRow = dtTestSegment.NewRow();
                    drNewRow["SNO"] = SNo++.ToString();
                    drNewRow["Cat_sub_id"] = ((HiddenField)gridViewRow.
                        FindControl("CreateAutomaticInterviewTest_byQuestion_categoryHiddenField")).Value;
                   
                    drNewRow["Category"] = ((HiddenField)gridViewRow.
                        FindControl("CreateAutomaticInterviewTest_byQuestion_categoryNameHiddenField")).Value.Trim();
                   
                    drNewRow["Subject"] = ((HiddenField)gridViewRow.
                        FindControl("CreateAutomaticInterviewTest_byQuestion_subjectNameHiddenField")).Value.Trim();
                  
                    int.TryParse(((TextBox)gridViewRow.
                        FindControl("CreateAutomaticInterviewTest_byQuestion_weightageTextBox")).Text, out Weightage);
                    if (Weightage > 0)
                        drNewRow["Weightage"] = Weightage;
                 
                    drNewRow["Keyword"] = ((TextBox)gridViewRow.
                        FindControl("CreateAutomaticInterviewTest_byQuestion_keywordTextBox")).Text;

                    drNewRow["TestArea"] = GetSelectedValuesfromCheckBoxList(
                        (CheckBoxList)gridViewRow.FindControl("CreateAutomaticInterviewTest_byQuestion_testAreaCheckBoxList"));
                    drNewRow["Complexity"] = GetSelectedValuesfromCheckBoxList(
                        (CheckBoxList)gridViewRow.FindControl("CreateAutomaticInterviewTest_byQuestion_complexityCheckBoxList"));
                    
                    if (((HtmlContainerControl)gridViewRow.FindControl("CreateAutomaticInterviewTest_byQuestion_expectedQuestionsDiv")).Visible)
                    {
                        drNewRow["ExpectedQuestions"] = ((Label)gridViewRow.FindControl("CreateAutomaticInterviewTest_byQuestion_expecetdQuestions")).Text;
                        drNewRow["PickedQuestions"] = ((Label)gridViewRow.FindControl("CreateAutomaticInterviewTest_byQuestions_returnQuestionsLabel")).Text;
                        drNewRow["Remarks"] = ((Label)gridViewRow.FindControl("CreateAutomaticInterviewTest_byQuestin_remarkLabel")).Text;
                        drNewRow["TotalRecordsinDB"] = ((Label)gridViewRow.FindControl("CreateAutomaticInterviewTest_byQuestion_noOfQuestionsLabel")).Text;
                    }

                    /*Added by MKN, this condition applied when not selecting any search criteria */
                    if (string.IsNullOrEmpty(Convert.ToString(drNewRow["Complexity"]))
                        && string.IsNullOrEmpty(Convert.ToString(drNewRow["TestArea"]))
                        && string.IsNullOrEmpty(Convert.ToString(drNewRow["Category"])))
                    {
                        drNewRow["ExpectedQuestions"] = DBNull.Value;
                        drNewRow["PickedQuestions"] = DBNull.Value;
                        drNewRow["TotalRecordsinDB"] = DBNull.Value;
                        drNewRow["Weightage"] = DBNull.Value;
                        drNewRow["Remarks"] = string.Empty;
                    }

                    dtTestSegment.Rows.Add(drNewRow);
                    drNewRow = null;
                }
                return dtTestSegment;
            }
            finally
            {
                if (dtTestSegment != null) dtTestSegment = null;
            }
        }

        /// <summary>
        /// Gets the elected ID's for a checkboxlist
        /// </summary>
        /// <param name="checkBoxList">checkboxlist to get the selected values</param>
        /// <returns>Selected ID's in the checkboxlist</returns>
        private string GetSelectedValuesfromCheckBoxList(CheckBoxList checkBoxList)
        {
            StringBuilder stringBuilder = null;
            try
            {
                if (stringBuilder == null)
                    stringBuilder = new StringBuilder();
                for (int i = 0; i < checkBoxList.Items.Count; i++)
                {
                    if (checkBoxList.Items[i].Selected)
                    {
                        stringBuilder.Append(checkBoxList.Items[i].Value);
                        stringBuilder.Append(",");
                    }
                }
                return stringBuilder.ToString().TrimEnd(',');
            }
            finally
            {
                if (stringBuilder != null) stringBuilder = null;
            }
        }

        /// <summary>
        /// Add an Emty row to the test segment grid (this methos will 
        /// also check for maximum segments limit and no of question given less than 
        /// segments or not)
        /// </summary>
        private void AddTestSegmentRow(bool AddPositionProfileRows)
        {
            DataTable dtTestSegment = BuildTableFromGridView();
            if (CreateAutomaticInterviewTest_byQuestion_totalNumberTextBox.Text.Trim() != "")
                if (Convert.ToInt32(CreateAutomaticInterviewTest_byQuestion_totalNumberTextBox.Text.Trim()) <= dtTestSegment.Rows.Count)
                {
                    base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                     CreateAutomaticInterviewTest_bottomErrorMessageLabel,
                     Resources.HCMResource.CreateAutomaticInterviewTest_SegmentsGreaterthanSegments);
                    return;
                }
            if (dtTestSegment.Rows.Count == 10)
            {
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel, CreateAutomaticInterviewTest_bottomErrorMessageLabel,
                    "Maximum limit reached");
                return;
            }
            DataRow drNewRow = null;
            if (AddPositionProfileRows)
            {
                string[] PositionProfiles = CreateAutomaticInterviewTest_byQuestion_positionProfileTagsHiddenField.Value.Split(',');
                for (int i = 0; i < PositionProfiles.Length; i++)
                {
                    if (dtTestSegment.Select("Keyword LIKE '%" + GetSkillName(PositionProfiles[i]) + "%'").Length > 0)
                        continue;
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drNewRow))
                        drNewRow = null;
                    drNewRow = dtTestSegment.NewRow();
                    drNewRow["Keyword"] = GetSkillName(PositionProfiles[i]);
                    drNewRow["Weightage"] = GetSkillWeightage(PositionProfiles[i]);
                    dtTestSegment.Rows.Add(drNewRow);
                    if (dtTestSegment.Rows.Count == 10)
                    {
                        base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel, CreateAutomaticInterviewTest_bottomErrorMessageLabel,
                            "Maximum limit reached");
                        break;
                    }
                }
            }
            else
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drNewRow))
                    drNewRow = null;
                drNewRow = dtTestSegment.NewRow();
                dtTestSegment.Rows.Add(drNewRow);
            }
            CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataSource = dtTestSegment;
            CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataBind();
        }

        private string GetSkillName(string strKeyWord)
        {
            if (strKeyWord.IndexOf(']') == -1)
                return strKeyWord;
            return strKeyWord.Substring(0, strKeyWord.IndexOf('['));
        }

        private string GetSkillWeightage(string strKeyWord)
        {
            if (strKeyWord.IndexOf(']') == -1)
                return "0";
            return strKeyWord.Substring(strKeyWord.IndexOf('[') + 1, strKeyWord.Length - strKeyWord.IndexOf('[') - 2);
        }

        /// <summary>
        /// Checks for an empty value in segment wise (atleast a value
        /// should be given for a criteria)
        /// </summary>
        /// <param name="dtTestSegment">Datatable of the test segment grid</param>
        /// <returns>true if a single segment had atleast one search criteria 
        /// else false</returns>
        private bool CheckForEmptyValuesInSegmentGrid(ref DataTable dtTestSegment)
        {
            bool isValid = true;
            int NoofQuestions = 0;
            StringBuilder sbErrorSegements = null;
            StringBuilder sbDeleteSegments = null;
            try
            {
                int.TryParse(CreateAutomaticInterviewTest_byQuestion_totalNumberTextBox.Text.Trim(), out NoofQuestions);
                if (NoofQuestions == 0)
                {
                    base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                        CreateAutomaticInterviewTest_bottomErrorMessageLabel, "Enter valid number of questions");
                    isValid = false;
                    return isValid;
                }
                if (NoofQuestions < dtTestSegment.Rows.Count)
                {
                    base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                          CreateAutomaticInterviewTest_bottomErrorMessageLabel,
                          Resources.HCMResource.CreateAutomaticInterviewTest_SegmentsGreaterthanSegments);
                    isValid = false;
                    return isValid;
                }
                int InvalidColumnCount = 0;
                int TotalColumns = 7;
                sbErrorSegements = new StringBuilder();
                sbDeleteSegments = new StringBuilder();
                for (int i = 0; i < dtTestSegment.Rows.Count; i++)
                {
                    for (int ColumnCount = 1; ColumnCount <= TotalColumns; ColumnCount++)
                        if (dtTestSegment.Rows[i][ColumnCount].ToString() == "")
                            InvalidColumnCount++;
                    if (InvalidColumnCount == TotalColumns)
                    {
                        sbErrorSegements.Append(i + 1);
                        sbErrorSegements.Append(",");
                        isValid = false;
                        if (i != 0)
                        {
                            sbDeleteSegments.Append(i + 1);
                            sbDeleteSegments.Append(",");
                        }
                    }
                    InvalidColumnCount = 0;
                }
                if (!isValid)
                {
                    base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                                    CreateAutomaticInterviewTest_bottomErrorMessageLabel,
                                    string.Format(Resources.HCMResource.CreateAutomaticInterviewTest_segmentEmpty,
                                    (sbDeleteSegments.ToString().Length == 0) ?
                                    sbErrorSegements.ToString().TrimEnd(',') :
                                    sbErrorSegements.ToString().TrimEnd(',') + " or delete segement " + sbDeleteSegments.ToString().TrimEnd(',')));
                }
                return isValid;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(sbErrorSegements)) sbErrorSegements = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(sbDeleteSegments)) sbDeleteSegments = null;
            }
        }

        /// <summary>
        /// Deletes the user selected search segment from the grid
        /// </summary>
        /// <param name="RowIndex">Index of the segment to delete</param>
        private void DeleteTestSegmentRow(int RowIndex)
        {
            DataTable dtTestSegment = BuildTableFromGridView();
            if (dtTestSegment.Rows.Count == 1)
            {
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                    CreateAutomaticInterviewTest_bottomErrorMessageLabel, Resources.HCMResource.CreateAutomaticInterviewTest_AllSegementsDelee);
                return;
            }
            dtTestSegment.Rows.RemoveAt(RowIndex);
            CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataSource = dtTestSegment;
            CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataBind();
        }

        /// <summary>
        /// This method do all validations for generate button clicks and 
        /// if all validations passed it will call approriate method for to 
        /// load test according to the user given serach criteria
        /// </summary>
        private void LoadAutomatedQuestions()
        {
            DataTable dtTestSearchCriteria = null;
            try
            {
                dtTestSearchCriteria = BuildTableFromGridView();
                CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataSource = dtTestSearchCriteria;
                CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataBind();
                if (!CheckForEmptyValuesInSegmentGrid(ref dtTestSearchCriteria))
                    return;
                LoadQuestions(Convert.ToInt32(CreateAutomaticInterviewTest_byQuestion_totalNumberTextBox.Text.Trim()),
                    ref dtTestSearchCriteria);
            }
            finally
            {
                if (dtTestSearchCriteria != null) dtTestSearchCriteria = null;
            }
        }


        /// <summary>
        /// This method loads the user provided search criteria 
        /// in to collection of 'TestSearchCriteria' data object
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid data table</param>
        /// <returns>List of 'TestSearchCriteria' data object that contains
        /// user given search criteria</returns>
        private List<TestSearchCriteria> GetQuestionSearchCriteria(ref DataTable dtSearchCriteria)
        {
            List<TestSearchCriteria> testSearchCriterias = null;
            TestSearchCriteria testSearchCriteria = null;
            Subject subject = null;
            try
            {
                for (int i = 0; i < dtSearchCriteria.Rows.Count; i++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria))
                        testSearchCriteria = new TestSearchCriteria();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Cat_sub_id"]))
                    {
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(subject))
                            subject = new Subject();
                        subject.SubjectID = Convert.ToInt32(dtSearchCriteria.Rows[i]["Cat_sub_id"]);
                        testSearchCriteria.Subjects = new List<Subject>();
                        testSearchCriteria.Subjects.Add(subject);
                    }
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["TestArea"]))
                        testSearchCriteria.TestAreasID = dtSearchCriteria.Rows[i]["TestArea"].ToString();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Complexity"]))
                        testSearchCriteria.Complexity = dtSearchCriteria.Rows[i]["Complexity"].ToString();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Keyword"]))
                        testSearchCriteria.Keyword = dtSearchCriteria.Rows[i]["Keyword"].ToString();
                    testSearchCriteria.NoOfQuestions = Convert.ToInt32(dtSearchCriteria.Rows[i]["ExpectedQuestions"]);
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriterias))
                        testSearchCriterias = new List<TestSearchCriteria>();
                    testSearchCriterias.Add(testSearchCriteria);
                    testSearchCriteria = null;
                    subject = null;
                }
                return testSearchCriterias;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria)) testSearchCriteria = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriterias)) testSearchCriterias = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(subject)) subject = null;
            }
        }

        /// <summary>
        /// This methods distributes the total questions to the segments
        /// according to the segment weightage 
        /// either given by user or system generated.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        private void LoadExpectedQuestions(ref DataTable dtSearchCriteria, int NoofQuestions, out bool ZeroExpectedQuestion)
        {
            ZeroExpectedQuestion = false;
            int SearchSegements = dtSearchCriteria.Rows.Count;
            int QuestionsAdded = 0;
            int ExpectedQuestions = 0;
            int LoopI = 0;
            for (LoopI = 0; LoopI < SearchSegements; LoopI++)
            {
                QuestionsAdded = Convert.ToInt32((Convert.ToDecimal(dtSearchCriteria.Rows[LoopI]["Weightage"])
                    / 100) * NoofQuestions);
                dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"] = QuestionsAdded;
                ExpectedQuestions += QuestionsAdded;
            }
            if (ExpectedQuestions > NoofQuestions)
            {
                int QuestionsToAdd = ExpectedQuestions - NoofQuestions;
                for (LoopI = SearchSegements - 1; LoopI >= 0; LoopI--)
                {
                    if (Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"]) - QuestionsToAdd <= 0)
                        continue;
                    dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"]) - QuestionsToAdd;
                    break;
                }
            }
            else if (ExpectedQuestions < NoofQuestions)
                dtSearchCriteria.Rows[LoopI - 1]["ExpectedQuestions"] = QuestionsAdded +
                    (NoofQuestions - ExpectedQuestions);
            if (dtSearchCriteria.Select("ExpectedQuestions <= 0").Length > 0)
            {
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                         CreateAutomaticInterviewTest_bottomErrorMessageLabel,
                         Resources.HCMResource.CreateAutomaticInterviewTest_ExpectedQuestionZero);
                ZeroExpectedQuestion = true;
            }
        }

        /// <summary>
        /// This method allocates weightages to the segements
        /// and ensures that the total weightage is 100% or not.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        /// <param name="TotalWeightage">Total weightage allocated by this method 
        /// (Note:- it should be out parametarized)</param>
        private void LoadWeightages(ref DataTable dtSearchCriteria, int NoofQuestions, out int TotalWeightage)
        {
            DataRow[] drWeightage = null;
            int GivenWeightage = 0;
            int SearchSegements = 0;
            int WeightagePercentage = 0;
            int LoopI = 0;
            int ToAddExtraWeightage = 0;
            try
            {
                SearchSegements = dtSearchCriteria.Rows.Count;
                drWeightage = dtSearchCriteria.Select("Weightage > 0");
                for (LoopI = 1; LoopI <= drWeightage.Length; LoopI++)
                    GivenWeightage += Convert.ToInt32(drWeightage[LoopI - 1]["Weightage"]);
                if ((GivenWeightage > 100) || ((GivenWeightage == 100) && (SearchSegements >= LoopI)))
                {
                    TotalWeightage = 101;
                    base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                         CreateAutomaticInterviewTest_bottomErrorMessageLabel,
                         Resources.HCMResource.CreateAutomaticInterviewTest_WeightageExceed);
                    return;
                }
                if (GivenWeightage == 0)
                    WeightagePercentage = 100 / SearchSegements;
                else if (GivenWeightage < 100)
                {
                    if (SearchSegements == drWeightage.Length)
                        dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"] = 100 -
                            (GivenWeightage - Convert.ToInt32(dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"]));
                    else
                        WeightagePercentage = (100 - GivenWeightage) / (SearchSegements - drWeightage.Length);
                }
                ToAddExtraWeightage = 100 - ((WeightagePercentage * (SearchSegements - (LoopI - 1))) + GivenWeightage);
                for (LoopI = 0; LoopI < SearchSegements; LoopI++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[LoopI]["Weightage"]))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] = WeightagePercentage;
                    if ((ToAddExtraWeightage > 0) && (WeightagePercentage != 0) && (LoopI == SearchSegements - 1))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["Weightage"]) + ToAddExtraWeightage;
                    dtSearchCriteria.Rows[LoopI]["PickedQuestions"] = 0;
                    dtSearchCriteria.Rows[LoopI]["TotalRecordsinDB"] = 0;
                    dtSearchCriteria.Rows[LoopI]["QuestionsDifference"] = 0;
                }
                TotalWeightage = 100;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drWeightage)) drWeightage = null;
            }
        }

        /// <summary>
        /// This method finally communicates with question detail BL manager
        /// to get the questions for the test with user provided serach
        /// criteria.
        /// </summary>
        /// <param name="NoofQuestions">Total number of questions to pick for 
        /// the test</param>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        private void LoadQuestions(int NoofQuestions, ref DataTable dtSearchCriteria)
        {
            int TotalWeightage = 0;
            bool ZeroExpectedQuestion = false;
            LoadWeightages(ref dtSearchCriteria, NoofQuestions, out TotalWeightage);
            CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataSource = dtSearchCriteria;
            CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataBind();
            if (TotalWeightage > 100)
                return;
            LoadExpectedQuestions(ref dtSearchCriteria, NoofQuestions, out ZeroExpectedQuestion);
            if (ZeroExpectedQuestion)
                return;
            List<QuestionDetail> questionDetails =
                         new QuestionBLManager().
                            GetAutomatedInterviewQuestions(
                             GetQuestionSearchCriteria(ref dtSearchCriteria)
                             , NoofQuestions, ref dtSearchCriteria,
                             Convert.ToInt32(CreateAutomaticInterviewTest_byQuestion_generateTestHitCountHiddenField.Value), base.userID, base.tenantID);
            if ((questionDetails == null) || (questionDetails.Count == 0))
            {
                CreateAutomaticInterviewTest_byQuestion_testDrftDiv.Visible = false;
                CreateAutomaticInterviewTest_byQuestion_automatedTestUserControl.Visible = false;
                CreateAutomaticInterviewTest_byQuestion_generateTestButton.Visible = false;
                CreateAutomaticInterviewTest_byQuestion_generateTestButton.Text = "Generate Interview";
                CreateAutomaticInterviewTest_byQuestion_generateTestHitCountHiddenField.Value = "0";
                CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl.SysRecommendedTime = 0;
                SetEnableStatusForSearchControls(true);
                SetVisibleStatusForButton(false);
                CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataSource = dtSearchCriteria;
                CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataBind();
                base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                        CreateAutomaticInterviewTest_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {

                ViewState[CreateAutomaticInterviewTest_QUESTIONVIEWSTATE] = questionDetails;
                TestStatistics testStatistics = AutomatedSummaryList(questionDetails);
                LoadTestStatisticsObject(ref questionDetails, ref testStatistics);
                CreateAutomaticInterviewTest_byQuestion_automatedTestUserControl.DataSource = testStatistics;
                CreateAutomaticInterviewTest_byQuestion_automatedTestUserControl.Visible = true;
                CreateAutomaticInterviewTest_byQuestion_testDrftDiv.Visible = true;
                if (NoofQuestions == questionDetails.Count)
                {
                    CreateAutomaticInterviewTest_byQuestion_generateTestButton.Text = "Regenerate Interview";
                    CreateAutomaticInterviewTest_byQuestion_generateTestHitCountHiddenField.Value =
                        (Convert.ToInt32(CreateAutomaticInterviewTest_byQuestion_generateTestHitCountHiddenField.Value) + 1).ToString();
                    if (Convert.ToInt32(CreateAutomaticInterviewTest_byQuestion_generateTestHitCountHiddenField.Value) > 0)
                        SetEnableStatusForSearchControls(false);
                }
                else
                {
                    CreateAutomaticInterviewTest_byQuestion_generateTestButton.Text = "Generate Interview";
                    CreateAutomaticInterviewTest_byQuestion_generateTestHitCountHiddenField.Value = "0";
                    SetEnableStatusForSearchControls(true);
                }
                ViewState["QuestionOrder"] = questionDetails.ToList();
                if (Session["QuestionOrder"] == null)
                {
                    Session["QuestionOrder"] = questionDetails.ToList();
                    CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataSource = (Session["QuestionOrder"] as List<QuestionDetail>).OrderBy(p => p.DisplayOrder);
                    CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataBind();
                }
                else
                {
                    CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataSource = (Session["QuestionOrder"] as List<QuestionDetail>).OrderBy(p => p.DisplayOrder);
                    CreateAutomaticInterviewTest_byQuestion_testDraftGridView.DataBind();
                }
                CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataSource = dtSearchCriteria;
                CreateAutomaticInterviewTest_byQuestion_testSegmantGridView.DataBind();
                HideLastRecord();
            }
        }



        private void SetEnableStatusForSearchControls(bool VisibilityStatus)
        {
            CreateAutomaticInterviewTest_byQuestion_totalNumberTextBox.Enabled = VisibilityStatus;
            CreateAutomaticInterviewTest_byQuestion_totalCostTextBox.Enabled = VisibilityStatus;
            CreateAutomaticInterviewTest_byQuestion_positionProfileTextBox.Enabled = VisibilityStatus;
            CreateAutomaticInterviewTest_byQuestion_authorImageButton.Visible = VisibilityStatus;
            CreateAutomaticInterviewTest_byQuestion_timeSearchTextbox.Enabled = VisibilityStatus;
            CreateAutomaticInterviewTest_byQuestion_addSegmantLinkButton.Visible = VisibilityStatus;
            SearchCategorySubjectControl_addLinkButton.Visible = VisibilityStatus;
        }

        /// <summary>
        /// This method will load all the test statistics user control data.
        /// </summary>
        /// <param name="questionDetails">Question details displaying to the user
        /// (Note:- You need to pass this parameter with 'ref' keyword)</param>
        /// <param name="testStatistics">Test statistics object assigning to 
        /// automated test user control as datasource. 
        /// (Note:- You need to pass this parameter with 'ref' keyword)
        /// This method will update this object.</param>
        private void LoadTestStatisticsObject(ref List<QuestionDetail> questionDetails, ref TestStatistics testStatistics)
        {
            TestDetail testDetail = null;
            try
            {
                testStatistics.NoOfQuestions = questionDetails.Count;
                testStatistics.TestCost = 0;
                for (int i = 0; i < questionDetails.Count; i++)
                    testStatistics.TestCost += questionDetails[i].CreditsEarned;
                if (testStatistics.TestCost == 0)
                    testStatistics.TestCost = 0.00M;
                testStatistics.AverageTimeTakenByCandidates =
                    Convert.ToInt32(questionDetails.Average(p => p.AverageTimeTaken)) * questionDetails.Count;
                testStatistics.AutomatedTestAverageComplexity = ((AttributeDetail)
                    (new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetails))).
                    AttributeName;
                LoadChartDetails(ref questionDetails, ref testStatistics);
                testDetail = CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl.TestDetailDataSource;
                testDetail.SystemRecommendedTime = 0;
                testDetail.SystemRecommendedTime = Convert.ToInt32(testStatistics.AverageTimeTakenByCandidates);
                CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl.TestDetailDataSource = testDetail;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail)) testDetail = null;
            }
        }

        /// <summary>
        /// This method loads the chart details to the test statistics object.
        /// </summary>
        /// <param name="questionDetails">Question details displaying to the user
        /// (Note:- You need to pass this parameter with 'ref' keyword)</param>
        /// <param name="testStatistics">Test statistics object assigning to 
        /// automated test user control as datasource. 
        /// (Note:- You need to pass this parameter with 'ref' keyword)
        /// This method will update this object by adding chart controls data.</param>
        private void LoadChartDetails(ref List<QuestionDetail> questionDetails,
            ref TestStatistics testStatistics)
        {
            ChartData chartData = null;
            List<ChartData> testAreaChartDatum = null;
            List<ChartData> complexityChartDatum = null;
            try
            {
                var complexityGroups =
                                          from q in questionDetails
                                          group q by q.ComplexityName into g
                                          select new { ComplexityName = g.Key, ComplexityCount = g.Count() };
                complexityChartDatum = new List<ChartData>();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(chartData))
                    chartData = new ChartData();
                foreach (var complexity in complexityGroups)
                {
                    chartData = new ChartData();
                    chartData.ChartXValue = complexity.ComplexityName;
                    chartData.ChartYValue = complexity.ComplexityCount;
                    complexityChartDatum.Add(chartData);
                }
                chartData = null;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.ComplexityStatisticsChartData))
                    testStatistics.ComplexityStatisticsChartData = new SingleChartData();
                testStatistics.ComplexityStatisticsChartData.ChartType = SeriesChartType.Pie;
                testStatistics.ComplexityStatisticsChartData.ChartData = complexityChartDatum;
                testStatistics.ComplexityStatisticsChartData.ChartLength = 140;
                testStatistics.ComplexityStatisticsChartData.ChartWidth = 300;
                testStatistics.ComplexityStatisticsChartData.ChartTitle = "Complexity Statistics";
                var testAreaCounts =
                        from t in questionDetails
                        group t by t.TestAreaName into g
                        select new { TestAreaName = g.Key, TestAreaCOunt = g.Count() };
                testAreaChartDatum = new List<ChartData>();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(chartData))
                    chartData = new ChartData();
                foreach (var testArea in testAreaCounts)
                {
                    chartData = new ChartData();
                    chartData.ChartXValue = testArea.TestAreaName;
                    chartData.ChartYValue = testArea.TestAreaCOunt;
                    testAreaChartDatum.Add(chartData);
                }
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.TestAreaStatisticsChartData))
                    testStatistics.TestAreaStatisticsChartData = new SingleChartData();
                testStatistics.TestAreaStatisticsChartData.ChartType = SeriesChartType.Pie;
                testStatistics.TestAreaStatisticsChartData.ChartData = testAreaChartDatum;
                testStatistics.TestAreaStatisticsChartData.ChartLength = 140;
                testStatistics.TestAreaStatisticsChartData.ChartWidth = 300;
                testStatistics.TestAreaStatisticsChartData.ChartTitle = "Interview Area Statistics";
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(chartData)) chartData = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testAreaChartDatum)) testAreaChartDatum = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(complexityChartDatum)) complexityChartDatum = null;
            }
        }

        /// <summary>
        /// This Method gives the summary of the result questions.
        /// Summary will be like 
        /// Category Name, subject, test area, complexity, total questions
        /// </summary>
        /// <param name="ResultQuestionDetails">Question List that o/p to user</param>
        /// <returns>List of strings with each string contains
        /// category & subject & Test Area & Complexity & 
        /// Toal Number of Questions shown to the user</returns>
        private TestStatistics AutomatedSummaryList(List<QuestionDetail> ResultQuestionDetails)
        {
            StringBuilder CategoryNames = new StringBuilder();
            AutomatedTestSummaryGrid automatedTestSummaryGrid = null;
            TestStatistics testStatistics = null;
            List<AutomatedTestSummaryGrid> automatedTestSummaryGridOrderdByQuestion = null;
            int TotalCategory = 0;
            try
            {
                for (int i = 0; i < ResultQuestionDetails.Count; i++)
                {
                    if (CategoryNames.ToString().IndexOf("CAT: " + ResultQuestionDetails[i].CategoryName.Trim() + " SUB: " + ResultQuestionDetails[i].SubjectName.Trim() + " TA: " + ResultQuestionDetails[i].TestAreaName.Trim() + " COMP: " + ResultQuestionDetails[i].Complexity.Trim()) >= 0)
                        continue;
                    TotalCategory = ResultQuestionDetails.FindAll(
                        p => "CAT: " + p.CategoryName.Trim() + " SUB: " + p.SubjectName.Trim() +
                    " TA: " + p.TestAreaName.Trim() +
                    " COMP: " + p.Complexity.Trim() == "CAT: " + ResultQuestionDetails[i].CategoryName.Trim() + " SUB: " + ResultQuestionDetails[i].SubjectName.Trim() +
                    " TA: " + ResultQuestionDetails[i].TestAreaName.Trim() +
                    " COMP: " + ResultQuestionDetails[i].Complexity.Trim()).Count;
                    CategoryNames.Append("CAT: " + ResultQuestionDetails[i].CategoryName.Trim() + " SUB: " + ResultQuestionDetails[i].SubjectName.Trim() +
                    " TA: " + ResultQuestionDetails[i].TestAreaName.Trim() +
                    " COMP: " + ResultQuestionDetails[i].Complexity.Trim());
                    CategoryNames.Append(",");
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(automatedTestSummaryGrid))
                        automatedTestSummaryGrid = new AutomatedTestSummaryGrid();
                    automatedTestSummaryGrid.CategoryName = ResultQuestionDetails[i].CategoryName;
                    automatedTestSummaryGrid.SubjectName = ResultQuestionDetails[i].SubjectName;
                    automatedTestSummaryGrid.TestAreaName = ResultQuestionDetails[i].TestAreaName;
                    automatedTestSummaryGrid.Complexity = ResultQuestionDetails[i].ComplexityName;
                    automatedTestSummaryGrid.NoofQuestionsInCategory = TotalCategory;
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics))
                        testStatistics = new TestStatistics();
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.AutomatedTestSummaryGrid))
                        testStatistics.AutomatedTestSummaryGrid = new List<AutomatedTestSummaryGrid>();
                    testStatistics.AutomatedTestSummaryGrid.Add(automatedTestSummaryGrid);
                    automatedTestSummaryGrid = null;
                    TotalCategory = 0;
                }
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.AutomatedTestSummaryGrid))
                {
                    automatedTestSummaryGridOrderdByQuestion = testStatistics.AutomatedTestSummaryGrid.OrderByDescending(p => p.NoofQuestionsInCategory).ToList();
                    testStatistics.AutomatedTestSummaryGrid = null;
                    testStatistics.AutomatedTestSummaryGrid = new List<AutomatedTestSummaryGrid>();
                    testStatistics.AutomatedTestSummaryGrid = automatedTestSummaryGridOrderdByQuestion;
                }
                testStatistics.NoOfQuestions = ResultQuestionDetails.Count;
                return testStatistics;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(CategoryNames)) CategoryNames = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics)) testStatistics = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(automatedTestSummaryGrid))
                    automatedTestSummaryGrid = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(automatedTestSummaryGridOrderdByQuestion))
                    automatedTestSummaryGridOrderdByQuestion = null;
            }
        }

        /// <summary>
        /// Gets the test area attribute table
        /// </summary>
        /// <returns></returns>
        private List<AttributeDetail> GetTestAreaList()
        {
            return new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA,
                  Constants.SortTypeConstants.ASCENDING);
        }

        /// <summary>
        /// Gets the complexity area attribute table
        /// </summary>
        /// <returns></returns>
        private List<AttributeDetail> GetComplexityList()
        {
            return new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY,
                Constants.SortTypeConstants.ASCENDING);
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(CreateAutomaticInterviewTest_byQuestion_isMaximizedHiddenField.Value) &&
                         CreateAutomaticInterviewTest_byQuestion_isMaximizedHiddenField.Value == "Y")
            {
                CreateAutomaticInterviewTest_byQuestion_serachDiv.Style["display"] = "none";
                CreateAutomaticInterviewTest_byQuestion_searchResultsUpSpan.Style["display"] = "block";
                CreateAutomaticInterviewTest_byQuestion_searchResultsDownSpan.Style["display"] = "none";
                CreateAutomaticInterviewTest_byQuestion_testDrftDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                CreateAutomaticInterviewTest_byQuestion_serachDiv.Style["display"] = "block";
                CreateAutomaticInterviewTest_byQuestion_searchResultsUpSpan.Style["display"] = "none";
                CreateAutomaticInterviewTest_byQuestion_searchResultsDownSpan.Style["display"] = "block";
                CreateAutomaticInterviewTest_byQuestion_testDrftDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Load all javascript tags
        /// </summary>
        private void SubscribeClientSideHandlers()
        {
            CreateAutomaticInterviewTest_byQuestion_authorImageButton.Attributes.Add("onclick",
                    "return ShowClientRequestToGetTagWeightages('" + CreateAutomaticInterviewTest_byQuestion_positionProfileTextBox.ClientID + "','" +
                    CreateAutomaticInterviewTest_byQuestion_positionProfileTagsHiddenField.ClientID + "','" +
                    CreateAutomaticInterviewTest_byQuestion_positionProfileIDHiddenField.ClientID + "');");
            CreateAutomaticInterviewTest_byQuestion_searchResultsUpSpan.Attributes.Add("onclick", "ExpandOrRestore('" +
                CreateAutomaticInterviewTest_byQuestion_testDrftDiv.ClientID + "','" +
                CreateAutomaticInterviewTest_byQuestion_serachDiv.ClientID + "','" +
                CreateAutomaticInterviewTest_byQuestion_searchResultsUpSpan.ClientID + "','" +
                CreateAutomaticInterviewTest_byQuestion_searchResultsDownSpan.ClientID + "','" +
                CreateAutomaticInterviewTest_byQuestion_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" + EXPANDED_HEIGHT + "');");
            CreateAutomaticInterviewTest_byQuestion_searchResultsDownSpan.Attributes.Add("onclick", "ExpandOrRestore('" +
                CreateAutomaticInterviewTest_byQuestion_testDrftDiv.ClientID + "','" +
                CreateAutomaticInterviewTest_byQuestion_serachDiv.ClientID + "','" +
                CreateAutomaticInterviewTest_byQuestion_searchResultsUpSpan.ClientID + "','" +
                CreateAutomaticInterviewTest_byQuestion_searchResultsDownSpan.ClientID + "','" +
                CreateAutomaticInterviewTest_byQuestion_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" + EXPANDED_HEIGHT + "');");
        }

        /// <summary>
        /// Sets the Visibility Status for save and create session buttons.
        /// </summary>
        /// <param name="VisibilityStatus">Visibility status for those button <br />
        /// Note(:- Save button Visibile status show with 'Not' gate)</param>
        private void SetVisibleStatusForButton(bool VisibilityStatus)
        {
            CreateAutomaticInterviewTest_topCreateSessionButton.Visible = VisibilityStatus;
            CreateAutomaticInterviewTest_bottomCreateSessionButton.Visible = VisibilityStatus;
            CreateAutomaticInterviewTest_bottomSaveButton.Visible = !VisibilityStatus;
            CreateAutomaticInterviewTest_topSaveButton.Visible = !VisibilityStatus;
            CreateAutomaticInterviewTest_byQuestion_generateTestButton.Visible = !VisibilityStatus;
        }

        /// <summary>
        /// This method enables or disables test segment grid controls
        /// according to the search status.
        /// </summary>
        /// <param name="RowIndex">Row index of the grid to disable controls</param>
        private void EnableOrDisableTestSegmentGridControls(GridViewRow CurrenRowBounding)
        {
            if (CreateAutomaticInterviewTest_byQuestion_generateTestHitCountHiddenField.Value == "")
                return;
            if (Convert.ToInt32(CreateAutomaticInterviewTest_byQuestion_generateTestHitCountHiddenField.Value) == 0)
                return;
            ((ImageButton)
                CurrenRowBounding.FindControl("CreateAutomaticInterviewTest_byQuestion_testSegmantGridView_deleteImageButton")).Visible = false;
            ((ImageButton)CurrenRowBounding
                .FindControl("CreateAutomaticInterviewTest_byQuestion_categoryImageButton")).Visible = false;
            ((TextBox)CurrenRowBounding.
                FindControl("CreateAutomaticInterviewTest_byQuestion_weightageTextBox")).Enabled = false;
            ((TextBox)CurrenRowBounding.
                FindControl("CreateAutomaticInterviewTest_byQuestion_keywordTextBox")).Enabled = false;
            ((CheckBoxList)CurrenRowBounding.
                FindControl("CreateAutomaticInterviewTest_byQuestion_complexityCheckBoxList")).Enabled = false;
            ((CheckBoxList)CurrenRowBounding.
                FindControl("CreateAutomaticInterviewTest_byQuestion_testAreaCheckBoxList")).Enabled = false;
        }


        private DataTable FilledSegmants(ref DataTable dtTestSegment)
        {
            int InvalidColumnCount = 0;
            int TotalColumns = 7;
            for (int i = 0; i < dtTestSegment.Rows.Count; i++)
            {
                for (int ColumnCount = 1; ColumnCount <= TotalColumns; ColumnCount++)
                    if (dtTestSegment.Rows[i][ColumnCount].ToString() == "")
                        InvalidColumnCount++;
                if (InvalidColumnCount == TotalColumns)
                {
                    dtTestSegment.Rows.Remove(dtTestSegment.Rows[i]);
                }
                InvalidColumnCount = 0;
            }
            return dtTestSegment;
        }


        #endregion Private Methods

        #region Protected Overriden Methods                                    

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool FirstTabPriority = false;
            int NoofQuestions = 0;

            List<QuestionDetail> questionDetailTestDraftResultList = null;
            TestDetail testDetail = null;
            try
            {
                int.TryParse(CreateAutomaticInterviewTest_byQuestion_totalNumberTextBox.Text.Trim(), out NoofQuestions);

                if (NoofQuestions == 0 && CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows.Count==0)
                {
                    base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                        CreateAutomaticInterviewTest_bottomErrorMessageLabel, "Enter valid number of questions");
                    isValidData = false;
                    CreateAutomaticInterviewTest_mainTabContainer.ActiveTabIndex = 0;
                    FirstTabPriority = true;
                }

                testDetail = new TestDetail();
                testDetail = CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl.TestDetailDataSource;
                //  int recommendedTime = testDetail.RecommendedCompletionTime;
                if (testDetail.Name.Trim().Length == 0)
                {
                    base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                       CreateAutomaticInterviewTest_bottomErrorMessageLabel,
                       Resources.HCMResource.CreateAutomaticInterviewTest_TestNameEmpty);
                    isValidData = false;
                    CreateAutomaticInterviewTest_mainTabContainer.ActiveTabIndex = 1;
                }
                if (testDetail.Description.Trim().Length == 0)
                {
                    base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                       CreateAutomaticInterviewTest_bottomErrorMessageLabel,
                       Resources.HCMResource.CreateManualInterviewTest_Test_Description_Empty);
                    isValidData = false;
                }
                if (CreateAutomaticInterviewTest_byQuestion_testDraftGridView!=null && 
                    CreateAutomaticInterviewTest_byQuestion_testDraftGridView.Rows.Count == 0)
                {
                    base.ShowMessage(CreateAutomaticInterviewTest_topErrorMessageLabel,
                        CreateAutomaticInterviewTest_bottomErrorMessageLabel,
                        Resources.HCMResource.CreateAutomaticInterviewTest_QuestionsEmpty);
                    isValidData = false;
                    CreateAutomaticInterviewTest_mainTabContainer.ActiveTabIndex = 0;
                    FirstTabPriority = true;
                }
                
                if (FirstTabPriority)
                    CreateAutomaticInterviewTest_mainTabContainer.ActiveTabIndex = 0;
                
                return isValidData;
            }
            finally
            {

                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList)) questionDetailTestDraftResultList = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testDetail)) testDetail = null;
            }
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            SetVisibleStatusForButton(false);
            SubscribeClientSideHandlers();

            CreateAutomaticInterviewTest_byQuestion_automatedTestUserControl.Visible = false;
            LoadPositionProfileDetail();
            if (Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING] != null &&
                Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING != string.Empty)
                return;

            BindEmptyTestSegmentGrid();
        }

        /// <summary>
        /// Represents the method to load the position profile details from the query string
        /// </summary>
        private void LoadPositionProfileDetail()
        {
            // Check if position profile ID is present.
            if (Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING] == null ||
                Request.QueryString[Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING].Trim().Length == 0)
            {
                return;
            }

            int positionProfileID = 0;

            // Check if the query string contains a valid position profile ID.
            if (!int.TryParse(Request.QueryString
                [Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING], out positionProfileID))
            {
                return;
            }

            PositionProfileDetail positionProfileDetail = new PositionProfileBLManager().GetPositionProfileKeyWord(positionProfileID);
            if (Support.Utility.IsNullOrEmpty(positionProfileDetail))
                return;

            CreateAutomaticInterviewTest_byQuestion_positionProfileTextBox.Text = positionProfileDetail.PositionProfileName;
            CreateAutomaticInterviewTest_byQuestion_positionProfileTagsHiddenField.Value = positionProfileDetail.Keys;
            try
            {
                AddTestSegmentRow(true);
            }
            finally
            {
                CreateAutomaticInterviewTest_byQuestion_positionProfileTagsHiddenField.Value = "";
            }
            //CreateAutomaticInterviewTest_byQuestion_positionProfileIdHiddenField.Value = ppID.ToString();
            CreateAutomaticInterviewTest_byQuestion_positionProfileIDHiddenField.Value = positionProfileDetail.PositionProfileID.ToString();

            // Assign the position profile name and ID to the test details tab.
            CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl.PositionProfileID = positionProfileID;
            CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl.PositionProfileName = positionProfileDetail.PositionProfileName;
        }

        #endregion Protected Overriden Methods
        protected void CreateAutomaticInterviewTest_topSaveButton_Click(object sender, EventArgs e)
        {

        }
        
}
}