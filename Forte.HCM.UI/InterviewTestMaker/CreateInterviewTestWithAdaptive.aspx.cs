#region Header                                                                                        

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreateManualTest.cs
// File that represents the user Serach the Question by various Key filed. 
// and Create the test Details and questions.
// This will helps Create a Test and have to implemeted the Certification details..

#endregion

#region Directives                                                                                    

using System;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.ExternalService;
using Forte.HCM.UI.CommonControls;


#endregion

namespace Forte.HCM.UI.InterviewTestMaker
{
    public partial class CreateInterviewTestWithAdaptive : PageBase
    {
        #region Declaration
        string sQryString = "";
        string questionID = "";
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        /// <summary>
        /// A <see cref="int"/> constant that holds the adaptive page size.
        /// </summary>
        private const int ADAPTIVE_PAGE_SIZE = 5;

        List<QuestionDetail> questionDetailSearchResultList;
        List<QuestionDetail> questionDetailTestDraftResultList;
        List<QuestionDetail> interviewQuestionDraftList;
        private const string ADAPTIVE_QUESTION_DETAILS_VIEWSTATE = "ADAPTIVEQUESTIONDETAILS";

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the Click event of the CreateInterviewTestWithAdaptive_addQuestionLinkButton_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CreateInterviewTestWithAdaptive_addQuestionLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                CreateInterviewTestWithAdaptive_addQuestionErrorLabel.Text = string.Empty;
                CreateInterviewTestWithAdaptive_addQuestionQuestTextBox.Text = string.Empty;
                CreateInterviewTestWithAdaptive_questionImage.ImageUrl = string.Empty;
                CreateInterviewTestWithAdaptive_addQuestionAnswerTextBox.Text = string.Empty;
                CreateInterviewTestWithAdaptive_isQuestionRepository.Checked = false;
                CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryTextBox.Text = string.Empty;
                CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_subjectTextBox.Text = string.Empty;
                CreateInterviewTestWithAdaptive_tagsTextBox.Text = string.Empty;
                

                // Binding test area to radiobutton list.
                CreateInterviewTestWithAdaptive_addQuestionRadioButtonList.DataSource =
               new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
                CreateInterviewTestWithAdaptive_addQuestionRadioButtonList.DataTextField = "AttributeName";
                CreateInterviewTestWithAdaptive_addQuestionRadioButtonList.DataValueField = "AttributeID";
                CreateInterviewTestWithAdaptive_addQuestionRadioButtonList.DataBind();
                CreateInterviewTestWithAdaptive_addQuestionRadioButtonList.Items[0].Selected = true;

                //Bind complexities/attributes based on the attribute type
                CreateInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.DataSource =
                    new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
                CreateInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.DataTextField = "AttributeName";
                CreateInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.DataValueField = "AttributeID";
                CreateInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.DataBind();

                CreateInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.Items.Insert(0, "--Select--");
                CreateInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.SelectedIndex = 0;

                CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryImageButton.Attributes.Add("onclick",
                        "return LoadCategorySubjectLookUpforReadOnly('" + CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryTextBox.ClientID +
                            "','" + CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_subjectTextBox.ClientID + "','" +
                            CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryHiddenField.ClientID + "','" +
                            CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryNameHiddenField.ClientID + "','" +
                            CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_subjectNameHiddenField.ClientID + "');");
                CreateInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                    CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exception.Message);

            }
        }


        /// <summary>
        /// Handles the Clicked event of the CreateInterviewTestWithAdaptive_addQuestioSaveButton_Clicked control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CreateInterviewTestWithAdaptive_addQuestioSaveButton_Clicked
                (object sender, EventArgs e)
        {
            try
            {
                if (CreateInterviewTestWithAdaptive_addQuestionQuestTextBox.Text.Length == 0)
                {
                    CreateInterviewTestWithAdaptive_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateInterviewTestWithAdaptive_addQuestionErrorLabel, "Enter the question");
                    CreateInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (CreateInterviewTestWithAdaptive_addQuestionAnswerTextBox.Text.Length == 0)
                {
                    CreateInterviewTestWithAdaptive_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateInterviewTestWithAdaptive_addQuestionErrorLabel, "Enter the answer");
                    CreateInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (CreateInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.SelectedItem.Text == "--Select--")
                {
                    CreateInterviewTestWithAdaptive_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateInterviewTestWithAdaptive_addQuestionErrorLabel, "Select the complexity");

                    CreateInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }

                if (CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryTextBox.Text.Length == 0)
                {
                    CreateInterviewTestWithAdaptive_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateInterviewTestWithAdaptive_addQuestionErrorLabel, "Select the category and subject");
                    CreateInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_subjectTextBox.Text.Length == 0)
                {
                    CreateInterviewTestWithAdaptive_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(CreateInterviewTestWithAdaptive_addQuestionErrorLabel, "Select the category and subject");
                    CreateInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }
                SaveNewQuestion();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                    CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (CreateInterviewTestWithAdaptive_mainTabContainer.ActiveTabIndex == 0)
                    Page.Form.DefaultButton = CreateInterviewTestWithAdaptive_topSearchButton.UniqueID;
                else if (CreateInterviewTestWithAdaptive_mainTabContainer.ActiveTabIndex == 1)
                    Page.Form.DefaultButton = CreateInterviewTestWithAdaptive_bottomSaveButton.UniqueID;

                StateMaintainence();
                CreateInterviewTestWithAdaptive_ConfirmPopupPanel.Style.Add("height", "220px");
                //SetSearchGridView();


                if (!IsPostBack)
                {
                    Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
                    CreateInterviewTestWithAdaptive_recommendAdaptiveQuestions.Style.Add("display", "none");
                    LoadValues();
                }
                Master.SetPageCaption("Create Interview");
                MessageLabel_Reset();
                questionID = hiddenValue.Value;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionID))
                    MoveToTestDraft("Add");
                ExpandRestore();
                CreateInterviewTestWithAdaptive_bottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (CreateInterviewTestWithAdaptive_pagingNavigator_PageNumberClick);

                // Create events for paging control
                CreateInterviewTestWithAdaptive_adaptivebottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (CreateInterviewTestWithAdaptive_adaptivepagingNavigator_PageNumberClick);

                CreateInterviewTestWithAdaptive_categorySubjectControl.ControlMessageThrown +=
                    new CategorySubjectControl.ControlMessageThrownDelegate(
                        CreateInterviewTestWithAdaptive_categorySubjectControl_ControlMessageThrown);

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// It shows error/success messages whenever the user calls this method
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        void CreateInterviewTestWithAdaptive_categorySubjectControl_ControlMessageThrown(object sender, ControlMessageEventArgs c)
        {
            // Show message on the label.
            if (c.MessageType == MessageType.Error)
            {
                CreateInterviewTestWithAdaptive_topErrorMessageLabel.Text = c.Message.ToString();
                CreateInterviewTestWithAdaptive_bottomErrorMessageLabel.Text = c.Message.ToString();
            }
            else if (c.MessageType == MessageType.Success)
            {
                CreateInterviewTestWithAdaptive_topSuccessMessageLabel.Text = c.Message.ToString();
                CreateInterviewTestWithAdaptive_bottomSuccessMessageLabel.Text = c.Message.ToString();
            }
            BindTestDraftGrid();
            BindSearchGridView();
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void CreateInterviewTestWithAdaptive_okClick(object sender, EventArgs e)
        {
            try
            {
                MoveToTestDraft("Delete");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                    CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }

        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Cancel Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel the Event will be raised.
        /// </remarks>
        protected void CreateInterviewTestWithAdaptive_cancelClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = "";
                CreateInterviewTestWithAdaptive_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("");
                //BindAdaptiveGridStateMaintain();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void CreateInterviewTestWithAdaptive_searchQuestionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    string[] questionCollection = e.CommandArgument.ToString().Trim().Split(':');
                    CreateInterviewTestWithAdaptive_previewQuestionAddHiddenField.Value = e.CommandArgument.ToString();
                    CreateInterviewTestWithAdaptive_questionDetailSummaryControl.ShowAddButton = true;
                    CreateInterviewTestWithAdaptive_questionDetailSummaryControl.LoadQuestionDetails
                        (questionCollection[0], int.Parse(questionCollection[1]));
                    CreateInterviewTestWithAdaptive_questionDetailSummaryControl.Visible = true;
                    CreateInterviewTestWithAdaptive_questionModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CreateInterviewTestWithAdaptive_searchQuestionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField CreateInterviewTestWithAdaptive_hidddenValue = (HiddenField)e.Row.FindControl("CreateInterviewTestWithAdaptive_hidddenValue");
                    string questionID = CreateInterviewTestWithAdaptive_hidddenValue.Value.Split(':')[0].ToString();
                    ImageButton alertImageButton = (ImageButton)e.Row.FindControl("CreateInterviewTestWithAdaptive_searchQuestionAlertImageButton");
                    alertImageButton.Attributes.Add("OnClick", "javascript:return LoadFlagQuestion('" + questionID + "');");

                    Image img = (Image)e.Row.FindControl("CreateInterviewTestWithAdaptive_searchQuestionSelectImage");
                    img.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + CreateInterviewTestWithAdaptive_searchQuestionGridView.ClientID + "');");
                    img.Attributes.Add("OnClick", "javascript:return singleqnsclick('" + CreateInterviewTestWithAdaptive_hidddenValue.Value + "','" + CreateInterviewTestWithAdaptive_selectMultipleImage.ClientID + "');");

                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }


        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CreateInterviewTestWithAdaptive_testDrftGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ImageButton upImageButton = (ImageButton)e.Row.FindControl
                        ("CreateInterviewTestWithAdaptive_byQuestion_moveUpQuestionImageButton");
                    if (e.Row.RowIndex == 0)
                    {
                        upImageButton.Visible = false;
                    }
                    Image img = (Image)e.Row.FindControl("CreateInterviewTestWithAdaptive_testDrftRemoveImage");
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void CreateInterviewTestWithAdaptive_testDrftGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    CreateInterviewTestWithAdaptive_questionModalPopupExtender.Show();
                }
                else if (e.CommandName == "Select")
                {
                    hiddenDeleteValue.Value = e.CommandArgument.ToString();
                    CreateInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Message = string.Format(Resources.HCMResource.SearchQuestion_Delete_Confirm_Message, e.CommandArgument.ToString().Split(':')[0]);
                    CreateInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    CreateInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    CreateInterviewTestWithAdaptive_ConfirmPopupExtender.Show();
                }
                else if ((e.CommandName == "MoveDown") || (e.CommandName == "MoveUp"))
                {
                    interviewQuestionDraftList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                    int order = int.Parse(e.CommandArgument.ToString());
                    //if the command name is move down , move the record one line down 
                    if (e.CommandName == "MoveDown")
                    {
                        foreach (QuestionDetail question in interviewQuestionDraftList)
                        {
                            //Change the display order of the draft list
                            if (question.DisplayOrder == order)
                            {
                                question.DisplayOrder = order + 1;
                                continue;
                            }
                            if (question.DisplayOrder == order + 1)
                            {
                                question.DisplayOrder = order;
                                continue;
                            }
                        }
                    }

                    //if the command name is moveup, move the record one line up
                    if (e.CommandName == "MoveUp")
                    {
                        foreach (QuestionDetail question in interviewQuestionDraftList)
                        {
                            if (question.DisplayOrder == order - 1)
                            {
                                question.DisplayOrder = order;
                                continue;
                            }
                            if (question.DisplayOrder == order)
                            {
                                question.DisplayOrder = order - 1;
                                continue;
                            }
                        }
                    }
                    CreateInterviewTestWithAdaptive_testDrftGridView.DataSource = interviewQuestionDraftList.OrderBy(p => p.DisplayOrder);
                    CreateInterviewTestWithAdaptive_testDrftGridView.DataBind();
                    ViewState["TESTDRAFTGRID"] = interviewQuestionDraftList.OrderBy(p => p.DisplayOrder).ToList();
                }
                HideLastRecord();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void CreateInterviewTestWithAdaptive_searchQuestionButton_Click(object sender, EventArgs e)
        {
            try
            {
                MoveToTestDraft("");
                CreateInterviewTestWithAdaptive_bottomPagingNavigator.Reset();
                LoadQuestion(1, ViewState["SORTDIRECTIONKEY"].ToString(),
                     ViewState["SORTEXPRESSION"].ToString());
                //BindAdaptiveGridStateMaintain();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the Move to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the move the Selected Question to Test Draft
        /// grid.
        /// </remarks>
        protected void CreateInterviewTestWithAdaptive_selectMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                AddSearchQuestionToTestDraft();
                if (hiddenValue.Value != "")
                {
                    MoveToTestDraft("Add");
                }
                else
                {
                    MoveToTestDraft("");
                    base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                                       CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, Resources.HCMResource.CreateManualTest_Question_From_Search_Result);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the Remove to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the delete the Selected Question from Test Draft
        /// grid.
        /// </remarks>
        protected void CreateInterviewTestWithAdaptive_removeMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool questionSelected = false;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
                {
                    foreach (GridViewRow row in CreateInterviewTestWithAdaptive_testDrftGridView.Rows)
                    {
                        CheckBox CreateInterviewTestWithAdaptive_testDrftCheckbox = (CheckBox)row.FindControl("CreateInterviewTestWithAdaptive_testDrftCheckbox");
                        HiddenField CreateInterviewTestWithAdaptive_questionKeyHiddenField = (HiddenField)row.FindControl("CreateInterviewTestWithAdaptive_questionKeyHiddenField");
                        if (CreateInterviewTestWithAdaptive_testDrftCheckbox.Checked)
                        {
                            questionID = questionID + CreateInterviewTestWithAdaptive_questionKeyHiddenField.Value + ",";
                            questionSelected = true;
                        }
                    }
                    // hiddenValue.Value = questionID.TrimEnd(',');

                }
                if (questionSelected)
                {
                    hiddenDeleteValue.Value = questionID.TrimEnd(',');
                    CreateInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Message = Resources.HCMResource.SearchQuestion_Delete_Multiple_Confirm_Message;
                    CreateInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    CreateInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    CreateInterviewTestWithAdaptive_ConfirmPopupExtender.Show();
                    // MoveToTestDraft("Delete");
                }
                else
                {
                    MoveToTestDraft("");
                    base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                                       CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, Resources.HCMResource.CreateManualInterviewTest_Question_From_InterviewTestDraft_Result);

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Interchange Advanced and Simple Search Link 
        /// and Search Criteria fields 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateInterviewTestWithAdaptive_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (CreateInterviewTestWithAdaptive_simpleLinkButton.Text.ToLower() == "simple")
                {
                    CreateInterviewTestWithAdaptive_simpleLinkButton.Text = "Advanced";
                    CreateInterviewTestWithAdaptive_simpleSearchDiv.Visible = true;
                    CreateInterviewTestWithAdaptive_advanceSearchDiv.Visible = false;
                }
                else
                {
                    CreateInterviewTestWithAdaptive_simpleLinkButton.Text = "Simple";
                    CreateInterviewTestWithAdaptive_simpleSearchDiv.Visible = false;
                    CreateInterviewTestWithAdaptive_advanceSearchDiv.Visible = true;
                }
                MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateInterviewTestWithAdaptive_pagingNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                MoveToTestDraft("");
                ViewState["PAGENUMBER"] = e.PageNumber;
                LoadQuestion(e.PageNumber, ViewState["SORTDIRECTIONKEY"].ToString(),
                    ViewState["SORTEXPRESSION"].ToString());
                //BindAdaptiveGridStateMaintain();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Create Test and Test Question 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CreateInterviewTestWithAdaptive_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if feature usage limit exceeds for creating test.
                if (new AdminBLManager().IsFeatureUsageLimitExceeds(base.tenantID, Constants.FeatureConstants.CREATE_TEST))
                {
                    base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                        CreateInterviewTestWithAdaptive_bottomErrorMessageLabel,
                        "You have reached the maximum limit based your subscription plan. Cannot create more interviews");
                    return;
                }

                if (!IsValidData())
                    return;

                ShowSaveConfirmPopUp();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void CreateInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindTestDraftGrid();
                BindSearchGridView();
                if (CreateInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList.SelectedIndex == 1)
                {
                    CreateInterviewTestWithAdaptive_adaptiveRecommendationsClicked.Value = "";
                    CreateInterviewTestWithAdaptive_recommendAdaptiveQuestions.Style.Add("display", "none");
                    CreateInterviewTestWithAdaptive_adaptiveGridView.Visible = false;
                    CreateInterviewTestWithAdaptive_adaptivePagingHiddenField.Value = "1";
                    ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE] = null;
                    BindEmptyAdaptiveGridView();
                    return;
                }
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                                    false), Convert.ToInt32(CreateInterviewTestWithAdaptive_adaptivePagingHiddenField.Value));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void CreateInterviewTestWithAdaptive_questionDetailPreviewControl_AddClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = CreateInterviewTestWithAdaptive_previewQuestionAddHiddenField.Value;
                CreateInterviewTestWithAdaptive_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("Add");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void CreateInterviewTestWithAdaptive_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void CreateInterviewTestWithAdaptive_adaptivepagingNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                BindSearchGridView();
                BindTestDraftGrid();
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                    ((CreateInterviewTestWithAdaptive_adaptiveRecommendationsClicked.Value == "") ? false : true)),
                    e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void CreateInterviewTestWithAdaptive_recommendAdaptiveQuestions_Click(object sender, EventArgs e)
        {
            try
            {
                BindTestDraftGrid();
                BindSearchGridView();
                CreateInterviewTestWithAdaptive_adaptiveRecommendationsClicked.Value = "1";
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                    true), Convert.ToInt32(CreateInterviewTestWithAdaptive_adaptivePagingHiddenField.Value));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void CreateInterviewTestWithAdaptive_adaptiveGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    CreateInterviewTestWithAdaptive_previewQuestionAddHiddenField.Value = e.CommandArgument.ToString();
                    string[] QuestionDetails = e.CommandArgument.ToString().Split(':');
                    CreateInterviewTestWithAdaptive_questionDetailSummaryControl.Visible = true;
                    CreateInterviewTestWithAdaptive_questionDetailSummaryControl.ShowAddButton = true;
                    CreateInterviewTestWithAdaptive_questionDetailSummaryControl.LoadQuestionDetails(QuestionDetails[0], Convert.ToInt32(QuestionDetails[1]));

                    CreateInterviewTestWithAdaptive_questionModalPopupExtender.Show();
                }
                if (e.CommandName == "AdaptiveQuestion")
                {
                    //CreateInterviewTestWithAdaptive_adaptiveQuestionPreviewModalPopupExtender.Show();
                    ViewState["RowIndex"] = (e.CommandArgument.ToString());
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }

        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CreateInterviewTestWithAdaptive_adaptiveGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                ImageButton CreateInterviewTestWithAdaptive_adaptiveSelectImage = (ImageButton)e.Row.FindControl("CreateInterviewTestWithAdaptive_adaptiveSelectImage");
                string questionID = ((HiddenField)e.Row.FindControl("CreateInterviewTestWithAdaptive_adaptiveQuestionhiddenValue")).Value;
                CreateInterviewTestWithAdaptive_adaptiveSelectImage.Attributes.Add("OnClick", "javascript:return singleqnsclick('" + questionID + "','" + CreateInterviewTestWithAdaptive_selectMultipleImage.ClientID + "');");
                Image alertImage = (Image)e.Row.FindControl("CreateInterviewTestWithAdaptive_adaptiveAlertImageButton");
                alertImage.Attributes.Add("onClick", "javascript:return LoadFlagQuestion('" + questionID.Split(':')[0] + "');");
                CreateInterviewTestWithAdaptive_adaptiveSelectImage.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + CreateInterviewTestWithAdaptive_adaptiveGridView.ClientID + "');");
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        protected void CreateInterviewTestWithAdaptive_questionDetailTopAddButton_Click(object sender, EventArgs e)
        {
            //CreateInterviewTestWithAdaptive_adaptiveQuestionPreviewModalPopupExtender.Hide();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "AddRow",
            //    "<script> AddRow(" + ViewState["RowIndex"] + "," + 1 + "); </script>", false);

            //ViewState["RowIndex"] = 0;

        }


        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and 
        /// OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void CreateInterviewTest_okPopUpClick(object sender, EventArgs e)
        {
            try
            {
                Button YesorNo = (Button)sender;
                if (YesorNo.Text.ToString() == "Ok")
                {
                    if (CreateInterviewTestSave_confirmPopupExtenderControl.ReviewAndSave)
                        SaveQuestions();
                    else
                        SaveInteviewQuestionsWithDefaultSettings();
                }
                else
                {
                    ShowSaveConfirmPopUp();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                    CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        ///  Event Handling for the common ConfirmMessage Popup Extenter Cancel and 
        ///  Close Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel/Close button Event will be raised.
        /// </remarks>
        protected void CreateInterviewTest_cancelPopUpClick(object sender, EventArgs e)
        {
            DataTable dtTestSearchCriteria = null;
            try
            {
                //CreateManualInterviewTest_testDetailsUserControl.TestDetailDataSource = (TestDetail)Session["controlTestDetail"];
                //CreateManualInterviewTest_testDrftGridView.DataSource = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>; ;
                //CreateManualInterviewTest_testDrftGridView.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                    CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtTestSearchCriteria)) dtTestSearchCriteria = null;
            }
        }

        /// <summary>
        /// Handler that helps to save the posted file in session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CreateInterviewTestWithAdaptive_questionImageOnUploadComplete(object sender,
            AsyncFileUploadEventArgs e)
        {
            System.Threading.Thread.Sleep(500);
            if (CreateInterviewTestWithAdaptive_questionImageUpload.PostedFile != null)
            {
                if (CreateInterviewTestWithAdaptive_questionImageUpload.PostedFile.ContentLength > 102400) // check if image size exceeds 100kb
                {
                    base.ShowMessage(CreateInterviewTestWithAdaptive_addQuestionErrorLabel,
                        Resources.HCMResource.BatchQuestionEntry_imageSizeExceeded);
                    CreateInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }

                // Check if the photo is of the desired extension.
                string extension = Path.GetExtension(CreateInterviewTestWithAdaptive_questionImageUpload.FileName);
                extension = extension.ToLower();

                if (extension != ".gif" && extension != ".png" && extension != ".jpg" && extension != ".jpeg")
                {
                    base.ShowMessage(CreateInterviewTestWithAdaptive_addQuestionErrorLabel, "Only gif/png/jpg/jpeg files are allowed");
                    CreateInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }

                // Read the bytes from the posted file.
                HttpPostedFile postedQstImage = CreateInterviewTestWithAdaptive_questionImageUpload.PostedFile;
                int nFileLen = postedQstImage.ContentLength;
                byte[] imgData = new byte[nFileLen];

                postedQstImage.InputStream.Read(imgData, 0, nFileLen);
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = imgData;

                CreateInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                return;
            }
            else
            {
                base.ShowMessage(CreateInterviewTestWithAdaptive_addQuestionErrorLabel, "Please select valid image to upload");
                CreateInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                return;
            }
        }

        /// <summary>
        /// Handler that helps to show the preview of the saved image
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>   
        protected void CreateInterviewTestWithAdaptive_questionImageButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["POSTED_INTERVIEW_QUESTION_IMAGE"] != null)
                {
                    CreateInterviewTestWithAdaptive_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=INTERVIEW_QUESTION_IMAGE";
                    CreateInterviewTestWithAdaptive_addQuestionImageTR.Style.Add("display", "none");
                    CreateInterviewTestWithAdaptive_displayQuestionImageTR.Style.Add("display", "");
                    CreateInterviewTestWithAdaptive_addImageLinkButton.Style.Add("display", "none");
                    CreateInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler that helps to delete the posted file.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateInterviewTestWithAdaptive_deleteLinkButtonClick(object sender, EventArgs e)
        {
            try
            {
                ResetQuestionImage();
                CreateInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                return;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                    exp.Message);
            }
        }
        #endregion

        #region Private  Methods
        /// <summary>
        /// Method to reset the question image
        /// </summary>
        private void ResetQuestionImage()
        {
            Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            CreateInterviewTestWithAdaptive_questionImage.ImageUrl = string.Empty;
            CreateInterviewTestWithAdaptive_addQuestionImageTR.Style.Add("display", "none");
            CreateInterviewTestWithAdaptive_displayQuestionImageTR.Style.Add("display", "none");
            CreateInterviewTestWithAdaptive_addImageLinkButton.Style.Add("display", "");
        }
        /// <summary>
        /// Saves the new question.
        /// </summary>
        private void SaveNewQuestion()
        {
            List<QuestionDetail> questions = null;
            QuestionDetail questionDetail = null;
            QuestionBLManager questionBLManager = new QuestionBLManager();

            questionDetail = new QuestionDetail();

            if (ViewState["TESTDRAFTGRID"] == null)
                questions = new List<QuestionDetail>();
            else
                questions = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

            questionDetail = ConstructQuestionDetails();
            new QuestionBLManager().SaveInterviewQuestion(questionDetail);

            questionDetail.QuestionRelationId = questionBLManager.GetQuestionRelationID(questionDetail.QuestionKey);
            questionDetail.CategoryName = questionBLManager.GetCategory(questionDetail.QuestionKey);
            questionDetail.SubjectName = questionBLManager.GetSubject(questionDetail.QuestionKey);
            questionDetail.Complexity = questionDetail.ComplexityName;
            if (questions != null)
                questionDetail.DisplayOrder = questions.Count + 1;
            else
                questionDetail.DisplayOrder = 1;
            List<AnswerChoice> questionAnswerList = new BL.QuestionBLManager().
                GetInterviewQuestionOptions(questionDetail.QuestionKey);
            if (questionAnswerList != null)
                questionDetail.Choice_Desc += questionAnswerList[0].Choice.ToString();
            questions.Add(questionDetail);

            ViewState["TESTDRAFTGRID"] = questions.ToList();
            CreateInterviewTestWithAdaptive_testDrftGridView.DataSource = questions;
            CreateInterviewTestWithAdaptive_testDrftGridView.DataBind();
            TestDraftSummary testDraftSummary = new TestDraftSummary();
            testDraftSummary.QuestionDetail = questions;
            testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questions);
            CreateInterviewTestWithAdaptive_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;
            HideLastRecord();
            ResetQuestionImage();
        }

        /// <summary>
        /// Constructs the question details.
        /// </summary>
        /// <returns></returns>
        private QuestionDetail ConstructQuestionDetails()
        {
            // Construct question detail object.
            QuestionDetail questionDetail = new QuestionDetail();

            QuestionBLManager questionBLManager = new QuestionBLManager();

            questionDetail.Question = CreateInterviewTestWithAdaptive_addQuestionQuestTextBox.Text.Trim();

            if (CreateInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.SelectedItem.Value != null &&
                CreateInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.SelectedItem.Value.Trim().ToUpper() != "--SELECT--")
            {
                questionDetail.Complexity = CreateInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.SelectedItem.Value;
                questionDetail.ComplexityName = CreateInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.SelectedItem.Text;
            }

            //Set question type

            if (CreateInterviewTestWithAdaptive_isQuestionRepository.Checked)
                questionDetail.IsActiveFlag = "Y";
            else
                questionDetail.IsActiveFlag = "N";

            // Construct the answer choice.
            List<AnswerChoice> answerChoices = new List<AnswerChoice>();
            AnswerChoice answerChoice = null;

            answerChoice = new AnswerChoice();
            answerChoice.ChoiceID = 1;
            answerChoice.Choice = CreateInterviewTestWithAdaptive_addQuestionAnswerTextBox.Text.Trim();
            answerChoices.Add(answerChoice);

            questionDetail.AnswerChoices = answerChoices;

            questionDetail.Author = Convert.ToInt32(base.userID);

            // Additional settings (Test AreaId).

            questionDetail.TestAreaID = CreateInterviewTestWithAdaptive_addQuestionRadioButtonList.SelectedValue;
            questionDetail.TestAreaName = CreateInterviewTestWithAdaptive_addQuestionRadioButtonList.SelectedItem.Text;

            questionDetail.Tag = CreateInterviewTestWithAdaptive_tagsTextBox.Text;

            questionDetail.CreatedBy = Convert.ToInt32(base.userID);
            questionDetail.CreatedDate = DateTime.Now;

            questionDetail.ModifiedBy = Convert.ToInt32(base.userID);

            // Get selected subject ids only. This will be used in QuestionBLManager:InsertQuestionRelation
            // method to insert multiple subjects simultanesouly.
            questionDetail.SelectedSubjectIDs = CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryHiddenField.Value.ToString();

            if (Session["POSTED_INTERVIEW_QUESTION_IMAGE"] != null)
            {
                questionDetail.QuestionImage = Session["POSTED_INTERVIEW_QUESTION_IMAGE"] as byte[];
                questionDetail.HasImage = true;
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            }
            else
                questionDetail.HasImage = false;

            return questionDetail;
        }

        /// <summary>
        /// Hide move down image of the last record
        /// </summary>
        private void HideLastRecord()
        {
            foreach (GridViewRow row in CreateInterviewTestWithAdaptive_testDrftGridView.Rows)
            {
                if (row.RowIndex == CreateInterviewTestWithAdaptive_testDrftGridView.Rows.Count - 1)
                {
                    ImageButton dowmImageButton = (ImageButton)row.FindControl
                        ("CreateInterviewTestWithAdaptive_byQuestion_moveDownQuestionImageButton");
                    dowmImageButton.Visible = false;
                }
            }
        }

        /// <summary>
        /// Method stores the interview and question details to sesssion
        /// </summary>
        private void SaveQuestions()
        {
            try
            {
                questionDetailTestDraftResultList = new List<QuestionDetail>();
                questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                string testKey = string.Empty;
                int questionCount = questionDetailTestDraftResultList.Count;
                TestDetail testDetail = new TestDetail();
                testDetail = CreateInterviewTestWithAdaptive_testDetailsUserControl.TestDetailDataSource;
                testDetail.Questions = questionDetailTestDraftResultList;
                testDetail.IsActive = true;
                testDetail.CertificationId = 0;
                testDetail.CreatedBy = userID;
                testDetail.IsDeleted = false;
                testDetail.ModifiedBy = userID;
                testDetail.NoOfQuestions = questionCount;
                testDetail.TestAuthorID = userID;
                testDetail.TestCreationDate = DateTime.Now;
                testDetail.TestStatus = TestStatus.Active;
                testDetail.TestType = TestType.Genenal;
                testDetail.TestMode = "MANUAL";
                testDetail.TestKey = testKey;

                InterviewDetail interviewDetail = new InterviewDetail();
                interviewDetail.InterviewTestKey = testDetail.TestKey;
                interviewDetail.InterviewName = testDetail.Name;
                interviewDetail.InterviewDescription = testDetail.Description;
                interviewDetail.PositionProfileId = Convert.ToInt32(testDetail.PositionProfileID);
                interviewDetail.PositionProfileName = testDetail.PositionProfileName;
                interviewDetail.TestAuthor = testDetail.TestAuthorName;
                interviewDetail.SessionIncludedActive = testDetail.SessionIncludedActive;
                interviewDetail.TestAuthorID = testDetail.TestAuthorID; ;
                interviewDetail.SessionIncluded = testDetail.SessionIncluded;

                Session["interviewDetail"] = (InterviewDetail)interviewDetail;
                Session["controlTestDetail"] = (TestDetail)testDetail;
                Session["questionDetailTestDraftResultList"] = questionDetailTestDraftResultList as List<QuestionDetail>;
                Response.Redirect("~/InterviewTestMaker/AddInterviewTestQuestionRatings.aspx?m=1&s=1&parentpage=INT_T_MANU_QUST", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
                    questionDetailTestDraftResultList = null;
            }
        }

        /// <summary>
        /// Method saves the interview and question details with default settings
        /// </summary>
        private void SaveInteviewQuestionsWithDefaultSettings()
        {
            try
            {
                questionDetailTestDraftResultList = new List<QuestionDetail>();
                //Assign Weightage for default settings
                AssignDefault((ViewState["TESTDRAFTGRID"] as List<QuestionDetail>).Count);
                //End Weightage
                questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                string testKey = string.Empty;
                int questionCount = questionDetailTestDraftResultList.Count;
                TestDetail testDetail = new TestDetail();
                testDetail = CreateInterviewTestWithAdaptive_testDetailsUserControl.TestDetailDataSource;

                testDetail.Questions = questionDetailTestDraftResultList;
                testDetail.IsActive = true;
                testDetail.CertificationId = 0;
                testDetail.CreatedBy = userID;
                testDetail.IsDeleted = false;
                testDetail.ModifiedBy = userID;
                testDetail.NoOfQuestions = questionCount;
                testDetail.TestAuthorID = userID;
                testDetail.TestCreationDate = DateTime.Now;
                testDetail.TestStatus = TestStatus.Active;
                testDetail.TestType = TestType.Genenal;
                testDetail.TestMode = "MANUAL";
                testDetail.TestKey = testKey;

                testKey = new InterviewBLManager().SaveInterviewTest(testDetail, userID,
                    new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY,
                    questionDetailTestDraftResultList).AttributeID.ToString());

                //CreateInterviewTestWithAdaptive_interviewTestKeyHiddenField.Value = testKey;

                CreateInterviewTestWithAdaptive_topCreateSessionButton.Visible = true;
                CreateInterviewTestWithAdaptive_bottomCreateSessionButton.Visible = true;
                CreateInterviewTestWithAdaptive_topSaveButton.Visible = false;
                CreateInterviewTestWithAdaptive_bottomSaveButton.Visible = false;

                // Show a success message.
                base.ShowMessage(CreateInterviewTestWithAdaptive_topSuccessMessageLabel,
                    CreateInterviewTestWithAdaptive_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.CreateAutomaticInterviewTest_TestSuccessfullMessage,
                    testDetail.TestKey));

                CreateInterviewTestWithAdaptive_testDetailsUserControl.TestDetailDataSource = testDetail;

                // Send a mail to the position profile creator.
                try
                {
                    if (testDetail.PositionProfileID != 0)
                    {
                        new EmailHandler().SendMail(EntityType.InterviewCreatedForPositionProfile, testDetail.TestKey);
                    }
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }

                Response.Redirect(Constants.TestMakerConstants.EDIT_INTERIVIEW_TEST_WA +
                    "?m=1&s=1&mode=new&parentpage=INT_T_MANU_QUST" +
                    "&testkey=" + testKey, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
                    questionDetailTestDraftResultList = null;
            }
        }

        /// <summary>
        /// This method allocates default weightages,limit and ratings to the questions
        /// and ensures that the total weightage is 100% or not.
        /// </summary>
        /// <param name="NoofQuestions">No. of question</param>
        private void AssignDefault(int NoofQuestions)
        {
            DataRow[] drWeightage = null;
            int WeightagePercentage = 0;
            int ToAddExtraWeightage = 0;
            int index = 0;
            try
            {
                interviewQuestionDraftList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                WeightagePercentage = 100 / NoofQuestions;
                ToAddExtraWeightage = 100 - (WeightagePercentage * NoofQuestions);

                interviewQuestionDraftList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                foreach (QuestionDetail question in interviewQuestionDraftList)
                {
                    question.Weightage = WeightagePercentage;
                    question.InterviewTestRatings = 10;
                    question.TimeLimit = 300;
                    if ((ToAddExtraWeightage > 0) && (WeightagePercentage != 0)
                        && (index == NoofQuestions - 1))
                        question.Weightage = question.Weightage + ToAddExtraWeightage;
                    index++;
                }
                ViewState["TESTDRAFTGRID"] = interviewQuestionDraftList;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drWeightage)) drWeightage = null;
            }
        }
        /// <summary>
        /// This method Invokes Confirm pop up when user tries to save
        /// the less number of questions in the test draft panel with the
        /// number of questions provided.
        /// </summary>
        /// 
        private void ShowSaveConfirmPopUp()
        {
            CreateInterviewTestSave_confirmPopupExtenderControl.Title = "Save Options";
            CreateInterviewTestSave_confirmPopupExtenderControl.Type = MessageBoxType.OkCancel;
            CreateInterviewTestSave_confirmPopupExtenderControl.DataBind();
            CreateInterviewTest_saveTestModalPopupExtender.Show();
        }

        /// <summary>
        /// Binds the search grid view
        /// </summary>
        protected void BindSearchGridView()
        {
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
                return;
            CreateInterviewTestWithAdaptive_searchQuestionGridView.DataSource = (List<QuestionDetail>)ViewState["SEARCHRESULTGRID"];
            CreateInterviewTestWithAdaptive_searchQuestionGridView.DataBind();
        }

        /// <summary>
        /// Add & remove Question from the Test Draft.
        /// </summary>
        private void AddSearchQuestionToTestDraft()
        {
            StringBuilder sbQuestionIds = null;
            try
            {
                sbQuestionIds = new StringBuilder();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
                {
                    foreach (GridViewRow row in CreateInterviewTestWithAdaptive_searchQuestionGridView.Rows)
                    {
                        CheckBox CreateInterviewTestWithAdaptive_searchQuestionCheckbox = (CheckBox)row.FindControl("CreateInterviewTestWithAdaptive_searchQuestionCheckbox");
                        HiddenField CreateInterviewTestWithAdaptive_questionKeyHiddenField = (HiddenField)row.FindControl("CreateInterviewTestWithAdaptive_questionKeyHiddenField");
                        if (CreateInterviewTestWithAdaptive_searchQuestionCheckbox.Checked)
                            sbQuestionIds.Append(CreateInterviewTestWithAdaptive_questionKeyHiddenField.Value + ",");
                    }
                    hiddenValue.Value = sbQuestionIds.ToString().TrimEnd(',');
                    if (CreateInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList.SelectedIndex == 1)
                        return;
                    if (!CreateInterviewTestWithAdaptive_adaptiveGridView.Visible)
                        return;
                    for (int i = 0; i < CreateInterviewTestWithAdaptive_adaptiveGridView.Rows.Count; i++)
                    {
                        if (((CheckBox)(CreateInterviewTestWithAdaptive_adaptiveGridView.Rows[i].FindControl("CreateInterviewTestWithAdaptive_adaptiveCheckbox"))).Checked)
                            sbQuestionIds.Append(((HiddenField)(CreateInterviewTestWithAdaptive_adaptiveGridView.Rows[i].
                                FindControl("CreateInterviewTestWithAdaptive_adaptiveQuestionKeyHiddenField"))).Value + ",");
                    }
                    hiddenValue.Value = "";
                    hiddenValue.Value = sbQuestionIds.ToString().TrimEnd(',');
                }
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(sbQuestionIds)) sbQuestionIds = null;
            }
        }

        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>                                         
        private void ExpandRestore()
        {
            if (CreateInterviewTestWithAdaptive_restoreHiddenField.Value == "Y")
            {
                CreateInterviewTestWithAdaptive_searchCriteriasDiv.Style["display"] = "none";
                CreateInterviewTestWithAdaptive_searchResultsUpSpan.Style["display"] = "block";
                CreateInterviewTestWithAdaptive_searchResultsDownSpan.Style["display"] = "none";
                CreateInterviewTestWithAdaptive_questionDiv.Style["height"] = EXPANDED_HEIGHT;
                CreateInterviewTestWithAdaptive_adaptiveQuestionDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                CreateInterviewTestWithAdaptive_searchCriteriasDiv.Style["display"] = "block";
                CreateInterviewTestWithAdaptive_searchResultsUpSpan.Style["display"] = "none";
                CreateInterviewTestWithAdaptive_searchResultsDownSpan.Style["display"] = "block";
                CreateInterviewTestWithAdaptive_questionDiv.Style["height"] = RESTORED_HEIGHT;
                CreateInterviewTestWithAdaptive_adaptiveQuestionDiv.Style["height"] = RESTORED_HEIGHT;
            }
            CreateInterviewTestWithAdaptive_searchResultsUpImage.Attributes.Add("onclick",
                "ExpandOrRestoreforAdaptive('" +
                CreateInterviewTestWithAdaptive_questionDiv.ClientID + "','" +
                CreateInterviewTestWithAdaptive_searchCriteriasDiv.ClientID + "','" +
                CreateInterviewTestWithAdaptive_searchResultsUpSpan.ClientID + "','" +
                CreateInterviewTestWithAdaptive_searchResultsDownSpan.ClientID + "','" +
                CreateInterviewTestWithAdaptive_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "','" +
                CreateInterviewTestWithAdaptive_adaptiveQuestionDiv.ClientID + "');");
            CreateInterviewTestWithAdaptive_searchResultsDownImage.Attributes.Add("onclick",
                "ExpandOrRestoreforAdaptive('" +
                CreateInterviewTestWithAdaptive_questionDiv.ClientID + "','" +
                CreateInterviewTestWithAdaptive_searchCriteriasDiv.ClientID + "','" +
                CreateInterviewTestWithAdaptive_searchResultsUpSpan.ClientID + "','" +
                CreateInterviewTestWithAdaptive_searchResultsDownSpan.ClientID + "','" +
                CreateInterviewTestWithAdaptive_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "','" +
                CreateInterviewTestWithAdaptive_adaptiveQuestionDiv.ClientID + "');");
        }

        /// <summary>
        /// Reset the Messeags.
        /// </summary>
        private void MessageLabel_Reset()
        {
            CreateInterviewTestWithAdaptive_topErrorMessageLabel.Text = string.Empty;
            CreateInterviewTestWithAdaptive_bottomErrorMessageLabel.Text = string.Empty;
            CreateInterviewTestWithAdaptive_topSuccessMessageLabel.Text = string.Empty;
            CreateInterviewTestWithAdaptive_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Get Search Results with Paging and Sorting. 
        /// </summary>
        /// <param name="pageNumber">Current Page number </param>
        /// <param name="orderBy">QuestionKey/ModifiedDate/Complexity/Question Description/Credids Earned</param>
        /// <param name="sortDirection">Descending/Assending</param>
        private void LoadQuestion(int pageNumber, string orderBy, string sortDirection)
        {
            hiddenValue.Value = null;
            int totalRecords = 0;
            StringBuilder complexityID = GetSelectedComplexityID();
            StringBuilder testAreaID = GetTestAreaID();

            QuestionDetailSearchCriteria questionDetailSearchCriteria = new QuestionDetailSearchCriteria();

            if (CreateInterviewTestWithAdaptive_simpleLinkButton.Text == "Advanced")
            {
                questionDetailSearchCriteria.CategoryName =
                                    CreateInterviewTestWithAdaptive_categoryTextBox.Text.Trim() == "" ?
                                    null : CreateInterviewTestWithAdaptive_categoryTextBox.Text.Trim();
                questionDetailSearchCriteria.SubjectName =
                                    CreateInterviewTestWithAdaptive_subjectTextBox.Text.Trim() == "" ?
                                    null : CreateInterviewTestWithAdaptive_subjectTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    CreateInterviewTestWithAdaptive_keywordsTextBox.Text.Trim() == "" ?
                                    null : CreateInterviewTestWithAdaptive_keywordsTextBox.Text.Trim();
                questionDetailSearchCriteria.Author = base.userID;
            }
            else
            {
                string SelectedSubjectIDs = "";
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(CreateInterviewTestWithAdaptive_categorySubjectControl.SubjectDataSource))
                {
                    List<Category> categoryList = new List<Category>();
                    foreach (Subject subject in CreateInterviewTestWithAdaptive_categorySubjectControl.SubjectDataSource)
                    {

                        if (subject.IsSelected == true)
                        {
                            Category category = new Category();
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                            category.CategoryID = subject.CategoryID;
                            categoryList.Add(category);
                        }

                    }
                    ///Get all Subject ids in  Un selected Subject List.
                    Category categoryID = new Category();
                    foreach (Subject subject in CreateInterviewTestWithAdaptive_categorySubjectControl.SubjectDataSource)
                    {
                        categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                        if (categoryID == null)
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                    }
                }
                questionDetailSearchCriteria.CategorySubjectsKey =
                                    SelectedSubjectIDs == "" ?
                                    null : SelectedSubjectIDs.TrimEnd(',');
                questionDetailSearchCriteria.TestAreaID =
                                    testAreaID.ToString() == "" ?
                                    null : testAreaID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.Complexities =
                                    complexityID.ToString() == "" ?
                                    null : complexityID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.QuestionKey =
                                    CreateInterviewTestWithAdaptive_questionIDTextBox.Text.Trim() == "" ?
                                    null : CreateInterviewTestWithAdaptive_questionIDTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    CreateInterviewTestWithAdaptive_keywordTextBox.Text.Trim() == "" ?
                                    null : CreateInterviewTestWithAdaptive_keywordTextBox.Text.Trim();

                questionDetailSearchCriteria.AuthorName =
                                   CreateInterviewTestWithAdaptive_authorTextBox.Text.Trim() == "" ?
                                   null : CreateInterviewTestWithAdaptive_authorIdHiddenField.Value;
                questionDetailSearchCriteria.ActiveFlag = null;
                questionDetailSearchCriteria.CreditsEarned =
                                    CreateInterviewTestWithAdaptive_creditTextBox.Text.Trim() == "" ?
                                    0 : Convert.ToDecimal(CreateInterviewTestWithAdaptive_creditTextBox.Text.Trim());
                questionDetailSearchCriteria.ClientRequest = CreateManulTest_positionProfileKeywordTextBox.Text.Trim() == "" ?
                    null : CreateManulTest_positionProfileKeywordTextBox.Text.Trim();

                questionDetailSearchCriteria.Author = base.userID;

            }


            List<QuestionDetail> questionDetail = new List<QuestionDetail>();
            questionDetail = new BL.InterviewBLManager().GetInterviewSearchQuestions(questionDetailSearchCriteria,
                base.GridPageSize, pageNumber, orderBy, sortDirection, out totalRecords);

            if (questionDetail == null || questionDetail.Count == 0)
            {
                CreateInterviewTestWithAdaptive_searchQuestionGridView.DataSource = null;
                CreateInterviewTestWithAdaptive_searchQuestionGridView.DataBind();
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                 CreateInterviewTestWithAdaptive_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                CreateInterviewTestWithAdaptive_bottomPagingNavigator.TotalRecords = 0;
                CreateInterviewTestWithAdaptive_bottomPagingNavigator.Reset();
                //CreateInterviewTestWithAdaptive_questionDiv.Visible = false;
            }
            else
            {
                CreateInterviewTestWithAdaptive_searchQuestionGridView.DataSource = questionDetail;
                CreateInterviewTestWithAdaptive_searchQuestionGridView.DataBind();
                CreateInterviewTestWithAdaptive_bottomPagingNavigator.PageSize = base.GridPageSize;
                CreateInterviewTestWithAdaptive_bottomPagingNavigator.TotalRecords = totalRecords;
                //CreateInterviewTestWithAdaptive_questionDiv.Visible = true;
            }
            ViewState["SEARCHRESULTGRID"] = questionDetail;
            // return questionDetail;
        }
        /// <summary>
        ///  Bind complexities/attributes based on the attribute type
        /// </summary>
        private void BindComplexities()
        {
            CreateInterviewTestWithAdaptive_complexityCheckBoxList.DataSource =
                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
            CreateInterviewTestWithAdaptive_complexityCheckBoxList.DataTextField = "AttributeName";
            CreateInterviewTestWithAdaptive_complexityCheckBoxList.DataValueField = "AttributeID";
            CreateInterviewTestWithAdaptive_complexityCheckBoxList.DataBind();
        }
        /// <summary>
        /// Bind test area based on TEST_AREA attribute type
        /// </summary>
        private void BindTestArea()
        {

            CreateInterviewTestWithAdaptive_testAreaCheckBoxList.DataSource =
               new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
            CreateInterviewTestWithAdaptive_testAreaCheckBoxList.DataTextField = "AttributeName";
            CreateInterviewTestWithAdaptive_testAreaCheckBoxList.DataValueField = "AttributeID";
            CreateInterviewTestWithAdaptive_testAreaCheckBoxList.DataBind();
        }

        /// <summary>
        /// Get Selected Test Area IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetTestAreaID()
        {
            StringBuilder testAreaID = new StringBuilder();
            for (int i = 0; i < CreateInterviewTestWithAdaptive_testAreaCheckBoxList.Items.Count; i++)
            {

                if (CreateInterviewTestWithAdaptive_testAreaCheckBoxList.Items[i].Selected)
                {

                    testAreaID.Append(CreateInterviewTestWithAdaptive_testAreaCheckBoxList.Items[i].Value.ToString());
                    testAreaID.Append(",");
                }

            }
            return testAreaID;
        }
        /// <summary>
        /// Get Selected Complexity IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetSelectedComplexityID()
        {
            StringBuilder complexityID = new StringBuilder();
            for (int i = 0; i < CreateInterviewTestWithAdaptive_complexityCheckBoxList.Items.Count; i++)
            {
                if (CreateInterviewTestWithAdaptive_complexityCheckBoxList.Items[i].Selected)
                {

                    complexityID.Append(CreateInterviewTestWithAdaptive_complexityCheckBoxList.Items[i].Value.ToString());
                    complexityID.Append(",");
                }
            }
            return complexityID;
        }
        /// <summary>
        /// Uncheck the checkbox 
        /// </summary>
        private void UncheckCheckBox()
        {
            foreach (GridViewRow row in CreateInterviewTestWithAdaptive_searchQuestionGridView.Rows)
            {
                if (((CheckBox)row.FindControl("CreateInterviewTestWithAdaptive_searchQuestionCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("CreateInterviewTestWithAdaptive_searchQuestionCheckbox")).Checked = false;
                }
            }
            foreach (GridViewRow row in CreateInterviewTestWithAdaptive_adaptiveGridView.Rows)
            {
                if (((CheckBox)row.FindControl("CreateInterviewTestWithAdaptive_adaptiveCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("CreateInterviewTestWithAdaptive_adaptiveCheckbox")).Checked = false;
                }
            }

        }
        /// <summary>
        /// add and remove the Test Draft 
        /// </summary>
        /// <param name="action"></param>
        private void MoveToTestDraft(string action)
        {
            questionDetailTestDraftResultList = new List<QuestionDetail>();
            QuestionDetail questionDetailSearchResult = new QuestionDetail();
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenDeleteValue.Value))
            {
                hiddenValue.Value = hiddenDeleteValue.Value;
                hiddenDeleteValue.Value = null;
            }

            int StartIndex = -1;
            int TotalCount = 0;
            string addedQuestionID = "";
            string notAddedQuestion = "";
            bool alertFlag = false;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
            {
                questionDetailSearchResultList = new List<QuestionDetail>();
                questionDetailSearchResultList = ViewState["SEARCHRESULTGRID"] as List<QuestionDetail>;
            }
            if ((ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE] != null) &&
                (questionDetailSearchResultList != null))
            {
                StartIndex = questionDetailSearchResultList.Count;
                questionDetailSearchResultList.AddRange((List<QuestionDetail>)ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE]);
                TotalCount = questionDetailSearchResultList.Count - StartIndex;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
            {

                string[] questionIds = hiddenValue.Value.Split(',');


                foreach (string ids in questionIds)
                {
                    if (ids != "")
                    {
                        string[] idList = ids.Split(':');
                        string id = idList[0];
                        string relationId = idList[1];

                        if (ViewState["TESTDRAFTGRID"] != null)
                        {
                            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                            if (action == "Add")
                            {
                                if (!questionDetailTestDraftResultList.Exists(p => p.QuestionKey.Trim() == id.Trim()))
                                {
                                    MessageLabel_Reset();
                                    questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                    questionDetailSearchResult.DisplayOrder = questionDetailTestDraftResultList.Count + 1;
                                    List<AnswerChoice> questionAnswerList = new BL.QuestionBLManager().GetInterviewQuestionOptions(id);
                                    if (questionAnswerList != null)
                                        questionDetailSearchResult.Choice_Desc += questionAnswerList[0].Choice.ToString();
                                    questionDetailSearchResult.ComplexityName = questionDetailSearchResult.Complexity;
                                    questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                                    ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                                    addedQuestionID = addedQuestionID + id + ", ";
                                    base.ShowMessage(CreateInterviewTestWithAdaptive_topSuccessMessageLabel,
                                        CreateInterviewTestWithAdaptive_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToInterviewDraft);
                                }
                                else
                                {
                                    notAddedQuestion = notAddedQuestion + id + ", ";
                                    alertFlag = true;
                                }
                            }
                            else if (action == "Delete")
                            {
                                MessageLabel_Reset();
                                questionDetailSearchResult = questionDetailTestDraftResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                questionDetailTestDraftResultList.Remove(questionDetailSearchResult);
                                base.ShowMessage(CreateInterviewTestWithAdaptive_topSuccessMessageLabel,
                                    CreateInterviewTestWithAdaptive_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualInterviewTest_RemoveToTestDraft);
                                ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                            }
                        }
                        else
                        {
                            questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                            questionDetailSearchResult.DisplayOrder = 1;
                            List<AnswerChoice> questionAnswerList = new BL.QuestionBLManager().GetInterviewQuestionOptions(id);
                            if (questionAnswerList != null)
                                questionDetailSearchResult.Choice_Desc += questionAnswerList[0].Choice.ToString();
                            questionDetailSearchResult.ComplexityName = questionDetailSearchResult.Complexity;
                            questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                            base.ShowMessage(CreateInterviewTestWithAdaptive_topSuccessMessageLabel,
                                       CreateInterviewTestWithAdaptive_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToInterviewDraft);
                            ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                        }

                    }
                }
            }
            if (CreateInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList.SelectedIndex == 0)
            {
                switch (action)
                {
                    case "Add":
                        if (addedQuestionID == "")
                        {
                            CreateInterviewTestWithAdaptive_adaptiveRecommendationsClicked.Value = CreateInterviewTestWithAdaptive_adaptiveRecommendationsClicked.Value;
                            CreateInterviewTestWithAdaptive_adaptivePagingHiddenField.Value = CreateInterviewTestWithAdaptive_adaptivePagingHiddenField.Value;
                        }
                        else
                        {
                            CreateInterviewTestWithAdaptive_adaptiveRecommendationsClicked.Value = "";
                            CreateInterviewTestWithAdaptive_adaptivePagingHiddenField.Value = "1";
                        }
                        break;
                    case "Delete":
                        CreateInterviewTestWithAdaptive_adaptiveRecommendationsClicked.Value = "";
                        CreateInterviewTestWithAdaptive_adaptivePagingHiddenField.Value = "1";
                        break;
                }
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                    ((CreateInterviewTestWithAdaptive_adaptiveRecommendationsClicked.Value == "") ? false : true)),
                    Convert.ToInt32(CreateInterviewTestWithAdaptive_adaptivePagingHiddenField.Value));
            }
            if (alertFlag)
            {
                string alertMessage = string.Format(Resources.HCMResource.CreateManualInterviewTest_QuestionExist, notAddedQuestion.Trim().TrimEnd(','));
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(addedQuestionID))
                {
                    alertMessage = alertMessage + "<br/>" + string.Format(Resources.HCMResource.CreateManualTest_QuestionAddedToTestDraft, addedQuestionID.Trim().TrimEnd(','));
                }
                CreateInterviewTestWithAdaptive_ConfirmPopupPanel.Style.Add("height", "270px");
                CreateInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Type = MessageBoxType.Ok;
                CreateInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Title = "Warning";
                CreateInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Message = alertMessage;
                CreateInterviewTestWithAdaptive_ConfirmPopupExtender.Show();
            }
            hiddenValue.Value = null;
            if (StartIndex != -1)
                questionDetailSearchResultList.RemoveRange(StartIndex, TotalCount);
            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            CreateInterviewTestWithAdaptive_testDrftGridView.DataSource = questionDetailTestDraftResultList;
            CreateInterviewTestWithAdaptive_testDrftGridView.DataBind();
            if (questionDetailSearchResultList != null)
            {
                CreateInterviewTestWithAdaptive_searchQuestionGridView.DataSource = questionDetailSearchResultList;
                //CreateInterviewTestWithAdaptive_questionDiv.Visible = true;
            }
            else
            {
                CreateInterviewTestWithAdaptive_searchQuestionGridView.DataSource = null;
                CreateInterviewTestWithAdaptive_bottomPagingNavigator.TotalRecords = 0;
                CreateInterviewTestWithAdaptive_bottomPagingNavigator.Reset();
                //CreateInterviewTestWithAdaptive_questionDiv.Visible = false;
            }


            CreateInterviewTestWithAdaptive_searchQuestionGridView.DataBind();

            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
            {
                if (action != "")
                {
                    questionDetailTestDraftResultList = new List<QuestionDetail>();
                    questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                }
                TestDraftSummary testDraftSummary = new TestDraftSummary();
                testDraftSummary.QuestionDetail = questionDetailTestDraftResultList;
                testDraftSummary.AttributeDetail =
                    new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY,
                    questionDetailTestDraftResultList);
                CreateInterviewTestWithAdaptive_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;
            }
            UncheckCheckBox();
            HideLastRecord();
        }

        /// <summary>
        /// This methiod maintains the results state.
        /// </summary>
        private void StateMaintainence()
        {
            switch (CreateInterviewTestWithAdaptive_resultsHiddenField.Value)
            {
                case "0":
                    CreateInterviewTestWithAdaptive_questionDiv.Style.Add("width", "936px");
                    CreateInterviewTestWithAdaptive_adaptiveQuestionDiv.Style.Add("width", "0px");
                    break;
                case "1":
                    CreateInterviewTestWithAdaptive_questionDiv.Style.Add("width", "0px");
                    CreateInterviewTestWithAdaptive_adaptiveQuestionDiv.Style.Add("width", "936px");
                    break;
                case "2":
                    CreateInterviewTestWithAdaptive_questionDiv.Style.Add("width", "450px");
                    CreateInterviewTestWithAdaptive_adaptiveQuestionDiv.Style.Add("width", "450px");
                    break;
                default:
                    CreateInterviewTestWithAdaptive_questionDiv.Style.Add("width", "450px");
                    CreateInterviewTestWithAdaptive_adaptiveQuestionDiv.Style.Add("width", "450px");
                    break;
            }
        }

        private void BindTestDraftGrid()
        {
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["TESTDRAFTGRID"]))
                return;
            CreateInterviewTestWithAdaptive_testDrftGridView.DataSource = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>; ;
            CreateInterviewTestWithAdaptive_testDrftGridView.DataBind();
        }

        #region Adaptive Question Recommendation Methods


        /// <summary>
        /// This method gets the question id's that are in the draft panel
        /// </summary>
        /// <param name="questionDetails">List of question details that are in draft panel</param>
        /// <param name="ForceLoad">Whether the questions should load though 
        /// questions exceeded the adaptive limit.</param>
        /// <returns>string contains the draft question ids</returns>
        private string GetSelectedQuestionsIds(List<QuestionDetail> questionDetails, bool ForceLoad)
        {
            StringBuilder sbQuestionDetails = null;
            try
            {
                CreateInterviewTestWithAdaptive_recommendAdaptiveQuestions.Style.Add("display", "none");
                BindEmptyAdaptiveGridView();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetails))
                    return string.Empty;
                if (questionDetails.Count == 0)
                    return string.Empty;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["ADAPTIVE_LIMIT"]))
                    return "";
                if ((ForceLoad == false) && (Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ADAPTIVE_LIMIT"])
                    < questionDetails.Count))
                {
                    if (CreateInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList.SelectedIndex != 1)
                        CreateInterviewTestWithAdaptive_recommendAdaptiveQuestions.Style.Add("display", "block");
                    return "";
                }
                else
                    if (CreateInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList.SelectedIndex == 1)
                        return "";
                sbQuestionDetails = new StringBuilder();
                for (int i = 0; i < questionDetails.Count; i++)
                {
                    sbQuestionDetails.Append(questionDetails[i].QuestionKey);
                    sbQuestionDetails.Append(",");
                }
                return sbQuestionDetails.ToString().TrimEnd(',');
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(sbQuestionDetails)) sbQuestionDetails = null;
            }
        }

        /// <summary>
        /// This will Make adaptive grid view empty
        /// </summary>
        private void BindEmptyAdaptiveGridView()
        {
            CreateInterviewTestWithAdaptive_adaptiveGridView.DataSource = null;
            CreateInterviewTestWithAdaptive_adaptiveGridView.DataBind();
            CreateInterviewTestWithAdaptive_adaptivebottomPagingNavigator.Reset();
            CreateInterviewTestWithAdaptive_adaptivebottomPagingNavigator.TotalRecords = 0;
        }

        /// <summary>
        /// This method communicates with the BL Manager and gets the questions
        /// </summary>
        /// <param name="SelectedQuestionIds">Questions that are in test draft panel</param>
        /// <param name="PageNumber">Page number to load in to the 
        /// adaptive grid.</param>
        private void BindAdaptiveQuestions(string SelectedQuestionIds, int PageNumber)
        {
            if (SelectedQuestionIds == "")
            {
                CreateInterviewTestWithAdaptive_adaptiveGridView.DataSource = null;
                CreateInterviewTestWithAdaptive_adaptiveGridView.DataBind();
                CreateInterviewTestWithAdaptive_adaptiveGridView.Visible = false;
                return;
            }
            int TotalNoOfRecords = 0;
            CreateInterviewTestWithAdaptive_adaptiveGridView.DataSource =
                new InterviewBLManager().GetAdaptiveInterviewQuestions(base.tenantID,
                base.userID,
                SelectedQuestionIds, ADAPTIVE_PAGE_SIZE, PageNumber,
                base.userID, out TotalNoOfRecords);
            CreateInterviewTestWithAdaptive_adaptiveGridView.DataBind();
            if (TotalNoOfRecords == 0)
            {
                if (CreateInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList.SelectedIndex == 0)
                    CreateInterviewTestWithAdaptive_adaptiveGridView.Visible = true;
                else
                    CreateInterviewTestWithAdaptive_adaptiveGridView.Visible = false;
                //CreateInterviewTestWithAdaptive_adaptiveQuestionDiv.Visible = false;
                CreateInterviewTestWithAdaptive_adaptivebottomPagingNavigator.TotalRecords = 0;
                CreateInterviewTestWithAdaptive_adaptivebottomPagingNavigator.Reset();
                CreateInterviewTestWithAdaptive_adaptivePagingHiddenField.Value = "1";
            }
            else
            {
                CreateInterviewTestWithAdaptive_adaptiveGridView.Visible = true;
                ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE] =
                    (List<QuestionDetail>)CreateInterviewTestWithAdaptive_adaptiveGridView.DataSource;
                CreateInterviewTestWithAdaptive_adaptivebottomPagingNavigator.Visible = true;
                CreateInterviewTestWithAdaptive_adaptivePagingHiddenField.Value = PageNumber.ToString();
                CreateInterviewTestWithAdaptive_adaptiveQuestionDiv.Visible = true;
                if (PageNumber == 1)
                    CreateInterviewTestWithAdaptive_adaptivebottomPagingNavigator.Reset();
                else
                    CreateInterviewTestWithAdaptive_adaptivebottomPagingNavigator.MoveToPage(PageNumber);
                CreateInterviewTestWithAdaptive_adaptivebottomPagingNavigator.TotalRecords = TotalNoOfRecords;
                CreateInterviewTestWithAdaptive_adaptivebottomPagingNavigator.PageSize = ADAPTIVE_PAGE_SIZE;
            }
        }

        #endregion Adaptive Question Recommendation Methods

        #endregion

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool isQuestionTabActive = false;


            TestDetail testDetail = new TestDetail();
            testDetail = CreateInterviewTestWithAdaptive_testDetailsUserControl.TestDetailDataSource;
            int recommendedTime = testDetail.RecommendedCompletionTime;
            CreateInterviewTestWithAdaptive_topErrorMessageLabel.Text = string.Empty;
            CreateInterviewTestWithAdaptive_bottomErrorMessageLabel.Text = string.Empty;

            questionDetailTestDraftResultList = new List<QuestionDetail>();
            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailTestDraftResultList))
            {
                isQuestionTabActive = true;
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                    CreateInterviewTestWithAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualInterviewTest_QuestionCount);
                isValidData = false;

            }
            else if (questionDetailTestDraftResultList.Count == 0)
            {
                isQuestionTabActive = true;
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                    CreateInterviewTestWithAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualInterviewTest_QuestionCount);
                isValidData = false;
            }


            if (testDetail.Name.Trim().Length == 0)
            {
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel,
                   "Please enter interview name");
                isValidData = false;
            }

            if (testDetail.Description.Trim().Length == 0)
            {
                base.ShowMessage(CreateInterviewTestWithAdaptive_topErrorMessageLabel,
                   CreateInterviewTestWithAdaptive_bottomErrorMessageLabel,
                   "Please enter interview description");
                isValidData = false;
            }

            if (isQuestionTabActive)
                CreateInterviewTestWithAdaptive_mainTabContainer.ActiveTab = CreateInterviewTestWithAdaptive_questionsTabPanel;
            else
                CreateInterviewTestWithAdaptive_mainTabContainer.ActiveTab = CreateInterviewTestWithAdaptive_testdetailsTabPanel;

            CreateInterviewTestWithAdaptive_testDetailsUserControl.TestDetailDataSource = testDetail;
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindTestArea();
            BindComplexities();
            CreateInterviewTestWithAdaptive_simpleSearchDiv.Visible = true;
            CreateInterviewTestWithAdaptive_advanceSearchDiv.Visible = false;

            CreateInterviewTestWithAdaptive_testSummaryControl.AverageTimeTakenByCandidatesLabel = false;
            CreateInterviewTestWithAdaptive_testSummaryControl.AverageTimeTakenByCandidatesText = false;

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTDIRECTIONKEY"]))
                ViewState["SORTDIRECTIONKEY"] = "A";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTEXPRESSION"]))
                ViewState["SORTEXPRESSION"] = "QUESTIONKEY";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                ViewState["PAGENUMBER"] = "1";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
                ViewState["SEARCHRESULTGRID"] = null;
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["mode"]))
                sQryString = Request.QueryString["mode"].ToUpper();

            CreateInterviewTestWithAdaptive_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                + CreateInterviewTestWithAdaptive_dummyAuthorID.ClientID + "','"
                + CreateInterviewTestWithAdaptive_authorIdHiddenField.ClientID + "','"
                + CreateInterviewTestWithAdaptive_authorTextBox.ClientID + "','QA')");

            CreateManulTest_positionProfileImageButton.Attributes.Add("onclick",
                    "return ShowClientRequestForManual('" + CreateManulTest_positionProfileTextBox.ClientID + "','" +
                    CreateManulTest_positionProfileKeywordTextBox.ClientID + "','" +
                     CreateInterviewTestWithAdaptive_positionProfileHiddenField.ClientID + "')");

            CreateInterviewTestWithAdaptive_testDrftGridView.DataSource = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>; ;
            CreateInterviewTestWithAdaptive_testDrftGridView.DataBind();
        }
        protected void CreateInterviewTestWithAdaptive_addQuestioCloseLinkButton_Click(object sender, EventArgs e)
        {
            ResetQuestionImage();
        }
        protected void CreateInterviewTestWithAdaptive_addQuestionTopCancelImageButton_Click(object sender, ImageClickEventArgs e)
        {
            ResetQuestionImage();
        }
        #endregion Protected Overridden Methods


       
}
}
