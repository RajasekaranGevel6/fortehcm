﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Add_InterviewTestQuestionRatings.aspx.cs
// File that represents the user interface for displaying test detail.
// This will also used to show the question detail against the test key.

#endregion Header

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Web.UI;
using Forte.HCM.UI.CommonControls;
using Forte.HCM.ExternalService;
using System.Data;

#endregion Directives

namespace Forte.HCM.UI.InterviewTestMaker
{
    public partial class AddInterviewTestQuestionRatings : PageBase
    {
        int _weightAgePercentage = 0;
        int _ToAddExtraWeightage=0;
        int _questionsCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set page title
            Master.SetPageCaption(Resources.HCMResource.ReviewCreateAutomaticInterviewTest_Title);
           if(!IsPostBack)
            LoadValues();
        }

        /// <summary>
        /// This event handler helps to rebind the grid with sorted data
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> holds the event data.
        /// </param>
        protected void Add_InterviewTestQuestionRatings_groupByDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                        // Call a method by passing testkey and selected sortexpression field
                        //BindTestQuestions(Request.QueryString["testkey"].ToString().Trim(),
                        //Add_InterviewTestQuestionRatings_groupByDDL.SelectedValue);
            }
            catch (Exception exp)
            {
                        //Logger.ExceptionLog(exp);
                        //base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                        //Add_InterviewTestQuestionRatings_bottomErrorMessageLabel, exp.Message);
                Response.Write(exp);
            }
        }

        /// <summary>
        /// This event handler helps to find a question number label 
        /// to bind the row number as question serial no(Eg: 1,2...)
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that contains the sender of the event
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> object which holds the event data.
        /// </param>
        protected void Add_InterviewTestQuestionRatings_testDrftGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                // Find the row type is DataRow
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Find a label from the current row.
                    Label Add_InterviewTestQuestionRatings_rowNoLabel = (Label)e.Row.FindControl("Add_InterviewTestQuestionRatings_rowNoLabel");
                    // Assign the row no to the label.
                    Add_InterviewTestQuestionRatings_rowNoLabel.Text = (e.Row.RowIndex + 1).ToString();
                    // Display the image if any
                    QuestionDetail questionDetail = ((QuestionDetail)e.Row.DataItem);
                    Image Add_InterviewTestQuestionRatings_questionImageDisplay = (Image)e.Row.FindControl("Add_InterviewTestQuestionRatings_questionImageDisplay");
                    if (((QuestionDetail)e.Row.DataItem).HasImage)
                    {
                        Add_InterviewTestQuestionRatings_questionImageDisplay.Visible = true;
                        Add_InterviewTestQuestionRatings_questionImageDisplay.ImageUrl
                            = @"~/Common/ImageHandler.ashx?questionKey="
                                + questionDetail.QuestionKey
                                + "&isThumb=0&source=VIEW_INTERVIEW_QUESTION_IMAGE";
                    }
                    else
                    {
                        Add_InterviewTestQuestionRatings_questionImageDisplay.Visible = false;
                    }

                    TextBox Weightage = (TextBox)e.Row.FindControl("Add_InterviewTestQuestionRatings_questionnWeightageGridViewTextbox");
                    if (e.Row.RowIndex + 1 == _questionsCount)
                        Weightage.Text = (_weightAgePercentage + _ToAddExtraWeightage).ToString();
                    else
                        Weightage.Text = _weightAgePercentage.ToString();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                    Add_InterviewTestQuestionRatings_bottomErrorMessageLabel, exception.Message);

            }
        }

        /// <summary>
        /// Handles the Click event of the Add_InterviewTestQuestionRatings_questionTimetakenApplyToAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Add_InterviewTestQuestionRatings_questionTimetakenApplyToAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Add_InterviewTestQuestionRatings_topErrorMessageLabel.Text = string.Empty;
                Add_InterviewTestQuestionRatings_bottomErrorMessageLabel.Text = string.Empty;

                int timeLimitValidation = 0;

                if (Add_InterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox.Text != "")
                {
                    if (!Forte.HCM.Support.Utility.VerifyMinutesSeconds(
                           Add_InterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox.Text.Trim()))
                    {
                        base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                            Add_InterviewTestQuestionRatings_bottomErrorMessageLabel,
                           "Time limit is invalid");
                        return;
                    }
                    timeLimitValidation = Convert.ToInt32(Forte.HCM.Support.Utility.ConvertMinutesSecondsToSeconds(Add_InterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox.Text));

                    if (timeLimitValidation <= 0)
                    {                        
                        base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                          Add_InterviewTestQuestionRatings_bottomErrorMessageLabel, "Time limit should be greater than 0");
                        return;
                    }
                    else if (timeLimitValidation > 300)
                    {
                        base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                              Add_InterviewTestQuestionRatings_bottomErrorMessageLabel, "Time limit should be less than 5 minutes");
                        return;
                    }
                }
                else {
                    base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                              Add_InterviewTestQuestionRatings_bottomErrorMessageLabel, "Please enter time limit for all the questions");
                  return;
               
                }

                for (int i = 0; i < Add_InterviewTestQuestionRatings_testDrftGridView.Rows.Count; i++)
                {
                    if (Add_InterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox.Text.Length == 0)
                    {
                        base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                        Add_InterviewTestQuestionRatings_bottomErrorMessageLabel, "Please enter time limit for all the questions");                    
                    }
                    else
                    {
                        TextBox Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox =
                            (TextBox)Add_InterviewTestQuestionRatings_testDrftGridView.Rows[i].FindControl("Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox");
                        Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox.Text = Add_InterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox.Text;
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                    Add_InterviewTestQuestionRatings_bottomErrorMessageLabel, exception.Message);

            }
        }

        /// <summary>
        /// Handles the Click event of the Add_InterviewTestQuestionRatings_questionTimetakenApplyToAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Add_InterviewTestQuestionRatings_questionRatingsApplyToAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Add_InterviewTestQuestionRatings_topErrorMessageLabel.Text = string.Empty;
                Add_InterviewTestQuestionRatings_bottomErrorMessageLabel.Text = string.Empty;

                    if (Add_InterviewTestQuestionRatings_questionRatingsApplyToAllDropDownList.SelectedItem.Text == "0")
                    {
                        base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                        Add_InterviewTestQuestionRatings_bottomErrorMessageLabel, "Please select ratings for all the questions ");
                        return;
                    }

                for (int i = 0; i < Add_InterviewTestQuestionRatings_testDrftGridView.Rows.Count; i++)
                {
                   DropDownList Add_InterviewTestQuestionRatings_questionRatingsGridViewDropDownList =
                          (DropDownList)Add_InterviewTestQuestionRatings_testDrftGridView.Rows[i].FindControl("Add_InterviewTestQuestionRatings_questionRatingsGridViewDropDownList");
                   Add_InterviewTestQuestionRatings_questionRatingsGridViewDropDownList.SelectedValue = Add_InterviewTestQuestionRatings_questionRatingsApplyToAllDropDownList.SelectedValue;
                   
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                    Add_InterviewTestQuestionRatings_bottomErrorMessageLabel, exception.Message);

            }
        }

        /// <summary>
        /// Handles the Click event of the Add_InterviewTestQuestionRatings_questionTimetakenApplyToAllLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Add_InterviewTestQuestionRatings_questionCommentsApplyToAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < Add_InterviewTestQuestionRatings_testDrftGridView.Rows.Count; i++)
                {
                    if (Add_InterviewTestQuestionRatings_questionCommentsApplyToAllTextbox.Text.Length == 0)
                    {
                        Add_InterviewTestQuestionRatings_topErrorMessageLabel.Text = string.Empty;
                        Add_InterviewTestQuestionRatings_bottomErrorMessageLabel.Text = string.Empty;

                        base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                        Add_InterviewTestQuestionRatings_bottomErrorMessageLabel, "Enter comments to apply for all the questions");
                        return;
                    }
                    else
                    {
                        Add_InterviewTestQuestionRatings_topErrorMessageLabel.Text = string.Empty;
                        Add_InterviewTestQuestionRatings_bottomErrorMessageLabel.Text = string.Empty;
                            
                        TextBox Add_InterviewTestQuestionRatings_questionCommentsGridViewTextbox =
                            (TextBox)Add_InterviewTestQuestionRatings_testDrftGridView.Rows[i].FindControl("Add_InterviewTestQuestionRatings_questionCommentsGridViewTextbox");
                        Add_InterviewTestQuestionRatings_questionCommentsGridViewTextbox.Text = Add_InterviewTestQuestionRatings_questionCommentsApplyToAllTextbox.Text;
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                    Add_InterviewTestQuestionRatings_bottomErrorMessageLabel, exception.Message);

            }
        }

        private bool GetWeightageValue() 
        {
            int _numWeightPercentage = 0;
            for (int i = 0; i < Add_InterviewTestQuestionRatings_testDrftGridView.Rows.Count; i++)
            {
                TextBox WeightAge_Textbox =
                    (TextBox)Add_InterviewTestQuestionRatings_testDrftGridView.Rows[i].FindControl("Add_InterviewTestQuestionRatings_questionnWeightageGridViewTextbox");

                if (WeightAge_Textbox != null)
                {
                    if (Utility.IsNullOrEmpty(WeightAge_Textbox.Text))
                    {
                        base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                            Add_InterviewTestQuestionRatings_bottomErrorMessageLabel,
                            string.Format("Weightage cannot be empty for  question {0}", i + 1));

                        return false;
                    }
                    _numWeightPercentage += Convert.ToInt32(WeightAge_Textbox.Text);
                }
            }
            if (_numWeightPercentage < 100 || _numWeightPercentage > 100)
                return false;
            else
                return true;
        }

        protected void Add_InterviewTestQuestionRatings_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                InterviewDetail testDetail = null;
                testDetail = new InterviewDetail();

                if (Session["interviewDetail"] != null)
                {
                    testDetail = Session["interviewDetail"] as InterviewDetail;
                    Add_InterviewTestQuestionRatings_testDetailsUserControl.TestDetailDataSource = testDetail;
                }

                if (testDetail.InterviewTestKey != "")
                    return;

                if (Add_InterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox.Text.Trim() != "")
                {
                    if (!Forte.HCM.Support.Utility.VerifyMinutesSeconds(
                                   Add_InterviewTestQuestionRatings_questionTimetakenApplyToAllTextbox.Text.Trim()))
                    {
                        base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                          Add_InterviewTestQuestionRatings_bottomErrorMessageLabel,
                           "Verify the time limit");
                        return;
                    }
                }

                //Verifying the weightage
                if (!GetWeightageValue())
                {
                    Add_InterviewTestQuestionRatings_topErrorMessageLabel.Text = "";
                    Add_InterviewTestQuestionRatings_bottomErrorMessageLabel.Text = "";
                    base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                         Add_InterviewTestQuestionRatings_bottomErrorMessageLabel,
                          "Questions weightage should be equal to 100");

                    return;
                }

                for (int i = 0; i < Add_InterviewTestQuestionRatings_testDrftGridView.Rows.Count; i++)
                {

                    TextBox Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox =
                        (TextBox)Add_InterviewTestQuestionRatings_testDrftGridView.Rows[i].FindControl("Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox");

                    if (Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox != null)
                    {
                        if (Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox.Text == "")
                        {
                            Add_InterviewTestQuestionRatings_topErrorMessageLabel.Text = string.Empty;
                            Add_InterviewTestQuestionRatings_bottomErrorMessageLabel.Text = string.Empty;

                            base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                                Add_InterviewTestQuestionRatings_bottomErrorMessageLabel,
                                string.Concat("Please enter time limit for question ", (i + 1)));
                            Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox.Text = "";
                            return;
                        }
                        else
                        {
                            if (!Forte.HCM.Support.Utility.VerifyMinutesSeconds(
                                Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox.Text.Trim()))
                            {
                                base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                                  Add_InterviewTestQuestionRatings_bottomErrorMessageLabel,
                                  string.Concat("Time limit is invalid for question ", (i + 1)));
                                return;
                            }
                            int timeLimitValidation = 0;
                            timeLimitValidation = Convert.ToInt32(Forte.HCM.Support.Utility.ConvertMinutesSecondsToSeconds(Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox.Text));

                            Add_InterviewTestQuestionRatings_topErrorMessageLabel.Text = string.Empty;
                            Add_InterviewTestQuestionRatings_bottomErrorMessageLabel.Text = string.Empty;

                            if (timeLimitValidation <= 0)
                            {
                                base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                                  Add_InterviewTestQuestionRatings_bottomErrorMessageLabel,
                                  string.Concat("Time limit should be greater than 0 for question ", (i + 1)));
                                return;
                            }
                            else if (timeLimitValidation > 300)
                            {
                                base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                                      Add_InterviewTestQuestionRatings_bottomErrorMessageLabel,
                                      string.Concat("Time limit should be less than 5 minutes for question ", (i + 1)));
                                Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox.Text = "";
                                return;
                            }
                        }
                    }

                    DropDownList Add_InterviewTestQuestionRatings_questionRatingsGridViewDropDownList =
                        (DropDownList)Add_InterviewTestQuestionRatings_testDrftGridView.Rows[i].FindControl("Add_InterviewTestQuestionRatings_questionRatingsGridViewDropDownList");
                    if (Add_InterviewTestQuestionRatings_questionRatingsGridViewDropDownList != null)
                    {
                        if (Add_InterviewTestQuestionRatings_questionRatingsGridViewDropDownList.SelectedValue == "0")
                        {
                            Add_InterviewTestQuestionRatings_topErrorMessageLabel.Text = string.Empty;
                            Add_InterviewTestQuestionRatings_bottomErrorMessageLabel.Text = string.Empty;

                            base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                                Add_InterviewTestQuestionRatings_bottomErrorMessageLabel,
                                string.Concat("Please select ratings for question ", (i + 1)));
                            return;

                        }
                    }
                }

                //Here is the logic for calculate weight
                DataTable dtTestSearchCriteria = null;
                dtTestSearchCriteria = BuildTableFromGridView();
                int TotalWeightage = 0;
                LoadWeightages(ref dtTestSearchCriteria, dtTestSearchCriteria.Rows.Count, out TotalWeightage);
                int index = 0;
                foreach (GridViewRow gridViewRow in Add_InterviewTestQuestionRatings_testDrftGridView.Rows)
                {
                    TextBox Weightage = (TextBox)gridViewRow.FindControl("Add_InterviewTestQuestionRatings_questionnWeightageGridViewTextbox");
                    Weightage.Text = dtTestSearchCriteria.Rows[index]["Weightage"].ToString();
                    index++;
                }

                List<QuestionDetail> questionDetails = new List<QuestionDetail>();
                //if (Session["questionDetailTestDraftResultList"] != null)
                //    questionDetails = Session["questionDetailTestDraftResultList"] as List<QuestionDetail>;

                TestDetail interviewIestDetail = null;
                interviewIestDetail = new TestDetail();

                if (Session["controlTestDetail"] != null)
                    interviewIestDetail = (TestDetail)Session["controlTestDetail"];

                questionDetails = ConstructQuestionInformation();

                // Save the test.
                interviewIestDetail.TestKey = new InterviewBLManager().
                    SaveInterviewTestWithRatings(interviewIestDetail, userID, new ControlUtility().
                    GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetails).AttributeID.ToString(), questionDetails);

                if (Session["isCopyInterview"] != null && Session["isCopyInterview"].ToString().ToUpper() == "Y")
                {
                    // Check if the new saved interview is equal to the parent interview based question.
                    // If yes update the copy and parent interview ID status.
                    new InterviewBLManager().UpdateCopyInterviewTestStatus(testDetail.ParentInterviewKey, interviewIestDetail.TestKey);
                }

                // CreaetAutomaticTest_byQuestion_testKeyHiddenField.Value = testDetail.TestKey;

                // Show a success message.
                Add_InterviewTestQuestionRatings_topErrorMessageLabel.Text = string.Empty;
                Add_InterviewTestQuestionRatings_bottomErrorMessageLabel.Text = string.Empty;
                Add_InterviewTestQuestionRatings_topSuccessMessageLabel.Text = string.Empty;
                Add_InterviewTestQuestionRatings_bottomSuccessMessageLabel.Text = string.Empty;

                if (Session["isCopyInterview"] !=null && Session["isCopyInterview"].ToString().ToUpper() == "Y")
                {
                    base.ShowMessage(Add_InterviewTestQuestionRatings_topSuccessMessageLabel,
                       Add_InterviewTestQuestionRatings_bottomSuccessMessageLabel,
                       string.Format(Resources.HCMResource.EditManualInterviewTest_CopySuccessfully,
                       testDetail.ParentInterviewKey));
                }
                else
                {
                    base.ShowMessage(Add_InterviewTestQuestionRatings_topSuccessMessageLabel,
                        Add_InterviewTestQuestionRatings_bottomSuccessMessageLabel,
                        string.Format(Resources.HCMResource.CreateAutomaticInterviewTest_TestSuccessfullMessage,
                        interviewIestDetail.TestKey));
                }
                Session["isCopyInterview"] = string.Empty;

                testDetail.InterviewTestKey = interviewIestDetail.TestKey;

                Add_InterviewTestQuestionRatings_topCreateSessionButton.Visible = true;
                Add_InterviewTestQuestionRatings_bottomCreateSessionButton.Visible = true;

                Add_InterviewTestQuestionRatings_topSaveButton.Visible = false;
                Add_InterviewTestQuestionRatings_bottomSaveButton.Visible = false;


                Add_InterviewTestQuestionRatings_interviewTestKeyHiddenField.Value = interviewIestDetail.TestKey;
                Add_InterviewTestQuestionRatings_testDetailsUserControl.TestDetailDataSource = testDetail;

                // CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl.TestDetailDataSource = testDetail;
                // Send a mail to the position profile creator.
                try
                {
                    if (interviewIestDetail.PositionProfileID != 0)
                    {
                        new EmailHandler().SendMail(EntityType.InterviewCreatedForPositionProfile, interviewIestDetail.TestKey);
                    }
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                    Add_InterviewTestQuestionRatings_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the create interview session button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Add_InterviewTestQuestionRatings_createSessionButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("CreateInterviewTestSession.aspx?m=1&s=2&parentpage=" +
                Constants.ParentPage.SEARCH_INTERVIEW + 
                "&interviewtestkey=" + Add_InterviewTestQuestionRatings_interviewTestKeyHiddenField.Value, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                    Add_InterviewTestQuestionRatings_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This method allocates weightages to the segements
        /// and ensures that the total weightage is 100% or not.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        /// <param name="TotalWeightage">Total weightage allocated by this method 
        /// (Note:- it should be out parametarized)</param>
        private void LoadWeightages(ref DataTable dtSearchCriteria, int NoofQuestions, out int TotalWeightage)
        {
            DataRow[] drWeightage = null;
            int GivenWeightage = 0;
            int SearchSegements = 0;
            int WeightagePercentage = 0;
            int LoopI = 0;
            int ToAddExtraWeightage = 0;
            try
            {
                SearchSegements = dtSearchCriteria.Rows.Count;
                drWeightage = dtSearchCriteria.Select("Weightage > 0");
                for (LoopI = 1; LoopI <= drWeightage.Length; LoopI++)
                    GivenWeightage += Convert.ToInt32(drWeightage[LoopI - 1]["Weightage"]);
                if ((GivenWeightage > 100) || ((GivenWeightage == 100) && (SearchSegements >= LoopI)))
                {
                    TotalWeightage = 101;
                    base.ShowMessage(Add_InterviewTestQuestionRatings_topErrorMessageLabel,
                         Add_InterviewTestQuestionRatings_bottomErrorMessageLabel,
                         Resources.HCMResource.CreateAutomaticInterviewTest_WeightageExceed);
                    return;
                }
                if (GivenWeightage == 0)
                    WeightagePercentage = 100 / SearchSegements;
                else if (GivenWeightage < 100)
                {
                    if (SearchSegements == drWeightage.Length)
                        dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"] = 100 -
                            (GivenWeightage - Convert.ToInt32(dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"]));
                    else
                        WeightagePercentage = (100 - GivenWeightage) / (SearchSegements - drWeightage.Length);
                }
                ToAddExtraWeightage = 100 - ((WeightagePercentage * (SearchSegements - (LoopI - 1))) + GivenWeightage);
                for (LoopI = 0; LoopI < SearchSegements; LoopI++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[LoopI]["Weightage"]))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] = WeightagePercentage;
                    if ((ToAddExtraWeightage > 0) && (WeightagePercentage != 0) && (LoopI == SearchSegements - 1))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["Weightage"]) + ToAddExtraWeightage;
                }
                TotalWeightage = 100;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drWeightage)) drWeightage = null;
            }
        }


        /// <summary>
        /// Builds data table from test segment grid view
        /// </summary>
        /// <returns>Data table that contains user given input for test segment grid</returns>
        private DataTable BuildTableFromGridView()
        {
            DataTable dttestDrft = null;
            try
            {
                dttestDrft = new DataTable();
                AddTestSegmentColumns(ref dttestDrft);

                DataRow drNewRow = null;
                int Weightage = 0;
                int SNo = 1;
                foreach (GridViewRow gridViewRow in Add_InterviewTestQuestionRatings_testDrftGridView.Rows)
                {
                    drNewRow = dttestDrft.NewRow();
                    drNewRow["SNO"] = SNo++.ToString();

                    int.TryParse(((TextBox)gridViewRow.
                        FindControl("Add_InterviewTestQuestionRatings_questionnWeightageGridViewTextbox")).Text, out Weightage);
                    if (Weightage > 0)
                        drNewRow["Weightage"] = Weightage;


                    dttestDrft.Rows.Add(drNewRow);
                    drNewRow = null;
                }
                return dttestDrft;
            }
            finally
            {
                if (dttestDrft != null) dttestDrft = null;
            }
        }

        /// <summary>
        /// Creates a Table structure for serach criteria
        /// </summary>
        /// <param name="dtTestSegmentGridColumns">Data table object to add columns (use ref: key word)</param>
        private void AddTestSegmentColumns(ref DataTable dtTestSegmentGridColumns)
        {
            dtTestSegmentGridColumns = new DataTable();
            dtTestSegmentGridColumns.Columns.Add("SNO", typeof(int));
            dtTestSegmentGridColumns.Columns.Add("Weightage", typeof(int));          
        }

        /// <summary>
        /// Constructs the question information.
        /// </summary>
        /// <returns></returns>
        private List<QuestionDetail> ConstructQuestionInformation()
        {
            // Construct question detail object.     
            List<QuestionDetail> qd = new List<QuestionDetail>();
            List<QuestionDetail> questionDetails = new List<QuestionDetail>();

            if (Session["questionDetailTestDraftResultList"] != null)
                questionDetails = Session["questionDetailTestDraftResultList"] as List<QuestionDetail>;

            for (int i = 0; i < Add_InterviewTestQuestionRatings_testDrftGridView.Rows.Count; i++)
            {

                QuestionDetail questionDetail = new QuestionDetail();
                questionDetail = questionDetails[i];

                TextBox Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox = (TextBox)Add_InterviewTestQuestionRatings_testDrftGridView.Rows[i].FindControl("Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox");

                if(Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox!=null)
                    questionDetail.TimeLimit = Convert.ToInt32(Forte.HCM.Support.Utility.ConvertMinutesSecondsToSeconds(Add_InterviewTestQuestionRatings_questionTimetakenGridViewTextbox.Text));

                DropDownList Add_InterviewTestQuestionRatings_questionRatingsGridViewDropDownList = (DropDownList)Add_InterviewTestQuestionRatings_testDrftGridView.Rows[i].FindControl("Add_InterviewTestQuestionRatings_questionRatingsGridViewDropDownList");
                questionDetail.InterviewTestRatings = Convert.ToInt32(Add_InterviewTestQuestionRatings_questionRatingsGridViewDropDownList.SelectedValue);

                TextBox Add_InterviewTestQuestionRatings_questionnWeightageGridViewTextbox = (TextBox)Add_InterviewTestQuestionRatings_testDrftGridView.Rows[i].FindControl("Add_InterviewTestQuestionRatings_questionnWeightageGridViewTextbox");
                if (Add_InterviewTestQuestionRatings_questionnWeightageGridViewTextbox != null)
                    questionDetail.Weightage = Convert.ToInt32(Add_InterviewTestQuestionRatings_questionnWeightageGridViewTextbox.Text);

                TextBox Add_InterviewTestQuestionRatings_questionCommentsGridViewTextbox = (TextBox)Add_InterviewTestQuestionRatings_testDrftGridView.Rows[i].FindControl("Add_InterviewTestQuestionRatings_questionCommentsGridViewTextbox");
                if (Add_InterviewTestQuestionRatings_questionCommentsGridViewTextbox != null)
                    questionDetail.InterviewTestQuestionComments = Add_InterviewTestQuestionRatings_questionCommentsGridViewTextbox.Text; 
                qd.Add(questionDetail);
            }

            return qd;
        }      



        #region Protected Overidden Methods

        /// <summary>
        /// Method which loads the default values and settings.
        /// </summary>
        protected override void LoadValues()
        {

            // Set default button
            Page.Form.DefaultButton = Add_InterviewTestQuestionRatings_topCancelLinkButton.UniqueID;

            Add_InterviewTestQuestionRatings_topCreateSessionButton.Visible = false;
            Add_InterviewTestQuestionRatings_bottomCreateSessionButton.Visible = false;

            // Clear label messages
            Add_InterviewTestQuestionRatings_topSuccessMessageLabel.Text = string.Empty;
            // Add_InterviewTestQuestionRatings_bottomSuccessMessageLabel.Text = string.Empty;
            InterviewDetail testDetail = null;
            testDetail = new InterviewDetail();          
       
            if (Session["interviewDetail"] != null)
            {
                testDetail = Session["interviewDetail"] as InterviewDetail;
                Add_InterviewTestQuestionRatings_testDetailsUserControl.TestDetailDataSource = testDetail;
            }

            List<QuestionDetail> questionDetails = new List<QuestionDetail>();

            if (Session["questionDetailTestDraftResultList"] != null)
                questionDetails = Session["questionDetailTestDraftResultList"] as List<QuestionDetail>;

               
            // Bind gridview
            _questionsCount = questionDetails.Count;
            _weightAgePercentage = Convert.ToInt32(100 / questionDetails.Count);
            _ToAddExtraWeightage = 100 - (_weightAgePercentage * questionDetails.Count);
 

            Add_InterviewTestQuestionRatings_testDrftGridView.DataSource = questionDetails;
            Add_InterviewTestQuestionRatings_testDrftGridView.DataBind();
           
            
        }


        /// <summary>
        /// Helps to validate the input data if it is provided.
        /// </summary>
        /// <returns></returns>
        protected override bool IsValidData()
        { return false; }

        #endregion Protected Overidden Methods

    }
}