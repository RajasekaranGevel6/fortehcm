<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateAutomaticInterviewTest.aspx.cs"
    Inherits="Forte.HCM.UI.InterviewTestMaker.CreateAutomaticInterviewTest" MasterPageFile="~/MasterPages/InterviewMaster.Master" %>

<%@ Register Src="~/CommonControls/InterviewQuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/AutomatedInterviewSummaryControl.ascx" TagName="AutomatedTestControl"
    TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/InterviewTestDetailsControl.ascx" TagName="TestDetailsControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc7" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc8" %>
<%-- <%@ Register Src="~/CommonControls/InterviewQuestionReviewConfirmControl.ascx" TagName="ReviewConfirmMsgControl"
    TagPrefix="uc9" %>--%>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ContentPlaceHolderID="InterviewMaster_body" ID="CreateAutomaticInterviewTest_bodyContent"
    runat="server">
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">

        var control;
        function setFocus(sender, e) {
            try {
                var activeTab = $find('<%=CreateAutomaticInterviewTest_mainTabContainer.ClientID%>').get_activeTabIndex();
                if (activeTab == "0") {
                    try {
                        control = $get('<%=CreateAutomaticInterviewTest_byQuestion_totalNumberTextBox.ClientID%>');
                        control.focus();
                    }
                    catch (er1) {

                    }
                }
                else {
                    control = $get('ctl00_OTMMaster_body_CreateAutomaticInterviewTest_mainTabContainer_CreateManualTest_testdetailsTabPanel_CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl_TestDetailsControl_testNameTextBox');
                    control.focus();
                }
            }
            catch (er) {
            }
        }

        function ShowAddImagePanel() {
            var addImageTR = document.getElementById('<%=CreateAutomaticInterviewTest_AddQuestionImageTR.ClientID%>');
            addImageTR.style.display = "";
            var addImageBtn = document.getElementById('<%=CreateAutomaticInterviewTest_addImageLinkButton.ClientID%>');
            addImageBtn.style.display = "none";
            return false;
        }

    </script>
    <script type="text/javascript" language="javascript">

        // Expand or Collapse question details in gridview
        function ExpORCollapseRows(targetControl) {
            var ctrl = document.getElementById('<%=CreateAutomaticInterviewTest_byQuestion_stateHiddenField.ClientID %>');
            targetValue = ctrl.value;
            if (targetValue == "0") {
                targetControl.innerHTML = "Collapse All";
                ExpandAllRows(targetControl);
                ctrl.value = 1;
            }
            else {
                targetControl.innerHTML = "Expand All";
                CollapseAllRows(targetControl);
                ctrl.value = 0;
            }

            return false;
        }

        // Expands all question details in test draft gridview
        function ExpandAllRows(targetControl) {
            var gridCtrl = document.getElementById("<%= CreateAutomaticInterviewTest_byQuestion_testDraftGridView.ClientID %>");
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (rowItems[indexRow].id.indexOf("CreateAutomaticInterviewTest_byQuestion_detailsDiv") != "-1") {
                        rowItems[indexRow].style.display = "block";
                    }
                }
            }
            return false;
        }

        // Collpase all question details in test draft gridview
        function CollapseAllRows(targetControl) {
            var gridCtrl = document.getElementById("<%= CreateAutomaticInterviewTest_byQuestion_testDraftGridView.ClientID %>");
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (rowItems[indexRow].id.indexOf("CreateAutomaticInterviewTest_byQuestion_detailsDiv") != "-1") {
                        rowItems[indexRow].style.display = "none";
                    }
                }
            }
            return false;
        }

        function ShowZoomedChart(sessionName) {
            var height = 620;
            var width = 950;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../Popup/ZoomedChart.aspx?sessionName=" + sessionName;
            //window.open(queryStringValue, window.self, sModalFeature);
            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }
    </script>
    <asp:Panel ID="CreateAutomaticText_mainPanel" runat="server" DefaultButton="CreateAutomaticInterviewTest_bottomSaveButton">
        <table width="100%" border="0" cellspacing="3" cellpadding="0">
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="72%" class="header_text_bold">
                                <asp:Literal ID="CreateAutomaticInterviewTest_headerLiteral" runat="server" Text="Create Automated Interview"></asp:Literal>
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td width="63%" align="right">
                                            <asp:Button ID="CreateAutomaticInterviewTest_topCreateSessionButton" runat="server"
                                                SkinID="sknButtonId" Text="Create Interview Session" OnClick="CreateAutomaticInterviewTest_CreateSessionButton_Click" />
                                            <asp:Button ID="CreateAutomaticInterviewTest_topSaveButton" runat="server" SkinID="sknButtonId"
                                                Text="Save" OnClick="CreateAutomaticInterviewTest_saveButton_Click" />
                                        </td>
                                        <td align="right" style="width: 15%">
                                            <asp:LinkButton ID="CreateAutomaticInterviewTest_topResetLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Reset" OnClick="CreateAutomaticInterviewTest_resetLinkButton_Click" />
                                        </td>
                                        <td width="4%" align="center">
                                            |
                                        </td>
                                        <td width="18%" align="left">
                                            <asp:LinkButton ID="CreateAutomaticInterviewTest_topCancelLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="CreateAutomaticInterviewTest_topMessagesUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="CreateAutomaticInterviewTest_topSuccessMessageLabel" runat="server"
                                SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="CreateAutomaticInterviewTest_topErrorMessageLabel" runat="server"
                                SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="CreateAutomaticInterviewTest_deletePanelUpdatepanel" runat="server">
                        <ContentTemplate>
                            <div style="display: none">
                                <asp:Button ID="CreateAutomaticInterviewTest_deleteInterviewQuestionHiddenButton"
                                    runat="server" />
                            </div>
                            <asp:Panel ID="CreateAutomaticInterviewTest_deleteInterviewQuestionsPopupPanel" runat="server"
                                Style="display: none" CssClass="popupcontrol_confirm_remove">
                                <uc3:ConfirmMsgControl ID="CreateAutomaticInterviewTest_deleteInterviewQuestionsConfirmMsgControl"
                                    runat="server" OnOkClick="CreateAutomaticInterviewTest_deleteInterviewQuestionsConfirmMsgControl_okClick"
                                    OnCancelClick="CreateAutomaticInterviewTest_deleteInterviewQuestionsConfirmMsgControl_cancelClick" />
                                <asp:HiddenField ID="CreateAutomaticInterviewTest_deleteHiddenField" runat="server" />
                            </asp:Panel>
                            <ajaxToolKit:ModalPopupExtender ID="CreateAutomaticInterviewTest_deleteInterviewQuestionsFormModalPopupExtender"
                                runat="server" PopupControlID="CreateAutomaticInterviewTest_deleteInterviewQuestionsPopupPanel"
                                TargetControlID="CreateAutomaticInterviewTest_deleteInterviewQuestionHiddenButton"
                                BackgroundCssClass="modalBackground">
                            </ajaxToolKit:ModalPopupExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <ajaxToolKit:TabContainer ID="CreateAutomaticInterviewTest_mainTabContainer" runat="server"
                        ActiveTabIndex="0" OnClientActiveTabChanged="setFocus">
                        <ajaxToolKit:TabPanel ID="CreateAutomaticInterviewTest_QuestionsTabPanel" HeaderText="questions"
                            runat="server">
                            <HeaderTemplate>
                                Questions
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:Panel ID="CreaetAutomaticTest_questionPanel" runat="server" DefaultButton="CreateAutomaticInterviewTest_byQuestion_generateTestButton">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td style="width: 100%" class="panel_bg">
                                                <asp:HiddenField ID="CreaetAutomaticTest_byQuestion_testKeyHiddenField" runat="server" />
                                                <div id="CreateAutomaticInterviewTest_byQuestion_serachDiv" runat="server" style="display: block;
                                                    width: 100%">
                                                    <asp:UpdatePanel ID="CreateAutomaticInterviewTest_byQuestion_searchCriteriaUpdatePanel"
                                                        runat="server">
                                                        <ContentTemplate>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="panel_inner_body_bg" style="width: 100%">
                                                                        <table width="100%" cellpadding="2" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td style="width: 12%">
                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_totalNumberLabel" runat="server"
                                                                                        SkinID="sknLabelFieldHeaderText" Text="Number Of Questions"></asp:Label><span class="mandatory">*</span>
                                                                                </td>
                                                                                <td style="width: 9%">
                                                                                    <asp:TextBox ID="CreateAutomaticInterviewTest_byQuestion_totalNumberTextBox" runat="server"
                                                                                        MaxLength="2" Width="25%"></asp:TextBox>
                                                                                    <ajaxToolKit:FilteredTextBoxExtender ID="CreateAutomaticInterviewTest_byQuestion_numberOfQuestionsFileteredExtender"
                                                                                        runat="server" TargetControlID="CreateAutomaticInterviewTest_byQuestion_totalNumberTextBox"
                                                                                        FilterType="Numbers">
                                                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                                                </td>
                                                                                <td style="width: 4%" align="center">
                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_totalCostLabel" runat="server"
                                                                                        SkinID="sknLabelFieldHeaderText" Text="Cost"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 10%">
                                                                                    <asp:TextBox ID="CreateAutomaticInterviewTest_byQuestion_totalCostTextBox" runat="server"></asp:TextBox>
                                                                                    <ajaxToolKit:FilteredTextBoxExtender ID="CreateAutomaticInterviewTest_byQuestion_totalCostFilteredTextBoxExtender"
                                                                                        runat="server" TargetControlID="CreateAutomaticInterviewTest_byQuestion_totalCostTextBox"
                                                                                        FilterType="Custom" ValidChars="1234567890.1234567890">
                                                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                                                    <asp:HiddenField ID="CreateAutomaticInterviewTest_byQuestion_generateTestHitCountHiddenField"
                                                                                        runat="server" Value="0" />
                                                                                </td>
                                                                                <td style="width: 14%" align="center">
                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_positionProfileLabel" runat="server"
                                                                                        SkinID="sknLabelFieldHeaderText" Text="Position Profile"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <div style="float: left; padding-right: 5px; width: 70%">
                                                                                        <asp:TextBox ID="CreateAutomaticInterviewTest_byQuestion_positionProfileTextBox"
                                                                                            MaxLength="50" ReadOnly="true" runat="server" Columns="38"></asp:TextBox>
                                                                                        <asp:HiddenField ID="CreateAutomaticInterviewTest_byQuestion_positionProfileTagsHiddenField"
                                                                                            runat="server" />
                                                                                        <asp:HiddenField ID="CreateAutomaticInterviewTest_byQuestion_positionProfileIDHiddenField"
                                                                                            runat="server" />
                                                                                    </div>
                                                                                    <div style="float: left; width: 20%;">
                                                                                        <table cellpadding="2" cellspacing="0" border="0" style="width: 100%">
                                                                                            <tr>
                                                                                                <td style="width: 40%">
                                                                                                    <asp:ImageButton ID="CreateAutomaticInterviewTest_byQuestion_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to select position profile" />
                                                                                                </td>
                                                                                                <td style="width: 60%">
                                                                                                    <asp:LinkButton ID="SearchCategorySubjectControl_addLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                                                        Text="Add" ToolTip="Click here to add position profile segments" OnClick="CreateAutomaticInterviewTest_byQuestion_addPositionProfileLinkButton_Click" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                                <td style="width: 4%">
                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_timeSearchLabel" runat="server"
                                                                                        SkinID="sknLabelFieldHeaderText" Text="Time"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 10%">
                                                                                    <asp:TextBox ID="CreateAutomaticInterviewTest_byQuestion_timeSearchTextbox" runat="server"
                                                                                        Columns="10"></asp:TextBox>
                                                                                    <ajaxToolKit:MaskedEditExtender ID="CreateAutomaticInterviewTest_byQuestion_timeSearchMaskedEditExtender"
                                                                                        runat="server" TargetControlID="CreateAutomaticInterviewTest_byQuestion_timeSearchTextbox"
                                                                                        Mask="99:99:99" MaskType="Time" ErrorTooltipEnabled="True" CultureAMPMPlaceholder=""
                                                                                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                                                                        CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                                                                        Enabled="True">
                                                                                    </ajaxToolKit:MaskedEditExtender>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_8">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="Td3" class="header_bg" runat="server">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 80%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="CreateAutomaticInterviewTest_byQuestion_testSegmentsLiteral" runat="server"
                                                                                        Text="Interview Segments"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%" class="grid_body_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="width: 100%; overflow: auto; display: block; background-color: Red">
                                                                                        <asp:GridView ID="CreateAutomaticInterviewTest_byQuestion_testSegmantGridView" runat="server"
                                                                                            AutoGenerateColumns="False" ShowHeader="False" SkinID="sknNewGridView" OnRowDataBound="CreateAutomaticInterviewTest_byQuestion_testSegmantGridView_RowDataBound"
                                                                                            AutoGenerateDeleteButton="false">
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <table width="100%" border="0" cellspacing="2" cellpadding="5">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:ImageButton ID="CreateAutomaticInterviewTest_byQuestion_testSegmantGridView_deleteImageButton"
                                                                                                                        runat="server" CommandName="Delete Segment" SkinID="sknDeleteImageButton" ToolTip="Delete segment"
                                                                                                                        OnClick="CreateAutomaticInterviewTest_byQuestion_testSegmantGridView_deleteImageButton_Click" />
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <div style="float: left; width: 92%;" class="grouping_border_bg">
                                                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                                                            <tr>
                                                                                                                                <td style="width: 15%">
                                                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_categoryLabel" runat="server"
                                                                                                                                        Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 48%; padding-left: 13px">
                                                                                                                                    <asp:TextBox ID="CreateAutomaticInterviewTest_byQuestion_categoryTextBox" ReadOnly="true"
                                                                                                                                        runat="server" Text='<%# Eval("Category") %>'></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td style="width: 20%">
                                                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_subjectLabel" runat="server"
                                                                                                                                        Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                                    <asp:HiddenField ID="CreateAutomaticInterviewTest_byQuestion_categoryHiddenField"
                                                                                                                                        runat="server" Value='<%# Eval("Cat_sub_id") %>' />
                                                                                                                                    <asp:HiddenField ID="CreateAutomaticInterviewTest_byQuestion_categoryNameHiddenField"
                                                                                                                                        runat="server" Value='<%# Eval("Category") %>' />
                                                                                                                                    <asp:HiddenField ID="CreateAutomaticInterviewTest_byQuestion_subjectNameHiddenField"
                                                                                                                                        runat="server" Value='<%# Eval("Subject") %>' />
                                                                                                                                </td>
                                                                                                                                <td style="width: 30%">
                                                                                                                                    <asp:TextBox ID="CreateAutomaticInterviewTest_byQuestion_subjectTextBox" ReadOnly="true"
                                                                                                                                        runat="server" Text='<%# Eval("Subject") %>'></asp:TextBox>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                    <div style="float: left; padding-left: 10px; padding-top: 5px;">
                                                                                                                        <asp:ImageButton ID="CreateAutomaticInterviewTest_byQuestion_categoryImageButton"
                                                                                                                            SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" ToolTip="Click here to select category and subject" />
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                                <td colspan="2">
                                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_weightageHeadLabel" runat="server"
                                                                                                                        Text="Weightage" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                    <asp:TextBox ID="CreateAutomaticInterviewTest_byQuestion_weightageTextBox" runat="server"
                                                                                                                        Width="8%" MaxLength="3" Text='<%# Eval("Weightage") %>'>
                                                                                                                    </asp:TextBox>
                                                                                                                    <ajaxToolKit:FilteredTextBoxExtender ID="CreateAutomaticInterviewTest_byQuestion_weightageFileteredExtender"
                                                                                                                        runat="server" TargetControlID="CreateAutomaticInterviewTest_byQuestion_weightageTextBox"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="1">
                                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_testAreaLabel" runat="server"
                                                                                                                        Text="Interview Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                </td>
                                                                                                                <td colspan="3" align="left">
                                                                                                                    <div class="checkbox_list_bg" style="padding-top:5px;width: 100%; height: 60px; overflow: auto">
                                                                                                                        <asp:CheckBoxList ID="CreateAutomaticInterviewTest_byQuestion_testAreaCheckBoxList"
                                                                                                                            runat="server" RepeatColumns="3" RepeatDirection="Horizontal" CellSpacing="5"
                                                                                                                            Width="100%" TextAlign="Right">
                                                                                                                        </asp:CheckBoxList>
                                                                                                                        <asp:HiddenField ID="CreateAutomaticInterviewTest_byQuestion_testAreaHiddenField"
                                                                                                                            runat="server" Value='<%# Eval("TestArea") %>' />
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 25px;">
                                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_rowSegmentLabel" SkinID="sknLabelTextRecordNumber"
                                                                                                                        runat="server" Text="Segment">
                                                                                                                    </asp:Label>&nbsp;<asp:Label ID="CreateAutomaticInterviewTest_byQuestion_rowIdLabel"
                                                                                                                        SkinID="sknLabelTextRecordNumber" runat="server" Text='<%# Container.DataItemIndex + 1 %>'>
                                                                                                                    </asp:Label>
                                                                                                                </td>
                                                                                                                <td align="left" style="width:450px;">
                                                                                                                    <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                                                                                        <tr>
                                                                                                                            <td style="width: 10%;">
                                                                                                                                <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_complexityLabel" runat="server"
                                                                                                                                    Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td align="left" style="width: 90%;">
                                                                                                                                <div class="checkbox_list_bg" style="height: 33px; overflow: auto;">
                                                                                                                                    <asp:CheckBoxList ID="CreateAutomaticInterviewTest_byQuestion_complexityCheckBoxList"
                                                                                                                                        runat="server" RepeatColumns="3" RepeatDirection="Horizontal" CellSpacing="5"
                                                                                                                                        Width="75%">
                                                                                                                                    </asp:CheckBoxList>
                                                                                                                                    <asp:HiddenField ID="CreateAutomaticText_byQuestion_complexityHiddenField" runat="server"
                                                                                                                                        Value='<%# Eval("Complexity") %>' />
                                                                                                                                </div>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <td colspan="2" style="width:150px;">
                                                                                                                    <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_keywordHeadLabel" runat="server"
                                                                                                                                    Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <div style="float: left">
                                                                                                                                    <asp:TextBox ID="CreateAutomaticInterviewTest_byQuestion_keywordTextBox" runat="server"
                                                                                                                                        Columns="35" MaxLength="100" Text='<%# Eval("Keyword") %>'></asp:TextBox>
                                                                                                                                </div>
                                                                                                                                <div style="float: left; padding-left: 5px">
                                                                                                                                    <asp:ImageButton ID="CreateAutomaticInterviewTest_byQuestion_keywordsImageButton"
                                                                                                                                        SkinID="sknHelpImageButton" runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;"
                                                                                                                                        ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="td_height_2">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="4">
                                                                                                                    <div id="CreateAutomaticInterviewTest_byQuestion_expectedQuestionsDiv" runat="server"
                                                                                                                        visible="false" style="width: 100%;">
                                                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td style="width: 15%;">
                                                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_expectedQuestionsHeadLabel"
                                                                                                                                        runat="server" Text="Expected Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 10%;">
                                                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_expecetdQuestions" runat="server"
                                                                                                                                        Text='<%# Eval("ExpectedQuestions") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 15%;">
                                                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestions_returnQuestionsHeadLabel"
                                                                                                                                        runat="server" Text="Picked Question" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 10%;">
                                                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestions_returnQuestionsLabel" runat="server"
                                                                                                                                        Text='<%# Eval("PickedQuestions") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 8%;">
                                                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_remarkHeadLabel" runat="server"
                                                                                                                                        Text="Remarks" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestin_remarkLabel" runat="server"
                                                                                                                                        Text='<%# Eval("Remarks") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_noOfQuestionsInDBHeadLabel"
                                                                                                                                        runat="server" Text="Total Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_noOfQuestionsLabel" runat="server"
                                                                                                                                        Text='<%# Eval("TotalRecordsinDB") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="padding-left: 1%">
                                                                        <asp:HiddenField ID="CreateAutomaticInterviewTest_isQuestionGenerated" runat="server" />
                                                                        <asp:LinkButton ID="CreateAutomaticInterviewTest_byQuestion_addSegmantLinkButton"
                                                                            SkinID="sknAddLinkButton" runat="server" Text="Add" ToolTip="Add new segment"
                                                                            OnClick="CreateAutomaticInterviewTest_byQuestion_addSegmantLinkButton_Click"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Button ID="CreateAutomaticInterviewTest_byQuestion_generateTestButton" runat="server"
                                                                            Text="Generate Interview" SkinID="sknButtonId" CausesValidation="False" OnClick="CreateAutomaticInterviewTest_byQuestion_generateTestButton_Click" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_5">
                                            </td>
                                        </tr>
                                        <tr id="CreateAutomaticInterviewTest_byQuestion_searchTestResultsTR" runat="server">
                                            <td id="Td1" class="header_bg" width="100%" runat="server">
                                                <asp:UpdatePanel ID="CreateAutomaticInterviewTest_byQuestion_headerUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                            <tr>
                                                                <td style="width: 50%;" align="left" class="header_text_bold">
                                                                </td>
                                                                <td style="width: 48%" align="left">
                                                                    <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:LinkButton ID="CreateManualTest_byQuestion_testDraftExpandLinkButton" runat="server"
                                                                                    Text="Expand All" SkinID="sknActionLinkButton" OnClientClick="javascript:return ExpORCollapseRows(this);"
                                                                                    Visible="False"></asp:LinkButton>
                                                                                <asp:HiddenField ID="CreateAutomaticInterviewTest_byQuestion_stateHiddenField" runat="server"
                                                                                    Value="0" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="width: 2%" align="right">
                                                                    <span id="CreateAutomaticInterviewTest_byQuestion_searchResultsUpSpan" runat="server"
                                                                        style="display: none;">
                                                                        <asp:Image ID="CreateAutomaticInterviewTest_byQuestion_searchResultsUpImage" runat="server"
                                                                            SkinID="sknMinimizeImage" />
                                                                    </span><span id="CreateAutomaticInterviewTest_byQuestion_searchResultsDownSpan" runat="server"
                                                                        style="display: block;">
                                                                        <asp:Image ID="CreateAutomaticInterviewTest_byQuestion_searchResultsDownImage" runat="server"
                                                                            SkinID="sknMaximizeImage" />
                                                                    </span>
                                                                    <asp:HiddenField ID="CreateAutomaticInterviewTest_byQuestion_isMaximizedHiddenField"
                                                                        runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr id="CreateManualTest_draftTr" style="width: 100%" runat="server">
                                            <td id="Td2" class="grid_body_bg" runat="server">
                                                <asp:UpdatePanel ID="CreateAutomaticInterviewTest_byQuestion_updatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <div style="width: 100%; overflow: auto; height: 300px;" runat="server" id="CreateAutomaticInterviewTest_byQuestion_testDrftDiv"
                                                            visible="False">
                                                            <asp:GridView ID="CreateAutomaticInterviewTest_byQuestion_testDraftGridView" OnRowDataBound="CreateAutomaticInterviewTest_byQuestion_testDraftGridView_RowDataBound"
                                                                runat="server" AutoGenerateColumns="False" GridLines="Horizontal" BorderColor="white"
                                                                BorderWidth="1px" OnRowCommand="CreateAutomaticInterviewTest_byQuestion_testDraftGridView_RowCommand"
                                                                OnSorting="CreateAutomaticInterviewTest_byQuestion_testDraftGridView_Sorting"
                                                                Width="100%">
                                                                <RowStyle CssClass="grid_alternate_row" />
                                                                <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                <HeaderStyle CssClass="grid_header_row" />
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="13%">
                                                                        <ItemTemplate>
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td style="width: 25%">
                                                                                        <asp:ImageButton ID="CreateAutomaticTes_byQuestion_testDrftRemoveImage" runat="server"
                                                                                            SkinID="sknAlertImageButton" CommandName="Select" ToolTip="Flag question" />
                                                                                    </td>
                                                                                    <td style="width: 25%">
                                                                                        <asp:UpdatePanel ID="CreateAutomaticTes_byQuestion_updatepanel" runat="server">
                                                                                            <ContentTemplate>
                                                                                                <asp:ImageButton ID="CreateAutomaticTes_byQuestion_testDrftmoveImageButton" CommandArgument='<%# Eval("QuestionKey") %>'
                                                                                                    runat="server" SkinID="sknDeleteImageButton" CommandName="DeleteQuestion" ToolTip="Delete Question" />
                                                                                            </ContentTemplate>
                                                                                        </asp:UpdatePanel>
                                                                                    </td>
                                                                                    <td style="width: 25%">
                                                                                        <asp:ImageButton ID="CreateAutomaticTest_byQuestion_GridView_moveDownQuestionImageButton"
                                                                                            runat="server" SkinID="sknMoveDown_ArrowImageButton" CommandName="MoveDown" CommandArgument='<%# Eval("DisplayOrder") %>'
                                                                                            ToolTip="Move Question Down" />
                                                                                    </td>
                                                                                    <td style="width: 25%">
                                                                                        <asp:ImageButton ID="CreateAutomaticTest_byQuestion_GridView_moveUpQuestionImageButton"
                                                                                            runat="server" SkinID="sknMoveUp_ArrowImageButton" CommandName="MoveUp" CommandArgument='<%# Eval("DisplayOrder") %>'
                                                                                            ToolTip="Move Question Up" />
                                                                                        <asp:HiddenField runat="server" ID="QuestionDetailPreviewControl_selectedQuestionGridView_displayOrderHiddenField"
                                                                                            Value='<%# Eval("DisplayOrder") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Question ID" ItemStyle-Width="14%">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="QuestionDetailPreviewControl_byQuestion_questionLinkButton" runat="server"
                                                                                Text='<%# Eval("QuestionKey") %>' CommandArgument='<%# Eval("QuestionKey") %>'
                                                                                CommandName="ViewInterviewQuestionDetails"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField ReadOnly="True" HeaderText="Category" ControlStyle-Width="6%" InsertVisible="False"
                                                                        DataField="CategoryName">
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField ReadOnly="True" HeaderText="Subject" ControlStyle-Width="7%" InsertVisible="False"
                                                                        DataField="SubjectName">
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="Question" ItemStyle-Width="40%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="SearchQuestion_byQuestion_questionGridLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),80) %>'
                                                                                ToolTip='<%# Eval("Question") %>'></asp:Label>
                                                                            <asp:HiddenField ID="SearchQuestion_byQuestion_questionGridHiddenfiels" runat="server"
                                                                                Value='<%# Eval("Question") %>' />
                                                                        </ItemTemplate>
                                                                        <ItemStyle Wrap="False" />
                                                                    </asp:TemplateField>
                                                                    <%--   <asp:TemplateField HeaderText="Time Limit" ItemStyle-Wrap="true" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="SearchQuestion_byQuestion_questionTimetakenGridViewTextbox" runat="server"
                                                                                Width="30px"></asp:TextBox>
                                                                           <ajaxToolKit:MaskedEditExtender ID="SearchQuestion_byQuestion_questionTimetakenGridViewTextboxMaskedEditExtender"
                                                                                runat="server" Enabled="True" ErrorTooltipEnabled="True" UserTimeFormat="None"
                                                                                Mask="99:99" MaskType="Time" TargetControlID="SearchQuestion_byQuestion_questionTimetakenGridViewTextbox"
                                                                                AutoComplete="true" AutoCompleteValue="00:00">
                                                                            </ajaxToolKit:MaskedEditExtender>
                                                                            <asp:RegularExpressionValidator ID="SearchQuestion_byQuestion_questionTimetakenGridViewTextboxRegularExpressionValidator"
                                                                                runat="server" ControlToValidate="SearchQuestion_byQuestion_questionTimetakenGridViewTextbox"></asp:RegularExpressionValidator>
                                                                        </ItemTemplate>
                                                                        <ItemStyle Wrap="False" />
                                                                    </asp:TemplateField>--%>
                                                                    <asp:TemplateField HeaderText="Complexity" ItemStyle-Width="8%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_complexityLabel" runat="server"
                                                                                Text='<%# Eval("ComplexityName") %>'></asp:Label>
                                                                            <asp:HiddenField ID="CreateAutomaticInterviewTest_byQuestion_complexityIDHiddenField"
                                                                                Value='<%# Eval("Complexity") %>' runat="server" />
                                                                            <asp:HiddenField ID="CreateAutomaticInterviewTest_byQuestion_questionrelationIDHiddenfield"
                                                                                runat="server" Value='<%# Eval("QuestionRelationId") %>' />
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td colspan="9" style="width: 100%">
                                                                                    <div id="CreateAutomaticInterviewTest_byQuestion_detailsDiv" runat="server" style="display: none;">
                                                                                        <table border="0" cellpadding="2" cellspacing="0" class="table_outline_bg">
                                                                                            <tr>
                                                                                                <td class="popup_question_icon" style="width: 35%" colspan="4">
                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_questionLabel" runat="server"
                                                                                                        SkinID="sknLabelText" Text='<%# Eval("Question").ToString()%>' Width="50%"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:HiddenField ID="QuestionDetailPreviewControl_selectedQuestionGridView_qestIDHiddenField"
                                                                                                        runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="td_padding_left_20" colspan="7">
                                                                                                    <asp:PlaceHolder ID="CreateAutomaticInterviewTest_byQuestion_answerChoicesPlaceHolder"
                                                                                                        runat="server"></asp:PlaceHolder>
                                                                                                    <asp:HiddenField ID="CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="td_height_5">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%">
                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_testAreaHeadLabel" runat="server"
                                                                                                        SkinID="sknLabelFieldHeaderText" Text="Interview Area"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 20%">
                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_testAreaTextBox" runat="server"
                                                                                                        ReadOnly="true" SkinID="sknLabelFieldText" Text='<%# Eval("TestAreaName") %>'></asp:Label>
                                                                                                    <asp:HiddenField ID="CreateAutomaticInterviewTest_byQuestion_testAreaIDHiddenField"
                                                                                                        runat="server" Value='<%# Eval("TestAreaID") %>' />
                                                                                                </td>
                                                                                                <td style="width: 16%">
                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_complexityHeadLabel" runat="server"
                                                                                                        SkinID="sknLabelFieldHeaderText" Text="Complexity"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CreateAutomaticInterviewTest_byQuestion_complexityTextBox" runat="server"
                                                                                                        ReadOnly="true" SkinID="sknLabelFieldText" Text='<%# Eval("ComplexityName") %>'></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="td_height_8">
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <%-- popup DIV --%>
                                                                                    <%-- <a href="#SearchQuestion_focusmeLink" id="CreateAutomaticInterviewTest_byQuestion_focusDownLink"
                                                                                runat="server"></a><a href="#" id="CreateAutomaticInterviewTest_byQuestion_focusmeLink" runat="server">
                                                                                </a>--%>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_5">
                                            </td>
                                        </tr>
                                        <tr runat="server" id="TR_ADDSEARCH">
                                            <td runat="server">
                                                <table>
                                                    <tr>
                                                        <td style="width: 150px">
                                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton ID="CreateAutomaticInterviewTest_addQuestionLinkButton" SkinID="sknAddLinkButton"
                                                                        runat="server" Text="Add new question" ToolTip="Add new question" OnClick="CreateAutomaticInterviewTest_addQuestionLinkButton_Click"></asp:LinkButton>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td>
                                                            <asp:UpdatePanel ID="CreateAutomaticInterviewTest_addNewQuestion" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton ID="CreateAutomaticInterviewTest_searchQuestionLinkButton" SkinID="sknAddLinkButton"
                                                                        runat="server" Text="Search & add question" ToolTip="Search & add question" OnClick="CreateAutomaticInterviewTest_searchQuestionLinkButton_Click"></asp:LinkButton>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:UpdatePanel ID="CreateAutomaticInterviewTest_byQuestion_automatedControlUpdatePanel"
                                                                runat="server">
                                                                <ContentTemplate>
                                                                    <uc2:AutomatedTestControl ID="CreateAutomaticInterviewTest_byQuestion_automatedTestUserControl"
                                                                        runat="server" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Panel ID="CreateAutomaticInterviewTest_byQuestion_questionPanel" runat="server"
                                                    Style="display: none" CssClass="popupcontrol_question_detail">
                                                    <div style="display: none">
                                                        <asp:Button ID="CreateAutomaticInterviewTest_byQuestion_hiddenButton" runat="server"
                                                            Text="Hidden" />
                                                    </div>
                                                    <asp:UpdatePanel ID="CreateAutomaticInterviewTest_byQuestion_previewUpdatepanel"
                                                        runat="server">
                                                        <ContentTemplate>
                                                            <uc4:QuestionDetailPreviewControl ID="CreateAutomaticInterviewTest_byQuestion_QuestionDetailPreviewControl"
                                                                runat="server" OnCancelClick="CreateAutomaticInterviewTest_cancelClick" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="CreateAutomaticInterviewTest_byQuestion_bottomQuestionModalPopupExtender"
                                                    runat="server" PopupControlID="CreateAutomaticInterviewTest_byQuestion_questionPanel"
                                                    TargetControlID="CreateAutomaticInterviewTest_byQuestion_hiddenButton" BackgroundCssClass="modalBackground"
                                                    DynamicServicePath="" Enabled="True">
                                                </ajaxToolKit:ModalPopupExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                        <ajaxToolKit:TabPanel ID="CreateManualTest_testdetailsTabPanel" runat="server">
                            <HeaderTemplate>
                                Interview Details
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="CreateAutomaticInterviewTest_byQuestion_testDetailsControlUpdatePanel"
                                    runat="server">
                                    <ContentTemplate>
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <uc1:TestDetailsControl ID="CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                    </ajaxToolKit:TabContainer>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="CreateAutomaticInterviewTest_byQuestion_bottomMessagesUpdatePanel"
                        runat="server">
                        <ContentTemplate>
                            <asp:Label ID="CreateAutomaticInterviewTest_bottomSuccessMessageLabel" runat="server"
                                SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="CreateAutomaticInterviewTest_bottomErrorMessageLabel" runat="server"
                                SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:UpdatePanel ID="CreateAutomaticInterviewTest_additionalQuestion" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="CreateAutomaticInterviewTest_addQuestionPanel" runat="server" CssClass="popupcontrol_addQuestion">
                                <div style="display: none;">
                                    <asp:Button ID="CreateAutomaticInterviewTest_addQuestionhiddenButton" runat="server"
                                        Text="Hidden" /></div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="popup_td_padding_10">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                        <asp:Literal ID="CreateAutomaticInterviewTest_addQuestionLiteral" runat="server"
                                                            Text="Add Question"></asp:Literal>
                                                    </td>
                                                    <td style="width: 50%" valign="top">
                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="CreateAutomaticInterviewTest_addQuestionTopCancelImageButton"
                                                                        runat="server" SkinID="sknCloseImageButton" 
                                                                        onclick="CreateAutomaticInterviewTest_addQuestionTopCancelImageButton_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popup_td_padding_10" valign="top">
                                            <table cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                <tr>
                                                    <td align="left" class="popup_td_padding_10">
                                                        <table border="0" cellpadding="0" cellspacing="5" class="tab_body_bg" align="left">
                                                            <tr>
                                                                <td valign="top">
                                                                    <table cellpadding="0" cellspacing="5">
                                                                        <tr>
                                                                            <td class="msg_align" colspan="4">
                                                                                <asp:Label ID="CreateAutomaticText_addQuestionErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CreateAutomaticInterviewTest_addQuestionQuestLabel" runat="server"
                                                                                    Text="Question" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                <span class='mandatory'>*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="CreateAutomaticInterviewTest_addQuestionQuestTextBox" runat="server"
                                                                                    MaxLength="3000" Height="52px" Width="498px" TextMode="MultiLine" Wrap="true"></asp:TextBox>
                                                                                <asp:LinkButton ID="CreateAutomaticInterviewTest_addImageLinkButton" runat="server"
                                                                                    SkinID="sknActionLinkButton" Text="Add Image"  ToolTip="Click here to add image"
                                                                                     OnClientClick="javascript: return ShowAddImagePanel();" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="CreateAutomaticInterviewTest_AddQuestionImageTR" runat="server" style="display: none">
                                                                            <td>
                                                                                <asp:Label ID="CreateAutomaticInterviewTest_selectImageLabel" runat="server" Text="Select Image"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                    <tr>
                                                                                        <td style="width:250px">
                                                                                            <ajaxToolKit:AsyncFileUpload ID="CreateAutomaticInterviewTest_questionImageUpload"
                                                                                                runat="server" OnUploadedComplete="CreateAutomaticInterviewTest_questionImageOnUploadComplete"
                                                                                                ClientIDMode="AutoID" />
                                                                                        </td>
                                                                                        <td style="width:30px">
                                                                                            <asp:ImageButton ID="CreateAutomaticInterviewTest_helpImageButton" runat="server"
                                                                                                SkinID="sknHelpImageButton" ToolTip="Add Image" OnClientClick="javascript:return false;" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Button ID="CreateAutomaticInterviewTest_questionImageButton" runat="server"
                                                                                                Text="Add" SkinID="sknButtonId" OnClick="CreateAutomaticInterviewTest_questionImageButtonClick"
                                                                                                ToolTip="Click to add image" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CreateAutomaticInterviewTest_addQuestionAnswerLabel" runat="server"
                                                                                    Text="Answer" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                <span class='mandatory'>*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="CreateAutomaticInterviewTest_addQuestionAnswerTextBox" runat="server"
                                                                                    MaxLength="500" Height="52px" Width="498px" TextMode="MultiLine"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td>
                                                                                <asp:CheckBox ID="CreateAutomaticInterviewTest_isQuestionRepository" runat="server"
                                                                                    Text="Save question to the repository<br>(Please leave this box unchecked if this question is meant only for this interview and you do not wish to add it to the permanent question database)"
                                                                                    Checked="false" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CreateAutomaticInterviewTest_addQuestioncomplexityLabel" runat="server"
                                                                                    Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                            </td>
                                                                            <td>
                                                                                <div>
                                                                                    <asp:DropDownList ID="CreateAutomaticInterviewTest_addQuestioncomplexityDropDownList"
                                                                                        runat="server" Width="133px">
                                                                                    </asp:DropDownList>
                                                                                    <asp:ImageButton ID="CreateAutomaticInterviewTest_addQuestioncomplexityImageButton"
                                                                                        SkinID="sknHelpImageButton" runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;"
                                                                                        ToolTip="Please select the complexity of the question here" />
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CreateAutomaticInterviewTest_addQuestionLabel" runat="server" Text="Interview Area"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                            </td>
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td class="checkboxaddquestion_list_bg">
                                                                                            <asp:RadioButtonList ID="CreateAutomaticInterviewTest_addQuestionRadioButtonList"
                                                                                                runat="server" RepeatColumns="2" RepeatDirection="Horizontal" CellSpacing="5"
                                                                                                TextAlign="Right" Width="100%" TabIndex="5">
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CreateAutomaticInterviewTest_addQuestion_byQuestion_categoryLabel"
                                                                                    runat="server" Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label><span
                                                                                        class='mandatory'>&nbsp;*</span>
                                                                            </td>
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:TextBox ID="CreateAutomaticInterviewAddQuestion_byQuestion_categoryTextBox"
                                                                                                ReadOnly="true" runat="server" Text='<%# Eval("Category") %>'></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="CreateAutomaticInterviewTest_addQuestion_byQuestion_subjectLabel"
                                                                                                runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            <asp:HiddenField ID="CreateAutomaticInterviewAddQuestion_byQuestion_categoryHiddenField"
                                                                                                runat="server" Value='<%# Eval("Cat_sub_id") %>' />
                                                                                            <asp:HiddenField ID="CreateAutomaticInterviewAddQuestion_byQuestion_categoryNameHiddenField"
                                                                                                runat="server" Value='<%# Eval("Category") %>' />
                                                                                            <asp:HiddenField ID="CreateAutomaticInterviewAddQuestion_byQuestion_subjectNameHiddenField"
                                                                                                runat="server" Value='<%# Eval("Subject") %>' />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="CreateAutomaticInterviewAddQuestion_byQuestion_subjectTextBox" ReadOnly="true"
                                                                                                runat="server" Text='<%# Eval("Subject") %>'></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:ImageButton ID="CreateAutomaticInterviewAddQuestion_byQuestion_categoryImageButton"
                                                                                                SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" ToolTip="Click here to select category and subject" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CreateAutomaticInterviewTest_tagsLabel" runat="server" Text="Tags"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="CreateAutomaticInterviewTest_tagsTextBox" runat="server" MaxLength="100"
                                                                                    Width="500px"></asp:TextBox>
                                                                                <asp:ImageButton ID="CreateAutomaticInterviewTest_tagsImageButton" runat="server" SkinID="sknHelpImageButton"
                                                                                    ToolTip="Please enter tags separated by commas" OnClientClick="javascript:return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td valign="top">
                                                                    <table cellpadding="0" cellspacing="5" class="tab_body_bg">
                                                                        <tr id="CreateAutomaticInterviewTest_DisplayQuestionImageTR" runat="server" style="display: none">
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Image runat="server" ID="CreateAutomaticInterviewTest_questionImage"/>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:LinkButton ID="CreateAutomaticInterviewTest_deleteLinkButton" runat="server"
                                                                                                Text="Delete" SkinID="sknActionLinkButton" OnClick="CreateAutomaticInterviewTest_deleteLinkButtonClick"
                                                                                                ToolTip="Click to delete image" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popup_td_padding_5">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td align="left" style="width: 20px; padding-right: 5px">
                                                        <asp:Button ID="CreateAutomaticText_addQuestioSaveButton" runat="server" SkinID="sknButtonId"
                                                            Text="Save" OnClick="CreateAutomaticText_addQuestioSaveButton_Clicked" />
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="CreateAutomaticText_addQuestioCloseLinkButton" SkinID="sknPopupLinkButton"
                                                            runat="server" Text="Cancel" 
                                                            onclick="CreateAutomaticText_addQuestioCloseLinkButton_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <ajaxToolKit:ModalPopupExtender ID="CreateAutomaticText_addQuestioModalPopupExtender"
                                runat="server" PopupControlID="CreateAutomaticInterviewTest_addQuestionPanel"
                                TargetControlID="CreateAutomaticInterviewTest_addQuestionhiddenButton" BackgroundCssClass="modalBackground">
                            </ajaxToolKit:ModalPopupExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="72%">
                                &nbsp;
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td width="63%" align="right">
                                            <asp:Button ID="CreateAutomaticInterviewTest_bottomSaveButton" runat="server" SkinID="sknButtonId"
                                                Text="Save" OnClick="CreateAutomaticInterviewTest_saveButton_Click" />
                                            <asp:Button ID="CreateAutomaticInterviewTest_bottomCreateSessionButton" runat="server"
                                                SkinID="sknButtonId" Text="Create Interview Session" OnClick="CreateAutomaticInterviewTest_CreateSessionButton_Click" />
                                        </td>
                                        <td align="right" style="width: 15%">
                                            <asp:LinkButton ID="CreateAutomaticInterviewTest_bottomResetLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Reset" OnClick="CreateAutomaticInterviewTest_resetLinkButton_Click" />
                                        </td>
                                        <td width="4%" align="center">
                                            |
                                        </td>
                                        <td width="18%" align="left">
                                            <asp:LinkButton ID="CreateAutomaticInterviewTest_bottomCancelLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <div id="SearchQuestion_hiddenDIV" runat="server" style="display: none">
        <asp:Button ID="CreateAutomaticText_hiddenPopupModalButton" runat="server" />
    </div>
    <ajaxToolKit:ModalPopupExtender ID="CreateAutomaticText_saveTestModalPopupExtender"
        runat="server" PopupControlID="CreateAutomaticText_saveTestConfrimPopUpPanel"
        TargetControlID="CreateAutomaticText_hiddenPopupModalButton" BackgroundCssClass="modalBackground">
    </ajaxToolKit:ModalPopupExtender>
    <asp:Panel ID="CreateAutomaticText_saveTestConfrimPopUpPanel" runat="server" Style="display: none"
        Height="210px" CssClass="popupcontrol_confirm">
        <uc3:ConfirmMsgControl ID="CreateAutomaticText_confirmPopupExtenderControl" runat="server"
            OnOkClick="CreateAutomaticText_okPopUpClick" OnCancelClick="CreateAutomaticText_cancelPopUpClick" />
    </asp:Panel>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load
    (
        function () {
            window.setTimeout(focus, 1);
        }
    )
        function focus() {
            try {
                document.getElementById('<%=CreateAutomaticInterviewTest_byQuestion_totalNumberTextBox.ClientID %>').focus();
            }
            catch (Err) {
                try {
                    document.getElementById('ctl00_OTMMaster_body_CreateAutomaticInterviewTest_mainTabContainer_CreateManualTest_testdetailsTabPanel_CreateAutomaticInterviewTest_byQuestion_testDetailsUserControl_TestDetailsControl_testNameTextBox').focus();
                }
                catch (Err1) {
                }
            }
        }
    </script>
</asp:Content>
