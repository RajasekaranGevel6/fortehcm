﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InterviewTestSession.aspx.cs" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    Inherits="Forte.HCM.UI.InterviewTestMaker.InterviewTestSession" Title="Interview Session" %>

<%@ Register Src="~/CommonControls/InterviewSessionPreviewControl.ascx" 
    TagName="InterviewSessionPreview" TagPrefix="uc4" %>

<%@ Register Src="~/CommonControls/CandidateInterviewDetailControl.ascx" TagName="CandidateDetail"
    TagPrefix="uc5" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ Register Src="../CommonControls/SearchCategorySubjectControl.ascx" TagName="SearchCategorySubjectControl"
    TagPrefix="uc6" %>
<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc7" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="InterviewTestSession_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <script type="text/javascript" language="javascript">
        // Method that invoke the Cancel modal popup
        function CancelPopUp(ctrl) {
            $find("<%= InterviewTestSession_cancelSessionModalPopupExtender.ClientID  %>").show();
            return false;
        }

        // Method that invoke candidatedetail modal pop up
        function CandidateDetailModalPopup() {
            $find("<%= InterviewTestSession_candidateDetailModalPopupExtender.ClientID %>").show();
            return false;
        }

        // Method that sets the active tab in the ajax tabl container
        function activeTab(index) {
            if (index !== $find("<%=InterviewTestSession_mainTabContainer.ClientID%>").get_activeTabIndex()) {
                $find("<%=InterviewTestSession_mainTabContainer.ClientID%>").set_activeTab($find("<%=InterviewTestSession_mainTabContainer.Tabs[1].ClientID%>"));
            }
        }

        // Method that changes the active tab and sets the textid textbox 
        // when user clicks view test session image button in 'bytest' tab
        function SetBySessionTestIDTextBox(Text) {
            $find("<%=InterviewTestSession_mainTabContainer.ClientID%>").set_activeTab($find("<%=InterviewTestSession_mainTabContainer.Tabs[1].ClientID%>"));
            document.getElementById("<%= InterviewTestSession_byInterviewTestSession_testIdTextBox.ClientID %>").value = Text;
            document.getElementById("<%= InterviewTestSession_byInterviewTestSession_candidateSessionIdTextBox.ClientID %>").value = "";
            document.getElementById("<%= InterviewTestSession_byInterviewTestSession_testSessionIdTextBox.ClientID %>").value = "";
            document.getElementById("<%= InterviewTestSession_byInterviewTestSession_testNameTextBox.ClientID %>").value = "";
            document.getElementById("<%= InterviewTestSession_byInterviewTestSession_schedulerNameTextBox.ClientID %>").value = "";
            document.getElementById("<%= InterviewTestSession_byInterviewTestSession_candidateNameTextBox.ClientID %>").value = "";
            document.getElementById("<%= InterviewTestSession_byInterviewTestSession_positionProfileTextBox.ClientID %>").value = "";
        }

        // Method that invokes cancel reason modal popup by assigning 
        // canceltext value to the div
        function ViewCancelReason(CancelText) {
            document.getElementById("<%= InterviewTestSession_bySession_cancelTestReasonDiv.ClientID %>").innerHTML = CancelText;
            $find("<%= InterviewTestSession_viewCancelReasonModalPopupExtender.ClientID %>").show();
            return false;
        }

        // Method is called when user click cancel button on cancel reason
        // modal pop up by clearing the div value
        function CancelReasonClose() {
            document.getElementById("<%= InterviewTestSession_bySession_cancelTestReasonDiv.ClientID %>").innerHTML = "";
            return false;
        }

    </script>
    <script type="text/javascript" language="javascript">

        var control;
        function getControl_TabClicked1(sender, e) {
            try {
                control = $get('<%=InterviewTestSession_byTest_categoryTextBox.ClientID%>');
            }
            catch (Err) {
                    control = $get('<%=InterviewTestSession_byTest_testIdTextBox.ClientID%>');
            }
        }
        function getControl_TabClicked2(sender, e) {
            control = $get('<%=InterviewTestSession_byInterviewTestSession_candidateSessionIdTextBox.ClientID%>');
        }

        function setFocus(sender, e) {
            try {
                control.focus();
            }
            catch (Err) {
                control = $get('<%=InterviewTestSession_byTest_testIdTextBox.ClientID%>');
                //control.focus();
            }
        } 


    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="InterviewTestSession_headerLiteral" runat="server" Text="Interview Session"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="2" align="right">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="InterviewTestSession_topResetLinkButton" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                            OnClick="InterviewTestSession_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="InterviewTestSession_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="InterviewTestSession_topMessagesUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="InterviewTestSession_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="InterviewTestSession_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 1%">
                        </td>
                        <td style="width: 15%">
                            <asp:Label ID="InterviewTestSession_creditsEarnedHeadLabel" runat="server" Text="Available Credits (in $)"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:LinkButton ID="InterviewTestSession_creditsEarnedLinkButton" runat="server" ReadOnly="true"
                                SkinID="sknActionLinkButton" Text="0" Font-Underline="false" ToolTip="Credits earned"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <ajaxToolKit:TabContainer ID="InterviewTestSession_mainTabContainer" runat="server" Width="100%"
                    ActiveTabIndex="0" OnClientActiveTabChanged="setFocus">
                    <ajaxToolKit:TabPanel ID="InterviewTestSession_BytestTabPanel" HeaderText="Search By Interview"
                        runat="server" TabIndex="0" OnClientClick="getControl_TabClicked1">
                        <HeaderTemplate>
                            Search By Interview
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:Panel ID="searchTEst" runat="server" DefaultButton="InterviewTestSession_byTest_topSearchButton">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="3" align="right">
                                                <tr>
                                                    <td>
                                                        <asp:UpdatePanel ID="InterviewTestSession_byTest_simpleUpdatPanel" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="InterviewTestSession_byTest_SimpleLinkButton" runat="server" Text="Advanced"
                                                                    SkinID="sknActionLinkButton" OnClick="InterviewTestSession_byTest_SimpleLinkButton_Click" />
                                                                <asp:HiddenField ID="InterviewTestSession_stateHiddenField" runat="server" Value="0" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="InterviewTestSession_byTest_searchByTestDiv" runat="server" style="display: block;">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="panel_bg">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td width="100%">
                                                                        <asp:UpdatePanel ID="InterviewTestSession_byTest_searchDivUpdatePanel" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <div id="InterviewTestSession_byTest_simpleSearchDiv" runat="server" style="display: block;
                                                                                    width: 100%; padding-top: 2px">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="InterviewTestSession_byTest_categoryHeadLabel" runat="server" Text="Category"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="InterviewTestSession_byTest_categoryTextBox" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="InterviewTestSession_byTest_subjectHeadLabel" runat="server" Text="Subject"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="InterviewTestSession_byTest_subjectTextBox" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="InterviewTestSession_byTest_keywordsHeadLabel" runat="server" Text="Keyword"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                    <asp:TextBox ID="InterviewTestSession_byTest_keywordsTextBox" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <div style="float: left;">
                                                                                                    <asp:ImageButton ID="InterviewTestSession_byTest_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>"
                                                                                                        Width="16px" />
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <div id="InterviewTestSession_byTest_advanceSearchDiv" runat="server" style="width: 100%"
                                                                                    visible="false">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <uc7:CategorySubjectControl ID="InterviewTestSession_byTest_categorySubjectControl" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="td_height_8">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="panel_inner_body_bg">
                                                                                                <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td style="width: 10%">
                                                                                                            <asp:Label ID="InterviewTestSession_byTest_testAreaHeadLabel" runat="server" Text="Interview Area"
                                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td class="checkbox_list_bg" style="width: 100%;" colspan="5">
                                                                                                            <asp:CheckBoxList ID="InterviewTestSession_byTest_testAreaCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                                RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                            </asp:CheckBoxList>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 10%;">
                                                                                                            <asp:Label ID="InterviewTestSession_byTest_testIdLabel" runat="server" Text="Interview ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="width: 25%;">
                                                                                                            <asp:TextBox ID="InterviewTestSession_byTest_testIdTextBox" runat="server"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td style="width: 16%">
                                                                                                            <asp:Label ID="InterviewTestSession_byTest_testNameLabel" runat="server" Text="Interview Name"
                                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="width: 20%;">
                                                                                                            <asp:TextBox ID="InterviewTestSession_byTest_testNameTextBox" runat="server"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td style="width: 10%;">
                                                                                                            <asp:Label ID="InterviewTestSession_byTest_authorHeadLabel" runat="server" Text="Interview Author"
                                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="width: 8%;">
                                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                                <asp:TextBox ID="InterviewTestSession_byTest_authorNameTextBox" runat="server"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="InterviewTestSession_byTest_autherNameHiddenField" runat="server" />
                                                                                                                <asp:HiddenField ID="InterviewTestSession_byTest_autherIdHiddenField" runat="server" />
                                                                                                            </div>
                                                                                                            <div style="float: left;">
                                                                                                                <asp:ImageButton ID="InterviewTestSession_byTest_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the interview author" /></div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="InterviewTestSession_byTest_certificateTestHeadLabel" runat="server" Text="Certificate Interview"
                                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:DropDownList ID="InterviewTestSession_byTest_certificateTestDropDownList" runat="server"
                                                                                                                Width="133px">
                                                                                                                <asp:ListItem Text="--Select--" Value="S"></asp:ListItem>
                                                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="InterviewTestSession_byTest_positionProfileLabel" runat="server" Text="Position Profile"
                                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                                <asp:TextBox ID="InterviewTestSession_byTest_positionProfileTextBox" runat="server" ReadOnly="true"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="InterviewTestSession_byTest_positionProfileIDHiddenField" runat="server" />
                                                                                                            </div>
                                                                                                            <div style="float: left;">
                                                                                                                <asp:ImageButton ID="InterviewTestSession_byTest_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the position profile" />
                                                                                                            </div>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="InterviewTestSession_byTest_keywordHeadLabel" runat="server" Text="Keyword"
                                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                                <asp:TextBox ID="InterviewTestSession_byTest_keywordAdvanceTextBox" runat="server"></asp:TextBox>
                                                                                                            </div>
                                                                                                            <div style="float: left;">
                                                                                                                &nbsp;<asp:ImageButton ID="InterviewTestSession_byTest_keywordImageButton" SkinID="sknHelpImageButton"
                                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, Search_Interview_KeywordHelp %>" />
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="InterviewTestSession_byTest_testCostHeadLabel" runat="server" Text="Interview Cost"
                                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <table width="50%" border="0" cellpadding="0" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td style="width: 20%">
                                                                                                                        <asp:TextBox ID="InterviewTestSession_byTest_testCostMinValueTextBox" runat="server" MaxLength="5"
                                                                                                                            Width="25"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td style="width: 40%">
                                                                                                                        <asp:TextBox ID="InterviewTestSession_byTest_testCostTextBox" runat="server" MaxLength="250"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td style="width: 20%">
                                                                                                                        <asp:TextBox ID="InterviewTestSession_byTest_testCostMaxValueTextBox" runat="server" MaxLength="5"
                                                                                                                            Width="25"></asp:TextBox>
                                                                                                                        <ajaxToolKit:MultiHandleSliderExtender ID="InterviewTestSession_testCostMultiHandleSliderExtender"
                                                                                                                            runat="server" Enabled="True" HandleAnimationDuration="0.1" Maximum="100" Minimum="0"
                                                                                                                            RailCssClass="slider_rail" HandleCssClass="slider_handler" TargetControlID="InterviewTestSession_byTest_testCostTextBox"
                                                                                                                            ShowHandleDragStyle="true" ShowHandleHoverStyle="true" EnableViewState="false">
                                                                                                                            <MultiHandleSliderTargets>
                                                                                                                                <ajaxToolKit:MultiHandleSliderTarget ControlID="InterviewTestSession_byTest_testCostMinValueTextBox"
                                                                                                                                    HandleCssClass="MultiHandleMinSliderCss" />
                                                                                                                                <ajaxToolKit:MultiHandleSliderTarget ControlID="InterviewTestSession_byTest_testCostMaxValueTextBox"
                                                                                                                                    HandleCssClass="MultiHandleMaxSliderCss" />
                                                                                                                            </MultiHandleSliderTargets>
                                                                                                                        </ajaxToolKit:MultiHandleSliderExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="InterviewTestSession_byTest_noOfQuestionsLabel" runat="server" Text="Number of Questions"
                                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <table width="40%" border="0" cellpadding="0" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td style="width: 20%">
                                                                                                                        <asp:TextBox ID="InterviewTestSession_byTest_noOfQuestionsMinValueTextBox" runat="server"
                                                                                                                            MaxLength="5" Width="25"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td style="width: 60%">
                                                                                                                        <asp:TextBox ID="InterviewTestSession_byTest_noOfQuestionsTextBox" runat="server" MaxLength="225"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td style="width: 20%">
                                                                                                                        <asp:TextBox ID="InterviewTestSession_byTest_noOfQuestionsMaxValueTextBox" runat="server"
                                                                                                                            MaxLength="5" Width="25"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                            <ajaxToolKit:MultiHandleSliderExtender ID="InterviewTestSession_byTest_noofQuestionsMultiHandleSliderExtender"
                                                                                                                runat="server" Enabled="True" HandleAnimationDuration="0.1" Maximum="100" Minimum="0"
                                                                                                                TargetControlID="InterviewTestSession_byTest_noOfQuestionsTextBox" RailCssClass="slider_rail"
                                                                                                                HandleCssClass="slider_handler" ShowHandleDragStyle="true" ShowHandleHoverStyle="true"
                                                                                                                EnableViewState="false">
                                                                                                                <MultiHandleSliderTargets>
                                                                                                                    <ajaxToolKit:MultiHandleSliderTarget ControlID="InterviewTestSession_byTest_noOfQuestionsMinValueTextBox"
                                                                                                                        HandleCssClass="MultiHandleMinSliderCss" />
                                                                                                                    <ajaxToolKit:MultiHandleSliderTarget ControlID="InterviewTestSession_byTest_noOfQuestionsMaxValueTextBox"
                                                                                                                        HandleCssClass="MultiHandleMaxSliderCss" />
                                                                                                                </MultiHandleSliderTargets>
                                                                                                            </ajaxToolKit:MultiHandleSliderExtender>
                                                                                                        </td>
                                                                                                         <td>
                                                                                                            <asp:Label ID="InterviewTestSession_byTest_showCopiedTestsLabel" runat="server" Text="Include Copied Interviews" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                                <asp:CheckBox ID="InterviewTestSession_byTest_showCopiedTestsCheckBox" runat="server"></asp:CheckBox>
                                                                                                            </div>
                                                                                                            <div style="float: left;">
                                                                                                                &nbsp;<asp:ImageButton ID="InterviewTestSession_byTest_showCopiedTestsImageButton" SkinID="sknHelpImageButton"
                                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Check this to search for copied interviews too" />
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" class="td_padding_top_5">
                                                                        <asp:UpdatePanel ID="InterviewTestSession_byTest_searchUpdatePanel" runat="server">
                                                                            <ContentTemplate>
                                                                                <asp:Button ID="InterviewTestSession_byTest_topSearchButton" runat="server" Text="Search"
                                                                                    OnClick="InterviewTestSession_topSearchButton_Click" SkinID="sknButtonId" UseSubmitBehavior="true" />&nbsp;
                                                                                <asp:LinkButton ID="InterviewTestSession_byTest_clearLinkButton" runat="server" Text="Clear"
                                                                                    SkinID="sknActionLinkButton" OnClick="InterviewTestSession_byTest_clearLinkButton_Click"></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr class="td_height_2">
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <asp:HiddenField ID="InterviewTestSession_byTest_isMaximizedHiddenField" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="InterviewTestSession_byTest_searchTestResultsTR" runat="server">
                                        <td id="Td1" class="header_bg" align="center" runat="server">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                        <asp:Literal ID="InterviewTestSession_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>
                                                        &nbsp;<asp:Label ID="InterviewTestSession_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                            Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 50%" align="right">
                                                        <span id="InterviewTestSession_byTest_searchResultsUpSpan" runat="server" style="display: none;">
                                                            <asp:Image ID="InterviewTestSession_byTest_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                        </span><span id="InterviewTestSession_byTest_searchResultsDownSpan" runat="server" style="display: block;">
                                                            <asp:Image ID="InterviewTestSession_byTest_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="grid_body_bg">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="left">
                                                        <asp:UpdatePanel ID="InterviewTestSession_byTest_GridVeiwUpdatePanel" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div style="height: 200px; overflow: auto;" runat="server" id="InterviewTestSession_byTest_testGridViewDiv">
                                                                    <asp:GridView ID="InterviewTestSession_byTest_testGridView" runat="server" AllowSorting="True"
                                                                        AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowDataBound="InterviewTestSession_byTest_testGridView_RowDataBound"
                                                                        OnSorting="InterviewTestSession_byTest_testGridView_Sorting" OnRowCreated="InterviewTestSession_byTest_testGridView_RowCreated"
                                                                        OnRowCommand="InterviewTestSession_byTest_testGridView_RowCommand">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    &nbsp;<asp:ImageButton ID="InterviewTestSession_byTest_viewSessionsImageButton" runat="server"
                                                                                        SkinID="sknViewImageButton" ToolTip="View Sessions" CommandName="ViewSessions"
                                                                                        CommandArgument='<%# Eval("TestKey") %>' />&nbsp;&nbsp;<asp:ImageButton ID="InterviewTestSession_byTest_createNewImageButton"
                                                                                            runat="server" SkinID="sknCreateNewSessionImageButton" ToolTip="Create Interview Session"
                                                                                            CommandName="Create Interview Session" CommandArgument='<%# Eval("TestKey") %>' />
                                                                                    <asp:HiddenField ID="InterviewTestSession_byTest_TestStatusHiddenField" runat="server" Value='<%# Eval("TestStatus") %>' />
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="7%" />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="Interview ID" DataField="TestKey" SortExpression="TESTKEY">
                                                                                <ItemStyle Width="95px" />
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Interview Name" SortExpression="TESTNAME">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="TestReport_testNameLabel" runat="server" Text='<%# TrimContent(Eval("Name").ToString(),20) %>'
                                                                                        ToolTip='<%# Eval("Name") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="150px" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Author" SortExpression="TESTAUTHOR" ItemStyle-Width="135px">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="TestReport_tesAuthorFullNameLabel" runat="server" Text='<%# Eval("TestAuthorName") %>'
                                                                                        ToolTip='<%# Eval("TestAuthorFullName") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="No Of Questions" DataField="NoOfQuestions" SortExpression="NOOFQUESTION"
                                                                                ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="right"
                                                                                HeaderStyle-CssClass="td_padding_right_20"></asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Created Date" SortExpression="CREATEDDATE DESC">
                                                                                <ItemStyle Width="125px" />
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="InterviewTestSession_byTest_CreatedDateLabel" runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("CreatedDate")))%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="Cost (in&nbsp;$)" DataField="TestCost" SortExpression="TESTCOST"
                                                                                ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="75px" ItemStyle-HorizontalAlign="right"
                                                                                HeaderStyle-CssClass="td_padding_right_20"></asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Certification Interview" SortExpression="CERTIFICATE" Visible="false">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="TestReport_testCertificationLabel" runat="server" Text='<%# GetExpansion(Convert.ToBoolean(Eval("IsCertification")))%>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="110px" />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="InterviewTestSession_byTest_testSessionPageNavigationUpdatePanel" runat="server"
                                                UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <uc3:PageNavigator ID="InterviewTestSession_byTest_testPagingNavigator" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="InterviewTestSession_byInterviewTestSession_testSessionTabPanel" runat="server"
                        TabIndex="1" OnClientClick="getControl_TabClicked2">
                        <HeaderTemplate>
                            Search By Interview Session
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:Panel ID="srfd" runat="server" DefaultButton="InterviewTestSession_byInterviewTestSession_topSearchButton">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="InterviewTestSession_byInterviewTestSession_serachUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <div id="InterviewTestSession_byInterviewTestSession_searchByTestSessionDiv" runat="server" style="display: block;">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="panel_bg">
                                                                    <table width="100%" cellpadding="" cellspacing="0">
                                                                        <tr>
                                                                            <td class="panel_inner_body_bg">
                                                                                <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                                                                    <tr>
                                                                                        <td style="width: 14%">
                                                                                            <asp:Label ID="InterviewTestSession_byInterviewTestSession_candidateSessionIdLabel" runat="server"
                                                                                                Text="Candidate Session&nbsp;ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 18%">
                                                                                            <asp:TextBox ID="InterviewTestSession_byInterviewTestSession_candidateSessionIdTextBox" runat="server"></asp:TextBox>
                                                                                            <asp:HiddenField ID="InterviewTestSession_byInterviewTestSession_isMaximizedHiddenField" runat="server" />
                                                                                        </td>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="InterviewTestSession__byInterviewTestSession_testSessionIdLabel" runat="server" Text="Interview Session ID"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 18%">
                                                                                            <asp:TextBox ID="InterviewTestSession_byInterviewTestSession_testSessionIdTextBox" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="InterviewTestSession_byInterviewTestSession_testSessionCreatorHeadLabel" runat="server"
                                                                                                Text="Interview Session Author" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="InterviewTestSession_byInterviewTestSession_testSessionCreatorTextBox" runat="server"
                                                                                                    ReadOnly="true"></asp:TextBox>
                                                                                                <asp:HiddenField ID="InterviewTestSession_byInterviewTestSession_testSessionCreatorHiddenField" runat="server" />
                                                                                                <asp:HiddenField ID="InterviewTestSession_byInterviewTestSession_testSessionCreatorIdHiddenField" runat="server" />
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                &nbsp;<asp:ImageButton ID="InterviewTestSession_byInterviewTestSession_testSessionCreatorImageButton"
                                                                                                    SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" />
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="InterviewTestSession_byInterviewTestSession_testIdLabel" runat="server" Text="Interview ID"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="InterviewTestSession_byInterviewTestSession_testIdTextBox" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="InterviewTestSession_byInterviewTestSession_testNameLabel" runat="server" Text="Interview Name"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="InterviewTestSession_byInterviewTestSession_testNameTextBox" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="InterviewTestSession_byInterviewTestSession_positionProfileHeadLabel" runat="server"
                                                                                                Text="Position Profile" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="InterviewTestSession_byInterviewTestSession_positionProfileTextBox" runat="server" ReadOnly="true"></asp:TextBox>
                                                                                                <asp:HiddenField ID="InterviewTestSession_byInterviewTestSession_positionProfileIDHiddenField" runat="server" />
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                &nbsp;<asp:ImageButton ID="InterviewTestSession_byInterviewTestSession_positionProfileImageButton"
                                                                                                    SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle"  ToolTip="Click here to select the position profile"/></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="InterviewTestSession_byInterviewTestSession_schedulerNameHeadLabel" runat="server" Text="Interview Schedule Creator"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="InterviewTestSession_byInterviewTestSession_schedulerNameTextBox" runat="server" ReadOnly="true"></asp:TextBox>
                                                                                                <asp:HiddenField ID="InterviewTestSession_byInterviewTestSession_schedulerNameHiddenField" runat="server" />
                                                                                                <asp:HiddenField ID="InterviewTestSession_byInterviewTestSession_schedulerIdHiddenField" runat="server" />
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                &nbsp;<asp:ImageButton ID="InterviewTestSession_byInterviewTestSession_schedulerNameImageButton" SkinID="sknbtnSearchicon"
                                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select interview schedule creator" />
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="InterviewTestSession_byInterviewTestSession_candidateNameHeadLabel" runat="server" Text="Candidate Name"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="InterviewTestSession_byInterviewTestSession_candidateNameTextBox" runat="server"></asp:TextBox>
                                                                                                <asp:HiddenField ID="InterviewTestSession_byInterviewTestSession_candidateIdHiddenField" runat="server" />
                                                                                                <asp:HiddenField ID="InterviewTestSession_byInterviewTestSession_candidateEmailHiddenField" runat="server" />
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                &nbsp;<asp:ImageButton ID="InterviewTestSession_byInterviewTestSession_candidateNameImageButton" SkinID="sknbtnSearchicon"
                                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select candidate name" /></div>
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_padding_top_5" align="right">
                                                                                <asp:Button ID="InterviewTestSession_byInterviewTestSession_topSearchButton" runat="server" Text="Search"
                                                                                    SkinID="sknButtonId" OnClick="InterviewTestSession_topSearchButton_Click" />&nbsp;
                                                                                <asp:LinkButton ID="InterviewTestSession_byInterviewTestSession_clearLinkButton" runat="server" Text="Clear"
                                                                                    SkinID="sknActionLinkButton" OnClick="InterviewTestSession_byTest_clearLinkButton_Click"></asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_2">
                                        </td>
                                    </tr>
                                    <tr id="InterviewTestSession_byInterviewTestSession_searchTestSessionResultsTR" runat="server">
                                        <td class="header_bg" align="center" runat="server">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                        <asp:Literal ID="InterviewTestSession_byInterviewTestSession_searchSessionResultsLiteral" runat="server"
                                                            Text="Search Results"></asp:Literal>&nbsp;<asp:Label ID="InterviewTestSession_byInterviewTestSession_searchSessionResults_sortHelpLabel"
                                                                runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 50%" align="right">
                                                        <span id="InterviewTestSession_byInterviewTestSession_searchSessionResultsUpSpan" runat="server" style="display: none;">
                                                            <asp:Image ID="InterviewTestSession_byInterviewTestSession_searchSessionResultsUpImage" runat="server"
                                                                SkinID="sknMinimizeImage" /></span><span id="InterviewTestSession_byInterviewTestSession_searchSessionResultsDownSpan"
                                                                    runat="server" style="display: block;"><asp:Image ID="InterviewTestSession_byInterviewTestSession_searchSessionResultsDownImage"
                                                                        runat="server" SkinID="sknMaximizeImage" /></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="grid_body_bg">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="left">
                                                        <asp:UpdatePanel ID="InterviewTestSession_byInterviewTestSession_testSessionGridViewUpdatePanel" runat="server"
                                                            UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div style="height: 200px; overflow: auto;" runat="server" id="InterviewTestSession_byInterviewTestSession_testSessionGridViewDiv">
                                                                    <asp:HiddenField ID="InterviewTestSession_byInterviewTestSession_pageNumberHiddenField" runat="server" />
                                                                    <asp:GridView ID="InterviewTestSession_byInterviewTestSession_testSessionGridView" runat="server" AllowSorting="True"
                                                                        AutoGenerateColumns="False" GridLines="None" Width="100%" OnSorting="InterviewTestSession_byInterviewTestSession_testSessionGridView_Sorting"
                                                                        OnRowDataBound="InterviewTestSession_byInterviewTestSession_testSessionGridView_RowDataBound" OnRowCreated="InterviewTestSession_byInterviewTestSession_testSessionGridView_RowCreated"
                                                                        OnRowCommand="InterviewTestSession_byInterviewTestSession_testSessionGridView_RowCommand" SkinID="sknWrapHeaderGrid"
                                                                        DataKeyNames="">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    &nbsp;
                                                                                    <asp:ImageButton ID="InterviewTestSession_byInterviewTestSession_editTestSessionImageButton" runat="server"
                                                                                        SkinID="sknEditSessionImageButton" ToolTip="Edit Interview Session" CommandName="Edit Interview Session"
                                                                                        CommandArgument='<%# Eval("TestSessionId") %>' />
                                                                                    &nbsp;<asp:ImageButton ID="InterviewTestSession_byInterviewTestSession_viewTestSessionImageButton"
                                                                                        runat="server" SkinID="sknViewTestSessionImageButton" ToolTip="View Interview Session"
                                                                                        CommandArgument='<%# Eval("TestSessionId") %>' CommandName="viewTestSession" />
                                                                                    <asp:ImageButton ID="InterviewTestSession_byInterviewTestSession_cancelTestSessionImageButton" runat="server"
                                                                                        SkinID="sknCancelImageButton" ToolTip="Cancel Interview Session" CommandArgument='<%# Eval("ShowCancelIcon") %>'
                                                                                        CommandName="Cancel Interview" />
                                                                                    <asp:ImageButton ID="InterviewTestSession_byInterviewTestSession_candidateDetailImageButton" runat="server"
                                                                                        SkinID="sknCandidateDetailImageButton" ToolTip="Candidate Detail" CommandArgument='<%# Eval("CandidateVisibleStatus") %>'
                                                                                        CommandName="Candidate Detail" />
                                                                                    <asp:ImageButton ID="InterviewTestSession_byInterviewTestSession_cancelReasonImageButton" runat="server"
                                                                                        SkinID="sknCancelReasonImageButton" ToolTip="View Cancel Reason" CommandName="viewCancelReason"
                                                                                        Visible="true" />
                                                                                    <asp:HiddenField ID="InterviewTestSession_byInterviewTestSession_cancelReasonHiddenField" runat="server"
                                                                                        Value='<%# Eval("CancelReason") %>' />
                                                                                    <asp:HiddenField ID="InterviewTestSession_byInterviewTestSession_attemptIDHiddenField" runat="server"
                                                                                        Value='<%# Eval("AttemptID") %>' />
                                                                                    <asp:HiddenField ID="InterviewTestSession_byInterviewTestSession_showTestScoreHiddenField" runat="server"
                                                                                        Value='<%# Eval("ShowTestScore") %>' />
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="13%" />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="Interview<br>Session&nbsp;ID" DataField="TestSessionId"
                                                                                SortExpression="TESTSESSIONID" HtmlEncode="false" />
                                                                            <asp:TemplateField HeaderText="Interview&nbsp;Key" Visible="false">
                                                                                <ItemStyle Width="10%" />
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="InterviewTestSession_byInterviewTestSession_testKeyLabel" runat="server" Text='<%# Eval("TestKey") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Candidate Session&nbsp;ID" SortExpression="CANDIDATESESSIONID DESC">
                                                                                <ItemStyle Width="10%" />
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="InterviewTestSession_byInterviewTestSession_candidateSessionIdLabel" runat="server"
                                                                                        Text='<%# Eval("CandidateSessionId") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="Client&nbsp;Request Number" DataField="ClientRequestNumber"
                                                                                SortExpression="CLIENTREQUESTNUMBER">
                                                                                <ItemStyle Width="12%" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="Date&nbsp;Of Purchase" DataField="DateofPurchase" SortExpression="Purchased_Date DESC">
                                                                                <ItemStyle Width="12%" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="Credits (in&nbsp;$)" DataField="Credits" SortExpression="Credits"
                                                                                ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right"
                                                                                HeaderStyle-CssClass="td_padding_right_20" />
                                                                            <asp:TemplateField HeaderText="Administered By" SortExpression="ADMINISTEREDBY">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="InterviewTestSession_byInterviewTestSession_testAuthorFullNameLabel" Text='<%# Eval("AdministeredBy") %>'
                                                                                        runat="server" ToolTip='<%# Eval("TestAuthorFullName") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Candidate Name" SortExpression="CANDNAME">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="InterviewTestSession_byInterviewTestSession_candidateFullNameLabel" Text='<%# Eval("CandidateName") %>'
                                                                                        runat="server" ToolTip='<%# Eval("CandidateFullName") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="Date Of Interview" DataField="DateofTest" SortExpression="DTOFTEST DESC" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="InterviewTestSession_byInterviewTestSession_testSessionPageNavigationUpdatePanel"
                                                runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <uc3:PageNavigator ID="InterviewTestSession_byInterviewTestSession_testSessionPagingNavigator" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_8">
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                </ajaxToolKit:TabContainer>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="InterviewTestSession_bottomMessagesUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="InterviewTestSession_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="InterviewTestSession_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr align="right">
            <td align="right" class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="2" align="right">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="InterviewTestSession_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="InterviewTestSession_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="InterviewTestSession_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="TestSesssion_popUpUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="InterviewTestSession_cancelTestPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_cancel_session">
                <div style="display: none">
                    <asp:Button ID="InterviewTestSession_hiddenButton" runat="server" Text="Hidden" />
                </div>
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                        <asp:Label ID="InterviewTestSession_questionResultLabel" runat="server" Text="Cancel Interview Session"></asp:Label>
                                    </td>
                                    <td style="width: 25%" align="right">
                                        <asp:ImageButton ID="InterviewTestSession_cancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td colspan="2" style="width: 100%" align="center">
                                                    <asp:Label ID="InterviewTestSession_cancelErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%">
                                                    <asp:Label ID="InterviewTestSession_cancelReasonLabel" runat="server" Text="Reason" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="InterviewTestSession_cancelTestReasonTextBox" runat="server" MaxLength="100"
                                                        SkinID="sknMultiLineTextBox" Columns="90" TextMode="MultiLine" Height="70" onchange="CommentsCount('100',this)"
                                                        onkeyup="CommentsCount('100',this)"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="popup_td_padding_5">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="left">
                                        <asp:Button ID="InterviewTestSession_saveCancellationButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="InterviewTestSession_saveCancellationButton_Click" />
                                        &nbsp;
                                        <asp:LinkButton ID="InterviewTestSession_closeLinkButton" runat="server" Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="InterviewTestSession_cancelSessionModalPopupExtender"
                runat="server" PopupControlID="InterviewTestSession_cancelTestPanel" TargetControlID="InterviewTestSession_hiddenButton"
                BackgroundCssClass="modalBackground" CancelControlID="InterviewTestSession_closeLinkButton">
            </ajaxToolKit:ModalPopupExtender>
            <asp:Panel ID="InterviewTestSession_byInterviewTestSession_viewCancelReasonTestPanel" runat="server"
                Style="display: none" CssClass="popupcontrol_cancel_session">
                <div style="display: none">
                    <asp:Button ID="InterviewTestSession_byInterviewTestSession_cancelReasonHiddenButton" runat="server"
                        Text="Hidden" />
                </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="td_height_20">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table width="90%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                        <asp:Label ID="InterviewTestSession_bySession_cancelReasonLiteral" runat="server" Text="Cancelled Interview Session Reason"></asp:Label>
                                    </td>
                                    <td style="width: 25%" align="right">
                                        <asp:ImageButton ID="InterviewTestSession_byInterviewTestSession_cancelReasonCancelImageButton" runat="server"
                                            SkinID="sknCloseImageButton" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_20">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%"  class="popupcontrol_question_inner_bg" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="popupcontrol_question_inner_bg" style="width: 15%">
                                                    <asp:Label ID="InterviewTestSession_byInterviewTestSession_cancelReasonLabel" runat="server" Text="Reason"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td  class="popup_td_padding_10">
                                                    <div runat="server" id="InterviewTestSession_bySession_cancelTestReasonDiv"  class="label_field_text" style="height: 70px;
                                                        width: 280px; overflow: auto;">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:LinkButton ID="InterviewTestSession_byInterviewTestSession_cancelReasonCancellationButton" runat="server"
                                            Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="InterviewTestSession_viewCancelReasonModalPopupExtender"
                runat="server" PopupControlID="InterviewTestSession_byInterviewTestSession_viewCancelReasonTestPanel"
                TargetControlID="InterviewTestSession_byInterviewTestSession_cancelReasonHiddenButton" CancelControlID="InterviewTestSession_byInterviewTestSession_cancelReasonCancellationButton"
                BackgroundCssClass="modalBackground" OnCancelScript="CancelReasonClose()">
            </ajaxToolKit:ModalPopupExtender>
            <asp:Panel ID="InterviewTestSession_previewTestSessionControlPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_question_detail">
                <div style="display: none">
                    <asp:Button ID="InterviewTestSession_viewTestSessionHiddenButton" runat="server" />
                </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                           <uc4:InterviewSessionPreview ID="InterviewTestSession_previewTestSessionControl_userControl" 
                            runat="server" Mode="view" />
                        </td>
                    </tr>
                    <tr>
                        <td class="td_padding_top_5" style="padding-left: 10px">
                            <asp:LinkButton ID="CreateInterviewTestSession_previewTestSessionControl_cancelButton2" runat="server"
                                Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="InterviewTestSession_previewTestSessionControl_modalpPopupExtender"
                runat="server" TargetControlID="InterviewTestSession_viewTestSessionHiddenButton" PopupControlID="InterviewTestSession_previewTestSessionControlPanel"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
            <asp:Panel ID="InterviewTestSession_canidateDetailPanel" runat="server" Style="display: none;
                height: auto;" CssClass="popupcontrol_question_detail">
                <div style="display: none">
                    <asp:Button ID="InterviewTestSession_canidateDetailHiddenButton" runat="server" />
                </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <uc5:CandidateDetail ID="InterviewTestSession_candidateDetailControl" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="InterviewTestSession_candidateDetailModalPopupExtender"
                runat="server" TargetControlID="InterviewTestSession_canidateDetailHiddenButton" PopupControlID="InterviewTestSession_canidateDetailPanel"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load
    (
        function () {
            window.setTimeout(focus, 1);
        }
    )
        function focus() {
            try {
                document.getElementById('<%=InterviewTestSession_byTest_categoryTextBox.ClientID %>').focus();
            }
            catch (Err) {
                try {
                    document.getElementById('<%=InterviewTestSession_byTest_testIdTextBox.ClientID %>').focus();
                }
                catch (er) {
                    document.getElementById('<%=InterviewTestSession_byInterviewTestSession_candidateSessionIdTextBox.ClientID %>').focus();
                }
            }
        }
    </script>
</asp:Content>
