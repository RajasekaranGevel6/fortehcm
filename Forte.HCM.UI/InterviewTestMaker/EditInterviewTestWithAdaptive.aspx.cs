#region Header                                                                                        

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EditManualTest.cs
// File that represents the user Serach the Question by various Key filed. 
// and Modified the Exists test Details and questions if Test included the Some of Session the new test will be created.
// This will helps edit & create a Test and have to implemeted the Certification details..

#endregion

#region Directives                                                                                    
using System;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Web;
using System.IO;
#endregion
namespace Forte.HCM.UI.InterviewTestMaker
{
    public partial class EditInterviewTestWithAdaptive : PageBase
    {
        #region Declaration
        string onmouseoverStyle = "className='grid_normal_row'";
        string onmouseoutStyle = "className='grid_alternate_row'";
        string parentPage = "";

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";
        string testKeyQuestyString = "";
        string questionID = "";
        List<QuestionDetail> questionDetailSearchResultList;
        List<QuestionDetail> interviewQuestionDraftList;
        /// <summary>
        /// A <see cref="int"/> constant that holds the adaptive page size.
        /// </summary>
        private const int ADAPTIVE_PAGE_SIZE = 5;
        private const string ADAPTIVE_QUESTION_DETAILS_VIEWSTATE = "ADAPTIVEQUESTIONDETAILS";

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                parentPage = Request.QueryString.Get("parentpage");
                if (EditInterviewTestWithAdaptive_mainTabContainer.ActiveTabIndex == 0)
                    Page.Form.DefaultButton = EditInterviewTestWithAdaptive_topSearchButton.UniqueID;
                else if (EditInterviewTestWithAdaptive_mainTabContainer.ActiveTabIndex == 1)
                    Page.Form.DefaultButton = EditInterviewTestWithAdaptive_bottomSaveButton.UniqueID;
                EditInterviewTestWithAdaptive_recommendAdaptiveQuestions.Style.Add("display", "none");
                StateMaintainence();
                MessageLabel_Reset();
                Master.SetPageCaption("Edit Interview");


                if (!IsPostBack)
                {
                    LoadValues();
                    EditInterviewTestWithAdaptive_simpleSearchDiv.Visible = true;
                    EditInterviewTestWithAdaptive_advanceSearchDiv.Visible = false;
                    EditInterviewTestWithAdaptive_testSummaryControl.AverageTimeTakenByCandidatesLabel = false;
                    EditInterviewTestWithAdaptive_testSummaryControl.AverageTimeTakenByCandidatesText = false;

                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTDIRECTIONKEY"]))
                        ViewState["SORTDIRECTIONKEY"] = "A";
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORTEXPRESSION"]))
                        ViewState["SORTEXPRESSION"] = "QUESTIONKEY";
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                        ViewState["PAGENUMBER"] = "1";
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
                        ViewState["SEARCHRESULTGRID"] = null;
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
                        testKeyQuestyString = Request.QueryString["testkey"].ToUpper();

                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["type"]))
                    {
                        EditInterviewTestWithAdaptive_topCreateSessionButton.Visible = false;
                        EditInterviewTestWithAdaptive_bottomCreateSessionButton.Visible = false;
                    }

                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["mode"]))
                    {
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString["showmessage"]))
                        {
                            if (Request.QueryString["mode"].ToLower() == "new")
                            {
                                base.ShowMessage(EditInterviewTestWithAdaptive_topSuccessMessageLabel,
                                        EditInterviewTestWithAdaptive_bottomSuccessMessageLabel, string.Format(Resources.HCMResource.EditManualInterviewTest_AddedSuccessfully, testKeyQuestyString));
                            }
                            else if (Request.QueryString["mode"].ToLower() == "edit")
                            {
                                base.ShowMessage(EditInterviewTestWithAdaptive_topSuccessMessageLabel,
                                       EditInterviewTestWithAdaptive_bottomSuccessMessageLabel, string.Format(Resources.HCMResource.EditManualInterviewTest_UpdatedSuccessfully, testKeyQuestyString));
                            }
                            else if (Request.QueryString["mode"].ToLower() == "copy")
                            {
                                base.ShowMessage(EditInterviewTestWithAdaptive_topSuccessMessageLabel,
                                      EditInterviewTestWithAdaptive_bottomSuccessMessageLabel, string.Format(Resources.HCMResource.EditManualInterviewTest_CopySuccessfully, testKeyQuestyString));
                                EditInterviewTestWithAdaptive_topCreateSessionButton.Visible = true;
                                EditInterviewTestWithAdaptive_bottomCreateSessionButton.Visible = true;
                            }
                        }
                    }

                    /*EditInterviewTestWithAdaptive_bottomCreateSessionButton.PostBackUrl = "CreateInterviewTestSession.aspx?m=1&s=2&parentpage=" + Constants.ParentPage.EDIT_TEST + "&interviewtestkey=" + testKeyQuestyString;
                    EditInterviewTestWithAdaptive_topCreateSessionButton.PostBackUrl = "CreateInterviewTestSession.aspx?m=1&s=2&parentpage=" + Constants.ParentPage.EDIT_TEST + "&interviewtestkey=" + testKeyQuestyString;*/

                    EditInterviewTestWithAdaptive_bottomCreateSessionButton.PostBackUrl = "CreateInterviewTestSession.aspx?m=1&s=2&parentpage=" + Constants.ParentPage.EDIT_ADAPTIVEINTERVIEW_TEST + "&interviewtestkey=" + testKeyQuestyString;
                    EditInterviewTestWithAdaptive_topCreateSessionButton.PostBackUrl = "CreateInterviewTestSession.aspx?m=1&s=2&parentpage=" + Constants.ParentPage.EDIT_ADAPTIVEINTERVIEW_TEST + "&interviewtestkey=" + testKeyQuestyString;

                    EditInterviewTestWithAdaptive_questionKeyHiddenField.Value = testKeyQuestyString;

                    BindTestQuestions(testKeyQuestyString);
                    BindTestDetails(testKeyQuestyString);

                    EditInterviewTestWithAdaptive_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                        + EditInterviewTestWithAdaptive_dummyAuthorID.ClientID + "','"
                        + EditInterviewTestWithAdaptive_authorIdHiddenField.ClientID + "','"
                        + EditInterviewTestWithAdaptive_authorTextBox.ClientID + "','QA')");

                    EditInterviewTestWithAdaptive_positionProfileImageButton.Attributes.Add("onclick",
                            "return ShowClientRequestForManual('" + EditInterviewTestWithAdaptive_positionProfileTextBox.ClientID + "','" +
                            EditInterviewTestWithAdaptive_positionProfileKeywordTextBox.ClientID + "','" +
                             EditInterviewTestWithAdaptive_positionProfileHiddenField.ClientID + "')");

                    EditInterviewTestWithAdaptive_testDrftGridView.DataSource = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>; ;
                    EditInterviewTestWithAdaptive_testDrftGridView.DataBind();
                    BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                                   false), Convert.ToInt32(EditInterviewTestWithAdaptive_adaptivePagingHiddenField.Value));
                    EditInterviewTestWithAdaptive_adaptivebottomPagingNavigator.Visible = true;
                }
                questionID = hiddenValue.Value;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(questionID))
                    MoveToTestDraft("Add");
                ExpandRestore();
                EditInterviewTestWithAdaptive_bottomPagingNavigator.PageNumberClick += new
                   PageNavigator.PageNumberClickEventHandler
                      (EditInterviewTestWithAdaptive_pagingNavigator_PageNumberClick);

                // Create events for paging control
                EditInterviewTestWithAdaptive_adaptivebottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (EditInterviewTestWithAdaptive_adaptivepagingNavigator_PageNumberClick);

                EditInterviewTestWithAdaptive_categorySubjectControl.ControlMessageThrown += new CategorySubjectControl.ControlMessageThrownDelegate(EditInterviewTestWithAdaptive_categorySubjectControl_ControlMessageThrown);
                HideLastRecord();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        private void StateMaintainence()
        {
            switch (EditInterviewTestWithAdaptive_resultsHiddenField.Value)
            {
                case "0":
                    EditInterviewTestWithAdaptive_questionDiv.Style.Add("width", "936px");
                    EditInterviewTestWithAdaptive_adaptiveQuestionDiv.Style.Add("width", "0px");
                    break;
                case "1":
                    EditInterviewTestWithAdaptive_questionDiv.Style.Add("width", "0px");
                    EditInterviewTestWithAdaptive_adaptiveQuestionDiv.Style.Add("width", "936px");
                    break;
                case "2":
                    EditInterviewTestWithAdaptive_questionDiv.Style.Add("width", "450px");
                    EditInterviewTestWithAdaptive_adaptiveQuestionDiv.Style.Add("width", "450px");
                    break;
                default:
                    EditInterviewTestWithAdaptive_questionDiv.Style.Add("width", "450px");
                    EditInterviewTestWithAdaptive_adaptiveQuestionDiv.Style.Add("width", "450px");
                    break;
            }
        }

        /// <summary>
        /// It shows error/success messages whenever the user calls this method
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        void EditInterviewTestWithAdaptive_categorySubjectControl_ControlMessageThrown(object sender, ControlMessageEventArgs c)
        {
            // Show message on the label.
            if (c.MessageType == MessageType.Error)
            {
                EditInterviewTestWithAdaptive_topErrorMessageLabel.Text = c.Message.ToString();
                EditInterviewTestWithAdaptive_bottomErrorMessageLabel.Text = c.Message.ToString();
            }
            else if (c.MessageType == MessageType.Success)
            {
                EditInterviewTestWithAdaptive_topSuccessMessageLabel.Text = c.Message.ToString();
                EditInterviewTestWithAdaptive_bottomSuccessMessageLabel.Text = c.Message.ToString();
            }
            BindSearchAndTestDraftGridView();
        }

        /// <summary>
        /// Handler method event is raised whenever recommend adaptive button
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        protected void EditInterviewTestWithAdaptive_recommendAdaptiveQuestions_Click(object sender, EventArgs e)
        {
            try
            {
                BindSearchAndTestDraftGridView();
                EditInterviewTestWithAdaptive_adaptiveRecommendClickedHiddenField.Value = "1";
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                    true), Convert.ToInt32(EditInterviewTestWithAdaptive_adaptivePagingHiddenField.Value));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void EditInterviewTestWithAdaptive_okClick(object sender, EventArgs e)
        {

            try
            {
                MoveToTestDraft("Delete");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                    EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }

        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void EditInterviewTestWithAdaptive_searchQuestionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "view")
                    return;
                string[] questionCollection = e.CommandArgument.ToString().Trim().Split(':');
                EditInterviewTestWithAdaptive_previewQuestionAddHiddenField.Value = e.CommandArgument.ToString();
                EditInterviewTestWithAdaptive_questionDetailSummaryControl.ShowAddButton = true;
                EditInterviewTestWithAdaptive_questionDetailSummaryControl.LoadQuestionDetails(questionCollection[0], int.Parse(questionCollection[1]));
                EditInterviewTestWithAdaptive_questionModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void EditInterviewTestWithAdaptive_searchQuestionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                HiddenField EditInterviewTestWithAdaptive_hidddenValue = (HiddenField)e.Row.FindControl("EditInterviewTestWithAdaptive_hidddenValue");
                string questionID = EditInterviewTestWithAdaptive_hidddenValue.Value.Split(':')[0].ToString();
                ImageButton alertImageButton = (ImageButton)e.Row.FindControl("EditInterviewTestWithAdaptive_searchQuestionAlertImageButton");
                alertImageButton.Attributes.Add("OnClick", "javascript:return LoadFlagQuestion('" + questionID + "');");
                Image img = (Image)e.Row.FindControl("EditInterviewTestWithAdaptive_searchQuestionSelectImage");
                img.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + EditInterviewTestWithAdaptive_searchQuestionGridView.ClientID + "');");
                // img.Attributes.Add("OnClick", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + EditInterviewTestWithAdaptive_searchQuestionGridView.ClientID + "');");
                img.Attributes.Add("OnClick", "javascript:return singleqnsclick('" + EditInterviewTestWithAdaptive_hidddenValue.Value + "','" + EditInterviewTestWithAdaptive_selectMultipleImage.ClientID + "');");
                e.Row.Attributes.Add("onmouseover", onmouseoverStyle);
                e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void EditInterviewTestWithAdaptive_adaptiveGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    EditInterviewTestWithAdaptive_previewQuestionAddHiddenField.Value = e.CommandArgument.ToString();
                    string[] QuestionDetails = e.CommandArgument.ToString().Split(':');
                    EditInterviewTestWithAdaptive_questionDetailSummaryControl.Visible = true;
                    EditInterviewTestWithAdaptive_questionDetailSummaryControl.ShowAddButton = true;
                    EditInterviewTestWithAdaptive_questionDetailSummaryControl.LoadQuestionDetails(QuestionDetails[0], Convert.ToInt32(QuestionDetails[1]));

                    EditInterviewTestWithAdaptive_questionModalPopupExtender.Show();
                }
                if (e.CommandName == "AdaptiveQuestion")
                {
                    EditInterviewTestWithAdaptive_adaptiveQuestionPreviewModalPopupExtender.Show();

                    ViewState["RowIndex"] = (e.CommandArgument.ToString());
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void EditInterviewTestWithAdaptive_adaptiveGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                ImageButton EditInterviewTestWithAdaptive_adaptiveSelectImage = (ImageButton)e.Row.FindControl("EditInterviewTestWithAdaptive_adaptiveSelectImage");
                string questionID = ((HiddenField)e.Row.FindControl("EditInterviewTestWithAdaptive_adaptiveQuestionhiddenValue")).Value;
                EditInterviewTestWithAdaptive_adaptiveSelectImage.Attributes.Add("OnClick", "javascript:return singleqnsclick('" + questionID + "','" + EditInterviewTestWithAdaptive_selectMultipleImage.ClientID + "');");
                ImageButton alertImage = (ImageButton)e.Row.FindControl("EditInterviewTestWithAdaptive_adaptiveAlertImageButton");
                alertImage.Attributes.Add("onClick", "javascript:return LoadFlagQuestion('" + questionID.Split(':')[0] + "');");
                Image img = (Image)e.Row.FindControl("EditInterviewTestWithAdaptive_adaptiveSelectImage");
                img.Attributes.Add("onmousedown", "javascript:return mousedown('" + e.Row.RowIndex.ToString() + "','" + EditInterviewTestWithAdaptive_adaptiveGridView.ClientID + "');");
                img.Attributes.Add("OnClick", "javascript:return singleqnsclick('" +
                    ((HiddenField)e.Row.FindControl("EditInterviewTestWithAdaptive_adaptiveQuestionhiddenValue")).Value + "','" + EditInterviewTestWithAdaptive_selectMultipleImage.ClientID + "');");
                e.Row.Attributes.Add("onmouseover", onmouseoverStyle);
                e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                        EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void EditInterviewTestWithAdaptive_testDrftGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ImageButton dowmImageButton = (ImageButton)e.Row.FindControl
                    ("EditInterviewTestWithAdaptive_byQuestion_moveDownQuestionImageButton");

                    ImageButton upImageButton = (ImageButton)e.Row.FindControl
                        ("EditInterviewTestWithAdaptive_byQuestion_moveUpQuestionImageButton");

                    Image img = (Image)e.Row.FindControl("EditInterviewTestWithAdaptive_testDrftRemoveImage");

                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);
                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);

                    if (e.Row.RowIndex == 0)
                    {
                        upImageButton.Visible = false;
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void EditInterviewTestWithAdaptive_testDrftGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "view")
                {
                    EditInterviewTestWithAdaptive_questionModalPopupExtender.Show();
                }
                else if (e.CommandName == "Select")
                {
                    EditInterviewTestWithAdaptive_ConfirmPopupPanel.Style.Add("height", "220px");
                    hiddenDeleteValue.Value = e.CommandArgument.ToString();
                    EditInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Message = string.Format(Resources.HCMResource.SearchQuestion_Delete_Confirm_Message, e.CommandArgument.ToString().Split(':')[0]);
                    EditInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    EditInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    EditInterviewTestWithAdaptive_ConfirmPopupExtender.Show();
                }
                else if ((e.CommandName == "MoveDown") || (e.CommandName == "MoveUp"))
                {

                    interviewQuestionDraftList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

                    int order = int.Parse(e.CommandArgument.ToString());
                    //if the command name is move down , move the record one line down 
                    if (e.CommandName == "MoveDown")
                    {
                        foreach (QuestionDetail question in interviewQuestionDraftList)
                        {
                            //Change the display order of the draft list
                            if (question.DisplayOrder == order)
                            {
                                question.DisplayOrder = order + 1;
                                continue;
                            }
                            if (question.DisplayOrder == order + 1)
                            {
                                question.DisplayOrder = order;
                                continue;
                            }
                        }
                    }

                    //if the command name is moveup, move the record one line up
                    if (e.CommandName == "MoveUp")
                    {
                        foreach (QuestionDetail question in interviewQuestionDraftList)
                        {
                            if (question.DisplayOrder == order - 1)
                            {
                                question.DisplayOrder = order;
                                continue;
                            }
                            if (question.DisplayOrder == order)
                            {
                                question.DisplayOrder = order - 1;
                                continue;
                            }
                        }
                    }
                }

                EditInterviewTestWithAdaptive_testDrftGridView.DataSource = interviewQuestionDraftList.OrderBy(p => p.DisplayOrder);
                EditInterviewTestWithAdaptive_testDrftGridView.DataBind();

                ViewState["TESTDRAFTGRID"] = interviewQuestionDraftList.OrderBy(p => p.DisplayOrder).ToList();

                HideLastRecord();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void EditInterviewTestWithAdaptive_searchQuestionButton_Click(object sender, EventArgs e)
        {
            try
            {
                EditInterviewTestWithAdaptive_bottomPagingNavigator.Reset();
                LoadQuestion(1, ViewState["SORTDIRECTIONKEY"].ToString(),
                     ViewState["SORTEXPRESSION"].ToString());
                MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the Move to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the move the Selected Question to Test Draft
        /// grid.
        /// </remarks>
        protected void EditInterviewTestWithAdaptive_selectMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                AddSearchQuestionToTestDraft();
                if (hiddenValue.Value != "")
                {
                    MoveToTestDraft("Add");
                }
                else
                {
                    MoveToTestDraft("");
                    base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                                       EditInterviewTestWithAdaptive_bottomErrorMessageLabel, Resources.HCMResource.CreateManualTest_Question_From_Search_Result);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }


        /// <summary>
        /// Handler method that will be called when the Remove to Test Draft button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the delete the Selected Question from Test Draft
        /// grid.
        /// </remarks>
        protected void EditInterviewTestWithAdaptive_removeMultipleImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool questionSelected = false;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
                {
                    foreach (GridViewRow row in EditInterviewTestWithAdaptive_testDrftGridView.Rows)
                    {
                        CheckBox EditInterviewTestWithAdaptive_testDrftCheckbox = (CheckBox)row.FindControl("EditInterviewTestWithAdaptive_testDrftCheckbox");
                        HiddenField EditInterviewTestWithAdaptive_testDraftQuestionKeyHiddenField = (HiddenField)row.FindControl("EditInterviewTestWithAdaptive_testDraftQuestionKeyHiddenField");
                        if (EditInterviewTestWithAdaptive_testDrftCheckbox.Checked)
                        {
                            questionID = questionID + EditInterviewTestWithAdaptive_testDraftQuestionKeyHiddenField.Value + ",";
                            questionSelected = true;
                        }
                    }
                    //  hiddenValue.Value = questionID.TrimEnd(',');

                }
                if (questionSelected)
                {
                    EditInterviewTestWithAdaptive_ConfirmPopupPanel.Style.Add("height", "220px");
                    hiddenDeleteValue.Value = questionID.TrimEnd(',');
                    EditInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Message = Resources.HCMResource.SearchQuestion_Delete_Multiple_Confirm_Message;
                    EditInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Title = "Delete Question(s)";
                    EditInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    EditInterviewTestWithAdaptive_ConfirmPopupExtender.Show();
                    //MoveToTestDraft("Delete");
                }
                else
                {
                    MoveToTestDraft("");
                    base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                                       EditInterviewTestWithAdaptive_bottomErrorMessageLabel, Resources.HCMResource.CreateManualInterviewTest_Question_From_InterviewTestDraft_Result);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Interchange Advanced and Simple Search Link 
        /// and Search Criteria fields 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        protected void EditInterviewTestWithAdaptive_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (EditInterviewTestWithAdaptive_simpleLinkButton.Text.ToLower() == "simple")
                {
                    EditInterviewTestWithAdaptive_simpleLinkButton.Text = "Advanced";
                    EditInterviewTestWithAdaptive_simpleSearchDiv.Visible = true;
                    EditInterviewTestWithAdaptive_advanceSearchDiv.Visible = false;
                }
                else
                {
                    EditInterviewTestWithAdaptive_simpleLinkButton.Text = "Simple";
                    EditInterviewTestWithAdaptive_simpleSearchDiv.Visible = false;
                    EditInterviewTestWithAdaptive_advanceSearchDiv.Visible = true;
                }
                MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void EditInterviewTestWithAdaptive_pagingNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            //BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
            //    ((EditInterviewTestWithAdaptive_adaptiveRecommendClickedHiddenField.Value == "") ? false : true)),
            //    Convert.ToInt32(EditInterviewTestWithAdaptive_adaptivePagingHiddenField.Value));
            MoveToTestDraft("");
            ViewState["PAGENUMBER"] = e.PageNumber;
            LoadQuestion(e.PageNumber, ViewState["SORTDIRECTIONKEY"].ToString(),
                ViewState["SORTEXPRESSION"].ToString());
        }

        /// <summary>
        /// Create Test and Test Question 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditInterviewTestWithAdaptive_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValidData())
                {
                    EditInterviewTest(true);
                }
                else
                {
                    MoveToTestDraft("");
                    if (EditInterviewTestWithAdaptive_questionDiv.Visible == true)
                        LoadQuestion(Convert.ToInt32(ViewState["PAGENUMBER"].ToString()), ViewState["SORTDIRECTIONKEY"].ToString(),
                            ViewState["SORTEXPRESSION"].ToString());
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void EditInterviewTestWithAdaptive_resetLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl + "&showmessage=n", false);
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void EditInterviewTestWithAdaptive_adaptivepagingNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            try
            {
                BindSearchAndTestDraftGridView();
                EditInterviewTestWithAdaptive_adaptivePagingHiddenField.Value = e.PageNumber.ToString();
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                    ((EditInterviewTestWithAdaptive_adaptiveRecommendClickedHiddenField.Value == "") ? false : true)),
                    e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Cancel Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel the Event will be raised.
        /// </remarks>
        protected void EditInterviewTestWithAdaptive_cancelClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = "";
                EditInterviewTestWithAdaptive_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter add Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void CreateManualTest_questionDetailPreviewControl_AddClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = EditInterviewTestWithAdaptive_previewQuestionAddHiddenField.Value;
                EditInterviewTestWithAdaptive_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("Add");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter add Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void EditInterviewTestWithAdaptive_questionDetailPreviewControl_AddClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = EditInterviewTestWithAdaptive_previewQuestionAddHiddenField.Value;
                EditInterviewTestWithAdaptive_previewQuestionAddHiddenField.Value = "";
                MoveToTestDraft("Add");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event Handling for the radio button list selected index changing event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void EditInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindSearchAndTestDraftGridView();
                if (EditInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList.SelectedIndex == 1)
                {
                    EditInterviewTestWithAdaptive_adaptiveRecommendClickedHiddenField.Value = "";
                    EditInterviewTestWithAdaptive_adaptivePagingHiddenField.Value = "1";
                    EditInterviewTestWithAdaptive_adaptiveGridView.Visible = false;
                    ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE] = null;
                    EditInterviewTestWithAdaptive_recommendAdaptiveQuestions.Style.Add("display", "none");
                    BindEmptyAdaptiveGridView();
                    return;
                }
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                    ((EditInterviewTestWithAdaptive_adaptiveRecommendClickedHiddenField.Value == "") ? false : true)),
                    Convert.ToInt32(EditInterviewTestWithAdaptive_adaptivePagingHiddenField.Value));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void EditInterviewTestWithAdaptive_questionDetail_TopAddButton_Click(object sender, EventArgs e)
        {
            //EditInterviewTestWithAdaptive_adaptiveQuestionPreviewModalPopupExtender.Hide();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "AddRow",
            //    "<script> AddRow(" + ViewState["RowIndex"] + "," + 1 + "); </script>", false);
            //ViewState["RowIndex"] = 0;
        }

        /// <summary>
        /// Handler that helps to save the posted file in session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditInterviewTestWithAdaptive_questionImageOnUploadComplete(object sender,
            AsyncFileUploadEventArgs e)
        {
            System.Threading.Thread.Sleep(500);
            if (EditInterviewTestWithAdaptive_questionImageUpload.PostedFile != null)
            {
                if (EditInterviewTestWithAdaptive_questionImageUpload.PostedFile.ContentLength > 102400) // check if image size exceeds 100kb
                {
                    base.ShowMessage(EditInterviewTestWithAdaptive_addQuestionErrorLabel,
                        Resources.HCMResource.BatchQuestionEntry_imageSizeExceeded);
                    EditInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }

                // Check if the photo is of the desired extension.
                string extension = Path.GetExtension(EditInterviewTestWithAdaptive_questionImageUpload.FileName);
                extension = extension.ToLower();

                if (extension != ".gif" && extension != ".png" && extension != ".jpg" && extension != ".jpeg")
                {
                    base.ShowMessage(EditInterviewTestWithAdaptive_addQuestionErrorLabel, "Only gif/png/jpg/jpeg files are allowed");
                    EditInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }

                // Read the bytes from the posted file.
                HttpPostedFile postedQstImage = EditInterviewTestWithAdaptive_questionImageUpload.PostedFile;
                int nFileLen = postedQstImage.ContentLength;
                byte[] imgData = new byte[nFileLen];

                postedQstImage.InputStream.Read(imgData, 0, nFileLen);
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = imgData;

                EditInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                return;
            }
            else
            {
                base.ShowMessage(EditInterviewTestWithAdaptive_addQuestionErrorLabel, "Please select valid image to upload");
                EditInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                return;
            }
        }

        /// <summary>
        /// Handler that helps to show the preview of the saved image
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>   
        protected void EditInterviewTestWithAdaptive_questionImageButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["POSTED_INTERVIEW_QUESTION_IMAGE"] != null)
                {
                    EditInterviewTestWithAdaptive_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=INTERVIEW_QUESTION_IMAGE";
                    EditInterviewTestWithAdaptive_addQuestionImageTR.Style.Add("display", "none");
                    EditInterviewTestWithAdaptive_displayQuestionImageTR.Style.Add("display", "");
                    EditInterviewTestWithAdaptive_addImageLinkButton.Style.Add("display", "none");
                    EditInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler that helps to delete the posted file.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EditInterviewTestWithAdaptive_deleteLinkButtonClick(object sender, EventArgs e)
        {
            try
            {
                ResetQuestionImage();
                EditInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                return;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                    exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private  Methods

        /// <summary>
        /// Method to reset the question image
        /// </summary>
        private void ResetQuestionImage()
        {
            Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            EditInterviewTestWithAdaptive_questionImage.ImageUrl = string.Empty;
            EditInterviewTestWithAdaptive_addQuestionImageTR.Style.Add("display", "none");
            EditInterviewTestWithAdaptive_displayQuestionImageTR.Style.Add("display", "none");
            EditInterviewTestWithAdaptive_addImageLinkButton.Style.Add("display", "");
        }

        /// <summary>
        /// Modify the Test.
        /// </summary>
        private void EditInterviewTest(bool isReviewQuestion)
        {
            questionDetailSearchResultList = new List<QuestionDetail>();
            questionDetailSearchResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            string testKey = string.Empty;
            Session["interviewDetail"] = null;
            Session["controlTestDetail"] = null;
            Session["interviewTestNew"] = null;

            string complexity = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY,
                questionDetailSearchResultList).AttributeID.ToString();
            int questionCount = Convert.ToInt32(questionDetailSearchResultList.Count.ToString());

            TestDetail testDetail = new TestDetail();
            testDetail = EditInterviewTestWithAdaptive_testDetailsUserControl.TestDetailDataSource;
            testDetail.Questions = questionDetailSearchResultList;
            testDetail.IsActive = true;
            testDetail.CreatedBy = userID;
            testDetail.IsDeleted = false;
            testDetail.ModifiedBy = userID;
            testDetail.NoOfQuestions = questionCount;
            testDetail.TestAuthorID = userID;
            testDetail.TestCreationDate = DateTime.Now;
            testDetail.TestStatus = TestStatus.Active;
            testDetail.TestType = TestType.Genenal;
            testDetail.TestMode = "MANUAL";

            if (Request.QueryString.Get("type") == "copy")
            {
                testDetail.TestKey = testKey;

                InterviewBLManager testBLManager = new InterviewBLManager();
                testKey = testBLManager.SaveInterviewTest(testDetail, userID, complexity);

                // Check if the new saved test is equal to the parent test (based on questions).
                // If yes update the copy and parent test ID status.
                string parentTestKey = Request.QueryString["testkey"];
                testBLManager.UpdateCopyTestStatus(parentTestKey, testKey);

                EditInterviewTestWithAdaptive_sessionInclucedKeyHiddenField.Value = "";
                Response.Redirect("EditInterviewTestWithAdaptive.aspx?m=1&s=2&mode=copy&parentpage=" + parentPage + "&testkey=" + testKey, false);
            }
            else
            {
                if (EditInterviewTestWithAdaptive_sessionInclucedKeyHiddenField.Value.ToLower() == "true")
                {
                    // Check if feature usage limit exceeds for creating test.
                    if (new AdminBLManager().IsFeatureUsageLimitExceeds(base.tenantID, Constants.FeatureConstants.CREATE_TEST))
                    {
                        base.ShowMessage(EditInterviewTestWithAdaptive_topSuccessMessageLabel,
                            EditInterviewTestWithAdaptive_bottomSuccessMessageLabel,
                            "You have reached the maximum limit based your subscription plan. Cannot create more tests");
                        return;
                    }
                    EditInterviewTestWithAdaptive_sessionInclucedKeyHiddenField.Value = "";
                    InterviewDetail interviewDetail = new InterviewDetail();
                    interviewDetail.InterviewTestKey = EditInterviewTestWithAdaptive_questionKeyHiddenField.Value;
                    interviewDetail.InterviewName = testDetail.Name;
                    interviewDetail.InterviewDescription = testDetail.Description;
                    interviewDetail.PositionProfileId = Convert.ToInt32(testDetail.PositionProfileID);
                    interviewDetail.PositionProfileName = testDetail.PositionProfileName;
                    interviewDetail.TestAuthor = testDetail.TestAuthorName;
                    interviewDetail.SessionIncludedActive = testDetail.SessionIncludedActive;
                    interviewDetail.TestAuthorID = testDetail.TestAuthorID; ;
                    interviewDetail.SessionIncluded = testDetail.SessionIncluded;

                    Session["interviewDetail"] = (InterviewDetail)interviewDetail;
                    Session["controlTestDetail"] = (TestDetail)testDetail;
                    Session["interviewTestNew"] = "Y";
                    Session["questionDetailTestDraftResultList"] = questionDetailSearchResultList as List<QuestionDetail>;
                    Response.Redirect("~/InterviewTestMaker/EditInterviewTestQuestionRatings.aspx?m=1&s=2&parentpage=" + parentPage, false);
                }
                else
                {
                    testDetail.TestKey = EditInterviewTestWithAdaptive_questionKeyHiddenField.Value;
                    // Check if the question is modified by some body.
                    if (new CommonBLManager().IsRecordModified("INTERVIEW_TEST", testDetail.TestKey,
                        (DateTime)ViewState["MODIFIED_DATE"]))
                    {
                        ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                            EditInterviewTestWithAdaptive_bottomErrorMessageLabel,
                                Resources.HCMResource.Edit_InterviewTest_Modifed_Another_User);
                        return;
                    }

                    if (isReviewQuestion)
                    {
                        InterviewDetail interviewDetail = new InterviewDetail();
                        interviewDetail.InterviewTestKey = testDetail.TestKey;
                        interviewDetail.InterviewName = testDetail.Name;
                        interviewDetail.InterviewDescription = testDetail.Description;
                        interviewDetail.PositionProfileId = Convert.ToInt32(testDetail.PositionProfileID);
                        interviewDetail.PositionProfileName = testDetail.PositionProfileName;
                        interviewDetail.TestAuthor = testDetail.TestAuthorName;
                        interviewDetail.SessionIncludedActive = testDetail.SessionIncludedActive;
                        interviewDetail.TestAuthorID = testDetail.TestAuthorID; ;
                        interviewDetail.SessionIncluded = testDetail.SessionIncluded;

                        Session["interviewDetail"] = (InterviewDetail)interviewDetail;
                        Session["controlTestDetail"] = (TestDetail)testDetail;
                        Session["questionDetailTestDraftResultList"] = questionDetailSearchResultList as List<QuestionDetail>;
                        Response.Redirect("~/InterviewTestMaker/EditInterviewTestQuestionRatings.aspx?m=1&s=2&parentpage=" + parentPage, false);
                    }
                    else
                    {
                        new InterviewBLManager().UpdateInterviewTest(testDetail, userID,
                            complexity, isReviewQuestion);
                        EditInterviewTestWithAdaptive_sessionInclucedKeyHiddenField.Value = "";
                        Response.Redirect("EditManualInterviewTest.aspx?m=1&s=" + Request.QueryString["s"] +
                            "&mode=edit&parentpage=" + parentPage + "&testkey=" + testDetail.TestKey, false);
                    }
                }
            }
            TestDraftSummary testDraftSummary = new TestDraftSummary();
            testDraftSummary.QuestionDetail = questionDetailSearchResultList;
            testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetailSearchResultList);
            EditInterviewTestWithAdaptive_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;
            base.ShowMessage(EditInterviewTestWithAdaptive_topSuccessMessageLabel,
                EditInterviewTestWithAdaptive_bottomSuccessMessageLabel, string.Format(Resources.HCMResource.CreateManualInterviewTest_AddedSuccessfully, testKey));
        }

        /// <summary>
        /// Expand or Restore the Result Grid 
        /// </summary>
        private void ExpandRestore()
        {
            if (EditInterviewTestWithAdaptive_restoreHiddenField.Value == "Y")
            {
                EditInterviewTestWithAdaptive_searchCriteriasDiv.Style["display"] = "none";
                EditInterviewTestWithAdaptive_searchResultsUpSpan.Style["display"] = "block";
                EditInterviewTestWithAdaptive_searchResultsDownSpan.Style["display"] = "none";
                EditInterviewTestWithAdaptive_questionDiv.Style["height"] = EXPANDED_HEIGHT;
                EditInterviewTestWithAdaptive_adaptiveQuestionDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                EditInterviewTestWithAdaptive_searchCriteriasDiv.Style["display"] = "block";
                EditInterviewTestWithAdaptive_searchResultsUpSpan.Style["display"] = "none";
                EditInterviewTestWithAdaptive_searchResultsDownSpan.Style["display"] = "block";
                EditInterviewTestWithAdaptive_questionDiv.Style["height"] = RESTORED_HEIGHT;
                EditInterviewTestWithAdaptive_adaptiveQuestionDiv.Style["height"] = RESTORED_HEIGHT;
            }
            if (IsPostBack)
                return;
            EditInterviewTestWithAdaptive_searchResultsUpImage.Attributes.Add("onclick",
                "ExpandOrRestoreforAdaptive('" +
                EditInterviewTestWithAdaptive_questionDiv.ClientID + "','" +
                EditInterviewTestWithAdaptive_searchCriteriasDiv.ClientID + "','" +
                EditInterviewTestWithAdaptive_searchResultsUpSpan.ClientID + "','" +
                EditInterviewTestWithAdaptive_searchResultsDownSpan.ClientID + "','" +
                EditInterviewTestWithAdaptive_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "','" +
                EditInterviewTestWithAdaptive_adaptiveQuestionDiv.ClientID + "');");
            EditInterviewTestWithAdaptive_searchResultsDownImage.Attributes.Add("onclick",
                "ExpandOrRestoreforAdaptive('" +
                EditInterviewTestWithAdaptive_questionDiv.ClientID + "','" +
                EditInterviewTestWithAdaptive_searchCriteriasDiv.ClientID + "','" +
                EditInterviewTestWithAdaptive_searchResultsUpSpan.ClientID + "','" +
                EditInterviewTestWithAdaptive_searchResultsDownSpan.ClientID + "','" +
                EditInterviewTestWithAdaptive_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "','" +
                EditInterviewTestWithAdaptive_adaptiveQuestionDiv.ClientID + "');");
        }

        /// <summary>
        /// Reset the message labels.
        /// </summary>
        private void MessageLabel_Reset()
        {
            EditInterviewTestWithAdaptive_topErrorMessageLabel.Text = string.Empty;
            EditInterviewTestWithAdaptive_bottomErrorMessageLabel.Text = string.Empty;
            EditInterviewTestWithAdaptive_topSuccessMessageLabel.Text = string.Empty;
            EditInterviewTestWithAdaptive_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Add & remove Question from the Test Draft.
        /// </summary>
        private void AddSearchQuestionToTestDraft()
        {
            StringBuilder sbQuestionIds = null;
            try
            {
                sbQuestionIds = new StringBuilder();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
                {
                    foreach (GridViewRow row in EditInterviewTestWithAdaptive_searchQuestionGridView.Rows)
                    {
                        CheckBox EditInterviewTestWithAdaptive_searchQuestionCheckbox = (CheckBox)row.FindControl("EditInterviewTestWithAdaptive_searchQuestionCheckbox");
                        HiddenField EditInterviewTestWithAdaptive_searchQuestionKeyHiddenField = (HiddenField)row.FindControl("EditInterviewTestWithAdaptive_searchQuestionKeyHiddenField");
                        if (EditInterviewTestWithAdaptive_searchQuestionCheckbox.Checked)
                            sbQuestionIds.Append(EditInterviewTestWithAdaptive_searchQuestionKeyHiddenField.Value + ",");
                    }
                    hiddenValue.Value = sbQuestionIds.ToString().TrimEnd(',');
                    if (EditInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList.SelectedIndex == 1)
                        return;
                    if (!EditInterviewTestWithAdaptive_adaptiveGridView.Visible)
                        return;
                    for (int i = 0; i < EditInterviewTestWithAdaptive_adaptiveGridView.Rows.Count; i++)
                    {
                        if (((CheckBox)(EditInterviewTestWithAdaptive_adaptiveGridView.Rows[i].FindControl("EditInterviewTestWithAdaptive_adaptiveCheckbox"))).Checked)
                            sbQuestionIds.Append(((HiddenField)(EditInterviewTestWithAdaptive_adaptiveGridView.Rows[i].
                                FindControl("EditInterviewTestWithAdaptive_adaptiveQuestionKeyHiddenField"))).Value + ",");
                    }
                    hiddenValue.Value = "";
                    hiddenValue.Value = sbQuestionIds.ToString().TrimEnd(',');
                }
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(sbQuestionIds)) sbQuestionIds = null;
            }
        }

        /// <summary>
        /// Get Search Results with Paging and Sorting. 
        /// </summary>
        /// <param name="pageNumber">Current Page number </param>
        /// <param name="orderBy">QuestionKey/ModifiedDate/Complexity/Question Description/Credids Earned</param>
        /// <param name="sortDirection">Descending/Assending</param>
        private void LoadQuestion(int pageNumber, string orderBy, string sortDirection)
        {
            hiddenValue.Value = null;
            int totalRecords = 0;
            StringBuilder complexityID = GetSelectedComplexityID();
            StringBuilder testAreaID = GetTestAreaID();

            QuestionDetailSearchCriteria questionDetailSearchCriteria = new QuestionDetailSearchCriteria();

            if (EditInterviewTestWithAdaptive_simpleLinkButton.Text == "Advanced")
            {
                questionDetailSearchCriteria.CategoryName =
                                    EditInterviewTestWithAdaptive_categoryTextBox.Text.Trim() == "" ?
                                    null : EditInterviewTestWithAdaptive_categoryTextBox.Text.Trim();
                questionDetailSearchCriteria.SubjectName =
                                    EditInterviewTestWithAdaptive_subjectTextBox.Text.Trim() == "" ?
                                    null : EditInterviewTestWithAdaptive_subjectTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    EditInterviewTestWithAdaptive_keywordsTextBox.Text.Trim() == "" ?
                                    null : EditInterviewTestWithAdaptive_keywordsTextBox.Text.Trim();
            }
            else
            {
                string SelectedSubjectIDs = "";
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(EditInterviewTestWithAdaptive_categorySubjectControl.SubjectDataSource))
                {
                    List<Category> categoryList = new List<Category>();
                    foreach (Subject subject in EditInterviewTestWithAdaptive_categorySubjectControl.SubjectDataSource)
                    {

                        if (subject.IsSelected == true)
                        {
                            Category category = new Category();
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                            category.CategoryID = subject.CategoryID;
                            categoryList.Add(category);
                        }

                    }
                    ///Get all Subject ids in  Un selected Subject List.
                    Category categoryID = new Category();
                    foreach (Subject subject in EditInterviewTestWithAdaptive_categorySubjectControl.SubjectDataSource)
                    {
                        categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                        if (categoryID == null)
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                    }
                }
                questionDetailSearchCriteria.CategorySubjectsKey =
                                    SelectedSubjectIDs == "" ?
                                    null : SelectedSubjectIDs.TrimEnd(',');
                questionDetailSearchCriteria.TestAreaID =
                                    testAreaID.ToString() == "" ?
                                    null : testAreaID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.Complexities =
                                    complexityID.ToString() == "" ?
                                    null : complexityID.ToString().TrimEnd(',');
                questionDetailSearchCriteria.QuestionKey =
                                    EditInterviewTestWithAdaptive_questionIDTextBox.Text.Trim() == "" ?
                                    null : EditInterviewTestWithAdaptive_questionIDTextBox.Text.Trim();
                questionDetailSearchCriteria.Tag =
                                    EditInterviewTestWithAdaptive_keywordTextBox.Text.Trim() == "" ?
                                    null : EditInterviewTestWithAdaptive_keywordTextBox.Text.Trim();
                questionDetailSearchCriteria.AuthorName =
                                    EditInterviewTestWithAdaptive_authorTextBox.Text.Trim() == "" ?
                                    null : EditInterviewTestWithAdaptive_authorTextBox.Text.Trim();
                questionDetailSearchCriteria.ActiveFlag = null;
                questionDetailSearchCriteria.CreditsEarned =
                                    EditInterviewTestWithAdaptive_creditTextBox.Text.Trim() == "" ?
                                    0 : Convert.ToDecimal(EditInterviewTestWithAdaptive_creditTextBox.Text.Trim());
                questionDetailSearchCriteria.ClientRequest = EditInterviewTestWithAdaptive_positionProfileKeywordTextBox.Text.Trim() == "" ?
                   null : EditInterviewTestWithAdaptive_positionProfileKeywordTextBox.Text.Trim();
            }


            List<QuestionDetail> questionDetail = new List<QuestionDetail>();
            questionDetail = new BL.InterviewBLManager().GetInterviewSearchQuestions(questionDetailSearchCriteria,
                base.GridPageSize, pageNumber, orderBy, sortDirection, out totalRecords);

            if (questionDetail == null || questionDetail.Count == 0)
            {
                EditInterviewTestWithAdaptive_searchQuestionGridView.DataSource = null;
                EditInterviewTestWithAdaptive_searchQuestionGridView.DataBind();
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                 EditInterviewTestWithAdaptive_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                EditInterviewTestWithAdaptive_bottomPagingNavigator.TotalRecords = 0;
                EditInterviewTestWithAdaptive_questionDiv.Visible = false;
            }
            else
            {
                EditInterviewTestWithAdaptive_searchQuestionGridView.DataSource = questionDetail;
                EditInterviewTestWithAdaptive_searchQuestionGridView.DataBind();
                EditInterviewTestWithAdaptive_bottomPagingNavigator.PageSize = base.GridPageSize;
                EditInterviewTestWithAdaptive_bottomPagingNavigator.TotalRecords = totalRecords;
                EditInterviewTestWithAdaptive_questionDiv.Visible = true;
            }
            ViewState["SEARCHRESULTGRID"] = questionDetail;
            // return questionDetail;
        }

        /// <summary>
        ///  Bind complexities/attributes based on the attribute type
        /// </summary>
        private void BindComplexities()
        {
            EditInterviewTestWithAdaptive_complexityCheckBoxList.DataSource =
                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
            EditInterviewTestWithAdaptive_complexityCheckBoxList.DataTextField = "AttributeName";
            EditInterviewTestWithAdaptive_complexityCheckBoxList.DataValueField = "AttributeID";
            EditInterviewTestWithAdaptive_complexityCheckBoxList.DataBind();
        }

        /// <summary>
        /// Bind test area based on TEST_AREA attribute type
        /// </summary>
        private void BindTestArea()
        {

            EditInterviewTestWithAdaptive_testAreaCheckBoxList.DataSource =
               new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
            EditInterviewTestWithAdaptive_testAreaCheckBoxList.DataTextField = "AttributeName";
            EditInterviewTestWithAdaptive_testAreaCheckBoxList.DataValueField = "AttributeID";
            EditInterviewTestWithAdaptive_testAreaCheckBoxList.DataBind();
        }

        /// <summary>
        /// Get Selected Test Area IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetTestAreaID()
        {
            StringBuilder testAreaID = new StringBuilder();
            for (int i = 0; i < EditInterviewTestWithAdaptive_testAreaCheckBoxList.Items.Count; i++)
            {

                if (EditInterviewTestWithAdaptive_testAreaCheckBoxList.Items[i].Selected)
                {

                    testAreaID.Append(EditInterviewTestWithAdaptive_testAreaCheckBoxList.Items[i].Value.ToString());
                    testAreaID.Append(",");
                }

            }
            return testAreaID;
        }

        /// <summary>
        /// Get Selected Complexity IDs.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetSelectedComplexityID()
        {
            StringBuilder complexityID = new StringBuilder();
            for (int i = 0; i < EditInterviewTestWithAdaptive_complexityCheckBoxList.Items.Count; i++)
            {
                if (EditInterviewTestWithAdaptive_complexityCheckBoxList.Items[i].Selected)
                {

                    complexityID.Append(EditInterviewTestWithAdaptive_complexityCheckBoxList.Items[i].Value.ToString());
                    complexityID.Append(",");
                }
            }
            return complexityID;
        }

        /// <summary>
        /// Uncheck the checkbox 
        /// </summary>
        private void UncheckCheckBox()
        {
            foreach (GridViewRow row in EditInterviewTestWithAdaptive_searchQuestionGridView.Rows)
            {
                if (((CheckBox)row.FindControl("EditInterviewTestWithAdaptive_searchQuestionCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("EditInterviewTestWithAdaptive_searchQuestionCheckbox")).Checked = false;
                }
            }
            foreach (GridViewRow row in EditInterviewTestWithAdaptive_adaptiveGridView.Rows)
            {
                if (((CheckBox)row.FindControl("EditInterviewTestWithAdaptive_adaptiveCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("EditInterviewTestWithAdaptive_adaptiveCheckbox")).Checked = false;
                }
            }

        }

        /// <summary>
        /// add and remove the Test Draft 
        /// </summary>
        /// <param name="action"></param>
        private void MoveToTestDraft(string action)
        {
            questionDetailSearchResultList = new List<QuestionDetail>();
            List<QuestionDetail> questionDetailTestDraftResultList = new List<QuestionDetail>();
            QuestionDetail questionDetailSearchResult = new QuestionDetail();

            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenDeleteValue.Value))
            {
                hiddenValue.Value = hiddenDeleteValue.Value;
                hiddenDeleteValue.Value = null;
            }
            int StartIndex = 0;
            int TotalCount = 0;
            string addedQuestionID = "";
            string notAddedQuestion = "";
            bool alertFlag = false;
            questionDetailSearchResultList = ViewState["SEARCHRESULTGRID"] as List<QuestionDetail>;
            if (ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE] != null)
            {
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailSearchResultList))
                    questionDetailSearchResultList = new List<QuestionDetail>();
                StartIndex = questionDetailSearchResultList.Count;
                questionDetailSearchResultList.AddRange((List<QuestionDetail>)ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE]);
                TotalCount = questionDetailSearchResultList.Count - StartIndex;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(hiddenValue.Value))
            {

                string[] questionIds = hiddenValue.Value.Split(',');

                foreach (string ids in questionIds)
                {
                    if (ids != "")
                    {
                        string[] idList = ids.Split(':');
                        string id = idList[0];
                        string relationId = idList[1];
                        if (ViewState["TESTDRAFTGRID"] != null)
                        {
                            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
                            if (action == "Add")
                            {
                                if (!questionDetailTestDraftResultList.Exists(p => p.QuestionKey.Trim() == id.Trim()))
                                {
                                    MessageLabel_Reset();
                                    questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                    questionDetailSearchResult.DisplayOrder = questionDetailTestDraftResultList.Count + 1;
                                    List<AnswerChoice> questionAnswerList = new BL.QuestionBLManager().GetInterviewQuestionOptions(id);
                                    if (questionAnswerList != null)
                                        questionDetailSearchResult.Choice_Desc += questionAnswerList[0].Choice.ToString();
                                    questionDetailSearchResult.ComplexityName = questionDetailSearchResult.Complexity;
                                    questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                                    ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                                    addedQuestionID = addedQuestionID + id + ", ";
                                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(EditInterviewTestWithAdaptive_insertedQuestionHiddenField.Value))
                                    {
                                        EditInterviewTestWithAdaptive_insertedQuestionHiddenField.Value = questionDetailSearchResult.QuestionKey + ",";
                                    }
                                    else
                                    {
                                        EditInterviewTestWithAdaptive_insertedQuestionHiddenField.Value = EditInterviewTestWithAdaptive_insertedQuestionHiddenField.Value + questionDetailSearchResult.QuestionKey + ",";
                                    }
                                    base.ShowMessage(EditInterviewTestWithAdaptive_topSuccessMessageLabel,
                                        EditInterviewTestWithAdaptive_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToInterviewDraft);
                                }
                                else
                                {
                                    notAddedQuestion = notAddedQuestion + id + ", ";
                                    alertFlag = true;
                                }
                            }
                            else if (action == "Delete")
                            {
                                MessageLabel_Reset();
                                questionDetailSearchResult = questionDetailTestDraftResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                                questionDetailTestDraftResultList.Remove(questionDetailSearchResult);
                                base.ShowMessage(EditInterviewTestWithAdaptive_topSuccessMessageLabel,
                                    EditInterviewTestWithAdaptive_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualInterviewTest_RemoveToTestDraft);
                                ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                                if (Forte.HCM.Support.Utility.IsNullOrEmpty(EditInterviewTestWithAdaptive_deletedQuestionHiddenField.Value))
                                {
                                    EditInterviewTestWithAdaptive_deletedQuestionHiddenField.Value = questionDetailSearchResult.QuestionKey + ",";
                                }
                                else
                                {
                                    EditInterviewTestWithAdaptive_deletedQuestionHiddenField.Value = EditInterviewTestWithAdaptive_deletedQuestionHiddenField.Value + questionDetailSearchResult.QuestionKey + ",";
                                }
                            }
                        }
                        else
                        {
                            questionDetailSearchResult = questionDetailSearchResultList.Find(p => p.QuestionKey.Trim() == id.Trim() && p.QuestionRelationId == Convert.ToInt32(relationId.Trim()));
                            questionDetailSearchResult.DisplayOrder = 1;
                            List<AnswerChoice> questionAnswerList = new BL.QuestionBLManager().GetInterviewQuestionOptions(id);
                            if (questionAnswerList != null)
                                questionDetailSearchResult.Choice_Desc += questionAnswerList[0].Choice.ToString();
                            questionDetailSearchResult.ComplexityName = questionDetailSearchResult.Complexity;
                            questionDetailTestDraftResultList.Add(questionDetailSearchResult);
                            ViewState["TESTDRAFTGRID"] = questionDetailTestDraftResultList;
                            base.ShowMessage(EditInterviewTestWithAdaptive_topSuccessMessageLabel,
                                        EditInterviewTestWithAdaptive_bottomSuccessMessageLabel, Resources.HCMResource.CreateManualTest_MoveToInterviewDraft);
                        }

                    }
                }

            }
            if (EditInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList.SelectedIndex == 0)
            {
                switch (action)
                {
                    case "Add":
                        if (addedQuestionID == "")
                        {
                            EditInterviewTestWithAdaptive_adaptiveRecommendClickedHiddenField.Value = EditInterviewTestWithAdaptive_adaptiveRecommendClickedHiddenField.Value;
                            EditInterviewTestWithAdaptive_adaptivePagingHiddenField.Value = EditInterviewTestWithAdaptive_adaptivePagingHiddenField.Value;
                        }
                        else
                        {
                            EditInterviewTestWithAdaptive_adaptiveRecommendClickedHiddenField.Value = "";
                            EditInterviewTestWithAdaptive_adaptivePagingHiddenField.Value = "1";
                        }
                        break;
                    case "Delete":
                        EditInterviewTestWithAdaptive_adaptiveRecommendClickedHiddenField.Value = "";
                        EditInterviewTestWithAdaptive_adaptivePagingHiddenField.Value = "1";
                        break;
                }
                BindAdaptiveQuestions(GetSelectedQuestionsIds((List<QuestionDetail>)ViewState["TESTDRAFTGRID"],
                    ((EditInterviewTestWithAdaptive_adaptiveRecommendClickedHiddenField.Value == "") ? false : true)),
                    Convert.ToInt32(EditInterviewTestWithAdaptive_adaptivePagingHiddenField.Value));
            }
            if (alertFlag)
            {
                string alertMessage = string.Format(Resources.HCMResource.CreateManualInterviewTest_QuestionExist, notAddedQuestion.Trim().TrimEnd(','));
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(addedQuestionID))
                {
                    alertMessage = alertMessage + "<br/>" + string.Format(Resources.HCMResource.CreateManualTest_QuestionAddedToTestDraft, addedQuestionID.Trim().TrimEnd(','));
                }
                EditInterviewTestWithAdaptive_ConfirmPopupPanel.Style.Add("height", "270px");
                EditInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Type = MessageBoxType.Ok;
                EditInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Title = "Warning";
                EditInterviewTestWithAdaptive_ConfirmPopupExtenderControl.Message = alertMessage;
                EditInterviewTestWithAdaptive_ConfirmPopupExtender.Show();

            }
            hiddenValue.Value = null;
            if (TotalCount != 0)
                questionDetailSearchResultList.RemoveRange(StartIndex, TotalCount);
            questionDetailTestDraftResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            EditInterviewTestWithAdaptive_testDrftGridView.DataSource = questionDetailTestDraftResultList;
            EditInterviewTestWithAdaptive_testDrftGridView.DataBind();
            //if (questionDetailSearchResultList != null)
            //{
            //    EditInterviewTestWithAdaptive_searchQuestionGridView.DataSource = questionDetailSearchResultList;
            //    EditInterviewTestWithAdaptive_questionDiv.Visible = true;
            //}
            //else
            //{
            //    EditInterviewTestWithAdaptive_searchQuestionGridView.DataSource = null;
            //    EditInterviewTestWithAdaptive_questionDiv.Visible = false;
            //}
            BindSearchAndTestDraftGridView();
            //EditInterviewTestWithAdaptive_searchQuestionGridView.DataBind();

            if (action != "")
            {
                questionDetailSearchResultList = new List<QuestionDetail>();
                questionDetailSearchResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

                List<decimal> timeTaken = new List<decimal>();

                for (int i = 0; i < questionDetailSearchResultList.Count; i++)
                {
                    timeTaken.Add(questionDetailSearchResultList[i].AverageTimeTaken);
                }
                if (questionDetailTestDraftResultList.Count > 0)
                {
                    EditInterviewTestWithAdaptive_testDetailsUserControl.SysRecommendedTime = Convert.ToInt32(timeTaken.Average()) * questionDetailTestDraftResultList.Count;
                }
                else
                {
                    EditInterviewTestWithAdaptive_testDetailsUserControl.SysRecommendedTime = 0;
                }
                TestDraftSummary testDraftSummary = new TestDraftSummary();
                testDraftSummary.QuestionDetail = questionDetailTestDraftResultList;
                testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetailTestDraftResultList);
                EditInterviewTestWithAdaptive_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;

            }

            UncheckCheckBox();
            HideLastRecord();
        }

        /// <summary>
        /// Hide move down image of the last record
        /// </summary>
        private void HideLastRecord()
        {
            foreach (GridViewRow row in EditInterviewTestWithAdaptive_testDrftGridView.Rows)
            {
                if (row.RowIndex == EditInterviewTestWithAdaptive_testDrftGridView.Rows.Count - 1)
                {
                    ImageButton dowmImageButton = (ImageButton)row.FindControl
                        ("EditInterviewTestWithAdaptive_byQuestion_moveDownQuestionImageButton");
                    dowmImageButton.Visible = false;
                }
            }
        }
        /// <summary>
        /// Helps to bind test question details against the testkey
        /// </summary>
        /// <param name="testKey"></param>
        private void BindTestQuestions(string testKey)
        {
            int displayOrder = 0;
            List<QuestionDetail> questionDetail = new List<QuestionDetail>();
            questionDetail =
                new InterviewBLManager().GetInterviewTestQuestionDetail(testKey, "");
            TestDraftSummary testDraftSummary = new TestDraftSummary();
            testDraftSummary.QuestionDetail = questionDetail;
            testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetail);
            EditInterviewTestWithAdaptive_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;
            //EditInterviewTestWithAdaptive_testSummaryControl.LoadQuestionDetailsSummary(questionDetail, GetComplexity());
            foreach (QuestionDetail question in questionDetail)
            {
                question.DisplayOrder = displayOrder + 1;
                displayOrder++;
            }

            ViewState["TESTDRAFTGRID"] = questionDetail;
        }

        /// <summary>
        /// Helps to bind test details against the testkey
        /// </summary>
        /// <param name="testKey"></param>
        private void BindTestDetails(string testKey)
        {
            TestDetail testDetail = new TestDetail();
            testDetail = new InterviewBLManager().GetInterviewTestDetailView(testKey);
            EditInterviewTestWithAdaptive_testDetailsUserControl.TestDetailDataSource = testDetail;
            EditInterviewTestWithAdaptive_sessionInclucedKeyHiddenField.Value = testDetail.SessionIncluded.ToString();
            ViewState["MODIFIED_DATE"] = testDetail.ModifiedDate;
            if (Request.QueryString.Get("type") == "copy")
            {
                EditInterviewTestWithAdaptive_topSaveButton.Text = "Copy Interview";
                EditInterviewTestWithAdaptive_bottomSaveButton.Text = "Copy Interview";

                // Clear position profile details.
                EditInterviewTestWithAdaptive_testDetailsUserControl.PositionProfileID = 0;
                EditInterviewTestWithAdaptive_testDetailsUserControl.PositionProfileName = string.Empty;

                return;
            }
            if (EditInterviewTestWithAdaptive_sessionInclucedKeyHiddenField.Value.ToLower() == "true")
            {
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                  EditInterviewTestWithAdaptive_bottomErrorMessageLabel, string.Format(Resources.HCMResource.EditManualInterviewTest_TestAlreadyExists, testKey));
            }

        }

        #region Adaptive Recommendation Questions Method

        /// <summary>
        /// This method loads the search and test area grid view.
        /// </summary>
        protected void BindSearchAndTestDraftGridView()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SEARCHRESULTGRID"]))
            {
                EditInterviewTestWithAdaptive_searchQuestionGridView.DataSource = (List<QuestionDetail>)ViewState["SEARCHRESULTGRID"];
                EditInterviewTestWithAdaptive_searchQuestionGridView.DataBind();
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["TESTDRAFTGRID"]))
            {
                EditInterviewTestWithAdaptive_testDrftGridView.DataSource = (List<QuestionDetail>)ViewState["TESTDRAFTGRID"];
                EditInterviewTestWithAdaptive_testDrftGridView.DataBind();
            }
        }

        /// <summary>
        /// Handles the Click event of the EditInterviewTestWithAdaptive_addQuestionLinkButton_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void EditInterviewTestWithAdaptive_addQuestionLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                EditInterviewTestWithAdaptive_addQuestionErrorLabel.Text = string.Empty;
                EditInterviewTestWithAdaptive_addQuestionQuestTextBox.Text = string.Empty;
                EditInterviewTestWithAdaptive_questionImage.ImageUrl = string.Empty;
                EditInterviewTestWithAdaptive_addQuestionAnswerTextBox.Text = string.Empty;
                EditInterviewTestWithAdaptive_isQuestionRepository.Checked = false;
                EditInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryTextBox.Text = string.Empty;
                EditInterviewTestWithAdaptiveAddQuestion_byQuestion_subjectTextBox.Text = string.Empty;
                EditInterviewTestWithAdaptive_tagsTextBox.Text = string.Empty;
                

                // Binding test area to radiobutton list.
                EditInterviewTestWithAdaptive_addQuestionRadioButtonList.DataSource =
               new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
                EditInterviewTestWithAdaptive_addQuestionRadioButtonList.DataTextField = "AttributeName";
                EditInterviewTestWithAdaptive_addQuestionRadioButtonList.DataValueField = "AttributeID";
                EditInterviewTestWithAdaptive_addQuestionRadioButtonList.DataBind();
                EditInterviewTestWithAdaptive_addQuestionRadioButtonList.Items[0].Selected = true;

                //Bind complexities/attributes based on the attribute type
                EditInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.DataSource =
                    new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
                EditInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.DataTextField = "AttributeName";
                EditInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.DataValueField = "AttributeID";
                EditInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.DataBind();

                EditInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.Items.Insert(0, "--Select--");
                EditInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.SelectedIndex = 0;


                EditInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryImageButton.Attributes.Add("onclick",
                        "return LoadCategorySubjectLookUpforReadOnly('" + EditInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryTextBox.ClientID +
                            "','" + EditInterviewTestWithAdaptiveAddQuestion_byQuestion_subjectTextBox.ClientID + "','" +
                            EditInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryHiddenField.ClientID + "','" +
                            EditInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryNameHiddenField.ClientID + "','" +
                            EditInterviewTestWithAdaptiveAddQuestion_byQuestion_subjectNameHiddenField.ClientID + "');");
                EditInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                    EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exception.Message);

            }
        }

        /// <summary>
        /// Handles the Clicked event of the EditInterviewTestWithAdaptive_addQuestioSaveButton_Clicked control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void EditInterviewTestWithAdaptive_addQuestioSaveButton_Clicked
                (object sender, EventArgs e)
        {
            try
            {

                if (EditInterviewTestWithAdaptive_addQuestionQuestTextBox.Text.Length == 0)
                {
                    EditInterviewTestWithAdaptive_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(EditInterviewTestWithAdaptive_addQuestionErrorLabel, "Enter the question");
                    EditInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (EditInterviewTestWithAdaptive_addQuestionAnswerTextBox.Text.Length == 0)
                {
                    EditInterviewTestWithAdaptive_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(EditInterviewTestWithAdaptive_addQuestionErrorLabel, "Enter the answer");
                    EditInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (EditInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.SelectedItem.Text == "--Select--")
                {
                    EditInterviewTestWithAdaptive_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(EditInterviewTestWithAdaptive_addQuestionErrorLabel, "Select the complexity");

                    EditInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }

                if (EditInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryTextBox.Text.Length == 0)
                {
                    EditInterviewTestWithAdaptive_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(EditInterviewTestWithAdaptive_addQuestionErrorLabel, "Select the category and subject");
                    EditInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }
                if (EditInterviewTestWithAdaptiveAddQuestion_byQuestion_subjectTextBox.Text.Length == 0)
                {
                    EditInterviewTestWithAdaptive_addQuestionErrorLabel.Text = null;
                    base.ShowMessage(EditInterviewTestWithAdaptive_addQuestionErrorLabel, "Select the category and subject");
                    EditInterviewTestWithAdaptive_addQuestioModalPopupExtender.Show();
                    return;
                }

                SaveNewQuestion();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                    EditInterviewTestWithAdaptive_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Saves the new question.
        /// </summary>
        private void SaveNewQuestion()
        {
            List<QuestionDetail> questions = null;
            QuestionDetail questionDetail = null;
            QuestionBLManager questionBLManager = new QuestionBLManager();

            questionDetail = new QuestionDetail();

            if (ViewState["TESTDRAFTGRID"] == null)
                questions = new List<QuestionDetail>();
            else
                questions = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;

            questionDetail = ConstructQuestionDetails();
            new QuestionBLManager().SaveInterviewQuestion(questionDetail);
            questionDetail.QuestionRelationId = questionBLManager.GetQuestionRelationID(questionDetail.QuestionKey);
            questionDetail.CategoryName = questionBLManager.GetCategory(questionDetail.QuestionKey);
            questionDetail.SubjectName = questionBLManager.GetSubject(questionDetail.QuestionKey);
            questionDetail.Complexity = questionDetail.ComplexityName;
            if (questions != null)
                questionDetail.DisplayOrder = questions.Count + 1;
            else
                questionDetail.DisplayOrder = 1;
            List<AnswerChoice> questionAnswerList = new BL.QuestionBLManager().
                GetInterviewQuestionOptions(questionDetail.QuestionKey);
            if (questionAnswerList != null)
                questionDetail.Choice_Desc += questionAnswerList[0].Choice.ToString();

            questions.Add(questionDetail);
            ViewState["TESTDRAFTGRID"] = questions.ToList();

            EditInterviewTestWithAdaptive_testDrftGridView.DataSource = questions;
            EditInterviewTestWithAdaptive_testDrftGridView.DataBind();

            TestDraftSummary testDraftSummary = new TestDraftSummary();
            testDraftSummary.QuestionDetail = questions;
            testDraftSummary.AttributeDetail = new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questions);
            EditInterviewTestWithAdaptive_testSummaryControl.TestDraftSummaryDataSource = testDraftSummary;
            HideLastRecord();
            ResetQuestionImage();
        }

        /// <summary>
        /// Constructs the question details.
        /// </summary>
        /// <returns></returns>
        private QuestionDetail ConstructQuestionDetails()
        {
            // Construct question detail object.
            QuestionDetail questionDetail = new QuestionDetail();

            QuestionBLManager questionBLManager = new QuestionBLManager();

            questionDetail.Question = EditInterviewTestWithAdaptive_addQuestionQuestTextBox.Text.Trim();

            if (EditInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.SelectedItem.Value != null &&
                EditInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.SelectedItem.Value.Trim().ToUpper() != "--SELECT--")
            {
                questionDetail.Complexity = EditInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.SelectedItem.Value;
                questionDetail.ComplexityName = EditInterviewTestWithAdaptive_addQuestioncomplexityDropDownList.SelectedItem.Text;
            }

            //Set question type

            if (EditInterviewTestWithAdaptive_isQuestionRepository.Checked)
                questionDetail.IsActiveFlag = "Y";
            else
                questionDetail.IsActiveFlag = "N";

            // Construct the answer choice.
            List<AnswerChoice> answerChoices = new List<AnswerChoice>();
            AnswerChoice answerChoice = null;


            answerChoice = new AnswerChoice();
            answerChoice.ChoiceID = 1;
            answerChoice.Choice = EditInterviewTestWithAdaptive_addQuestionAnswerTextBox.Text.Trim();
            answerChoices.Add(answerChoice);

            questionDetail.AnswerChoices = answerChoices;
            questionDetail.Author = Convert.ToInt32(base.userID);

            // Additional settings (Test AreaId).

            questionDetail.TestAreaID = EditInterviewTestWithAdaptive_addQuestionRadioButtonList.SelectedValue;
            questionDetail.TestAreaName = EditInterviewTestWithAdaptive_addQuestionRadioButtonList.SelectedItem.Text;

            questionDetail.Tag = EditInterviewTestWithAdaptive_tagsTextBox.Text;

            questionDetail.CreatedBy = Convert.ToInt32(base.userID);
            questionDetail.CreatedDate = DateTime.Now;

            questionDetail.ModifiedBy = Convert.ToInt32(base.userID);

            // Get selected subject ids only. This will be used in QuestionBLManager:InsertQuestionRelation
            // method to insert multiple subjects simultanesouly.
            questionDetail.SelectedSubjectIDs =
                EditInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryHiddenField.Value.ToString();

            if (Session["POSTED_INTERVIEW_QUESTION_IMAGE"] != null)
            {
                questionDetail.QuestionImage = Session["POSTED_INTERVIEW_QUESTION_IMAGE"] as byte[];
                questionDetail.HasImage = true;
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            }
            else
                questionDetail.HasImage = false;

            return questionDetail;
        }

        /// <summary>
        /// This method gets the question id's that are in the draft panel
        /// </summary>
        /// <param name="questionDetails">List of question details that are in draft panel</param>
        /// <param name="ForceLoad">Whether the questions should load though 
        /// questions exceeded the adaptive limit.</param>
        /// <returns>string contains the draft question ids</returns>
        private string GetSelectedQuestionsIds(List<QuestionDetail> questionDetails, bool ForceLoad)
        {
            StringBuilder sbQuestionDetails = null;
            try
            {
                EditInterviewTestWithAdaptive_recommendAdaptiveQuestions.Style.Add("display", "none");
                BindEmptyAdaptiveGridView();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetails))
                    return string.Empty;
                if (questionDetails.Count == 0)
                    return string.Empty;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["ADAPTIVE_LIMIT"]))
                    return "";
                if ((ForceLoad == false) && (Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ADAPTIVE_LIMIT"])
                    < questionDetails.Count))
                {
                    if (EditInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList.SelectedIndex != 1)
                        EditInterviewTestWithAdaptive_recommendAdaptiveQuestions.Style.Add("display", "block");
                    return "";
                }
                else
                    if (EditInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList.SelectedIndex == 1)
                        return "";
                sbQuestionDetails = new StringBuilder();
                for (int i = 0; i < questionDetails.Count; i++)
                {
                    sbQuestionDetails.Append(questionDetails[i].QuestionKey);
                    sbQuestionDetails.Append(",");
                }
                return sbQuestionDetails.ToString().TrimEnd(',');
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(sbQuestionDetails)) sbQuestionDetails = null;
            }
        }

        /// <summary>
        /// This will Make adaptive grid view empty
        /// </summary>
        private void BindEmptyAdaptiveGridView()
        {
            ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE] = null;
            EditInterviewTestWithAdaptive_adaptiveGridView.DataSource = null;
            EditInterviewTestWithAdaptive_adaptiveGridView.DataBind();
            EditInterviewTestWithAdaptive_adaptivebottomPagingNavigator.TotalRecords = 0;
            EditInterviewTestWithAdaptive_adaptivebottomPagingNavigator.Reset();
        }

        /// <summary>
        /// This method communicates with the BL Manager and gets the questions
        /// </summary>
        /// <param name="SelectedQuestionIds">Questions that are in test draft panel</param>
        /// <param name="PageNumber">Page number to load in to the 
        /// adaptive grid.</param>
        private void BindAdaptiveQuestions(string SelectedQuestionIds, int PageNumber)
        {
            if (SelectedQuestionIds == "")
            {
                EditInterviewTestWithAdaptive_adaptiveGridView.DataSource = null;
                EditInterviewTestWithAdaptive_adaptiveGridView.DataBind();
                EditInterviewTestWithAdaptive_adaptiveGridView.Visible = false;
                return;
            }
            int TotalNoOfRecords = 0;
            EditInterviewTestWithAdaptive_adaptiveGridView.DataSource =
                new InterviewBLManager().GetAdaptiveInterviewQuestions(base.tenantID, base.userID,
                SelectedQuestionIds,
                ADAPTIVE_PAGE_SIZE, PageNumber, base.userID, out TotalNoOfRecords);
            EditInterviewTestWithAdaptive_adaptiveGridView.DataBind();
            if (TotalNoOfRecords == 0)
            {
                if (EditInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList.SelectedIndex == 0)
                    EditInterviewTestWithAdaptive_adaptiveGridView.Visible = true;
                else
                    EditInterviewTestWithAdaptive_adaptiveGridView.Visible = false;
                EditInterviewTestWithAdaptive_adaptivebottomPagingNavigator.TotalRecords = 0;
                EditInterviewTestWithAdaptive_adaptivebottomPagingNavigator.Reset();
                EditInterviewTestWithAdaptive_adaptivePagingHiddenField.Value = "1";
            }
            else
            {
                EditInterviewTestWithAdaptive_adaptiveGridView.Visible = true;
                ViewState[ADAPTIVE_QUESTION_DETAILS_VIEWSTATE] =
                    (List<QuestionDetail>)EditInterviewTestWithAdaptive_adaptiveGridView.DataSource;
                EditInterviewTestWithAdaptive_adaptivebottomPagingNavigator.Visible = true;
                EditInterviewTestWithAdaptive_adaptivePagingHiddenField.Value = PageNumber.ToString();
                if (PageNumber == 1)
                    EditInterviewTestWithAdaptive_adaptivebottomPagingNavigator.Reset();
                else
                    EditInterviewTestWithAdaptive_adaptivebottomPagingNavigator.MoveToPage(PageNumber);
                EditInterviewTestWithAdaptive_adaptivebottomPagingNavigator.TotalRecords = TotalNoOfRecords;
                EditInterviewTestWithAdaptive_adaptivebottomPagingNavigator.PageSize = ADAPTIVE_PAGE_SIZE;
            }
        }

        #endregion Adaptive Recommendation Questions Method

        #endregion

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool isQuestionTabActive = false;

            EditInterviewTestWithAdaptive_topErrorMessageLabel.Text = string.Empty;
            EditInterviewTestWithAdaptive_bottomErrorMessageLabel.Text = string.Empty;
            TestDetail testDetail = new TestDetail();
            testDetail = EditInterviewTestWithAdaptive_testDetailsUserControl.TestDetailDataSource;
            int recommendedTime = testDetail.RecommendedCompletionTime;

            questionDetailSearchResultList = new List<QuestionDetail>();
            questionDetailSearchResultList = ViewState["TESTDRAFTGRID"] as List<QuestionDetail>;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(questionDetailSearchResultList))
            {
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                    EditInterviewTestWithAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualInterviewTest_QuestionCount);
                isValidData = false;
                isQuestionTabActive = true;
            }
            else if (questionDetailSearchResultList.Count == 0)
            {
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                    EditInterviewTestWithAdaptive_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateManualInterviewTest_QuestionCount);
                isValidData = false;
                isQuestionTabActive = true;
            }

            if (testDetail.Name.Trim().Length == 0)
            {
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel,
                   "Please enter interview name");
                isValidData = false;
            }
            if (testDetail.Description.Trim().Length == 0)
            {
                base.ShowMessage(EditInterviewTestWithAdaptive_topErrorMessageLabel,
                   EditInterviewTestWithAdaptive_bottomErrorMessageLabel,
                   "Please enter interview description");
                isValidData = false;
            }

            if (isQuestionTabActive)
                EditInterviewTestWithAdaptive_mainTabContainer.ActiveTab = EditInterviewTestWithAdaptive_questionsTabPanel;
            else
                EditInterviewTestWithAdaptive_mainTabContainer.ActiveTab = EditInterviewTestWithAdaptive_testdetailsTabPanel;

            EditInterviewTestWithAdaptive_testDetailsUserControl.TestDetailDataSource = testDetail;
            return isValidData;
        }
        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindTestArea();
            BindComplexities();
        }
        protected void EditInterviewTestWithAdaptive_addQuestionTopCancelImageButton_Click(object sender, ImageClickEventArgs e)
        {
            ResetQuestionImage();
        }
        protected void EditInterviewTestWithAdaptive_addQuestioCloseLinkButton_Click(object sender, EventArgs e)
        {
            ResetQuestionImage();
        }
        #endregion Protected Overridden Methods

        
}
}
