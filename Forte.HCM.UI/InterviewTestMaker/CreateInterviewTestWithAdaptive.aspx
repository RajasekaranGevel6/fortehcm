<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    CodeBehind="CreateInterviewTestWithAdaptive.aspx.cs" Inherits="Forte.HCM.UI.InterviewTestMaker.CreateInterviewTestWithAdaptive" %>

<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/QuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc3" %>
<%@ Register Src="../CommonControls/InterviewQuestionDetailSummaryControl.ascx" TagName="QuestionDetailSummaryControl"
    TagPrefix="uc7" %>
<%@ Register Src="../CommonControls/InterviewTestDetailsControl.ascx" TagName="TestDetailsControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/ManualInterviewTestSummaryControl.ascx" TagName="ManualTestSummaryControl"
    TagPrefix="uc5" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc6" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ContentPlaceHolderID="InterviewMaster_body" ID="CreateInterviewTestWithAdaptive_content"
    runat="server">
    <script type="text/javascript" language="javascript">

        var control;
        function setFocus(sender, e) {
            try {
                var activeTab = $find('<%=CreateInterviewTestWithAdaptive_mainTabContainer.ClientID%>').get_activeTabIndex();
                if (activeTab == "0") {
                    try {
                        control = $get('<%=CreateInterviewTestWithAdaptive_categoryTextBox.ClientID%>');
                        control.focus();
                    }
                    catch (er1) {
                        try {
                            control = $get('<%=CreateInterviewTestWithAdaptive_keywordTextBox.ClientID%>');
                            control.focus();
                        }
                        catch (er2) {
                        }
                    }
                }
                else {
                    control = $get('ctl00_InterviewMaster_body_CreateInterviewTestWithAdaptive_mainTabContainer_CreateInterviewTestWithAdaptive_testdetailsTabPanel_CreateInterviewTestWithAdaptive_testDetailsUserControl_TestDetailsControl_testNameTextBox');
                    control.focus();
                }
            }
            catch (er) {
            }
        }
        function ShowAddImagePanel() {
            var addImageTR = document.getElementById('<%=CreateInterviewTestWithAdaptive_addQuestionImageTR.ClientID%>');
            addImageTR.style.display = "";
            var addImageBtn = document.getElementById('<%=CreateInterviewTestWithAdaptive_addImageLinkButton.ClientID%>');
            addImageBtn.style.display = "none";
            return false;
        }
    </script>
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">

        // Display  Or Hide the adaptive TestDraft Or Manual test Draft Questions.
        function ShowOrHideDiv(caseValue) {
            switch (caseValue.toString()) {
                case '0':
                    {
                        document.getElementById("<%= CreateInterviewTestWithAdaptive_resultsHiddenField.ClientID %>").value = "0";
                        document.getElementById("CreateInterviewTestWithAdaptive_manualDiv").style.display = "block";
                        document.getElementById("CreateInterviewTestWithAdaptive_adaptivelDiv").style.display = "none";
                        document.getElementById("CreateInterviewTestWithAdaptive_manualDiv").style.width = "100%";
                        document.getElementById("CreateInterviewTestWithAdaptive_adaptiveTD").style.display = "none";
                        document.getElementById("<%= CreateInterviewTestWithAdaptive_questionDiv.ClientID %>").style.width = "936px";
                        document.getElementById("<%= CreateInterviewTestWithAdaptive_adaptiveQuestionDiv.ClientID %>").style.width = "0";
                        break;
                    }
                case '1':
                    {
                        document.getElementById("<%= CreateInterviewTestWithAdaptive_resultsHiddenField.ClientID %>").value = "1";
                        document.getElementById("CreateInterviewTestWithAdaptive_manualDiv").style.display = "none";
                        document.getElementById("CreateInterviewTestWithAdaptive_adaptivelDiv").style.display = "block";
                        document.getElementById("CreateInterviewTestWithAdaptive_manualDiv").style.width = "0";
                        document.getElementById("CreateInterviewTestWithAdaptive_adaptiveTD").style.display = "block";
                        document.getElementById("CreateInterviewTestWithAdaptive_adaptivelDiv").style.width = "100%";

                        document.getElementById("<%= CreateInterviewTestWithAdaptive_questionDiv.ClientID %>").style.width = "0";
                        document.getElementById("<%= CreateInterviewTestWithAdaptive_adaptiveQuestionDiv.ClientID %>").style.width = "936px";
                        break;
                    }
                case '2':
                    {
                        document.getElementById("<%= CreateInterviewTestWithAdaptive_resultsHiddenField.ClientID %>").value = "2";
                        document.getElementById("CreateInterviewTestWithAdaptive_manualDiv").style.display = "block";
                        document.getElementById("CreateInterviewTestWithAdaptive_adaptivelDiv").style.display = "block";
                        document.getElementById("CreateInterviewTestWithAdaptive_manualDiv").style.width = "49%";
                        document.getElementById("CreateInterviewTestWithAdaptive_adaptiveTD").style.display = "block";
                        document.getElementById("CreateInterviewTestWithAdaptive_adaptivelDiv").style.width = "50%";

                        document.getElementById("<%= CreateInterviewTestWithAdaptive_questionDiv.ClientID %>").style.width = "450px";
                        document.getElementById("<%= CreateInterviewTestWithAdaptive_adaptiveQuestionDiv.ClientID %>").style.width = "456px";
                        break;
                    }
                default:
                    {
                        document.getElementById("<%= CreateInterviewTestWithAdaptive_resultsHiddenField.ClientID %>").value = "0";
                        document.getElementById("CreateInterviewTestWithAdaptive_manualDiv").style.display = "block";
                        document.getElementById("CreateInterviewTestWithAdaptive_adaptivelDiv").style.display = "block";
                        document.getElementById("CreateInterviewTestWithAdaptive_adaptiveTD").style.display = "block";
                        document.getElementById("CreateInterviewTestWithAdaptive_manualDiv").style.width = "49%";
                        document.getElementById("CreateInterviewTestWithAdaptive_adaptivelDiv").style.width = "50%";

                        document.getElementById("<%= CreateInterviewTestWithAdaptive_questionDiv.ClientID %>").style.width = "450px";
                        document.getElementById("<%= CreateInterviewTestWithAdaptive_adaptiveQuestionDiv.ClientID %>").style.width = "456px";
                        break;
                    }
            }
            return false;
        }

        var modifiedRow = null;
        var dataCollection = new Array();
        dataCollection = null;
        var sourceRow = null;
        var sourcePosition = null;
        var sourceTable = null;
        var currenttable = null;
        var dropTargets = null;
        var dragObject = null;
        var mouseOffSet = null;
        var destTable = null;
        var oldY = 0;
        this.table = null;

        //move the Single Question add to Testdraft.
        function singleqnsclick(QuestionID, buttonID) {
            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
            hiddenvalue.value = QuestionID;
            __doPostBack(buttonID, "OnClick");
        }

        //related to the Drag and Drop questions 
        function mousedown(QuestionID, source) {
            sourceTable = document.getElementById(source);

            if (checkChecked(QuestionID)) {
                CheckNoOfChecked(QuestionID);
            }
            else {
                CheckCorrectQuestion(QuestionID);
            }
        }

        //related to the Drag and Drop questions 
        function CheckCorrectQuestion(id) {
            var table = sourceTable;
            var testVar = document.getElementById('<%=CreateInterviewTestWithAdaptive_searchQuestionGridView.ClientID %>');
            var testVarTwo = document.getElementById('<%=CreateInterviewTestWithAdaptive_adaptiveGridView.ClientID %>');
            id++;
            var rowid = 1;
            var chkbox;
            var rowCount = table.rows.length;
            for (var i = 1; i < rowCount; i++) {
                if (id == i) {
                    var row = table.rows[i];
                    if (detectBrowser()) {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else {
                        chkbox = row.cells[0].childNodes[0];
                    }

                    if (null != chkbox) {
                        chkbox.checked = true;
                        // row.cells[0].childNodes[0].checked = true;
                    }
                }
                if ((testVar == sourceTable) || (testVarTwo == sourceTable)) {
                    CheckNoOfChecked(id);
                }
            }
        }

        //find the Browser details
        function detectBrowser() {
            var browser = navigator.appName;
            var b_version = navigator.appVersion;
            var version = parseFloat(b_version);
            if (browser == "Netscape") {
                return true;
            }
            else {
                return false; ;
            }
        }
        // Question remove from the Test Draft.
        function RemoveRows(rowIndex) {
            sourceTable = document.getElementById('<%=CreateInterviewTestWithAdaptive_testDrftGridView.ClientID %>');

            if (confirm('Are you sure to delete these question(s)?')) {
                if (checkChecked(rowIndex)) {
                    DelRows();

                }
                else {
                    CheckCorrectQuestion(rowIndex);
                    DelRows();
                }
            }
            else
                return false;
        }

        function DelRows() {
            try {
                var table = document.getElementById('<%=CreateInterviewTestWithAdaptive_testDrftGridView.ClientID %>');
                var rowCount = table.rows.length;
                for (var i = 0; i < rowCount; i++) {
                    var row = table.rows[i];
                    var chkbox;
                    if (detectBrowser()) {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else {
                        chkbox = row.cells[0].childNodes[0];
                    }
                    if (null != chkbox && true == chkbox.checked) {
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                    }
                }
            } catch (e) {
                alert(e);
            }
            return false;
        }

        function CheckNoOfChecked(QuestionID) {

            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');

            hiddenvalue.value = "";

            var numberOfQuestions = 0;

            var table = sourceTable;
            // document.getElementById('<%=CreateInterviewTestWithAdaptive_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {

                    numberOfQuestions = numberOfQuestions + 1;

                    var noOfQuestionsTextBox = document.getElementById('<%=sampleDIVTextBox.ClientID %>');

                    noOfQuestionsTextBox.value = 'Move ' + numberOfQuestions + ' question(s)';

                    dragObject = document.getElementById('<%=moveDisplayDIV.ClientID %>');

                    if (detectBrowser()) {
                        if (hiddenvalue.value == "") {
                            hiddenvalue.value = row.childNodes[5].childNodes[1].defaultValue + ',';
                        }
                        else {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[5].childNodes[1].defaultValue + ',';
                        }
                    }
                    else {
                        if (hiddenvalue.value == "") {
                            hiddenvalue.value = row.childNodes[4].childNodes[0].value + ',';
                        }
                        else {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[4].childNodes[0].value + ',';
                        }
                    }
                }

            }
            //var ShowDiv =
        }

        function checkChecked(id) {

            var isChecked = false;

            var table = sourceTable;
            // document.getElementById('<%=CreateInterviewTestWithAdaptive_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {
                    isChecked = true;
                }

                if (isChecked) {
                    return true;
                }
                else {
                    return false;
                }
            }

        }

        function mouseMove(ev) {
            ev = ev || window.event;
            var mousePos = mouseCoords(ev);

            if (dragObject) {
                //mouseOffset = getMouseOffset(dragObject, ev);
                dragObject.style.display = 'block';
                dragObject.style.backgroundColor = "#eee";
                dragObject.style.position = 'absolute';
                if (detectBrowser()) {
                    dragObject.style.top = mousePos.y + 'px';
                    dragObject.style.left = mousePos.x + 'px';
                }
                else {
                    dragObject.style.top = mousePos.y + document.documentElement.scrollTop + 10;
                    dragObject.style.left = mousePos.x + 10;
                }
            }
            else {

            }
            return false;
        }
        function mouseCoords(ev) {
            if (ev != null) {
                if (ev.pageX || ev.pageY) { return { x: ev.pageX, y: ev.pageY }; }
                return {
                    x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
                    y: ev.clientY + document.body.scrollTop - document.body.clientTop
                };
            }
        }

        function srsmouseup(ev) {
            if (dragObject == null)
                return false;
            var ev = ev || window.event;
            var mousePos = mouseCoords(ev);
            var curTarget = document.getElementById('<%=CreateInterviewTestWithAdaptive_testDrftGridView.ClientID %>');
            var buttonID = document.getElementById('<%=CreateInterviewTestWithAdaptive_selectMultipleImage.ClientID %>');
            var targPos = getPosition(curTarget);
            var scrolltargPos;
            if (detectBrowser()) {
                scrolltargPos = targPos.y;
            }
            else {
                scrolltargPos = targPos.y - document.documentElement.scrollTop;
            }

            var targWidth = parseInt(curTarget.offsetWidth);
            var targHeight = parseInt(curTarget.offsetHeight);
            if (
            (mousePos.x >= targPos.x) &&
            (mousePos.y >= scrolltargPos)) {

                // __doPostBack('ctl00_HCMMaster_body_CreateInterviewTestWithAdaptive_mainTabContainer_CreateInterviewTestWithAdaptive_questionsTabPanel_CreateInterviewTestWithAdaptive_selectMultipleImage', "OnClick");
                __doPostBack('<%=CreateInterviewTestWithAdaptive_selectMultipleImage.ClientID %>', "OnClick");

                dragObject.style.display = 'none';
            }
            else {
                if (dragObject != null) {
                    makeUnchekabale();
                    dragObject.style.display = 'none';

                    var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
                    hiddenvalue.value = "";
                }
            } dragObject = null;
        }

        function makeUnchekabale() {
            var table = sourceTable;
            //document.getElementById('<%=CreateInterviewTestWithAdaptive_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {
                    chkbox.checked = false;
                }
            }
        }

        function getPosition(e) {
            var left = 0;
            var top = 0;

            if (e != null) {
                while (e.offsetParent) {
                    left += e.offsetLeft;
                    top += e.offsetTop;
                    e = e.offsetParent;
                }
                left += e.offsetLeft;
                top += e.offsetTop;
            }

            return { x: left, y: top };
        }
    </script>
    <asp:Panel ID="CreateInterviewTestWithAdaptive_mainPanel" runat="server" DefaultButton="CreateInterviewTestWithAdaptive_bottomSaveButton">
        <table width="100%" border="0" cellspacing="3" cellpadding="0">
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="72%" class="header_text_bold">
                                <asp:Literal ID="CreateInterviewTestWithAdaptive_headerLiteral" runat="server" Text="Create Interview"></asp:Literal>
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td width="22%">
                                        </td>
                                        <td width="40%" align="right">
                                            <asp:Button ID="CreateInterviewTestWithAdaptive_topSaveButton" runat="server" SkinID="sknButtonId"
                                                Text="Save" OnClick="CreateInterviewTestWithAdaptive_saveButton_Click" />
                                            <asp:Button ID="CreateInterviewTestWithAdaptive_topCreateSessionButton" runat="server"
                                                SkinID="sknButtonId" Text="Create Session" Visible="false" />
                                        </td>
                                        <td width="16%" align="right">
                                            <asp:LinkButton ID="CreateInterviewTestWithAdaptive_topResetLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Reset" OnClick="CreateInterviewTestWithAdaptive_resetLinkButton_Click" />
                                        </td>
                                        <td width="4%" align="center">
                                            |
                                        </td>
                                        <td width="18%" align="left">
                                            <asp:LinkButton ID="CreateInterviewTestWithAdaptive_topCancelLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="CreateInterviewTestWithAdaptive_topMessageUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="CreateInterviewTestWithAdaptive_adaptiveRecommendationsClicked"
                                runat="server" />
                            <asp:Label ID="CreateInterviewTestWithAdaptive_topSuccessMessageLabel" runat="server"
                                SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="CreateInterviewTestWithAdaptive_topErrorMessageLabel" runat="server"
                                SkinID="sknErrorMessage"></asp:Label><asp:HiddenField ID="CreateInterviewTestWithAdaptive_stateHiddenField"
                                    runat="server" Value="0" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <ajaxToolKit:TabContainer ID="CreateInterviewTestWithAdaptive_mainTabContainer" runat="server"
                        ActiveTabIndex="0" OnClientActiveTabChanged="setFocus">
                        <ajaxToolKit:TabPanel ID="CreateInterviewTestWithAdaptive_questionsTabPanel" HeaderText="questions"
                            runat="server">
                            <HeaderTemplate>
                                Questions
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:Panel ID="CreateInterviewTestWithAdaptive_testQuestionDetailsPanel" runat="server"
                                    DefaultButton="CreateInterviewTestWithAdaptive_topSearchButton">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div id="CreateInterviewTestWithAdaptive_searchCriteriasDiv" runat="server" style="display: block;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="right" class="td_padding_bottom_5">
                                                                <asp:UpdatePanel ID="CreateInterviewTestWithAdaptive_simpleLinkButtonUpdatePanel"
                                                                    runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="CreateInterviewTestWithAdaptive_simpleLinkButton" runat="server"
                                                                            Text="Advanced" SkinID="sknActionLinkButton" OnClick="CreateInterviewTestWithAdaptive_simpleLinkButton_Click" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="panel_bg">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td width="100%">
                                                                            <asp:UpdatePanel ID="CreateInterviewTestWithAdaptive_searchDiv_UpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <div id="CreateInterviewTestWithAdaptive_simpleSearchDiv" runat="server">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="CreateInterviewTestWithAdaptive_categoryHeadLabel" runat="server"
                                                                                                        Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="CreateInterviewTestWithAdaptive_categoryTextBox" runat="server"
                                                                                                        MaxLength="50" TabIndex="0"></asp:TextBox>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CreateInterviewTestWithAdaptive_subjectHeadLabel" runat="server" Text="Subject"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="CreateInterviewTestWithAdaptive_subjectTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CreateInterviewTestWithAdaptive_keywordsHeadLabel" runat="server"
                                                                                                        Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <div style="float: left; padding-right: 5px;">
                                                                                                        <asp:TextBox ID="CreateInterviewTestWithAdaptive_keywordsTextBox" runat="server"
                                                                                                            MaxLength="100"></asp:TextBox></div>
                                                                                                    <div style="float: left;">
                                                                                                        <asp:ImageButton ID="CreateInterviewTestWithAdaptive_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div id="CreateInterviewTestWithAdaptive_advanceSearchDiv" runat="server">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <uc1:CategorySubjectControl ID="CreateInterviewTestWithAdaptive_categorySubjectControl"
                                                                                                        runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="td_height_8">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="panel_inner_body_bg">
                                                                                                    <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td style="width: 10%">
                                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_testAreaHeadLabel" runat="server"
                                                                                                                    Text="Interview Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="5" class="checkbox_list_bg" align="left" style="width: 90%">
                                                                                                                <asp:CheckBoxList ID="CreateInterviewTestWithAdaptive_testAreaCheckBoxList" runat="server"
                                                                                                                    RepeatColumns="3" RepeatDirection="Vertical" CellSpacing="5" Width="100%">
                                                                                                                </asp:CheckBoxList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 10%">
                                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_complexityLabel" runat="server" Text="Complexity"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="3" align="left" valign="middle">
                                                                                                                <div style="float: left; padding-right: 5px; width: 90%; vertical-align: middle;"
                                                                                                                    class="checkbox_list_bg">
                                                                                                                    <asp:CheckBoxList ID="CreateInterviewTestWithAdaptive_complexityCheckBoxList" runat="server"
                                                                                                                        RepeatColumns="3" RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                                    </asp:CheckBoxList>
                                                                                                                </div>
                                                                                                                <div style="float: left; vertical-align: middle; padding-left: 5px; padding-top: 5px;">
                                                                                                                    <asp:ImageButton ID="CreateInterviewTestWithAdaptive_complexityImageButton" SkinID="sknHelpImageButton"
                                                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, ComplexityHelp %>" />
                                                                                                                </div>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_keywordHeadLabel" runat="server" Text="Keyword"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="CreateInterviewTestWithAdaptive_keywordTextBox" runat="server" Columns="50"
                                                                                                                        MaxLength="100"></asp:TextBox></div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="CreateInterviewTestWithAdaptive_keywordImageButton" SkinID="sknHelpImageButton"
                                                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" />
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_questionLabel" runat="server" Text="Question ID"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="CreateInterviewTestWithAdaptive_questionIDTextBox" runat="server"
                                                                                                                    MaxLength="15"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_creditHeadLabel" runat="server" Text="Credit"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="CreateInterviewTestWithAdaptive_creditTextBox" runat="server"></asp:TextBox>
                                                                                                                <ajaxToolKit:FilteredTextBoxExtender ID="CreateInterviewTestWithAdaptive_weightageFileteredExtender"
                                                                                                                    runat="server" TargetControlID="CreateInterviewTestWithAdaptive_creditTextBox"
                                                                                                                    FilterType="Numbers" Enabled="True">
                                                                                                                </ajaxToolKit:FilteredTextBoxExtender>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_authorHeadLabel" runat="server" Text="Author"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="CreateInterviewTestWithAdaptive_authorTextBox" runat="server"></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="CreateInterviewTestWithAdaptive_dummyAuthorID" runat="server" />
                                                                                                                </div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="CreateInterviewTestWithAdaptive_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                                                        runat="server" ImageAlign="Middle" /></div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManulTest_positionProfileLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                    Text="Position Profile"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="3">
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="CreateManulTest_positionProfileTextBox" runat="server" MaxLength="200"
                                                                                                                        Columns="55"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="CreateManulTest_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to select position profile" /></div>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManulTest_positionProfileKeywordLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                    Text="Position Profile Keyword"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px; width: 100%">
                                                                                                                    <asp:TextBox ID="CreateManulTest_positionProfileKeywordTextBox" runat="server" MaxLength="500"></asp:TextBox>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                            <div style="display: none">
                                                                                                            </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        </td> </tr> </table>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" class="td_padding_top_5">
                                                                            <asp:Button ID="CreateInterviewTestWithAdaptive_topSearchButton" runat="server" Text="Search"
                                                                                SkinID="sknButtonId" OnClick="CreateInterviewTestWithAdaptive_searchQuestionButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr id="CreateInterviewTestWithAdaptive_searchResultsTR" runat="server">
                                            <td id="Td1" class="header_bg" runat="server">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 80%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="CreateInterviewTestWithAdaptive_searchResultsLiteral" runat="server"
                                                                Text="Questions"></asp:Literal>
                                                            &#160;<asp:Label ID="CreateInterviewTestWithAdaptive_sortHelpLabel" runat="server"
                                                                SkinID="sknLabelText"></asp:Label>
                                                            <asp:HiddenField ID="CreateInterviewTestWithAdaptive_resultsHiddenField" runat="server" />
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="CreateInterviewTestWithAdaptive_manualImageButton" runat="server"
                                                                SkinID="sknManualImageButton" ToolTip="Click here to view 'Search Results' only"
                                                                OnClientClick="javascript:return ShowOrHideDiv('0');" />
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="CreateInterviewTestWithAdaptive_adaptiveImageButton" runat="server"
                                                                SkinID="sknAdaptiveImageButton" ToolTip="Click here to view 'Adaptive Recommended Questions' only"
                                                                OnClientClick="javascript:return ShowOrHideDiv('1');" />
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="CreateInterviewTestWithAdaptive_bothImageButton" runat="server"
                                                                SkinID="sknCombinedAMImageButton" ToolTip="Click here to view both 'Search Results' and 'Adaptive Recommended Questions'"
                                                                OnClientClick="javascript:return ShowOrHideDiv('2');" />
                                                        </td>
                                                        <td style="width: 5%" align="right">
                                                            <span id="CreateInterviewTestWithAdaptive_searchResultsUpSpan" runat="server" style="display: none;">
                                                                <asp:Image ID="CreateInterviewTestWithAdaptive_searchResultsUpImage" runat="server"
                                                                    SkinID="sknMinimizeImage" />
                                                            </span><span id="CreateInterviewTestWithAdaptive_searchResultsDownSpan" runat="server"
                                                                style="display: block;">
                                                                <asp:Image ID="CreateInterviewTestWithAdaptive_searchResultsDownImage" runat="server"
                                                                    SkinID="sknMaximizeImage" />
                                                            </span>
                                                            <asp:HiddenField ID="CreateInterviewTestWithAdaptive_restoreHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" id="CreateInterviewTestWithAdaptive_adaptiveTD">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="CreateInterviewTestWithAdaptive_adaptiveQuestionsHeadLabel" runat="server"
                                                                Text="Adaptive Recommendations" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td align="right">
                                                            <asp:UpdatePanel ID="CreateInterviewTestWithAdaptive_adaptiveYesNoUpdatePanel" runat="server">
                                                                <ContentTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:RadioButtonList ID="CreateInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList"
                                                                                    runat="server" RepeatColumns="2" OnSelectedIndexChanged="CreateInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList_SelectedIndexChanged"
                                                                                    AutoPostBack="true">
                                                                                    <asp:ListItem Text="On" Value="0"></asp:ListItem>
                                                                                    <asp:ListItem Text="Off" Value="1" Selected="True"></asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button ID="CreateInterviewTestWithAdaptive_recommendAdaptiveQuestions" runat="server"
                                                                                    Text="Recommend Questions" SkinID="sknButtonId" OnClick="CreateInterviewTestWithAdaptive_recommendAdaptiveQuestions_Click" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td valign="top">
                                                <div id="CreateInterviewTestWithAdaptive_manualDiv" style="width: 49%; float: left;
                                                    display: block;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="grid_header_bg">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="header_text_bold">
                                                                            <asp:Literal ID="CreateInterviewTestWithAdaptive_manualQuestLiteral" runat="server"
                                                                                Text="Search Results"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="grid_body_bg">
                                                                <asp:UpdatePanel ID="CreateInterviewTestWithAdaptive_searchQuestionGridView_UpdatePanel"
                                                                    runat="server">
                                                                    <ContentTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="height: 180px; width: 100%; overflow: auto;" runat="server" id="CreateInterviewTestWithAdaptive_questionDiv">
                                                                                        <asp:GridView ID="CreateInterviewTestWithAdaptive_searchQuestionGridView" runat="server"
                                                                                            AutoGenerateColumns="False" OnRowDataBound="CreateInterviewTestWithAdaptive_searchQuestionGridView_RowDataBound"
                                                                                            OnRowCommand="CreateInterviewTestWithAdaptive_searchQuestionGridView_RowCommand">
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:CheckBox ID="CreateInterviewTestWithAdaptive_searchQuestionCheckbox" runat="server" />
                                                                                                        <asp:HiddenField ID="CreateInterviewTestWithAdaptive_questionKeyHiddenField" runat="server"
                                                                                                            Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateInterviewTestWithAdaptive_previewImageButton" runat="server"
                                                                                                            SkinID="sknPreviewImageButton" CommandName="view" CommandArgument='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>'
                                                                                                            ToolTip="Question Details Summary" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateInterviewTestWithAdaptive_searchQuestionAlertImageButton"
                                                                                                            runat="server" SkinID="sknAlertImageButton" ToolTip="Flag Question" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateInterviewTestWithAdaptive_searchQuestionSelectImage" runat="server"
                                                                                                            SkinID="sknMoveDown_ArrowImageButton" CommandName="Select" ToolTip="Select Question" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="CreateInterviewTestWithAdaptive_hidddenValue" runat="server"
                                                                                                            Value='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField DataField="QuestionID" Visible="False"></asp:BoundField>
                                                                                                <asp:BoundField DataField="QuestionRelationID" Visible="False"></asp:BoundField>
                                                                                                <asp:TemplateField HeaderText="Question">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateInterviewTestWithAdaptive_questionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),65) %>'
                                                                                                            Width="320px"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Category">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateInterviewTestWithAdaptive_categoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                                            Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Subject">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateInterviewTestWithAdaptive_subjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                                            Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <%--<asp:TemplateField HeaderText="Answer">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateInterviewTestWithAdaptive_answerLabel" runat="server" Text='<%# TrimContent(Eval("AnswerID").ToString(),20) %>'
                                                                                                            Width="120px" ToolTip='<%# Eval("AnswerID") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>--%>
                                                                                                <asp:TemplateField HeaderText="Complexity">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateInterviewTestWithAdaptive_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                                            Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <uc2:PageNavigator ID="CreateInterviewTestWithAdaptive_bottomPagingNavigator" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="CreateInterviewTestWithAdaptive_topSearchButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="moveDisplayDIV" style="display: none; background-color: Lime" runat="server">
                                                    <asp:TextBox ID="sampleDIVTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                                                </div>
                                                <div id="CreateInterviewTestWithAdaptive_adaptivelDiv" style="width: 50%; float: right;
                                                    display: block;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="grid_header_bg">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="header_text_bold">
                                                                            <asp:Literal ID="CreateInterviewTestWithAdaptive_adaptiveQuestLiteral" runat="server"
                                                                                Text="Adaptive Recommended Questions"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                            <td class="grid_body_bg">
                                                                <asp:UpdatePanel ID="CreateInterviewTestWithAdaptive_adaptiveUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="height: 180px; width: 0px; overflow: auto;" runat="server" id="CreateInterviewTestWithAdaptive_adaptiveQuestionDiv">
                                                                                        <asp:GridView ID="CreateInterviewTestWithAdaptive_adaptiveGridView" runat="server"
                                                                                            AutoGenerateColumns="False" OnRowDataBound="CreateInterviewTestWithAdaptive_adaptiveGridView_RowDataBound"
                                                                                            OnRowCommand="CreateInterviewTestWithAdaptive_adaptiveGridView_RowCommand">
                                                                                            <EmptyDataTemplate>
                                                                                                <center>
                                                                                                    <asp:Label ID="CreateInterviewTestWithAdaptive_adaptiveEmptyLabel" runat="server"
                                                                                                        Text="No question found for recommendation" SkinID="sknErrorMessage"></asp:Label>
                                                                                                </center>
                                                                                            </EmptyDataTemplate>
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:CheckBox ID="CreateInterviewTestWithAdaptive_adaptiveCheckbox" runat="server" />
                                                                                                        <asp:HiddenField ID="CreateInterviewTestWithAdaptive_adaptiveQuestionKeyHiddenField"
                                                                                                            runat="server" Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateInterviewTestWithAdaptive_adaptivepreviewImageButton"
                                                                                                            runat="server" SkinID="sknPreviewImageButton" CommandName="view" CommandArgument='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>'
                                                                                                            ToolTip="Question Details Summary" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateInterviewTestWithAdaptive_adaptiveAlertImageButton" runat="server"
                                                                                                            SkinID="sknAlertImageButton" ToolTip="Flag Question" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateInterviewTestWithAdaptive_adaptiveSelectImage" runat="server"
                                                                                                            SkinID="sknMoveDown_ArrowImageButton" CommandName="Select" ToolTip="Select Question" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="CreateInterviewTestWithAdaptive_adaptiveQuestionhiddenValue"
                                                                                                            runat="server" Value='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField DataField="QuestionID" Visible="False"></asp:BoundField>
                                                                                                <asp:BoundField DataField="QuestionRelationID" Visible="False"></asp:BoundField>
                                                                                                <asp:TemplateField HeaderText="Question">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateInterviewTestWithAdaptive_adaptiveQuestionLabel" runat="server"
                                                                                                            Text='<%# TrimContent(Eval("Question").ToString(),65) %>' Width="320px"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Category">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateInterviewTestWithAdaptive_adaptiveCategoryLabel" runat="server"
                                                                                                            Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>' Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Subject">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateInterviewTestWithAdaptive_adaptiveSubjectLabel" runat="server"
                                                                                                            Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>' Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <%--<asp:TemplateField HeaderText="Answer">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateInterviewTestWithAdaptive_adaptiveAnswerLabel" runat="server" Text='<%# TrimContent(Eval("AnswerID").ToString(),20) %>'
                                                                                                            Width="120px" ToolTip='<%# Eval("AnswerID") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>--%>
                                                                                                <asp:TemplateField HeaderText="Complexity">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateInterviewTestWithAdaptive_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                                            Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                            <%--<Columns>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="CreateInterviewTestWithAdaptive_adaptiveCheckbox" runat="server" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="CreateInterviewTestWithAdaptive_adptivepreviewImageButton" runat="server" SkinID="sknPreviewImageButton"
                                                                                            CommandName="view" ToolTip="Preview Question" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="CreateInterviewTestWithAdaptive_adaptiveAlertImageButton" runat="server" SkinID="sknAlertImageButton"
                                                                                            ToolTip="Flag Question" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="CreateInterviewTestWithAdaptive_adaptiveSelectImage" runat="server" SkinID="sknMoveDown_ArrowImageButton"
                                                                                            ToolTip="Select Question" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:HiddenField ID="CreateInterviewTestWithAdaptive_hidddenValue" runat="server" Value='<%#Eval("QuestionID") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="CreateInterviewTestWithAdaptive_adaptiveQuestionDetailsImage" runat="server"
                                                                                            SkinID="sknDraftImageButton" ToolTip="Adaptive Question Details" CommandName="AdaptiveQuestion" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="QuestionID" Visible="False"></asp:BoundField>
                                                                                <asp:BoundField DataField="QuestionRelationID" Visible="False"></asp:BoundField>
                                                                                <asp:TemplateField HeaderText="Question">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="CreateInterviewTestWithAdaptive_questionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),70) %>'></asp:Label>
                                                                                        <asp:Panel ID="CreateInterviewTestWithAdaptive_hoverPanel" runat="server" CssClass="popupMenu">
                                                                                            <table width="100%" border="0" cellspacing="2" cellpadding="3" class="table_outline_bg">
                                                                                                <tr>
                                                                                                    <td class="popup_question_icon">
                                                                                                        <asp:Label ID="CreateInterviewTestWithAdaptive_questionDetailPreviewControlQuestionLabel" Text='<%# Eval("Question") %>'
                                                                                                            runat="server" SkinID="sknLabelText"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="td_padding_left_20">
                                                                                                        <asp:RadioButtonList ID="CreateInterviewTestWithAdaptive_questionPreviewControlAnswerRadioButtonList"
                                                                                                            runat="server" RepeatColumns="1" RepeatDirection="Horizontal" CellSpacing="5"
                                                                                                            TextAlign="Right" Width="100%">
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </asp:Panel>
                                                                                        <ajaxToolKit:HoverMenuExtender ID="CreateInterviewTestWithAdaptive_hoverMenuExtender1" runat="server"
                                                                                            PopupControlID="CreateInterviewTestWithAdaptive_hoverPanel" PopupPosition="Left" HoverCssClass="popupHover"
                                                                                            TargetControlID="CreateInterviewTestWithAdaptive_hoverPanel" PopDelay="50" />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Wrap="False" />
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ReadOnly="True" HeaderText="Category" InsertVisible="False" DataField="CategoryName">
                                                                                    <ItemStyle HorizontalAlign="Left" Width="140px"></ItemStyle>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField ReadOnly="True" HeaderText="Subject" InsertVisible="False" DataField="SubjectName">
                                                                                    <ItemStyle HorizontalAlign="Left" Width="140px" Wrap="False"></ItemStyle>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField ReadOnly="True" HeaderText="Answer" InsertVisible="False" DataField="AnswerID">
                                                                                    <ItemStyle HorizontalAlign="Left" Width="120px" Wrap="False"></ItemStyle>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField ReadOnly="True" HeaderText="Complexity" InsertVisible="False" DataField="ComplexityName">
                                                                                    <ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
                                                                                </asp:BoundField>
                                                                            </Columns>--%>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                    <asp:HiddenField ID="CreateInterviewTestWithAdaptive_adaptiveGridViewHiddenField"
                                                                                        runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:HiddenField ID="CreateInterviewTestWithAdaptive_adaptivePagingHiddenField" runat="server"
                                                                                        Value="1" />
                                                                                    <uc2:PageNavigator ID="CreateInterviewTestWithAdaptive_adaptivebottomPagingNavigator"
                                                                                        runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td align="center" class="grid_header_bg">
                                                <table width="10%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="CreateInterviewTestWithAdaptive_selectMultipleImage" runat="server"
                                                                SkinID="sknMultiDown_ArrowImageButton" CommandName="Select" ToolTip="Move questions to interview draft"
                                                                OnClick="CreateInterviewTestWithAdaptive_selectMultipleImage_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="CreateInterviewTestWithAdaptive_removeMultipleImage" runat="server"
                                                                SkinID="sknMultiUp_ArrowImageButton" CommandName="Select" ToolTip="Remove questions from interview draft"
                                                                OnClick="CreateInterviewTestWithAdaptive_removeMultipleImage_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="CreateInterviewTestWithAdaptive_testDrftGridView_UpdatePanel"
                                                    runat="server">
                                                    <ContentTemplate>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                                <td class="grid_header_bg">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="header_text_bold">
                                                                                <asp:Literal ID="CreateInterviewTestWithAdaptive_testDraftLiteral" runat="server"
                                                                                    Text="Interview Draft"></asp:Literal>&nbsp;
                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_testDrafttHelpLabel" runat="server"
                                                                                    SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"
                                                                                    Visible="false"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr onmouseup="javascript:srsmouseup(event);" onmousemove="javascript:mouseMove(event);"
                                                                style="height: 35px">
                                                                <td class="grid_body_bg" valign="top">
                                                                    <asp:GridView ID="CreateInterviewTestWithAdaptive_testDrftGridView" OnRowDataBound="CreateInterviewTestWithAdaptive_testDrftGridView_RowDataBound"
                                                                        runat="server" OnRowCommand="CreateInterviewTestWithAdaptive_testDrftGridView_RowCommand"
                                                                        EmptyDataText="&nbsp;">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="CreateInterviewTestWithAdaptive_testDrftCheckbox" runat="server" />
                                                                                    <asp:HiddenField ID="CreateInterviewTestWithAdaptive_questionKeyHiddenField" runat="server"
                                                                                        Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="CreateInterviewTestWithAdaptive_testDrftRemoveImage" runat="server"
                                                                                        SkinID="sknDeleteImageButton" CommandName="Select" ToolTip="Remove Question"
                                                                                        CommandArgument='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="CreateInterviewTestWithAdaptive_byQuestion_moveDownQuestionImageButton"
                                                                                        runat="server" SkinID="sknMoveDown_ArrowImageButton" CommandName="MoveDown" ToolTip="Move Question Down"
                                                                                        CommandArgument='<%# Eval("DisplayOrder") %>' /></ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="CreateInterviewTestWithAdaptive_byQuestion_moveUpQuestionImageButton"
                                                                                        runat="server" SkinID="sknMoveUp_ArrowImageButton" CommandName="MoveUp" ToolTip="Move Question Up"
                                                                                        CommandArgument='<%# Eval("DisplayOrder") %>' />
                                                                                    <asp:HiddenField runat="server" ID="CreateInterviewTestWithAdaptive_displayOrderHiddenField"
                                                                                        Value='<%# Eval("DisplayOrder") %>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField ReadOnly="True" HeaderText="Question&nbsp;ID" InsertVisible="False"
                                                                                DataField="QuestionID" Visible="False">
                                                                                <ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Question">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="CreateInterviewTestWithAdaptive_draftQuestionLabel" runat="server"
                                                                                        Text='<%# TrimContent(Eval("Question").ToString(),65) %>' Width="320px"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Wrap="true" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Category">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="CreateInterviewTestWithAdaptive_categoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                        Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Subject">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="CreateInterviewTestWithAdaptive_subjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                        Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:TemplateField>
                                                                            <%-- <asp:TemplateField HeaderText="Answer">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="CreateInterviewTestWithAdaptive_answerLabel" runat="server" Text='<%# TrimContent(Eval("AnswerID").ToString(),20) %>'
                                                                                        Width="120px" ToolTip='<%# Eval("AnswerID") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:TemplateField>--%>
                                                                            <asp:TemplateField HeaderText="Complexity">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="CreateInterviewTestWithAdaptive_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                        Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Wrap="False" />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="CreateInterviewTestWithAdaptive_selectMultipleImage" />
                                                        <asp:AsyncPostBackTrigger ControlID="CreateInterviewTestWithAdaptive_removeMultipleImage" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="CreateInterviewTestWithAdaptive_addQuestionLinkButton" SkinID="sknAddLinkButton"
                                                    runat="server" Text="Add new question" ToolTip="Add new question" OnClick="CreateInterviewTestWithAdaptive_addQuestionLinkButton_Click"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="CreateInterviewTestWithAdaptive_testSummaryUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <uc5:ManualTestSummaryControl ID="CreateInterviewTestWithAdaptive_testSummaryControl"
                                                            runat="server" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                        <ajaxToolKit:TabPanel ID="CreateInterviewTestWithAdaptive_testdetailsTabPanel" runat="server">
                            <HeaderTemplate>
                                Interview Details
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td style="width: 100%">
                                            <asp:UpdatePanel ID="CreateInterviewTestWithAdaptive_testDetailsUserControl_UpdatePanel"
                                                runat="server">
                                                <ContentTemplate>
                                                    <uc4:TestDetailsControl ID="CreateInterviewTestWithAdaptive_testDetailsUserControl"
                                                        runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                    </ajaxToolKit:TabContainer>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="CreateInterviewTestWithAdaptive_bottomMessageupdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="CreateInterviewTestWithAdaptive_previewQuestionAddHiddenField"
                                runat="server" />
                            <asp:Label ID="CreateInterviewTestWithAdaptive_bottomSuccessMessageLabel" runat="server"
                                SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="CreateInterviewTestWithAdaptive_bottomErrorMessageLabel" runat="server"
                                SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <!-- To add additional question popup -->
            <tr>
                <td>
                    <asp:UpdatePanel ID="CreateInterviewTestWithAdaptive_questionUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="CreateInterviewTestWithAdaptive_addQuestionPanel" runat="server" CssClass="popupcontrol_addQuestion">
                                <div style="display: none;">
                                    <asp:Button ID="CreateInterviewTestWithAdaptive_addQuestionhiddenButton" runat="server"
                                        Text="Hidden" /></div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="popup_td_padding_10">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                        <asp:Literal ID="CreateInterviewTestWithAdaptive_addQuestionLiteral" runat="server"
                                                            Text="Add Question"></asp:Literal>
                                                    </td>
                                                    <td style="width: 50%" valign="top">
                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="CreateInterviewTestWithAdaptive_addQuestionTopCancelImageButton"
                                                                        runat="server" SkinID="sknCloseImageButton" 
                                                                        onclick="CreateInterviewTestWithAdaptive_addQuestionTopCancelImageButton_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popup_td_padding_10">
                                            <table cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                <tr>
                                                    <td align="left" class="popup_td_padding_10">
                                                        <table border="0" cellpadding="0" cellspacing="5" class="tab_body_bg" align="left">
                                                            <tr>
                                                                <td valign="top">
                                                                    <table cellpadding="0" cellspacing="5">
                                                                        <tr>
                                                                            <td class="msg_align" colspan="4">
                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_addQuestionErrorLabel" runat="server"
                                                                                    SkinID="sknErrorMessage"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_addQuestionQuestLabel" runat="server"
                                                                                    Text="Question" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                <span class='mandatory'>*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="CreateInterviewTestWithAdaptive_addQuestionQuestTextBox" runat="server"
                                                                                    MaxLength="3000" Height="52px" Width="498px" TextMode="MultiLine" Wrap="true"></asp:TextBox>
                                                                                <asp:LinkButton ID="CreateInterviewTestWithAdaptive_addImageLinkButton" runat="server"
                                                                                    SkinID="sknActionLinkButton" Text="Add Image"
                                                                                    ToolTip="Click here to add image"
                                                                                    OnClientClick="javascript: return ShowAddImagePanel();" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="CreateInterviewTestWithAdaptive_addQuestionImageTR" runat="server" style="display: none">
                                                                            <td>
                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_selectImageLabel" runat="server" Text="Select Image"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                    <tr>
                                                                                        <td style="width: 250px">
                                                                                            <ajaxToolKit:AsyncFileUpload ID="CreateInterviewTestWithAdaptive_questionImageUpload"
                                                                                                runat="server" 
                                                                                                OnUploadedComplete="CreateInterviewTestWithAdaptive_questionImageOnUploadComplete"
                                                                                                ClientIDMode="AutoID" />
                                                                                        </td>
                                                                                        <td style="width: 30px">
                                                                                            <asp:ImageButton ID="CreateInterviewTestWithAdaptive_helpImageButton" runat="server"
                                                                                                SkinID="sknHelpImageButton" ToolTip="Add Image" OnClientClick="javascript:return false;" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Button ID="CreateInterviewTestWithAdaptive_questionImageButton" runat="server"
                                                                                                Text="Add" SkinID="sknButtonId" OnClick="CreateInterviewTestWithAdaptive_questionImageButtonClick"
                                                                                                ToolTip="Click to add image" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_addQuestionAnswerLabel" runat="server"
                                                                                    Text="Answer" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                <span class='mandatory'>*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="CreateInterviewTestWithAdaptive_addQuestionAnswerTextBox" runat="server"
                                                                                    MaxLength="500" Height="52px" Width="498px" TextMode="MultiLine"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td>
                                                                                <asp:CheckBox ID="CreateInterviewTestWithAdaptive_isQuestionRepository" runat="server"
                                                                                    Text="Save question to the repository<br>(Please leave this box unchecked if this question is meant only for this interview and you do not wish to add it to the permanent question database)"
                                                                                    Checked="false" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_addQuestioncomplexityLabel" runat="server"
                                                                                    Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                            </td>
                                                                            <td>
                                                                                <div>
                                                                                    <asp:DropDownList ID="CreateInterviewTestWithAdaptive_addQuestioncomplexityDropDownList"
                                                                                        runat="server" Width="133px">
                                                                                    </asp:DropDownList>
                                                                                    <asp:ImageButton ID="CreateInterviewTestWithAdaptive_addQuestioncomplexityImageButton"
                                                                                        SkinID="sknHelpImageButton" runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;"
                                                                                        ToolTip="Please select the complexity of the question here" />
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_addQuestionLabel" runat="server" Text="Interview Area"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                            </td>
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td class="checkboxaddquestion_list_bg">
                                                                                            <asp:RadioButtonList ID="CreateInterviewTestWithAdaptive_addQuestionRadioButtonList"
                                                                                                runat="server" RepeatColumns="2" RepeatDirection="Horizontal" CellSpacing="5"
                                                                                                TextAlign="Right" Width="50%" TabIndex="5">
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_addQuestion_byQuestion_categoryLabel"
                                                                                    runat="server" Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label><span
                                                                                        class='mandatory'>&nbsp;*</span>
                                                                            </td>
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:TextBox ID="CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryTextBox"
                                                                                                ReadOnly="true" runat="server" Text='<%# Eval("Category") %>'></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="CreateInterviewTestWithAdaptive_addQuestion_byQuestion_subjectLabel"
                                                                                                runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            <asp:HiddenField ID="CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryHiddenField"
                                                                                                runat="server" Value='<%# Eval("Cat_sub_id") %>' />
                                                                                            <asp:HiddenField ID="CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryNameHiddenField"
                                                                                                runat="server" Value='<%# Eval("Category") %>' />
                                                                                            <asp:HiddenField ID="CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_subjectNameHiddenField"
                                                                                                runat="server" Value='<%# Eval("Subject") %>' />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_subjectTextBox"
                                                                                                ReadOnly="true" runat="server" Text='<%# Eval("Subject") %>'></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:ImageButton ID="CreateInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryImageButton"
                                                                                                SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" ToolTip="Click here to select category and subject" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_tagsLabel" runat="server" Text="Tags"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="CreateInterviewTestWithAdaptive_tagsTextBox" runat="server" MaxLength="100"
                                                                                    Width="500px"></asp:TextBox>
                                                                                <asp:ImageButton ID="CreateInterviewTestWithAdaptive_tagsImageButton" runat="server" SkinID="sknHelpImageButton"
                                                                                    ToolTip="Please enter tags separated by commas" OnClientClick="javascript:return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td valign="top">
                                                                    <table cellpadding="0" cellspacing="5" class="tab_body_bg">
                                                                        <tr id="CreateInterviewTestWithAdaptive_displayQuestionImageTR" runat="server" style="display: none">
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Image runat="server" ID="CreateInterviewTestWithAdaptive_questionImage" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:LinkButton ID="CreateInterviewTestWithAdaptive_deleteLinkButton" runat="server"
                                                                                                Text="Delete" SkinID="sknActionLinkButton" OnClick="CreateInterviewTestWithAdaptive_deleteLinkButtonClick"
                                                                                                ToolTip="Click to delete image" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popup_td_padding_5">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td align="left" style="width: 20px; padding-right: 5px">
                                                        <asp:Button ID="CreateInterviewTestWithAdaptive_addQuestioSaveButton" runat="server"
                                                            SkinID="sknButtonId" Text="Save" OnClick="CreateInterviewTestWithAdaptive_addQuestioSaveButton_Clicked" />
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="CreateInterviewTestWithAdaptive_addQuestioCloseLinkButton" SkinID="sknPopupLinkButton"
                                                            runat="server" Text="Cancel" 
                                                            onclick="CreateInterviewTestWithAdaptive_addQuestioCloseLinkButton_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <ajaxToolKit:ModalPopupExtender ID="CreateInterviewTestWithAdaptive_addQuestioModalPopupExtender"
                                runat="server" PopupControlID="CreateInterviewTestWithAdaptive_addQuestionPanel"
                                TargetControlID="CreateInterviewTestWithAdaptive_addQuestionhiddenButton" BackgroundCssClass="modalBackground">
                            </ajaxToolKit:ModalPopupExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <!-- End additional popup window-->
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="72%" class="header_text">
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td width="22%">
                                        </td>
                                        <td width="40%" align="right">
                                            <asp:Button ID="CreateInterviewTestWithAdaptive_bottomSaveButton" runat="server"
                                                SkinID="sknButtonId" Text="Save" OnClick="CreateInterviewTestWithAdaptive_saveButton_Click" />
                                            <asp:Button ID="CreateInterviewTestWithAdaptive_bottomCreateSessionButton" runat="server"
                                                SkinID="sknButtonId" Text="Create Session" Visible="false" />
                                        </td>
                                        <td width="16%" align="right">
                                            <asp:LinkButton ID="CreateInterviewTestWithAdaptive_bottompResetLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Reset" OnClick="CreateInterviewTestWithAdaptive_resetLinkButton_Click" />
                                        </td>
                                        <td width="4%" align="center">
                                            |
                                        </td>
                                        <td width="18%" align="left">
                                            <asp:LinkButton ID="CreateInterviewTestWithAdaptive_bottomCancelLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="CreateInterviewTestWithAdaptive_questionDetailPanel" runat="server"
                                                CssClass="popupcontrol_question_draft" Style="display: none">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="td_height_20">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                        <asp:Literal ID="CreateInterviewTestWithAdaptive_questionDetailResultLiteral" runat="server"
                                                                            Text="Question Detail"></asp:Literal>
                                                                    </td>
                                                                    <td style="width: 50%" valign="top">
                                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:ImageButton ID="CreateInterviewTestWithAdaptive_questionDetailPreviewControlTopCancelImageButton"
                                                                                        runat="server" SkinID="sknCloseImageButton" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_20">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:DataGrid ID="CreateInterviewTestWithAdaptive_questionDetailCategoryDataGrid"
                                                                                        runat="server" AutoGenerateColumns="false">
                                                                                        <Columns>
                                                                                            <asp:BoundColumn HeaderText="Category" DataField="Category"></asp:BoundColumn>
                                                                                            <asp:BoundColumn HeaderText="Subject" DataField="Subject"></asp:BoundColumn>
                                                                                        </Columns>
                                                                                    </asp:DataGrid>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="popup_panel_inner_bg">
                                                                                    <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_questionDetailTestAreaHeadLabel" runat="server"
                                                                                                    Text="Interview Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_questionDetailTestAreaLabel" runat="server"
                                                                                                    Text="Concept" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_questionDetailNoOfTestLabel" runat="server"
                                                                                                    Text="Number Of Administered Interview " SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_questionDetailTestAdministeredLabel"
                                                                                                    runat="server" Text="5" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_questionDetailAvgTimeLabel" runat="server"
                                                                                                    Text="Average Time Taken(in minutes)" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_questionDetailAvgTimeValueLabel" runat="server"
                                                                                                    Text="15.5" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_questionDetailCorrectToAttendedRatioHeadLabel"
                                                                                                    runat="server" Text=" Ratio of Correct Answer To Attended" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_questionDetailCorrectToAttendedRatioLabel"
                                                                                                    runat="server" Text="36:50" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_questionDetailComplexityHeadLabel"
                                                                                                    runat="server" Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateInterviewTestWithAdaptive_questionDetailPreviewControlComplexityLabel"
                                                                                                    runat="server" Text="Normal" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="popup_question_icon">
                                                                                    <asp:Label ID="CreateInterviewTestWithAdaptive_questionDetailQuestionLabel" runat="server"
                                                                                        SkinID="sknLabelText"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_padding_left_20">
                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <asp:RadioButtonList ID="CreateInterviewTestWithAdaptive_questionDetailAnswerRadioButtonList"
                                                                                            runat="server" RepeatColumns="2" RepeatDirection="Horizontal" CellSpacing="5"
                                                                                            TextAlign="Right" Width="100%">
                                                                                        </asp:RadioButtonList>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_8">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="padding-left: 30px">
                                                                        <table border="0" cellpadding="0" cellspacing="0" align="left">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Button ID="CreateInterviewTestWithAdaptive_questionDetailTopAddButton" runat="server"
                                                                                        Text="Add" SkinID="sknButtonId" OnClick="CreateInterviewTestWithAdaptive_questionDetailTopAddButton_Click" />
                                                                                </td>
                                                                                <td style="padding-left: 10px">
                                                                                    <asp:LinkButton ID="CreateInterviewTestWithAdaptive_questionDetailTopCancelButton"
                                                                                        runat="server" SkinID="sknCancelLinkButton" Text="Cancel" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_8">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <div style="display: none">
                                                <asp:Button ID="CreateInterviewTestWithAdaptive_adaptiveQuestionPreviewPanelTargeButton"
                                                    runat="server" />
                                            </div>
                                            <ajaxToolKit:ModalPopupExtender ID="CreateInterviewTestWithAdaptive_adaptiveQuestionPreviewModalPopupExtender"
                                                runat="server" PopupControlID="CreateInterviewTestWithAdaptive_questionDetailPanel"
                                                TargetControlID="CreateInterviewTestWithAdaptive_adaptiveQuestionPreviewPanelTargeButton"
                                                BackgroundCssClass="modalBackground" CancelControlID="CreateInterviewTestWithAdaptive_questionDetailTopCancelButton">
                                            </ajaxToolKit:ModalPopupExtender>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:UpdatePanel ID="CreateInterviewTestWithAdaptive_questionDetailPreviewUpdatePanel"
        runat="server">
        <ContentTemplate>
            <asp:Panel ID="CreateInterviewTestWithAdaptive_questionPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_question_detail">
                <div style="display: none">
                    <asp:Button ID="CreateInterviewTestWithAdaptive_questionModalTargeButton" runat="server" />
                </div>
                <%-- <uc3:QuestionDetailPreviewControl ID="CreateInterviewTestWithAdaptive_questionDetailPreviewControl"
                    runat="server" Title="Question Detail Preview" OnCancelClick="CreateInterviewTestWithAdaptive_cancelClick" OnAddClick="CreateInterviewTestWithAdaptive_questionDetailPreviewControl_AddClick" />--%>
                <uc7:QuestionDetailSummaryControl ID="CreateInterviewTestWithAdaptive_questionDetailSummaryControl"
                    runat="server" Title="Question Details Summary" OnCancelClick="CreateInterviewTestWithAdaptive_cancelClick"
                    OnAddClick="CreateInterviewTestWithAdaptive_questionDetailPreviewControl_AddClick" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="CreateInterviewTestWithAdaptive_questionModalPopupExtender"
                runat="server" PopupControlID="CreateInterviewTestWithAdaptive_questionPanel"
                TargetControlID="CreateInterviewTestWithAdaptive_questionModalTargeButton" BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="CreateInterviewTestWithAdaptive_ConfirmPopupUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="CreateInterviewTestWithAdaptive_authorIdHiddenField" runat="server" />
            <asp:HiddenField ID="CreateInterviewTestWithAdaptive_positionProfileHiddenField"
                runat="server" />
            <asp:HiddenField ID="hiddenValue" runat="server" />
            <asp:HiddenField ID="hiddenDeleteValue" runat="server" />
            <div id="CreateInterviewTestWithAdaptive_hiddenDIV" runat="server" style="display: none">
                <asp:Button ID="CreateInterviewTestWithAdaptive_hiddenPopupModalButton" runat="server" />
            </div>
            <asp:Panel ID="CreateInterviewTestWithAdaptive_ConfirmPopupPanel" runat="server"
                Style="display: none" CssClass="popupcontrol_confirm_remove">
                <uc6:ConfirmMsgControl ID="CreateInterviewTestWithAdaptive_ConfirmPopupExtenderControl"
                    runat="server" OnOkClick="CreateInterviewTestWithAdaptive_okClick" OnCancelClick="CreateInterviewTestWithAdaptive_cancelClick" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="CreateInterviewTestWithAdaptive_ConfirmPopupExtender"
                runat="server" PopupControlID="CreateInterviewTestWithAdaptive_ConfirmPopupPanel"
                TargetControlID="CreateInterviewTestWithAdaptive_hiddenPopupModalButton" BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
            <!-- Popup Save Button -->
            <div id="SearchQuestion_hiddenDIV" runat="server" style="display: none">
                <asp:Button ID="CreateInterviewTest_hiddenPopupModalButton1" runat="server" />
            </div>
            <ajaxToolKit:ModalPopupExtender ID="CreateInterviewTest_saveTestModalPopupExtender"
                runat="server" PopupControlID="CreateInterviewTest_saveTestConfrimPopUpPanel"
                TargetControlID="CreateInterviewTest_hiddenPopupModalButton1" BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
            <asp:Panel ID="CreateInterviewTest_saveTestConfrimPopUpPanel" runat="server" Style="display: none"
                Height="210px" CssClass="popupcontrol_confirm">
                <uc6:ConfirmMsgControl ID="CreateInterviewTestSave_confirmPopupExtenderControl" runat="server"
                    OnOkClick="CreateInterviewTest_okPopUpClick" OnCancelClick="CreateInterviewTest_cancelPopUpClick" />
            </asp:Panel>
            <!-- End Popup Save Buttion -->
        </ContentTemplate>
    </asp:UpdatePanel>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load
    (
        function () {
            window.setTimeout(focus, 1);
        }
    )
        function focus() {
            try {
                document.getElementById('<%=CreateInterviewTestWithAdaptive_categoryTextBox.ClientID %>').focus();
            }
            catch (Err) {
                try {
                    document.getElementById('<%=CreateInterviewTestWithAdaptive_keywordTextBox.ClientID %>').focus();
                }
                catch (Err1) {
                    document.getElementById('ctl00$InterviewMaster_body$CreateInterviewTestWithAdaptive_mainTabContainer$CreateInterviewTestWithAdaptive_testdetailsTabPanel$CreateInterviewTestWithAdaptive_testDetailsUserControl$TestDetailsControl_testNameTextBox').focus();

                }
            }
        }
    </script>
</asp:Content>
