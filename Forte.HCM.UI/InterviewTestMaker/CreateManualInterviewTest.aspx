<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    CodeBehind="CreateManualInterviewTest.aspx.cs" Inherits="Forte.HCM.UI.InterviewTestMaker.CreateManualInterviewTest" %>

<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/InterviewQuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc3" %>
<%@ Register Src="../CommonControls/InterviewTestDetailsControl.ascx" TagName="TestDetailsControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/ManualInterviewTestSummaryControl.ascx" TagName="ManualTestSummaryControl"
    TagPrefix="uc5" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc6" %>
<%@ Register Src="../CommonControls/InterviewQuestionDetailSummaryControl.ascx" TagName="QuestionDetailSummaryControl"
    TagPrefix="uc7" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ContentPlaceHolderID="InterviewMaster_body" ID="CreateManualInterviewTest_content"
    runat="server">
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        var control;
        function setFocus(sender, e) {
            try {
                var activeTab = $find('<%=CreateManualInterviewTest_mainTabContainer.ClientID%>').get_activeTabIndex();
                if (activeTab == "0") {
                    try {
                        control = $get('<%=CreateManualInterviewTest_categoryTextBox.ClientID%>');
                        control.focus();
                    }
                    catch (er1) {
                        try {
                            control = $get('<%=CreateManualInterviewTest_keywordTextBox.ClientID%>');
                            control.focus();
                        }
                        catch (er2) {
                        }
                    }
                }
                else {
                    control = $get('ctl00_OTMMaster_body_CreateManualInterviewTest_mainTabContainer_CreateManualInterviewTest_testdetailsTabPanel_CreateManualInterviewTest_testDetailsUserControl_TestDetailsControl_testNameTextBox');
                    control.focus();
                }
            }
            catch (er) {
            }
        }

        function ShowAddImagePanel() {
            var addImageTR = document.getElementById('<%=CreateManualInterviewTest_AddQuestionImageTR.ClientID%>');
            addImageTR.style.display = "";
            var addImageBtn = document.getElementById('<%=CreateManualInterviewTest_addImageLinkButton.ClientID%>');
            addImageBtn.style.display = "none";
            return false;
        }
    </script>
    <script type="text/javascript" language="javascript">

        // Display  Or Hide the adaptive Interview Draft Or Manual Interview Draft Questions.
        function ShowOrHideDiv(caseValue) {
            switch (caseValue.toString()) {
                case '0':
                    {
                        document.getElementById("CreateManualInterviewTest_manualTd").style.display = "block";
                        document.getElementById("CreateManualInterviewTest_adaptiveTd").style.display = "none";
                        document.getElementById("CreateManualInterviewTest_manualTd").style.width = "100%";

                        document.getElementById("<%= CreateManualInterviewTest_questionDiv.ClientID %>").style.width = "936px";
                        document.getElementById("<%= CreateManualInterviewTest_adaptiveQuestionDiv.ClientID %>").style.width = "0";
                        break;
                    }
                case '1':
                    {
                        document.getElementById("CreateManualInterviewTest_manualTd").style.display = "none";
                        document.getElementById("CreateManualInterviewTest_adaptiveTd").style.display = "block";
                        document.getElementById("CreateManualInterviewTest_manualTd").style.width = "0";
                        document.getElementById("CreateManualInterviewTest_adaptiveTd").style.width = "100%";

                        document.getElementById("<%= CreateManualInterviewTest_questionDiv.ClientID %>").style.width = "0";
                        document.getElementById("<%= CreateManualInterviewTest_adaptiveQuestionDiv.ClientID %>").style.width = "936px";
                        break;
                    }
                case '2':
                    {
                        document.getElementById("CreateManualInterviewTest_manualTd").style.display = "block";
                        document.getElementById("CreateManualInterviewTest_adaptiveTd").style.display = "block";
                        document.getElementById("CreateManualInterviewTest_manualTd").style.width = "49%";
                        document.getElementById("CreateManualInterviewTest_adaptiveTd").style.width = "50%";

                        document.getElementById("<%= CreateManualInterviewTest_questionDiv.ClientID %>").style.width = "450px";
                        document.getElementById("<%= CreateManualInterviewTest_adaptiveQuestionDiv.ClientID %>").style.width = "456px";
                        break;
                    }
                default:
                    {
                        document.getElementById("CreateManualInterviewTest_manualTd").style.display = "block";
                        document.getElementById("CreateManualInterviewTest_adaptiveTd").style.display = "block";
                        document.getElementById("CreateManualInterviewTest_manualTd").style.width = "49%";
                        document.getElementById("CreateManualInterviewTest_adaptiveTd").style.width = "50%";

                        document.getElementById("<%= CreateManualInterviewTest_questionDiv.ClientID %>").style.width = "450px";
                        document.getElementById("<%= CreateManualInterviewTest_adaptiveQuestionDiv.ClientID %>").style.width = "456px";
                        break;
                    }
            }
            return false;
        }

        var modifiedRow = null;
        var dataCollection = new Array();
        dataCollection = null;
        var sourceRow = null;
        var sourcePosition = null;
        var sourceTable = null;
        var currenttable = null;
        var dropTargets = null;
        var dragObject = null;
        var mouseOffSet = null;
        var destTable = null;
        var oldY = 0;
        this.table = null;

        //move the Single Question add to Testdraft.
        function singleqnsclick(QuestionID, buttonID) {
            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
            hiddenvalue.value = QuestionID;
            __doPostBack(buttonID, "OnClick");
        }

        //related to the Drag and Drop questions 
        function mousedown(QuestionID, source) {
            sourceTable = document.getElementById(source);

            if (checkChecked(QuestionID)) {
                CheckNoOfChecked(QuestionID);
            }
            else {
                CheckCorrectQuestion(QuestionID);
            }
        }

        //related to the Drag and Drop questions 
        function CheckCorrectQuestion(id) {
            var table = sourceTable;
            var testVar = document.getElementById('<%=CreateManualInterviewTest_searchQuestionGridView.ClientID %>');
            var testVarTwo = document.getElementById('<%=CreateManualInterviewTest_adaptiveGridView.ClientID %>');
            id++;
            var rowid = 1;
            var chkbox;
            var rowCount = table.rows.length;
            for (var i = 1; i < rowCount; i++) {
                if (id == i) {
                    var row = table.rows[i];
                    if (detectBrowser()) {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else {
                        chkbox = row.cells[0].childNodes[0];
                    }

                    if (null != chkbox) {
                        chkbox.checked = true;
                        // row.cells[0].childNodes[0].checked = true;
                    }
                }
                if ((testVar == sourceTable) || (testVarTwo == sourceTable)) {
                    CheckNoOfChecked(id);
                }
            }
        }

        //find the Browser details
        function detectBrowser() {
            var browser = navigator.appName;
            var b_version = navigator.appVersion;
            var version = parseFloat(b_version);
            if (browser == "Netscape") {
                return true;
            }
            else {
                return false; ;
            }
        }
        // Question remove from the Interview Draft.
        function RemoveRows(rowIndex) {
            sourceTable = document.getElementById('<%=CreateManualInterviewTest_testDrftGridView.ClientID %>');

            if (confirm('Are you sure to delete these question(s)?')) {
                if (checkChecked(rowIndex)) {
                    DelRows();

                }
                else {
                    CheckCorrectQuestion(rowIndex);
                    DelRows();
                }
            }
            else
                return false;
        }

        function DelRows() {
            try {
                var table = document.getElementById('<%=CreateManualInterviewTest_testDrftGridView.ClientID %>');
                var rowCount = table.rows.length;
                for (var i = 0; i < rowCount; i++) {
                    var row = table.rows[i];
                    var chkbox;
                    if (detectBrowser()) {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else {
                        chkbox = row.cells[0].childNodes[0];
                    }
                    if (null != chkbox && true == chkbox.checked) {
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                    }
                }
            } catch (e) {
                alert(e);
            }
            return false;
        }

        function CheckNoOfChecked(QuestionID) {

            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');

            hiddenvalue.value = "";

            var numberOfQuestions = 0;

            var table = sourceTable;
            // document.getElementById('<%=CreateManualInterviewTest_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {

                    numberOfQuestions = numberOfQuestions + 1;

                    var noOfQuestionsTextBox = document.getElementById('<%=sampleDIVTextBox.ClientID %>');

                    noOfQuestionsTextBox.value = 'Move ' + numberOfQuestions + ' question(s)';

                    dragObject = document.getElementById('<%=moveDisplayDIV.ClientID %>');

                    if (detectBrowser()) {
                        if (hiddenvalue.value == "") {
                            hiddenvalue.value = row.childNodes[5].childNodes[1].defaultValue + ',';
                        }
                        else {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[5].childNodes[1].defaultValue + ',';
                        }
                    }
                    else {
                        if (hiddenvalue.value == "") {
                            hiddenvalue.value = row.childNodes[4].childNodes[0].value + ',';
                        }
                        else {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[4].childNodes[0].value + ',';
                        }
                    }
                }

            }
            //var ShowDiv =
        }

        function checkChecked(id) {

            var isChecked = false;

            var table = sourceTable;
            // document.getElementById('<%=CreateManualInterviewTest_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {
                    isChecked = true;
                }

                if (isChecked) {
                    return true;
                }
                else {
                    return false;
                }
            }

        }

        function mouseMove(ev) {
            ev = ev || window.event;
            var mousePos = mouseCoords(ev);

            if (dragObject) {
                //mouseOffset = getMouseOffset(dragObject, ev);
                dragObject.style.display = 'block';
                dragObject.style.backgroundColor = "#eee";
                dragObject.style.position = 'absolute';
                if (detectBrowser()) {
                    dragObject.style.top = mousePos.y + 'px';
                    dragObject.style.left = mousePos.x + 'px';
                }
                else {
                    dragObject.style.top = mousePos.y + document.documentElement.scrollTop + 10;
                    dragObject.style.left = mousePos.x + 10;
                }
            }
            else {

            }
            return false;
        }
        function mouseCoords(ev) {
            if (ev != null) {
                if (ev.pageX || ev.pageY) { return { x: ev.pageX, y: ev.pageY }; }
                return {
                    x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
                    y: ev.clientY + document.body.scrollTop - document.body.clientTop
                };
            }
        }

        function srsmouseup(ev) {
            if (dragObject == null)
                return false;
            var ev = ev || window.event;
            var mousePos = mouseCoords(ev);
            var curTarget = document.getElementById('<%=CreateManualInterviewTest_testDrftGridView.ClientID %>');
            var buttonID = document.getElementById('<%=CreateManualInterviewTest_selectMultipleImage.ClientID %>');
            var targPos = getPosition(curTarget);
            var scrolltargPos;
            if (detectBrowser()) {
                scrolltargPos = targPos.y;
            }
            else {
                scrolltargPos = targPos.y - document.documentElement.scrollTop;
            }

            var targWidth = parseInt(curTarget.offsetWidth);
            var targHeight = parseInt(curTarget.offsetHeight);
            if (
            (mousePos.x >= targPos.x) &&
            (mousePos.y >= scrolltargPos)) {

                // __doPostBack('ctl00_HCMMaster_body_CreateManualInterviewTest_mainTabContainer_CreateManualInterviewTest_questionsTabPanel_CreateManualInterviewTest_selectMultipleImage', "OnClick");
                __doPostBack('<%= CreateManualInterviewTest_selectMultipleImage.ClientID %>', "OnClick");

                dragObject.style.display = 'none';
            }
            else {
                if (dragObject != null) {
                    makeUnchekabale();
                    dragObject.style.display = 'none';

                    var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
                    hiddenvalue.value = "";
                }
            } dragObject = null;
        }

        function makeUnchekabale() {
            var table = sourceTable;
            //document.getElementById('<%=CreateManualInterviewTest_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {
                    chkbox.checked = false;
                }
            }
        }

        function getPosition(e) {
            var left = 0;
            var top = 0;

            if (e != null) {
                while (e.offsetParent) {
                    left += e.offsetLeft;
                    top += e.offsetTop;
                    e = e.offsetParent;
                }
                left += e.offsetLeft;
                top += e.offsetTop;
            }

            return { x: left, y: top };
        }
    </script>
    <asp:Panel ID="CreateManualTest_mainPanel" runat="server" DefaultButton="CreateManualInterviewTest_bottomSaveButton">
        <table width="100%" border="0" cellspacing="3" cellpadding="0">
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="68%" class="header_text_bold">
                                <asp:Literal ID="CreateManualInterviewTest_headerLiteral" runat="server" Text="Create Manual Interview"></asp:Literal>
                            </td>
                            <td width="32%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td width="20%" align="right">
                                            <asp:Button ID="CreateManualInterviewTest_topSaveButton" runat="server" SkinID="sknButtonId"
                                                Text="Save" OnClick="CreateManualInterviewTest_saveButton_Click" />
                                            <asp:Button ID="CreateManualInterviewTest_topCreateSessionButton" runat="server"
                                                SkinID="sknButtonId" Text="Create Session" Visible="false" />
                                        </td>
                                        <td width="48%" align="center">
                                            <asp:LinkButton ID="CreateManualInterviewTest_adptiveTestLinkButton" SkinID="sknActionLinkButton"
                                                runat="server" Text="Create Interview With Adaptive" OnClick="CreateManualInterviewTest_adptiveTestLinkButton_Click">
                                            </asp:LinkButton>
                                        </td>
                                        <td width="1%" align="center">
                                            |
                                        </td>
                                        <td width="12%" align="right">
                                            <asp:LinkButton ID="CreateManualInterviewTest_topResetLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Reset" OnClick="CreateManualInterviewTest_resetLinkButton_Click" />
                                        </td>
                                        <td width="1%" align="center">
                                            |
                                        </td>
                                        <td width="10%" align="left">
                                            <asp:LinkButton ID="CreateManualInterviewTest_topCancelLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="CreateManualInterviewTest_topMessageUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="CreateManualInterviewTest_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="CreateManualInterviewTest_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                                ID="CreateManualInterviewTest_stateHiddenField" runat="server" Value="0" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <ajaxToolKit:TabContainer ID="CreateManualInterviewTest_mainTabContainer" runat="server"
                        ActiveTabIndex="0" OnClientActiveTabChanged="setFocus">
                        <ajaxToolKit:TabPanel ID="CreateManualInterviewTest_questionsTabPanel" HeaderText="questions"
                            runat="server">
                            <HeaderTemplate>
                                Questions</HeaderTemplate>
                            <ContentTemplate>
                                <asp:Panel ID="CreateManualTest_testDetailsPanel" runat="server" DefaultButton="CreateManualInterviewTest_topSearchButton">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout: fixed">
                                        <tr>
                                            <td>
                                                <div id="CreateManualInterviewTest_searchCriteriasDiv" runat="server" style="display: block;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="right" class="td_padding_bottom_5">
                                                                <asp:UpdatePanel ID="CreateManualInterviewTest_simpleLinkButtonUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="CreateManualInterviewTest_simpleLinkButton" runat="server" Text="Advanced"
                                                                            SkinID="sknActionLinkButton" OnClick="CreateManualInterviewTest_simpleLinkButton_Click" /></ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="panel_bg">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td width="100%">
                                                                            <asp:UpdatePanel ID="CreateManualInterviewTest_searchDiv_UpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <div id="CreateManualInterviewTest_simpleSearchDiv" runat="server">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="CreateManualInterviewTest_categoryHeadLabel" runat="server" Text="Category"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="CreateManualInterviewTest_categoryTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CreateManualInterviewTest_subjectHeadLabel" runat="server" Text="Subject"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="CreateManualInterviewTest_subjectTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CreateManualInterviewTest_keywordsHeadLabel" runat="server" Text="Keyword"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <div style="float: left; padding-right: 5px;">
                                                                                                        <asp:TextBox ID="CreateManualInterviewTest_keywordsTextBox" runat="server" MaxLength="100"></asp:TextBox></div>
                                                                                                    <div style="float: left;">
                                                                                                        <asp:ImageButton ID="CreateManualInterviewTest_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div id="CreateManualInterviewTest_advanceSearchDiv" runat="server">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <uc1:CategorySubjectControl ID="CreateManualInterviewTest_categorySubjectControl"
                                                                                                        runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="td_height_8">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="panel_inner_body_bg">
                                                                                                    <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td style="width: 10%">
                                                                                                                <asp:Label ID="CreateManualInterviewTest_testAreaHeadLabel" runat="server" Text="Interview Area"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="5" class="checkbox_list_bg" align="left" style="width: 90%">
                                                                                                                <asp:CheckBoxList ID="CreateManualInterviewTest_testAreaCheckBoxList" runat="server"
                                                                                                                    RepeatColumns="3" RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                                </asp:CheckBoxList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 10%">
                                                                                                                <asp:Label ID="CreateManualInterviewTest_complexityLabel" runat="server" Text="Complexity"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="3" align="left" valign="middle">
                                                                                                                <div style="float: left; padding-right: 5px; width: 90%; vertical-align: middle;"
                                                                                                                    class="checkbox_list_bg">
                                                                                                                    <asp:CheckBoxList ID="CreateManualInterviewTest_complexityCheckBoxList" runat="server"
                                                                                                                        RepeatColumns="4" RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                                    </asp:CheckBoxList>
                                                                                                                </div>
                                                                                                                <div style="float: left; vertical-align: middle; padding-left: 5px; padding-top: 5px;">
                                                                                                                    <asp:ImageButton ID="CreateManualInterviewTest_complexityImageButton" SkinID="sknHelpImageButton"
                                                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, ComplexityHelp %>" /></div>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualInterviewTest_keywordHeadLabel" runat="server" Text="Keyword"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="CreateManualInterviewTest_keywordTextBox" runat="server" Columns="50"
                                                                                                                        MaxLength="100"></asp:TextBox></div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="CreateManualInterviewTest_keywordImageButton" SkinID="sknHelpImageButton"
                                                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualInterviewTest_questionLabel" runat="server" Text="Question ID"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="CreateManualInterviewTest_questionIDTextBox" runat="server" MaxLength="15"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                &#160;&#160;
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                &#160;&#160;
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualInterviewTest_authorHeadLabel" runat="server" Text="Author"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="CreateManualInterviewTest_authorTextBox" runat="server"></asp:TextBox><asp:HiddenField
                                                                                                                        ID="CreateManualInterviewTest_dummyAuthorID" runat="server" />
                                                                                                                </div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="CreateManualInterviewTest_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                                                        runat="server" ImageAlign="Middle" /></div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualInterviewTest_positionProfileLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                    Text="Position Profile"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="3">
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="CreateManualInterviewTest_positionProfileTextBox" runat="server"
                                                                                                                        MaxLength="200" Columns="55"></asp:TextBox></div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="CreateManualInterviewTest_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to select position profile" /></div>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="CreateManualInterviewTest_positionProfileKeywordLabel" runat="server"
                                                                                                                    SkinID="sknLabelFieldHeaderText" Text="Position Profile Keyword"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px; width: 100%">
                                                                                                                    <asp:TextBox ID="CreateManualInterviewTest_positionProfileKeywordTextBox" runat="server"
                                                                                                                        MaxLength="500"></asp:TextBox></div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" class="td_padding_top_5">
                                                                            <asp:Button ID="CreateManualInterviewTest_topSearchButton" runat="server" Text="Search"
                                                                                SkinID="sknButtonId" OnClick="CreateManualInterviewTest_searchQuestionButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr id="CreateManualInterviewTest_searchResultsTR" runat="server">
                                            <td class="header_bg" runat="server">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 80%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="CreateManualInterviewTest_searchResultsLiteral" runat="server" Text="Questions"></asp:Literal>&#160;<asp:Label
                                                                ID="CreateManualInterviewTest_sortHelpLabel" runat="server" SkinID="sknLabelText"></asp:Label>
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="CreateManualInterviewTest_manualImageButton" runat="server"
                                                                SkinID="sknManualImageButton" ToolTip="Click here to view 'Search Results' only"
                                                                OnClientClick="javascript:return ShowOrHideDiv('0');" />
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="CreateManualInterviewTest_adaptiveImageButton" runat="server"
                                                                SkinID="sknAdaptiveImageButton" ToolTip="Click here to view 'Adaptive Recommended Questions' only"
                                                                OnClientClick="javascript:return ShowOrHideDiv('1');" />
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="CreateManualInterviewTest_bothImageButton" runat="server" SkinID="sknCombinedAMImageButton"
                                                                ToolTip="Click here to view both 'Search Results' and 'Adaptive Recommended Questions'"
                                                                OnClientClick="javascript:return ShowOrHideDiv('2');" />
                                                        </td>
                                                        <td style="width: 5%" align="right">
                                                            <span id="CreateManualInterviewTest_searchResultsUpSpan" runat="server" style="display: none;">
                                                                <asp:Image ID="CreateManualInterviewTest_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                                    id="CreateManualInterviewTest_searchResultsDownSpan" runat="server" style="display: block;"><asp:Image
                                                                        ID="CreateManualInterviewTest_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span><asp:HiddenField
                                                                            ID="CreateManualInterviewTest_restoreHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td valign="top">
                                                <div id="CreateManualInterviewTest_manualTd" runat="server" style="width: 100%; float: left;
                                                    display: block;">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="grid_header_bg">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td class="header_text_bold">
                                                                            <asp:Literal ID="CreateManualInterviewTest_manualQuestLiteral" runat="server" Text="Search Results"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="grid_body_bg" valign="top">
                                                                <asp:UpdatePanel ID="CreateManualInterviewTest_searchQuestionGridView_UpdatePanel"
                                                                    runat="server">
                                                                    <ContentTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <div id="CreateManualInterviewTest_questionDiv" runat="server" style="height: 180px;
                                                                                        width: 939px; overflow: auto;">
                                                                                        <asp:GridView ID="CreateManualInterviewTest_searchQuestionGridView" runat="server"
                                                                                            OnRowCommand="CreateManualInterviewTest_searchQuestionGridView_RowCommand" OnRowDataBound="CreateManualInterviewTest_searchQuestionGridView_RowDataBound"
                                                                                            OnSorting="CreateManualInterviewTest_searchQuestionGridView_Sorting">
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:CheckBox ID="CreateManualInterviewTest_searchQuestionCheckbox" runat="server" /><asp:HiddenField
                                                                                                            ID="CreateManualInterviewTest_questionKeyHiddenField" runat="server" Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateManualInterviewTest_previewImageButton" runat="server"
                                                                                                            CommandArgument='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' CommandName="view"
                                                                                                            SkinID="sknPreviewImageButton" ToolTip="Preview Question" /></ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateManualInterviewTest_searchQuestionAlertImageButton" runat="server"
                                                                                                            SkinID="sknAlertImageButton" ToolTip="Flag Question" /></ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="CreateManualInterviewTest_searchQuestionSelectImage" runat="server"
                                                                                                            CommandName="Select" SkinID="sknMoveDown_ArrowImageButton" ToolTip="Select Question" /></ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="CreateManualInterviewTest_hidddenValue" runat="server" Value='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField DataField="QuestionID" Visible="False" />
                                                                                                <asp:BoundField DataField="QuestionRelationID" Visible="False" />
                                                                                                <asp:TemplateField HeaderText="Question">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualInterviewTest_questionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),60) %>'
                                                                                                            Width="320px"></asp:Label></ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Category">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualInterviewTest_categoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                                            ToolTip='<%# Eval("CategoryName") %>' Width="135px"></asp:Label></ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Subject">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualInterviewTest_subjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                                            ToolTip='<%# Eval("SubjectName") %>' Width="135px"></asp:Label></ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Complexity">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateManualInterviewTest_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                                            ToolTip='<%# Eval("Complexity") %>' Width="75px"></asp:Label></ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <uc2:PageNavigator ID="CreateManualInterviewTest_bottomPagingNavigator" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <div id="moveDisplayDIV" runat="server" style="display: none; background-color: Lime">
                                                                                <asp:TextBox ID="sampleDIVTextBox" runat="server" ReadOnly="True"></asp:TextBox></div>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="CreateManualInterviewTest_topSearchButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="CreateManualInterviewTest_adaptiveTd" style="width: 0%; float: right; display: none;">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="grid_header_bg">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td class="header_text_bold">
                                                                            <asp:Literal ID="CreateManualInterviewTest_adaptiveQuestLiteral" runat="server" Text="Adaptive Recommended Questions"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                            <td class="grid_body_bg">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <div id="CreateManualInterviewTest_adaptiveQuestionDiv" runat="server" style="height: 180px;
                                                                                width: 0px; overflow: auto;">
                                                                                <asp:GridView ID="CreateManualInterviewTest_adaptiveGridView" runat="server" EnableModelValidation="True"
                                                                                    OnRowCommand="CreateManualInterviewTest_adaptiveGridView_RowCommand" OnRowDataBound="CreateManualInterviewTest_adaptiveGridView_RowDataBound"
                                                                                    OnSorting="CreateManualInterviewTest_adaptiveGridView_Sorting">
                                                                                    <Columns>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="CreateManualInterviewTest_adaptiveCheckbox" runat="server" /></ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="CreateManualInterviewTest_adptivepreviewImageButton" runat="server"
                                                                                                    CommandName="view" SkinID="sknPreviewImageButton" ToolTip="Preview Question" /></ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="CreateManualInterviewTest_adaptiveAlertImageButton" runat="server"
                                                                                                    SkinID="sknAlertImageButton" ToolTip="Flag Question" /></ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="CreateManualInterviewTest_adaptiveSelectImage" runat="server"
                                                                                                    SkinID="sknMoveDown_ArrowImageButton" ToolTip="Select Question" /></ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="CreateManualInterviewTest_hidddenValue" runat="server" Value='<%#Eval("QuestionID") %>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="CreateManualInterviewTest_adaptiveQuestionDetailsImage" runat="server"
                                                                                                    CommandName="AdaptiveQuestion" SkinID="sknDraftImageButton" ToolTip="Adaptive Question Details" /></ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="QuestionID" Visible="False" />
                                                                                        <asp:BoundField DataField="QuestionRelationID" Visible="False" />
                                                                                        <asp:TemplateField HeaderText="Question">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="CreateManualInterviewTest_questionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),65) %>'
                                                                                                    Width="340px"></asp:Label><asp:Panel ID="CreateManualInterviewTest_hoverPanel" runat="server"
                                                                                                        CssClass="table_outline_bg">
                                                                                                        <table border="0" cellpadding="3" cellspacing="2" width="100%">
                                                                                                            <tr>
                                                                                                                <td class="popup_question_icon">
                                                                                                                    <asp:Label ID="CreateManualInterviewTest_questionDetailPreviewControlQuestionLabel"
                                                                                                                        runat="server" SkinID="sknLabelText" Text='<%# Eval("Question") %>'></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="td_padding_left_20">
                                                                                                                    <asp:RadioButtonList ID="CreateManualInterviewTest_questionPreviewControlAnswerRadioButtonList"
                                                                                                                        runat="server" CellSpacing="5" RepeatColumns="1" RepeatDirection="Horizontal"
                                                                                                                        TextAlign="Right" Width="100%">
                                                                                                                    </asp:RadioButtonList>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </asp:Panel>
                                                                                                <ajaxToolKit:HoverMenuExtender ID="CreateManualInterviewTest_hoverMenuExtender1"
                                                                                                    runat="server" HoverCssClass="popupHover" PopDelay="50" PopupControlID="CreateManualInterviewTest_hoverPanel"
                                                                                                    PopupPosition="Left" TargetControlID="CreateManualInterviewTest_hoverPanel" />
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="Categories" HeaderText="Category" InsertVisible="False"
                                                                                            ReadOnly="True">
                                                                                            <ItemStyle HorizontalAlign="Left" Width="140px" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="subject" HeaderText="Subject" InsertVisible="False" ReadOnly="True">
                                                                                            <ItemStyle HorizontalAlign="Left" Width="140px" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="Complexity" HeaderText="Complexity" InsertVisible="False"
                                                                                            ReadOnly="True">
                                                                                            <ItemStyle HorizontalAlign="Left" Width="75px" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                            <asp:HiddenField ID="CreateManualInterviewTest_adaptiveGridViewHiddenField" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <uc2:PageNavigator ID="CreateManualInterviewTest_adaptivebottomPagingNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td align="center" class="grid_header_bg">
                                                <table width="10%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="CreateManualInterviewTest_selectMultipleImage" runat="server"
                                                                SkinID="sknMultiDown_ArrowImageButton" CommandName="Select" ToolTip="Move questions to interview draft"
                                                                OnClick="CreateManualInterviewTest_selectMultipleImage_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="CreateManualInterviewTest_removeMultipleImage" runat="server"
                                                                SkinID="sknMultiUp_ArrowImageButton" CommandName="Select" ToolTip="Remove questions from interview draft"
                                                                OnClick="CreateManualInterviewTest_removeMultipleImage_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <asp:UpdatePanel ID="CreateManualInterviewTest_testDrftGridView_UpdatePanel" runat="server">
                                                                <ContentTemplate>
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                                            <td class="grid_header_bg">
                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="header_text_bold">
                                                                                            <asp:Literal ID="CreateManualInterviewTest_testDraftLiteral" runat="server" Text="Interview Draft"></asp:Literal>&nbsp;
                                                                                            <asp:Label ID="CreateManualInterviewTest_testDrafttHelpLabel" runat="server" SkinID="sknLabelText"
                                                                                                Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>" Visible="false"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr onmouseup="javascript:srsmouseup(event);" onmousemove="javascript:mouseMove(event);"
                                                                            style="height: 35px">
                                                                            <td class="grid_body_bg" valign="top">
                                                                                <div style="table-layout: fixed; overflow: auto; width: 100%">
                                                                                    <asp:GridView ID="CreateManualInterviewTest_testDrftGridView" OnRowDataBound="CreateManualInterviewTest_testDrftGridView_RowDataBound"
                                                                                        runat="server" OnRowCommand="CreateManualInterviewTest_testDrftGridView_RowCommand"
                                                                                        OnSorting="CreateManualInterviewTest_testDrftGridView_Sorting" EmptyDataText="&nbsp;">
                                                                                        <Columns>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="CreateManualInterviewTest_testDrftCheckbox" runat="server" /><asp:HiddenField
                                                                                                        ID="CreateManualInterviewTest_questionKeyHiddenField" runat="server" Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:ImageButton ID="CreateManualInterviewTest_testDrftRemoveImage" runat="server"
                                                                                                        SkinID="sknDeleteImageButton" CommandName="Select" ToolTip="Remove Question"
                                                                                                        CommandArgument='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' /></ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:ImageButton ID="CreateManualInterviewTest_byQuestion_moveDownQuestionImageButton"
                                                                                                        runat="server" SkinID="sknMoveDown_ArrowImageButton" CommandName="MoveDown" ToolTip="Move Question Down"
                                                                                                        CommandArgument='<%# Eval("DisplayOrder") %>' /></ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:ImageButton ID="CreateManualInterviewTest_byQuestion_moveUpQuestionImageButton"
                                                                                                        runat="server" SkinID="sknMoveUp_ArrowImageButton" CommandName="MoveUp" ToolTip="Move Question Up"
                                                                                                        CommandArgument='<%# Eval("DisplayOrder") %>' /><asp:HiddenField runat="server" ID="CreateManualInterviewTest_displayOrderHiddenField"
                                                                                                            Value='<%# Eval("DisplayOrder") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:BoundField ReadOnly="True" HeaderText="Question&nbsp;ID" InsertVisible="False"
                                                                                                DataField="QuestionID" Visible="False">
                                                                                                <ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
                                                                                            </asp:BoundField>
                                                                                            <asp:TemplateField HeaderText="Question">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="CreateManualInterviewTest_draftQuestionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),70) %>'
                                                                                                        Width="350px"></asp:Label></ItemTemplate>
                                                                                                <ItemStyle Wrap="true" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Category">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="CreateManualInterviewTest_categoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                                        Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label></ItemTemplate>
                                                                                                <ItemStyle Wrap="False" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Subject">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="CreateManualInterviewTest_subjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                                        Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label></ItemTemplate>
                                                                                                <ItemStyle Wrap="False" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Complexity">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="CreateManualInterviewTest_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                                        Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label></ItemTemplate>
                                                                                                <ItemStyle Wrap="False" />
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="CreateManualInterviewTest_selectMultipleImage" />
                                                                    <asp:AsyncPostBackTrigger ControlID="CreateManualInterviewTest_removeMultipleImage" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr id="trAddQuestion" runat="server">
                                            <td runat="server">
                                                <asp:LinkButton ID="CreateManualInterviewTest_addQuestionLinkButton" SkinID="sknAddLinkButton"
                                                    runat="server" Text="Add" ToolTip="Add new question" OnClick="CreateManualInterviewTest_addQuestionLinkButton_Click"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="CreateManualInterviewTest_testSummaryUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <uc5:ManualTestSummaryControl ID="CreateManualInterviewTest_testSummaryControl" runat="server" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                        <ajaxToolKit:TabPanel ID="CreateManualInterviewTest_testdetailsTabPanel" runat="server">
                            <HeaderTemplate>
                                Interview Details</HeaderTemplate>
                            <ContentTemplate>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td style="width: 100%">
                                            <asp:UpdatePanel ID="CreateManualInterviewTest_testDetailsUserControl_UpdatePanel"
                                                runat="server">
                                                <ContentTemplate>
                                                    <uc4:TestDetailsControl ID="CreateManualInterviewTest_testDetailsUserControl" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                    </ajaxToolKit:TabContainer>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="CreateManualInterviewTest_bottomMessageupdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="CreateManualInterviewTest_bottomSuccessMessageLabel" runat="server"
                                SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="CreateManualInterviewTest_bottomErrorMessageLabel" runat="server"
                                SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="CreateManualInterviewTest_questionUpdatePanel" runat="server">
                    <ContentTemplate>
                    <asp:Panel ID="CreateManualInterviewTest_addQuestionPanel" runat="server" CssClass="popupcontrol_addQuestion">
                        <div style="display: none;">
                            <asp:Button ID="CreateManualInterviewTest_addQuestionhiddenButton" runat="server"
                                Text="Hidden" /></div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="popup_td_padding_10">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                <asp:Literal ID="CreateManualInterviewTest_addQuestionLiteral" runat="server" Text="Add Question"></asp:Literal>
                                            </td>
                                            <td style="width: 50%" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="CreateManualInterviewTest_addQuestionTopCancelImageButton" runat="server"
                                                                SkinID="sknCloseImageButton" OnClick="CreateManualInterviewTest_addQuestionTopCancelImageButton_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_td_padding_10">
                                    <table cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                        <tr>
                                            <td align="left" class="popup_td_padding_10">
                                                <table border="0" cellpadding="0" cellspacing="5" class="tab_body_bg" align="left">
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="5">
                                                                <tr>
                                                                    <td class="msg_align" colspan="4">
                                                                        <asp:Label ID="CreateManualInterviewTest_addQuestionErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="CreateManualInterviewTest_addQuestionQuestLabel" runat="server" Text="Question"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class='mandatory'>*</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="CreateManualInterviewTest_addQuestionQuestTextBox" runat="server"
                                                                            MaxLength="3000" Height="52px" Width="498px" TextMode="MultiLine" Wrap="true"></asp:TextBox>
                                                                        <asp:LinkButton ID="CreateManualInterviewTest_addImageLinkButton" runat="server"
                                                                            SkinID="sknActionLinkButton" Text="Add Image" OnClientClick="javascript: return ShowAddImagePanel();" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr id="CreateManualInterviewTest_AddQuestionImageTR" runat="server" style="display: none">
                                                                    <td>
                                                                        <asp:Label ID="CreateManualInterviewTest_selectImageLabel" runat="server" Text="Select Image"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td style="width: 250px">
                                                                                    <ajaxToolKit:AsyncFileUpload ID="CreateManualInterviewTest_questionImageUpload" runat="server"
                                                                                        OnUploadedComplete="CreateManualInterviewTest_questionImageOnUploadComplete"
                                                                                        ClientIDMode="AutoID" />
                                                                                </td>
                                                                                <td style="width:30px">
                                                                                    <asp:ImageButton ID="CreateManualInterviewTest_helpImageButton" runat="server" SkinID="sknHelpImageButton"
                                                                                        ToolTip="Add Image" OnClientClick="javascript:return false;" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="CreateManualInterviewTest_questionImageButton" runat="server" Text="Add"
                                                                                        SkinID="sknButtonId" OnClick="CreateManualInterviewTest_questionImageButtonClick"
                                                                                        ToolTip="Click to add image" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="CreateManualInterviewTest_isQuestionRepository" runat="server"
                                                                            Text="Save question to the repository<br>(Please leave this box unchecked if this question is meant only for this interview and you do not wish to add it to the permanent question database)"
                                                                            Checked="false" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="CreateManualInterviewTest_addQuestionAnswerLabel" runat="server" Text="Answer"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class='mandatory'>*</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="CreateManualInterviewTest_addQuestionAnswerTextBox" runat="server"
                                                                            MaxLength="500" Height="52px" Width="498px" TextMode="MultiLine"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="CreateManualInterviewTest_addQuestioncomplexityLabel" runat="server"
                                                                            Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                    </td>
                                                                    <td>
                                                                        <div>
                                                                            <asp:DropDownList ID="CreateManualInterviewTest_addQuestioncomplexityDropDownList"
                                                                                runat="server" Width="133px">
                                                                            </asp:DropDownList>
                                                                            <asp:ImageButton ID="CreateManualInterviewTest_addQuestioncomplexityImageButton"
                                                                                SkinID="sknHelpImageButton" runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;"
                                                                                ToolTip="Please select the complexity of the question here" />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="CreateManualInterviewTest_addQuestionLabel" runat="server" Text="Interview Area"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                    </td>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td class="checkboxaddquestion_list_bg">
                                                                                    <asp:RadioButtonList ID="CreateManualInterviewTest_addQuestionRadioButtonList" runat="server"
                                                                                        RepeatColumns="2" RepeatDirection="Horizontal" CellSpacing="5" TextAlign="Right"
                                                                                        Width="100%" TabIndex="5">
                                                                                    </asp:RadioButtonList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="CreateManualInterviewTest_addQuestion_byQuestion_categoryLabel" runat="server"
                                                                            Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                    </td>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="CreateManualInterviewAddQuestion_byQuestion_categoryTextBox" ReadOnly="true"
                                                                                        runat="server" Text='<%# Eval("Category") %>'></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="CreateManualInterviewTest_addQuestion_byQuestion_subjectLabel" runat="server"
                                                                                        Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    <asp:HiddenField ID="CreateManualInterviewAddQuestion_byQuestion_categoryHiddenField"
                                                                                        runat="server" Value='<%# Eval("Cat_sub_id") %>' />
                                                                                    <asp:HiddenField ID="CreateManualInterviewAddQuestion_byQuestion_categoryNameHiddenField"
                                                                                        runat="server" Value='<%# Eval("Category") %>' />
                                                                                    <asp:HiddenField ID="CreateManualInterviewAddQuestion_byQuestion_subjectNameHiddenField"
                                                                                        runat="server" Value='<%# Eval("Subject") %>' />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="CreateManualInterviewAddQuestion_byQuestion_subjectTextBox" ReadOnly="true"
                                                                                        runat="server" Text='<%# Eval("Subject") %>'></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:ImageButton ID="CreateManualInterviewAddQuestion_byQuestion_categoryImageButton"
                                                                                        SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" ToolTip="Click here to select category and subject" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="CreateManualInterviewTest_tagsLabel" runat="server" Text="Tags"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="CreateManualInterviewTest_tagsTextBox" runat="server" MaxLength="100"
                                                                            Width="500px"></asp:TextBox>
                                                                        <asp:ImageButton ID="CreateManualInterviewTest_tagsImageButton" runat="server" SkinID="sknHelpImageButton"
                                                                            ToolTip="Please enter tags separated by commas" OnClientClick="javascript:return false;" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="5" class="tab_body_bg">
                                                                <tr id="CreateManualInterviewTest_DisplayQuestionImageTR" runat="server" style="display: none">
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Image runat="server" ID="CreateManualInterviewTest_questionImage" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="CreateManualInterviewTest_deleteLinkButton" runat="server" Text="Delete"
                                                                                        SkinID="sknActionLinkButton" OnClick="CreateManualInterviewTest_deleteLinkButtonClick"
                                                                                        ToolTip="Click to delete image" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_td_padding_5">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td align="left" style="width: 20px; padding-right: 5px">
                                                <asp:Button ID="CreateManualInterviewTest_addQuestioSaveButton" runat="server" SkinID="sknButtonId"
                                                    Text="Save" OnClick="CreateManualInterviewTest_addQuestioSaveButton_Clicked" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="CreateManualInterviewTest_addQuestioCloseLinkButton" SkinID="sknPopupLinkButton"
                                                    runat="server" Text="Cancel" 
                                                    OnClick="CreateManualInterviewTest_addQuestioCloseLinkButton_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <ajaxToolKit:ModalPopupExtender ID="CreateManualInterviewTest_addQuestioModalPopupExtender"
                        runat="server" PopupControlID="CreateManualInterviewTest_addQuestionPanel" TargetControlID="CreateManualInterviewTest_addQuestionhiddenButton"
                        BackgroundCssClass="modalBackground">
                    </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="72%" class="header_text">
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td width="22%">
                                        </td>
                                        <td width="40%" align="right">
                                            <asp:HiddenField ID="CreateManualInterviewTest_interviewTestKeyHiddenField" runat="server" />
                                            <asp:Button ID="CreateManualInterviewTest_bottomSaveButton" runat="server" SkinID="sknButtonId"
                                                Text="Save" OnClick="CreateManualInterviewTest_saveButton_Click" />
                                            <asp:Button ID="CreateManualInterviewTest_bottomCreateSessionButton" runat="server"
                                                SkinID="sknButtonId" Text="Create Session" Visible="false" />
                                        </td>
                                        <td width="16%" align="right">
                                            <asp:LinkButton ID="CreateManualInterviewTest_bottompResetLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Reset" OnClick="CreateManualInterviewTest_resetLinkButton_Click" />
                                        </td>
                                        <td width="4%" align="center">
                                            |
                                        </td>
                                        <td width="18%" align="left">
                                            <asp:LinkButton ID="CreateManualInterviewTest_bottomCancelLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="CreateManualInterviewTest_questionDetailPanel" runat="server" CssClass="popupcontrol_question_draft"
                                                Style="display: none">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="td_height_20">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                        <asp:Literal ID="CreateManualInterviewTest_questionDetailResultLiteral" runat="server"
                                                                            Text="Question Detail"></asp:Literal>
                                                                    </td>
                                                                    <td style="width: 50%" valign="top">
                                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:ImageButton ID="CreateManualInterviewTest_questionDetailPreviewControlTopCancelImageButton"
                                                                                        runat="server" SkinID="sknCloseImageButton" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_20">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:DataGrid ID="CreateManualInterviewTest_questionDetailCategoryDataGrid" runat="server"
                                                                                        AutoGenerateColumns="false">
                                                                                        <Columns>
                                                                                            <asp:BoundColumn HeaderText="Category" DataField="Category"></asp:BoundColumn>
                                                                                            <asp:BoundColumn HeaderText="Subject" DataField="Subject"></asp:BoundColumn>
                                                                                        </Columns>
                                                                                    </asp:DataGrid>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="popup_panel_inner_bg">
                                                                                    <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualInterviewTest_questionDetailTestAreaHeadLabel" runat="server"
                                                                                                    Text="Interview Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualInterviewTest_questionDetailTestAreaLabel" runat="server"
                                                                                                    Text="Concept" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualInterviewTest_questionDetailNoOfTestLabel" runat="server"
                                                                                                    Text="Number Of Administered Interview " SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualInterviewTest_questionDetailTestAdministeredLabel" runat="server"
                                                                                                    Text="5" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualInterviewTest_questionDetailAvgTimeLabel" runat="server"
                                                                                                    Text="Average Time Taken(in minutes)" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualInterviewTest_questionDetailAvgTimeValueLabel" runat="server"
                                                                                                    Text="15.5" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualInterviewTest_questionDetailCorrectToAttendedRatioHeadLabel"
                                                                                                    runat="server" Text=" Ratio of Correct Answer To Attended" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualInterviewTest_questionDetailCorrectToAttendedRatioLabel"
                                                                                                    runat="server" Text="36:50" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualInterviewTest_questionDetailComplexityHeadLabel" runat="server"
                                                                                                    Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="CreateManualInterviewTest_questionDetailPreviewControlComplexityLabel"
                                                                                                    runat="server" Text="Normal" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="popup_question_icon">
                                                                                    <asp:Label ID="CreateManualInterviewTest_questionDetailQuestionLabel" runat="server"
                                                                                        SkinID="sknLabelText"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_padding_left_20">
                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <asp:RadioButtonList ID="CreateManualInterviewTest_questionDetailAnswerRadioButtonList"
                                                                                            runat="server" RepeatColumns="2" RepeatDirection="Horizontal" CellSpacing="5"
                                                                                            TextAlign="Right" Width="100%">
                                                                                        </asp:RadioButtonList>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_8">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="padding-left: 30px">
                                                                        <table border="0" cellpadding="0" cellspacing="0" align="left">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Button ID="CreateManualInterviewTest_questionDetailTopAddButton" runat="server"
                                                                                        Text="Add" SkinID="sknButtonId" OnClick="CreateManualInterviewTest_questionDetailTopAddButton_Click" />
                                                                                </td>
                                                                                <td style="padding-left: 10px">
                                                                                    <asp:LinkButton ID="CreateManualInterviewTest_questionDetailTopCancelButton" runat="server"
                                                                                        SkinID="sknCancelLinkButton" Text="Cancel" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_8">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <div style="display: none">
                                                <asp:Button ID="CreateManualInterviewTest_adaptiveQuestionPreviewPanelTargeButton"
                                                    runat="server" />
                                            </div>
                                            <ajaxToolKit:ModalPopupExtender ID="CreateManualInterviewTest_adaptiveQuestionPreviewModalPopupExtender"
                                                runat="server" PopupControlID="CreateManualInterviewTest_questionDetailPanel"
                                                TargetControlID="CreateManualInterviewTest_adaptiveQuestionPreviewPanelTargeButton"
                                                BackgroundCssClass="modalBackground" CancelControlID="CreateManualInterviewTest_questionDetailTopCancelButton">
                                            </ajaxToolKit:ModalPopupExtender>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:UpdatePanel ID="CreateManualInterviewTest_questionDetailPreviewUpdatePanel"
        runat="server">
        <ContentTemplate>
            <asp:Panel ID="CreateManualInterviewTest_questionPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_question_detail">
                <div style="display: none">
                    <asp:Button ID="CreateManualInterviewTest_questionModalTargeButton" runat="server" />
                </div>
                <asp:HiddenField ID="CreateManualInterviewTest_previewQuestionAddHiddenField" runat="server" />
                <%--<uc3:QuestionDetailPreviewControl ID="CreateManualInterviewTest_questionDetailPreviewControl"
                    runat="server" OnCancelClick="CreateManualInterviewTest_cancelClick" />--%>
                <uc7:QuestionDetailSummaryControl ID="CreateManualInterviewTest_questionDetailSummaryControl"
                    runat="server" Title="Question Details Summary" OnCancelClick="CreateManualInterviewTest_cancelClick"
                    OnAddClick="CreateManualInterviewTest_questionDetailPreviewControl_AddClick" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="CreateManualInterviewTest_questionModalPopupExtender"
                runat="server" PopupControlID="CreateManualInterviewTest_questionPanel" TargetControlID="CreateManualInterviewTest_questionModalTargeButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="CreateManualInterviewTest_ConfirmPopupUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="CreateManualInterviewTest_authorIdHiddenField" runat="server" />
            <asp:HiddenField ID="CreateManualInterviewTest_positionProfileHiddenField" runat="server" />
            <asp:HiddenField ID="hiddenValue" runat="server" />
            <asp:HiddenField ID="hiddenDeleteValue" runat="server" />
            <div id="CreateManualInterviewTest_hiddenDIV" runat="server" style="display: none">
                <asp:Button ID="CreateManualInterviewTest_hiddenPopupModalButton" runat="server" />
            </div>
            <asp:Panel ID="CreateManualInterviewTest_ConfirmPopupPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_confirm_remove">
                <uc6:ConfirmMsgControl ID="CreateManualInterviewTest_ConfirmPopupExtenderControl"
                    runat="server" OnOkClick="CreateManualInterviewTest_okClick" OnCancelClick="CreateManualInterviewTest_cancelClick" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="CreateManualInterviewTest_ConfirmPopupExtender"
                runat="server" PopupControlID="CreateManualInterviewTest_ConfirmPopupPanel" TargetControlID="CreateManualInterviewTest_hiddenPopupModalButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
            <!-- Popup Save Button -->
            <div id="SearchQuestion_hiddenDIV" runat="server" style="display: none">
                <asp:Button ID="CreateManualInterviewTest_hiddenPopupModalButton1" runat="server" />
            </div>
            <asp:Panel ID="CreateManualInterviewTest_saveTestConfrimPopUpPanel" runat="server"
                Style="display: none" Height="210px" CssClass="popupcontrol_confirm">
                <uc6:ConfirmMsgControl ID="CreateManualInterviewTestSave_confirmPopupExtenderControl"
                    runat="server" OnOkClick="CreateManualInterviewTest_okPopUpClick" OnCancelClick="CreateManualInterviewTest_cancelPopUpClick" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="CreateManualInterviewTest_saveTestModalPopupExtender"
                runat="server" PopupControlID="CreateManualInterviewTest_saveTestConfrimPopUpPanel"
                TargetControlID="CreateManualInterviewTest_hiddenPopupModalButton1" BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
            <!-- End Popup Save Buttion -->
        </ContentTemplate>
    </asp:UpdatePanel>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load
    (
        function () {
            window.setTimeout(focus, 1);
        }
    )
        function focus() {
            try {
                document.getElementById('<%=CreateManualInterviewTest_categoryTextBox.ClientID %>').focus();
            }
            catch (Err) {
                try {
                    document.getElementById('<%=CreateManualInterviewTest_keywordTextBox.ClientID %>').focus();
                }
                catch (Err1) {
                    document.getElementById('ctl00_OTMMaster_body_CreateManualInterviewTest_mainTabContainer_CreateManualInterviewTest_testdetailsTabPanel_CreateManualInterviewTest_testDetailsUserControl_TestDetailsControl_testNameTextBox').focus();

                }
            }
        }
    </script>
</asp:Content>
