<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    CodeBehind="EditManualInterviewTest.aspx.cs" Inherits="Forte.HCM.UI.InterviewTestMaker.EditManualInterviewTest" %>

<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/QuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc3" %>
<%@ Register Src="../CommonControls/InterviewTestDetailsControl.ascx" TagName="TestDetailsControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/ManualInterviewTestSummaryControl.ascx" TagName="ManualTestSummaryControl"
    TagPrefix="uc5" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc6" %>
<%--<%@ Register Src="../CommonControls/QuestionDetailSummaryControl.ascx" TagName="QuestionDetailSummaryControl"
    TagPrefix="uc7" %>--%>
<%@ Register Src="../CommonControls/InterviewQuestionDetailSummaryControl.ascx" TagName="QuestionDetailSummaryControl"
    TagPrefix="uc7" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ContentPlaceHolderID="InterviewMaster_body" ID="EditManualInterviewTest_content"
    runat="server">
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        var control;
        function setFocus(sender, e) {
            try {
                var activeTab = $find('<%=EditManualInterviewTest_mainTabContainer.ClientID%>').get_activeTabIndex();
                if (activeTab == "0") {
                    try {
                        control = $get('<%=EditManualInterviewTest_categoryTextBox.ClientID%>');
                        control.focus();
                    }
                    catch (er1) {
                        try {
                            control = $get('<%=EditManualInterviewTest_keywordTextBox.ClientID%>');
                            control.focus();
                        }
                        catch (er2) {
                        }
                    }
                }
                else {
                    control = $get('ctl00_OTMMaster_body_EditManualInterviewTest_mainTabContainer_EditManualInterviewTest_testdetailsTabPanel_EditManualInterviewTest_testDetailsUserControl_TestDetailsControl_testNameTextBox');
                    control.focus();
                }
            }
            catch (er) {
            }
        }
        function ShowAddImagePanel() {
            var addImageTR = document.getElementById('<%=EditManualInterviewTest_AddQuestionImageTR.ClientID%>');
            addImageTR.style.display = "";
            var addImageBtn = document.getElementById('<%=EditManualInterviewTest_addImageLinkButton.ClientID%>');
            addImageBtn.style.display = "none";
            return false;
        }
    </script>
    <script type="text/javascript" language="javascript">
        // Display  Or Hide the adaptive TestDraft Or Manual interview Draft Questions.
        function ShowOrHideDiv(caseValue) {
            switch (caseValue.toString()) {
                case '0':
                    {
                        document.getElementById("EditManualInterviewTest_manualTd").style.display = "block";
                        document.getElementById("EditManualInterviewTest_adaptiveTd").style.display = "none";
                        document.getElementById("EditManualInterviewTest_manualTd").style.width = "100%";

                        document.getElementById("<%= EditManualInterviewTest_questionDiv.ClientID %>").style.width = "936px";
                        document.getElementById("<%= EditManualInterviewTest_adaptiveQuestionDiv.ClientID %>").style.width = "0";
                        break;
                    }
                case '1':
                    {
                        document.getElementById("EditManualInterviewTest_manualTd").style.display = "none";
                        document.getElementById("EditManualInterviewTest_adaptiveTd").style.display = "block";
                        document.getElementById("EditManualInterviewTest_manualTd").style.width = "0";
                        document.getElementById("EditManualInterviewTest_adaptiveTd").style.width = "100%";

                        document.getElementById("<%= EditManualInterviewTest_questionDiv.ClientID %>").style.width = "0";
                        document.getElementById("<%= EditManualInterviewTest_adaptiveQuestionDiv.ClientID %>").style.width = "936px";
                        break;
                    }
                case '2':
                    {
                        document.getElementById("EditManualInterviewTest_manualTd").style.display = "block";
                        document.getElementById("EditManualInterviewTest_adaptiveTd").style.display = "block";
                        document.getElementById("EditManualInterviewTest_manualTd").style.width = "49%";
                        document.getElementById("EditManualInterviewTest_adaptiveTd").style.width = "50%";

                        document.getElementById("<%= EditManualInterviewTest_questionDiv.ClientID %>").style.width = "450px";
                        document.getElementById("<%= EditManualInterviewTest_adaptiveQuestionDiv.ClientID %>").style.width = "456px";
                        break;
                    }
                default:
                    {
                        document.getElementById("EditManualInterviewTest_manualTd").style.display = "block";
                        document.getElementById("EditManualInterviewTest_adaptiveTd").style.display = "block";
                        document.getElementById("EditManualInterviewTest_manualTd").style.width = "49%";
                        document.getElementById("EditManualInterviewTest_adaptiveTd").style.width = "50%";

                        document.getElementById("<%= EditManualInterviewTest_questionDiv.ClientID %>").style.width = "450px";
                        document.getElementById("<%= EditManualInterviewTest_adaptiveQuestionDiv.ClientID %>").style.width = "456px";
                        break;
                    }
            }
            return false;
        }

        var modifiedRow = null;
        var dataCollection = new Array();
        dataCollection = null;
        var sourceRow = null;
        var sourcePosition = null;
        var sourceTable = null;
        var currenttable = null;
        var dropTargets = null;
        var dragObject = null;
        var mouseOffSet = null;
        var destTable = null;
        var oldY = 0;
        this.table = null;

        //move the Single Question add to Testdraft.
        function singleqnsclick(QuestionID, buttonID) {
            //alert(buttonID);
            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
            hiddenvalue.value = QuestionID;
            __doPostBack(buttonID, "OnClick");
        }

        //related to the Drag and Drop questions 
        function mousedown(QuestionID, source) {
            sourceTable = document.getElementById(source);

            if (checkChecked(QuestionID)) {
                CheckNoOfChecked(QuestionID);
            }
            else {
                CheckCorrectQuestion(QuestionID);
            }
        }

        //related to the Drag and Drop questions 
        function CheckCorrectQuestion(id) {
            var table = sourceTable;
            var testVar = document.getElementById('<%=EditManualInterviewTest_searchQuestionGridView.ClientID %>');
            var testVarTwo = document.getElementById('<%=EditManualInterviewTest_adaptiveGridView.ClientID %>');
            id++;
            var rowid = 1;
            var chkbox;
            var rowCount = table.rows.length;
            for (var i = 1; i < rowCount; i++) {
                if (id == i) {
                    var row = table.rows[i];
                    if (detectBrowser()) {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else {
                        chkbox = row.cells[0].childNodes[0];
                    }

                    if (null != chkbox) {
                        chkbox.checked = true;
                        // row.cells[0].childNodes[0].checked = true;

                    }

                }
                if ((testVar == sourceTable) || (testVarTwo == sourceTable)) {
                    CheckNoOfChecked(id);
                }
            }

        }

        //find the Browser details
        function detectBrowser() {
            var browser = navigator.appName;
            var b_version = navigator.appVersion;
            var version = parseFloat(b_version);
            if (browser == "Netscape") {
                return true;
            }
            else {
                return false; ;
            }
        }

        // Question remove from the Interview Draft.
        function RemoveRows(rowIndex) {
            sourceTable = document.getElementById('<%=EditManualInterviewTest_testDrftGridView.ClientID %>');

            if (confirm('Are you sure to delete these question(s)?')) {
                if (checkChecked(rowIndex)) {
                    DelRows();

                }
                else {
                    CheckCorrectQuestion(rowIndex);
                    DelRows();
                }
            }
            else
                return false;
        }

        function DelRows() {
            try {
                var table = document.getElementById('<%=EditManualInterviewTest_testDrftGridView.ClientID %>');
                var rowCount = table.rows.length;
                for (var i = 0; i < rowCount; i++) {
                    var row = table.rows[i];
                    var chkbox;
                    if (detectBrowser()) {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else {
                        chkbox = row.cells[0].childNodes[0];
                    }
                    if (null != chkbox && true == chkbox.checked) {
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                    }
                }
            } catch (e) {
                alert(e);
            }
            return false;
        }

        function CheckNoOfChecked(QuestionID) {

            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');

            hiddenvalue.value = "";

            var numberOfQuestions = 0;

            var table = sourceTable;
            // document.getElementById('<%=EditManualInterviewTest_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {

                    numberOfQuestions = numberOfQuestions + 1;

                    var noOfQuestionsTextBox = document.getElementById('<%=sampleDIVTextBox.ClientID %>');

                    noOfQuestionsTextBox.value = 'Move' + numberOfQuestions + ' questions';

                    dragObject = document.getElementById('<%=moveDisplayDIV.ClientID %>');

                    if (detectBrowser()) {
                        if (hiddenvalue.value == "") {
                            hiddenvalue.value = row.childNodes[5].childNodes[1].defaultValue + ',';
                        }
                        else {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[5].childNodes[1].defaultValue + ',';
                        }
                    }
                    else {
                        if (hiddenvalue.value == "") {
                            hiddenvalue.value = row.childNodes[4].childNodes[0].value + ',';
                        }
                        else {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[4].childNodes[0].value + ',';
                        }
                    }
                }
            }
            //var ShowDiv =
        }

        function checkChecked(id) {

            var isChecked = false;

            var table = sourceTable;
            // document.getElementById('<%=EditManualInterviewTest_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {
                    isChecked = true;
                }

                if (isChecked) {
                    return true;
                }
                else {
                    return false;
                }
            }

        }

        function mouseMove(ev) {
            ev = ev || window.event;
            var mousePos = mouseCoords(ev);

            if (dragObject) {
                //mouseOffset = getMouseOffset(dragObject, ev);
                dragObject.style.display = 'block';
                dragObject.style.backgroundColor = "#eee";
                dragObject.style.position = 'absolute';
                if (detectBrowser()) {
                    dragObject.style.top = mousePos.y + 'px';
                    dragObject.style.left = mousePos.x + 'px';
                }
                else {
                    dragObject.style.top = mousePos.y + document.documentElement.scrollTop + 10;
                    dragObject.style.left = mousePos.x + 10;
                }
            }
            else {

            }
            return false;
        }
        function mouseCoords(ev) {
            if (ev != null) {
                if (ev.pageX || ev.pageY) { return { x: ev.pageX, y: ev.pageY }; }
                return {
                    x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
                    y: ev.clientY + document.body.scrollTop - document.body.clientTop
                };
            }
        }

        function srsmouseup(ev) {
            if (dragObject == null)
                return false;
            var ev = ev || window.event;
            var mousePos = mouseCoords(ev);
            var curTarget = document.getElementById('<%=EditManualInterviewTest_testDrftGridView.ClientID %>');
            var targPos = getPosition(curTarget);

            var scrolltargPos;
            if (detectBrowser()) {
                scrolltargPos = targPos.y;
            }
            else {
                scrolltargPos = targPos.y - document.documentElement.scrollTop;
            }

            var targWidth = parseInt(curTarget.offsetWidth);
            var targHeight = parseInt(curTarget.offsetHeight);
            if (
            (mousePos.x >= targPos.x) &&
            (mousePos.y >= scrolltargPos)) {

                __doPostBack('ctl00_HCMMaster_body_EditManualInterviewTest_mainTabContainer_EditManualInterviewTest_questionsTabPanel_EditManualInterviewTest_selectMultipleImage', "OnClick");
                dragObject.style.display = 'none';
            }
            else {
                if (dragObject != null) {
                    makeUnchekabale();
                    dragObject.style.display = 'none';

                    var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
                    hiddenvalue.value = "";
                }
            } dragObject = null;
        }

        function makeUnchekabale() {
            var table = sourceTable;
            //document.getElementById('<%=EditManualInterviewTest_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {
                    chkbox.checked = false;
                }
            }
        }

        function getPosition(e) {
            var left = 0;
            var top = 0;

            if (e != null) {
                while (e.offsetParent) {
                    left += e.offsetLeft;
                    top += e.offsetTop;
                    e = e.offsetParent;
                }


                left += e.offsetLeft;
                top += e.offsetTop;
            }

            return { x: left, y: top };
        }
    
    </script>
    <asp:Panel ID="EditManualTest_mainPanel" runat="server" DefaultButton="EditManualInterviewTest_bottomSaveButton">
        <table width="100%" border="0" cellspacing="3" cellpadding="0">
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="60%" class="header_text_bold">
                                <asp:Literal ID="EditManualInterviewTest_headerLiteral" runat="server" Text="Edit Interview"></asp:Literal>
                            </td>
                            <td width="40%" align="right">
                                <table border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="EditManualInterviewTest_topSaveButton" runat="server" SkinID="sknButtonId"
                                                Text="Save" OnClick="EditManualInterviewTest_saveButton_Click" />
                                        </td>
                                        <td align="center">
                                            <asp:Button ID="EditManualInterviewTest_topCreateSessionButton" runat="server" SkinID="sknButtonId"
                                                Text="Create Interview Session" />
                                        </td>
                                        <td align="center">
                                            <asp:LinkButton ID="EditManualInterviewTest_adptiveTestLinkButton" SkinID="sknActionLinkButton"
                                                runat="server" Text="Edit Interview With Adaptive" OnClick="EditManualInterviewTest_adptiveTestLinkButton_Click">
                                            </asp:LinkButton>
                                        </td>
                                        <td align="center">
                                            |
                                        </td>
                                        <td align="center">
                                            <asp:LinkButton ID="EditManualInterviewTest_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                Text="Reset" OnClick="EditManualInterviewTest_resetLinkButton_Click" />
                                        </td>
                                        <td align="center">
                                            |
                                        </td>
                                        <td align="left">
                                            <asp:LinkButton ID="EditManualInterviewTest_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                Text="Cancel" OnClick="ParentPageRedirect" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="EditManualInterviewTest_topMessageUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="EditManualInterviewTest_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="EditManualInterviewTest_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <ajaxToolKit:TabContainer ID="EditManualInterviewTest_mainTabContainer" runat="server"
                        ActiveTabIndex="0">
                        <ajaxToolKit:TabPanel ID="EditManualInterviewTest_questionsTabPanel" HeaderText="questions"
                            runat="server">
                            <HeaderTemplate>
                                Questions</HeaderTemplate>
                            <ContentTemplate>
                                <asp:Panel ID="EditManualTest_testDetailsPanel" runat="server" DefaultButton="EditManualInterviewTest_topSearchButton">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div id="EditManualInterviewTest_searchCriteriasDiv" runat="server" style="display: block;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="right" class="td_padding_bottom_5">
                                                                <asp:UpdatePanel ID="EditManualInterviewTest_simpleLinkButtonUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="EditManualInterviewTest_simpleLinkButton" runat="server" Text="Advanced"
                                                                            SkinID="sknActionLinkButton" OnClick="EditManualInterviewTest_simpleLinkButton_Click" /></ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="panel_bg">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td width="100%">
                                                                            <asp:UpdatePanel ID="EditManualInterviewTest_searchDiv_UpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <div id="EditManualInterviewTest_simpleSearchDiv" runat="server">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="EditManualInterviewTest_categoryHeadLabel" runat="server" Text="Category"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="EditManualInterviewTest_categoryTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="EditManualInterviewTest_subjectHeadLabel" runat="server" Text="Subject"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="EditManualInterviewTest_subjectTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="EditManualInterviewTest_keywordsHeadLabel" runat="server" Text="Keyword"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <div style="float: left; padding-right: 5px;">
                                                                                                        <asp:TextBox ID="EditManualInterviewTest_keywordsTextBox" runat="server" MaxLength="100"></asp:TextBox></div>
                                                                                                    <div style="float: left;">
                                                                                                        <asp:ImageButton ID="EditManualInterviewTest_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div id="EditManualInterviewTest_advanceSearchDiv" runat="server">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <uc1:CategorySubjectControl ID="EditManualInterviewTest_categorySubjectControl" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="td_height_8">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="panel_inner_body_bg">
                                                                                                    <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td style="width: 10%">
                                                                                                                <asp:Label ID="EditManualInterviewTest_testAreaHeadLabel" runat="server" Text="Interview Area"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="5" class="checkbox_list_bg" align="left" style="width: 90%">
                                                                                                                <asp:CheckBoxList ID="EditManualInterviewTest_testAreaCheckBoxList" runat="server"
                                                                                                                    RepeatColumns="4" RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                                </asp:CheckBoxList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 10%">
                                                                                                                <asp:Label ID="EditManualInterviewTest_complexityLabel" runat="server" Text="Complexity"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="3" align="left" valign="middle">
                                                                                                                <div style="float: left; padding-right: 5px; width: 90%; vertical-align: middle;"
                                                                                                                    class="checkbox_list_bg">
                                                                                                                    <asp:CheckBoxList ID="EditManualInterviewTest_complexityCheckBoxList" runat="server"
                                                                                                                        RepeatColumns="4" RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                                    </asp:CheckBoxList>
                                                                                                                </div>
                                                                                                                <div style="float: left; vertical-align: middle; padding-left: 5px; padding-top: 5px;">
                                                                                                                    <asp:ImageButton ID="EditManualInterviewTest_complexityImageButton" SkinID="sknHelpImageButton"
                                                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, ComplexityHelp %>" /></div>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="EditManualInterviewTest_keywordHeadLabel" runat="server" Text="Keyword"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="EditManualInterviewTest_keywordTextBox" runat="server" Columns="50"
                                                                                                                        MaxLength="100"></asp:TextBox></div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="EditManualInterviewTest_keywordImageButton" SkinID="sknHelpImageButton"
                                                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="EditManualInterviewTest_questionLabel" runat="server" Text="Question ID"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="EditManualInterviewTest_questionIDTextBox" runat="server" MaxLength="15"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="EditManualInterviewTest_creditHeadLabel" runat="server" Text="Credit"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="EditManualInterviewTest_creditTextBox" runat="server"></asp:TextBox><ajaxToolKit:FilteredTextBoxExtender
                                                                                                                    ID="EditManualInterviewTest_weightageFileteredExtender" runat="server" TargetControlID="EditManualInterviewTest_creditTextBox"
                                                                                                                    FilterType="Numbers" Enabled="True">
                                                                                                                </ajaxToolKit:FilteredTextBoxExtender>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="EditManualInterviewTest_authorHeadLabel" runat="server" Text="Author"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="EditManualInterviewTest_authorTextBox" runat="server"></asp:TextBox><asp:HiddenField
                                                                                                                        ID="EditManualInterviewTest_dummyAuthorID" runat="server" />
                                                                                                                </div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="EditManualInterviewTest_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                                                        runat="server" ImageAlign="Middle" /></div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="EditManualInterviewTest_positionProfileLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                    Text="Position Profile"></asp:Label>
                                                                                                            </td>
                                                                                                            <td colspan="3">
                                                                                                                <div style="float: left; padding-right: 5px;">
                                                                                                                    <asp:TextBox ID="EditManualInterviewTest_positionProfileTextBox" runat="server" MaxLength="200"
                                                                                                                        Columns="55"></asp:TextBox></div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="EditManualInterviewTest_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to select position profile" /></div>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="EditManualInterviewTest_positionProfileKeywordLabel" runat="server"
                                                                                                                    SkinID="sknLabelFieldHeaderText" Text="Position Profile Keyword" MaxLength="500"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="float: left; padding-right: 5px; width: 100%">
                                                                                                                    <asp:TextBox ID="EditManualInterviewTest_positionProfileKeywordTextBox" runat="server"
                                                                                                                        MaxLength="25"></asp:TextBox></div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" class="td_padding_top_5">
                                                                            <asp:Button ID="EditManualInterviewTest_topSearchButton" runat="server" Text="Search"
                                                                                SkinID="sknButtonId" OnClick="EditManualInterviewTest_searchQuestionButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr id="EditManualInterviewTest_searchResultsTR" runat="server">
                                            <td class="header_bg" runat="server">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 80%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="EditManualInterviewTest_searchResultsLiteral" runat="server" Text="Questions"></asp:Literal>&#160;<asp:Label
                                                                ID="EditManualInterviewTest_sortHelpLabel" runat="server" SkinID="sknLabelText"></asp:Label>
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="EditManualInterviewTest_manualImageButton" runat="server" SkinID="sknManualImageButton"
                                                                ToolTip="Click here to view 'Search Results' only" OnClientClick="javascript:return ShowOrHideDiv('0');" />
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="EditManualInterviewTest_adaptiveImageButton" runat="server"
                                                                SkinID="sknAdaptiveImageButton" ToolTip="Click here to view 'Adaptive Recommended Questions' only"
                                                                OnClientClick="javascript:return ShowOrHideDiv('1');" />
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:ImageButton ID="EditManualInterviewTest_bothImageButton" runat="server" SkinID="sknCombinedAMImageButton"
                                                                ToolTip="Click here to view both 'Search Results' and 'Adaptive Recommended Questions'"
                                                                OnClientClick="javascript:return ShowOrHideDiv('2');" />
                                                        </td>
                                                        <td style="width: 5%" align="right">
                                                            <span id="EditManualInterviewTest_searchResultsUpSpan" runat="server" style="display: none;">
                                                                <asp:Image ID="EditManualInterviewTest_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                                    id="EditManualInterviewTest_searchResultsDownSpan" runat="server" style="display: block;"><asp:Image
                                                                        ID="EditManualInterviewTest_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span><asp:HiddenField
                                                                            ID="EditManualInterviewTest_restoreHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td valign="top">
                                                <div id="EditManualInterviewTest_manualTd" runat="server" style="width: 100%; float: left;
                                                    display: block;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="grid_header_bg">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="header_text_bold">
                                                                            <asp:Literal ID="EditManualInterviewTest_manualQuestLiteral" runat="server" Text="Search Results"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="grid_body_bg">
                                                                <asp:UpdatePanel ID="EditManualInterviewTest_searchQuestionGridView_UpdatePanel"
                                                                    runat="server">
                                                                    <ContentTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="height: 180px; width: 939px; overflow: auto;" runat="server" id="EditManualInterviewTest_questionDiv">
                                                                                        <asp:GridView ID="EditManualInterviewTest_searchQuestionGridView" runat="server"
                                                                                            AutoGenerateColumns="False" AllowSorting="True" GridLines="None" Width="100%"
                                                                                            OnRowDataBound="EditManualInterviewTest_searchQuestionGridView_RowDataBound"
                                                                                            OnRowCommand="EditManualInterviewTest_searchQuestionGridView_RowCommand" OnSorting="EditManualInterviewTest_searchQuestionGridView_Sorting">
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:CheckBox ID="EditManualInterviewTest_searchQuestionCheckbox" runat="server" /><asp:HiddenField
                                                                                                            ID="EditManualInterviewTest_searchQuestionKeyHiddenField" runat="server" Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="EditManualInterviewTest_previewImageButton" runat="server" SkinID="sknPreviewImageButton"
                                                                                                            CommandName="view" CommandArgument='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>'
                                                                                                            ToolTip="Preview Question" /></ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="EditManualInterviewTest_searchQuestionAlertImageButton" runat="server"
                                                                                                            SkinID="sknAlertImageButton" ToolTip="Flag Question" /></ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="EditManualInterviewTest_searchQuestionSelectImage" runat="server"
                                                                                                            SkinID="sknMoveDown_ArrowImageButton" CommandName="Select" ToolTip="Select Question" /></ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="EditManualInterviewTest_hidddenValue" runat="server" Value='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField DataField="QuestionID" Visible="False"></asp:BoundField>
                                                                                                <asp:BoundField DataField="QuestionRelationID" Visible="False"></asp:BoundField>
                                                                                                <asp:TemplateField HeaderText="Question">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="EditManualInterviewTest_questionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),65) %>'
                                                                                                            Width="360px" ToolTip='<%# Eval("Question").ToString() %>'></asp:Label></ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Category">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="EditManualInterviewTest_categoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                                            Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label></ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Subject">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="EditManualInterviewTest_subjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                                            Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label></ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Answer">
                                                                                                    <ItemTemplate>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Complexity">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="EditManualInterviewTest_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                                            Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label></ItemTemplate>
                                                                                                    <ItemStyle Wrap="False" />
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <uc2:PageNavigator ID="EditManualInterviewTest_bottomPagingNavigator" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <div id="moveDisplayDIV" style="display: none; background-color: Lime" runat="server">
                                                                                <asp:TextBox ID="sampleDIVTextBox" runat="server" ReadOnly="True"></asp:TextBox></div>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="EditManualInterviewTest_topSearchButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="EditManualInterviewTest_adaptiveTd" style="width: 0%; float: right; display: none;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="grid_header_bg">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="header_text_bold">
                                                                            <asp:Literal ID="EditManualInterviewTest_adaptiveQuestLiteral" runat="server" Text="Adaptive Recommended Questions"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                            <td class="grid_body_bg">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="height: 180px; width: 0px; overflow: auto;" runat="server" id="EditManualInterviewTest_adaptiveQuestionDiv">
                                                                                <asp:GridView ID="EditManualInterviewTest_adaptiveGridView" runat="server" AutoGenerateColumns="False"
                                                                                    PageSize="4" AllowSorting="True" GridLines="None" Width="100%" OnRowDataBound="EditManualInterviewTest_adaptiveGridView_RowDataBound"
                                                                                    OnRowCommand="EditManualInterviewTest_adaptiveGridView_RowCommand" OnSorting="EditManualInterviewTest_adaptiveGridView_Sorting"
                                                                                    EnableModelValidation="True">
                                                                                    <Columns>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="EditManualInterviewTest_adaptiveCheckbox" runat="server" /></ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditManualInterviewTest_adptivepreviewImageButton" runat="server"
                                                                                                    SkinID="sknPreviewImageButton" CommandName="view" ToolTip="Preview Question" /></ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditManualInterviewTest_adaptiveAlertImageButton" runat="server"
                                                                                                    SkinID="sknAlertImageButton" ToolTip="Flag Question" /></ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditManualInterviewTest_adaptiveSelectImage" runat="server"
                                                                                                    SkinID="sknMoveDown_ArrowImageButton" ToolTip="Select Question" /></ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="EditManualInterviewTest_hidddenValue" runat="server" Value='<%#Eval("QuestionID") %>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditManualInterviewTest_adaptiveQuestionDetailsImage" runat="server"
                                                                                                    SkinID="sknDraftImageButton" ToolTip="Adaptive Question Details" CommandName="AdaptiveQuestion" /></ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="QuestionID" Visible="False"></asp:BoundField>
                                                                                        <asp:BoundField DataField="QuestionRelationID" Visible="False"></asp:BoundField>
                                                                                        <asp:TemplateField HeaderText="Question">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditManualInterviewTest_questionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),70) %>'></asp:Label><asp:Panel
                                                                                                    ID="EditManualInterviewTest_hoverPanel" runat="server" CssClass="popupMenu table_outline_bg">
                                                                                                    <table width="100%" border="0" cellspacing="2" cellpadding="3">
                                                                                                        <tr>
                                                                                                            <th class="popup_question_icon">
                                                                                                                <asp:Label ID="EditManualInterviewTest_questionDetailPreviewControlQuestionLabel"
                                                                                                                    Text='<%# Eval("Question") %>' runat="server" SkinID="sknLabelText"></asp:Label>
                                                                                                            </th>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <th class="td_padding_left_20">
                                                                                                                <asp:RadioButtonList ID="EditManualInterviewTest_questionDetailPreviewControlAnswerRadioButtonList"
                                                                                                                    runat="server" RepeatColumns="1" RepeatDirection="Horizontal" CellSpacing="5"
                                                                                                                    TextAlign="Right" Width="100%">
                                                                                                                </asp:RadioButtonList>
                                                                                                            </th>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </asp:Panel>
                                                                                                <ajaxToolKit:HoverMenuExtender ID="EditManualInterviewTest_hoverMenuExtender1" runat="server"
                                                                                                    PopupControlID="EditManualInterviewTest_hoverPanel" PopupPosition="Left" HoverCssClass="popupHover"
                                                                                                    TargetControlID="EditManualInterviewTest_hoverPanel" PopDelay="50" />
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField ReadOnly="True" HeaderText="Category" InsertVisible="False" DataField="Categories">
                                                                                            <ItemStyle HorizontalAlign="Left" Width="140px"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField ReadOnly="True" HeaderText="Subject" InsertVisible="False" DataField="subject">
                                                                                            <ItemStyle HorizontalAlign="Left" Width="140px" Wrap="False"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField ReadOnly="True" HeaderText="Answer" InsertVisible="False">
                                                                                            <ItemStyle HorizontalAlign="Left" Width="120px" Wrap="False"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField ReadOnly="True" HeaderText="Complexity" InsertVisible="False" DataField="Complexity">
                                                                                            <ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                            <asp:HiddenField ID="EditManualInterviewTest_adaptiveGridViewHiddenField" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <uc2:PageNavigator ID="EditManualInterviewTest_adaptivebottomPagingNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td align="center" class="grid_header_bg">
                                                <table width="10%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="EditManualInterviewTest_selectMultipleImage" runat="server"
                                                                SkinID="sknMultiDown_ArrowImageButton" CommandName="Select" ToolTip="Move questions to interview draft"
                                                                OnClick="EditManualInterviewTest_selectMultipleImage_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="EditManualInterviewTest_removeMultipleImage" runat="server"
                                                                SkinID="sknMultiUp_ArrowImageButton" CommandName="Select" ToolTip="Remove questions from interview draft"
                                                                OnClick="EditManualInterviewTest_removeMultipleImage_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="EditManualInterviewTest_testDrftGridView_UpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                                <td class="grid_header_bg">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="header_text_bold">
                                                                                <asp:Literal ID="EditManualInterviewTest_testDraftLiteral" runat="server" Text="Interview Draft"></asp:Literal>&nbsp;
                                                                                <asp:Label ID="EditManualInterviewTest_testDrafttHelpLabel" runat="server" SkinID="sknLabelText"
                                                                                    Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>" Visible="false"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr onmouseup="javascript:srsmouseup(event);" onmousemove="javascript:mouseMove(event);"
                                                                style="height: 35px">
                                                                <td class="grid_body_bg" valign="top">
                                                                    <div style="overflow: auto; width: 939px">
                                                                        <asp:GridView ID="EditManualInterviewTest_testDrftGridView" OnRowDataBound="EditManualInterviewTest_testDrftGridView_RowDataBound"
                                                                            runat="server" GridLines="Horizontal" BorderColor="white" BorderWidth="1px" Width="100%"
                                                                            OnRowCommand="EditManualInterviewTest_testDrftGridView_RowCommand" OnSorting="EditManualInterviewTest_testDrftGridView_Sorting"
                                                                            EmptyDataText="&nbsp;">
                                                                            <RowStyle CssClass="grid_alternate_row" />
                                                                            <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                            <HeaderStyle CssClass="grid_header_row" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="EditManualInterviewTest_testDrftCheckbox" runat="server" /><asp:HiddenField
                                                                                            ID="EditManualInterviewTest_testDraftQuestionKeyHiddenField" runat="server" Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId")%>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="EditManualInterviewTest_testDrftRemoveImage" runat="server"
                                                                                            SkinID="sknDeleteImageButton" CommandName="Select" ToolTip="Remove Question"
                                                                                            CommandArgument='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' /></ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="EditManualInterviewTest_byQuestion_moveDownQuestionImageButton"
                                                                                            runat="server" SkinID="sknMoveDown_ArrowImageButton" CommandName="MoveDown" ToolTip="Move Question Down"
                                                                                            CommandArgument='<%# Eval("DisplayOrder") %>' /></ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="EditManualInterviewTest_byQuestion_moveUpQuestionImageButton"
                                                                                            runat="server" SkinID="sknMoveUp_ArrowImageButton" CommandName="MoveUp" ToolTip="Move Question Up"
                                                                                            CommandArgument='<%# Eval("DisplayOrder") %>' /><asp:HiddenField runat="server" ID="EditManualInterviewTest_displayOrderHiddenField"
                                                                                                Value='<%# Eval("DisplayOrder") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ReadOnly="True" HeaderText="Question&nbsp;ID" InsertVisible="False"
                                                                                    DataField="QuestionID" Visible="False">
                                                                                    <ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField HeaderText="Question">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="EditManualInterviewTest_draftQuestionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),70) %>'
                                                                                            Width="370px" ToolTip='<%# Eval("Question").ToString() %>'></asp:Label></ItemTemplate>
                                                                                    <ItemStyle Wrap="true" />
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Category">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="EditManualInterviewTest_categoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                            Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label></ItemTemplate>
                                                                                    <ItemStyle Wrap="False" />
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Subject">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="EditManualInterviewTest_subjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                            Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label></ItemTemplate>
                                                                                    <ItemStyle Wrap="False" />
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Complexity">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="EditManualInterviewTest_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                            Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label></ItemTemplate>
                                                                                    <ItemStyle Wrap="False" />
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="EditManualInterviewTest_selectMultipleImage" />
                                                        <asp:AsyncPostBackTrigger ControlID="EditManualInterviewTest_removeMultipleImage" />
                                                        <asp:AsyncPostBackTrigger ControlID="EditManualInterviewTest_searchQuestionGridView" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="EditManualInterviewTest_addQuestionLinkButton" SkinID="sknAddLinkButton"
                                                    runat="server" Text="Add new question" ToolTip="Add new question" OnClick="EditManualInterviewTest_addQuestionLinkButton_Click"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <uc5:ManualTestSummaryControl ID="EditManualInterviewTest_testSummaryControl" runat="server" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                        <ajaxToolKit:TabPanel ID="EditManualInterviewTest_testdetailsTabPanel" runat="server">
                            <HeaderTemplate>
                                Interview Details</HeaderTemplate>
                            <ContentTemplate>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td style="width: 100%">
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
                                                    <uc4:TestDetailsControl ID="EditManualInterviewTest_testDetailsUserControl" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                    </ajaxToolKit:TabContainer>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="EditManualInterviewTest_bottomMessageupdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="EditManualInterviewTest_bottomSuccessMessageLabel" runat="server"
                                SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="EditManualInterviewTest_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <!-- Popup Added -->
            <tr>
                <td>
                    <asp:UpdatePanel ID="EditManualInterviewTest_questionUpdatePanel" runat="server">
                    <ContentTemplate>                    
                    <asp:Panel ID="EditManualInterviewTest_addQuestionPanel" runat="server" CssClass="popupcontrol_addQuestion">
                        <div style="display: none;">
                            <asp:Button ID="EditManualInterviewTest_addQuestionhiddenButton" runat="server" Text="Hidden" /></div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="popup_td_padding_10">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                <asp:Literal ID="EditManualInterviewTest_addQuestionLiteral" runat="server" Text="Add Question"></asp:Literal>
                                            </td>
                                            <td style="width: 50%" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="EditManualInterviewTest_addQuestionTopCancelImageButton" runat="server"
                                                                SkinID="sknCloseImageButton" 
                                                                onclick="EditManualInterviewTest_addQuestionTopCancelImageButton_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_td_padding_10">
                                    <table cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                        <tr>
                                            <td align="left" class="popup_td_padding_10">
                                                <table border="0" cellpadding="0" cellspacing="5" class="tab_body_bg" align="left">
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="5">
                                                                <tr>
                                                                    <td class="msg_align" colspan="4">
                                                                        <asp:Label ID="EditManualInterviewTest_addQuestionErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="EditManualInterviewTest_addQuestionQuestLabel" runat="server" Text="Question"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class='mandatory'>*</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="EditManualInterviewTest_addQuestionQuestTextBox" runat="server"
                                                                            MaxLength="3000" Height="52px" Width="498px" TextMode="MultiLine" Wrap="true"></asp:TextBox>
                                                                        <asp:LinkButton ID="EditManualInterviewTest_addImageLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                            Text="Add Image"  ToolTip="Click here to add image" OnClientClick="javascript: return ShowAddImagePanel();" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr id="EditManualInterviewTest_AddQuestionImageTR" runat="server" style="display: none">
                                                                    <td>
                                                                        <asp:Label ID="EditManualInterviewTest_selectImageLabel" runat="server" Text="Select Image"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td style="width: 250px">
                                                                                    <ajaxToolKit:AsyncFileUpload ID="EditManualInterviewTest_questionImageUpload" runat="server"
                                                                                        OnUploadedComplete="EditManualInterviewTest_questionImageOnUploadComplete" ClientIDMode="AutoID" />
                                                                                </td>
                                                                                <td style="width: 30px">
                                                                                    <asp:ImageButton ID="EditManualInterviewTest_helpImageButton" runat="server" SkinID="sknHelpImageButton"
                                                                                        ToolTip="Add Image" OnClientClick="javascript:return false;" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="EditManualInterviewTest_questionImageButton" runat="server" Text="Add"
                                                                                        SkinID="sknButtonId" OnClick="EditManualInterviewTest_questionImageButtonClick"
                                                                                        ToolTip="Click to add image" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="EditManualInterviewTest_addQuestionAnswerLabel" runat="server" Text="Answer"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class='mandatory'>*</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="EditManualInterviewTest_addQuestionAnswerTextBox" runat="server"
                                                                            MaxLength="500" Height="52px" Width="498px" TextMode="MultiLine"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="EditManualInterviewTest_isQuestionRepository" runat="server"
                                                                            Text="Save question to the repository<br>(Please leave this box unchecked if this question is meant only for this interview and you do not wish to add it to the permanent question database)"
                                                                            Checked="false" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="EditManualInterviewTest_addQuestioncomplexityLabel" runat="server"
                                                                            Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                    </td>
                                                                    <td>
                                                                        <div>
                                                                            <asp:DropDownList ID="EditManualInterviewTest_addQuestioncomplexityDropDownList"
                                                                                runat="server" Width="133px">
                                                                            </asp:DropDownList>
                                                                            <asp:ImageButton ID="EditManualInterviewTest_addQuestioncomplexityImageButton" SkinID="sknHelpImageButton"
                                                                                runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the complexity of the question here" />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="EditManualInterviewTest_addQuestionLabel" runat="server" Text="Interview Area"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                    </td>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td class="checkboxaddquestion_list_bg">
                                                                                    <asp:RadioButtonList ID="EditManualInterviewTest_addQuestionRadioButtonList" runat="server"
                                                                                        RepeatColumns="2" RepeatDirection="Horizontal" CellSpacing="5" TextAlign="Right"
                                                                                        Width="100%" TabIndex="5">
                                                                                    </asp:RadioButtonList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="EditManualInterviewTest_addQuestion_byQuestion_categoryLabel" runat="server"
                                                                            Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                    </td>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="EditManualInterviewAddQuestion_byQuestion_categoryTextBox" ReadOnly="true"
                                                                                        runat="server" Text='<%# Eval("Category") %>'></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="EditManualInterviewTest_addQuestion_byQuestion_subjectLabel" runat="server"
                                                                                        Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    <asp:HiddenField ID="EditManualInterviewAddQuestion_byQuestion_categoryHiddenField"
                                                                                        runat="server" Value='<%# Eval("Cat_sub_id") %>' />
                                                                                    <asp:HiddenField ID="EditManualInterviewAddQuestion_byQuestion_categoryNameHiddenField"
                                                                                        runat="server" Value='<%# Eval("Category") %>' />
                                                                                    <asp:HiddenField ID="EditManualInterviewAddQuestion_byQuestion_subjectNameHiddenField"
                                                                                        runat="server" Value='<%# Eval("Subject") %>' />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="EditManualInterviewAddQuestion_byQuestion_subjectTextBox" ReadOnly="true"
                                                                                        runat="server" Text='<%# Eval("Subject") %>'></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:ImageButton ID="EditManualInterviewAddQuestion_byQuestion_categoryImageButton"
                                                                                        SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" ToolTip="Click here to select category and subject" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="EditManualInterviewTest_tagsLabel" runat="server" Text="Tags"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="EditManualInterviewTest_tagsTextBox" runat="server" MaxLength="100"
                                                                            Width="500px"></asp:TextBox>
                                                                        <asp:ImageButton ID="EditManualInterviewTest_tagsImageButton" runat="server" SkinID="sknHelpImageButton"
                                                                            ToolTip="Please enter tags separated by commas" OnClientClick="javascript:return false;" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="5" class="tab_body_bg">
                                                                <tr id="EditManualInterviewTest_DisplayQuestionImageTR" runat="server" style="display: none">
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Image runat="server" ID="EditManualInterviewTest_questionImage" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="EditManualInterviewTest_deleteLinkButton" runat="server" Text="Delete"
                                                                                        SkinID="sknActionLinkButton" OnClick="EditManualInterviewTest_deleteLinkButtonClick"
                                                                                        ToolTip="Click to delete image" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_td_padding_5">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td align="left" style="width: 20px; padding-right: 5px">
                                                <asp:Button ID="EditManualInterviewTest_addQuestioSaveButton" runat="server" SkinID="sknButtonId"
                                                    Text="Save" OnClick="EditManualInterviewTest_addQuestioSaveButton_Clicked" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="EditManualInterviewTest_addQuestioCloseLinkButton" SkinID="sknPopupLinkButton"
                                                    runat="server" Text="Cancel" 
                                                    onclick="EditManualInterviewTest_addQuestioCloseLinkButton_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <ajaxToolKit:ModalPopupExtender ID="EditManualInterviewTest_addQuestioModalPopupExtender"
                        runat="server" PopupControlID="EditManualInterviewTest_addQuestionPanel" TargetControlID="EditManualInterviewTest_addQuestionhiddenButton"
                        BackgroundCssClass="modalBackground">
                    </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <!-- End Popup Added -->
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="72%" class="header_text">
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td width="22%">
                                            <asp:Button ID="EditManualInterviewTest_bottomSaveButton" runat="server" SkinID="sknButtonId"
                                                Text="Save" OnClick="EditManualInterviewTest_saveButton_Click" />
                                        </td>
                                        <td width="40%">
                                            <asp:Button ID="EditManualInterviewTest_bottomCreateSessionButton" runat="server"
                                                SkinID="sknButtonId" Text="Create Interview Session" />
                                        </td>
                                        <td width="16%" align="right">
                                            <asp:LinkButton ID="EditManualInterviewTest_bottompResetLinkButton" runat="server"
                                                SkinID="sknActionLinkButton" Text="Reset" OnClick="EditManualInterviewTest_resetLinkButton_Click" />
                                        </td>
                                        <td width="4%" align="center">
                                            |
                                            </td>
                                            <td width="18%" align="left">
                                                <asp:LinkButton ID="EditManualInterviewTest_bottomCancelLinkButton" runat="server"
                                                    SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect" />
                                            </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="EditManualInterviewTest_questionDetailPanel" runat="server" CssClass="popupcontrol_question_draft"
                                                Style="display: none">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="td_height_20">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                        <asp:Literal ID="EditManualInterviewTest_questionDetail_questionResultLiteral" runat="server"
                                                                            Text="Question Detail"></asp:Literal>
                                                                    </td>
                                                                    <td style="width: 50%" valign="top">
                                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:ImageButton ID="EditManualInterviewTest_questionDetailPreviewControl_topCancelImageButton"
                                                                                        runat="server" SkinID="sknCloseImageButton" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_20">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:DataGrid ID="EditManualInterviewTest_questionDetailCategoryDataGrid" runat="server"
                                                                                        AutoGenerateColumns="false">
                                                                                        <Columns>
                                                                                            <asp:BoundColumn HeaderText="Category" DataField="Category"></asp:BoundColumn>
                                                                                            <asp:BoundColumn HeaderText="Subject" DataField="Subject"></asp:BoundColumn>
                                                                                        </Columns>
                                                                                    </asp:DataGrid>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="popup_panel_inner_bg">
                                                                                    <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="EditManualInterviewTest_questionDetailTestAreaHeadLabel" runat="server"
                                                                                                    Text="Interview Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="EditManualInterviewTest_questionDetailTestAreaLabel" runat="server"
                                                                                                    Text="Concept" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="EditManualInterviewTest_questionDetailNoOfTestLabel" runat="server"
                                                                                                    Text="Number Of Administered Interview " SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="EditManualInterviewTest_questionDetailTestAdministeredLabel" runat="server"
                                                                                                    Text="5" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="EditManualInterviewTest_questionDetailAvgTimeLabel" runat="server"
                                                                                                    Text="Average Time Taken(in minutes)" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="EditManualInterviewTest_questionDetailAvgTimeValueLabel" runat="server"
                                                                                                    Text="15.5" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="EditManualInterviewTest_questionDetailCorrectToAttendedRatioHeadLabel"
                                                                                                    runat="server" Text=" Ratio of Correct Answer To Attended" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="EditManualInterviewTest_questionDetailCorrectToAttendedRatioLabel"
                                                                                                    runat="server" Text="36:50" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="EditManualInterviewTest_questionDetailComplexityHeadLabel" runat="server"
                                                                                                    Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="EditManualInterviewTest_questionDetailPreviewControlComplexityLabel"
                                                                                                    runat="server" Text="Normal" SkinID="sknLabelFieldText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_5">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="popup_question_icon">
                                                                                    <asp:Label ID="EditManualInterviewTest_questionDetailQuestionLabel" runat="server"
                                                                                        SkinID="sknLabelText"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_padding_left_20">
                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <asp:RadioButtonList ID="EditManualInterviewTest_questionDetailAnswerRadioButtonList"
                                                                                            runat="server" RepeatColumns="2" RepeatDirection="Horizontal" CellSpacing="5"
                                                                                            TextAlign="Right" Width="100%">
                                                                                        </asp:RadioButtonList>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_8">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="padding-left: 30px">
                                                                        <table border="0" cellpadding="0" cellspacing="0" align="left">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Button ID="EditManualInterviewTest_questionDetail_TopAddButton" runat="server"
                                                                                        Text="Add" SkinID="sknButtonId" OnClick="EditManualInterviewTest_questionDetail_TopAddButton_Click" />
                                                                                </td>
                                                                                <td style="padding-left: 10px">
                                                                                    <asp:LinkButton ID="EditManualInterviewTest_questionDetail_TopCancelButton" runat="server"
                                                                                        SkinID="sknCancelLinkButton" Text="Cancel" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_8">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <div style="display: none">
                                                <asp:Button ID="EditManualInterviewTest_adaptiveQuestionPreviewPanelTargeButton"
                                                    runat="server" />
                                            </div>
                                            <ajaxToolKit:ModalPopupExtender ID="EditManualInterviewTest_adaptiveQuestionPreviewModalPopupExtender"
                                                runat="server" PopupControlID="EditManualInterviewTest_questionDetailPanel" TargetControlID="EditManualInterviewTest_adaptiveQuestionPreviewPanelTargeButton"
                                                BackgroundCssClass="modalBackground" CancelControlID="EditManualInterviewTest_questionDetail_TopCancelButton">
                                            </ajaxToolKit:ModalPopupExtender>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:UpdatePanel ID="EditManualInterviewTest_questionDetailPreviewUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="EditManualInterviewTest_questionPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_question_detail">
                <asp:HiddenField ID="EditManualInterviewTest_previewQuestionAddHiddenField" runat="server" />
                <%--<uc3:QuestionDetailPreviewControl ID="EditManualInterviewTest_questionDetailPreviewControl"
                    runat="server" OnCancelClick="EditManualInterviewTest_cancelClick" />--%>
                <uc7:QuestionDetailSummaryControl ID="EditManualInterviewTest_questionDetailSummaryControl"
                    runat="server" Title="Question Details Summary" OnCancelClick="EditManualInterviewTest_cancelClick"
                    OnAddClick="EditManualInterviewTest_questionDetailPreviewControl_AddClick" />
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="EditManualInterviewTest_questionModalTargeButton" runat="server" />
            </div>
            <ajaxToolKit:ModalPopupExtender ID="EditManualInterviewTest_questionModalPopupExtender"
                runat="server" PopupControlID="EditManualInterviewTest_questionPanel" TargetControlID="EditManualInterviewTest_questionModalTargeButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="EditManualInterviewTest_ConfirmPopupUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="EditManualInterviewTest_sessionInclucedKeyHiddenField" runat="server" />
            <asp:HiddenField ID="EditManualInterviewTest_questionKeyHiddenField" runat="server" />
            <asp:HiddenField ID="EditManualInterviewTest_deletedQuestionHiddenField" runat="server" />
            <asp:HiddenField ID="EditManualInterviewTest_insertedQuestionHiddenField" runat="server" />
            <asp:HiddenField ID="EditManualInterviewTest_authorIdHiddenField" runat="server" />
            <asp:HiddenField ID="EditManualInterviewTest_positionProfileHiddenField" runat="server" />
            <asp:HiddenField ID="hiddenValue" runat="server" />
            <asp:HiddenField ID="hiddenDeleteValue" runat="server" />
            <div id="EditManualInterviewTest_hiddenDIV" runat="server" style="display: none">
                <asp:Button ID="EditManualInterviewTest_hiddenPopupModalButton" runat="server" />
            </div>
            <asp:Panel ID="EditManualInterviewTest_ConfirmPopupPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_confirm_remove">
                <uc6:ConfirmMsgControl ID="EditManualInterviewTest_ConfirmPopupExtenderControl" runat="server"
                    OnOkClick="EditManualInterviewTest_okClick" OnCancelClick="EditManualInterviewTest_cancelClick" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="EditManualInterviewTest_ConfirmPopupExtender"
                runat="server" PopupControlID="EditManualInterviewTest_ConfirmPopupPanel" TargetControlID="EditManualInterviewTest_hiddenPopupModalButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
            <!-- Popup Save Button -->
            <div id="SearchQuestion_hiddenDIV" runat="server" style="display: none">
                <asp:Button ID="EditInterviewTest_hiddenPopupModalButton1" runat="server" />
            </div>
            <ajaxToolKit:ModalPopupExtender ID="EditInterviewTest_saveTestModalPopupExtender"
                runat="server" PopupControlID="EditInterviewTest_saveTestConfrimPopUpPanel" TargetControlID="EditInterviewTest_hiddenPopupModalButton1"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
            <asp:Panel ID="EditInterviewTest_saveTestConfrimPopUpPanel" runat="server" Style="display: none"
                Height="210px" CssClass="popupcontrol_confirm">
                <uc6:ConfirmMsgControl ID="EditInterviewTestSave_confirmPopupExtenderControl" OnOkClick="EditManualInterviewTest_okPopUpClick"
                    OnCancelClick="EditManualInterviewTest_cancelPopUpClick" runat="server" />
            </asp:Panel>
            <!-- End Popup Save Buttion -->
        </ContentTemplate>
    </asp:UpdatePanel>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load
    (
        function () {
            window.setTimeout(focus, 1);
        }
    )
        function focus() {
            try {
                document.getElementById('<%=EditManualInterviewTest_categoryTextBox.ClientID %>').focus();
            }
            catch (Err) {
                try {
                    document.getElementById('<%=EditManualInterviewTest_keywordTextBox.ClientID %>').focus();
                }
                catch (Err1) {
                    document.getElementById('ctl00_OTMMaster_body_EditManualInterviewTest_mainTabContainer_EditManualInterviewTest_testdetailsTabPanel_EditManualInterviewTest_testDetailsUserControl_TestDetailsControl_testNameTextBox').focus();
                }
            }
        }
    </script>
</asp:Content>
