<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    CodeBehind="EditInterviewTestWithAdaptive.aspx.cs" Inherits="Forte.HCM.UI.InterviewTestMaker.EditInterviewTestWithAdaptive" %>

<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/QuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc3" %>
<%@ Register Src="../CommonControls/InterviewTestDetailsControl.ascx" TagName="TestDetailsControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/ManualInterviewTestSummaryControl.ascx" TagName="ManualTestSummaryControl"
    TagPrefix="uc5" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc6" %>
<%@ Register Src="../CommonControls/InterviewQuestionDetailSummaryControl.ascx" TagName="QuestionDetailSummaryControl"
    TagPrefix="uc7" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ContentPlaceHolderID="InterviewMaster_body" ID="EditInterviewTestWithAdaptive_content" runat="server">

    <script src="../JS/ChartScript.js" type="text/javascript"></script>
     <script type="text/javascript" language="javascript">
         var control;
         function setFocus(sender, e) {
             try {
                 var activeTab = $find('<%=EditInterviewTestWithAdaptive_mainTabContainer.ClientID%>').get_activeTabIndex();
                 if (activeTab == "0") {
                     try {
                         control = $get('<%=EditInterviewTestWithAdaptive_categoryTextBox.ClientID%>');
                         control.focus();
                     }
                     catch (er1) {
                         try {
                             control = $get('<%=EditInterviewTestWithAdaptive_keywordTextBox.ClientID%>');
                             control.focus();
                         }
                         catch (er2) {
                         }
                     }
                 }
                 else {
                     control = $get('ctl00_InterviewMaster_body_EditInterviewTestWithAdaptive_mainTabContainer_EditInterviewTestWithAdaptive_testdetailsTabPanel_EditInterviewTestWithAdaptive_testDetailsUserControl_TestDetailsControl_testNameTextBox');
                     control.focus();
                 }
             }
             catch (er) {
             }
         }

         function ShowAddImagePanel() {
             var addImageTR = document.getElementById('<%=EditInterviewTestWithAdaptive_addQuestionImageTR.ClientID%>');
             addImageTR.style.display = "";
             var addImageBtn = document.getElementById('<%=EditInterviewTestWithAdaptive_addImageLinkButton.ClientID%>');
             addImageBtn.style.display = "none";
             return false;
         }

    </script>
    <script type="text/javascript" language="javascript">

        // Display  Or Hide the adaptive TestDraft Or Manual test Draft Questions.
        function ShowOrHideDiv(caseValue) {
            switch (caseValue.toString()) {
                case '0':
                    {
                        document.getElementById("<%= EditInterviewTestWithAdaptive_resultsHiddenField.ClientID %>").value = "0";
                        document.getElementById("EditInterviewTestWithAdaptive_manualTd").style.display = "block";
                        document.getElementById("EditInterviewTestWithAdaptive_adaptiveDiv").style.display = "none";
                        document.getElementById("EditInterviewTestWithAdaptive_manualTd").style.width = "100%";
                        document.getElementById("EditInterviewTestWithAdaptive_adaptiveTD").style.display = "none";
                        document.getElementById("<%= EditInterviewTestWithAdaptive_questionDiv.ClientID %>").style.width = "936px";
                        document.getElementById("<%= EditInterviewTestWithAdaptive_adaptiveQuestionDiv.ClientID %>").style.width = "0";
                        break;
                    }
                case '1':
                    {
                        document.getElementById("<%= EditInterviewTestWithAdaptive_resultsHiddenField.ClientID %>").value = "1";
                        document.getElementById("EditInterviewTestWithAdaptive_manualTd").style.display = "none";
                        document.getElementById("EditInterviewTestWithAdaptive_adaptiveDiv").style.display = "block";
                        document.getElementById("EditInterviewTestWithAdaptive_manualTd").style.width = "0";
                        document.getElementById("EditInterviewTestWithAdaptive_adaptiveTD").style.display = "block";
                        document.getElementById("EditInterviewTestWithAdaptive_adaptiveDiv").style.width = "100%";

                        document.getElementById("<%= EditInterviewTestWithAdaptive_questionDiv.ClientID %>").style.width = "0";
                        document.getElementById("<%= EditInterviewTestWithAdaptive_adaptiveQuestionDiv.ClientID %>").style.width = "936px";
                        break;
                    }
                case '2':
                    {
                        document.getElementById("<%= EditInterviewTestWithAdaptive_resultsHiddenField.ClientID %>").value = "2";
                        document.getElementById("EditInterviewTestWithAdaptive_manualTd").style.display = "block";
                        document.getElementById("EditInterviewTestWithAdaptive_adaptiveDiv").style.display = "block";
                        document.getElementById("EditInterviewTestWithAdaptive_manualTd").style.width = "49%";
                        document.getElementById("EditInterviewTestWithAdaptive_adaptiveTD").style.display = "block";
                        document.getElementById("EditInterviewTestWithAdaptive_adaptiveDiv").style.width = "50%";

                        document.getElementById("<%= EditInterviewTestWithAdaptive_questionDiv.ClientID %>").style.width = "450px";
                        document.getElementById("<%= EditInterviewTestWithAdaptive_adaptiveQuestionDiv.ClientID %>").style.width = "456px";
                        break;
                    }
                default:
                    {
                        document.getElementById("<%= EditInterviewTestWithAdaptive_resultsHiddenField.ClientID %>").value = "0";
                        document.getElementById("EditInterviewTestWithAdaptive_manualTd").style.display = "block";
                        document.getElementById("EditInterviewTestWithAdaptive_adaptiveDiv").style.display = "block";
                        document.getElementById("EditInterviewTestWithAdaptive_adaptiveTD").style.display = "block";
                        document.getElementById("EditInterviewTestWithAdaptive_manualTd").style.width = "49%";
                        document.getElementById("EditInterviewTestWithAdaptive_adaptiveDiv").style.width = "50%";

                        document.getElementById("<%= EditInterviewTestWithAdaptive_questionDiv.ClientID %>").style.width = "450px";
                        document.getElementById("<%= EditInterviewTestWithAdaptive_adaptiveQuestionDiv.ClientID %>").style.width = "456px";
                        break;
                    }
            }
            return false;
        }


        var modifiedRow = null;
        var dataCollection = new Array();
        dataCollection = null;
        var sourceRow = null;
        var sourcePosition = null;
        var sourceTable = null;
        var currenttable = null;
        var dropTargets = null;
        var dragObject = null;
        var mouseOffSet = null;
        var destTable = null;
        var oldY = 0;
        this.table = null;

        //move the Single Question add to Testdraft.
        function singleqnsclick(QuestionID, buttonID) {
            //alert(buttonID);
            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
            hiddenvalue.value = QuestionID;
            __doPostBack(buttonID, "OnClick");
        }

        //related to the Drag and Drop questions 
        function mousedown(QuestionID, source) {
            sourceTable = document.getElementById(source);

            if (checkChecked(QuestionID)) {
                CheckNoOfChecked(QuestionID);
            }
            else {
                CheckCorrectQuestion(QuestionID);
            }
        }

        //related to the Drag and Drop questions 
        function CheckCorrectQuestion(id) {
            var table = sourceTable;
            var testVar = document.getElementById('<%=EditInterviewTestWithAdaptive_searchQuestionGridView.ClientID %>');
            var testVarTwo = document.getElementById('<%=EditInterviewTestWithAdaptive_adaptiveGridView.ClientID %>');
            id++;
            var rowid = 1;
            var chkbox;
            var rowCount = table.rows.length;
            for (var i = 1; i < rowCount; i++) {
                if (id == i) {
                    var row = table.rows[i];
                    if (detectBrowser()) {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else {
                        chkbox = row.cells[0].childNodes[0];
                    }

                    if (null != chkbox) {
                        chkbox.checked = true;
                        // row.cells[0].childNodes[0].checked = true;

                    }

                }
                if ((testVar == sourceTable) || (testVarTwo == sourceTable)) {
                    CheckNoOfChecked(id);
                }
            }

        }

        //find the Browser details
        function detectBrowser() {
            var browser = navigator.appName;
            var b_version = navigator.appVersion;
            var version = parseFloat(b_version);
            if (browser == "Netscape") {
                return true;
            }
            else {
                return false; ;
            }
        }

        // Question remove from the Test Draft.
        function RemoveRows(rowIndex) {
            sourceTable = document.getElementById('<%=EditInterviewTestWithAdaptive_testDrftGridView.ClientID %>');

            if (confirm('Are you sure to delete these question(s)?')) {
                if (checkChecked(rowIndex)) {
                    DelRows();

                }
                else {
                    CheckCorrectQuestion(rowIndex);
                    DelRows();
                }
            }
            else
                return false;
        }

        function DelRows() {
            try {
                var table = document.getElementById('<%=EditInterviewTestWithAdaptive_testDrftGridView.ClientID %>');
                var rowCount = table.rows.length;
                for (var i = 0; i < rowCount; i++) {
                    var row = table.rows[i];
                    var chkbox;
                    if (detectBrowser()) {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else {
                        chkbox = row.cells[0].childNodes[0];
                    }
                    if (null != chkbox && true == chkbox.checked) {
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                    }
                }
            } catch (e) {
                alert(e);
            }
            return false;
        }

        function CheckNoOfChecked(QuestionID) {

            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');

            hiddenvalue.value = "";

            var numberOfQuestions = 0;

            var table = sourceTable;
            // document.getElementById('<%=EditInterviewTestWithAdaptive_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {

                    numberOfQuestions = numberOfQuestions + 1;

                    var noOfQuestionsTextBox = document.getElementById('<%=sampleDIVTextBox.ClientID %>');

                    noOfQuestionsTextBox.value = 'Move' + numberOfQuestions + ' questions';

                    dragObject = document.getElementById('<%=moveDisplayDIV.ClientID %>');

                    if (detectBrowser()) {
                        if (hiddenvalue.value == "") {
                            hiddenvalue.value = row.childNodes[5].childNodes[1].defaultValue + ',';
                        }
                        else {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[5].childNodes[1].defaultValue + ',';
                        }
                    }
                    else {
                        if (hiddenvalue.value == "") {
                            hiddenvalue.value = row.childNodes[4].childNodes[0].value + ',';
                        }
                        else {
                            hiddenvalue.value = hiddenvalue.value + row.childNodes[4].childNodes[0].value + ',';
                        }
                    }
                }
            }
            //var ShowDiv =
        }

        function checkChecked(id) {

            var isChecked = false;

            var table = sourceTable;
            // document.getElementById('<%=EditInterviewTestWithAdaptive_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {
                    isChecked = true;
                }

                if (isChecked) {
                    return true;
                }
                else {
                    return false;
                }
            }

        }

        function mouseMove(ev) {
            ev = ev || window.event;
            var mousePos = mouseCoords(ev);

            if (dragObject) {
                //mouseOffset = getMouseOffset(dragObject, ev);
                dragObject.style.display = 'block';
                dragObject.style.backgroundColor = "#eee";
                dragObject.style.position = 'absolute';
                if (detectBrowser()) {
                    dragObject.style.top = mousePos.y + 'px';
                    dragObject.style.left = mousePos.x + 'px';
                }
                else {
                    dragObject.style.top = mousePos.y + document.documentElement.scrollTop + 10;
                    dragObject.style.left = mousePos.x + 10;
                }
            }
            else {

            }
            return false;
        }
        function mouseCoords(ev) {
            if (ev != null) {
                if (ev.pageX || ev.pageY) { return { x: ev.pageX, y: ev.pageY }; }
                return {
                    x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
                    y: ev.clientY + document.body.scrollTop - document.body.clientTop
                };
            }
        }

        function srsmouseup(ev) {
            if (dragObject == null)
                return false;
            var ev = ev || window.event;
            var mousePos = mouseCoords(ev);
            var curTarget = document.getElementById('<%=EditInterviewTestWithAdaptive_testDrftGridView.ClientID %>');
            var targPos = getPosition(curTarget);

            var scrolltargPos;
            if (detectBrowser()) {
                scrolltargPos = targPos.y;
            }
            else {
                scrolltargPos = targPos.y - document.documentElement.scrollTop;
            }

            var targWidth = parseInt(curTarget.offsetWidth);
            var targHeight = parseInt(curTarget.offsetHeight);
            if (
            (mousePos.x >= targPos.x) &&
            (mousePos.y >= scrolltargPos)) {

                __doPostBack('ctl00_HCMMaster_body_EditInterviewTestWithAdaptive_mainTabContainer_EditInterviewTestWithAdaptive_questionsTabPanel_EditInterviewTestWithAdaptive_selectMultipleImage', "OnClick");
                dragObject.style.display = 'none';
            }
            else {
                if (dragObject != null) {
                    makeUnchekabale();
                    dragObject.style.display = 'none';

                    var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
                    hiddenvalue.value = "";
                }
            } dragObject = null;
        }

        function makeUnchekabale() {
            var table = sourceTable;
            //document.getElementById('<%=EditInterviewTestWithAdaptive_searchQuestionGridView.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {
                    chkbox.checked = false;
                }
            }
        }

        function getPosition(e) {
            var left = 0;
            var top = 0;

            if (e != null) {
                while (e.offsetParent) {
                    left += e.offsetLeft;
                    top += e.offsetTop;
                    e = e.offsetParent;
                }


                left += e.offsetLeft;
                top += e.offsetTop;
            }

            return { x: left, y: top };
        }
    
    </script>
    <asp:Panel ID="EditInterviewTestWithAdaptive_mainPanel" runat="server" DefaultButton="EditInterviewTestWithAdaptive_bottomSaveButton">

    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="EditInterviewTestWithAdaptive_headerLiteral" runat="server" Text="Edit Interview"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td width="22%">
                                        <asp:Button ID="EditInterviewTestWithAdaptive_topSaveButton" runat="server" SkinID="sknButtonId"
                                            Text="Save" OnClick="EditInterviewTestWithAdaptive_saveButton_Click" />
                                    </td>
                                    <td width="40%">
                                        <asp:Button ID="EditInterviewTestWithAdaptive_topCreateSessionButton" runat="server" SkinID="sknButtonId"
                                            Text="Create Interview Session" />
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="EditInterviewTestWithAdaptive_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Reset" OnClick="EditInterviewTestWithAdaptive_resetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="EditInterviewTestWithAdaptive_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="EditInterviewTestWithAdaptive_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="EditInterviewTestWithAdaptive_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="EditInterviewTestWithAdaptive_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <ajaxToolKit:TabContainer ID="EditInterviewTestWithAdaptive_mainTabContainer" runat="server" ActiveTabIndex="0" OnClientActiveTabChanged="setFocus">
                    <ajaxToolKit:TabPanel ID="EditInterviewTestWithAdaptive_questionsTabPanel" HeaderText="questions"
                        runat="server">
                        <HeaderTemplate>
                            Questions</HeaderTemplate>
                        <ContentTemplate>
                        <asp:Panel ID="EditInterviewTestWithAdaptive_testQuestionDetailsPanel" runat="server" DefaultButton="EditInterviewTestWithAdaptive_topSearchButton">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <div id="EditInterviewTestWithAdaptive_searchCriteriasDiv" runat="server" style="display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="right" class="td_padding_bottom_5">
                                                        <asp:UpdatePanel ID="EditInterviewTestWithAdaptive_simpleLinkButtonUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <asp:HiddenField ID="EditInterviewTestWithAdaptive_adaptiveRecommendClickedHiddenField" runat="server" />
                                                                <asp:LinkButton ID="EditInterviewTestWithAdaptive_simpleLinkButton" runat="server" Text="Advanced"
                                                                    SkinID="sknActionLinkButton" OnClick="EditInterviewTestWithAdaptive_simpleLinkButton_Click" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="panel_bg">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td width="100%">
                                                                    <asp:UpdatePanel ID="EditInterviewTestWithAdaptive_searchDiv_UpdatePanel" runat="server">
                                                                        <ContentTemplate>
                                                                            <div id="EditInterviewTestWithAdaptive_simpleSearchDiv" runat="server">
                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_categoryHeadLabel" runat="server" Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="EditInterviewTestWithAdaptive_categoryTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_subjectHeadLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="EditInterviewTestWithAdaptive_subjectTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_keywordsHeadLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="EditInterviewTestWithAdaptive_keywordsTextBox" runat="server" MaxLength="100"></asp:TextBox></div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="EditInterviewTestWithAdaptive_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                            <div id="EditInterviewTestWithAdaptive_advanceSearchDiv" runat="server">
                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <uc1:CategorySubjectControl ID="EditInterviewTestWithAdaptive_categorySubjectControl" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_height_8">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="panel_inner_body_bg">
                                                                                            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td style="width: 10%">
                                                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_testAreaHeadLabel" runat="server" Text="Interview Area"
                                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td colspan="5" class="checkbox_list_bg" align="left" style="width: 90%">
                                                                                                        <asp:CheckBoxList ID="EditInterviewTestWithAdaptive_testAreaCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                            RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                        </asp:CheckBoxList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 10%">
                                                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_complexityLabel" runat="server" Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td colspan="3" align="left" valign="middle">
                                                                                                        <div style="float: left; padding-right: 5px; width: 90%; vertical-align: middle;"
                                                                                                            class="checkbox_list_bg">
                                                                                                            <asp:CheckBoxList ID="EditInterviewTestWithAdaptive_complexityCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                                RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                            </asp:CheckBoxList>
                                                                                                        </div>
                                                                                                        <div style="float: left; vertical-align: middle; padding-left: 5px; padding-top: 5px;">
                                                                                                            <asp:ImageButton ID="EditInterviewTestWithAdaptive_complexityImageButton" SkinID="sknHelpImageButton"
                                                                                                                runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, ComplexityHelp %>" />
                                                                                                        </div>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_keywordHeadLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                                            <asp:TextBox ID="EditInterviewTestWithAdaptive_keywordTextBox" runat="server" Columns="50" MaxLength="100"></asp:TextBox></div>
                                                                                                        <div style="float: left;">
                                                                                                            <asp:ImageButton ID="EditInterviewTestWithAdaptive_keywordImageButton" SkinID="sknHelpImageButton"
                                                                                                                runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" />
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_questionLabel" runat="server" Text="Question ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="EditInterviewTestWithAdaptive_questionIDTextBox" runat="server" MaxLength="15"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_creditHeadLabel" runat="server" Text="Credit" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="EditInterviewTestWithAdaptive_creditTextBox" runat="server"></asp:TextBox>
                                                                                                        <ajaxToolKit:FilteredTextBoxExtender ID="EditInterviewTestWithAdaptive_weightageFileteredExtender"
                                                                                                            runat="server" TargetControlID="EditInterviewTestWithAdaptive_creditTextBox" FilterType="Numbers"
                                                                                                            Enabled="True">
                                                                                                        </ajaxToolKit:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_authorHeadLabel" runat="server" Text="Author" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                                            <asp:TextBox ID="EditInterviewTestWithAdaptive_authorTextBox" runat="server"></asp:TextBox>
                                                                                                            <asp:HiddenField ID="EditInterviewTestWithAdaptive_dummyAuthorID" runat="server" />
                                                                                                        </div>
                                                                                                        <div style="float: left;">
                                                                                                            <asp:ImageButton ID="EditInterviewTestWithAdaptive_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                                                runat="server" ImageAlign="Middle" /></div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_positionProfileLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                            Text="Position Profile"></asp:Label>
                                                                                                    </td>
                                                                                                   <td colspan="3">
                                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                                            <asp:TextBox ID="EditInterviewTestWithAdaptive_positionProfileTextBox" runat="server" MaxLength="200" Columns="55"></asp:TextBox>
                                                                                                        </div>
                                                                                                        <div style="float: left;">
                                                                                                            <asp:ImageButton ID="EditInterviewTestWithAdaptive_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select position profile" /></div>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_positionProfileKeywordLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                            Text="Position Profile Keyword"></asp:Label>
                                                                                                    </td>
                                                                                                    <td >
                                                                                                        <div style="float: left; padding-right: 5px; width: 100%">
                                                                                                           <asp:TextBox ID="EditInterviewTestWithAdaptive_positionProfileKeywordTextBox" runat="server" MaxLength="500"></asp:TextBox>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" class="td_padding_top_5">
                                                                    <asp:Button ID="EditInterviewTestWithAdaptive_topSearchButton" runat="server" Text="Search" SkinID="sknButtonId"
                                                                        OnClick="EditInterviewTestWithAdaptive_searchQuestionButton_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr id="EditInterviewTestWithAdaptive_searchResultsTR" runat="server">
                                    <td class="header_bg" runat="server">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 80%" align="left" class="header_text_bold">
                                                    <asp:Literal ID="EditInterviewTestWithAdaptive_searchResultsLiteral" runat="server" Text="Questions"></asp:Literal>&nbsp;<asp:Label
                                                        ID="EditInterviewTestWithAdaptive_sortHelpLabel" runat="server" SkinID="sknLabelText"></asp:Label>
                                                </td>
                                                <td style="width: 5%">
                                                    <asp:ImageButton ID="EditInterviewTestWithAdaptive_manualImageButton" runat="server" SkinID="sknManualImageButton"
                                                        ToolTip="Click here to view 'Search Results' only" OnClientClick="javascript:return ShowOrHideDiv('0');" />
                                                    <asp:HiddenField ID="EditInterviewTestWithAdaptive_resultsHiddenField" runat="server" />
                                                </td>
                                                <td style="width: 5%">
                                                    <asp:ImageButton ID="EditInterviewTestWithAdaptive_adaptiveImageButton" runat="server" SkinID="sknAdaptiveImageButton"
                                                        ToolTip="Click here to view 'Adaptive Recommended Questions' only" OnClientClick="javascript:return ShowOrHideDiv('1');" />
                                                </td>
                                                <td style="width: 5%">
                                                    <asp:ImageButton ID="EditInterviewTestWithAdaptive_bothImageButton" runat="server" SkinID="sknCombinedAMImageButton"
                                                        ToolTip="Click here to view both 'Search Results' and 'Adaptive Recommended Questions'"
                                                        OnClientClick="javascript:return ShowOrHideDiv('2');" />
                                                </td>
                                                <td style="width: 5%" align="right">
                                                    <span id="EditInterviewTestWithAdaptive_searchResultsUpSpan" runat="server" style="display: none;">
                                                        <asp:Image ID="EditInterviewTestWithAdaptive_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                    </span><span id="EditInterviewTestWithAdaptive_searchResultsDownSpan" runat="server" style="display: block;">
                                                        <asp:Image ID="EditInterviewTestWithAdaptive_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                    </span>
                                                    <asp:HiddenField ID="EditInterviewTestWithAdaptive_restoreHiddenField" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" id="EditInterviewTestWithAdaptive_adaptiveTD">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="EditInterviewTestWithAdaptive_adaptiveQuestionsHeadLabel" runat="server" Text="Adaptive Recommendations"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td align="right">
                                                    <asp:UpdatePanel ID="EditInterviewTestWithAdaptive_adaptiveYesNoUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RadioButtonList ID="EditInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList" runat="server"
                                                                            RepeatColumns="2" OnSelectedIndexChanged="EditInterviewTestWithAdaptive_adaptiveYesNoRadioButtonList_SelectedIndexChanged"
                                                                            AutoPostBack="true">
                                                                            <asp:ListItem Text="On" Value="0"></asp:ListItem>
                                                                            <asp:ListItem Text="Off" Value="1" Selected="True"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="EditInterviewTestWithAdaptive_recommendAdaptiveQuestions" runat="server" Text="Recommend Questions"
                                                                            SkinID="sknButtonId" OnClick="EditInterviewTestWithAdaptive_recommendAdaptiveQuestions_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                    <td valign="top">
                                        <div id="EditInterviewTestWithAdaptive_manualTd" style="width: 49%; float: left; display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="grid_header_bg">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="header_text_bold">
                                                                    <asp:Literal ID="EditInterviewTestWithAdaptive_manualQuestLiteral" runat="server" Text="Search Results"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" class="grid_body_bg">
                                                        <asp:UpdatePanel ID="EditInterviewTestWithAdaptive_searchQuestionGridView_UpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="height: 225px; overflow: auto;" runat="server" id="EditInterviewTestWithAdaptive_questionDiv">
                                                                                <asp:GridView ID="EditInterviewTestWithAdaptive_searchQuestionGridView" runat="server" AutoGenerateColumns="False"
                                                                                    AllowSorting="false" GridLines="None" Width="100%" OnRowDataBound="EditInterviewTestWithAdaptive_searchQuestionGridView_RowDataBound"
                                                                                    OnRowCommand="EditInterviewTestWithAdaptive_searchQuestionGridView_RowCommand">
                                                                                    <Columns>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="EditInterviewTestWithAdaptive_searchQuestionCheckbox" runat="server" />
                                                                                                <asp:HiddenField ID="EditInterviewTestWithAdaptive_searchQuestionKeyHiddenField" runat="server"
                                                                                                    Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditInterviewTestWithAdaptive_previewImageButton" runat="server" SkinID="sknPreviewImageButton"
                                                                                                    CommandName="view" CommandArgument='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>'
                                                                                                    ToolTip="Question Details Summary " />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditInterviewTestWithAdaptive_searchQuestionAlertImageButton" runat="server"
                                                                                                    SkinID="sknAlertImageButton" ToolTip="Flag Question" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditInterviewTestWithAdaptive_searchQuestionSelectImage" runat="server" SkinID="sknMoveDown_ArrowImageButton"
                                                                                                    CommandName="Select" ToolTip="Select Question" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="EditInterviewTestWithAdaptive_hidddenValue" runat="server" 
                                                                                                Value='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="QuestionID" Visible="False"></asp:BoundField>
                                                                                        <asp:BoundField DataField="QuestionRelationID" Visible="False"></asp:BoundField>
                                                                                        <asp:TemplateField HeaderText="Question">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditInterviewTestWithAdaptive_questionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),65) %>'
                                                                                                    Width="360px"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Category">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditInterviewTestWithAdaptive_categoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                                    Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Subject">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditInterviewTestWithAdaptive_subjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                                    Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                       <%-- <asp:TemplateField HeaderText="Answer">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditInterviewTestWithAdaptive_answerLabel" runat="server" Text='<%# TrimContent(Eval("AnswerID").ToString(),20) %>'
                                                                                                    Width="120px" ToolTip='<%# Eval("AnswerID") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>--%>
                                                                                        <asp:TemplateField HeaderText="Complexity">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditInterviewTestWithAdaptive_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                                    Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <uc2:PageNavigator ID="EditInterviewTestWithAdaptive_bottomPagingNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="EditInterviewTestWithAdaptive_topSearchButton" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="moveDisplayDIV" style="display: none; background-color: Lime" runat="server">
                                            <asp:TextBox ID="sampleDIVTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                                        </div>
                                        <div id="EditInterviewTestWithAdaptive_adaptiveDiv" style="width: 50%; float: right; display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="grid_header_bg">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="header_text_bold">
                                                                    <asp:Literal ID="EditInterviewTestWithAdaptive_adaptiveQuestLiteral" runat="server" Text="Adaptive Recommended Questions"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                    <td class="grid_body_bg">
                                                        <asp:UpdatePanel ID="EditInterviewTestWithAdaptive_adaptiveUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="height: 225px; width: 0px; overflow: auto;" runat="server" id="EditInterviewTestWithAdaptive_adaptiveQuestionDiv">
                                                                                <asp:GridView ID="EditInterviewTestWithAdaptive_adaptiveGridView" runat="server" AutoGenerateColumns="False"
                                                                                    AllowSorting="false" GridLines="None" Width="100%" OnRowDataBound="EditInterviewTestWithAdaptive_adaptiveGridView_RowDataBound"
                                                                                    OnRowCommand="EditInterviewTestWithAdaptive_adaptiveGridView_RowCommand">
                                                                                    <EmptyDataTemplate>
                                                                                        <center>
                                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_adaptiveEmptyLabel" runat="server" Text="No question found for recommendation"
                                                                                                SkinID="sknErrorMessage"></asp:Label>
                                                                                        </center>
                                                                                    </EmptyDataTemplate>
                                                                                    <Columns>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="EditInterviewTestWithAdaptive_adaptiveCheckbox" runat="server" />
                                                                                                <asp:HiddenField ID="EditInterviewTestWithAdaptive_adaptiveQuestionKeyHiddenField" runat="server"
                                                                                                    Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditInterviewTestWithAdaptive_adaptivepreviewImageButton" runat="server" SkinID="sknPreviewImageButton"
                                                                                                    CommandName="view" CommandArgument='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>'
                                                                                                    ToolTip="Question Details Summary " />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditInterviewTestWithAdaptive_adaptiveAlertImageButton" runat="server" SkinID="sknAlertImageButton"
                                                                                                    ToolTip="Flag Question" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="EditInterviewTestWithAdaptive_adaptiveSelectImage" runat="server" SkinID="sknMoveDown_ArrowImageButton"
                                                                                                    CommandName="Select" ToolTip="Select Question" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="EditInterviewTestWithAdaptive_adaptiveQuestionhiddenValue" runat="server" Value='<%#Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="QuestionID" Visible="False"></asp:BoundField>
                                                                                        <asp:BoundField DataField="QuestionRelationID" Visible="False"></asp:BoundField>
                                                                                        <asp:TemplateField HeaderText="Question">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditInterviewTestWithAdaptive_adaptiveQuestionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),65) %>'
                                                                                                    Width="360px"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Category">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditInterviewTestWithAdaptive_adaptiveCategoryLabel" runat="server" Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                                    Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Subject">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditInterviewTestWithAdaptive_adaptiveSubjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                                    Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                       <%-- <asp:TemplateField HeaderText="Answer">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditInterviewTestWithAdaptive_adaptiveAnswerLabel" runat="server" Text='<%# TrimContent(Eval("AnswerID").ToString(),20) %>'
                                                                                                    Width="120px" ToolTip='<%# Eval("AnswerID") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>--%>
                                                                                        <asp:TemplateField HeaderText="Complexity">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="EditInterviewTestWithAdaptive_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                                    Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Wrap="False" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                            <asp:HiddenField ID="EditInterviewTestWithAdaptive_adaptiveGridViewHiddenField" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:HiddenField ID="EditInterviewTestWithAdaptive_adaptivePagingHiddenField" runat="server" Value="1" />
                                                                            <uc2:PageNavigator ID="EditInterviewTestWithAdaptive_adaptivebottomPagingNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                    <td align="center" class="grid_header_bg">
                                        <table width="10%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="EditInterviewTestWithAdaptive_selectMultipleImage" runat="server" SkinID="sknMultiDown_ArrowImageButton"
                                                        CommandName="Select" ToolTip="Move questions to interview draft" OnClick="EditInterviewTestWithAdaptive_selectMultipleImage_Click" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="EditInterviewTestWithAdaptive_removeMultipleImage" runat="server" SkinID="sknMultiUp_ArrowImageButton"
                                                        CommandName="Select" ToolTip="Remove questions from interview draft" OnClick="EditInterviewTestWithAdaptive_removeMultipleImage_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="EditInterviewTestWithAdaptive_testDrftGridView_UpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                        <td class="grid_header_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="header_text_bold">
                                                                        <asp:Literal ID="EditInterviewTestWithAdaptive_testDraftLiteral" runat="server" Text="Interview Draft"></asp:Literal>&nbsp;
                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_testDrafttHelpLabel" runat="server" SkinID="sknLabelText"
                                                                            Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>" Visible="false"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr onmouseup="javascript:srsmouseup(event);" onmousemove="javascript:mouseMove(event);"
                                                        style="height: 35px">
                                                        <td class="grid_body_bg" valign="top">
                                                            <asp:GridView ID="EditInterviewTestWithAdaptive_testDrftGridView" OnRowDataBound="EditInterviewTestWithAdaptive_testDrftGridView_RowDataBound"
                                                                runat="server" GridLines="None" Width="100%" OnRowCommand="EditInterviewTestWithAdaptive_testDrftGridView_RowCommand"
                                                                EmptyDataText="&nbsp;">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="EditInterviewTestWithAdaptive_testDrftCheckbox" runat="server" />
                                                                            <asp:HiddenField ID="EditInterviewTestWithAdaptive_testDraftQuestionKeyHiddenField" runat="server"
                                                                                Value='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId")%>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="EditInterviewTestWithAdaptive_testDrftRemoveImage" runat="server" SkinID="sknDeleteImageButton"
                                                                                CommandName="Select" ToolTip="Remove Question" CommandArgument='<%# Eval("QuestionKey")+":"+Eval("QuestionRelationId") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="EditInterviewTestWithAdaptive_byQuestion_moveDownQuestionImageButton"
                                                                                runat="server" SkinID="sknMoveDown_ArrowImageButton" CommandName="MoveDown" ToolTip="Move Question Down"
                                                                                CommandArgument='<%# Eval("DisplayOrder") %>' /></ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="EditInterviewTestWithAdaptive_byQuestion_moveUpQuestionImageButton"
                                                                                runat="server" SkinID="sknMoveUp_ArrowImageButton" CommandName="MoveUp" ToolTip="Move Question Up"
                                                                                CommandArgument='<%# Eval("DisplayOrder") %>' />
                                                                                <asp:HiddenField runat="server" ID="EditInterviewTestWithAdaptive_displayOrderHiddenField"
                                                                                    Value='<%# Eval("DisplayOrder") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField ReadOnly="True" HeaderText="Question&nbsp;ID" InsertVisible="False"
                                                                        DataField="QuestionID" Visible="False">
                                                                        <ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="Question">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_draftQuestionLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),70) %>'
                                                                                Width="370px"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle Wrap="true" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Category">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_categoryLabel" runat="server" 
                                                                                Text='<%# TrimContent(Eval("CategoryName").ToString(),27) %>'
                                                                                Width="135px" ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle Wrap="False" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Subject">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_subjectLabel" runat="server" Text='<%# TrimContent(Eval("SubjectName").ToString(),20) %>'
                                                                                Width="135px" ToolTip='<%# Eval("SubjectName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle Wrap="False" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Complexity">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_complexityLabel" runat="server" Text='<%# TrimContent(Eval("Complexity").ToString(),15) %>'
                                                                                Width="75px" ToolTip='<%# Eval("Complexity") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle Wrap="False" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                            <asp:HiddenField ID="EditInterviewTestWithAdaptive_previewQuestionAddHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="EditInterviewTestWithAdaptive_selectMultipleImage" />
                                                <asp:AsyncPostBackTrigger ControlID="EditInterviewTestWithAdaptive_removeMultipleImage" />
                                                <asp:AsyncPostBackTrigger ControlID="EditInterviewTestWithAdaptive_searchQuestionGridView" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="EditManualInterviewTest_addQuestionLinkButton" SkinID="sknAddLinkButton"
                                        runat="server" Text="Add new question" ToolTip="Add new question" 
                                        OnClick="EditInterviewTestWithAdaptive_addQuestionLinkButton_Click"></asp:LinkButton>    
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <uc5:ManualTestSummaryControl ID="EditInterviewTestWithAdaptive_testSummaryControl" 
                                                runat="server" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="EditInterviewTestWithAdaptive_testdetailsTabPanel" runat="server">
                        <HeaderTemplate>
                            Interview Details</HeaderTemplate>
                        <ContentTemplate>
                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td style="width: 100%">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <uc4:TestDetailsControl ID="EditInterviewTestWithAdaptive_testDetailsUserControl" runat="server" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                </ajaxToolKit:TabContainer>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="EditInterviewTestWithAdaptive_bottomMessageupdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="EditInterviewTestWithAdaptive_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="EditInterviewTestWithAdaptive_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <!-- Popup Added -->
            <tr>
                <td>
                    <asp:UpdatePanel ID="EditInterviewTestWithAdaptive_questionUpdatePanel" runat="server">
                    <ContentTemplate>
                    <asp:Panel ID="EditInterviewTestWithAdaptive_addQuestionPanel" runat="server" CssClass="popupcontrol_addQuestion">
                        <div style="display: none;">
                            <asp:Button ID="EditInterviewTestWithAdaptive_addQuestionhiddenButton" runat="server" Text="Hidden" /></div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="popup_td_padding_10">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                <asp:Literal ID="EditInterviewTestWithAdaptive_addQuestionLiteral" runat="server" Text="Add Question"></asp:Literal>
                                            </td>
                                            <td style="width: 50%" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="EditInterviewTestWithAdaptive_addQuestionTopCancelImageButton" runat="server"
                                                                SkinID="sknCloseImageButton" 
                                                                onclick="EditInterviewTestWithAdaptive_addQuestionTopCancelImageButton_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_td_padding_10">
                                    <table cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                        <tr>
                                            <td align="left" class="popup_td_padding_10">
                                                <table border="0" cellpadding="0" cellspacing="5" class="tab_body_bg" align="left">
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="5">
                                                                <tr>
                                                                    <td class="msg_align" colspan="4">
                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_addQuestionErrorLabel" runat="server"
                                                                            SkinID="sknErrorMessage"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_addQuestionQuestLabel" runat="server"
                                                                            Text="Question" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class='mandatory'>*</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="EditInterviewTestWithAdaptive_addQuestionQuestTextBox" runat="server"
                                                                            MaxLength="3000" Height="52px" Width="498px" TextMode="MultiLine" Wrap="true"></asp:TextBox>
                                                                        <asp:LinkButton ID="EditInterviewTestWithAdaptive_addImageLinkButton" runat="server"
                                                                            SkinID="sknActionLinkButton" Text="Add Image"  ToolTip="Click here to add image" OnClientClick="javascript: return ShowAddImagePanel();" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr id="EditInterviewTestWithAdaptive_addQuestionImageTR" runat="server" style="display: none">
                                                                    <td>
                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_selectImageLabel" runat="server" Text="Select Image"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td style="width: 250px">
                                                                                    <ajaxToolKit:AsyncFileUpload ID="EditInterviewTestWithAdaptive_questionImageUpload"
                                                                                        runat="server" OnUploadedComplete="EditInterviewTestWithAdaptive_questionImageOnUploadComplete"
                                                                                        ClientIDMode="AutoID" />
                                                                                </td>
                                                                                <td style="width:30px">
                                                                                    <asp:ImageButton ID="EditInterviewTestWithAdaptive_helpImageButton" runat="server"
                                                                                        SkinID="sknHelpImageButton" ToolTip="Add Image" OnClientClick="javascript:return false;" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="EditInterviewTestWithAdaptive_questionImageButton" runat="server"
                                                                                        Text="Add" SkinID="sknButtonId" OnClick="EditInterviewTestWithAdaptive_questionImageButtonClick"
                                                                                        ToolTip="Click to add image" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="EditInterviewTestWithAdaptive_isQuestionRepository" runat="server"
                                                                            Text="Save question to the repository<br>(Please leave this box unchecked if this question is meant only for this interview and you do not wish to add it to the permanent question database)"
                                                                            Checked="false" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_addQuestionAnswerLabel" runat="server"
                                                                            Text="Answer" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class='mandatory'>*</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="EditInterviewTestWithAdaptive_addQuestionAnswerTextBox" runat="server"
                                                                            MaxLength="500" Height="52px" Width="498px" TextMode="MultiLine"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_addQuestioncomplexityLabel" runat="server"
                                                                            Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                    </td>
                                                                    <td>
                                                                        <div>
                                                                            <asp:DropDownList ID="EditInterviewTestWithAdaptive_addQuestioncomplexityDropDownList"
                                                                                runat="server" Width="133px">
                                                                            </asp:DropDownList>
                                                                            <asp:ImageButton ID="EditInterviewTestWithAdaptive_addQuestioncomplexityImageButton"
                                                                                SkinID="sknHelpImageButton" runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;"
                                                                                ToolTip="Please select the complexity of the question here" />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_addQuestionLabel" runat="server" Text="Interview Area"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label><span class='mandatory'>&nbsp;*</span>
                                                                    </td>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td class="checkboxaddquestion_list_bg">
                                                                                    <asp:RadioButtonList ID="EditInterviewTestWithAdaptive_addQuestionRadioButtonList"
                                                                                        runat="server" RepeatColumns="2" RepeatDirection="Horizontal" CellSpacing="5"
                                                                                        TextAlign="Right" Width="100%" TabIndex="5">
                                                                                    </asp:RadioButtonList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_addQuestion_byQuestion_categoryLabel"
                                                                            runat="server" Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label><span
                                                                                class='mandatory'>&nbsp;*</span>
                                                                    </td>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="EditInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryTextBox"
                                                                                        ReadOnly="true" runat="server" Text='<%# Eval("Category") %>'></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="EditInterviewTestWithAdaptive_addQuestion_byQuestion_subjectLabel"
                                                                                        runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    <asp:HiddenField ID="EditInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryHiddenField"
                                                                                        runat="server" Value='<%# Eval("Cat_sub_id") %>' />
                                                                                    <asp:HiddenField ID="EditInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryNameHiddenField"
                                                                                        runat="server" Value='<%# Eval("Category") %>' />
                                                                                    <asp:HiddenField ID="EditInterviewTestWithAdaptiveAddQuestion_byQuestion_subjectNameHiddenField"
                                                                                        runat="server" Value='<%# Eval("Subject") %>' />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="EditInterviewTestWithAdaptiveAddQuestion_byQuestion_subjectTextBox"
                                                                                        ReadOnly="true" runat="server" Text='<%# Eval("Subject") %>'></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:ImageButton ID="EditInterviewTestWithAdaptiveAddQuestion_byQuestion_categoryImageButton"
                                                                                        SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" ToolTip="Click here to select category and subject" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="EditInterviewTestWithAdaptive_tagsLabel" runat="server" Text="Tags"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="EditInterviewTestWithAdaptive_tagsTextBox" runat="server" MaxLength="100"
                                                                            Width="500px"></asp:TextBox>
                                                                        <asp:ImageButton ID="EditInterviewTestWithAdaptive_tagsImageButton" runat="server"
                                                                            SkinID="sknHelpImageButton" ToolTip="Please enter tags separated by commas" OnClientClick="javascript:return false;" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="5" class="tab_body_bg">
                                                                <tr id="EditInterviewTestWithAdaptive_displayQuestionImageTR" runat="server" style="display: none">
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Image runat="server" ID="EditInterviewTestWithAdaptive_questionImage" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="EditInterviewTestWithAdaptive_deleteLinkButton" runat="server"
                                                                                        Text="Delete" SkinID="sknActionLinkButton" OnClick="EditInterviewTestWithAdaptive_deleteLinkButtonClick"
                                                                                        ToolTip="Click to delete image" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_td_padding_5">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td align="left" style="width: 20px; padding-right: 5px">
                                                <asp:Button ID="EditInterviewTestWithAdaptive_addQuestioSaveButton" runat="server" SkinID="sknButtonId"
                                                    Text="Save" OnClick="EditInterviewTestWithAdaptive_addQuestioSaveButton_Clicked" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="EditInterviewTestWithAdaptive_addQuestioCloseLinkButton" SkinID="sknPopupLinkButton"
                                                    runat="server" Text="Cancel" 
                                                    onclick="EditInterviewTestWithAdaptive_addQuestioCloseLinkButton_Click"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <ajaxToolKit:ModalPopupExtender ID="EditInterviewTestWithAdaptive_addQuestioModalPopupExtender"
                        runat="server" PopupControlID="EditInterviewTestWithAdaptive_addQuestionPanel" TargetControlID="EditInterviewTestWithAdaptive_addQuestionhiddenButton"
                        BackgroundCssClass="modalBackground">
                    </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <!-- End Popup Added -->
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text">
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td width="22%">
                                        <asp:Button ID="EditInterviewTestWithAdaptive_bottomSaveButton" runat="server" SkinID="sknButtonId"
                                            Text="Save" OnClick="EditInterviewTestWithAdaptive_saveButton_Click" />
                                    </td>
                                    <td width="40%">
                                        <asp:Button ID="EditInterviewTestWithAdaptive_bottomCreateSessionButton" runat="server" SkinID="sknButtonId"
                                            Text="Create Interview Session" />
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="EditInterviewTestWithAdaptive_bottompResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Reset" OnClick="EditInterviewTestWithAdaptive_resetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center">
                                        |
                                        <td width="18%" align="left">
                                            <asp:LinkButton ID="EditInterviewTestWithAdaptive_bottomCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                Text="Cancel" OnClick="ParentPageRedirect" />
                                        </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="EditInterviewTestWithAdaptive_questionDetailPanel" runat="server" CssClass="popupcontrol_question_draft"
                                            Style="display: none">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="td_height_20">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                    <asp:Literal ID="EditInterviewTestWithAdaptive_questionDetail_questionResultLiteral" runat="server"
                                                                        Text="Question Detail"></asp:Literal>
                                                                </td>
                                                                <td style="width: 50%" valign="top">
                                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:ImageButton ID="EditInterviewTestWithAdaptive_questionDetailPreviewControl_topCancelImageButton"
                                                                                    runat="server" SkinID="sknCloseImageButton" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_20">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:DataGrid ID="EditInterviewTestWithAdaptive_questionDetailCategoryDataGrid" runat="server" AutoGenerateColumns="false">
                                                                                    <Columns>
                                                                                        <asp:BoundColumn HeaderText="Category" DataField="Category"></asp:BoundColumn>
                                                                                        <asp:BoundColumn HeaderText="Subject" DataField="Subject"></asp:BoundColumn>
                                                                                    </Columns>
                                                                                </asp:DataGrid>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="popup_panel_inner_bg">
                                                                                <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_questionDetailTestAreaHeadLabel" runat="server" Text="Interview Area"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_questionDetailTestAreaLabel" runat="server" Text="Concept"
                                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_questionDetailNoOfTestLabel" runat="server" Text="Number Of Administered Interview"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_questionDetailTestAdministeredLabel" runat="server"
                                                                                                Text="5" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_questionDetailAvgTimeLabel" runat="server" Text="Average Time Taken(in minutes)"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_questionDetailAvgTimeValueLabel" runat="server" Text="15.5"
                                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_questionDetailCorrectToAttendedRatioHeadLabel" runat="server"
                                                                                                Text=" Ratio of Correct Answer To Attended" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_questionDetailCorrectToAttendedRatioLabel" runat="server"
                                                                                                Text="36:50" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_questionDetailComplexityHeadLabel" runat="server" Text="Complexity"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EditInterviewTestWithAdaptive_questionDetailPreviewControlComplexityLabel" runat="server"
                                                                                                Text="Normal" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="popup_question_icon">
                                                                                <asp:Label ID="EditInterviewTestWithAdaptive_questionDetailQuestionLabel" runat="server" SkinID="sknLabelText"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_padding_left_20">
                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                    <asp:RadioButtonList ID="EditInterviewTestWithAdaptive_questionDetailAnswerRadioButtonList" runat="server"
                                                                                        RepeatColumns="2" RepeatDirection="Horizontal" CellSpacing="5" TextAlign="Right"
                                                                                        Width="100%">
                                                                                    </asp:RadioButtonList>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_8">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" style="padding-left: 30px">
                                                                    <table border="0" cellpadding="0" cellspacing="0" align="left">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Button ID="EditInterviewTestWithAdaptive_questionDetail_TopAddButton" runat="server" Text="Add"
                                                                                    SkinID="sknButtonId" OnClick="EditInterviewTestWithAdaptive_questionDetail_TopAddButton_Click" />
                                                                            </td>
                                                                            <td style="padding-left: 10px">
                                                                                <asp:LinkButton ID="EditInterviewTestWithAdaptive_questionDetail_TopCancelButton" runat="server"
                                                                                    SkinID="sknCancelLinkButton" Text="Cancel" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_8">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <div style="display: none">
                                            <asp:Button ID="EditInterviewTestWithAdaptive_adaptiveQuestionPreviewPanelTargeButton" runat="server" />
                                        </div>
                                        <ajaxToolKit:ModalPopupExtender ID="EditInterviewTestWithAdaptive_adaptiveQuestionPreviewModalPopupExtender"
                                            runat="server" PopupControlID="EditInterviewTestWithAdaptive_questionDetailPanel" TargetControlID="EditInterviewTestWithAdaptive_adaptiveQuestionPreviewPanelTargeButton"
                                            BackgroundCssClass="modalBackground" CancelControlID="EditInterviewTestWithAdaptive_questionDetail_TopCancelButton">
                                        </ajaxToolKit:ModalPopupExtender>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:UpdatePanel ID="EditInterviewTestWithAdaptive_questionDetailPreviewUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="EditInterviewTestWithAdaptive_questionPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_question_detail">
                <%--<uc3:QuestionDetailPreviewControl ID="EditInterviewTestWithAdaptive_questionDetailPreviewControl"
                    runat="server" OnCancelClick="EditInterviewTestWithAdaptive_cancelClick" />--%>
                <uc7:QuestionDetailSummaryControl ID="EditInterviewTestWithAdaptive_questionDetailSummaryControl"
                    runat="server" Title="Question Details Summary" OnCancelClick="EditInterviewTestWithAdaptive_cancelClick"
                    OnAddClick="EditInterviewTestWithAdaptive_questionDetailPreviewControl_AddClick" />
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="EditInterviewTestWithAdaptive_questionModalTargeButton" runat="server" />
            </div>
            <ajaxToolKit:ModalPopupExtender ID="EditInterviewTestWithAdaptive_questionModalPopupExtender" runat="server"
                PopupControlID="EditInterviewTestWithAdaptive_questionPanel" TargetControlID="EditInterviewTestWithAdaptive_questionModalTargeButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="EditInterviewTestWithAdaptive_ConfirmPopupUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="EditInterviewTestWithAdaptive_sessionInclucedKeyHiddenField" runat="server" />
            <asp:HiddenField ID="EditInterviewTestWithAdaptive_questionKeyHiddenField" runat="server" />
            <asp:HiddenField ID="EditInterviewTestWithAdaptive_deletedQuestionHiddenField" runat="server" />
            <asp:HiddenField ID="EditInterviewTestWithAdaptive_insertedQuestionHiddenField" runat="server" />
            <asp:HiddenField ID="EditInterviewTestWithAdaptive_authorIdHiddenField" runat="server" />
            <asp:HiddenField ID="EditInterviewTestWithAdaptive_positionProfileHiddenField" runat="server" />
            <asp:HiddenField ID="hiddenValue" runat="server" />
            <asp:HiddenField ID="hiddenDeleteValue" runat="server" />
            <div id="EditInterviewTestWithAdaptive_hiddenDIV" runat="server" style="display: none">
                <asp:Button ID="EditInterviewTestWithAdaptive_hiddenPopupModalButton" runat="server" />
            </div>
            <asp:Panel ID="EditInterviewTestWithAdaptive_ConfirmPopupPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_confirm_remove">
                <uc6:ConfirmMsgControl ID="EditInterviewTestWithAdaptive_ConfirmPopupExtenderControl" runat="server"
                    OnOkClick="EditInterviewTestWithAdaptive_okClick" OnCancelClick="EditInterviewTestWithAdaptive_cancelClick" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="EditInterviewTestWithAdaptive_ConfirmPopupExtender" runat="server"
                PopupControlID="EditInterviewTestWithAdaptive_ConfirmPopupPanel" TargetControlID="EditInterviewTestWithAdaptive_hiddenPopupModalButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load
    (
        function () {
            window.setTimeout(focus, 1);
        }
    )
        function focus() {
            try {
                document.getElementById('<%=EditInterviewTestWithAdaptive_categoryTextBox.ClientID %>').focus();
            }
            catch (Err) {
                try {
                    document.getElementById('<%=EditInterviewTestWithAdaptive_keywordTextBox.ClientID %>').focus();
                }
                catch (Err1) {
                    document.getElementById('ctl00$OTMMaster_body$CreateManualTest_mainTabContainer$CreateManualTest_testdetailsTabPanel$CreateManualTest_testDetailsUserControl$TestDetailsControl_testNameTextBox').focus();
                }
            }
        }
    </script>
</asp:Content>
