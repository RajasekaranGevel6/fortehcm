﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateInterviewTestSession.aspx.cs"
    MasterPageFile="~/MasterPages/InterviewMaster.Master" Inherits="Forte.HCM.UI.InterviewTestMaker.CreateInterviewTestSession" %>

<%@ Register Src="~/CommonControls/CandidateInterviewDetailControl.ascx" TagName="CandidateDetail"
    TagPrefix="uc5" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/InterviewSessionPreviewControl.ascx" TagName="InterviewSessionPreviewControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="CreateInterviewTestSession_bodyContent" ContentPlaceHolderID="InterviewMaster_body"
    runat="server">
    <script type="text/javascript">
        // Function calls when user clicks on the cancel interview session image button
        function CandidateDetailModalPopup() {
            $find("<%= CreateInterviewTestSession_candidateDetailModalPopupExtender.ClientID %>").show();
            return false;
        }
    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="50%" class="header_text_bold">
                            <asp:Literal ID="CreateInterviewTestSession_headerLiteral" runat="server" Text="Create Interview Session"></asp:Literal>
                        </td>
                        <td width="50%" align="right">
                            <asp:UpdatePanel ID="CreateInterviewTestSession_topButtonsUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="4">
                                        <tr>
                                            <td>
                                                <asp:Button ID="CreateInterviewTestSession_topScheduleCandidateButton" runat="server"
                                                    Text="Schedule Candidate" SkinID="sknButtonId" Visible="false" OnClick="CreateInterviewTestSession_scheduleCandidateButton_Click" />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="CreateInterviewTestSession_emailImageButton" runat="server"
                                                    ToolTip="Click here to email the interview sessions" SkinID="sknMailImageButton" Visible="false" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="CreateInterviewTestSession_topResetLinkButton" runat="server"
                                                    SkinID="sknActionLinkButton" Text="Reset" OnClick="CreateInterviewTestSession_resetButton_Click" />
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="CreateInterviewTestSession_topCancelLinkButton" runat="server"
                                                    SkinID="sknActionLinkButton" Text="Cancel" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CreateInterviewTestSession_topSuccessErrorMsgUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CreateInterviewTestSession_topSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CreateInterviewTestSession_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="non_tab_body_bg">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="panel_bg">
                            <asp:UpdatePanel ID="CreateInterviewTestSession_sessionDetailsUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <div id="CreateInterviewTestSession_sessionDetailsDIV" runat="server">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="panel_inner_body_bg">
                                                                    <tr>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="CreateInterviewTestSession_testKeyHeadLabel" runat="server" Text="Interview ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="CreateInterviewTestSession_testKeyValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="CreateInterviewTestSession_testNameHeadLabel" runat="server" Text="Interview Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%;">
                                                                            <asp:Label ID="CreateInterviewTestSession_testNameValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="CreateInterviewTestSession_sessionKeyHeadLabel" runat="server" Text="Interview Session ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="CreateInterviewTestSession_sessionKeyValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                Width="60%"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%;">
                                                                            <asp:Label ID="CreateInterviewTestSession_creditHeadLabel" runat="server" Text="Total Credit (in $)"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:Label ID="CreateInterviewTestSession_creditValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                Text="0"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="8" align="left" style="width: 100%">
                                                                            <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                                                                <tr>
                                                                                    <td style="width: 12%;">
                                                                                        <asp:Label ID="CreateInterviewTestSession_positionProfileHeadLabel" runat="server"
                                                                                            Text="Position Profile" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 85%">
                                                                                        <asp:Label ID="CreateInterviewTestSession_positionProfileValueLabel" runat="server"
                                                                                            SkinID="sknLabelFieldText" Text=""></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="header_bg_testsession" style="width: 50%;">
                                                                <asp:Label ID="CreateInterviewTestSession_sessionDescHeadLabel" runat="server" Text="Session Description"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td class="header_bg_testsession" style="width: 50%;">
                                                                <asp:Label ID="CreateInterviewTestSession_instructionsHeadLabel" runat="server" Text="Interview Instructions"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%;">
                                                                <asp:TextBox ID="CreateInterviewTestSession_sessionDescTextBox" runat="server" TextMode="MultiLine"
                                                                    SkinID="sknMultiLineTextBox" MaxLength="8000" Width="466px" Height="80px" onkeyup="CommentsCount(8000,this)"
                                                                    onchange="CommentsCount(8000,this)" TabIndex="1"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 50%;">
                                                                <asp:TextBox ID="CreateInterviewTestSession_instructionsTextBox" runat="server" TextMode="MultiLine"
                                                                    MaxLength="8000" Width="466px" Height="80px" onkeyup="CommentsCount(8000,this)"
                                                                    onchange="CommentsCount(8000,this)" TabIndex="2"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_bg_testsession">
                                                    Settings
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="non_tab_body_bg">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="2" cellspacing="3" width="100%">
                                                                    <tr>
                                                                        <td style="width: 40%;">
                                                                            <asp:Label ID="CreateInterviewTestSession_sessionNoHeadLabel" runat="server" Text="Number of Sessions"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">&nbsp;*</span>
                                                                        </td>
                                                                        <td>
                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="CreateInterviewTestSession_sessionNoTextBox" MaxLength="2" runat="server"
                                                                                            Width="30px" TabIndex="3"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td style="vertical-align: bottom">
                                                                                                    <asp:ImageButton ID="CreateInterviewTestSession_upImageButton" runat="server" ImageAlign="AbsBottom"
                                                                                                        SkinID="sknNumericUpArrowImage" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="vertical-align: top">
                                                                                                    <asp:ImageButton ID="CreateInterviewTestSession_downImageButton" runat="server" ImageAlign="Top"
                                                                                                        SkinID="sknNumericDownArrowImage" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <ajaxToolKit:NumericUpDownExtender ID="CreateInterviewTestSession_NumericUpDownExtender"
                                                                                Width="60" runat="server" Minimum="0" Maximum="30" Step="1" TargetControlID="CreateInterviewTestSession_sessionNoTextBox"
                                                                                TargetButtonUpID="CreateInterviewTestSession_upImageButton" TargetButtonDownID="CreateInterviewTestSession_downImageButton">
                                                                            </ajaxToolKit:NumericUpDownExtender>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CreateInterviewTestSession_expiryDateHeadLabel" runat="server" Text="Expiry Date"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">&nbsp;*</span>
                                                                        </td>
                                                                        <td>
                                                                            <table width="97.5%" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td style="width: 40%">
                                                                                        <asp:TextBox ID="CreateInterviewTestSession_expiryDateTextBox" runat="server" MaxLength="10"
                                                                                            AutoCompleteType="None" Columns="15" TabIndex="5"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:ImageButton ID="CreateInterviewTestSession_calendarImageButton" SkinID="sknCalendarImageButton"
                                                                                            runat="server" ImageAlign="Middle" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <ajaxToolKit:MaskedEditExtender ID="CreateInterviewTestSession_MaskedEditExtender"
                                                                                runat="server" TargetControlID="CreateInterviewTestSession_expiryDateTextBox"
                                                                                Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="CreateInterviewTestSession_MaskedEditValidator"
                                                                                runat="server" ControlExtender="CreateInterviewTestSession_MaskedEditExtender"
                                                                                ControlToValidate="CreateInterviewTestSession_expiryDateTextBox" EmptyValueMessage="Date is required"
                                                                                InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="CreateInterviewTestSession_customCalendarExtender"
                                                                                runat="server" TargetControlID="CreateInterviewTestSession_expiryDateTextBox"
                                                                                CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="CreateInterviewTestSession_calendarImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="CreateInterviewTestSession_allowPauseInterviewLabel" Text="Allow Interview To Be Paused"
                                                                                runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="CreateInterviewTestSession_allowPauseInterviewCheckbox" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="td_v_line">
                                                            </td>
                                                            <td style="width: 50%">
                                                                <table width="100%" cellpadding="2" cellspacing="3" border="0" height="100px">
                                                                    <tr id="TrInterviewAssessor" runat="server">
                                                                        <td align="right" valign="top" style="height: 23px">
                                                                            <asp:LinkButton ID="CreateInterviewTestSession_recommendAssessorLinkButton" runat="server"
                                                                                SkinID="sknAddLinkButton" Text="Search and Add Assessors" ToolTip="Search and Add Assessors" 
                                                                                onclick="CreateInterviewTestSession_recommendAssessorLinkButton_Click" 
                                                                                ></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="CreateInterviewTestSession_assessorDetailsTr" style="width: 100%" runat="server">
                                                                        <td id="Td2" class="tab_body_bg" runat="server">
                                                                            <asp:UpdatePanel ID="CreateInterviewTestSession_assessorDetailsupdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <div style="width: 100%; overflow: auto; height: 80px;" runat="server" id="CreateInterviewTestSession_assessorDetailsDiv"
                                                                                        visible="False">
                                                                                        <asp:GridView ID="CreateInterviewTestSession_assessorDetailsGridView" runat="server"
                                                                                            AutoGenerateColumns="True"
                                                                                            GridLines="Horizontal" BorderColor="white" BorderWidth="1px" Width="100%">
                                                                                            <RowStyle CssClass="grid_alternate_row" />
                                                                                            <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                                            <HeaderStyle CssClass="grid_header_row" />
                                                                                            <Columns>
                                                                                                <asp:TemplateField HeaderText="Name">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateInterviewTestSession_assessorDetailsFirstNameLabel" runat="server"
                                                                                                            Text='<%# Eval("FirstName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Email">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="CreateInterviewTestSession_assessorDetailsEmailLabel" runat="server"
                                                                                                            Text='<%# Eval("UserEmail") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="td_padding_top_5">
                                                    <asp:Button ID="CreateInterviewTestSession_topSaveButton" runat="server" Text="Save"
                                                        SkinID="sknButtonId" OnClick="CreateInterviewTestSession_saveButton_Click" TabIndex="9" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header_bg" align="center">
                                        <asp:UpdatePanel ID="CreateInterviewTestSession_maxMinButtonUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="CreateInterviewTestSession_searchSessionResultsLiteral" runat="server"
                                                                Text="Interview Sessions"></asp:Literal>
                                                            <asp:Label ID="CreateInterviewTestSession_searchSessionResultsHelpLabel" runat="server"
                                                                SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%" align="right" id="CreateInterviewTestSession_searchTestSessionResultsTR"
                                                            runat="server">
                                                            <span id="CreateInterviewTestSession_searchSessionResultsUpSpan" runat="server" style="display: none;">
                                                                <asp:Image ID="CreateInterviewTestSession_searchSessionResultsUpImage" runat="server"
                                                                    SkinID="sknMinimizeImage" /></span><span id="CreateInterviewTestSession_searchSessionResultsDownSpan"
                                                                        runat="server" style="display: none;"><asp:Image ID="CreateInterviewTestSession_searchSessionResultsDownImage"
                                                                            runat="server" SkinID="sknMaximizeImage" /></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tab_body_bg">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:HiddenField ID="CreateInterviewTestSession_restoreHiddenField" runat="server" />
                                                    <asp:UpdatePanel ID="CreateInterviewTestSession_testSessionUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr id="CreateInterviewTestSession_testSessionGridviewTR" runat="server" visible="false">
                                                                    <td align="left">
                                                                        <div style="height: 200px; overflow: auto;" runat="server" id="CreateInterviewTestSession_testSessionDiv">
                                                                            <asp:GridView ID="CreateInterviewTestSession_testSessionGridView" runat="server"
                                                                                AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Width="100%"
                                                                                OnSorting="CreateInterviewTestSession_testSessionGridView_Sorting" OnRowCommand="CreateInterviewTestSession_testSessionGridView_RowCommand"
                                                                                OnRowDataBound="CreateInterviewTestSession_testSessionGridView_RowDataBound"
                                                                                OnRowCreated="CreateInterviewTestSession_testSessionGridView_RowCreated" SkinID="sknWrapHeaderGrid">
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="CreateInterviewTestSession_candidateDetailImageButton" runat="server"
                                                                                                SkinID="sknCandidateDetailImageButton" ToolTip="Candidate Detail" CommandName="CandidateDetail" />
                                                                                            <asp:ImageButton ID="CreateInterviewTestSession_viewTestSessionsImageButton" runat="server"
                                                                                                SkinID="sknViewSessionImageButton" ToolTip="View Interview Session" CommandName="ViewTestSession"
                                                                                                CommandArgument='<%# Eval("InterviewTestSessionID") %>' Visible="true" />
                                                                                            <asp:ImageButton ID="CreateInterviewTestSession_cancelCreateTestSessionImageButton"
                                                                                                runat="server" SkinID="sknCancelImageButton" ToolTip="Cancel Interview Session"
                                                                                                CommandName="CancelTestSession" CommandArgument='<%# Eval("InterviewTestSessionID") %>' />
                                                                                            <asp:HiddenField runat="server" ID="CreateInterviewTestSession_attemptIdHiddenField"
                                                                                                Value='<%# Eval("AttemptID") %>' />
                                                                                            <asp:HiddenField runat="server" ID="CreateInterviewTestSession_statusHiddenField"
                                                                                                Value='<%# Eval("Status") %>' />
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="10%" HorizontalAlign="Left" />
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="Interview&nbsp;Session&nbsp;ID" DataField="InterviewTestSessionID"
                                                                                        SortExpression="InterviewSessionID">
                                                                                        <ItemStyle Width="10%" />
                                                                                    </asp:BoundField>
                                                                                    <asp:TemplateField HeaderText="Interview&nbsp;Key" Visible="false">
                                                                                        <ItemStyle Width="10%" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="CreateInterviewTestSession_byTestSession_testKeyLabel" runat="server"
                                                                                                Text='<%# Eval("InterviewTestID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Candidate Session&nbsp;ID" SortExpression="CandidateInterviewSessionID">
                                                                                        <ItemStyle Width="10%" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="CreateInterviewTestSession_byTestSession_candidateSessionIdLabel"
                                                                                                runat="server" Text='<%# Eval("CandidateTestSessionID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="Position Profile" DataField="PositionProfileName" SortExpression="PositionProfile">
                                                                                        <ItemStyle Width="12%" />
                                                                                    </asp:BoundField>
                                                                                    <asp:TemplateField HeaderText="Administered By">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="CreateInterviewTestSession_testSessionAuthorFullnameLabel" runat="server"
                                                                                                Text='<%# Eval("InteriewSessionAuthor") %>' ToolTip='<%# Eval("InterviewSessionAuthorFullName")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Candidate Name">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="CreateInterviewTestSession_candidateFullnameLabel" runat="server"
                                                                                                Text='<%# Eval("CandidateName") %>' ToolTip='<%# Eval("CandidateFullName")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Date of interview" SortExpression="ScheduledDate">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="CreateInterviewTestSession_scheduledDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateInterviewSessionDetail)Container.DataItem).ScheduledDate) %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="CreateInterviewTestSession_pageNavigatorUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <uc1:PageNavigator ID="CreateInterviewTestSession_bottomSessionPagingNavigator" runat="server" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:UpdatePanel ID="CreateInterviewTestSession_cancelTestPopupUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="CreateInterviewTestSession_cancelTestPanel" runat="server" Style="display: none"
                                                    CssClass="popupcontrol_cancel_session">
                                                    <div style="display: none">
                                                        <asp:Button ID="CreateInterviewTestSession_hiddenButton" runat="server" Text="Hidden" />
                                                    </div>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="popup_td_padding_10">
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                                                            <asp:Label ID="CreateInterviewTestSession_questionResultLiteral" runat="server" Text="Cancel Interview Session"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25%" align="right">
                                                                            <asp:ImageButton ID="CreateInterviewTestSession_cancelImageButton" runat="server"
                                                                                SkinID="sknCloseImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popup_td_padding_10">
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                                    <tr>
                                                                        <td class="popup_td_padding_10">
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td colspan="2" style="width: 100%" align="center">
                                                                                        <asp:Label ID="CreateInterviewTestSession_cancelErrorMessageLabel" runat="server"
                                                                                            SkinID="sknErrorMessage"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%">
                                                                                        <asp:Label ID="CreateInterviewTestSession_cancelReasonLabel" runat="server" Text="Reason"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="CreateInterviewTestSession_cancelTestReasonTextBox" runat="server"
                                                                                            MaxLength="100" SkinID="sknMultiLineTextBox" Columns="90" TextMode="MultiLine"
                                                                                            Height="70" onkeyup="CommentsCount(100,this)" onchange="CommentsCount(100,this)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_8">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popup_td_padding_10">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Button ID="CreateInterviewTestSession_saveCancellationButton" runat="server"
                                                                                Text="Save" SkinID="sknButtonId" OnClick="CreateInterviewTestSession_saveCancellationButton_Click" />
                                                                            &nbsp;
                                                                            <asp:LinkButton ID="CreateInterviewTestSession_closeCancellationButton" runat="server"
                                                                                Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="CreateInterviewTestSession_cancelSessionModalPopupExtender"
                                                    runat="server" PopupControlID="CreateInterviewTestSession_cancelTestPanel" TargetControlID="CreateInterviewTestSession_hiddenButton"
                                                    BackgroundCssClass="modalBackground" CancelControlID="CreateInterviewTestSession_closeCancellationButton">
                                                </ajaxToolKit:ModalPopupExtender>
                                                <asp:Panel ID="CreateInterviewTestSession_canidateDetailPanel" runat="server" Style="display: none;
                                                    height: auto;" CssClass="popupcontrol_question_detail">
                                                    <div style="display: none">
                                                        <asp:Button ID="CreateInterviewTestSession_canidateDetailHiddenButton" runat="server" />
                                                    </div>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <uc5:CandidateDetail ID="CreateInterviewTestSession_candidateDetailControl" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="CreateInterviewTestSession_candidateDetailModalPopupExtender"
                                                    runat="server" TargetControlID="CreateInterviewTestSession_canidateDetailHiddenButton"
                                                    PopupControlID="CreateInterviewTestSession_canidateDetailPanel" BackgroundCssClass="modalBackground">
                                                </ajaxToolKit:ModalPopupExtender>
                                                <asp:Panel ID="CreateInterviewTestSession_viewTestSessionSavePanel" runat="server"
                                                    Style="display: none" CssClass="preview_interview_session">
                                                    <div style="display: none">
                                                        <asp:Button ID="CreateInterviewTestSession_viewTestSessionSaveButton" runat="server" />
                                                    </div>
                                                    <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <uc2:InterviewSessionPreviewControl ID="CreateInterviewTestSession_viewTestSessionSave_UserControl"
                                                                    runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <table border="0" cellspacing="3" cellpadding="3" align="left">
                                                                    <tr>
                                                                        <td class="td_padding_top_5" style="padding-left: 10px">
                                                                            <asp:Button ID="CreateInterviewTestSession_previewTestSessionControl_createButton"
                                                                                runat="server" Text="Create" OnClick="CreateInterviewTestSession_previewTestSessionControl_createButton_Click"
                                                                                SkinID="sknButtonId" />
                                                                        </td>
                                                                        <td class="td_padding_top_5">
                                                                            <asp:LinkButton ID="CreateInterviewTestSession_previewTestSessionControl_cancelButton"
                                                                                runat="server" Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="CreateInterviewTestSession_viewTestSessionSave_modalpPopupExtender"
                                                    runat="server" TargetControlID="CreateInterviewTestSession_viewTestSessionSaveButton"
                                                    PopupControlID="CreateInterviewTestSession_viewTestSessionSavePanel" BackgroundCssClass="modalBackground">
                                                </ajaxToolKit:ModalPopupExtender>
                                                <asp:Panel ID="CreateInterviewTestSession_previewTestSessionPanel" runat="server"
                                                    CssClass="preview_interview_session" Style="display: none">
                                                    <div style="display: none">
                                                        <asp:Button ID="CreateInterviewTestSession_previewTestSessionControl_Button" runat="server" />
                                                    </div>
                                                    <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <uc2:InterviewSessionPreviewControl ID="CreateInterviewTestSession_previewTestSessionControl_userControl1"
                                                                    runat="server" Mode="view" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_padding_top_5" style="padding-left: 10px">
                                                                <asp:LinkButton ID="CreateInterviewTestSession_previewTestSessionControl_cancelButton2"
                                                                    runat="server" Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <ajaxToolKit:ModalPopupExtender ID="CreateInterviewTestSession_previewTestSessionControl_modalpPopupExtender1"
                                                    runat="server" TargetControlID="CreateInterviewTestSession_previewTestSessionControl_Button"
                                                    PopupControlID="CreateInterviewTestSession_previewTestSessionPanel" BackgroundCssClass="modalBackground">
                                                </ajaxToolKit:ModalPopupExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="CreateInterviewTestSession_bottomSuccessErrorMsgUpdatePanel"
                    runat="server">
                    <ContentTemplate>
                        <asp:Label ID="CreateInterviewTestSession_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CreateInterviewTestSession_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="CreateInterviewTestSession_TestKeyHiddenField" runat="server" />
    <asp:HiddenField ID="CreateInterviewTestSession_positionProfileIDHiddenField" runat="server" />
    <asp:HiddenField ID="CreateInterviewTestSession_candidateSessionIDsHiddenField" runat="server" />
    <asp:HiddenField ID="CreateInterviewTestSession_testSessionIDHiddenField" runat="server" />
</asp:Content>
