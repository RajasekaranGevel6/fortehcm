﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreateInterviewTestSession.cs
// File that represents the user interface for the create a new test
// session. This will interact with the TestBLManager to insert test 
// session details to the database.

#endregion Header

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Data;
using System.Text;

#endregion Directives

namespace Forte.HCM.UI.InterviewTestMaker
{
    /// <summary>
    /// Class that represents the user interface and layout and 
    /// functionalities for CreateTestSession page. This is used
    /// to create a test session by providing number of candidates,
    /// session description, test instructions etc. 
    /// Also this page provides some set of links in the test session
    /// grid for viewing candidate detail, test session and cancel the
    /// cancel the candidate test session. This class is inherited from
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class CreateInterviewTestSession : PageBase
    {
        #region Private Variables

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Variables

        #region Event Handlers

        /// <summary>
        /// Handler that will fire when the page is loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.DefaultButton = CreateInterviewTestSession_saveCancellationButton.UniqueID;
                Master.SetPageCaption("Create Interview Session");

                // Call pagination event handler
                CreateInterviewTestSession_bottomSessionPagingNavigator.PageNumberClick
                    += new PageNavigator.PageNumberClickEventHandler
                        (CreateInterviewTestSession_bottomSessionPagingNavigator_PageNumberClick);
               
                if (!IsPostBack)
                {
                    // Set default focus field
                    Page.Form.DefaultFocus = CreateInterviewTestSession_sessionDescTextBox.UniqueID;
                    CreateInterviewTestSession_sessionDescTextBox.Focus();
                    Session["ASSESSOR"] = null;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_DIRECTION_KEY"]))
                        ViewState["SORT_DIRECTION_KEY"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_EXPRESSION"]))
                        ViewState["SORT_EXPRESSION"] = "InterviewSessionID";

                    if (Utility.IsNullOrEmpty(ViewState["PAGE_NUMBER"]))
                        ViewState["PAGE_NUMBER"] = "1";

                    if (Request.QueryString["interviewtestkey"] != null)
                    {
                        CreateInterviewTestSession_TestKeyHiddenField.Value =
                            Request.QueryString["interviewtestkey"].ToString();
                    }
                    //To get list of subjectids
                    Session["SESSION_SUBJECTIDS"] = null;
                    Session["SESSION_POSITION_PROFILE_ID"] = null;
                    LoadSubjectIds(CreateInterviewTestSession_TestKeyHiddenField.Value);

                    LoadTestDetail(CreateInterviewTestSession_TestKeyHiddenField.Value);
                }

                CreateInterviewTestSession_recommendAssessorLinkButton.Attributes.Add("onclick",
                    "return SearchInterviewRecommendAssessor('search',null,null,null,null);");

                // Add javascript function for expanding and collapsing gridview.
                CreateInterviewTestSession_searchTestSessionResultsTR.Attributes.Add("onclick", "ExpandorCompress('"
                  + CreateInterviewTestSession_testSessionDiv.ClientID + "','"
                  + CreateInterviewTestSession_sessionDetailsDIV.ClientID
                  + "','CreateInterviewTestSession_searchSessionResultsUpSpan',"
                  + "'CreateInterviewTestSession_searchSessionResultsDownSpan')");

                // Add handler for email button.
                CreateInterviewTestSession_emailImageButton.Attributes.Add
                    ("onclick", "javascript:return ShowEmailCandidateSessions('EMAIL_INTERVIEW_SESSION')");

                // Validation for expand all and collapse all image button
                if (!Utility.IsNullOrEmpty(CreateInterviewTestSession_restoreHiddenField.Value) &&
                   CreateInterviewTestSession_restoreHiddenField.Value == "Y")
                {
                    CreateInterviewTestSession_sessionDetailsDIV.Style["display"] = "none";
                    CreateInterviewTestSession_searchSessionResultsUpSpan.Style["display"] = "block";
                    CreateInterviewTestSession_searchSessionResultsDownSpan.Style["display"] = "none";
                    CreateInterviewTestSession_testSessionDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    CreateInterviewTestSession_sessionDetailsDIV.Style["display"] = "block";
                    CreateInterviewTestSession_searchSessionResultsUpSpan.Style["display"] = "none";
                    CreateInterviewTestSession_searchSessionResultsDownSpan.Style["display"] = "block";
                    CreateInterviewTestSession_testSessionDiv.Style["height"] = RESTORED_HEIGHT;
                }
                CreateInterviewTestSession_searchTestSessionResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    CreateInterviewTestSession_testSessionDiv.ClientID + "','" +
                    CreateInterviewTestSession_sessionDetailsDIV.ClientID + "','" +
                    CreateInterviewTestSession_searchSessionResultsUpSpan.ClientID + "','" +
                    CreateInterviewTestSession_searchSessionResultsDownSpan.ClientID + "','" +
                    CreateInterviewTestSession_restoreHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestSession_topErrorMessageLabel,
                    CreateInterviewTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when a page number is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/> that contains the event data.
        /// </param>
        void CreateInterviewTestSession_bottomSessionPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            LoadTestSessions(e.PageNumber);
            ViewState["PAGE_NUMBER"] = e.PageNumber;
        }

        /// <summary>
        /// Handler that will help to validate input fields before they saved.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateInterviewTestSession_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();

                // Validate the data
                if (!IsValidData())
                    return;

                InterviewSessionDetail interviewSessionDetail = PreviewTestSession();

                CreateInterviewTestSession_viewTestSessionSave_UserControl.Mode = "view";

                CreateInterviewTestSession_viewTestSessionSave_UserControl.DataSource = interviewSessionDetail;
                CreateInterviewTestSession_viewTestSessionSave_modalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestSession_topErrorMessageLabel,
                    CreateInterviewTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when clicking on create button which shows
        /// the given information before its getting inserted in the database.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateInterviewTestSession_previewTestSessionControl_createButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                // Clear success/error label message
                ClearLabelMessage();

                InterviewSessionDetail testSessionDetail = ConstructTestSessionDetail();

                CreditRequestDetail creditRequestDetail = new CreditRequestDetail();

                creditRequestDetail.Amount = decimal.Parse(CreateInterviewTestSession_creditValueLabel.Text);

                string testSessionID = null;
                string candidateSessionIDs = null;
                bool isMailSent = false;

                //Assessor list and selected skill id
                List<AssessorDetail> assessorDetails = null;

                if (Session["ASSESSOR"] == null)
                    assessorDetails = new List<AssessorDetail>();
                else
                    assessorDetails = Session["ASSESSOR"] as List<AssessorDetail>;

                new InterviewSessionBLManager().SaveInterviewSession(testSessionDetail, assessorDetails, base.userID,
                    out testSessionID, out candidateSessionIDs, out isMailSent);

                // Keep the TestSessionID in Viewstate. 
                // This will be used by ScheduleCandidate button event handler
                ViewState["TEST_SESSION_ID"] = testSessionID;

                base.ShowMessage(CreateInterviewTestSession_topSuccessMessageLabel,
                    CreateInterviewTestSession_bottomSuccessMessageLabel,
                    string.Format(Resources.HCMResource.
                    CreateInterviewTestSession_TestSessionAndCandidateSessionsCreatedSuccessfully,
                    testSessionID, candidateSessionIDs));

                // To hide the add assessor link
                if (!string.IsNullOrEmpty(testSessionID))
                    TrInterviewAssessor.Visible = false;
                    
                
                // If the mail sent failed, then show the error message.
                if (isMailSent == false)
                {
                    base.ShowMessage(CreateInterviewTestSession_topErrorMessageLabel,
                        CreateInterviewTestSession_bottomErrorMessageLabel,
                         "<br>Mail cannot be sent to the session author. Contact your administrator");
                }

                CreateInterviewTestSession_sessionKeyValueLabel.Text =
                    (testSessionID != null ? testSessionID : string.Empty);

                // Set visible false once the session is created.
                CreateInterviewTestSession_topSaveButton.Visible = false;

                // Enable the schedule candidate button
                CreateInterviewTestSession_topScheduleCandidateButton.Visible = true;

                // Show the email session button.
                CreateInterviewTestSession_emailImageButton.Visible = true;

                // Keep the created test session details in session, so that it can
                // be used in the email popup.
                Session["EMAIL_INTERVIEW_SESSION_SUBJECT"] = string.Format
                    ("Interview session '{0}' created", testSessionID);
                Session["EMAIL_INTERVIEW_SESSION_MESSAGE"] = string.Format
                    ("Dear Viewer,\n\nSub: Interview session '{0}' created.\n\nThe following candidate sessions are associated:\n{1}",
                    testSessionID, candidateSessionIDs);

                // Refresh test session details gridview i.e. Once a testsession is
                // created, that will be loaded in the gridview
                LoadTestSessions(1);
            }
            catch (Exception exp)
            {
                // Catch the error
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestSession_topErrorMessageLabel,
                    CreateInterviewTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }
        
        /// <summary>
        /// Handler that will call when clicking anyone of the icons such as,
        /// CandidateDetail/View Test Session/Cancel Test Session by passing 
        /// its command name and command argument.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that contains the event data.
        /// </param>
        protected void CreateInterviewTestSession_testSessionGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "CandidateDetail")
                {
                    CandidateTestDetail candidateDetail = new CandidateTestDetail();
                    candidateDetail.CandidateSessionID = ((Label)((ImageButton)e.CommandSource).
                        FindControl("CreateInterviewTestSession_byTestSession_candidateSessionIdLabel")).Text;
                    candidateDetail.AttemptID = int.Parse(((HiddenField)((ImageButton)e.CommandSource).
                        FindControl("CreateInterviewTestSession_attemptIdHiddenField")).Value);
                    candidateDetail.TestID = ((Label)((ImageButton)e.CommandSource).
                        FindControl("CreateInterviewTestSession_byTestSession_testKeyLabel")).Text;
                    candidateDetail.CandidateName = ((Label)((ImageButton)e.CommandSource).
                       FindControl("CreateInterviewTestSession_candidateFullnameLabel")).Text;
                    candidateDetail.ShowTestScore = new TestSessionBLManager().GetShowTestScore(candidateDetail.CandidateSessionID);
                    CreateInterviewTestSession_candidateDetailControl.Datasource = candidateDetail;
                    CreateInterviewTestSession_candidateDetailModalPopupExtender.CancelControlID =
                        ((ImageButton)CreateInterviewTestSession_candidateDetailControl.
                        FindControl("CandidateInterviewDetailControl_topCancelImageButton")).ClientID;
                    CreateInterviewTestSession_candidateDetailModalPopupExtender.Show();
                }
                else if (e.CommandName == "ViewTestSession")
                {
                    CreateInterviewTestSession_previewTestSessionControl_userControl1.DataSource =
                    new InterviewSchedulerBLManager().GetInteviewSessionDetail(e.CommandArgument.ToString(), "dummy",
                        "CandidateSessionId", 0);
                    CreateInterviewTestSession_previewTestSessionControl_modalpPopupExtender1.Show();
                }
                else if (e.CommandName == "CancelTestSession")
                {
                    CreateInterviewTestSession_cancelTestReasonTextBox.Text = string.Empty;

                    HiddenField attemptIdHiddenField = (e.CommandSource as ImageButton).Parent.
                        FindControl("CreateInterviewTestSession_attemptIdHiddenField") as HiddenField;

                    ViewState["CANDIDATE_SESSION_ID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;

                    CreateInterviewTestSession_cancelErrorMessageLabel.Text = string.Empty;
                    CreateInterviewTestSession_cancelSessionModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestSession_topErrorMessageLabel,
                    CreateInterviewTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call whenever a row gets the information. 
        /// It implements the mouse over and out style and showing icons
        /// based on the validation.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> contains the event data.
        /// </param>
        protected void CreateInterviewTestSession_testSessionGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                ImageButton CreateInterviewTestSession_candidateDetailImageButton = (ImageButton)
                    e.Row.FindControl("CreateInterviewTestSession_candidateDetailImageButton");

                ImageButton CreateInterviewTestSession_cancelCreateTestSessionImageButton = (ImageButton)
                    e.Row.FindControl("CreateInterviewTestSession_cancelCreateTestSessionImageButton");

                HiddenField CreateInterviewTestSession_statusHiddenField = (HiddenField)
                    e.Row.FindControl("CreateInterviewTestSession_statusHiddenField");

                // Set visibility based on the attempt status
                if (CreateInterviewTestSession_statusHiddenField.Value.Trim()
                    == Constants.CandidateAttemptStatus.SCHEDULED)
                {
                    CreateInterviewTestSession_candidateDetailImageButton.Visible = true;
                    CreateInterviewTestSession_cancelCreateTestSessionImageButton.Visible = true;
                }

                if (CreateInterviewTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.NOT_SCHEDULED
                    || CreateInterviewTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.CANCELLED)
                {
                    CreateInterviewTestSession_candidateDetailImageButton.Visible = false;
                    CreateInterviewTestSession_cancelCreateTestSessionImageButton.Visible = false;
                }
                else if (CreateInterviewTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.IN_PROGRESS
                    || CreateInterviewTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.CYBER_PROC_INIT
                    || CreateInterviewTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.COMPLETED
                    || CreateInterviewTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.ELAPSED
                    || CreateInterviewTestSession_statusHiddenField.Value.Trim() ==
                    Constants.CandidateAttemptStatus.QUIT)
                {
                    CreateInterviewTestSession_candidateDetailImageButton.Visible = true;
                    CreateInterviewTestSession_cancelCreateTestSessionImageButton.Visible = false;
                }
            }
        }

        /// <summary>
        /// Handler that will trigger on clicking the save button. 
        /// This cancels the candidate test session by updating the reason.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateInterviewTestSession_saveCancellationButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();

                if (ViewState["ATTEMPT_ID"] == null || ViewState["CANDIDATE_SESSION_ID"] == null)
                    return;

                // Check if the reason text box is empty. If it is, show the error message.
                // Otherwise, update the session status as CANCEL.
                if (CreateInterviewTestSession_cancelTestReasonTextBox.Text.Trim().Length == 0)
                {
                    CreateInterviewTestSession_cancelErrorMessageLabel.Text = "Reason cannot be empty";
                    CreateInterviewTestSession_cancelSessionModalPopupExtender.Show();
                }
                else
                {
                    int attemptId = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());
                    string candidateSessionId = ViewState["CANDIDATE_SESSION_ID"].ToString();

                    bool isMailSent = true;

                    new TestConductionBLManager().UpdateSessionStatus(candidateSessionId,
                       attemptId, Constants.CandidateAttemptStatus.CANCELLED,
                       Constants.CandidateSessionStatus.CANCELLED, base.userID, out isMailSent);

                    CandidateTestSessionDetail candidateTestSession = new CandidateTestSessionDetail();

                    candidateTestSession.AttemptID = attemptId;
                    candidateTestSession.CandidateTestSessionID = candidateSessionId;
                    candidateTestSession.CancelReason = CreateInterviewTestSession_cancelTestReasonTextBox.Text.Trim();
                    candidateTestSession.ModifiedBy = base.userID;

                    new TestBLManager().CancelTestSession(candidateTestSession);

                    base.ShowMessage(CreateInterviewTestSession_topSuccessMessageLabel,
                        CreateInterviewTestSession_bottomSuccessMessageLabel,
                        string.Format(Resources.HCMResource.CreateInterviewTestSession_SessionCancelled,
                        candidateSessionId));

                    LoadTestSessions(Convert.ToInt32(ViewState["PAGE_NUMBER"]));
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(CreateInterviewTestSession_topErrorMessageLabel,
                    CreateInterviewTestSession_bottomErrorMessageLabel, ex.Message);
            }
        }

        /// <summary>
        /// Handler that will perform reset/clear the input fields.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CreateInterviewTestSession_resetButton_Click(object sender, EventArgs e)
        {
            CreateInterviewTestSession_sessionDescTextBox.Text = string.Empty;
            CreateInterviewTestSession_instructionsTextBox.Text = string.Empty;
            CreateInterviewTestSession_sessionNoTextBox.Text = "0";
            CreateInterviewTestSession_expiryDateTextBox.Text = string.Empty;
            CreateInterviewTestSession_assessorDetailsGridView.DataSource = null;
            CreateInterviewTestSession_assessorDetailsGridView.DataBind();
            CreateInterviewTestSession_assessorDetailsDiv.Visible = false;
            CreateInterviewTestSession_allowPauseInterviewCheckbox.Checked = false;
            ClearLabelMessage();
        }

        /// <summary>
        /// Handler that performs to redirect to schedule candidate page.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> contains the event data.
        /// </param>
        protected void CreateInterviewTestSession_scheduleCandidateButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/InterviewScheduler/InterviewScheduleCandidate.aspx?m=2&s=1&parentpage=S_TSN"
                + "&interviewsessionid="
                + ViewState["TEST_SESSION_ID"].ToString().Trim(), false);

        }

        /// <summary>
        /// Handler to show the assessor skill matrix popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CreateInterviewTestSession_recommendAssessorLinkButton_Click(object sender, EventArgs e)
        {
            if (Session["ASSESSOR"] == null)
                CreateInterviewTestSession_recommendAssessorLinkButton.Attributes.Add("onclick", "javascript:return SearchInterviewRecommendAssessor('search',null,null,null,null);");
            else
            {
                List<AssessorDetail> slots = Session["ASSESSOR"] as List<AssessorDetail>;
                CreateInterviewTestSession_assessorDetailsGridView.DataSource = Session["ASSESSOR"] as List<AssessorDetail>;
                CreateInterviewTestSession_assessorDetailsGridView.DataBind();
                CreateInterviewTestSession_assessorDetailsDiv.Visible = true;
            }
        }
        #endregion Event Handlers

        #region Public Methods

        /// <summary>
        /// Method that will show pop up extender when download button clicked on user control.
        /// </summary>
        public void ShowModalPopup()
        {
            CreateInterviewTestSession_candidateDetailModalPopupExtender.Show();
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Method that will preview the test session details once the user clicked 
        /// on the save button.
        /// </summary>
        /// <returns>Returns the <see cref="TestSessionDetail"/> object</returns>
        private InterviewSessionDetail PreviewTestSession()
        {
            // Initialize the test session detail object
            InterviewSessionDetail interviewSessionDetail = new InterviewSessionDetail();

            // Get the information from the controls and assign to the object members
            interviewSessionDetail.InterviewTestID = Request.QueryString["interviewtestkey"];
            interviewSessionDetail.InterviewTestName = CreateInterviewTestSession_testNameValueLabel.Text;
            interviewSessionDetail.NumberOfCandidateSessions
                = Convert.ToInt32(CreateInterviewTestSession_sessionNoTextBox.Text);

            interviewSessionDetail.PositionProfileName = CreateInterviewTestSession_positionProfileValueLabel.Text;

            // Set the time to maximum in expiry date.
            interviewSessionDetail.ExpiryDate = (Convert.ToDateTime(CreateInterviewTestSession_expiryDateTextBox.Text)).Add(new TimeSpan(23, 59, 59));

            interviewSessionDetail.InterviewSessionDesc = CreateInterviewTestSession_sessionDescTextBox.Text;
            interviewSessionDetail.Instructions = CreateInterviewTestSession_instructionsTextBox.Text;

            interviewSessionDetail.CreatedBy = base.userID;
            interviewSessionDetail.ModifiedBy = base.userID;

            return interviewSessionDetail;
        }

        /// <summary>
        /// This method gets the seconds values and updated the time format as hh:mm:yyyy
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public string GetTimeFormat(int seconds)
        {
            return Utility.ConvertSecondsToHoursMinutesSeconds(seconds);
        }

        /// <summary>
        /// This method passes the testId and pagennumber to retrieve the candidate session ids.
        /// </summary>
        /// <param name="testID"></param>
        /// <param name="pageNumber"></param>
        private void LoadTestSessions(int pageNumber)
        {
            int totalRecords = 0;
            List<CandidateInterviewSessionDetail> candidateTestSessions =
                new InterviewSessionBLManager().GetCandidateInterviewSessions(CreateInterviewTestSession_TestKeyHiddenField.Value,
                pageNumber, base.GridPageSize, ViewState["SORT_EXPRESSION"].ToString(),
                (SortType)ViewState["SORT_DIRECTION_KEY"], base.userID, out totalRecords);

            // Check if the candidateTestSession object is null, 
            // Then hide the gridview. Otherwise, show the loaded gridview 
            if (candidateTestSessions == null)
                CreateInterviewTestSession_testSessionGridviewTR.Visible = false;
            else
            {
                CreateInterviewTestSession_testSessionGridviewTR.Visible = true;
                CreateInterviewTestSession_testSessionGridView.DataSource = candidateTestSessions;
                CreateInterviewTestSession_testSessionGridView.DataBind();
            }

            // Update page size and total records
            CreateInterviewTestSession_bottomSessionPagingNavigator.PageSize = base.GridPageSize;
            CreateInterviewTestSession_bottomSessionPagingNavigator.TotalRecords = totalRecords;
        }
        /// <summary>
        /// This method gets the all the subject ids related to this interview test
        /// </summary>
        /// <param name="interviewTestKey"></param>
        private void LoadSubjectIds(string interviewTestKey)
        {
            string subjectIds = new InterviewSessionBLManager().GetInterviewTestSubjectIdsByKey(interviewTestKey);
            Session["SESSION_SUBJECTIDS"] = subjectIds;
        }

        /// <summary>
        /// On loading the page, below method displays all the informations about the test.
        /// </summary>
        /// <param name="testID">testID is the key passed in query string</param>
        private void LoadTestDetail(string testID)
        {
            // Get test detail information by calling GetTestDetail method
            TestDetail testDetail = new InterviewBLManager().GetInterviewTestDetail(testID);

            if (testDetail == null)
            {
                base.ShowMessage(CreateInterviewTestSession_bottomErrorMessageLabel,
                    CreateInterviewTestSession_topErrorMessageLabel,
                    Resources.HCMResource.CreateInterviewTestSession_InValidTestKey);
                return;
            }
            Session["SESSION_POSITION_PROFILE_ID"] = testDetail.PositionProfileID;
            // Assign test id to the text field
            CreateInterviewTestSession_testKeyValueLabel.Text = testID;
            CreateInterviewTestSession_testNameValueLabel.Text = testDetail.Name;
            CreateInterviewTestSession_creditValueLabel.Text = "0";
            CreateInterviewTestSession_positionProfileIDHiddenField.Value = testDetail.PositionProfileID.ToString();
            CreateInterviewTestSession_positionProfileValueLabel.Text = testDetail.PositionProfileName;

            // Call load test session method by passing test key and page number
            LoadTestSessions(1);
        }

        /// <summary>
        /// This method returns the TestSessionDetails instance with all the values 
        /// given as input by user.
        /// </summary>
        /// <returns></returns>
        private InterviewSessionDetail ConstructTestSessionDetail()
        {
            // Initialize test session detail object
            InterviewSessionDetail interviewSessionDetail = new InterviewSessionDetail();

            // Check whether a test key is empty or not before assigning to 
            // the testid
            if (Request.QueryString["interviewtestkey"] != null)
                interviewSessionDetail.InterviewTestID = Request.QueryString["interviewtestkey"];

            // Set test name
            interviewSessionDetail.InterviewTestName = CreateInterviewTestSession_testNameValueLabel.Text;

            // Set number of candidate session (session count)
            interviewSessionDetail.NumberOfCandidateSessions =
                Convert.ToInt32(CreateInterviewTestSession_sessionNoTextBox.Text);


            // Set position profile ID.
            if (!Utility.IsNullOrEmpty(CreateInterviewTestSession_positionProfileIDHiddenField.Value))
                interviewSessionDetail.PositionProfileID =
                    Convert.ToInt32(CreateInterviewTestSession_positionProfileIDHiddenField.Value);
            else
                interviewSessionDetail.PositionProfileID = 0;


            // Set expiry date
            interviewSessionDetail.ExpiryDate = Convert.ToDateTime(CreateInterviewTestSession_expiryDateTextBox.Text);

            // Set created by
            interviewSessionDetail.CreatedBy = base.userID;

            // Set modified by
            interviewSessionDetail.ModifiedBy = base.userID;

            // Set test instructions
            interviewSessionDetail.Instructions =
                CreateInterviewTestSession_instructionsTextBox.Text.ToString().Trim();

            // Set session descriptions
            interviewSessionDetail.InterviewSessionDesc =
                CreateInterviewTestSession_sessionDescTextBox.Text.ToString().Trim();

            if (Session["ASSESSOR"] != null)
            {
                List<AssessorDetail> assessorDetails = Session["ASSESSOR"] as List<AssessorDetail>;
                for (int i = 0; i < assessorDetails.Count; i++)
                {
                    interviewSessionDetail.AssessorIDs += assessorDetails[i].UserID + ",";
                }
                interviewSessionDetail.AssessorIDs = interviewSessionDetail.AssessorIDs.ToString().Remove(interviewSessionDetail.AssessorIDs.Length - 1);
            }
          
            
            if (CreateInterviewTestSession_allowPauseInterviewCheckbox.Checked == true)
                interviewSessionDetail.AllowPauseInterview = 'Y';
            else
                interviewSessionDetail.AllowPauseInterview = 'N';

            return interviewSessionDetail;
        }

        /// <summary>
        /// Method that will clear success/error label messages.
        /// </summary>
        private void ClearLabelMessage()
        {
            CreateInterviewTestSession_topSuccessMessageLabel.Text = string.Empty;
            CreateInterviewTestSession_bottomSuccessMessageLabel.Text = string.Empty;
            CreateInterviewTestSession_topErrorMessageLabel.Text = string.Empty;
            CreateInterviewTestSession_bottomErrorMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            // Validate number of session field
            if (Convert.ToInt32(CreateInterviewTestSession_sessionNoTextBox.Text) <= 0 ||
                (Convert.ToInt32(CreateInterviewTestSession_sessionNoTextBox.Text) > 30))
            {
                isValidData = false;
                base.ShowMessage(CreateInterviewTestSession_topErrorMessageLabel,
                    CreateInterviewTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateInterviewTestSession_SessionCountCannotBeZero);
            }

            CreateInterviewTestSession_MaskedEditValidator.Validate();
            if (CreateInterviewTestSession_expiryDateTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(CreateInterviewTestSession_topErrorMessageLabel,
                    CreateInterviewTestSession_bottomErrorMessageLabel,
                    Resources.HCMResource.CreateInterviewTestSession_ExpiryDateCannotBeEmpty);
            }
            else
            {
                // This will check if the date is valid date. i.e. 99/99/9999
                if (!CreateInterviewTestSession_MaskedEditValidator.IsValid)
                {
                    isValidData = false;
                    base.ShowMessage(CreateInterviewTestSession_topErrorMessageLabel,
                    CreateInterviewTestSession_bottomErrorMessageLabel,
                    CreateInterviewTestSession_MaskedEditValidator.ErrorMessage);
                }
                else
                {
                    // Check if the expiry date is less than current date
                    if (DateTime.Parse(CreateInterviewTestSession_expiryDateTextBox.Text) < DateTime.Today)
                    {
                        isValidData = false;
                        base.ShowMessage(CreateInterviewTestSession_topErrorMessageLabel,
                        CreateInterviewTestSession_bottomErrorMessageLabel,
                        Resources.HCMResource.CreateInterviewTestSession_ExpiryDateCannotBeLessThanCurrentDate);
                    }
                }
            }
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Methods

        #region Sort Related Coding

        /// <summary>
        /// This event handler is used to show an image icon on the header column
        /// which is clicked
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> contains the event data.
        /// </param>
        protected void CreateInterviewTestSession_testSessionGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    // Get last clicked column index
                    int sortColumnIndex = GetSortColumnIndex
                        (CreateInterviewTestSession_testSessionGridView,
                        (string)ViewState["SORT_EXPRESSION"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_DIRECTION_KEY"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestSession_topErrorMessageLabel,
                    CreateInterviewTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Here, grid view data will be re-arranged either ascending or descending order
        /// At the same time, pagination and grid will be reloaded
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the send of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        protected void CreateInterviewTestSession_testSessionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                // Assign sort expression column to the viewstate
                if (ViewState["SORT_EXPRESSION"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_DIRECTION_KEY"] =
                        ((SortType)ViewState["SORT_DIRECTION_KEY"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                {
                    ViewState["SORT_DIRECTION_KEY"] = SortType.Ascending;
                }

                ViewState["SORT_EXPRESSION"] = e.SortExpression;

                // Reload the pagination and gridview controls
                CreateInterviewTestSession_bottomSessionPagingNavigator.Reset();
                LoadTestSessions(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateInterviewTestSession_topErrorMessageLabel,
                    CreateInterviewTestSession_bottomErrorMessageLabel, exp.Message);
            }
        }
        #endregion Sort Related Coding
       
}
}