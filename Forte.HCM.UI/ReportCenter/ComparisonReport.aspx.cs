﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ComparisonReport.cs
// File that represents the For selected candidate Reports based on the test.
// This will helps take print and download as a PDF file.
//

#endregion

#region Directives                                                             

using System;
using System.Threading;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Kalitte;
using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;

#endregion
namespace Forte.HCM.UI.ReportCenter
{
    /// <summary>                                                  
    /// Class that represents the user interface layout and functionalities
    /// for the ComparisonReport page. This page helps view various report for multiple 
    /// Candidate against the Test
    /// This class inherits the DashboardPage class.
    /// </summary>
    public partial class ComparisonReport : DashboardPage
    {
        #region Even Handling                                                  

        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(Session["CANDIDATEDETAIL"]))
                {
                    Response.Redirect("TestStatisticsInfo.aspx?m=3&s=0&testkey=" +
                        Request.QueryString["testkey"] + "&parentpage=" + Constants.ParentPage.TEST_REPORT 
                        + "&tab=CS&session=expired", false);
                }
                if (!IsPostBack)
                {
                    new DashBoardBLManager().DeleteWidgetInstance(Thread.CurrentPrincipal.Identity.Name);
                    Surface1.DashboardKey = Request.QueryString["dashboardkey"];
                    ComparisonReport_widgetTypesListControl.WidgetTypeDataSource = 
                        new DashBoardBLManager().GetWidgetTypes(
                        Constants.DashboardConstants.COMPARISON_REPORT+",All");
                }
                Session["PRINTER"] = false;
                Master.SetPageCaption(Resources.HCMResource.ComparisonReport_Title);
                
                base.OnInit(e);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ComparisonReport_topErrorMessageLabel,
                ComparisonReport_bottomErrorMessageLabel, exp.Message);
            }

        }

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                ComparisonReport_fileNameHiddenField.Value = Request.QueryString["filename"] + "-" + Request.QueryString["testkey"] + ".pdf";

        }

        #region Widget Related Events                                          

        /// <summary>
        /// Get the Dashboard 
        /// </summary>
        protected override DashboardSurface Dashboard
        {
            get { return Surface1; }
        }
        /// <summary>
        /// Widget Properties Setting.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="WidgetEventArgs"/> that holds the event data.
        /// </param>
        protected void surface_WidgetPropertiesSetting(object sender, WidgetEventArgs e)
        {
        }

        /// <summary>
        /// Widget Type Menu Prepare.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="WidgetTypeMenuPrepareEventArgs"/> that holds the event data.
        /// </param>
        protected void surface_WidgetTypeMenuPrepare(object sender, WidgetTypeMenuPrepareEventArgs e)
        {
        }

        /// <summary>
        /// Dashboard Bottombar Prepare.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="DashboardToolbarPrepareEventArgs"/> that holds the event data.
        /// </param>
        protected void surface_DashboardBottombarPrepare(object sender, DashboardToolbarPrepareEventArgs e)
        {
        }

        #endregion

        /// <summary>
        ///  Override the OnPreRender method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                base.OnPreRender(e);
                Surface1.HideEditor();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ComparisonReport_topErrorMessageLabel,
                ComparisonReport_bottomErrorMessageLabel, exp.Message);
            }
        }
      
        /// <summary>
        /// Event Handling for the addWidgetInstance.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="RepeaterCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Add Widget insance based on the Widget type id.
        /// </remarks>
        protected void ComparisonReport_addWidgetInstanceClick(object sender, 
            RepeaterCommandEventArgs e)
        {
            try
            {
                string columnKey = "0";
                if (Utility.IsNullOrEmpty(Session["COMPARISON_COLUMNKEY"]))
                {
                    columnKey = "1";
                    Session["COMPARISON_COLUMNKEY"] = columnKey;
                }
                else
                {
                    columnKey = Session["COMPARISON_COLUMNKEY"] as string;
                    if (columnKey == "0")
                        columnKey = "1";
                    else
                        columnKey = "0";

                    Session["COMPARISON_COLUMNKEY"] = columnKey;
                }
                WidgetType widgetType = DashboardFramework.GetWidgetType(
                    e.CommandArgument.ToString().ToLower());
                WidgetInstance widgetInstance = widgetType.NewWidget(Guid.NewGuid(),
                    Request.QueryString["dashboardsectionkey"], columnKey); // instance from type
                Surface1.CreateWidget(widgetInstance, 
                    new DashboardSurface().DefaultWidgetAddMode, true); // register widget instance with provider
            }
            catch (Exception exp)
            {
                if (!exp.Message.Contains("aborted"))
                {
                    Logger.ExceptionLog(exp);
                    base.ShowMessage(ComparisonReport_topErrorMessageLabel,
                    ComparisonReport_bottomErrorMessageLabel, exp.Message);
                }
            }
        }
        
        /// <summary>
        /// Handler method that will be called when the Cancel button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Redirect to previous page.
        /// </remarks>
        protected void ComparisonReport_resetLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Handler method that will be called when the Cancel button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Redirect to previous page.
        /// </remarks>
        protected void ComparisonReport_cancelLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("TestStatisticsInfo.aspx?m=3&s=0&testkey=" +
                Request.QueryString["testkey"] + "&parentpage=" + Constants.ParentPage.TEST_REPORT + "&tab=CS", false);
        }
       
        /// <summary>
        /// Handler method that will be called when the Download button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void ComparisonReport_downloadButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.AppendHeader("Content-Disposition", "attachment; filename=" +
                    ComparisonReport_fileNameHiddenField.Value);
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(GeneratePDF());
                Response.End();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ComparisonReport_topErrorMessageLabel,
                ComparisonReport_bottomErrorMessageLabel, exp.Message);
            }
        }
       
        /// <summary>
        /// Handler method that will be called when the Email button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void ComparisonReport_emailButton_Click(object sender, EventArgs e)
        {
            try
            {
                Session[Constants.SessionConstants.REPORT_RESULTS_PDF] = GeneratePDF();
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), " ", "javascript:OpenEmailAttachmentAndCloseConfirmPopUp('" + ComparisonReport_fileNameHiddenField.Value + "')", true);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ComparisonReport_topErrorMessageLabel,
                ComparisonReport_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the Download button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void ComparisonReport_printerFriendlyLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                printPage();
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(),
                   " ", "javascript:DashBoardPreview('" + Request.QueryString["dashboardkey"] + "')", true);
                //Response.Redirect("DesignReportPrint.aspx?dashboardkey=" + Request.QueryString["dashboardkey"], false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ComparisonReport_topErrorMessageLabel,
                ComparisonReport_bottomErrorMessageLabel, exp.Message);
            }
        }
        #endregion

        #region Private Methods                                                
        /// <summary>
        /// Function which will create pdf document.
        /// </summary>
        /// <returns></returns>
        private byte[] GeneratePDF()
        {
            List<Dictionary<object, object>> dictionaryList = new List<Dictionary<object, object>>();

            for (int i = 0; i < Surface1.GetWidgets().Count; i++)
            {
                Dictionary<object, object> dictionary = new Dictionary<object, object>();

                if (Surface1.GetWidgets()[i].WidgetControl.FindControl("WidgetMultiSelectControl") != null)
                {
                    CheckBoxList checkBoxList =
                        (CheckBoxList)Surface1.GetWidgets()[i].WidgetControl.FindControl(
                        "WidgetMultiSelectControl").FindControl(
                        "WidgetMultiSelectControl_propertyCheckBoxList");

                    if (checkBoxList != null)
                    {
                        dictionary.Add(Surface1.GetWidgets()[i].Instance.Title, "");
                        for (int j = 0; j < checkBoxList.Items.Count; j++)
                        {
                            if (checkBoxList.Items[j].Selected)
                                dictionary.Add(checkBoxList.Items[j].Text, checkBoxList.Items[j].Value);
                        }
                    }
                }
                dictionaryList.Add(dictionary);
            }
            WidgetReport widgetReport = new WidgetReport();
            return widgetReport.GetPDFReportByteArray("", Server.MapPath("../chart/"), Request.QueryString["testkey"], (CandidateReportDetail)Session["CANDIDATEDETAIL"], dictionaryList);
        }

        private void printPage()
        {
            List<Dictionary<object, object>> dictionaryList = new List<Dictionary<object, object>>();
            for (int i = 0; i < Surface1.GetWidgets().Count; i++)
            {
                Dictionary<object, object> dictionary = new Dictionary<object, object>();
                if (Surface1.GetWidgets()[i].WidgetControl.FindControl("WidgetMultiSelectControlPrint") != null)
                {
                    TextBox textBox = (TextBox)Surface1.GetWidgets()[i].WidgetControl.FindControl(
                        "WidgetMultiSelectControlPrint").FindControl(
                        "WidgetMultiSelectControl_propertyTextBox");
                    dictionary.Add("", textBox.Text);
                }
                dictionaryList.Add(dictionary);
            }
            Session["DICTINARYLIST"] = dictionaryList;
        }
        #endregion
    }
}
