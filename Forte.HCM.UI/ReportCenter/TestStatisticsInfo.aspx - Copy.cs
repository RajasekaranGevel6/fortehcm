﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestStatisticsInfo.cs
// File that represents the user interface for the 
// test statistics infor page

#endregion

#region Directives                                                             

using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

using Kalitte.Dashboard.Framework.Types;
using Kalitte.Dashboard.Framework;
using System.Threading;
using System.Web.UI;
using System.Data;
using OfficeOpenXml;
using System.Web;

#endregion Directives

namespace Forte.HCM.UI.ReportCenter
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities 
    /// for the question settings page. This page helps to view the 
    /// test statistics information of the test.
    /// </summary>
    public partial class TestStatisticsInfo : PageBase
    {
        #region PrivateVariables                                               

        const string SELECTED_CANDIDATE_ID = "selectedCandidateId";
        const string SELECTED_CANDIDATE_DETAILS = "selectedCandidateDetails";
        string onmouseoverStyle = "className='grid_normal_row'";
        string onmouseoutStyle = "className='grid_alternate_row'";
        DashBoard dashBoard;
        CandidateReportDetail candidateReportDetail = null;
        UserDetail userDetail = null;
        #endregion PrivateVariables

        #region Constants                                                      
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Constants

        #region Event Handlers                                                 
        /// <summary>
        /// Hanlder method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckAndSetExpandorRestore();
                TestStatisticsInfo_bottonErrorLabel.Text = string.Empty;
                TestStatisticsInfo_topErrorMessageLabel.Text = string.Empty;
                TestStatisticsInfo_candidateTabPanelErrorLabel.Text = string.Empty;
                Master.SetPageCaption("Test Statistics Information");
                TestStatisticsInfo_candidatesGridViewTestKeyHiddenField.Value = string.Empty;
                string testID = Request.QueryString["testkey"];
                TestStatisticsInfo_candidatesGridViewTestKeyHiddenField.Value = testID;
                if (!IsPostBack)
                {
                    ViewState[SELECTED_CANDIDATE_DETAILS] = null;
                    TestStatisticsInfo_selectedcanidates.Value = null;
                    if (Request.QueryString["tab"] != null && Request.QueryString["tab"] == "CS")
                    {
                        // Activate the candidate statistics tab.
                        TestStatisticsInfo_mainTabContainer.ActiveTab = TestStatisticsInfo_candidateTabPanel;
                        if (Request.QueryString["session"] != null && Request.QueryString["session"] == "expired")
                        {
                            ShowMessage(TestStatisticsInfo_topErrorMessageLabel, TestStatisticsInfo_bottonErrorLabel,
                                "Candidate Details Sessions are Expired");
                        }
                    }
                    else
                    {
                        base.ClearSearchTestStaticticsCriteriaSession();
                    }
                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_STATISTICS] != null)
                        FillSearchCriteria((TestStatisticsSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_STATISTICS]);
                   
                    CheckAndSetExpandorRestore();
                    dashBoard = GetDashBoard("");
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dashBoard))
                    {
                        new DashBoardBLManager().DeleteWidgetInstance(Thread.CurrentPrincipal.Identity.Name);
                    }

                    //Take the id  and name from query string and assign it to the 
                    //textbox
                    TestStatisticsInfo_testIdLabel.Text = Request.QueryString["testkey"];

                    string testName = null;

                    testName = new ReportBLManager().
                        GetTestName(Request.QueryString["testkey"]);

                    TestStatisticsInfo_testNameLabel.Text = testName;

                    ViewState["QuestionType"] = null;
                    //set visibility for question statistics table based on question type
                    if (Request.QueryString["Questiontype"] != null && Request.QueryString["Questiontype"] == "1")
                    {
                        ViewState["QuestionType"] = Request.QueryString["Questiontype"];
                        TestStatisticsInfo_questionStatisticsUpdatePanel_OpenTextTable.Visible = false;
                            //.Attributes.Add("style", "display: none;");
                        TestStatisticsInfo_questionStatisticsUpdatePanel_MultipleChoiceTable.Visible = true;
                    }
                    else if (Request.QueryString["Questiontype"] != null && Request.QueryString["Questiontype"] == "2")
                    {
                        ViewState["QuestionType"] = Request.QueryString["Questiontype"];
                        TestStatisticsInfo_questionStatisticsUpdatePanel_OpenTextTable.Visible = true;
                        TestStatisticsInfo_questionStatisticsUpdatePanel_MultipleChoiceTable.Visible = false;
                    }
                    else
                    {
                        TestStatisticsInfo_questionStatisticsUpdatePanel_OpenTextTable.Visible = false;
                        TestStatisticsInfo_questionStatisticsUpdatePanel_MultipleChoiceTable.Visible = true;
                    }

                    //Assign the sort order for the question statistics grid view
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Descending;

                    //Assign the sort expression for the question statistics grid view
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["QuestionSortExpression"]))
                        ViewState["QuestionSortExpression"] = "Question";

                    List<QuestionStatisticsDetail> QuestionStatisticsDataSoure
                        = new List<QuestionStatisticsDetail>();

                    //Get the question details from DB
                    QuestionStatisticsDataSoure = new ReportBLManager().
                        GetQuestionStatistics(testID);

                    //Assign the data to the cache
                    Cache["SearchDataTables"] = QuestionStatisticsDataSoure;


                    //Assign the sort order for the candidate statistics grid view
                    ViewState["CANDIDATE_SORT_ORDER"] = SortType.Descending;

                    //Assign the sort expression  for the candidate statistics grid view
                    ViewState["CANDIDATE_SORT_EXPRESSION"] = "DATE";

                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["CANDIDATE_PAGE_NO"]))
                        ViewState["CANDIDATE_PAGE_NO"] = "1";

                    LoadCandidateDetails();
                    // Check if page is redirected from any child page. If the page
                    // if redirected from any child page, fill the search criteria
                    // and apply the search.

                }
                else
                {
                    ViewState[SELECTED_CANDIDATE_DETAILS] = SellectedCanidateList();
                }

                //Sort the questions gridview 
                SortGridView("Question", "Ascending");

                //Get the general test statistics info for the test id
                TestStatisticsInfo_generalTestStatisticsControl.TestStatisticsDataSource =
                    new ReportBLManager().GetTestSummaryDetails(testID);

                //Get the general test statistics and load the 
                //test summary details
                TestStatisticsInfo_generalTestStatisticsControl.LoadTestSummaryDetails();



                TestStatisticsInfo_candidateTabPanel_searchCandidateResultsTR.Attributes.Add("onclick",
                  "ExpandOrRestore('" +
                  TestStatisticsInfo_candidateTabPanel_questionDiv.ClientID + "','" +
                  TestStatisticsInfo_candidateTabPanel_searchCriteriasDiv.ClientID + "','" +
                  TestStatisticsInfo_candidateTabPanel_searchResultsUpSpan.ClientID + "','" +
                  TestStatisticsInfo_candidateTabPanel_searchResultsDownSpan.ClientID + "','" +
                  TestStatisticsInfo_heightHiddenField.ClientID + "','" +
                  RESTORED_HEIGHT + "','" +
                  EXPANDED_HEIGHT + "')");


                //Subbscribe for the page number click event
                TestStatisticsInfo_candidateTabPanel_bottomPagingNavigator.PageNumberClick += new
                PageNavigator.PageNumberClickEventHandler
                (TestStatisticsInfo_candidateTabPanel_pagingNavigator_PageNumberClick);


                foreach (MultiHandleSliderTarget target in TestStatisticsInfo_candidateTabPanel_testCostMultiHandleSliderExtender.MultiHandleSliderTargets)
                    target.ControlID = TestStatisticsInfo_candidateTabPanel_testCostMultiHandleSliderExtender.Parent.FindControl(target.ControlID).ClientID;

                foreach (MultiHandleSliderTarget target in TestStatisticsInfo_candidateTabPanel_relativeScoreMultiHandleSliderExtender.MultiHandleSliderTargets)
                    target.ControlID = TestStatisticsInfo_candidateTabPanel_relativeScoreMultiHandleSliderExtender.Parent.FindControl(target.ControlID).ClientID;

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);
            }
        }

        private void CheckAndSetExpandorRestore()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TestStatisticsInfo_heightHiddenField.Value) &&
               TestStatisticsInfo_heightHiddenField.Value == "Y")
            {
                TestStatisticsInfo_candidateTabPanel_searchCriteriasDiv.Style["display"] = "none";
                TestStatisticsInfo_candidateTabPanel_searchResultsUpSpan.Style["display"] = "block";
                TestStatisticsInfo_candidateTabPanel_searchResultsDownSpan.Style["display"] = "none";
                TestStatisticsInfo_candidateTabPanel_questionDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                TestStatisticsInfo_candidateTabPanel_searchCriteriasDiv.Style["display"] = "block";
                TestStatisticsInfo_candidateTabPanel_searchResultsUpSpan.Style["display"] = "none";
                TestStatisticsInfo_candidateTabPanel_searchResultsDownSpan.Style["display"] = "block";
                TestStatisticsInfo_candidateTabPanel_questionDiv.Style["height"] = RESTORED_HEIGHT;
            }

            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_STATISTICS]))
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TestStatisticsInfo_heightHiddenField.Value))
                    ((TestStatisticsSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_STATISTICS]).IsMaximized =
                        TestStatisticsInfo_heightHiddenField.Value == "Y" ? true : false;
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void TestStatisticsInfo_candidateTabPanel_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_STATISTICS] = null;

                Response.Redirect("~/ReportCenter/TestStatisticsInfo.aspx?m=3&s=0&tab=CS&testkey="
                    + Request.QueryString["testkey"].ToString(), false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void TestStatisticsInfo_candidateTabPanel_comparisonReportLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                SetSelectedCandidate();
                List<CandidateReportDetail> candidateReportDetailList = new List<CandidateReportDetail>();
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState[SELECTED_CANDIDATE_DETAILS]))
                    candidateReportDetailList = (List<CandidateReportDetail>)ViewState[SELECTED_CANDIDATE_DETAILS];
                if (candidateReportDetailList.Count > 5)
                {
                    ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel,
                    Resources.HCMResource.TestStatisticsInfo_CMPSelectMoreThenOneCandidate);
                    return;
                }

                candidateReportDetail = new CandidateReportDetail();
                candidateReportDetail = IsValidComparison();
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(candidateReportDetail.AttemptsID))
                {
                    if (candidateReportDetail.AttemptsID.Contains(','))
                    {
                        dashBoard = GetDashBoard(Constants.DashboardConstants.COMPARISON_REPORT);
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(dashBoard))
                        {
                            CreateDashBoard(Constants.DashboardConstants.COMPARISON_REPORT);
                            dashBoard = GetDashBoard(Constants.DashboardConstants.COMPARISON_REPORT);
                        }
                        userDetail = new UserDetail();
                        userDetail = (UserDetail)Session["USER_DETAIL"];
                        candidateReportDetail.TestKey = Request.QueryString["testkey"];
                        candidateReportDetail.TestName = TestStatisticsInfo_testNameLabel.Text;
                        candidateReportDetail.LoginUser = userDetail.FirstName + "," + userDetail.LastName;
                        candidateReportDetail.CandidateName = "COMPARISON REPORT";
                        Session["CANDIDATEDETAIL"] = candidateReportDetail;
                        TestStatisticsSearchCriteria testStatisticsSearchCriteria = Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_STATISTICS] as TestStatisticsSearchCriteria;
                        Response.Redirect("~/ReportCenter/ComparisonReport.aspx?m=3&s=0" +
                            "&dashboardkey=" + dashBoard.DashBoardID +
                            "&dashboardsectionkey=" + dashBoard.DashBoardSectionID +
                            "&testkey=" + Request.QueryString["testkey"] +
                            "&filename=" + testStatisticsSearchCriteria.SellectedCandidateDetails.Replace(',', '-').Replace(' ', '-').Replace(':','-') +
                            "&parentpage=" + Constants.ParentPage.TEST_STATISTICS_INFO +
                            "&sourcepage=" + Request.QueryString["parentpage"], false);
                    }
                    else
                    {
                        ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                        TestStatisticsInfo_bottonErrorLabel,
                        Resources.HCMResource.TestStatisticsInfo_CMPSelectMoreThenOneCandidate);
                    }
                }
                else
                {
                    ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel,
                    Resources.HCMResource.TestStatisticsInfo_CMPSelectMoreThenOneCandidate);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void TestStatisticsInfo_candidateTabPanel_groupAnalysisReportLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                candidateReportDetail = new CandidateReportDetail();
                candidateReportDetail = IsValidComparison();

                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(candidateReportDetail.AttemptsID))
                {
                    if (candidateReportDetail.AttemptsID.Contains(','))
                    {
                        SetSelectedCandidate();
                        List<CandidateReportDetail> candidateReportDetailList = new List<CandidateReportDetail>();
                        if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState[SELECTED_CANDIDATE_DETAILS]))
                            candidateReportDetailList = (List<CandidateReportDetail>)ViewState[SELECTED_CANDIDATE_DETAILS];
                        if (candidateReportDetailList.Count > 5)
                        {
                            ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                            TestStatisticsInfo_bottonErrorLabel,
                            Resources.HCMResource.TestStatisticsInfo_GASelectTwoCandidates);
                            return;
                        }


                        dashBoard = GetDashBoard(Constants.DashboardConstants.GROUP_REPORT);
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(dashBoard))
                        {
                            CreateDashBoard(Constants.DashboardConstants.GROUP_REPORT);
                            dashBoard = GetDashBoard(Constants.DashboardConstants.GROUP_REPORT);
                        }
                        userDetail = new UserDetail();
                        userDetail = (UserDetail)Session["USER_DETAIL"];
                        candidateReportDetail.TestKey = Request.QueryString["testkey"];
                        candidateReportDetail.TestName = TestStatisticsInfo_testNameLabel.Text;
                        candidateReportDetail.LoginUser = userDetail.FirstName + "," + userDetail.LastName;
                        candidateReportDetail.CandidateName = "GROUP ANALYSIS REPORT";
                        Session["CANDIDATEDETAIL"] = candidateReportDetail;
                        TestStatisticsSearchCriteria testStatisticsSearchCriteria = Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_STATISTICS] as TestStatisticsSearchCriteria;
                        Response.Redirect("~/ReportCenter/GroupAnalysisReport.aspx?m=3&s=0" +
                            "&dashboardkey=" + dashBoard.DashBoardID +
                            "&dashboardsectionkey=" + dashBoard.DashBoardSectionID +
                            "&testkey=" + Request.QueryString["testkey"] +
                            "&filename=" + testStatisticsSearchCriteria.SellectedCandidateDetails.Replace(',', '-').Replace(' ', '-').Replace(':','-') +
                            "&parentpage=" + Constants.ParentPage.TEST_STATISTICS_INFO + 
                            "&sourcepage=" + Request.QueryString["parentpage"], false);
                    }
                    else
                    {
                        ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                        TestStatisticsInfo_bottonErrorLabel,
                        Resources.HCMResource.TestStatisticsInfo_GASelectTwoCandidates);
                    }
                }
                else
                {
                    ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel,
                    Resources.HCMResource.TestStatisticsInfo_GASelectTwoCandidates);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the export to excel link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void TestStatisticsInfo_candidateTabPanel_exportToExcelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable rawTable = GetExportCandidateDetails();

                // Check if table is not null and contain rows.
                if (rawTable == null || rawTable.Rows.Count == 0)
                {
                    return;
                }

                // Remove total rows.
                DataView dv = new DataView(rawTable);
                dv.RowFilter = "TOTAL IS NULL";
                DataTable table = dv.ToTable();

                // Remove the unncessary columns from the table.
                table.Columns.Remove("ROWNUMBER");
                table.Columns.Remove("IS_CYBER_PROCTORING");
                table.Columns.Remove("CANDIDATE_ID");
                table.Columns.Remove("CANDIDATE_NAME");
                table.Columns.Remove("SESSION_CREATOR");
                table.Columns.Remove("SCHEDULE_CREATOR");
                table.Columns.Remove("RECRUITER");
                table.Columns.Remove("TOTAL");

                // Change the column names.
                table.Columns["CANDIDATE_SESSION_KEY"].ColumnName = "Candidate Session Key";
                table.Columns["CANDIDATE_ATTEMPT_ID"].ColumnName = "Attempt";
                table.Columns["CANDIDATE_FULL_NAME"].ColumnName = "Candidate Name";
                table.Columns["SESSION_CREATOR_FULLNAME"].ColumnName = "Session Created By";
                table.Columns["SCHEDULE_CREATOR_FULLNAME"].ColumnName = "Scheduled By";
                table.Columns["DATE_COMPLETED"].ColumnName = "Test Date";
                table.Columns["ANSWERED_CORRECT_PERCENTAGE"].ColumnName = "Correct Answer %";
                table.Columns["TIME_TAKEN"].ColumnName = "Time Taken (Seconds)";
                table.Columns["AVERAGE_TIME_TAKEN"].ColumnName = "Average Time Taken (Seconds)";
                table.Columns["ABSOLUTE_SCORE_ANSWER"].ColumnName = "Absolute Score";
                table.Columns["RELATIVE_SCORE"].ColumnName = "Relative Score";
                table.Columns["PERCENTILE"].ColumnName = "Percentile";
                table.Columns["RANK_ORDER"].ColumnName = "Rank";
                table.Columns["RELATIVE_RANK_ORDER"].ColumnName = "Relative Rank";

                using (ExcelPackage pck = new ExcelPackage())
                {
                    // Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add(Constants.ExcelExportSheetName.CANDIDATE_DETAILS);

                    // Load the datatable into the sheet, starting from the 
                    // cell A1 and print the column names on row 1.
                    ws.Cells[Constants.ExcelExport.STARTING_CELL].LoadFromDataTable(table, true);

                    string decimalFormat = Constants.ExcelExportFormats.DECIMAL_COMMA;

                    foreach (DataColumn column in table.Columns)
                    {
                        if (column.DataType == typeof(System.DateTime))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                            {
                                col.Style.Numberformat.Format = Constants.ExcelExportFormats.DATE;
                            }
                        }
                        else if (column.DataType == typeof(decimal))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                            {
                                if (decimalFormat == "C")
                                {
                                    /*for (int row = 2; row <= ws.Dimension.End.Row; row++)
                                    {
                                        ws.Cells[row, column.Ordinal + 1].Value =
                                            ws.Cells[row, column.Ordinal + 1].Value != null ? ws.Cells[row, column.Ordinal + 1].Value.ToString().Replace(".", ",") : string.Empty;
                                    }*/
                                    col.Style.Numberformat.Format = "0.00";
                                }
                                else
                                    col.Style.Numberformat.Format = "###0.0000";
                            }
                        }
                    }

                    // Construct file name.
                    string fileName = string.Format("{0}_{1}.xlsx",
                        Constants.ExcelExportFileName.CANDIDATE_DETAILS, Request.QueryString["testkey"]);

                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                    Response.BinaryWrite(pck.GetAsByteArray());
                    //Response.End();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    //Response.End();
                    Response.Flush();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);
            }
        }

        private void SetSelectedCandidate()
        {
            TestStatisticsSearchCriteria testStatisticsSearchCriteria = new TestStatisticsSearchCriteria();
            testStatisticsSearchCriteria = (TestStatisticsSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_STATISTICS];
            testStatisticsSearchCriteria.SellectedCandidateDetails = TestStatisticsInfo_selectedcanidates.Value;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_STATISTICS] = testStatisticsSearchCriteria;
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void TestStatisticsInfo_candidateTabPanel_CandidateReportLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                dashBoard = GetDashBoard(Constants.DashboardConstants.CANDIDATE_REPORT);

                if (Forte.HCM.Support.Utility.IsNullOrEmpty(dashBoard))
                {
                    CreateDashBoard(Constants.DashboardConstants.CANDIDATE_REPORT);
                    dashBoard = GetDashBoard(Constants.DashboardConstants.CANDIDATE_REPORT);
                }

                candidateReportDetail = new CandidateReportDetail();
                candidateReportDetail.TestKey = Request.QueryString["testkey"];
                candidateReportDetail.CandidateID = 1;
                Session["CANDIDATEDETAIL"] = candidateReportDetail;

                Response.Redirect("~/ReportCenter/CandidateComparisonReport.aspx?m=3&s=1" +
                    "&dashboardkey=" + dashBoard.DashBoardID +
                    "&dashboardsectionkey=" + dashBoard.DashBoardSectionID +
                    "&parentpage=" + Constants.ParentPage.TEST_STATISTICS_INFO +
                    "&sourcepage=" + Request.QueryString["parentpage"], false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);
            }
        }

        #region QuestionTab

        /// <summary>
        /// Hanlder method that will be called when the row is created in the grid
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void TestStatisticsInfo_questionStatisticsGridView_RowCreated
        (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex(TestStatisticsInfo_questionStatisticsGridView,
                    (string)ViewState["QuestionSortExpression"]);
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Hanlder method that will be called when the row is created in the grid
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void TestStatisticsInfo_questionStatisticsGridView_OpenText_RowCreated
      (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex(TestStatisticsInfo_questionStatisticsGridView_OpenText,
                    (string)ViewState["QuestionSortExpression"]);
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row is 
        /// binded in the grid view 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void TestStatisticsInfo_questionStatisticsGridView_RowDataBound
         (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    QuestionDetail questionDetail = new QuestionDetail();

                    LinkButton TestStatisticsInfo_questionStatisticsGridView_questionGridLinkButton =
                        (LinkButton)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_questionGridLinkButton");

                    LinkButton TestStatisticsInfo_questionStatisticsGridView_questionIdGridLinkButton =
                     (LinkButton)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_questionIdGridLinkButton");

                    HtmlContainerControl TestStatisticsInfo_questionStatisticsGridView_detailsDiv =
                        (HtmlContainerControl)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_detailsDiv");

                    HtmlAnchor TestStatisticsInfo_focusDownLink =
                        (HtmlAnchor)e.Row.FindControl("TestStatisticsInfo_focusDownLink");

                    questionDetail = new ReportBLManager().GetSingleQuestionDetails(
                        Request.QueryString["testkey"],
                        TestStatisticsInfo_questionStatisticsGridView_questionIdGridLinkButton.Text.ToString());

                    TestStatisticsInfo_questionStatisticsGridView_questionGridLinkButton.Text =
                        TrimContent(questionDetail.Question, 50);

                    ((QuestionDetail)(e.Row.DataItem)).Question = questionDetail.Question;

                    TestStatisticsInfo_questionStatisticsGridView_questionGridLinkButton.Attributes.Add
                        ("onclick", "return TestHideDetails('" +
                    TestStatisticsInfo_questionStatisticsGridView_detailsDiv.ClientID + "','" +
                    TestStatisticsInfo_focusDownLink.ClientID + "')");

                    Label TestStatisticsInfo_questionStatisticsGridView_questionLabel =
                       (Label)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_questionLabel");

                    TestStatisticsInfo_questionStatisticsGridView_questionLabel.Text =
                        questionDetail.Question == null ? questionDetail.Question : questionDetail.Question.ToString().Replace(Environment.NewLine, "<br />");

                    Label TestStatisticsInfo_questionStatisticsGridView_categoryValueLabel =
                       (Label)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_categoryValueLabel");

                    TestStatisticsInfo_questionStatisticsGridView_categoryValueLabel.Text = questionDetail.CategoryName;

                    Label TestStatisticsInfo_questionStatisticsGridView_subjectValueLabel =
                       (Label)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_subjectValueLabel");

                    TestStatisticsInfo_questionStatisticsGridView_subjectValueLabel.Text = questionDetail.SubjectName;

                    Label TestStatisticsInfo_questionStatisticsGridView_testAreaValueLabel =
                       (Label)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_testAreaValueLabel");

                    TestStatisticsInfo_questionStatisticsGridView_testAreaValueLabel.Text = questionDetail.TestAreaName;

                    Label TestStatisticsInfo_questionStatisticsGridView_complexityValueLabel =
                       (Label)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_complexityValueLabel");

                    TestStatisticsInfo_questionStatisticsGridView_complexityValueLabel.Text = questionDetail.ComplexityName;

                    Label TestStatisticsInfo_questionStatisticsGridView_testAuthorNameLabel =
                   (Label)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_testAuthorNameLabel");

                    TestStatisticsInfo_questionStatisticsGridView_testAuthorNameLabel.Text = questionDetail.AuthorName;
                    TestStatisticsInfo_questionStatisticsGridView_testAuthorNameLabel.ToolTip = questionDetail.QuestionAuthorFullName;

                    PlaceHolder SearchQuestion_answerChoicesPlaceHolder =
                        (PlaceHolder)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_answerChoicesPlaceHolder");

                    SearchQuestion_answerChoicesPlaceHolder.Controls.Add
                                        (GetAnswerChoices(questionDetail.AnswerChoices));

                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
                }
            }
            catch (Exception exception)
            {

                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);
            }
        }



        /// <summary>
        /// Handler method that will be called when the row is 
        /// binded in the grid view 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void TestStatisticsInfo_questionStatisticsGridView_OpenText_RowDataBound
         (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    QuestionDetail questionDetail = new QuestionDetail();

                    LinkButton TestStatisticsInfo_questionStatisticsGridView_questionGridLinkButton =
                        (LinkButton)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_questionGridLinkButton_OpenText");

                    LinkButton TestStatisticsInfo_questionStatisticsGridView_questionIdGridLinkButton =
                     (LinkButton)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_questionIdGridLinkButton_OpenText");

                    HtmlContainerControl TestStatisticsInfo_questionStatisticsGridView_detailsDiv =
                        (HtmlContainerControl)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_detailsDiv_OpenText");

                    HtmlAnchor TestStatisticsInfo_focusDownLink =
                        (HtmlAnchor)e.Row.FindControl("TestStatisticsInfo_focusDownLink_OpenText");

                    questionDetail = new ReportBLManager().GetSingleQuestionDetailsOpenText(
                        Request.QueryString["testkey"],
                        TestStatisticsInfo_questionStatisticsGridView_questionIdGridLinkButton.Text.ToString());

                    TestStatisticsInfo_questionStatisticsGridView_questionGridLinkButton.Text =
                        TrimContent(questionDetail.Question, 50);

                    ((QuestionDetail)(e.Row.DataItem)).Question = questionDetail.Question;

                    TestStatisticsInfo_questionStatisticsGridView_questionGridLinkButton.Attributes.Add
                        ("onclick", "return TestHideDetails('" +
                    TestStatisticsInfo_questionStatisticsGridView_detailsDiv.ClientID + "','" +
                    TestStatisticsInfo_focusDownLink.ClientID + "')");

                    Label TestStatisticsInfo_questionStatisticsGridView_questionLabel =
                       (Label)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_questionLabel_OpenText");

                    TestStatisticsInfo_questionStatisticsGridView_questionLabel.Text =
                        questionDetail.Question == null ? questionDetail.Question : questionDetail.Question.ToString().Replace(Environment.NewLine, "<br />");

                    Label TestStatisticsInfo_questionStatisticsGridView_categoryValueLabel =
                       (Label)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_categoryValueLabel_OpenText");

                    TestStatisticsInfo_questionStatisticsGridView_categoryValueLabel.Text = questionDetail.CategoryName;

                    Label TestStatisticsInfo_questionStatisticsGridView_subjectValueLabel =
                       (Label)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_subjectValueLabel_OpenText");

                    TestStatisticsInfo_questionStatisticsGridView_subjectValueLabel.Text = questionDetail.SubjectName;

                    Label TestStatisticsInfo_questionStatisticsGridView_testAreaValueLabel =
                       (Label)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_testAreaValueLabel_OpenText");

                    TestStatisticsInfo_questionStatisticsGridView_testAreaValueLabel.Text = questionDetail.TestAreaName;

                    Label TestStatisticsInfo_questionStatisticsGridView_complexityValueLabel =
                       (Label)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_complexityValueLabel_OpenText");

                    TestStatisticsInfo_questionStatisticsGridView_complexityValueLabel.Text = questionDetail.ComplexityName;

                    Label TestStatisticsInfo_questionStatisticsGridView_testAuthorNameLabel =
                   (Label)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_testAuthorNameLabel_OpenText");

                    TestStatisticsInfo_questionStatisticsGridView_testAuthorNameLabel.Text = questionDetail.AuthorName;
                    TestStatisticsInfo_questionStatisticsGridView_testAuthorNameLabel.ToolTip = questionDetail.QuestionAuthorFullName;

                    Literal TestStatisticsInfo_questionStatisticsGridView_Answer_OpenText =
                        (Literal)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_Answer_OpenText");

                    //SearchQuestion_answerChoicesPlaceHolder.Controls.Add
                    //                    (GetAnswerChoices(questionDetail.AnswerChoices));
                    QuestionAttribute questionAttribute = (QuestionAttribute)questionDetail.QuestionAttribute;
                    TestStatisticsInfo_questionStatisticsGridView_Answer_OpenText.Text = questionAttribute.AnswerReference;

                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);
                }
            }
            catch (Exception exception)
            {

                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row is 
        /// being sorted in the grid view
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        protected void TestStatisticsInfo_questionStatisticsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                TestStatisticsInfo_questionStatisticsTestDraftExpandLinkButton.Text = "Expand All";
                TestStatisticsInfo_questionStatisticsStateExpandHiddenField.Value = "0";
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                //check whether the sort field in view state is same as 
                //the sort expression 
                if (ViewState["QuestionSortExpression"].ToString() == e.SortExpression)
                {
                    //Swap the sort order in the view state
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                //Store the sort field in the view state
                ViewState["QuestionSortExpression"] = e.SortExpression;

                SortGridView(e.SortExpression, ViewState["SORT_ORDER"].ToString());

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);

            }
        }

        protected void TestStatisticsInfo_questionStatisticsGridView_OpenText_Sorting
          (object sender, GridViewSortEventArgs e)
        {
            try
            {
                TestStatisticsInfo_questionStatisticsTestDraftExpandLinkButton_OpenText.Text = "Expand All";
                TestStatisticsInfo_questionStatisticsStateExpandHiddenField_OpenText.Value = "0";
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                //check whether the sort field in view state is same as 
                //the sort expression 
                if (ViewState["QuestionSortExpression"].ToString() == e.SortExpression)
                {
                    //Swap the sort order in the view state
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                //Store the sort field in the view state
                ViewState["QuestionSortExpression"] = e.SortExpression;

                SortGridView(e.SortExpression, ViewState["SORT_ORDER"].ToString());

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);

            }
        }
        #endregion QuestionTab

        #region CandidateTab

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the completed tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void TestStatisticsInfo_candidateTabPanel_candidateGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                // Check if event is fired when clicking on sort.
                if (e.CommandName == null || e.CommandName.Trim().ToUpper() == "SORT")
                    return;

                int index = Convert.ToInt32(e.CommandArgument);
                string candidateSessionID = (TestStatisticsInfo_candidateTabPanel_candidateGridView.Rows
                    [index].FindControl("TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewCandidateSessionIDHiddenField") as HiddenField).Value;

                int attemptID = Convert.ToInt32((TestStatisticsInfo_candidateTabPanel_candidateGridView.Rows
                    [index].FindControl("TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewAttemptIDHiddenField") as HiddenField).Value);

                if (e.CommandName == "DesignReport")
                {
                  
                    string candidateName = (TestStatisticsInfo_candidateTabPanel_candidateGridView.Rows
                      [index].FindControl("TestStatisticsInfo_candidateStatisticsTab_candidateGridViewFirstNameHiddenField") as HiddenField).Value;
                  
                    int candidateID = Convert.ToInt32((TestStatisticsInfo_candidateTabPanel_candidateGridView.Rows
                        [index].FindControl("TestStatisticsInfo_candidateStatisticsTab_candidateGridViewCandidateIDHiddenField") as HiddenField).Value);
                    dashBoard = GetDashBoard(Constants.DashboardConstants.DESIGN_REPORT);
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(dashBoard))
                    {
                        CreateDashBoard(Constants.DashboardConstants.DESIGN_REPORT);
                        dashBoard = GetDashBoard(Constants.DashboardConstants.DESIGN_REPORT);
                    }
                    userDetail = new UserDetail();
                    userDetail = (UserDetail)Session["USER_DETAIL"];
                    candidateReportDetail = new CandidateReportDetail();
                    candidateReportDetail.CandidateID = candidateID;
                    candidateReportDetail.TestKey = Request.QueryString["testkey"];
                    candidateReportDetail.CandidateSessionkey = candidateSessionID;
                    candidateReportDetail.AttemptID = attemptID;
                    candidateReportDetail.TestName = TestStatisticsInfo_testNameLabel.Text;
                    candidateReportDetail.CandidateName = candidateName;
                    candidateReportDetail.LoginUser = userDetail.FirstName + "," + userDetail.LastName;
                    Session["CANDIDATEDETAIL"] = candidateReportDetail;
                    Response.Redirect("~/ReportCenter/DesignReport.aspx?m=3&s=0" +
                        "&testkey=" + Request.QueryString["testkey"] +
                        "&dashboardkey=" + dashBoard.DashBoardID +
                        "&dashboardsectionkey=" + dashBoard.DashBoardSectionID +
                        "&filename=" + candidateReportDetail.CandidateName.Replace(' ','-') +
                        "&parentpage=" + Constants.ParentPage.TEST_STATISTICS_INFO +
                        "&sourcepage=" + Request.QueryString["parentpage"], false);
                }
                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search
        /// button is clicked 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void TestStatisticsInfo_candidateTabPanel_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;
                ViewState[SELECTED_CANDIDATE_DETAILS] = null;
                TestStatisticsInfo_selectedcanidates.Value = null;
                LoadValues("CANDIDATE_NAME", "A", 1);

                if (TestStatisticsInfo_candidateTabPanel_candidateGridView.Rows.Count == 0)
                {
                    ShowMessage(TestStatisticsInfo_candidateTabPanelErrorLabel,
                        Resources.HCMResource.Common_Empty_Grid);
                    TestStatistics_candidateDetailsReportLinkTR.Visible = false;
                }
                else
                {
                    TestStatistics_candidateDetailsReportLinkTR.Visible = true;
                }

                TestStatisticsInfo_candidateTabPanel_relativeScoreMultiHandleSliderExtender.ClientState = "0";
                TestStatisticsInfo_candidateTabPanel_testCostMultiHandleSliderExtender.ClientState = "0";

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);

            }
        }

        /// <summary>
        /// Handler method that will be called when the row is binded 
        /// in grid view
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void TestStatisticsInfo_candidateTabPanel_candidateGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                    e.Row.Attributes.Add("onmouseout", onmouseoutStyle);

                    var TestStatisticsInfo_candidateTabPanel_candidateGridView_selectCheckBox =
                   (CheckBox)e.Row.FindControl("TestStatisticsInfo_candidateTabPanel_candidateGridView_selectCheckBox");
                    
                    HiddenField candidateSessionID = (HiddenField)e.Row.FindControl("TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewCandidateSessionIDHiddenField");
                    HiddenField attemptID = (HiddenField)e.Row.FindControl("TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewAttemptIDHiddenField");

                    #region Open candidate test details in new window
                    HyperLink TestStatisticsInfo_candidateTabPanel_candidateGridView_canidateReportHyperLinkButton = 
                        (HyperLink)e.Row.FindControl("TestStatisticsInfo_candidateTabPanel_candidateGridView_canidateReportHyperLinkButton");
                    TestStatisticsInfo_candidateTabPanel_candidateGridView_canidateReportHyperLinkButton.NavigateUrl =
                        "~/ReportCenter/CandidateTestDetails.aspx?m=3&s=0" +
                        "&testkey=" + Request.QueryString["testkey"] +
                        "&candidatesession=" + candidateSessionID.Value +
                        "&attemptid=" + attemptID.Value +
                        "&tab=CS" +
                        "&parentpage=" + Constants.ParentPage.TEST_STATISTICS_INFO;
                    #endregion

                    #region Open cyper proctoring information in new window
                    HyperLink TestStatisticsInfo_candidateTabPanel_candidateGridView_trackingHyperLink =
                        (HyperLink)e.Row.FindControl("TestStatisticsInfo_candidateTabPanel_candidateGridView_trackingHyperLink");

                      TestStatisticsInfo_candidateTabPanel_candidateGridView_trackingHyperLink.NavigateUrl=
                      "~/ReportCenter/TrackingDetails.aspx" +
                       "?m=3&s=0&candidatesession=" + candidateSessionID.Value +
                       "&attemptid=" + attemptID.Value +
                       "&tab=CS" +
                       "&testkey=" + Request.QueryString["testkey"] +
                       "&parentpage=" + Constants.ParentPage.TEST_STATISTICS_INFO;
                    #endregion
                    TestStatisticsInfo_candidateTabPanel_candidateGridView_selectCheckBox.Attributes.Add("onclick", "selectedCanidates(" + TestStatisticsInfo_candidateTabPanel_candidateGridView_selectCheckBox.ClientID + ",'" + candidateSessionID.Value + ":" + attemptID.Value + "')");

                    HyperLink TestStatisticsInfo_candidateStatisticsTab_candidateFullnameHyperLink =
                        (HyperLink)e.Row.FindControl("TestStatisticsInfo_candidateStatisticsTab_candidateFullnameHyperLink");

                    HiddenField TestStatisticsInfo_candidateStatisticsTab_candidateGridViewCandidateIDHiddenField =
                        (HiddenField)e.Row.FindControl("TestStatisticsInfo_candidateStatisticsTab_candidateGridViewCandidateIDHiddenField");

                    if (TestStatisticsInfo_candidateStatisticsTab_candidateFullnameHyperLink != null)
                    {
                        if (TestStatisticsInfo_candidateStatisticsTab_candidateGridViewCandidateIDHiddenField != null)
                        {
                            TestStatisticsInfo_candidateStatisticsTab_candidateFullnameHyperLink.NavigateUrl =
                                "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&puid=" + TestStatisticsInfo_candidateStatisticsTab_candidateGridViewCandidateIDHiddenField.Value;
                        }
                    }

                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row is sorted 
        /// in grid view
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        protected void TestStatisticsInfo_candidateTabPanel_candidateGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortOrder = string.Empty;
                string sortExpression = e.SortExpression;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }

                if (ViewState["CANDIDATE_SORT_EXPRESSION"].ToString() == e.SortExpression)
                {
                    ViewState["CANDIDATE_SORT_ORDER"] =
                        ViewState["CANDIDATE_SORT_ORDER"].ToString() == SortType.Ascending.ToString() ?
                         SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["CANDIDATE_SORT_ORDER"] = SortType.Descending;
                else
                {
                    ViewState["CANDIDATE_SORT_ORDER"] = SortType.Ascending;
                }

                ViewState["CANDIDATE_SORT_EXPRESSION"] = e.SortExpression;

                TestStatisticsInfo_candidateTabPanel_bottomPagingNavigator.Reset();

                SortGridView(e.SortExpression, ViewState["CANDIDATE_SORT_ORDER"].ToString(), 1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);

            }

        }

        /// <summary>
        /// Handler method that will be called when the row is created
        /// in grid view
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void TestStatisticsInfo_candidateTabPanel_candidateGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex(TestStatisticsInfo_candidateTabPanel_candidateGridView, (string)ViewState["CANDIDATE_SORT_EXPRESSION"]);
                    if (sortColumnIndex != -1)
                    {
                        AddCandidateSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);

            }
        }

        /// <summary>
        /// Hanlder method that is called when the page number
        /// is clicked in the page number
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="PageNumberEventArgs"/>that holds the event data
        /// </param>
        protected void TestStatisticsInfo_candidateTabPanel_pagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ViewState["CANDIDATE_PAGE_NO"] = e.PageNumber.ToString();
                SortGridView(ViewState["CANDIDATE_SORT_EXPRESSION"].ToString()
                       , ViewState["CANDIDATE_SORT_ORDER"].ToString(), e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(TestStatisticsInfo_topErrorMessageLabel,
                    TestStatisticsInfo_bottonErrorLabel, exception.Message);
            }

        }

        protected void TestStatisticsInfo_candidateTabPanel_candidateGridView_selectCheckBox_CheckedChanged(
            object sender, EventArgs e)
        {
            List<CandidateReportDetail> candidateReportDetailList = new List<CandidateReportDetail>();
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState[SELECTED_CANDIDATE_DETAILS]))
                candidateReportDetailList = (List<CandidateReportDetail>)ViewState[SELECTED_CANDIDATE_DETAILS];
            foreach (GridViewRow row in TestStatisticsInfo_candidateTabPanel_candidateGridView.Rows)
            {
                var CandidateID =
                    (HiddenField)row.FindControl("TestStatisticsInfo_candidateStatisticsTab_candidateGridViewCandidateIDHiddenField");
                var AttemptID =
                   (HiddenField)row.FindControl("TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewAttemptIDHiddenField");
                var CandidateSessionID =
                   (HiddenField)row.FindControl("TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewCandidateSessionIDHiddenField");
                var TestStatisticsInfo_candidateTabPanel_candidateGridView_selectCheckBox =
                    (CheckBox)row.FindControl("TestStatisticsInfo_candidateTabPanel_candidateGridView_selectCheckBox");
                var CandiatefullName = (Label)row.FindControl("TestStatisticsInfo_candidateStatisticsTab_candidateFullnameLabel");
                candidateReportDetail = new CandidateReportDetail();
                candidateReportDetail.AttemptID = int.Parse(AttemptID.Value);
                candidateReportDetail.CandidatesID = CandidateID.Value;
                candidateReportDetail.CandidateSessionkey = CandidateSessionID.Value;
                //candidateReportDetail.CombinedSessionKeyAttemtID = combinedSessionKeyAttemtID.TrimEnd(',');
                candidateReportDetail.CandidateNames = CandiatefullName.Text;

                if (TestStatisticsInfo_candidateTabPanel_candidateGridView_selectCheckBox.Checked)
                {
                    if (!candidateReportDetailList.Exists(p => p.CandidateSessionkey == CandidateSessionID.Value && p.AttemptID == int.Parse(AttemptID.Value)))
                        candidateReportDetailList.Add(candidateReportDetail);
                }
                else
                {
                    candidateReportDetail = candidateReportDetailList.Find(p => p.CandidateSessionkey == CandidateSessionID.Value && p.AttemptID == int.Parse(AttemptID.Value));
                    candidateReportDetailList.Remove(candidateReportDetail);
                }
                ViewState[SELECTED_CANDIDATE_DETAILS] = candidateReportDetailList;
            }
        }
        #endregion CandidateTab

        #endregion Event Handlers

        #region PrivateMethods                                                 

        #region QuestionTab

        /// <summary>
        /// Method to sort the Candidate details grid view
        /// </summary>
        /// <param name="sortExpression">
        /// A<see cref="string"/>that holds the string expression 
        /// </param>
        /// <param name="direction">
        /// A<see cref="string"/>that holds the sorting direction
        /// </param>
        private void SortGridView(string sortExpression, string direction)
        {

            if (Cache["SearchDataTables"] == null)
                return;

            //Get the list of statistics details stored in cache
            List<QuestionStatisticsDetail> questionStatisticsDetails =
               Cache["SearchDataTables"] as List<QuestionStatisticsDetail>;

            //Initialise the new list to store the sorted list
            List<QuestionStatisticsDetail> sortedQuestionStatisticsDetails =
                new List<QuestionStatisticsDetail>();

            //Check the sorting expression 
            switch (sortExpression)
            {
                //Check if the expression is LocalTime
                case "LocalTime":
                    //If the direction is  ascending then it is sorted in 
                    //ascending order against local time 
                    if (direction == "Ascending")
                    {

                        var questionSortedList = from c in questionStatisticsDetails
                                                 orderby c.AverageTimeTakenWithinTest ascending
                                                 select c;
                        sortedQuestionStatisticsDetails = questionSortedList.ToList();
                    }
                    //else sort the questions by descending order         
                    else
                    {
                        var questionDescSortedList = from c in questionStatisticsDetails
                                                     orderby c.AverageTimeTakenWithinTest descending
                                                     select c;
                        sortedQuestionStatisticsDetails = questionDescSortedList.ToList();
                    }
                    break;

                //Check if the expression is Global Time
                case "GlobalTime":

                    //If the direction is  ascending then it is sorted in 
                    //ascending order against global time 
                    if (direction == "Ascending")
                    {
                        var questionSortedList = from c in questionStatisticsDetails
                                                 orderby c.AverageTimeTakenAcrossTest ascending
                                                 select c;
                        sortedQuestionStatisticsDetails = questionSortedList.ToList();
                    }
                    //else sort the questions by descending order
                    else
                    {
                        var questionDescSortedList = from c in questionStatisticsDetails
                                                     orderby c.AverageTimeTakenAcrossTest descending
                                                     select c;
                        sortedQuestionStatisticsDetails = questionDescSortedList.ToList();
                    }
                    break;
                //Check if the expression is Ratio
                case "Ratio":

                    //If the direction is  ascending then it is sorted in 
                    //ascending order against ratio
                    if (direction == "Ascending")
                    {
                        var questionSortedList = from c in questionStatisticsDetails
                                                 orderby c.Ratio ascending
                                                 select c;
                        sortedQuestionStatisticsDetails = questionSortedList.ToList();
                    }
                    //else sort the questions by descending order    
                    else
                    {
                        var questionDescSortedList = from c in questionStatisticsDetails
                                                     orderby c.Ratio descending
                                                     select c;
                        sortedQuestionStatisticsDetails = questionDescSortedList.ToList();
                    }
                    break;
                //Check if the expression is author
                case "Author":
                    //If the direction is  ascending then it is sorted in 
                    //ascending order against ratio
                    if (direction == "Ascending")
                    {
                        var questionSortedList = from c in questionStatisticsDetails
                                                 orderby c.Author ascending
                                                 select c;
                        sortedQuestionStatisticsDetails = questionSortedList.ToList();
                    }
                    //else sort the questions by descending order  
                    else
                    {
                        var questionDescSortedList = from c in questionStatisticsDetails
                                                     orderby c.Author descending
                                                     select c;
                        sortedQuestionStatisticsDetails = questionDescSortedList.ToList();
                    }
                    break;
                //by Degfault the expression will be question 
                default:

                    //If the direction is  ascending then it is sorted in 
                    //ascending order against question 
                    if (direction == "Ascending")
                    {
                        var questionSortedList = from c in questionStatisticsDetails
                                                 orderby c.Question ascending
                                                 select c;
                        sortedQuestionStatisticsDetails = questionSortedList.ToList();
                    }
                    //else sort the questions by descending order  
                    else
                    {
                        var questionDescSortedList = from c in questionStatisticsDetails
                                                     orderby c.Question descending
                                                     select c;
                        sortedQuestionStatisticsDetails = questionDescSortedList.ToList();
                    }
                    break;
            }

            //Bind the data source with the sorted questions
            if (ViewState["QuestionType"] != null)
            {
                if (ViewState["QuestionType"].ToString()== "1")
                {
                    TestStatisticsInfo_questionStatisticsGridView.DataSource =
                sortedQuestionStatisticsDetails;
                    TestStatisticsInfo_questionStatisticsGridView.DataBind();
                }
                else
                {
                TestStatisticsInfo_questionStatisticsGridView_OpenText.DataSource =
            sortedQuestionStatisticsDetails;
                TestStatisticsInfo_questionStatisticsGridView_OpenText.DataBind();
                }

            }
                       
        }

        /// <summary>
        /// Method to get the column index of the column of the grid
        /// </summary>
        /// <returns>
        /// A<see cref="int"/>that holds the column index
        /// </returns>
        //private int GetSortColumnIndex()
        //{
        //    //Get the control fields in each columns of the grid 
        //    foreach (DataControlField field in TestStatisticsInfo_questionStatisticsGridView.Columns)
        //    {
        //        //check if the sort expression of the field is same as 
        //        //the sort expression in view state
        //        if (field.SortExpression ==
        //            (string)ViewState["QuestionSortExpression"])
        //        {
        //            //if it is same return the column index
        //            return TestStatisticsInfo_questionStatisticsGridView.Columns.IndexOf(field);
        //        }
        //    }
        //    return -1;
        //}

        /// <summary>
        /// Method to add the sort image to the grid header 
        /// </summary>
        /// <param name="columnIndex">
        /// A<see cref="int"/>that holds the column index
        /// </param>
        /// <param name="headerRow">
        /// A<see cref="GridViewRow"/>that holds the grid view row
        /// </param>
        private void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            // Create the sorting image based on the sort direction.
            Image sortImage = new Image();
            if (ViewState["SORT_ORDER"].ToString() == SortType.Ascending.ToString())
            {
                sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_asc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_desc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            // Add the image to the appropriate header cell.
            headerRow.Cells[columnIndex].Controls.Add(sortImage);

        }

        /// <summary>
        /// Represents the method to get the answer choices of the question 
        /// </summary>
        /// <param name="answerChoice">
        /// A<see cref="List<AnswerChoice>"/>that holds the list of answerchoices
        /// </param>
        /// <returns>
        /// A<see cref="Table"/>a table that contain the answer choice
        /// </returns>
        private Table GetAnswerChoices(List<AnswerChoice> answerChoice)
        {
            //This method will return the table containing the answer choices
            return (new ControlUtility().GetAnswerChoices(answerChoice,false));
        }

        /// <summary>
        /// Represents the method to get the  correct date
        /// </summary>        /// 
        /// <param name="date">
        /// A<see cref="decimal"/>date that holds the date
        /// </param>
        /// <returns>
        /// A<see cref="string"/>the correct date
        /// </returns>
        protected string GetCorrectDate(decimal date)
        {
            return Forte.HCM.Support.Utility.ConvertSecondsToHoursMinutesSeconds(decimal.ToInt32(date));
        }
        #endregion QuestionTab

        #region CandidateTab
        /// <summary>
        /// Create Dashboard .
        /// </summary>
        private void CreateDashBoard(string dashBoardName)
        {

            DashboardInstance instance = new DashboardInstance()
            {
                InstanceKey = Guid.NewGuid(),
                Title = dashBoardName + " DashBoard",
                Username = Thread.CurrentPrincipal.Identity.Name,
                Width = 830,
                Header = false,
                AutoHeight = true,
                Group = dashBoardName,
                BodyStyle = "background-color: #FFFFFF;"

            };

            DashboardSectionInstance row1 = new DashboardSectionInstance(instance.InstanceKey, 0, Guid.NewGuid());
            DashboardColumn col1 = new DashboardColumn(row1.InstanceKey, 0, 50, null, null);
            DashboardColumn col2 = new DashboardColumn(row1.InstanceKey, 1, 50, null, null);
            row1.BodyStyle = "background-color: #FFFFFF;";
            row1.Columns.Add(col1);
            row1.Columns.Add(col2);
            instance.Rows.Add(row1);

            DashboardFramework.CreateDashboard(instance);

        }
        /// <summary>
        /// Get Existing Datshboard Details based on the User ID.
        /// </summary>
        /// <returns> 
        /// dashboard Id and dashboard Section ID.
        /// </returns>
        private DashBoard GetDashBoard(string groupName)
        {
            dashBoard = new DashBoard();
            dashBoard = new DashBoardBLManager().GetDashBoardId(Thread.CurrentPrincipal.Identity.Name, groupName);
            return dashBoard;
        }

        /// <summary>
        /// Method to add the sort image to the grid header 
        /// </summary>
        /// <param name="columnIndex">
        /// A<see cref="int"/>that holds the column index
        /// </param>
        /// <param name="headerRow">
        /// A<see cref="GridViewRow"/>that holds the grid view row
        /// </param>
        private void AddCandidateSortImage(int columnIndex, GridViewRow headerRow)
        {
            // Create the sorting image based on the sort direction.
            Image sortImage = new Image();
            if (ViewState["CANDIDATE_SORT_ORDER"].ToString() == SortType.Ascending.ToString())
            {
                sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_asc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_desc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            // Add the image to the appropriate header cell.
            headerRow.Cells[columnIndex].Controls.Add(sortImage);

        }

        /// <summary>
        /// Method to sort candidate deatils the grid view 
        /// </summary>
        /// <param name="sortExpression">
        /// A<see cref="string"/>that holds the sort expression
        /// </param>
        /// <param name="direction">
        /// A<see cref="string"/>that holds the sort direction 
        /// </param>
        /// <param name="pagenumber">
        /// A<see cref="int"/>that holds the page number 
        /// </param>
        private void SortGridView(string sortExpression, string direction, int pagenumber)
        {
            //This method will sort the grid view 
            //based on the given expression, page number and  page size
            LoadValues(sortExpression, direction
                == "Ascending" ? "A" : "D", pagenumber);
        }

        /// <summary>
        /// Method to load the candidate details.
        /// </summary>
        private void LoadCandidateDetails()
        {
            //Make the candidate div as visible true
            TestStatisticsInfo_candidateTabPanel_questionDiv.Visible = true;

            //Add the click attribute for the select candidate image button 
            TestStatisticsInfo_candidateTabPanel_candidateNameImageButton.Attributes.
                Add("onclick", "return LoadCandidate('" +
                TestStatisticsInfo_candidateTabPanel_candidateNameTextBox.ClientID + "','','')");

            //Add the click attribute for the select created by image button 
            TestStatisticsInfo_candidateTabPanel_createdByImageButton.Attributes.Add(
                "onclick", "return LoadAdminName('"
                + TestStatisticsInfo_candidateTabPanel_dummyCreatedBy.ClientID + "','"
                + TestStatisticsInfo_candidateTabPanel_createdBy_HiddenField.ClientID + "','"
                + TestStatisticsInfo_candidateTabPanel_createdByTextBox.ClientID + "','TS')");

            //Add the clicke attribute for the scheduled by image button 
            TestStatisticsInfo_candidateTabPanel_scheduledByImageButton.Attributes.Add(
                "onclick", "return LoadAdminName('"
                + TestStatisticsInfo_candidateTabPanel_dummyCreatedBy.ClientID + "','"
                + TestStatisticsInfo_candidateTabPanel_createdBy_HiddenField.ClientID + "','"
                + TestStatisticsInfo_candidateTabPanel_scheduledByTextBox.ClientID + "','TC')");

            //Load the candidate details with default
            //sort expression and sort direction
            LoadValues("DATE", "D",int.Parse(ViewState["CANDIDATE_PAGE_NO"].ToString()));

            //Check if the candidate grid is empty 
            if (TestStatisticsInfo_candidateTabPanel_candidateGridView.Rows.Count == 0)
            {
                //If the grid is empty then  display a error message to 
                // the user
                ShowMessage(TestStatisticsInfo_candidateTabPanelErrorLabel,
                    Resources.HCMResource.
                    TestStatisticsInfo_CandidateHasNotBeenAdministered);
                TestStatistics_candidateDetailsReportLinkTR.Visible = false;
            }
            else
            {
                TestStatistics_candidateDetailsReportLinkTR.Visible = true;
            }
        }

        /// <summary>
        /// Method to get the values for the candidate grid view according to the 
        /// search fields
        /// </summary>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the  order by expression
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/>that holds the order by direction
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size
        /// </param>
        private void LoadValues(string orderBy, string orderByDirection,
            int pageNumber)
        {
            TestStatisticsSearchCriteria testStatisticsSearchCriteria = new TestStatisticsSearchCriteria();
            // Declare and initialis the out variable totalRecords
            int totalRecords = 0;

            //Get the candidate name to search
            testStatisticsSearchCriteria.CandidateName = TestStatisticsInfo_candidateTabPanel_candidateNameTextBox.Text.Trim();

            //Get the session created by user name
            testStatisticsSearchCriteria.SessionCreatedBy = TestStatisticsInfo_candidateTabPanel_createdByTextBox.Text.Trim();

            //Get the schedule created by user name
            testStatisticsSearchCriteria.ScheduleCreatedBy = TestStatisticsInfo_candidateTabPanel_scheduledByTextBox.Text.Trim();

            //Get the starting test date 
            testStatisticsSearchCriteria.TestDateFrom = TestStatisticsInfo_candidateTabPanel_testDateTextBox.Text.Trim();

            //Get the last test date
            testStatisticsSearchCriteria.TestDateTo = TestStatisticsInfo_candidateTabPanel_testToDateTextBox.Text.Trim();

            //Get the absolute score starting value
            testStatisticsSearchCriteria.AbsoluteScoreFrom = TestStatisticsInfo_candidateTabPanel_absoluteScoreMinValueTextBox.Text.Trim();

            //Get the absolute score ending value
            testStatisticsSearchCriteria.AbsoluteScoreTo = TestStatisticsInfo_candidateTabPanel_absoluteScoreMaxValueTextBox.Text.Trim();

            //Get the relative score starting value
            testStatisticsSearchCriteria.RelativeScoreFrom = TestStatisticsInfo_candidateTabPanel_relativeScoreMinValueTextBox.Text.Trim();

            //Get the relative score ending value
            testStatisticsSearchCriteria.RelativeScoreTo = TestStatisticsInfo_candidateTabPanel_relativeScoreMaxValueTextBox.Text.Trim();
            testStatisticsSearchCriteria.TestKey = Request.QueryString["testkey"];

            testStatisticsSearchCriteria.CurrentPage = pageNumber;
            testStatisticsSearchCriteria.IsMaximized = TestStatisticsInfo_heightHiddenField.Value.Trim() == "Y" ? true : false;

            testStatisticsSearchCriteria.SortDirection = orderBy;
            testStatisticsSearchCriteria.SortExpression = orderByDirection;
            testStatisticsSearchCriteria.SellectedCandidateDetails = TestStatisticsInfo_selectedcanidates.Value;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_STATISTICS] = testStatisticsSearchCriteria;
            //Get the candidate details records and total count based on the search terms
            TestStatisticsInfo_candidateTabPanel_candidateGridView.DataSource =
                new ReportBLManager().GetReportCandidateStatisticsDetails
                (testStatisticsSearchCriteria,
                    orderBy, orderByDirection, pageNumber, GridPageSize,
                    out totalRecords);
            //Bind the candidate grid with the details
            TestStatisticsInfo_candidateTabPanel_candidateGridView.DataBind();
            //Set the total records for the paging control
            TestStatisticsInfo_candidateTabPanel_bottomPagingNavigator.TotalRecords = totalRecords;
            //Set the page size for the paging control 
            TestStatisticsInfo_candidateTabPanel_bottomPagingNavigator.PageSize = GridPageSize;
            RePopulateCheckBoxes();
        }

        /// <summary>
        /// Method that exports the candidate details.
        /// </summary>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the  order by expression
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/>that holds the order by direction
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size
        /// </param>
        private DataTable GetExportCandidateDetails()
        {
            TestStatisticsSearchCriteria testStatisticsSearchCriteria = new TestStatisticsSearchCriteria();
            // Declare and initialis the out variable totalRecords
            int totalRecords = 0;

            //Get the candidate name to search
            testStatisticsSearchCriteria.CandidateName = TestStatisticsInfo_candidateTabPanel_candidateNameTextBox.Text.Trim();

            //Get the session created by user name
            testStatisticsSearchCriteria.SessionCreatedBy = TestStatisticsInfo_candidateTabPanel_createdByTextBox.Text.Trim();

            //Get the schedule created by user name
            testStatisticsSearchCriteria.ScheduleCreatedBy = TestStatisticsInfo_candidateTabPanel_scheduledByTextBox.Text.Trim();

            //Get the starting test date 
            testStatisticsSearchCriteria.TestDateFrom = TestStatisticsInfo_candidateTabPanel_testDateTextBox.Text.Trim();

            //Get the last test date
            testStatisticsSearchCriteria.TestDateTo = TestStatisticsInfo_candidateTabPanel_testToDateTextBox.Text.Trim();

            //Get the absolute score starting value
            testStatisticsSearchCriteria.AbsoluteScoreFrom = TestStatisticsInfo_candidateTabPanel_absoluteScoreMinValueTextBox.Text.Trim();

            //Get the absolute score ending value
            testStatisticsSearchCriteria.AbsoluteScoreTo = TestStatisticsInfo_candidateTabPanel_absoluteScoreMaxValueTextBox.Text.Trim();

            //Get the relative score starting value
            testStatisticsSearchCriteria.RelativeScoreFrom = TestStatisticsInfo_candidateTabPanel_relativeScoreMinValueTextBox.Text.Trim();

            //Get the relative score ending value
            testStatisticsSearchCriteria.RelativeScoreTo = TestStatisticsInfo_candidateTabPanel_relativeScoreMaxValueTextBox.Text.Trim();
            testStatisticsSearchCriteria.TestKey = Request.QueryString["testkey"];

            testStatisticsSearchCriteria.CurrentPage = 1;
            testStatisticsSearchCriteria.IsMaximized = TestStatisticsInfo_heightHiddenField.Value.Trim() == "Y" ? true : false;

            testStatisticsSearchCriteria.SortDirection = "A";
            testStatisticsSearchCriteria.SortExpression = "CANDIDATE_NAME";
            testStatisticsSearchCriteria.SellectedCandidateDetails = TestStatisticsInfo_selectedcanidates.Value;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_STATISTICS] = testStatisticsSearchCriteria;
            //Get the candidate details records and total count based on the search terms
            return new ReportBLManager().GetReportCandidateStatisticsDetailsTable
                (testStatisticsSearchCriteria, "A", "CANDIDATE_NAME", 1, int.MaxValue,
                    out totalRecords);
        }

        /// <summary>
        /// Method to get the column index of the candidate grid  
        /// </summary>
        /// <returns>
        /// A<see cref="int"/>that holds the column index 
        /// </returns>
        //private int GetCandidateGridSortColumnIndex()
        //{
        //    //Get the control field in every grid view columns 
        //    foreach (DataControlField field in TestStatisticsInfo_candidateTabPanel_candidateGridView.Columns)
        //    {
        //        //Check the field sort expression is 
        //        //equal to the sort expression in view state
        //        if (field.SortExpression ==
        //                     (string)ViewState["CANDIDATE_SORT_EXPRESSION"])
        //        {
        //            //return the matched index of the columns
        //            return TestStatisticsInfo_candidateTabPanel_candidateGridView.Columns.IndexOf(field);
        //        }
        //    }
        //    return -1;
        //}

        /// <summary>
        /// Method to get the column index of the candidate grid 
        /// </summary>
        /// <returns>
        /// A<see cref="int"/>that holds the count of the selected Candidate. 
        /// </returns>
        private CandidateReportDetail IsValidComparison()
        {
            string attemptsID = "";
            string candidatesID = "";
            string candidatesNames = "";
            string candidateSessionskey = "";
            string combinedSessionKeyAttemtID = "";

            List<CandidateReportDetail> candidateReportDetailList = new List<CandidateReportDetail>();
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState[SELECTED_CANDIDATE_DETAILS]))
                candidateReportDetailList = (List<CandidateReportDetail>)ViewState[SELECTED_CANDIDATE_DETAILS];
            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
            for (int i = 0; i < candidateReportDetailList.Count; i++)
            {
                combinedSessionKeyAttemtID = combinedSessionKeyAttemtID + candidateReportDetailList[i].CandidateSessionkey + "|" + candidateReportDetailList[i].AttemptID + ",";
                attemptsID = attemptsID + candidateReportDetailList[i].AttemptID + ",";
                candidatesID = candidatesID + candidateReportDetailList[i].CandidateID + ",";
                candidateSessionskey = candidateSessionskey + candidateReportDetailList[i].CandidateSessionkey + ",";
                candidatesNames = candidatesNames + candidateReportDetailList[i].CandidateNames + ",";
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(attemptsID))
            {
                candidateReportDetail = new CandidateReportDetail();
                candidateReportDetail.AttemptsID = attemptsID.TrimEnd(',');
                candidateReportDetail.CandidatesID = candidatesID.TrimEnd(',');
                candidateReportDetail.CandidateSessionkey = candidateSessionskey.TrimEnd(',');
                candidateReportDetail.CombinedSessionKeyAttemtID = combinedSessionKeyAttemtID.TrimEnd(',');
                candidateReportDetail.CandidateNames = candidatesNames.TrimEnd(',');
            }
            return candidateReportDetail;
        }

        private List<Int32> SelectedCandidateId
        {
            get
            {
                if (ViewState[SELECTED_CANDIDATE_ID] == null)
                {
                    ViewState[SELECTED_CANDIDATE_ID] = new List<Int32>();
                }

                return (List<Int32>)ViewState[SELECTED_CANDIDATE_ID];
            }
        }

        private void PersistRowIndex(int index)
        {
            if (!SelectedCandidateId.Exists(i => i == index))
            {
                SelectedCandidateId.Add(index);
            }
        }

        private void RemoveRowIndex(int index)
        {
            SelectedCandidateId.Remove(index);
        }

        private void RePopulateCheckBoxes1()
        {

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState[SELECTED_CANDIDATE_DETAILS]))
                return;
            List<CandidateReportDetail> candidateReportDetailList = new List<CandidateReportDetail>();
            candidateReportDetailList = (List<CandidateReportDetail>)ViewState[SELECTED_CANDIDATE_DETAILS];
            candidateReportDetailList.Distinct();
            foreach (GridViewRow row in TestStatisticsInfo_candidateTabPanel_candidateGridView.Rows)
            {
                var chkBox = row.FindControl("TestStatisticsInfo_candidateTabPanel_candidateGridView_selectCheckBox") as CheckBox;
                var CandidateID =
                   (HiddenField)row.FindControl("TestStatisticsInfo_candidateStatisticsTab_candidateGridViewCandidateIDHiddenField");
                var AttemptID =
                   (HiddenField)row.FindControl("TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewAttemptIDHiddenField");
                var CandidateSessionID =
                   (HiddenField)row.FindControl("TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewCandidateSessionIDHiddenField");
                if (candidateReportDetailList.Exists(x => x.AttemptID == int.Parse(AttemptID.Value) && x.CandidateSessionkey == CandidateSessionID.Value))
                {
                    chkBox.Checked = true;
                }
                else
                {
                    chkBox.Checked = false;
                }
            }
        }

        private List<CandidateReportDetail> SelectedCandidateDetails
        {
            get
            {
                List<CandidateReportDetail> candidateReportDetailList = new List<CandidateReportDetail>();
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TestStatisticsInfo_selectedcanidates.Value))
                {
                    string[] candidates = TestStatisticsInfo_selectedcanidates.Value.Split(',');
                    foreach (string a in candidates)
                    {
                        if (a.Length > 0)
                        {
                            CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
                            candidateReportDetail.CandidateSessionkey = a.Split(':')[0];
                            candidateReportDetail.AttemptID = int.Parse(a.Split(':')[1]);
                            candidateReportDetailList.Add(candidateReportDetail);
                        }
                    }
                }
                return candidateReportDetailList;
            }
        }

        private List<CandidateReportDetail> SellectedCanidateList()
        {
            List<CandidateReportDetail> candidateReportDetailList = new List<CandidateReportDetail>();

            string[] candidates = TestStatisticsInfo_selectedcanidates.Value.Split(',');
            foreach (string a in candidates)
            {
                if (a.Length > 0)
                {
                    CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
                    candidateReportDetail.CandidateSessionkey = a.Split(':')[0];
                    candidateReportDetail.AttemptID = int.Parse(a.Split(':')[1]);
                    candidateReportDetailList.Add(candidateReportDetail);
                }
            }
            return candidateReportDetailList;
        }
        private void RePopulateCheckBoxes()
        {

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(TestStatisticsInfo_selectedcanidates.Value))
                return;
            List<CandidateReportDetail> candidateReportDetailList = SellectedCanidateList();

            foreach (GridViewRow row in TestStatisticsInfo_candidateTabPanel_candidateGridView.Rows)
            {
                var chkBox = row.FindControl("TestStatisticsInfo_candidateTabPanel_candidateGridView_selectCheckBox") as CheckBox;
                var CandidateID =
                   (HiddenField)row.FindControl("TestStatisticsInfo_candidateStatisticsTab_candidateGridViewCandidateIDHiddenField");
                var AttemptID =
                   (HiddenField)row.FindControl("TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewAttemptIDHiddenField");
                var CandidateSessionID =
                   (HiddenField)row.FindControl("TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewCandidateSessionIDHiddenField");
                if (candidateReportDetailList.Exists(x => x.AttemptID == int.Parse(AttemptID.Value) && x.CandidateSessionkey == CandidateSessionID.Value))
                {
                    chkBox.Checked = true;
                }
                else
                {
                    chkBox.Checked = false;
                }
            }
        }

        private List<CandidateReportDetail> SelectedCandidateDetails1
        {
            get
            {
                if (ViewState[SELECTED_CANDIDATE_DETAILS] == null)
                {
                    ViewState[SELECTED_CANDIDATE_DETAILS] = new List<CandidateReportDetail>();
                }
                return (List<CandidateReportDetail>)ViewState[SELECTED_CANDIDATE_DETAILS];
            }
        }

        #endregion CandidateTab

        /// <summary>
        /// Method that will fill the search criteria to the appropriate fields.
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A <see cref="TestSearchCriteria"/> that contains the test search
        /// criteria fields.
        /// </param>
        public void FillSearchCriteria(TestStatisticsSearchCriteria testStatisticsSearchCriteria)
        {

            //Get the candidate name to search
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatisticsSearchCriteria.CandidateName))
                TestStatisticsInfo_candidateTabPanel_candidateNameTextBox.Text = testStatisticsSearchCriteria.CandidateName;

            //Get the session created by user name
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatisticsSearchCriteria.SessionCreatedBy))
                TestStatisticsInfo_candidateTabPanel_createdByTextBox.Text = testStatisticsSearchCriteria.SessionCreatedBy;

            //Get the schedule created by user name
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatisticsSearchCriteria.ScheduleCreatedBy))
                TestStatisticsInfo_candidateTabPanel_scheduledByTextBox.Text = testStatisticsSearchCriteria.ScheduleCreatedBy;

            //Get the starting test date 
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatisticsSearchCriteria.TestDateFrom))
                TestStatisticsInfo_candidateTabPanel_testDateTextBox.Text = testStatisticsSearchCriteria.TestDateFrom;

            //Get the last test date
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatisticsSearchCriteria.TestDateTo))
                TestStatisticsInfo_candidateTabPanel_testToDateTextBox.Text = testStatisticsSearchCriteria.TestDateTo;

            //Get the absolute score starting value
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatisticsSearchCriteria.AbsoluteScoreFrom))
                TestStatisticsInfo_candidateTabPanel_absoluteScoreMinValueTextBox.Text = testStatisticsSearchCriteria.AbsoluteScoreFrom;

            //Get the absolute score ending value
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatisticsSearchCriteria.AbsoluteScoreTo))
                TestStatisticsInfo_candidateTabPanel_absoluteScoreMaxValueTextBox.Text = testStatisticsSearchCriteria.AbsoluteScoreTo;

            //Get the relative score starting value
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatisticsSearchCriteria.RelativeScoreFrom))
                TestStatisticsInfo_candidateTabPanel_relativeScoreMinValueTextBox.Text = testStatisticsSearchCriteria.RelativeScoreFrom;

            //Get the relative score ending value
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatisticsSearchCriteria.RelativeScoreTo))
                TestStatisticsInfo_candidateTabPanel_relativeScoreMaxValueTextBox.Text = testStatisticsSearchCriteria.RelativeScoreTo;

            //Get the relative score ending value
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testStatisticsSearchCriteria.SellectedCandidateDetails))
                TestStatisticsInfo_selectedcanidates.Value = testStatisticsSearchCriteria.SellectedCandidateDetails == null ? "" : testStatisticsSearchCriteria.SellectedCandidateDetails;

            TestStatisticsInfo_heightHiddenField.Value = testStatisticsSearchCriteria.IsMaximized == true ? "Y" : "N";
            // Apply search
            LoadValues(testStatisticsSearchCriteria.SortExpression, testStatisticsSearchCriteria.SortDirection, testStatisticsSearchCriteria.CurrentPage);

            // Highlight the last page number which is stored in session
            // when the page is launched from somewhere else.
            ViewState["CANDIDATE_PAGE_NO"] = testStatisticsSearchCriteria.CurrentPage.ToString();
            TestStatisticsInfo_candidateTabPanel_bottomPagingNavigator.MoveToPage(testStatisticsSearchCriteria.CurrentPage);
        }
        #endregion PrivateMethods

        #region Protected Methods                                              

        /// <summary>
        /// Method that retrieves the qualified status for cyber proctoring . This
        /// helps to show or hide the view cyber proctoring link icon in the 
        /// completed test grid section.
        /// </summary>
        /// <param name="cyberProctoringStatus">
        /// A <see cref="string"/> that holds the cyber proctoring status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the cyber proctoring status.
        /// </returns>
        protected bool IsCyberProctoring(string cyberProctoringStatus)
        {
            //return the corresponding cyber proctoring status
            return cyberProctoringStatus.ToUpper() == "TRUE" ? true : false;
        }

        #endregion Protected Methods

        #region Protected Overridden Methods                                   
        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            //declare the valid date as true
            bool isValidDate = true;

            //Validate the masked edit validator to validate the testDate text box
            TestStatisticsInfo_candidateTabPanel_maskedEditValidator.Validate();

            //Validate the masked edit validator to validate the to TestDate text box
            TestStatisticsInfo_candidateTabPanel_toDateMaskedEditValidator.Validate();

            if (TestStatisticsInfo_candidateTabPanel_testDateTextBox.Text != string.Empty)
            {

                if (!TestStatisticsInfo_candidateTabPanel_maskedEditValidator.IsValid)
                {
                    isValidDate = false;
                    base.ShowMessage(TestStatisticsInfo_candidateTabPanelErrorLabel,
                    Resources.HCMResource.TestStatisticsInfo_EnterCorrectDate);
                }
                else
                {
                    DateTime fromDate = Convert.ToDateTime
                       (TestStatisticsInfo_candidateTabPanel_testDateTextBox.Text.Trim());

                    if (fromDate < Convert.ToDateTime("1/1/1753"))
                    {
                        isValidDate = false;
                        base.ShowMessage(TestStatisticsInfo_candidateTabPanelErrorLabel,
                         Resources.HCMResource.
                         TestStatisticsInfo_EnterCorrectStartDateValidation);
                    }
                }
            }
            if (TestStatisticsInfo_candidateTabPanel_testToDateTextBox.Text != string.Empty)
            {
                if (!TestStatisticsInfo_candidateTabPanel_toDateMaskedEditValidator.IsValid)
                {
                    isValidDate = false;
                    base.ShowMessage(TestStatisticsInfo_candidateTabPanelErrorLabel,
                    Resources.HCMResource.
                    TestStatisticsInfo_EnterCorrectEndDate);
                }
                else
                {
                    DateTime toDate = Convert.ToDateTime
                        (TestStatisticsInfo_candidateTabPanel_testToDateTextBox.Text.Trim());

                    if (toDate > Convert.ToDateTime("12/31/9999"))
                    {
                        isValidDate = false;

                        base.ShowMessage(TestStatisticsInfo_candidateTabPanelErrorLabel,
                          Resources.HCMResource.
                            TestStatisticsInfo_EnterCorrectEndDateValidation);
                    }
                }
            }

            if (TestStatisticsInfo_candidateTabPanel_testDateTextBox.Text != string.Empty &&
               TestStatisticsInfo_candidateTabPanel_testToDateTextBox.Text != string.Empty)
            {
                if (TestStatisticsInfo_candidateTabPanel_toDateMaskedEditValidator.IsValid
                    && TestStatisticsInfo_candidateTabPanel_maskedEditValidator.IsValid)
                {
                    DateTime fromDate = Convert.ToDateTime
                    (TestStatisticsInfo_candidateTabPanel_testDateTextBox.Text.Trim());
                    DateTime toDate = Convert.ToDateTime
                        (TestStatisticsInfo_candidateTabPanel_testToDateTextBox.Text.Trim());

                    if (toDate < fromDate)
                    {
                        isValidDate = false;
                        base.ShowMessage(TestStatisticsInfo_candidateTabPanelErrorLabel,
                     Resources.HCMResource.
                    Test_StatisticsInfo_StartDateLessThanEndDate);

                    }
                }
            }

            return isValidDate;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion Protected Overridden Methods         
    }
}

