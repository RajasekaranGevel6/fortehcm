﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/ResumeRepositoryMaster.Master"
    AutoEventWireup="true" CodeBehind="CandidateDashboard.aspx.cs" Inherits="Forte.HCM.UI.ReportCenter.CandidateDashboard" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/ConfirmInterviewScoreMsgControl.ascx" TagName="ConfirmInterviewScoreMsgControl"
    TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="DeleteDocConfirmMsgControl"
    TagPrefix="uc4" %>
<%@ MasterType VirtualPath="../MasterPages/ResumeRepositoryMaster.Master" %>
<asp:Content ID="ViewCandidateActivityLog_bodyContent" ContentPlaceHolderID="ResumeRepositoryMaster_body"
    runat="server">
    <script type="text/javascript">
        function uploadError(sender, args) {
            //  __doPostBack("<%= CandidateDashboard_additionalDocSave_Button.ClientID %>", "");
            return false;
        }

        function OnClientAsyncResumeFileUploadComplete(sender, args) {
            var fileName = args.get_fileName();
            var fileExtn = (/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName) : undefined;

            if (fileExtn != "doc" && fileExtn != "docx" && fileExtn != "xls" && fileExtn != "xlsx"
            && fileExtn != "pdf" && fileExtn != "jpg" && fileExtn != "jpeg" && fileExtn != "png"
            && fileExtn != "gif" && fileExtn != "bmp" &&

            fileExtn != "DOC" && fileExtn != "DOCX" && fileExtn != "XLS" && fileExtn != "XLSX"
            && fileExtn != "PDF" && fileExtn != "JPG" && fileExtn != "JPEG" && fileExtn != "PNG"
            && fileExtn != "GIF" && fileExtn != "BMP"
            ) {
                var errMsg = "Please select valid file format.(.doc,.docx,.xls,.xlsx,.png,.jpg,.bmp and .pdf)";
                $get("<%=CandidateDashboard_errorLabel.ClientID%>").innerHTML = errMsg;
                args.set_cancel(true);
                var who1 = document.getElementsByName('<%=CandidateDashboard_additionalDoc_FileUpload.UniqueID %>')[0];
                who1.value = "";
                var who3 = who1.cloneNode(false);
                who3.onchange = who1.onchange;
                who1.parentNode.replaceChild(who3, who1);

                return false;
                alert(errMsg);
            }
        }

        // Submit the photo as soon as it was selected.
        function SubmitPhoto() {
            document.getElementById('<%=CandidateDashboard_photoUploadedHiddenFiled.ClientID %>').value = 'Y';
            __doPostBack('', '');
        }
        function OpenAddNotesCandidateActivity(candidateID) {
            var height = 260;
            var width = 620;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:"
                + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../Popup/AddNotes.aspx" +
            "?candidateid=" + candidateID;

            window.open(queryStringValue, window.self, sModalFeature);
        }

        function ResumeDivToggle(resumeDiv, skillDiv) {
            var candidateName = document.getElementById('<%=CandidateDashboard_candidateNameTextBox.ClientID %>');
            if (candidateName == null || candidateName.value.trim() == '') return;
            var state = document.getElementById(resumeDiv).style.display;
            if (state == 'block') {
                var userDiv = document.getElementById(resumeDiv);
                userDiv.style.display = 'none';
                userDiv.style.height = "0px";
                var upSpan = document.getElementById('<%=CandidateDashboard_displayResumesUpSpan.ClientID %>');
                upSpan.style.display = 'block';

                var downSpan = document.getElementById('<%=CandidateDashboard_displayResumesDownSpan.ClientID %>');
                downSpan.style.display = 'none';

            } else {
                var userDiv = document.getElementById(resumeDiv);
                userDiv.style.display = 'block';
                userDiv.style.height = '220px';
                userDiv.style.overflow = 'auto';
                userDiv.style.width = '900px';

                var upSpan = document.getElementById('<%=CandidateDashboard_displayResumesUpSpan.ClientID %>');
                upSpan.style.display = 'none';

                var downSpan = document.getElementById('<%=CandidateDashboard_displayResumesDownSpan.ClientID %>');
                downSpan.style.display = "block";
            }

            var skillDiv = document.getElementById(skillDiv);
            skillDiv.style.display = 'none';
            skillDiv.style.height = '0px';
            skillDiv.style.overflow = 'auto';
            skillDiv.style.width = '0px';

            var tr = document.getElementById('<%=CandidateDashboard_skillMatrixTR.ClientID %>');
            tr.style.display = 'none';
            return false;
        }

        function SkillDivToggle(skillDiv, resumeDiv) {

            var resumeDiv = document.getElementById(resumeDiv);
            resumeDiv.style.display = 'block';
            resumeDiv.style.height = '220px';
            resumeDiv.style.overflow = 'auto';
            resumeDiv.style.width = '900px';

            var upSpan = document.getElementById('<%=CandidateDashboard_displayResumesUpSpan.ClientID %>');
            upSpan.style.display = 'none';

            var downSpan = document.getElementById('<%=CandidateDashboard_displayResumesDownSpan.ClientID %>');
            downSpan.style.display = "block";

            var skillDiv = document.getElementById(skillDiv);
            skillDiv.style.display = 'block';
            skillDiv.style.height = '250px';
            skillDiv.style.overflow = 'auto';
            skillDiv.style.width = '900px';

            return false;
        }

        // Function that downloads the candidate resume.
        function DownloadCandidateResume(fileName, mimeType) {
            var url = '../Common/Download.aspx?fname=' + fileName + '&mtype=' + mimeType + '&type=DownloadResume';

            window.open(url, '', 'toolbar=0,resizable=0,width=1,height=1', '');
        }

        function DownloadDocument(documentId) {
            var url = '../Common/Download.aspx?type=additionaldoc&docpath=' + documentId;

            window.open(url, '', 'toolbar=0,resizable=0,width=1,height=1', '');
        }
        // Function that downloads the candidate resume.
        function DownloadResumeID(resumeID) {
            var url = '../Common/Download.aspx?id=' + resumeID;

            window.open(url, '', 'toolbar=0,resizable=0,width=1,height=1', '');

            var resumeDiv = document.getElementById('<%=CandidateDashboard_candidateResumesDiv.ClientID %>');
            resumeDiv.style.display = 'block';
            resumeDiv.style.height = '220px';
            resumeDiv.style.overflow = 'auto';
            resumeDiv.style.width = '900px';

            var resumeDiv = document.getElementById('<%=CandidateDashboard_candidateSkillMatrixDiv.ClientID %>');
            resumeDiv.style.display = 'block';
            resumeDiv.style.height = '220px';
            resumeDiv.style.overflow = 'auto';
            resumeDiv.style.width = '900px';
            /*resumeDiv.style.display = 'none';
            resumeDiv.style.height = '0px';
            resumeDiv.style.overflow = 'auto';
            resumeDiv.style.width = '0px';*/

            //var tblRow= document.getElementById('<%=CandidateDashboard_skillMatrixTR.ClientID%>');
            return false;
        }

        function ExpandOrRestore(ctrlExpandHide, expandImage, compressImage) {
            if (document.getElementById(ctrlExpandHide) != null) {
                var crtlStyle = document.getElementById(ctrlExpandHide).style.display;

                if (crtlStyle == "block") {
                    document.getElementById(ctrlExpandHide).style.display = "none";
                    document.getElementById(expandImage).style.display = "block";
                    document.getElementById(compressImage).style.display = "none";
                }
                else {
                    document.getElementById(ctrlExpandHide).style.display = "block";
                    document.getElementById(expandImage).style.display = "none";
                    document.getElementById(compressImage).style.display = "block";
                }
            }
            return false;
        }
    </script>
    <asp:UpdatePanel ID="CandidateDashboard_mainUpdatePanel" runat="server">
        <ContentTemplate>
            <table cellpadding="2" cellspacing="2" border="0" width="100%">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td align="center">
                                    <asp:UpdatePanel ID="CandidateDashboard_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Label ID="CandidateDashboard_errorLabel" runat="server" SkinID="sknErrorMessage" />
                                            <asp:Label ID="CandidateDashboard_SuccessMessageLabel" runat="server" SkinID="sknSuccessMessage" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="CandidateDashboard_candidateResumesDataList" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="header_bg">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td class="header_text_bold" width="25%">
                                                <asp:Label ID="CandidateDashboard_titleLabel" runat="server" Text="Candidate Activity Dashboard"></asp:Label>
                                            </td>
                                            <td align="right">
                                                <asp:ImageButton ID="CandidateDashboard_candidateImageButton" ToolTip="Click here to select the candidate"
                                                    runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/client_view.gif" Style="cursor: hand" />
                                                <div style="display: none">
                                                    <asp:TextBox ID="CandidateDashboard_candidateNameTextBox" runat="server"></asp:TextBox>
                                                    <asp:Button ID="CandidateDashboard_candidateActvityButton" runat="server" Text="Button"
                                                        OnClick="CandidateDashboard_candidateActvityButton_Click" />
                                                </div>
                                            </td>
                                            <td align="center" width="5%">
                                                <asp:LinkButton ID="CandidateDashboard_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="CandidateDashboard_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td class="link_button" align="center" width="1%">
                                                |
                                            </td>
                                            <td align="center" width="5%">
                                                <asp:LinkButton ID="CandidateDashboard_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                    Text="Cancel" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="candidate_dashboard_header_bg">
                                    <table cellpadding="0" cellspacing="0" border="0" width="96%">
                                        <tr>
                                            <td align="left" width="600px">
                                                <asp:UpdatePanel ID="CandidateDashboard_candidateInformation_UpdatePanel" runat="server"
                                                    UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td width="20%">
                                                                    <asp:Image ID="CandidateDashboard_candidatePhotoImage" runat="server" ImageUrl="~/Common/CandidateImageHandler.ashx?source=CAND_DASH_PAGE"
                                                                        AlternateText="Photo not available" />
                                                                </td>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" border="0" width="300px">
                                                                                    <tr>
                                                                                        <td width="240px" class="candidate_dashboardname_text_bold">
                                                                                            <asp:Label ID="CandidateDashboard_candidateNameLabel" runat="server" Text=""></asp:Label>
                                                                                            <asp:HiddenField ID="CandidateDashboard_candidateIDHiddenField" runat="server" />
                                                                                        </td>
                                                                                        <td width="10px">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <td width="50px">
                                                                                            <asp:LinkButton ID="CandidateDashboard_candidateEditLinkButton" SkinID="sknActionLinkButton"
                                                                                                runat="server" ToolTip="Click here to edit the candidate first name and last name"
                                                                                                OnClick="CandidateDashboard_candidateEditLinkButton_Click">Edit</asp:LinkButton>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <div id="CandidateDashboard_candidateNameChangeDiv" runat="server" style="display: block">
                                                                                                <table cellpadding="0" cellspacing="0" border="0" height="22px" width="300px">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:TextBox ID="CandidateDashboard_candidateFirstNameTextBox" SkinID="sknHomePageTextBox"
                                                                                                                Width="130px" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td width="2px">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:TextBox ID="CandidateDashboard_candidateLastNameTextBox" SkinID="sknHomePageTextBox"
                                                                                                                Width="130px" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td width="2px">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:LinkButton ID="CandidateDashboard_candidateUpdateLinkButton" SkinID="sknAddLinkButton"
                                                                                                                runat="server" ToolTip="Click here to update the candidate first name and last name"
                                                                                                                OnClick="CandidateDashboard_candidateUpdateLinkButton_Click">Update</asp:LinkButton>
                                                                                                        </td>
                                                                                                        <td width="2px">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:LinkButton ID="CandidateDashboard_candidateCancelLinkButton" SkinID="sknActionLinkButton"
                                                                                                                runat="server" ToolTip="Click here to cancel the updation" OnClick="CandidateDashboard_candidateCancelLinkButton_Click">Cancel</asp:LinkButton>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="candidate_dashboardname_text">
                                                                                <asp:Label ID="CandidateDashboard_mobileNoLabel" runat="server" Text=""></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="candidate_dashboardname_text">
                                                                                <asp:HyperLink ID="CandidateDashboard_emailIDHyperLink" runat="server" ToolTip="Click here to send email to candidate"></asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="candidate_dashboardname_text">
                                                                                <asp:Label ID="CandidateDashboard_recruitedByLabel" runat="server" Text=""></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" align="left">
                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td align="left" colspan="2">
                                                                                <asp:Label ID="CandidateDashboard_selectPhotoLabel" runat="server" Text="Select Photo"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 25%" align="left">
                                                                                <asp:Button ID="CandidateDashboard_browseButton" runat="server" Visible="false" OnClientClick="javascript:return document.getElementById('<%= this.CandidateDashboard_selectPhotoFileUpload.ClientID %>').click();"
                                                                                    Text="Browse..." />
                                                                                <asp:FileUpload ID="CandidateDashboard_selectPhotoFileUpload" size="1" type="file"
                                                                                    runat="server" onchange="javascript:SubmitPhoto()" Style="visibility: visible;" />
                                                                                <asp:HiddenField runat="server" ID="CandidateDashboard_photoUploadedHiddenFiled" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="CandidateDashboard_candidateActvityButton" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td width="80px" align="right">
                                                <%--  <span id="CandidateDashboard_skillMatrixUpSpan" runat="server"  style="display: block;">
                                                    <asp:ImageButton ID="CandidateDashboard_skillMatrixUpImage" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/skill_matrixup.png"
                                                         ToolTip="Click here to expand the skill matrix"
                                                        OnClientClick="javascript:return toggle('CandidateDashboard_candidateSkillMatrixDiv');"/>
                                                </span>
                                                <span id="CandidateDashboard_skillMatrixDownSpan" runat="server" style="display: none;">
                                                    <asp:ImageButton ID="SearchCandidateRepository_skillMatrixDownImage" runat="server"
                                                        ImageUrl="~/App_Themes/DefaultTheme/Images/skill_matrixdown.png"  
                                                        ToolTip="Click here to shrink the skill matrix" OnClientClick="javascript:return toggle('CandidateDashboard_candidateSkillMatrixDiv');" />
                                                </span>--%>
                                            </td>
                                            <td align="right" width="80px">
                                                &nbsp; <span id="CandidateDashboard_displayResumesUpSpan" runat="server" style="display: block;">
                                                    <asp:ImageButton ID="CandidateDashboard_expandResumesList_ImageButton" runat="server"
                                                        ToolTip="Click here to expand the resumes list" ImageUrl="~/App_Themes/DefaultTheme/Images/document_download_down.png" />
                                                </span><span id="CandidateDashboard_displayResumesDownSpan" runat="server" style="display: none;">
                                                    <asp:ImageButton ID="CandidateDashboard_closeResumesList_ImageButton" runat="server"
                                                        ToolTip="Click here to shrink the resumes list" ImageUrl="~/App_Themes/DefaultTheme/Images/document_download_up.png" />
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" border="0" width="96%">
                                        <tr>
                                            <td style="padding-left: 1px; padding-top: 20px">
                                                <div id="CandidateDashboard_candidateResumesDiv" runat="server" style="height: 220px">
                                                    <asp:DataList ID="CandidateDashboard_candidateResumesDataList" runat="server" CellPadding="5"
                                                        CellSpacing="10" BackColor="#E7EDEF" RepeatColumns="3" RepeatDirection="Horizontal"
                                                        ShowFooter="false" ShowHeader="false" BorderStyle="None" Width="100%" OnItemDataBound="CandidateDashboard_candidateResumesDataList_ItemDataBound">
                                                        <SelectedItemStyle BackColor="Yellow" />
                                                        <ItemTemplate>
                                                            <table cellpadding="1" cellspacing="3" border="0" width="100%">
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <asp:Label ID="CandidateDashboard_resumeNameLabel" CssClass="candidate_dashboard_GRID_text_bold"
                                                                            runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <asp:Label ID="CandidateDashboard_uploadedByLabel" SkinID="sknLabelText" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <asp:Label ID="CandidateDashboard_processDateLabel" SkinID="sknLabelText" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" align="right">
                                                                        <asp:HiddenField ID="CandidateDashboard_resumeCandidateIDHiddenField" runat="server" />
                                                                        <asp:HiddenField ID="CandidateDashboard_resumeIDHiddenField" runat="server" />
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="40%">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:ImageButton ID="CandidateDashboard_downloadResumeImageButton" runat="server"
                                                                                        CommandName="Resume" ImageUrl="~/App_Themes/DefaultTheme/Images/download.png"
                                                                                        ToolTip="Click here to download the resume" />
                                                                                </td>
                                                                                <td width="1%">
                                                                                </td>
                                                                                <td>
                                                                                    <asp:ImageButton ID="CandidateDashboard_viewResumeSkillImageButton" runat="server"
                                                                                        CommandName="SkillMatrix" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_skilldetail.gif"
                                                                                        ToolTip="Click here to view the resume skill matrix" OnCommand="CandidateDashboard_viewResumeSkillImageButton_Command" />
                                                                                </td>
                                                                                <td width="1%">
                                                                                </td>
                                                                                <td>
                                                                                    <asp:ImageButton ID="CandidateDashboard_deleteResumeImageButton" runat="server" CommandName="DeleteResume"
                                                                                        CommandArgument='<%# Eval("CandidateResumeId") %>' SkinID="sknDeleteResumeImageButton"
                                                                                        ToolTip="Click here to delete the resume" Visible='<%# IsAllowDeleteResume(Convert.ToInt32(Eval("CandidateResumeId")), Convert.ToInt32(Eval("Recruiterid")))%>'
                                                                                        OnCommand="CandidateDashboard_deleteResumeImageButton_Command" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <ItemStyle BorderStyle="Solid" BorderWidth="1px" Width="30%" BorderColor="#D8D8D8"
                                                            BackColor="#ffffff" />
                                                    </asp:DataList>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="CandidateDashboard_deleteResumeUpdatePanel" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div style="float: left;">
                                                            <asp:Panel ID="CandidateDashboard_deletePanel" runat="server" CssClass="client_confirm_message_box"
                                                                Style="display: none;">
                                                                <div id="CandidateDashboard_deleteConfirmationDiv" style="display: none">
                                                                    <asp:Button ID="CandidateDashboard_deleteConfirmation_hiddenButton" runat="server" />
                                                                </div>
                                                                <uc3:ConfirmMsgControl ID="CandidateDashboard_deleteResume_confirmMessageControl"
                                                                    runat="server" Title="Delete candidate resume" Type="YesNo" OnOkClick="CandidateDashboard_deleteResume_okButtonClick" />
                                                            </asp:Panel>
                                                            <ajaxToolKit:ModalPopupExtender ID="CandidateDashboard_deleteResume_confirmModalPopupExtender"
                                                                runat="server" PopupControlID="CandidateDashboard_deletePanel" TargetControlID="CandidateDashboard_deleteConfirmation_hiddenButton"
                                                                BackgroundCssClass="modalBackground">
                                                            </ajaxToolKit:ModalPopupExtender>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="10px">
                                            </td>
                                        </tr>
                                        <tr id="CandidateDashboard_skillMatrixTR" runat="server" width="900px">
                                            <td align="right" valign="middle" style="padding-left: 10px">
                                                <table cellpadding="0" cellspacing="0" border="0" width="890px">
                                                    <tr class="header_bg">
                                                        <td width="98%">
                                                            &nbsp;
                                                        </td>
                                                        <td style="padding-bottom: 5px; padding-top: 5px">
                                                            <span id="SkillsMatrixControl_resumeUpSpan" runat="server" style="display: block;">
                                                                <asp:Image ID="SkillsMatrixControl_resumeUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                            </span><span id="SkillsMatrixControl_resumeDownSpan" runat="server" style="display: none;">
                                                                <asp:Image ID="SkillsMatrixControl_resumeDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 10px; padding-bottom: 15px; padding-top: 25px" width="100%">
                                                <div id="CandidateDashboard_candidateSkillMatrixDiv" runat="server" style="height: 220px;
                                                    width: 880px">
                                                    <asp:UpdatePanel ID="CandidateDashboard_skillMatrixUpdatePanel" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="CandidateDashboard_skillMatrixGridView" runat="server" BorderStyle="None"
                                                                AutoGenerateColumns="true" CellPadding="0" CellSpacing="0" BorderWidth="0" OnPreRender="CandidateDashboard_skillMatrixGridView_PreRender"
                                                                Width="880px" OnRowDataBound="CandidateDashboard_skillMatrixGridView_RowDataBound">
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="CandidateDashboard_candidateResumesDataList" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="10px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr id="tr_CandidateAdditionalDoc" runat="server">
                                                                    <td class="header_text_bold" width="25%">
                                                                        <asp:Label ID="CandidateDashboard_additionalDocTitleLabel" runat="server" Text="Candidate Additional Documents"></asp:Label>
                                                                    </td>
                                                                    <td align="right" style="width: 2%">
                                                                        <span id="CandidateDashboard_additionalDocUpSpan" runat="server" style="display: none;">
                                                                            <asp:Image ID="CandidateDashboard_additionalDocUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                        </span>
                                                                        <span id="CandidateDashboard_additionalDocDownSpan" runat="server" style="display: block;">
                                                                            <asp:Image ID="CandidateDashboard_additionalDocDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                        </span>
                                                                        <asp:HiddenField ID="CandidateDashboard_additionalDocrestoreHiddenField" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="candidate_dashboard_header_bg_grid" id="td_CandidateAdditionalDoc" runat="server" style="display:none">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td style="width: 18%">
                                                                        <asp:Label ID="CandidateDashboard_additionalDoctitle_Label" runat="server" Text="Upload additional documents "></asp:Label>
                                                                    </td>
                                                                    <td style="width: 45%; padding: 5px">
                                                                        <ajaxToolKit:AsyncFileUpload ID="CandidateDashboard_additionalDoc_FileUpload" runat="server"
                                                                            Width="410" Height="24px" ThrobberID="Throbber" FailedValidation="False" PersistFile="false"
                                                                            SkinID="sknCanTextBox" OnUploadedComplete="ResumeEditor_fileUpload_UploadedComplete"
                                                                            ClientIDMode="AutoID" OnClientUploadError="uploadError" OnClientUploadComplete="OnClientAsyncResumeFileUploadComplete" />
                                                                    </td>
                                                                    <td style="width: 47%; visibility: visible; padding: 5px" align="left" valign="top">
                                                                        <asp:Button ID="CandidateDashboard_additionalDocSave_Button" runat="server" Text="Upload"
                                                                            SkinID="sknButtonId" UseSubmitBehavior="false" OnClick="CandidateDashboard_additionalDocSave_Button_Click" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <div id="CandidateDashboard_additionalDocDiv" runat="server" style="height: 200px;
                                                                            padding-top: 10px; overflow: auto">
                                                                            <asp:UpdatePanel ID="CandidateDashboard_additionalDoc_UpdatePanel" runat="server"
                                                                                UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:DataList ID="CandidateDashboard_additionalDocDataList" runat="server" CellPadding="5"
                                                                                        CellSpacing="10" BackColor="#E7EDEF" RepeatColumns="3" RepeatDirection="Horizontal"
                                                                                        ShowFooter="false" ShowHeader="false" BorderStyle="None" Width="100%" OnItemDataBound="CandidateDashboard_additionalDocDataList_ItemDataBound"
                                                                                        OnItemCommand="CandidateDashboard_additionalDocDataList_ItemCommand">
                                                                                        <SelectedItemStyle BackColor="Yellow" />
                                                                                        <ItemTemplate>
                                                                                            <table cellpadding="1" cellspacing="3" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td colspan="2">
                                                                                                        <asp:Label ID="CandidateDashboard_additionalDocNameLabel" CssClass="candidate_dashboard_GRID_text_bold"
                                                                                                            runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2">
                                                                                                        <asp:Label ID="CandidateDashboard_additionalDocUploadedByLabel" SkinID="sknLabelText"
                                                                                                            runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2">
                                                                                                        <asp:Label ID="CandidateDashboard_additionalDocProcessDateLabel" SkinID="sknLabelText"
                                                                                                            runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" align="right">
                                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="40%">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:ImageButton ID="CandidateDashboard_additionalDocDownloadImageButton" runat="server"
                                                                                                                        CommandName="Download" ImageUrl="~/App_Themes/DefaultTheme/Images/download.png"
                                                                                                                        ToolTip="Click here to download the document" />
                                                                                                                </td>
                                                                                                                <td width="1%">
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:ImageButton ID="CandidateDashboard_additionalDocDeleteResumeImageButton" runat="server"
                                                                                                                        CommandName="DeleteDoc" CommandArgument='<%# Eval("DocumentID") %>' SkinID="sknDeleteResumeImageButton"
                                                                                                                        ToolTip="Click here to delete the document" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle BorderStyle="Solid" BorderWidth="1px" Width="30%" BorderColor="#D8D8D8"
                                                                                            BackColor="#ffffff" />
                                                                                    </asp:DataList>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="CandidateDashboard_additionalDocSave_Button" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:UpdatePanel ID="AdditionalDoc_deleteUpdatePanel" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <div style="float: left;">
                                                                        <asp:Panel ID="AdditionalDoc_deletePanel" runat="server" CssClass="client_confirm_message_box"
                                                                            Style="display: none;">
                                                                            <div id="AdditionalDoc_Div" style="display: none">
                                                                                <asp:Button ID="additionalDoc_deleteButton" runat="server" />
                                                                            </div>
                                                                            <uc4:DeleteDocConfirmMsgControl ID="AdditionalDoc_deleteConfirmMsgControl" runat="server"
                                                                                Title="Delete candidate document" Type="YesNo" OnOkClick="AdditionalDoc_deleteConfirmMsgControl_okButtonClick" />
                                                                        </asp:Panel>
                                                                        <ajaxToolKit:ModalPopupExtender ID="AdditionalDoc_deleteModalPopupExtender" runat="server"
                                                                            PopupControlID="AdditionalDoc_deletePanel" TargetControlID="additionalDoc_deleteButton"
                                                                            BackgroundCssClass="modalBackground">
                                                                        </ajaxToolKit:ModalPopupExtender>
                                                                    </div>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="candidate_dashboard_header_bg_grid">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr height="1px">
                                <td width="46%">
                                </td>
                                <td width="2%">
                                </td>
                                <td width="42%">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Label ID="CandidateDashboard_searchTitleLabel" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding-top: 10px; padding-bottom: 15px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td width="30%" align="right" class="header_text_bold" style="padding-right: 10px;">
                                                <asp:Label ID="CandidateDashboard_keywordsLabel" runat="server" Text="Keywords "></asp:Label>
                                            </td>
                                            <td width="40%">
                                                <asp:TextBox ID="CandidateDashboard_searchKeywordsTextBox" CssClass="text_box" Width="370px"
                                                    Height="20px" runat="server" MaxLength="500"></asp:TextBox>
                                            </td>
                                            <td width="30%" valign="middle">
                                                <asp:ImageButton ID="CandidateDashboard_searchImageButton" runat="server" ImageUrl="~/Images/icon_sign_up.png"
                                                    OnClick="CandidateDashboard_searchImageButton_Click" Style="margin-left: 0px"
                                                    ToolTip="Click here to search the candidate activities" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="44%" style="padding-left: 5px; vertical-align: top">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td class="header_bg">
                                                <asp:Label ID="CandidateDashboard_intelliTestLabel" runat="server" Text="intelliTEST"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="candidate_dashboard_intellitag_bg" valign="top" align="center">
                                                <asp:UpdatePanel ID="CandidateDashboard_intelliTEST_UpdatePanel" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Label ID="CandidateDashboard_intelliTEST_messgeLabel" runat="server" Text=""
                                                            Visible="false" SkinID="sknErrorMessage"></asp:Label>
                                                        <asp:GridView ID="CandidateDashboard_intelliTESTGridView" runat="server" AutoGenerateColumns="False"
                                                            Width="100%" BackColor="#E7EDEF" ShowHeader="false" ShowFooter="false" CellPadding="5"
                                                            CellSpacing="10" SkinID="sknCompetency" OnRowCreated="CandidateDashboard_intelliTESTGridView_RowCreated"
                                                            OnRowDataBound="CandidateDashboard_intelliTESTGridView_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="CandidateDashboard_candidateSessionIDHiddenField" runat="server"
                                                                            Value='<%# Eval("CandidateTestSessionID") %>' />
                                                                        <asp:HiddenField ID="CandidateDashboard_testIDHiddenField" runat="server" Value='<%# Eval("TestID") %>' />
                                                                        <asp:HiddenField ID="CandidateDashboard_attemptIDHiddenField" runat="server" Value='<%# Eval("AttemptID") %>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="templateVisibility" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <table cellpadding="0" cellspacing="3" border="0" runat="server" id="CandidateDashboard_intelliTESTTable"
                                                                            width="100%" height="130px">
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <asp:Label ID="CandidateDashboard_testNameLabel" runat="server" Text='<%# Eval("TestName") %>'
                                                                                        CssClass="candidate_dashboard_GRID_text_bold"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="50%">
                                                                                    <asp:Label ID="CandidateDashboard_completedDateLabel" runat="server" Text='<%# Eval("CompletedDate") %>'></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="CandidateDashboard_scheduledByLabel" runat="server" Text='<%# Eval("SchedulerName") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <asp:Label ID="CandidateDashboard_profileNameLabel" CssClass="label_field_text" runat="server"
                                                                                        Text='<%# Eval("ProfileName") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <asp:Label ID="CandidateDashboard_clientNameLabel" CssClass="label_field_text" runat="server"
                                                                                        Text='<%# Eval("ClientName") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <asp:Label ID="CandidateDashboard_scoreLabel" runat="server" Text="Absolute Score "></asp:Label>
                                                                                    <asp:Label ID="CandidateDashboard_scoreValueLabel" runat="server" CssClass="candidate_dashboard_GRID_SCORE_bold"
                                                                                        Text='<%# Eval("AbsoluteScore") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" align="right">
                                                                                    <table cellpadding="1" cellspacing="1" border="0" align="right">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:HyperLink ID="CandidateDashboard_testReportHyperLink" runat="server" ToolTip="Click here to view candidate test result summary"
                                                                                                    Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/test_candidate_report_icon.png">
                                                                                                </asp:HyperLink>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:HyperLink ID="CandidateDashboard_testCandidateReportHyperLink" runat="server"
                                                                                                    ToolTip="Click here to view candidate test results" Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/test_report_icon.png">
                                                                                                </asp:HyperLink>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                    <ItemStyle BorderStyle="Solid" BorderWidth="1px" BorderColor="#D8D8D8" BackColor="#ffffff" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                        <uc1:PageNavigator ID="CandidateDashboard_intelliTESTPageNavigator" runat="server" />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="CandidateDashboard_searchImageButton" />
                                                        <asp:AsyncPostBackTrigger ControlID="CandidateDashboard_candidateActvityButton" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="2%">
                                    &nbsp;
                                </td>
                                <td width="40%" style="padding-left: 5px; vertical-align: top">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td class="header_bg">
                                                <asp:Label ID="CandidateDashboard_intelliViewLabel" runat="server" Text="intelliVIEW"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="candidate_dashboard_intellitag_bg" valign="top" align="center">
                                                <asp:UpdatePanel ID="CandidateDashboard_intelliVIEW_UpdatePanel" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Label ID="CandidateDashboard_intelliVIEW_messageLabel" runat="server" SkinID="sknErrorMessage"
                                                            Text="" Visible="false"></asp:Label>
                                                        <asp:GridView ID="CandidateDashboard_intelliVIEWGridView" runat="server" AutoGenerateColumns="False"
                                                            Width="100%" SkinID="sknCompetency" BackColor="#E7EDEF" ShowHeader="false" ShowFooter="false"
                                                            CellPadding="5" CellSpacing="10" OnRowCreated="CandidateDashboard_intelliVIEWGridView_RowCreated"
                                                            OnRowDataBound="CandidateDashboard_intelliVIEWGridView_RowDataBound" OnRowCommand="CandidateDashboard_intelliVIEWGridView_RowCommand">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="CandidateDashboard_intelliVIEWGridView_candidateSessionIDHiddenField"
                                                                            runat="server" Value='<%# Eval("CandidateInterviewSessionID") %>' />
                                                                        <asp:HiddenField ID="CandidateDashboard_intelliVIEWGridView_testkeyHiddenField" runat="server"
                                                                            Value='<%# Eval("InterviewTestKey") %>' />
                                                                        <asp:HiddenField ID="CandidateDashboard_intelliVIEWGridView_sessionkeyHiddenField"
                                                                            runat="server" Value='<%# Eval("InterviewSessionKey") %>' />
                                                                        <asp:HiddenField ID="CandidateDashboard_intelliVIEWGridView_assessorIDHiddenField"
                                                                            runat="server" Value='<%# Eval("AssessorID") %>' />
                                                                        <asp:HiddenField ID="CandidateDashboard_intelliVIEWGridView_attemptIDHiddenField"
                                                                            runat="server" Value='<%# Eval("AttemptID") %>' />
                                                                        <asp:HiddenField ID="CandidateDashboard_intelliVIEWGridView_publishHiddenField" runat="server" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle BorderStyle="None" CssClass="templateVisibility" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <table cellpadding="0" cellspacing="3" border="0" runat="server" id="CandidateDashboard_intelliVIEWTable"
                                                                            width="100%" height="130px">
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <asp:Label ID="CandidateDashboard_interviewNameLabel" runat="server" Text='<%# Eval("InterviewTestName") %>'
                                                                                        CssClass="candidate_dashboard_GRID_text_bold"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="50%">
                                                                                    <asp:Label ID="CandidateDashboard_interviewCompletedDateLabel" runat="server" Text='<%# Eval("CompletedDate") %>'></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="CandidateDashboard_interviewScheduledByLabel" runat="server" Text='<%# Eval("SchedulerName") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <asp:Label ID="CandidateDashboard_interviewProfileNameLabel" CssClass="label_field_text"
                                                                                        runat="server" Text='<%# Eval("PositionProfileName") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <asp:Label ID="CandidateDashboard_interviewClientNameLabel" CssClass="label_field_text"
                                                                                        runat="server" Text='<%# Eval("ClientName") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" align="right">
                                                                                    <table cellpadding="0" cellspacing="5" border="0" align="right">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:HyperLink ID="CandidateDashboard_intelliVIEWGridView_interviewResultHyperLink"
                                                                                                    runat="server" ToolTip="Click here to view candidate interview result summary"
                                                                                                    Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/talent_search_icon.png"></asp:HyperLink>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:HyperLink ID="CandidateDashboard_intelliVIEWGridView_interviewReportHyperLink"
                                                                                                    runat="server" Target="_blank" ToolTip="Click here to view candidate interview assessment summary"
                                                                                                    ImageUrl="~/App_Themes/DefaultTheme/Images/test_report_icon.png"></asp:HyperLink>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:ImageButton ID="CandidateDashboard_interviewPublishImage" runat="server" ToolTip="Click here to publish candidate interview response"
                                                                                                    ImageUrl="~/App_Themes/DefaultTheme/Images/publish.png" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                                                                                    CommandName="Publish" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                    <ItemStyle BorderStyle="Solid" BorderWidth="1px" BorderColor="#D8D8D8" BackColor="#ffffff" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                        <uc1:PageNavigator ID="CandidateDashboard_intelliVIEWPageNavigator" runat="server" />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="CandidateDashboard_searchImageButton" />
                                                        <asp:AsyncPostBackTrigger ControlID="CandidateDashboard_candidateActvityButton" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="CandidateDashboard_showPublishUrlUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <div style="float: left;">
                                                            <asp:Panel ID="CandidateDashboard_emailConfirmationPanel" runat="server" CssClass="popupcontrol_confirm_publish_candidate_interview_response"
                                                                Style="display: none; height: 254px;">
                                                                <div id="CandidateDashboard_emailConfirmationDiv" style="display: none">
                                                                    <asp:Button ID="CandidateDashboard_emailConfirmation_hiddenButton" runat="server" />
                                                                </div>
                                                                <uc2:ConfirmInterviewScoreMsgControl ID="CandidateDashboard_ConfirmMsgControl" runat="server"
                                                                    Title="Publish Candidate Interview Response" Type="InterviewScoreConfirmType" />
                                                            </asp:Panel>
                                                            <ajaxToolKit:ModalPopupExtender ID="CandidateDashboard_emailConfirmation_ModalPopupExtender"
                                                                runat="server" PopupControlID="CandidateDashboard_emailConfirmationPanel" TargetControlID="CandidateDashboard_emailConfirmation_hiddenButton"
                                                                BackgroundCssClass="modalBackground">
                                                            </ajaxToolKit:ModalPopupExtender>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding-top: 10px">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td class="header_bg">
                                                <asp:Label ID="CandidateDashboard_candidateActivityLog_TitleLabel" runat="server"
                                                    Text="Candidate Activity Log" Style="font-weight: bold"></asp:Label>
                                                &nbsp;
                                                <asp:Label ID="CandidateDashboard_candidateActivityLog_titleSortLabel" runat="server"
                                                    SkinID="sknLabelText" Text="- Click column headers to sort"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="candidate_dashboard_header_bg_grid">
                                                <asp:UpdatePanel ID="CandidateDashboard_candidateActivityGridView_UpdatePanel" runat="server"
                                                    UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div style="overflow: auto; height: 250px">
                                                            <asp:GridView ID="CandidateDashboard_candidateActivityGridView" runat="server" AutoGenerateColumns="False"
                                                                CellPadding="0" CellSpacing="0" EnableModelValidation="false" Width="100%" AllowSorting="true"
                                                                OnSorting="CandidateDashboard_candidateActivityGridView_Sorting" OnRowCreated="CandidateDashboard_candidateActivityGridView_RowCreated">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Activity Date" SortExpression="ACTIVITY_DATE" HeaderStyle-Width="18%"
                                                                        ItemStyle-Width="18%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="CandidateActivityViewerControl_activityLogDetailGridView_activityDateLabel"
                                                                                runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("ActivityDate")))%>'>
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Activity By" SortExpression="ACTIVITY_BY_NAME" HeaderStyle-Width="18%"
                                                                        ItemStyle-Width="18%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="CandidateActivityViewerControl_activityLogDetailGridView_activityByLabel"
                                                                                runat="server" Text='<%# Eval("ActivityByName") %>'>
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Activity" SortExpression="ACTIVITY_TYPE" ItemStyle-Width="18%"
                                                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="18%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="CandidateActivityViewerControl_activityLogDetailGridView_activityTypeLabel"
                                                                                runat="server" Text='<%# Eval("ActivityTypeName") %>'>
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Comments" SortExpression="COMMENTS" ItemStyle-Width="18%"
                                                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="18%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="CandidateActivityViewerControl_activityLogDetailGridView_commentsLabel"
                                                                                runat="server" Text='<%# Eval("Comments") %>'>
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                        <uc1:PageNavigator ID="CandidateDashboard_candidateActivityPageNavigator" runat="server" />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="CandidateDashboard_searchImageButton" />
                                                        <asp:AsyncPostBackTrigger ControlID="CandidateDashboard_candidateActvityButton" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="5px">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <asp:UpdatePanel ID="CandidateDashboard_exportUpdatePanel" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:HyperLink ID="AssessInterviewResponse_exportExcelHyperLink" runat="server"></asp:HyperLink>
                                                                    <asp:ImageButton ID="CandidateDashboard_exportExcelImageButton" runat="server" ToolTip="Click here to export to excel"
                                                                        ImageUrl="~/App_Themes/DefaultTheme/Images/create_pp_icon.png" Text="excel" />&nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:ImageButton ID="CandidateDashboard_addNotesImageButton" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/search_existing_test_icon.gif"
                                                                        ToolTip="Click here to add notes to candidate" OnClick="CandidateDashboard_addNotesImageButton_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="header_bg">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td class="header_text_bold" width="25%">
                                </td>
                                <td align="right">
                                </td>
                                <td align="center" width="5%">
                                    <asp:LinkButton ID="CandidateDashboard_bottomResetLinkButton" runat="server" Text="Reset"
                                        SkinID="sknActionLinkButton" OnClick="CandidateDashboard_resetLinkButton_Click"></asp:LinkButton>
                                </td>
                                <td class="link_button" align="center" width="1%">
                                    |
                                </td>
                                <td align="center" width="5%">
                                    <asp:LinkButton ID="CandidateDashboard_bottomCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                        Text="Cancel" OnClick="ParentPageRedirect" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
