﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// DesignReport.cs
// File that represents the Design Reports based on the test and canditate.
// This will helps take Email and download as a PDF file.
//

#endregion

#region Directives

using System;
using System.Threading;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Kalitte;
using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;

#endregion

namespace Forte.HCM.UI.ReportCenter
{
    /// <summary>                                                  
    /// Class that represents the user interface layout and functionalities
    /// for the DesignReport page. This page helps view various report for 
    /// Candidate against the Test
    /// This class inherits the DashboardPage class.
    /// </summary>
    public partial class DesignReportPrint : DashboardPage
    {
        #region Even Handling

        /// <summary>
        /// Override the OnInit method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnInit(EventArgs e)
        {
            Session["PRINTER"] = true;
            Surface1.DashboardKey = Request.QueryString["dashboardkey"];
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            PrintMethod();
        }

        private void PrintMethod()
        {
            List<Dictionary<object, object>> dictionaryList = new List<Dictionary<object, object>>();
            dictionaryList = (List<Dictionary<object, object>>)Session["DICTINARYLIST"];
            for (int i = 0; i < Surface1.GetWidgets().Count; i++)
            {
                if (Surface1.GetWidgets()[i].WidgetControl.FindControl("WidgetMultiSelectControl") != null)
                {
                    TextBox textBox = (TextBox)Surface1.GetWidgets()[i].WidgetControl.FindControl(
                        "WidgetMultiSelectControlPrint").FindControl("WidgetMultiSelectControl_propertyTextBox");

                    if (textBox != null)
                    {
                        textBox.Text = "";
                        foreach (KeyValuePair<object, object> kvp in dictionaryList[i])
                        {
                            textBox.Text = textBox.Text + kvp.Value.ToString() + ",";
                        }
                    }
                }
            }
        }

        #region Widget Related Events
        /// <summary>
        /// set the widget surface.
        /// </summary>
        protected override DashboardSurface Dashboard
        {
            get { return Surface1; }
        }
        /// <summary>
        /// Widget Properties Setting.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="WidgetEventArgs"/> that holds the event data.
        /// </param>
        protected void surface_WidgetPropertiesSetting(object sender, WidgetEventArgs e)
        {
        }

        /// <summary>
        /// Widget Type Menu Prepare.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="WidgetTypeMenuPrepareEventArgs"/> that holds the event data.
        /// </param>
        protected void surface_WidgetTypeMenuPrepare(object sender, WidgetTypeMenuPrepareEventArgs e)
        {
        }

        /// <summary>
        /// Dashboard Bottombar Prepare.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="DashboardToolbarPrepareEventArgs"/> that holds the event data.
        /// </param>
        protected void surface_DashboardBottombarPrepare(object sender, DashboardToolbarPrepareEventArgs e)
        {
        }

        #endregion

        /// <summary>
        ///  Override the OnPreRender method
        /// </summary>
        /// A <see cref="EventArgs"/>that holds the event data.
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            Surface1.HideEditor();
        }
        #endregion
    }
}




