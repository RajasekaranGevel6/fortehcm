﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateTestDetails.cs
// File that represents the Candidate Test question Details with answers.
// y.
//

#endregion Header

#region Directives                                                             

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Data;
using System.Web;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;

using iTextSharp.text;
using iTextSharp.text.pdf;

#endregion Directives

namespace Forte.HCM.UI.ReportCenter
{
    public partial class CandidateTestDetails : PageBase
    {

        #region Declaration

        public string TestQuestionType
        {
            get { return Session["questionType"].ToString(); }
            set { Session["questionType"] = value; }
        }

        #endregion
       

        #region Event Handler                                                  
        
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["Questiontype"] != null)
                    {
                        TestQuestionType = Request.QueryString["Questiontype"].ToString();
                    }
                    LoadValues();
                }

                if (Request.QueryString["parentpage"] != null &&
                    Request.QueryString["parentpage"] == "mail")
                {
                    // Hide menus.
                    Master.HideMenu();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CanidateTestDetails_topErrorMessageLabel,
                CanidateTestDetails_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the RowDataBound event of the CanidateTestDetails_testDrftGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void CanidateTestDetails_testDrftGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                // Find the row type is DataRow
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField questionKey = (HiddenField)e.Row.FindControl("CandidateTestDetails_questionKey");
                    QuestionDetail questionDetail = new ReportBLManager().GetSingleQuestionDetails(
                           Request.QueryString["testkey"], questionKey.Value, int.Parse(Request.QueryString["attemptid"]), Request.QueryString["candidatesession"]);
                    // Find a label from the current row.
                    Label CanidateTestDetails_rowNoLabel = (Label)e.Row.FindControl("CanidateTestDetails_rowNoLabel");

                    // Assign the row no to the label.
                    CanidateTestDetails_rowNoLabel.Text = (e.Row.RowIndex + 1).ToString();


                    PlaceHolder SearchQuestion_answerChoicesPlaceHolder =
                          (PlaceHolder)e.Row.FindControl("TestStatisticsInfo_questionStatisticsGridView_answerChoicesPlaceHolder");

                    SearchQuestion_answerChoicesPlaceHolder.Controls.Add
                                        (GetAnswerChoices(questionDetail.AnswerChoices));

                    //  e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                    //e.Row.Attributes.Add("onmouseout", onmouseoutStyle);

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CanidateTestDetails_topErrorMessageLabel,
                CanidateTestDetails_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void CanidateTestDetails_testDrftGridView_OpenText_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                // Find the row type is DataRow
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField questionKey = (HiddenField)e.Row.FindControl("CandidateTestDetails_questionKey");
                    QuestionDetail questionDetail = new ReportBLManager().GetSingleQuestionDetailsOpenText(
                           Request.QueryString["testkey"], questionKey.Value, int.Parse(Request.QueryString["attemptid"]), Request.QueryString["candidatesession"]);
                    // Find a label from the current row.
                    Label CanidateTestDetails_rowNoLabel = (Label)e.Row.FindControl("CanidateTestDetails_rowNoLabel");

                    // Assign the row no to the label.
                    CanidateTestDetails_rowNoLabel.Text = (e.Row.RowIndex + 1).ToString();

                    //QuestionAttribute questionAttribute = (QuestionAttribute)questionDetail.QuestionAttribute;

                    Literal CandidateTestDetails_AnswerReference = (Literal)e.Row.FindControl("CandidateTestDetails_AnswerReference");
                    Literal CandidateTestDetails_Answer = (Literal)e.Row.FindControl("CandidateTestDetails_Answer");

                    Label CanidateTestDetails_MarksLabel = (Label)e.Row.FindControl("CanidateTestDetails_MarksLabel");
                    Label CanidateTestDetails_MarksObtainedLabel = (Label)e.Row.FindControl("CanidateTestDetails_MarksObtainedLabel");
                    if (questionDetail != null)
                    {
                        CandidateTestDetails_AnswerReference.Text = questionDetail.QuestionAttribute.AnswerReference;
                        CandidateTestDetails_Answer.Text = questionDetail.QuestionAttribute.Answer;
                        CanidateTestDetails_MarksLabel.Text = questionDetail.QuestionAttribute.Marks.ToString();
                        CanidateTestDetails_MarksObtainedLabel.Text = questionDetail.QuestionAttribute.MarksObtained.ToString();
                    }
                    //e.Row.Attributes.Add("onmouseover", onmouseoverStyle);

                    //e.Row.Attributes.Add("onmouseout", onmouseoutStyle);

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CanidateTestDetails_topErrorMessageLabel,
                CanidateTestDetails_bottomErrorMessageLabel, exp.Message);
            }
        }
        #endregion Event Handler

        #region Private methods                                                
        /// <summary>
        /// Represents the method to get the answer choices of the question 
        /// </summary>
        /// <param name="answerChoice">
        /// A<see cref="List<AnswerChoice>"/>that holds the list of answerchoices
        /// </param>
        /// <returns>
        /// A<see cref="Table"/>a table that contain the answer choice
        /// </returns>
        private Table GetAnswerChoices(List<AnswerChoice> answerChoice)
        {
            //This method will return the table containing the answer choices
            return (new ControlUtility().GetAnswerChoices(answerChoice, false));
        }

        /// <summary>
        /// Loads the candidate statistics details.
        /// </summary>
        /// <param name="CandidateStatisticsDataSource">The candidate statistics data source.</param>
        private void LoadCandidateStatisticsDetails(CandidateStatisticsDetail CandidateStatisticsDataSource)
        {

            decimal averageTimeTaken = 0;
            if (!Utility.IsNullOrEmpty(CandidateStatisticsDataSource.AverageTimeTaken)
                && CandidateStatisticsDataSource.AverageTimeTaken.Length != 0)
            {
                averageTimeTaken = decimal.Parse(CandidateStatisticsDataSource.AverageTimeTaken);
            }

            if (Request.QueryString["QuestionType"] == "1"||TestQuestionType=="1")
            {
                CanidateTestDetails_noOfQuestionLabelValue.Text =
                    CandidateStatisticsDataSource.TotalQuestion.ToString();

                CanidateTestDetails_noOfQuestionattenedLabelValue.Text =
                    CandidateStatisticsDataSource.TotalQuestionAttended.ToString();

                CanidateTestDetails_absoluteScoreLabelValue.Text =
                    CandidateStatisticsDataSource.AbsoluteScore.ToString();

                CanidateTestDetails_noOfQuestionSkippedLabelValue.Text =
                    CandidateStatisticsDataSource.TotalQuestionSkipped.ToString();

                CanidateTestDetails_relativeScoreLabelValue.Text = CandidateStatisticsDataSource.RelativeScore.ToString();

                CanidateTestDetails_noOfQuestionAnsweredCorrectlyLabelValue.Text =
                    CandidateStatisticsDataSource.QuestionsAnsweredCorrectly.ToString();

                CanidateTestDetails_percentileLabelValue.Text =
                CandidateStatisticsDataSource.Percentile.ToString();

                CanidateTestDetails_noOfQuestionAnsweredWronglyLabelValue.Text
                    = CandidateStatisticsDataSource.QuestionsAnsweredWrongly.ToString();

                CanidateTestDetails_noOfQuestionTimeTakenLabelValue.Text
                    = CandidateStatisticsDataSource.TimeTaken;

                CanidateTestDetails_percentageQuestionsAnsweredCorrectlyLabelValue.Text
                    = CandidateStatisticsDataSource.AnsweredCorrectly.ToString();

                CanidateTestDetails_myScoreLabelValue.Text =
                    CandidateStatisticsDataSource.MyScore.ToString();

                CanidateTestDetails_testStatusLabelValue.Text = CandidateStatisticsDataSource.TestStatus;

                CanidateTestDetails_avgTimeTakenLabelValue.Text =
               Utility.ConvertSecondsToHoursMinutesSeconds
               (decimal.ToInt32(averageTimeTaken));
            }
            else
            {
                CanidateTestDetails_OpenText_noOfQuestionLabelValue.Text = 
                    CandidateStatisticsDataSource.TotalQuestion.ToString();

                CanidateTestDetails_OpenText_marksScoredLabelValue.Text =
                    CandidateStatisticsDataSource.MarksObtained.ToString();

                CanidateTestDetails_OpenText_marksLabelValue.Text =
                    CandidateStatisticsDataSource.Marks.ToString();

                CanidateTestDetails_OpenText_noOfQuestionattenedLabelValue.Text =
                    CandidateStatisticsDataSource.TotalQuestionAttended.ToString();

                CanidateTestDetails_OpenText_noOfQuestionSkippedLabelValue.Text =
                    CandidateStatisticsDataSource.TotalQuestionSkipped.ToString();

                CanidateTestDetails_OpenText_noOfQuestionTimeTakenLabelValue.Text =
                    CandidateStatisticsDataSource.TimeTaken; ;

                CanidateTestDetails_OpenText_percentageQuestionsAnsweredCorrectlyLabelValue.Text =
                    CandidateStatisticsDataSource.AnsweredCorrectly.ToString();

                CanidateTestDetails_OpenText_avgTimeTakenLabelValue.Text =
                    Utility.ConvertSecondsToHoursMinutesSeconds
               (decimal.ToInt32(averageTimeTaken));

                if (!Utility.IsNullOrEmpty(CandidateStatisticsDataSource.TestStatus))
                CanidateTestDetails_OpenText_testStatusLabelValue.Text =
                    CandidateStatisticsDataSource.TestStatus.ToString();

            }
            if (!Utility.IsNullOrEmpty(CandidateStatisticsDataSource.TestName))
                CanidateTestDetails_TestNameLabel.Text = CandidateStatisticsDataSource.TestName.ToString();
            if (!Utility.IsNullOrEmpty(CandidateStatisticsDataSource.CandidateFullName))
            {
                CanidateTestDetails_CandidateNameHyperLink.Text = CandidateStatisticsDataSource.CandidateFullName;
                if (Request.QueryString["candidatesession"] != null)
                    CanidateTestDetails_CandidateNameHyperLink.NavigateUrl = "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&candidatesession=" + Request.QueryString["candidatesession"];
            }            
           
        }
       
        /// <summary>
        /// Binds the test questions.
        /// </summary>
        /// <param name="testKey">The test key.</param>
        /// <param name="sortExpression">The sort expression.</param>
        private void BindTestQuestions(string testKey, string candidateSessionKey,int attemptID)
        {
            List<QuestionDetail> questionDetails = new List<QuestionDetail>();

            // Call a method to build question detail object
            // by passing testkey and sortexpression
            questionDetails = new TestBLManager().GetCandidateTestQuestionDetail(testKey, candidateSessionKey,attemptID);
            if (questionDetails.Count > 0)
            {
                if (Request.QueryString["QuestionType"] == null)
                    TestQuestionType = questionDetails[0].QuestionType == QuestionType.OpenText ? "2" : "1";
                // if (questionDetails[0].QuestionType == QuestionType.OpenText)
            }
                if(Request.QueryString["questiontype"]!=null && Request.QueryString["questiontype"]=="2"||TestQuestionType=="2")
                {
                    CanidateTestDetails_testDrft_OpenText_Table.Visible = true;
                    CanidateTestDetails_testDrft_MultipleChoiceTable.Visible = false;
                    CanidateTestDetails_testDrftGridView_OpenText.DataSource = questionDetails;
                    CanidateTestDetails_testDrftGridView_OpenText.DataBind();
                }
                else
                {

                    CanidateTestDetails_testDrft_OpenText_Table.Visible = false;
                    CanidateTestDetails_testDrft_MultipleChoiceTable.Visible = true;
                    CanidateTestDetails_testDrftGridView.DataSource = questionDetails;
                    CanidateTestDetails_testDrftGridView.DataBind();
                }
            
            
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            Master.SetPageCaption(Resources.HCMResource.CandidateTestQuestionsDetail_Title);
            BindTestQuestions(Request.QueryString.Get("testkey"), Request.QueryString.Get("candidatesession"), int.Parse(Request.QueryString["attemptid"]));
            CandidateStatisticsDetail candidateStatisticsDetail=null;
            if (Request.QueryString["QuestionType"]!=null && Request.QueryString["QuestionType"] == "1"|| TestQuestionType=="1")
            {
                CanidateTestDetails_OpenText.Visible = false;
                CanidateTestDetails_MultipleChoice.Visible = true;
                candidateStatisticsDetail = new ReportBLManager().GetCandidateTestResultStatisticsDetails
                    (Request.QueryString.Get("candidatesession"), Request.QueryString.Get("testkey"), int.Parse(Request.QueryString["attemptid"]));
            }
            else
            {
                CanidateTestDetails_OpenText.Visible = true;
                CanidateTestDetails_MultipleChoice.Visible = false;
                candidateStatisticsDetail = new ReportBLManager().GetCandidateOpenTextTestResultStatisticsDetails
                   (Request.QueryString.Get("candidatesession"), Request.QueryString.Get("testkey"), int.Parse(Request.QueryString["attemptid"]));
            }

            LoadCandidateStatisticsDetails(candidateStatisticsDetail);
        }

         protected static string ConvertSecondsToHoursMinutesSeconds(int seconds)
        {
            TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);

            string hoursMinutesSeconds = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                    timeSpan.Hours,
                                    timeSpan.Minutes,
                                    timeSpan.Seconds);

            return hoursMinutesSeconds;
        }
      
        #endregion Protected Overridden Methods

      /// <summary>
      /// Top Download Button to Download the Candidate Test Result Summary
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
         protected void CanidateTestDetails_topDownloadButton_Click(object sender, EventArgs e)
         {
             try
             {
                 if (Request.QueryString["QuestionType"] != null && Request.QueryString["QuestionType"].ToString() == "2" || TestQuestionType=="2")
                 {
                     DownloadOpenTextCandidateTestResultSummary();
                 }
                 else
                 {
                     DataTable candidateTestDetailTable = new DataTable();
                     if (Request.QueryString["candidatesession"] == null && Request.QueryString["testkey"] == null)
                     {
                         base.ShowMessage(CanidateTestDetails_bottomErrorMessageLabel, CanidateTestDetails_topErrorMessageLabel, "No Test Details Found");
                         return;
                     }
                     CandidateStatisticsDetail candidateStatisticsDetail = new CandidateStatisticsDetail();
                     candidateStatisticsDetail = new ReportBLManager().GetCandidateTestResultStatisticsDetails
                    (Request.QueryString.Get("candidatesession"), Request.QueryString.Get("testkey"), int.Parse(Request.QueryString["attemptid"]));

                     List<QuestionDetail> questionDetails = new List<QuestionDetail>();
                     questionDetails = new TestBLManager().GetCandidateTestQuestionDetail
                     (Request.QueryString.Get("testkey"), Request.QueryString.Get("candidatesession"), int.Parse(Request.QueryString["attemptid"]));

                     string testkey = Request.QueryString.Get("testkey").ToString();
                     int attemptid = int.Parse(Request.QueryString["attemptid"]);
                     string candidatesession = Request.QueryString["candidatesession"].ToString();

                     PDFDocumentWriter pdfWriter = new PDFDocumentWriter();
                     string filename = candidateStatisticsDetail.CandidateFullName + "_" + "Candidate Test Result Summary.pdf";

                     Response.ContentType = "application/pdf";
                     Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                     Response.Cache.SetCacheability(HttpCacheability.NoCache);

                     Document doc = new Document(PDFDocumentWriter.paper,
                     PDFDocumentWriter.leftMargin, PDFDocumentWriter.rightMargin,
                     PDFDocumentWriter.topMargin, PDFDocumentWriter.bottomMargin);

                     string serverPath = Server.MapPath("~/");

                     PdfPTable pageHeader = new PdfPTable(pdfWriter.PageHeader(serverPath, "Candidate Test Result Summary"));
                     pageHeader.SpacingAfter = 10f;
                     PdfPTable tableHeaderDetail = new PdfPTable(1);
                     PdfPCell cellBorder = new PdfPCell(new Phrase(""));
                     cellBorder.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                     cellBorder.Border = 0;
                     tableHeaderDetail.WidthPercentage = 90;
                     cellBorder.PaddingTop = 20f;
                     tableHeaderDetail.AddCell(cellBorder);
                     tableHeaderDetail.SpacingAfter = 30f;
                     PdfWriter.GetInstance(doc, Response.OutputStream);
                     doc.Open();
                     doc.Add(pageHeader);
                     doc.Add(tableHeaderDetail);
                     doc.Add(pdfWriter.TestResultSummary(candidateStatisticsDetail));
                     doc.Add(pdfWriter.TestResultSummaryQuestions(serverPath, questionDetails, testkey, attemptid, candidatesession));
                     doc.Close();
                 }
             }
             catch (Exception exp)
             {
                 Logger.ExceptionLog(exp);
                 base.ShowMessage(CanidateTestDetails_bottomErrorMessageLabel,CanidateTestDetails_topErrorMessageLabel, exp.Message);
                
             }
         }

         public void DownloadOpenTextCandidateTestResultSummary()
         {
             try
             {
                 DataTable candidateTestDetailTable = new DataTable();
                 if (Request.QueryString["candidatesession"] == null && Request.QueryString["testkey"] == null)
                 {
                     base.ShowMessage(CanidateTestDetails_bottomErrorMessageLabel, CanidateTestDetails_topErrorMessageLabel, "No Test Details Found");
                     return;
                 }

                 CandidateStatisticsDetail candidateStatisticsDetail = new CandidateStatisticsDetail(); 
                 candidateStatisticsDetail = new ReportBLManager().GetCandidateOpenTextTestResultStatisticsDetails
                  (Request.QueryString.Get("candidatesession"), Request.QueryString.Get("testkey"), int.Parse(Request.QueryString["attemptid"]));                                  

                 List<QuestionDetail> questionDetails = new List<QuestionDetail>();
                 questionDetails = new TestBLManager().GetCandidateTestQuestionDetail
                 (Request.QueryString.Get("testkey"), Request.QueryString.Get("candidatesession"), int.Parse(Request.QueryString["attemptid"]));

                 string testkey = Request.QueryString.Get("testkey").ToString();
                 int attemptid = int.Parse(Request.QueryString["attemptid"]);
                 string candidatesession = Request.QueryString["candidatesession"].ToString();

                 PDFDocumentWriter pdfWriter = new PDFDocumentWriter();
                 string filename = candidateStatisticsDetail.CandidateFullName + "_" + "Candidate Test Result Summary.pdf";

                 Response.ContentType = "application/pdf";
                 Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                 Response.Cache.SetCacheability(HttpCacheability.NoCache);

                 Document doc = new Document(PDFDocumentWriter.paper,
                 PDFDocumentWriter.leftMargin, PDFDocumentWriter.rightMargin,
                 PDFDocumentWriter.topMargin, PDFDocumentWriter.bottomMargin);

                 string serverPath = Server.MapPath("~/");

                 PdfPTable pageHeader = new PdfPTable(pdfWriter.PageHeader(serverPath, "Candidate Test Result Summary"));
                 pageHeader.SpacingAfter = 10f;
                 PdfPTable tableHeaderDetail = new PdfPTable(1);
                 PdfPCell cellBorder = new PdfPCell(new Phrase(""));
                 cellBorder.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                 cellBorder.Border = 0;
                 tableHeaderDetail.WidthPercentage = 90;
                 cellBorder.PaddingTop = 20f;
                 tableHeaderDetail.AddCell(cellBorder);
                 tableHeaderDetail.SpacingAfter = 30f;
                 PdfWriter.GetInstance(doc, Response.OutputStream);
                 doc.Open();
                 doc.Add(pageHeader);
                 doc.Add(tableHeaderDetail);
                 doc.Add(pdfWriter.OpenTextTestResultSummary(candidateStatisticsDetail));
                 doc.Add(pdfWriter.OpenTextTestResultSummaryQuestions(serverPath, questionDetails, testkey, attemptid, candidatesession));
                 doc.Close();
             }
             catch (Exception exp)
             {
                 Logger.ExceptionLog(exp);
                 base.ShowMessage(CanidateTestDetails_bottomErrorMessageLabel, CanidateTestDetails_topErrorMessageLabel, exp.Message);

             }
         }

        /// <summary>
        /// Bottom Download Button to Download the Candidate Test Result Summary
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
         protected void CanidateTestDetails_downloadButton_Click(object sender, EventArgs e)
         {
             try
             {
                  
                 if (Request.QueryString["QuestionType"] != null && Request.QueryString["QuestionType"].ToString() == "2"||TestQuestionType=="2")
                 {
                     DownloadOpenTextCandidateTestResultSummary();
                 }
                 else
                 {
                     DataTable candidateTestDetailTable = new DataTable();
                     if (Request.QueryString["candidatesession"] == null && Request.QueryString["testkey"] == null)
                     {
                         base.ShowMessage(CanidateTestDetails_bottomErrorMessageLabel, CanidateTestDetails_topErrorMessageLabel, "No Test Details Found");
                         return;
                     }
                     CandidateStatisticsDetail candidateStatisticsDetail = new CandidateStatisticsDetail();
                     candidateStatisticsDetail = new ReportBLManager().GetCandidateTestResultStatisticsDetails
                    (Request.QueryString.Get("candidatesession"), Request.QueryString.Get("testkey"), int.Parse(Request.QueryString["attemptid"]));

                     List<QuestionDetail> questionDetails = new List<QuestionDetail>();
                     questionDetails = new TestBLManager().GetCandidateTestQuestionDetail
                     (Request.QueryString.Get("testkey"), Request.QueryString.Get("candidatesession"), int.Parse(Request.QueryString["attemptid"]));

                     string testkey = Request.QueryString.Get("testkey").ToString();
                     int attemptid = int.Parse(Request.QueryString["attemptid"]);
                     string candidatesession = Request.QueryString["candidatesession"].ToString();

                     PDFDocumentWriter pdfWriter = new PDFDocumentWriter();
                     string filename = candidateStatisticsDetail.CandidateFullName + "_" + "Candidate Test Result Summary.pdf";

                     Response.ContentType = "application/pdf";
                     Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                     Response.Cache.SetCacheability(HttpCacheability.NoCache);

                     Document doc = new Document(PDFDocumentWriter.paper,
                     PDFDocumentWriter.leftMargin, PDFDocumentWriter.rightMargin,
                     PDFDocumentWriter.topMargin, PDFDocumentWriter.bottomMargin);
                     string serverPath = Server.MapPath("~/");

                     PdfPTable pageHeader = new PdfPTable(pdfWriter.PageHeader(serverPath, "Candidate Test Result Summary"));
                     pageHeader.SpacingAfter = 10f;
                     PdfPTable tableHeaderDetail = new PdfPTable(1);
                     PdfPCell cellBorder = new PdfPCell(new Phrase(""));
                     cellBorder.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                     cellBorder.Border = 0;
                     tableHeaderDetail.WidthPercentage = 90;
                     cellBorder.PaddingTop = 20f;
                     tableHeaderDetail.AddCell(cellBorder);
                     tableHeaderDetail.SpacingAfter = 30f;
                     PdfWriter.GetInstance(doc, Response.OutputStream);
                     doc.Open();
                     doc.Add(pageHeader);
                     doc.Add(tableHeaderDetail);
                     doc.Add(pdfWriter.TestResultSummary(candidateStatisticsDetail));
                     doc.Add(pdfWriter.TestResultSummaryQuestions(serverPath, questionDetails, testkey, attemptid, candidatesession));
                     doc.Close();
                 }
             }
             catch (Exception exp)
             {
                 Logger.ExceptionLog(exp);
                 base.ShowMessage(CanidateTestDetails_bottomErrorMessageLabel, CanidateTestDetails_topErrorMessageLabel, exp.Message);
             }
         }
}
}