<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OTMMaster.Master"
    CodeBehind="CandidateReport.aspx.cs" Inherits="Forte.HCM.UI.ReportCenter.CandidateReport" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="TestDownload_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">

    <script type="text/javascript" language="javascript">


        //move the Single Question add to Testdraft.
        function singleqnsclick(QuestionID, buttonID, type) {
            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
            var hiddenvalue1 = document.getElementById('<%=isCandidateHiddenField.ClientID %>')
            document.getElementById('<%=CandidateReport_postbackHiddenField.ClientID %>').value = "1"
            hiddenvalue1.value = type;
            hiddenvalue.value = QuestionID;
            __doPostBack(buttonID, "OnClick");
        }

        var sourceTable;
        var targetTable;
        var dragObject = null;
        var mouseOffSet = null;
        this.table = null;
        document.mousemove = mouseMove;

        function mousedown(QuestionID, source, target) {
            sourceTable = document.getElementById(source);
            targetTable = document.getElementById(target);


            if (isChecked(QuestionID, source)) {
                CheckNoOfChecked(QuestionID);
            }
            else {
                CheckCorrectQuestion(QuestionID);
            }
        }
        function isChecked(Id, source) {
            var isChecked = false;
            var table = sourceTable;
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                var inputs = row.getElementsByTagName('input');
                for (var j = 0; j < inputs.length; j++) {
                    if (inputs[j].type == 'checkbox') {
                        var chkbox = inputs[j];
                    }
                    if (null != chkbox && true == chkbox.checked) {
                        isChecked = true;
                    }
                }
            }
            if (isChecked) {
                return true;
            }
            else {
                return false;
            }
        }

        function CheckNoOfChecked(QuestionID) {

            var candidateTable = document.getElementById('<%=CandidateReport_candidateDetailsGridView.ClientID %>');

            var categoryTable = document.getElementById('<%=CandidateReport_categoryDetailsGridView.ClientID %>');

            var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
            var hiddenvalue1 = document.getElementById('<%=isCandidateHiddenField.ClientID %>');

            hiddenvalue1.value = "";
            hiddenvalue.value = "";

            var numberOfQuestions = 0;

            var table = sourceTable;

            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {

                    numberOfQuestions = numberOfQuestions + 1;

                    var noOfQuestionsTextBox = document.getElementById('<%=DIVTextBox.ClientID %>');

                    noOfQuestionsTextBox.value = 'Move ' + numberOfQuestions + ' information';

                    dragObject = document.getElementById('<%=moveDisplayDIV.ClientID %>');

                    if (detectBrowser()) {

                        if (categoryTable == sourceTable) {
                            if (hiddenvalue.value == "") {
                                hiddenvalue.value = row.childNodes[3].childNodes[1].defaultValue + ',';
                            }
                            else {
                                hiddenvalue.value = hiddenvalue.value + row.childNodes[3].childNodes[1].defaultValue + ',';
                            }
                        }
                        if (candidateTable == sourceTable) {
                            if (hiddenvalue.value == "") {
                                hiddenvalue.value = row.childNodes[3].childNodes[0].data + ',';
                            }
                            else {
                                hiddenvalue.value = hiddenvalue.value + row.childNodes[3].childNodes[0].data + ',';
                            }
                        }
                    }
                    else {
                        if (categoryTable == sourceTable) {
                            if (hiddenvalue.value == "") {
                                hiddenvalue.value = row.childNodes[2].childNodes[0].value + ',';
                            }
                            else {
                                hiddenvalue.value = hiddenvalue.value + row.childNodes[2].childNodes[0].value + ',';
                            }
                        }
                        if (candidateTable == sourceTable) {
                            if (hiddenvalue.value == "") {
                                hiddenvalue.value = row.childNodes[2].childNodes[0].data + ',';
                            }
                            else {
                                hiddenvalue.value = hiddenvalue.value + row.childNodes[2].childNodes[0].data + ',';
                            }
                        }
                    }
                }
            }
            //var ShowDiv =
        }

        function CheckCorrectQuestion(id) {
            var table = sourceTable;
            id++;
            var rowid = 1;
            var chkbox;
            var rowCount = table.rows.length;
            for (var i = 1; i < rowCount; i++) {
                if (id == i) {
                    var row = table.rows[i];
                    if (detectBrowser()) {
                        chkbox = row.cells[0].childNodes[1];
                    }
                    else {
                        chkbox = row.cells[0].childNodes[0];
                    }

                    if (null != chkbox) {
                        chkbox.checked = true;
                        // row.cells[0].childNodes[0].checked = true;
                    }
                }
                CheckNoOfChecked(id);
            }
        }

        function mouseMove(ev) {
            ev = ev || window.event;
            var mousePos = mouseCoords(ev);

            if (dragObject) {
                //mouseOffset = getMouseOffset(dragObject, ev);

                dragObject.style.display = 'block';
                dragObject.style.backgroundColor = "#eee";
                dragObject.style.position = 'absolute';
                if (detectBrowser()) {
                    var mousex = mousePos.x + 10;
                    var mousey = mousePos.y + 10;
                    dragObject.style.top = mousey + 'px';
                    dragObject.style.left = mousex + 'px';
                }
                else {
                    dragObject.style.top = mousePos.y + document.documentElement.scrollTop + 10;
                    dragObject.style.left = mousePos.x + 10;
                }
            }
            else {
                return false;

            }
            return false;
        }
        function mouseCoords(ev) {
            if (ev != null) {
                if (ev.pageX || ev.pageY) { return { x: ev.pageX, y: ev.pageY }; }
                return {
                    x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
                    y: ev.clientY + document.body.scrollTop - document.body.clientTop
                };
            }
        }

        function srsmouseup(ev) {
            if (dragObject == null)
                return false;

            var candidateSourceTable = document.getElementById('<%=CandidateReport_candidateDetailsGridView.ClientID %>');

            var categorySourceTable = document.getElementById('<%=CandidateReport_categoryDetailsGridView.ClientID %>');

            var ev = ev || window.event;
            var mousePos = mouseCoords(ev);
            var curTarget = targetTable;
            var targPos = getPosition(curTarget);

            var scrolltargPos;
            if (detectBrowser()) {
                scrolltargPos = targPos.y;
            }
            else {
                scrolltargPos = targPos.y - document.documentElement.scrollTop;
            }

            var targWidth = parseInt(curTarget.offsetWidth);
            var targHeight = parseInt(curTarget.offsetHeight);
            if (candidateSourceTable == sourceTable) {
                document.getElementById('<%=isCandidateHiddenField.ClientID %>').value = "CANDIDATE";
                // var ButtonId = document.getElementById('<%=CandidateReport_moveToDraftButton.ClientID%>');

                // alert(ButtonId);
                if (
            (mousePos.x >= targPos.x) &&
            (mousePos.y >= scrolltargPos) && (mousePos.x <= targPos.x + targWidth)) {
                    document.getElementById('<%=CandidateReport_postbackHiddenField.ClientID %>').value = "1";
                    __doPostBack('ctl00_HCMMaster_body_CandidateReport_moveToDraftButton', "OnClick");
                    dragObject.style.display = 'none';
                }
                else {
                    if (dragObject != null) {
                        makeUnchekabale();
                        dragObject.style.display = 'none';
                        var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
                        hiddenvalue.value = "";
                    }
                }
            }
            else {
                if (
            (mousePos.x >= targPos.x) &&
            (mousePos.y >= scrolltargPos)) {
                    document.getElementById('<%=isCandidateHiddenField.ClientID %>').value = "";
                    document.getElementById('<%=CandidateReport_postbackHiddenField.ClientID %>').value = "1";
                    __doPostBack('ctl00_HCMMaster_body_CandidateReport_moveToDraftButton', "OnClick");

                    dragObject.style.display = 'none';
                }
                else {
                    if (dragObject != null) {
                        makeUnchekabale();
                        dragObject.style.display = 'none';

                        var hiddenvalue = document.getElementById('<%=hiddenValue.ClientID %>');
                        hiddenvalue.value = "";
                    }
                }
            }
            dragObject = null;
            return false;
        }

        function makeUnchekabale() {
            var table = sourceTable;
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                if (detectBrowser()) {
                    chkbox = row.cells[0].childNodes[1];
                }
                else {
                    chkbox = row.cells[0].childNodes[0];
                }

                if (null != chkbox && true == chkbox.checked) {
                    chkbox.checked = false;
                }
            }
        }

        function getPosition(e) {
            var left = 0;
            var top = 0;

            if (e != null) {
                while (e.offsetParent) {
                    left += e.offsetLeft;
                    top += e.offsetTop;
                    e = e.offsetParent;
                }
                left += e.offsetLeft;
                top += e.offsetTop;
            }
            return { x: left, y: top };
        }

        function detectBrowser() {
            var browser = navigator.appName;
            var b_version = navigator.appVersion;
            var version = parseFloat(b_version);
            if (browser == "Netscape") {
                return true;
            }
            else {
                return false; ;
            }
        }
    </script>

    <asp:UpdatePanel ID="CandidateReport_updatePanel" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="CandidateReport_candidateGridPageNoHiddenField" runat="server"
                Value="1" />
            <asp:HiddenField ID="CandidateReport_categoryGridPageNoHiddenField" runat="server"
                Value="1" />
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td class="header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="72%" class="header_text_bold">
                                    <asp:Literal ID="CandidateReport_headerLiteral" runat="server" Text="Candidate Report"></asp:Literal>
                                </td>
                                <td width="28%" align="right">
                                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                        <tr>
                                            <td style="width: 62%">
                                                &nbsp;
                                            </td>
                                            <td width="16%" align="right">
                                                <asp:LinkButton ID="CandidateReport_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="CandidateReport_searchCandidate_resetButton_Click" />
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td width="18%" align="left">
                                                <asp:LinkButton ID="CandidateReport_topCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align td_height_20">
                        <asp:Label ID="CandidateReport_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CandidateReport_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <asp:HiddenField ID="CandidateReport_postbackHiddenField" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="tab_body_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td style="width: 49.5%" valign="top">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="panel_bg">
                                                        <asp:Panel ID="CandidateReport_candidateSearchPanel" runat="server" DefaultButton="CandidateReport_candidateSearchButton">
                                                            <table width="100%" border="0" cellspacing="3" cellpadding="2%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="CandidateReport_candidateFirstNameLabel" runat="server" Text="First Name"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="CandidateReport_candidateFirstNameTextBox" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="CandidateReport_candidateLastNameLabel" runat="server" Text="Last Name"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="CandidateReport_candidateLastNameTextBox" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="CandidateReport_candidateEmailLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="CandidateReport_candidateEmailTextBox" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td align="left" colspan="2">
                                                                        <asp:Button ID="CandidateReport_candidateSearchButton" runat="server" SkinID="sknButtonId"
                                                                            Text="Search" OnClick="CandidateReport_candidateSearchButton_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="header_bg" runat="server" style="width: 100%">
                                                            <div id="CandidateReport_searchTestResultsDiv" runat="server" style="display: block">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td style="width: 80%">
                                                                            <asp:Literal ID="CandidateReport_candidateSearchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;
                                                                        </td>
                                                                        <td style="width: 20%" align="right">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                        <td class="grid_body_bg">
                                                            <div style="height: 150px; overflow: auto;" runat="server" id="CandidateReport_searchCandidate_testDiv">
                                                                <asp:GridView ID="CandidateReport_candidateDetailsGridView" runat="server" Width="100%"
                                                                    GridLines="Horizontal" BorderColor="white" BorderWidth="1px" AutoGenerateColumns="false"
                                                                    OnRowDataBound="CandidateReport_candidateDetailsGridView_RowDataBound" EmptyDataRowStyle-CssClass="error_message_text"
                                                                    EmptyDataText='<%#Resources.HCMResource.Common_Empty_Grid %>'>
                                                                    <RowStyle CssClass="grid_alternate_row" />
                                                                    <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                    <HeaderStyle CssClass="grid_header_row" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="CandidateReport_candidateDetailsGridViewCheckbox" runat="server" />
                                                                                <asp:HiddenField ID="CandidateReport_hidddenValue" runat="server" Value='<%#Eval("UserID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="CandidateReport_selectCandidateImage" runat="server" SkinID="sknMoveDown_ArrowImageButton"
                                                                                    CommandName="Select" ToolTip="Select Candidate" CommandArgument='<%#Eval("UserID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="UserID" HeaderText="Candidate ID" />
                                                                        <asp:TemplateField HeaderText="Candidate Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="CandidateReport_fullNameLabel" runat="server" Text='<%# Eval("FirstName") %>'
                                                                                    ToolTip='<%# Eval("FullName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="City" HeaderText="Location" />
                                                                        <asp:BoundField DataField="Email" HeaderText="Email" />
                                                                        <asp:BoundField DataField="Phone" HeaderText="Phone" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <uc1:PageNavigator ID="CandidateReport_candidate_bottomPageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                        <td class="header_bg" style="width: 100%">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 80%">
                                                                        <asp:Literal ID="Literal1" runat="server" Text="Selected Candidates"></asp:Literal>&nbsp;
                                                                    </td>
                                                                    <td style="width: 20%" align="right">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                        <td class="grid_body_bg">
                                                            <div style="height: 150px; overflow: auto;" runat="server" id="CandidateReport_candidateDraftDiv">
                                                                <asp:GridView ID="CandidateReport_candidateDraftGridView" runat="server" Width="100%"
                                                                    GridLines="Horizontal" BorderColor="white" BorderWidth="1px" AutoGenerateColumns="false"
                                                                    OnRowCommand="CandidateReport_candidateDraftGridView_RowCommand" EmptyDataText="&nbsp;">
                                                                    <RowStyle CssClass="grid_alternate_row" />
                                                                    <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                    <HeaderStyle CssClass="grid_header_row" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="CandidateReport_candidateDraftRemoveImageButton" runat="server"
                                                                                    SkinID="sknDeleteImageButton" CommandName="DeleteCandidate" ToolTip="Delete Candidate"
                                                                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserID") %>' />
                                                                                <asp:HiddenField ID="CandidateReport_hidddenValue" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "UserID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="UserID" HeaderText="Candidate ID" />
                                                                        <asp:TemplateField HeaderText="Candidate Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="CandidateReport_candidateDraft_fullNameLabel" runat="server" Text='<%# Eval("FirstName") %>'
                                                                                    ToolTip='<%# Eval("FullName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="City" HeaderText="Location" />
                                                                        <asp:BoundField DataField="Email" HeaderText="Email" />
                                                                        <asp:BoundField DataField="Phone" HeaderText="Phone" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <uc1:PageNavigator ID="CandidateReport_draftPageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 1%">
                                            </td>
                                            <td style="width: 49.5%" valign="top">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                        <td class="panel_bg">
                                                        <asp:Panel ID="CandidateReport_searchSubjectPanel" runat="server" DefaultButton="CandidateReport_searchSubjectButton">
                                                            <table width="100%" border="0" cellspacing="3" cellpadding="2%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="CandidateReport_categoryHeadLabel" runat="server" Text="Category"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="CandidateReport_categoryTextBox" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="CandidateReport_subjectHeadLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="CandidateReport_subjectTextBox" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="CandidateReport_keywordsHeadLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="CandidateReport_keywordsTextBox" runat="server"></asp:TextBox></div>
                                                                    </td>
                                                                    <td align="left" colspan="2">
                                                                        <asp:Button ID="CandidateReport_searchSubjectButton" runat="server" SkinID="sknButtonId"
                                                                            Text="Search" OnClick="CandidateReport_searchSubjectButton_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                        <td id="Td1" class="header_bg" runat="server" style="width: 100%">
                                                            <div id="Div1" runat="server" style="display: block">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td style="width: 80%">
                                                                            <asp:Literal ID="CandidateReport_searchSubjectHeaderLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;
                                                                        </td>
                                                                        <td style="width: 20%" align="right">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                        <td class="grid_body_bg">
                                                            <div style="height: 150px; overflow: auto;" runat="server" id="Searchsubject_testDiv">
                                                                <asp:GridView ID="CandidateReport_categoryDetailsGridView" runat="server" AutoGenerateColumns="False"
                                                                    GridLines="Horizontal" BorderColor="white" BorderWidth="1px" Width="100%" OnRowDataBound="CandidateReport_categoryDetailsGridView_RowDataBound"
                                                                    EmptyDataRowStyle-CssClass="error_message_text" EmptyDataText='<%#Resources.HCMResource.Common_Empty_Grid %>'>
                                                                    <RowStyle CssClass="grid_alternate_row" />
                                                                    <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                    <HeaderStyle CssClass="grid_header_row" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="CandidateReport_categoryDetailsGridViewCheckbox" runat="server" />
                                                                                <asp:HiddenField ID="CandidateReport_hidddenValue" runat="server" Value='<%#Eval("SubjectID") %>' />
                                                                            </ItemTemplate>
                                                                            <ItemStyle Width="10px" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="CandidateReport_selectCategorySubjectImage" runat="server" SkinID="sknMoveDown_ArrowImageButton"
                                                                                    CommandName="Select" ToolTip="Select Subject" CommandArgument='<%#Eval("SubjectID") %>' />
                                                                            </ItemTemplate>
                                                                            <ItemStyle Width="10px" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="CandidateReport_selectCategorySubjectHiddenField" runat="server"
                                                                                    Value='<%#Eval("SubjectID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="CategoryName" HeaderText="Category" />
                                                                        <asp:BoundField DataField="SubjectName" HeaderText="Subject" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <uc1:PageNavigator ID="CandidateReport_categoryDetailsBottomPageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                        <td class="header_bg" style="width: 100%">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 80%">
                                                                        <asp:Literal ID="CanidateReport_categoryDraftHeaderLiteral" runat="server" Text="Selected Subjects"></asp:Literal>&nbsp;
                                                                    </td>
                                                                    <td style="width: 20%" align="right">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr onmousemove="javascript:mouseMove(event);" onmouseup="javascript:srsmouseup(event);">
                                                        <td class="grid_body_bg">
                                                            <div style="height: 150px; overflow: auto;" runat="server" id="CandidateReport_categoryDetailsDraftDiv">
                                                                <asp:GridView ID="CandidateReport_categoryDetailsDraftGridView" runat="server" AutoGenerateColumns="False"
                                                                    GridLines="Horizontal" BorderColor="white" BorderWidth="1px" Width="100%" OnRowCommand="candidateReport_categoryDetailsDraftGridView_RowCommand"
                                                                    EmptyDataText="&nbsp;">
                                                                    <RowStyle CssClass="grid_alternate_row" />
                                                                    <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                    <HeaderStyle CssClass="grid_header_row" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="CanidateReport_categoryDetailsDraftGridView_removeImageButton"
                                                                                    runat="server" SkinID="sknDeleteImageButton" CommandName="DeleteCategory" ToolTip="Delete"
                                                                                    CommandArgument='<%#Eval("SubjectID") %>' />
                                                                                <asp:HiddenField ID="CandidateReport_hidddenValue" Value='<%#Eval("SubjectID") %>'
                                                                                    runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="CategoryName" HeaderText="Category" />
                                                                        <asp:BoundField DataField="SubjectName" HeaderText="Subject" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <uc1:PageNavigator ID="CandidateDetails_categoryDetailsDraftPageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_5">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" colspan="3">
                                                <asp:Button ID="CandidateReport_comparisonReportButton" runat="server" Text="Group Analysis / Comparison Report"
                                                    SkinID="sknButtonId" OnClick="CandidateReport_comparisonReportButton_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="CandidateReport_confirmPopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <div id="CandidateReport_hiddenDIV" runat="server" style="display: none">
                                <asp:Button ID="CandidateReport_hiddenPopupModalButton" runat="server" />
                                <asp:Button ID="CandidateReport_moveToDraftButton" runat="server" OnClick="CandidateReport_moveToDraftButton_Click" />
                            </div>
                            <uc3:ConfirmMsgControl ID="CandidateReport_confirmPopupExtenderControl" runat="server"
                                OnOkClick="CandidateReport_okClick" OnCancelClick="CandidateReport_cancelClick" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="CandidateReport_confirmPopupExtender" runat="server"
                            PopupControlID="CandidateReport_confirmPopupPanel" TargetControlID="CandidateReport_hiddenPopupModalButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HiddenField ID="hiddenValue" runat="server" />
                        <asp:HiddenField ID="isCandidateHiddenField" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="msg_align td_height_20">
                        <asp:Label ID="CandidateReport_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CandidateReport_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </td>
                </tr>
            </table>
            <div id="moveDisplayDIV" style="display: none; background-color: Lime" runat="server">
                <asp:TextBox ID="DIVTextBox" runat="server" ReadOnly="True"></asp:TextBox>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
