﻿using System;
using System.IO;
using System.Web;
using System.Linq;
using System.Data;
using OfficeOpenXml;
using System.Web.UI;
using System.Drawing;
using System.Configuration;
using System.Drawing.Imaging;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;



namespace Forte.HCM.UI.ReportCenter
{
    public partial class CandidateDashboard : PageBase
    {

        #region Enum

        private enum SubscriptionRolesEnum
        {
            SR_COR_AMN = 3,
            SR_FRE = 1,
            SR_STA = 2,
            SR_COR_USR = 4
        }

        #endregion Enum

        #region Variable Declaration
        private string AdditionalDocpath = "~/CandidateDocuments/";
        #endregion
        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CandidateDashboard_errorLabel.Text = string.Empty;
                CandidateDashboard_SuccessMessageLabel.Text = string.Empty;

                CheckAndSetExpandorRestore();
                Master.SetPageCaption("Candidate Activity Dashboard");

                Page.Form.DefaultButton = CandidateDashboard_searchImageButton.UniqueID;

                tr_CandidateAdditionalDoc.Attributes.Add("onclick",
                 "ExpandOrRestoreResumeControls('" +
                 td_CandidateAdditionalDoc.ClientID + "','" +
                 CandidateDashboard_additionalDocUpSpan.ClientID + "','" +
                 CandidateDashboard_additionalDocDownSpan.ClientID + "','" +
                 CandidateDashboard_additionalDocrestoreHiddenField.ClientID + "')");

                if (!IsPostBack)
                {
                  
                    CandidateDashboard_candidateNameTextBox.Text = "";
                    ViewState["SORT_FIELD"] = "ACTIVITY_DATE";
                    ViewState["SORT_ORDER"] = SortType.Descending;
                    // Clear candidate photo from the session.
                    Session["CANDIDATE_THUMBNAIL_PHOTO"] = null;
                    Session["PHOTO_CHANGED"] = false;

                    EditControlsVisible(false, false);

                    CandidateDashboard_expandResumesList_ImageButton.Attributes.Add("onclick",
                      "javascript:return ResumeDivToggle('" + CandidateDashboard_candidateResumesDiv.ClientID + "','" + CandidateDashboard_candidateSkillMatrixDiv.ClientID + "');");
                    CandidateDashboard_closeResumesList_ImageButton.Attributes.Add("onclick",
                        "javascript:return ResumeDivToggle('" + CandidateDashboard_candidateResumesDiv.ClientID + "','" + CandidateDashboard_candidateSkillMatrixDiv.ClientID + "');");

                    CandidateDashboard_skillMatrixTR.Attributes.Add("onclick", "ExpandOrRestore('" + CandidateDashboard_candidateSkillMatrixDiv.ClientID + "','" +
                              SkillsMatrixControl_resumeDownSpan.ClientID + "','" +
                                SkillsMatrixControl_resumeUpSpan.ClientID + "')");

                    int candidateID = 0;

                    if (Request.QueryString["candidateID"] != null)
                        candidateID = int.Parse(Request.QueryString["candidateid"]);

                    if (Request.QueryString["puid"] != null)
                    {
                        candidateID = new CandidateBLManager().
                            GetCandidateID(int.Parse(Request.QueryString["puid"]), null);
                    }
                    if (Request.QueryString["candidatesession"] != null)
                    {
                        candidateID = new CandidateBLManager().
                            GetCandidateID(0, Request.QueryString["candidatesession"]);
                    }

                    if (candidateID > 0)
                    {
                        CandidateDashboard_selectPhotoFileUpload.Enabled = true;
                        CandidateDashboard_candidateIDHiddenField.Value = candidateID.ToString();
                        EditControlsVisible(true, false);
                        Load_CandidateActivities(candidateID);
                    }

                    CandidateDashboard_intelliVIEWPageNavigator.Reset();
                    CandidateDashboard_candidateImageButton.Attributes.Add("onclick",
                   "return LoadCandidateInformation('" + CandidateDashboard_candidateIDHiddenField.ClientID + "','"
                   + CandidateDashboard_candidateNameTextBox.ClientID + "','CAND_DASHBOARD');");

                    CandidateDashboard_skillMatrixTR.Style["display"] = "none";
                    CandidateDashboard_candidateSkillMatrixDiv.Style["display"] = "none";
                    CandidateDashboard_candidateSkillMatrixDiv.Style["height"] = "00px";
                    CandidateDashboard_candidateSkillMatrixDiv.Style["width"] = "00px";
                    CandidateDashboard_candidateSkillMatrixDiv.Style["overflow"] = "auto";

                    CandidateDashboard_candidateResumesDiv.Style["display"] = "none";
                    CandidateDashboard_candidateResumesDiv.Style["height"] = "220px";
                    CandidateDashboard_candidateResumesDiv.Style["width"] = "900px";
                    CandidateDashboard_candidateResumesDiv.Style["overflow"] = "auto";

                    if (candidateID == 0)
                        CandidateDashboard_selectPhotoFileUpload.Enabled = false;

                    CandidateDashboard_additionalDocDataList.DataSource = null;
                    CandidateDashboard_additionalDocDataList.DataBind();
                }


                // Check is post back is due to photo upload. If yes then show 
                // the preview.
                if (!Utility.IsNullOrEmpty(CandidateDashboard_photoUploadedHiddenFiled.Value) &&
                    CandidateDashboard_photoUploadedHiddenFiled.Value.Trim().ToUpper() == "Y")
                {
                    // Show preview.
                    if (!Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
                        ShowPreview(Convert.ToInt32(CandidateDashboard_candidateIDHiddenField.Value));
                }


                CandidateDashboard_candidateActivityPageNavigator.PageNumberClick += new
                      PageNavigator.PageNumberClickEventHandler
                      (CandidateDashboard_candidateActivityPageNavigator_PageNumberClick);

                CandidateDashboard_intelliTESTPageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                      (CandidateDashboard_intelliTESTPageNavigator_PageNumberClick);

                CandidateDashboard_intelliVIEWPageNavigator.PageNumberClick += new
                PageNavigator.PageNumberClickEventHandler
                      (CandidateDashboard_intelliVIEWPageNavigator_PageNumberClick);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_candidateActivityGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                CandidateDashboard_candidateActivityPageNavigator.Reset();
                LoadActivities(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void CandidateDashboard_candidateActivityGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                int sortColumnIndex = new ControlUtility().GetSortColumnIndex
                    (CandidateDashboard_candidateActivityGridView,
                    (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    new ControlUtility().AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        private void CandidateDashboard_candidateActivityPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadActivities(e.PageNumber);

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        private void CandidateDashboard_intelliTESTPageNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            try
            {
                string searchText = null;
                if (CandidateDashboard_searchKeywordsTextBox.Text.Trim() != "")
                    searchText = CandidateDashboard_searchKeywordsTextBox.Text.Trim();

                TestActivity(Convert.ToInt32(CandidateDashboard_candidateIDHiddenField.Value), e.PageNumber, 3, searchText);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        private void CandidateDashboard_intelliVIEWPageNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            try
            {
                string searchText = null;
                if (CandidateDashboard_searchKeywordsTextBox.Text.Trim() != "")
                    searchText = CandidateDashboard_searchKeywordsTextBox.Text.Trim();

                InterviewActivity(Convert.ToInt32(CandidateDashboard_candidateIDHiddenField.Value), e.PageNumber, 3, searchText);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_candidateActvityButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (CandidateDashboard_candidateIDHiddenField.Value != null)
                {
                    int candidateID = int.Parse(CandidateDashboard_candidateIDHiddenField.Value);

                    CandidateDashboard_selectPhotoFileUpload.Enabled = true;

                    EditControlsVisible(true, false);

                    Load_CandidateActivities(candidateID);

                    CandidateDashboard_intelliTESTPageNavigator.MoveToPage(1);
                    CandidateDashboard_intelliVIEWPageNavigator.MoveToPage(1);
                    CandidateDashboard_candidateActivityPageNavigator.MoveToPage(1);

                    //CandidateDashboard_additionalDocSave_Button_Click(new object(), new EventArgs());
                    LoadCandidateAdditionalDocs(candidateID);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_intelliVIEWGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                int sortColumnIndex = new ControlUtility().GetSortColumnIndex
                    (CandidateDashboard_intelliVIEWGridView,
                    (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    new ControlUtility().AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_intelliTESTGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                int sortColumnIndex = new ControlUtility().GetSortColumnIndex
                    (CandidateDashboard_intelliTESTGridView,
                    (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    new ControlUtility().AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_intelliTESTGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField CandidateDashboard_intelliTESTGridView_CandidateSessionKeyID = (HiddenField)e.Row.FindControl
                      ("CandidateDashboard_candidateSessionIDHiddenField");

                    HiddenField CandidateDashboard_intelliTESTGridView_TestID = (HiddenField)e.Row.FindControl
                        ("CandidateDashboard_testIDHiddenField");

                    HiddenField CandidateDashboard_intelliTESTGridView_AttemptID = (HiddenField)e.Row.FindControl
                        ("CandidateDashboard_attemptIDHiddenField");

                    HyperLink CandidateDashboard_testReportHyperLink =
                        (HyperLink)e.Row.FindControl("CandidateDashboard_testReportHyperLink");

                    CandidateDashboard_testReportHyperLink.NavigateUrl = "~/ReportCenter/CandidateTestDetails.aspx?m=3&s=0&testkey="
                        + CandidateDashboard_intelliTESTGridView_TestID.Value.ToString() +
                        "&candidatesession=" + CandidateDashboard_intelliTESTGridView_CandidateSessionKeyID.Value.ToString() +
                        "&attemptid=" + CandidateDashboard_intelliTESTGridView_AttemptID.Value.ToString() +
                        "&tab=CS&parentpage=T_STAT";

                    HyperLink CandidateDashboard_testCandidateReportHyperLink =
                        (HyperLink)e.Row.FindControl("CandidateDashboard_testCandidateReportHyperLink");

                    CandidateDashboard_testCandidateReportHyperLink.NavigateUrl = "~/TestMaker/TestResult.aspx?m=2&s=0&candidatesession="
                        + CandidateDashboard_intelliTESTGridView_CandidateSessionKeyID.Value.ToString() +
                        "&attemptid=" + CandidateDashboard_intelliTESTGridView_AttemptID.Value.ToString() +
                        "&testkey=" + CandidateDashboard_intelliTESTGridView_TestID.Value.ToString() +
                        "&parentpage=TST_SCHD";

                }
            }
            catch (Exception exp)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_intelliVIEWGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string candidateSessionID = string.Empty;
                    string testKey = string.Empty;
                    string sessionKey = string.Empty;
                    int assessorID = 0;
                    int attemptID = 0;
                    string urlPublish = string.Empty;

                    HiddenField CandidateDashboard_intelliVIEWGridView_CandidateSessionKeyID = (HiddenField)e.Row.FindControl
                      ("CandidateDashboard_intelliVIEWGridView_candidateSessionIDHiddenField");

                    HiddenField CandidateDashboard_intelliVIEWGridView_Testkey = (HiddenField)e.Row.FindControl
                        ("CandidateDashboard_intelliVIEWGridView_testkeyHiddenField");

                    HiddenField CandidateDashboard_intelliVIEWGridView_SessionKey = (HiddenField)e.Row.FindControl
                        ("CandidateDashboard_intelliVIEWGridView_sessionkeyHiddenField");

                    HiddenField CandidateDashboard_intelliVIEWGridView_AssessorID = (HiddenField)e.Row.FindControl
                        ("CandidateDashboard_intelliVIEWGridView_assessorIDHiddenField");

                    HiddenField CandidateDashboard_intelliVIEWGridView_AttemptID = (HiddenField)e.Row.FindControl
                        ("CandidateDashboard_intelliVIEWGridView_attemptIDHiddenField");

                    HiddenField CandidateDashboard_intelliVIEWGridView_PublishID = (HiddenField)e.Row.FindControl
                       ("CandidateDashboard_intelliVIEWGridView_publishHiddenField");


                    if (!Utility.IsNullOrEmpty(CandidateDashboard_intelliVIEWGridView_CandidateSessionKeyID.Value))
                        candidateSessionID = CandidateDashboard_intelliVIEWGridView_CandidateSessionKeyID.Value;

                    if (!Utility.IsNullOrEmpty(CandidateDashboard_intelliVIEWGridView_Testkey.Value))
                        testKey = CandidateDashboard_intelliVIEWGridView_Testkey.Value;

                    if (!Utility.IsNullOrEmpty(CandidateDashboard_intelliVIEWGridView_SessionKey.Value))
                        sessionKey = CandidateDashboard_intelliVIEWGridView_SessionKey.Value;

                    if (!Utility.IsNullOrEmpty(CandidateDashboard_intelliVIEWGridView_AssessorID.Value))
                        assessorID = Convert.ToInt32(CandidateDashboard_intelliVIEWGridView_AssessorID.Value);

                    if (!Utility.IsNullOrEmpty(CandidateDashboard_intelliVIEWGridView_AttemptID.Value))
                        attemptID = Convert.ToInt32(CandidateDashboard_intelliVIEWGridView_AttemptID.Value);

                    if (!Utility.IsNullOrEmpty(CandidateDashboard_intelliVIEWGridView_PublishID.Value))
                        urlPublish = CandidateDashboard_intelliVIEWGridView_PublishID.Value;

                    HyperLink CandidateDashboard_intelliVIEWGridView_interviewResultHyperLink =
                       (HyperLink)e.Row.FindControl("CandidateDashboard_intelliVIEWGridView_interviewResultHyperLink");

                    HyperLink CandidateDashboard_intelliVIEWGridView_interviewReportHyperLink =
                       (HyperLink)e.Row.FindControl("CandidateDashboard_intelliVIEWGridView_interviewReportHyperLink");

                    //CandidateDashboard_interviewPublishImage."&assessorId="+ assessorID +
                    if (candidateSessionID != string.Empty && testKey != string.Empty
                        && sessionKey != string.Empty && attemptID > 0)
                    {
                        CandidateDashboard_intelliVIEWGridView_interviewResultHyperLink.NavigateUrl = "~/InterviewReportCenter/InterviewCandidateTestDetails.aspx?m=4&s=0&candidatesession="
                            + candidateSessionID + "&testkey=" + testKey + "&sessionkey=" + sessionKey + "&attemptid=" + attemptID + "&parentpage=MY_ASMT";
                    }

                    if (candidateSessionID != string.Empty && testKey != string.Empty
                        && attemptID > 0)
                    {
                        CandidateDashboard_intelliVIEWGridView_interviewReportHyperLink.NavigateUrl = "~/Assessments/CandidateAssessorSummary.aspx?m=4&s=0&candidatesessionid="
                            + candidateSessionID + "&attemptid=" + attemptID + "&ISK=" + testKey + "&parentpage=MY_ASMT";
                    }

                }
            }
            catch (Exception exp)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_intelliVIEWGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Publish")
            {
                try
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    //  CandidateDashboard_intelliVIEWGridView.Rows[index].FindControl("");

                    HiddenField CandidateDashboard_intelliVIEWGridView_CandidateSessionKeyID = (HiddenField)
                        CandidateDashboard_intelliVIEWGridView.Rows[index].FindControl("CandidateDashboard_intelliVIEWGridView_candidateSessionIDHiddenField");

                    HiddenField CandidateDashboard_intelliTESTGridView_AttemptID = (HiddenField)
                        CandidateDashboard_intelliVIEWGridView.Rows[index].FindControl("CandidateDashboard_intelliVIEWGridView_attemptIDHiddenField");

                    HiddenField CandidateDashboard_intelliTESTGridView_PublishID = (HiddenField)
                        CandidateDashboard_intelliVIEWGridView.Rows[index].FindControl("CandidateDashboard_intelliVIEWGridView_publishHiddenField");

                    InterviewScoreParamDetail interviewScoreParamDetail = null;
                    if (interviewScoreParamDetail == null)
                        interviewScoreParamDetail = new InterviewScoreParamDetail();

                    int attemptID = 0;
                    attemptID = Convert.ToInt32(CandidateDashboard_intelliTESTGridView_AttemptID.Value);

                    interviewScoreParamDetail.CandidateInterviewSessionKey =
                         CandidateDashboard_intelliVIEWGridView_CandidateSessionKeyID.Value.ToString();

                    interviewScoreParamDetail.AttemptID = attemptID;
                    interviewScoreParamDetail.CandidateName = CandidateDashboard_candidateNameTextBox.Text.ToString();
                    CandidateDashboard_ConfirmMsgControl.InterviewParams = interviewScoreParamDetail;

                    CandidateDashboard_emailConfirmation_ModalPopupExtender.Show();
                    CandidateDashboard_ConfirmMsgControl.ShowScoreUrl();
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
                }
            }
        }
        protected void CandidateDashboard_addNotesImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                if (Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
                {
                    base.ShowMessage(CandidateDashboard_errorLabel, "Select candidate");
                    return;
                }

                // Retrieve and check if candidate ID is present.
                int candidateID = Convert.ToInt32(CandidateDashboard_candidateIDHiddenField.Value);

                if (candidateID == 0)
                {
                    base.ShowMessage(CandidateDashboard_errorLabel,
                        "No candidate was selected");

                    return;
                }

                // Add the handler to the add notes link.
                ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenAddNotesCandidateActivity",
                    "javascript: OpenAddNotesCandidateActivity('" + candidateID + "')", true);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_searchImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
                {
                    base.ShowMessage(CandidateDashboard_errorLabel, "Select candidate");
                    return;
                }
                /*if (Utility.IsNullOrEmpty(CandidateDashboard_searchKeywordsTextBox.Text.Trim()))
                {
                    base.ShowMessage(CandidateDashboard_errorLabel, "Enter search keywords");
                    return;
                }*/

                if (CandidateDashboard_candidateIDHiddenField.Value != null)
                {
                    string searchText = null;

                    CandidateDashboard_intelliVIEWPageNavigator.PageSize = 0;
                    CandidateDashboard_intelliVIEWPageNavigator.TotalRecords = 0;
                    CandidateDashboard_intelliVIEWPageNavigator.Reset();

                    CandidateDashboard_intelliTESTPageNavigator.PageSize = 0;
                    CandidateDashboard_intelliTESTPageNavigator.TotalRecords = 0;
                    CandidateDashboard_intelliTESTPageNavigator.Reset();

                    if (CandidateDashboard_searchKeywordsTextBox.Text.Trim() != "")
                        searchText = CandidateDashboard_searchKeywordsTextBox.Text.Trim();

                    TestActivity(Convert.ToInt32(CandidateDashboard_candidateIDHiddenField.Value), 1, 3, searchText);
                    InterviewActivity(Convert.ToInt32(CandidateDashboard_candidateIDHiddenField.Value), 1, 3, searchText);

                    LoadActivities(1);

                }
                CandidateDashboard_skillMatrixTR.Style["display"] = "none";
                CandidateDashboard_candidateSkillMatrixDiv.Style["display"] = "none";
                CandidateDashboard_candidateResumesDiv.Style["display"] = "none";
                CandidateDashboard_displayResumesDownSpan.Style["display"] = "none";
                CandidateDashboard_displayResumesUpSpan.Style["display"] = "block";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_exportExcelImageButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if candidate ID is present.
                if (Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
                {
                    base.ShowMessage(CandidateDashboard_errorLabel, "No candidate was selected");
                    return;
                }

                // Get the table that contains the rows for download.
                DataTable table = GetDownloadTable();

                // Check if table is not null and contain rows.
                if (table == null || table.Rows.Count == 0)
                {
                    base.ShowMessage(CandidateDashboard_errorLabel, "No activities found to download");
                    return;
                }

                // Remove the row where 'total' columns is not null (which 
                // contains the total records used for paging).
                DataRow[] foundRows =
                    table.Select("[TOTAL] is not null", "", DataViewRowState.OriginalRows);

                // Check if rows are found with 'total column is not null'
                if (foundRows != null && foundRows.Length > 0)
                {
                    // Remove the 0th row.
                    table.Rows.Remove(foundRows[0]);
                }

                // Remove the unncessary columns from the table.
                table.Columns.Remove("ACTIVITY_BY");
                table.Columns.Remove("ACTIVITY_TYPE");
                table.Columns.Remove("TOTAL");

                // Change the column names.
                table.Columns["ACTIVITY_DATE"].ColumnName = "Activity Date";
                table.Columns["ACTIVITY_BY_NAME"].ColumnName = "Activity By";
                table.Columns["ACTIVITY_TYPE_NAME"].ColumnName = "Activity";
                table.Columns["COMMENTS"].ColumnName = "Comments";

                using (ExcelPackage pck = new ExcelPackage())
                {
                    // Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add
                        (Constants.ExcelExportSheetName.CANDIDATE_ACTIVITY_LOG);

                    // Load the datatable into the sheet, starting from the 
                    // cell A1 and print the column names on row 1.
                    ws.Cells[Constants.ExcelExport.STARTING_CELL].LoadFromDataTable(table, true);

                    string decimalFormat = Constants.ExcelExportFormats.DECIMAL_COMMA;

                    foreach (DataColumn column in table.Columns)
                    {
                        if (column.DataType == typeof(System.DateTime))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                            {
                                col.Style.Numberformat.Format = Constants.ExcelExportFormats.DATE;
                            }
                        }
                        else if (column.DataType == typeof(decimal))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                            {
                                if (decimalFormat == "C")
                                {
                                    for (int row = 2; row <= ws.Dimension.End.Row; row++)
                                    {
                                        ws.Cells[row, column.Ordinal + 1].Value =
                                            ws.Cells[row, column.Ordinal + 1].Value != null ? ws.Cells[row, column.Ordinal + 1].Value.ToString().Replace(".", ",") : string.Empty;
                                    }
                                    col.Style.Numberformat.Format = "0\",\"0000";
                                }
                                else
                                    col.Style.Numberformat.Format = "###0.0000";
                            }
                        }
                    }

                    // Construct file name.
                    string fileName = string.Format("{0}_{1}",
                        Constants.ExcelExportFileName.CANDIDATE_ACTIVITY_LOG,
                        Utility.GetValidExportFileNameString(CandidateDashboard_candidateNameTextBox.Text, 50));

                    Response.ContentType = "application/octetstream";// "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                    Response.BinaryWrite(pck.GetAsByteArray());
                    Response.End();

                    /*  Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT] = null;

                      Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT] = pck.GetAsByteArray();

                    string script = "javascript:DownloadCandidateResume('" + fileName + "','" + "xlsx'" + ")";

                      ScriptManager.RegisterStartupScript(this, this.GetType(), "downloadresume", script, true); */

                    /* AssessInterviewResponse_exportExcelHyperLink.NavigateUrl = "../Common/CandidateImageHandler.ashx?filepath=" + fileName + "&filetype=xlsx";
                      string downloadScript = "<script>window.location.href = document.getElementById('AssessInterviewResponse_exportExcelHyperLink').href;</script>";
                      ScriptManager.RegisterStartupScript(this, this.GetType(), "exceldownload", downloadScript, false); */

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_skillMatrix_Command(object sender, CommandEventArgs e)
        {
            if (Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
            {
                base.ShowMessage(CandidateDashboard_errorLabel, "Select candidate");
                return;
            }

            if (e.CommandName.ToString() == "SkillMatrixUp")
            {
                /*CandidateDashboard_candidateSkillMatrixDiv.Style["display"] = "block";
                CandidateDashboard_candidateSkillMatrixDiv.Style["height"] = "250px";
                CandidateDashboard_candidateSkillMatrixDiv.Style["width"] = "900px";
                CandidateDashboard_candidateSkillMatrixDiv.Style["overflow"] = "auto";*/
                //CandidateDashboard_skillMatrixUpSpan.Style["display"] = "none";
                // CandidateDashboard_skillMatrixDownSpan.Style["display"] = "block";
            }
            else
            {
                /*CandidateDashboard_candidateSkillMatrixDiv.Style["display"] = "none";
                CandidateDashboard_candidateSkillMatrixDiv.Style["height"] = "0px";*/
                //CandidateDashboard_skillMatrixDownSpan.Style["display"] = "none";
                //CandidateDashboard_skillMatrixUpSpan.Style["display"] = "block";

            }
        }

        protected void CandidateDashboard_skillMatrixGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //Header section
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[1].BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    e.Row.Cells[1].Attributes.Add("style", "text-align:center;height:50px;width:70px;");

                    for (int i = 2; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].Attributes.Add("style", "text-align:center;height:50px;width:70px;");
                        e.Row.Cells[i].BackColor = i % 2 == 0 ? ColorTranslator.FromHtml("#DAE6EB") :
                                               ColorTranslator.FromHtml("#BDC9CE");
                    }
                }

                //Item section
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[0].Attributes.Add("style", "text-align:center;width:100px;");

                    e.Row.Cells[0].Attributes.Add("style", "text-align:center;width:100px;");
                    e.Row.Cells[0].BackColor = e.Row.RowIndex % 2 == 0 ? ColorTranslator.FromHtml("#A3C0CD") :
                                               ColorTranslator.FromHtml("#BCD2DC");

                    e.Row.Cells[1].Attributes.Add("style", "text-align:center;height:50px;width:70px;");
                    e.Row.Cells[1].Attributes.Add("style", "text-align:center;height:50px;width:70px;");
                    e.Row.Cells[1].BackColor = e.Row.RowIndex % 2 == 0 ? ColorTranslator.FromHtml("#DAE6EB") :
                                               ColorTranslator.FromHtml("#BDC9CE");

                    string columnColor = "#77CDE9";
                    string alternateColoumnColor = "#BEE562";

                    if (e.Row.RowIndex % 2 == 0)
                    {
                        for (int i = 2; i < e.Row.Cells.Count; i++)
                        {
                            e.Row.Cells[i].Attributes.Add("style", "text-align:center;height:50px;width:70px;");
                            e.Row.Cells[i].Attributes.Add("style", "text-align:center;height:50px;width:70px;");
                            e.Row.Cells[i].BackColor = i % 2 == 0 ? ColorTranslator.FromHtml(columnColor) :
                                              ColorTranslator.FromHtml(alternateColoumnColor);
                        }
                    }
                    else
                    {
                        for (int i = 2; i < e.Row.Cells.Count; i++)
                        {
                            e.Row.Cells[i].Attributes.Add("style", "text-align:center;height:50px;width:70px;");
                            e.Row.Cells[i].BackColor = i % 2 == 0 ? ColorTranslator.FromHtml(alternateColoumnColor) :
                                              ColorTranslator.FromHtml(columnColor);
                        }
                    }

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_skillMatrixGridView_PreRender(object sender, EventArgs e)
        {
            try
            {
                SpanRows(CandidateDashboard_skillMatrixGridView);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_candidateResumesDataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                DataListItem dataListItem = e.Item;

                CandidateResumeDetails resumeDetail = null;

                resumeDetail = (CandidateResumeDetails)dataListItem.DataItem;

                Label CandidateDashboard_resumeNameLabel = (Label)e.Item.FindControl("CandidateDashboard_resumeNameLabel");
                if (CandidateDashboard_resumeNameLabel != null)
                    CandidateDashboard_resumeNameLabel.Text = resumeDetail.FileName;

                Label CandidateDashboard_uploadedByLabel = (Label)e.Item.FindControl("CandidateDashboard_uploadedByLabel");
                if (CandidateDashboard_uploadedByLabel != null)
                    CandidateDashboard_uploadedByLabel.Text = resumeDetail.Recruiter;

                Label CandidateDashboard_processDateLabel = (Label)e.Item.FindControl("CandidateDashboard_processDateLabel");
                if (CandidateDashboard_processDateLabel != null)
                    CandidateDashboard_processDateLabel.Text = "Uploaded on " + resumeDetail.ProcessedDate.ToShortDateString();

                HiddenField CandidateDashboard_resumeCandidateIDHiddenField = (HiddenField)e.Item.FindControl("CandidateDashboard_resumeCandidateIDHiddenField");
                if (CandidateDashboard_resumeCandidateIDHiddenField != null)
                    CandidateDashboard_resumeCandidateIDHiddenField.Value = resumeDetail.CandidateId.ToString();

                HiddenField CandidateDashboard_resumeIDHiddenField = (HiddenField)e.Item.FindControl("CandidateDashboard_resumeIDHiddenField");
                if (CandidateDashboard_resumeIDHiddenField != null)
                    CandidateDashboard_resumeIDHiddenField.Value = resumeDetail.CandidateResumeId.ToString();

                ImageButton CandidateDashboard_downloadResumeImageButton = (ImageButton)e.Item.FindControl("CandidateDashboard_downloadResumeImageButton");
                if (CandidateDashboard_downloadResumeImageButton != null)
                {
                    if (resumeDetail.MimeType.ToLower() == "doc" || resumeDetail.MimeType.ToLower() == "docx")
                        CandidateDashboard_downloadResumeImageButton.ImageUrl = "~/App_Themes/DefaultTheme/Images/icon_word.gif";
                    else
                        CandidateDashboard_downloadResumeImageButton.ImageUrl = "~/App_Themes/DefaultTheme/Images/icon_pdf.gif";

                    CandidateDashboard_downloadResumeImageButton.CommandArgument = resumeDetail.CandidateResumeId.ToString();
                    CandidateDashboard_downloadResumeImageButton.OnClientClick = "javascript:return DownloadResumeID('" + resumeDetail.CandidateResumeId.ToString() + "');";
                }

                ImageButton CandidateDashboard_viewResumeSkillImageButton = (ImageButton)e.Item.FindControl("CandidateDashboard_viewResumeSkillImageButton");
                if (CandidateDashboard_viewResumeSkillImageButton != null)
                {
                    CandidateDashboard_viewResumeSkillImageButton.ImageUrl = "../App_Themes/DefaultTheme/Images/icon_skilldetail.gif";
                    CandidateDashboard_viewResumeSkillImageButton.CommandArgument = resumeDetail.CandidateResumeId.ToString();

                    CandidateDashboard_viewResumeSkillImageButton.Command += new CommandEventHandler(CandidateDashboard_viewResumeSkillImageButton_Command);
                }

                ImageButton CandidateDashboard_deleteResumeImageButton = (ImageButton)e.Item.FindControl("CandidateDashboard_deleteResumeImageButton");
                CandidateDashboard_deleteResumeImageButton.Command += new CommandEventHandler(CandidateDashboard_deleteResumeImageButton_Command);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_deleteResumeImageButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteResume")
                {
                    if (Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
                    {
                        base.ShowMessage(CandidateDashboard_errorLabel, "Select candidate");
                        return;
                    }

                    int resumeID = 0;

                    if (e.CommandArgument != null)
                    {
                        resumeID = Convert.ToInt32(e.CommandArgument);
                    }

                    if (resumeID == 0) return;

                    ViewState["CANDIADTE_RESUME_ID"] = resumeID;

                    // Show the delete confirmation message.
                    // Set message, title and type for the confirmation control for edit entry.
                    CandidateDashboard_deleteResume_confirmMessageControl.Message =
                        "Are you sure to delete the resume ?";
                    CandidateDashboard_deleteResume_confirmMessageControl.Title =
                        "Delete Resume";
                    CandidateDashboard_deleteResume_confirmMessageControl.Type =
                        MessageBoxType.YesNo;

                    CandidateDashboard_deleteResume_confirmModalPopupExtender.Show();
                    CandidateDashboard_deleteResumeUpdatePanel.Update();

                    CandidateDashboard_displayResumesDownSpan.Style["display"] = "block";
                    CandidateDashboard_displayResumesUpSpan.Style["display"] = "none";

                    CandidateDashboard_candidateResumesDiv.Style["display"] = "block";
                    CandidateDashboard_candidateResumesDiv.Style["height"] = "220px";
                    CandidateDashboard_candidateResumesDiv.Style["width"] = "900px";
                    CandidateDashboard_candidateResumesDiv.Style["overflow"] = "auto";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_viewResumeSkillImageButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "SkillMatrix")
                {
                    if (Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
                    {
                        base.ShowMessage(CandidateDashboard_errorLabel, "Select candidate");
                        return;
                    }

                    int candidateID = 0;

                    if (!Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
                        candidateID = Convert.ToInt32(CandidateDashboard_candidateIDHiddenField.Value.ToString());
                    else
                        candidateID = 0;


                    int resumeID = Convert.ToInt32(e.CommandArgument.ToString());

                    BindSkillMatrix(candidateID, resumeID);

                    CandidateDashboard_skillMatrixTR.Style["display"] = "block";
                    CandidateDashboard_skillMatrixTR.Style["width"] = "900px";

                    CandidateDashboard_candidateSkillMatrixDiv.Style["display"] = "block";

                    CandidateDashboard_candidateSkillMatrixDiv.Style["height"] = "220px";
                    CandidateDashboard_candidateSkillMatrixDiv.Style["width"] = "900px";
                    CandidateDashboard_candidateSkillMatrixDiv.Style["overflow"] = "auto";

                    CandidateDashboard_displayResumesDownSpan.Style["display"] = "block";
                    CandidateDashboard_displayResumesUpSpan.Style["display"] = "none";

                    CandidateDashboard_candidateResumesDiv.Style["display"] = "block";
                    CandidateDashboard_candidateResumesDiv.Style["height"] = "220px";
                    CandidateDashboard_candidateResumesDiv.Style["width"] = "900px";
                    CandidateDashboard_candidateResumesDiv.Style["overflow"] = "auto";
                }
                else
                {
                    CandidateDashboard_skillMatrixTR.Style["display"] = "none";
                    CandidateDashboard_candidateSkillMatrixDiv.Style["display"] = "none";
                    CandidateDashboard_candidateResumesDiv.Style["display"] = "none";
                    CandidateDashboard_displayResumesDownSpan.Style["display"] = "none";
                    CandidateDashboard_displayResumesUpSpan.Style["display"] = "block";

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked in
        /// the delete resume confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will delete the resume for the selected candidate
        /// </remarks>
        protected void CandidateDashboard_deleteResume_okButtonClick(object sender, EventArgs e)
        {
            try
            {
                // Check if candidate resume ID available.
                if (Utility.IsNullOrEmpty(ViewState["CANDIADTE_RESUME_ID"]))
                {
                    base.ShowMessage(CandidateDashboard_errorLabel, "No resume found to delete");
                    return;
                }

                // Delete the resume.
                new CandidateBLManager().DeleteCandidateResume(Convert.ToInt32
                    (ViewState["CANDIADTE_RESUME_ID"]));

                // Reset the value.
                ViewState["CANDIADTE_RESUME_ID"] = null;

                // Reload the data.
                ResumesHistory(int.Parse(CandidateDashboard_candidateIDHiddenField.Value));

                CandidateDashboard_displayResumesDownSpan.Style["display"] = "block";
                CandidateDashboard_displayResumesUpSpan.Style["display"] = "none";

                CandidateDashboard_candidateResumesDiv.Style["display"] = "block";
                CandidateDashboard_candidateResumesDiv.Style["height"] = "220px";
                CandidateDashboard_candidateResumesDiv.Style["width"] = "900px";
                CandidateDashboard_candidateResumesDiv.Style["overflow"] = "auto";

                // Show a success message.
                base.ShowMessage(CandidateDashboard_SuccessMessageLabel, "Candidate resume deleted successfully");
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CandidateDashboard_errorLabel, exception.Message);
            }
        }


        protected void CandidateDashboard_candidateEditLinkButton_Click(object sender, EventArgs e)
        {
            EditControlsVisible(false, true);
        }

        protected void CandidateDashboard_candidateUpdateLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
                {
                    if (Utility.IsNullOrEmpty(CandidateDashboard_candidateFirstNameTextBox.Text))
                    {
                        base.ShowMessage(CandidateDashboard_errorLabel, "Enter the first name");
                        return;
                    }
                    if (Utility.IsNullOrEmpty(CandidateDashboard_candidateLastNameTextBox.Text))
                    {
                        base.ShowMessage(CandidateDashboard_errorLabel, "Enter the last name");
                        return;
                    }

                    int candiateID = Convert.ToInt32(CandidateDashboard_candidateIDHiddenField.Value);
                    CandidateInformation candidateInformation = new CandidateInformation();

                    candidateInformation.caiFirstName = CandidateDashboard_candidateFirstNameTextBox.Text.Trim(); ;
                    candidateInformation.caiLastName = CandidateDashboard_candidateLastNameTextBox.Text.Trim();
                    candidateInformation.caiID = candiateID;

                    UpdateCandiadteName(candidateInformation);

                    Candidate_Information(candiateID);
                    base.ShowMessage(CandidateDashboard_SuccessMessageLabel, "Updated successfully");
                }

                EditControlsVisible(true, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_candidateCancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
                {
                    int candiateID = Convert.ToInt32(CandidateDashboard_candidateIDHiddenField.Value);
                    Candidate_Information(candiateID);
                }
                EditControlsVisible(true, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        protected void CandidateDashboard_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        #endregion

        #region Protected Overridden Methods
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Private Methods
        private void EditControlsVisible(bool flag1, bool flag2)
        {
            CandidateDashboard_candidateEditLinkButton.Visible = flag1;
            CandidateDashboard_candidateNameLabel.Visible = flag1;

            /*CandidateDashboard_candidateFirstNameTextBox.Visible = flag2;
            CandidateDashboard_candidateUpdateLinkButton.Visible = flag2;
            CandidateDashboard_candidateLastNameTextBox.Visible = flag2;
            CandidateDashboard_candidateCancelLinkButton.Visible = flag2;*/
            if (flag2 == true)
                CandidateDashboard_candidateNameChangeDiv.Style["display"] = "block";
            else
                CandidateDashboard_candidateNameChangeDiv.Style["display"] = "none";
        }

        private void ResumesHistory(int candidateID)
        {
            CandidateDashboard_candidateResumesDataList.DataSource = null;
            CandidateDashboard_candidateResumesDataList.DataBind();

            CandidateDashboard_candidateResumesDataList.DataSource =
                new ResumeRepositoryBLManager().GetCandidateReviewResumes(candidateID);

            CandidateDashboard_candidateResumesDataList.DataBind();
        }

        /// <summary>
        /// Method that retrieves the allow delete resume status.
        /// </summary>
        /// <param name="candidateResumeID">
        /// A <see cref="int"/> that holds the candidate resume ID.
        /// </param>
        /// <param name="recruiterID">
        /// A <see cref="recruiterID"/> that holds the recruiter ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of allow delete 
        /// status. True represents allow delete and false do not allow
        /// </returns>
        /// <remarks>
        /// Corporate admin can delete all candidate resume. Other users can 
        /// delete their own candidate resume only.
        /// </remarks>
        protected bool IsAllowDeleteResume(int candidateResumeID, int recruiterID)
        {
            // Check if candidate resume ID is valid.
            if (candidateResumeID == 0)
                return false;

            // Check if user is corporate admin.
            if (((UserDetail)Session["USER_DETAIL"]).SubscriptionRole ==
                Enum.GetName(typeof(SubscriptionRolesEnum), 3))
                return true;
            else
                return (recruiterID == base.userID ? true : false);
        }

        private void LoadActivities(int pageNumber)
        {
            int totalRecords = 0;

            if (GetSearchCriteria() == null) return;
            // Get search critera.
            CandidateActivityLogSearchCriteria searchCriteria = GetSearchCriteria();

            // Get searched results.
            List<CandidateActivityLog> logs = new ResumeRepositoryBLManager().GetCandidateActivities(
                    searchCriteria,
                    pageNumber,
                    this.GridPageSize,
                     ViewState["SORT_FIELD"].ToString(),
                    ((SortType)ViewState["SORT_ORDER"]),
                    out totalRecords);

            CandidateDashboard_candidateActivityGridView.DataSource = logs;
            CandidateDashboard_candidateActivityGridView.DataBind();
            CandidateDashboard_candidateActivityPageNavigator.PageSize = this.GridPageSize;
            CandidateDashboard_candidateActivityPageNavigator.TotalRecords = totalRecords;

            GetExportExcelData();

            string fileName = string.Format("{0}_{1}",
                       Constants.ExcelExportFileName.CANDIDATE_ACTIVITY_LOG,
                       Utility.GetValidExportFileNameString(CandidateDashboard_candidateNameTextBox.Text, 50));

            CandidateDashboard_exportExcelImageButton.Attributes.Add("onclick", "javascript:DownloadCandidateResume('" + fileName + "','" + "xlsx'" + ")");
        }

        private CandidateActivityLogSearchCriteria GetSearchCriteria()
        {
            string date = string.Empty;

            if (Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
            {
                base.ShowMessage(CandidateDashboard_errorLabel, "Select candidate");
                return null;
            }

            CandidateActivityLogSearchCriteria searchCriteria = new CandidateActivityLogSearchCriteria();
            int candidateID = 0;
            //TextBox txt = (TextBox)this.FindControl("CandidateDashboard_candidateNameLabelTextBox");
            //CandidateDashboard_candidateIDHiddenField.Value 
            //HiddenField hiddenfield_CandidateID = (HiddenField)this.FindControl("CandidateDashboard_candidateIDHiddenField");
            if (!Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
                candidateID = Convert.ToInt32(CandidateDashboard_candidateIDHiddenField.Value.ToString());
            else
                candidateID = 0;

            searchCriteria.CandidateID = candidateID;
            searchCriteria.ActivityType = string.Empty;

            /*if (!Utility.IsNullOrEmpty(CandidateActivityViewerControl_activityByHiddenField.Value))
                searchCriteria.ActivityBy = Convert.ToInt32(CandidateActivityViewerControl_activityByHiddenField.Value);*/

            /*searchCriteria.ActivityDateFrom = date == string.Empty ?
                Convert.ToDateTime("1/1/1753") : Convert.ToDateTime(DateTime.Now);
            
            searchCriteria.ActivityDateTo = date == string.Empty ?
                Convert.ToDateTime("12/31/9999") : Convert.ToDateTime(DateTime.Now).AddDays(1); */

            searchCriteria.ActivityDateFrom = Convert.ToDateTime(DateTime.Now);
            searchCriteria.ActivityDateTo = Convert.ToDateTime(DateTime.Now).AddDays(1);

            searchCriteria.Comments = CandidateDashboard_searchKeywordsTextBox.Text;//CandidateActivityViewerControl_commentsTextBox.Text.Trim(); 

            return searchCriteria;
        }

        private void Load_CandidateActivities(int candidateID)
        {
            string searchText = null;
            //string fileName = null;
            //string mimeType = null;
            CandidateDashboard_skillMatrixGridView.DataSource = null;
            CandidateDashboard_skillMatrixGridView.DataBind();

            if (CandidateDashboard_searchKeywordsTextBox.Text.Trim() != "")
                searchText = CandidateDashboard_searchKeywordsTextBox.Text.Trim();

            Candidate_Information(candidateID);
            CandidateDashboard_candidatePhotoImage.ImageUrl =
           "~/Common/CandidateImageHandler.ashx?source=CAS_PAGE&candidateid=" + candidateID;

            TestActivity(candidateID, 1, 3, searchText);
            InterviewActivity(candidateID, 1, 3, searchText);

            LoadActivities(1);
            //BindSkillMatrix(candidateID);
            ResumesHistory(candidateID);
        }

        private void Candidate_Information(int candidateID)
        {
            CandidateDetail candidateDetail =
                new CandidateBLManager().GetCandidateInformation(candidateID);

            if (candidateDetail == null) return;

            CandidateDashboard_candidateNameLabel.Text = candidateDetail.FirstName + " " + candidateDetail.LastName;

            CandidateDashboard_candidateFirstNameTextBox.Text = candidateDetail.FirstName;
            CandidateDashboard_candidateLastNameTextBox.Text = candidateDetail.LastName;

            if (CandidateDashboard_candidateNameTextBox.Text == "")
                CandidateDashboard_candidateNameTextBox.Text = candidateDetail.FirstName;

            CandidateDashboard_mobileNoLabel.Text = candidateDetail.Mobile;
            if (candidateDetail.EMailID != null)
            {
                CandidateDashboard_emailIDHyperLink.Text = "<a href=mailto:" + candidateDetail.EMailID + ">" + candidateDetail.EMailID + "</a>";
                CandidateDashboard_emailIDHyperLink.ToolTip = "Click here to send email the candidate";
            }
            CandidateDashboard_recruitedByLabel.Text = "Candidate record created by " + candidateDetail.RecruiterName;
        }

        private void TestActivity(int candidateID, int pageNumber, int pageSize, string searchText)
        {
            CandidateDashboard_intelliTESTGridView.DataSource = null;
            CandidateDashboard_intelliTESTGridView.DataBind();
            CandidateDashboard_intelliTESTPageNavigator.PageSize = 1;
            CandidateDashboard_intelliTESTPageNavigator.TotalRecords = 1;
            int totalRecords;
            List<CandidateTestSessionDetail> listTestSession =
                new CandidateBLManager().GetTestActivityDashboard(candidateID, pageNumber, pageSize, searchText, out totalRecords);

            if (listTestSession != null)
            {
                CandidateDashboard_intelliTEST_messgeLabel.Visible = false;
                CandidateDashboard_intelliTESTGridView.DataSource = listTestSession;
                CandidateDashboard_intelliTESTGridView.DataBind();

                CandidateDashboard_intelliTESTPageNavigator.Visible = true;
                CandidateDashboard_intelliTESTPageNavigator.PageSize = 3;
                CandidateDashboard_intelliTESTPageNavigator.TotalRecords = totalRecords;
            }
            else
            {
                CandidateDashboard_intelliTEST_messgeLabel.Visible = true;
                CandidateDashboard_intelliTEST_messgeLabel.Text = "No records found";
                CandidateDashboard_intelliTESTPageNavigator.Visible = false;
            }
        }

        private void InterviewActivity(int candidateID, int pageNumber, int pageSize, string searchText)
        {
            CandidateDashboard_intelliVIEWGridView.DataSource = null;
            CandidateDashboard_intelliVIEWGridView.DataBind();
            int totalRecords;

            List<CandidateInterviewSessionDetail> listInterviewSession =
                new CandidateBLManager().GetInterviewActivityDashboard(candidateID, pageNumber, pageSize, searchText, out totalRecords);
            CandidateDashboard_intelliVIEWPageNavigator.TotalRecords = 1;
            if (listInterviewSession != null)
            {
                CandidateDashboard_intelliVIEW_messageLabel.Visible = false;
                CandidateDashboard_intelliVIEWGridView.DataSource = listInterviewSession;
                CandidateDashboard_intelliVIEWGridView.DataBind();

                CandidateDashboard_intelliVIEWPageNavigator.Visible = true;
                CandidateDashboard_intelliVIEWPageNavigator.PageSize = 3;
                CandidateDashboard_intelliVIEWPageNavigator.TotalRecords = totalRecords;
            }
            else
            {
                CandidateDashboard_intelliVIEW_messageLabel.Visible = true;
                CandidateDashboard_intelliVIEW_messageLabel.Text = "No records found";
                CandidateDashboard_intelliVIEWPageNavigator.Visible = false;
            }
        }

        private byte[] Candidate_resumeContent(int candidateID, out string fileName, out string mimeType)
        {
            byte[] resumeContent = null;
            fileName = "";
            mimeType = "";
            try
            {
                Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT] = null;

                resumeContent = new CandidateBLManager().GetResumeContentsByCandidiateID(int.Parse(CandidateDashboard_candidateIDHiddenField.Value),
                    out mimeType, out fileName);

                Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT] = resumeContent;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);

            }
            return resumeContent;
        }
        /// <summary>
        /// This method loads Skill Matrix for a specific candidate id
        /// </summary>
        /// <param name="candidateSessionID"></param>
        private void BindSkillMatrix(int candidateID, int resumeID)
        {
            DataTable dtSkillMarix = new DataTable();
            DataTable dtBindSkills = new DataTable();
            DataTable dtVector = new DataTable();
            DataSet dsSkillMarix = new DataSet();

            CandidateDashboard_skillMatrixGridView.Columns.Clear();
            CandidateDashboard_skillMatrixGridView.DataSource = null;
            CandidateDashboard_skillMatrixGridView.DataBind();


            dsSkillMarix = new AdminBLManager().GetCandidateSkillSubstance(candidateID, resumeID);

            if (dsSkillMarix.Tables[0].Rows.Count <= 0)
            {
                base.ShowMessage(CandidateDashboard_errorLabel, "Skills not available this resume");
                /*CandidateDashboard_skillMatrixDownSpan.Visible = false;
                CandidateDashboard_skillMatrixUpSpan.Visible = false;*/
                return;
            }
            /*else
            {
                CandidateDashboard_skillMatrixDownSpan.Visible = true;
                CandidateDashboard_skillMatrixUpSpan.Visible = true;
            } */

            DataView dvSkillMarix = dsSkillMarix.Tables[0].DefaultView;
            dtSkillMarix = dvSkillMarix.ToTable(true, "PARAMETER_NAME");
            BoundField skillColumn = null;
            skillColumn = new BoundField();
            skillColumn.DataField = "GROUP_NAME";
            skillColumn.HeaderText = "";
            CandidateDashboard_skillMatrixGridView.Columns.Add(skillColumn);
            skillColumn = new BoundField();
            skillColumn.DataField = "VECTOR_NAME";
            skillColumn.HeaderText = "";
            CandidateDashboard_skillMatrixGridView.Columns.Add(skillColumn);
            for (int i = 0; i < dtSkillMarix.Rows.Count; i++)
            {
                dtBindSkills.Columns.Add(dtSkillMarix.Rows[i]["PARAMETER_NAME"].ToString().Trim());
                skillColumn = new BoundField();
                skillColumn.DataField = dtSkillMarix.Rows[i]["PARAMETER_NAME"].ToString().ToString().Trim();
                skillColumn.HeaderText = dtSkillMarix.Rows[i]["PARAMETER_NAME"].ToString().ToString().Trim();
                CandidateDashboard_skillMatrixGridView.Columns.Add(skillColumn);
            }
            dtBindSkills.Columns.Add("VECTOR_NAME");
            dtBindSkills.Columns.Add("GROUP_NAME");
            DataView dvVector = dsSkillMarix.Tables[0].DefaultView;
            dtVector = dvVector.ToTable(true, "VECTOR_NAME");
            DataView dvRowFilter = dsSkillMarix.Tables[0].DefaultView;
            for (int j = 0; j < dtVector.Rows.Count; j++)
            {
                string strFilter = "VECTOR_NAME = '" + dtVector.Rows[j][0].ToString().Trim() + "'";
                dvRowFilter.RowFilter = strFilter;
                DataTable dtFilteredVector = dvRowFilter.ToTable();
                DataRow dRow = dtBindSkills.NewRow();
                for (int k = 0; k < dtFilteredVector.Rows.Count; k++)
                {
                    int i = 0;
                    for (i = 0; i < dtSkillMarix.Rows.Count; i++)
                    {
                        if (dtSkillMarix.Rows[i][0].ToString().Trim() == dtFilteredVector.Rows[k]["PARAMETER_NAME"].ToString().Trim())
                        {
                            dRow[i] = dtFilteredVector.Rows[k]["COMPETENCY_VALUE"].ToString().Trim();
                        }
                    }
                    dRow[i] = dtVector.Rows[j][0].ToString().Trim();
                    dRow[i + 1] = dtFilteredVector.Rows[0]["GROUP_NAME"].ToString().Trim();
                }
                dtBindSkills.Rows.Add(dRow);
            }
            dtBindSkills.Columns["GROUP_NAME"].SetOrdinal(dtBindSkills.Columns[0].Ordinal);
            dtBindSkills.Columns["VECTOR_NAME"].SetOrdinal(dtBindSkills.Columns[1].Ordinal);
            //Bind data table value    
            DataView dv = new DataView(dtBindSkills);
            dv.Sort = "Recency DESC";
            CandidateDashboard_skillMatrixGridView.DataSource = dv;
            CandidateDashboard_skillMatrixGridView.DataBind();
        }

        /// <summary>
        /// This method spans the rows for group name
        /// </summary>
        /// <param name="GridView"></param>        
        private static void SpanRows(GridView CandidateDashboard_skillMatrixGridView)
        {
            for (int rowIndex = CandidateDashboard_skillMatrixGridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = CandidateDashboard_skillMatrixGridView.Rows[rowIndex];
                GridViewRow previousRow = CandidateDashboard_skillMatrixGridView.Rows[rowIndex + 1];
                if (row.Cells[0].Text == previousRow.Cells[0].Text)
                {
                    row.Cells[0].RowSpan = previousRow.Cells[0].RowSpan < 2 ? 2 :
                                           previousRow.Cells[0].RowSpan + 1;
                    previousRow.Cells[0].Visible = false;
                }
            }
        }

        /// <summary>
        /// Method that show the preview for the uploaded photo. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This method is called once a candidate photo has been uploaded.
        /// </remarks>
        private void ShowPreview(int candidateID)
        {
            try
            {
                // Reset the status.
                CandidateDashboard_photoUploadedHiddenFiled.Value = "N";

                // Check if photo upload control has file.
                if (!CandidateDashboard_selectPhotoFileUpload.HasFile)
                {
                    base.ShowMessage(CandidateDashboard_errorLabel,
                        "No photo was selected");

                    return;
                }

                // Check if the photo is of the desired extension.
                string extension = Path.GetExtension(CandidateDashboard_selectPhotoFileUpload.FileName);
                extension = extension.ToLower();

                if (extension != ".gif" && extension != ".png" && extension != ".jpg" && extension != ".jpeg")
                {
                    base.ShowMessage(CandidateDashboard_errorLabel,
                        "Only gif/png/jpg/jpeg files are allowed");

                    return;
                }

                // Check if photo size exceeds the maximum limit.
                int maxFileSize = Convert.ToInt32
                    (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_MAX_SIZE_IN_BYTES"]);

                if (CandidateDashboard_selectPhotoFileUpload.FileBytes.Length > maxFileSize)
                {
                    base.ShowMessage(CandidateDashboard_errorLabel,
                       string.Format("File size exceeds the maximum limit of {0} bytes", maxFileSize));

                    return;
                }

                // Retrieve the original photo from the file upload control.
                System.Drawing.Image originalPhoto = System.Drawing.Image.FromStream
                    (new MemoryStream(CandidateDashboard_selectPhotoFileUpload.FileBytes));

                int thumbnailWidth = Convert.ToInt32
                    (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_THUMBNAIL_WIDTH"]);
                int thumbnailHeight = Convert.ToInt32
                    (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_THUMBNAIL_HEIGHT"]);

                // Check the width and height of the original image exceeds the
                // size of the standard size. If exceeds convert the image to
                // thumbnail of the standard size and store.
                if (originalPhoto.Width > thumbnailWidth && originalPhoto.Height > thumbnailHeight)
                {
                    // Resize both width and height.
                    System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                        (thumbnailWidth, thumbnailHeight, null, System.IntPtr.Zero);

                    // Keep the thumbnail photo in the session.
                    Session["CANDIDATE_THUMBNAIL_PHOTO"] = thumbnailPhoto;
                }
                else if (originalPhoto.Width > thumbnailWidth)
                {
                    // Resize only the width.
                    System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                        (thumbnailWidth, originalPhoto.Height, null, System.IntPtr.Zero);

                    // Keep the thumbnail photo in the session.
                    Session["CANDIDATE_THUMBNAIL_PHOTO"] = thumbnailPhoto;
                }
                else if (originalPhoto.Height > thumbnailHeight)
                {
                    // Resize only the height.
                    System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                        (originalPhoto.Width, thumbnailHeight, null, System.IntPtr.Zero);

                    // Keep the thumbnail photo in the session.
                    Session["CANDIDATE_THUMBNAIL_PHOTO"] = thumbnailPhoto;
                }
                else
                {
                    // Keep the original photo in the session.
                    Session["CANDIDATE_THUMBNAIL_PHOTO"] = originalPhoto;
                }

                // Set the phto changed status.
                Session["PHOTO_CHANGED"] = true;
                Save_CandiatePhoto(candidateID);
                // Set the visibility of the 'clear' photo link.
                //CreateCandidate_selectPhotoClearLinkButton.Visible = true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
            }
        }

        private void Save_CandiatePhoto(int candidateID)
        {
            // Save candidate photo.
            if (!Utility.IsNullOrEmpty(Session["PHOTO_CHANGED"]) &&
                Convert.ToBoolean(Session["PHOTO_CHANGED"]) == true)
            {
                System.Drawing.Image thumbnail = null;
                if (Session["CANDIDATE_THUMBNAIL_PHOTO"] != null)
                {
                    thumbnail = Session["CANDIDATE_THUMBNAIL_PHOTO"] as System.Drawing.Image;
                }

                // Save the thumbnail image into server path.
                string filePath = Server.MapPath("~/") +
                    ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                    "\\" + candidateID + "." +
                    ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];

                // Check if the file exist. If exist, then remove it.
                if (File.Exists(filePath))
                    File.Delete(filePath);

                thumbnail.Save(filePath, base.GetCandidatePhotoFormat());

                // Reset the status.
                Session["PHOTO_CHANGED"] = false;

                //Clear session 
                Session["CANDIDATE_THUMBNAIL_PHOTO"] = null;
            }
        }

        private void UpdateCandiadteName(CandidateInformation candidateInformation)
        {
            new ResumeRepositoryBLManager().UpdateCandidateNames(candidateInformation, base.userID);
        }

        private void GetExportExcelData()
        {
            // Check if candidate ID is present.
            if (Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
            {
                base.ShowMessage(CandidateDashboard_errorLabel, "No candidate was selected");
                return;
            }

            // Get the table that contains the rows for download.
            DataTable table = GetDownloadTable();

            // Check if table is not null and contain rows.
            if (table == null || table.Rows.Count == 0)
            {
                base.ShowMessage(CandidateDashboard_errorLabel, "No activities found to download");
                return;
            }

            // Remove the row where 'total' columns is not null (which 
            // contains the total records used for paging).
            DataRow[] foundRows =
                table.Select("[TOTAL] is not null", "", DataViewRowState.OriginalRows);

            // Check if rows are found with 'total column is not null'
            if (foundRows != null && foundRows.Length > 0)
            {
                // Remove the 0th row.
                table.Rows.Remove(foundRows[0]);
            }

            // Remove the unncessary columns from the table.
            table.Columns.Remove("ACTIVITY_BY");
            table.Columns.Remove("ACTIVITY_TYPE");
            table.Columns.Remove("TOTAL");

            // Change the column names.
            table.Columns["ACTIVITY_DATE"].ColumnName = "Activity Date";
            table.Columns["ACTIVITY_BY_NAME"].ColumnName = "Activity By";
            table.Columns["ACTIVITY_TYPE_NAME"].ColumnName = "Activity";
            table.Columns["COMMENTS"].ColumnName = "Comments";

            using (ExcelPackage pck = new ExcelPackage())
            {
                // Create the worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add
                    (Constants.ExcelExportSheetName.CANDIDATE_ACTIVITY_LOG);

                // Load the datatable into the sheet, starting from the 
                // cell A1 and print the column names on row 1.
                ws.Cells[Constants.ExcelExport.STARTING_CELL].LoadFromDataTable(table, true);

                string decimalFormat = Constants.ExcelExportFormats.DECIMAL_COMMA;

                foreach (DataColumn column in table.Columns)
                {
                    if (column.DataType == typeof(System.DateTime))
                    {
                        using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                        {
                            col.Style.Numberformat.Format = Constants.ExcelExportFormats.DATE;
                        }
                    }
                    else if (column.DataType == typeof(decimal))
                    {
                        using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                        {
                            if (decimalFormat == "C")
                            {
                                for (int row = 2; row <= ws.Dimension.End.Row; row++)
                                {
                                    ws.Cells[row, column.Ordinal + 1].Value =
                                        ws.Cells[row, column.Ordinal + 1].Value != null ? ws.Cells[row, column.Ordinal + 1].Value.ToString().Replace(".", ",") : string.Empty;
                                }
                                col.Style.Numberformat.Format = "0\",\"0000";
                            }
                            else
                                col.Style.Numberformat.Format = "###0.0000";
                        }
                    }
                }
                Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT] = null;

                Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT] = pck.GetAsByteArray();
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method that retrieves the table that is used for download.
        /// </summary>
        public DataTable GetDownloadTable()
        {
            try
            {
                // Get search critera.
                CandidateActivityLogSearchCriteria searchCriteria = GetSearchCriteria();

                int totalRecords = 0;

                // Get searched results.
                return new ResumeRepositoryBLManager().GetCandidateActivitiesTable(
                     searchCriteria,
                     1,
                     this.GridPageSize,
                     ViewState["SORT_FIELD"].ToString(),
                    (SortType)SortType.Descending,
                     out totalRecords);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateDashboard_errorLabel, exp.Message);
                return null;
            }
        }

        #endregion

        protected void CandidateDashboard_additionalDocSave_Button_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value)) return;

                if (Session["ADDITIONAL_FILE_NAME"] == null || Session["ADDITIONAL_FILE_SOURCE"] == null) return;
                
                int candidateId = Convert.ToInt32(CandidateDashboard_candidateIDHiddenField.Value);

                if (Session["ADDITIONAL_FILE_NAME"] != null && Session["ADDITIONAL_FILE_SOURCE"] != null)
                {
                    WriteToFile(Session["ADDITIONAL_FILE_NAME"].ToString(), (byte[])Session["ADDITIONAL_FILE_SOURCE"]);

                    string fileNam =Session["ADDITIONAL_FILE_NAME"].ToString();

                    int ind=fileNam.LastIndexOf('\\')+1;

                    string userFileName = fileNam.Substring(ind);

                    string mainDocumentPath = Server.MapPath(AdditionalDocpath);

                    string candidateDocumentPath = mainDocumentPath + "\\" + CandidateDashboard_candidateIDHiddenField.Value;

                    if (!Directory.Exists(candidateDocumentPath))
                        Directory.CreateDirectory(candidateDocumentPath); 
                    
                    AdditionalDocuments additionalDoc = new AdditionalDocuments();
                    additionalDoc.CandidateID = Convert.ToInt32(CandidateDashboard_candidateIDHiddenField.Value);
                    additionalDoc.FileName = userFileName;
                    additionalDoc.FilePath = candidateDocumentPath;
                    additionalDoc.FileExtension = Path.GetExtension(Session["ADDITIONAL_FILE_NAME"].ToString()).ToLower();
                    additionalDoc.UploadedDate = DateTime.Now.Date;
                    additionalDoc.CreatedBy = base.userID;
                    additionalDoc.CreatedDate = DateTime.Now.Date;

                    new CandidateBLManager().InsertAdditionalDocuments(additionalDoc);

                    Session["ADDITIONAL_FILE_SOURCE"] = null;
                    Session["ADDITIONAL_FILE_NAME"] = null;
                }
                 
                LoadCandidateAdditionalDocs(candidateId);

                CandidateDashboard_additionalDoc_UpdatePanel.Update();
                CandidateDashboard_additionalDoc_FileUpload.ClearFileFromPersistedStore();
                CandidateDashboard_additionalDoc_FileUpload.ClearAllFilesFromPersistedStore();
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(CandidateDashboard_errorLabel, ex.Message);
            }
        }

        private void LoadCandidateAdditionalDocs(int candidateID)
        {
            List<AdditionalDocuments> documentList = new CandidateBLManager().GetCandidateAdditionalDocuments(candidateID);
            CandidateDashboard_additionalDocDataList.DataSource = null;
            CandidateDashboard_additionalDocDataList.DataBind();

            CandidateDashboard_additionalDocDataList.DataSource = documentList;
            CandidateDashboard_additionalDocDataList.DataBind();
        }

        // Writes file to current folder
        private void WriteToFile(string strPath, byte[] Buffer)
        {
            // Create a file
            FileStream newFile = new FileStream(strPath, FileMode.Create);

            // Write data to the file
            newFile.Write(Buffer, 0, Buffer.Length);

            // Close file
            newFile.Close();
        }

        /// <summary>
        /// Handler will be called when the browse button is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="AjaxControlToolkit.AsyncFileUploadEventArgs"/> holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply to upload the file to server.
        /// </remarks>
        protected void ResumeEditor_fileUpload_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                if (!CandidateDashboard_additionalDoc_FileUpload.HasFile)
                {
                    base.ShowMessage(CandidateDashboard_errorLabel, "Upload document");
                    return;
                }

                if (Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
                {
                    base.ShowMessage(CandidateDashboard_errorLabel, "Select candidate");
                    return;
                }

                // Validation for file extension
                if (((!Path.GetExtension(e.FileName).ToLower().Contains(".doc")) &&
                    (!Path.GetExtension(e.FileName).ToLower().Contains(".docx")) &&
                    (!Path.GetExtension(e.FileName).ToLower().Contains(".pdf")) &&
                    (!Path.GetExtension(e.FileName).ToLower().Contains(".xls")) &&
                    (!Path.GetExtension(e.FileName).ToLower().Contains(".xlsx")) &&
                    (!Path.GetExtension(e.FileName).ToLower().Contains(".png")) &&
                    (!Path.GetExtension(e.FileName).ToLower().Contains(".bmp")) &&
                    (!Path.GetExtension(e.FileName).ToLower().Contains(".jpeg")) &&
                    (!Path.GetExtension(e.FileName).ToLower().Contains(".jpg")) &&
                    (!Path.GetExtension(e.FileName).ToLower().Contains(".gif"))))
                {
                    base.ShowMessage(CandidateDashboard_errorLabel, "File format supported to doc,docx,xls,xlsx.pdf,png,jpg,gif");
                    return;
                }

                string fileName = CandidateDashboard_additionalDoc_FileUpload.FileName;
                string candidateDocumentPath = string.Empty;
                string mainDocumentPath = Server.MapPath(AdditionalDocpath);

                if (!Directory.Exists(mainDocumentPath))
                    Directory.CreateDirectory(mainDocumentPath);

                candidateDocumentPath = mainDocumentPath + "\\" + CandidateDashboard_candidateIDHiddenField.Value;

                if (!Directory.Exists(candidateDocumentPath))
                    Directory.CreateDirectory(candidateDocumentPath);

                string userFileName = CandidateDashboard_candidateIDHiddenField.Value.Trim() + "_" + CandidateDashboard_additionalDoc_FileUpload.FileName;
                string saveAsPath = candidateDocumentPath + "\\" + userFileName;

                HttpPostedFile myFile = CandidateDashboard_additionalDoc_FileUpload.PostedFile;
                
                // Get size of uploaded file
                int nFileLen = myFile.ContentLength;
                byte[] myData = null;
                // make sure the size of the file is > 0
				if( nFileLen > 0 )
				{
					// Allocate a buffer for reading of the file
					myData = new byte[nFileLen];

					// Read uploaded file from the Stream
					myFile.InputStream.Read(myData, 0, nFileLen);
                } 

                Session["ADDITIONAL_FILE_SOURCE"] = myData;
                Session["ADDITIONAL_FILE_NAME"] = saveAsPath;

                /* //MemoryStream memStream = new MemoryStream(CandidateDashboard_additionalDoc_FileUpload.FileBytes);
                 * if (File.Exists(saveAsPath) != true)
                     CandidateDashboard_additionalDoc_FileUpload.SaveAs(saveAsPath);

                 AdditionalDocuments additionalDoc = new AdditionalDocuments();
                 additionalDoc.CandidateID = Convert.ToInt32(CandidateDashboard_candidateIDHiddenField.Value);
                 additionalDoc.FileName = userFileName;
                 additionalDoc.FilePath = candidateDocumentPath;
                 additionalDoc.FileExtension = Path.GetExtension(e.FileName).ToLower();
                 additionalDoc.UploadedDate = DateTime.Now.Date;
                 additionalDoc.CreatedBy = base.userID;
                 additionalDoc.CreatedDate = DateTime.Now.Date;

                 new CandidateBLManager().InsertAdditionalDocuments(additionalDoc);*/

                /*CandidateDashboard_additionalDocSave_Button_Click(new object(), new EventArgs());

                CandidateDashboard_additionalDoc_FileUpload.ClearFileFromPersistedStore();
                CandidateDashboard_additionalDoc_UpdatePanel.Update();*/
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(CandidateDashboard_errorLabel, ex.Message);
            }
        }

        protected void CandidateDashboard_additionalDocDataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                DataListItem dataListItem = e.Item;

                AdditionalDocuments additionaldocument = null;

                additionaldocument = (AdditionalDocuments)dataListItem.DataItem;

                Label CandidateDashboard_additionalDocNameLabel = (Label)e.Item.FindControl("CandidateDashboard_additionalDocNameLabel");
                if (CandidateDashboard_additionalDocNameLabel != null)
                    CandidateDashboard_additionalDocNameLabel.Text = additionaldocument.FileName;

                Label CandidateDashboard_additionalDocUploadedByLabel = (Label)e.Item.FindControl("CandidateDashboard_additionalDocUploadedByLabel");
                if (CandidateDashboard_additionalDocUploadedByLabel != null)
                    CandidateDashboard_additionalDocUploadedByLabel.Text = additionaldocument.UploadedBy;

                Label CandidateDashboard_additionalDocProcessDateLabel = (Label)e.Item.FindControl("CandidateDashboard_additionalDocProcessDateLabel");
                if (CandidateDashboard_additionalDocProcessDateLabel != null)
                    CandidateDashboard_additionalDocProcessDateLabel.Text = "Uploaded on " + additionaldocument.UploadedDate.ToString("MM-dd-yyyy");

                ImageButton CandidateDashboard_additionalDocDownloadImageButton = (ImageButton)e.Item.FindControl("CandidateDashboard_additionalDocDownloadImageButton");
                string args = "~/CandidateDocuments/" + CandidateDashboard_candidateIDHiddenField.Value + "/" + additionaldocument.FileName;
                CandidateDashboard_additionalDocDownloadImageButton.CommandArgument = args;
                CandidateDashboard_additionalDocDownloadImageButton.OnClientClick = "javascript:return DownloadDocument('" + args + "');";
                 
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(CandidateDashboard_errorLabel, ex.Message);
            }
        } 

        /// <summary>
        /// Handler method that will be called when the ok button is clicked in
        /// the delete resume confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will delete the resume for the selected candidate
        /// </remarks>
        protected void AdditionalDoc_deleteConfirmMsgControl_okButtonClick(object sender, EventArgs e)
        {
            try
            {
                // Check if candidate resume ID available.
                if (Utility.IsNullOrEmpty(ViewState["CANDIADTE_DOC_ID"]))
                {
                    base.ShowMessage(CandidateDashboard_errorLabel, "No document found to delete");
                    return;
                }

                // Delete the document.
                new CandidateBLManager().DeleteAdditionalDocument(Convert.ToInt32(ViewState["CANDIADTE_DOC_ID"]));
                CandidateDashboard_additionalDocSave_Button_Click(new object(), new EventArgs());

                // Reset the value.
                ViewState["CANDIADTE_DOC_ID"] = null;
                // Show a success message.
                base.ShowMessage(CandidateDashboard_SuccessMessageLabel, "Candidate document deleted successfully"); 
                 
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CandidateDashboard_errorLabel, exception.Message);
            }
        }


        protected void CandidateDashboard_additionalDocDataList_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteDoc")
                {
                    if (Utility.IsNullOrEmpty(CandidateDashboard_candidateIDHiddenField.Value))
                    {
                        base.ShowMessage(CandidateDashboard_errorLabel, "Select candidate");
                        return;
                    }

                    int resumeID = 0;

                    if (e.CommandArgument != null)
                    {
                        resumeID = Convert.ToInt32(e.CommandArgument);
                    }

                    if (resumeID == 0) return;

                    ViewState["CANDIADTE_DOC_ID"] = resumeID;

                    // Show the delete confirmation message.
                    // Set message, title and type for the confirmation control for edit entry.
                    AdditionalDoc_deleteConfirmMsgControl.Message =
                        "Are you sure to delete the document ?";
                    AdditionalDoc_deleteConfirmMsgControl.Title =
                        "Delete Document";
                    AdditionalDoc_deleteConfirmMsgControl.Type =
                        MessageBoxType.YesNo;

                    AdditionalDoc_deleteModalPopupExtender.Show();
                     
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(CandidateDashboard_errorLabel, exception.Message);
            }
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Utility.IsNullOrEmpty(CandidateDashboard_additionalDocrestoreHiddenField.Value) &&
                         CandidateDashboard_additionalDocrestoreHiddenField.Value == "N")
            {
                td_CandidateAdditionalDoc.Style["display"] = "block";  
                CandidateDashboard_additionalDocUpSpan.Style["display"] = "block";
                CandidateDashboard_additionalDocDownSpan.Style["display"] = "none"; 
                //CandidateDashboard_additionalDocrestoreHiddenField.Value = "N";
                 
            }
            /*else
            {
                td_CandidateAdditionalDoc.Style["display"] = "none";
                CandidateDashboard_additionalDocrestoreHiddenField.Value = "Y"; 
                CandidateDashboard_additionalDocUpSpan.Style["display"] = "none";
                CandidateDashboard_additionalDocDownSpan.Style["display"] = "block"; 
            } */
        }
    }
}