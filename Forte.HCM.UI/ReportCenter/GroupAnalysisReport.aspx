﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/DefaultMaster.Master"
    CodeBehind="GroupAnalysisReport.aspx.cs" Inherits="Forte.HCM.UI.ReportCenter.GroupAnalysisReport" %>
<%@ Register Src="~/CommonControls/WidgetTypesListControl.ascx" TagName="WidgetTypesListControl"
    TagPrefix="uc1" %>
<%@ Register Assembly="Kalitte.Dashboard.Framework" Namespace="Kalitte.Dashboard.Framework"
    TagPrefix="kalitte" %>
<%@ MasterType VirtualPath="~/DefaultMaster.Master" %>    
<asp:Content ID="GroupAnalysisReport_content" runat="server" ContentPlaceHolderID="ctlContentPh">
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
    <asp:HiddenField ID="GroupAnalysisReport_fileNameHiddenField" runat="server" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div style="float: left; width: 24%; height: 100%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td colspan="3" class="msg_align">
                                <asp:UpdatePanel runat="server" ID="GroupAnalysisReport_topMessageUpdatePanel">
                                    <ContentTemplate>
                                        <asp:Label ID="GroupAnalysisReport_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                        <asp:Label ID="GroupAnalysisReport_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 49%" valign="top">
                                <asp:UpdatePanel ID="GroupAnalysisReport_widgetTypesUpdatePanel" runat="server">
                                    <ContentTemplate>
                                        <uc1:WidgetTypesListControl ID="GroupAnalysisReport_widgetTypesListControl" runat="server"
                                            OnOkClick="GroupAnalysisReport_addWidgetInstanceClick" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="width: 2%">
                                &nbsp;
                            </td>
                            <td style="width: 49%;" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="dashboard_surface_header_bg">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="60%" class="header_text_bold">
                                                        <asp:Literal ID="GroupAnalysisReport_headerLiteral" runat="server" Text="Group Analysis Report"></asp:Literal>
                                                    </td>
                                                    <td width="40%" align="right">
                                                     <asp:UpdatePanel ID="GroupAnalysisReport_secondLevelMenuUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Button ID="GroupAnalysisReport_downloadButton" runat="server"
                                                                SkinID="sknButtonId" Text="Download" Width="75px" OnClick="GroupAnalysisReport_downloadButton_Click" />&nbsp;
                                                            <asp:Button ID="GroupAnalysisReport_emailButton"
                                                                runat="server" SkinID="sknButtonId" Text="Email" Width="60px" OnClick="GroupAnalysisReport_emailButton_Click" />
                                                                &nbsp;
                                                          <asp:LinkButton ID="GroupAnalysisReport_printerFriendlyLinkButton" runat="server" Text="Printer Friendly" SkinID="sknActionLinkButton"
                                                            OnClick="GroupAnalysisReport_printerFriendlyLinkButton_Click" /> |
                                                                  <asp:LinkButton ID="GroupAnalysisReport_topResetLinkButton" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                                             OnClick="GroupAnalysisReport_resetLinkButton_Click" /> |
                                                         <asp:LinkButton ID="GroupAnalysisReport_topCancelLinkButton" runat="server" Text="Cancel"
                                                                    SkinID="sknActionLinkButton" OnClick="GroupAnalysisReport_cancelLinkButton_Click" />
                                                        </ContentTemplate>
                                                        <Triggers>
                                                        <asp:PostBackTrigger ControlID="GroupAnalysisReport_downloadButton"/>
                                                        <asp:PostBackTrigger ControlID="GroupAnalysisReport_topResetLinkButton"/>
                                                        <asp:PostBackTrigger ControlID="GroupAnalysisReport_topCancelLinkButton"/>
                                                        </Triggers>
                                                     </asp:UpdatePanel>                                                                    
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <kalitte:DashboardSurface ID="Surface1" runat="server">
                                                <PanelSettings Icon="None" Border="False" BodyBorder="False" Collapsible="False"
                                                    HideCollapseTool="False" AutoWidth="False" AutoHeight="False" Enabled="True"
                                                    Frame="False" Header="False" Padding="0" Shim="False" TitleCollapse="False" Unstyled="False"
                                                    HeaderDisplayMode="Always" Dragable="False" AutoScroll="False" FitLayout="False"
                                                    Stretch="False" Hidden="False" AutoHideTools="False"></PanelSettings>
                                                <WindowSettings AutoShow="False" Modal="False" Resizable="True" Maximizable="True"
                                                    Minimizable="False" Closable="True" Icon="None" Width="500" Height="500" Border="True"
                                                    BodyBorder="True" Collapsible="False" HideCollapseTool="False" AutoWidth="False"
                                                    AutoHeight="False" Enabled="True" Frame="False" Header="True" Padding="0" Shim="False"
                                                    TitleCollapse="False" Unstyled="False" HeaderDisplayMode="Always" Dragable="True"
                                                    AutoScroll="True" FitLayout="False" Stretch="False" Hidden="False" AutoHideTools="False">
                                                </WindowSettings>
                                            </kalitte:DashboardSurface>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="msg_align">
                                <asp:UpdatePanel runat="server" ID="GroupAnalysisReport_bottomMessageUpdatePanel">
                                    <ContentTemplate>
                                        <asp:Label ID="GroupAnalysisReport_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                        <asp:Label ID="GroupAnalysisReport_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                                            ID="TestReport_authorIdHiddenField" runat="server" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
