﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/DefaultMaster.Master" CodeBehind="CandidateComparisonReport.aspx.cs" Inherits="Forte.HCM.UI.ReportCenter.CandidateComparisonReport" %>

<%@ Register Src="~/CommonControls/WidgetTypesListControl.ascx" TagName="WidgetTypesListControl"
    TagPrefix="uc1" %>
<%@ Register Assembly="Kalitte.Dashboard.Framework" Namespace="Kalitte.Dashboard.Framework"
    TagPrefix="kalitte" %>
<%@ MasterType VirtualPath="~/DefaultMaster.Master" %>
<asp:Content ID="CandidateComparisonReport_content" runat="server" ContentPlaceHolderID="ctlContentPh">
<asp:HiddenField ID="CandidateComparisonReport_fileNameHiddenField" runat="server" />
 <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td> 
                <div style="float: left; width: 24%; height: 100%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                        <td colspan="3" class="msg_align">
                            <asp:UpdatePanel runat="server" ID="ComparisonReport_topMessageUpdatePanel">
                                <ContentTemplate>
                                    <asp:Label ID="CandidateComparisonReport_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="CandidateComparisonReport_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>    
                     </td>
                    </tr>
                        <tr>
                            <td style="width: 49%" valign="top">   
                            <asp:UpdatePanel ID="CandidateComparisonReport_widgetTypesUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <uc1:WidgetTypesListControl ID="CandidateComparisonReport_widgetTypesListControl"  runat="server" OnOkClick="CandidateComparisonReport_addWidgetInstanceClick"/>
                                 </ContentTemplate>
                           </asp:UpdatePanel>
                            </td>
                            <td style="width: 2%">
                                &nbsp;
                            </td>
                            <td style="width: 49%;" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="dashboard_surface_header_bg">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="55%" class="header_text_bold">
                                                        <asp:Literal ID="CandidateComparisonReport_headerLiteral" runat="server" Text="Candidate Report"></asp:Literal>
                                                    </td>
                                                    <td width="45%" align="right">
                                                    <asp:UpdatePanel runat="server" ID="DesignReport_downloadButton_secondLevelUpdatePanel">
                                                         <ContentTemplate>
                                                             <asp:Button ID="CandidateComparisonReport_downloadButton" runat="server" SkinID="sknButtonId" 
                                                                Text="Download" Width="75px" OnClick="CandidateComparisonReport_downloadButton_Click" />&nbsp;
                                                             <asp:Button ID="CandidateComparisonReport_emailButton" runat="server" SkinID="sknButtonId" 
                                                                Text="Email"  Width="65px" OnClick="CandidateComparisonReport_emailClick" />&nbsp;
                                                                <asp:LinkButton ID="CandidateComparisonReport_printerFriendlyLinkButton" runat="server" Text="Printer Friendly" SkinID="sknActionLinkButton"
                                                                    OnClick="CandidateComparisonReport_printerFriendlyLinkButton_Click" /> |
                                                             <asp:LinkButton ID="CandidateComparisonReport_topResetLinkButton" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                                                OnClick="CandidateComparisonReport_resetLinkButton_Click" /> |
                                                             <asp:LinkButton ID="CandidateComparisonReport_topCancelLinkButton" runat="server" Text="Cancel"
                                                                        SkinID="sknActionLinkButton" OnClick="CandidateComparisonReport_cancelLinkButton_Click" />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                    <asp:PostBackTrigger ControlID="CandidateComparisonReport_downloadButton" />
                                                    <asp:PostBackTrigger ControlID="CandidateComparisonReport_topResetLinkButton" />
                                                    <asp:PostBackTrigger ControlID="CandidateComparisonReport_topCancelLinkButton" />
                                                    </Triggers>
                                                    </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <kalitte:DashboardSurface ID="Surface1" runat="server">
                                                <PanelSettings Icon="None" Border="False" BodyBorder="False" Collapsible="False" 
                                                HideCollapseTool="False" AutoWidth="False" AutoHeight="False" Enabled="True" Frame="False" 
                                                Header="False" Padding="0" Shim="False" TitleCollapse="False" Unstyled="False" 
                                                HeaderDisplayMode="Always" Dragable="False" AutoScroll="False" FitLayout="False" 
                                                Stretch="False" Hidden="False" AutoHideTools="False"></PanelSettings>

                                                <WindowSettings AutoShow="False" Modal="False" Resizable="True" Maximizable="True" Minimizable="False" 
                                                Closable="True" Icon="None" Width="500" Height="500" Border="True" BodyBorder="True" Collapsible="False" 
                                                HideCollapseTool="False" AutoWidth="False" AutoHeight="False" Enabled="True" Frame="False" Header="True" 
                                                Padding="0" Shim="False" TitleCollapse="False" Unstyled="False" HeaderDisplayMode="Always" Dragable="True" 
                                                AutoScroll="True" FitLayout="False" Stretch="False" Hidden="False" AutoHideTools="False">
                                                </WindowSettings>
                                            </kalitte:DashboardSurface>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                           <td colspan="3" class="msg_align">
                            <asp:UpdatePanel runat="server" ID="CandidateComparisonReport_bottomMessageUpdatePanel">
                            <ContentTemplate>
                                <asp:Label ID="CandidateComparisonReport_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                <asp:Label ID="CandidateComparisonReport_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField ID="TestReport_authorIdHiddenField" runat="server" />
                                
                            </ContentTemplate>
                         </asp:UpdatePanel>  
                          </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
