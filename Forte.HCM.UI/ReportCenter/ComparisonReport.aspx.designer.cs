﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.ReportCenter {
    
    
    public partial class ComparisonReport {
        
        /// <summary>
        /// ComparisonReport_fileNameHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ComparisonReport_fileNameHiddenField;
        
        /// <summary>
        /// ComparisonReport_topMessageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ComparisonReport_topMessageUpdatePanel;
        
        /// <summary>
        /// ComparisonReport_topSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ComparisonReport_topSuccessMessageLabel;
        
        /// <summary>
        /// ComparisonReport_topErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ComparisonReport_topErrorMessageLabel;
        
        /// <summary>
        /// ComparisonReport_widgetTypesUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ComparisonReport_widgetTypesUpdatePanel;
        
        /// <summary>
        /// ComparisonReport_widgetTypesListControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.WidgetTypesListControl ComparisonReport_widgetTypesListControl;
        
        /// <summary>
        /// ComparisonReport_headerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ComparisonReport_headerLiteral;
        
        /// <summary>
        /// UpdatePanel1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel UpdatePanel1;
        
        /// <summary>
        /// ComparisonReport_downloadButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ComparisonReport_downloadButton;
        
        /// <summary>
        /// ComparisonReport_emailButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ComparisonReport_emailButton;
        
        /// <summary>
        /// ComparisonReport_printerFriendlyLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ComparisonReport_printerFriendlyLinkButton;
        
        /// <summary>
        /// ComparisonReport_topResetLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ComparisonReport_topResetLinkButton;
        
        /// <summary>
        /// ComparisonReport_topCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ComparisonReport_topCancelLinkButton;
        
        /// <summary>
        /// Surface1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Kalitte.Dashboard.Framework.DashboardSurface Surface1;
        
        /// <summary>
        /// ComparisonReport_bottomMessageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ComparisonReport_bottomMessageUpdatePanel;
        
        /// <summary>
        /// ComparisonReport_bottomSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ComparisonReport_bottomSuccessMessageLabel;
        
        /// <summary>
        /// ComparisonReport_bottomErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ComparisonReport_bottomErrorMessageLabel;
        
        /// <summary>
        /// TestReport_authorIdHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField TestReport_authorIdHiddenField;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.DefaultMaster Master {
            get {
                return ((Forte.HCM.UI.DefaultMaster)(base.Master));
            }
        }
    }
}
