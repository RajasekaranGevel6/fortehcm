﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestReport.cs
// File that represents the user Serach the TestReport by various Key filed.
// This will helps Search a Test From the Test repository.
//

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.ReportCenter
{
    public partial class TestReport : PageBase
    {
        #region Declaration                                                    

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Declaration                                                 

        #region Event Handler                                                  

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button.
                Page.Form.DefaultButton = TestReport_topSearchButton.UniqueID;

                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    // Set default focus field.
                    Page.Form.DefaultFocus = TestReport_categoryTextBox.UniqueID;
                    TestReport_categoryTextBox.Focus();

                    LoadValues();
                    TestReport_testDiv.Visible = false;

                    // Check if the page is launched from menu. If so, clear the existing
                    // session variables.
                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU
                         || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_LANDING
                         || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_ASSESSMENT
                         || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_MAJOR
                         ))
                    {
                        base.ClearSearchCriteriaSession();
                        Session["CATEGORY_SUBJECTS"] = null;
                    }

                    // Check if page is redirected from any child page. If the page
                    // if redirected from any child page, fill the search criteria
                    // and apply the search.
                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_REPORT] != null)
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_REPORT]
                            as TestSearchCriteria);
                    CheckAndSetExpandorRestore();
                }
                else
                {
                    TestReport_testDiv.Visible = true;
                }

                TestReport_topSuccessMessageLabel.Text = string.Empty;
                TestReport_bottomSuccessMessageLabel.Text = string.Empty;
                TestReport_topErrorMessageLabel.Text = string.Empty;
                TestReport_bottomErrorMessageLabel.Text = string.Empty;

                TestReport_searchCategorySubjectControl.ControlMessageThrown += 
                    new CategorySubjectControl.ControlMessageThrownDelegate
                        (TestReport_searchCategorySubjectControl_ControlMessageThrown);

                TestReport_bottomPagingNavigator.PageNumberClick += new
                   PageNavigator.PageNumberClickEventHandler
                      (TestReport_pagingNavigator_PageNumberClick);

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestReport_topErrorMessageLabel,
                TestReport_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// It shows error/success messages whenever the user calls this method
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        void TestReport_searchCategorySubjectControl_ControlMessageThrown
            (object sender, ControlMessageEventArgs c)
        {
            try
            {
                // Show message on the label.
                if (c.MessageType == MessageType.Error)
                {
                    base.ShowMessage(TestReport_topErrorMessageLabel,
                        TestReport_bottomErrorMessageLabel, c.Message);
                }
                else if (c.MessageType == MessageType.Success)
                {
                    base.ShowMessage(TestReport_topSuccessMessageLabel,
                        TestReport_bottomSuccessMessageLabel, c.Message);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestReport_topErrorMessageLabel,
                TestReport_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void TestReport_testGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                TestReport_bottomPagingNavigator.Reset();
                LoadTests(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestReport_topErrorMessageLabel,
                TestReport_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void TestReport_topSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = "CREATEDDATE";

                // Reset the paging control.
                TestReport_bottomPagingNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                LoadTests(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestReport_topErrorMessageLabel,
                TestReport_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void TestReport_pagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ViewState["PAGENUMBER"] = e.PageNumber;
                LoadTests(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestReport_topErrorMessageLabel,
                TestReport_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void TestReport_testGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                
                // Set row styles.
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestReport_topErrorMessageLabel,
                TestReport_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call whenever the link button is clicked on grid view.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that contains the event data.
        /// </param>
        protected void TestReport_testGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "TestStatisticsInfo")
                    return;

                Response.Redirect("~/ReportCenter/TestStatisticsInfo.aspx" +
                        "?m=3&s=0&testkey=" + ((LinkButton)e.CommandSource).Text +
                        "&parentpage=" + Constants.ParentPage.TEST_REPORT, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestReport_topErrorMessageLabel,
                TestReport_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void TestReport_testGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (TestReport_testGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestReport_topErrorMessageLabel,
                TestReport_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Interchange Advanced and Simple Search Link 
        /// and Search Criteria fields 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ControlMessageEventArgs"/>that holds the event data.
        /// </param>
        protected void TestReport_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (TestReport_simpleLinkButton.Text.ToLower() == "simple")
                {
                    TestReport_simpleLinkButton.Text = "Advanced";
                    TestReport_simpleSearchDiv.Visible = true;
                    TestReport_advanceSearchDiv.Visible = false;
                }
                else
                {
                    TestReport_simpleLinkButton.Text = "Simple";
                    TestReport_simpleSearchDiv.Visible = false;
                    TestReport_advanceSearchDiv.Visible = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestReport_topErrorMessageLabel,
                TestReport_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void TestReport_resetLinkButton_Click(object sender, EventArgs e)
        {
            base.ClearSearchCriteriaSession();
            Session["CATEGORY_SUBJECTS"] = null;
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Handler that will call when the cancel link button is clicked.
        /// During that time, it will clear all the search criteria session,
        /// category subject session values.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void TestReport_cancelLinkButton_Click(object sender, EventArgs e)
        {
            base.ClearSearchCriteriaSession();
            Session["CATEGORY_SUBJECTS"] = null;
            Response.Redirect("~/OTMHome.aspx", false);
        }

        #endregion Event Handler                                               

        #region Private methods                                                

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>                                                              
        private void LoadTests(int pageNumber)
        {
            int totalRecords = 0;
            StringBuilder testAreaID = GetTestAreaID();
            StringBuilder complexityID = GetSelectedComplexityID();
            TestSearchCriteria testSearchCriteria = new TestSearchCriteria();
            testSearchCriteria.IsCertification = null;
            List<Subject> categorySubjects = null;

            if (TestReport_simpleLinkButton.Text == "Advanced")
            {
                testSearchCriteria.Category = TestReport_categoryTextBox.Text.Trim() == "" ?
                                    null : TestReport_categoryTextBox.Text.Trim();
                testSearchCriteria.Subject = TestReport_subjectTextBox.Text.Trim() == "" ?
                                    null : TestReport_subjectTextBox.Text.Trim();
                testSearchCriteria.Keyword = TestReport_simpleKeywordTextBox.Text.Trim() == "" ?
                                    null : TestReport_simpleKeywordTextBox.Text.Trim();

                testSearchCriteria.SearchType = SearchType.Simple;
                testSearchCriteria.CurrentPage = pageNumber;
                testSearchCriteria.IsMaximized = 
                    TestReport_restoreHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

                testSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                testSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();

            }
            else
            {
                string SelectedSubjectIDs = "";

                if (!Utility.IsNullOrEmpty(TestReport_searchCategorySubjectControl.SubjectDataSource))
                {
                    List<Category> categoryList = new List<Category>();
                    foreach (Subject subject in TestReport_searchCategorySubjectControl.SubjectDataSource)
                    {
                        if (categorySubjects == null)
                            categorySubjects = new List<Subject>();

                        Subject subjectCat = new Subject();

                        if (subject.IsSelected == true)
                        {
                            Category category = new Category();
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                            category.CategoryID = subject.CategoryID;
                            categoryList.Add(category);
                        }

                        // Get all the subjects and categories and then assign to the list.
                        subjectCat.CategoryID = subject.CategoryID;
                        subjectCat.CategoryName = subject.CategoryName;
                        subjectCat.SubjectID = subject.SubjectID;
                        subjectCat.SubjectName = subject.SubjectName;
                        subjectCat.IsSelected = subject.IsSelected;
                        categorySubjects.Add(subjectCat);
                    }

                    //Get all Subject ids in  Un selected Subject List.
                    Category categoryID = new Category();

                    foreach (Subject subject in TestReport_searchCategorySubjectControl.SubjectDataSource)
                    {
                        categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                        if (categoryID == null)
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                    }
                }

                // Keep the categories and subjects in Session
                Session["CATEGORY_SUBJECTS"] = categorySubjects;

                testSearchCriteria.CategoriesID = SelectedSubjectIDs == "" ?
                                null : SelectedSubjectIDs.TrimEnd(',');
                testSearchCriteria.TestAreasID = testAreaID.ToString() == "" ?
                                null : testAreaID.ToString().TrimEnd(',');
                testSearchCriteria.TestKey = TestReport_testIdTextBox.Text.Trim() == "" ?
                                null : TestReport_testIdTextBox.Text.Trim();
                testSearchCriteria.Name = TestReport_testNameTextBox.Text.Trim() == "" ?
                                null : TestReport_testNameTextBox.Text.Trim();
                testSearchCriteria.TestAuthorName = TestReport_authorTextBox.Text.Trim() == "" ?
                                null : TestReport_authorTextBox.Text.Trim();
                if (TestReport_positionProfileIDHiddenField.Value == null ||
                   TestReport_positionProfileIDHiddenField.Value.Trim().Length == 0)
                {
                    testSearchCriteria.PositionProfileID = 0;
                }
                else
                {
                    testSearchCriteria.PositionProfileID = Convert.ToInt32(TestReport_positionProfileIDHiddenField.Value);
                }
                testSearchCriteria.PositionProfileName = TestReport_positionProfileTextBox.Text;

                testSearchCriteria.Keyword = TestReport_advancedKeywordTextBox.Text.Trim() == "" ?
                                null : TestReport_advancedKeywordTextBox.Text.Trim();
                testSearchCriteria.Complexity = complexityID.ToString() == "" ?
                                null : complexityID.ToString().TrimEnd(',');

                testSearchCriteria.CurrentPage = pageNumber;
                testSearchCriteria.SearchType = SearchType.Advanced;
                testSearchCriteria.IsMaximized = TestReport_restoreHiddenField.Value.Trim() == "Y" ? true : false;

                testSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                testSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();
            }

            // Keep the search criteria in Session if the page is launched from 
            // somewhere else.
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_REPORT] = testSearchCriteria;

            testSearchCriteria.TestAuthorID = base.userID;

            List<TestDetail> testDetail = new ReportBLManager().GetTests(testSearchCriteria, base.GridPageSize,
                pageNumber, testSearchCriteria.SortExpression, testSearchCriteria.SortDirection, out totalRecords);

            if (testDetail == null || testDetail.Count == 0)
            {
                base.ShowMessage(TestReport_topErrorMessageLabel,
                TestReport_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                TestReport_testGridView.DataSource = null;
                TestReport_testGridView.DataBind();
                TestReport_bottomPagingNavigator.TotalRecords = 0;
                TestReport_testDiv.Visible = false;
            }
            else
            {
                TestReport_testGridView.DataSource = testDetail;
                TestReport_testGridView.DataBind();
                TestReport_bottomPagingNavigator.PageSize = base.GridPageSize;
                TestReport_bottomPagingNavigator.TotalRecords = totalRecords;
                TestReport_testDiv.Visible = true;
            }
        }

        /// <summary>
        /// Bind complexities/attributes based on the attribute type
        /// </summary>
        private void BindComplexities()
        {
            TestReport_complexityCheckBoxList.DataSource =
                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, Constants.SortTypeConstants.ASCENDING);
            TestReport_complexityCheckBoxList.DataTextField = "AttributeName";
            TestReport_complexityCheckBoxList.DataValueField = "AttributeID";
            TestReport_complexityCheckBoxList.DataBind();
        }

        /// <summary>
        /// Get Selected Complexity IDs and its used in the Search.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetSelectedComplexityID()
        {
            StringBuilder complexityID = new StringBuilder();
            for (int i = 0; i < TestReport_complexityCheckBoxList.Items.Count; i++)
            {
                if (TestReport_complexityCheckBoxList.Items[i].Selected)
                {

                    complexityID.Append(TestReport_complexityCheckBoxList.Items[i].Value.ToString() + ",");
                }
            }
            return complexityID;
        }

        /// <summary>
        /// Bind test area based on TEST_AREA attribute type
        /// </summary>
        private void BindTestArea()
        {
            TestReport_testAreaCheckBoxList.DataSource =
               new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, Constants.SortTypeConstants.ASCENDING);
            TestReport_testAreaCheckBoxList.DataTextField = "AttributeName";
            TestReport_testAreaCheckBoxList.DataValueField = "AttributeID";
            TestReport_testAreaCheckBoxList.DataBind();
        }

        /// <summary>
        /// Get Selected Test Area IDs and its used in the Search.
        /// </summary>
        /// <returns></returns>
        private StringBuilder GetTestAreaID()
        {
            StringBuilder testAreaID = new StringBuilder();
            for (int i = 0; i < TestReport_testAreaCheckBoxList.Items.Count; i++)
            {
                if (TestReport_testAreaCheckBoxList.Items[i].Selected)
                {
                    testAreaID.Append(TestReport_testAreaCheckBoxList.Items[i].Value.ToString() + ",");
                }
            }
            return testAreaID;
        }

        /// <summary>
        /// Method that will fill the search criteria to the appropriate fields.
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A <see cref="TestSearchCriteria"/> that contains the test search
        /// criteria fields.
        /// </param>
        public void FillSearchCriteria(TestSearchCriteria testSearchCriteria)
        {
            string[] testAreaIDs = null;
            string[] complexities = null;

            // Check if the simple link button is clicked.
            if (testSearchCriteria.SearchType == SearchType.Simple)
            {
                TestReport_simpleLinkButton.Text = "Advanced";
                TestReport_simpleSearchDiv.Visible = true;
                TestReport_advanceSearchDiv.Visible = false;

                if (testSearchCriteria.Category != null)
                    TestReport_categoryTextBox.Text = testSearchCriteria.Category;

                if (testSearchCriteria.Subject != null)
                    TestReport_subjectTextBox.Text = testSearchCriteria.Subject;

                if (testSearchCriteria.Keyword != null)
                    TestReport_simpleKeywordTextBox.Text = testSearchCriteria.Keyword;

                TestReport_restoreHiddenField.Value = testSearchCriteria.IsMaximized == true ? "Y" : "N";
                ViewState["SORT_FIELD"] = testSearchCriteria.SortExpression;
                ViewState["SORT_ORDER"] = testSearchCriteria.SortDirection;

                // Apply search
                LoadTests(testSearchCriteria.CurrentPage);

                // Highlight the last page number which is stored in session
                // when the page is launched from somewhere else.
                TestReport_bottomPagingNavigator.MoveToPage(testSearchCriteria.CurrentPage);
            }
            else
            {
                TestReport_simpleLinkButton.Text = "Simple";
                TestReport_simpleSearchDiv.Visible = false;
                TestReport_advanceSearchDiv.Visible = true;

                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria.TestAreasID))
                {
                    testAreaIDs = testSearchCriteria.TestAreasID.Split(new char[] { ',' });

                    // Make selection test area.
                    for (int idx = 0; idx < testAreaIDs.Length; idx++)
                        TestReport_testAreaCheckBoxList.Items.FindByValue(testAreaIDs[idx]).Selected = true;
                }

                if (!Utility.IsNullOrEmpty(testSearchCriteria.Complexity))
                {
                    complexities = testSearchCriteria.Complexity.Split(new char[] { ',' });

                    // Make selection of complexities.
                    for (int idx = 0; idx < complexities.Length; idx++)
                        TestReport_complexityCheckBoxList.Items.FindByValue(complexities[idx]).Selected = true;
                }

                if (testSearchCriteria.TestKey != null)
                    TestReport_testIdTextBox.Text = testSearchCriteria.TestKey.Trim();

                if (testSearchCriteria.Name != null)
                    TestReport_testNameTextBox.Text = testSearchCriteria.Name.Trim();

                if (testSearchCriteria.TestAuthorName != null)
                    TestReport_authorTextBox.Text = testSearchCriteria.TestAuthorName.Trim();

                if (!Utility.IsNullOrEmpty(testSearchCriteria.PositionProfileName))
                    TestReport_positionProfileTextBox.Text = testSearchCriteria.PositionProfileName;

                TestReport_positionProfileIDHiddenField.Value = testSearchCriteria.PositionProfileID.ToString();

                if (testSearchCriteria.Keyword != null)
                    TestReport_advancedKeywordTextBox.Text = testSearchCriteria.Keyword.Trim();

                TestReport_restoreHiddenField.Value = testSearchCriteria.IsMaximized ? "Y" : "N";

                if (Session["CATEGORY_SUBJECTS"] != null)
                {
                    QuestionDetail questionDetail = new QuestionDetail();
                    questionDetail.Subjects = Session["CATEGORY_SUBJECTS"] as List<Subject>;

                    // Fetch unique categories from the list.
                    var distinctCategories = (Session["CATEGORY_SUBJECTS"] as List<Subject>).GroupBy
                        (x => x.CategoryID).Select(x => x.First());

                    // This datasource is used to make the selection of the subjects
                    // which are already selected by the user to search.
                    TestReport_searchCategorySubjectControl.SubjectsToBeSelected = 
                        Session["CATEGORY_SUBJECTS"] as List<Subject>;

                    // Bind distinct categories in the category datalist
                    TestReport_searchCategorySubjectControl.CategoryDataSource =
                        distinctCategories.ToList<Subject>();

                    // Bind subjects for the categories which are bind previously.
                    TestReport_searchCategorySubjectControl.SubjectDataSource =
                        distinctCategories.ToList<Subject>();
                }

                ViewState["SORT_FIELD"] = testSearchCriteria.SortExpression;
                ViewState["SORT_ORDER"] = testSearchCriteria.SortDirection;

                // Apply search
                LoadTests(testSearchCriteria.CurrentPage);

                // Highlight the last page number which is stored in session
                // when the page is launched from somewhere else.
                TestReport_bottomPagingNavigator.MoveToPage(testSearchCriteria.CurrentPage);
            }
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TestReport_restoreHiddenField.Value) &&
                         TestReport_restoreHiddenField.Value == "Y")
            {
                TestReport_searchByTestDiv.Style["display"] = "none";
                TestReport_searchResultsUpSpan.Style["display"] = "block";
                TestReport_searchResultsDownSpan.Style["display"] = "none";
                TestReport_testDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                TestReport_searchByTestDiv.Style["display"] = "block";
                TestReport_searchResultsUpSpan.Style["display"] = "none";
                TestReport_searchResultsDownSpan.Style["display"] = "block";
                TestReport_testDiv.Style["height"] = RESTORED_HEIGHT;
            }
            if (!Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_REPORT]))
                if (!Utility.IsNullOrEmpty(TestReport_restoreHiddenField.Value))
                    ((TestSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_REPORT]).IsMaximized =
                        TestReport_restoreHiddenField.Value == "Y" ? true : false;
        }

        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            Master.SetPageCaption(Resources.HCMResource.TestReport_Title);
            BindTestArea();
            BindComplexities();

            TestReport_positionProfileImageButton.Attributes.Add("onclick", "return LoadPositionProfileName('"
                + TestReport_positionProfileTextBox.ClientID
                + "','" + TestReport_positionProfileIDHiddenField.ClientID + "')");

            TestReport_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                + TestReport_dummyAuthorID.ClientID + "','"
                + TestReport_authorIdHiddenField.ClientID + "','"
                + TestReport_authorTextBox.ClientID + "','TR')");

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                ViewState["SORT_ORDER"] = SortType.Descending;

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                ViewState["SORT_FIELD"] = "CREATEDDATE";

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                ViewState["PAGENUMBER"] = "1";

            TestReport_searchTestResultsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                TestReport_testDiv.ClientID + "','" +
                TestReport_searchByTestDiv.ClientID + "','" +
                TestReport_searchResultsUpSpan.ClientID + "','" +
                TestReport_searchResultsDownSpan.ClientID + "','" +
                TestReport_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
        }

        #endregion Protected Overridden Methods                                
    }
}