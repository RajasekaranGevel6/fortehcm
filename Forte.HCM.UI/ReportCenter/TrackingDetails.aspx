<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrackingDetails.aspx.cs"
    MasterPageFile="~/MasterPages/OTMMaster.Master" Inherits="Forte.HCM.UI.ReportCenter.TrackingDetails" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="TrackingDetails_bodyContent" ContentPlaceHolderID="OTMMaster_body"
    runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="TrackingDetails_headerLiteral" runat="server" Text="Cyber Proctoring Information"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td class="td_padding_5">
                                        <asp:LinkButton ID="TrackingDetails_topCancelButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="TrackingDetails_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="TrackingDetails_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <ajaxToolKit:TabContainer ID="TrackingDetails_mainTabContainer" runat="server" Width="100%"
                    ActiveTabIndex="2" CssClass="">
                    <ajaxToolKit:TabPanel ID="TrackingDetails_logsTabPanel" HeaderText="Application Logs"
                        runat="server" Visible="false" >
                        <HeaderTemplate>
                            Application Logs
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="TrackingDetails_applicationLogsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <div style="height: 300px; overflow: auto;" id="TrackingDetails_applicationLogsDiv"
                                                    runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="TrackingDetails_applicationLogsGridView" runat="server" AutoGenerateColumns="False"
                                                                    Width="100%" AllowSorting="False">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Message" HeaderText="Message" />
                                                                        <asp:BoundField DataField="Date" HeaderText="Date & Time" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div>
                                                    <uc1:PageNavigator ID="TrackingDetails_applicationLogsPageNavigator" runat="server" />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="TrackingDetails_operatingSystemEventsTabPanel" HeaderText="Operating System Events"
                        runat="server" Visible="false">
                        <HeaderTemplate>
                            Operating System Events
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="TrackingDetails_operatingSystemEventsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <div style="height: 300px; overflow: auto;" id="TrackingDetails_operatingSystemEventsDiv"
                                                    runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="TrackingDetails_operatingSystemEventsGridView" runat="server" AutoGenerateColumns="False"
                                                                    Width="100%" AllowSorting="False">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="ApplicationName" HeaderText="Application Name" />
                                                                        <asp:BoundField DataField="ApplicationPath" HeaderText="File Path" />
                                                                        <asp:BoundField DataField="EventStatus" HeaderText="Status" />
                                                                        <asp:BoundField DataField="Date" HeaderText="Date & Time" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div>
                                                    <uc1:PageNavigator ID="TrackingDetails_operatingSystemEventsPageNavigator" runat="server" />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="TrackingDetails_webcamCaptureTabPanel" runat="server" >
                        <HeaderTemplate>
                            Webcam Capture
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="TrackingDetails_webcamCaptureUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <div id="TrackingDetails_webcamCaptureDiv" runat="server">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td class="td_height_5" align="center">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 6%" align="center">
                                                    <asp:ImageButton ID="TrackingDetails_webcamCapturePreviousImageButton" runat="server" SkinID="sknViewPreviousImageButton"
                                                        ToolTip="Previous" OnClick="TrackingDetails_webcamCapturePreviousImageButton_Click" Style="width: 14px;
                                                        height: 16px" />
                                                </td>
                                                <td style="width: 88%" align="center">
                                                    <asp:DataList ID="TrackingDetails_webcamCaptureDataList" runat="server" RepeatColumns="5"
                                                        RepeatLayout="Table" GridLines="None" RepeatDirection="Horizontal" Width="100%"
                                                        OnItemDataBound="TrackingDetails_webcamCaptureDataList_ItemDataBound">
                                                        <ItemTemplate>
                                                            <div style="float: left; height: 140px; width: 95%;">
                                                                <table width="95%" cellpadding="0" cellspacing="5" border="0" align="center">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Image ID="TrackingDetails_webcamCaptureImage" runat="server" Height="120px" Style="cursor: pointer"
                                                                                Width="120px" ImageUrl='<%# "../Common/ImageHandler.ashx?source=CP&ImageID="+ Eval("ImageId") +" & isThumb=1"  %>' />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </td>
                                                <td style="width: 6%" align="center">
                                                    <asp:ImageButton ID="TrackingDetails_webcamCaptureNextImageButton" runat="server" SkinID="sknViewNextImageButton"
                                                        ToolTip="Next" OnClick="TrackingDetails_webcamCaptureNextImageButton_Click" Style="width: 14px;
                                                        height: 16px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="TrackingDetails_desktopCaptureTabPanel" runat="server">
                        <HeaderTemplate>
                            Desktop Capture
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="TrackingDetails_desktopCaptureUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <div id="TrackingDetails_desktopCaptureDiv" runat="server">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td class="td_height_5" colspan="3">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 6%" align="center">
                                                    <asp:ImageButton ID="TrackingDetails_desktopCapturePreviousImageButton" runat="server" SkinID="sknViewPreviousImageButton"
                                                        ToolTip="Previous" OnClick="TrackingDetails_desktopCapturePreviousImageButton_Click" Style="width: 14px;
                                                        height: 16px" />
                                                </td>
                                                <td style="width: 88%; height: 260px;" align="center" valign="top">
                                                    <asp:DataList ID="TrackingDetails_desktopCaptureDatalist" runat="server" RepeatColumns="5"
                                                        RepeatLayout="Table" GridLines="None" RepeatDirection="Horizontal" Width="100%"
                                                        OnItemDataBound="TrackingDetails_desktopCaptureDatalist_ItemDataBound">
                                                        <ItemTemplate>
                                                            <div style="float: left; height: 140px; width: 95%;">
                                                                <table width="95%" cellpadding="0" cellspacing="5" border="0" align="center">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Image ID="TrackingDetails_desktopCaptureImage" runat="server" Height="120px"
                                                                                Style="cursor: pointer" Width="120px" ImageUrl='<%# "../Common/ImageHandler.ashx?source=CP&ImageID="+ Eval("ImageId") + " & isThumb=1"  %>' />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </td>
                                                <td style="width: 6%" align="center">
                                                    <asp:ImageButton ID="TrackingDetails_desktopCaptureNextImageButton" runat="server" SkinID="sknViewNextImageButton"
                                                        ToolTip="Next" OnClick="TrackingDetails_desktopCaptureNextImageButton_Click" Style="width: 14px;
                                                        height: 16px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="TrackingDetails_exceptionsTabPanel" HeaderText="Exceptions" runat="server" Visible="false"> 
                        <HeaderTemplate>
                            Exceptions
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="TrackingDetails_exceptionsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <div style="height: 300px; overflow: auto;" id="TrackingDetails_exceptionsDiv"
                                                    runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="TrackingDetails_exceptionsGridView" runat="server" AutoGenerateColumns="False"
                                                                    Width="100%" AllowSorting="False">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Exception" HeaderText="Exception" />
                                                                        <asp:BoundField DataField="Date" HeaderText="Date and Time" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div>
                                                    <uc1:PageNavigator ID="TrackingDetails_exceptionsPageNavigator" runat="server" />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="TrackingDetails_runningApplicationsTabPanel" HeaderText="Search By Test" Visible="false"
                        runat="server">
                        <HeaderTemplate>
                            Running Applications
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table width="100%" cellpadding="0" cellspacing="0" runat="server">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="TrackingDetails_runningApplicationsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <div style="height: 300px; overflow: auto;" id="TrackingDetails_runningApplicationsDiv"
                                                    runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="TrackingDetails_runningApplicationsGridView" runat="server" AutoGenerateColumns="False"
                                                                    Width="100%" AllowSorting="False">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Application" HeaderText="Process Name" />
                                                                        <asp:BoundField DataField="ExePath" HeaderText="File Path" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div>
                                                    <uc1:PageNavigator ID="TrackingDetails_runningApplicationsPageNavigator" runat="server" />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                </ajaxToolKit:TabContainer>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="TrackingDetails_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="TrackingDetails_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="header_bg" align="right">
                <table width="100%" cellpadding="0" cellspacing="0" align="right" border="0">
                    <tr>
                        <td style="width: 50%;">
                        </td>
                        <td style="width: 50%;" align="right">
                            <asp:LinkButton ID="TrackingDetails_bottomCancelButton" runat="server" Text="Cancel"
                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
