﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateReport.cs
// File that represents the user Serach the candidate and Category, Subject  by various Key filed. 
// This will helps generate the Group analysis and Comaparison the Selected candidate and selected 
// Category subject reports..

#endregion

#region Directives                                                             
using System;
using System.Threading;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;

using Kalitte.Dashboard.Framework.Types;
using Kalitte.Dashboard.Framework;
#endregion
namespace Forte.HCM.UI.ReportCenter
{
    public partial class CandidateReport : PageBase
    {
        #region Declaration                                                    
        List<Subject> subjectList = null;
        List<Subject> subjectDraftList = null;
        List<UserDetail> userDetailList = null;
        List<UserDetail> userDraftDetailList = null;
        UserDetail userDetail = new UserDetail();
        Subject subject = new Subject();
        DashBoard dashBoard = null;
        private const string CANDIDATE_SEARCH_DETAILS_VIEWSTATE = 
            "CANDIDATE_SEARCH_DETAILS_VIEWSTATE";
        private const string CANDIDATE_DRAFT_DETAILS_VIEWSTATE = 
            "CANDIDATE_DRAFT_DETAILS_VIEWSTATE";
        private const string CATEGORY_SUBJECT_SEARCH_DETAILS_VIEWSTATE =
            "CATEGORY_SUBJECT_SEARCH_DETAILS_VIEWSTATE";
        private const string CATEGORY_SUBJECT_DRAFT_DETAILS_VIEWSTATE =
            "CATEGORY_SUBJECT_DRAFT_DETAILS_VIEWSTATE";
        private const string CANDIDATE_HIDDENVALUE = "CANDIDATE";
        #endregion

        #region Event Handlers                                                 
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                MessageLabel_Reset();
                MoveToDraft("Add");
                LoadValues();
                CandidateReport_categoryDetailsBottomPageNavigator.PageNumberClick +=
                    new PageNavigator.PageNumberClickEventHandler(
                        CandidateReport_categoryDetailsBottomPageNavigator_PageNumberClick);
                CandidateReport_candidate_bottomPageNavigator.PageNumberClick +=
                    new PageNavigator.PageNumberClickEventHandler(
                        CandidateReport_candidate_bottomPageNavigator_PageNumberClick);
                if (!IsPostBack)
                {
                    // Check if the page is launched from menu. If so, clear the existing
                    // session variables.
                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU) )
                    {
                        base.ClearSearchCriteriaSession();
                    }
                    else if((!Utility.IsNullOrEmpty(Request.QueryString["reset"])))
                    {
                        base.ClearSearchCriteriaSession();
                    }
                    

                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE_REPORT] != null)
                        FillSearchCriteria((CandidateReportSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE_REPORT]);
                    BindData();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateReport_topErrorMessageLabel,
                    CandidateReport_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CandidateReport_candidateDetailsGridView_RowDataBound(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    HiddenField CandidateReport_hidddenValue = (HiddenField)e.Row.FindControl(
                        "CandidateReport_hidddenValue");
                    ImageButton imgButton = (ImageButton)e.Row.FindControl(
                        "CandidateReport_selectCandidateImage");
                    imgButton.Attributes.Add("onmousedown", "javascript:return mousedown('"
                        + e.Row.RowIndex.ToString() + "','" + CandidateReport_candidateDetailsGridView.ClientID
                        + "','" + CandidateReport_candidateDraftGridView.ClientID + "');");
                    imgButton.Attributes.Add("OnClick", "javascript:return singleqnsclick('"
                        + CandidateReport_hidddenValue.Value + "','" + CandidateReport_moveToDraftButton.ClientID
                        + "','" + CANDIDATE_HIDDENVALUE + "');");
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateReport_topErrorMessageLabel,
                    CandidateReport_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CandidateReport_categoryDetailsGridView_RowDataBound(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                    HiddenField CandidateReport_hidddenValue = (HiddenField)e.Row.FindControl(
                        "CandidateReport_hidddenValue");
                    ImageButton imgButton = (ImageButton)e.Row.FindControl(
                        "CandidateReport_selectCategorySubjectImage");
                    imgButton.Attributes.Add("onmousedown", "javascript:return mousedown('"
                        + e.Row.RowIndex.ToString() + "','" + CandidateReport_categoryDetailsGridView.ClientID
                        + "','" + CandidateReport_categoryDetailsDraftGridView.ClientID + "');");
                    imgButton.Attributes.Add("OnClick", "javascript:return singleqnsclick('"
                        + CandidateReport_hidddenValue.Value + "','" + CandidateReport_moveToDraftButton.ClientID
                        + "','');");
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateReport_topErrorMessageLabel,
                    CandidateReport_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void CandidateReport_candidateDraftGridView_RowCommand(object sender,
            GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteCandidate")
                {
                    isCandidateHiddenField.Value = CANDIDATE_HIDDENVALUE;
                    hiddenValue.Value = e.CommandArgument.ToString();
                    CandidateReport_confirmPopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.CandidateReport_CandidateDelete_Confirm_Message,
                        hiddenValue.Value);
                    CandidateReport_confirmPopupExtenderControl.Title = "Delete Candidate(s)";
                    CandidateReport_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    CandidateReport_confirmPopupExtender.Show();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateReport_topErrorMessageLabel,
                    CandidateReport_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This event is commonly used to handle button controls 
        /// with a given CommandName value in the GridView control. 
        /// </remarks>
        protected void candidateReport_categoryDetailsDraftGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteCategory")
                {
                    isCandidateHiddenField.Value = "";
                    hiddenValue.Value = e.CommandArgument.ToString();
                    CandidateReport_confirmPopupExtenderControl.Message = string.Format(
                        Resources.HCMResource.CandidateReport_SubjectDelete_Confirm_Message,
                        hiddenValue.Value);

                    CandidateReport_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                    CandidateReport_confirmPopupExtenderControl.Title = "Delete Category(s) and Subject(s)";
                    CandidateReport_confirmPopupExtender.Show();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateReport_topErrorMessageLabel,
                    CandidateReport_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void CandidateReport_searchCandidate_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl + "&reset=" + Constants.ParentPage.MENU, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateReport_topErrorMessageLabel,
                    CandidateReport_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will redirect to the candidate Caomparison Report page .
        /// </remarks>
        protected void CandidateReport_comparisonReportButton_Click(object sender, EventArgs e)
        {
            try
            {
                CandidateReportDetail candidateReportDetail = null;
                dashBoard = GetDashBoard(Constants.DashboardConstants.CANDIDATE_REPORT);

                if (Forte.HCM.Support.Utility.IsNullOrEmpty(dashBoard))
                {
                    CreateDashBoard(Constants.DashboardConstants.CANDIDATE_REPORT);
                    dashBoard = GetDashBoard(Constants.DashboardConstants.CANDIDATE_REPORT);
                }
                candidateReportDetail = new CandidateReportDetail();
                candidateReportDetail = GetSelectedItems(candidateReportDetail);
                if (!Utility.IsNullOrEmpty(candidateReportDetail))
                {
                    if (candidateReportDetail.CombinedCandidateIDs.Length > 0 && candidateReportDetail.CombinedSubjectIDs.Length > 0)
                    {
                        CandidateReportSearchCriteria candidateReportSearchCriteria = new CandidateReportSearchCriteria();
                        candidateReportSearchCriteria.CandidateFirstName = CandidateReport_candidateFirstNameTextBox.Text.Trim();
                        candidateReportSearchCriteria.CandidateLastName = CandidateReport_candidateLastNameTextBox.Text.Trim();
                        candidateReportSearchCriteria.CandidateNameEmail = CandidateReport_candidateEmailTextBox.Text.Trim();
                        candidateReportSearchCriteria.CategoryName = CandidateReport_categoryTextBox.Text.Trim();
                        candidateReportSearchCriteria.Keyword = CandidateReport_keywordsTextBox.Text.Trim();
                        candidateReportSearchCriteria.SubjectName = CandidateReport_subjectTextBox.Text.Trim();
                        candidateReportSearchCriteria.CategoryCurrentPage = int.Parse(CandidateReport_categoryGridPageNoHiddenField.Value);
                        candidateReportSearchCriteria.CandidateCurrentPage =int.Parse(CandidateReport_candidateGridPageNoHiddenField.Value);
                        candidateReportSearchCriteria.CandidateDetailList = (List<UserDetail>)ViewState[CANDIDATE_DRAFT_DETAILS_VIEWSTATE];
                        candidateReportSearchCriteria.SubjectDetailList = (List<Subject>)ViewState[CATEGORY_SUBJECT_DRAFT_DETAILS_VIEWSTATE];

                        Session[Constants.SearchCriteriaSessionKey.SEARCH_CANDIDATE_REPORT] = candidateReportSearchCriteria;
                        userDetail = new UserDetail();
                        userDetail = (UserDetail)Session["USER_DETAIL"];
                        candidateReportDetail.TestName = "CANDIDATE COMPARISON REPORT";
                        candidateReportDetail.LoginUser = userDetail.FirstName + "," + userDetail.LastName;
                       // candidateReportDetail.CandidateName = "CANDIDATE COMPARISON REPORT";
                        Session["CANDIDATEDETAIL"] = candidateReportDetail;
                        Response.Redirect("~/ReportCenter/CandidateComparisonReport.aspx?m=3&s=1" +
                            "&dashboardkey=" + dashBoard.DashBoardID +
                            "&dashboardsectionkey=" + dashBoard.DashBoardSectionID +
                            "&filename=" + candidateReportDetail.CandidateNames.Replace(',', '-').Replace(' ', '-') +
                            "&parentpage=" + Constants.ParentPage.CANDIDATE_REPORT, false);
                    }
                    else
                    {
                        ShowMessage(CandidateReport_topErrorMessageLabel,
                        CandidateReport_bottomErrorMessageLabel,Resources.HCMResource.CandidateReport_SelectAtleastOne);
                    }
                }
                else
                {
                    ShowMessage(CandidateReport_topErrorMessageLabel,
                     CandidateReport_bottomErrorMessageLabel, Resources.HCMResource.CandidateReport_SelectAtleastOne);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateReport_topErrorMessageLabel,
                    CandidateReport_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void CandidateReport_candidateSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                CandidateReport_candidate_bottomPageNavigator.Reset();
                LoadCanditate(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateReport_topErrorMessageLabel,
                    CandidateReport_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void CandidateReport_searchSubjectButton_Click(object sender, EventArgs e)
        {
            try
            {
                CandidateReport_categoryDetailsBottomPageNavigator.Reset();
                LoadCategorySubject(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateReport_topErrorMessageLabel,
                    CandidateReport_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when Deag and Drop the Grid.
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        ///Selected Data moved to the Draft panels.
        /// </remarks>
        protected void CandidateReport_moveToDraftButton_Click(object sender, EventArgs e)
        {
            try
            {
                MoveToDraft("Add");
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateReport_topErrorMessageLabel,
                    CandidateReport_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void CandidateReport_okClick(object sender, EventArgs e)
        {
            try
            {
                CandidateReport_postbackHiddenField.Value = "1";
                MoveToDraft("Delete");
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateReport_topErrorMessageLabel,
                    CandidateReport_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Cancel Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel the Event will be raised.
        /// </remarks>
        protected void CandidateReport_cancelClick(object sender, EventArgs e)
        {
            try
            {
                hiddenValue.Value = "";
                CandidateReport_postbackHiddenField.Value = "";
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateReport_topErrorMessageLabel,
                    CandidateReport_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        void CandidateReport_candidate_bottomPageNavigator_PageNumberClick(object sender,
            Forte.HCM.EventSupport.PageNumberEventArgs e)
        {
            try
            {
                CandidateReport_candidateGridPageNoHiddenField.Value = e.PageNumber.ToString();
                LoadCanditate(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateReport_topErrorMessageLabel,
                    CandidateReport_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        void CandidateReport_categoryDetailsBottomPageNavigator_PageNumberClick(object sender,
            Forte.HCM.EventSupport.PageNumberEventArgs e)
        {
            try
            {
                CandidateReport_categoryGridPageNoHiddenField.Value = e.PageNumber.ToString();
                LoadCategorySubject(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(CandidateReport_topErrorMessageLabel,
                    CandidateReport_bottomErrorMessageLabel, exception.Message);
            }
        }
        #endregion

        #region Private  Methods                                               
        /// <summary>
        /// Method that will fill the search criteria to the appropriate fields.
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A <see cref="TestSearchCriteria"/> that contains the test search
        /// criteria fields.
        /// </param>
        public void FillSearchCriteria(CandidateReportSearchCriteria candidateReportSearchCriteria)
        {
            if(!Utility.IsNullOrEmpty(candidateReportSearchCriteria.CandidateFirstName))
                CandidateReport_candidateFirstNameTextBox.Text = candidateReportSearchCriteria.CandidateFirstName;
            if (!Utility.IsNullOrEmpty(candidateReportSearchCriteria.CandidateLastName))
                CandidateReport_candidateLastNameTextBox.Text = candidateReportSearchCriteria.CandidateLastName;
            if (!Utility.IsNullOrEmpty(candidateReportSearchCriteria.CandidateNameEmail))
                CandidateReport_candidateEmailTextBox.Text = candidateReportSearchCriteria.CandidateNameEmail;
            if (!Utility.IsNullOrEmpty(candidateReportSearchCriteria.CategoryName))
                CandidateReport_categoryTextBox.Text = candidateReportSearchCriteria.CategoryName;
            if (!Utility.IsNullOrEmpty(candidateReportSearchCriteria.Keyword))
                CandidateReport_keywordsTextBox.Text = candidateReportSearchCriteria.Keyword;
            if (!Utility.IsNullOrEmpty(candidateReportSearchCriteria.SubjectName))
                CandidateReport_subjectTextBox.Text = candidateReportSearchCriteria.SubjectName;
            
            ViewState[CANDIDATE_DRAFT_DETAILS_VIEWSTATE] = candidateReportSearchCriteria.CandidateDetailList;
            ViewState[CATEGORY_SUBJECT_DRAFT_DETAILS_VIEWSTATE] = candidateReportSearchCriteria.SubjectDetailList;

            LoadCategorySubject(candidateReportSearchCriteria.CategoryCurrentPage);
            LoadCanditate(candidateReportSearchCriteria.CandidateCurrentPage);

            CandidateReport_candidate_bottomPageNavigator.MoveToPage(candidateReportSearchCriteria.CandidateCurrentPage);
            CandidateReport_categoryDetailsBottomPageNavigator.MoveToPage(candidateReportSearchCriteria.CategoryCurrentPage);
        }

        /// <summary>
        /// Represents the method to get the selected items
        /// </summary>
        /// <returns>
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report details
        /// </returns>
        private CandidateReportDetail GetSelectedItems(CandidateReportDetail candidateReportDetail)
        {
            if (CandidateReport_candidateDraftGridView.Rows.Count == 0 || CandidateReport_categoryDetailsDraftGridView.Rows.Count == 0)
            {
                return null;
            }

            //Select the candidate ids for the selected candidate
            foreach (GridViewRow row in CandidateReport_candidateDraftGridView.Rows)
            {
                //Add the candidate ids to the combined candidate ids
                candidateReportDetail.CombinedCandidateIDs += ((HiddenField)row.FindControl("CandidateReport_hidddenValue")).Value + ",";
                candidateReportDetail.CandidateNames += ((Label)row.FindControl("CandidateReport_candidateDraft_fullNameLabel")).Text + ",";
            }
            //Remove the last comma in the candidate ids
            candidateReportDetail.CombinedCandidateIDs = candidateReportDetail.CombinedCandidateIDs.TrimEnd(',');
            candidateReportDetail.CandidateNames = candidateReportDetail.CandidateNames.TrimEnd(',');

            //Select the candidate ids for the selected candidate
            foreach (GridViewRow row in CandidateReport_categoryDetailsDraftGridView.Rows)
            {
                //Add the candidate ids to the combined candidate ids
                candidateReportDetail.CombinedSubjectIDs += ((HiddenField)row.FindControl("CandidateReport_hidddenValue")).Value + ",";

            }
            //Remove the last comma in the candidate ids
            candidateReportDetail.CombinedSubjectIDs = candidateReportDetail.CombinedSubjectIDs.Remove(candidateReportDetail.CombinedSubjectIDs.Length - 1, 1);

            return candidateReportDetail;
        }

        /// <summary>
        /// Bind the Category and Candidate Details.
        /// </summary>
        private void BindData()
        {
            CandidateReport_categoryDetailsDraftGridView.DataSource =
                (List<Subject>)ViewState[CATEGORY_SUBJECT_DRAFT_DETAILS_VIEWSTATE];
            CandidateReport_categoryDetailsDraftGridView.DataBind();
            CandidateReport_candidateDraftGridView.DataSource =
                (List<UserDetail>)ViewState[CANDIDATE_DRAFT_DETAILS_VIEWSTATE];
            CandidateReport_candidateDraftGridView.DataBind();
        }

        /// <summary>
        /// add and remove the Candidate/Subjects Draft.
        /// </summary>
        /// <param name="action">
        /// Add /delete
        /// </param>
        private void MoveToDraft(string action)
        {
            if (CandidateReport_postbackHiddenField.Value == "1")
            {
                userDetailList = new List<UserDetail>();
                userDetailList = (List<UserDetail>)ViewState[CANDIDATE_SEARCH_DETAILS_VIEWSTATE];
                subjectList = new List<Subject>();
                subjectList = (List<Subject>)ViewState[CATEGORY_SUBJECT_SEARCH_DETAILS_VIEWSTATE];
                string addedID = "";
                string notAddedID = "";
                bool alertFlag = false;


                string questionID = hiddenValue.Value;
                string[] questionIds = hiddenValue.Value.Split(',');

                foreach (string id in questionIds)
                {
                    if (id != "")
                    {
                        if (isCandidateHiddenField.Value == CANDIDATE_HIDDENVALUE)
                        {
                            if (ViewState[CANDIDATE_DRAFT_DETAILS_VIEWSTATE] != null)
                            {
                                userDraftDetailList = new List<UserDetail>();
                                userDraftDetailList =
                                    (List<UserDetail>)ViewState[CANDIDATE_DRAFT_DETAILS_VIEWSTATE];
                                if (action == "Add")
                                {
                                    if (!userDraftDetailList.Exists(p => p.UserID.ToString() == id.Trim()))
                                    {
                                        userDetail = userDetailList.Find(p => p.UserID.ToString() == id.Trim());
                                        userDraftDetailList.Add(userDetail);
                                        ViewState[CANDIDATE_DRAFT_DETAILS_VIEWSTATE] = userDraftDetailList;
                                        addedID = addedID + id + ", ";
                                        MessageLabel_Reset();
                                        base.ShowMessage(CandidateReport_topSuccessMessageLabel,
                                            CandidateReport_bottomSuccessMessageLabel,
                                            Resources.HCMResource.CandidateReport_MovetoCandidate_Draft);
                                    }
                                    else
                                    {
                                        notAddedID = notAddedID + id + ", ";
                                        alertFlag = true;
                                    }
                                }
                                else if (action == "Delete")
                                {
                                    userDetail = userDraftDetailList.Find(p => p.UserID.ToString() == id.Trim());
                                    userDraftDetailList.Remove(userDetail);
                                    MessageLabel_Reset();
                                    base.ShowMessage(CandidateReport_topSuccessMessageLabel,
                                        CandidateReport_bottomSuccessMessageLabel,
                                        Resources.HCMResource.CandidateReport_RemoveFromCandidate_Draft);
                                    ViewState[CANDIDATE_DRAFT_DETAILS_VIEWSTATE] = userDraftDetailList;
                                }
                            }
                            else
                            {
                                userDraftDetailList = new List<UserDetail>();
                                userDetail = userDetailList.Find(p => p.UserID.ToString() == id.Trim());
                                userDraftDetailList.Add(userDetail);
                                ViewState[CANDIDATE_DRAFT_DETAILS_VIEWSTATE] = userDraftDetailList;
                                //addedQuestionID = addedQuestionID + id + ", ";
                                MessageLabel_Reset();
                                base.ShowMessage(CandidateReport_topSuccessMessageLabel,
                                    CandidateReport_bottomSuccessMessageLabel,
                                    Resources.HCMResource.CandidateReport_MovetoCandidate_Draft);
                            }
                        }
                        else
                        {
                            if (ViewState[CATEGORY_SUBJECT_DRAFT_DETAILS_VIEWSTATE] != null)
                            {
                                subjectDraftList = new List<Subject>();
                                subjectDraftList =
                                    (List<Subject>)ViewState[CATEGORY_SUBJECT_DRAFT_DETAILS_VIEWSTATE];
                                if (action == "Add")
                                {
                                    if (!subjectDraftList.Exists(p => p.SubjectID.ToString() == id.Trim()))
                                    {
                                        subject = subjectList.Find(p => p.SubjectID.ToString() == id.Trim());
                                        subjectDraftList.Add(subject);
                                        ViewState[CATEGORY_SUBJECT_DRAFT_DETAILS_VIEWSTATE] = subjectDraftList;
                                        addedID = addedID + id + ", ";
                                        MessageLabel_Reset();
                                        base.ShowMessage(CandidateReport_topSuccessMessageLabel,
                                            CandidateReport_bottomSuccessMessageLabel,
                                            Resources.HCMResource.CandidateReport_MovetoSubject_Draft);
                                    }
                                    else
                                    {
                                        notAddedID = notAddedID + id + ", ";
                                        alertFlag = true;
                                    }
                                }
                                else if (action == "Delete")
                                {
                                    subject = subjectDraftList.Find(p => p.SubjectID.ToString() == id.Trim());
                                    subjectDraftList.Remove(subject);
                                    MessageLabel_Reset();
                                    base.ShowMessage(CandidateReport_topSuccessMessageLabel,
                                        CandidateReport_bottomSuccessMessageLabel,
                                        Resources.HCMResource.CandidateReport_RemoveFromSubject_Draft);
                                    ViewState[CATEGORY_SUBJECT_DRAFT_DETAILS_VIEWSTATE] = subjectDraftList;
                                }
                            }
                            else
                            {
                                subjectDraftList = new List<Subject>();
                                subject = new Subject();
                                subject = subjectList.Find(p => p.SubjectID.ToString() == id.Trim());
                                subjectDraftList.Add(subject);
                                MessageLabel_Reset();
                                ViewState[CATEGORY_SUBJECT_DRAFT_DETAILS_VIEWSTATE] = subjectDraftList;
                                //addedQuestionID = addedQuestionID + id + ", ";
                                base.ShowMessage(CandidateReport_topSuccessMessageLabel,
                                    CandidateReport_bottomSuccessMessageLabel,
                                    Resources.HCMResource.CandidateReport_MovetoSubject_Draft);
                            }

                        }
                    }
                }
                UncheckCheckBox();
                CandidateReport_postbackHiddenField.Value = string.Empty;
                if (alertFlag)
                {
                    string alertMessage = string.Empty;
                    if (isCandidateHiddenField.Value == CANDIDATE_HIDDENVALUE)
                    {
                        alertMessage = string.Format(Resources.HCMResource.CandidateReport_CandidateExist,
                            notAddedID.Trim().TrimEnd(','));
                        if (!Forte.HCM.Support.Utility.IsNullOrEmpty(addedID))
                        {
                            alertMessage = alertMessage + "<br/>" + string.Format(
                                Resources.HCMResource.CandidateReport_CandidateAddedToCandidateDraft,
                                addedID.Trim().TrimEnd(','));
                        }
                    }
                    else
                    {
                        alertMessage = string.Format(Resources.HCMResource.CandidateReport_SubjectExist,
                            notAddedID.Trim().TrimEnd(','));

                        if (!Forte.HCM.Support.Utility.IsNullOrEmpty(addedID))
                        {
                            alertMessage = alertMessage + "<br/>" + string.Format(
                                Resources.HCMResource.CandidateReport_SubjectAddedToCandidateDraft,
                                addedID.Trim().TrimEnd(','));
                        }
                    }
                    hiddenValue.Value = string.Empty;
                    CandidateReport_confirmPopupExtenderControl.Type = MessageBoxType.YesNoSmall;
                    CandidateReport_confirmPopupExtenderControl.Title = "Warning";
                    CandidateReport_confirmPopupExtenderControl.Message = alertMessage;
                    CandidateReport_confirmPopupExtender.Show();
                }
                BindData();
            }
        }

        /// <summary>
        /// Uncheck the checkbox 
        /// </summary>
        private void UncheckCheckBox()
        {
            foreach (GridViewRow row in CandidateReport_candidateDetailsGridView.Rows)
            {
                if (((CheckBox)row.FindControl("CandidateReport_candidateDetailsGridViewCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("CandidateReport_candidateDetailsGridViewCheckbox")).Checked = false;
                }
            }

            foreach (GridViewRow row in CandidateReport_categoryDetailsGridView.Rows)
            {
                if (((CheckBox)row.FindControl("CandidateReport_categoryDetailsGridViewCheckbox")).Checked)
                {
                    ((CheckBox)row.FindControl("CandidateReport_categoryDetailsGridViewCheckbox")).Checked = false;
                }
            }
        }

        /// <summary>
        /// Get Search Results. 
        /// </summary>
        /// <param name="pageNumber">
        /// Current Page number 
        /// </param>
        private void LoadCategorySubject(int pageNo)
        {
            int totalRecords = 0;
            subjectList = new List<Subject>();

            subjectList = new BL.CommonBLManager().GetCategorySubjects(pageNo, base.tenantID, string.Empty,
                CandidateReport_categoryTextBox.Text.Trim(), string.Empty,
                CandidateReport_subjectTextBox.Text.Trim(), GridPageSize, out totalRecords, string.Empty,
                SortType.Ascending, CandidateReport_keywordsTextBox.Text.Trim());

            if (subjectList == null || subjectList.Count == 0)
            {
                CandidateReport_categoryDetailsGridView.DataSource = null;
                CandidateReport_categoryDetailsGridView.DataBind();
                CandidateReport_categoryDetailsBottomPageNavigator.TotalRecords = 0;
                //  CandidateReport_testDiv.Visible = false;
            }
            else
            {
                CandidateReport_categoryDetailsGridView.DataSource = subjectList;
                CandidateReport_categoryDetailsGridView.DataBind();
                CandidateReport_categoryDetailsBottomPageNavigator.PageSize = base.GridPageSize;
                CandidateReport_categoryDetailsBottomPageNavigator.TotalRecords = totalRecords;
                // CandidateReport_testDiv.Visible = true;
            }
            ViewState[CATEGORY_SUBJECT_SEARCH_DETAILS_VIEWSTATE] = subjectList;
        }

        /// <summary>
        /// Get Search Results. 
        /// </summary>
        /// <param name="pageNumber">
        /// Current Page number 
        /// </param>
        private void LoadCanditate(int pageNo)
        {
            int totalRecords = 0;
            userDetailList = new List<UserDetail>();

            userDetailList = new BL.CommonBLManager().GetCandidatesForCandidateReport(string.Empty,
                CandidateReport_candidateFirstNameTextBox.Text,
                CandidateReport_candidateLastNameTextBox.Text, CandidateReport_candidateEmailTextBox.Text,
                "FIRSTNAME", SortType.Ascending, pageNo, GridPageSize, out totalRecords, base.tenantID);

            if (userDetailList == null || userDetailList.Count == 0)
            {
                CandidateReport_candidateDetailsGridView.DataSource = null;
                CandidateReport_candidateDetailsGridView.DataBind();
                CandidateReport_candidate_bottomPageNavigator.TotalRecords = 0;
                //  CandidateReport_testDiv.Visible = false;
            }
            else
            {
                CandidateReport_candidateDetailsGridView.DataSource = userDetailList;
                CandidateReport_candidateDetailsGridView.DataBind();
                CandidateReport_candidate_bottomPageNavigator.PageSize = base.GridPageSize;
                CandidateReport_candidate_bottomPageNavigator.TotalRecords = totalRecords;
                // CandidateReport_testDiv.Visible = true;
            }
            ViewState[CANDIDATE_SEARCH_DETAILS_VIEWSTATE] = userDetailList;

        }

        /// <summary>
        /// Reset the Messeags.
        /// </summary>
        private void MessageLabel_Reset()
        {
            CandidateReport_topErrorMessageLabel.Text = string.Empty;
            CandidateReport_bottomErrorMessageLabel.Text = string.Empty;
            CandidateReport_topSuccessMessageLabel.Text = string.Empty;
            CandidateReport_bottomSuccessMessageLabel.Text = string.Empty;
        }
        /// <summary>
        /// Create Dashboard .
        /// </summary>
        private void CreateDashBoard(string dashBoardName)
        {
            DashboardInstance instance = new DashboardInstance()
            {
                InstanceKey = Guid.NewGuid(),
                Title = dashBoardName + " DashBoard",
                Username = Thread.CurrentPrincipal.Identity.Name,
                Width = 830,
                Header = false,
                AutoHeight = true,
                Group = dashBoardName,
                BodyStyle = "background-color: #FFFFFF;"

            };

            DashboardSectionInstance row1 = new DashboardSectionInstance(instance.InstanceKey, 0,
                Guid.NewGuid());
            DashboardColumn col1 = new DashboardColumn(row1.InstanceKey, 0, 50, null, null);
            DashboardColumn col2 = new DashboardColumn(row1.InstanceKey, 1, 50, null, null);
            row1.BodyStyle = "background-color: #FFFFFF;";
            row1.Columns.Add(col1);
            row1.Columns.Add(col2);
            instance.Rows.Add(row1);

            DashboardFramework.CreateDashboard(instance);

        }

        /// <summary>
        /// Get Existing Datshboard Details based on the User ID.
        /// </summary>
        /// <returns> 
        /// dashboard Id and dashboard Section ID.
        /// </returns>
        private DashBoard GetDashBoard(string groupName)
        {
            dashBoard = new DashBoard();
            dashBoard = new DashBoardBLManager().GetDashBoardId(Thread.CurrentPrincipal.Identity.Name,
                groupName);
            return dashBoard;
        }

        #endregion

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            CandidateReport_candidateFirstNameTextBox.Focus();
            Master.SetPageCaption(Resources.HCMResource.CandidateReport_Title);
        }

        #endregion Protected Overridden Methods
    }
}