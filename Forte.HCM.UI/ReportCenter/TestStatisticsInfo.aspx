<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OTMMaster.Master" Inherits="Forte.HCM.UI.ReportCenter.TestStatisticsInfo" CodeBehind="TestStatisticsInfo.aspx.cs" %>

<%@ Register Src="../CommonControls/GeneralTestStatisticsControl.ascx" TagName="GeneralTestStatisticsControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/QuestionStatisticsControl.ascx" TagName="QuestionStatisticsControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="TestStatisticsInfo_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
//        function getUrlParameter(name) {
//            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
//            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
//            var results = regex.exec(location.search);
//            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
//        }

        function getUrlQueryString(name) {
            var query = window.location.search.substring(1);
            var parms = query.split('&');
            for (var i = 0; i < parms.length; i++) {              
                var pos = parms[i].indexOf('=');               
                if (pos > 0) {
                    var key = parms[i].substring(0, pos);
                    var val = parms[i].substring(pos + 1);
                    if (key === name) {                        
                        return val;
                    }
                }
            }
        }

        function ExpORCollapseQuestRows(targetControl) {
            var ctrl;
            var paramType = getUrlQueryString('Questiontype');           
            if (paramType === "2") {              
                ctrl = document.getElementById('<%=TestStatisticsInfo_questionStatisticsStateExpandHiddenField_OpenText.ClientID %>');
            }
            else {               
                ctrl = document.getElementById('<%=TestStatisticsInfo_questionStatisticsStateExpandHiddenField.ClientID %>');
            }       
            targetValue = ctrl.value;
            if (targetValue == "0") {
                targetControl.innerHTML = "Collapse All";
                ExpandAllQuestRows(targetControl);
                ctrl.value = 1;
            }
            else {
                targetControl.innerHTML = "Expand All";
                CollapseAllRows(targetControl);
                ctrl.value = 0;
            }
            return false;
        }


        function ExpandAllQuestRows(targetControl) {
            var gridCtrl = null;
            var paramType = getUrlQueryString('Questiontype');
            if (paramType === "1")
                 gridCtrl = document.getElementById("<%= TestStatisticsInfo_questionStatistics_questionDiv.ClientID %>");
            else
                 gridCtrl = document.getElementById("<%= TestStatisticsInfo_questionStatistics_questionDiv_OpenText.ClientID %>");

            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (paramType === "1") {
                        if (rowItems[indexRow].id.indexOf("TestStatisticsInfo_questionStatisticsGridView_detailsDiv") != "-1") {
                            rowItems[indexRow].style.display = "block";
                        }
                    }
                    else {
                        if (rowItems[indexRow].id.indexOf("TestStatisticsInfo_questionStatisticsGridView_detailsDiv_OpenText") != "-1") {
                            rowItems[indexRow].style.display = "block";
                        }
                    }
                }
            }
            return false;
        }
        function CollapseAllRows(targetControl) {
            var gridCtrl = null;
            var paramType = getUrlQueryString('Questiontype');
            if (paramType === "1")
                gridCtrl = document.getElementById("<%= TestStatisticsInfo_questionStatistics_questionDiv.ClientID %>");
            else
                gridCtrl = document.getElementById("<%= TestStatisticsInfo_questionStatistics_questionDiv_OpenText.ClientID %>");
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (paramType === "1") {
                        if (rowItems[indexRow].id.indexOf("TestStatisticsInfo_questionStatisticsGridView_detailsDiv") != "-1") {
                            rowItems[indexRow].style.display = "none";
                        }
                    }
                    else {
                        if (rowItems[indexRow].id.indexOf("TestStatisticsInfo_questionStatisticsGridView_detailsDiv_OpenText") != "-1") {
                            rowItems[indexRow].style.display = "none";
                        }
                    }
                }
            }
            if (paramType === "1") {
                var linkcontrol = document.getElementById
            ("<%=TestStatisticsInfo_questionStatisticsTestDraftExpandLinkButton.ClientID %>");
                if (linkcontrol.innerHTML = "Collapse All") {
                    linkcontrol.innerHTML = "Expand All";
                    var ctrl = document.getElementById('<%=TestStatisticsInfo_questionStatisticsStateExpandHiddenField.ClientID %>');
                    ctrl.value = 0;

                }
            } else {
                var linkcontrol = document.getElementById
            ("<%=TestStatisticsInfo_questionStatisticsTestDraftExpandLinkButton_OpenText.ClientID %>");
                if (linkcontrol.innerHTML = "Collapse All") {
                    linkcontrol.innerHTML = "Expand All";
                    var ctrl = document.getElementById('<%=TestStatisticsInfo_questionStatisticsStateExpandHiddenField_OpenText.ClientID %>');
                    ctrl.value = 0;

                }
            }

            return false;
        }

        function HideQuestDetails(ctrlId) {
            if (document.getElementById(ctrlId).style.display == "none") {
                CollapseAllRows(ctrlId);
                document.getElementById(ctrlId).style.display = "block";
            }
            else {
                CollapseAllRows(ctrlId);
                document.getElementById(ctrlId).style.display = "none";
            }
            return false;
        }

        function GetMouseClickedPos(ctrlID) {
            if (document.getElementById(ctrlID) != null) {
                document.getElementById(ctrlID).focus();
            }
            return true;
        }

        // Handler method that will be called when the maximize or restore
        // button is clicked. This will maximize or restore the panel to
        // show or hide full view.
        function ExpandOrRestore(ctrlExpand, ctrlHide, expandImage, compressImage,
            isMaximizedHidden, restoredHeight, expandedHeight) {
            if (document.getElementById(ctrlExpand) != null) {
                document.getElementById(expandImage).style.display = "none";
                document.getElementById(compressImage).style.display = "none";
                if (document.getElementById(ctrlExpand).style.height.toString() == restoredHeight) {
                    document.getElementById(ctrlExpand).style.height = expandedHeight;
                    document.getElementById(ctrlHide).style.display = "none";
                    document.getElementById(expandImage).style.display = "block";
                    document.getElementById(isMaximizedHidden).value = "Y";
                }
                else {
                    document.getElementById(ctrlExpand).style.height = restoredHeight;
                    document.getElementById(ctrlHide).style.display = "block";
                    document.getElementById(compressImage).style.display = "block";
                    document.getElementById(isMaximizedHidden).value = "N";
                }
            }
            return false;
        }

        function selectedCanidates(a, b) {
            try {
                var candidates = document.getElementById('<%=TestStatisticsInfo_selectedcanidates.ClientID %>').value;
                if (a.checked) {
                    if (candidates.length == 0)
                        candidates = b;
                    else
                        candidates = candidates + "," + b;
                }
                else {
                    candidates = candidates.replace(b, "");
                }
                document.getElementById('<%=TestStatisticsInfo_selectedcanidates.ClientID %>').value = candidates;
            }
            catch (exp) {
                // alert(exp);
            }
        }
    </script>
    <asp:HiddenField ID="TestStatisticsInfo_selectedcanidates" runat="server" />
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="TestStatisticsInfo_headerLiteral" runat="server" Text="Test Statistics Information"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td width="18%" align="right">
                                        <asp:LinkButton ID="TestStatisticsInfo_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="TestStatisticsInfo_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="TestStatisticsInfo_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="TestStatisticsInfo_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <div id="TestStatisticsInfo_searchByTestSessionDiv" runat="server" style="display: block;">
                    <table width="90%" border="0" cellspacing="3" cellpadding="0">
                        <tr>
                            <td style="width: 7%">
                                <asp:Label ID="TestStatisticsInfo_testIdHeadLabel" runat="server" Text="Test ID"
                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="TestStatisticsInfo_testIdLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                            </td>
                            <td style="width: 10%">
                                <asp:Label ID="TestStatisticsInfo_testNameHeadLabel" runat="server" Text="Test Name"
                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="TestStatisticsInfo_testNameLabel" runat="server" Text="Net test - 3 years"
                                    SkinID="sknLabelFieldText"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="height: 100%; overflow: auto; display: block;" runat="server" id="TestStatisticsInfo_testSessionDiv">
                    <ajaxToolKit:TabContainer ID="TestStatisticsInfo_mainTabContainer" runat="server"
                        Width="100%" ActiveTabIndex="0">
                        <ajaxToolKit:TabPanel ID="TestStatisticsInfo_generalTabPanel" HeaderText="Search By Test"
                            runat="server">
                            <HeaderTemplate>
                                General Statistics
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <uc1:GeneralTestStatisticsControl ID="TestStatisticsInfo_generalTestStatisticsControl"
                                                runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                        <ajaxToolKit:TabPanel ID="TestStatisticsInfo_questionTabPanel" runat="server">
                            <HeaderTemplate>
                                Question Statistics
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:UpdatePanel ID="TestStatisticsInfo_questionStatisticsUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" id="TestStatisticsInfo_questionStatisticsUpdatePanel_MultipleChoiceTable" visible="true">
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:LinkButton ID="TestStatisticsInfo_questionStatisticsTestDraftExpandLinkButton"
                                                                                runat="server" Text="Expand All" SkinID="sknActionLinkButton" OnClientClick="javascript:return ExpORCollapseQuestRows(this);"></asp:LinkButton>
                                                                            <asp:HiddenField ID="TestStatisticsInfo_questionStatisticsStateExpandHiddenField"
                                                                                runat="server" Value="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <div style="height: 320px; overflow: auto;" runat="server" id="TestStatisticsInfo_questionStatistics_questionDiv">
                                                                                <asp:GridView ID="TestStatisticsInfo_questionStatisticsGridView" runat="server" AllowSorting="true"
                                                                                    SkinID="sknWrapHeaderGrid" AutoGenerateColumns="false" Width="100%" OnSorting="TestStatisticsInfo_questionStatisticsGridView_Sorting"
                                                                                    OnRowDataBound="TestStatisticsInfo_questionStatisticsGridView_RowDataBound" OnRowCreated="TestStatisticsInfo_questionStatisticsGridView_RowCreated">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Question ID" Visible="false">
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="TestStatisticsInfo_questionStatisticsGridView_questionIdGridLinkButton"
                                                                                                    runat="server" Text='<%# Eval("QuestionKey") %>' ToolTip="Question Details"></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Font-Underline="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Question" SortExpression="Question DESC" HeaderStyle-CssClass="td_padding_right_20"
                                                                                            ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="350px">
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="TestStatisticsInfo_questionStatisticsGridView_questionGridLinkButton"
                                                                                                    runat="server"></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Avg&nbsp;Time&nbsp;Taken (Within Test in Sec)" SortExpression="LocalTime DESC"
                                                                                            ItemStyle-CssClass="td_padding_right_20" HeaderStyle-CssClass="td_padding_right_20"
                                                                                            ItemStyle-HorizontalAlign="right" ItemStyle-Width="14%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_WithinTest" runat="server"
                                                                                                    Text='<%# GetCorrectDate(Convert.ToDecimal(Eval("AverageTimeTakenWithinTest"))) %>'>
                                                                                                </asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Avg&nbsp;Time&nbsp;Taken (Across All Test in Sec)"
                                                                                            SortExpression="GlobalTime DESC" ItemStyle-CssClass="td_padding_right_20" HeaderStyle-CssClass="td_padding_right_20"
                                                                                            ItemStyle-HorizontalAlign="right" ItemStyle-Width="16%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_acrossTest" runat="server"
                                                                                                    Text='<%# GetCorrectDate(Convert.ToDecimal(Eval("AverageTimeTakenAcrossTest"))) %>'>
                                                                                                </asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField HeaderText="Right Answer : Administered" DataField="Ratio" HeaderStyle-CssClass="td_padding_right_20"
                                                                                            SortExpression="Ratio" ItemStyle-HorizontalAlign="center" ItemStyle-Width="15%" />
                                                                                        <asp:TemplateField HeaderText="Author" SortExpression="Author" ItemStyle-Width="16%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_testAuthorNameLabel"
                                                                                                    runat="server"></asp:Label>
                                                                                                <tr>
                                                                                                    <td colspan="8">
                                                                                                        <div id="TestStatisticsInfo_questionStatisticsGridView_detailsDiv" runat="server"
                                                                                                            style="display: none;">
                                                                                                            <table border="0" cellpadding="3" cellspacing="3" width="850px" class="table_outline_bg">
                                                                                                                <tr>
                                                                                                                    <td class="popup_question_icon">
                                                                                                                        <div style="word-wrap: break-word; white-space: normal;">
                                                                                                                        <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_questionLabel" runat="server"
                                                                                                                            SkinID="sknLabelFieldMultiText"></asp:Label>
                                                                                                                        </div>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="td_padding_left_20">
                                                                                                                        <asp:PlaceHolder ID="TestStatisticsInfo_questionStatisticsGridView_answerChoicesPlaceHolder"
                                                                                                                            runat="server"></asp:PlaceHolder>
                                                                                                                        <asp:HiddenField ID="CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="td_padding_10">
                                                                                                                        <table border="0" cellpadding="0" cellspacing="4" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td style="width: 8%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_categoryLabel" runat="server"
                                                                                                                                        SkinID="sknLabelFieldHeaderText" Text="Category"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 15%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_categoryValueLabel"
                                                                                                                                        runat="server" ReadOnly="true" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 6%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_subjectLabel" runat="server"
                                                                                                                                        SkinID="sknLabelFieldHeaderText" Text="Subject"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 20%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_subjectValueLabel" runat="server"
                                                                                                                                        ReadOnly="true" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 8%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_testAreaLabel" runat="server"
                                                                                                                                        SkinID="sknLabelFieldHeaderText" Text="Test Area"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 12%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_testAreaValueLabel"
                                                                                                                                        runat="server" ReadOnly="true" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 10%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_complexityLabel" runat="server"
                                                                                                                                        SkinID="sknLabelFieldHeaderText" Text="Complexity"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 21%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_complexityValueLabel"
                                                                                                                                        runat="server" ReadOnly="true" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                        <%-- popup DIV --%>
                                                                                                        <a href="#SearchQuestion_focusmeLink" id="TestStatisticsInfo_focusDownLink" runat="server">
                                                                                                        </a><a href="#" id="TestStatisticsInfo_focusmeLink" runat="server"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                                 <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" id="TestStatisticsInfo_questionStatisticsUpdatePanel_OpenTextTable" visible="false">
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:LinkButton ID="TestStatisticsInfo_questionStatisticsTestDraftExpandLinkButton_OpenText"
                                                                                runat="server" Text="Expand All" SkinID="sknActionLinkButton" OnClientClick="javascript:return ExpORCollapseQuestRows(this);"></asp:LinkButton>
                                                                            <asp:HiddenField ID="TestStatisticsInfo_questionStatisticsStateExpandHiddenField_OpenText"
                                                                                runat="server" Value="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <div style="height: 320px; overflow: auto;" runat="server" id="TestStatisticsInfo_questionStatistics_questionDiv_OpenText">
                                                                                <asp:GridView ID="TestStatisticsInfo_questionStatisticsGridView_OpenText" runat="server" AllowSorting="true"
                                                                                    SkinID="sknWrapHeaderGrid" AutoGenerateColumns="false" Width="100%" OnSorting="TestStatisticsInfo_questionStatisticsGridView_OpenText_Sorting" OnRowCreated="TestStatisticsInfo_questionStatisticsGridView_OpenText_RowCreated" OnRowDataBound="TestStatisticsInfo_questionStatisticsGridView_OpenText_RowDataBound" >
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Question ID" Visible="false">
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="TestStatisticsInfo_questionStatisticsGridView_questionIdGridLinkButton_OpenText"
                                                                                                    runat="server" Text='<%# Eval("QuestionKey") %>' ToolTip="Question Details"></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Font-Underline="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Question" SortExpression="Question DESC" HeaderStyle-CssClass="td_padding_right_20"
                                                                                            ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="350px">
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="TestStatisticsInfo_questionStatisticsGridView_questionGridLinkButton_OpenText"
                                                                                                    runat="server"></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Avg&nbsp;Time&nbsp;Taken (Within Test in Sec)" SortExpression="LocalTime DESC"
                                                                                            ItemStyle-CssClass="td_padding_right_20" HeaderStyle-CssClass="td_padding_right_20"
                                                                                            ItemStyle-HorizontalAlign="right" ItemStyle-Width="14%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_WithinTest_OpenText" runat="server"
                                                                                                    Text='<%# GetCorrectDate(Convert.ToDecimal(Eval("AverageTimeTakenWithinTest"))) %>'>
                                                                                                </asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Avg&nbsp;Time&nbsp;Taken (Across All Test in Sec)"
                                                                                            SortExpression="GlobalTime DESC" ItemStyle-CssClass="td_padding_right_20" HeaderStyle-CssClass="td_padding_right_20"
                                                                                            ItemStyle-HorizontalAlign="right" ItemStyle-Width="16%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_acrossTest_OpenText" runat="server"
                                                                                                    Text='<%# GetCorrectDate(Convert.ToDecimal(Eval("AverageTimeTakenAcrossTest"))) %>'>
                                                                                                </asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField HeaderText="Right Answer : Administered" DataField="Ratio" HeaderStyle-CssClass="td_padding_right_20"
                                                                                            SortExpression="Ratio" ItemStyle-HorizontalAlign="center" ItemStyle-Width="15%" />
                                                                                        <asp:TemplateField HeaderText="Author" SortExpression="Author" ItemStyle-Width="16%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_testAuthorNameLabel_OpenText"
                                                                                                    runat="server"></asp:Label>
                                                                                                <tr>
                                                                                                    <td colspan="8">
                                                                                                        <div id="TestStatisticsInfo_questionStatisticsGridView_detailsDiv_OpenText" runat="server"
                                                                                                            style="display: none;">
                                                                                                            <table border="0" cellpadding="3" cellspacing="3" width="850px" class="table_outline_bg">
                                                                                                                <tr>
                                                                                                                    <td class="popup_question_icon">
                                                                                                                        <div style="word-wrap: break-word; white-space: normal;">
                                                                                                                        <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_questionLabel_OpenText" runat="server"
                                                                                                                            SkinID="sknLabelFieldMultiText"></asp:Label>
                                                                                                                        </div>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="td_padding_left_20">                                                                                                                         
                                                                                                                            <%--<asp:TextBox TextMode="MultiLine" ID="TestStatisticsInfo_questionStatisticsGridView_Answer_OpenText" runat="server" ReadOnly="true"></asp:TextBox>--%>
                                                                                                                            <div class="label_multi_field_text" style="height: 40px; overflow: auto; word-wrap: break-word; white-space: normal;">
                                                                                                                            <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_OpenText_AnswerLabel" runat="server" SkinID="sknLabelFieldHeaderText" Text="Answer:"></asp:Label>&nbsp;&nbsp;&nbsp; 
                                                                                                                                <asp:Literal ID="TestStatisticsInfo_questionStatisticsGridView_Answer_OpenText" runat="server" SkinID="sknMultiLineText"
                                                                                                                                    Text="Screening candidates for Yes Banking project. The candidates must have minimum 3 years experience in the field of ASP.Net UI design, Java Script, C# Programming & OOPS."></asp:Literal>
                                                                                                                            </div>
                                                                                                                        <asp:HiddenField ID="CorrectAnswerHiddenField_OpenText" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="td_padding_10">
                                                                                                                        <table border="0" cellpadding="0" cellspacing="4" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td style="width: 8%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_categoryLabel_OpenText" runat="server"
                                                                                                                                        SkinID="sknLabelFieldHeaderText" Text="Category"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 15%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_categoryValueLabel_OpenText"
                                                                                                                                        runat="server" ReadOnly="true" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 6%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_subjectLabel_OpenText" runat="server"
                                                                                                                                        SkinID="sknLabelFieldHeaderText" Text="Subject"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 20%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_subjectValueLabel_OpenText" runat="server"
                                                                                                                                        ReadOnly="true" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 8%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_testAreaLabel_OpenText" runat="server"
                                                                                                                                        SkinID="sknLabelFieldHeaderText" Text="Test Area"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 12%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_testAreaValueLabel_OpenText"
                                                                                                                                        runat="server" ReadOnly="true" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 10%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_complexityLabel_OpenText" runat="server"
                                                                                                                                        SkinID="sknLabelFieldHeaderText" Text="Complexity"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 21%">
                                                                                                                                    <asp:Label ID="TestStatisticsInfo_questionStatisticsGridView_complexityValueLabel_OpenText"
                                                                                                                                        runat="server" ReadOnly="true" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                        <%-- popup DIV --%>
                                                                                                        <a href="#SearchQuestion_focusmeLink" id="TestStatisticsInfo_focusDownLink_OpenText" runat="server">
                                                                                                        </a><a href="#" id="TestStatisticsInfo_focusmeLink_OpenText" runat="server"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                        <ajaxToolKit:TabPanel ID="TestStatisticsInfo_candidateTabPanel" runat="server">
                            <HeaderTemplate>
                                Candidate Statistics
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td class="msg_align">
                                            <asp:Label ID="TestStatisticsInfo_candidateTabPanelErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_5">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="panel_bg">
                                            <div id="TestStatisticsInfo_candidateTabPanel_searchCriteriasDiv" runat="server"
                                                style="display: block;">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td>
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="panel_inner_body_bg">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="TestStatisticsInfo_candidateTabPanel_candidateNameLabel" runat="server"
                                                                            Text="Candidate Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="TestStatisticsInfo_candidateTabPanel_candidateNameTextBox" runat="server"
                                                                                Text="" MaxLength="50"></asp:TextBox></div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="TestStatisticsInfo_candidateTabPanel_candidateNameImageButton"
                                                                                SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" /></div>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="TestStatisticsInfo_candidateTabPanel_createdByLabel" runat="server"
                                                                            Text="Test Session Creator" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="TestStatisticsInfo_candidateTabPanel_createdByTextBox" runat="server"
                                                                                Text="" MaxLength="50">
                                                                            </asp:TextBox>
                                                                            <asp:HiddenField ID="TestStatisticsInfo_candidateTabPanel_dummyCreatedBy" runat="server" />
                                                                            <asp:HiddenField ID="TestStatisticsInfo_candidateTabPanel_createdBy_HiddenField"
                                                                                runat="server" />
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="TestStatisticsInfo_candidateTabPanel_createdByImageButton" SkinID="sknbtnSearchicon"
                                                                                runat="server" ImageAlign="Middle" /></div>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="TestStatisticsInfo_candidateTabPanel_scheduledByHeadLabel" runat="server"
                                                                            Text="Test Schedule Creator" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="TestStatisticsInfo_candidateTabPanel_scheduledByTextBox" runat="server"
                                                                                MaxLength="50"></asp:TextBox></div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="TestStatisticsInfo_candidateTabPanel_scheduledByImageButton"
                                                                                SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" /></div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2" colspan="6">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="TestStatisticsInfo_candidateTabPanel_testDateHeadLabel" runat="server"
                                                                            Text="Test Date From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td style="width: 40%; padding-right: 2px">
                                                                                    <asp:TextBox ID="TestStatisticsInfo_candidateTabPanel_testDateTextBox" runat="server"
                                                                                        MaxLength="10" AutoCompleteType="None" Columns="17"></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:ImageButton ID="TestStatisticsInfo_candidateTabPanel_calendarImageButton" SkinID="sknCalendarImageButton"
                                                                                        runat="server" ImageAlign="Middle" />
                                                                                </td>
                                                                                <td>
                                                                                    <ajaxToolKit:MaskedEditExtender ID="TestStatisticsInfo_candidateTabPanel_maskedEditExtender"
                                                                                        runat="server" TargetControlID="TestStatisticsInfo_candidateTabPanel_testDateTextBox"
                                                                                        Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                        OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                        ErrorTooltipEnabled="True" />
                                                                                    <ajaxToolKit:MaskedEditValidator ID="TestStatisticsInfo_candidateTabPanel_maskedEditValidator"
                                                                                        runat="server" ControlExtender="TestStatisticsInfo_candidateTabPanel_maskedEditExtender"
                                                                                        ControlToValidate="TestStatisticsInfo_candidateTabPanel_testDateTextBox" EmptyValueMessage="Date is required"
                                                                                        InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                        EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                                    <ajaxToolKit:CalendarExtender ID="TestStatisticsInfo_candidateTabPanel_customCalendarExtender"
                                                                                        runat="server" TargetControlID="TestStatisticsInfo_candidateTabPanel_testDateTextBox"
                                                                                        CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="TestStatisticsInfo_candidateTabPanel_calendarImageButton" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="TestStatisticsInfo_candidateTabPanel_testToDateHeadLabel" runat="server"
                                                                            Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td style="width: 40%; padding-right: 2px">
                                                                                    <asp:TextBox ID="TestStatisticsInfo_candidateTabPanel_testToDateTextBox" runat="server"
                                                                                        MaxLength="10" AutoCompleteType="None" Columns="17"></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:ImageButton ID="TestStatisticsInfo_candidateTabPanel_calendarToImageButton"
                                                                                        SkinID="sknCalendarImageButton" runat="server" ImageAlign="Middle" />
                                                                                </td>
                                                                                <td>
                                                                                    <ajaxToolKit:MaskedEditExtender ID="TestStatisticsInfo_candidateTabPanel_MaskedEditExtender1"
                                                                                        runat="server" TargetControlID="TestStatisticsInfo_candidateTabPanel_testToDateTextBox"
                                                                                        Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                        OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                        ErrorTooltipEnabled="True" />
                                                                                    <ajaxToolKit:MaskedEditValidator ID="TestStatisticsInfo_candidateTabPanel_toDateMaskedEditValidator"
                                                                                        runat="server" ControlExtender="TestStatisticsInfo_candidateTabPanel_MaskedEditExtender"
                                                                                        ControlToValidate="TestStatisticsInfo_candidateTabPanel_testToDateTextBox" EmptyValueMessage="Date is required"
                                                                                        InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                        EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                                    <ajaxToolKit:CalendarExtender ID="TestStatisticsInfo_candidateTabPanel_customCalendarExtender1"
                                                                                        runat="server" TargetControlID="TestStatisticsInfo_candidateTabPanel_testToDateTextBox"
                                                                                        CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="TestStatisticsInfo_candidateTabPanel_calendarToImageButton" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2" colspan="6">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="TestStatisticsInfo_candidateTabPanel_absoluteScoreHeadLabel" runat="server"
                                                                            Text="Absolute Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <table border="0" cellpadding="1" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="TestStatisticsInfo_candidateTabPanel_absoluteScoreMinValueTextBox"
                                                                                        runat="server" MaxLength="5" Width="25" Text=""></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="TestStatisticsInfo_candidateTabPanel_absoluteScoreTextBox" runat="server"
                                                                                        MaxLength="200"></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="TestStatisticsInfo_candidateTabPanel_absoluteScoreMaxValueTextBox"
                                                                                        runat="server" MaxLength="5" Width="25" Text=""></asp:TextBox><ajaxToolKit:MultiHandleSliderExtender
                                                                                            ID="TestStatisticsInfo_candidateTabPanel_testCostMultiHandleSliderExtender" runat="server"
                                                                                            Enabled="True" HandleAnimationDuration="0.1" Maximum="10" Minimum="0" RailCssClass="slider_rail"
                                                                                            Steps="10" HandleCssClass="slider_handler" TargetControlID="TestStatisticsInfo_candidateTabPanel_absoluteScoreTextBox"
                                                                                            ShowHandleDragStyle="true" ShowHandleHoverStyle="true">
                                                                                            <MultiHandleSliderTargets>
                                                                                                <ajaxToolKit:MultiHandleSliderTarget ControlID="TestStatisticsInfo_candidateTabPanel_absoluteScoreMinValueTextBox"
                                                                                                    HandleCssClass="MultiHandleMinSliderCss" />
                                                                                                <ajaxToolKit:MultiHandleSliderTarget ControlID="TestStatisticsInfo_candidateTabPanel_absoluteScoreMaxValueTextBox"
                                                                                                    HandleCssClass="MultiHandleMaxSliderCss" />
                                                                                            </MultiHandleSliderTargets>
                                                                                        </ajaxToolKit:MultiHandleSliderExtender>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="TestStatisticsInfo_candidateTabPanel_relativeScoreHeadLabel" runat="server"
                                                                            Text="Relative Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <table border="0" cellpadding="3" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="TestStatisticsInfo_candidateTabPanel_relativeScoreMinValueTextBox"
                                                                                        runat="server" MaxLength="5" Width="25" Text=""></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="TestStatisticsInfo_candidateTabPanel_relativeScoreTextBox" runat="server"
                                                                                        MaxLength="200"></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="TestStatisticsInfo_candidateTabPanel_relativeScoreMaxValueTextBox"
                                                                                        runat="server" MaxLength="5" Width="25" Text=""></asp:TextBox><ajaxToolKit:MultiHandleSliderExtender
                                                                                            ID="TestStatisticsInfo_candidateTabPanel_relativeScoreMultiHandleSliderExtender"
                                                                                            runat="server" Enabled="True" HandleAnimationDuration="0.1" Maximum="100" Minimum="0"
                                                                                            RailCssClass="slider_rail" HandleCssClass="slider_handler" TargetControlID="TestStatisticsInfo_candidateTabPanel_relativeScoreTextBox"
                                                                                            ShowHandleDragStyle="true" ShowHandleHoverStyle="true">
                                                                                            <MultiHandleSliderTargets>
                                                                                                <ajaxToolKit:MultiHandleSliderTarget ControlID="TestStatisticsInfo_candidateTabPanel_relativeScoreMinValueTextBox"
                                                                                                    HandleCssClass="MultiHandleMinSliderCss" />
                                                                                                <ajaxToolKit:MultiHandleSliderTarget ControlID="TestStatisticsInfo_candidateTabPanel_relativeScoreMaxValueTextBox"
                                                                                                    HandleCssClass="MultiHandleMaxSliderCss" />
                                                                                            </MultiHandleSliderTargets>
                                                                                        </ajaxToolKit:MultiHandleSliderExtender>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <table border="0" cellpadding="0" cellspacing="5" align="right">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Button ID="TestStatisticsInfo_candidateTabPanel_searchButton" runat="server"
                                                                            SkinID="sknButtonId" Text="Search" OnClick="TestStatisticsInfo_candidateTabPanel_searchButton_Click" />
                                                                    </td>
                                                                    <td class="link_button">
                                                                        |
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="TestStatisticsInfo_candidateTabPanel_resetLinkButton" runat="server"
                                                                            SkinID="sknActionLinkButton" Text="Reset" OnClick="TestStatisticsInfo_candidateTabPanel_resetLinkButton_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_5">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="header_bg" align="center">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr id="TestStatisticsInfo_candidateTabPanel_searchCandidateResultsTR" runat="server">
                                                    <td style="width: 50%" align="left">
                                                        <asp:Literal ID="TestStatisticsInfo_candidateTabPanel_searchResultsLiteral" runat="server"
                                                            Text="Candidate List"></asp:Literal>&nbsp;<asp:Label ID="CandidateStatisticsControl_sortHelpLabel"
                                                                runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 50%" align="right">
                                                        <span id="TestStatisticsInfo_candidateTabPanel_searchResultsUpSpan" runat="server"
                                                            style="display: none;">
                                                            <asp:Image ID="TestStatisticsInfo_candidateTabPanel_searchResultsUpImage" runat="server"
                                                                SkinID="sknMinimizeImage" /></span><span id="TestStatisticsInfo_candidateTabPanel_searchResultsDownSpan"
                                                                    runat="server" style="display: block;"><asp:Image ID="TestStatisticsInfo_candidateTabPanel_searchResultsDownImage"
                                                                        runat="server" SkinID="sknMaximizeImage" /></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="grid_body_bg">
                                            <asp:UpdatePanel ID="TestStatistics_candidateDetailsUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr id="TestStatistics_candidateDetailsReportLinkTR" runat="server" visible="false">
                                                            <td align="right" class="td_height_20" valign="middle">
                                                                <table align="right" border="0" cellspacing="2" cellpadding="2">
                                                                    <tr>
                                                                         <td align="right">
                                                                            <asp:LinkButton ID="TestStatisticsInfo_candidateTabPanel_exportToExcelLinkButton"
                                                                                runat="server" Text="Export To Excel" SkinID="sknActionLinkButton" CommandName="Group"
                                                                                OnClick="TestStatisticsInfo_candidateTabPanel_exportToExcelLinkButton_Click" />
                                                                        </td>
                                                                        <td class="link_button" align="center">
                                                                            |
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:LinkButton ID="TestStatisticsInfo_candidateTabPanel_groupAnalysisLinkButton"
                                                                                runat="server" Text="Group Analysis Report" SkinID="sknActionLinkButton" CommandName="Group"
                                                                                OnClick="TestStatisticsInfo_candidateTabPanel_groupAnalysisReportLinkButton_Click" />
                                                                        </td>
                                                                        <td class="link_button" align="center">
                                                                            |
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:LinkButton ID="TestStatisticsInfo_candidateTabPanel_comparisonReportLinkButton"
                                                                                runat="server" Text="Comparison Report" SkinID="sknActionLinkButton" CommandName="Comparison"
                                                                                OnClick="TestStatisticsInfo_candidateTabPanel_comparisonReportLinkButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <table border="0" cellspacing="0" cellpadding="0" width="100%" style="table-layout: fixed">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="height: 200px; overflow: auto" runat="server" id="TestStatisticsInfo_candidateTabPanel_questionDiv"
                                                                                visible="false">
                                                                                <asp:GridView ID="TestStatisticsInfo_candidateTabPanel_candidateGridView" runat="server"
                                                                                    AllowSorting="true" AutoGenerateColumns="false" Width="100%" SkinID="sknWrapHeaderGrid"
                                                                                    OnSorting="TestStatisticsInfo_candidateTabPanel_candidateGridView_Sorting" 
                                                                                    OnRowDataBound="TestStatisticsInfo_candidateTabPanel_candidateGridView_RowDataBound"
                                                                                    OnRowCreated="TestStatisticsInfo_candidateTabPanel_candidateGridView_RowCreated"
                                                                                    OnRowCommand="TestStatisticsInfo_candidateTabPanel_candidateGridView_RowCommand">
                                                                                    <Columns>
                                                                                        <asp:TemplateField ItemStyle-Wrap="false">
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="TestStatisticsInfo_candidateTabPanel_candidateGridView_selectCheckBox"
                                                                                                    runat="server" />&nbsp;
                                                                                                <asp:ImageButton ID="TestStatisticsInfo_candidateTabPanel_candidateGridView_designReportImageButton"
                                                                                                    runat="server" ToolTip="Design Report" SkinID="sknDesignReportImageButton" CommandName="DesignReport"
                                                                                                    CommandArgument="<%# Container.DataItemIndex %>" />
                                                                                                <asp:HyperLink ID="TestStatisticsInfo_candidateTabPanel_candidateGridView_canidateReportHyperLinkButton"
                                                                                                    runat="server" Target="_blank" ToolTip="Candidate Test Details" 
                                                                                                    ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif">
                                                                                                </asp:HyperLink>
                                                                                                <asp:HyperLink ID="TestStatisticsInfo_candidateTabPanel_candidateGridView_trackingHyperLink"
                                                                                                 runat="server" ToolTip="Cyber Proctoring Information" Target="_blank" 
                                                                                                 ImageUrl="~/App_Themes/DefaultTheme/Images/tracking_icon.gif" 
                                                                                                 Visible='<%# IsCyberProctoring(Eval("IsCyberProctoring").ToString()) %>' >
                                                                                                </asp:HyperLink>
                                                                                                <asp:HiddenField ID="TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewCandidateSessionIDHiddenField"
                                                                                                    runat="server" Value='<%# Eval("CandidateSessionID") %>' />
                                                                                                <asp:HiddenField ID="TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewIsCyberProctoringHiddenField"
                                                                                                    runat="server" Value='<%# Eval("IsCyberProctoring") %>' />
                                                                                                <asp:HiddenField ID="TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewAttemptIDHiddenField"
                                                                                                    runat="server" Value='<%# Eval("AttemptNumber") %>' />
                                                                                                <asp:HiddenField ID="TestStatisticsInfo_candidateStatisticsTab_candidateGridViewCandidateIDHiddenField"
                                                                                                    runat="server" Value='<%# Eval("CandidateID") %>' />
                                                                                                <asp:HiddenField ID="TestStatisticsInfo_candidateStatisticsTab_candidateGridViewFirstNameHiddenField"
                                                                                                    runat="server" Value='<%# Eval("CandidateFullName") %>' />
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Width="80px" />
                                                                                        </asp:TemplateField>
                                                                                        <%--<asp:BoundField HeaderText="Candidate&nbsp;Name" DataField="FirstName" SortExpression="CANDIDATE_NAME"
                                                                                ItemStyle-Wrap="false" ItemStyle-Width="150px" />--%>
                                                                                        <asp:TemplateField HeaderText="Candidate&nbsp;Name" SortExpression="CANDIDATE_NAME"
                                                                                            ItemStyle-Wrap="false" ItemStyle-Width="150px">
                                                                                            <ItemTemplate>
                                                                                                <asp:HyperLink ID="TestStatisticsInfo_candidateStatisticsTab_candidateFullnameHyperLink" Target="_blank" SkinID="sknLabelFieldTextHyperLink"
                                                                                                    runat="server" Text='<%# Eval("FirstName") %>' ToolTip='<%# Eval("CandidateFullName") %>'></asp:HyperLink>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField HeaderText="Attempt" DataField="AttemptNumber" HeaderStyle-CssClass="grid_header_row_wrap" ItemStyle-CssClass="td_padding_right_20" SortExpression="ATTEMPT_NUMBER" HeaderStyle-Width="120px" />
                                                                                        <%--<asp:BoundField HeaderText="Test Session Creator" DataField="SessionCreator" SortExpression="TEST_SESSION_CREATOR"
                                                                                ItemStyle-Wrap="false" ItemStyle-Width="450px" HeaderStyle-CssClass="grid_header_row_wrap"
                                                                                HeaderStyle-Width="100px"></asp:BoundField>--%>
                                                                                        <asp:TemplateField HeaderText="Test Session Creator" SortExpression="TEST_SESSION_CREATOR"
                                                                                            ItemStyle-Wrap="false" ItemStyle-Width="140px" HeaderStyle-CssClass="grid_header_row_wrap"
                                                                                            HeaderStyle-Width="100px">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestStatisticsInfo_candidateStatisticsTab_testSessionCreatorFullnameLabel"
                                                                                                    runat="server" Text='<%# Eval("SessionCreator") %>' ToolTip='<%# Eval("SessionCreatorFullName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--<asp:BoundField HeaderText="Test Schedule Creator" DataField="ScheduleCreator" ItemStyle-Wrap="false"
                                                                                SortExpression="TEST_SCHEDULE_CREATOR" ItemStyle-Width="200px" HeaderStyle-CssClass="grid_header_row_wrap"
                                                                                HeaderStyle-Width="450px"></asp:BoundField>--%>
                                                                                        <asp:TemplateField HeaderText="Test Schedule Creator" ItemStyle-Wrap="false" SortExpression="TEST_SCHEDULE_CREATOR"
                                                                                            ItemStyle-Width="200px" HeaderStyle-CssClass="grid_header_row_wrap" HeaderStyle-Width="450px">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestStatisticsInfo_candidateStatisticsTab_testScheduleCreatorFullnameLabel"
                                                                                                    runat="server" Text='<%# Eval("ScheduleCreator") %>' ToolTip='<%# Eval("ScheduleCreatorFullName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Test Date" SortExpression="DATE DESC" ItemStyle-Width="120px">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestReport_testCreatedDateLabel" runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("TestDate"))) %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField HeaderText="Answered Correctly(%)" DataField="AnsweredCorrectly"
                                                                                            SortExpression="ANSWERCORECTLY DESC" HeaderStyle-CssClass="grid_header_row_wrap" ItemStyle-CssClass="td_padding_right_20"
                                                                                            ItemStyle-HorizontalAlign="right" HeaderStyle-Width="250px" ItemStyle-Width="170px" />
                                                                                        <asp:BoundField HeaderText="Total Time&nbsp;Taken" DataField="TimeTaken" SortExpression="TOTALTIMETAKEN"
                                                                                            HeaderStyle-CssClass="grid_header_row_wrap" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="left" />
                                                                                        <asp:BoundField HeaderText="Avg&nbsp;Time&nbsp;Taken (Per Question)" DataField="AverageTimeTaken"
                                                                                            SortExpression="AVGTOTALTIMETAKEN" ItemStyle-Wrap="false" HeaderStyle-CssClass="grid_header_row_wrap"
                                                                                            HeaderStyle-Width="200px" />
                                                                                        <asp:BoundField HeaderText="Absolute Score" DataField="AbsoluteScore" SortExpression="ABSOLUTE_SCORE DESC"
                                                                                            ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right"
                                                                                            HeaderStyle-CssClass="td_padding_right_20 grid_header_row_wrap" />
                                                                                        <asp:BoundField HeaderText="Relative Score" DataField="RelativeScore" SortExpression="RELATIVE_SCORE DESC"
                                                                                            ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right"
                                                                                            HeaderStyle-CssClass="td_padding_right_20 grid_header_row_wrap" />
                                                                                        <asp:BoundField HeaderText="Percentile" DataField="Percentile" SortExpression="Percentile DESC"
                                                                                            ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right"
                                                                                            HeaderStyle-CssClass="td_padding_right_20" />
                                                                                        <asp:BoundField HeaderText="Rank" DataField="RankOrder" SortExpression="RANK_ORDER ASC"
                                                                                            ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right"
                                                                                            HeaderStyle-CssClass="td_padding_right_20" />
                                                                                        <asp:BoundField HeaderText="Relative Rank" DataField="RelativeRankOrder" SortExpression="RELATIVE_RANK_ORDER ASC"
                                                                                            ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right"
                                                                                            HeaderStyle-CssClass="td_padding_right_20" />
                                                                                        <%--<asp:BoundField HeaderText="Recruiter" DataField="Recruiter" SortExpression="Recruiter"
                                                                                HeaderStyle-Width="100px" />--%>
                                                                                        <asp:TemplateField HeaderText="Recruiter" SortExpression="Recruiter" HeaderStyle-Width="100px">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestStatisticsInfo_candidateStatisticsTab_recruiterFullnameLabel"
                                                                                                    runat="server" Text='<%# Eval("ScheduleCreator") %>' ToolTip='<%# Eval("ScheduleCreatorFullName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>

                                                                                     <div style="height: 200px; overflow: auto" runat="server" id="TestStatisticsInfo_candidateTabPanel_questionDiv_OpenText"
                                                                                visible="false">
                                                                                <asp:GridView ID="TestStatisticsInfo_candidateTabPanel_candidateGridView_OpenText" runat="server"
                                                                                    AllowSorting="true" AutoGenerateColumns="false" Width="100%" SkinID="sknWrapHeaderGrid"
                                                                                    OnSorting="TestStatisticsInfo_candidateTabPanel_candidateGridView_OpenText_Sorting" 
                                                                                    OnRowDataBound="TestStatisticsInfo_candidateTabPanel_candidateGridView_OpenText_RowDataBound"
                                                                                    OnRowCreated="TestStatisticsInfo_candidateTabPanel_candidateGridView_OpenText_RowCreated"
                                                                                    OnRowCommand="TestStatisticsInfo_candidateTabPanel_candidateGridView_OpenText_RowCommand">
                                                                                    <Columns>
                                                                                    <asp:TemplateField HeaderText="S.No">
                                                                                            <ItemTemplate>
                                                                                                <%# Container.DataItemIndex + 1 %>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                            <asp:TemplateField ItemStyle-Wrap="false">
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="TestStatisticsInfo_candidateTabPanel_candidateGridView_selectCheckBox"
                                                                                                    runat="server" Visible="false" />&nbsp;
                                                                                                <asp:ImageButton ID="TestStatisticsInfo_candidateTabPanel_candidateGridView_designReportImageButton"
                                                                                                    runat="server" ToolTip="Design Report" SkinID="sknDesignReportImageButton" CommandName="DesignReport"
                                                                                                    CommandArgument="<%# Container.DataItemIndex %>" Visible="false" />
                                                                                                <asp:HyperLink ID="TestStatisticsInfo_candidateTabPanel_candidateGridView_canidateReportHyperLinkButton"
                                                                                                    runat="server" Target="_blank" ToolTip="Candidate Test Details" 
                                                                                                    ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif">
                                                                                                </asp:HyperLink>
                                                                                                <asp:HyperLink ID="TestStatisticsInfo_candidateTabPanel_candidateGridView_trackingHyperLink"
                                                                                                 runat="server" ToolTip="Cyber Proctoring Information" Target="_blank" 
                                                                                                 ImageUrl="~/App_Themes/DefaultTheme/Images/tracking_icon.gif" 
                                                                                                 Visible='<%# IsCyberProctoring(Eval("IsCyberProctoring").ToString()) %>' >
                                                                                                </asp:HyperLink>
                                                                                                <asp:HiddenField ID="TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewCandidateSessionIDHiddenField"
                                                                                                    runat="server" Value='<%# Eval("CandidateSessionID") %>' />
                                                                                                <asp:HiddenField ID="TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewIsCyberProctoringHiddenField"
                                                                                                    runat="server" Value='<%# Eval("IsCyberProctoring") %>' />
                                                                                                <asp:HiddenField ID="TestStatisticsInfo_candidateStatisticsTab_candidatesGridViewAttemptIDHiddenField"
                                                                                                    runat="server" Value='<%# Eval("AttemptNumber") %>' />
                                                                                                <asp:HiddenField ID="TestStatisticsInfo_candidateStatisticsTab_candidateGridViewCandidateIDHiddenField"
                                                                                                    runat="server" Value='<%# Eval("CandidateID") %>' />
                                                                                                <asp:HiddenField ID="TestStatisticsInfo_candidateStatisticsTab_candidateGridViewFirstNameHiddenField"
                                                                                                    runat="server" Value='<%# Eval("CandidateFullName") %>' />
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Width="80px" />
                                                                                        </asp:TemplateField>
                                                                                        <%--<asp:BoundField HeaderText="Candidate&nbsp;Name" DataField="FirstName" SortExpression="CANDIDATE_NAME"
                                                                                ItemStyle-Wrap="false" ItemStyle-Width="150px" />--%>
                                                                                        <asp:TemplateField HeaderText="Candidate&nbsp;Name" SortExpression="CANDIDATE_NAME"
                                                                                            ItemStyle-Wrap="false" ItemStyle-Width="150px">
                                                                                            <ItemTemplate>
                                                                                                <asp:HyperLink ID="TestStatisticsInfo_candidateStatisticsTab_candidateFullnameHyperLink" Target="_blank" SkinID="sknLabelFieldTextHyperLink"
                                                                                                    runat="server" Text='<%# Eval("FirstName") %>' ToolTip='<%# Eval("CandidateFullName") %>'></asp:HyperLink>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField Visible="false" HeaderText="Attempt" DataField="AttemptNumber" HeaderStyle-CssClass="grid_header_row_wrap" ItemStyle-CssClass="td_padding_right_20" SortExpression="ATTEMPT_NUMBER" HeaderStyle-Width="120px" />
                                                                                        <%--<asp:BoundField HeaderText="Test Session Creator" DataField="SessionCreator" SortExpression="TEST_SESSION_CREATOR"
                                                                                ItemStyle-Wrap="false" ItemStyle-Width="450px" HeaderStyle-CssClass="grid_header_row_wrap"
                                                                                HeaderStyle-Width="100px"></asp:BoundField>--%>
                                                                                        <asp:TemplateField HeaderText="Test Session Creator" SortExpression="TEST_SESSION_CREATOR"
                                                                                            ItemStyle-Wrap="false" ItemStyle-Width="140px" HeaderStyle-CssClass="grid_header_row_wrap"
                                                                                            HeaderStyle-Width="100px">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestStatisticsInfo_candidateStatisticsTab_testSessionCreatorFullnameLabel"
                                                                                                    runat="server" Text='<%# Eval("SessionCreator") %>' ToolTip='<%# Eval("SessionCreatorFullName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--<asp:BoundField HeaderText="Test Schedule Creator" DataField="ScheduleCreator" ItemStyle-Wrap="false"
                                                                                SortExpression="TEST_SCHEDULE_CREATOR" ItemStyle-Width="200px" HeaderStyle-CssClass="grid_header_row_wrap"
                                                                                HeaderStyle-Width="450px"></asp:BoundField>--%>
                                                                                        <asp:TemplateField HeaderText="Test Schedule Creator" ItemStyle-Wrap="false" SortExpression="TEST_SCHEDULE_CREATOR"
                                                                                            ItemStyle-Width="200px" HeaderStyle-CssClass="grid_header_row_wrap" HeaderStyle-Width="450px">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestStatisticsInfo_candidateStatisticsTab_testScheduleCreatorFullnameLabel"
                                                                                                    runat="server" Text='<%# Eval("ScheduleCreator") %>' ToolTip='<%# Eval("ScheduleCreatorFullName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Test Date" SortExpression="DATE DESC" ItemStyle-Width="120px">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestReport_testCreatedDateLabel" runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("TestDate"))) %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                         <asp:BoundField HeaderText="Score" DataField="AbsoluteScore" SortExpression="ABSOLUTE_SCORE DESC"
                                                                                            ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right"
                                                                                            HeaderStyle-CssClass="td_padding_right_20 grid_header_row_wrap" />
                                                                                        <asp:BoundField HeaderText="Score Percentage(%)" DataField="AnsweredCorrectly"
                                                                                            SortExpression="ANSWERCORECTLY DESC" HeaderStyle-CssClass="grid_header_row_wrap" ItemStyle-CssClass="td_padding_right_20"
                                                                                            ItemStyle-HorizontalAlign="right" HeaderStyle-Width="250px" ItemStyle-Width="170px" />
                                                                                        <asp:BoundField HeaderText="Total Time&nbsp;Taken" DataField="TimeTaken" SortExpression="TOTALTIMETAKEN"
                                                                                            HeaderStyle-CssClass="grid_header_row_wrap" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="left" />
                                                                                        <asp:BoundField HeaderText="Avg&nbsp;Time&nbsp;Taken (Per Question)" DataField="AverageTimeTaken"
                                                                                            SortExpression="AVGTOTALTIMETAKEN" ItemStyle-Wrap="false" HeaderStyle-CssClass="grid_header_row_wrap"
                                                                                            HeaderStyle-Width="200px" />
                                                                                       
                                                                                        <asp:BoundField HeaderText="Relative Score" DataField="RelativeScore" SortExpression="RELATIVE_SCORE DESC"
                                                                                            ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right"
                                                                                            HeaderStyle-CssClass="td_padding_right_20 grid_header_row_wrap" Visible="false" />
                                                                                        <asp:BoundField HeaderText="Percentile" DataField="Percentile" SortExpression="Percentile DESC"
                                                                                            ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right"
                                                                                            HeaderStyle-CssClass="td_padding_right_20" Visible="false" />
                                                                                        <asp:BoundField HeaderText="Rank" DataField="RankOrder" SortExpression="RANK_ORDER ASC"
                                                                                            ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right"
                                                                                            HeaderStyle-CssClass="td_padding_right_20" />
                                                                                        <asp:BoundField HeaderText="Relative Rank" DataField="RelativeRankOrder" SortExpression="RELATIVE_RANK_ORDER ASC"
                                                                                            ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right"
                                                                                            HeaderStyle-CssClass="td_padding_right_20" Visible="false"/>
                                                                                        <%--<asp:BoundField HeaderText="Recruiter" DataField="Recruiter" SortExpression="Recruiter"
                                                                                HeaderStyle-Width="100px" />--%>
                                                                                        <asp:TemplateField HeaderText="Recruiter" SortExpression="Recruiter" HeaderStyle-Width="100px">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestStatisticsInfo_candidateStatisticsTab_recruiterFullnameLabel"
                                                                                                    runat="server" Text='<%# Eval("ScheduleCreator") %>' ToolTip='<%# Eval("ScheduleCreatorFullName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>                                                                
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <uc3:PageNavigator ID="TestStatisticsInfo_candidateTabPanel_bottomPagingNavigator"
                                                                    runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="TestStatisticsInfo_candidateTabPanel_exportToExcelLinkButton" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                    </ajaxToolKit:TabContainer>
                </div>
            </td>
        </tr>
        <tr>
            <td class="header_bg" align="right" style="width: 100%;">
                <table border="0" cellpadding="0" cellspacing="2" width="100%">
                    <tr>
                        <td align="right">
                            <asp:LinkButton ID="TestStatisticsInfo_bottomCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                Text="Cancel" OnClick="ParentPageRedirect" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="TestStatisticsInfo_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="TestStatisticsInfo_bottomSuccessLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="TestStatisticsInfo_bottonErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="TestStatisticsInfo_heightHiddenField" runat="server" />
                <asp:HiddenField ID="TestStatisticsInfo_candidatesGridViewTestKeyHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
