<%@ Page Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" 
    CodeBehind="DesignReportPrint.aspx.cs" Inherits="Forte.HCM.UI.ReportCenter.DesignReportPrint"
    MaintainScrollPositionOnPostback="false"%>
<%@ Register Assembly="Kalitte.Dashboard.Framework" Namespace="Kalitte.Dashboard.Framework"
    TagPrefix="kalitte" %>
    <%@ MasterType VirtualPath="~/DefaultMaster.Master" %>
    
 <asp:Content ID="DefaultMaster_designReportContentHeader" runat="server" ContentPlaceHolderID="ContentPlaceHolderHeader" Visible="false">
 
 </asp:Content>
<asp:Content ID="DefaultMaster_designReportContent" runat="server" ContentPlaceHolderID="ctlContentPh">
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
      <kalitte:DashboardSurface ID="Surface1" runat="server">
        <PanelSettings Icon="None" Border="False" BodyBorder="False" Collapsible="False" 
        HideCollapseTool="False" AutoWidth="False" AutoHeight="False" Enabled="True" Frame="False" 
        Header="False" Padding="0" Shim="False" TitleCollapse="False" Unstyled="False" 
        HeaderDisplayMode="Always" Dragable="False" AutoScroll="False" FitLayout="False" 
        Stretch="False" Hidden="False" AutoHideTools="False"></PanelSettings>

        <WindowSettings AutoShow="False" Modal="False" Resizable="True" Maximizable="True" Minimizable="False" 
        Closable="True" Icon="None" Width="400" Height="500" Border="True" BodyBorder="True" Collapsible="False" 
        HideCollapseTool="False" AutoWidth="False" AutoHeight="False" Enabled="True" Frame="False" Header="True" 
        Padding="0" Shim="False" TitleCollapse="False" Unstyled="False" HeaderDisplayMode="Always" Dragable="True" 
        AutoScroll="True" FitLayout="False" Stretch="False" Hidden="False" AutoHideTools="False">
        </WindowSettings>
    </kalitte:DashboardSurface>
</asp:Content>

<asp:Content ID="DefaultMaster_designReportContentFooter" runat="server" ContentPlaceHolderID="ContentPlaceHolder1Footer" Visible="false">
 </asp:Content>

