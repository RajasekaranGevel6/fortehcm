﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OTMMaster.Master" AutoEventWireup="true" Inherits="Forte.HCM.UI.ReportCenter.CandidateTestDetails" CodeBehind="CandidateTestDetails.aspx.cs" %>

<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="OTMMaster_body" runat="server">
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td class="header_bg">
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Label ID="CanidateTestDetails_headerLiteral" runat="server" Text="Candidate Test Result Summary"></asp:Label>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="2" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="CanidateTestDetails_topDownloadButton" runat="server" Text="Download"
                                           ToolTip="Click here to download"   SkinID="sknButtonId" onclick="CanidateTestDetails_topDownloadButton_Click"/>
                                    </td>
                                    <td>
                                        <asp:Button ID="CanidateTestDetails_topEmailButton" OnClientClick="javascript:return ShowEmailConfirm();"
                                            runat="server" Text="Email" SkinID="sknButtonId" Visible="false" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="CanidateTestDetails_topCancelButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="CanidateTestDetails_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="CanidateTestDetails_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="4" border="0">
                    <tr>
                        <td style="width:50%">
                            <table width="100%" cellpadding="0" cellspacing="4" border="0">
                                <tr>
                                    <td style="width:30%">
                                        <asp:Label ID="Label9" runat="server" Text="Candidate Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width:70%">
                                        <asp:HyperLink ID="CanidateTestDetails_CandidateNameHyperLink" runat="server" SkinID="sknLabelFieldTextHyperLink" Target="_blank"
                                            Text="" ToolTip="Click here to view the candidate activity dashboard"></asp:HyperLink>
                                    </td>
                              
                                  
                                </tr>
                            </table>
                        </td>
                        <td align="right" style="width:50%">
                            <table width="100%" cellpadding="0" cellspacing="4" border="0">
                                <tr>
                                  <td style="width:40%" align="right">
                                        <asp:Label ID="Label11" runat="server" Text="Test Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width:60%" align="right">
                                        <asp:Label ID="CanidateTestDetails_TestNameLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="4" border="0" runat="server" id="CanidateTestDetails_MultipleChoice" visible="false">
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td style="width: 25%">
                                        <asp:Label ID="CanidateTestDetails_noOfQuestionHeadLabel" runat="server" Text="Number Of Questions"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_noOfQuestionLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td style="width: 13%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_myScoreLabel" runat="server" Text="My Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_myScoreLabelValue" runat="server" SkinID="sknLabelFieldText"
                                            Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_noOfQuestionattenedLabel" runat="server" Text="Total Questions Attended"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_noOfQuestionattenedLabelValue" runat="server"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td style="width: 13%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_absoluteScoreLabel" runat="server" Text="Absolute Score"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_absoluteScoreLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_noOfQuestionSkippedLabel" runat="server" Text="Total Questions Skipped"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_noOfQuestionSkippedLabelValue" runat="server"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td style="width: 13%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_relativeScoreLabel" runat="server" Text="Relative Score"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_relativeScoreLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_noOfQuestionAnsweredCorrectlyLabel" runat="server"
                                            Text="Questions Answered Correctly" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_noOfQuestionAnsweredCorrectlyLabelValue" runat="server"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td style="width: 13%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_percentileLabel" runat="server" Text="Percentile"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_percentileLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_noOfQuestionAnsweredWronglyLabel" runat="server"
                                            Text="Questions Answered Wrongly" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_noOfQuestionAnsweredWronglyLabelValue" runat="server"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td style="width: 13%">
                                    </td>
                                    <td style="width: 30%">
                                        <asp:Label ID="CanidateTestDetails_noOfQuestionTimeTakenLabel" runat="server" Text="Total Time Taken "
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_noOfQuestionTimeTakenLabelValue" runat="server"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_percentageQuestionsAnsweredCorrectlyLabel" runat="server"
                                            Text="Questions Answered Correctly (in %)" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_percentageQuestionsAnsweredCorrectlyLabelValue"
                                            runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td style="width: 13%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_avgTimeTakenLabel" runat="server" Text="Average Time Taken To Answer Each Question"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_avgTimeTakenLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_testStatusLabel" runat="server" Text="Test Status"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_testStatusLabelValue" runat="server" SkinID="sknLabelFieldTextBold"></asp:Label>
                                    </td>
                                    <td style="width: 13%">
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>

                            <table width="100%" cellpadding="0" cellspacing="4" border="0" runat="server" id="CanidateTestDetails_OpenText" visible="false">
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td style="width: 25%">
                                        <asp:Label ID="CanidateTestDetails_OpenText_noOfQuestionHeadLabel" runat="server" Text="Number Of Questions"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_noOfQuestionLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td style="width: 13%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_marksScoredLabel" runat="server" Text="Marks Scored" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_marksScoredLabelValue" runat="server" SkinID="sknLabelFieldText"
                                            Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_noOfQuestionattenedLabel" runat="server" Text="Total Questions Attended"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_noOfQuestionattenedLabelValue" runat="server"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td style="width: 13%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_marksLabel" runat="server" Text="Marks"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_marksLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_noOfQuestionSkippedLabel" runat="server" Text="Total Questions Skipped"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_noOfQuestionSkippedLabelValue" runat="server"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td style="width: 13%">
                                    </td>
                                    <td style="width: 30%">
                                        <asp:Label ID="CanidateTestDetails_OpenText_noOfQuestionTimeTakenLabel" runat="server" Text="Total Time Taken "
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_noOfQuestionTimeTakenLabelValue" runat="server"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_percentageQuestionsAnsweredCorrectlyLabel" runat="server"
                                            Text="Score (in %)" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_percentageQuestionsAnsweredCorrectlyLabelValue"
                                            runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td style="width: 13%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_avgTimeTakenLabel" runat="server" Text="Average Time Taken To Answer Each Question"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_avgTimeTakenLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_testStatusLabel" runat="server" Text="Test Status"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="CanidateTestDetails_OpenText_testStatusLabelValue" runat="server" SkinID="sknLabelFieldTextBold"></asp:Label>
                                    </td>
                                    <td style="width: 13%">
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" runat="server" id="CanidateTestDetails_testDrft_MultipleChoiceTable" visible="true">
                                <tr>
                                    <td>
                                        <asp:GridView ID="CanidateTestDetails_testDrftGridView" runat="server" AutoGenerateColumns="False"
                                            SkinID="sknNewGridView" OnRowDataBound="CanidateTestDetails_testDrftGridView_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                            <tr>
                                                                <td valign="middle" style="width: 5%">
                                                                    <asp:Image ID="CanidateTestDetails_questionImage" runat="server" SkinID="sknQuestionImage"
                                                                        ToolTip="Question" />
                                                                    <asp:Label ID="CanidateTestDetails_rowNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                        runat="server">
                                                                    </asp:Label>
                                                                </td>
                                                                <td colspan="6" style="width: 92%">
                                                                    <div class="label_multi_field_text">
                                                                        <asp:Literal ID="CanidateTestDetails_questionLiteral" runat="server" Text='<%# Eval("Question") %>'></asp:Literal>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label ID="CanidateTestDetails_answerQuestionLabel" runat="server" Text="Answer"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td colspan="5">
                                                                    <div class="label_multi_field_text">
                                                                        <asp:HiddenField ID="CandidateTestDetails_questionKey" runat="server" Value='<%# Eval("QuestionKey") %>' />
                                                                        <asp:PlaceHolder ID="TestStatisticsInfo_questionStatisticsGridView_answerChoicesPlaceHolder"
                                                                            runat="server"></asp:PlaceHolder>
                                                                        <asp:HiddenField ID="CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label ID="CanidateTestDetails_categoryQuestionLabel" runat="server" Text="Category"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="CanidateTestDetails_categoryQuestionTextBox" SkinID="sknLabelFieldText"
                                                                        runat="server" Text='<%# Eval("CategoryName") %>'></asp:Label>
                                                                </td>
                                                                <td style="width: 6%">
                                                                    <asp:Label ID="CanidateTestDetails_subjectQuestionLabel" runat="server" Text="Subject"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="CanidateTestDetails_subjectQuestionTextBox" runat="server" Text='<%# Eval("SubjectName") %>'
                                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label ID="CanidateTestDetails_testAreaQuestionHeadLabel" runat="server" Text="Test Area"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="CanidateTestDetails_testAreaQuestionLabel" SkinID="sknLabelFieldText"
                                                                        runat="server" Text='<%# Eval("TestAreaName") %>' />
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label ID="CanidateTestDetails_complexityQuestionHeadLabel" runat="server" Text="Complexity"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="CanidateTestDetails_complexityQuestionLabel" SkinID="sknLabelFieldText"
                                                                        runat="server" Text='<%# Eval("Complexity") %>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label runat="server" Text="Absolute Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Label SkinID="sknLabelFieldText" runat="server" Text='<%# Eval("AbsoluteScore") %>'></asp:Label>
                                                                </td>
                                                                <td style="width: 6%">
                                                                    <asp:Label runat="server" Text="Relative Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 20%">
                                                                    <asp:Label runat="server" Text='<%# Eval("RelativeScore") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label runat="server" Text="Time Taken" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Label SkinID="sknLabelFieldText" runat="server" Text='<%# ConvertSecondsToHoursMinutesSeconds(Convert.ToInt32(Eval("TimeTaken"))) %>' />
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label  runat="server" Text="Skipped" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label  SkinID="sknLabelFieldText" runat="server" Text='<%# Eval("Skipped") %>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label runat="server" Text="Question Weightage" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Label runat="server" SkinID="sknLabelFieldText" Text='<%# Eval("QuestionWeightage") %>'></asp:Label>
                                                                </td>
                                                                <td style="width: 6%">
                                                                </td>
                                                                <td style="width: 20%">
                                                                </td>
                                                                <td style="width: 8%">
                                                                </td>
                                                                <td style="width: 15%">
                                                                </td>
                                                                <td style="width: 8%">
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="grid_header" />
                                            <AlternatingRowStyle CssClass="grid_001" />
                                            <RowStyle CssClass="grid" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>

                            <table width="100%" cellpadding="0" cellspacing="0" border="0" runat="server" id="CanidateTestDetails_testDrft_OpenText_Table" visible="false">
                                <tr>
                                    <td>
                                        <asp:GridView ID="CanidateTestDetails_testDrftGridView_OpenText" runat="server" AutoGenerateColumns="False"
                                            SkinID="sknNewGridView" OnRowDataBound="CanidateTestDetails_testDrftGridView_OpenText_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                            <tr>
                                                                <td valign="middle" style="width: 5%">
                                                                    <asp:Image ID="CanidateTestDetails_questionImage" runat="server" SkinID="sknQuestionImage"
                                                                        ToolTip="Question" />
                                                                    <asp:Label ID="CanidateTestDetails_rowNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                        runat="server">
                                                                    </asp:Label>
                                                                </td>
                                                                <td colspan="6" style="width: 92%">
                                                                    <div class="label_multi_field_text">
                                                                        <asp:Literal ID="CanidateTestDetails_questionLiteral" runat="server" Text='<%# Eval("Question") %>'></asp:Literal>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label ID="CanidateTestDetails_answerQuestionLabel" runat="server" Text="Answer Reference"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td colspan="5">
                                                                    <div class="label_multi_field_text">
                                                                        <asp:HiddenField ID="CandidateTestDetails_questionKey" runat="server" Value='<%# Eval("QuestionKey") %>' />
                                                                        <asp:Literal ID="CandidateTestDetails_AnswerReference" runat="server" SkinID="sknMultiLineText"></asp:Literal><br />                                                                        
                                                                        <asp:HiddenField ID="CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label ID="Label10" runat="server" Text="Answer"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td colspan="5">
                                                                    <div class="label_multi_field_text">                                                                       
                                                                        <asp:Literal ID="CandidateTestDetails_Answer" runat="server" SkinID="sknMultiLineText"></asp:Literal>
                                                                        <asp:HiddenField ID="HiddenField2" runat="server" Value='<%# Eval("Answer") %>' />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label ID="CanidateTestDetails_categoryQuestionLabel" runat="server" Text="Category"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="CanidateTestDetails_categoryQuestionTextBox" SkinID="sknLabelFieldText"
                                                                        runat="server" Text='<%# Eval("CategoryName") %>'></asp:Label>
                                                                </td>
                                                                <td style="width: 6%">
                                                                    <asp:Label ID="CanidateTestDetails_subjectQuestionLabel" runat="server" Text="Subject"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="CanidateTestDetails_subjectQuestionTextBox" runat="server" Text='<%# Eval("SubjectName") %>'
                                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label ID="CanidateTestDetails_testAreaQuestionHeadLabel" runat="server" Text="Test Area"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="CanidateTestDetails_testAreaQuestionLabel" SkinID="sknLabelFieldText"
                                                                        runat="server" Text='<%# Eval("TestAreaName") %>' />
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label ID="CanidateTestDetails_complexityQuestionHeadLabel" runat="server" Text="Complexity"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="CanidateTestDetails_complexityQuestionLabel" SkinID="sknLabelFieldText"
                                                                        runat="server" Text='<%# Eval("Complexity") %>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label ID="Label1" runat="server" Text="Marks" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="CanidateTestDetails_MarksLabel" SkinID="sknLabelFieldText" runat="server" Text=''></asp:Label>
                                                                </td>
                                                                <td style="width: 6%">
                                                                    <asp:Label ID="Label3" runat="server" Text="Marks Obtained" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="CanidateTestDetails_MarksObtainedLabel" runat="server" Text='' SkinID="sknLabelFieldText"></asp:Label>
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label ID="Label5" runat="server" Text="Time Taken" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="Label6" SkinID="sknLabelFieldText" runat="server" Text='<%# ConvertSecondsToHoursMinutesSeconds(Convert.ToInt32(Eval("TimeTaken"))) %>' />
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Label ID="Label7"  runat="server" Text="Skipped" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Label8"  SkinID="sknLabelFieldText" runat="server" Text='<%# Eval("Skipped") %>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                               <%-- <td style="width: 8%">
                                                                    <asp:Label ID="Label10" runat="server" Text="Question Weightage" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="Label12" runat="server" SkinID="sknLabelFieldText" Text='<%# Eval("QuestionWeightage") %>'></asp:Label>
                                                                </td>--%>
                                                                <td style="width: 6%">
                                                                </td>
                                                                <td style="width: 20%">
                                                                </td>
                                                                <td style="width: 8%">
                                                                </td>
                                                                <td style="width: 15%">
                                                                </td>
                                                                <td style="width: 8%">
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="grid_header" />
                                            <AlternatingRowStyle CssClass="grid_001" />
                                            <RowStyle CssClass="grid" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="CanidateTestDetails_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="CanidateTestDetails_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="header_bg">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td style="width: 50%">
                        </td>
                        <td style="width: 50%" align="right">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="CanidateTestDetails_bottomDownloadButton" runat="server" Text="Download" OnClick="CanidateTestDetails_downloadButton_Click"
                                            SkinID="sknButtonId" />
                                    </td>
<%--                                    <td>
                                        <asp:Button ID="CanidateTestDetails_bottomEmailButton" OnClientClick="javascript:return ShowEmailConfirm();"
                                            runat="server" Text="Email" SkinID="sknButtonId" />
                                    </td>--%>
                                    <td>
                                        <asp:LinkButton ID="CanidateTestDetails_bottomCancelButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
