<%@ Page Language="C#" MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" 
    CodeBehind="DesignReport.aspx.cs" Inherits="Forte.HCM.UI.ReportCenter.DesignReport"
    MaintainScrollPositionOnPostback="false"%>

<%@ Register Src="~/CommonControls/WidgetTypesListControl.ascx" TagName="WidgetTypesListControl"
    TagPrefix="uc1" %>
<%@ Register Assembly="Kalitte.Dashboard.Framework" Namespace="Kalitte.Dashboard.Framework"
    TagPrefix="kalitte" %>
    <%@ MasterType VirtualPath="~/DefaultMaster.Master" %>
    
<asp:Content ID="DefaultMaster_designReportContent" runat="server" ContentPlaceHolderID="ctlContentPh">
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
     <asp:HiddenField ID="DesignReport_fileNameHiddenField" runat="server" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td> 
                <div style="float: left; width: 24%; height: 100%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                        <td colspan="3" class="msg_align">
                            <asp:UpdatePanel runat="server" ID="DesignReport_topMessageUpdatePanel">
                                <ContentTemplate>
                                    <asp:Label ID="DesignReport_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="DesignReport_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>    
                     </td>
                    </tr>
                        <tr>
                            <td style="width: 49%;" valign="top">   
                                <asp:UpdatePanel ID="DesignReport_widgetTypesListUpdatePanel" runat="server">
                                    <ContentTemplate>
                                        <uc1:WidgetTypesListControl ID="DesignReport_widgetTypesListControl"  runat="server" OnOkClick="DesignReport_addWidgetInstanceClick"/>
                                     </ContentTemplate>
                           </asp:UpdatePanel>
                            </td>
                            <td style="width: 2%">
                                &nbsp;
                            </td>
                            <td style="width: 49%;" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="dashboard_surface_header_bg">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="55%" class="header_text_bold">
                                                        <asp:Literal ID="DesignReport_headerLiteral" runat="server" Text="Design Report"></asp:Literal>
                                                    </td>
                                                    <td width="45%" align="right" valign="middle">
                                                      <asp:UpdatePanel runat="server" ID="DesignReport_downloadButton_secondLevelUpdatePanel">
                                                         <ContentTemplate>
                                                         <asp:Button ID="DesignReport_downloadButton" runat="server" SkinID="sknButtonId" Width="75px"
                                                            Text="Download" onclick="DesignReport_downloadButton_Click" />&nbsp;
                                                         <asp:Button ID="DesignReport_emailButton" runat="server" SkinID="sknButtonId" Text="Email" Width="65px"
                                                          onclick="DesignReport_emailButton_Click"/>&nbsp;
                                                          <asp:LinkButton ID="DesignReport_printerFriendlyLinkButton" runat="server" Text="Printer Friendly" SkinID="sknActionLinkButton"
                                                            OnClick="DesignReport_printerFriendlyLinkButton_Click" /> |
                                                          <asp:LinkButton ID="DesignReport_topResetLinkButton" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                                            OnClick="DesignReport_resetLinkButton_Click" /> |
                                                         <asp:LinkButton ID="DesignReport_topCancelLinkButton" runat="server" Text="Cancel"
                                                                    SkinID="sknActionLinkButton" OnClick="DesignReport_cancelLinkButton_Click" />
                                                        </ContentTemplate>
                                                        <Triggers>
                                                        <asp:PostBackTrigger ControlID="DesignReport_topResetLinkButton" />
                                                        <asp:PostBackTrigger  ControlID="DesignReport_topCancelLinkButton"/>
                                                        <asp:PostBackTrigger ControlID="DesignReport_downloadButton" />
                                                        </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <kalitte:DashboardSurface ID="Surface1" runat="server">
                                                <PanelSettings Icon="None" Border="False" BodyBorder="False" Collapsible="False" 
                                                HideCollapseTool="False" AutoWidth="False" AutoHeight="False" Enabled="True" Frame="False" 
                                                Header="False" Padding="0" Shim="False" TitleCollapse="False" Unstyled="False" 
                                                HeaderDisplayMode="Always" Dragable="False" AutoScroll="False" FitLayout="False" 
                                                Stretch="False" Hidden="False" AutoHideTools="False"></PanelSettings>

                                                <WindowSettings AutoShow="False" Modal="False" Resizable="True" Maximizable="True" Minimizable="False" 
                                                Closable="True" Icon="None" Width="500" Height="500" Border="True" BodyBorder="True" Collapsible="False" 
                                                HideCollapseTool="False" AutoWidth="False" AutoHeight="False" Enabled="True" Frame="False" Header="True" 
                                                Padding="0" Shim="False" TitleCollapse="False" Unstyled="False" HeaderDisplayMode="Always" Dragable="True" 
                                                AutoScroll="True" FitLayout="False" Stretch="False" Hidden="False" AutoHideTools="False">
                                                </WindowSettings>
                                            </kalitte:DashboardSurface>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                           <td colspan="3" class="msg_align">
                            <asp:UpdatePanel runat="server" ID="DesignReport_bottomMessageUpdatePanel">
                            <ContentTemplate>
                                <asp:Label ID="DesignReport_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                <asp:Label ID="DesignReport_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField ID="TestReport_authorIdHiddenField" runat="server" />
                                
                            </ContentTemplate>
                         </asp:UpdatePanel>  
                          </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>

</asp:Content>
