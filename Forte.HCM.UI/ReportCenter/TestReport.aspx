<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OTMMaster.Master"
    CodeBehind="TestReport.aspx.cs" Inherits="Forte.HCM.UI.ReportCenter.TestReport" %>

<%@ Register Src="~/CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="TestReport_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="TestReport_headerLiteral" runat="server" Text="Test Report"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="TestReport_topResetLinkButton" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                            OnClick="TestReport_resetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="TestReport_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="TestReport_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="TestReport_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="TestReport_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div id="TestReport_searchByTestDiv" runat="server" style="display: block;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="right" class="td_padding_bottom_5">
                                            <asp:UpdatePanel runat="server" ID="TestReport_simpleLinkUpdatePanel">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="TestReport_simpleLinkButton" runat="server" Text="Advanced" SkinID="sknActionLinkButton"
                                                        OnClick="TestReport_simpleLinkButton_Click" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="panel_bg">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="100%">
                                                        <asp:UpdatePanel runat="server" ID="TestReport_searchDivUpdatePanel">
                                                            <ContentTemplate>
                                                                <div id="TestReport_questionTypeDiv" runat="server">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                        <tr>
                                                                            <td style="width: 10%">
                                                                                <div style="float: left; padding-top: 4px">
                                                                                    <asp:Label ID="TestReport_questionTypeLabel" runat="server" Text="Question Type"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">*</span>
                                                                                </div>
                                                                                <div style="float: left; padding-left: 5px; padding-top: 2px">
                                                                                    <asp:DropDownList ID="TestReport_questionTypeDropDownList" SkinID="sknQuestionTypeDropDown"
                                                                                        runat="server" AutoPostBack="false" EnableViewState="true">
                                                                                        <asp:ListItem Text="Multiple Choice" Value="1"></asp:ListItem>
                                                                                        <asp:ListItem Text="Open Text" Value="2"></asp:ListItem>   
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div style="padding-top: 4px">
                                                                </div>
                                                                <div id="TestReport_simpleSearchDiv" runat="server" style="width: 100%">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="TestReport_categoryHeadLabel" runat="server" Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TestReport_categoryTextBox" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="TestReport_subjectHeadLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TestReport_subjectTextBox" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="TestReport_keywordsHeadLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <div style="float: left; padding-right: 5px;">
                                                                                    <asp:TextBox ID="TestReport_simpleKeywordTextBox" runat="server"></asp:TextBox></div>
                                                                                <div style="float: left;">
                                                                                    <asp:ImageButton ID="TestReport_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, Search_Test_KeywordHelp %>" /></div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div id="TestReport_advanceSearchDiv" runat="server" visible="false" style="width: 100%">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <uc1:CategorySubjectControl ID="TestReport_searchCategorySubjectControl" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_8">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="panel_inner_body_bg">
                                                                                <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="TestReport_testAreaHeadLabel" runat="server" Text="Test Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="7" class="checkbox_list_bg" align="left" style="width: 90%">
                                                                                            <asp:CheckBoxList ID="TestReport_testAreaCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                            </asp:CheckBoxList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="TestReport_complexityLabel" runat="server" Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="3" align="left" valign="middle">
                                                                                            <div style="float: left; padding-right: 5px; width: 90%; vertical-align: middle;"
                                                                                                class="checkbox_list_bg">
                                                                                                <asp:CheckBoxList ID="TestReport_complexityCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                    RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                </asp:CheckBoxList>
                                                                                            </div>
                                                                                            <div style="float: left; vertical-align: middle; padding-left: 5px; padding-top: 5px;">
                                                                                                <asp:ImageButton ID="TestReport_complexityImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, ComplexityHelp %>" />
                                                                                            </div>
                                                                                        </td>
                                                                                        <td colspan="4">
                                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td style="width: 50%">
                                                                                                        <asp:Label ID="TestReport_positionProfileLabel" runat="server" Text="Position Profile"
                                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                                            <asp:TextBox ID="TestReport_positionProfileTextBox" runat="server" ReadOnly="true"></asp:TextBox>
                                                                                                            <asp:HiddenField ID="TestReport_positionProfileIDHiddenField" runat="server" />
                                                                                                        </div>
                                                                                                        <div style="float: left;">
                                                                                                            <asp:ImageButton ID="TestReport_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select the position profile" />
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="TestReport_testIdLabel" runat="server" Text="Test ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="TestReport_testIdTextBox" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="TestReport_testNameLabel" runat="server" Text="Test Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="TestReport_testNameTextBox" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td style="width: 8%">
                                                                                            <asp:Label ID="TestReport_keywordHeadLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="TestReport_advancedKeywordTextBox" runat="server"></asp:TextBox></div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="TestReport_keywordImageButton" SkinID="sknHelpImageButton" runat="server"
                                                                                                    ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, Search_Test_KeywordHelp %>" /></div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="TestReport_authorHeadLabel" runat="server" Text="Author" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="TestReport_authorTextBox" runat="server"></asp:TextBox>
                                                                                                <asp:HiddenField ID="TestReport_dummyAuthorID" runat="server" />
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="TestReport_authorImageButton" SkinID="sknbtnSearchicon" runat="server"
                                                                                                    ImageAlign="Middle" ToolTip="Click here to select the test author" /></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="td_padding_top_5">
                                                        <asp:Button ID="TestReport_topSearchButton" runat="server" Text="Search" SkinID="sknButtonId"
                                                            OnClick="TestReport_topSearchButton_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr id="TestReport_searchTestResultsTR" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 98%" align="left" class="header_text_bold">
                                        <asp:Literal ID="TestReport_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                            ID="TestReport_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                    </td>
                                    <td style="width: 2%" align="right">
                                        <span id="TestReport_searchResultsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="TestReport_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                        </span><span id="TestReport_searchResultsDownSpan" runat="server" style="display: block;">
                                            <asp:Image ID="TestReport_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                        </span>
                                        <asp:HiddenField ID="TestReport_restoreHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <asp:UpdatePanel runat="server" ID="TestReport_testGridViewUpdatePanel">
                                <ContentTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left">
                                                <div style="height: 200px; overflow: auto;" runat="server" id="TestReport_testDiv"
                                                    visible="false">
                                                    <asp:GridView ID="TestReport_testGridView" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                        GridLines="Horizontal" BorderColor="White" BorderWidth="1px" Width="100%" OnSorting="TestReport_testGridView_Sorting"
                                                        OnRowDataBound="TestReport_testGridView_RowDataBound" OnRowCreated="TestReport_testGridView_RowCreated"
                                                        OnRowCommand="TestReport_testGridView_RowCommand">
                                                        <RowStyle CssClass="grid_alternate_row" />
                                                        <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                        <HeaderStyle CssClass="grid_header_row" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Test&nbsp;ID" SortExpression="TESTKEY">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="TestReport_testIdLinkButton" CommandName="TestStatisticsInfo"
                                                                        runat="server" Text='<%# Eval("TestKey") %>'>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="9%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Test Name" SortExpression="TESTNAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TestReport_testNameLabel" runat="server" Text='<%# TrimContent(Eval("Name").ToString(),50) %>'
                                                                        ToolTip='<%# Eval("Name") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="35%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Created Date" SortExpression="CREATEDDATE DESC" ItemStyle-Width="60px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TestReport_testCreatedDateLabel" runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("CreatedDate"))) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Author" SortExpression="USERNAME" ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TestReport_testAuthorFullNameLabel" runat="server" Text='<%# Eval("TestAuthorName") %>'
                                                                        ToolTip='<%# Eval("TestAuthorFullName") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="12%" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderStyle-CssClass="td_padding_right_20" HeaderText="No of Questions"
                                                                DataField="NoOfQuestions" SortExpression="TOTALQUESTION DESC">
                                                                <ItemStyle Width="1%" CssClass="td_padding_right_20" HorizontalAlign="right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField HeaderText="Cost (in&nbsp;$)" HeaderStyle-CssClass="td_padding_right_20"
                                                                DataField="TestCost" SortExpression="TESTCOST" Visible="false">
                                                                <ItemStyle Width="1%" CssClass="td_padding_right_20" HorizontalAlign="right" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Certification Test" SortExpression="CERTIFICATION" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TestReport_testCertificationLabel" runat="server" Text='<%# GetExpansion(Convert.ToBoolean(Eval("IsCertification")))%>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="10%" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <uc2:PageNavigator ID="TestReport_bottomPagingNavigator" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="TestReport_topSearchButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="TestReport_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="TestReport_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label><asp:Label
                            ID="TestReport_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                                ID="TestReport_authorIdHiddenField" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%">
                            &nbsp;
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="TestReport_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="TestReport_resetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="TestReport_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
