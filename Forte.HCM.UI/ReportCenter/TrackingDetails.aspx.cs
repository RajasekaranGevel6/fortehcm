﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SerachQuestion.cs
// File that represents the user Serach the Question by various Key filed.
// This will helps Search a question From the question repository.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.ReportCenter
{
    public partial class TrackingDetails : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="int"/> that holds the current page number of  desktop ThumbImage tab page
        /// /// </summary>
        private int desktopThumbImagepageNo;

        /// <summary>
        /// A <see cref="int"/> that holds the current page number of  Webcam ThumbImage tab page
        /// /// </summary>
        private int webCamThumbImagepageNo;

        /// <summary>
        /// A <see cref="int"/> that holds the Candidate session key
        /// /// </summary>
        private string canSessionKey = string.Empty;

        /// <summary>
        /// A <see cref="int"/> that holds the Candidate cuurent attemp id
        /// /// </summary>
        private int attempID = 0;

        #endregion Private Variables                                           

        #region Event Handler                                                  

        /// <summary>
        /// Handler will get fired when the page is loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SubscriptEvents();
                GetQueryString();

                Master.SetPageCaption("Cyber Proctoring Information");

                if (!IsPostBack)
                {
                    desktopThumbImagepageNo = 1;
                    webCamThumbImagepageNo = 1;
                    Session["TrackingDetailsDesktopThumbImagePageNo"] = desktopThumbImagepageNo;
                    Session["TrackingDetailsWebCamImagePageNo"] = webCamThumbImagepageNo;
                    LoadDesktopThumbnails(desktopThumbImagepageNo);
                    LoadWebcamThumbnails(webCamThumbImagepageNo);
                    LoadApplicationLogs(1);
                    LoadOperatingSystemEvents(1);
                    LoadCyberProctorExceptions(1);
                    LoadRunningApplications(1);
                }
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp);
                base.ShowMessage(TrackingDetails_bottomErrorMessageLabel, exp.Message);
                base.ShowMessage(TrackingDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of application logs section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void TrackingDetails_applicationLogsPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadApplicationLogs(e.PageNumber);
            }
            catch (Exception exp)
            {
                base.ShowMessage(TrackingDetails_topErrorMessageLabel,
                    TrackingDetails_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of running applications section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void TrackingDetails_runningApplicationsPageNavigator_PageNumberClick
            (object sender,PageNumberEventArgs e)
        {
            try
            {
                LoadRunningApplications(e.PageNumber);
            }
            catch (Exception exp)
            {
                base.ShowMessage(TrackingDetails_topErrorMessageLabel,
                    TrackingDetails_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of exceptions section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void TrackingDetails_exceptionPageNavigator_PageNumberClick
            (object sender,PageNumberEventArgs e)
        {
            try
            {
                LoadCyberProctorExceptions(e.PageNumber);
            }
            catch (Exception exp)
            {
                base.ShowMessage(TrackingDetails_topErrorMessageLabel,
                    TrackingDetails_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of operating system events section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void TrackingDetails_operatingSystemEventsPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadOperatingSystemEvents(e.PageNumber);
            }
            catch (Exception exp)
            {
                base.ShowMessage(TrackingDetails_topErrorMessageLabel,
                    TrackingDetails_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handle the Click Event of the TrackingDetails_DesktopNextImageButton
        /// this binds the cyberproctoring desktop thumnail images to the Datalist
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/></param>
        /// <param name="e">
        /// A<see cref="ImageClickEventArgs"/></param>
        protected void TrackingDetails_desktopCaptureNextImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            try
            {
                desktopThumbImagepageNo = (int)Session["TrackingDetailsDesktopThumbImagePageNo"];
                desktopThumbImagepageNo = desktopThumbImagepageNo + 1;
                Session["TrackingDetailsDesktopThumbImagePageNo"] = desktopThumbImagepageNo;
                LoadDesktopThumbnails(desktopThumbImagepageNo);
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp);
                base.ShowMessage(TrackingDetails_bottomErrorMessageLabel, exp.Message);
                base.ShowMessage(TrackingDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handle the Click Event of the TrackingDetails_DescktopPrevImageButton
        /// this binds the cyberproctoring desktop thumnail images to the Datalist
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/></param>
        /// <param name="e">
        /// A<see cref="ImageClickEventArgs"/></param>
        protected void TrackingDetails_desktopCapturePreviousImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            try
            {
                desktopThumbImagepageNo = (int)Session["TrackingDetailsDesktopThumbImagePageNo"];
                desktopThumbImagepageNo = desktopThumbImagepageNo - 1;
                Session["TrackingDetailsDesktopThumbImagePageNo"] = desktopThumbImagepageNo;
                LoadDesktopThumbnails(desktopThumbImagepageNo);
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp);
                base.ShowMessage(TrackingDetails_bottomErrorMessageLabel, exp.Message);
                base.ShowMessage(TrackingDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handle the Click Event of the TrackingDetails_webcamNextImageButton
        /// this binds the cyberproctoring webcam thumnail images to the Datalist
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/></param>
        /// <param name="e">
        /// A<see cref="ImageClickEventArgs"/></param>
        protected void TrackingDetails_webcamCaptureNextImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            try
            {
                webCamThumbImagepageNo = (int)Session["TrackingDetailsWebCamImagePageNo"];
                webCamThumbImagepageNo = webCamThumbImagepageNo + 1;
                Session["TrackingDetailsWebCamImagePageNo"] = webCamThumbImagepageNo;
                LoadWebcamThumbnails(webCamThumbImagepageNo);
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp);
                base.ShowMessage(TrackingDetails_bottomErrorMessageLabel, exp.Message);
                base.ShowMessage(TrackingDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handle the Click Event of the TrackingDetails_webcamPrevImageButton
        /// this binds the cyberproctoring webcam thumnail images to the Datalist
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/></param>
        /// <param name="e">
        /// A<see cref="ImageClickEventArgs"/></param>
        protected void TrackingDetails_webcamCapturePreviousImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            try
            {
                webCamThumbImagepageNo = (int)Session["TrackingDetailsWebCamImagePageNo"];
                webCamThumbImagepageNo = webCamThumbImagepageNo - 1;
                Session["TrackingDetailsWebCamImagePageNo"] = webCamThumbImagepageNo;
                LoadWebcamThumbnails(webCamThumbImagepageNo);
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp);
                base.ShowMessage(TrackingDetails_bottomErrorMessageLabel, exp.Message);
                base.ShowMessage(TrackingDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handle the item databount event of the TrackingDetails_imageDatalist 
        /// is add the onclick attribute for the TrackingDetails_ScreenShotImageButton 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/></param>
        /// <param name="e">
        /// A<see cref="DataListItemEventArgs"/></param>
        protected void TrackingDetails_desktopCaptureDatalist_ItemDataBound
            (object sender, DataListItemEventArgs e)
        {
            Image img = null;
            int deskTopPageIndex = 0;
            try
            {
                img = ((Image)e.Item.FindControl("TrackingDetails_desktopCaptureImage"));

                if (img != null)
                {
                    desktopThumbImagepageNo = (int)Session["TrackingDetailsDesktopThumbImagePageNo"];
                    int.TryParse(e.Item.ItemIndex.ToString(), out deskTopPageIndex);

                    img.Attributes.Add("onclick", "javascript:return OpenScreenShot('CP','" +
                        (deskTopPageIndex + ((desktopThumbImagepageNo - 1) * base.GridPageSize)).ToString() 
                        + "','SCREEN_IMG','" + canSessionKey +"','"+ attempID + "');");
                }
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp);
                base.ShowMessage(TrackingDetails_bottomErrorMessageLabel, exp.Message);
                base.ShowMessage(TrackingDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handle the item databount event of the TrackingDetails_WebcamimageDatalist 
        /// is add the onclick attribute for the TrackingDetails_WebcamImage 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/></param>
        /// <param name="e">
        /// A<see cref="DataListItemEventArgs"/></param>
        protected void TrackingDetails_webcamCaptureDataList_ItemDataBound
            (object sender, DataListItemEventArgs e)
        {
            Image img = null;
            int WebCamItemIndex = 0;
            try
            {
                img = ((Image)e.Item.FindControl("TrackingDetails_webcamCaptureImage"));
                if (img != null)
                {
                    webCamThumbImagepageNo = (int)Session["TrackingDetailsWebCamImagePageNo"];
                    int.TryParse(e.Item.ItemIndex.ToString(), out WebCamItemIndex);

                    img.Attributes.Add("onclick", "javascript:return OpenScreenShot('CP','" + (WebCamItemIndex +
                        ((webCamThumbImagepageNo - 1) * base.GridPageSize)).ToString() + "','USER_IMG','" + canSessionKey+"','"+attempID+"');");
                }
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp);
                base.ShowMessage(TrackingDetails_bottomErrorMessageLabel, exp.Message);
                base.ShowMessage(TrackingDetails_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handler                                               

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the application logs.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadApplicationLogs(int pageNumber)
        {
            int totalRecords = 0;

            List<SystemLogDetail> applicationLogs = new TrackingBLManager().
                GetCyberProctorApplicationLogs(canSessionKey, attempID, pageNumber,
                 base.GridPageSize, out totalRecords);

            TrackingDetails_applicationLogsPageNavigator.PageSize = base.GridPageSize;
            TrackingDetails_applicationLogsPageNavigator.TotalRecords = totalRecords;

            if (applicationLogs == null || applicationLogs.Count == 0)
            {
                TrackingDetails_applicationLogsGridView.DataSource = null;
                TrackingDetails_applicationLogsGridView.DataBind();
                TrackingDetails_applicationLogsDiv.Visible = false;
                return;
            }
            TrackingDetails_applicationLogsDiv.Visible = true;
            TrackingDetails_applicationLogsGridView.DataSource = applicationLogs;
            TrackingDetails_applicationLogsGridView.DataBind();
        }

        /// <summary>
        /// To Load CyberProctor Events 
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/></param>
        private void LoadOperatingSystemEvents(int pageNumber)
        {
            int totalRecords = 0;

            List<ApplicationEventDetail> cyberProctorEvents = new TrackingBLManager().
                GetCyberProctorEvents(canSessionKey, attempID, pageNumber, 
                base.GridPageSize, out totalRecords);

            TrackingDetails_operatingSystemEventsPageNavigator.PageSize = base.GridPageSize;
            TrackingDetails_operatingSystemEventsPageNavigator.TotalRecords = totalRecords;

            if (cyberProctorEvents == null || cyberProctorEvents.Count == 0)
            {
                TrackingDetails_operatingSystemEventsGridView.DataSource = null;
                TrackingDetails_operatingSystemEventsGridView.DataBind();
                TrackingDetails_operatingSystemEventsDiv.Visible = false;
                return;
            }
            TrackingDetails_operatingSystemEventsDiv.Visible = true;
            TrackingDetails_operatingSystemEventsGridView.DataSource = cyberProctorEvents;
            TrackingDetails_operatingSystemEventsGridView.DataBind();
        }

        /// <summary>
        /// To Load CyberProctor Exceptions
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/></param>
        private void LoadCyberProctorExceptions(int pageNumber)
        {
            int totalRecords = 0;
            List<ApplicationExceptionDetail> cyberProctorExceptions = new TrackingBLManager().
                GetCyberProctorExceptions(canSessionKey, attempID, pageNumber, base.GridPageSize,
                out totalRecords);

            TrackingDetails_exceptionsPageNavigator.PageSize = base.GridPageSize;
            TrackingDetails_exceptionsPageNavigator.TotalRecords = totalRecords;

            if (cyberProctorExceptions == null || cyberProctorExceptions.Count == 0)
            {
                TrackingDetails_exceptionsGridView.DataSource = cyberProctorExceptions;
                TrackingDetails_exceptionsGridView.DataBind();
                TrackingDetails_exceptionsGridView.Visible = true;
                return;
            }

            TrackingDetails_exceptionsGridView.Visible = true;
            TrackingDetails_exceptionsGridView.DataSource = cyberProctorExceptions;
            TrackingDetails_exceptionsGridView.DataBind();
        }

        /// <summary>
        /// to Load Desktop Thumbnails
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/></param>
        private void LoadDesktopThumbnails(int pageNumber)
        {
            int totalRec = 0;
            List<TrackingImageDetail> cyberProctorDesktopImages = null;
            cyberProctorDesktopImages = new TrackingBLManager().GetCyberProctorImage(
                canSessionKey, attempID, "SCREEN_IMG", pageNumber, base.GridPageSize, out totalRec);

            TrackingDetails_desktopCaptureDatalist.DataSource = cyberProctorDesktopImages;
            TrackingDetails_desktopCaptureDatalist.DataBind();

            Session["cyberProctorDesktopImages"] = cyberProctorDesktopImages;
           
            if (pageNumber > 1)
                TrackingDetails_desktopCapturePreviousImageButton.Visible = true;
            else
                TrackingDetails_desktopCapturePreviousImageButton.Visible = false;

            if ((pageNumber * base.GridPageSize) >= totalRec)
                TrackingDetails_desktopCaptureNextImageButton.Visible = false;
            else
                TrackingDetails_desktopCaptureNextImageButton.Visible = true;

        }

        /// <summary>
        /// to Load Webcam Thumbnails
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/></param>
        private void LoadWebcamThumbnails(int pageNumber)
        {
            int totalRec = 0;
            List<TrackingImageDetail> cyberProctorDesktopImages = new TrackingBLManager().GetCyberProctorImage(
                canSessionKey, attempID, "USER_IMG", pageNumber, base.GridPageSize, out totalRec);

            TrackingDetails_webcamCaptureDataList.DataSource = cyberProctorDesktopImages;
            TrackingDetails_webcamCaptureDataList.DataBind();

            Session["cyberWebCamDesktopImages"] = cyberProctorDesktopImages;

            if (pageNumber > 1)
                TrackingDetails_webcamCapturePreviousImageButton.Visible = true;
            else
                TrackingDetails_webcamCapturePreviousImageButton.Visible = false;

            if ((pageNumber *base.GridPageSize) >= totalRec)
                TrackingDetails_webcamCaptureNextImageButton.Visible = false;
            else
                TrackingDetails_webcamCaptureNextImageButton.Visible = true;

        }
        
        /// <summary>
        /// to Load Existing Applications
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/></param>
        private void LoadRunningApplications(int pageNumber)
        {
            int totalRecords = 0;
            List<ApplicationDetail> cyberProctorExistApplications = null;
            cyberProctorExistApplications = new TrackingBLManager().GetCyberProctorExistApplications(
                canSessionKey, attempID, pageNumber, base.GridPageSize, out totalRecords);

            TrackingDetails_runningApplicationsPageNavigator.PageSize = base.GridPageSize;
            TrackingDetails_runningApplicationsPageNavigator.TotalRecords = totalRecords;
            if (cyberProctorExistApplications == null || cyberProctorExistApplications.Count == 0)
            {
                TrackingDetails_runningApplicationsGridView.DataSource = null;
                TrackingDetails_runningApplicationsGridView.DataBind();
                TrackingDetails_runningApplicationsDiv.Visible = false;
                return;
            }

            TrackingDetails_runningApplicationsDiv.Visible = true;
            TrackingDetails_runningApplicationsGridView.DataSource = cyberProctorExistApplications;
            TrackingDetails_runningApplicationsGridView.DataBind();
        }
        
        /// <summary>
        /// to Subscript the Events
        /// </summary>
        private void SubscriptEvents()
        {
            TrackingDetails_applicationLogsPageNavigator.PageNumberClick +=
                new PageNavigator.PageNumberClickEventHandler
                (TrackingDetails_applicationLogsPageNavigator_PageNumberClick);
            TrackingDetails_operatingSystemEventsPageNavigator.PageNumberClick +=
                new PageNavigator.PageNumberClickEventHandler
                (TrackingDetails_operatingSystemEventsPageNavigator_PageNumberClick);
            TrackingDetails_exceptionsPageNavigator.PageNumberClick += 
                new PageNavigator.PageNumberClickEventHandler(
                TrackingDetails_exceptionPageNavigator_PageNumberClick);
            TrackingDetails_runningApplicationsPageNavigator.PageNumberClick +=
                new PageNavigator.PageNumberClickEventHandler(
                TrackingDetails_runningApplicationsPageNavigator_PageNumberClick);
        }
        
        /// <summary>
        /// to read the query string
        /// </summary>
        private void GetQueryString()
        {
            if (Request.QueryString["candidatesession"] == null)
                throw new Exception(Resources.HCMResource.TrackingDetails_QueryStringNotFound);
            canSessionKey = canSessionKey = Request.QueryString["candidatesession"];
            if (Request.QueryString["attemptid"] == null)
                throw new Exception(Resources.HCMResource.TrackingDetails_QueryStringNotFound);
            int.TryParse(Request.QueryString["attemptid"], out attempID);
        }

        #endregion Private Methods                                             

        #region Overridden Methods                                             

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Overridden Methods                                          
    }
}
