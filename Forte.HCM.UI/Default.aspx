﻿

<%@ Page Title="ForteHCM" Language="C#" MasterPageFile="~/Signout.Master" AutoEventWireup="true" 
CodeBehind="Default.aspx.cs" Inherits="Forte.HCM.UI.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript" src="JQuery/jquery.validate.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            //for form validation

             $("#aspnetForm").validate({
                rules: {
                    <%=ForgotPassword_userNameTextBox.UniqueID %>: {
                        minlength: 2,
                        required: true
                    }
                }, messages: {
                    <%=ForgotPassword_userNameTextBox.UniqueID %>:{
                        required: "* Required Field *",
                        minlength: "* Please enter atleast 2 characters *"
                    }
               }
            });

           
            //select all the a tag with name equal to modal
            $('a[name=modal]').click(function (e) {
                //Cancel the link behavior
                e.preventDefault();

                //Get the A tag
                var id = $(this).attr('href');

                //Get the screen height and width
                var maskHeight = $(document).height();
                var maskWidth = $(window).width();

                //Set heigth and width to mask to fill up the whole screen
                $('#mask').css({ 'width': maskWidth, 'height': maskHeight });

                //transition effect		
                $('#mask').fadeIn(1000);
                $('#mask').fadeTo("slow", 0.8);

                //Get the window height and width
                var winH = $(window).height();
                var winW = $(window).width();

                //Set the popup window to center
                $(id).css('top', winH / 2 - $(id).height() / 2);
                $(id).css('left', winW / 2 - $(id).width() / 2);

                //transition effect
                $(id).fadeIn(2000);

            });

            //if close button is clicked
            $('.window .close').click(function (e) {
                //Cancel the link behavior
                e.preventDefault();

                $('#mask').hide();
                $('.window').hide();
            });

            //if mask is clicked
            $('#mask').click(function () {
                $(this).hide();
                $('.window').hide();
            });

            $(window).resize(function () {
                var box = $('#boxes .window');

                //Get the screen height and width
                var maskHeight = $(document).height();
                var maskWidth = $(window).width();

                //Set height and width to mask to fill up the whole screen
                $('#mask').css({ 'width': maskWidth, 'height': maskHeight });

                //Get the window height and width
                var winH = $(window).height();
                var winW = $(window).width();

                //Set the popup window to center
                box.css('top', winH / 2 - box.height() / 2);
                box.css('left', winW / 2 - box.width() / 2);
            });
        });
    </script>
    <style>
        body
        {
            font-family: verdana;
            font-size: 35px;
        }
        #mask
        {
            position: absolute;
            left: 0;
            top: 0;
            z-index: 9000;
            background-color: #000;
            display: none;
        }
        #boxes .window
        {
            position: fixed;
            left: 0;
            top: 0;
            width: 340px;
            height: 150px;
            display: none;
            z-index: 9999;
            padding: 20px;
        }
        #boxes #dialog
        {
            width: 375px;
            height: 130px;
            padding: 10px;
            background-position: top;
            background-repeat:no-repeat;
            repeat-x scroll left bottom #e9ecee;
            border: 1px solid #DFDCDC;
            border-radius: 15px 15px 15px 15px;
            background: url(images/pp_workflow_panel_bg.png);
            float: left;
            background-color:#ffffff;
        }
        .popup_bg
        {
            width: 150px;
            height: 50px;
            padding: 10px;
            background-color: #ffffff;
        }
        .link_text
        {
            font-size: 11px !important;
            color: #148EC0 !important;
            font-weight: bold !important;
            text-decoration: None;
        }

        .link_text:hover
        {
            text-decoration: Underline;
        }
        
        .grid_title
        {
            font-size: 11px !important;
            font-weight: bold !important;
            white-space: pre-line !important;
            color: #33424B !important;
            font-weight: bold !important;
            border-bottom: 0px solid #D1D8DA !important;
            text-align: left !important;
            padding-left: 5px;
            height: 24px;
            padding:0px 0px 0px 0px;
        }

         .label_title
        {
            font-size: 10px !important;
            font-weight: bold !important;
            white-space: pre-line !important;
            color: #33424B !important;
            font-weight: normal !important;
            border-bottom: 0px solid #D1D8DA !important;
            text-align: left !important;
            padding-left: 5px;
            height: 24px;
            padding:0px 0px 0px 0px;
        }


        .forgot_password_textbox { 
            padding: 5px;
            border: solid 1px #E5E5E5;
            outline: 0;
            font: normal 13px/100% Verdana, Tahoma, sans-serif;
            width: 230px;
            background: #FFFFFF url('bg_form.png') left top repeat-x;
            background: -webkit-gradient(linear, left top, left 25, from(#FFFFFF), color-stop(4%, #EEEEEE), to(#FFFFFF));
            background: -moz-linear-gradient(top, #FFFFFF, #EEEEEE 1px, #FFFFFF 25px);
            box-shadow: rgba(0,0,0, 0.1) 0px 0px 8px;
            -moz-box-shadow: rgba(0,0,0, 0.1) 0px 0px 8px;
            -webkit-box-shadow: rgba(0,0,0, 0.1) 0px 0px 8px;
        }

        .forgot_password_textbox:hover, .forgot_password_textbox:focus
        {
            border-color: #C9C9C9; 
            -webkit-box-shadow: rgba(0, 0, 0, 0.15) 0px 0px 8px;
        }

    </style>
    <div>
        <div id="main_container">
            <div id="header_outer">
                <div id="header_inner">
                    <div id="header_left_logo">
                        <a href="default.aspx">
                            <img src="Images/logo.png" alt="Forte HCM" title="FORTE HCM" width="123" height="69"
                                border="0" /></a></div>
                    <div id="header_right">
                        <!-- customized login script starts here -->
                        <div class="ddpanel">
                            <div id="mypanelcontent" class="ddpanelcontent">
                                <div id="loginpadding_top">
                                </div>
                                <div id="login_title">
                                    Login</div>
                                <div id="login_form_outer"> 

                                    <div id="login_form_username">
                                        User Name:</div>
                                    <div id="login_form_texbox">
                                        <asp:TextBox ID="Index_userNameTextBox" runat="server" MaxLength="50"
                                          CssClass="login_form_texbox_inner"   Height="11px"></asp:TextBox> 
                                       
                                    </div>
                                    <div id="login_form_password">
                                        Password:</div>
                                    <div id="login_form_texbox">
                                        <asp:TextBox ID="Index_passwordTextBox" runat="server" MaxLength="50" TextMode="Password"
                                            CssClass="login_form_texbox_inner" Height="11px"></asp:TextBox> 
                                    </div>
                                    <div id="login_form_btn">
                                        <asp:Button ID="Index_loginButton" runat="server" Text="Login"  
                                            CssClass="login_btn_bg" BackColor="Transparent" BorderStyle="None" 
                                            onclick="Index_loginButton_Click" />
                                    </div>
                                    <div id="error_msg">
                                        <asp:Label ID="Index_loginErrorLabel" runat="server" Text=""></asp:Label>
                                    </div> 
                                </div>
                                <div id="login_Remember">
                                    <div style="float: left">
                                        <asp:CheckBox ID="HomePage_rememberMeCheckBox" runat="server" Text="Remember me"
                                            CssClass="username_Remember" TabIndex="15" />
                                    </div>
                                    <div style="float: left;margin-top:3px;">
                                        <a href="#dialog" name="modal" style="color:White;padding-left:20px;">Forgot Password</a>
                                    </div>
                                    <!-- Forgot Password -->
                                    <div>
                                        <div style="font-size: 10px; color: #ccc">
                                        </div>
                                        <div id="boxes">
                                            <div class="signin_middle_bg">
                                            <div id="dialog" class="window">
                                                <div style="text-align: right">
                                                    <a href="#" class="close" style="color:#148EC0;">Close</a></div>
                                                <div style="padding-left:30px">
                                                    <div>
                                                        <div class="signin_left_panel">
                                                            <div class="signin_signup_ex_user_line_bottom">
                                                                <asp:Label runat="server" ID="ForgotPassword_titleLabel" CssClass="grid_title"
                                                                    Text="Forgot Password"></asp:Label>
                                                            </div>
                                                            <div style="height: 30px"></div>
                                                            <div class="signin_form_container">
                                                                <div style="float:left;width:80px;">
                                                                    <asp:Label runat="server" ID="ForgotPassword_userNameLabel" Text="User Name" CssClass="label_title"></asp:Label>&nbsp;<span class="mandatory">*</span>
                                                                </div>
                                                                <div style="float:left;width:150px;">
                                                                    <asp:TextBox ID="ForgotPassword_userNameTextBox" Width="200px" MaxLength="50" CssClass="forgot_password_textbox"  runat="server"
                                                                        ToolTip="Enter user name" onFocus="javascript:this.select()"></asp:TextBox>
                                                                </div>
                                                                <div style="height: 40px"></div>
                                                                <div style="clear:both;padding-left:80px">
                                                                    <asp:Button ID="ForgotPassword_sendPasswordButton" runat="server" SkinID="sknButtonId"
                                                                        OnClick="ForgotPassword_sendPasswordButton_Click" Text="Send" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Mask to cover the whole screen -->
                                            <div id="mask">
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Password -->
                                </div>
                            </div>
                            
                            <div id="login_btn_outer">
                                <div id="mypaneltab" class="ddpaneltab">
                                    <a href="#"><span></span></a>
                                </div>
                                <%--<div id="mypaneltab" class="ddpaneltab_free_trail">
                                    <a href="https://www.forte-hcm.com/Signup.aspx"><span></span></a>
                                </div>--%>
                            </div>
                        </div>
                        <!-- customized login script ends here -->
                        <div id="index_main_menu">
                            <ul>
                                <li><a href="#" class="active">Home</a></li>
                                <li><%--<a href="https://www.forte-hcm.com/ResourceSelection.aspx">--%>Resource Selection<%--</a>--%></li>
                                <li><%--<a href="https://www.forte-hcm.com/Assessment.aspx">--%>Assessment <%--</a>--%></li>
                                <li><%--<a href="https://www.forte-hcm.com/EnterpriseTalent.aspx">--%>Enterprise Talent<%--</a>--%></li>
                                <li><%--<a href="https://www.forte-hcm.com/RPO.aspx">--%>RPO<%--</a>--%></li>
                                <li><%--<a href="https://www.forte-hcm.com/Staffing.aspx">--%>Staffing<%--</a>--%></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="header_banner_outer">
                    <!-- header banner slider starts here -->
                    <div id="slider">
                        <div id="bg_color">
                            <div id="container">
                                <div id="example"> 
                                    <div id="slides">
                                        <div class="slides_container">
                                            <div class="slide">
                                                <img src="Images/slide-1.png" width="900" height="340" title="Objective Hiring! Made Simple"
                                                    alt="Objective Hiring! Made Simple" border="0"> 
                                            </div>

			                                <div align="center" class="slide"> 
                                                <div>
                                                    <object width="560" height="315">
                                                        <param name="movie" value="https://www.youtube.com/v/k0l3_kyQAxY?version=3&amp;hl=en_US">
                                                        </param>
                                                        <param name="allowFullScreen" value="true"></param>
                                                        <param name="allowscriptaccess" value="always"></param>
                                                        <embed src="https://www.youtube.com/v/k0l3_kyQAxY?version=3&amp;hl=en_US" type="application/x-shockwave-flash"
                                                            width="560" height="315" allowscriptaccess="always" allowfullscreen="true"></embed></object>
                                                </div> 
                                                <div align="right" class="iframe_video_txt">
                                                    Play the video and place your mouse pointer over the video to view full video
                                                </div>
                                            </div>

                                            <div class="slide">
                                                <%--<a href="https://www.forte-hcm.com/ResourceSelection.aspx" title="Talent Search">--%>
                                                    <img src="Images/slide-2.png" width="900" height="340" alt="Talent Search"
                                                        border="0" title="Talent Search"><%--</a> --%>
                                            </div>
                                            <div class="slide">
                                                <%--<a href="https://www.forte-hcm.com/Assessment.aspx" title="Assessment">--%>
                                                    <img src="Images/slide-3.png" width="900" height="340" title="Assessment"
                                                        alt="Assessment"><%--</a> --%>
                                            </div>
                                            <div class="slide">
                                                <%--<a href="https://www.forte-hcm.com/Reporting.aspx" title="Reporting">--%>
                                                    <img src="Images/slide-4.png" width="900" height="340" title="Reporting"
                                                        alt="Reporting"><%--</a> --%>
                                            </div>
                                            <div class="slide">
                                                <img src="Images/slide-5.png" title="Talent Infrastructure" width="900"
                                                    height="340" alt="Talent Infrastructure"> 
                                            </div> 
                                        </div>
                                        <a href="#" class="prev">
                                            <img src="Images/arrow-prev.png" width="24" height="43" alt="Arrow Prev"
                                                border="0"></a> <a href="#" class="next">
                                                    <img src="Images/arrow-next.png" width="24" height="43" alt="Arrow Next"
                                                        border="0"></a>
                                    </div> 
                                </div> 
                            </div>
                        </div>
                    </div> 
                    <!-- header banner slider ends here -->
                </div>
            </div>
            <div id="middle_bg">
                <%--<div id="middle_btn_outer">
                    <div id="middle_btn_inner">
                        <a href="https://www.forte-hcm.com/Workflow.aspx">
                            <img src="Images/btn_view.png" width="236" title="Learn More" alt="Learn More" height="57"
                                border="0" /></a></div>
                    <div id="middle_btn_inner_right">
                        <a href="https://www.forte-hcm.com/Signup.aspx">
                            <img src="Images/btn_sign.png" width="236" height="57" title="Sign Up Today!" alt="Sign Up Today!"
                                border="0" /></a></div>
 
                </div>--%>
            </div>
            <div id="content_area">
                <!--content resource area start here-->
                <div id="cnt_outer">
                    <div id="cnt_left_outer">
                        <div id="cnt_left_outer_title">
                            <%--<a href="https://www.forte-hcm.com/ResourceSelection.aspx">--%>Resource Selection<%--</a>--%></div>
                        <div id="resource_cnt">
                            <%--<a href="https://www.forte-hcm.com/ResourceSelection.aspx">--%>
                                <img src="Images/icon_resource.png" name="resource_left_icon" border="0" align="left"
                                    id="resource_left_icon" title="Resource Selection" alt="Resource Selection" /><%--</a>--%>A
                            talent mining tool that integrates with assessment data & engages users to search
                            based on position profile or weights; compare candidate profiles, view standardized
                            & ranked results.
                        </div>
                        <%--<div align="right">
                            <a href="https://www.forte-hcm.com/ResourceSelection.aspx">
                                <img src="Images/btn_readmore.png" title="Read More" width="78" height="24" border="0" /></a></div>--%>
                    </div>
                    <div id="cnt_left_outer_border">
                        &nbsp;</div>
                    <div id="cnt_left_outer">
                        <div id="ass_title">
                            <%--<a href="https://www.forte-hcm.com/Assessment.aspx">--%>Assessment<%--</a>--%></div>
                        <div id="resource_cnt">
                            <%--<a href="https://www.forte-hcm.com/Assessment.aspx">--%>
                                <img src="Images/icon_Assessment.png" name="resource_left_icon" width="66" height="66"
                                    border="0" align="left" id="resource_left_icon" title="Assessment" alt="Assessment" /><%--</a>--%>An
                            assessment platform with a multitude of features like adaptive test authoring, customized
                            tests, cyber- proctoring, collaborative interview, granular scoring &amp; performance
                            reporting across tests.
                        </div>
                        <%--<div align="right">
                            <a href="https://www.forte-hcm.com/Assessment.aspx">
                                <img src="Images/btn_readmore.png" title="Read More" width="78" height="24" border="0" /></a></div>--%>
                    </div>
                    <div id="cnt_left_outer_border">
                        &nbsp;</div>
                    <div id="cnt_left_outer">
                        <div id="rpo_title">
                            <%--<a href="https://www.forte-hcm.com/Reporting.aspx">--%>Reporting<%--</a>--%></div>
                        <div id="resource_cnt">
                            <%--<a href="https://www.forte-hcm.com/Reporting.aspx">--%>
                                <img src="Images/icon_Reporting.png" name="resource_left_icon" width="66" height="66"
                                    border="0" align="left" id="resource_left_icon" title="Reporting" alt="Reporting" /><%--</a>--%>Custom
                            reports that add value in the decision making process with features like candidate
                            compare & group analysis: used to compare candidate performances in a specific test
                            or generate reports that are representative of the combined performances of the
                            select group.
                        </div>
                        <%--<div align="right" id="w280">
                            <a href="https://www.forte-hcm.com/Reporting.aspx">
                                <img src="Images/btn_readmore.png" title="Read More" width="78" height="24" border="0" /></a></div>--%>
                    </div>
                </div>
                <!--content resource area End here-->
                <div id="cnt_outer">
                    <div>
                        <div id="Something_About_cnt">
                            <div id="about_title">
                                <%--<a href="https://www.forte-hcm.com/AboutUs.aspx">--%>Something About Us<%--</a>--%></div>
                            <div id="Something_About_inner">
                                <%--<a href="https://www.forte-hcm.com/AboutUs.aspx">--%>
                                    <img src="Images/icon_Something.png" width="67" height="66" border="0" align="left"
                                        id="Something_About_left" title="Something About Us" alt="Something About Us" /><%--</a>--%>We
                                are a Talent acquisition infrastructure company. In addition to helping our clients
                                build a qualified talent funnel, our patent pending platform with features like
                                contextual search, proactive build, customizable assessments &amp; cyber proctoring
                                puts objectivity back in hiring!&nbsp;
                            </div>
                            <%--<div align="right">
                                <a href="https://www.forte-hcm.com/AboutUs.aspx">
                                    <img src="Images/btn_readmore.png" width="78" title="Read More" height="24" border="0" /></a></div>--%>
                        </div>
                        <div id="cnt_left_outer_border">
                        </div>
                        <div id="Download_outer">
                            <div id="download_title">
                                <%--<a href="https://www.forte-hcm.com/Downloads.aspx">--%>Download<%--</a>--%>s</div>
                            <div id="Download_inner">
                                <%--<a href="https://www.forte-hcm.com/Downloads.aspx">--%>
                                    <img src="Images/icon_Download.png" name="Download_inner_left" width="67" height="66"
                                        border="0" align="left" id="Download_inner_left" title="Downloads" alt="Downloads" /><%--</a>--%>
                                Learn more about our products and benefits.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="offer_strip_outer">
            <div id="offer_strip_inner">
                <div id="offer_strip_inner_left">
                    Qualified talent pool for Enterprises, RPO's, System Integrators, Staffing firms
                    & YOU!</div>
            </div>
            <div id="arrow">
            </div>
        </div>
        <div id="footer_main_outer">
            <div id="footer_strip">
                <div id="footer_inner">
                    <div id="footer_inner_top">
                        <div id="footer_left">
                            <div id="about_team_title">
                                <%--<a href="https://www.forte-hcm.com/RecentEvents.aspx">--%>Recent Events<%--</a>--%></div>
                            <div id="footer_left_inner">
                                <div id="footer_right_arrow">
                                   ForteHCM to present at LEHRN  
                                    <br>
                                    (Leading Edge Human Resource Network)...</div>
                            </div>
                            <%--<div>
                                <a href="https://www.forte-hcm.com/RecentEvents.aspx">Read more</a></div>--%>
                        </div>
                        <div id="about_team">
                            <div id="about_team_title">
                                <%--<a href="https://www.forte-hcm.com/AboutUs.aspx">--%>About Us<%--</a>--%></div>
                            <div id="about_team_inner_cnt">
                                We are devoted to helping our clients make informed choices when it comes to hiring...</div>
                           <%-- <div>
                                <a href="https://www.forte-hcm.com/AboutUs.aspx">Read more</a></div>--%>
                        </div>
                        <div id="Pricing_team">
                            <div id="about_team_title">
                                <%--<a href="https://www.forte-hcm.com/FeaturesAndPricing.aspx">--%>Features and Pricing<%--</a>--%></div>
                            <div id="Pricing_team_inner_cnt">
                                Learn about our account types,<br>
                                features and pricing…</div>
                            <%--<div>
                                <a href="https://www.forte-hcm.com/FeaturesAndPricing.aspx">Read more</a></div>--%>
                        </div>
                    </div>
                    <div id="footer_inner_bottom">
                        <div id="Partnership_team">
                            <div id="about_team_title">
                                <%--<a href="https://www.forte-hcm.com/Partnership.aspx">--%>Partnership<%--</a>--%></div>
                            <div id="Partnership_team_inner_cnt">
                                Partnering with ForteHCM can help you<br />
                                to increase revenue, build your brand...</div>
                            <%--<div>
                                <a href="https://www.forte-hcm.com/Partnership.aspx">Read more</a></div>--%>
                        </div>
                        <div id="contact_team">
                            <div id="contact_team_title">
                                <%--<a href="https://www.forte-hcm.com/ContactUs.aspx">--%>Contact Us<%--</a>--%></div>
                            <div id="contact_team_inner_cnt">
                                <b>Forte HCM Inc</b><br>
                                2500 W. Higgins Rd, #870,
                                <br>
                                Hoffman Estates, IL- 60169<br>
                                <img src="Images/icon_telephone.png">: 877-377-9681
                                <img src="Images/icon_fax.png">: 847-841-3796<br>
                                <img src="Images/icon_mail.png">: <a href="mailto:hello@forte-hcm.com">hello@forte-hcm.com</a>
                            </div>
                        </div>
                        <div id="contact_team">
                            <div id="contact_team_title">
                                <%--<a href="https://www.forte-hcm.com/Whitepaper.aspx">--%>Whitepaper<%--</a>--%></div>
                            <div id="contact_team_inner_cnt">                                
                               Towards a New State in Talent <br />Acquisition                              
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--<div id="twitter_bottom_outer">
            <div id="twitter_bottom_inner">
                <div id="twitter_bottom_inner_left">
                    Follow us on :</div>
                <div id="twitter_bottom_inner_right">
                    <a href="https://twitter.com/#!/ForteHCM" target="_blank">
                        <img src="Images/icon_twitter.png" title="Twitter" alt="Twitter" width="27" height="21"
                            border="0"></a><a href="RSSFile.xml"><img src="Images/icon_reader.png" title="Rss Reader"
                                alt="Rss Reader" width="28" height="22" border="0"></a><a href="https://www.linkedin.com/company/forte-hcm-inc"
                                    target="_blank"><img src="Images/linkedin.png" alt="Linkedin" width="22" height="22"
                                        border="0" title="Linkedin"></a></div>
            </div>
        </div>--%>
        <div id="footer_bottom">
            <div id="footer_bottom_inner">
                <div id="footer_bottom_inner_left">
                    <ul>
                        <li><a href="#" class="active">Home</a></li>
                        <li><%--<a href="https://www.forte-hcm.com/ResourceSelection.aspx">--%>Resource Selection<%--</a>--%></li>
                        <li><%--<a href="https://www.forte-hcm.com/Assessment.aspx">--%>Assessment <%--</a>--%></li>
                        <li><%--<a href="https://www.forte-hcm.com/EnterpriseTalent.aspx">--%>Enterprise Talent<%--</a>--%></li>
                        <li><%--<a href="https://www.forte-hcm.com/RPO.aspx">--%>RPO<%--</a>--%></li>
                        <li><%--<a href="https://www.forte-hcm.com/Staffing.aspx">--%>Staffing<%--</a>--%></li>
                    </ul>
                </div> 
            </div>
        </div>
    </div>
</asp:Content>
