﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashboardSample.aspx.cs"
    Inherits="Forte.HCM.UI.DashboardSample" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Simple Accordion</title>
    <style type="text/css">
        .accordion
        {
            width: 400px;
        }
        
        .accordionHeader
        {
            border: 1px solid #2F4F4F;
            color: white;
            background-color: #2E4d7B;
            font-family: Arial, Sans-Serif;
            font-size: 12px;
            font-weight: bold;
            padding: 5px;
            margin-top: 5px;
            cursor: pointer;
        }
        
        .accordionHeaderSelected
        {
            border: 1px solid #2F4F4F;
            color: white;
            background-color: #5078B3;
            font-family: Arial, Sans-Serif;
            font-size: 12px;
            font-weight: bold;
            padding: 5px;
            margin-top: 5px;
            cursor: pointer;
        }
        
        .accordionContent
        {
            background-color: #D3DEEF;
            border: 1px dashed #2F4F4F;
            border-top: none;
            padding: 5px;
            padding-top: 10px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <asp:Accordion ID="Accordion1" CssClass="accordion" HeaderCssClass="accordionHeader"
            HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent"
            runat="server">
            <Panes>
                <asp:AccordionPane ID="AccordionPane1" runat="server">
                    <Header>
                        <asp:ImageButton runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/pp_leftIcon.png" />
                        Create Position Profile</Header>
                </asp:AccordionPane>
                <asp:AccordionPane ID="AccordionPane2" runat="server">
                    <Header>
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/db_TS_left_icon.png" />
                        Weighted Search</Header>
                </asp:AccordionPane>

                <asp:AccordionPane ID="AccordionPane3" runat="server">
                    <Header>
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/db_test_left_icon.png" />
                        Assessment</Header>
                    <Content>
                         <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/db_test_left_icon.png" /><asp:LinkButton runat="server" ID="l1" Text="Upload Content"></asp:LinkButton><br />
                        <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/db_test_left_icon.png" /> <asp:LinkButton runat="server" ID="LinkButton1" Text="Create Test"></asp:LinkButton><br />
                         <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/db_test_left_icon.png" /><asp:LinkButton runat="server" ID="LinkButton2" Text="Search Test/ Test Session"></asp:LinkButton><br />
                         <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/db_test_left_icon.png" /><asp:LinkButton runat="server" ID="LinkButton3" Text="Schedule Test"></asp:LinkButton>
                    </Content>
                </asp:AccordionPane>
                <asp:AccordionPane ID="AccordionPane4" runat="server">
                    <Header>
                    <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/pp_leftIcon.png" />
                        View Reports</Header>
                </asp:AccordionPane>
            </Panes>
        </asp:Accordion>
    </div>
    <div style="margin-left: 40px">
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <asp:TreeView ExpandDepth="0" runat="server" ID="Dashboard_TreeView" NodeStyle-BackColor="DarkBlue"
            NodeStyle-BorderWidth="2" NodeStyle-ForeColor="White" NodeStyle-NodeSpacing="1"
            NodeStyle-Width="250px">
            <HoverNodeStyle Font-Underline="False" ForeColor="Red" />
            <Nodes>
                <asp:TreeNode Text="Create Position Profile" ImageUrl="~/App_Themes/DefaultTheme/Images/pp_leftIcon.png">
                </asp:TreeNode>
                <asp:TreeNode Text="Weighted Search" ImageUrl="~/App_Themes/DefaultTheme/Images/db_TS_left_icon.png">
                </asp:TreeNode>
                <asp:TreeNode Text="Assessment" ImageUrl="~/App_Themes/DefaultTheme/Images/db_test_left_icon.png">
                    <asp:TreeNode ImageUrl="~/App_Themes/DefaultTheme/Images/01_dashboard_cand_stats.gif"
                        Text="Upload Content"></asp:TreeNode>
                    <asp:TreeNode ImageUrl="~/App_Themes/DefaultTheme/Images/01_dashboard_cand_stats.gif"
                        Text="Create Test"></asp:TreeNode>
                    <asp:TreeNode ImageUrl="~/App_Themes/DefaultTheme/Images/01_dashboard_cand_stats.gif"
                        Text="Search test/Test Session"></asp:TreeNode>
                    <asp:TreeNode ImageUrl="~/App_Themes/DefaultTheme/Images/01_dashboard_cand_stats.gif"
                        Text="Schedule Test"></asp:TreeNode>
                </asp:TreeNode>
                <asp:TreeNode Text="View Reports" ImageUrl="~/App_Themes/DefaultTheme/Images/db_report_left_icon.png">
                </asp:TreeNode>
            </Nodes>
        </asp:TreeView>
    </div>
    </form>
</body>
</html>
