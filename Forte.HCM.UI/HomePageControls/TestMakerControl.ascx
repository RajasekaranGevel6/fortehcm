﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestMakerControl.ascx.cs"
    Inherits="Forte.HCM.UI.HomePageControls.TestMakerControl" %>
<table style="width: 100%" border="1">
    <tr>
        <td style="width: 30%">
            Logo
        </td>
        <td rowspan="2" style="width: 70%">
            Manual composition of tests enables the Test Author to customize an entire test
            by giving him/her the ability to choose each question that will form the test. In
            addition, the screen will provide for a feature named ‘System Picks’ built around
            the concept of ‘Adaptive Question Recommendation’, where the system will make real-time
            question recommendations to the Test Author based on the questions already picked
            by the Test Author.
            <br />
            <br />
            Automated generation of test enables a Test Author to specify broad term attributes
            (such as 'time', 'complexity', 'credits cost'), along with a breakdown of category/subject
            area distribution (with optional search terms) to the system, and get it to generate
            a test based on the specified attributes.
        </td>
    </tr>
    <tr>
        <td>
            Test Maker is intended to be used by ‘Test Authors’ to, 1. Create Tests using questions
            from the repository 2. View and edit existing tests 3. Create test sessions
        </td>
    </tr>
</table>
