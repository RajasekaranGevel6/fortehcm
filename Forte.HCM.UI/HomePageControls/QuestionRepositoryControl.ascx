﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuestionRepositoryControl.ascx.cs" Inherits="Forte.HCM.UI.HomePageControls.QuestionRepositoryControl" %>
<table style="width: 100%" border="1">
    <tr>
        <td style="width: 30%" class="home_question_icon">
        </td>
        <td rowspan="2" style="width: 70%">
            <asp:Literal ID="QuestionRepositoryControl_messageLiteral" runat="server"
                Text="The Question Repository is the sub-module that enrolled Question Authors will use to </br>enter/edit/delete questions into/from the Assessment Module’s question repository. </br>On this sub-module, Question Authors will also be able to track question usage, </br>generate usage reports, and review credits they have earned for usage of their questions.">
            </asp:Literal>
        </td>
    </tr>
    <tr>
        <td>
        </td>
    </tr>
</table>