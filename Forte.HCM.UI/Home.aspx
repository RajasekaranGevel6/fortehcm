﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Forte.HCM.UI.Home"
    Theme="DefaultTheme" MasterPageFile="~/MasterPages/HomeMaster.Master" %>
<%@ Register Assembly="System.Web.Silverlight" Namespace="System.Web.UI.SilverlightControls"
    TagPrefix="aspq" %>
<%@ MasterType VirtualPath="~/MasterPages/HomeMaster.Master" %>
<asp:Content ID="ModuleHOme_bodyContent" ContentPlaceHolderID="HomeMaster_body"
    runat="server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="height: 100%">
        <tr>
            <td style="height: 100%" valign="top">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" style="height: 100%">
                    <tr>
                        <td class="banner_home_bg">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td align="center" style="width: 60%; background-color:#f1f5f8" >
                                                    <aspq:Silverlight ID="SL" runat="server" Width="100%" Height="225px" 
                                                        Source="~/Forte.HCM.ImageRotation/Forte.HCM.ImageRotation.xap" 
                                                        Windowless="true">
                                                    </aspq:Silverlight>
                                                </td>
                                                <td align="right" valign="bottom" style="width: 20%">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="shade_fortehcm" style="width: 16px; table-layout: fixed">
                                                                &nbsp;
                                                            </td>
                                                            <td class="rightside_bg_fortehcm">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td style="padding-top: 28px"  >
                                                                            <asp:ImageButton ID="HomePage_tryImageButton" runat="server" 
                                                                                SkinID="sknHomePageTryImageButton" onclick="HomePage_tryImageButton_Click"  />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="HomePage_contactImageButton" runat="server" SkinID="sknHomePageContactImageButton" PostBackUrl="~/General/ContactSales.aspx" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="HomePage_tourImageButton" runat="server" SkinID="sknHomePageTourImageButton" PostBackUrl="~/General/QuickTour.aspx" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="padding_top_bottom">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td style="width: 25%; height: 150px" class="contentbox_right">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td valign="top" style="height: 130px">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="body_header">
                                                                Integration & Collaboration
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="body_content">
                                                                FORTE HCM IS' is the platform
                                                                <br />
                                                                that automates the business<br />
                                                                operations of FORTE HCM,
                                                                <br />
                                                                enables information sharing<br />
                                                                with employees and clients,
                                                                <br />
                                                                and enhances ...
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="middle" style="padding-top: 10px">
                                                    <asp:Image ID="HomePage_integrationImage" runat="server" ImageAlign="Right" SkinID="sknHomePageIntegrationImage" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="line_thin" colspan="2">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="right" valign="top">
                                                    <asp:LinkButton ID="HomePage_moreLinkButton" runat="server" Text="Learn More" CssClass="more_btn"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 25%" class="contentbox_center">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td valign="top" style="height: 130px">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="body_header">
                                                                Candidate Analytics
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="body_content">
                                                                FORTE HCM IS' is the platform
                                                                <br />
                                                                that automates the business<br />
                                                                operations of FORTE HCM,
                                                                <br />
                                                                enables information sharing<br />
                                                                with employees and clients,
                                                                <br />
                                                                and enhances ...
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="middle" style="padding-top: 10px">
                                                    <asp:Image ID="HomePage_candidateImage" runat="server" ImageAlign="Right" SkinID="sknHomePageCandidateAnalyticImage" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="line_thin" colspan="2">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="right" valign="top">
                                                    <asp:LinkButton ID="HomePage_candidateMoreLinkButton" runat="server" Text="Learn More"
                                                        CssClass="more_btn"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 25%" class="contentbox_left">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td valign="top" style="height: 130px">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="body_header">
                                                                News
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="body_content">
                                                                FORTE HCM IS' is the platform
                                                                <br />
                                                                that automates the business<br />
                                                                operations of FORTE HCM,
                                                                <br />
                                                                enables information sharing<br />
                                                                with employees and clients,
                                                                <br />
                                                                and enhances ...
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="middle" style="padding-top: 10px">
                                                    <asp:Image ID="HomePage_newsImage" runat="server" ImageAlign="Right" SkinID="sknHomePageNewsImage" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="line_thin" colspan="2">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="right" valign="top">
                                                    <asp:LinkButton ID="HomePage_newsMoreLinkButton" runat="server" Text="Learn More"
                                                        CssClass="more_btn"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
        
</asp:Content>
