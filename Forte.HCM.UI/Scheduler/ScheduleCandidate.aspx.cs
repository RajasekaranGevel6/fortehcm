﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ScheduleCandidate.aspx.cs
// This page allows the recruiter to schedule the test to candidates. 
// They can reschedule, schedule, unschedule the candidate session tests.

#endregion Header

#region Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Resources;


#endregion Directives

namespace Forte.HCM.UI.Scheduler
{
    /// <summary>
    /// This page allows the recruiter to schedule the test to candidates. 
    /// They can reschedule, schedule, unschedule the candidate session tests.
    /// </summary>
    public partial class ScheduleCandidate : PageBase
    {
        #region Private Members

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "100%";
        private string _tsid = string.Empty;

        #endregion Private Members

        #region Event Handlers

        /// <summary>
        /// Handler that will call when a page is loaded. During that time,
        /// it'll take the responsibility to implement the default settings
        /// such as page title, default button, and calling javascript
        /// functions etc.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set page title
                Master.SetPageCaption("Schedule Candidate");
                CheckAndSetExpandorRestore();

                // Set default button.
                Page.Form.DefaultButton = ScheduleCandidate_showButton.UniqueID;
                UserDetail userDetail = new UserDetail();
                userDetail = (UserDetail)Session["USER_DETAIL"];

                // MDM added
                ScheduleCandidate_testSessionImageButton.Visible = true;
                ///MDM 
                //if (userDetail != null && userDetail.Roles != null)
                //{
                //    if (userDetail.Roles.Exists(item => item != UserRole.Recruiter))
                //        ScheduleCandidate_testSessionImageButton.Visible = true;
                //}

                if (!Page.IsPostBack)
                {
                    LoadValues();

                    // Set default focus
                    ScheduleCandidate_testSessionIdTextBox.Focus();

                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU
                         || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_LANDING))
                    {
                        base.ClearSearchCriteriaSession();
                    }
                    else
                    {
                        // If any querystring is passed(i.e. testsessionid)
                        if (!Utility.IsNullOrEmpty(Request.QueryString["testsessionid"]))
                        {
                            _tsid = Convert.ToString(Request.QueryString["testsessionid"]);

                            // If test session id is passed in Querystring, then display all the informations
                            // with respect to testSessionId.
                            ScheduleCandidate_testSessionIdTextBox.Text = _tsid;
                            ScheduleCandidate_testDetailsDiv.Style["display"] = "block";
                            ScheduleCandidate_testDetailsGridDiv.Style["display"] = "block";
                            ScheduleCandidate_candidateSessionHeader.Style["display"] = "block";
                            ScheduleCandidate_bottomScheduleButtonTable.Style["display"] = "block";


                            // Load the candidate session details grid.
                            LoadTestAndCandidateSessions();
                        }
                        else
                        {
                            if (Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE] != null)
                            {
                                // if redirected from any child page, fill the search criteria
                                // and apply the search.
                                FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE]
                                    as TestScheduleSearchCriteria);

                                // Reload the maximize hidden value.
                                CheckAndSetExpandorRestore();
                            }
                        }
                    }

                    // Load the search test session popup automatically, when 
                    // navigating from talentscout.
                    // Check if page is navigated from talentscout and position profile is found
                    if (Request.QueryString["parentpage"] != null &&
                        Request.QueryString["parentpage"].Trim().ToUpper() == Constants.ParentPage.TALENT_SCOUT &&
                        Request.QueryString["positionprofileid"] != null)
                    {
                        // Get position profile ID.
                        int positionProfileID = 0;
                        if (int.TryParse(Request.QueryString["positionprofileid"].Trim(), out positionProfileID) == true)
                        {
                            // Load the position profile popup.
                            ScriptManager.RegisterStartupScript
                                (this, this.GetType(), "LoadTestSessionWithPositionProfile",
                                "javascript: LoadTestSessionWithPositionProfile('"
                                + ScheduleCandidate_testSessionIdTextBox.ClientID + "','" +
                                positionProfileID + "')", true);
                        }
                    }
                }

                if (ScheduleCandidate_expiryDateLabel.Text != null && ScheduleCandidate_expiryDateLabel.Text != "")
                {
                    int result = DateTime.Compare(Convert.ToDateTime(ScheduleCandidate_expiryDateLabel.Text), DateTime.Now.Add(new TimeSpan(23, 59, 59)));
                    if (result >= 0)
                    {
                        ScheduleCandidate_bottomScheduleButtonTable.Style["display"] = "block";

                        ScheduleCandidate_Popup_createCandidateButton.Attributes.Add("onclick",
                          "return ShowCandidatePopup('" + ScheduleCandidate_Popup_candidateNameTextBox.ClientID + "','"
                          + ScheduleCandidate_Popup_emailTextBox.ClientID + "','"
                          + ScheduleCandidate_candidateIdHiddenField.ClientID + "')");

                        ScheduleCandidate_bottomBulkScheduleCandidateLinkButton.Attributes.Add("onclick",
                       "return ShowBulkScheduleCandidatePopup('" + ScheduleCandidate_testSessionIdTextBox.Text + "','"
                       + ScheduleCandidate_expiryDateLabel.Text + "','"
                       + ScheduleCandidate_Popup_candidateNameTextBox.ClientID + "','"
                       + ScheduleCandidate_Popup_emailTextBox.ClientID + "','" + ScheduleCandidate_candidateIdHiddenField.ClientID + "')");
                    }
                    else
                    {
                        ScheduleCandidate_bottomScheduleButtonTable.Style["display"] = "none";
                    }
                }

                // Clear the error and success messages on every postback.
                ScheduleCandidate_bottomErrorMessageLabel.Text =
                    ScheduleCandidate_bottomSuccessMessageLabel.Text =
                    ScheduleCandidate_topErrorMessageLabel.Text =
                    ScheduleCandidate_topSuccessMessageLabel.Text = "";
                // ScheduleCandidate_Popup_createCandidateButton.Attributes.Add("onclick",
                // "return ShowCandidatePopup('" + ScheduleCandidate_Popup_candidateNameTextBox.ClientID + "','" + ScheduleCandidate_Popup_emailTextBox.ClientID + "','" + ScheduleCandidate_candidateIdHiddenField.ClientID + "')");

                // ScheduleCandidate_bottomBulkScheduleCandidateLinkButton.Attributes.Add("onclick",
                //"return ShowBulkScheduleCandidatePopup('" + ScheduleCandidate_testSessionIdTextBox.Text + "','" + ScheduleCandidate_expiryDateLabel.Text + "','" + ScheduleCandidate_Popup_candidateNameTextBox.ClientID + "','" + ScheduleCandidate_Popup_emailTextBox.ClientID + "','" + ScheduleCandidate_candidateIdHiddenField.ClientID + "')");

            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);

                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// While unscheduling the candidate, it asks the user to confirm. Once he confirms to 
        /// unschedule, it triggers the below method to specify the button type 'Yes'/'No'
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ConfirmMessageEventArgs"/> that contains the event data.
        /// </param>
        /// <remarks>
        /// This parameter has a property ButtonType which specify the Actiontype
        /// </remarks>
        void ScheduleCandidate_inactiveConfirmMsgControl_ConfirmMessageThrown
            (object sender, ConfirmMessageEventArgs e)
        {
            try
            {
                if (e.ButtonType == Forte.HCM.DataObjects.ButtonType.Yes)
                {
                    string candidateSessionId = ViewState["CANDIDATE_SESSIONID"].ToString();
                    string attemptId = ViewState["ATTEMPT_ID"].ToString();

                    bool isMailSent = true;

                    // Update the session status as 'Not Scheduled'
                    new TestConductionBLManager().UpdateSessionStatus
                        (candidateSessionId, Convert.ToInt32(attemptId),
                        Constants.CandidateAttemptStatus.NOT_SCHEDULED,
                        Constants.CandidateSessionStatus.NOT_SCHEDULED, base.userID, out isMailSent);

                    // TODO : Instead of loading the whole grid, update only that row.
                    //LoadValues();
                    LoadTestAndCandidateSessions();
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);

                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Clicking on unschedule will open the popup window to get the 
        /// confirmation from user. Selecting Ok button will unschedule the candidate.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleCandidate_OkClick(object sender, EventArgs e)
        {
            try
            {
                string candidateSessionId = ViewState["CANDIDATE_SESSIONID"].ToString();
                int attemptId = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());

                bool isMailSent = true;
                bool isUnscheduled = true;

                // Get candidate details for the given candidate test session id and attempt id.
                CandidateTestSessionDetail candidateTestSessionDetail = new TestSchedulerBLManager().
                         GetCandidateTestSession(candidateSessionId, attemptId);

                // Update the session status as 'Not Scheduled'
                new TestConductionBLManager().UpdateSessionStatus(candidateTestSessionDetail,
                     Constants.CandidateAttemptStatus.NOT_SCHEDULED,
                     Constants.CandidateSessionStatus.NOT_SCHEDULED,
                     base.userID, isUnscheduled, out isMailSent);

                // Update candidate activity log. Handled in the trigger[TRUPDATE_SESSION_CANDIDATE]
                //if (candidateTestSessionDetail.CandidateID != null && candidateTestSessionDetail.CandidateID.Trim().Length > 0)
                //{
                //    new CommonBLManager().  InsertCandidateActivityLog
                //        (0, Convert.ToInt32(candidateTestSessionDetail.CandidateID), "Unscheduled from test", base.userID, 
                //        Constants.CandidateActivityLogType.CANDIDATE_UNSCHEDULED);
                //}

                if (isMailSent == false)
                {
                    ScheduleCandidate_topErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    ScheduleCandidate_bottomErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                }

                // TODO : Instead of loading the whole grid, update only that row.
                LoadTestAndCandidateSessions();
                base.ShowMessage(ScheduleCandidate_topSuccessMessageLabel,
                         ScheduleCandidate_bottomSuccessMessageLabel,
                         Resources.HCMResource.ScheduleCandidate_UnScheduledStatus);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This handler gets triggered on clicking the reset button.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleCandidate_resetLinkButton_Click
            (object sender, EventArgs e)
        {
            Response.Redirect("ScheduleCandidate.aspx?m=2&s=1", false);
        }

        /// <summary>
        /// Show button will display the test session details and its corresponding 
        /// candidate session keys.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        public void ScheduleCandidate_showButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check the user input, if it is valid, get the test session details.
                if (!IsValidData())
                {
                    ScheduleCandidate_testDetailsDiv.Style["display"] = "none";
                    ScheduleCandidate_testDetailsGridDiv.Style["display"] = "none";
                    ScheduleCandidate_bottomScheduleButtonTable.Style["display"] = "none";
                    ScheduleCandidate_bottomScheduleButtonTable.Style["display"] = "none";
                    return;
                }

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "CandidateSessionId";

                // Load the candidate session details grid.
                LoadTestAndCandidateSessions();


                if (ScheduleCandidate_expiryDateLabel.Text != null && ScheduleCandidate_expiryDateLabel.Text != "")
                {
                    int result = DateTime.Compare(Convert.ToDateTime(ScheduleCandidate_expiryDateLabel.Text), DateTime.Now.Add(new TimeSpan(-23, -59, -59)));
                    if (result >= 0)
                    {
                        ScheduleCandidate_bottomScheduleButtonTable.Style["display"] = "block";
                        
                        ScheduleCandidate_Popup_createCandidateButton.Attributes.Add("onclick",
                          "return ShowCandidatePopup('" + ScheduleCandidate_Popup_candidateNameTextBox.ClientID + "','"
                          + ScheduleCandidate_Popup_emailTextBox.ClientID + "','"
                          + ScheduleCandidate_candidateIdHiddenField.ClientID + "')");

                        ScheduleCandidate_bottomBulkScheduleCandidateLinkButton.Attributes.Add("onclick",
                       "return ShowBulkScheduleCandidatePopup('" + ScheduleCandidate_testSessionIdTextBox.Text + "','"
                       + ScheduleCandidate_expiryDateLabel.Text + "','"
                       + ScheduleCandidate_Popup_candidateNameTextBox.ClientID + "','"
                       + ScheduleCandidate_Popup_emailTextBox.ClientID + "','" + ScheduleCandidate_candidateIdHiddenField.ClientID + "')");
                    }
                    else
                    {
                        ScheduleCandidate_bottomScheduleButtonTable.Style["display"] = "none";
                    }
                }

            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);

                if (exp.Message.Contains("Test Session Id not found"))
                {
                    ScheduleCandidate_testDetailsDiv.Style["display"] = "none";
                    ScheduleCandidate_testDetailsGridDiv.Style["display"] = "none";
                    ScheduleCandidate_bottomScheduleButtonTable.Style["display"] = "none";
                }
            }
        }
        /// <summary>
        /// Display the schedule/unschedule/reschedule/viewresults image buttons 
        /// according to the status field
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewRowEventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleCandidate_candidateSessionGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                Label ScheduleCandidate_statusLabel =
                    (Label)e.Row.FindControl("ScheduleCandidate_statusLabel");
                ImageButton ScheduleCandidate_scheduleImageButton =
                    (ImageButton)e.Row.FindControl("ScheduleCandidate_scheduleImageButton");
                ImageButton ScheduleCandidate_rescheduleImageButton =
                    (ImageButton)e.Row.FindControl("ScheduleCandidate_rescheduleImageButton");
                ImageButton ScheduleCandidate_unscheduleImageButton =
                    (ImageButton)e.Row.FindControl("ScheduleCandidate_unscheduleImageButton");
                ImageButton ScheduleCandidate_viewShedulerImageButton =
                    (ImageButton)e.Row.FindControl("ScheduleCandidate_viewShedulerImageButton");
                ImageButton ScheduleCandidate_retakeRequestImageButton =
                    (ImageButton)e.Row.FindControl("ScheduleCandidate_retakeRequestImageButton");
                ImageButton ScheduleCandidate_viewCancelReasonImageButton =
                    (ImageButton)e.Row.FindControl("ScheduleCandidate_cancelReasonDisplayImageButton");
                HiddenField ScheduleCandidate_candidateTestSessionIDHiddenField =
                    (HiddenField)e.Row.FindControl("ScheduleCandidate_candidateTestSessionIDHiddenField");
                HiddenField ScheduleCandidate_attemptIDHiddenField =
                    (HiddenField)e.Row.FindControl("ScheduleCandidate_attemptIDHiddenField");

                HyperLink ScheduleCandidate_viewTestSummaryHyperLink =
                    (HyperLink)e.Row.FindControl("ScheduleCandidate_viewTestSummaryHyperLink");
                HyperLink ScheduleCandidate_viewResultHyperLink =
                    (HyperLink)e.Row.FindControl("ScheduleCandidate_viewResultHyperLink");

                // Initialize all the image buttons visibility as false.
                ScheduleCandidate_viewResultHyperLink.Visible = false;
                ScheduleCandidate_viewShedulerImageButton.Visible = false;
                ScheduleCandidate_scheduleImageButton.Visible = false;
                ScheduleCandidate_rescheduleImageButton.Visible = false;
                ScheduleCandidate_unscheduleImageButton.Visible = false;
                ScheduleCandidate_retakeRequestImageButton.Visible = false;
                ScheduleCandidate_viewCancelReasonImageButton.Visible = false;
                ScheduleCandidate_viewTestSummaryHyperLink.Visible = false;

                // Check the status and display the image buttons accordingly.
                if (ScheduleCandidate_statusLabel.Text == "Not Scheduled")
                    ScheduleCandidate_scheduleImageButton.Visible = true;

                else if (ScheduleCandidate_statusLabel.Text == "Scheduled")
                {
                    ScheduleCandidate_viewShedulerImageButton.Visible = true;
                    ScheduleCandidate_rescheduleImageButton.Visible = true;
                    ScheduleCandidate_unscheduleImageButton.Visible = true;
                }

                else if (ScheduleCandidate_statusLabel.Text == "Cancelled")
                    ScheduleCandidate_viewCancelReasonImageButton.Visible = true;

                else if (ScheduleCandidate_statusLabel.Text == "In Progress")
                {
                    ScheduleCandidate_viewShedulerImageButton.Visible = true;
                    ScheduleCandidate_viewTestSummaryHyperLink.Visible = true;
                }
                if (ScheduleCandidate_testTypeHiddenField.Value == "1" && ScheduleCandidate_statusLabel.Text == "Completed" ||
                    ScheduleCandidate_statusLabel.Text == "Quit" ||
                    ScheduleCandidate_statusLabel.Text == "Elapsed"
                    )
                {
                    ScheduleCandidate_viewResultHyperLink.Visible = true;
                    ScheduleCandidate_viewShedulerImageButton.Visible = true;
                    ScheduleCandidate_viewTestSummaryHyperLink.Visible = true;
                    // Check the retake request field and display the RetakeRequest image.
                    HiddenField retakeRequestHiddenField =
                        e.Row.FindControl("ScheduleCandidate_requestRetakeHiddenField") as HiddenField;

                    string retakeRequest = retakeRequestHiddenField.Value;

                    if (retakeRequest == "Y")
                        ScheduleCandidate_retakeRequestImageButton.Visible = true;
                }
                else if (ScheduleCandidate_testTypeHiddenField.Value == "2" && ScheduleCandidate_statusLabel.Text == "Evaluated")
                {
                    ScheduleCandidate_viewResultHyperLink.Visible = true;
                    ScheduleCandidate_viewShedulerImageButton.Visible = true;
                    ScheduleCandidate_viewTestSummaryHyperLink.Visible = true;
                    // Check the retake request field and display the RetakeRequest image.
                    HiddenField retakeRequestHiddenField =
                        e.Row.FindControl("ScheduleCandidate_requestRetakeHiddenField") as HiddenField;

                    string retakeRequest = retakeRequestHiddenField.Value;

                    if (retakeRequest == "Y")
                        ScheduleCandidate_retakeRequestImageButton.Visible = true;
                }

                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                ScheduleCandidate_viewResultHyperLink.NavigateUrl = "~/TestMaker/TestResult.aspx?m=2&s=1" +
                        "&candidatesession=" + ScheduleCandidate_candidateTestSessionIDHiddenField.Value +
                        "&attemptid=" + ScheduleCandidate_attemptIDHiddenField.Value +
                        "&testkey=" + ScheduleCandidate_testIDLabel.Text +
                        "&parentpage=SCH_CAND";

                ScheduleCandidate_viewTestSummaryHyperLink.NavigateUrl =
                        "~/ReportCenter/CandidateTestDetails.aspx?m=3&s=0" +
                        "&testkey=" + ScheduleCandidate_testIDLabel.Text +
                        "&candidatesession=" + ScheduleCandidate_candidateTestSessionIDHiddenField.Value +
                        "&attemptid=" + ScheduleCandidate_attemptIDHiddenField.Value +
                        "&tab=CS" +
                        "&parentpage=" + Constants.ParentPage.TEST_STATISTICS_INFO;

                HyperLink ScheduleCandidate_candidateHyperLink =
                    (HyperLink)e.Row.FindControl("ScheduleCandidate_candidateHyperLink");

                HiddenField ScheduleCandidate_Grid_candidateIDHiddenField =
                    (HiddenField)e.Row.FindControl("ScheduleCandidate_Grid_candidateIDHiddenField");

                if (ScheduleCandidate_candidateHyperLink != null)
                {
                    if (ScheduleCandidate_Grid_candidateIDHiddenField != null)
                    {
                        ScheduleCandidate_candidateHyperLink.NavigateUrl = "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&puid="
                            + ScheduleCandidate_Grid_candidateIDHiddenField.Value;
                    }
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Check whether the clicked button is schedule/reschedule/unschedule/viewschedule
        /// and display the Modal popup accordingly.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewCommandEventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleCandidate_candidateSessionGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandSource is ImageButton))
                    return;

                HiddenField attemptIdHiddenField = ((ImageButton)e.CommandSource).Parent.
                        FindControl("ScheduleCandidate_attemptIDHiddenField") as HiddenField;
                HiddenField ScheduleCandidate_modifiedDateHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("ScheduleCandidate_modifiedDateHiddenField") as HiddenField;
                HiddenField ScheduleCandidate_Grid_ExpiryDateHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("ScheduleCandidate_Grid_ExpiryDateHiddenField") as HiddenField;
                HiddenField ScheduleCandidate_Grid_candidateEmailHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("ScheduleCandidate_Grid_candidateEmailHiddenField") as HiddenField;
                HiddenField ScheduleCandidate_Grid_candidateNameHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("ScheduleCandidate_Grid_candidateNameHiddenField") as HiddenField;
                HiddenField ScheduleCandidate_Grid_candidateIDHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("ScheduleCandidate_Grid_candidateIDHiddenField") as HiddenField;

                HiddenField ScheduleCandidate_Grid_emailReminderHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("ScheduleCandidate_Grid_emailReminderHiddenField") as HiddenField;

                ScheduleCandidate_emailRemainderCheckBox.Checked = false;

                // Schedule Candidate
                if (e.CommandName == "schedule")
                {
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    ViewState["STATUS"] = "SCHEDULE";

                    // Assign modified date in ViewState
                    ViewState["MODIFIED_DATE"] =
                        Convert.ToDateTime(ScheduleCandidate_modifiedDateHiddenField.Value);

                    ScheduleCandidate_questionResultLiteral.Text = "Schedule";

                    // Clear the fields before displaying the schedule popup
                    ClearSchedulePopup();

                    ScheduleCandidate_Popup_sessionExpiryDateValueLabel.Text =
                        ScheduleCandidate_expiryDateLabel.Text.Trim();

                    ScheduleCandidate_candidateNameImageButton.Visible = true;
                    ScheduleCandidate_positionProfileCandidateImageButton.Visible = true;
                    ScheduleCandidate_Popup_createCandidateButton.Visible = true;
                    ScheduleCandidate_scheduleModalPopupExtender.Show();
                }
                else if (e.CommandName == "reschedule")
                {
                    // Reschedule candidate 
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    ScheduleCandidate_questionResultLiteral.Text = "Reschedule";
                    ViewState["STATUS"] = "RESCHEDULE";

                    // Clear the fields before displaying the reschedule popup
                    ClearSchedulePopup();

                    // This will be used in SAVE button event handler of the schedule popup 
                    // to compare with current date.
                    ScheduleCandidate_Popup_sessionExpiryDateValueLabel.Text =
                        ScheduleCandidate_expiryDateLabel.Text;
                    ScheduleCandidate_Popup_candidateNameTextBox.Text =
                        ScheduleCandidate_Grid_candidateNameHiddenField.Value.Trim();
                    ScheduleCandidate_Popup_emailTextBox.Text =
                        ScheduleCandidate_Grid_candidateEmailHiddenField.Value.Trim();
                    ScheduleCandidate_reScheduleCandidateIDHiddenField.Value =
                        ScheduleCandidate_Grid_candidateIDHiddenField.Value.Trim();
                    ScheduleCandidate_scheduleDatePopupTextBox.Text =
                        ScheduleCandidate_Grid_ExpiryDateHiddenField.Value;

                    if (ScheduleCandidate_Grid_emailReminderHiddenField != null)
                        if (ScheduleCandidate_Grid_emailReminderHiddenField.Value == "Y")
                        {
                            ScheduleCandidate_emailRemainderCheckBox.Checked = true;
                        }

                    ScheduleCandidate_candidateNameImageButton.Visible = false;
                    ScheduleCandidate_positionProfileCandidateImageButton.Visible = false;
                    ScheduleCandidate_Popup_createCandidateButton.Visible = false;
                    ScheduleCandidate_scheduleModalPopupExtender.Show();
                }
                else if (e.CommandName == "retakeTest")
                {
                    //Get the values 
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    ViewState["STATUS"] = "RETAKE";

                    //Check the retake validation . whether the retake are allowed . 
                    //Check the number of elapsed days a
                    string retakeCount = string.Empty;
                    Dictionary<string, string> dicRetakeDetails = new
                        Dictionary<string, string>();
                    string isCertificateTest = string.Empty;
                    string testKey = ScheduleCandidate_testIDLabel.Text;
                    int attemptID = int.Parse(attemptIdHiddenField.Value);
                    string pendingAttempt = new CandidateBLManager().TestRetakeValidation(e.CommandArgument.ToString(), testKey,
                        attemptID, out retakeCount, out isCertificateTest, out dicRetakeDetails);

                    if (Convert.ToInt32(isCertificateTest) > 0)
                    {
                        if (dicRetakeDetails != null && dicRetakeDetails.Count > 0)
                        {
                            foreach (KeyValuePair<string, string> retakeDetail in dicRetakeDetails)//Temporary-will be removed//
                            {
                                if (Convert.ToInt32(retakeDetail.Key) > 0)
                                {
                                    ScheduleCandidate_RetakeValidation_ConfirmMsgControl.Message =
                                        "You are allowed to retake your test only after " + retakeDetail.Key + " days from "
                                        + retakeDetail.Value;

                                    ScheduleCandidate_RetakeValidation_ModalPopupExtender.Show();
                                }
                                else
                                {
                                    // Code for retaking test request                    
                                    ScheduleCandidate_questionResultLiteral.Text = "Schedule Retake Request";
                                    ClearSchedulePopup();

                                    // Bind all fields to retake request
                                    ScheduleCandidate_Popup_sessionExpiryDateValueLabel.Text =
                                        ScheduleCandidate_expiryDateLabel.Text;
                                    ScheduleCandidate_Popup_candidateNameTextBox.Text =
                                        ScheduleCandidate_Grid_candidateNameHiddenField.Value.Trim();
                                    ScheduleCandidate_Popup_emailTextBox.Text =
                                        ScheduleCandidate_Grid_candidateEmailHiddenField.Value.Trim();
                                    ScheduleCandidate_reScheduleCandidateIDHiddenField.Value =
                                        ScheduleCandidate_Grid_candidateIDHiddenField.Value.Trim();

                                    ScheduleCandidate_candidateNameImageButton.Visible = false;
                                    ScheduleCandidate_positionProfileCandidateImageButton.Visible = false;
                                    ScheduleCandidate_Popup_createCandidateButton.Visible = false;
                                    ScheduleCandidate_scheduleModalPopupExtender.Show();
                                }
                            }
                        }
                    }
                    else
                    {
                        // Code for retaking test request                    
                        ScheduleCandidate_questionResultLiteral.Text = "Schedule Retake Request";
                        ClearSchedulePopup();

                        // Bind all fields to retake request
                        ScheduleCandidate_Popup_sessionExpiryDateValueLabel.Text =
                            ScheduleCandidate_expiryDateLabel.Text;
                        ScheduleCandidate_Popup_candidateNameTextBox.Text =
                            ScheduleCandidate_Grid_candidateNameHiddenField.Value.Trim();
                        ScheduleCandidate_Popup_emailTextBox.Text =
                            ScheduleCandidate_Grid_candidateEmailHiddenField.Value.Trim();
                        ScheduleCandidate_reScheduleCandidateIDHiddenField.Value =
                            ScheduleCandidate_Grid_candidateIDHiddenField.Value.Trim();

                        ScheduleCandidate_candidateNameImageButton.Visible = false;
                        ScheduleCandidate_positionProfileCandidateImageButton.Visible = false;
                        ScheduleCandidate_Popup_createCandidateButton.Visible = false;
                        ScheduleCandidate_scheduleModalPopupExtender.Show();

                    }

                }
                else if (e.CommandName == "unschedule")
                {
                    // Code for unschedule
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;

                    ScheduleCandidate_unscheduleCandidateModalPopupExtender.Show();
                }
                else if (e.CommandName == "viewSchedule")
                {
                    // Code for view schedule
                    ScheduleCandidate_viewTestSchedule.CandidateSessionId = e.CommandArgument.ToString();
                    TestSessionDetail testSession = null;
                    if (Cache["SEARCH_DATATABLE"] != null)
                    {
                        testSession = Cache["SEARCH_DATATABLE"] as TestSessionDetail;

                        // Assign the TestSessionDetail instance to ViewTestSchedule popup to 
                        // display all information.
                        ScheduleCandidate_viewTestSchedule.TestSession = testSession;
                        ScheduleCandidate_viewScheduleModalPopupExtender.Show();
                    }
                }
                else if (e.CommandName == "cyberProctoringDetails")
                {
                    // Navigate to TrackDetails page by passing the CandidateSessionId and AttemptId.
                    Response.Redirect("~/ReportCenter/TrackingDetails.aspx" +
                        "?m=2&s=1&candidatesession=" + e.CommandArgument +
                        "&attemptid=" + attemptIdHiddenField.Value +
                        "&parentpage=SCH_CAND", false);
                }
                else if (e.CommandName == "viewCancelReason")
                {
                    // Code for View cancel reason
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    TestSessionDetail testSession = null;

                    if (Cache["SEARCH_DATATABLE"] != null)
                    {
                        testSession = Cache["SEARCH_DATATABLE"] as TestSessionDetail;
                        GridViewRow row = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                        int strCurrentRowIndex = row.RowIndex;

                        // Load the cancel reason text and display the popup.
                        ScheduleCandidate_cancelTestReasonDiv.InnerHtml =
                            testSession.CandidateTestSessions[strCurrentRowIndex].CancelReason.ToString();
                        ScheduleCandidate_cancelSessionModalPopupExtender.Show();
                    }
                }
            }
            catch (Exception exp)
            {
                // Catch the error
                Logger.ExceptionLog(exp);
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Open popup when schedule candidate button clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleCandidate_ScheduleCandidateLinkButton_Click(object sender, EventArgs e)
        {
            ClearSchedulePopup();

            ScheduleCandidate_Popup_sessionExpiryDateValueLabel.Text =
                ScheduleCandidate_expiryDateLabel.Text.Trim();

            ScheduleCandidate_candidateNameImageButton.Visible = true;
            ScheduleCandidate_positionProfileCandidateImageButton.Visible = true;
            ScheduleCandidate_Popup_createCandidateButton.Visible = true;
            ScheduleCandidate_scheduleModalPopupExtender.Show();
        }

        /// <summary>
        /// Open popup when schedule bulk candidate button clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleCandidate_BulkScheduleCandidateLinkButton_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Method to check whether given email id is valid or not
        /// </summary>
        /// <param name="strUserEmailId"></param>
        /// <returns></returns>
        private bool IsValidEmailAddress(string strUserEmailId)
        {
            //Regex regex = new Regex(".+@.+\\.[a-zA-Z]+", RegexOptions.Compiled);
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$", RegexOptions.Compiled);
            return regex.IsMatch(strUserEmailId);
        }

        /// <summary>
        /// Clicking on Save button in Schedule popup will trigger this method.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ScheduleCandidate_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Validate the scheduled date and display the error message if any.
                ScheduleCandidate_MaskedEditValidator.Validate();

                if (!ScheduleCandidate_MaskedEditValidator.IsValid)
                {
                    ScheduleCandidate_scheduleErrorMsgLabel.Text =
                        ScheduleCandidate_MaskedEditValidator.ErrorMessage;
                    ScheduleCandidate_scheduleModalPopupExtender.Show();
                    return;
                }

                // Scheduled date should be between current date and test session's expiry date.
                TestSessionDetail testSession = Cache["SEARCH_DATATABLE"] as TestSessionDetail;

                if (testSession != null)
                {
                    if (DateTime.Parse(ScheduleCandidate_scheduleDatePopupTextBox.Text) > testSession.ExpiryDate
                        || DateTime.Parse(ScheduleCandidate_scheduleDatePopupTextBox.Text) < DateTime.Today)
                    {
                        ScheduleCandidate_scheduleErrorMsgLabel.Text =
                            Resources.HCMResource.ScheduleCandidate_ScheduledDateInvalid;
                        ScheduleCandidate_scheduleModalPopupExtender.Show();

                        // Since the candidate name and email textboxes are readonly, its values are not 
                        // maintained on postback. Use Request[controlName.UniqueID] to get the value.
                        ScheduleCandidate_Popup_candidateNameTextBox.Text =
                            Request[ScheduleCandidate_Popup_candidateNameTextBox.UniqueID].Trim();
                        ScheduleCandidate_Popup_emailTextBox.Text =
                            Request[ScheduleCandidate_Popup_emailTextBox.UniqueID].Trim();
                        return;
                    }
                }


                if (ScheduleCandidate_Popup_emailTextBox.Text.Trim() != "")
                {
                    if (!IsValidEmailAddress(ScheduleCandidate_Popup_emailTextBox.Text.Trim()))
                    {
                        ScheduleCandidate_scheduleErrorMsgLabel.Text =
                            HCMResource.NewCandidate_EnterValidEmailID;

                        ScheduleCandidate_scheduleModalPopupExtender.Show();
                        return;
                    }
                }
                string isCandidateAlreadyAssignedSchedulerName1 = new TestSchedulerBLManager().CheckCandidateAlreadyAssigned(ScheduleCandidate_testSessionIdTextBox.Text.Trim(), ScheduleCandidate_candidateIdHiddenField.Value);
                if (isCandidateAlreadyAssignedSchedulerName1 != null && isCandidateAlreadyAssignedSchedulerName1.Length > 0 && (!Utility.IsNullOrEmpty(ViewState["STATUS"]) &&
                   ViewState["STATUS"].ToString().ToUpper() == "SCHEDULE"))
                {
                    base.ShowMessage(ScheduleCandidate_scheduleErrorMsgLabel,
                           "Already Candidate " + ScheduleCandidate_Popup_candidateNameTextBox.Text +
                           " has been scheduled in this test session");
                    ScheduleCandidate_scheduleModalPopupExtender.Show();
                    return;
                }
                if (ViewState["CANDIDATE_SESSIONID"] == null || ViewState["ATTEMPT_ID"] == null)
                {
                    CreateCandidateSession(1);
                }


                TestScheduleDetail testSchedule = new TestScheduleDetail();
                testSchedule.CandidateID = ScheduleCandidate_candidateIdHiddenField.Value;
                testSchedule.CandidateTestSessionID = ViewState["CANDIDATE_SESSIONID"].ToString();
                testSchedule.AttemptID = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());
                testSchedule.EmailId = Request[ScheduleCandidate_Popup_emailTextBox.UniqueID].Trim();

                if (ScheduleCandidate_emailRemainderCheckBox.Checked)
                    testSchedule.EmailReminder = "Y";
                else
                    testSchedule.EmailReminder = "N";

                // Set the time to maximum in expiry date.
                testSchedule.ExpiryDate = (Convert.ToDateTime(
                    ScheduleCandidate_scheduleDatePopupTextBox.Text)).Add(new TimeSpan(23, 59, 59));

                bool isMailSent = true;
                if (!Utility.IsNullOrEmpty(ViewState["STATUS"]) &&
                    ViewState["STATUS"].ToString().ToUpper() == "RESCHEDULE")
                {
                    testSchedule.IsRescheduled = true;

                    if (!Utility.IsNullOrEmpty(ScheduleCandidate_reScheduleCandidateIDHiddenField.Value))
                        testSchedule.CandidateID = ScheduleCandidate_reScheduleCandidateIDHiddenField.Value;

                    new TestSchedulerBLManager().ScheduleCandidate(testSchedule, base.userID, base.tenantID, out isMailSent);

                    // Once the data is updated show the success message.
                    base.ShowMessage(ScheduleCandidate_topSuccessMessageLabel,
                            ScheduleCandidate_bottomSuccessMessageLabel,
                            Resources.HCMResource.ScheduleCandidate_ReScheduledSuccesss);

                    // Check if the mail has been sent successfully.
                    if (isMailSent == false)
                    {
                        ScheduleCandidate_topErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                        ScheduleCandidate_bottomErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    }
                }
                else if (!Utility.IsNullOrEmpty(ViewState["STATUS"]) &&
                    ViewState["STATUS"].ToString().ToUpper() == "SCHEDULE")
                {
                    if (testSession == null || testSchedule == null)
                    {
                        base.ShowMessage(ScheduleCandidate_topErrorMessageLabel,
                            ScheduleCandidate_bottomErrorMessageLabel,
                            "Your current session seems to be expired. Reload the page and try again.");
                        return;
                    }
                    //check whether the same candidate is assigned for test session id
                    string isCandidateAlreadyAssignedSchedulerName = new TestSchedulerBLManager().CheckCandidateAlreadyAssigned(testSession.TestSessionID, testSchedule.CandidateID);

                    if (isCandidateAlreadyAssignedSchedulerName == null ||
                        isCandidateAlreadyAssignedSchedulerName.Trim().Length == 0)
                    {
                        if (new AdminBLManager().IsFeatureUsageLimitExceeds(this.tenantID, Constants.FeatureConstants.ONLINE_TEST_ADMINISTERED))
                        {
                            ShowMessage(ScheduleCandidate_scheduleErrorMsgLabel, "You have reached the maximum limit based your subscription plan. Cannot schedule more candidates");
                            ScheduleCandidate_scheduleModalPopupExtender.Show();
                            return;
                        }

                        new TestSchedulerBLManager().ScheduleCandidate(testSchedule, base.userID, base.tenantID, out isMailSent);
                        base.ShowMessage(ScheduleCandidate_topSuccessMessageLabel,
                            ScheduleCandidate_bottomSuccessMessageLabel,
                            Resources.HCMResource.ScheduleCandidate_ScheduledSuccess);
                    }
                    else
                    {
                        base.ShowMessage(ScheduleCandidate_scheduleErrorMsgLabel,
                            "Already Candidate " + ScheduleCandidate_Popup_candidateNameTextBox.Text +
                            " has been scheduled in this test session");
                        ScheduleCandidate_scheduleModalPopupExtender.Show();
                        return;
                    }

                    // Check if the mail has been sent successfully.
                    if (isMailSent == false)
                    {
                        ScheduleCandidate_topErrorMessageLabel.Text =
                               Resources.HCMResource.TestScheduler_MailCannotBeSent;
                        ScheduleCandidate_bottomErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    }
                }
                else // Retake a test
                {
                    new TestSchedulerBLManager().RetakeTest(testSchedule, base.userID, out isMailSent,base.tenantID);
                    base.ShowMessage(ScheduleCandidate_topSuccessMessageLabel,
                        ScheduleCandidate_bottomSuccessMessageLabel,
                        Resources.HCMResource.ScheduleCandidate_ReTakeSuccesss);

                    // Check if the mail has been sent successfully.
                    if (isMailSent == false)
                    {
                        ScheduleCandidate_topErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                        ScheduleCandidate_bottomErrorMessageLabel.Text =
                            Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    }
                }

                // TODO : Instead of loading the whole grid, update only that row.
                LoadTestAndCandidateSessions();
                ViewState["STATUS"] = null;
                ViewState["CANDIDATE_SESSIONID"] = null;
                ViewState["ATTEMPT_ID"] = null;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Methods

        /// <summary>
        /// Return the proper status message according to the DB session values.
        /// </summary>
        /// <param name="pStatus">
        /// A <see cref="string"/> that contains the test status
        /// </param>
        /// <returns>
        /// It will return the test status.
        /// </returns>
        protected void CreateCandidateSession(int noOfCandidateSession)
        {
            string candidateSessionIDs = null;
            new TestBLManager().CreateCandidateTestSession(noOfCandidateSession, base.userID, ScheduleCandidate_testSessionIdTextBox.Text.ToString(), out candidateSessionIDs);
            ViewState["CANDIDATE_SESSIONID"] = candidateSessionIDs;
            ViewState["ATTEMPT_ID"] = 1;
            ViewState["STATUS"] = "SCHEDULE";
        }

        /// <summary>
        /// Return the proper status message according to the DB session values.
        /// </summary>
        /// <param name="pStatus">
        /// A <see cref="string"/> that contains the test status
        /// </param>
        /// <returns>
        /// It will return the test status.
        /// </returns>
        protected string GetStatus(string pStatus)
        {
            string status = "";
            switch (pStatus.Trim())
            {
                case Constants.CandidateSessionStatus.ELAPSED:
                    status = "Elapsed";
                    break;
                case Constants.CandidateSessionStatus.IN_PROGRESS:
                    status = "In Progress";
                    break;
                case Constants.CandidateSessionStatus.NOT_SCHEDULED:
                    status = "Not Scheduled";
                    break;
                case Constants.CandidateSessionStatus.QUIT:
                    status = "Quit";
                    break;
                case Constants.CandidateSessionStatus.SCHEDULED:
                    status = "Scheduled";
                    break;
                case Constants.CandidateSessionStatus.CANCELLED:
                    status = "Cancelled";
                    break;
                case Constants.CandidateSessionStatus.COMPLETED:
                    status = "Completed";
                    break;
                case Constants.CandidateSessionStatus.EVALUATED:
                    status = "Evaluated";
                    break;
                default:
                    status = "Not Scheduled";
                    break;
            }
            return status;
        }

        /// <summary>
        /// This method returns true when CyberProctoring is enabled as well as 
        /// the candidate session is completed.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds the status of the candidate.
        /// </param>
        /// <returns></returns>
        protected bool IsCyberProctoring(string status)
        {
            TestSessionDetail testSession = Cache["SEARCH_DATATABLE"] as TestSessionDetail;
            if (testSession != null)
                return (status.Trim() == Constants.CandidateSessionStatus.COMPLETED
                    && testSession.IsCyberProctoringEnabled) ? true : false;
            else
                return false;
        }

        /// <summary>
        /// This method clears the textboxes in Schedule/Reschedule popup.
        /// </summary>
        protected void ClearSchedulePopup()
        {
            ScheduleCandidate_Popup_candidateNameTextBox.Text = "";
            ScheduleCandidate_Popup_emailTextBox.Text = "";
            ScheduleCandidate_scheduleDatePopupTextBox.Text = "";
            ScheduleCandidate_scheduleErrorMsgLabel.Text = "";
        }

        /// <summary>
        /// This method clears the test session and candidate session details
        /// </summary>
        protected void ClearSessionValues()
        {
            try
            {
                ScheduleCandidate_testDetailsDiv.Style["display"] = "none";
                ScheduleCandidate_testDetailsGridDiv.Style["display"] = "none";
                ScheduleCandidate_candidateSessionHeader.Style["display"] = "none";
                ScheduleCandidate_bottomScheduleButtonTable.Style["display"] = "none";

                // Store the scheduled date in view state

                ScheduleCandidate_testNameLabel.Text = "";
                ScheduleCandidate_testIDLabel.Text = "";
                ScheduleCandidate_emailLabel.Text = "";
                ScheduleCandidate_testAuthorLabel.Text = "";
                ScheduleCandidate_testDescLiteral.Text = "";
                ScheduleCandidate_timeLimitLabel.Text = "";
                ScheduleCandidate_expiryDateLabel.Text = "";
                ScheduleCandidate_positionProfileLabel.Text = "";
                ScheduleCandidate_instructionsLiteral.Text = "";
                ScheduleCandidate_cyberProctorateLabel.Text = "";
                ScheduleCandidate_displayResultsLabel.Text = "";

                Cache["SEARCH_DATATABLE"] = "";
                ViewState["TEST_SESSION_ID"] = "";
                ViewState["CANDIDATE_SESSION_IDS"] = "";
                ScheduleCandidate_candidateSessionGridView.DataSource = null;
                ScheduleCandidate_candidateSessionGridView.DataBind();
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Protected Methods

        #region Sorting Related Methods

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void ScheduleCandidate_candidateSessionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }

                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] = ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                LoadTestAndCandidateSessions();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }
        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void ScheduleCandidate_candidateSessionGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                            (ScheduleCandidate_candidateSessionGridView,
                            (string)ViewState["SORT_FIELD"]);
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            (SortType)ViewState["SORT_ORDER"]);
                    }
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Sorting Related Methods

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            // Maintain the expand/restore gridview.
            if (!Utility.IsNullOrEmpty(ScheduleCandidate_isMaximizedHiddenField.Value) &&
                ScheduleCandidate_isMaximizedHiddenField.Value == "Y")
            {
                ScheduleCandidate_testSessionDetailsDiv.Style["display"] = "none";
                ScheduleCandidate_searchResultsUpSpan.Style["display"] = "block";
                ScheduleCandidate_searchResultsDownSpan.Style["display"] = "none";
                ScheduleCandidate_candidateSessionGridViewDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                ScheduleCandidate_testSessionDetailsDiv.Style["display"] = "block";
                ScheduleCandidate_searchResultsUpSpan.Style["display"] = "none";
                ScheduleCandidate_searchResultsDownSpan.Style["display"] = "block";
                ScheduleCandidate_candidateSessionGridViewDIV.Style["height"] = RESTORED_HEIGHT;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE]))
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ScheduleCandidate_isMaximizedHiddenField.Value))
                    ((TestScheduleSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE]).IsMaximized =
                        ScheduleCandidate_isMaximizedHiddenField.Value == "Y" ? true : false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadTestAndCandidateSessions()
        {
            try
            {
                TestScheduleSearchCriteria scheduleCandidateSearchCriteria = new TestScheduleSearchCriteria();

                // Check if the search criteria fields are empty.
                // If this is empty, pass null. Or else, pass its value.
                scheduleCandidateSearchCriteria.TestSessionID =
                    ScheduleCandidate_testSessionIdTextBox.Text.Trim() == "" ?
                    string.Empty : ScheduleCandidate_testSessionIdTextBox.Text.Trim();

                scheduleCandidateSearchCriteria.CandidateSessionIDs =
                    ScheduleCandidate_candidateSessionIdTextBox.Text.Trim() == "" ?
                    string.Empty : ScheduleCandidate_candidateSessionIdTextBox.Text.Trim();

                scheduleCandidateSearchCriteria.IsMaximized =
                    ScheduleCandidate_isMaximizedHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

                scheduleCandidateSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                scheduleCandidateSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();

                // Keep the search criteria in Session if the page is launched from 
                // somewhere else.
                Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE] = scheduleCandidateSearchCriteria;

                TestSessionDetail testSession = new TestSchedulerBLManager().
                    GetTestSessionDetail(scheduleCandidateSearchCriteria,
                        scheduleCandidateSearchCriteria.SortExpression,
                        scheduleCandidateSearchCriteria.SortDirection, base.userID);

                if (testSession != null)
                {
                    UserDetail userDetail = new UserDetail();
                    userDetail = (UserDetail)Session["USER_DETAIL"];

                    ScheduleCandidate_testDetailsDiv.Style["display"] = "block";
                    ScheduleCandidate_testDetailsGridDiv.Style["display"] = "block";
                    ScheduleCandidate_candidateSessionHeader.Style["display"] = "block";
                    ScheduleCandidate_bottomScheduleButtonTable.Style["display"] = "block";

                    // Store the scheduled date in view state
                    ScheduleCandidate_testNameLabel.Text = testSession.TestName;
                    ScheduleCandidate_testIDLabel.Text = testSession.TestID;
                    ScheduleCandidate_emailLabel.Text = testSession.TestSessionAuthorEmail;
                    ScheduleCandidate_testAuthorLabel.Text = testSession.TestSessionAuthor;
                    ScheduleCandidate_testDescLiteral.Text =
                        testSession.TestSessionDesc == null ? testSession.TestSessionDesc :
                        testSession.TestSessionDesc.ToString().Replace(Environment.NewLine, "<br />");
                    ScheduleCandidate_timeLimitLabel.Text =
                        Utility.ConvertSecondsToHoursMinutesSeconds(Convert.ToInt32(testSession.TimeLimit.ToString()));
                    ScheduleCandidate_expiryDateLabel.Text = GetDateFormat(testSession.ExpiryDate);
                    ScheduleCandidate_positionProfileLabel.Text = testSession.PositionProfileName;
                    ScheduleCandidate_instructionsLiteral.Text =
                        testSession.Instructions == null ? testSession.Instructions :
                        testSession.Instructions.ToString().Replace(Environment.NewLine, "<br />");
                    ScheduleCandidate_cyberProctorateLabel.Text = testSession.IsCyberProctoringEnabled ? "Yes" : "No";
                    ScheduleCandidate_displayResultsLabel.Text = testSession.IsDisplayResultsToCandidate ? "Yes" : "No";
                    ScheduleCandidate_testTypeHiddenField.Value = testSession.TestType.ToString();

                    Cache["SEARCH_DATATABLE"] = testSession;
                    ViewState["TEST_SESSION_ID"] = ScheduleCandidate_testSessionIdTextBox.Text.Trim();
                    ViewState["CANDIDATE_SESSION_IDS"] = ScheduleCandidate_candidateSessionIdTextBox.Text.Trim();
                    ScheduleCandidate_candidateSessionGridView.DataSource = testSession.CandidateTestSessions;
                    ScheduleCandidate_candidateSessionGridView.DataBind();

                    // Remove 'onclick' handler for 'load position profile candidate' icon.
                    if (ScheduleCandidate_positionProfileCandidateImageButton.Attributes["onclick"] != null)
                        ScheduleCandidate_positionProfileCandidateImageButton.Attributes.Remove("onclick");

                    // Assign handler for 'load position profile candidate' icon.
                    ScheduleCandidate_positionProfileCandidateImageButton.Attributes.Add("onclick", "return LoadPositionProfileCandidates('"
                       + ScheduleCandidate_Popup_candidateNameTextBox.ClientID + "','"
                       + ScheduleCandidate_Popup_emailTextBox.ClientID + "','"
                       + ScheduleCandidate_candidateIdHiddenField.ClientID + "','"
                       + testSession.PositionProfileID + "')");
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                    ScheduleCandidate_topErrorMessageLabel, exp.Message);

                Logger.ExceptionLog(exp);
                ClearSessionValues();
            }
        }

        /// <summary>
        /// Method that will fill the search criteria to the appropriate fields.
        /// </summary>
        /// <param name="scheduleSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the search
        /// criteria fields.
        /// </param>
        private void FillSearchCriteria(TestScheduleSearchCriteria scheduleSearchCriteria)
        {
            if (scheduleSearchCriteria.TestSessionID != null)
                ScheduleCandidate_testSessionIdTextBox.Text = scheduleSearchCriteria.TestSessionID;

            if (scheduleSearchCriteria.EMail != null)
                ScheduleCandidate_candidateSessionIdTextBox.Text = scheduleSearchCriteria.CandidateSessionIDs;

            ScheduleCandidate_isMaximizedHiddenField.Value =
                    scheduleSearchCriteria.IsMaximized == true ? "Y" : "N";

            ViewState["SORT_ORDER"] = scheduleSearchCriteria.SortDirection;
            ViewState["SORT_FIELD"] = scheduleSearchCriteria.SortExpression;

            // Apply search
            LoadTestAndCandidateSessions();
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// This method checks the user input. If both testSession and CandidateSession IDs 
        /// are not given, then return false.
        /// </summary>
        /// <returns>
        /// </returns>
        protected override bool IsValidData()
        {
            if (ScheduleCandidate_testSessionIdTextBox.Text != "" ||
                ScheduleCandidate_candidateSessionIdTextBox.Text != "")
            {
                // This method passes the list of candidateSession ids separated by comma and
                // it returns the candidate session keys which are not existing in test session.
                List<string> notMatchedCandSessionKeys =
                    new TestSchedulerBLManager().ValidateTestSessionInput
                    (ScheduleCandidate_testSessionIdTextBox.Text.Trim(),
                    ScheduleCandidate_candidateSessionIdTextBox.Text.Trim(), base.userID);

                //if (notMatchedCandSessionKeys != null && notMatchedCandSessionKeys.Count > 0)
                //{
                //    base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                //        ScheduleCandidate_topErrorMessageLabel, "No matching records found");
                //    return false;
                //}
                //else
                    return true;
            }
            base.ShowMessage(ScheduleCandidate_bottomErrorMessageLabel,
                ScheduleCandidate_topErrorMessageLabel,
                Resources.HCMResource.ScheduleCandidate_EmptySessionIds);
            return false;
        }

        /// <summary>
        /// This method loads the test session and scheduled candidate details.
        /// </summary>
        protected override void LoadValues()
        {
            // Assign the client side onclick events to header
            ScheduleCandidate_searchTestResultsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                ScheduleCandidate_candidateSessionGridViewDIV.ClientID + "','" +
                ScheduleCandidate_testSessionDetailsDiv.ClientID + "','" +
                ScheduleCandidate_searchResultsUpSpan.ClientID + "','" +
                ScheduleCandidate_searchResultsDownSpan.ClientID + "','" +
                ScheduleCandidate_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");

            ScheduleCandidate_candidateNameImageButton.Attributes.Add("onclick", "return LoadCandidate('"
                + ScheduleCandidate_Popup_candidateNameTextBox.ClientID + "','"
                + ScheduleCandidate_Popup_emailTextBox.ClientID + "','"
                + ScheduleCandidate_candidateIdHiddenField.ClientID
                + "')");


            // Check if page is navigated from talentscout and position profile is found
            if (Request.QueryString["parentpage"] != null &&
                Request.QueryString["parentpage"].Trim().ToUpper() == Constants.ParentPage.TALENT_SCOUT &&
                Request.QueryString["positionprofileid"] != null)
            {
                // Get position profile ID.
                int positionProfileID = 0;
                if (int.TryParse(Request.QueryString["positionprofileid"].Trim(), out positionProfileID) == true)
                {
                    ScheduleCandidate_testSessionImageButton.Attributes.Add("onclick", "return LoadTestSessionWithPositionProfile('"
                    + ScheduleCandidate_testSessionIdTextBox.ClientID + "','" +
                    positionProfileID + "')");
                }
                else
                {
                    ScheduleCandidate_testSessionImageButton.Attributes.Add("onclick", "return LoadTestSession('"
                    + ScheduleCandidate_testSessionIdTextBox.ClientID + "')");
                }
            }
            else
            {
                ScheduleCandidate_testSessionImageButton.Attributes.Add("onclick", "return LoadTestSession('"
                    + ScheduleCandidate_testSessionIdTextBox.ClientID + "')");
            }

            ScheduleCandidate_saveButton.Attributes.Add("onclick", "return VaildateSchedulePopup('"
                + ScheduleCandidate_Popup_candidateNameTextBox.ClientID + "','"
                + ScheduleCandidate_scheduleDatePopupTextBox.ClientID
                + "','" + ScheduleCandidate_scheduleErrorMsgLabel.ClientID + "')");

            ScheduleCandidate_testSessionIdTextBox.Attributes.Add("onkeydown", "TestSessionKeyDown(event,'"
                + ScheduleCandidate_showButton.ClientID + "')");

            // Assign default sort field and order keys.
            if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                ViewState["SORT_ORDER"] = SortType.Ascending;

            if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                ViewState["SORT_FIELD"] = "CandidateSessionID";
        }
        #endregion Protected Overridden Methods
    }
}