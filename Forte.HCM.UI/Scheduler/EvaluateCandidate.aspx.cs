﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ScheduleCandidate.aspx.cs
// This page allows the recruiter to schedule the test to candidates. 
// They can reschedule, schedule, unschedule the candidate session tests.

#endregion Header

#region Directives

using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

using Resources;

#endregion Directives

namespace Forte.HCM.UI.Scheduler
{
    /// <summary>
    /// This page allows the recruiter to schedule the test to candidates. 
    /// They can reschedule, schedule, unschedule the candidate session tests.
    /// </summary>
    public partial class EvaluateCandidate : PageBase
    {
        #region Private Members

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "100%";
        private string _tsid = string.Empty;

        #endregion Private Members

        #region Event Handlers

        /// <summary>
        /// Handler that will call when a page is loaded. During that time,
        /// it'll take the responsibility to implement the default settings
        /// such as page title, default button, and calling javascript
        /// functions etc.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set page title
                Master.SetPageCaption("Evaluate Candidate");
                CheckAndSetExpandorRestore();

                // Set default button.
                Page.Form.DefaultButton = EvaluateCandidate_showButton.UniqueID;
                UserDetail userDetail = new UserDetail();
                userDetail = (UserDetail)Session["USER_DETAIL"];

                // MDM added
                EvaluateCandidate_testSessionImageButton.Visible = true;
                ///MDM 
                //if (userDetail != null && userDetail.Roles != null)
                //{
                //    if (userDetail.Roles.Exists(item => item != UserRole.Recruiter))
                //        EvaluateCandidate_testSessionImageButton.Visible = true;
                //}

                if (!Page.IsPostBack)
                {
                    LoadValues();

                    // Set default focus
                    EvaluateCandidate_testSessionIdTextBox.Focus();

                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU
                         || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_LANDING))
                    {
                        base.ClearSearchCriteriaSession();
                    }
                    else
                    {
                        // If any querystring is passed(i.e. testsessionid)
                        if (!Utility.IsNullOrEmpty(Request.QueryString["testsessionid"]))
                        {
                            _tsid = Convert.ToString(Request.QueryString["testsessionid"]);

                            // If test session id is passed in Querystring, then display all the informations
                            // with respect to testSessionId.
                            EvaluateCandidate_testSessionIdTextBox.Text = _tsid;
                            EvaluateCandidate_testDetailsDiv.Style["display"] = "block";
                            EvaluateCandidate_testDetailsGridDiv.Style["display"] = "block";
                            EvaluateCandidate_candidateSessionHeader.Style["display"] = "block";

                            // Load the candidate session details grid.
                            LoadTestAndCandidateSessions();
                        }
                        else
                        {
                            if (Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE] != null)
                            {
                                // if redirected from any child page, fill the search criteria
                                // and apply the search.
                                FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE]
                                    as TestScheduleSearchCriteria);

                                // Reload the maximize hidden value.
                                CheckAndSetExpandorRestore();
                            }
                        }
                    }

                    // Load the search test session popup automatically, when 
                    // navigating from talentscout.
                    // Check if page is navigated from talentscout and position profile is found
                    if (Request.QueryString["parentpage"] != null &&
                        Request.QueryString["parentpage"].Trim().ToUpper() == Constants.ParentPage.TALENT_SCOUT &&
                        Request.QueryString["positionprofileid"] != null)
                    {
                        // Get position profile ID.
                        int positionProfileID = 0;
                        if (int.TryParse(Request.QueryString["positionprofileid"].Trim(), out positionProfileID) == true)
                        {
                            // Load the position profile popup.
                            ScriptManager.RegisterStartupScript
                                (this, this.GetType(), "LoadTestSessionWithPositionProfile",
                                "javascript: LoadTestSessionWithPositionProfile('"
                                + EvaluateCandidate_testSessionIdTextBox.ClientID + "','" +
                                positionProfileID + "')", true);
                        }
                    }
                }

                // Clear the error and success messages on every postback.
                EvaluateCandidate_bottomErrorMessageLabel.Text =
                    EvaluateCandidate_bottomSuccessMessageLabel.Text =
                    EvaluateCandidate_topErrorMessageLabel.Text =
                    EvaluateCandidate_topSuccessMessageLabel.Text = "";
            }
            catch (Exception exp)
            {
                base.ShowMessage(EvaluateCandidate_bottomErrorMessageLabel,
                    EvaluateCandidate_topErrorMessageLabel, exp.Message);

                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// While unscheduling the candidate, it asks the user to confirm. Once he confirms to 
        /// unschedule, it triggers the below method to specify the button type 'Yes'/'No'
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ConfirmMessageEventArgs"/> that contains the event data.
        /// </param>
        /// <remarks>
        /// This parameter has a property ButtonType which specify the Actiontype
        /// </remarks>
        void EvaluateCandidate_inactiveConfirmMsgControl_ConfirmMessageThrown
            (object sender, ConfirmMessageEventArgs e)
        {
            try
            {
                if (e.ButtonType == Forte.HCM.DataObjects.ButtonType.Yes)
                {
                    string candidateSessionId = ViewState["CANDIDATE_SESSIONID"].ToString();
                    string attemptId = ViewState["ATTEMPT_ID"].ToString();

                    bool isMailSent = true;

                    // Update the session status as 'Not Scheduled'
                    new TestConductionBLManager().UpdateSessionStatus
                        (candidateSessionId, Convert.ToInt32(attemptId),
                        Constants.CandidateAttemptStatus.NOT_SCHEDULED,
                        Constants.CandidateSessionStatus.NOT_SCHEDULED, base.userID, out isMailSent);

                    // TODO : Instead of loading the whole grid, update only that row.
                    //LoadValues();
                    LoadTestAndCandidateSessions();
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(EvaluateCandidate_bottomErrorMessageLabel,
                    EvaluateCandidate_topErrorMessageLabel, exp.Message);

                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Clicking on unschedule will open the popup window to get the 
        /// confirmation from user. Selecting Ok button will unschedule the candidate.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EvaluateCandidate_OkClick(object sender, EventArgs e)
        {
            try
            {
                string candidateSessionId = ViewState["CANDIDATE_SESSIONID"].ToString();
                int attemptId = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());

                bool isMailSent = true;
                bool isUnscheduled = true;

                // Get candidate details for the given candidate test session id and attempt id.
                CandidateTestSessionDetail candidateTestSessionDetail = new TestSchedulerBLManager().
                         GetCandidateTestSession(candidateSessionId, attemptId);

                // Update the session status as 'Not Scheduled'
                new TestConductionBLManager().UpdateSessionStatus(candidateTestSessionDetail,
                     Constants.CandidateAttemptStatus.NOT_SCHEDULED,
                     Constants.CandidateSessionStatus.NOT_SCHEDULED,
                     base.userID, isUnscheduled, out isMailSent);

                // Update candidate activity log. Handled in the trigger[TRUPDATE_SESSION_CANDIDATE]
                //if (candidateTestSessionDetail.CandidateID != null && candidateTestSessionDetail.CandidateID.Trim().Length > 0)
                //{
                //    new CommonBLManager().  InsertCandidateActivityLog
                //        (0, Convert.ToInt32(candidateTestSessionDetail.CandidateID), "Unscheduled from test", base.userID, 
                //        Constants.CandidateActivityLogType.CANDIDATE_UNSCHEDULED);
                //}

                if (isMailSent == false)
                {
                    EvaluateCandidate_topErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    EvaluateCandidate_bottomErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                }

                // TODO : Instead of loading the whole grid, update only that row.
                LoadTestAndCandidateSessions();
                base.ShowMessage(EvaluateCandidate_topSuccessMessageLabel,
                         EvaluateCandidate_bottomSuccessMessageLabel,
                         Resources.HCMResource.ScheduleCandidate_UnScheduledStatus);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EvaluateCandidate_bottomErrorMessageLabel,
                    EvaluateCandidate_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This handler gets triggered on clicking the reset button.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EvaluateCandidate_resetLinkButton_Click
            (object sender, EventArgs e)
        {
            Response.Redirect("ScheduleCandidate.aspx?m=2&s=1", false);
        }

        /// <summary>
        /// Show button will display the test session details and its corresponding 
        /// candidate session keys.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        public void EvaluateCandidate_showButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadDetails();
            }
            catch (Exception exp)
            {
                base.ShowMessage(EvaluateCandidate_bottomErrorMessageLabel,
                    EvaluateCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);

                if (exp.Message.Contains("Test Session Id not found"))
                {
                    EvaluateCandidate_testDetailsDiv.Style["display"] = "none";
                    EvaluateCandidate_testDetailsGridDiv.Style["display"] = "none";
                }
            }
        }

        private void LoadDetails()
        {
            // Check the user input, if it is valid, get the test session details.
            if (!IsValidData())
            {
                EvaluateCandidate_testDetailsDiv.Style["display"] = "none";
                EvaluateCandidate_testDetailsGridDiv.Style["display"] = "none";
                return;
            }

            // Reset default sort field and order keys.
            ViewState["SORT_ORDER"] = SortType.Ascending;
            ViewState["SORT_FIELD"] = "CandidateSessionId";

            // Load the candidate session details grid.
            LoadTestAndCandidateSessions();
        }

        /// <summary>
        /// Display the schedule/unschedule/reschedule/viewresults image buttons 
        /// according to the status field
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewRowEventArgs"/> that contains the event data.
        /// </param>
        protected void EvaluateCandidate_candidateSessionGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                Label EvaluateCandidate_statusLabel =
                    (Label)e.Row.FindControl("EvaluateCandidate_statusLabel");
                ImageButton EvaluateCandidate_evaluateImageButton =
                    (ImageButton)e.Row.FindControl("EvaluateCandidate_evaluateImageButtion");
                HiddenField EvaluateCandidate_candidateTestSessionIDHiddenField =
                    (HiddenField)e.Row.FindControl("EvaluateCandidate_candidateTestSessionIDHiddenField");
                HiddenField EvaluateCandidate_attemptIDHiddenField =
                    (HiddenField)e.Row.FindControl("EvaluateCandidate_attemptIDHiddenField");

                HyperLink EvaluateCandidate_viewTestSummaryHyperLink =
                    (HyperLink)e.Row.FindControl("EvaluateCandidate_viewTestSummaryHyperLink");
                HyperLink EvaluateCandidate_viewResultHyperLink =
                    (HyperLink)e.Row.FindControl("EvaluateCandidate_viewResultHyperLink");

                // Initialize all the image buttons visibility as false.
                EvaluateCandidate_viewResultHyperLink.Visible = false;
                EvaluateCandidate_evaluateImageButton.Visible = false;
                EvaluateCandidate_viewTestSummaryHyperLink.Visible = false;

                // Check the status and display the image buttons accordingly.
                if (EvaluateCandidate_statusLabel.Text == "Completed")
                    EvaluateCandidate_evaluateImageButton.Visible = true;

                if (EvaluateCandidate_statusLabel.Text == "Completed" ||
                    EvaluateCandidate_statusLabel.Text == "Quit" ||
                    EvaluateCandidate_statusLabel.Text == "Elapsed" ||
                    EvaluateCandidate_statusLabel.Text == "Evaluated")
                {
                    EvaluateCandidate_viewResultHyperLink.Visible = true;
                    EvaluateCandidate_viewTestSummaryHyperLink.Visible = true;
                }

                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                EvaluateCandidate_viewResultHyperLink.NavigateUrl = "~/TestMaker/TestResult.aspx?m=2&s=1" +
                    "&candidatesession=" + EvaluateCandidate_candidateTestSessionIDHiddenField.Value +
                    "&attemptid=" + EvaluateCandidate_attemptIDHiddenField.Value +
                    "&testkey=" + EvaluateCandidate_testIDLabel.Text +
                    "&parentpage=EVL_CAND";

                EvaluateCandidate_viewTestSummaryHyperLink.NavigateUrl =
                        "~/ReportCenter/CandidateTestDetails.aspx?m=3&s=0" +
                        "&testkey=" + EvaluateCandidate_testIDLabel.Text +
                        "&candidatesession=" + EvaluateCandidate_candidateTestSessionIDHiddenField.Value +
                        "&attemptid=" + EvaluateCandidate_attemptIDHiddenField.Value +
                        "&tab=CS" +
                        "&questionType=2" +
                        "&parentpage=" + Constants.ParentPage.TEST_STATISTICS_INFO;

                HyperLink EvaluateCandidate_candidateHyperLink =
                    (HyperLink)e.Row.FindControl("EvaluateCandidate_candidateHyperLink");

                HiddenField EvaluateCandidate_Grid_candidateIDHiddenField =
                    (HiddenField)e.Row.FindControl("EvaluateCandidate_Grid_candidateIDHiddenField");

                if (EvaluateCandidate_candidateHyperLink != null)
                {
                    if (EvaluateCandidate_Grid_candidateIDHiddenField != null)
                    {
                        EvaluateCandidate_candidateHyperLink.NavigateUrl = "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&puid="
                            + EvaluateCandidate_Grid_candidateIDHiddenField.Value;
                    }
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(EvaluateCandidate_bottomErrorMessageLabel,
                    EvaluateCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Check whether the clicked button is schedule/reschedule/unschedule/viewschedule
        /// and display the Modal popup accordingly.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewCommandEventArgs"/> that contains the event data.
        /// </param>
        protected void EvaluateCandidate_candidateSessionGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandSource is ImageButton))
                    return;

                HiddenField attemptIdHiddenField = ((ImageButton)e.CommandSource).Parent.
                        FindControl("EvaluateCandidate_attemptIDHiddenField") as HiddenField;
                HiddenField EvaluateCandidate_modifiedDateHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("EvaluateCandidate_modifiedDateHiddenField") as HiddenField;
                HiddenField EvaluateCandidate_Grid_ExpiryDateHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("EvaluateCandidate_Grid_ExpiryDateHiddenField") as HiddenField;
                HiddenField EvaluateCandidate_Grid_candidateEmailHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("EvaluateCandidate_Grid_candidateEmailHiddenField") as HiddenField;
                HiddenField EvaluateCandidate_Grid_candidateNameHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("EvaluateCandidate_Grid_candidateNameHiddenField") as HiddenField;
                HiddenField EvaluateCandidate_Grid_candidateIDHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("EvaluateCandidate_Grid_candidateIDHiddenField") as HiddenField;

                HiddenField EvaluateCandidate_Grid_emailReminderHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("EvaluateCandidate_Grid_emailReminderHiddenField") as HiddenField;

                // Evaluate Candidate
                if (e.CommandName == "evaluate")
                {
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    ViewState["STATUS"] = "SCHEDULE";

                    // Assign modified date in ViewState
                    ViewState["MODIFIED_DATE"] =
                        Convert.ToDateTime(EvaluateCandidate_modifiedDateHiddenField.Value);

                    EvaluateCandidate_titleLiteral.Text = "Evaluate";

                    // Clear the fields before displaying the schedule popup
                    ClearEvaluatePopup();
                    //EvaluateCandidate_answerGridView.DataSource = new TestSchedulerBLManager().GetCandidateOpenTextResponse
                    // (ViewState["CANDIDATE_SESSIONID"].ToString(), Convert.ToInt32(ViewState["ATTEMPT_ID"]));
                    //EvaluateCandidate_answerGridView.DataBind();
                    EvaluateCandidate_evaluateModalPopupExtender.Show();
                    EvaluateCandidate_answerGridView_marksObtainedTextBox.Attributes.Add("onkeyup", "return marksKeyDown('"
                    + EvaluateCandidate_answerGridView_marksObtainedTextBox.ClientID + "','" + EvaluateCandidate_scheduleErrorMsgLabel.ClientID + "')");
                    GetCandidateOpenTextQuestionDetails();

                }
            }
            catch (Exception exp)
            {
                // Catch the error
                Logger.ExceptionLog(exp);
                base.ShowMessage(EvaluateCandidate_bottomErrorMessageLabel,
                    EvaluateCandidate_topErrorMessageLabel, exp.Message);
            }
        }

        private void GetCandidateOpenTextQuestionDetails()
        {
            List<CandidateOpenTextResponseDetail> candidateOpenTextResponseDetail = new TestSchedulerBLManager().GetCandidateOpenTextResponse
            (ViewState["CANDIDATE_SESSIONID"].ToString(), Convert.ToInt32(ViewState["ATTEMPT_ID"]));
            if (candidateOpenTextResponseDetail.Count > 0)
            {
                ViewState["CURRENT_QUESTION_INDEX"] = 0;
                ViewState["QUESTIONS_DETAIL"] = candidateOpenTextResponseDetail;
                DisplayQuestion(candidateOpenTextResponseDetail);

            }
        }

        private void DisplayQuestion(List<CandidateOpenTextResponseDetail> candidateOpenTextResponseDetail)
        {
            if (ViewState["CURRENT_QUESTION_INDEX"] == null || ViewState["QUESTIONS_DETAIL"] == null)
                return;
            int currentIndex = Convert.ToInt32(ViewState["CURRENT_QUESTION_INDEX"].ToString());
            int totalQuestionCount = Convert.ToInt32(candidateOpenTextResponseDetail.Count);
            EvaluateCandidate_totalQuestCountLabel.Text = totalQuestionCount.ToString();


            BindToControls(currentIndex, candidateOpenTextResponseDetail);
        }

        private void BindToControls(int currentIndex, List<CandidateOpenTextResponseDetail> candidateOpenTextResponseDetail)
        {
            if (currentIndex == 0)
                EvaluateCandidate_prevButton.Enabled = false;
            else
                EvaluateCandidate_prevButton.Enabled = true; ;
            if (currentIndex == candidateOpenTextResponseDetail.Count - 1)
                EvaluateCandidate_nextButton.Enabled = false;
            else
                EvaluateCandidate_nextButton.Enabled = true;

            for (int i = 0; i < candidateOpenTextResponseDetail.Count; i++)
            {
                if (i == currentIndex)
                {
                    CandidateOpenTextResponseDetail QuestionDetail = candidateOpenTextResponseDetail[i];
                    EvaluateCandidate_answerGridView_questionKeyHiddenField.Value = QuestionDetail.QuestionKey;
                    EvaluateCandidate_answerGridView_marksHiddenField.Value = QuestionDetail.QuestionID.ToString(); ;
                    EvaluateCandidate_answerGridView_StatusHiddenField.Value = QuestionDetail.Status;
                    EvaluateCandidate_answerGridView_questionLabel.Text = QuestionDetail.QuestionDescription;
                    EvaluateCandidate_answerGridView_answerReferenceTextBox.Text = QuestionDetail.AnswerReference;
                    EvaluateCandidate_answerGridView_answerTextBox.Text = QuestionDetail.Answer;
                    EvaluateCandidate_answerGridView_marksLabel.Text = QuestionDetail.Marks.ToString();
                    EvaluateCandidate_answerGridView_marksObtainedTextBox.Text = QuestionDetail.MarksObtained.ToString();
                    EvaluateCandidate_currentQuestCountLabel.Text = (i + 1).ToString();
                    EvaluateCandidate_answerGridView_questionNo.Text = (i + 1).ToString();

                    EvaluateCandidate_answerGridView_marksObtainedTextBox.ReadOnly = false;
                    if (EvaluateCandidate_answerGridView_answerTextBox.Text.Length <= 0 || EvaluateCandidate_answerGridView_StatusHiddenField.Value.Trim() == "NOT_ATTD")
                    {
                        EvaluateCandidate_answerGridView_marksObtainedTextBox.Text = "0";
                        EvaluateCandidate_answerGridView_marksObtainedTextBox.ReadOnly = true;
                    }
                    else
                    {
                        EvaluateCandidate_answerGridView_marksObtainedTextBox.Text = EvaluateCandidate_answerGridView_marksObtainedTextBox.Text != "" ?
                            EvaluateCandidate_answerGridView_marksObtainedTextBox.Text : "";
                    }
                    break;
                }
            }
        }

        protected void EvaluateCandidate_prevButton_Click(object sender, EventArgs e)
        {
            if (ValidateCurrentControls())
            {
                List<CandidateOpenTextResponseDetail> candidateOpenTextResponseDetail = (List<CandidateOpenTextResponseDetail>)ViewState["QUESTIONS_DETAIL"];
                int currentIndex = Convert.ToInt32(ViewState["CURRENT_QUESTION_INDEX"].ToString());
                if (currentIndex != 0)
                {
                    ViewState["CURRENT_QUESTION_INDEX"] = --currentIndex;
                    BindToControls(currentIndex, candidateOpenTextResponseDetail);
                    EvaluateCandidate_evaluateModalPopupExtender.Show();
                }
            }
        }

        private bool ValidateCurrentControls()
        {
            EvaluateCandidate_scheduleErrorMsgLabel.Text = string.Empty;
            StringBuilder errorMessages = new StringBuilder();
            int marks = Convert.ToInt16(EvaluateCandidate_answerGridView_marksLabel.Text);
            if (EvaluateCandidate_answerGridView_marksObtainedTextBox.Text.ToString() == "")
            {
                if (errorMessages.Length > 0)
                    errorMessages.Append(string.Format(",  Answer is not evaluated"));
                else
                    errorMessages.Append(string.Format("Answer is not evaluated"));
            }
            int marksObtained = 0;
            if (EvaluateCandidate_answerGridView_marksObtainedTextBox.Text != "" && EvaluateCandidate_answerGridView_marksObtainedTextBox.Text != null)
            {
                marksObtained = Convert.ToInt16(EvaluateCandidate_answerGridView_marksObtainedTextBox.Text);
            }

            if (marksObtained > marks)
            {
                if (errorMessages.Length > 0)
                    errorMessages.Append(string.Format(", Marks obtained is greater than max marks"));
                else
                    errorMessages.Append(string.Format(" Marks obtained is greater than max marks"));
            }


            if (errorMessages.Length > 0)
            {
                EvaluateCandidate_scheduleErrorMsgLabel.Text = errorMessages.ToString();
                EvaluateCandidate_evaluateModalPopupExtender.Show();
                return false;
            }
            else
            {
                // int currentIndex = Convert.ToInt32(ViewState["CURRENT_QUESTION_INDEX"].ToString());
                string questionKey = EvaluateCandidate_answerGridView_questionKeyHiddenField.Value;
                List<CandidateOpenTextResponseDetail> candidateOpenTextResponseDetail = (List<CandidateOpenTextResponseDetail>)ViewState["QUESTIONS_DETAIL"];
                for (int i = 0; i < candidateOpenTextResponseDetail.Count; i++)
                {
                    if (questionKey == candidateOpenTextResponseDetail[i].QuestionKey)
                    {
                        candidateOpenTextResponseDetail[i].MarksObtained = Convert.ToInt16(EvaluateCandidate_answerGridView_marksObtainedTextBox.Text);
                        break;
                    }
                }
            }
            return true;
        }

        protected void EvaluateCandidate_nextButton_Click(object sender, EventArgs e)
        {
            if (ValidateCurrentControls())
            {
                List<CandidateOpenTextResponseDetail> candidateOpenTextResponseDetail = (List<CandidateOpenTextResponseDetail>)ViewState["QUESTIONS_DETAIL"];
                int currentIndex = Convert.ToInt32(ViewState["CURRENT_QUESTION_INDEX"].ToString());
                int totalQuestionCount = Convert.ToInt32(candidateOpenTextResponseDetail.Count);
                if (currentIndex != totalQuestionCount)
                {
                    ViewState["CURRENT_QUESTION_INDEX"] = ++currentIndex;
                    BindToControls(currentIndex, candidateOpenTextResponseDetail);
                    EvaluateCandidate_evaluateModalPopupExtender.Show();
                }
            }
        }
        /// <summary>
        /// set default value to marks field according to question status field        
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewRowEventArgs"/> that contains the event data.
        /// </param>
        protected void EvaluateCandidate_answerGridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    TextBox EvaluateCandidate_answerGridView_marksObtainedTextBox
                //        = (TextBox)e.Row.FindControl("EvaluateCandidate_answerGridView_marksObtainedTextBox");

                //    TextBox EvaluateCandidate_answerGridView_answerTextBox
                //        = (TextBox)e.Row.FindControl("EvaluateCandidate_answerGridView_answerTextBox");

                //    HiddenField EvaluateCandidate_answerGridView_StatusHiddenField
                //        = (HiddenField)e.Row.FindControl("EvaluateCandidate_answerGridView_StatusHiddenField");
                //    if (EvaluateCandidate_answerGridView_answerTextBox == null && EvaluateCandidate_answerGridView_StatusHiddenField == null)
                //        return;

                //    if (EvaluateCandidate_answerGridView_answerTextBox.Text.Length <= 0 || EvaluateCandidate_answerGridView_StatusHiddenField.Value.Trim() == "NOT_ATTD")
                //    {
                //        EvaluateCandidate_answerGridView_marksObtainedTextBox.Text = "0";
                //        EvaluateCandidate_answerGridView_marksObtainedTextBox.ReadOnly = true;
                //    }
                //    else
                //    {
                //        EvaluateCandidate_answerGridView_marksObtainedTextBox.Text = EvaluateCandidate_answerGridView_marksObtainedTextBox.Text != "" ? 
                //            EvaluateCandidate_answerGridView_marksObtainedTextBox.Text : "";
                //    }
                //}
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EvaluateCandidate_bottomErrorMessageLabel,
                    EvaluateCandidate_topErrorMessageLabel, exp.Message);
            }

        }

        /// <summary>
        /// Method to check whether given email id is valid or not
        /// </summary>
        /// <param name="strUserEmailId"></param>
        /// <returns></returns>
        private bool IsValidEmailAddress(string strUserEmailId)
        {
            //Regex regex = new Regex(".+@.+\\.[a-zA-Z]+", RegexOptions.Compiled);
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$", RegexOptions.Compiled);
            return regex.IsMatch(strUserEmailId);
        }

        /// <summary>
        /// Clicking on Save button in evaluate popup will trigger this method.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EvaluateCandidate_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateCurrentControls())
                {
                    string questionKey = EvaluateCandidate_answerGridView_questionKeyHiddenField.Value;
                    List<CandidateOpenTextResponseDetail> candidateOpenTextResponseDetail = (List<CandidateOpenTextResponseDetail>)ViewState["QUESTIONS_DETAIL"];
                    for (int i = 0; i < candidateOpenTextResponseDetail.Count; i++)
                    {
                        if (questionKey == candidateOpenTextResponseDetail[i].QuestionKey)
                        {
                            candidateOpenTextResponseDetail[i].MarksObtained = Convert.ToInt16(EvaluateCandidate_answerGridView_marksObtainedTextBox.Text);
                            break;
                        }
                    }
                }
                else
                    return;
                SaveScore(false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EvaluateCandidate_bottomErrorMessageLabel,
                    EvaluateCandidate_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Clicking on Submit button in evaluate popup will trigger this method.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EvaluateCandidate_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateCurrentControls())
                {
                    string questionKey = EvaluateCandidate_answerGridView_questionKeyHiddenField.Value;
                    List<CandidateOpenTextResponseDetail> candidateOpenTextResponseDetail = (List<CandidateOpenTextResponseDetail>)ViewState["QUESTIONS_DETAIL"];
                    for (int i = 0; i < candidateOpenTextResponseDetail.Count; i++)
                    {
                        if (questionKey == candidateOpenTextResponseDetail[i].QuestionKey)
                        {
                            candidateOpenTextResponseDetail[i].MarksObtained = Convert.ToInt16(EvaluateCandidate_answerGridView_marksObtainedTextBox.Text);
                            break;
                        }
                    }
                }
                else
                    return;
                SaveScore(true);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EvaluateCandidate_bottomErrorMessageLabel,
                    EvaluateCandidate_topErrorMessageLabel, exp.Message);
            }
        }

        private void SaveScore(bool submit)
        {
            //StringBuilder errorMessages = new StringBuilder();
            #region old validate
            //foreach (GridViewRow gvRow in EvaluateCandidate_answerGridView.Rows)
            //{
            //    if (((TextBox)gvRow.FindControl("EvaluateCandidate_answerGridView_marksObtainedTextBox")).Text.ToString() == "")
            //    {
            //        if (errorMessages.Length > 0)
            //            errorMessages.Append(string.Format(", Row {0}: is not evaluated", gvRow.RowIndex + 1));
            //        else
            //            errorMessages.Append(string.Format("Row {0}: is not evaluated", gvRow.RowIndex + 1));
            //    }
            //}

            //List<CandidateOpenTextResponseDetail> responses = GetScores();

            //int row = 1;
            //foreach (CandidateOpenTextResponseDetail response in responses)
            //{
            //    if (response.MarksObtained > response.Marks)
            //    {
            //        if (errorMessages.Length > 0)
            //            errorMessages.Append(string.Format(", Row {0}: Marks obtained in greater than max marks", row));
            //        else
            //            errorMessages.Append(string.Format("Row {0}: Marks obtained in greater than max marks", row));
            //    }
            //    row++;
            //}

            //if (errorMessages.Length > 0)
            //{
            //    EvaluateCandidate_scheduleErrorMsgLabel.Text = errorMessages.ToString();
            //    EvaluateCandidate_evaluateModalPopupExtender.Show();
            //    return;
            //}
            //new TestSchedulerBLManager().UpdateCandidateOpenTextResponseScore(ViewState["CANDIDATE_SESSIONID"].ToString(),
            //    Convert.ToInt32(ViewState["ATTEMPT_ID"]), responses);

            //if (submit)
            //{
            //    base.ShowMessage(EvaluateCandidate_topSuccessMessageLabel, EvaluateCandidate_bottomSuccessMessageLabel,
            //        "Scores submitted successfully");

            //    new TestSchedulerBLManager().UpdateCandidateSessionStatus(ViewState["CANDIDATE_SESSIONID"].ToString(),
            //        Convert.ToInt32(ViewState["ATTEMPT_ID"]), Constants.CandidateSessionStatus.EVALUATED, base.userID);

            //    LoadDetails();
            //}
            //else
            //{
            //    base.ShowMessage(EvaluateCandidate_topSuccessMessageLabel, EvaluateCandidate_bottomSuccessMessageLabel,
            //        "Scores saved successfully");
            //}

            #endregion

            if (ViewState["QUESTIONS_DETAIL"] != null)
            {
                int evlRemainingCount = 0;
                List<CandidateOpenTextResponseDetail> candidateOpenTextResponseDetail = (List<CandidateOpenTextResponseDetail>)ViewState["QUESTIONS_DETAIL"];
                foreach (var _candidateOpenTextResponseDetail in candidateOpenTextResponseDetail)
                {
                    if (_candidateOpenTextResponseDetail.MarksObtained.ToString() == "")
                    {
                        ++evlRemainingCount;
                    }
                }
                if (evlRemainingCount > 0 && submit)
                {
                    EvaluateCandidate_scheduleErrorMsgLabel.Text = evlRemainingCount + " question(s) are not evaluated. Clicking on Save will save the changes and later evaluator open it again to continue evaluation.";
                    EvaluateCandidate_evaluateModalPopupExtender.Show();
                    return;
                }
                else
                {
                    new TestSchedulerBLManager().UpdateCandidateOpenTextResponseScore(ViewState["CANDIDATE_SESSIONID"].ToString(),
                        Convert.ToInt32(ViewState["ATTEMPT_ID"]), candidateOpenTextResponseDetail);

                    if (submit)
                    {
                        base.ShowMessage(EvaluateCandidate_topSuccessMessageLabel, EvaluateCandidate_bottomSuccessMessageLabel,
                            "Scores submitted successfully");

                        new TestSchedulerBLManager().UpdateCandidateSessionStatus(ViewState["CANDIDATE_SESSIONID"].ToString(),
                            Convert.ToInt32(ViewState["ATTEMPT_ID"]), Constants.CandidateSessionStatus.EVALUATED, base.userID);

                        LoadDetails();
                    }
                    else
                    {
                        base.ShowMessage(EvaluateCandidate_topSuccessMessageLabel, EvaluateCandidate_bottomSuccessMessageLabel,
                            "Scores saved successfully");
                    }
                }
            }

        }

        private List<CandidateOpenTextResponseDetail> GetScores()
        {
            List<CandidateOpenTextResponseDetail> responses = new List<CandidateOpenTextResponseDetail>();
            //CandidateOpenTextResponseDetail response = null;
            //foreach (GridViewRow row in EvaluateCandidate_answerGridView.Rows)
            //{
            //    response = new CandidateOpenTextResponseDetail();

            //    response.QuestionKey = ((HiddenField)row.FindControl("EvaluateCandidate_answerGridView_questionKeyHiddenField")).Value;

            //    int marks = 0;
            //    int.TryParse(((HiddenField)row.FindControl("EvaluateCandidate_answerGridView_marksHiddenField")).Value.Trim(), out marks);
            //    response.Marks = marks;

            //    int marksObtained = 0;
            //    int.TryParse(((TextBox)row.FindControl("EvaluateCandidate_answerGridView_marksObtainedTextBox")).Text.Trim(), out marksObtained);
            //    response.MarksObtained = marksObtained;

            //    responses.Add(response);
            //}

            return responses;
        }

        #endregion Event Handlers

        #region Protected Methods

        /// <summary>
        /// Return the proper status message according to the DB session values.
        /// </summary>
        /// <param name="pStatus">
        /// A <see cref="string"/> that contains the test status
        /// </param>
        /// <returns>
        /// It will return the test status.
        /// </returns>
        protected string GetStatus(string pStatus)
        {
            string status = "";
            switch (pStatus.Trim())
            {
                case Constants.CandidateSessionStatus.ELAPSED:
                    status = "Elapsed";
                    break;
                case Constants.CandidateSessionStatus.IN_PROGRESS:
                    status = "In Progress";
                    break;
                case Constants.CandidateSessionStatus.NOT_SCHEDULED:
                    status = "Not Scheduled";
                    break;
                case Constants.CandidateSessionStatus.QUIT:
                    status = "Quit";
                    break;
                case Constants.CandidateSessionStatus.SCHEDULED:
                    status = "Scheduled";
                    break;
                case Constants.CandidateSessionStatus.CANCELLED:
                    status = "Cancelled";
                    break;
                case Constants.CandidateSessionStatus.COMPLETED:
                    status = "Completed";
                    break;
                case Constants.CandidateSessionStatus.EVALUATED:
                    status = "Evaluated";
                    break;
                default:
                    status = "Not Scheduled";
                    break;
            }
            return status;
        }

        /// <summary>
        /// This method returns true when CyberProctoring is enabled as well as 
        /// the candidate session is completed.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds the status of the candidate.
        /// </param>
        /// <returns></returns>
        protected bool IsCyberProctoring(string status)
        {
            TestSessionDetail testSession = Cache["SEARCH_DATATABLE"] as TestSessionDetail;
            if (testSession != null)
                return (status.Trim() == Constants.CandidateSessionStatus.COMPLETED
                    && testSession.IsCyberProctoringEnabled) ? true : false;
            else
                return false;
        }

        /// <summary>
        /// This method clears the unwanted values in the evaluate popup.
        /// </summary>
        protected void ClearEvaluatePopup()
        {
            EvaluateCandidate_scheduleErrorMsgLabel.Text = string.Empty;
        }

        /// <summary>
        /// This method clears the test session and candidate session details
        /// </summary>
        protected void ClearSessionValues()
        {
            try
            {
                EvaluateCandidate_testDetailsDiv.Style["display"] = "none";
                EvaluateCandidate_testDetailsGridDiv.Style["display"] = "none";
                EvaluateCandidate_candidateSessionHeader.Style["display"] = "none";

                // Store the scheduled date in view state

                EvaluateCandidate_testNameLabel.Text = "";
                EvaluateCandidate_testIDLabel.Text = "";
                EvaluateCandidate_emailLabel.Text = "";
                EvaluateCandidate_testAuthorLabel.Text = "";
                EvaluateCandidate_testDescLiteral.Text = "";
                EvaluateCandidate_timeLimitLabel.Text = "";
                EvaluateCandidate_expiryDateLabel.Text = "";
                EvaluateCandidate_positionProfileLabel.Text = "";
                EvaluateCandidate_instructionsLiteral.Text = "";
                EvaluateCandidate_cyberProctorateLabel.Text = "";
                EvaluateCandidate_displayResultsLabel.Text = "";

                Cache["SEARCH_DATATABLE"] = "";
                ViewState["TEST_SESSION_ID"] = "";
                ViewState["CANDIDATE_SESSION_IDS"] = "";
                EvaluateCandidate_candidateSessionGridView.DataSource = null;
                EvaluateCandidate_candidateSessionGridView.DataBind();
            }
            catch (Exception exp)
            {
                base.ShowMessage(EvaluateCandidate_bottomErrorMessageLabel,
                    EvaluateCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Protected Methods

        #region Sorting Related Methods

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void EvaluateCandidate_candidateSessionGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }

                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] = ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                LoadTestAndCandidateSessions();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }
        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void EvaluateCandidate_candidateSessionGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                            (EvaluateCandidate_candidateSessionGridView,
                            (string)ViewState["SORT_FIELD"]);
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            (SortType)ViewState["SORT_ORDER"]);
                    }
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(EvaluateCandidate_bottomErrorMessageLabel,
                    EvaluateCandidate_topErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Sorting Related Methods

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            // Maintain the expand/restore gridview.
            if (!Utility.IsNullOrEmpty(EvaluateCandidate_isMaximizedHiddenField.Value) &&
                EvaluateCandidate_isMaximizedHiddenField.Value == "Y")
            {
                EvaluateCandidate_testSessionDetailsDiv.Style["display"] = "none";
                EvaluateCandidate_searchResultsUpSpan.Style["display"] = "block";
                EvaluateCandidate_searchResultsDownSpan.Style["display"] = "none";
                EvaluateCandidate_candidateSessionGridViewDIV.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                EvaluateCandidate_testSessionDetailsDiv.Style["display"] = "block";
                EvaluateCandidate_searchResultsUpSpan.Style["display"] = "none";
                EvaluateCandidate_searchResultsDownSpan.Style["display"] = "block";
                EvaluateCandidate_candidateSessionGridViewDIV.Style["height"] = RESTORED_HEIGHT;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE]))
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(EvaluateCandidate_isMaximizedHiddenField.Value))
                    ((TestScheduleSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE]).IsMaximized =
                        EvaluateCandidate_isMaximizedHiddenField.Value == "Y" ? true : false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadTestAndCandidateSessions()
        {
            try
            {
                TestScheduleSearchCriteria scheduleCandidateSearchCriteria = new TestScheduleSearchCriteria();

                // Check if the search criteria fields are empty.
                // If this is empty, pass null. Or else, pass its value.
                scheduleCandidateSearchCriteria.TestSessionID =
                    EvaluateCandidate_testSessionIdTextBox.Text.Trim() == "" ?
                    string.Empty : EvaluateCandidate_testSessionIdTextBox.Text.Trim();

                scheduleCandidateSearchCriteria.CandidateSessionIDs =
                    EvaluateCandidate_candidateSessionIdTextBox.Text.Trim() == "" ?
                    string.Empty : EvaluateCandidate_candidateSessionIdTextBox.Text.Trim();

                scheduleCandidateSearchCriteria.IsMaximized =
                    EvaluateCandidate_isMaximizedHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

                scheduleCandidateSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
                scheduleCandidateSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();

                // Keep the search criteria in Session if the page is launched from 
                // somewhere else.
                Session[Constants.SearchCriteriaSessionKey.SEARCH_SCHEDULE_CANDIDATE] = scheduleCandidateSearchCriteria;

                TestSessionDetail testSession = new TestSchedulerBLManager().
                    GetTestSessionDetail(scheduleCandidateSearchCriteria,
                        scheduleCandidateSearchCriteria.SortExpression,
                        scheduleCandidateSearchCriteria.SortDirection, base.userID);

                if (testSession != null)
                {
                    UserDetail userDetail = new UserDetail();
                    userDetail = (UserDetail)Session["USER_DETAIL"];

                    EvaluateCandidate_testDetailsDiv.Style["display"] = "block";
                    EvaluateCandidate_testDetailsGridDiv.Style["display"] = "block";
                    EvaluateCandidate_candidateSessionHeader.Style["display"] = "block";

                    // Store the scheduled date in view state
                    EvaluateCandidate_testNameLabel.Text = testSession.TestName;
                    EvaluateCandidate_testIDLabel.Text = testSession.TestID;
                    EvaluateCandidate_emailLabel.Text = testSession.TestSessionAuthorEmail;
                    EvaluateCandidate_testAuthorLabel.Text = testSession.TestSessionAuthor;
                    EvaluateCandidate_testDescLiteral.Text =
                        testSession.TestSessionDesc == null ? testSession.TestSessionDesc :
                        testSession.TestSessionDesc.ToString().Replace(Environment.NewLine, "<br />");
                    EvaluateCandidate_timeLimitLabel.Text =
                        Utility.ConvertSecondsToHoursMinutesSeconds(Convert.ToInt32(testSession.TimeLimit.ToString()));
                    EvaluateCandidate_expiryDateLabel.Text = GetDateFormat(testSession.ExpiryDate);
                    EvaluateCandidate_positionProfileLabel.Text = testSession.PositionProfileName;
                    EvaluateCandidate_instructionsLiteral.Text =
                        testSession.Instructions == null ? testSession.Instructions :
                        testSession.Instructions.ToString().Replace(Environment.NewLine, "<br />");
                    EvaluateCandidate_cyberProctorateLabel.Text = testSession.IsCyberProctoringEnabled ? "Yes" : "No";
                    EvaluateCandidate_displayResultsLabel.Text = testSession.IsDisplayResultsToCandidate ? "Yes" : "No";

                    Cache["SEARCH_DATATABLE"] = testSession;
                    ViewState["TEST_SESSION_ID"] = EvaluateCandidate_testSessionIdTextBox.Text.Trim();
                    ViewState["CANDIDATE_SESSION_IDS"] = EvaluateCandidate_candidateSessionIdTextBox.Text.Trim();
                    EvaluateCandidate_candidateSessionGridView.DataSource = testSession.CandidateTestSessions;
                    EvaluateCandidate_candidateSessionGridView.DataBind();
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(EvaluateCandidate_bottomErrorMessageLabel,
                    EvaluateCandidate_topErrorMessageLabel, exp.Message);

                Logger.ExceptionLog(exp);
                ClearSessionValues();
            }
        }

        /// <summary>
        /// Method that will fill the search criteria to the appropriate fields.
        /// </summary>
        /// <param name="scheduleSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the search
        /// criteria fields.
        /// </param>
        private void FillSearchCriteria(TestScheduleSearchCriteria scheduleSearchCriteria)
        {
            if (scheduleSearchCriteria.TestSessionID != null)
                EvaluateCandidate_testSessionIdTextBox.Text = scheduleSearchCriteria.TestSessionID;

            if (scheduleSearchCriteria.EMail != null)
                EvaluateCandidate_candidateSessionIdTextBox.Text = scheduleSearchCriteria.CandidateSessionIDs;

            EvaluateCandidate_isMaximizedHiddenField.Value =
                    scheduleSearchCriteria.IsMaximized == true ? "Y" : "N";

            ViewState["SORT_ORDER"] = scheduleSearchCriteria.SortDirection;
            ViewState["SORT_FIELD"] = scheduleSearchCriteria.SortExpression;

            // Apply search
            LoadTestAndCandidateSessions();
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// This method checks the user input. If both testSession and CandidateSession IDs 
        /// are not given, then return false.
        /// </summary>
        /// <returns>
        /// </returns>
        protected override bool IsValidData()
        {
            if (EvaluateCandidate_testSessionIdTextBox.Text != "" ||
                EvaluateCandidate_candidateSessionIdTextBox.Text != "")
            {
                // This method passes the list of candidateSession ids separated by comma and
                // it returns the candidate session keys which are not existing in test session.
                List<string> notMatchedCandSessionKeys =
                    new TestSchedulerBLManager().ValidateTestSessionInput
                    (EvaluateCandidate_testSessionIdTextBox.Text.Trim(),
                    EvaluateCandidate_candidateSessionIdTextBox.Text.Trim(), base.userID);

                if (notMatchedCandSessionKeys != null && notMatchedCandSessionKeys.Count > 0)
                {
                    base.ShowMessage(EvaluateCandidate_bottomErrorMessageLabel,
                        EvaluateCandidate_topErrorMessageLabel, "No matching records found");
                    return false;
                }
                else
                    return true;
            }
            base.ShowMessage(EvaluateCandidate_bottomErrorMessageLabel,
                EvaluateCandidate_topErrorMessageLabel,
                Resources.HCMResource.ScheduleCandidate_EmptySessionIds);
            return false;
        }

        /// <summary>
        /// This method loads the test session and scheduled candidate details.
        /// </summary>
        protected override void LoadValues()
        {
            // Assign the client side onclick events to header
            EvaluateCandidate_searchTestResultsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                EvaluateCandidate_candidateSessionGridViewDIV.ClientID + "','" +
                EvaluateCandidate_testSessionDetailsDiv.ClientID + "','" +
                EvaluateCandidate_searchResultsUpSpan.ClientID + "','" +
                EvaluateCandidate_searchResultsDownSpan.ClientID + "','" +
                EvaluateCandidate_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");


            // Check if page is navigated from talentscout and position profile is found
            if (Request.QueryString["parentpage"] != null &&
                Request.QueryString["parentpage"].Trim().ToUpper() == Constants.ParentPage.TALENT_SCOUT &&
                Request.QueryString["positionprofileid"] != null)
            {
                // Get position profile ID.
                int positionProfileID = 0;
                if (int.TryParse(Request.QueryString["positionprofileid"].Trim(), out positionProfileID) == true)
                {
                    EvaluateCandidate_testSessionImageButton.Attributes.Add("onclick", "return LoadTestSessionWithPositionProfile('"
                    + EvaluateCandidate_testSessionIdTextBox.ClientID + "','" +
                    positionProfileID + "')");
                }
                else
                {
                    EvaluateCandidate_testSessionImageButton.Attributes.Add("onclick", "return LoadTestSession('"
                    + EvaluateCandidate_testSessionIdTextBox.ClientID + "')");
                }
            }
            else
            {
                EvaluateCandidate_testSessionImageButton.Attributes.Add("onclick", "return LoadTestSession('"
                    + EvaluateCandidate_testSessionIdTextBox.ClientID + "')");
            }

            EvaluateCandidate_testSessionIdTextBox.Attributes.Add("onkeydown", "TestSessionKeyDown(event,'"
                + EvaluateCandidate_showButton.ClientID + "')");

            // Assign default sort field and order keys.
            if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                ViewState["SORT_ORDER"] = SortType.Ascending;

            if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                ViewState["SORT_FIELD"] = "CandidateSessionID";
        }

        #endregion Protected Overridden Methods
    }
}