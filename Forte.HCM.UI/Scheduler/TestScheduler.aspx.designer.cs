﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.Scheduler {
    
    
    public partial class TestScheduler {
        
        /// <summary>
        /// TestScheduler_headerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal TestScheduler_headerLiteral;
        
        /// <summary>
        /// TestScheduler_topScheduleCandidateButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button TestScheduler_topScheduleCandidateButton;
        
        /// <summary>
        /// TestScheduler_topResetLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton TestScheduler_topResetLinkButton;
        
        /// <summary>
        /// TestScheduler_topCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton TestScheduler_topCancelLinkButton;
        
        /// <summary>
        /// TestScheduler_topSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_topSuccessMessageLabel;
        
        /// <summary>
        /// TestScheduler_topErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_topErrorMessageLabel;
        
        /// <summary>
        /// TestScheduler_searchCriteriasDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl TestScheduler_searchCriteriasDiv;
        
        /// <summary>
        /// TestScheduler_candidateNameHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_candidateNameHeadLabel;
        
        /// <summary>
        /// TestScheduler_candidateNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestScheduler_candidateNameTextBox;
        
        /// <summary>
        /// TestScheduler_candidateNameImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton TestScheduler_candidateNameImageButton;
        
        /// <summary>
        /// TestScheduler_emailIdHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_emailIdHeadLabel;
        
        /// <summary>
        /// TestScheduler_emailIdTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestScheduler_emailIdTextBox;
        
        /// <summary>
        /// TestScheduler_testSessionAuthorHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_testSessionAuthorHeadLabel;
        
        /// <summary>
        /// TestScheduler_testSessionAuthorTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestScheduler_testSessionAuthorTextBox;
        
        /// <summary>
        /// TestScheduler_testScheduleAuthorImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton TestScheduler_testScheduleAuthorImageButton;
        
        /// <summary>
        /// TestScheduler_testSessionIdHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_testSessionIdHeadLabel;
        
        /// <summary>
        /// TestScheduler_testSessionIdTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestScheduler_testSessionIdTextBox;
        
        /// <summary>
        /// TestScheduler_testNameHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_testNameHeadLabel;
        
        /// <summary>
        /// TestScheduler_testNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestScheduler_testNameTextBox;
        
        /// <summary>
        /// TestScheduler_testCompletionHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_testCompletionHeadLabel;
        
        /// <summary>
        /// TestScheduler_statusDropDownList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList TestScheduler_statusDropDownList;
        
        /// <summary>
        /// TestScheduler_statusImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton TestScheduler_statusImageButton;
        
        /// <summary>
        /// TestScheduler_searchButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button TestScheduler_searchButton;
        
        /// <summary>
        /// TestScheduler_searchResultsTR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow TestScheduler_searchResultsTR;
        
        /// <summary>
        /// TestScheduler_searchResultsLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal TestScheduler_searchResultsLiteral;
        
        /// <summary>
        /// TestScheduler_sortHelpLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_sortHelpLabel;
        
        /// <summary>
        /// TestScheduler_searchResultsUpSpan control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl TestScheduler_searchResultsUpSpan;
        
        /// <summary>
        /// TestScheduler_searchResultsUpImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image TestScheduler_searchResultsUpImage;
        
        /// <summary>
        /// TestScheduler_searchResultsDownSpan control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl TestScheduler_searchResultsDownSpan;
        
        /// <summary>
        /// TestScheduler_searchResultsDownImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image TestScheduler_searchResultsDownImage;
        
        /// <summary>
        /// TestScheduler_restoreHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField TestScheduler_restoreHiddenField;
        
        /// <summary>
        /// TestScheduler_schedulerDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl TestScheduler_schedulerDiv;
        
        /// <summary>
        /// TestScheduler_scheduleGridView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView TestScheduler_scheduleGridView;
        
        /// <summary>
        /// TestScheduler_pagingNavigator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.PageNavigator TestScheduler_pagingNavigator;
        
        /// <summary>
        /// TestScheduler_bottomSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_bottomSuccessMessageLabel;
        
        /// <summary>
        /// TestScheduler_bottomErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_bottomErrorMessageLabel;
        
        /// <summary>
        /// TestScheduler_bottomScheduleCandidateButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button TestScheduler_bottomScheduleCandidateButton;
        
        /// <summary>
        /// TestScheduler_bottomResetLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton TestScheduler_bottomResetLinkButton;
        
        /// <summary>
        /// TestScheduler_bottomCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton TestScheduler_bottomCancelLinkButton;
        
        /// <summary>
        /// TestScheduler_schedulePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel TestScheduler_schedulePanel;
        
        /// <summary>
        /// TestScheduler_hiddenButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button TestScheduler_hiddenButton;
        
        /// <summary>
        /// TestScheduler_scheduleCandiateLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal TestScheduler_scheduleCandiateLiteral;
        
        /// <summary>
        /// TestScheduler_topCancelImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton TestScheduler_topCancelImageButton;
        
        /// <summary>
        /// TestScheduler_scheduleErrorMsgLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_scheduleErrorMsgLabel;
        
        /// <summary>
        /// TestScheduler_Popup_candidateNameHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_Popup_candidateNameHeadLabel;
        
        /// <summary>
        /// TestScheduler_Popup_candidateNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestScheduler_Popup_candidateNameTextBox;
        
        /// <summary>
        /// TestScheduler_Popup_candidateNameImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton TestScheduler_Popup_candidateNameImageButton;
        
        /// <summary>
        /// TestScheduler_Popup_emailHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_Popup_emailHeadLabel;
        
        /// <summary>
        /// TestScheduler_Popup_emailTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestScheduler_Popup_emailTextBox;
        
        /// <summary>
        /// TestScheduler_Popup_scheduleDateLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_Popup_scheduleDateLabel;
        
        /// <summary>
        /// TestScheduler_Popup_scheduleDateTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TestScheduler_Popup_scheduleDateTextBox;
        
        /// <summary>
        /// TestScheduler_Popup_calendarImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton TestScheduler_Popup_calendarImageButton;
        
        /// <summary>
        /// TestScheduler_MaskedEditExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditExtender TestScheduler_MaskedEditExtender;
        
        /// <summary>
        /// TestScheduler_MaskedEditValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditValidator TestScheduler_MaskedEditValidator;
        
        /// <summary>
        /// TestScheduler_customCalendarExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.CalendarExtender TestScheduler_customCalendarExtender;
        
        /// <summary>
        /// TestScheduler_Popup_sessionExpiryDateHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_Popup_sessionExpiryDateHeadLabel;
        
        /// <summary>
        /// TestScheduler_Popup_sessionExpiryDateValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_Popup_sessionExpiryDateValueLabel;
        
        /// <summary>
        /// TestScheduler_Popup_candidateIDHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField TestScheduler_Popup_candidateIDHiddenField;
        
        /// <summary>
        /// TestScheduler_saveButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button TestScheduler_saveButton;
        
        /// <summary>
        /// TestScheduler_bottomCloseLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton TestScheduler_bottomCloseLinkButton;
        
        /// <summary>
        /// TestScheduler_scheduleModalPopupExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender TestScheduler_scheduleModalPopupExtender;
        
        /// <summary>
        /// TestScheduler_unscheduleCandidatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel TestScheduler_unscheduleCandidatePanel;
        
        /// <summary>
        /// TestScheduler_hiddenUnscheduleButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button TestScheduler_hiddenUnscheduleButton;
        
        /// <summary>
        /// TestScheduler_inactiveConfirmMsgControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.ConfirmMsgControl TestScheduler_inactiveConfirmMsgControl;
        
        /// <summary>
        /// TestScheduler_unschedulePopupExtenter control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender TestScheduler_unschedulePopupExtenter;
        
        /// <summary>
        /// TestScheduler_hiddenViewScheduleButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button TestScheduler_hiddenViewScheduleButton;
        
        /// <summary>
        /// TestScheduler_hiddenViewSchedulePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel TestScheduler_hiddenViewSchedulePanel;
        
        /// <summary>
        /// TestScheduler_viewTestSchedule control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.TestScheduleControl TestScheduler_viewTestSchedule;
        
        /// <summary>
        /// TestScheduler_viewScheduleModalPopupExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender TestScheduler_viewScheduleModalPopupExtender;
        
        /// <summary>
        /// TestScheduler_inactivepopupPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel TestScheduler_inactivepopupPanel;
        
        /// <summary>
        /// TestScheduler_inactiveHiddenButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button TestScheduler_inactiveHiddenButton;
        
        /// <summary>
        /// TestScheduler_inactivePopupExtenderControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.ConfirmMsgControl TestScheduler_inactivePopupExtenderControl;
        
        /// <summary>
        /// TestScheduler_inactivepopupExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender TestScheduler_inactivepopupExtender;
        
        /// <summary>
        /// TestScheduler_cancelTestPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel TestScheduler_cancelTestPanel;
        
        /// <summary>
        /// TestScheduler_cancelReasonHiddenButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button TestScheduler_cancelReasonHiddenButton;
        
        /// <summary>
        /// TestScheduler_cancelReasonLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestScheduler_cancelReasonLiteral;
        
        /// <summary>
        /// TestScheduler_cancelReasonCancelImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton TestScheduler_cancelReasonCancelImageButton;
        
        /// <summary>
        /// TestScheduler_cancelTestReasonDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl TestScheduler_cancelTestReasonDiv;
        
        /// <summary>
        /// TestScheduler_cancelReasonCancellationButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton TestScheduler_cancelReasonCancellationButton;
        
        /// <summary>
        /// TestScheduler_cancelSessionModalPopupExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender TestScheduler_cancelSessionModalPopupExtender;
        
        /// <summary>
        /// TestScheduler_testSessionAuthorIdHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField TestScheduler_testSessionAuthorIdHiddenField;
        
        /// <summary>
        /// TestScheduler_testSessionAuthorHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField TestScheduler_testSessionAuthorHiddenField;
        
        /// <summary>
        /// TestScheduler_candidateSessionIDHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField TestScheduler_candidateSessionIDHiddenField;
        
        /// <summary>
        /// TestScheduler_testSessionIDHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField TestScheduler_testSessionIDHiddenField;
        
        /// <summary>
        /// TestScheduler_isMaximizedHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField TestScheduler_isMaximizedHiddenField;
        
        /// <summary>
        /// TestScheduler_expiryDateHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField TestScheduler_expiryDateHiddenField;
        
        /// <summary>
        /// TestScheduler_candidateEmailHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField TestScheduler_candidateEmailHiddenField;
        
        /// <summary>
        /// TestScheduler_sessoinExpiryDateHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField TestScheduler_sessoinExpiryDateHiddenField;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.OTMMaster Master {
            get {
                return ((Forte.HCM.UI.MasterPages.OTMMaster)(base.Master));
            }
        }
    }
}
