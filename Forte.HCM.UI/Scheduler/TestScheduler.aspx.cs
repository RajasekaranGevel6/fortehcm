﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestScheduler.cs
// File that represents the user interface for searching scheduled candidates.
// This will interact with TestSchedulerBLManager to retrieve the candidate
// information from the database.

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Scheduler
{
    /// <summary>
    /// Class that represents the user interface for searching scheduled candidates.
    /// This will also provide to the recruiter to reschedule, unschedule, view the 
    /// candidate details, and results information.
    /// </summary>
    public partial class TestScheduler : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Event Handlers                                                 

        /// <summary>
        /// Handler that will call when the page is loade. 
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/>that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                TestScheduler_pagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler(TestScheduler_pagingNavigator_PageNumberClick);

                Master.SetPageCaption("Test Scheduler");
                CheckAndSetExpandorRestore();

                if (!IsPostBack)
                {
                    // Load default values.
                    LoadValues();

                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU
                         || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_LANDING
                         || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_ASSESSMENT))
                    {
                        base.ClearSearchCriteriaSession();

                        // Load the candidate session details
                        LoadScheduleDetails(1);
                    }
                    else

                        if (Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_SCHEDULER] != null)
                        {
                            // Check if page is redirected from any child page. If the page
                            // if redirected from any child page, fill the search criteria
                            // and apply the search.
                            FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_SCHEDULER]
                                as TestScheduleSearchCriteria);

                            // Reload the maximize hidden value.
                            CheckAndSetExpandorRestore();
                        }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestScheduler_topErrorMessageLabel,
                    TestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will fire when the page number is clicked on gridview.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/> that contains the event data.
        /// </param>
        void TestScheduler_pagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            LoadScheduleDetails(e.PageNumber);
            ViewState["PAGE_NUMBER"] = e.PageNumber;
        }

        /// <summary>
        /// Handler that will call when the header of the gridview column is clicked.
        /// Then, it will sort the column accordingly.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that contains the event data.
        /// </param>
        protected void TestScheduler_scheduleGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }

                // Assign sort expression column to the viewstate
                if (ViewState["SORT_EXPRESSION"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_DIRECTION_KEY"] =
                        ((SortType)ViewState["SORT_DIRECTION_KEY"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_DIRECTION_KEY"] = SortType.Descending;
                else
                    ViewState["SORT_DIRECTION_KEY"] = SortType.Ascending;
                ViewState["SORT_EXPRESSION"] = e.SortExpression;

                // Reload the pagination and gridview controls
                TestScheduler_pagingNavigator.Reset();
                LoadScheduleDetails(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestScheduler_topErrorMessageLabel,
                    TestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when every row is getting loaded with the data.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that contains the event data.
        /// </param>
        protected void TestScheduler_scheduleGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField TestScheduler_candidateTestSessionIDHiddenField = (HiddenField)
                        e.Row.FindControl("TestScheduler_candidateTestSessionIDHiddenField");
                    HiddenField TestScheduler_Grid_testSessionIDHiddenField = (HiddenField)
                        e.Row.FindControl("TestScheduler_Grid_testSessionIDHiddenField");
                    HiddenField TestScheduler_Grid_ExpiryDateHiddenField = (HiddenField)
                        e.Row.FindControl("TestScheduler_Grid_ExpiryDateHiddenField");
                    HiddenField TestScheduler_Grid_testIDHiddenField = (HiddenField)
                        e.Row.FindControl("TestScheduler_Grid_testIDHiddenField");
                    HiddenField TestScheduler_attemptIDHiddenField = (HiddenField)
                        e.Row.FindControl("TestScheduler_attemptIDHiddenField");

                    Label TestScheduler_statusLabel = (Label)
                        e.Row.FindControl("TestScheduler_statusLabel");

                    HyperLink TestScheduler_viewResultHyperLinkButton =
                        (HyperLink)e.Row.FindControl("TestScheduler_viewResultHyperLinkButton");

                    HyperLink TestScheduler_canidateReportHyperLinkButton =
                        (HyperLink)e.Row.FindControl("TestScheduler_canidateReportHyperLinkButton");

                    ImageButton TestScheduler_viewShedulerImageButton =
                        (ImageButton)e.Row.FindControl("TestScheduler_viewShedulerImageButton");

                    ImageButton TestScheduler_rescheduleImageButton =
                        (ImageButton)e.Row.FindControl("TestScheduler_rescheduleImageButton");

                    ImageButton TestScheduler_unscheduleImageButton =
                        (ImageButton)e.Row.FindControl("TestScheduler_unscheduleImageButton");

                    ImageButton TestScheduler_viewCancelReasonImageButton =
                        (ImageButton)e.Row.FindControl("TestScheduler_viewCancelReasonImageButton");

                    // Initialize all the image buttons visibility as false.
                    TestScheduler_viewShedulerImageButton.Visible = false;
                    TestScheduler_rescheduleImageButton.Visible = false;
                    TestScheduler_unscheduleImageButton.Visible = false;
                    TestScheduler_viewResultHyperLinkButton.Visible = false;
                    TestScheduler_canidateReportHyperLinkButton.Visible = false;
                    TestScheduler_viewCancelReasonImageButton.Visible = false;
                    
                    // Check the status and display the image buttons accordingly.
                    if (TestScheduler_statusLabel.Text == "Scheduled")
                    {
                        TestScheduler_viewShedulerImageButton.Visible = true;
                        TestScheduler_rescheduleImageButton.Visible = true;
                        TestScheduler_unscheduleImageButton.Visible = true;
                    }
                    else if (TestScheduler_statusLabel.Text == "Not Scheduled")
                        TestScheduler_rescheduleImageButton.Visible = false;
                    else if (TestScheduler_statusLabel.Text == "Cancelled")
                        TestScheduler_viewCancelReasonImageButton.Visible = true;
                    else if (TestScheduler_statusLabel.Text == "In Progress")
                    {
                        TestScheduler_viewShedulerImageButton.Visible = true;
                        TestScheduler_viewResultHyperLinkButton.Visible = true;
                    }
                    if (TestScheduler_statusLabel.Text == "Completed" ||
                        TestScheduler_statusLabel.Text == "Quit" ||
                        TestScheduler_statusLabel.Text == "Elapsed" || TestScheduler_statusLabel.Text == "Evaluated")
                    {
                        TestScheduler_viewResultHyperLinkButton.Visible = true;
                        TestScheduler_viewShedulerImageButton.Visible = true;
                        TestScheduler_canidateReportHyperLinkButton.Visible = true;
                    }
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    #region Open view result in new window
                    TestScheduler_viewResultHyperLinkButton.NavigateUrl =
                        "~/ReportCenter/CandidateTestDetails.aspx?m=3&s=0" +
                        "&testkey=" + TestScheduler_Grid_testIDHiddenField.Value +
                        "&candidatesession=" + TestScheduler_candidateTestSessionIDHiddenField.Value +
                        "&attemptid=" + TestScheduler_attemptIDHiddenField.Value +
                        "&tab=CS" +
                        "&parentpage=" + Constants.ParentPage.TEST_STATISTICS_INFO;
                    #endregion

                    #region Open candidate test details in new window

                    TestScheduler_canidateReportHyperLinkButton.NavigateUrl = "~/TestMaker/TestResult.aspx?m=2&s=0" +
                        "&candidatesession=" + TestScheduler_candidateTestSessionIDHiddenField.Value +
                        "&attemptid=" + TestScheduler_attemptIDHiddenField.Value +
                        "&testkey=" + TestScheduler_Grid_testIDHiddenField.Value + "&parentpage=TST_SCHD";
                    #endregion

                    HyperLink TestScheduler_candidateFullNameHyperLink =
                        (HyperLink)e.Row.FindControl("TestScheduler_candidateFullNameHyperLink");

                    HiddenField TestScheduler_Grid_candidateIDHiddenField =
                        (HiddenField)e.Row.FindControl("TestScheduler_Grid_candidateIDHiddenField");

                    if (TestScheduler_candidateFullNameHyperLink != null)
                    {
                        if (TestScheduler_Grid_candidateIDHiddenField != null)
                        {
                            TestScheduler_candidateFullNameHyperLink.NavigateUrl = "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&puid=" + TestScheduler_Grid_candidateIDHiddenField.Value;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(TestScheduler_topErrorMessageLabel,
                    TestScheduler_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// While unscheduling the candidate, it asks the user to confirm. Once he confirms to 
        /// unschedule, it triggers the below method to specify the button type 'Yes'/'No'
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ConfirmMessageEventArgs"/> that has a property ButtonType which
        /// specify the Actiontype.
        /// </param>
        void ScheduleCandidate_inactiveConfirmMsgControl_ConfirmMessageThrown
            (object sender, ConfirmMessageEventArgs e)
        {
            try
            {
                if (e.ButtonType ==Forte.HCM.DataObjects.ButtonType.Yes)
                {
                    ClearLabelMessage();

                    string candidateSessionId = ViewState["CANDIDATE_SESSIONID"].ToString();
                    string attemptId = ViewState["ATTEMPT_ID"].ToString();

                    bool isMailSent = true;

                    // Update the session status as 'Not Scheduled'
                    new TestConductionBLManager().UpdateSessionStatus
                        (candidateSessionId, Convert.ToInt32(attemptId),
                        Constants.CandidateAttemptStatus.NOT_SCHEDULED,
                        Constants.CandidateSessionStatus.NOT_SCHEDULED, base.userID, out isMailSent);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestScheduler_topErrorMessageLabel,
                   TestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will fire when reschedule/unschedule/viewresult/view candidate 
        /// commands are called.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that contains the event data.
        /// </param>
        protected void TestScheduler_scheduleGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ClearLabelMessage();

                if (!(e.CommandSource is ImageButton))
                    return;

                HiddenField attemptIdHiddenField = ((ImageButton)e.CommandSource).Parent.
                        FindControl("TestScheduler_attemptIDHiddenField") as HiddenField;
                HiddenField testSessionID = ((ImageButton)e.CommandSource).Parent.
                    FindControl("TestScheduler_Grid_testSessionIDHiddenField") as HiddenField;
                HiddenField TestScheduler_Grid_testIDHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("TestScheduler_Grid_testIDHiddenField") as HiddenField;
                HiddenField TestScheduler_Grid_ExpiryDateHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("TestScheduler_Grid_ExpiryDateHiddenField") as HiddenField;
                HiddenField TestScheduler_Grid_candidateNameHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("TestScheduler_Grid_candidateNameHiddenField") as HiddenField;
                HiddenField TestScheduler_candidateEmailHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("TestScheduler_candidateEmailHiddenField") as HiddenField;
                HiddenField TestScheduler_Grid_candidateIDHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("TestScheduler_Grid_candidateIDHiddenField") as HiddenField;
                HiddenField TestScheduler_Grid_sessionExpiryDateHiddenField = ((ImageButton)e.CommandSource).Parent.
                    FindControl("TestScheduler_Grid_sessionExpiryDateHiddenField") as HiddenField;

                if (e.CommandName == "reschedule")
                {
                    // Reschedule candidate 
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    TestScheduler_scheduleCandiateLiteral.Text = "Reschedule";

                    ViewState["RESCHEDULE_STATUS"] = true;

                    ClearSchedulePopup();

                    // This will be used in SAVE button event handler of the schedule popup 
                    // to compare with current date.
                    TestScheduler_Popup_candidateIDHiddenField.Value = TestScheduler_Grid_candidateIDHiddenField.Value;
                    TestScheduler_expiryDateHiddenField.Value = TestScheduler_Grid_ExpiryDateHiddenField.Value;
                    TestScheduler_Popup_sessionExpiryDateValueLabel.Text = TestScheduler_Grid_sessionExpiryDateHiddenField.Value;
                    TestScheduler_Popup_candidateNameTextBox.Text = TestScheduler_Grid_candidateNameHiddenField.Value.Trim();
                    TestScheduler_Popup_emailTextBox.Text = TestScheduler_candidateEmailHiddenField.Value.Trim();
                    TestScheduler_Popup_scheduleDateTextBox.Text = TestScheduler_Grid_ExpiryDateHiddenField.Value;
                    TestScheduler_Popup_candidateNameImageButton.Visible = false;

                    TestScheduler_scheduleModalPopupExtender.Show();
                }
                else if (e.CommandName == "unschedule")
                {
                    // Code for unschedule
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;

                    TestScheduler_unschedulePopupExtenter.Show();
                }
                else if (e.CommandName == "viewSchedule")
                {
                    // Code for view schedule
                    TestScheduler_viewTestSchedule.CandidateSessionId = e.CommandArgument.ToString();

                    TestSessionDetail testSession = new TestSchedulerBLManager().GetTestSessionDetail
                    (testSessionID.Value.ToString().Trim(), e.CommandArgument.ToString(),base.userID);

                    TestScheduler_viewTestSchedule.TestSession = testSession;
                    TestScheduler_viewScheduleModalPopupExtender.Show();
                }
                else if (e.CommandName == "viewCancelReason")
                {
                    // Code for View cancel reason
                    ViewState["CANDIDATE_SESSIONID"] = e.CommandArgument;
                    ViewState["ATTEMPT_ID"] = attemptIdHiddenField.Value;

                    TestSessionDetail testSession =
                        new TestSchedulerBLManager().GetTestSessionDetail(testSessionID.Value.ToString(),
                        ViewState["CANDIDATE_SESSIONID"].ToString(),base.userID);

                    // Load the cancel reason text and display the popup.
                    TestScheduler_cancelTestReasonDiv.InnerHtml =
                        testSession.CandidateTestSessions[0].CancelReason.ToString();
                    TestScheduler_cancelSessionModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestScheduler_topErrorMessageLabel,
                   TestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Clicking on Save button in Schedule popup will trigger this method.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void TestScheduler_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();
                bool isMailSent = true;

                // Validate the scheduled date and display the error message if any.
                TestScheduler_MaskedEditValidator.Validate();

                if (!TestScheduler_MaskedEditValidator.IsValid)
                {
                    TestScheduler_scheduleErrorMsgLabel.Text =
                        TestScheduler_MaskedEditValidator.ErrorMessage;
                    TestScheduler_scheduleModalPopupExtender.Show();
                    return;
                }

                // Scheduled date should be between current date and test session's expiry date.
                if (DateTime.Parse(TestScheduler_Popup_scheduleDateTextBox.Text) >
                    DateTime.Parse(TestScheduler_Popup_sessionExpiryDateValueLabel.Text)
                    || DateTime.Parse(TestScheduler_Popup_scheduleDateTextBox.Text) < DateTime.Today)
                {
                    TestScheduler_scheduleErrorMsgLabel.Text =
                        Resources.HCMResource.ScheduleCandidate_ScheduledDateInvalid;
                    TestScheduler_scheduleModalPopupExtender.Show();

                    // Since the candidate name and email textboxes are readonly, its values are not 
                    // maintained on postback. Use Request[controlName.UniqueID] to get the value.
                    TestScheduler_Popup_candidateNameTextBox.Text =
                        Request[TestScheduler_Popup_candidateNameTextBox.UniqueID].Trim();
                    TestScheduler_Popup_emailTextBox.Text =
                        Request[TestScheduler_Popup_emailTextBox.UniqueID].Trim();
                    return;
                }

                if (ViewState["CANDIDATE_SESSIONID"] == null || ViewState["ATTEMPT_ID"] == null)
                    return;

                TestScheduleDetail testSchedule = new TestScheduleDetail();
                testSchedule.CandidateID = TestScheduler_Popup_candidateIDHiddenField.Value;
                testSchedule.CandidateTestSessionID = ViewState["CANDIDATE_SESSIONID"].ToString();
                testSchedule.AttemptID = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());
                testSchedule.EmailId = Request[TestScheduler_Popup_emailTextBox.UniqueID].Trim();

                // Set the time to maximum in expiry date.
                testSchedule.ExpiryDate = (Convert.ToDateTime(
                    TestScheduler_Popup_scheduleDateTextBox.Text)).Add(new TimeSpan(23, 59, 59));

                if (!Utility.IsNullOrEmpty(Convert.ToBoolean(ViewState["RESCHEDULE_STATUS"])) &&
                    Convert.ToBoolean(ViewState["RESCHEDULE_STATUS"]) == true)
                {
                    testSchedule.IsRescheduled = true;
                    if (!Utility.IsNullOrEmpty(TestScheduler_Popup_candidateIDHiddenField.Value))
                        testSchedule.CandidateID = TestScheduler_Popup_candidateIDHiddenField.Value;
                }

                // Reschedule candidate 
                new TestSchedulerBLManager().ScheduleCandidate(testSchedule, base.userID, base.tenantID, out isMailSent);

                if (isMailSent == false)
                {
                    TestScheduler_topErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    TestScheduler_bottomErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                }

                // Reload the grid
                LoadScheduleDetails(Convert.ToInt32(ViewState["PAGE_NUMBER"]));

                base.ShowMessage(TestScheduler_topSuccessMessageLabel,
                    TestScheduler_bottomSuccessMessageLabel,
                    Resources.HCMResource.TestScheduler_RescheduledSuccessfully);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestScheduler_topErrorMessageLabel,
                    TestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Clicking on unschedule will open the popup window to get the 
        /// confirmation from user. Selecting Ok button will unschedule 
        /// the candidate.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void TestScheduler_OkClick(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();

                string candidateSessionId = ViewState["CANDIDATE_SESSIONID"].ToString();
                int attemptId = Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString());
                bool isMailSent = true;
                bool isUnscheduled = true;

                // Get candidate details for the given candidate test session id and attempt id.
                CandidateTestSessionDetail candidateTestSessionDetail = new TestSchedulerBLManager().
                         GetCandidateTestSession(candidateSessionId, attemptId);

                // Update the session status as 'Not Scheduled' for the 'Unscheduled' candidates.
                new TestConductionBLManager().UpdateSessionStatus(candidateTestSessionDetail,
                    Constants.CandidateAttemptStatus.NOT_SCHEDULED,
                    Constants.CandidateSessionStatus.NOT_SCHEDULED, base.userID,
                    isUnscheduled, out isMailSent);

                if (isMailSent == false)
                {
                    TestScheduler_topErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                    TestScheduler_bottomErrorMessageLabel.Text =
                        Resources.HCMResource.TestScheduler_MailCannotBeSent;
                }

                // TODO : Instead of loading the whole grid, update only that row.

                base.ShowMessage(TestScheduler_topSuccessMessageLabel,
                         TestScheduler_bottomSuccessMessageLabel,
                         Resources.HCMResource.ScheduleCandidate_UnScheduledStatus);

                // Reload the grid
                LoadScheduleDetails(Convert.ToInt32(ViewState["PAGE_NUMBER"]));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestScheduler_topErrorMessageLabel,
                    TestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event handler that will load the candidate session detail
        /// whenever its been clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/>that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/>that contains the event data.
        /// </param>
        protected void TestScheduler_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // To maintain author name when the page is post backed.
                TestScheduler_testSessionAuthorTextBox.Text =
                    Request[TestScheduler_testSessionAuthorTextBox.UniqueID].Trim();

                ClearLabelMessage();

                TestScheduler_pagingNavigator.Reset();
                ViewState["SORT_DIRECTION_KEY"] = SortType.Descending;
                ViewState["SORT_EXPRESSION"] = "SESSION_CREATED_ON";

                LoadScheduleDetails(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestScheduler_topErrorMessageLabel,
                    TestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when a row is created in the gridview.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/>that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewRowEventArgs"/>that contains the event data.
        /// </param>
        protected void TestScheduler_scheduleGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    // Get last clicked column index
                    int sortColumnIndex = GetSortColumnIndex
                        (TestScheduler_scheduleGridView,
                        (string)ViewState["SORT_EXPRESSION"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_DIRECTION_KEY"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestScheduler_topErrorMessageLabel,
                    TestScheduler_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the reset link button is clicked.
        /// It will load grid as it was when first time loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/>that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/>that contains the event data.
        /// </param>
        protected void TestScheduler_resetLinkButton_Click(object sender, EventArgs e)
        {
            base.ClearSearchCriteriaSession();
            Response.Redirect(Request.RawUrl, false);
        }

        /// <summary>
        /// Handler that will call when the cancel link button is clicked.
        /// It will clear all the session values.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TestScheduler_cancelLinkButton_Click(object sender, EventArgs e)
        {
            base.ClearSearchCriteriaSession();
            Response.Redirect("~/OTMHome.aspx", false);
        }

        /// <summary>
        /// Handler that will call when schedule candidate button is clicked.
        /// It will redirect to the schedule candidate page.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void TestScheduler_scheduleCandidateButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Scheduler/ScheduleCandidate.aspx?m=2&s=0&parentpage=TST_SCHD", false);
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that will get candidate details who are scheduled for the test.
        /// </summary>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that contains the page number.
        /// </param>
        private void LoadScheduleDetails(int pageNumber)
        {
            int totalRecords = 0;
            int sessionAuthorID = 0;
            TestScheduleSearchCriteria scheduleSearchCriteria = new TestScheduleSearchCriteria();

            // Check if the user is currently logged in.
            // Otherwise, pass the selected userid from the search user popup.
            if (Utility.IsNullOrEmpty(TestScheduler_testSessionAuthorIdHiddenField.Value.Trim()))
                sessionAuthorID = 0;
            else
                sessionAuthorID =
                    Convert.ToInt32(TestScheduler_testSessionAuthorIdHiddenField.Value.Trim());

            // Check if the search criteria fields are empty.
            // If this is empty, pass null. Or else, pass its value.
            scheduleSearchCriteria.CandidateName = TestScheduler_candidateNameTextBox.Text.Trim() == "" ?
                null : TestScheduler_candidateNameTextBox.Text.Trim();
            scheduleSearchCriteria.EMail = TestScheduler_emailIdTextBox.Text.Trim() == "" ?
                null : TestScheduler_emailIdTextBox.Text.Trim();
            scheduleSearchCriteria.TestScheduleAuthor = TestScheduler_testSessionAuthorTextBox.Text.Trim() == "" ?
                null : TestScheduler_testSessionAuthorTextBox.Text.Trim();
            scheduleSearchCriteria.TestSessionID = TestScheduler_testSessionIdTextBox.Text.Trim() == "" ?
                null : TestScheduler_testSessionIdTextBox.Text.Trim();
            scheduleSearchCriteria.TestName = TestScheduler_testNameTextBox.Text.Trim() == "" ?
                null : TestScheduler_testNameTextBox.Text.Trim();

            if (TestScheduler_statusDropDownList.SelectedValue == "0")
                scheduleSearchCriteria.TestCompletedStatus = TestCompletedStatus.None;
            else
                scheduleSearchCriteria.TestCompletedStatus =
                    TestScheduler_statusDropDownList.SelectedValue == "2" ?
                    TestCompletedStatus.Completed : TestCompletedStatus.NotCompleted;

            scheduleSearchCriteria.CurrentPage = pageNumber;
            scheduleSearchCriteria.IsMaximized =
                TestScheduler_isMaximizedHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

            scheduleSearchCriteria.SortDirection = (SortType)ViewState["SORT_DIRECTION_KEY"];
            scheduleSearchCriteria.SortExpression = ViewState["SORT_EXPRESSION"].ToString();

            // Keep the search criteria in Session if the page is launched from 
            // somewhere else.
            Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_SCHEDULER] = scheduleSearchCriteria;

            List<TestScheduleDetail> testScheduleDetails = new TestSchedulerBLManager().GetScheduler
               (sessionAuthorID, pageNumber, base.GridPageSize, out totalRecords, scheduleSearchCriteria,
               scheduleSearchCriteria.SortExpression, scheduleSearchCriteria.SortDirection);

            if (testScheduleDetails.Count == 0)
            {
                base.ShowMessage(TestScheduler_topErrorMessageLabel,
                    TestScheduler_bottomErrorMessageLabel,
                    Resources.HCMResource.TestScheduler_NoDataFoundToDisplay);

                // Invisible paging control and gridview DIV.
                TestScheduler_schedulerDiv.Visible = false;
                TestScheduler_pagingNavigator.Visible = false;
            }
            else
            {
                TestScheduler_schedulerDiv.Visible = true;

                TestScheduler_scheduleGridView.DataSource = testScheduleDetails;
                TestScheduler_scheduleGridView.DataBind();

                TestScheduler_pagingNavigator.Visible = true;
                TestScheduler_pagingNavigator.PageSize = base.GridPageSize;
                TestScheduler_pagingNavigator.TotalRecords = totalRecords;
            }
        }

        /// <summary>
        /// This method clears the textboxes in Schedule/Reschedule popup.
        /// </summary>
        private void ClearSchedulePopup()
        {
            TestScheduler_Popup_candidateNameTextBox.Text = "";
            TestScheduler_Popup_emailTextBox.Text = "";
            TestScheduler_Popup_scheduleDateTextBox.Text = "";
            TestScheduler_scheduleErrorMsgLabel.Text = "";
        }

        /// <summary>
        /// Method that will clear all the success/error label message.
        /// </summary>
        private void ClearLabelMessage()
        {
            TestScheduler_topSuccessMessageLabel.Text = string.Empty;
            TestScheduler_bottomSuccessMessageLabel.Text = string.Empty;
            TestScheduler_topErrorMessageLabel.Text = string.Empty;
            TestScheduler_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            // Maintain the expand/restore gridview.
            if (!Utility.IsNullOrEmpty(TestScheduler_isMaximizedHiddenField.Value) &&
                TestScheduler_isMaximizedHiddenField.Value == "Y")
            {
                TestScheduler_searchCriteriasDiv.Style["display"] = "none";
                TestScheduler_searchResultsUpSpan.Style["display"] = "block";
                TestScheduler_searchResultsDownSpan.Style["display"] = "none";
                TestScheduler_schedulerDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                TestScheduler_searchCriteriasDiv.Style["display"] = "block";
                TestScheduler_searchResultsUpSpan.Style["display"] = "none";
                TestScheduler_searchResultsDownSpan.Style["display"] = "block";
                TestScheduler_schedulerDiv.Style["height"] = RESTORED_HEIGHT;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_SCHEDULER]))
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(TestScheduler_isMaximizedHiddenField.Value))
                    ((TestScheduleSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_TEST_SCHEDULER]).IsMaximized =
                        TestScheduler_isMaximizedHiddenField.Value == "Y" ? true : false;
        }

        /// <summary>
        /// Method that will fill the search criteria to the appropriate fields.
        /// </summary>
        /// <param name="scheduleSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the search
        /// criteria fields.
        /// </param>
        public void FillSearchCriteria(TestScheduleSearchCriteria scheduleSearchCriteria)
        {
            if (scheduleSearchCriteria.CandidateName != null)
                TestScheduler_candidateNameTextBox.Text = scheduleSearchCriteria.CandidateName;

            if (scheduleSearchCriteria.EMail != null)
                TestScheduler_emailIdTextBox.Text = scheduleSearchCriteria.EMail;

            if (scheduleSearchCriteria.TestScheduleAuthor != null)
                TestScheduler_testSessionAuthorTextBox.Text = scheduleSearchCriteria.TestScheduleAuthor;

            if (scheduleSearchCriteria.TestSessionID != null)
                TestScheduler_testSessionIdTextBox.Text = scheduleSearchCriteria.TestSessionID;

            if (scheduleSearchCriteria.TestName != null)
                TestScheduler_testNameTextBox.Text = scheduleSearchCriteria.TestName;

            if (scheduleSearchCriteria.TestScheduleAuthor != null)
                TestScheduler_testSessionAuthorTextBox.Text = scheduleSearchCriteria.TestScheduleAuthor;

            if (scheduleSearchCriteria.TestCompletedStatus == TestCompletedStatus.Completed)
                TestScheduler_statusDropDownList.SelectedValue = "2";
            else if (scheduleSearchCriteria.TestCompletedStatus == TestCompletedStatus.NotCompleted)
                TestScheduler_statusDropDownList.SelectedValue = "1";
            else
                TestScheduler_statusDropDownList.SelectedValue = "0";

            TestScheduler_isMaximizedHiddenField.Value =
                    scheduleSearchCriteria.IsMaximized == true ? "Y" : "N";

            ViewState["SORT_DIRECTION_KEY"] = scheduleSearchCriteria.SortDirection;
            ViewState["SORT_EXPRESSION"] = scheduleSearchCriteria.SortExpression;

            // Apply search
            LoadScheduleDetails(scheduleSearchCriteria.CurrentPage);
            TestScheduler_pagingNavigator.MoveToPage(scheduleSearchCriteria.CurrentPage);
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            Page.Form.DefaultButton = TestScheduler_searchButton.UniqueID;

            // Assign the client side onclick events to gridview header
            TestScheduler_searchResultsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                TestScheduler_schedulerDiv.ClientID + "','" +
                TestScheduler_searchCriteriasDiv.ClientID + "','" +
                TestScheduler_searchResultsUpSpan.ClientID + "','" +
                TestScheduler_searchResultsDownSpan.ClientID + "','" +
                TestScheduler_isMaximizedHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");

            // Candidate Name Popup
            TestScheduler_candidateNameImageButton.Attributes.Add("onclick",
                "return LoadCandidates('" + TestScheduler_candidateNameTextBox.ClientID + "','"
                + TestScheduler_candidateSessionIDHiddenField.ClientID + "')");

            // Author name popup
            TestScheduler_testScheduleAuthorImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                + TestScheduler_testSessionAuthorHiddenField.ClientID + "','"
                + TestScheduler_testSessionAuthorIdHiddenField.ClientID + "','"
                + TestScheduler_testSessionAuthorTextBox.ClientID + "','TC')");

            // Open candidate detail popup for unschedule
            TestScheduler_Popup_candidateNameImageButton.Attributes.Add("onclick", "return LoadCandidate('"
                    + TestScheduler_Popup_candidateNameTextBox.ClientID + "','"
                    + TestScheduler_Popup_emailTextBox.ClientID + "','"
                    + TestScheduler_Popup_candidateIDHiddenField.ClientID + "')");

            // Validate reschedule popup input fields.
            TestScheduler_saveButton.Attributes.Add("onclick", "return VaildateSchedulePopup('"
                        + TestScheduler_Popup_candidateNameTextBox.ClientID + "','"
                        + TestScheduler_Popup_scheduleDateTextBox.ClientID
                        + "','" + TestScheduler_scheduleErrorMsgLabel.ClientID + "')");

            // Keep the sort column, sort direction and the page number
            // in ViewState.
            if (Utility.IsNullOrEmpty(ViewState["SORT_DIRECTION_KEY"]))
                ViewState["SORT_DIRECTION_KEY"] = SortType.Descending;

            if (Utility.IsNullOrEmpty(ViewState["SORT_EXPRESSION"]))
                ViewState["SORT_EXPRESSION"] = "SESSION_CREATED_ON";

            if (Utility.IsNullOrEmpty(ViewState["PAGE_NUMBER"]))
                ViewState["PAGE_NUMBER"] = "1";

            TestScheduler_testSessionAuthorTextBox.Text = new
                CommonBLManager().GetUserDetail(base.userID).FirstName;

            TestScheduler_testSessionAuthorIdHiddenField.Value = base.userID.ToString();

            //// If the logged in user is admin, then show the imagebutton.
            ////if (base.isAdmin)
            //{
            //    TestScheduler_testScheduleAuthorImageButton.Visible = true;
            //    TestScheduler_testSessionAuthorTextBox.Text = string.Empty;
            //    TestScheduler_testSessionAuthorIdHiddenField.Value = "0";
            //}
        }

        #endregion Protected Overridden Methods
    }
}