﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Forte.HCM.UI.Scheduler.ScheduleCandidate"
    MasterPageFile="~/MasterPages/OTMMaster.Master" CodeBehind="ScheduleCandidate.aspx.cs" %>

<%@ Register Src="~/CommonControls/TestScheduleControl.ascx" TagName="TestScheduleControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="ScheduleCandidate_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">
    <script language="javascript" type="text/javascript">
        // Validate the schedule popup. If candidate name or expiry date is empty, display the message to user.
        function VaildateSchedulePopup(nameTxt, expiryDateTxt, errorMsgLbl) {
            if (document.getElementById(nameTxt).value == "") {
                document.getElementById(errorMsgLbl).innerHTML = "Candidate name cannot be empty";
                return false;
            }
            else if (document.getElementById(expiryDateTxt).value == "") {
                document.getElementById(errorMsgLbl).innerHTML = "Expiry date cannot be empty";
                return false;
            }
        }

        // Pressing tab key from 'Test Session Id' textbox should 
        // load the test session and schedule candidate details.
        function TestSessionKeyDown(event, showButtonCtrl) {
            if (event.which || event.keyCode) {
                if ((event.which == 9) || (event.keyCode == 9)) {
                    var ctrlId = showButtonCtrl.toString();
                    document.getElementById(showButtonCtrl).click();
                    return;
                }
            }
        }

        // Function that shows the create candidate popup
        function ShowCandidatePopup(nameCtrl, emailCtrl, idCtrl) {
            var height = 540;
            var width = 980;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:"
            + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../popup/AddCandidate.aspx?namectrl=" + nameCtrl;
            queryStringValue += "&emailctrl=" + emailCtrl;
            queryStringValue += "&idctrl=" + idCtrl;
            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }

        // Function that shows the create candidate popup
        function ShowBulkScheduleCandidatePopup(tkey, expDate, nameCtrl, emailCtrl, idCtrl) {
            debugger;
            var height = 340;
            var width = 1180;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:"
            + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../popup/BulkCandidateUpload.aspx?tKey=" + tkey + "&expDate=" + expDate + "&namectrl=" + nameCtrl;
            queryStringValue += "&emailctrl=" + emailCtrl;
            queryStringValue += "&idctrl=" + idCtrl;
            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }
        
    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td valign="top">
                <asp:UpdatePanel ID="ScheduleCandidate_mainUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="3" cellpadding="0">
                            <tr>
                                <td class="header_bg">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="72%" class="header_text_bold">
                                                <asp:Literal ID="ScheduleCandidate_headerLiteral" runat="server" Text="Schedule Candidate"></asp:Literal>
                                            </td>
                                            <td width="28%" align="right">
                                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                                    <tr>
                                                        <td width="81%" align="right">
                                                            <asp:LinkButton ID="ScheduleCandidate_topResetLinkButton" runat="server" Text="Reset"
                                                                SkinID="sknActionLinkButton" OnClick="ScheduleCandidate_resetLinkButton_Click" />
                                                        </td>
                                                        <td width="4%" align="center" class="link_button">
                                                            |
                                                        </td>
                                                        <td width="15%" align="left">
                                                            <asp:LinkButton ID="ScheduleCandidate_topCancelButton" runat="server" Text="Cancel"
                                                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="msg_align">
                                    <asp:Label ID="ScheduleCandidate_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="ScheduleCandidate_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tab_body_bg">
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td>
                                                <div id="ScheduleCandidate_testSessionDetailsDiv" runat="server" style="display: block">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td class="td_height_8">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <asp:Label ID="ScheduleCandidate_testSessionIdHeadLabel" runat="server" Text="Test Session ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 18%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="ScheduleCandidate_testSessionIdTextBox" runat="server"></asp:TextBox></div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="ScheduleCandidate_testSessionImageButton" runat="server" ImageAlign="Middle"
                                                                                    SkinID="sknbtnSearchicon" ToolTip="Click here to search session ID" Visible="false" /></div>
                                                                        </td>
                                                                        <td style="width: 14%">
                                                                            <asp:Label ID="ScheduleCandidate_candidateSessionIdHeadLabel" runat="server" Text="Candidate Session ID(s)"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 18%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="ScheduleCandidate_candidateSessionIdTextBox" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="ScheduleCandidate_candidateSessionHelpImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Enter one or more candidate session ID's separated by commas"
                                                                                    Height="16px" /></div>
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:Button ID="ScheduleCandidate_showButton" runat="server" Text="Show Details"
                                                                                OnClick="ScheduleCandidate_showButton_Click" SkinID="sknButtonId" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div id="ScheduleCandidate_testDetailsDiv" runat="server" style="display: none">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class=" header_bg">
                                                                                <table cellpadding="0" cellspacing="0" width="98%" border="0">
                                                                                    <tr>
                                                                                        <td style="width: 93%" align="left">
                                                                                            <asp:Literal ID="ScheduleCandidate_testDetailsLiteral" runat="server" Text="Test Session Details"></asp:Literal>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="grid_body_bg">
                                                                                <table cellpadding="0" cellspacing="5" width="100%" border="0">
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="ScheduleCandidate_testIDHeadLabel" runat="server" Text="Test ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="ScheduleCandidate_testIDLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                                Text=""></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 9%">
                                                                                            <asp:Label ID="ScheduleCandidate_testNameHeadLabel" runat="server" Text="Test Name"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 21%">
                                                                                            <asp:Label ID="ScheduleCandidate_testNameLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 12%">
                                                                                            <asp:Label ID="ScheduleCandidate_testAuthorHeadLabel" runat="server" Text="Test Author"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 12%">
                                                                                            <asp:Label ID="ScheduleCandidate_testAuthorLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 9%">
                                                                                            <asp:Label ID="ScheduleCandidate_emailHeadLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 17%">
                                                                                            <asp:Label ID="ScheduleCandidate_emailLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleCandidate_timeLimitHeadLabel" runat="server" Text="Time Limit"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleCandidate_timeLimitLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleCandidate_expiryDateHeadLabel" runat="server" Text="Expiry Date"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleCandidate_expiryDateLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleCandidate_cyberProctorateHeadLabel" runat="server" Text="Cyber Proctoring"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleCandidate_cyberProctorateLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 11%">
                                                                                            <asp:Label ID="ScheduleCandidate_displayResultsHeadLabel" runat="server" Text="Display Results"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleCandidate_displayResultsLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                                Text=""></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleCandidate_positionProfileHeadLabel" runat="server" Text="Position Profile"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="7">
                                                                                            <asp:Label ID="ScheduleCandidate_positionProfileLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4">
                                                                                            <asp:Label ID="ScheduleCandidate_testDescHeadLabel" runat="server" Text="Session Description"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            <asp:HiddenField ID="ScheduleCandidate_testTypeHiddenField" runat="server" Value="" />
                                                                                        </td>
                                                                                        <td colspan="4">
                                                                                            <asp:Label ID="ScheduleCandidate_instructionsLabel" runat="server" Text="Instructions"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4" valign="top">
                                                                                            <div class="label_multi_field_text" style="height: 50px; overflow: auto; word-wrap: break-word;
                                                                                                white-space: normal;">
                                                                                                <asp:Literal ID="ScheduleCandidate_testDescLiteral" runat="server" SkinID="sknMultiLineText"
                                                                                                    Text="Screening candidates for Yes Banking project. The candidates must have minimum 3 years experience in the field of ASP.Net UI design, Java Script, C# Programming & OOPS."></asp:Literal>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td colspan="4" valign="top">
                                                                                            <div class="label_multi_field_text" style="height: 50px; overflow: auto; word-wrap: break-word;
                                                                                                white-space: normal;">
                                                                                                <asp:Literal ID="ScheduleCandidate_instructionsLiteral" runat="server" SkinID="sknMultiLineText"
                                                                                                    Text="Candidates are requested to abide to the rules and regulations of the test. Since cyber proctoring is enabled the scores will be calculated only based on the test tracking details."></asp:Literal>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="ScheduleCandidate_testDetailsGridDiv" runat="server" style="display: none">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr id="ScheduleCandidate_searchTestResultsTR" runat="server">
                                                            <td class="header_bg">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td id="ScheduleCandidate_candidateSessionHeader" runat="server">
                                                                            <asp:Literal ID="ScheduleCandidate_candidateSessionDetailsLiteral" runat="server"
                                                                                Text="Candidate Session Details"></asp:Literal>
                                                                        </td>
                                                                        <td align="right" id="ScheduleCandidate_searchResultsTR" runat="server">
                                                                            <span id="ScheduleCandidate_searchResultsUpSpan" style="display: none;" runat="server">
                                                                                <asp:Image ID="ScheduleCandidate_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                            </span><span id="ScheduleCandidate_searchResultsDownSpan" style="display: block;"
                                                                                runat="server">
                                                                                <asp:Image ID="ScheduleCandidate_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="grid_body_bg">
                                                                <div id="ScheduleCandidate_candidateSessionGridViewDIV" runat="server" style="height: 200px;
                                                                    overflow: auto;">
                                                                    <asp:UpdatePanel ID="ScheduleCandidate_candidateSessionGridViewUpdatePanel" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:GridView ID="ScheduleCandidate_candidateSessionGridView" runat="server" AutoGenerateColumns="false"
                                                                                AllowSorting="True" OnRowCommand="ScheduleCandidate_candidateSessionGridView_RowCommand"
                                                                                OnRowDataBound="ScheduleCandidate_candidateSessionGridView_RowDataBound" OnSorting="ScheduleCandidate_candidateSessionGridView_Sorting"
                                                                                OnRowCreated="ScheduleCandidate_candidateSessionGridView_RowCreated" SkinID="sknWrapHeaderGrid">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-Width="10%">
                                                                                        <ItemTemplate>
                                                                                            <asp:HyperLink ID="ScheduleCandidate_viewTestSummaryHyperLink" runat="server" Target="_blank"
                                                                                                ToolTip="View Test Summary" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_test_result.gif">
                                                                                            </asp:HyperLink>
                                                                                            <asp:HyperLink ID="ScheduleCandidate_viewResultHyperLink" runat="server" Target="_blank"
                                                                                                ToolTip="Candidate Test Details" ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif">
                                                                                            </asp:HyperLink>
                                                                                            <asp:ImageButton ID="ScheduleCandidate_scheduleImageButton" runat="server" SkinID="sknScheduleImageButton"
                                                                                                ToolTip="Schedule Candidate" CommandName="schedule" Visible="true" CommandArgument='<%# Eval("CandidateTestSessionID") %>' />
                                                                                            <asp:ImageButton ID="ScheduleCandidate_rescheduleImageButton" runat="server" SkinID="sknRescheduleImageButton"
                                                                                                ToolTip="Reschedule Candidate" CommandName="reschedule" Visible="true" CommandArgument='<%# Eval("CandidateTestSessionID") %>' />
                                                                                            <asp:ImageButton ID="ScheduleCandidate_viewShedulerImageButton" runat="server" SkinID="sknViewScheduleImageButton"
                                                                                                CommandName="viewSchedule" ToolTip="View Candidate Schedule" CommandArgument='<%# Eval("CandidateTestSessionID") %>' />
                                                                                            <asp:ImageButton ID="ScheduleCandidate_unscheduleImageButton" runat="server" SkinID="sknUnscheduleImageButton"
                                                                                                ToolTip="Unschedule Candidate" CommandArgument='<%# Eval("CandidateTestSessionID") %>'
                                                                                                CommandName="unschedule" />
                                                                                            <asp:ImageButton ID="ScheduleCandidate_retakeRequestImageButton" runat="server" SkinID="sknScheduleImageButton"
                                                                                                ToolTip="Schedule Retake Request" CommandName="retakeTest" Visible="true" CommandArgument='<%# Eval("CandidateTestSessionID") %>' />
                                                                                            <asp:ImageButton ID="ScheduleCandidate_cancelReasonDisplayImageButton" runat="server"
                                                                                                SkinID="sknCancelReasonImageButton" ToolTip="View Cancel Reason" CommandName="viewCancelReason"
                                                                                                Visible="true" CommandArgument='<%# Eval("CandidateTestSessionID") %>' />
                                                                                            <asp:ImageButton ID="ScheduleCandidate_viewCyberProctoringDetailsImageButton" runat="server"
                                                                                                SkinID="sknViewCertificateImageButton" ToolTip="View Cyber Proctoring Details"
                                                                                                CommandArgument='<%# Eval("CandidateTestSessionID") %>' CommandName="cyberProctoringDetails"
                                                                                                Visible='<%# IsCyberProctoring(Eval("Status").ToString()) %>' />
                                                                                            <asp:HiddenField ID="ScheduleCandidate_candidateTestSessionIDHiddenField" runat="server"
                                                                                                Value='<%# Eval("CandidateTestSessionID") %>' />
                                                                                            <asp:HiddenField ID="ScheduleCandidate_attemptIDHiddenField" runat="server" Value='<%# Eval("AttemptID") %>' />
                                                                                            <asp:HiddenField ID="ScheduleCandidate_requestRetakeHiddenField" runat="server" Value='<%# Eval("RetakeRequest") %>' />
                                                                                            <asp:HiddenField ID="ScheduleCandidate_modifiedDateHiddenField" runat="server" Value='<%# Eval("ModifiedDate") %>' />
                                                                                            <asp:HiddenField ID="ScheduleCandidate_Grid_ExpiryDateHiddenField" runat="server"
                                                                                                Value='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateTestSessionDetail)Container.DataItem).ScheduledDate) %>' />
                                                                                            <asp:HiddenField ID="ScheduleCandidate_Grid_candidateEmailHiddenField" runat="server"
                                                                                                Value='<%# Eval("Email") %>' />
                                                                                            <asp:HiddenField ID="ScheduleCandidate_Grid_candidateNameHiddenField" runat="server"
                                                                                                Value='<%# Eval("CandidateFirstName") %>' />
                                                                                            <asp:HiddenField ID="ScheduleCandidate_Grid_candidateIDHiddenField" runat="server"
                                                                                                Value='<%# Eval("CandidateID") %>' />
                                                                                            <asp:HiddenField ID="ScheduleCandidate_Grid_emailReminderHiddenField" runat="server"
                                                                                                Value='<%# Eval("EmailReminder") %>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Candidate Session ID" SortExpression="CandidateSessionId"
                                                                                        ItemStyle-Width="12%" HeaderStyle-CssClass="td_padding_right_20">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ScheduleCandidate_candidateSessionLabel" runat="server" Text='<%# Eval("CandidateTestSessionID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Candidate Name" SortExpression="CandidateName" ItemStyle-Width="22%">
                                                                                        <ItemTemplate>
                                                                                            <asp:HyperLink ID="ScheduleCandidate_candidateHyperLink" ToolTip='<%# Eval("CandidateFullName") %>'
                                                                                                runat="server" Text='<%# Eval("CandidateFirstName") %>' Target="_blank" SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Email ID" SortExpression="EmailId" HeaderStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ScheduleCandidate_emailLabel" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Status" SortExpression="Status" HeaderStyle-Width="10%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ScheduleCandidate_statusLabel" runat="server" Text='<%# GetStatus(((Forte.HCM.DataObjects.CandidateTestSessionDetail)Container.DataItem).Status) %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Expiry Date" SortExpression="ScheduledDate DESC">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ScheduleCandidate_scheduledDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateTestSessionDetail)Container.DataItem).ScheduledDate) %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Completed Date" SortExpression="DateCompleted DESC">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ScheduleCandidate_completedDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateTestSessionDetail)Container.DataItem).DateCompleted) %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="msg_align">
                                    <asp:Label ID="ScheduleCandidate_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="ScheduleCandidate_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="header_bg" align="right">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="72%" class="header_text_bold">
                                                <table width="44%" border="0" cellpadding="0" cellspacing="0" runat="server" id="ScheduleCandidate_bottomScheduleButtonTable" style="display:none">
                                                    <tr>
                                                        <td width="40%"><img alt="" height="10px" width="10px" src="../App_Themes/DefaultTheme/Images/pp_add_icon.png" />
                                                            <asp:LinkButton runat="server" ID="ScheduleCandidate_bottomScheduleCandidateLinkButton"
                                                                SkinID="sknActionLinkButton" Text="Schedule Candidate" OnClick="ScheduleCandidate_ScheduleCandidateLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                         <td width="4%" align="center" class="link_button">
                                                           </td>
                                                        <td width="44%" ><img alt="" height="10px" width="10px" src="../App_Themes/DefaultTheme/Images/pp_add_icon.png" />
                                                            <asp:LinkButton runat="server" ID="ScheduleCandidate_bottomBulkScheduleCandidateLinkButton" SkinID="sknActionLinkButton" Text="Bulk Schedule Candidate" ></asp:LinkButton>
                                                        </td>
                                                    </tr>  
                                                </table>
                                            </td>
                                            <td width="28%" align="right">
                                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                                    <tr>
                                                        <td width="81%" align="right">
                                                            <asp:LinkButton ID="ScheduleCandidate_bottonResetLinkButton" runat="server" Text="Reset"
                                                                SkinID="sknActionLinkButton" OnClick="ScheduleCandidate_resetLinkButton_Click" />
                                                        </td>
                                                        <td width="4%" align="center" class="link_button">
                                                            
                                                        </td>
                                                        <td width="15%" align="left">
                                                            <asp:LinkButton ID="ScheduleCandidate_bottonCancelLinkButton" runat="server" Text="Cancel"
                                                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="ScheduleCandidate_schedulePanel" runat="server" Style="display: none"
                                        CssClass="popupcontrol_scheduler">
                                        <div style="display: none;">
                                            <asp:Button ID="ScheduleCandidate_hiddenButton" runat="server" Text="Hidden" /></div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Literal ID="ScheduleCandidate_questionResultLiteral" runat="server" Text="Schedule"></asp:Literal>
                                                            </td>
                                                            <td style="width: 50%" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="ScheduleCandidate_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                        <tr>
                                                            <td align="left" class="popup_td_padding_10">
                                                                <table border="0" cellpadding="0" cellspacing="5" width="100%" class="tab_body_bg"
                                                                    align="left">
                                                                    <tr>
                                                                        <td class="msg_align" colspan="4">
                                                                            <asp:Label ID="ScheduleCandidate_scheduleErrorMsgLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="ScheduleCandidate_candidateNamePopHeadLabel" runat="server" Text="Candidate Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class='mandatory'>*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="ScheduleCandidate_Popup_candidateNameTextBox" runat="server" ReadOnly="true"
                                                                                Text=""></asp:TextBox>&nbsp;<asp:ImageButton ID="ScheduleCandidate_candidateNameImageButton"
                                                                                    SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" ToolTip="Click here to search for candidates" />&nbsp;<asp:ImageButton
                                                                                        ID="ScheduleCandidate_positionProfileCandidateImageButton" SkinID="sknBtnSearchPositinProfileCandidateIcon"
                                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to search for candidates associated with the position profile" />&nbsp;
                                                                            <asp:Button ID="ScheduleCandidate_Popup_createCandidateButton" Text="New" runat="server"
                                                                                SkinID="sknButtonId" ToolTip="Click here to create a new candidate" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ScheduleCandidate_Popup_emailHeadLabel" runat="server" Text="Email"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="ScheduleCandidate_Popup_emailTextBox" runat="server" Text="" ReadOnly="true"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="ScheduleCandidate_scheduleDatePopLabel" runat="server" Text="Test Expiry Date"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            <span class='mandatory'>*</span>
                                                                        </td>
                                                                        <td>
                                                                            <table width="97.5%" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td style="width: 40%">
                                                                                        <asp:TextBox ID="ScheduleCandidate_scheduleDatePopupTextBox" runat="server" MaxLength="10"
                                                                                            AutoCompleteType="None" Text=""></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:ImageButton ID="ScheduleCandidate_Popup_calendarImageButton" SkinID="sknCalendarImageButton"
                                                                                            runat="server" ImageAlign="Middle" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <ajaxToolKit:MaskedEditExtender ID="ScheduleCandidate_MaskedEditExtender" runat="server"
                                                                                TargetControlID="ScheduleCandidate_scheduleDatePopupTextBox" Mask="99/99/9999"
                                                                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                            <ajaxToolKit:MaskedEditValidator ID="ScheduleCandidate_MaskedEditValidator" runat="server"
                                                                                ControlExtender="ScheduleCandidate_MaskedEditExtender" ControlToValidate="ScheduleCandidate_scheduleDatePopupTextBox"
                                                                                EmptyValueMessage="Expiry Date is required" InvalidValueMessage="Expiry Date is invalid"
                                                                                Display="None" TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                                ValidationGroup="MKE" />
                                                                            <ajaxToolKit:CalendarExtender ID="ScheduleCandidate_customCalendarExtender" runat="server"
                                                                                TargetControlID="ScheduleCandidate_scheduleDatePopupTextBox" CssClass="MyCalendar"
                                                                                Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="ScheduleCandidate_Popup_calendarImageButton" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ScheduleCandidate_sessionExpiryDateHeadLabel" runat="server" Text="Session Expiry Date"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="ScheduleCandidate_Popup_sessionExpiryDateValueLabel" runat="server"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:CheckBox ID="ScheduleCandidate_emailRemainderCheckBox" runat="server" Text="Send reminder to candidate every 24 hours"
                                                                                TextAlign="right" SkinID="sknMyRecordCheckBox" />
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popup_td_padding_5">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td align="left" style="width: 20px; padding-right: 5px">
                                                                <asp:HiddenField ID="ScheduleCandidate_candidateIdHiddenField" runat="server" />
                                                                <asp:Button ID="ScheduleCandidate_saveButton" runat="server" SkinID="sknButtonId"
                                                                    Text="Save" OnClick="ScheduleCandidate_saveButton_Click" />
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="ScheduleCandidate_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                                                                    runat="server" Text="Cancel" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="ScheduleCandidate_scheduleModalPopupExtender"
                                        runat="server" PopupControlID="ScheduleCandidate_schedulePanel" TargetControlID="ScheduleCandidate_hiddenButton"
                                        BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="ScheduleCandidate_hiddenViewSchedulePanel" runat="server" Style="display: none"
                                        CssClass="popupcontrol_question_detail">
                                        <div style="display: none;">
                                            <asp:Button ID="ScheduleCandidate_hiddenViewScheduleButton" runat="server" Text="Hidden" /></div>
                                        <uc1:TestScheduleControl ID="ScheduleCandidate_viewTestSchedule" runat="server" />
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="ScheduleCandidate_viewScheduleModalPopupExtender"
                                        runat="server" PopupControlID="ScheduleCandidate_hiddenViewSchedulePanel" TargetControlID="ScheduleCandidate_hiddenViewScheduleButton"
                                        BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="ScheduleCandidate_unscheduleCandidatePanel" runat="server" Style="display: none"
                                        CssClass="popupcontrol_confirm_remove">
                                        <div style="display: none;">
                                            <asp:Button ID="ScheduleCandidate_hiddenUnscheduleButton" runat="server" Text="Hidden" /></div>
                                        <uc2:ConfirmMsgControl ID="ScheduleCandidate_inactiveConfirmMsgControl" runat="server"
                                            Title="Unschedule" Type="YesNo" Message="Are you sure do you want to cancel the scheduled test session?"
                                            OnOkClick="ScheduleCandidate_OkClick" />
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="ScheduleCandidate_unscheduleCandidateModalPopupExtender"
                                        runat="server" PopupControlID="ScheduleCandidate_unscheduleCandidatePanel" TargetControlID="ScheduleCandidate_hiddenUnscheduleButton"
                                        BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="ScheduleCandidate_cancelTestPanel" runat="server" Style="display: none"
                                        CssClass="popupcontrol_cancel_session">
                                        <div style="display: none">
                                            <asp:Button ID="ScheduleCandidate_cancelReasonHiddenButton" runat="server" Text="Hidden" />
                                        </div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Label ID="ScheduleCandidate_cancelReasonLiteral" runat="server" Text="Cancelled Test Session Reason"></asp:Label>
                                                            </td>
                                                            <td style="width: 25%" align="right">
                                                                <asp:ImageButton ID="ScheduleCandidate_cancelReasonCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                        <tr>
                                                            <td class="popup_td_padding_10">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <div runat="server" id="ScheduleCandidate_cancelTestReasonDiv" class="label_field_text"
                                                                                style="height: 70px; width: 400px; overflow: auto;">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popup_td_padding_5">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:LinkButton ID="ScheduleCandidate_cancelReasonCancellationButton" runat="server"
                                                                    Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="ScheduleCandidate_cancelSessionModalPopupExtender"
                                        runat="server" PopupControlID="ScheduleCandidate_cancelTestPanel" TargetControlID="ScheduleCandidate_cancelReasonHiddenButton"
                                        CancelControlID="ScheduleCandidate_cancelReasonCancellationButton" BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="ScheduleCandidate_RetakeValidationPopupPanel" runat="server" Style="display: none;
                                        height: 202px;" CssClass="popupcontrol_confirm">
                                        <div id="ScheduleCandidate_RetakeValidationDiv" style="display: none">
                                            <asp:Button ID="ScheduleCandidate_RetakeValidation_hiddenButton" runat="server" />
                                        </div>
                                        <uc2:ConfirmMsgControl ID="ScheduleCandidate_RetakeValidation_ConfirmMsgControl"
                                            runat="server" Title="Request to Retake" Type="OkSmall" />
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="ScheduleCandidate_RetakeValidation_ModalPopupExtender"
                                        runat="server" PopupControlID="ScheduleCandidate_RetakeValidationPopupPanel"
                                        TargetControlID="ScheduleCandidate_RetakeValidation_hiddenButton" BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="ScheduleCandidate_isMaximizedHiddenField" runat="server" />
                                    <asp:HiddenField ID="ScheduleCandidate_candidateNameHiddenField" runat="server" />
                                    <asp:HiddenField ID="ScheduleCandidate_reScheduleCandidateIDHiddenField" runat="server" />
                                    <asp:HiddenField ID="ScheduleCandidate_sessionExpiryDateHiddenField" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
