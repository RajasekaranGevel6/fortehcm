<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/OTMMaster.Master"
    CodeBehind="TestScheduler.aspx.cs" Inherits="Forte.HCM.UI.Scheduler.TestScheduler" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/TestScheduleControl.ascx" TagName="TestScheduleControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="TestScheduler_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">

    <script language="javascript" type="text/javascript">

        // Validate the schedule popup. If candidate name or expiry date is empty, 
        // display the message to user.
        function VaildateSchedulePopup(nameTxt, expiryDateTxt, errorMsgLbl) {
            if (document.getElementById(nameTxt).value == "") {
                document.getElementById(errorMsgLbl).innerHTML = "Candidate name cannot be empty";
                return false;
            }
            else if (document.getElementById(expiryDateTxt).value == "") {
                document.getElementById(errorMsgLbl).innerHTML = "Expiry date cannot be empty";
                return false;
            }
        }
    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="TestScheduler_headerLiteral" runat="server" Text="Scheduler"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="TestScheduler_topScheduleCandidateButton" runat="server" Text="Schedule Candidate"
                                            SkinID="sknButtonId" OnClick="TestScheduler_scheduleCandidateButton_Click" />
                                        &nbsp;<asp:LinkButton ID="TestScheduler_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="TestScheduler_resetLinkButton_Click"></asp:LinkButton>
                                        &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="TestScheduler_topCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="TestScheduler_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="TestScheduler_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" valign="middle">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
                    <tr>
                        <td class="panel_bg">
                            <div id="TestScheduler_searchCriteriasDiv" runat="server" style="display: block;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                                                <tr>
                                                    <td style="width: 12%;">
                                                        <asp:Label ID="TestScheduler_candidateNameHeadLabel" runat="server" Text="Candidate Name"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 18%;">
                                                        <div style="float: left; padding-right: 5px;">
                                                            <asp:TextBox ID="TestScheduler_candidateNameTextBox" MaxLength="50" runat="server"></asp:TextBox>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:ImageButton ID="TestScheduler_candidateNameImageButton" SkinID="sknbtnSearchicon"
                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select the candidate" />
                                                        </div>
                                                    </td>
                                                    <td style="width: 9%;">
                                                        <asp:Label ID="TestScheduler_emailIdHeadLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td style="width: 17%;">
                                                        <asp:TextBox ID="TestScheduler_emailIdTextBox" MaxLength="50" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 15%;">
                                                        <asp:Label ID="TestScheduler_testSessionAuthorHeadLabel" runat="server" Text="Test Schedule Creator"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <div style="float: left; padding-right: 5px;">
                                                            <asp:TextBox ID="TestScheduler_testSessionAuthorTextBox" MaxLength="50" runat="server"
                                                                ReadOnly="true"></asp:TextBox>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:ImageButton ID="TestScheduler_testScheduleAuthorImageButton" SkinID="sknbtnSearchicon"
                                                                runat="server" ToolTip="Click here to select the schedule author" Visible="false" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_2">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="TestScheduler_testSessionIdHeadLabel" runat="server" Text="Test Session ID"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TestScheduler_testSessionIdTextBox" MaxLength="15" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="TestScheduler_testNameHeadLabel" runat="server" Text="Test Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TestScheduler_testNameTextBox" MaxLength="50" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="TestScheduler_testCompletionHeadLabel" runat="server" Text="Test Completed"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <div style="float: left; padding-right: 5px;">
                                                            <asp:DropDownList ID="TestScheduler_statusDropDownList" runat="server" Width="130px">
                                                                <asp:ListItem Text="Both" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Yes" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="No" Value="1"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:ImageButton ID="TestScheduler_statusImageButton" SkinID="sknHelpImageButton"
                                                                runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the test completion status here" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table align="right">
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="TestScheduler_searchButton" runat="server" SkinID="sknButtonId" Text="Search"
                                                            OnClick="TestScheduler_searchButton_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr id="TestScheduler_searchResultsTR" runat="server">
                        <td class="header_bg" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="TestScheduler_searchResultsLiteral" runat="server" Text="Candidate Session Details"></asp:Literal>
                                        &nbsp;<asp:Label ID="TestScheduler_sortHelpLabel" runat="server" SkinID="sknLabelText"></asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="TestScheduler_searchResultsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="TestScheduler_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span>
                                        <span id="TestScheduler_searchResultsDownSpan" style="display: block;" runat="server">
                                            <asp:Image ID="TestScheduler_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                        <asp:HiddenField ID="TestScheduler_restoreHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="grid_body_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr>
                                    <td align="left">
                                        <div style="height: 200px; width: 100%; overflow: auto" runat="server" id="TestScheduler_schedulerDiv">
                                            <asp:GridView ID="TestScheduler_scheduleGridView" runat="server" OnSorting="TestScheduler_scheduleGridView_Sorting"
                                                OnRowDataBound="TestScheduler_scheduleGridView_RowDataBound" OnRowCommand="TestScheduler_scheduleGridView_RowCommand"
                                                OnRowCreated="TestScheduler_scheduleGridView_RowCreated" SkinID="sknWrapHeaderExtenderGrid">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="10%">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="TestScheduler_viewShedulerImageButton" runat="server" SkinID="sknViewScheduleImageButton"
                                                                ToolTip="View Candidate Schedule" CommandArgument='<%# Eval("CandidateTestSessionID") %>'
                                                                CommandName="viewSchedule" />
                                                            <asp:HyperLink ID="TestScheduler_viewResultHyperLinkButton"
                                                                runat="server" Target="_blank" ToolTip="View Test Summary" 
                                                                ImageUrl="~/App_Themes/DefaultTheme/Images/icon_test_result.gif">
                                                            </asp:HyperLink>
                                                             <asp:HyperLink ID="TestScheduler_canidateReportHyperLinkButton"
                                                                runat="server" Target="_blank" ToolTip="Candidate Test Details" 
                                                                ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif">
                                                            </asp:HyperLink>
                                                            <asp:ImageButton ID="TestScheduler_rescheduleImageButton" runat="server" SkinID="sknRescheduleImageButton"
                                                                ToolTip="Reschedule Candidate" CommandName="reschedule" Visible="true" CommandArgument='<%# Eval("CandidateTestSessionID") %>' />
                                                            <asp:ImageButton ID="TestScheduler_unscheduleImageButton" runat="server" SkinID="sknUnscheduleImageButton"
                                                                ToolTip="Unschedule Candidate" CommandName="unschedule" CommandArgument='<%# Eval("CandidateTestSessionID") %>' />
                                                            <asp:ImageButton ID="TestScheduler_viewCancelReasonImageButton" runat="server" SkinID="sknCancelReasonImageButton"
                                                                ToolTip="View Cancel Reason" CommandName="viewCancelReason" Visible="true" CommandArgument='<%# Eval("CandidateTestSessionID") %>' />
                                                            <asp:HiddenField ID="TestScheduler_candidateTestSessionIDHiddenField" runat="server"
                                                                Value='<%# Eval("CandidateTestSessionID") %>' />
                                                            <asp:HiddenField ID="TestScheduler_attemptIDHiddenField" runat="server" Value='<%# Eval("AttemptID") %>' />
                                                            <asp:HiddenField ID="TestScheduler_Grid_testSessionIDHiddenField" runat="server"
                                                                Value='<%# Eval("TestSessionID") %>' />
                                                            <asp:HiddenField ID="TestScheduler_Grid_testIDHiddenField" runat="server" Value='<%# Eval("TestID") %>' />
                                                            <asp:HiddenField ID="TestScheduler_Grid_ExpiryDateHiddenField" runat="server" Value='<%# GetDateFormat(((Forte.HCM.DataObjects.TestScheduleDetail)Container.DataItem).ExpiryDate) %>' />
                                                            <asp:HiddenField ID="TestScheduler_Grid_sessionExpiryDateHiddenField" runat="server"
                                                                Value='<%# GetDateFormat(((Forte.HCM.DataObjects.TestScheduleDetail)Container.DataItem).SessionExpiryDate) %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Client&nbsp;Request Number" DataField="ClientRequestID"
                                                        SortExpression="CLIENT_REQ_NO DESC" HeaderStyle-Width="150px"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Test&nbsp;Session ID" DataField="TestSessionID" SortExpression="TEST_SESS_ID DESC"
                                                        HeaderStyle-Width="150px" ItemStyle-Width="150px"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Candidate Session&nbsp;ID" DataField="CandidateTestSessionID"
                                                        SortExpression="CAND_SESS_KEY DESC" HeaderStyle-Width="180px" ItemStyle-Width="180px">
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Candidate&nbsp;Name" SortExpression="CANDIDATE_NAME"
                                                        HeaderStyle-Width="160px" ItemStyle-Width="160px">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="TestScheduler_candidateFullNameHyperLink" runat="server" Text='<%# Eval("CandidateName") %>'
                                                                ToolTip='<%# Eval("CandidateFullName") %>' Target="_blank" SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Email&nbsp;ID" DataField="EmailId" SortExpression="CANDIDATE_EMAIL"
                                                        HeaderStyle-Width="200px" ItemStyle-Width="200px"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="Created&nbsp;Date" SortExpression="SESSION_CREATED_ON DESC">
                                                        <ItemTemplate>
                                                            <asp:Label ID="TestScheduler_createdDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.TestScheduleDetail)Container.DataItem).CreatedDate) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Scheduled&nbsp;By" SortExpression="SCHEDULED_BY" HeaderStyle-Width="120px"
                                                        ItemStyle-Width="120px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="TestScheduler_schedulerFullNameLabel" runat="server" Text='<%# Eval("SchedulerFirstName") %>'
                                                                ToolTip='<%# Eval("SchedulerFullName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Expiry&nbsp;Date" SortExpression="EXPIRY_DATE DESC">
                                                        <ItemTemplate>
                                                            <asp:Label ID="TestScheduler_expiryDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.TestScheduleDetail)Container.DataItem).ExpiryDate) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status" SortExpression="TEST_COMPLETED_STATUS" HeaderStyle-Width="120px"
                                                        ItemStyle-Width="120px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="TestScheduler_statusLabel" runat="server" Text='<%# Eval("TestStatus") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Completed Date" SortExpression="TEST_COMPLETED_DATE DESC">
                                                        <ItemTemplate>
                                                            <asp:Label ID="TestScheduler_completedDateLabel" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.TestScheduleDetail)Container.DataItem).CompletedDate) %>'
                                                                runat="server"></asp:Label>
                                                            <asp:HiddenField ID="TestScheduler_Grid_candidateNameHiddenField" runat="server"
                                                                Value='<%# Eval("CandidateName") %>' />
                                                            <asp:HiddenField ID="TestScheduler_candidateEmailHiddenField" runat="server" Value='<%# Eval("EmailId") %>' />
                                                            <asp:HiddenField ID="TestScheduler_Grid_candidateIDHiddenField" runat="server" Value='<%# Eval("CandidateID") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc3:PageNavigator ID="TestScheduler_pagingNavigator" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="TestScheduler_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="TestScheduler_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="0" align="right">
                    <tr>
                        <td>
                            <asp:Button ID="TestScheduler_bottomScheduleCandidateButton" runat="server" Text="Schedule Candidate"
                                SkinID="sknButtonId" OnClick="TestScheduler_scheduleCandidateButton_Click" />
                            &nbsp;<asp:LinkButton ID="TestScheduler_bottomResetLinkButton" runat="server" Text="Reset"
                                SkinID="sknActionLinkButton" OnClick="TestScheduler_resetLinkButton_Click"></asp:LinkButton>
                            &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="TestScheduler_bottomCancelLinkButton" runat="server"
                                Text="Cancel" OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                        </td>
                    </tr>
                    <%-- Schedule Candiate Popup TR--%>
                    <tr>
                        <td>
                            <asp:Panel ID="TestScheduler_schedulePanel" runat="server" Style="display: none"
                                CssClass="popupcontrol_scheduler">
                                <div style="display: none;">
                                    <asp:Button ID="TestScheduler_hiddenButton" runat="server" Text="Hidden" /></div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="popup_td_padding_10">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                        <asp:Literal ID="TestScheduler_scheduleCandiateLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                    <td style="width: 50%" valign="top">
                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="TestScheduler_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popup_td_padding_10">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                <tr>
                                                    <td align="left" class="popup_td_padding_10">
                                                        <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg"
                                                            align="left">
                                                            <tr>
                                                                <td class="msg_align" colspan="4">
                                                                    <asp:Label ID="TestScheduler_scheduleErrorMsgLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="TestScheduler_Popup_candidateNameHeadLabel" runat="server" Text="Candidate Name"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    <span class='mandatory'>*</span>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TestScheduler_Popup_candidateNameTextBox" runat="server" ReadOnly="true"
                                                                        Text=""></asp:TextBox>&nbsp;<asp:ImageButton ID="TestScheduler_Popup_candidateNameImageButton"
                                                                            SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="TestScheduler_Popup_emailHeadLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TestScheduler_Popup_emailTextBox" runat="server" Text="" ReadOnly="true"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="TestScheduler_Popup_scheduleDateLabel" runat="server" Text="Test Expiry Date"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    <span class='mandatory'>*</span>
                                                                </td>
                                                                <td>
                                                                    <table width="97.5%" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="width: 40%">
                                                                                <asp:TextBox ID="TestScheduler_Popup_scheduleDateTextBox" runat="server" MaxLength="10"
                                                                                    AutoCompleteType="None"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:ImageButton ID="TestScheduler_Popup_calendarImageButton" SkinID="sknCalendarImageButton"
                                                                                    runat="server" ImageAlign="Middle" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <ajaxToolKit:MaskedEditExtender ID="TestScheduler_MaskedEditExtender" runat="server"
                                                                        TargetControlID="TestScheduler_Popup_scheduleDateTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                                                        OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Date"
                                                                        DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                    <ajaxToolKit:MaskedEditValidator ID="TestScheduler_MaskedEditValidator" runat="server"
                                                                        ControlExtender="TestScheduler_MaskedEditExtender" ControlToValidate="TestScheduler_Popup_scheduleDateTextBox"
                                                                        EmptyValueMessage="Expiry Date is required" InvalidValueMessage="Expiry Date is invalid"
                                                                        Display="None" TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                        ValidationGroup="MKE" />
                                                                    <ajaxToolKit:CalendarExtender ID="TestScheduler_customCalendarExtender" runat="server"
                                                                        TargetControlID="TestScheduler_Popup_scheduleDateTextBox" CssClass="MyCalendar"
                                                                        Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="TestScheduler_Popup_calendarImageButton" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="TestScheduler_Popup_sessionExpiryDateHeadLabel" runat="server" Text="Session Expiry Date"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="TestScheduler_Popup_sessionExpiryDateValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_2">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popup_td_padding_5">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td align="left" style="width: 40px;">
                                                        <asp:HiddenField ID="TestScheduler_Popup_candidateIDHiddenField" runat="server" />
                                                        <asp:Button ID="TestScheduler_saveButton" runat="server" SkinID="sknButtonId" Text="Save"
                                                            OnClick="TestScheduler_saveButton_Click" />
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="TestScheduler_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                                                            runat="server" Text="Cancel" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <ajaxToolKit:ModalPopupExtender ID="TestScheduler_scheduleModalPopupExtender" runat="server"
                                PopupControlID="TestScheduler_schedulePanel" TargetControlID="TestScheduler_hiddenButton"
                                BackgroundCssClass="modalBackground">
                            </ajaxToolKit:ModalPopupExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="TestScheduler_unscheduleCandidatePanel" runat="server" Style="display: none"
                                CssClass="popupcontrol_confirm_remove">
                                <div style="display: none;">
                                    <asp:Button ID="TestScheduler_hiddenUnscheduleButton" runat="server" Text="Hidden"
                                        SkinID="sknButtonId" />
                                </div>
                                <uc3:ConfirmMsgControl ID="TestScheduler_inactiveConfirmMsgControl" runat="server"
                                    Title="Unschedule" Type="YesNo" Message="Are you sure you want to cancel the scheduled test session?"
                                    OnOkClick="TestScheduler_OkClick" />
                            </asp:Panel>
                            <ajaxToolKit:ModalPopupExtender ID="TestScheduler_unschedulePopupExtenter" runat="server"
                                PopupControlID="TestScheduler_unscheduleCandidatePanel" TargetControlID="TestScheduler_hiddenUnscheduleButton"
                                BackgroundCssClass="modalBackground">
                            </ajaxToolKit:ModalPopupExtender>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <div style="display: none;">
                    <asp:Button ID="TestScheduler_hiddenViewScheduleButton" runat="server" Text="Hidden"
                        SkinID="sknButtonId" /></div>
                <asp:Panel ID="TestScheduler_hiddenViewSchedulePanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_question_detail">
                    <uc1:TestScheduleControl ID="TestScheduler_viewTestSchedule" runat="server" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="TestScheduler_viewScheduleModalPopupExtender"
                    runat="server" PopupControlID="TestScheduler_hiddenViewSchedulePanel" TargetControlID="TestScheduler_hiddenViewScheduleButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="TestScheduler_inactivepopupPanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_confirm_remove">
                    <div id="TestScheduler_hiddenDIV" style="display: none">
                        <asp:Button ID="TestScheduler_inactiveHiddenButton" runat="server" />
                    </div>
                    <uc3:ConfirmMsgControl ID="TestScheduler_inactivePopupExtenderControl" runat="server" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="TestScheduler_inactivepopupExtender" runat="server"
                    PopupControlID="TestScheduler_inactivepopupPanel" TargetControlID="TestScheduler_hiddenButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
                <%-- View Cancel Reason Popup --%>
                <asp:Panel ID="TestScheduler_cancelTestPanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_cancel_session">
                    <div style="display: none">
                        <asp:Button ID="TestScheduler_cancelReasonHiddenButton" runat="server" Text="Hidden" />
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="td_height_20">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="90%" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                            <asp:Label ID="TestScheduler_cancelReasonLiteral" runat="server" Text="Cancelled Test Session Reason"></asp:Label>
                                        </td>
                                        <td style="width: 25%" align="right">
                                            <asp:ImageButton ID="TestScheduler_cancelReasonCancelImageButton" runat="server"
                                                SkinID="sknCloseImageButton" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_20">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table width="100%"  class="popupcontrol_question_inner_bg" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="popup_td_padding_10">
                                                        <div runat="server" id="TestScheduler_cancelTestReasonDiv"  class="label_field_text" style="height: 70px; width: 400px;
                                                            overflow: auto;">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_8">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:LinkButton ID="TestScheduler_cancelReasonCancellationButton" runat="server"
                                                Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_height_8">
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="TestScheduler_cancelSessionModalPopupExtender"
                    runat="server" PopupControlID="TestScheduler_cancelTestPanel" TargetControlID="TestScheduler_cancelReasonHiddenButton"
                    CancelControlID="TestScheduler_cancelReasonCancellationButton" BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
                <asp:HiddenField ID="TestScheduler_testSessionAuthorIdHiddenField" runat="server" />
                <asp:HiddenField ID="TestScheduler_testSessionAuthorHiddenField" runat="server" />
                <asp:HiddenField ID="TestScheduler_candidateSessionIDHiddenField" runat="server" />
                <asp:HiddenField ID="TestScheduler_testSessionIDHiddenField" runat="server" />
                <asp:HiddenField ID="TestScheduler_isMaximizedHiddenField" runat="server" />
                <asp:HiddenField ID="TestScheduler_expiryDateHiddenField" runat="server" />
                <asp:HiddenField ID="TestScheduler_candidateEmailHiddenField" runat="server" />
                <asp:HiddenField ID="TestScheduler_sessoinExpiryDateHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
