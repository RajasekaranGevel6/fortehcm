﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EvaluateCandidate.aspx.cs"
    Inherits="Forte.HCM.UI.Scheduler.EvaluateCandidate" MasterPageFile="~/MasterPages/OTMMaster.Master"  %>

<%@ Register Src="~/CommonControls/TestScheduleControl.ascx" TagName="TestScheduleControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/OTMMaster.Master" %>
<asp:Content ID="EvaluateCandidate_bodyContent" runat="server" ContentPlaceHolderID="OTMMaster_body">
    <script language="javascript" type="text/javascript">
        // Validate the schedule popup. If candidate name or expiry date is empty, display the message to user.
        function VaildateSchedulePopup(nameTxt, expiryDateTxt, errorMsgLbl) {
            if (document.getElementById(nameTxt).value == "") {
                document.getElementById(errorMsgLbl).innerHTML = "Candidate name cannot be empty";
                return false;
            }
            else if (document.getElementById(expiryDateTxt).value == "") {
                document.getElementById(errorMsgLbl).innerHTML = "Expiry date cannot be empty";
                return false;
            }
        }

        // Pressing tab key from 'Test Session Id' textbox should 
        // load the test session and schedule candidate details.
        function TestSessionKeyDown(event, showButtonCtrl) {
            if (event.which || event.keyCode) {
                if ((event.which == 9) || (event.keyCode == 9)) {
                    var ctrlId = showButtonCtrl.toString();
                    document.getElementById(showButtonCtrl).click();
                    return;
                }
            }
        }

        function marksKeyDown(valueCtrl, errMsgCrtl) {                       
            if (document.getElementById(valueCtrl).value == "") {
                document.getElementById(errMsgCrtl).innerHTML = "Enter valid integer";
                return false;
            }
            if (isNaN(document.getElementById(valueCtrl).value)) {
                document.getElementById(errMsgCrtl).innerHTML = "Enter valid integer";
                return false;
            } else {
                document.getElementById(errMsgCrtl).innerHTML = "";
                document.getElementById(valueCtrl).focus();
                return true;
            }
        }

        // Function that shows the create candidate popup
        function ShowCandidatePopup(nameCtrl, emailCtrl, idCtrl) {
            var height = 540;
            var width = 980;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:"
            + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../popup/AddCandidate.aspx?namectrl=" + nameCtrl;
            queryStringValue += "&emailctrl=" + emailCtrl;
            queryStringValue += "&idctrl=" + idCtrl;
            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }
        
    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td valign="top">
                <asp:UpdatePanel ID="EvaluateCandidate_mainUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="3" cellpadding="0">
                            <tr>
                                <td class="header_bg">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="72%" class="header_text_bold">
                                                <asp:Literal ID="EvaluateCandidate_headerLiteral" runat="server" Text="Schedule Candidate"></asp:Literal>
                                            </td>
                                            <td width="28%" align="right">
                                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                                    <tr>
                                                        <td width="81%" align="right">
                                                            <asp:LinkButton ID="EvaluateCandidate_topResetLinkButton" runat="server" Text="Reset"
                                                                SkinID="sknActionLinkButton" OnClick="EvaluateCandidate_resetLinkButton_Click" />
                                                        </td>
                                                        <td width="4%" align="center" class="link_button">
                                                            |
                                                        </td>
                                                        <td width="15%" align="left">
                                                            <asp:LinkButton ID="EvaluateCandidate_topCancelButton" runat="server" Text="Cancel"
                                                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="msg_align">
                                    <asp:Label ID="EvaluateCandidate_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="EvaluateCandidate_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage">
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tab_body_bg">
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td>
                                                <div id="EvaluateCandidate_testSessionDetailsDiv" runat="server" style="display: block">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td class="td_height_8">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <asp:Label ID="EvaluateCandidate_testSessionIdHeadLabel" runat="server" Text="Test Session ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 18%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="EvaluateCandidate_testSessionIdTextBox" runat="server"></asp:TextBox></div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="EvaluateCandidate_testSessionImageButton" runat="server" ImageAlign="Middle"
                                                                                    SkinID="sknbtnSearchicon" ToolTip="Click here to search session ID" Visible="false" /></div>
                                                                        </td>
                                                                        <td style="width: 14%">
                                                                            <asp:Label ID="EvaluateCandidate_candidateSessionIdHeadLabel" runat="server" Text="Candidate Session ID(s)"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 18%">
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="EvaluateCandidate_candidateSessionIdTextBox" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="EvaluateCandidate_candidateSessionHelpImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Enter one or more candidate session ID's separated by commas"
                                                                                    Height="16px" /></div>
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:Button ID="EvaluateCandidate_showButton" runat="server" Text="Show Details"
                                                                                OnClick="EvaluateCandidate_showButton_Click" SkinID="sknButtonId" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div id="EvaluateCandidate_testDetailsDiv" runat="server" style="display: none">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class=" header_bg">
                                                                                <table cellpadding="0" cellspacing="0" width="98%" border="0">
                                                                                    <tr>
                                                                                        <td style="width: 93%" align="left">
                                                                                            <asp:Literal ID="EvaluateCandidate_testDetailsLiteral" runat="server" Text="Test Session Details"></asp:Literal>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="grid_body_bg">
                                                                                <table cellpadding="0" cellspacing="5" width="100%" border="0">
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="EvaluateCandidate_testIDHeadLabel" runat="server" Text="Test ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="EvaluateCandidate_testIDLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                                Text=""></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 9%">
                                                                                            <asp:Label ID="EvaluateCandidate_testNameHeadLabel" runat="server" Text="Test Name"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 21%">
                                                                                            <asp:Label ID="EvaluateCandidate_testNameLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 12%">
                                                                                            <asp:Label ID="EvaluateCandidate_testAuthorHeadLabel" runat="server" Text="Test Author"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 12%">
                                                                                            <asp:Label ID="EvaluateCandidate_testAuthorLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 9%">
                                                                                            <asp:Label ID="EvaluateCandidate_emailHeadLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 17%">
                                                                                            <asp:Label ID="EvaluateCandidate_emailLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="EvaluateCandidate_timeLimitHeadLabel" runat="server" Text="Time Limit"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EvaluateCandidate_timeLimitLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EvaluateCandidate_expiryDateHeadLabel" runat="server" Text="Expiry Date"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EvaluateCandidate_expiryDateLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EvaluateCandidate_cyberProctorateHeadLabel" runat="server" Text="Cyber Proctoring"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EvaluateCandidate_cyberProctorateLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 11%">
                                                                                            <asp:Label ID="EvaluateCandidate_displayResultsHeadLabel" runat="server" Text="Display Results"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="EvaluateCandidate_displayResultsLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                                Text=""></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="EvaluateCandidate_positionProfileHeadLabel" runat="server" Text="Position Profile"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="7">
                                                                                            <asp:Label ID="EvaluateCandidate_positionProfileLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4">
                                                                                            <asp:Label ID="EvaluateCandidate_testDescHeadLabel" runat="server" Text="Session Description"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="4">
                                                                                            <asp:Label ID="EvaluateCandidate_instructionsLabel" runat="server" Text="Instructions"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4" valign="top">
                                                                                            <div class="label_multi_field_text" style="height: 112px; overflow: auto; word-wrap: break-word;
                                                                                                white-space: normal;">
                                                                                                <asp:Literal ID="EvaluateCandidate_testDescLiteral" runat="server" SkinID="sknMultiLineText"
                                                                                                    Text="Screening candidates for Yes Banking project. The candidates must have minimum 3 years experience in the field of ASP.Net UI design, Java Script, C# Programming & OOPS."></asp:Literal>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td colspan="4" valign="top">
                                                                                            <div class="label_multi_field_text" style="height: 112px; overflow: auto; word-wrap: break-word;
                                                                                                white-space: normal;">
                                                                                                <asp:Literal ID="EvaluateCandidate_instructionsLiteral" runat="server" SkinID="sknMultiLineText"
                                                                                                    Text="Candidates are requested to abide to the rules and regulations of the test. Since cyber proctoring is enabled the scores will be calculated only based on the test tracking details."></asp:Literal>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="EvaluateCandidate_testDetailsGridDiv" runat="server" style="display: none">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr id="EvaluateCandidate_searchTestResultsTR" runat="server">
                                                            <td class="header_bg">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td id="EvaluateCandidate_candidateSessionHeader" runat="server">
                                                                            <asp:Literal ID="EvaluateCandidate_candidateSessionDetailsLiteral" runat="server"
                                                                                Text="Candidate Session Details"></asp:Literal>
                                                                        </td>
                                                                        <td align="right" id="EvaluateCandidate_searchResultsTR" runat="server">
                                                                            <span id="EvaluateCandidate_searchResultsUpSpan" style="display: none;" runat="server">
                                                                                <asp:Image ID="EvaluateCandidate_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                            </span><span id="EvaluateCandidate_searchResultsDownSpan" style="display: block;"
                                                                                runat="server">
                                                                                <asp:Image ID="EvaluateCandidate_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="grid_body_bg">
                                                                <div id="EvaluateCandidate_candidateSessionGridViewDIV" runat="server" style="height: 200px;
                                                                    overflow: auto;">
                                                                    <asp:UpdatePanel ID="EvaluateCandidate_candidateSessionGridViewUpdatePanel" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:GridView ID="EvaluateCandidate_candidateSessionGridView" runat="server" AutoGenerateColumns="false"
                                                                                AllowSorting="True" OnRowCommand="EvaluateCandidate_candidateSessionGridView_RowCommand"
                                                                                OnRowDataBound="EvaluateCandidate_candidateSessionGridView_RowDataBound" OnSorting="EvaluateCandidate_candidateSessionGridView_Sorting"
                                                                                OnRowCreated="EvaluateCandidate_candidateSessionGridView_RowCreated" SkinID="sknWrapHeaderGrid">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-Width="10%">
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="EvaluateCandidate_evaluateImageButtion" runat="server" SkinID="sknEvaluateImageButton"
                                                                                                ToolTip="Evaluate Candidate" CommandName="evaluate" Visible="true" CommandArgument='<%# Eval("CandidateTestSessionID") %>' />
                                                                                            <asp:HyperLink ID="EvaluateCandidate_viewTestSummaryHyperLink" runat="server" Target="_blank"
                                                                                                ToolTip="View Test Summary" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_test_result.gif">
                                                                                            </asp:HyperLink>
                                                                                            <asp:HyperLink ID="EvaluateCandidate_viewResultHyperLink" runat="server" Target="_blank"
                                                                                                ToolTip="Candidate Test Details" ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif">
                                                                                            </asp:HyperLink>
                                                                                            <asp:HiddenField ID="EvaluateCandidate_candidateTestSessionIDHiddenField" runat="server"
                                                                                                Value='<%# Eval("CandidateTestSessionID") %>' />
                                                                                            <asp:HiddenField ID="EvaluateCandidate_attemptIDHiddenField" runat="server" Value='<%# Eval("AttemptID") %>' />
                                                                                            <asp:HiddenField ID="EvaluateCandidate_requestRetakeHiddenField" runat="server" Value='<%# Eval("RetakeRequest") %>' />
                                                                                            <asp:HiddenField ID="EvaluateCandidate_modifiedDateHiddenField" runat="server" Value='<%# Eval("ModifiedDate") %>' />
                                                                                            <asp:HiddenField ID="EvaluateCandidate_Grid_ExpiryDateHiddenField" runat="server"
                                                                                                Value='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateTestSessionDetail)Container.DataItem).ScheduledDate) %>' />
                                                                                            <asp:HiddenField ID="EvaluateCandidate_Grid_candidateEmailHiddenField" runat="server"
                                                                                                Value='<%# Eval("Email") %>' />
                                                                                            <asp:HiddenField ID="EvaluateCandidate_Grid_candidateNameHiddenField" runat="server"
                                                                                                Value='<%# Eval("CandidateFirstName") %>' />
                                                                                            <asp:HiddenField ID="EvaluateCandidate_Grid_candidateIDHiddenField" runat="server"
                                                                                                Value='<%# Eval("CandidateID") %>' />
                                                                                            <asp:HiddenField ID="EvaluateCandidate_Grid_emailReminderHiddenField" runat="server"
                                                                                                Value='<%# Eval("EmailReminder") %>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Candidate Session ID" SortExpression="CandidateSessionId"
                                                                                        ItemStyle-Width="12%" HeaderStyle-CssClass="td_padding_right_20">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="EvaluateCandidate_candidateSessionLabel" runat="server" Text='<%# Eval("CandidateTestSessionID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Candidate Name" SortExpression="CandidateName" ItemStyle-Width="22%">
                                                                                        <ItemTemplate>
                                                                                            <asp:HyperLink ID="EvaluateCandidate_candidateHyperLink" ToolTip='<%# Eval("CandidateFullName") %>'
                                                                                                runat="server" Text='<%# Eval("CandidateFirstName") %>' Target="_blank" SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Email ID" SortExpression="EmailId" HeaderStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="EvaluateCandidate_emailLabel" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Status" SortExpression="Status" HeaderStyle-Width="10%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="EvaluateCandidate_statusLabel" runat="server" Text='<%# GetStatus(((Forte.HCM.DataObjects.CandidateTestSessionDetail)Container.DataItem).Status) %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Expiry Date" SortExpression="ScheduledDate DESC">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="EvaluateCandidate_scheduledDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateTestSessionDetail)Container.DataItem).ScheduledDate) %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Completed Date" SortExpression="DateCompleted DESC">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="EvaluateCandidate_completedDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateTestSessionDetail)Container.DataItem).DateCompleted) %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="msg_align">
                                    <asp:Label ID="EvaluateCandidate_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="EvaluateCandidate_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="header_bg" align="right">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="72%" class="header_text_bold">
                                            </td>
                                            <td width="28%" align="right">
                                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                                    <tr>
                                                        <td width="81%" align="right">
                                                            <asp:LinkButton ID="EvaluateCandidate_bottonResetLinkButton" runat="server" Text="Reset"
                                                                SkinID="sknActionLinkButton" OnClick="EvaluateCandidate_resetLinkButton_Click" />
                                                        </td>
                                                        <td width="4%" align="center" class="link_button">
                                                            |
                                                        </td>
                                                        <td width="15%" align="left">
                                                            <asp:LinkButton ID="EvaluateCandidate_bottonCancelLinkButton" runat="server" Text="Cancel"
                                                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="EvaluateCandidate_evaluatePanel" runat="server" Style="display: none"
                                        CssClass="popupcontrol_evaluate">
                                        <div style="display: none;">
                                            <asp:Button ID="EvaluateCandidate_hiddenButton" runat="server" Text="Hidden" /></div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Literal ID="EvaluateCandidate_titleLiteral" runat="server" Text="Evaluate"></asp:Literal>
                                                            </td>
                                                            <td style="width: 50%" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="EvaluateCandidate_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                        <tr>
                                                            <td align="left" class="popup_td_padding_10">
                                                                <table border="0" cellpadding="0" cellspacing="5" width="100%" class="tab_body_bg"
                                                                    align="left">
                                                                    <tr>
                                                                        <td class="msg_align" colspan="4">
                                                                            <asp:Label ID="EvaluateCandidate_scheduleErrorMsgLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div style="height: 300px; overflow: auto; border-color: #D8D8D8; border-width: 1px;
                                                                                border-style: Solid; background-color: #f8f8f8; height: 235px;">
                                                                                <%--<asp:GridView ID="EvaluateCandidate_answerGridView" runat="server" AutoGenerateColumns="False"
                                                                                    ShowHeader="False" SkinID="sknNewGridView" OnRowDataBound="EvaluateCandidate_answerGridView_OnRowDataBound">
                                                                                    <Columns>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                                                    <tr>
                                                                                                        <td valign="middle" style="width: 100%">
                                                                                                            <asp:HiddenField ID="EvaluateCandidate_answerGridView_questionKeyHiddenField" runat="server"
                                                                                                                Value='<%# Eval("QuestionKey") %>' />
                                                                                                            <asp:HiddenField ID="EvaluateCandidate_answerGridView_marksHiddenField" runat="server"
                                                                                                                Value='<%# Eval("Marks") %>' />
                                                                                                                <asp:HiddenField ID="EvaluateCandidate_answerGridView_StatusHiddenField" runat="server"
                                                                                                                Value='<%# Eval("Status") %>' />
                                                                                                            <div style="float: left; overflow: auto; height: 40px">
                                                                                                                <div style="float: left">
                                                                                                                    <asp:Image ID="EvaluateCandidate_answerGridView_questionImage" runat="server" SkinID="sknQuestionImage"
                                                                                                                        ToolTip="Question" /></div>
                                                                                                                <div style="float: left; padding-left: 10px">
                                                                                                                    <span class="sno_label">
                                                                                                                        <%# Eval("QuestionSNo") %>.</span>
                                                                                                                    <asp:Label ID="EvaluateCandidate_answerGridView_questionLabel" runat="server" SkinID="sknLabelEvaluateQuestion"
                                                                                                                        Text='<%# Eval("QuestionDescription") %>'></asp:Label></div>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="middle" style="width: 100%">
                                                                                                            <asp:Label ID="EvaluateCandidate_answerGridView_answerReferenceLabel" runat="server"
                                                                                                                Text="Answer Reference" SkinID="sknLabelFieldHeaderText"></asp:Label><span class="info_label">
                                                                                                                    (used as a reference only)</span>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="middle" style="width: 100%">
                                                                                                            <asp:TextBox ID="EvaluateCandidate_answerGridView_answerReferenceTextBox" runat="server"
                                                                                                                SkinID="sknMultiLineTextBox" TextMode="MultiLine" Width="95%" Height="40" Text='<%# Eval("AnswerReference") %>'
                                                                                                                ReadOnly="true"></asp:TextBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="middle" style="width: 100%">
                                                                                                            <asp:Label ID="EvaluateCandidate_answerGridView_answerLabel" runat="server" Text="Answer"
                                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label><span class="info_label"> (response by
                                                                                                                    candidate)</span>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="middle" style="width: 100%">
                                                                                                            <asp:TextBox ID="EvaluateCandidate_answerGridView_answerTextBox" runat="server" SkinID="sknMultiLineTextBox"
                                                                                                                TextMode="MultiLine" Width="95%" Height="40" Text='<%# Eval("Answer") %>' ReadOnly="true"></asp:TextBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="middle" style="width: 100%">
                                                                                                            <div style="float: left; padding-top: 6px">
                                                                                                                <div style="float: left">
                                                                                                                    <asp:Label ID="EvaluateCandidate_answerGridView_marksLabel" runat="server" Text="Marks"
                                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                                </div>
                                                                                                                <div style="float: left; padding-left: 4px">
                                                                                                                    <asp:TextBox ID="EvaluateCandidate_answerGridView_marksObtainedTextBox" runat="server"
                                                                                                                        Text='<%# Eval("MarksObtained") %>' MaxLength="5" Width="30px"></asp:TextBox><span class="marks_info_label"> out of </span><span class="marks_info_value_label"><%# Eval("Marks")%> </span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>--%>
                                                                                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                                    <tr>
                                                                                        <td valign="middle" style="width: 100%">
                                                                                            <asp:HiddenField ID="EvaluateCandidate_answerGridView_questionKeyHiddenField" runat="server"
                                                                                                Value="0" />
                                                                                            <asp:HiddenField ID="EvaluateCandidate_answerGridView_marksHiddenField" runat="server"
                                                                                                Value="0" />
                                                                                            <asp:HiddenField ID="EvaluateCandidate_answerGridView_StatusHiddenField" runat="server"
                                                                                                Value="" />
                                                                                            <div style="float: left; overflow: auto; height: 40px">
                                                                                                <div style="float: left">
                                                                                                    <asp:Image ID="EvaluateCandidate_answerGridView_questionImage" runat="server" SkinID="sknQuestionImage"
                                                                                                        ToolTip="Question" /></div>
                                                                                                <div style="float: left; padding-left: 10px">
                                                                                                    <span class="sno_label"><asp:Label runat="server" ID="EvaluateCandidate_answerGridView_questionNo" SkinID="sknLabelEvaluateQuestion"></asp:Label>.</span>
                                                                                                    <asp:Label ID="EvaluateCandidate_answerGridView_questionLabel" runat="server" SkinID="sknLabelEvaluateQuestion"
                                                                                                        Text=""></asp:Label></div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="middle" style="width: 100%">
                                                                                            <asp:Label ID="EvaluateCandidate_answerGridView_answerReferenceLabel" runat="server"
                                                                                                Text="Answer Reference" SkinID="sknLabelFieldHeaderText"></asp:Label><span class="info_label">
                                                                                                    (used as a reference only)</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="middle" style="width: 100%">
                                                                                            <asp:TextBox ID="EvaluateCandidate_answerGridView_answerReferenceTextBox" runat="server"
                                                                                                SkinID="sknMultiLineTextBox" TextMode="MultiLine" Width="95%" Height="40" Text=""
                                                                                                ReadOnly="true"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="middle" style="width: 100%">
                                                                                            <asp:Label ID="EvaluateCandidate_answerGridView_answerLabel" runat="server" Text="Answer"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label><span class="info_label"> (response by
                                                                                                    candidate)</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="middle" style="width: 100%">
                                                                                            <asp:TextBox ID="EvaluateCandidate_answerGridView_answerTextBox" runat="server" SkinID="sknMultiLineTextBox"
                                                                                                TextMode="MultiLine" Width="95%" Height="40" Text="" ReadOnly="true"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="middle" style="width: 100%">
                                                                                            <div style="float: left; padding-top: 6px">
                                                                                                <div style="float: left">
                                                                                                    
                                                                                                </div>
                                                                                                <div style="float: left; padding-left: 4px;background-color:White;">
                                                                                                    <asp:TextBox ID="EvaluateCandidate_answerGridView_marksObtainedTextBox" runat="server"
                                                                                                        Text="" MaxLength="5" Width="30px"></asp:TextBox><span class="marks_info_label"> out
                                                                                                            of </span><span class="marks_info_value_label"> <asp:Label ID="EvaluateCandidate_answerGridView_marksLabel" runat="server" Text="Marks"></asp:Label></span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="border-color: #D8D8D8; border-width: 1px; border-style: Solid; width: 100%;
                                                                        border-collapse: collapse; background-color: #283033; height: 25px;">
                                                                        <td>
                                                                            <div>
                                                                                <table style="margin: 0 auto">
                                                                                    <tr>
                                                                                        <td style="padding-right:10px">
                                                                                        <asp:Button ID="EvaluateCandidate_prevButton" runat="server" SkinID="sknButtonWithImage" Text="<" OnClick="EvaluateCandidate_prevButton_Click"/>
                                                                                           <%-- <asp:ImageButton ImageUrl="~/App_Themes/DefaultTheme/Images/backwardpng.png" ID="EvaluateCandidate_prevButton"
                                                                                                runat="server" SkinID="sknImageButtonId" Height="10px" Width="10px" />--%>
                                                                                        </td>
                                                                                        <td style="background-color: white;padding-left: 5px;padding-right: 5px;">
                                                                                            <div>
                                                                                                <asp:Label ID="EvaluateCandidate_currentQuestCountLabel" runat="server" SkinID="sknLabelTestPerformProgressText"
                                                                                                    Text=""></asp:Label>
                                                                                                <asp:Label ID="EvaluateCandidate_ofLabel" runat="server" SkinID="sknLabelTestPerformProgressText"
                                                                                                    Text=" of "></asp:Label>
                                                                                                <asp:Label ID="EvaluateCandidate_totalQuestCountLabel" runat="server" SkinID="sknLabelTestPerformProgressText"
                                                                                                    Text=""></asp:Label>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="padding-left:10px">
                                                                                        <asp:Button ID="EvaluateCandidate_nextButton" runat="server" SkinID="sknButtonWithImage" Text=">" OnClick="EvaluateCandidate_nextButton_Click" />

                                                                                            <%--<asp:ImageButton ImageUrl="~/App_Themes/DefaultTheme/Images/forwardpng.png" ID="EvaluateCandidate_nextButton"
                                                                                                runat="server" SkinID="sknImageButtonId" Height="10px" Width="10px" />--%>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="border-color: #D8D8D8; border-width: 1px; border-style: Solid; width: 100%;
                                                                        border-collapse: collapse; background-color: #283033;">
                                                                        <td>
                                                                            <div>
                                                                                <table style="margin: 0 auto">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Button ID="EvaluateCandidate_saveButton" runat="server" SkinID="sknButtonId"
                                                                                                Text="Save" OnClick="EvaluateCandidate_saveButton_Click" />
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Button ID="EvaluateCandidate_submitButton" runat="server" SkinID="sknButtonId"
                                                                                                Text="Submit" OnClick="EvaluateCandidate_submitButton_Click" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popup_td_padding_5">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td align="left" style="width: 20px; padding-right: 5px">
                                                                <asp:HiddenField ID="EvaluateCandidate_candidateIdHiddenField" runat="server" />
                                                                <%--<asp:Button ID="EvaluateCandidate_saveButton" runat="server" SkinID="sknButtonId"
                                                                    Text="Save" OnClick="EvaluateCandidate_saveButton_Click" />--%>
                                                            </td>
                                                            <td style="padding-right: 5px">
                                                                <%--<asp:Button ID="EvaluateCandidate_submitButton" runat="server" SkinID="sknButtonId"
                                                                    Text="Submit" OnClick="EvaluateCandidate_submitButton_Click" />--%>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="EvaluateCandidate_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                                                                    runat="server" Text="Cancel" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="EvaluateCandidate_evaluateModalPopupExtender"
                                        runat="server" PopupControlID="EvaluateCandidate_evaluatePanel" TargetControlID="EvaluateCandidate_hiddenButton"
                                        BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="EvaluateCandidate_isMaximizedHiddenField" runat="server" />
                                    <asp:HiddenField ID="EvaluateCandidate_candidateNameHiddenField" runat="server" />
                                    <asp:HiddenField ID="EvaluateCandidate_reScheduleCandidateIDHiddenField" runat="server" />
                                    <asp:HiddenField ID="EvaluateCandidate_sessionExpiryDateHiddenField" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
