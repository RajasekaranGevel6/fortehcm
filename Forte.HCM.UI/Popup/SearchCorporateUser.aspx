<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="SearchCorporateUser.aspx.cs"  Inherits="Forte.HCM.UI.Popup.SearchCorporateUser" %>
    <%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="AddAssessorConfirmMsgControl"
    TagPrefix="uc2" %> 
<asp:Content ID="SearchCorporateUser_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
<script type="text/javascript" language="javascript">

    // Handler method that will be called when the 'Select' link is 
    // clicked in the grid row.
    function OnSelectClick(ctrl) 
    {
        var firstNameCtrl = '<%= Request.QueryString["firstName"] %>';
        var lastNameCtrl = '<%= Request.QueryString["lastName"] %>';
        var userNameCtrl = '<%= Request.QueryString["userName"] %>';
        var useridCtrl = '<%= Request.QueryString["userID"] %>';
       // var btnCnrl = '<%= Request.QueryString["btnCnrl"] %>';

        if (window.dialogArguments) 
        {
            window.opener = window.dialogArguments;
        }

        // Set the user name field value. Replace the 'link button name' with the 
        // 'user name hidden field name' and retrieve the value.
        if (useridCtrl != null && useridCtrl != '') 
        {
            window.opener.document.getElementById(useridCtrl).value
                = document.getElementById(ctrl.id.replace("SearchCorporateUser_selectLinkButton", "SearchCorporateUser_userIDHiddenfield")).value;
        }
        // Set the user name field value. Replace the 'link button name' with the 
        // 'user name hidden field name' and retrieve the value.
        if (userNameCtrl != null && userNameCtrl != '') 
        {
            window.opener.document.getElementById(userNameCtrl).value
                = document.getElementById(ctrl.id.replace("SearchCorporateUser_selectLinkButton", "SearchCorporateUser_userEmailHiddenfield")).value;
        }

        // Set the user name field value. Replace the 'link button name' with the 
        // 'user name hidden field name' and retrieve the value.
        if (firstNameCtrl != null && firstNameCtrl != '') 
        {
            window.opener.document.getElementById(firstNameCtrl).value
                = document.getElementById(ctrl.id.replace("SearchCorporateUser_selectLinkButton", "SearchCorporateUser_userFirstNameHiddenfield")).value;
        }
        // Set the user name field value. Replace the 'link button name' with the 
        // 'user name hidden field name' and retrieve the value.
        if (lastNameCtrl != null && lastNameCtrl != '') 
        {
            window.opener.document.getElementById(lastNameCtrl).value
                = document.getElementById(ctrl.id.replace("SearchCorporateUser_selectLinkButton", "SearchCorporateUser_userLastNameHiddenfield")).value;
        }

        // Trigger the click event of the refresh button in the parent page.
        //if (btnCnrl != null && window.opener.document.getElementById(btnCnrl) != null)
        //   window.opener.document.getElementById(btnCnrl).click();

        self.close();
    }

    </script>
  
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchCorporateUser_headerLiteral" runat="server" Text="Search User"></asp:Literal>
                            <asp:HiddenField ID="SearchCorporateUser_browserHiddenField" runat="server" />
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SearchCorporateUser_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:UpdatePanel ID="SearchCorporateUser_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="SearchCorporateUser_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="SearchCorporateUser_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchCorporateUser_searchButton" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchCorporateUser_pageNavigator" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="SearchCorporateUser_searchCriteriasDiv" runat="server" style="display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="SearchCorporateUser_searchCriteriaUpdatePanel" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <table width="100%" border="0" cellspacing="3%" cellpadding="2%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="SearchCorporateUser_userNameLabel" runat="server" Text="User ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchCorporateUser_userNameTextBox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="SearchCorporateUser_firstNameLabel" runat="server" Text="First Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchCorporateUser_firstNameTextBox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="SearchCorporateUser_middleNameLabel" runat="server" Text="Middle Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchCorporateUser_middleNameTextBox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="SearchCorporateUser_lastNameLabel" runat="server" Text="Last Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchCorporateUser_lastNameTextBox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="4">
                                                                            <asp:Button ID="SearchCorporateUser_searchButton" runat="server" SkinID="sknButtonId" Text="Search"
                                                                                OnClick="SearchCorporateUser_searchButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="SearchCorporateUser_searchResultsUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr id="SearchCorporateUser_searchTestResultsTR" runat="server">
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 50%">
                                                                        <asp:Literal ID="SearchCorporateUser_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                            ID="SearchQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 50%" align="right">
                                                                        <span id="SearchCorporateUser_searchResultsUpSpan" style="display: none;" runat="server">
                                                                            <asp:Image ID="SearchCorporateUser_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                        </span><span id="SearchCorporateUser_searchResultsDownSpan" style="display: block;" runat="server">
                                                                            <asp:Image ID="SearchCorporateUser_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="height: 220px; overflow: auto;" runat="server" id="SearchCorporateUser_testDiv">
                                                                            <asp:GridView ID="SearchCorporateUser_testGridView" runat="server" 
                                                                                AllowSorting="True" AutoGenerateColumns="False"
                                                                                GridLines="Horizontal" BorderColor="white" BorderWidth="1px" Width="100%" 
                                                                                OnSorting="SearchCorporateUser_testGridView_Sorting" OnRowDataBound="SearchCorporateUser_testGridView_RowDataBound"
                                                                                OnRowCreated="SearchCorporateUser_testGridView_RowCreated" 
                                                                                onrowcommand="SearchCorporateUser_testGridView_RowCommand">
                                                                                <RowStyle CssClass="grid_alternate_row" />
                                                                                    <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                                    <HeaderStyle CssClass="grid_header_row" />
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="SearchCorporateUser_selectLinkButton" runat="server" Text="Select" 
                                                                                            OnClientClick="javascript:return OnSelectClick(this);"
                                                                                             Visible='<% # Convert.ToBoolean(IsAssessor(Convert.ToString(Eval("IsAssessor"))))  %>'
                                                                                                ToolTip="Select"></asp:LinkButton>
                                                                                            <asp:CheckBox ID="SearchCorporateUser_userSelectCheckbox" runat="server"/>
                                                                                            <asp:HiddenField ID="SearchCorporateUser_userNameHiddenfield" runat="server" Value='<%# Eval("UserName") %>' />
                                                                                            <asp:HiddenField ID="SearchCorporateUser_userIDHiddenfield" runat="server" Value='<%# Eval("UserID") %>' />
                                                                                            <asp:HiddenField ID="SearchCorporateUser_userFirstNameHiddenfield" runat="server" Value='<%# Eval("FIRSTNAME") %>' />
                                                                                            <asp:HiddenField ID="SearchCorporateUser_userLastNameHiddenfield" runat="server" Value='<%# Eval("LastName") %>' />
                                                                                            <asp:HiddenField ID="SearchCorporateUser_userEmailHiddenfield" runat="server" Value='<%# Eval("Email") %>' />
                                                                                            <asp:HiddenField ID="SearchCorporateUser_userIsOwnerHiddenfield" runat="server" Value='<%# Eval("IsOwner") %>' />
                                                                                             <asp:ImageButton ID="SearchCorporateUser_createAssessorImageButton"
                                                                                                runat="server" CommandName="CreateAssessor" CommandArgument='<%# Eval("UserID") %>' 
                                                                                                SkinID="sknCreateCandidateUserNameImageButton"  ToolTip="Mark As An Assessor" Visible='<% # Convert.ToBoolean(IsNotAssessor(Convert.ToString(Eval("IsAssessor"))))  %>'/>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="User_ID" DataField="UserID" Visible="false" />
                                                                                    <asp:BoundField HeaderText="User ID" DataField="UserName" SortExpression="USERNAME" />
                                                                                    <asp:BoundField HeaderText="First Name" DataField="FirstName" SortExpression="FIRSTNAME" />
                                                                                    <asp:BoundField HeaderText="Middle Name" DataField="MiddleName" SortExpression="MIDDLENAME" />
                                                                                    <asp:BoundField HeaderText="Last Name" DataField="LastName" SortExpression="LASTNAME" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <uc1:PageNavigator ID="SearchCorporateUser_pageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <asp:HiddenField ID="SearchCorporateUser_createAssessorIdHiddenField" runat="server" />
                                                            <asp:Panel ID="SearchCorporateUser_createAssessorPopupPanel" runat="server" Style="display: none"
                                                                CssClass="client_confirm_message_box">
                                                                <uc2:AddAssessorConfirmMsgControl ID="SearchCorporateUser_createAssessorConfirmMsgControl"
                                                                    runat="server" OnCancelClick="SearchCorporateUser_createAssessorConfirmMsgControl_CancelClick"
                                                                    OnOkClick="SearchCorporateUser_createAssessorConfirmMsgControl_OkClick" />
                                                            </asp:Panel>
                                                            <ajaxToolKit:ModalPopupExtender ID="SearchCorporateUser_createAssessorModalPopupExtender"
                                                                runat="server" PopupControlID="SearchCorporateUser_createAssessorPopupPanel" TargetControlID="SearchCorporateUser_createAssessorIdHiddenField"
                                                                BackgroundCssClass="modalBackground">
                                                            </ajaxToolKit:ModalPopupExtender>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchCorporateUser_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_5">
                <table  cellpadding="0" cellspacing="0">
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Button ID="SearchCorporateUser_selectButton" runat="server" SkinID="sknButtonId" Text="Select"
                                OnClick="SearchCorporateUser_selectButton_Click" />
                        </td>
                        <td>&nbsp;</td>
                        <td valign="top">
                            <asp:LinkButton ID="SearchCorporateUser_topResetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="SearchCorporateUser_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="SearchCorporateUser_topCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="SearchCorporateUser_isMaximizedHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
