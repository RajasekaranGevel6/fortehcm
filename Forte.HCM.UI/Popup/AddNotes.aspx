﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddNotes.aspx.cs" MasterPageFile="~/MasterPages/PopupMaster.Master"
    Inherits="Forte.HCM.UI.Popup.AddNotes"  %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="AddNotes_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="AddNotes_titleLiteral" runat="server">Add Notes</asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="AddNotes_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" style="width: 100%">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_subscription_type_bg">
                    <tr>
                        <td style="width: 100%">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="AddNotes_topErrorMessageLabel" runat="server" EnableViewState="false"
                                            SkinID="sknErrorMessage" Width="100%"></asp:Label>
                                        <asp:Label ID="AddNotes_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                            SkinID="sknSuccessMessage" Width="100%"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 100%">
                                        <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                            <tr>
                                                <td style="width: 39%" align="left">
                                                    <asp:Label ID="AddNotes_notesLabel" runat="server" Text="Notes" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    <span class="mandatory">*</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%; height: 90px">
                                                    <asp:TextBox ID="AddNotes_notesTextBox" runat="server" MaxLength="2000" Width="99%"
                                                        Height="100%" TextMode="MultiLine" onchange="CommentsCount(2000,this)" onkeyup="CommentsCount(2500,this)"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8" colspan="2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_10">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="middle" align="left">
                            <asp:Button ID="AddNotesControl_addButton" runat="server" Text="Add" SkinID="sknButtonId"
                                ValidationGroup="a" OnClick="AddNotes_addButton_Click" />
                        </td>
                        <td valign="middle" align="left">
                            &nbsp;<asp:LinkButton ID="AddNotesControl_resetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="AddNotes_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="middle">
                            |
                        </td>
                        <td valign="middle" align="left">
                            <asp:LinkButton ID="AddNotes_cancelLinkButton" runat="server" Text="Cancel" SkinID="sknPopupLinkButton"
                                OnClientClick="javascript:CloseMe();"> </asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10">
            </td>
        </tr>
    </table>
</asp:Content>
