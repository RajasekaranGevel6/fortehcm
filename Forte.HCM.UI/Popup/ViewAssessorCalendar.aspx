﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewAssessorCalendar.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.ViewAssessorCalendar"
    Title="Assessor Calendar" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ViewAssessorCalendar_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        function CollapseAllRows(targetControl)
        {
            var gridCtrl = document.getElementById("<%= ViewAssessorCalendar_timeSlotsGridView_resultsDiv.ClientID %>");
            if (gridCtrl != null)
            {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++)
                {
                    if (rowItems[indexRow].id.indexOf("ViewAssessorCalendar_timeSlotsGridView_detailsDiv") != "-1")
                    {
                        rowItems[indexRow].style.display = "none";
                    }
                }
            }

            return false;
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="td_padding_top_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="ViewAssessorCalendar_titleLiteral" runat="server">Assessor Calendar</asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="ViewAssessorCalendar_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <table width="100%" cellpadding="0" cellspacing="3" border="0" class="popupcontrol_assessor_calendar_slot_bg">
                    <tr>
                        <td style="width: 100%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="td_height_20">
                                        <asp:UpdatePanel ID="ViewAssessorCalendar_messageUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="ViewAssessorCalendar_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknErrorMessage" Width="100%" Text=""></asp:Label>
                                                <asp:Label ID="ViewAssessorCalendar_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknSuccessMessage" Width="100%" Text=""></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 100%">
                                        <asp:UpdatePanel ID="ViewAssessorCalendar_inputControlsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2" style="width: 100%" valign="middle" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="center" class="request_time_slot_details_bg" valign="middle">
                                                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td style="width: 10%" align="left" class="td_height_20" valign="middle">
                                                                                    <asp:Label ID="ViewAssessorCalendar_assessorNameLabel" runat="server" Text="Assessor"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 89%" align="left" valign="middle">
                                                                                    <asp:Label ID="ViewAssessorCalendar_assessorNameValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                        Text=""></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 26%" valign="top" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="ViewAssessorCalendar_calendarLiteral" runat="server" Text="Calendar"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="view_assessor_calendar_grid_body_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Calendar ID="ViewAssessorCalendar_selectDateCalendar" runat="server" BackColor="White"
                                                                                        BorderColor="Black" BorderWidth="1px" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black"
                                                                                        Height="168px" Width="188px" SelectionMode="Day" OtherMonthDayStyle-Font-Underline="false"
                                                                                        DayHeaderStyle-Font-Underline="false" ShowGridLines="false" DayHeaderStyle-Font-Overline="false"
                                                                                        DayStyle-Font-Underline="false" DayStyle-Font-Overline="false" Font-Underline="false"
                                                                                        TodayDayStyle-Font-Underline="false" OnPreRender="ViewAssessorCalendar_selectDateCalendar_PreRender"
                                                                                        OnSelectionChanged="ViewAssessorCalendar_selectDateCalendar_SelectionChanged"
                                                                                        OnVisibleMonthChanged="ViewAssessorCalendar_selectDateCalendar_VisibleMonthChanged"
                                                                                        OnDayRender="ViewAssessorCalendar_selectDateCalendar_DayRender">
                                                                                        <DayStyle Font-Underline="false" Wrap="false" CssClass="no_underline" />
                                                                                        <SelectedDayStyle Font-Underline="false" BackColor="#59C1EE" ForeColor="Black"></SelectedDayStyle>
                                                                                        <OtherMonthDayStyle Font-Underline="false" BackColor="#EEEEEE" ForeColor="Black" />
                                                                                        <TodayDayStyle Font-Underline="false" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                                                                    </asp:Calendar>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="height: 2px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center" style="padding-left: 1px">
                                                                                    <table cellpadding="0" cellspacing="1" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td style="width: 10%; background-color: #59c1ee; height: 18px;" align="center">
                                                                                            </td>
                                                                                            <td style="width: 90%; padding-left: 4px" align="left">
                                                                                                <asp:Label ID="ViewAssessorCalendar_legendSelectedDateCaptionLabel" runat="server"
                                                                                                    Text="Selected Date" SkinID="sknLabelFieldLegendText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 10%; background-color: #8dde99; height: 18px" align="center">
                                                                                            </td>
                                                                                            <td style="width: 90%; padding-left: 4px" align="left">
                                                                                                <asp:Label ID="ViewAssessorCalendar_legendWithScheduleCaptionLabel" runat="server"
                                                                                                    Text="Dates With Schedule" SkinID="sknLabelFieldLegendText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 10%; background-color: #fa9476; height: 18px" align="center">
                                                                                            </td>
                                                                                            <td style="width: 90%; padding-left: 4px" align="left">
                                                                                                <asp:Label ID="ViewAssessorCalendar_legendVacationDatesCaptionLabel" runat="server"
                                                                                                    Text="Vacation Dates" SkinID="sknLabelFieldLegendText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 73%" valign="top" class="popup_td_padding_2" rowspan="2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="ViewAssessorCalendar_timeSlotsLiteral" runat="server" Text="Time Slots"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" class="view_assessor_calendar_grid_body_bg">
                                                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <div style="height: 26px; overflow: auto;">
                                                                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td align="left">
                                                                                                    <asp:ImageButton ID="ViewAssessorCalendar_previousDate" runat="server" ToolTip="Go to the previous date"
                                                                                                        SkinID="sknPreviousDateImage" OnClick="ViewAssessorCalendar_previousDate_Click"
                                                                                                        Height="18px" Width="18px" />
                                                                                                </td>
                                                                                                <td align="center">
                                                                                                    <asp:Label ID="ViewAssessorCalendar_selectedDateLabel" runat="server" Text="" SkinID="sknLabelSelectedDateTextSmall"></asp:Label>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:ImageButton ID="ViewAssessorCalendar_nextDate" runat="server" ToolTip="Go to the next date"
                                                                                                        SkinID="sknNextDateImage" OnClick="ViewAssessorCalendar_nextDate_Click" Height="18px"
                                                                                                        Width="18px" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="height: 317px; overflow: auto;" id="ViewAssessorCalendar_timeSlotsGridView_resultsDiv"
                                                                                        runat="server">
                                                                                        <asp:GridView ID="ViewAssessorCalendar_timeSlotsGridView" runat="server" AllowSorting="True"
                                                                                            AutoGenerateColumns="False" Width="100%" OnRowCreated="ViewAssessorCalendar_timeSlotsGridView_RowCreated"
                                                                                            OnRowDataBound="ViewAssessorCalendar_timeSlotsGridView_RowDataBound" Height="100%">
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="ViewAssessorCalendar_timeSlotsGridView_timeSlotIDHiddenField"
                                                                                                            Value='<%# Eval("ID") %>' runat="server" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Skill">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:LinkButton ID="ViewAssessorCalendar_timeSlotsGridView_skillLinkButton" runat="server"
                                                                                                            PostBackUrl="#" Text='<%# Eval("Skill") %>' ToolTip="Click here to show/hide expanded view"></asp:LinkButton>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Initiated By">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="ViewAssessorCalendar_timeSlotsGridView_initiatedByLabel" runat="server"
                                                                                                            Text='<%# Eval("InitiatedByName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Initiated Date">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="ViewAssessorCalendar_timeSlotsGridView_initiatedDateLabel" runat="server"
                                                                                                            Text='<%# Eval("InitiatedDateString") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Time Slot">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="ViewAssessorCalendar_timeSlotsGridView_timeSlotLabel" runat="server"
                                                                                                            Text='<%# Eval("TimeSlot") %>'></asp:Label>
                                                                                                        <tr>
                                                                                                            <td class="grid_padding_right" colspan="7" style="width: 100%">
                                                                                                                <div id="ViewAssessorCalendar_timeSlotsGridView_detailsDiv" runat="server" style="display: none;"
                                                                                                                    class="table_outline_bg_view_assessor_calendar">
                                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                        <tr>
                                                                                                                            <td style="width: 13%; height: 20px" align="left" valign="top">
                                                                                                                                <asp:Label ID="ViewAssessorCalendar_timeSlotsGridView_sessionLabel" runat="server"
                                                                                                                                    SkinID="sknLabelFieldHeaderText" Text="Session"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="width: 86%" align="left" valign="top">
                                                                                                                                <div style="height: 44px; overflow: auto;">
                                                                                                                                    <asp:Label ID="ViewAssessorCalendar_timeSlotsGridView_sessionValueLabel" runat="server"
                                                                                                                                        SkinID="sknLabelFieldText" Text='<%# Eval("SessionDesc") %>'></asp:Label>
                                                                                                                                </div>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="height: 6px">
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 13%; height: 20px" align="left" valign="top">
                                                                                                                                <asp:Label ID="ViewAssessorCalendar_timeSlotsGridView_commentsLabel" runat="server"
                                                                                                                                    SkinID="sknLabelFieldHeaderText" Text="Comments"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="width: 86%" align="left" valign="top">
                                                                                                                                <div style="height: 44px; overflow: auto;">
                                                                                                                                    <asp:Label ID="ViewAssessorCalendar_timeSlotsGridView_commentsValueLabel" runat="server"
                                                                                                                                        SkinID="sknLabelFieldText" Text='<%# Eval("Comments") %>'></asp:Label>
                                                                                                                                </div>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </div>
                                                                                                                <%-- popup DIV --%>
                                                                                                                <a href="#ViewAssessorCalendar_timeSlotsGridView_detailsViewFocusMeLink" id="ViewAssessorCalendar_timeSlotsGridView_detailsViewFocusDownLink"
                                                                                                                    runat="server"></a><a href="#" id="ViewAssessorCalendar_timeSlotsGridView_detailsViewFocusMeLink"
                                                                                                                        runat="server"></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                            <EmptyDataTemplate>
                                                                                                <table style="width: 100%; height: 100%">
                                                                                                    <tr>
                                                                                                        <td style="height: 120px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="middle" align="center" style="height: 100%" class="error_message_text_normal">
                                                                                                            No time slots found to display
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </EmptyDataTemplate>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="ViewAssessorCalendar_summaryLiteral" runat="server" Text="Date Summary"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" class="view_assessor_calendar_grid_body_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 68%; height: 23px" align="left" valign="middle">
                                                                                    <asp:Label ID="ViewAssessorCalendar_summaryDateLabel" runat="server" Text="Selected Date"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 31%" align="left" valign="middle">
                                                                                    <asp:Label ID="ViewAssessorCalendar_summaryDateValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                        Text=""></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 68%; height: 23px" align="left" valign="middle">
                                                                                    <asp:Label ID="ViewAssessorCalendar_totalScheduledTimeSlotsLabel" runat="server"
                                                                                        Text="Total Scheduled Slots" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 31%" align="left" valign="middle">
                                                                                    <asp:Label ID="ViewAssessorCalendar_totalScheduledTimeSlotsValueLabel" runat="server"
                                                                                        SkinID="sknLabelFieldText" Text=""></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 68%; height: 23px" align="left" valign="middle">
                                                                                    <asp:Label ID="ViewAssessorCalendar_totalScheduledHoursLabel" runat="server" Text="Total Scheduled Hours"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 31%" align="left" valign="middle">
                                                                                    <asp:Label ID="ViewAssessorCalendar_totalScheduledHoursValueLabel" runat="server"
                                                                                        SkinID="sknLabelFieldText" Text=""></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 8px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="ViewAssessorCalendar_buttonControlsUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="left">
                                    <asp:LinkButton ID="ViewAssessorCalendar_cancelLinkButton" runat="server" Text="Cancel"
                                        SkinID="sknPopupLinkButton" OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window"> </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_2">
            </td>
        </tr>
    </table>
</asp:Content>
