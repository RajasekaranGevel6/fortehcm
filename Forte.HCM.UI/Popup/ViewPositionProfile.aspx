<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="ViewPositionProfile.aspx.cs" Inherits="Forte.HCM.UI.Popup.ViewPositionProfile" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ViewPositionProfile_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="ViewPositionProfile_headerLiteral" runat="server" Text="Position Profile Detail"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="ViewPositionProfile_topCloseImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:window.close();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" class="popup_td_padding_10" style="width: 100%">
                <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_view_position_profile_bg">
                    <tr>
                        <td class="msg_align" valign="top">
                            <asp:Label ID="ViewPositionProfile_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="ViewPositionProfile_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="popup_td_padding_10" style="width: 100%" valign="top" align="left">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td align="left" style="width: 12%" >
                                        <asp:Label ID="ViewPositionProfile_positionProfileNameHeaderLabel" runat="server"
                                            Text="Position Profile Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 76%" colspan="2">
                                        <asp:Label ID="ViewPositionProfile_positionProfileNameLabel" runat="server" Text=""
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td align="right" style="width: 12%">
                                       <asp:HyperLink ID="ViewPositionProfile_moreHyperLink" runat="server" ToolTip="Click here to view more details on the position profile" 
                                            Text="more..." Target="_blank" SkinID="sknMoreOrangeHyperLink"></asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 12%">
                                         <asp:Label ID="ViewPositionProfile_registeredByHeaderLabel" runat="server" Text="Registered By"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 37%">
                                         <asp:Label ID="ViewPositionProfile_registeredByLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 12%">
                                         <asp:Label ID="ViewPositionProfile_dateOfRegistrationHeaderLabel" runat="server"
                                            Text="Date Of Registration" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 37%">
                                         <asp:Label ID="ViewPositionProfile_dateOfRegistrationLabel" runat="server" Text=""
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 12%">
                                        <asp:Label ID="ViewPositionProfile_clientNameHeaderLabel" runat="server" Text="Client Name"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 37%">
                                       <asp:Label ID="ViewPositionProfile_clientNameLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 12%">
                                        <asp:Label ID="ViewPositionProfile_submittalDeadlineHeaderLabel" runat="server" Text="Submittal Deadline"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 37%">
                                        <asp:Label ID="ViewPositionProfile_submittalDeadlineLabel" runat="server" Text=""
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <div style="height: 400px; overflow: auto;">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" style="width: 100%">
                                                        <asp:Panel ID="ViewPositionProfile_segmentsPanel" runat="server">
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_5">
                <asp:LinkButton ID="ViewPositionProfile_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClientClick="javascript:window.close();" ToolTip="Click here to close the window" />
            </td>
        </tr>
    </table>
</asp:Content>
