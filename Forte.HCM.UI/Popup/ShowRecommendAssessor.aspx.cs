using System;
using Resources;
using System.Web;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
 

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;


namespace Forte.HCM.UI.Popup
{
    public partial class ShowRecommendAssessor : PageBase
    {
        #region Event Handlers

        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const int _pageSize = 2;

        /// <summary>
        /// A <see cref="System.String"/> constant that holds the 
        /// view state name of the sort direction
        /// </summary>
        private const string SORTDIRECTION_VIEWSTATE = "GRIDVIEWSORT_DIRECTION";

        /// <summary>
        ///  A <see cref="System.String"/> constant that holds the 
        /// view state name of the sort expression
        /// </summary>
        private const string SORTEXPRESSION_VIEWSTATE = "GRIDVIEWSORT_EXPRESSION";

        #endregion Private Constants
        #region Enum

        private enum SubscriptionRolesEnum
        {
            SR_COR_AMN = 3,
            SR_FRE = 1,
            SR_STA = 2,
            SR_COR_USR = 4
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button and focus.
                Page.Form.DefaultButton = ShowRecommendAssessor_topSearchButton.UniqueID;

                if (!IsPostBack)
                {
                    string skill=string.Empty;
                    ShowRecommendAssessor_assesserDetailsGridView.AllowSorting = false;
                    // Set default button and focus.
                    Page.Form.DefaultButton = ShowRecommendAssessor_topSearchButton.UniqueID;
                    ShowRecommendAssessor_showPerPageTextBox.Text = _pageSize.ToString();
                    //ShowRecommendAssessor_topSearchButton_Click(ShowRecommendAssessor_topSearchButton, new EventArgs());

                    if (Request.QueryString["attemptId"] != null)
                        ViewState["ATTEMPT_ID"] = Request.QueryString["attemptId"];

                    ShowRecommendAssessor_addAssessorLinkButton.Attributes.Add("onclick",
                       "return ShowSearchAssessor('" + ShowRecommendAssessor_addedUserIdHiddenField.ClientID +
                       "','" + ShowRecommendAssessor_assessorNameTextBox.ClientID + "','" +
                       ShowRecommendAssessor_refreshGridButton.ClientID + "','Y');");

                    FormAssessorSkillMatrix();

                    LoadUsersValues();
                }
                LoadAssessorGrid();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel,
                    exp.Message.ToString());
            }
        }

        protected void ShowRecommendAssessor_topSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                if(!IsValidData())
                    return;
                FormAssessorSkillMatrix();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the refresh button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ShowRecommendAssessor_refreshGridButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Get the assessor details add assign 
                int userID=0;
                string gridColumn=string.Empty;

                if (!string.IsNullOrEmpty(ShowRecommendAssessor_addedUserIdHiddenField.Value))
                    userID = Convert.ToInt32(ShowRecommendAssessor_addedUserIdHiddenField.Value);

                //Get the assessor details
                //userId = 12;
                AssessorDetail assessorDetail = new AssessorBLManager().
                    GetAssessorByUserId(userID);

                //Generate new colum
                gridColumn = string.Concat(assessorDetail.FirstName," ",assessorDetail.LastName,":",
                    assessorDetail.UserEmail,":",assessorDetail.UserID);

                // Get the existing selected list.
                DataTable dtAssessorAppend = null;

                if (ViewState["DATALIST_FULL"] == null)
                    dtAssessorAppend = new DataTable();
                else
                    dtAssessorAppend = (DataTable)ViewState["DATALIST_FULL"];

                if (Utility.IsNullOrEmpty(dtAssessorAppend))
                {
                    string skillEmptySearched = string.Empty;
                    string skillEmptySearchedText = string.Empty;
                    int n = 0;
                    string[] rowEmptyValues = new string[1];
                    string rowEmptyCloumnName = string.Empty;


                    dtAssessorAppend.Columns.Add("Skills/Areas");

                    // Retrieve the subjects against the subject ids
                    List<Subject> subjects = new List<Subject>();
                    subjects = new CommonBLManager().GetSubjects(Session["SESSION_SUBJECTIDS"].ToString());

                    foreach (Subject subject in subjects)
                    {
                        skillEmptySearched = skillEmptySearched + subject.SubjectID + ",";
                        skillEmptySearchedText = skillEmptySearchedText + subject.SubjectName + ",";
                    }

                    skillEmptySearched = skillEmptySearched.ToString().TrimEnd(',');
                    skillEmptySearchedText = skillEmptySearchedText.ToString().TrimEnd(',');
                    ShowRecommendAssessor_skillLabel.Text = skillEmptySearchedText.ToString().ToUpper();

                    
                    //Split the searched skill text
                    string[] skillEmptyMatrix = skillEmptySearched.Split(',');
                    //Split the searched skill id
                    string[] skillEmptyMatrixText = skillEmptySearchedText.Split(',');

                    foreach (string skill in skillEmptyMatrix)
                    {
                        DataRow drAddNewEmptyRow = dtAssessorAppend.NewRow();
                        rowEmptyCloumnName = string.Concat(skillEmptyMatrixText[n].ToString().Trim().ToUpper(), ":", skill.ToString().Trim());
                        rowEmptyValues[0] = rowEmptyCloumnName;
                        drAddNewEmptyRow.ItemArray = rowEmptyValues;
                        dtAssessorAppend.Rows.Add(drAddNewEmptyRow);
                        dtAssessorAppend.AcceptChanges();
                        n++;
                    }
                    dtAssessorAppend.AcceptChanges();    
                }

                //DataTable dtAssessorAppend = (DataTable)ViewState["DATALIST_FULL"];

                //Check whether column already exists already or not
                if (dtAssessorAppend!=null && dtAssessorAppend.Columns.Contains(gridColumn))
                {
                    base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel, "Assessor is already included");
                    return;
                }

                dtAssessorAppend.Columns.Add(gridColumn);
                dtAssessorAppend.AcceptChanges();

                //Append Default Row Values
                string ColVal = string.Empty;
                int skillID = 0;
                int skillMathcedCnt = 0;
                for (int i = 0; i <= dtAssessorAppend.Rows.Count - 1; i++)
                {
                    //Get skill id
                    ColVal=dtAssessorAppend.Rows[i][0].ToString();
                    if(!Utility.IsNullOrEmpty(ColVal))
                        skillID = Convert.ToInt32(ColVal.ToString().Trim().Split(':')[1]);

                    //Check selected assessor is matched with this skill
                    skillMathcedCnt = new AssessorBLManager().GetSkillMatchedWithAssessor(userID, skillID);
                    if(skillMathcedCnt>0)
                        dtAssessorAppend.Rows[i][gridColumn] = "Y:N";
                    else
                        dtAssessorAppend.Rows[i][gridColumn] = "N:N";
                }

                dtAssessorAppend.AcceptChanges();
                ViewState["DATALIST_FULL"] = dtAssessorAppend;
                FilterDataTable(string.Empty);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel,exp.Message);
            }
        }

        /// <summary>
        /// Event handles the rowcommand functionalities
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowRecommendAssessor_assesserDetailsGridView_RowCommand(object sender,
            GridViewCommandEventArgs e)
        {

            try
            {
                #region select/unselect assessor skills
                if (e.CommandName == "Change")
                {
                    string[] arqCollection = null;
                    string[] arrCellValue = null;
                    string cellValue = null;
                    string imgType = string.Empty;
                    string newCellValue = string.Empty;

                    DataTable dtAssList = (DataTable)ViewState["DATALIST_FULL"];

                    if (Convert.ToString(e.CommandArgument) != string.Empty)
                        arqCollection = e.CommandArgument.ToString().Trim().Split(',');

                    //Retrieve existing cell value
                    cellValue = dtAssList.Rows[Convert.ToInt32(arqCollection[0])]
                                    [arqCollection[1]].ToString();

                    if (!Utility.IsNullOrEmpty(cellValue))
                        arrCellValue = cellValue.ToString().Trim().Split(':');

                    if (arrCellValue[1] == "Y")
                        imgType = "N";
                    else
                        imgType = "Y";

                    newCellValue = string.Concat(arrCellValue[0], ":", imgType);
                    dtAssList.Rows[Convert.ToInt32(arqCollection[0])][arqCollection[1]] = newCellValue;
                    dtAssList.AcceptChanges();
                    ViewState["DATALIST_FULL"] = dtAssList;
                    FilterDataTable(string.Empty);
                    LoadAssessorGrid();
                }
                #endregion

                #region delete row from table
                if (e.CommandName == "Delete")
                {
                    int delRowNo = Convert.ToInt32(e.CommandArgument);
                    DataTable dtAssList = (DataTable)ViewState["DATALIST_FULL"];
                    dtAssList.Rows.RemoveAt(delRowNo);
                    dtAssList.AcceptChanges();
                    ViewState["DATALIST_FULL"] = dtAssList;
                    FilterDataTable(string.Empty);
                }
                #endregion

                #region link to profile
                if (e.CommandName == "ProfileLink")
                {
                    try
                    {
                        int assessorId = Convert.ToInt32(e.CommandArgument);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowAvailabilityForm", "<script language='javascript'>ShowAssessorProfile(" + assessorId + ");</script>", false);
                    }
                    catch (Exception exception)
                    {
                        Logger.ExceptionLog(exception);
                        base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel, exception.Message);
                    }
                }

                #endregion
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel,
                    exp.Message.ToString());
            }
        }

        /// <summary>
        /// Handler to generate and apply image and calendar option for each cell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowRecommendAssessor_assesserDetailsGridView_RowDataBound(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int i = 0;
                    string[] assessorData = null;
                    for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                    {
                        if (i == 0)
                        {
                            Label lblSkill = new Label();
                            lblSkill.Text = e.Row.Cells[i].Text;
                            e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                            e.Row.Cells[i].Controls.Add(lblSkill);
                            e.Row.Cells[i].Width = Unit.Pixel(150);
                        }
                        else
                        {
                            LinkButton lnkProfile = new LinkButton();
                            e.Row.Cells[i].Wrap = true;
                            if (!Utility.IsNullOrEmpty(e.Row.Cells[i].Text))
                            {
                                assessorData = e.Row.Cells[i].Text.ToString().Trim().Split(':');
                                lnkProfile.Text = assessorData[0].ToString();
                                lnkProfile.CommandArgument = assessorData[2].ToString();
                                lnkProfile.CommandName = "ProfileLink";
                                lnkProfile.CssClass = "profile_link_text";
                                e.Row.Cells[i].BorderColor = System.Drawing.Color.Red;
                                e.Row.Cells[i].Controls.Add(lnkProfile);
                            }

                            Label lblBreak = new Label();
                            lblBreak.Text = "<br><br>";
                            e.Row.Cells[i].Controls.Add(lblBreak);

                            HyperLink hlkEmalMail = new HyperLink();
                            hlkEmalMail.Target = "_blank";
                            hlkEmalMail.NavigateUrl = "mailto:" + assessorData[1].ToString();
                            hlkEmalMail.Text = assessorData[1];
                            e.Row.Cells[i].Controls.Add(hlkEmalMail);
                            e.Row.Cells[i].Width = Unit.Pixel(150);
                        }
                        e.Row.Cells[i].Height = Unit.Pixel(75);
                    }
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    int k = 0;
                    string columnName = string.Empty;
                    for (k = 0; k <= e.Row.Cells.Count - 1; k++)
                    {
                        if (k == 0)
                        {
                            Label lblSpace1 = new Label();
                            lblSpace1.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                            e.Row.Cells[k].Controls.Add(lblSpace1);

                            ImageButton imgDeleteButton = new ImageButton();
                            imgDeleteButton.SkinID = "sknDeletePositionProfile";
                            imgDeleteButton.ID = string.Concat("imgDelete", e.Row.RowIndex.ToString(), k.ToString());
                            imgDeleteButton.ToolTip = "Delete Skill";
                            imgDeleteButton.CommandArgument = e.Row.RowIndex.ToString();
                            imgDeleteButton.CommandName = "Delete";
                            imgDeleteButton.Visible = false;
                            e.Row.Cells[k].Controls.Add(imgDeleteButton);
                            e.Row.Cells[k].Height = Unit.Pixel(70);

                            Label lblSpace2 = new Label();
                            lblSpace2.Text = "&nbsp;&nbsp;";
                            e.Row.Cells[k].Controls.Add(lblSpace2);

                            Label lblSkill = new Label();
                            if (!Utility.IsNullOrEmpty(e.Row.Cells[k].Text))
                            {
                                string[] rowColCollection = null;
                                rowColCollection = e.Row.Cells[k].Text.ToString().Split(':');
                                lblSkill.Text = rowColCollection[0];
                            }
                            e.Row.Cells[k].Controls.Add(lblSkill);
                            e.Row.Cells[k].HorizontalAlign = HorizontalAlign.Left;
                        }
                        else
                        {
                            columnName = ShowRecommendAssessor_assesserDetailsGridView.HeaderRow.Cells[k].Text;
                            if (!Utility.IsNullOrEmpty(e.Row.Cells[k].Text))
                            {
                                string[] cellCollection = null;
                                cellCollection = e.Row.Cells[k].Text.ToString().Trim().Split(':');

                                ImageButton imgSelected = new ImageButton();
                                if (cellCollection[1] == "Y")
                                {
                                    imgSelected.ImageUrl = "~/App_Themes/DefaultTheme/Images/correct_answer.png";
                                    imgSelected.ToolTip = "Delete";
                                }
                                else
                                {
                                    imgSelected.ImageUrl = "~/App_Themes/DefaultTheme/Images/wrong_answer.png";
                                    imgSelected.ToolTip = "Add";
                                }

                                imgSelected.ID = string.Concat("imgSelected", e.Row.RowIndex.ToString(), k.ToString());
                                imgSelected.CommandName = "Change";

                                if (cellCollection[0] == "Y")
                                    e.Row.Cells[k].BackColor = System.Drawing.ColorTranslator.FromHtml("#F2FFE8");
                                else
                                    e.Row.Cells[k].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFEDE1");

                                imgSelected.CommandArgument = string.Concat(e.Row.RowIndex.ToString(), ",", columnName);
                                e.Row.Cells[k].Controls.Add(imgSelected);
                                e.Row.Cells[k].Height = Unit.Pixel(70);
                                e.Row.Cells[k].HorizontalAlign = HorizontalAlign.Center;
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel,
                    exp.Message.ToString());
            }
        }
        protected void ShowRecommendAssessor_assesserDetailsGridView_RowDeleting(object sender,
            GridViewDeleteEventArgs e)
        {

        }

        /// <summary>
        /// Event launches a availability screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowRecommendAssessor_emailAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConstructSession())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowAvailabilityForm", "<script language='javascript'>ShowAvailabilityRequest();</script>", false);
                }
                else
                {
                    base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel,
                    "Please select at least one assessor with skill selected");
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel,
                    exp.Message.ToString());
            }
        }
        /// <summary>
        /// Event to close the popup buttion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowRecommendAssessor_addAssessorButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConstructSession())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Closeform", "<script language='javascript'>self.close();</script>", false);
                }
                else
                {
                    base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel,
                    "Please select at least one assessor with skill selected");
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ShowRecommendAssessor_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event to close the popup buttion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowRecommendAssessor_addNewAssessorSetButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConstructSession())
                {
                    string candidateSessionKey = Request.QueryString["Key"].ToString();
                    string deletedAssessor = string.Empty;
                    int attemptId = 0;

                    if (!Utility.IsNullOrEmpty(Request.QueryString["attemptId"]))
                        attemptId = Convert.ToInt32(Request.QueryString["attemptId"].ToString());
                    
                    //Save the newly added/modified assessor and skill to track table
                    //Assessor list and selected skill id
                    List<AssessorDetail> assessorDetails = Session["ASSESSOR"] as List<AssessorDetail>;

                    new InterviewSchedulerBLManager().SaveInterviewSessionAssessorSkillForCandidate(assessorDetails,
                       candidateSessionKey, attemptId, base.userID);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "AssessorSetCloseForm", "<script language='javascript'>OnSaveClick();</script>", false);
                }
                else
                {
                    base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel,
                    "Please select at least one assessor with skill selected");
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ShowRecommendAssessor_topErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void ShowAssesserLookup_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                ShowRecommendAssessor_interviewedPastCheckBox.Checked = false;
                ShowRecommendAssessor_matchesAllAreasCheckBox.Checked = false;
                ShowRecommendAssessor_sortOrderDropDownList.SelectedIndex = 0;
                ShowRecommendAssessor_sortByDropDownList.SelectedIndex = 0;
                ShowRecommendAssessor_showAllRadioButton.Checked = true;
                ShowRecommendAssessor_showTopRadioButton.Checked = false;
                ShowRecommendAssessor_topMatchesTextBox.Text = "0";
                ShowRecommendAssessor_showPerPageTextBox.Text = _pageSize.ToString();
                //ShowRecommendAssessor_topSearchButton_Click(ShowRecommendAssessor_topSearchButton, new EventArgs());
                FormAssessorSkillMatrix();
                // Set default focus.
                Page.Form.DefaultFocus = ShowRecommendAssessor_topSearchButton.UniqueID;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ShowRecommendAssessor_topErrorMessageLabel, exp.Message);
            }
        }
        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method that forms skill matrix according to the search conditions
        /// </summary>
        private void FormAssessorSkillMatrix()
        {
            string skillSearched = string.Empty;
            string skillSearchedText = string.Empty;
            string assessorsMatched = string.Empty;
            string assessorTimeSlotMatched = string.Empty;
            string assessorInterviewMatched = string.Empty;
            string flag = string.Empty;
            DataTable dtFullData = null;
            flag = Request.QueryString["flag"].ToString().ToUpper();

            int positionProfileID = 0;
            if (Session["SESSION_POSITION_PROFILE_ID"] != null)
                positionProfileID = Convert.ToInt32(Session["SESSION_POSITION_PROFILE_ID"].ToString());

            if (flag == "CANDIDATEKEY")
            {
                ShowRecommendAssessor_addNewAssessorSetButton.Visible = true;
                ShowRecommendAssessor_addAssessorButton.Visible = false;
            }
            else
            {
                ShowRecommendAssessor_addNewAssessorSetButton.Visible = false;
                ShowRecommendAssessor_addAssessorButton.Visible = true;
            }
            if (flag.ToString() == "SEARCH")
            {
                ShowRecommendAssessor_ShowSearchCriteriaTable.Style["display"] = "block";
                ShowRecommendAssessor_searchAssessor.Text = "Search and Add Assessors";
                Master.SetPageCaption("Search and Add Assessors");
                // Retrieve the subjects against the subject ids
                List<Subject> subjects = new List<Subject>();
                subjects = new CommonBLManager().GetSubjects(Session["SESSION_SUBJECTIDS"].ToString());

                foreach (Subject subject in subjects)
                {
                    skillSearched = skillSearched + subject.SubjectID + ",";
                    skillSearchedText = skillSearchedText + subject.SubjectName + ",";
                }

                skillSearched = skillSearched.ToString().TrimEnd(',');
                skillSearchedText = skillSearchedText.ToString().TrimEnd(',');
                ShowRecommendAssessor_skillLabel.Text = skillSearchedText.ToString().ToUpper();

                dtFullData = new AssessorBLManager().GetAssessorListBySkill(base.tenantID,
                    skillSearched, positionProfileID);

                //If no matches found
                if (dtFullData.Rows.Count == 0)
                {
                    AssignEmptyGrid();
                    return;
                }
            }
            else
            {
                ShowRecommendAssessor_ShowSearchCriteriaTable.Style["display"] = "block";
                ShowRecommendAssessor_searchAssessor.Text = "View/Change Assessors";
                Master.SetPageCaption("View/Change Assessors");
                string selectedSubjectIds = string.Empty;
                string key = string.Empty; /* Interview Session Key or Candidate Session Key */
                key = Request.QueryString["Key"].ToString();
                
                DataSet assessorAndSkillDataset = new InterviewSessionBLManager().
                            GetAssessorAndSkillBySessionOrCandidateKey(key,flag.ToString().ToUpper(),positionProfileID);
                dtFullData = assessorAndSkillDataset.Tables[0];

                //If no matches found
                if (dtFullData.Rows.Count == 0)
                {
                    AssignEmptyGrid();
                    return;
                }

                DataTable dtSkillSet = assessorAndSkillDataset.Tables[1];

                if (Session["SESSION_SUBJECTIDS"] != null)
                {
                    selectedSubjectIds = Session["SESSION_SUBJECTIDS"].ToString();
                }
                else
                {
                    if (dtSkillSet != null && dtSkillSet.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtSkillSet.Rows)
                            selectedSubjectIds = selectedSubjectIds + row["SKILL_ID"].ToString() + ",";
                    }
                }
 
                List<Subject> subjects = new List<Subject>();
                subjects = new CommonBLManager().GetSubjects(selectedSubjectIds.ToString().TrimEnd(','));

                foreach (Subject subject in subjects)
                {
                    skillSearched = skillSearched + subject.SubjectID + ",";
                    skillSearchedText = skillSearchedText + subject.SubjectName + ",";
                }
                skillSearched = skillSearched.ToString().TrimEnd(',');
                skillSearchedText = skillSearchedText.ToString().TrimEnd(',');
            }
            
            
            //Split the searched skill text
            string[] skillMatrix = skillSearched.Split(',');
            //Split the searched skill id
            string[] skillMatrixText = skillSearchedText.Split(',');


            //Get assessor list who interviewed past for this particular client
            if (ShowRecommendAssessor_interviewedPastCheckBox.Checked)
            {
                DataView dvFilterByInterviewedPast = new DataView(dtFullData);
                dvFilterByInterviewedPast.RowFilter = "INTERVIEWED_PAST > 0";

                DataTable dtFilterByInterviewedPast = dvFilterByInterviewedPast.ToTable();
                dtFilterByInterviewedPast.AcceptChanges();

                if (dtFilterByInterviewedPast == null || dtFilterByInterviewedPast.Rows.Count == 0)
                {
                    AssignEmptyGrid();
                    return;
                }
                else
                {
                    foreach (DataRow row in dtFilterByInterviewedPast.Rows)
                    {
                        assessorInterviewMatched = assessorInterviewMatched + row["USER_ID"].ToString().Trim() + ",";
                    }
                }
             }

            if (ShowRecommendAssessor_matchesAllAreasCheckBox.Checked)
            {
               
                //Get the searched skill count
                int skillSearchCnt = skillMatrix.Length;

                //To find who matched all
                DataView dvFilterMatchedAllAreas = new DataView(dtFullData);
                dvFilterMatchedAllAreas.RowFilter = "MATCHED_COUNT=" + skillSearchCnt;

                DataTable dtMaxSkillMatchedAssers = dvFilterMatchedAllAreas.ToTable().
                    DefaultView.ToTable(false, "USER_ID");

                dtMaxSkillMatchedAssers.AcceptChanges();
                if (dtMaxSkillMatchedAssers == null || dtMaxSkillMatchedAssers.Rows.Count == 0)
                {
                    AssignEmptyGrid();
                    return;
                }
                else
                {
                    foreach (DataRow row in dtMaxSkillMatchedAssers.Rows)
                        assessorsMatched = assessorsMatched + row["USER_ID"].ToString().Trim() + ",";
                }
            }
            //Get distinctData from retrieved data table
            DataTable dtAssesorSet = dtFullData.DefaultView.ToTable(true, "USER_ID", "EMAIL", "ASSESSOR_NAME", "MATCHED_COUNT", "INTERVIEWED_PAST");
            dtAssesorSet.AcceptChanges();

            if (ShowRecommendAssessor_matchesAllAreasCheckBox.Checked)
            {
                //Filter by matches all reas
                assessorsMatched = assessorsMatched.ToString().Trim().TrimEnd(',');
                DataView dvMatches = new DataView(dtAssesorSet);
                dvMatches.RowFilter = "USER_ID IN (" + assessorsMatched + ")";
                dtAssesorSet = null;
                dtAssesorSet = dvMatches.ToTable();
            }

            if (ShowRecommendAssessor_interviewedPastCheckBox.Checked)
            {
                //Filter by who interviewd past
                assessorInterviewMatched = assessorInterviewMatched.ToString().Trim().TrimEnd(',');
                DataView dvInterviewedMatches = new DataView(dtAssesorSet);
                dvInterviewedMatches.RowFilter = "USER_ID IN (" + assessorInterviewMatched + ")";
                dtAssesorSet = null;
                dtAssesorSet = dvInterviewedMatches.ToTable();
            }

            dtAssesorSet.AcceptChanges();
            if (dtAssesorSet.Rows.Count == 0)
            {
                AssignEmptyGrid();
                return;
            }

            //Sort by who can access most areas
            string sortOrder = ShowRecommendAssessor_sortOrderDropDownList.SelectedValue.ToString();
            string soryBy = ShowRecommendAssessor_sortByDropDownList.SelectedValue.ToString();

            //Order by who can access most areas
            if (soryBy.Trim().ToUpper() == "CAN_ASSESS_MOST_AREAS")
            {
                DataView dvSortByMostAreas = new DataView(dtAssesorSet);
                dvSortByMostAreas.Sort = "MATCHED_COUNT " + sortOrder;
                dtAssesorSet = null;
                dtAssesorSet = dvSortByMostAreas.ToTable();
            }

            //Order by who interview for this client past
            if (soryBy.Trim().ToUpper() == "INTERVIEWED_PAST")
            {
                DataView dvSortByMostAreas = new DataView(dtAssesorSet);
                dvSortByMostAreas.Sort = "INTERVIEWED_PAST " + sortOrder;
                dtAssesorSet = null;
                dtAssesorSet = dvSortByMostAreas.ToTable();
            }

            dtAssesorSet.AcceptChanges();
            //Construct new data table with filtered data
            DataTable dtGenerateColumn = new DataTable();
            dtGenerateColumn.Columns.Add("Skills/Areas");

            //Get top matches enabled
            int topColumnCnt = 0;
            if (ShowRecommendAssessor_showTopRadioButton.Checked)
            {
                topColumnCnt = Convert.ToInt32(ShowRecommendAssessor_topMatchesTextBox.Text);
                if (topColumnCnt > dtAssesorSet.Rows.Count)
                    topColumnCnt = dtAssesorSet.Rows.Count;
            }
            else
                topColumnCnt = dtAssesorSet.Rows.Count;

            for (int k = 0; k <= topColumnCnt - 1; k++)
                dtGenerateColumn.Columns.Add(string.Concat(dtAssesorSet.Rows[k]["ASSESSOR_NAME"].
                    ToString().Trim(), ":", dtAssesorSet.Rows[k]["EMAIL"].ToString().Trim(),
                    ":", dtAssesorSet.Rows[k]["USER_ID"].ToString().Trim()));

            dtGenerateColumn.AcceptChanges();
            int m = 0;
            int columnCount = 0;
            columnCount = dtGenerateColumn.Columns.Count;
            ViewState["columnCount"] = columnCount;
            string[] rowValues = new string[columnCount];
            string rowCloumnName = string.Empty;
            foreach (string skill in skillMatrix)
            {
                string[] splitColumnName = null;
                string expression = string.Empty;
                string skillMatched = string.Empty;
                string defultCellSelected = string.Empty;
                defultCellSelected = "N";

                DataRow drAddNewRow = dtGenerateColumn.NewRow();
                rowCloumnName = string.Concat(skillMatrixText[m].ToString().Trim().ToUpper(), ":", skill.ToString().Trim());
                rowValues[0] = rowCloumnName;
                for (int i = 1; i <= columnCount - 1; i++)
                {
                    splitColumnName = dtGenerateColumn.Columns[i].ColumnName.Split(':');

                    // Construct filter expression.
                    expression = "EMAIL='" + splitColumnName[1].ToString().Trim() +
                        "' AND SKILL_MATCHED='" + skill.ToString().Trim() + "'";

                    // Get filtered rows.
                    DataRow[] filteredRows = dtFullData.Select(expression);
                    if (filteredRows == null || filteredRows.Length == 0)
                        skillMatched = "N";
                    else
                        skillMatched = "Y";

                    if (flag.ToString() != "SEARCH")
                        defultCellSelected = skillMatched;

                    rowValues[i] = string.Concat(skillMatched, ":", defultCellSelected);
                    if (i == dtGenerateColumn.Columns.Count - 1)
                    {
                        drAddNewRow.ItemArray = rowValues;
                        dtGenerateColumn.Rows.Add(drAddNewRow);
                        dtGenerateColumn.AcceptChanges();
                    }
                }
                m++;
            }

            ViewState["DATALIST"] = null;
            ViewState["DATALIST"] = dtGenerateColumn;

            //For pageing 
            ViewState["DATALIST_FULL"] = dtGenerateColumn;
            ShowRecommendAssessor_pageNumberHiddenField.Value = "1";
            FilterDataTable(string.Empty);
            //LoadAssessorGrid();
        }

        /// <summary>
        /// Method that clears the existing and data and grid
        /// </summary>
        private void AssignEmptyGrid()
        {
            ViewState["DATALIST"] = null;
            LoadAssessorGrid();
        }

        /// <summary>
        /// Method to construct the session table;
        /// </summary>
        private  bool ConstructSession()
        {
            // Clear messages.
            ClearMessages();
            int i = 0;
            int rowCnt = 0;
            int colCnt = 0;
            int j = 0;
            string deletedAssessors=string.Empty;
            bool flagSave = false;
            
            SessionDetail sessionDetail = new SessionDetail();
            sessionDetail.SessionName = "Interview Session";
            sessionDetail.SessionKey = string.Empty;

            sessionDetail.Assessors = new List<AssessorDetail>();
            AssessorDetail assessorDetail = null;

            List<AssessorDetail> assessorDetails = null;
            Session["ASSESSOR"] = null;
            Session["DELETED_ASSESSOR"] = null;
            assessorDetails = new List<AssessorDetail>();
            
            //Data Table
            if (ViewState["DATALIST_FULL"] != null)
            {
                DataTable dtAssessor = (DataTable)ViewState["DATALIST_FULL"];
                rowCnt = dtAssessor.Rows.Count;
                colCnt = dtAssessor.Columns.Count;

                for (i = 1; i <= dtAssessor.Columns.Count - 1; i++)
                {
                    string rowValues = string.Empty;
                    string[] arrayRowValues = new string[4];
                    string ColumnName = string.Empty;
                    string[] arrColumn = null;
                    string skillColumn = string.Empty;
                    ColumnName = dtAssessor.Columns[i].ColumnName.ToString();
                    arrColumn = ColumnName.ToString().Trim().Split(':');

                    string skillIds = string.Empty;
                    assessorDetail = new AssessorDetail();
                    assessorDetail.Skills = new List<SkillDetail>();
                    for (j = 0; j <= rowCnt - 1; j++)
                    {
                        rowValues = dtAssessor.Rows[j][dtAssessor.Columns[i].ColumnName].ToString();
                        if (rowValues.ToString().Trim().Split(':')[1] == "Y")
                        {
                            flagSave = true;
                            skillColumn = dtAssessor.Rows[j]["Skills/Areas"].ToString();
                            skillIds = skillIds + skillColumn.ToString().Trim().Split(':')[1] + ",";
                            assessorDetail.Skills.Add(new
                                SkillDetail(Convert.ToInt32(skillColumn.ToString().Trim().Split(':')[1].ToString()),
                                skillColumn.ToString().Trim().Split(':')[0].ToString()));
                        }
                    }
                    //For session Construction
                    if (!Utility.IsNullOrEmpty(skillIds))
                    {
                        assessorDetail.UserID = Convert.ToInt32(arrColumn[2].ToString());
                        assessorDetail.FirstName = arrColumn[0].ToString();
                        assessorDetail.UserEmail = arrColumn[1].ToString();
                        assessorDetail.AlternateEmailID = arrColumn[1].ToString();
                        sessionDetail.Assessors.Add(assessorDetail);

                        //To assign to parent grid and to db values
                        assessorDetail.Skill = skillIds.ToString().Trim().TrimEnd(',');
                        assessorDetails.Add(assessorDetail);
                    }
                    else
                    {
                        //Capture the deleted ids
                        deletedAssessors = deletedAssessors + Convert.ToInt32(arrColumn[2].ToString()) + ",";
                    }
                    //End session construction
                }

                //Launch My Availability
                if (!Utility.IsNullOrEmpty(deletedAssessors))
                    Session["DELETED_ASSESSOR"] = deletedAssessors.ToString().TrimEnd(',');

                Session["ASSESSOR"] = assessorDetails;
                Session["SESSION_ASSESSORS"] = sessionDetail;
            }
            return flagSave;
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            
            if (ShowRecommendAssessor_showTopRadioButton.Checked && 
                ShowRecommendAssessor_topMatchesTextBox.Text.Trim()=="0")
            {
                isValidData = false;
                base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel,
                    "Please select the show top matches");
            }

            if (ShowRecommendAssessor_showAllRadioButton.Checked &&
                ShowRecommendAssessor_topMatchesTextBox.Text.Trim() != "0")
                ShowRecommendAssessor_topMatchesTextBox.Text = "0";

            if (ShowRecommendAssessor_showPerPageTextBox.Text.Trim() == "0")
            {
                isValidData = false;
                base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel,
                    "Assessors per page should be greater than zero");
            }
            return isValidData;
        }

       
        /// <summary>
        /// Method assigns the searched list to grid
        /// </summary>
        private void LoadAssessorGrid()
        {
            ShowRecommendAssessor_topErrorMessageLabel.Text = string.Empty;
            if (ViewState["DATALIST"] == null || ((DataTable)ViewState["DATALIST"]).Rows.Count == 0)
            {
                
                ShowRecommendAssessor_assesserDetailsGridView.AutoGenerateColumns = true;
                ShowRecommendAssessor_assesserDetailsGridView.DataSource = null;
                ShowRecommendAssessor_assesserDetailsGridView.DataBind();
                ShowRecommendAssessor_ShowAssesserResultsTR.Visible = false;
                ShowRecommendAssessor_ShowAssesserResultsHeaderTR.Visible = false;
                base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel, "No data found to display");
                return;
            }
            ShowRecommendAssessor_ShowAssesserResultsTR.Visible = true;
            ShowRecommendAssessor_ShowAssesserResultsHeaderTR.Visible = true;

            ShowRecommendAssessor_assesserDetailsGridView.AutoGenerateColumns = true;
            ShowRecommendAssessor_assesserDetailsGridView.DataSource = ViewState["DATALIST"];
            ShowRecommendAssessor_assesserDetailsGridView.DataBind();
        }

        /// <summary>
        /// Method that clears the messages.
        /// </summary>
        private void ClearMessages()
        {
            ShowRecommendAssessor_topErrorMessageLabel.Text = string.Empty;
            ShowRecommendAssessor_topSuccessMessageLabel.Text = string.Empty;
        }


        #region onNavigatePage

        /// <summary>
        /// Called when [navigate page].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void OnNavigatePage(object sender, EventArgs e)
        {
            try
            {
                ImageButton navigateButton = (ImageButton)sender;
                FilterDataTable(navigateButton.CommandName);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel,
                    exception.Message);
            }
        }

        #endregion

        #region Pageing Method
        /// <summary>
        /// Method handles/filters according to the page navigation and reassigns the data
        /// </summary>
        /// <param name="navigationType">navigationType</param>
        private void FilterDataTable(string navigationType)
        {
            int i = 0;
            int j = 0;
            int k = 0;

            int pageNum = 0;
            int pageSize = 0;
            int pageStartIndex=0;
            int pageEndIndex=0;

            pageSize = Convert.ToInt32(ShowRecommendAssessor_showPerPageTextBox.Text);
            pageNum = Convert.ToInt32(ShowRecommendAssessor_pageNumberHiddenField.Value);

            if (navigationType == "Previous")
                pageNum = pageNum - 1;
            if (navigationType == "Next")
                pageNum = pageNum + 1;
                
            pageStartIndex = ((pageNum - 1) * pageSize) + 1;
            pageEndIndex = pageNum * pageSize;
            ShowRecommendAssessor_pageNumberHiddenField.Value = pageNum.ToString();

            //Filter the data table
            DataTable dtFilterForPageing = ((DataTable)ViewState["DATALIST_FULL"]);

            //Check page index exceeds the total records
            if (pageEndIndex >= dtFilterForPageing.Columns.Count)
            {
                pageEndIndex = dtFilterForPageing.Columns.Count - 1;
            }

            DataTable dtNewFiltered = new DataTable();
            dtNewFiltered.Columns.Add("Skills/Areas");

            for (i = pageStartIndex; i <= pageEndIndex; i++)
            {
                dtNewFiltered.Columns.Add(dtFilterForPageing.Columns[i].ColumnName);
            }
            
            int columCnt = 0;
            columCnt = dtNewFiltered.Columns.Count;
            string[] arrayFilteredRow = new string[columCnt];

            //Create row apply data
            for (k = 0; k <= dtFilterForPageing.Rows.Count - 1; k++)
            {
                DataRow drFilter = dtNewFiltered.NewRow();
                for (j = 0; j <= columCnt - 1; j++)
                    arrayFilteredRow[j] = dtFilterForPageing.Rows[k][dtNewFiltered.
                        Columns[j].ColumnName].ToString();
                
                drFilter.ItemArray = arrayFilteredRow;
                dtNewFiltered.Rows.Add(drFilter);
                dtNewFiltered.AcceptChanges();
            }
            ShowRecommendAssessor_previousImageButton.Enabled = true;
            ShowRecommendAssessor_nextImageButton.Enabled = true;

            if (pageStartIndex == 1)
                ShowRecommendAssessor_previousImageButton.Enabled=false;
            if (pageEndIndex >= dtFilterForPageing.Columns.Count-1)
                ShowRecommendAssessor_nextImageButton.Enabled = false;


            ViewState["DATALIST"] = dtNewFiltered;
            LoadAssessorGrid();
        }
        #endregion

        #endregion Private Methods

        #region Protected Overridden Methods



        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
         

         private void LoadUsersValues()
         {
             ViewState[SORTEXPRESSION_VIEWSTATE] = "EMAIL";
             ViewState[SORTDIRECTION_VIEWSTATE] = SortDirection.Ascending;
             BindCorporateChildUsers();
         } 
        /// <summary>
        /// A Method that checks and binds the logged in corporate account users
        /// </summary>
        private void BindCorporateChildUsers()
        {
            List<UserRegistrationInfo> userRegistrationInfo = null;

            try
            {
                userRegistrationInfo = new UserRegistrationBLManager().GetSelectedCorporateAccountUsers(base.tenantID,
                    ViewState[SORTEXPRESSION_VIEWSTATE].ToString(), (SortType)ViewState[SORTDIRECTION_VIEWSTATE]);

                if ((userRegistrationInfo == null || userRegistrationInfo.Count == 0) && base.isSiteAdmin == false)
                {
                    return;
                }
             string strAdminRole = Enum.GetName(typeof(SubscriptionRolesEnum), 3);
                 var CheckIsAdminUser = System.Linq.Enumerable.Where(userRegistrationInfo, p => p.SubscriptionRole == strAdminRole);
                 if ((CheckIsAdminUser == null || CheckIsAdminUser.ToList().Count == 0) && base.isSiteAdmin == false)
                 {
                   
                     return;
                 } 
                var AdminUser = CheckIsAdminUser.ToList();

             if (AdminUser.Count > 0)
               { 
                   UserRegistrationInfo userRegistrationInfoNew = new UserRegistrationInfo();
                   userRegistrationInfoNew.UserID = Convert.ToInt32(AdminUser[0].TenantID);
                   userRegistrationInfoNew.Company = AdminUser[0].Company;
                   userRegistrationInfoNew.Phone = AdminUser[0].Phone;
                   userRegistrationInfoNew.Title = AdminUser[0].Title;
                   ShowRecommendAssessor_CorporateUserSignUp.InsertNewDataSource = userRegistrationInfoNew;

                   string strNormalUserRole = Enum.GetName(typeof(SubscriptionRolesEnum), 4);
                   var NormalUsers = System.Linq.Enumerable.Where(userRegistrationInfo, p => p.SubscriptionRole == strNormalUserRole);

                   ShowRecommendAssessor_addNewUserLinkButton.Visible = true;
               }
               else
               {
                   ShowRecommendAssessor_addNewUserLinkButton.Visible = false;
               } 
            }
            finally
            {
                if (userRegistrationInfo != null) userRegistrationInfo = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        public void ShowSignUpPopUp()
        {
            ShowRecommendAssessor_addNewUserModalPopupExtender.Show();
        }

        protected void ShowRecommendAssessor_addNewUserLinkButton_Click(object sender, EventArgs e)
        {
            ShowRecommendAssessor_addNewUserModalPopupExtender.Show();
        }

        public void ShowSuccessMessage(string Message)
        {
            base.ShowMessage(ShowRecommendAssessor_topSuccessMessageLabel, Message);
        }
        public void RebindGridView()
        {
            //BindCorporateChildUsers();
        }
        public void ShowErrMessage(string Message)
        {
            base.ShowMessage(ShowRecommendAssessor_topErrorMessageLabel, Message);
        }

}
}