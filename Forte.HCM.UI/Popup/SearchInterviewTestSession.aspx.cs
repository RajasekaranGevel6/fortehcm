
#region Header

// Copyright (C) Forte. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchInterviewTestSession.aspx.cs
// This page allows the user to search the existing testSessions 
// and to view all its informations.
#endregion Header

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// This page allows the user to search the existing testSessions 
    /// and to view all its informations.
    /// </summary>
    public partial class SearchInterviewTestSession : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "224px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "374px";

        #endregion Private Constants

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set Browser Tittle
                Master.SetPageCaption("Search Interview Session");
                // Set default button
                Page.Form.DefaultButton = SearchInterviewTestSession_topSearchButton.UniqueID;

                // Create events for paging control
                SearchInterviewTestSession_bottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchInterviewTestSession_pagingNavigator_PageNumberClick);

                // Check whether the grid is maximized or not and assign the height accordingly.
                if (!Utility.IsNullOrEmpty(ScheduleCandidate_isMaximizedHiddenField.Value) &&
                        ScheduleCandidate_isMaximizedHiddenField.Value == "Y")
                {
                    SearchInterviewTestSession_searchCriteriasDiv.Style["display"] = "none";
                    SearchInterviewTestSession_searchResultsUpSpan.Style["display"] = "block";
                    SearchInterviewTestSession_searchResultsDownSpan.Style["display"] = "none";
                    SearchInterviewTestSession_testDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchInterviewTestSession_searchCriteriasDiv.Style["display"] = "block";
                    SearchInterviewTestSession_searchResultsUpSpan.Style["display"] = "none";
                    SearchInterviewTestSession_searchResultsDownSpan.Style["display"] = "block";
                    SearchInterviewTestSession_testDiv.Style["height"] = RESTORED_HEIGHT;
                }

                // Call expand or restore method
                SearchInterviewTestSession_searchTestResultsTR.Attributes.Add("onclick",
                        "ExpandOrRestore('" +
                        SearchInterviewTestSession_testDiv.ClientID + "','" +
                        SearchInterviewTestSession_searchCriteriasDiv.ClientID + "','" +
                        SearchInterviewTestSession_searchResultsUpSpan.ClientID + "','" +
                        SearchInterviewTestSession_searchResultsDownSpan.ClientID + "','" +
                        ScheduleCandidate_isMaximizedHiddenField.ClientID + "','" +
                        RESTORED_HEIGHT + "','" +
                        EXPANDED_HEIGHT + "')");

                if (!IsPostBack)
                {
                    // Set default focus
                    Page.Form.DefaultFocus = SearchInterviewTestSession_testSessionIdTextBox.UniqueID;
                    SearchInterviewTestSession_testSessionIdTextBox.Focus();

                    SearchInterviewTestSession_testDiv.Visible = false;

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "TestSessionID";

                    SetAuthorDetails();
                }
                else
                {
                    SearchInterviewTestSession_testDiv.Visible = true;
                }

                // Assign handler to scheduler selection popup.
                SearchInterviewTestSession_schedulerNameImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                    + SearchInterviewTestSession_schedulerHiddenField.ClientID + "','"
                    + SearchInterviewTestSession_schedulerIDHiddenField.ClientID + "','"
                    + SearchInterviewTestSession_schedulerNameTextBox.ClientID + "','TC')");

                // Assign handler to session author selection popup.
                SearchInterviewTestSession_sessionAuthorNameImageButton.Attributes.Add("onclick",
                    "return LoadAdminName('" +SearchInterviewTestSession_sessionAuthorNameHiddenField.ClientID + "','" +
                    SearchInterviewTestSession_sessionAuthorIDHiddenField.ClientID + "','" +
                    SearchInterviewTestSession_sessionAuthorNameTextBox.ClientID + "','TS')");

                // Assign handler to position profile selection popup.
                SearchInterviewTestSession_positionProfileImageButton.Attributes.Add("onclick",
                    "return LoadPositionProfileName('" + SearchInterviewTestSession_positionProfileTextBox.ClientID + "','"
                    + SearchInterviewTestSession_positionProfileIDHiddenField.ClientID + "')");

                // Clear message.
                SearchInterviewTestSession_topErrorMessageLabel.Text =
                    SearchInterviewTestSession_topSuccessMessageLabel.Text = "";

                // Assign position profile.
                if (!IsPostBack)
                {
                    AssignPositionProfile();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTestSession_topErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// This button handler will get trigger on clicking the search button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchInterviewTestSession_SearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear error message label
                SearchInterviewTestSession_topErrorMessageLabel.Text = string.Empty;
                SearchInterviewTestSession_positionProfileTextBox.Text =
                    Request[SearchInterviewTestSession_positionProfileTextBox.UniqueID].ToString();

                // Reset the paging control.
                SearchInterviewTestSession_bottomPagingNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1
                SearchInterviewTestSessionDetail(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchInterviewTestSession_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the PageNumberClick event of the SearchInterviewTestSession_pagingNavigator control.
        /// This binds the data into the grid for the pagenumber.
        /// </summary>
        /// <param name="sender">Source of the event.</param>
        /// <param name="e"></param>
        protected void SearchInterviewTestSession_pagingNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                SearchInterviewTestSessionDetail(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchInterviewTestSession_topErrorMessageLabel,
                    exception.Message);
                SearchInterviewTestSession_topErrorMessageLabel.Text = exception.Message;
            }
        }

        /// <summary>
        /// Clicking on Reset button will trigger this method. 
        /// This clears all the informations and reset to empty.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchInterviewTestSession_bottomReset_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }
        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method that assigns the position profile and loads the default 
        /// search results in the grid.
        /// </summary>
        private void AssignPositionProfile()
        {
            // Load the position profile name in the search criteria,
            // if position profile is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                return;

            // Get position profile ID.
            int positionProfileID = 0;
            if (int.TryParse(Request.QueryString["positionprofileid"].Trim(),
                out positionProfileID) == false)
            {
                return;
            }
            PositionProfileDetail positionProfile =
                new PositionProfileBLManager().GetPositionProfileInformation(positionProfileID);

            // Assign the position profile ID and name to the corresponding fields.
            SearchInterviewTestSession_positionProfileTextBox.Text = positionProfile.PositionProfileName;
            SearchInterviewTestSession_positionProfileIDHiddenField.Value = positionProfileID.ToString();

            // Apply the default search.
            // Clear error message label
            SearchInterviewTestSession_topErrorMessageLabel.Text = string.Empty;

            if (Request[SearchInterviewTestSession_positionProfileTextBox.UniqueID] != null)
            {
                SearchInterviewTestSession_positionProfileTextBox.Text =
                    Request[SearchInterviewTestSession_positionProfileTextBox.UniqueID].ToString();
            }

            // Reset the paging control.
            SearchInterviewTestSession_bottomPagingNavigator.Reset();

            // By default search button click retrieves data for
            // page number 1
            SearchInterviewTestSessionDetail(1);

            // Update the search results update panel.
           // SearchCategory_searchResultsUpdatePanel.Update();
            //SearchInterviewTestSession_updatePanel.Update();
        }

        /// <summary>
        /// This method creates an instance for search criteria and 
        /// passes to db to get the matched test sessions
        /// </summary>
        /// <param name="pageNumber"></param>
        private void SearchInterviewTestSessionDetail(int pageNumber)
        {
            TestSessionSearchCriteria tsSearchCriteria = new TestSessionSearchCriteria();
            tsSearchCriteria.TestKey = SearchInterviewTestSession_testIdTextBox.Text.Trim();
            tsSearchCriteria.TestName = SearchInterviewTestSession_testNameTextBox.Text.Trim();
            tsSearchCriteria.SessionKey = SearchInterviewTestSession_testSessionIdTextBox.Text.Trim();

            // Since the session author textbox is readonly, get the text value using Request.
            if (Request[SearchInterviewTestSession_sessionAuthorNameTextBox.UniqueID] != null)
            {
                SearchInterviewTestSession_sessionAuthorNameTextBox.Text =
                  Request[SearchInterviewTestSession_sessionAuthorNameTextBox.UniqueID].Trim();
            }
            tsSearchCriteria.TestSessionCreator = SearchInterviewTestSession_sessionAuthorNameTextBox.Text.Trim();

            tsSearchCriteria.PositionProfileID =
                (SearchInterviewTestSession_positionProfileIDHiddenField.Value == null || SearchInterviewTestSession_positionProfileIDHiddenField.Value.Trim().Length == 0) ? 0 :
                Convert.ToInt32(SearchInterviewTestSession_positionProfileIDHiddenField.Value.Trim());

            // Since the SchedulerName textbox is readonly, get the text value using Request.
            if (Request[SearchInterviewTestSession_schedulerNameTextBox.UniqueID] != null)
            {
                SearchInterviewTestSession_schedulerNameTextBox.Text =
                    Request[SearchInterviewTestSession_schedulerNameTextBox.UniqueID].Trim();
            }
            tsSearchCriteria.SchedulerName = SearchInterviewTestSession_schedulerNameTextBox.Text;
            tsSearchCriteria.SchedulerNameID =int.Parse(SearchInterviewTestSession_schedulerIDHiddenField.Value);
            tsSearchCriteria.TestSessionCreatorID = int.Parse(SearchInterviewTestSession_sessionAuthorIDHiddenField.Value);

            int totalRecords = 0;

            List<InterviewSessionDetail> testSession =
                new InterviewSchedulerBLManager().SearchInterviewTestSessionDetails
                (tsSearchCriteria, pageNumber, base.GridPageSize,
                out totalRecords, ViewState["SORT_FIELD"].ToString(), ((SortType)ViewState["SORT_ORDER"]));

            // Bind the retrieved test sessions in grid.
            SearchInterviewTestSession_testGridView.DataSource = testSession;
            SearchInterviewTestSession_testGridView.DataBind();
            if (testSession == null)
            {
                SearchInterviewTestSession_testDiv.Visible = false;
                base.ShowMessage(SearchInterviewTestSession_topErrorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                SearchInterviewTestSession_testDiv.Visible = true;
            }

            SearchInterviewTestSession_bottomPagingNavigator.PageSize = base.GridPageSize;
            SearchInterviewTestSession_bottomPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method used to set the author name and first name for the first time
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user id 
        /// </param>
        private void SetAuthorDetails()
        {
            //Get the user details from DB and assign it
            UserDetail userDetail = new QuestionBLManager().GetAuthorIDAndName(base.userID);

            if (userDetail == null)
                return;

            SearchInterviewTestSession_schedulerIDHiddenField.Value ="0";
            SearchInterviewTestSession_sessionAuthorIDHiddenField.Value = base.userID.ToString();
            SearchInterviewTestSession_sessionAuthorNameTextBox.Text = userDetail.FirstName;

            //if (base.isAdmin)
            //{
            //    SearchInterviewTestSession_sessionAuthorNameImageButton.Visible = true;
            //    SearchInterviewTestSession_sessionAuthorNameTextBox.Text = string.Empty;
            //    SearchInterviewTestSession_schedulerIDHiddenField.Value = "0";
            //    SearchInterviewTestSession_sessionAuthorIDHiddenField.Value = "0";
            //}
        }


        #endregion Private Methods

        #region Sort Columns Related Code

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchInterviewTestSession_testGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                SearchInterviewTestSession_bottomPagingNavigator.Reset();
                SearchInterviewTestSessionDetail(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTestSession_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchInterviewTestSession_testGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchInterviewTestSession_testGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTestSession_topErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchInterviewTestSession_testGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchInterviewTestSession_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Sort Columns Related Code

        #region Override Methods

        protected override bool IsValidData()
        {
            return true;
        }
        protected override void LoadValues()
        {
            //Do Nothing
        }
        #endregion
    }
}
