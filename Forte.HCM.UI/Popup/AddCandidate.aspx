﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCandidate.aspx.cs" MasterPageFile="~/MasterPages/PopupMaster.Master"
    Inherits="Forte.HCM.UI.Popup.AddCandidate" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="AddCandidate_contentID" ContentPlaceHolderID="PopupMaster_contentPlaceHolder"
    runat="server">
    <script type="text/javascript" language="javascript">

        
        // Show or hide other text based on work authorization status.
        function ShowOtherTextBox(dropdown, div, textBoxID) {
            if (document.getElementById(dropdown).value == 'WAS_OTHER') {
                document.getElementById(div).style["display"] = "block";
                document.getElementById(textBoxID).focus();
                document.getElementById(textBoxID).value = '';
            }
            else {
                document.getElementById(div).style["display"] = "none";
                document.getElementById(textBoxID).value = '';
            }

            return false;
        }


        //This function helps to assign the user name to user email id
        function AssignUserName() {

            var emailIDObj = document.getElementById('<%= AddCandidate_emailTextBox.ClientID %>');
            var userNameObj = document.getElementById('<%= AddCandidate_createUserNamePanel_userNameTextBox.ClientID %>');

            if (emailIDObj == null) return;

            if (userNameObj == null) return;

            userNameObj.value = emailIDObj.value; 

        }

        // This function helps to close the window
        function CloseMe() {
            self.close();
            return false;
        }
        function OnCandidateCreation(ctrlId) {
            var nameCtrl = '<%= Request.QueryString["namectrl"] %>';
            var emailCtrl = '<%= Request.QueryString["emailctrl"] %>';
            var idCtrl = '<%= Request.QueryString["idctrl"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Set the user name field value. Replace the 'button name' with the 
            // 'user name hidden field name' and retrieve the value.
            if (nameCtrl != null && nameCtrl != '') {
                window.opener.document.getElementById(nameCtrl).value
                    = document.getElementById(ctrlId.replace("AddCandidate_bottomSaveButton", "AddCandidate_firstNameTextBox")).value;
            }

            // Set the email hidden field value. Replace the 'button name' with the 
            // 'eamil hidden field name' and retrieve the value.
            if (emailCtrl != null && emailCtrl != '') {
                window.opener.document.getElementById(emailCtrl).value
                    = document.getElementById(ctrlId.replace("AddCandidate_bottomSaveButton", "AddCandidate_emailTextBox")).value;
            }
            // Set the user ID hidden field value. Replace the 'link button name' with the 
            // 'user ID hidden field name' and retrieve the value.
            if (idCtrl != null && idCtrl != '') {
                window.opener.document.getElementById(idCtrl).value
                    = document.getElementById(ctrlId.replace("AddCandidate_bottomSaveButton", "AddCandidate_userIDHiddenfield")).value;
            }
            self.close();
        }

        //To validate the resume control
        function AddCandidate_resumeClientUploadComplete(sender, args) {
            var fileName = args.get_fileName();
            var fileExtn = (/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName) : undefined;
            if (fileExtn != "doc" && fileExtn != "DOC" && fileExtn != "pdf" && fileExtn != "PDF" && fileExtn != "docx" && fileExtn != "DOCX") {
                var errMsg = "Please select valid file format.(.doc,.docx,.pdf and rtf)";
                $get("<%=AddCandidate_errorMessageLabel.ClientID%>").innerHTML = errMsg;
                args.set_cancel(true);
                var who1 = document.getElementsByName('<%=AddCandidate_resumeAsyncFileUpload.UniqueID %>')[0];
                who1.value = "";
                var who3 = who1.cloneNode(false);
                who3.onchange = who1.onchange;
                who1.parentNode.replaceChild(who3, who1);

                return false;
                alert(errMsg);
            }
        }

        //To validate the photo control
        function AddCandidate_photoClientUploadComplete(sender, args) {
            var handlerPage = '<%= Page.ResolveClientUrl("~/Common/CandidateImageHandler.ashx")%>';
            var queryString = '?source=CAND_CREA_POPUP&randomno=' + getRandomNumber();
            var src = handlerPage + queryString;
            var clientId = '<%=AddCandidate_candidateImage.ClientID %>';
            document.getElementById(clientId).setAttribute("src", src);
            var clearButton = document.getElementById("<%=AddCandidate_selectPhotoClearLinkButton.ClientID%>");
        }
        function getRandomNumber() {
            var randomnumber = Math.random(10000);
            return randomnumber;
        }
        function AddCandidate_resumeUploadError(sender, args) {
            return false;
        }

        function AddCandidate_photoUploadError(sender, args) {
            return false;
        }
    </script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="width: 50%" class="popup_header_text_grey" align="left">
                                        <asp:Literal ID="AddCandidate_headerLiteral" Text="New Candidate" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <asp:ImageButton ID="AddCandidate_closeImageButton" runat="server" SkinID="sknCloseImageButton"
                                            OnClientClick="javascript:CloseMe();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="msg_align">
                            <asp:UpdatePanel ID="AddCandidate_messageUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="AddCandidate_successMessageLabel" runat="server" SkinID="sknSuccessMessage">
                                    </asp:Label>
                                    <asp:Label ID="AddCandidate_errorMessageLabel" runat="server" SkinID="sknErrorMessage">
                                    </asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="popup_td_padding_10">
                            <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">
                                <tr>
                                    <td class="header_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 50%" class="header_text_bold">
                                                    <asp:Literal ID="AddCandidate_candidateDetailsLiteral" runat="server" Text="Candidate Details"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tab_body_bg">
                                        <asp:UpdatePanel ID="AddCandidate_bodyUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="2" cellspacing="3" border="0" style="text-align: left">
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 8%">
                                                            <asp:Label ID="AddCandidate_firstNameHeaderLabel" runat="server" Text="First Name"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            <span class="mandatory">*</span>
                                                        </td>
                                                        <td style="width: 1%">
                                                        </td>
                                                        <td class="style9">
                                                            <asp:TextBox ID="AddCandidate_firstNameTextBox" runat="server" MaxLength="50" TabIndex="1"
                                                                Width="95%"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 1%">
                                                        </td>
                                                        <td class="style8">
                                                            <asp:Label ID="AddCandidate_addressLabel" runat="server" Text="Address" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 1%">
                                                        </td>
                                                        <td rowspan="3" valign="top" style="width: 18%">
                                                            <asp:TextBox ID="AddCandidate_addressTextBox" runat="server" TextMode="MultiLine"
                                                                Columns="25" Height="70" MaxLength="100" SkinID="sknMultiLineTextBox" onkeyup="CommentsCount(100,this)"
                                                                onchange="CommentsCount(100,this)" TabIndex="9"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 1%">
                                                        </td>
                                                        <td style="width: 25%" rowspan="9" class="candidate_photo" align="center" valign="middle">
                                                            <asp:UpdatePanel ID="AddCandidate_previewPhotoUpdatePanel" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:Image ID="AddCandidate_candidateImage" runat="server" ImageUrl="~/Common/CandidateImageHandler.ashx?source=CAND_CREA_POPUP"
                                                                        AlternateText="Photo not available" />
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="AddCandidate_photoAsyncFileUpload" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="AddCandidate_middleNameLabel" runat="server" Text="Middle Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="style9">
                                                            <asp:TextBox ID="AddCandidate_middleNameTextBox" runat="server" MaxLength="50" TabIndex="2"
                                                                Width="95%"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="style8">
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td style="width: 1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="AddCandidate_lastNameLabel" runat="server" Text="Last Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="style9">
                                                            <asp:TextBox ID="AddCandidate_lastNameTextBox" runat="server" MaxLength="50" TabIndex="3"
                                                                Width="95%"></asp:TextBox>
                                                        </td>
                                                        <td colspan="4">
                                                        </td>
                                                        <td style="width: 1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="AddCandidate_emailLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="style9">
                                                            <asp:TextBox ID="AddCandidate_emailTextBox" runat="server" MaxLength="100" TabIndex="4"
                                                                Width="95%" onblur="javascript:return AssignUserName();"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="style8">
                                                            <asp:Label ID="AddCandidate_homephoneLabel" runat="server" Text="Home Phone" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="AddCandidate_homephoneTextBox" runat="server" MaxLength="20" Width="95%"
                                                                TabIndex="10"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="AddCandidate_genderLabel" runat="server" Text="Gender" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="style9">
                                                            <asp:DropDownList ID="AddCandidate_genderDropDownList" runat="server" Width="120px"
                                                                TabIndex="5">
                                                                <asp:ListItem Text="--Select--" Value="0">
                                                                </asp:ListItem>
                                                                <asp:ListItem Text="Male" Value="1">
                                                                </asp:ListItem>
                                                                <asp:ListItem Text="Female" Value="2">
                                                                </asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="style8">
                                                            <asp:Label ID="AddCandidate_cellPhoneLabel" runat="server" Text="Cell Phone" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="AddCandidate_cellPhoneTextBox" runat="server" MaxLength="20" Width="95%"
                                                                TabIndex="11"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="AddCandidate_dateOfBirthLabel" runat="server" Text="Date Of Birth"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="style9">
                                                            <asp:TextBox ID="AddCandidate_dateOfBirthTextBox" runat="server" MaxLength="100"
                                                                Width="94px" ReadOnly="true" TabIndex="6"></asp:TextBox>
                                                            <asp:ImageButton ID="AddCandidate_dateOfBirthLinkButton" runat="server" SkinID="sknCalendarImageButton"
                                                                ImageAlign="Middle" />
                                                            <ajaxToolKit:CalendarExtender ID="AddCandidate_dateOfBirthCalenderExtender" runat="server"
                                                                TargetControlID="AddCandidate_dateOfBirthTextBox" CssClass="MyCalendar" Format="MM/dd/yyyy"
                                                                PopupPosition="BottomLeft" PopupButtonID="AddCandidate_dateOfBirthLinkButton" />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="style8">
                                                            <asp:Label ID="AddCandidate_typeLabel" runat="server" Text="Type" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:RadioButton ID="AddCandidate_candidateRadioButton" runat="server" GroupName="type"
                                                                            TabIndex="11" Checked="true" />
                                                                        <asp:Label ID="AddCandidate_candidateLabel" runat="server" Text="Candidate" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:RadioButton ID="AddCandidate_employeeRadioButton" runat="server" GroupName="type" />
                                                                        <asp:Label ID="AddCandidate_employeeLabel" runat="server" Text="Employee" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="AddCandidate_workAuthorizationStatusLabel" runat="server" Text="Work Authorization"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="style9">
                                                            <div style="float: left; width: 60%">
                                                                <asp:DropDownList ID="AddCandidate_workAuthorizationStatusDropDownList" runat="server"
                                                                    Width="120px" TabIndex="7">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div id="AddCandidate_workAuthorizationStatus_otherDIV" runat="server" style="float: left;
                                                                display: none;">
                                                                <span class="mandatory">*</span>&nbsp;<asp:TextBox ID="AddCandidate_workAuthorizationStatus_otherTextBox"
                                                                    runat="server" MaxLength="50" Width="60px"></asp:TextBox>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="style8">
                                                            <asp:Label ID="AddCandidate_limitedAccessLabel" runat="server" Text="Limited Access"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox runat="server" ID="AddCandidate_limitedAccessCheckBox" Checked="true"
                                                                TabIndex="12" />
                                                        </td>
                                                        <td style="width: 1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="AddCandidate_LocationLabel" runat="server" Text="Location" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="style9">
                                                            <asp:TextBox ID="AddCandidate_LocationTextBox" ReadOnly="true" runat="server"
                                                                Width="86%"></asp:TextBox>
                                                            <asp:ImageButton ID="AddCandidate_locationImageButton" SkinID="sknbtnSearchicon"
                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select city, state, country" />
                                                            <asp:HiddenField ID="AddCandidate_cityIDHiddenField" runat="server" />
                                                            <asp:HiddenField ID="AddCandidate_stateIDHiddenField" runat="server" />
                                                            <asp:HiddenField ID="AddCandidate_countryIDHiddenField" runat="server" />
                                                        </td>
                                                        <td style="width: 1%">
                                                        </td>
                                                        <td class="style8">
                                                            <asp:Label ID="AddCandidate_IsactiveLabel" runat="server" Text="Is Active" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            &nbsp;</td>
                                                        <td>
                                                            <%--  <asp:HiddenField ID="AddCandidate_createUserNamePanel_firstNameHiddenField" runat="server" />
                                                            <asp:HiddenField ID="AddCandidate_createUserNamePanel_lastNameHiddenField" runat="server" />
                                                            <asp:HiddenField ID="AddCandidate_createUserNamePanel_candidateIDHiddenField" runat="server" />--%>
                                                            <asp:CheckBox ID="AddCandidate_IsactiveCheckbox" runat="server" 
                                                                Checked="true" />
                                                        </td>
                                                        <td style="width: 1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="AddCandidate_createUserNamePanel_userNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="User Name"></asp:Label>
                                                            <span class="mandatory">*</span>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td class="style9">
                                                            <asp:TextBox ID="AddCandidate_createUserNamePanel_userNameTextBox" runat="server"
                                                                MaxLength="50" TabIndex="8" Width="95%"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td class="style8">
                                                            <asp:Label ID="AddCandidate_createUserNamePanel_passwordLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="Password"></asp:Label>
                                                           <span class="mandatory">*</span>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="AddCandidate_createUserNamePanel_passwordTextBox" runat="server"
                                                                MaxLength="50" TabIndex="13" TextMode="Password" Width="95%"></asp:TextBox>
                                                            <asp:HiddenField ID="AddCandidate_userIDHiddenfield" runat="server" />
                                                        </td>
                                                        <td style="width: 1%">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <div style="float: left; padding-right: 5px;">
                                                                <asp:LinkButton ID="AddCandidate_createUserNamePanel_checkUserEmailIdAvailableButton"
                                                                    CssClass="link_button_hcm" runat="server" Text="Check" OnClick="AddCandidate_createUserNamePanel_checkUserEmailIdAvailableButton_Click">
                                                                </asp:LinkButton>
                                                            </div>
                                                            <div style="float: left;">
                                                                <div id="AddCandidate_createUserNamePanel_userEmailAvailableStatusDiv" runat="server"
                                                                    style="display: none;">
                                                                    <asp:Label ID="AddCandidate_createUserNamePanel_inValidEmailAvailableStatus" runat="server"
                                                                        EnableViewState="false" ForeColor="Red"></asp:Label>
                                                                    <asp:Label ID="AddCandidate_createUserNamePanel_validEmailAvailableStatus" runat="server"
                                                                        EnableViewState="false" ForeColor="Green"></asp:Label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="style8">
                                                            <asp:Label ID="AddCandidate_createUserNamePanel_retypePasswordLabel" Text="Retype Password"
                                                                runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="AddCandidate_createUserNamePanel_retypePasswordTextBox" runat="server"
                                                                TabIndex="14" Width="95%" TextMode="Password"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="style9">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_10">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 50%" class="header_text_bold">
                                                    <asp:Literal ID="AddCandidate_uploadResumeAndPhotoLiteral" runat="server" Text="Upload Resume & Photo"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td class="td_padding_top_bottom">
                                                    <table border="0" cellpadding="0" cellspacing="2" runat="server" width="100%" id="AddCandidate_candidateTable"
                                                        style="text-align: left">
                                                        <tr>
                                                            <td style="width: 8%">
                                                                <asp:Label ID="AddCandidate_fileNameLabel" runat="server" Text="Select Resume" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 2%">
                                                            </td>
                                                            <td style="width: 25%" align="right">
                                                                <ajaxToolKit:AsyncFileUpload ID="AddCandidate_resumeAsyncFileUpload" runat="server"
                                                                    Width="300" Height="24px" ThrobberID="Throbber" FailedValidation="False" PersistFile="false"
                                                                    SkinID="sknCanTextBox" OnUploadedComplete="AddCandidate_resumeUploadedComplete"
                                                                    ClientIDMode="AutoID" OnClientUploadError="AddCandidate_resumeUploadError"
                                                                    OnClientUploadComplete="AddCandidate_resumeClientUploadComplete" />
                                                            </td>
                                                            <td align="left" style="width: 1%">
                                                                <asp:ImageButton ID="AddCandidate_helpImageButton" SkinID="sknHelpImageButton" runat="server"
                                                                    OnClientClick="javascript:return false;" ToolTip="Click here to select the doc/docx/pdf/rtf file that contains the resume"
                                                                    ImageAlign="Middle" />
                                                            </td>
                                                            <td style="width: 25%">
                                                            </td>
                                                            <td style="width: 8%">
                                                                <asp:Label ID="AddCandidate_selectPhotoLabel" runat="server" Text="Select Photo"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 2%">
                                                            </td>
                                                            <td style="width: 25%" align="right">
                                                                <asp:UpdatePanel ID="AddCandidate_photoUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <ajaxToolKit:AsyncFileUpload ID="AddCandidate_photoAsyncFileUpload" runat="server"
                                                                            Width="300" Height="24px" ThrobberID="Throbber" FailedValidation="False" PersistFile="false"
                                                                            SkinID="sknCanTextBox" OnUploadedComplete="AddCandidate_photoUploadedComplete"
                                                                            ClientIDMode="AutoID" OnClientUploadError="AddCandidate_photoUploadError"
                                                                            OnClientUploadComplete="AddCandidate_photoClientUploadComplete" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                            <td align="left" style="width: 1%">
                                                                <asp:ImageButton ID="AddCandidate_selectPhotoHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" OnClientClick="javascript:return false;" ToolTip="Click here to select the gif/png/jpg file that contains the photo"
                                                                    ImageAlign="Middle" />
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                &nbsp;<asp:LinkButton ID="AddCandidate_selectPhotoClearLinkButton" runat="server"
                                                                    Visible="false" SkinID="sknActionLinkButton" Text="Clear" ToolTip="Click here to clear the photo"
                                                                    OnClick="AddCandidate_selectPhotoClearLinkButton_Click"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <%--<asp:RegularExpressionValidator ID="AddCandidate_regularExpressionValidataor" runat="server"
                                                                    ControlToValidate="AddCandidate_fileUpload" ErrorMessage="Only doc/docx/pdf/rtf files are allowed"
                                                                    ValidationExpression="^([a-zA-Z].*|[0-9].*)\.(((p|P)(d|D)(f|F))|((r|R)(t|T)(f|F))|((d|D)(o|O)(c|C))|((d|D)(o|O)(c|C)(x|X)))$"> </asp:RegularExpressionValidator>--%>
                                                            </td>
                                                            <td style="width: 2%">
                                                            </td>
                                                            <td style="width: 2%" colspan="3">
                                                                <%--<asp:RegularExpressionValidator ID="AddCandidate_selectPhotoRegularExpressionValidataor"
                                                                    runat="server" ControlToValidate="AddCandidate_selectPhotoFileUpload" ErrorMessage="Only gif/png/jpg/jpeg  files are allowed"
                                                                    ValidationExpression="^([a-zA-Z].*|[0-9].*)\.(((g|F)(i|I)(f|F))|((p|P)(n|N)(g|G))|((j|J)(p|P)(g|G))|((j|J)(p|P)(e|E)(g|G)))$"> </asp:RegularExpressionValidator>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
         <!-- Show popup extender -->
        <tr>
            <td>
               <asp:UpdatePanel ID="AddCandidate_candidatePopupUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="AddCandidate_schedulePanel" runat="server" CssClass="popupcontrol_add_candidate"
                            Style="display: none">
                            <div style="display: none;">
                                <asp:Button ID="AddCandidate_hiddenButton" runat="server" Text="Hidden" />
                            </div>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" class="popup_header_text" style="width: 50%" valign="middle">
                                                    <asp:Literal ID="AddCandidate_questionResultLiteral" runat="server" Text="Review & Confirm Candidate Creation"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table align="right" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="AddCandidate_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table border="0" cellpadding="0" cellspacing="0" class="popupcontrol_question_inner_bg"
                                                        width="100%">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <div id="AddCandidate_enteredDiv" runat="server">
                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="panel_bg">
                                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                                <tr id="Tr1" runat="server">
                                                                                                    <td class="header_bg">
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                            <tr>
                                                                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                                                                    <asp:Literal ID="AddCandidate_enteredLiteral" runat="server" Text="You entered"></asp:Literal>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="grid_body_bg">
                                                                                                        <table width="100%" cellpadding="2" cellspacing="3">
                                                                                                            <tr>
                                                                                                                <td style="width: 10%">
                                                                                                                    <asp:Label ID="AddCandidate_candidateFirstNameLabel" runat="server" 
                                                                                                                        SkinID="sknLabelFieldHeaderText" Text="First Name"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 25%">
                                                                                                                    <asp:Label ID="AddCandidate_candidateFirstNameLabelValue" SkinID="sknLabelFieldText"
                                                                                                                        runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 10%">
                                                                                                                    <asp:Label ID="AddCandidate_candidateLastNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                        Text="Last Name"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 25%">
                                                                                                                    <asp:Label ID="AddCandidate_candidateLastNameLabelValue" SkinID="sknLabelFieldText"
                                                                                                                        runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 10%">
                                                                                                                    <asp:Label ID="AddCandidate_candidateEmailLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                        Text="Email"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 35%">
                                                                                                                    <asp:Label ID="AddCandidate_candidateEmailLabelValue" SkinID="sknLabelFieldText"
                                                                                                                        runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="AddCandidate_candidateGridViewUpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <div id="AddCandidate_candidateGridViewDIV" runat="server">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">

                                                                                            <tr>
                                                                                                <td class="panel_bg">
                                                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                                                        <tr id="AddCandidate_candidateGridViewHeaderTR" runat="server">
                                                                                                            <td class="header_bg">
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td style="width: 50%" align="left" class="header_text_bold">
                                                                                                                            <asp:Literal ID="AddCandidate_resultsLiteral" runat="server" Text="Following list of duplicate candidate(s) are identified"></asp:Literal>&nbsp;<asp:Label
                                                                                                                                ID="AddCandidate_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="You can anyway create the candidate or cancel this"></asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="grid_body_bg">
                                                                                                                <div id="AddCandidate_candidateDetailDIV" runat="server" style="height: 220px;
                                                                                                                    overflow: auto;">
                                                                                                                    <asp:GridView ID="AddCandidate_candidateDetailGridView" runat="server" AllowSorting="true"
                                                                                                                        AutoGenerateColumns="false" OnRowDataBound="AddCandidate_candidateDetailGridView_RowDataBound">
                                                                                                                        <Columns>
                                                                                                                            <asp:TemplateField HeaderText="First Name" HeaderStyle-Width="14%" ItemStyle-Width="14%">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="AddCandidate_firstNameGridViewLabel" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                                <ItemStyle Wrap="true" />
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField HeaderText="Last Name" ItemStyle-Width="14%" HeaderStyle-HorizontalAlign="Center"
                                                                                                                                HeaderStyle-Width="14%">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="AddCandidate_lastNameGridViewLabel" runat="server" Text='<%# Eval("LastName") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                                    </asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                                <ItemStyle Wrap="true" />
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField HeaderText="Email" ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                                                                                                HeaderStyle-Width="15%">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="AddCandidate_emailGridViewLabel" runat="server" Text='<%# Eval("EMailID") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                                    </asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                                <ItemStyle Wrap="true" />
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField HeaderText="Reason" ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                                                                                                HeaderStyle-Width="15%">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="AddCandidate_reasonGridViewLabel" runat="server" Text='<%# Eval("Reason")==null ? Eval("Reason") : Eval("Reason").ToString().Replace(".", "<br />") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                                    </asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                                <ItemStyle Wrap="true" />
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField HeaderText="Uploaded By" ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                                                                                                HeaderStyle-Width="15%">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="AddCandidate_uploadedByGridViewLabel" runat="server" Text='<%# Eval("UploadedByName") %>'></asp:Label>
                                                                                                                                    <asp:HiddenField ID="AddCandidate_candidateIDHiddenField" runat="server" Value='<%# Eval("CandidateID") %>' />
                                                                                                                                    <asp:HiddenField ID="AddCandidate_candidateResumeIDHiddenField" runat="server"
                                                                                                                                        Value='<%# Eval("CandidateResumeID") %>' />
                                                                                                                                </ItemTemplate>
                                                                                                                                <ItemStyle Wrap="true" />
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField HeaderText="Uploaded Date" ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                                                                                                HeaderStyle-Width="15%">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="AddCandidate_uploadedDateGridViewLabel" runat="server" Text='<%# Eval("UploadedDate") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                                    </asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                                <ItemStyle Wrap="true" />
                                                                                                                            </asp:TemplateField>
                                                                                                                        </Columns>
                                                                                                                    </asp:GridView>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                          
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_5">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left" style="width: 20px; padding-left: 5px">
                                                    <asp:Button ID="AddCandidate_newCandidateButton" runat="server" OnClick="AddCandidate_newCandidateButton_Click"
                                                        SkinID="sknButtonId" Text="Create Anyway" />
                                                </td>
                                                <td style="padding-left: 5px">
                                                    <asp:LinkButton ID="AddCandidate_bottomCloseLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                                        Text="Cancel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="AddCandidate_scheduleModalPopupExtender" runat="server"
                            BackgroundCssClass="modalBackground" PopupControlID="AddCandidate_schedulePanel"
                            TargetControlID="AddCandidate_hiddenButton">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="AddCandidate_bottomSaveButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>

        <!-- End show popup extender-->
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="left">
                                <tr>
                                    <td>
                                        <asp:Button ID="AddCandidate_bottomSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="AddCandidate_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="AddCandidate_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknPopupLinkButton" OnClick="AddCandidate_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="pop_divider_line">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="AddCandidate_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknPopupLinkButton" OnClientClick="javascript:CloseMe();"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .style5
        {
            width: 28%;
        }
        .style8
        {
            width: 16%;
        }
        .style9
        {
            width: 27%;
        }
    </style>
</asp:Content>

