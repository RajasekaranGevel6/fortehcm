﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/PopupMaster.Master"
    AutoEventWireup="true" CodeBehind="PreviewForm.aspx.cs" Inherits="Forte.HCM.UI.Popup.PreviewForm" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../Segments/VerticalBackgroundRequirementControl.ascx" TagName="VerticalBackgroundRequirementControl"
    TagPrefix="uc2" %>
<%@ Register Src="../Segments/TechnicalSkillRequirementControl.ascx" TagName="TechnicalSkillRequirementControl"
    TagPrefix="uc3" %>
<%@ Register Src="../Segments/RoleRequirementControl.ascx" TagName="RoleRequirementControl"
    TagPrefix="uc4" %>
<%@ Register Src="../Segments/EducationRequirementControl.ascx" TagName="EducationRequirementControl"
    TagPrefix="uc5" %>
<%@ Register Src="../Segments/ClientPositionDetailsControl.ascx" TagName="ClientPositionDetailsControl"
    TagPrefix="uc6" %>
<asp:Content ID="PreivewForm_content" ContentPlaceHolderID="PopupMaster_contentPlaceHolder"
    runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="PreviewForm_headerLiteral" runat="server" Text="Preview Form"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="PreviewForm_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <asp:UpdatePanel ID="PreviewForm_tabUpdatePabel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                            <tr>
                                <td class="popup_td_padding_10">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="msg_align">
                                                <asp:Label ID="PreviewForm_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="PreviewForm_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_20" colspan="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_20" colspan="2">
                                                <table cellpadding="0" cellspacing="0" style="width: 100%; overflow: hidden" class="tab_body_bg">
                                                    <tr>
                                                        <td align="left">
                                                            <div id="PreviewForm_tabContainerDIV" runat="server" style="width: 700px">
                                                                <input type="hidden" runat="server" id="IsDeleteClicked" />
                                                                <ajaxToolKit:TabContainer ID="PreviewForm_tabContainer" runat="server" ActiveTabIndex="0"
                                                                    AutoPostBack="true" SkinID="sknPositionProfileTabContainer" OnActiveTabChanged="PreviewForm_tabContainer_ActiveTabChanged">
                                                                    <ajaxToolKit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                                                                    </ajaxToolKit:TabPanel>
                                                                </ajaxToolKit:TabContainer>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%">
                                                            <uc2:VerticalBackgroundRequirementControl ID="PreviewForm_verticalBackgroundRequirementControl"
                                                                runat="server" Visible="false" />
                                                            <uc3:TechnicalSkillRequirementControl ID="PreviewForm_technicalSkillRequirementControl"
                                                                runat="server" Visible="false" />
                                                            <uc4:RoleRequirementControl ID="PreviewForm_roleRequirementControl" runat="server"
                                                                Visible="false" />
                                                            <uc5:EducationRequirementControl ID="PreviewForm_educationRequirementControl" runat="server"
                                                                Visible="false" />
                                                            <uc6:ClientPositionDetailsControl ID="PreviewForm_clientPositionDetailsControl" runat="server"
                                                                Visible="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_20">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" align="left">
                <asp:LinkButton ID="PreviewForm_previewFormCancelLinkButton" runat="server" Text="Cancel"
                    SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();"></asp:LinkButton>
            </td>
        </tr>
    </table>
</asp:Content>
