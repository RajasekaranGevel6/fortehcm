﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewNonAvailability.aspx.cs
// File that represents the user interface layout and functionalities for
// the ViewNonAvailability page. This will helps the assessors to view and
// set the non availability dates in their calendar.

#endregion Header

#region Directives                                                             

using System;
using System.Linq;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// File that represents the user interface layout and functionalities for
    /// the ViewNonAvailability page. This will helps the assessors to view and
    /// set the non availability dates in their calendar. This class inherits 
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ViewNonAvailability : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Set Vacation Dates");

                if (!IsPostBack)
                {
                    // Call and assign query string values to hidden fields.
                    if (RetrieveQueryStringValues() == false)
                    {
                        // Hide the save button.
                        ViewNonAvailability_saveButton.Visible = false;

                        return;
                    }

                    // Clear session.
                    Session["VIEW_NON_AVAILABILITY_SELECTED_DATES"] = null;

                    // Set current date as selected date.
                    ViewNonAvailability_nonAvailabilityCalendar.SelectedDates.Clear();

                    // Load non availability dates.
                    LoadNonAvailabilityDates();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewNonAvailability_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the calender control is 
        /// prerendered.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void ViewNonAvailability_nonAvailabilityCalendar_PreRender(object sender, EventArgs e)
        {
            try
            {
                // Clear selected dates.
                ViewNonAvailability_nonAvailabilityCalendar.SelectedDates.Clear();

                if (Utility.IsNullOrEmpty(Session["VIEW_NON_AVAILABILITY_SELECTED_DATES"]))
                    return;

                // Highlight the selected dates.
                foreach (DateTime selectedDate in Session["VIEW_NON_AVAILABILITY_SELECTED_DATES"] as List<DateTime>)
                {
                    ViewNonAvailability_nonAvailabilityCalendar.SelectedDates.Add(selectedDate);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewNonAvailability_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when a date in the calender control
        /// is selected.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        protected void ViewNonAvailability_nonAvailabilityCalendar_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                // Instantiate dates list.
                List<DateTime> selectedDates = new List<DateTime>();

                // Assign selected dates from session.
                if (Session["VIEW_NON_AVAILABILITY_SELECTED_DATES"] != null)
                    selectedDates =  Session["VIEW_NON_AVAILABILITY_SELECTED_DATES"] as List<DateTime>;

                if (selectedDates.Contains(ViewNonAvailability_nonAvailabilityCalendar.SelectedDate))
                    selectedDates.Remove(ViewNonAvailability_nonAvailabilityCalendar.SelectedDate);
                else
                    selectedDates.Add(ViewNonAvailability_nonAvailabilityCalendar.SelectedDate);

                // Keep selected dates in session.
                Session["VIEW_NON_AVAILABILITY_SELECTED_DATES"] = selectedDates;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewNonAvailability_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void ViewNonAvailability_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                string nonAvailabilityDates = string.Empty;

                // Check if non availability dates present in session.
                if (Session["VIEW_NON_AVAILABILITY_SELECTED_DATES"] != null)
                {
                    // Loop through the list of non availability dates and 
                    // convert it to comma separated.
                    foreach (DateTime nonAvailabilityDate in 
                        Session["VIEW_NON_AVAILABILITY_SELECTED_DATES"] as List<DateTime>)
                    {
                        if (nonAvailabilityDate.ToShortDateString() != "12/29/9999")
                            nonAvailabilityDates = nonAvailabilityDates + nonAvailabilityDate.ToShortDateString() + ",";
                    }
                    nonAvailabilityDates = nonAvailabilityDates.TrimEnd(',');
                }

                // Save the non availability dates to database.
                new AssessorBLManager().UpdateAssessorNonAvailabilityDates
                    (Convert.ToInt32(ViewNonAvailability_assessorIDHiddenField.Value), 
                    nonAvailabilityDates);

                // Close popup window.
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "Closeform", "<script language='javascript'>CloseViewNonAvailabilityWindow();</script>", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewNonAvailability_topErrorMessageLabel, exp.Message);
            }
        }
        
        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that retrieves the query string values and assign it to 
        /// hidden fields.
        /// </summary>
        private bool RetrieveQueryStringValues()
        {
            // Check if assessor ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["assessorid"]))
            {
                base.ShowMessage(ViewNonAvailability_topErrorMessageLabel, "Assessor ID is not passed");
                return false;
            }

            // Check if assessor ID is valid.
            int assessorID = 0;
            int.TryParse(Request.QueryString["assessorid"], out assessorID);

            if (assessorID == 0)
            {
                base.ShowMessage(ViewNonAvailability_topErrorMessageLabel, "Invalid assessor ID");
                return false;
            }

            // Assign assessor ID.
            ViewNonAvailability_assessorIDHiddenField.Value = assessorID.ToString();

            return true;
        }

        /// <summary>
        /// Method that loads the time slots for the selected date.
        /// </summary>
        private void LoadNonAvailabilityDates()
        {
            // Load assessor detail.
            string dates = new AssessorBLManager().GetAssessorNonAvailabilityDates
                (Convert.ToInt32(ViewNonAvailability_assessorIDHiddenField.Value));

            // Create a non availability date list object.
            List<DateTime> nonAvailabilitydates = null;

            if (!Utility.IsNullOrEmpty(dates))
            {
                nonAvailabilitydates = dates.Split
                    (',').Select(n => Convert.ToDateTime(n)).ToList();
            }

            // Keep the non availability dates in session.
            Session["VIEW_NON_AVAILABILITY_SELECTED_DATES"] = nonAvailabilitydates;

            // Set visible date.
            if (Utility.IsNullOrEmpty(Request.QueryString["from"]))
                return;

            if (Request.QueryString["from"].Trim().ToUpper() == "MYAVB")
            {
                if (Session["MY_AVAILABILITY_VISIBLE_DATE"] != null)
                {
                    ViewNonAvailability_nonAvailabilityCalendar.VisibleDate =
                        (DateTime)Session["MY_AVAILABILITY_VISIBLE_DATE"];
                }
            }
        }

        #endregion Private Methods
    }
}