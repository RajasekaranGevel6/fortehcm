﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdvancedSearchCustomer.aspx.cs"  MasterPageFile="~/MasterPages/PopupMaster.Master"
Inherits="Forte.HCM.UI.Popup.AdvancedSearchCustomer" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>

<asp:Content ID="SearchUser_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">

    <script type="text/javascript" language="javascript">

        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl) {

            var ctrlDisplayField = '<%= Request.QueryString["ctrlId"] %>';
            var ctrlHiddenField = '<%= Request.QueryString["ctrlhidId"] %>';
            var ctrlFirstNameField = '<%= Request.QueryString["ctrlFirstNameid"] %>';
            var ctrlEmailField = '<%= Request.QueryString["ctrlEmail"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Set the display field value. Replace the 'link button name' with the 
            // 'user name hidden field name' and retrieve the value.
            if (ctrlDisplayField != null && ctrlDisplayField != '') {
                window.opener.document.getElementById(ctrlDisplayField).value
                    = document.getElementById(ctrl.id.replace("SearchUser_selectLinkButton", "SearchUser_userNameHiddenfield")).value;
            }

            // Set the ID hidden field value. Replace the 'link button name' with the 
            // 'user ID hidden field name' and retrieve the value.
            if (ctrlHiddenField != null && ctrlHiddenField != '') {
                window.opener.document.getElementById(ctrlHiddenField).value
                    = document.getElementById(ctrl.id.replace("SearchUser_selectLinkButton", "SearchUser_userIDHiddenfield")).value;
            }

            if (ctrlFirstNameField != null && ctrlFirstNameField != '') {
                var value = window.opener.document.getElementById(ctrlFirstNameField);
                if (value.type == "text") {
                    window.opener.document.getElementById(ctrlFirstNameField).value =
                    document.getElementById(ctrl.id.replace("SearchUser_selectLinkButton", "SearchUser_userFirstNameHiddenfield")).value;
                }
                else {
                    window.opener.document.getElementById(ctrlFirstNameField).innerHTML =
                    document.getElementById(ctrl.id.replace("SearchUser_selectLinkButton", "SearchUser_userFirstNameHiddenfield")).value;
                }
                window.opener.document.getElementById(ctrlFirstNameField.replace("ViewContributorSummary_questionAuthorIDTextBox", "ViewContributorSummary_loadDetailsButton")).click();
            }

            // Set the ID hidden field value. Replace the 'link button name' with the 
            // 'user ID hidden field name' and retrieve the value.
            if (ctrlEmailField != null && ctrlEmailField != '') {
                window.opener.document.getElementById(ctrlEmailField).value
                    = document.getElementById(ctrl.id.replace("SearchUser_selectLinkButton", "SearchUser_userEmailHiddenfield")).value;
            }
            self.close();
        }

    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchUser_headerLiteral" runat="server" Text="Search User"></asp:Literal>
                            <asp:HiddenField ID="SearchUser_browserHiddenField" runat="server" />
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SearchUser_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:UpdatePanel ID="SearchUser_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="SearchUser_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="SearchUser_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchUser_searchButton" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchUser_pageNavigator" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="SearchUser_searchCriteriasDiv" runat="server" style="display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="SearchUser_searchCriteriaUpdatePanel" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <table width="100%" border="0" cellspacing="3%" cellpadding="2%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="SearchUser_userNameLabel" runat="server" Text="User ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchUser_userNameTextBox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="SearchUser_firstNameLabel" runat="server" Text="First Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchUser_firstNameTextBox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="SearchUser_middleNameLabel" runat="server" Text="Middle Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchUser_middleNameTextBox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="SearchUser_lastNameLabel" runat="server" Text="Last Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchUser_lastNameTextBox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="4">
                                                                            <asp:Button ID="SearchUser_searchButton" runat="server" SkinID="sknButtonId" Text="Search"
                                                                                OnClick="SearchUser_searchButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="SearchUser_searchResultsUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr id="SearchUser_searchTestResultsTR" runat="server">
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 50%">
                                                                        <asp:Literal ID="SearchUser_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                            ID="SearchQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 50%" align="right">
                                                                        <span id="SearchUser_searchResultsUpSpan" style="display: none;" runat="server">
                                                                            <asp:Image ID="SearchUser_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                        </span><span id="SearchUser_searchResultsDownSpan" style="display: block;" runat="server">
                                                                            <asp:Image ID="SearchUser_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="height: 220px; overflow: auto;" runat="server" id="SearchUser_testDiv">
                                                                            <asp:GridView ID="SearchUser_testGridView" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                GridLines="Horizontal" BorderColor="white" BorderWidth="1px" Width="100%" OnSorting="SearchUser_testGridView_Sorting" OnRowDataBound="SearchUser_testGridView_RowDataBound"
                                                                                OnRowCreated="SearchUser_testGridView_RowCreated">
                                                                                <RowStyle CssClass="grid_alternate_row" />
                                                                                    <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                                    <HeaderStyle CssClass="grid_header_row" />
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="SearchUser_selectLinkButton" runat="server" Text="Select" OnClientClick="javascript:return OnSelectClick(this);"
                                                                                                ToolTip="Select"></asp:LinkButton>
                                                                                            <asp:HiddenField ID="SearchUser_userNameHiddenfield" runat="server" Value='<%# Eval("UserName") %>' />
                                                                                            <asp:HiddenField ID="SearchUser_userIDHiddenfield" runat="server" Value='<%# Eval("UserID") %>' />
                                                                                            <asp:HiddenField ID="SearchUser_userFirstNameHiddenfield" runat="server" Value='<%# Eval("FIRSTNAME") %>' />
                                                                                            <asp:HiddenField ID="SearchUser_userEmailHiddenfield" runat="server" Value='<%# Eval("Email") %>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="User_ID" DataField="UserID" Visible="false" />
                                                                                    <asp:BoundField HeaderText="User ID" DataField="UserName" SortExpression="USERNAME" />
                                                                                    <asp:BoundField HeaderText="First Name" DataField="FirstName" SortExpression="FIRSTNAME" />
                                                                                    <asp:BoundField HeaderText="Middle Name" DataField="MiddleName" SortExpression="MIDDLENAME" />
                                                                                    <asp:BoundField HeaderText="Last Name" DataField="LastName" SortExpression="LASTNAME" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <uc1:PageNavigator ID="SearchUser_pageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchUser_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_5">
                <table  cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="SearchUser_topResetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="SearchUser_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="SearchUser_topCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="SearchUser_isMaximizedHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
