<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchOnlineInterviewSession.aspx.cs"
    Inherits="Forte.HCM.UI.Popup.SearchOnlineInterviewSession" MasterPageFile="~/MasterPages/PopupMaster.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ID="SearchOnlineInterviewSession_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript">
        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl)
        {
            var from = '<%= Request.QueryString["from"] %>';
            var ctrlId = '<%= Request.QueryString["ctrlId"] %>';
            var ctrlName = '<%= Request.QueryString["ctrlname"] %>';
            var owrID = '<%= Request.QueryString["owrid"] %>';
            var owrName = '<%= Request.QueryString["owrname"] %>';

            if (window.dialogArguments) 
            {
                window.opener = window.dialogArguments; 
            }
            if (ctrlId != null && ctrlId != '')
            {
                if (from == "ATS")
                {
                    // Replace the 'link button name' with the 
                    // 'hidden field name' and set the value in opener control.
                    window.opener.document.getElementById(ctrlId).value
                        = document.getElementById(ctrl.id.replace("SearchOnlineInterviewSession_selectLinkButton", "SearchOnlineInterviewSession_selectIdHiddenfield")).value;

                    if (ctrlName != null && ctrlName != '')
                    {
                        window.opener.document.getElementById(ctrlName).value
                        = document.getElementById(ctrl.id.replace("SearchOnlineInterviewSession_selectLinkButton", "SearchOnlineInterviewSession_sessionNameHiddenfield")).value;
                    }

                    if (owrID != null && owrID != '')
                    {
                        window.opener.document.getElementById(owrID).value
                            = document.getElementById(ctrl.id.replace("SearchOnlineInterviewSession_selectLinkButton", "SearchOnlineInterviewSession_sessionAuthorIDHiddenField")).value;
                    }

                    if (owrName != null && owrName != '')
                    {
                        window.opener.document.getElementById(owrName).value
                            = document.getElementById(ctrl.id.replace("SearchOnlineInterviewSession_selectLinkButton", "SearchOnlineInterviewSession_sessionAuthorHiddenField")).value;
                    }
                }
                else
                {
                    // Replace the 'link button name' with the 
                    // 'hidden field name' and set the value in opener control.
                    window.opener.document.getElementById(ctrlId).value
                        = document.getElementById(ctrl.id.replace("SearchOnlineInterviewSession_selectLinkButton", "SearchOnlineInterviewSession_selectIdHiddenfield")).value;
                    
                    //Trigger the click event of the show button.
                    window.opener.document.getElementById(ctrlId.replace("ScheduleOnlineInterviewCandidate_onlineSessionIdTextBox", "ScheduleOnlineInterviewCandidate_showButton")).click();
                }
            }
            self.close();
        }
    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchOnlineInterviewSession_headerLiteral" runat="server" Text="Search Online Interview Session"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="QuestionDetailPreviewControl_topCancelImageButton" runat="server"
                                SkinID="sknCloseImageButton" OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                    <tr>
                        <td class="msg_align" colspan="2">
                            <asp:UpdatePanel ID="SearchOnlineInterviewSession_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="SearchOnlineInterviewSession_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="SearchOnlineInterviewSession_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <div id="SearchOnlineInterviewSession_searchCriteriasDiv" runat="server" style="display: block;">
                                            <table width="100%" border="0" cellspacing="3" cellpadding="2">
                                                <tr>
                                                    <td>
                                                        <table style="width: 100%" cellpadding="2" cellspacing="0">
                                                        <asp:UpdatePanel ID="SearchOnlineInterviewSession_searchCriteriaUpdatePanel" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <tr>
                                                                    <td style="width: 18%">
                                                                        <asp:Label ID="SearchOnlineInterviewSession_sessionIdLabel" runat="server" Text="Online Interview Session ID"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="SearchOnlineInterviewSession_sessionIdTextBox" runat="server"></asp:TextBox>
                                                                    </td>
                                                                     <td>
                                                                        <asp:Label ID="SearchOnlineInterviewSession_sessionNameLabel" runat="server" Text="Session Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="SearchOnlineInterviewSession_sessionNameTextBox" runat="server"></asp:TextBox>
                                                                    </td>

                                                                   
                                                                </tr>
                                                                <tr>
                                                                   

                                                                    <td>
                                                                        <asp:Label ID="SearchOnlineInterviewSession_positionProfileLabel" runat="server" Text="Position Profile"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="SearchOnlineInterviewSession_positionProfileTextBox" ReadOnly="true" runat="server"></asp:TextBox>
                                                                        </div>
                                                                        &nbsp;
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="SearchOnlineInterviewSession_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                runat="server" ImageAlign="Middle" />
                                                                        </div>
                                                                        <asp:HiddenField ID="SearchOnlineInterviewSession_positionProfileIDHiddenField" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="SearchOnlineInterviewSession_sessionAuthorNameHeadLabel" runat="server" Text="Session Author"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="SearchOnlineInterviewSession_sessionAuthorNameTextBox" runat="server" ReadOnly="true"
                                                                                Text=""></asp:TextBox>
                                                                            <asp:HiddenField ID="SearchOnlineInterviewSession_sessionAuthorNameHiddenField" runat="server" />
                                                                            <asp:HiddenField ID="SearchOnlineInterviewSession_sessionAuthorIDHiddenField" runat="server" />
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="SearchOnlineInterviewSession_sessionAuthorNameImageButton" SkinID="sknbtnSearchicon"
                                                                                Visible="false" runat="server" ImageAlign="Middle" Width="16px" ToolTip="Click here to select the session author" /></div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    
                                                                    <td>
                                                                        <asp:Label ID="SearchOnlineInterviewSession_schedulerNameHeadLabel" runat="server" Text="Online Interview Schedule Creator"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="SearchOnlineInterviewSession_schedulerNameTextBox" runat="server" ReadOnly="true"
                                                                                Text=""></asp:TextBox>
                                                                            <asp:HiddenField ID="SearchOnlineInterviewSession_schedulerIDHiddenField" runat="server" />
                                                                            <asp:HiddenField ID="SearchOnlineInterviewSession_schedulerHiddenField" runat="server" />
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="SearchOnlineInterviewSession_schedulerNameImageButton" SkinID="sknbtnSearchicon"
                                                                                runat="server" ImageAlign="Middle" Width="16px" ToolTip="Click here to select the schedule creator" /></div>
                                                                    </td>
                                                                     <td style="width: 22%">
                                                                        <asp:Label ID="SearchOnlineInterviewSession_sessionSkillLabel" runat="server" Text="Skills/Areas" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="SearchOnlineInterviewSession_sessionSkillTextBox" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" align="right">
                                                                        <asp:Button ID="SearchOnlineInterviewSession_topSearchButton" runat="server" Text="Search" OnClick="SearchOnlineInterviewSession_SearchButton_Click"
                                                                            SkinID="sknButtonId" />
                                                                    </td>
                                                                </tr>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="SearchOnlineInterviewSession_searchTestResultsTR" runat="server">
                                    <td id="Td1" class="header_bg" align="center" runat="server">
                                        <asp:UpdatePanel ID="SearchCategory_searchResultsUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%">
                                                            <asp:Literal ID="SearchOnlineInterviewSession_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>
                                                            &nbsp;<asp:Label ID="SearchOnlineInterviewSession_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                                Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%" align="right">
                                                            <span id="SearchOnlineInterviewSession_searchResultsUpSpan" style="display: none;" runat="server">
                                                                <asp:Image ID="SearchOnlineInterviewSession_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                            </span><span id="SearchOnlineInterviewSession_searchResultsDownSpan" style="display: block;"
                                                                runat="server">
                                                                <asp:Image ID="SearchOnlineInterviewSession_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="left">
                                                    <asp:UpdatePanel ID="SearchOnlineInterviewSession_updatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <div style="height: 200px; overflow: auto;" runat="server" id="SearchOnlineInterviewSession_testDiv">
                                                                <asp:GridView ID="SearchOnlineInterviewSession_testGridView" runat="server" AllowSorting="True"
                                                                    AutoGenerateColumns="False" GridLines="None" Width="100%" OnSorting="SearchOnlineInterviewSession_testGridView_Sorting"
                                                                    OnRowCreated="SearchOnlineInterviewSession_testGridView_RowCreated" OnRowDataBound="SearchOnlineInterviewSession_testGridView_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="SearchOnlineInterviewSession_selectLinkButton" runat="server" Text="Select"
                                                                                    OnClientClick="javascript:return OnSelectClick(this);" ToolTip="Select"></asp:LinkButton>
                                                                                <asp:HiddenField ID="SearchOnlineInterviewSession_selectIdHiddenfield" runat="server" 
                                                                                    Value='<%# Eval("OnlineInterviewSessionKey") %>' />
                                                                                <asp:HiddenField ID="SearchOnlineInterviewSession_sessionNameHiddenfield" runat="server" 
                                                                                    Value='<%# Eval("OnlineInterviewSessionName") %>' />
                                                                                <asp:HiddenField ID="SearchOnlineInterviewSession_sessionAuthorIDHiddenField" runat="server" 
                                                                                    Value='<%# Eval("SessionAuthorID") %>' />
                                                                                <asp:HiddenField ID="SearchOnlineInterviewSession_sessionAuthorHiddenField" runat="server" 
                                                                                    Value='<%# Eval("SessionAuthor") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="ONLINEINTERVIEWSESSIONID" HeaderText="Online Interview Session ID">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchOnlineInterviewSession_sessionIDLabel" runat="server" Text='<%# Eval("OnlineInterviewSessionKey") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="ONLINEINTERVIEWSESSIONNAME" HeaderText="Session&nbsp;Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchOnlineInterviewSession_sessionNameLabel" runat="server" Text='<%# TrimContent(Eval("OnlineInterviewSessionName").ToString(),20) %>'
                                                                                    ToolTip='<%# Eval("OnlineInterviewSessionName")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="ONLINEINTERVIEWSESSIONAUTHOR" HeaderText="Author">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchOnlineInterviewSession_sessionAuthor" runat="server" ToolTip='<%# Eval("SessionAuthorFullName") %>'
                                                                                    Text='<%# Eval("SessionAuthor") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="ONLINEINTERVIEWSESSIONSKILL" HeaderText="Skills/Areas">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchOnlineInterviewSession_sessionSkills" runat="server" ToolTip='<%# Eval("skills") %>'
                                                                                    Text='<%# Eval("skills") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="ONLINEINTERVIEWCREATEDDATE" HeaderText="Created Date">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchOnlineInterviewSession_createdDateLabel" runat="server" Text='<%# Convert.ToDateTime(Eval("CreatedDate")).ToString("MM/dd/yyyy") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                       
                                                                        
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="SearchOnlineInterviewSession_pagingControlUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <uc1:PageNavigator ID="SearchOnlineInterviewSession_bottomPagingNavigator" runat="server" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" align="left">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="SearchOnlineInterviewSession_bottomLinksUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="SearchOnlineInterviewSession_bottomReset" runat="server" Text="Reset" SkinID="sknPopupLinkButton"
                                                    OnClick="SearchOnlineInterviewSession_bottomReset_Click"></asp:LinkButton>
                                            </td>
                                            <td class="pop_divider_line">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="SearchOnlineInterviewSession_bottomCancel" runat="server" Text="Cancel"
                                                    SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="ScheduleCandidate_isMaximizedHiddenField" runat="server" />
</asp:Content>
