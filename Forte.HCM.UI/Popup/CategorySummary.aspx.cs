﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CategorySummary.cs
// File that represents the user interface for showing categories
// which are contributed in the question by particular author.
// This will interact with the QuestionBLManager to get category list from DB.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface for showing
    /// report regarding categories contributed in a question
    /// based on the question author. This class is inherited
    /// from PageBase.
    /// </summary>
    public partial class CategorySummary : PageBase
    {
        #region Event Handler                                                  

        /// <summary>
        /// Event handler will get fired when a page is loaded
        /// and load the category information based on the authorid
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> object holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Categories Contributed");

                // Subscribes to the page number click event.
                CategorySummary_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                    (CategorySummary_pageNavigator_PageNumberClick);

                if (!IsPostBack)
                {
                    // Check if the querystring contains author id
                    if (!Utility.IsNullOrEmpty(Request.QueryString["authorid"]))
                    {
                        LoadCategories(1);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CategorySummary_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        void CategorySummary_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadCategories(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CategorySummary_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// triggered in the test inclusion detail gridview.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CategorySummary_categoriesGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
            }
        }

        #endregion Event Handler                                               

        #region Private Methods                                                
        
        /// <summary>
        /// Load categories list
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="pageNumber"/> that contains the current page number.
        /// </param>
        private void LoadCategories(int pageNumber)
        {
            int totalRecords = 0;

            List<Category> categories = null;


            if(Request.QueryString["type"]!=null)
                if(Request.QueryString["type"].ToString() == "Test")
            categories =
                new QuestionBLManager().GetCategoriesContributed
                (Convert.ToInt32(Request.QueryString["authorid"].ToString().Trim()),pageNumber,
                base.GridPageSize, out totalRecords);
                else
          categories =
               new QuestionBLManager().GetInterviewCategoriesContributed
               (Convert.ToInt32(Request.QueryString["authorid"].ToString().Trim()), pageNumber,
               base.GridPageSize, out totalRecords);

            if (categories != null)
            {
                CategorySummary_categoriesGridView.DataSource = categories;
                CategorySummary_categoriesGridView.DataBind();
                CategorySummary_pageNavigator.PageSize = base.GridPageSize;
                CategorySummary_pageNavigator.TotalRecords = totalRecords;
            }
        }

        #endregion Private Methods


        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods                                
    }
}
