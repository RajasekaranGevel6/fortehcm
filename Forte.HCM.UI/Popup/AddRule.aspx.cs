﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AddRule.aspx.cs
// File that represents the user interface layout and functionalities for
// the AddRule page. This will helps to add new rule

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the AddRule page. This will helps to add new rule
    /// This class inherits
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class AddRule : PageBase
    {
        #region Private Variables                                              
          
        /// <summary>
        /// A <see cref="string"/> that holds the rowCounter.
        /// </summary>
        private int rowCounter;

        /// <summary>
        /// A <see cref="string"/> that holds the mode.
        /// </summary>
        private string mode = string.Empty;

        #endregion Private Variables

        #region Private Properties                                             

        private List<ResumeRuleDetail> dataSource;

        #endregion Private Properties

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Check if mode is edit.
                if (!Utility.IsNullOrEmpty(Request.QueryString["mode"]))
                    mode = Request.QueryString["mode"].Trim().ToUpper();

                // Set title.
                SetTitle(mode);

                if (!IsPostBack)
                {
                    // Set default button and focus.
                    List<ResumeElementsDetail> resumeElement = new List<ResumeElementsDetail>();
                    resumeElement = new ResumeRepositoryBLManager().GetResumeRuleElements();
                    if (resumeElement != null && resumeElement.Count > 0)
                        Session["RULE_ELEMENTS"] = resumeElement;

                    Page.Form.DefaultButton = AddRule_saveButton.UniqueID;

                    if (mode == "E")
                    {
                        // Load rule for edit mode.
                        LoadEditRule();
                    }
                    else
                    {
                        SetRule(null);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AddRule_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when add conditon is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void AddRule_addConditionLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                SetViewState();
                AddRule_listView.DataSource = null;
                if (ViewState["RULE_CONDITION_LIST"] != null)
                {
                    this.dataSource = (List<ResumeRuleDetail>)ViewState["RULE_CONDITION_LIST"];
                }
                else
                    this.dataSource = new List<ResumeRuleDetail>();
                this.dataSource.Add(new ResumeRuleDetail());
                ViewState["RULE_CONDITION_LIST"] = this.dataSource;
                rowCounter = this.dataSource.Count;
                AddRule_listView.DataSource = this.dataSource;
                AddRule_listView.DataBind();
                BindNodeAttributes();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(AddRule_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when delete conditon image is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void RoleControl_delete_Click(object sender, EventArgs e)
        {
            try
            {
                int intDeletedRowIndex = Convert.ToInt32(AddRule_deletedRowHiddenField.Value);
                if (intDeletedRowIndex != 0)
                {
                    this.dataSource = (List<ResumeRuleDetail>)ViewState["RULE_CONDITION_LIST"];
                    this.dataSource.RemoveAt(intDeletedRowIndex - 1);
                }
                else
                {
                    int intLastRecord = AddRule_listView.Items.Count - 1;
                    this.dataSource = (List<ResumeRuleDetail>)ViewState["RULE_CONDITION_LIST"];
                    this.dataSource.RemoveAt(intLastRecord);
                }
                rowCounter = dataSource.Count;
                AddRule_listView.DataSource = dataSource;
                AddRule_listView.DataBind();
                BindNodeAttributes();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(AddRule_topErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Handler method that will be called when dropdown value is selected.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void AddRule_destinationNodeDropDown_SelectedIndexChanged(
            object sender, EventArgs e)
        {
            try
            {
                //Get the selected values
                DropDownList AddRule_destinationNodeDropDown = ((DropDownList)sender);
                ListViewItem item = ((ListViewItem)AddRule_destinationNodeDropDown.NamingContainer);
                TextBox AddRule_destinationValueTextBox = 
                    (TextBox)item.FindControl("AddRule_destinationValueTextBox");
                if (AddRule_destinationNodeDropDown.SelectedValue == "TheValue")
                {
                    AddRule_destinationValueTextBox.Enabled = true;
                }
                else
                {
                    AddRule_destinationValueTextBox.Text = string.Empty;
                    AddRule_destinationValueTextBox.Enabled = false;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(AddRule_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when delete is callded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void AddRule_listView_ItemCommand(object sender, 
            ListViewCommandEventArgs e)
        {
            try
            {
                SetViewState();
                if (e.CommandName == "deleteRole")
                {
                    ImageButton AddRule_deleteConditionImageButton =
                        (ImageButton)e.Item.FindControl("AddRule_deleteConditionImageButton");
                    Label txtRowIndex = (Label)e.Item.FindControl("AddRule_rowIndexLabel");
                    AddRule_deletedRowHiddenField.Value = txtRowIndex.Text;
                    RoleControl_delete_Click(AddRule_deleteConditionImageButton, new EventArgs());
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(AddRule_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will be called when save button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void AddRule_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;

                string actions=string.Empty;
                string conditionComments = string.Empty;
                string xmlFields = string.Empty;
                string sqlFields = string.Empty;
                string xmlConditions = string.Empty;

                
                SetViewState();
                //Construct duplication rule
                ResumeRuleDetail resumeRuleDetail = new ResumeRuleDetail();
                resumeRuleDetail.RuleName = AddRule_nameTextBox.Text;
                resumeRuleDetail.RuleReason = AddRule_reasonTextBox.Text;
               

                //Construct duplication rule entries
                List<ResumeRuleDetail> resumeRuleDetails = new List<ResumeRuleDetail>();
                if (ViewState["RULE_CONDITION_LIST"] != null)
                {
                    resumeRuleDetails = (List<ResumeRuleDetail>)ViewState["RULE_CONDITION_LIST"];
                }

                //Construct xml fields
                if (!Utility.IsNullOrEmpty(resumeRuleDetails))
                {
                    string xmlSyntax = "CH.HR_XML.value";
                    string phoneFunctionName = "[DBO].[FNGET_PHONE_NUMBER]";
                    string sourceXmlNode = string.Empty;
                    string sourceSqlField = string.Empty;
                    string destinationXmlNode = string.Empty;
                    string destinationSqlField = string.Empty;

                    string sourceElementNode = string.Empty;
                    string DestinationElementNode = string.Empty;
                    string expressionType=string.Empty;
                    string logicalId = string.Empty;
                    string matchKeyWord = string.Empty;
                    
                    foreach (ResumeRuleDetail resumeRule in resumeRuleDetails)
                    {
                        sourceElementNode = resumeRule.SourceElementNode.ToString().Trim().Replace(" ", "");
                        DestinationElementNode = resumeRule.DestinationElementNode.ToString().Trim().Replace(" ", "");
                        List<string> oSourceList=new ResumeRepositoryBLManager().
                            GetRumeRuleXmlNode(resumeRule.SourceElementID);
                        if (oSourceList != null)
                        {
                            sourceSqlField = oSourceList[0];
                            sourceXmlNode = oSourceList[1];
                        }

                        List<string> oDestList = new ResumeRepositoryBLManager().
                            GetRumeRuleXmlNode(resumeRule.DestinationElementID);
                        if (oDestList != null)
                        {
                            destinationSqlField = oDestList[0];
                            destinationXmlNode = oDestList[1];
                        }

                        if (resumeRule.SourceElementID == resumeRule.DestinationElementID 
                            || resumeRule.DestinationElementID==1)
                        {
                            xmlFields = xmlFields + string.Format("{0}({1}) {2}", xmlSyntax, 
                                sourceXmlNode, sourceElementNode) + "~";

                            //Construxt sql fields
                            sqlFields = sqlFields + string.Format("CI.{0}", sourceSqlField) + "~";
                        }
                        else
                        {
                            xmlFields = xmlFields + string.Format("{0}({1}) {2}", xmlSyntax, 
                                sourceXmlNode, sourceElementNode)
                                + "~" + string.Format("{0}({1}) {2}", xmlSyntax,
                                destinationXmlNode, DestinationElementNode) + "~";

                            //Construxt sql fields
                            sqlFields = sqlFields + string.Format("CI.{0}", 
                                sourceSqlField) + "~" + string.Format("CI.{0}", destinationSqlField) + "~";
                        }

                        if (resumeRule.ExpressionType.ToString().Trim() == 
                            Constants.ResumeDuplicationRuleExpressionType.MATCHES)
                        {
                            expressionType = "LIKE";
                            matchKeyWord = "Matches";
                        }
                        else
                        {
                            expressionType = "NOT LIKE";
                            matchKeyWord = "Not Matches";
                        }

                        logicalId = null;
                        if ((!Utility.IsNullOrEmpty(resumeRule.LogicalOperaterId)) 
                            && resumeRule.LogicalOperaterId != "--Select--")
                        {
                            if (resumeRule.LogicalOperaterId.ToString().Trim() == 
                                Constants.ResumeDuplicationRuleLogicalOperator.AND)
                                logicalId = "AND";
                            else
                                logicalId = "OR";
                        }

                        //Construct xml conditions
                        if (DestinationElementNode.ToString().Trim() == "TheValue")
                        {
                            if (resumeRule.SourceElementID == 11 || resumeRule.SourceElementID == 12)
                            {
                                xmlConditions = xmlConditions +
                                    string.Format("{0}(CA.{1})={2}('{3}') OR {4}({5})={6}('{7}') {8} ",
                                    phoneFunctionName,sourceElementNode, phoneFunctionName, 
                                    resumeRule.DestinationValue.ToString().Trim(), phoneFunctionName,
                                    sourceSqlField, phoneFunctionName, 
                                    resumeRule.DestinationValue.ToString().Trim(), logicalId);
                            }
                            else
                            {
                                xmlConditions = xmlConditions +
                                    string.Format("CA.{0} {1} '%{2}%' OR {3} {4} '%{5}%' {6} ",
                                    sourceElementNode, expressionType,
                                    resumeRule.DestinationValue.ToString().Trim(),
                                    sourceSqlField, expressionType,
                                    resumeRule.DestinationValue.ToString().Trim(), 
                                    logicalId);
                            }
                              //Construct condition comments
                            conditionComments = conditionComments + string.Format("{0} {1} {2} {3} ",
                                resumeRule.SourceElementNode.ToString(), matchKeyWord, 
                                resumeRule.DestinationValue.ToString(), logicalId);
                        }
                        else
                        {
                            if (resumeRule.SourceElementID == 11 || resumeRule.SourceElementID == 12)
                            {
                                xmlConditions = xmlConditions +
                                    string.Format("(CEX.{0} IS NOT NULL AND CEX.{1} <>'' AND ({2}(CA.{3})={4}(CEX.{5}) OR {6}({7})={8}(CEX.{9}))) {10} ",
                                    DestinationElementNode,DestinationElementNode,
                                    phoneFunctionName,sourceElementNode, 
                                    phoneFunctionName, DestinationElementNode,phoneFunctionName,
                                    sourceSqlField, phoneFunctionName, 
                                    DestinationElementNode, logicalId);
                            }
                            else
                            {
                                xmlConditions = xmlConditions +
                                    string.Format("(CEX.{0} IS NOT NULL AND CEX.{1} <>'' AND (CA.{2} {3} '%'+ CEX.{4} +'%' OR {5} {6} '%'+ CEX.{7} +'%')) {8} ",
                                    DestinationElementNode,DestinationElementNode,
                                    sourceElementNode, expressionType, DestinationElementNode,
                                    sourceSqlField, expressionType, DestinationElementNode, logicalId);
                            }
                              //Construct condition comments
                            conditionComments = conditionComments + string.Format("{0} {1} {2} {3} ",
                                resumeRule.SourceElementNode.ToString(), matchKeyWord, 
                                resumeRule.DestinationElementNode.ToString(), logicalId);
                        }
                    }
                    if (!Utility.IsNullOrEmpty(sqlFields))
                    {
                        string[] splitSqlFields = sqlFields.Split('~').Distinct().ToArray();

                        sqlFields = null;
                        foreach (string sqlField in splitSqlFields)
                            sqlFields = sqlFields + sqlField + "~";

                        sqlFields = sqlFields.TrimEnd('~');
                    }
                    string[] splitXMLFields = xmlFields.Split('~').Distinct().ToArray();

                    xmlFields = null;
                    foreach (string xmlNode in splitXMLFields)
                        xmlFields = xmlFields + xmlNode + "~";

                    xmlFields = xmlFields.TrimEnd('~');

                    resumeRuleDetail.XmlFields = xmlFields;
                    resumeRuleDetail.SqlFields = sqlFields;
                    resumeRuleDetail.XmlCondition = xmlConditions;
                    resumeRuleDetail.RuleComments = conditionComments;
                }

                //Construct duplication rule actions
                if (AddRule_addToExistingRecordRadioButton.Checked)
                    actions = Constants.ResumeDuplicationRuleActions.ADD_TO_EXISTING_CANDIDATE_RECORD;

                if (AddRule_markAsDuplicateCheckBoxRadioButton.Checked)
                    actions = Constants.ResumeDuplicationRuleActions.MARK_AS_DUPLICATE;

                // Check if mode is edit.
                if (!Utility.IsNullOrEmpty(Request.QueryString["mode"]))
                    mode = Request.QueryString["mode"].Trim().ToUpper();

                if (mode == "A")
                {
                    new ResumeRepositoryBLManager().InsertResumeDuplicationRule(resumeRuleDetails,
                        resumeRuleDetail, actions, base.tenantID, base.userID);
                }
                else if (mode == "E")
                {
                    int ruleID = 0;

                    // Check if rule is present.
                    if (!Utility.IsNullOrEmpty(Request.QueryString["id"]))
                        ruleID = Convert.ToInt32(Request.QueryString["id"]);
                    
                    new ResumeRepositoryBLManager().UpdateResumeDuplicationRule(resumeRuleDetails,
                        resumeRuleDetail, ruleID, actions, base.userID);
                }
                // Close popup window.
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "Closeform", "<script language='javascript'>CloseAddRuleWindow();</script>", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(AddRule_topErrorMessageLabel, exp.Message);
            }
        }


        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValid = true;

            if (Utility.IsNullOrEmpty(AddRule_nameTextBox.Text))
            {
                base.ShowMessage(AddRule_topErrorMessageLabel, "Please enter rule name");
                isValid = false;
            }

            if (Utility.IsNullOrEmpty(AddRule_reasonTextBox.Text))
            {
                base.ShowMessage(AddRule_topErrorMessageLabel, "Please enter reason");
                isValid = false;
            }

            isValid = ValidateListView();

            if (!AddRule_addToExistingRecordRadioButton.Checked &&
                !AddRule_markAsDuplicateCheckBoxRadioButton.Checked)
            {
                base.ShowMessage(AddRule_topErrorMessageLabel, "Please select action");
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method to validate the list view grid
        /// </summary>
        /// <returns>returns true or fals</returns>
        private bool ValidateListView()
        {
            int rowCnt = 0;
            bool isValid = true;
            if (AddRule_listView == null || AddRule_listView.Items.Count == 0)
            {
                base.ShowMessage(AddRule_topErrorMessageLabel,
                    "Please add condition");
                isValid = false;
            }
            foreach (ListViewDataItem item in AddRule_listView.Items)
            {
                rowCnt = rowCnt + 1;
                DropDownList AddRule_sourceNodeDropDown = 
                    (DropDownList)item.FindControl("AddRule_sourceNodeDropDown");
                DropDownList AddRule_expressionTypeDropDown = 
                    (DropDownList)item.FindControl("AddRule_expressionTypeDropDown");
                DropDownList AddRule_destinationNodeDropDown =
                    (DropDownList)item.FindControl("AddRule_destinationNodeDropDown");
                DropDownList AddRule_logicalOperatorDropDown = 
                        (DropDownList)item.FindControl("AddRule_logicalOperatorDropDown");
                TextBox AddRule_destinationValueTextBox = 
                        (TextBox)item.FindControl("AddRule_destinationValueTextBox");

                if (AddRule_sourceNodeDropDown.SelectedValue == "--Select--" ||
                    AddRule_expressionTypeDropDown.SelectedValue == "--Select--" ||
                    AddRule_destinationNodeDropDown.SelectedValue == "--Select--")
                {
                    base.ShowMessage(AddRule_topErrorMessageLabel, 
                        string.Format("Please fill the condition for row no. {0}", rowCnt));
                    isValid = false;
                }

                if (AddRule_destinationNodeDropDown.
                    SelectedValue.ToString().Trim() == "TheValue" &&
                    Utility.IsNullOrEmpty(AddRule_destinationValueTextBox.Text))
                {
                    base.ShowMessage(AddRule_topErrorMessageLabel,
                        string.Format("Please enter match value for row no. {0}", rowCnt));
                    isValid = false;
                }

                if (AddRule_listView.Items.Count != rowCnt && 
                    AddRule_logicalOperatorDropDown.SelectedValue == "--Select--")
                {
                    base.ShowMessage(AddRule_topErrorMessageLabel,
                        string.Format("Please choose and/or for row no. {0}", rowCnt));
                    isValid = false;
                }

                if (AddRule_listView.Items.Count == rowCnt)
                {
                    if (AddRule_logicalOperatorDropDown.SelectedValue != "--Select--")
                    {
                        base.ShowMessage(AddRule_topErrorMessageLabel,
                            string.Format("And/Or is not required for row no. {0}", rowCnt));
                        isValid = false;
                    }
                }
            }
            return isValid;
        }

        /// <summary>
        /// Method that reads each rows of list view and construct list 
        /// </summary>
        private void SetViewState()
        {
            try
            {
                List<ResumeRuleDetail> oRuleList = null;
                int intRowCnt = 0;
                foreach (ListViewDataItem item in AddRule_listView.Items)
                {
                    if (oRuleList == null)
                    {
                        oRuleList = new List<ResumeRuleDetail>();
                    }
                    ResumeRuleDetail oRule = new ResumeRuleDetail();
                    DropDownList AddRule_sourceNodeDropDown = 
                        (DropDownList)item.FindControl("AddRule_sourceNodeDropDown");
                    DropDownList AddRule_expressionTypeDropDown = 
                        (DropDownList)item.FindControl("AddRule_expressionTypeDropDown");
                    DropDownList AddRule_destinationNodeDropDown = 
                        (DropDownList)item.FindControl("AddRule_destinationNodeDropDown");
                    TextBox AddRule_destinationValueTextBox = 
                        (TextBox)item.FindControl("AddRule_destinationValueTextBox");
                    DropDownList AddRule_logicalOperatorDropDown = 
                        (DropDownList)item.FindControl("AddRule_logicalOperatorDropDown");
                    HiddenField AddRule_xmlNodeHiddenField = 
                        (HiddenField)item.FindControl("AddRule_xmlNodeHiddenField");
                    intRowCnt = intRowCnt + 1;
                    oRule.RowID = intRowCnt;
                    if (AddRule_sourceNodeDropDown.SelectedValue != "--Select--")
                        oRule.SourceElementID = 
                            Convert.ToInt32(AddRule_sourceNodeDropDown.SelectedValue);

                    if (AddRule_destinationNodeDropDown.SelectedValue != "--Select--")
                    {
                        if (AddRule_destinationNodeDropDown.SelectedValue.Trim() == "TheValue")
                            oRule.DestinationElementID = 1;
                        else
                            oRule.DestinationElementID = 
                                Convert.ToInt32(AddRule_destinationNodeDropDown.SelectedValue);
                    }
                    else
                    {
                        oRule.DestinationElementID = 0;
                    }
                    oRule.ExpressionType = AddRule_expressionTypeDropDown.SelectedValue;
                    oRule.DestinationValue = AddRule_destinationValueTextBox.Text.Trim();

                    if (AddRule_logicalOperatorDropDown.SelectedValue != "--Select--")
                        oRule.LogicalOperaterId = 
                            AddRule_logicalOperatorDropDown.SelectedValue;

                    oRule.SourceElementNode = 
                        AddRule_sourceNodeDropDown.SelectedItem.Text.ToString().Trim();
                    oRule.DestinationElementNode = 
                        AddRule_destinationNodeDropDown.SelectedItem.Text.ToString().Trim();

                    oRuleList.Add(oRule);
                }
                AddRule_listView.DataSource = oRuleList;
                AddRule_listView.DataBind();
                ViewState["RULE_CONDITION_LIST"] = oRuleList;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(AddRule_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method to fill the drop down list
        /// </summary>
        /// <param name="ddl">ddl</param>
        /// <param name="index">index</param>
        /// <param name="selectedId">selectedId</param>
        private void CreateDDL(DropDownList ddl, int index, string selectedId)
        {
            switch (index)
            {
                case 0:
                    ddl.DataSource = new AttributeBLManager().
                        GetAttributesByType(Constants.AttributeTypes.RESUME_DUPLICATE_EXPRESSION, "A");
                    ddl.DataTextField = "AttributeName";
                    ddl.DataValueField = "AttributeID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, "--Select--");
                    ddl.SelectedIndex = 0;
                    if (!Utility.IsNullOrEmpty(selectedId))
                        ddl.SelectedValue = selectedId.ToString();
                    break;
                case 1:
                    ddl.DataSource = new AttributeBLManager().
                        GetAttributesByType(Constants.AttributeTypes.RESUME_DUPLICATE_EXPRESSION_OPTION, "A");
                    ddl.DataTextField = "AttributeName";
                    ddl.DataValueField = "AttributeID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, "--Select--");
                    ddl.SelectedIndex = 0;
                    if (!Utility.IsNullOrEmpty(selectedId))
                        ddl.SelectedValue = selectedId.ToString();
                    break;
                case 2:
                    List<ResumeElementsDetail> resumeElements = null;
                    if (Session["RULE_ELEMENTS"] == null)
                        resumeElements = new List<ResumeElementsDetail>();
                    else
                        resumeElements = Session["RULE_ELEMENTS"] as List<ResumeElementsDetail>;

                    if (resumeElements != null && resumeElements.Count > 0)
                    {
                        ddl.DataSource = resumeElements;
                        ddl.DataTextField = "Description";
                        ddl.DataValueField = "ElementId";
                        ddl.DataBind();
                        ddl.Items.Insert(0, "--Select--");
                        if (ddl.ID == "AddRule_destinationNodeDropDown")
                        {
                            ddl.Items.Insert(1, "TheValue");
                        }
                        ddl.SelectedIndex = 0;
                        if (!Utility.IsNullOrEmpty(selectedId))
                        {
                            if(selectedId=="1")
                                ddl.SelectedIndex = 1;
                            else
                                ddl.SelectedValue = selectedId.ToString();
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Method to load all the attributes
        /// </summary>
        private void BindNodeAttributes()
        {
            int rowCnt = 0;
            int sourceElementID = 0;
            int destinationElementID = 0;
            string expressionTypeId = string.Empty;
            string logicalOperaterId = string.Empty;
            List<ResumeRuleDetail> resumeRuleDetail = null;
            if (ViewState["RULE_CONDITION_LIST"] != null)
            {
                resumeRuleDetail = (List<ResumeRuleDetail>)ViewState["RULE_CONDITION_LIST"];
            }
            foreach (ListViewDataItem item in AddRule_listView.Items)
            {
                DropDownList AddRule_sourceNodeDropDown = (DropDownList)item.FindControl("AddRule_sourceNodeDropDown");
                DropDownList AddRule_destinationNodeDropDown = (DropDownList)item.FindControl("AddRule_destinationNodeDropDown");
                DropDownList AddRule_expressionTypeDropDown = (DropDownList)item.FindControl("AddRule_expressionTypeDropDown");
                DropDownList AddRule_logicalOperatorDropDown = (DropDownList)item.FindControl("AddRule_logicalOperatorDropDown");
                if (resumeRuleDetail != null && resumeRuleDetail.Count > 0)
                {
                    sourceElementID = resumeRuleDetail[rowCnt].SourceElementID;
                    destinationElementID = resumeRuleDetail[rowCnt].DestinationElementID;
                    expressionTypeId = resumeRuleDetail[rowCnt].ExpressionType;
                    logicalOperaterId = resumeRuleDetail[rowCnt].LogicalOperaterId;
                }
                CreateDDL(AddRule_sourceNodeDropDown, 2,  sourceElementID.ToString());
                CreateDDL(AddRule_destinationNodeDropDown, 2, destinationElementID.ToString());
                CreateDDL(AddRule_expressionTypeDropDown, 0, expressionTypeId);
                CreateDDL(AddRule_logicalOperatorDropDown, 1, logicalOperaterId);
                rowCnt++;
            }
        }

        /// <summary>
        /// Method that sets the title based on the mode.
        /// </summary>
        /// <param name="mode">
        /// A <see cref="string"/> that holds the mode. Value 'A' represents
        /// 'Add' and 'E' represents 'Edit'
        /// </param>
        private void SetTitle(string mode)
        {
            if (mode == "A")
            {
                Master.SetPageCaption("Add Rule");
                AddRule_titleLiteral.Text = "Add Rule";
            }
            else if (mode == "E")
            {
                Master.SetPageCaption("Edit Rule");
                AddRule_titleLiteral.Text = "Edit Rule";
            }
        }

        /// <summary>
        /// Method that loads the data for edit mode.
        /// </summary>
        private void LoadEditRule()
        {
            // Check if time slot id is present.
            if (Utility.IsNullOrEmpty(Request.QueryString["id"]))
            {
                base.ShowMessage(AddRule_topErrorMessageLabel, "No rule id is passed");
                return;
            }

            int id = 0;
            int.TryParse(Request.QueryString["id"], out id);

            if (id == 0)
            {
                base.ShowMessage(AddRule_topErrorMessageLabel, "No valid rule is passed");
                return;
            }

            //GetResumeRuleDetail
            ResumeRuleDetail resumeRuleDetail = null;
            resumeRuleDetail = new ResumeRepositoryBLManager().GetResumeRuleDetail(id);

            if (!Utility.IsNullOrEmpty(resumeRuleDetail))
            {
                //Set rule name and reason
                AddRule_nameTextBox.Text = resumeRuleDetail.RuleName;
                AddRule_reasonTextBox.Text = resumeRuleDetail.RuleReason;

                //Set rule condition list
                SetRule(resumeRuleDetail.ResumeRuleEntries);

                //Set action & event
                if (!Utility.IsNullOrEmpty(resumeRuleDetail.RuleAction))
                {
                    foreach (string action in resumeRuleDetail.RuleAction.Split(new char[] { ',' }))
                    {
                        if (action.ToString().Trim() == Constants.ResumeDuplicationRuleActions.ADD_TO_EXISTING_CANDIDATE_RECORD)
                            AddRule_addToExistingRecordRadioButton.Checked = true;
                        if (action.ToString().Trim() == Constants.ResumeDuplicationRuleActions.MARK_AS_DUPLICATE)
                            AddRule_markAsDuplicateCheckBoxRadioButton.Checked = true;
                    }
                }
            }
        }

       /// <summary>
       /// Method to load the rule condition
       /// </summary>
       /// <param name="resumeRuleDetails"></param>
        private void SetRule(List<ResumeRuleDetail> resumeRuleDetails)
        {
            
            List<ResumeRuleDetail> oListRule = new List<ResumeRuleDetail>();
            if (resumeRuleDetails == null || resumeRuleDetails.Count == 0)
            {
                ResumeRuleDetail oResumeReule = new ResumeRuleDetail();
                oResumeReule.RowID = 1;

                oResumeReule.SourceElementID = 0;
                oResumeReule.ExpressionType = string.Empty;
                oResumeReule.DestinationElementID = 0;
                oResumeReule.DestinationValue = string.Empty;
                oResumeReule.LogicalOperaterId = string.Empty;
                oListRule.Add(oResumeReule);
            }
            else
            {
                int intRefCnt = 0;
                foreach (ResumeRuleDetail resumeRuleDetail in resumeRuleDetails)
                {
                    ResumeRuleDetail resumeRuleEntries = new ResumeRuleDetail();
                    intRefCnt = intRefCnt + 1;

                    resumeRuleEntries.RowID = intRefCnt;
                    resumeRuleEntries.SourceElementID = resumeRuleDetail.SourceElementID;
                    resumeRuleEntries.ExpressionType = resumeRuleDetail.ExpressionType.ToString().Trim();
                    resumeRuleEntries.DestinationElementID = resumeRuleDetail.DestinationElementID;
                    resumeRuleEntries.DestinationValue = resumeRuleDetail.DestinationValue.ToString().Trim();
                    resumeRuleEntries.LogicalOperaterId = resumeRuleDetail.LogicalOperaterId.ToString().Trim();
                    
                    oListRule.Add(resumeRuleEntries);
                }
            }

            AddRule_listView.DataSource = oListRule;
            AddRule_listView.DataBind();
            ViewState["RULE_CONDITION_LIST"] = oListRule;
            BindNodeAttributes();
        }

        #endregion Private Methods

        #region Public Properties                                              
            
        /// <summary>
        /// Method that stores the rule condition
        /// </summary>
        public List<ResumeRuleDetail> DataSource
        {
            set
            {
                if (value == null)
                {
                    return;
                }
                AddRule_listView.DataSource = value;
                AddRule_listView.DataBind();
                ViewState["RULE_CONDITION_LIST"] = value;
            }
            get
            {
                if (ViewState["RULE_CONDITION_LIST"] != null)
                    return (List<ResumeRuleDetail>)ViewState["RULE_CONDITION_LIST"];
                else
                    return null;
            }
        }

        #endregion Public Properties

    }
}