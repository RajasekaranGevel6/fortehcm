#region Header

// Copyright (C) Forte. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ApproveCandidateSelfSkill.aspx.cs
// File that represents the user interface layout and functionalities 
// for the approvecandidateselfskill page. This page helps to approve candidate self skill.
// This class inherits Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives

using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Data;
using Forte.HCM.Common.DL;


#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the ApproveCandidateSelfSkill class that defines the user interface
    /// layout and functionalities for the Approve Candidate Self Skill page. This page helps to 
    /// view approve Candidate Self Skill.This class inherits Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class ApproveCandidateSelfSkill : PageBase
    {
        #region Events Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {   //Clear cache
                Response.Cache.SetCacheability(HttpCacheability.NoCache);

                // Set page title
                Master.SetPageCaption("Approve/Reject Skill");

                //Load values
                if (!IsPostBack)
                {
                    if (Request.QueryString["skillId"] != null)
                        ViewState["SKILL_ID"] = Request.QueryString["skillId"].ToString();
                    if (Request.QueryString["candidateId"] != null)
                        ViewState["CANDIDATE_ID"] = Request.QueryString["candidateId"].ToString();

                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ApproveCandidateSelfSkill_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void ApproveCandidateSelfSkill_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow gv in ApproveCandidateSelfSkill_skillGridView.Rows)
                {
                    RadioButton ApproveCandidateSelfSkill_skillRadioButton = gv.FindControl("ApproveCandidateSelfSkill_skillRadioButton") as RadioButton;
                    if (ApproveCandidateSelfSkill_skillRadioButton.Checked)
                    {
                        ApproveCandidateSelfSkill_skillRadioButton.Checked = false;
                        break;
                    }
                }
                DisplaySkillName();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ApproveCandidateSelfSkill_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void ApproveCandidateSelfSkill_skillGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ApproveCandidateSelfSkill_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods
        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadRelatedSkills(string skillName)
        {
            // Get the skills.
            List<SkillDetail> skills = new AdminBLManager().GetRelatedCandidateSelfSkills(skillName);

            if (skills == null || skills.Count == 0)
                ApproveCandidateSelfSkill_changeSkillButton.Visible = false;

            ApproveCandidateSelfSkill_skillGridView.DataSource = skills;
            ApproveCandidateSelfSkill_skillGridView.DataBind();

        }

        /// <summary>
        /// Method to display the proposed skill name
        /// </summary>
        private void DisplaySkillName()
        {
            // Get the skill name.
            string skillName = string.Empty;
            skillName = new AdminBLManager().
                GetCandidateSelfSkillBySkillId(Convert.ToInt32(ViewState["SKILL_ID"]));
            ApproveCandidateSelfSkill_newSkillNameTextBox.Text = skillName;
        }
        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input control and grid
        /// </summary>
        protected override void LoadValues()
        {
            // Clear messages.
            ApproveCandidateSelfSkill_errorMessageLabel.Text = string.Empty;
            ApproveCandidateSelfSkill_successMessageLabel.Text = string.Empty;

            //Display skill name in text box
            DisplaySkillName();

            // Load related skills
            LoadRelatedSkills(ApproveCandidateSelfSkill_newSkillNameTextBox.Text.ToString().Trim());
        }

        #endregion Protected Overridden Methods

        #region Protected Methods
        /// <summary>
        /// Method to copy the new skill to skill table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ApproveCandidateSelfSkill_addNewSkillButton_Click(object sender, EventArgs e)
        {
            try
            {
                new AdminBLManager().InsertSkillFromCandidateSelfSkill(Convert.ToInt32(ViewState["SKILL_ID"]),
                    ApproveCandidateSelfSkill_newSkillNameTextBox.Text.ToString().Trim(), base.userID);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowCandidateSelfSkillForm", "<script language='javascript'>OnClick();</script>", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ApproveCandidateSelfSkill_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler to change the skill in candidate self skill
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ApproveCandidateSelfSkill_changeSkillButton_Click(object sender, EventArgs e)
        {
            try
            {
                string skillName = string.Empty;
                int skillID = 0;
                bool skillSelected = false;
                foreach (GridViewRow gv in ApproveCandidateSelfSkill_skillGridView.Rows)
                {
                    RadioButton ApproveCandidateSelfSkill_skillRadioButton = gv.FindControl("ApproveCandidateSelfSkill_skillRadioButton") as RadioButton;
                    if (ApproveCandidateSelfSkill_skillRadioButton.Checked)
                    {
                        HiddenField ApproveCandidateSelfSkill_skillIDHiddenField = gv.FindControl("ApproveCandidateSelfSkill_skillIDHiddenField") as HiddenField;
                        skillName = gv.Cells[1].Text.ToUpper().Trim();
                        skillID = Convert.ToInt32(ApproveCandidateSelfSkill_skillIDHiddenField.Value);
                        skillSelected = true;
                        break;
                    }
                }
                if (skillSelected)
                {
                    new AdminBLManager().UpdateCandidateSelfSkill(Convert.ToInt32(ViewState["SKILL_ID"]),skillID,
                        skillName, base.userID);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowCandidateSelfSkillForm", "<script language='javascript'>OnClick();</script>", false);
                }
                else
                {
                    ShowMessage(ApproveCandidateSelfSkill_errorMessageLabel,"Please select skill");
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ApproveCandidateSelfSkill_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler to delete the candidate self skill
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ApproveCandidateSelfSkill_rejectSkillButton_Click(object sender, EventArgs e)
        {
            try
            {
                IDbTransaction transaction = new TransactionManager().Transaction;
                int SkillID = new CandidateBLManager().DeleteCandidateSkill(Convert.ToInt32(Request.QueryString["candidateId"].ToString()),
                           Convert.ToInt32(ViewState["SKILL_ID"]), transaction);
                transaction.Commit();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowCandidateSelfSkillForm", "<script language='javascript'>OnClick();</script>", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ApproveCandidateSelfSkill_errorMessageLabel, exp.Message);
            }
        }
        #endregion Protected Methods
    }
}
