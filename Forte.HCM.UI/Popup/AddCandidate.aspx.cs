﻿using System;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.ResumeParser.ResumeValidator;
using Resources;
using System.Collections.Generic;

namespace Forte.HCM.UI.Popup
{
    public partial class AddCandidate : PageBase
    {
        #region Event Handler

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("New Candidate");

                AddCandidate_firstNameTextBox.Focus();

                Page.Form.DefaultButton = AddCandidate_bottomSaveButton.UniqueID;

                AddCandidate_errorMessageLabel.Text = string.Empty;
                AddCandidate_successMessageLabel.Text = string.Empty;

                AddCandidate_workAuthorizationStatusDropDownList.Attributes.Add("onchange",
                    "javascript:ShowOtherTextBox('" + AddCandidate_workAuthorizationStatusDropDownList.ClientID + "','" +
                    AddCandidate_workAuthorizationStatus_otherDIV.ClientID +
                    "','" + AddCandidate_workAuthorizationStatus_otherTextBox.ClientID + "');");

                if (!IsPostBack)
                {
                    // Clear candidate photo from the session.
                    Session["CANDIDATE_THUMBNAIL_PHOTO_POPUP"] = null;
                    Session["PHOTO_CHANGED_POPUP"] = false;

                    // Assign data source to work authorization.
                    AddCandidate_workAuthorizationStatusDropDownList.DataSource =
                    new AttributeBLManager().GetAttributesByType("WA_STATUS ", "V");
                    AddCandidate_workAuthorizationStatusDropDownList.DataTextField = "AttributeName";
                    AddCandidate_workAuthorizationStatusDropDownList.DataValueField = "AttributeID";
                    AddCandidate_workAuthorizationStatusDropDownList.DataBind();
                    AddCandidate_workAuthorizationStatusDropDownList.Items.Insert(0, "--Select--");
                }

                // Show the other div based on the authorization status type selected.
                if (AddCandidate_workAuthorizationStatusDropDownList.SelectedValue != null &&
                    AddCandidate_workAuthorizationStatusDropDownList.SelectedValue == "WAS_OTHER")
                    AddCandidate_workAuthorizationStatus_otherDIV.Style.Add("display", "block");
                else
                    AddCandidate_workAuthorizationStatus_otherDIV.Style.Add("display", "none");

                
                // Set the visibility of the 'clear' photo link.
                AddCandidate_selectPhotoClearLinkButton.Visible =
                    !(Session["CANDIDATE_THUMBNAIL_PHOTO_POPUP"] == null);

                AddCandidate_locationImageButton.Attributes.Add("onclick",
                    "return SearchLocation('" + AddCandidate_cityIDHiddenField.ClientID + "','" +
                     AddCandidate_stateIDHiddenField.ClientID + "','" +
                     AddCandidate_countryIDHiddenField.ClientID + "','" +
                     AddCandidate_LocationTextBox.ClientID + "')");
            }
            catch (Exception exception)
            {
                base.ShowMessage(AddCandidate_errorMessageLabel,
                    exception.Message);

                Logger.ExceptionLog(exception);
            }
        }
        /// <summary>
        /// Handles the Click event of the AddCandidate_saveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void AddCandidate_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                {
                    base.ShowMessage(AddCandidate_errorMessageLabel, HCMResource.NewCandidate_EnterRequiredFields);

                    return;
                }

                if (!IsValidEmailAddress(AddCandidate_emailTextBox.Text.Trim()))
                {
                    base.ShowMessage(AddCandidate_errorMessageLabel, HCMResource.NewCandidate_EnterValidEmailID);

                    return;
                }
                if (AddCandidate_createUserNamePanel_passwordTextBox.Text.Trim() !=
                    AddCandidate_createUserNamePanel_retypePasswordTextBox.Text.Trim())
                {
                    base.ShowMessage(AddCandidate_errorMessageLabel,
                       HCMResource.SearchCandidateRepository_EnterCorrectPassword);
                    return;
                }

                if (!CheckForEmailAddressAvailability
                    (AddCandidate_createUserNamePanel_userNameTextBox.Text.Trim(), base.tenantID))
                {
                    base.ShowMessage(AddCandidate_errorMessageLabel,
                    HCMResource.UserRegistration_UserEmailNotAvailable);
                    AddCandidate_createUserNamePanel_userEmailAvailableStatusDiv.Style.Add
                        ("display", "block");

                    return;
                }

                //if (!IsValidEmailAddress(AddCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
                //{
                //    base.ShowMessage(AddCandidate_errorMessageLabel, HCMResource.NewCandidate_EnterValidEmailID);

                //    return;
                //}

                CandidateInformation candidateInformation = new CandidateInformation();

                // Get candidate information.
                candidateInformation.caiFirstName = AddCandidate_firstNameTextBox.Text.Trim();
                candidateInformation.caiMiddleName = AddCandidate_middleNameTextBox.Text.Trim();
                candidateInformation.caiLastName = AddCandidate_lastNameTextBox.Text.Trim();
                candidateInformation.caiEmail = AddCandidate_emailTextBox.Text.Trim();
                candidateInformation.caiGender = int.Parse(AddCandidate_genderDropDownList.SelectedValue);
                candidateInformation.caiIsActive = AddCandidate_IsactiveCheckbox.Checked ? 'Y' : 'N';

                if (!Utility.IsNullOrEmpty(AddCandidate_cityIDHiddenField.Value))
                    candidateInformation.caiCity = Convert.ToInt32(AddCandidate_cityIDHiddenField.Value);

                if (!Utility.IsNullOrEmpty(AddCandidate_stateIDHiddenField.Value))
                    candidateInformation.staID = Convert.ToInt32(AddCandidate_stateIDHiddenField.Value);

                if (!Utility.IsNullOrEmpty(AddCandidate_countryIDHiddenField.Value))
                    candidateInformation.couID = Convert.ToInt32(AddCandidate_countryIDHiddenField.Value);

                if (AddCandidate_dateOfBirthTextBox.Text.Trim().Length != 0)
                {
                    candidateInformation.caiDOB = Convert.ToDateTime(AddCandidate_dateOfBirthTextBox.Text.Trim());
                }
                else
                {
                    candidateInformation.caiDOB = DateTime.MinValue;
                }

                candidateInformation.caiAddress = AddCandidate_addressTextBox.Text;

                if (AddCandidate_workAuthorizationStatusDropDownList.SelectedIndex != 0)
                {
                    candidateInformation.caiWorkAuthorization = AddCandidate_workAuthorizationStatusDropDownList.SelectedValue;
                    candidateInformation.caiWorkAuthorizationOther = AddCandidate_workAuthorizationStatus_otherTextBox.Text.Trim();
                }

                candidateInformation.caiHomePhone = AddCandidate_homephoneTextBox.Text;
                candidateInformation.caiCell = AddCandidate_cellPhoneTextBox.Text;
                candidateInformation.caiType = "CT_MANUAL";
                candidateInformation.caiEmployee = AddCandidate_candidateRadioButton.Checked ? 'N' : 'Y';
                candidateInformation.caiLimitedAccess = AddCandidate_limitedAccessCheckBox.Checked;
                candidateInformation.UserName = AddCandidate_createUserNamePanel_userNameTextBox.Text.Trim();
                candidateInformation.Password = new EncryptAndDecrypt().EncryptString(AddCandidate_createUserNamePanel_passwordTextBox.Text.Trim());

                
                //Get validator instance
                IResumeValidator validator = Validator.GetInstance();
                List<DuplicateCandidateDetail> duplicateCandidates = null;

                //Check for the duplicated candidate and get the list
                bool isDuplicate = validator.CheckDuplicate(tenantID, AddCandidate_firstNameTextBox.Text.Trim(),
                    AddCandidate_lastNameTextBox.Text.ToString().Trim(), AddCandidate_emailTextBox.Text.Trim(),
                    out duplicateCandidates);

                ViewState["CANDIDATE_INFORMATION_POPUP"] = null;
                ViewState["CANDIDATE_INFORMATION_POPUP"] = candidateInformation;

                if (isDuplicate)
                {
                    if (duplicateCandidates != null && duplicateCandidates.Count > 0)
                    {
                        AddCandidate_candidateFirstNameLabelValue.Text = candidateInformation.caiFirstName;
                        AddCandidate_candidateLastNameLabelValue.Text = candidateInformation.caiLastName;
                        AddCandidate_candidateEmailLabelValue.Text = candidateInformation.caiEmail;
                        AddCandidate_candidateDetailGridView.DataSource = duplicateCandidates;
                        AddCandidate_candidateDetailGridView.DataBind();
                        AddCandidate_scheduleModalPopupExtender.Show();
                        return;
                    }
                }
                SaveCandidateInformation();
            }
            catch (Exception exception)
            {
                base.ShowMessage(AddCandidate_errorMessageLabel,
                    exception.Message);

                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Handles the Click event of the AddCandidate_newCandidateButton_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void AddCandidate_newCandidateButton_Click(object sender, EventArgs e)
        {
            try
            {
                SaveCandidateInformation();
            }
            catch (Exception exception)
            {
                base.ShowMessage(AddCandidate_errorMessageLabel,exception.Message);

                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Handles the Click event of the AddCandidate_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void AddCandidate_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                base.ShowMessage(AddCandidate_errorMessageLabel,
                    exception.Message);

                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the candidate grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void AddCandidate_candidateDetailGridView_RowDataBound
            (object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != System.Web.UI.WebControls.DataControlRowType.DataRow)
                    return;

                // Set row styles.
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                base.ShowMessage(AddCandidate_errorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler to check for the email availability
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddCandidate_createUserNamePanel_checkUserEmailIdAvailableButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(AddCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
                {
                    //if (!IsValidEmailAddress(AddCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
                    //{
                    //    base.ShowMessage(AddCandidate_errorMessageLabel,
                    //            HCMResource.NewCandidate_EnterValidEmailID);
                    //    return;
                    //}

                    if (CheckForEmailAddressAvailability(AddCandidate_createUserNamePanel_userNameTextBox.Text.Trim(), base.tenantID))
                        AddCandidate_createUserNamePanel_validEmailAvailableStatus.Text = Resources.HCMResource.UserRegistration_UserEmailAvailable;
                    else
                        AddCandidate_createUserNamePanel_inValidEmailAvailableStatus.Text = HCMResource.UserRegistration_UserEmailNotAvailable;
                    AddCandidate_createUserNamePanel_userEmailAvailableStatusDiv.Style.Add("display", "block");
                }
                else
                {
                    base.ShowMessage(AddCandidate_errorMessageLabel,
                                HCMResource.SearchCandidateRepository_PleaseEnterTheUsername);
                    return;
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(AddCandidate_errorMessageLabel,
                     exception.Message);
            }
        }

        /// <summary>
        /// Method that is called when the clear link is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will clear the selected photo.
        /// </remarks>
        protected void AddCandidate_selectPhotoClearLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear candidate photo from the session.
                Session["CANDIDATE_THUMBNAIL_PHOTO_POPUP"] = null;
                Session["PHOTO_CHANGED_POPUP"] = true;

                // Set the visibility of the 'clear' photo link.
                AddCandidate_selectPhotoClearLinkButton.Visible = false;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                base.ShowMessage(AddCandidate_errorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler will be called when the browse button is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="AjaxControlToolkit.AsyncFileUploadEventArgs"/> holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply to upload the file to server.
        /// </remarks>
        protected void AddCandidate_resumeUploadedComplete(object sender,
            AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                if (AddCandidate_resumeAsyncFileUpload.HasFile)
                {
                    // Validation for file extension
                    if (((!Path.GetExtension(e.FileName).ToLower().Contains(".doc")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".docx")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".pdf")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".rtf"))))
                    {
                        ShowMessage(AddCandidate_errorMessageLabel,
                            "Please select valid file format.(.doc,.docx,.pdf and rtf)");
                        return;
                    }

                    string filePath = ConfigurationManager.AppSettings["UPLOADED_RESUMES_PATH"].ToString();
                    string saveAsPath = string.Empty;
                    Session["UPLOADED_RESUME_NAME_POPUP"] = string.Empty;

                    saveAsPath = filePath + "\\" + e.FileName;
                    AddCandidate_resumeAsyncFileUpload.SaveAs(saveAsPath);
                    Session["UPLOADED_RESUME_NAME_POPUP"] = e.FileName;
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(AddCandidate_errorMessageLabel,
                    ex.Message);
            }
        }

        /// <summary>
        /// Handler will be called when the browse button is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="AjaxControlToolkit.AsyncFileUploadEventArgs"/> holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply to upload the file to server.
        /// </remarks>
        protected void AddCandidate_photoUploadedComplete(object sender,
            AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                if (AddCandidate_photoAsyncFileUpload.HasFile)
                {
                    // Validation for file extension
                    if (((!Path.GetExtension(e.FileName).ToLower().Contains(".jpg")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".gif")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".png")) &&
                        (!Path.GetExtension(e.FileName).ToLower().Contains(".jpeg"))))
                    {
                        ShowMessage(AddCandidate_errorMessageLabel,
                            "Only gif/png/jpg/jpeg files are allowed)");
                        return;
                    }

                    // Check if photo size exceeds the maximum limit.
                    int maxFileSize = Convert.ToInt32
                        (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_MAX_SIZE_IN_BYTES"]);

                    if (AddCandidate_photoAsyncFileUpload.FileBytes.Length > maxFileSize)
                    {
                        base.ShowMessage(AddCandidate_errorMessageLabel,
                           string.Format("File size exceeds the maximum limit of {0} bytes", maxFileSize));

                        return;
                    }

                    // Retrieve the original photo from the file upload control.
                    System.Drawing.Image originalPhoto = System.Drawing.Image.FromStream
                        (new MemoryStream(AddCandidate_photoAsyncFileUpload.FileBytes));

                    int thumbnailWidth = Convert.ToInt32
                        (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_THUMBNAIL_WIDTH"]);
                    int thumbnailHeight = Convert.ToInt32
                        (ConfigurationManager.AppSettings["CANDIDATE_PHOTO_THUMBNAIL_HEIGHT"]);

                    // Check the width and height of the original image exceeds the
                    // size of the standard size. If exceeds convert the image to
                    // thumbnail of the standard size and store.
                    if (originalPhoto.Width > thumbnailWidth && originalPhoto.Height > thumbnailHeight)
                    {
                        // Resize both width and height.
                        System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                            (thumbnailWidth, thumbnailHeight, null, System.IntPtr.Zero);

                        // Keep the thumbnail photo in the session.
                        Session["CANDIDATE_THUMBNAIL_PHOTO_POPUP"] = thumbnailPhoto;
                    }
                    else if (originalPhoto.Width > thumbnailWidth)
                    {
                        // Resize only the width.
                        System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                            (thumbnailWidth, originalPhoto.Height, null, System.IntPtr.Zero);

                        // Keep the thumbnail photo in the session.
                        Session["CANDIDATE_THUMBNAIL_PHOTO_POPUP"] = thumbnailPhoto;
                    }
                    else if (originalPhoto.Height > thumbnailHeight)
                    {
                        // Resize only the height.
                        System.Drawing.Image thumbnailPhoto = originalPhoto.GetThumbnailImage
                            (originalPhoto.Width, thumbnailHeight, null, System.IntPtr.Zero);

                        // Keep the thumbnail photo in the session.
                        Session["CANDIDATE_THUMBNAIL_PHOTO_POPUP"] = thumbnailPhoto;
                    }
                    else
                    {
                        // Keep the original photo in the session.
                        Session["CANDIDATE_THUMBNAIL_PHOTO_POPUP"] = originalPhoto;
                    }

                    // Set the phto changed status.
                    Session["PHOTO_CHANGED_POPUP"] = true;

                    // Set the visibility of the 'clear' photo link.
                    AddCandidate_selectPhotoClearLinkButton.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(AddCandidate_errorMessageLabel,
                    ex.Message);
            }
        }
        #endregion Event Handler

        #region Private Methods

        /// <summary>
        /// Method to save the candidate information
        /// </summary>
        private void SaveCandidateInformation()
        {
            CandidateInformation candidateInformation = null;
            if (ViewState["CANDIDATE_INFORMATION_POPUP"] != null)
            {
                candidateInformation = ViewState["CANDIDATE_INFORMATION_POPUP"] as CandidateInformation;
                // Save candidate.
                int candidateID = new ResumeRepositoryBLManager().InsertNewCandidate
                    (candidateInformation, base.userID, base.tenantID);

                candidateInformation.caiID = candidateID;

                // Show a success message.
                base.ShowMessage(AddCandidate_successMessageLabel,
                    HCMResource.NewCandidate_CandidateInsertedSuccessfully);

                //Save candidate resume
                SaveCandidateResume(candidateInformation.caiID + "_" + candidateInformation.caiFirstName);

                //Save candidate Photo
                SaveCandidatePhoto(candidateInformation.caiID);

                // Save user.
                SaveUserName(candidateInformation);

                //Clear the view state candidate information
                ViewState["CANDIDATE_INFORMATION_POPUP"] = null;

                // Close and assign the candidate.
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "AddCandidate",
                    "javascript: OnCandidateCreation('" + AddCandidate_bottomSaveButton.ClientID + "')", true);
            }
        }

        /// <summary>
        /// Saves the name of the user.
        /// </summary>
        private void SaveUserName(CandidateInformation candidateInformation)
        {
            string confirmationCode = GetConfirmationCode();
            AddCandidate_userIDHiddenfield.Value = Convert.ToString(new ResumeRepositoryBLManager().
                InsertCandidateUsers(base.tenantID, candidateInformation,
                base.userID, confirmationCode));
        }

        /// <summary>
        /// A Method that generates a random string and used 
        /// the encrypted string as confirmation code for a user
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that holds the encrypted string
        /// </returns>
        private string GetConfirmationCode()
        {
            return new EncryptAndDecrypt().EncryptString(RandomString(15));
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }

        /// <summary>
        /// Method to check for the email availability
        /// </summary>
        /// <param name="User_Email"></param>
        /// <returns></returns>
        private bool CheckForEmailAddressAvailability(string usrEmail, int tenantID)
        {
            return new UserRegistrationBLManager().CheckUserEmailExists(usrEmail, tenantID);
        }

        /// <summary>
        /// Method to check whether given email id is valid or not
        /// </summary>
        /// <param name="strUserEmailId"></param>
        /// <returns></returns>
        private bool IsValidEmailAddress(string strUserEmailId)
        {
            //Regex regex = new Regex(".+@.+\\.[a-zA-Z]+", RegexOptions.Compiled);
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$", RegexOptions.Compiled);
            return regex.IsMatch(strUserEmailId);
        }

        /// <summary>
        /// Method to save the candidate photo 
        /// </summary>
        /// <param name="candidateID"></param>
        private void SaveCandidatePhoto(int candidateID)
        {
            // Save candidate photo.
            if (!Utility.IsNullOrEmpty(Session["PHOTO_CHANGED_POPUP"]) &&
                Convert.ToBoolean(Session["PHOTO_CHANGED_POPUP"]) == true)
            {
                System.Drawing.Image thumbnail = null;
                if (Session["CANDIDATE_THUMBNAIL_PHOTO_POPUP"] != null)
                {
                    thumbnail = Session["CANDIDATE_THUMBNAIL_PHOTO_POPUP"] as
                        System.Drawing.Image;
                }

                // Save the thumbnail image into server path.
                string filePath = Server.MapPath("~/") +
                    ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                    "\\" + candidateID + "." +
                    ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];

                // Check if the file exist. If exist, then remove it.
                if (File.Exists(filePath))
                    File.Delete(filePath);

                thumbnail.Save(filePath, base.GetCandidatePhotoFormat());

                // Reset the status.
                Session["PHOTO_CHANGED_POPUP"] = false;

                // Clear photo
                Session["CANDIDATE_THUMBNAIL_PHOTO_POPUP"] = null;
            }
        }

        /// <summary>
        /// Method to save the candidate resume details
        /// </summary>
        private void SaveCandidateResume(string fileNamePrefix)
        {
            #region save resume
            string serverPath = ConfigurationManager.AppSettings["UPLOADED_RESUMES_PATH"].ToString();
            string newFilePath = string.Empty;
            string oldFilePath = string.Empty;
            string getExtension = string.Empty;

            if (!Utility.IsNullOrEmpty(Session["UPLOADED_RESUME_NAME_POPUP"]))
            {
                //To get the file extension
                getExtension = Path.GetExtension(Session["UPLOADED_RESUME_NAME_POPUP"].ToString()).ToLower();

                //Old file path
                oldFilePath = serverPath + "\\" + Session["UPLOADED_RESUME_NAME_POPUP"].ToString();
                //To save word file as 
                newFilePath = serverPath + "\\" + fileNamePrefix + getExtension;

                //Check if file exists already then delete it
                if (File.Exists(newFilePath) == true)
                    File.Delete(newFilePath);

                //Copy file now
                File.Copy(oldFilePath, newFilePath);

                //Delete the temp file
                File.Delete(oldFilePath);
                Session["UPLOADED_RESUME_NAME_POPUP"] = string.Empty;
            }
            #endregion save resume
        }

        #endregion Private Methods

        #region Protected Override Methods
        protected override bool IsValidData()
        {
            bool value = true;

            if (AddCandidate_firstNameTextBox.Text.Trim().Length == 0)
            {
                value = false;
            }
            if (AddCandidate_lastNameTextBox.Text.Trim().Length == 0)
            {
                value = false;
            }
            if (AddCandidate_emailTextBox.Text.Trim().Length == 0)
            {
                value = false;
            }
            if (AddCandidate_workAuthorizationStatusDropDownList.SelectedValue != null &&
                    AddCandidate_workAuthorizationStatusDropDownList.SelectedValue == "WAS_OTHER")
            {
                if (AddCandidate_workAuthorizationStatus_otherTextBox.Text.Trim().Length == 0)
                    value = false;
            }
            if (string.IsNullOrEmpty(AddCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
            {
                value = false;
            }
            if (string.IsNullOrEmpty(AddCandidate_createUserNamePanel_passwordTextBox.Text.Trim()))
            {
                value = false;
            }
            if (string.IsNullOrEmpty(AddCandidate_createUserNamePanel_retypePasswordTextBox.Text.Trim()))
            {
                value = false;
            }
            return value;
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion Protected Override Methods
    }
}