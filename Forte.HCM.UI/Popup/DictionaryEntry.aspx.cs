﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.Popup
{
    public partial class DictionaryEntry : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set default button to 'add' button.
            Page.Form.DefaultButton = DictionaryEntry_addEntryButton.UniqueID;

            if (!IsPostBack)
            {
                LoadValues();
            }
        }

        protected void DictionaryEntry_addEntryButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Dictionary Entry");
                // Clear message.
                DictionaryEntry_errorMessageLabel.Text = string.Empty;
                DictionaryEntry_successMessageLabel.Text = string.Empty;

                // Check if the dictionary type is selected.
                if (Utility.IsNullOrEmpty(DictionaryEntry_dictionaryTypeDropDownList.SelectedValue))
                {
                    ShowMessage(DictionaryEntry_errorMessageLabel, "No dictionary type was selected");
                    DictionaryEntry_dictionaryTypeDropDownList.Focus();
                    return;
                }

                // Check if the head type is selected.
                if (Utility.IsNullOrEmpty(DictionaryEntry_headTypeDropDownList.SelectedValue))
                {
                    ShowMessage(DictionaryEntry_errorMessageLabel, "No head type was selected");
                    DictionaryEntry_headTypeDropDownList.Focus();
                    return;
                }

                // Check if the alias name is entered.
                if (DictionaryEntry_addAliasTextBox.Text.Trim().Length == 0)
                {
                    ShowMessage(DictionaryEntry_errorMessageLabel, "Alias name cannot be empty");
                    DictionaryEntry_addAliasTextBox.Focus();
                    return;
                }

                // Check if the alias name already exist.
                if (ViewState["ALIAS_LIST"] != null)
                {
                    if ((ViewState["ALIAS_LIST"] as List<AliasDetail>).Find(item => item.Name.ToUpper().
                        Trim() == DictionaryEntry_addAliasTextBox.Text.Trim().ToUpper()) != null)
                    {
                        ShowMessage(DictionaryEntry_errorMessageLabel, "Alias name already exist");
                        return;
                    }
                }

                // Check if the dictionary type is node or attribute.
                bool isAttribute = false;
                if (DictionaryEntry_dictionaryTypeDropDownList.SelectedValue.Trim() == "2")
                    isAttribute = true;

                // Retrieve the element ID.
                int elementID = Convert.ToInt32(DictionaryEntry_headTypeDropDownList.
                    SelectedValue.Trim());

                // Construct alias detail.
                AliasDetail aliasDetail = new AliasDetail();
                aliasDetail.ElementID = elementID;
                aliasDetail.Name = DictionaryEntry_addAliasTextBox.Text.Trim();

                if (base.userID == 0)
                {
                    ShowMessage(DictionaryEntry_editEntryPanel_errorMessageLabel,
                        "Your session was expired");
                    return;
                }

                aliasDetail.CreatedBy = base.userID;

                if (isAttribute)
                    new ResumeRepositoryBLManager().InsertAttributeDictionary(aliasDetail);
                else
                    new ResumeRepositoryBLManager().InsertTitleDictionary(aliasDetail);

                // Clear text boxes.
                DictionaryEntry_addAliasTextBox.Text = string.Empty;

                // Refresh the grid data.
                ShowAliases();

                // Show success message.
                ShowMessage(DictionaryEntry_successMessageLabel, "Entry added successfully");

                // Set the focus to add alias text box.
                DictionaryEntry_addAliasTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DictionaryEntry_errorMessageLabel, exp.Message);
            }
        }

        protected void DictionaryEntry_resetButton_Click
            (object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void DictionaryEntry_aliasesGridView_RowCommand(object sender,
            GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandSource is ImageButton))
                    return;

                // Clear message.
                DictionaryEntry_errorMessageLabel.Text = string.Empty;
                DictionaryEntry_successMessageLabel.Text = string.Empty;

                // Update the message update panel.
                DictionaryEntry_messageUpdatePanel.Update();

                // Keep the selected values in view state.
                ViewState["SELECTED_ID"] = (((ImageButton)e.CommandSource).Parent.
                    FindControl("DictionaryEntry_aliasesGridView_idHiddenField") as HiddenField).Value;
                ViewState["SELECTED_ALIAS"] = (((ImageButton)e.CommandSource).Parent.
                     FindControl("DictionaryEntry_aliasesGridView_aliasNameLabel") as Label).Text;

                if (e.CommandName == "EditEntry")
                {
                    // Clear any previous messages.
                    DictionaryEntry_editEntryPanel_errorMessageLabel.Text = string.Empty;

                    // Assign values and show the edit entry panel.
                    DictionaryEntry_editEntryPanel_aliasTextBox.Text =
                        (ViewState["SELECTED_ALIAS"] == null ? string.Empty : ViewState["SELECTED_ALIAS"].ToString());
                    DictionaryEntry_editEntryPanel_aliasTextBox.Focus();
                    DictionaryEntry_editEntryPanelModalPopupExtender.Show();
                    DictionaryEntry_editEntryUpdatePanel.Update();
                }
                else if (e.CommandName == "DeleteEntry")
                {
                    // Set message, title and type for the confirmation control for edit entry.
                    DictionaryEntry_deleteEntryPanel_confirmMessageControl.Message =
                        string.Format("Are you sure to delete the dictionary entry '{0}'?", ViewState["SELECTED_ALIAS"]);
                    DictionaryEntry_deleteEntryPanel_confirmMessageControl.Title = 
                        "Delete Dictionary Entry";
                    DictionaryEntry_deleteEntryPanel_confirmMessageControl.Type = 
                        MessageBoxType.YesNo;

                    DictionaryEntry_deleteEntryPanel_confirmModalPopupExtender.Show();
                    DictionaryEntry_deleteEntryUpdatePanel.Update();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DictionaryEntry_errorMessageLabel, exp.Message);
            }
        }

        protected void DictionaryEntry_editEntryPanel_updateButton_Click(object sender, EventArgs e)
        {
            // Clear any previous messages in the edit entry panel.
            DictionaryEntry_editEntryPanel_errorMessageLabel.Text = string.Empty;
     
            // Check if the alias name is entered.
            if (DictionaryEntry_editEntryPanel_aliasTextBox.Text.Trim().Length == 0)
            {
                ShowMessage(DictionaryEntry_editEntryPanel_errorMessageLabel, 
                    "Alias name cannot be empty");

                // Reshow the edit entry panel.
                DictionaryEntry_editEntryPanelModalPopupExtender.Show();
                DictionaryEntry_editEntryUpdatePanel.Update();

                return;
            }

            // Check if the alias name already exist.
            if (ViewState["ALIAS_LIST"] != null)
            {
                if ((ViewState["ALIAS_LIST"] as List<AliasDetail>).Find(item => item.Name.ToUpper().
                    Trim() == DictionaryEntry_editEntryPanel_aliasTextBox.Text.Trim().ToUpper() && 
                    item.ID != Convert.ToInt32(ViewState["SELECTED_ID"])) != null)
                {
                    ShowMessage(DictionaryEntry_editEntryPanel_errorMessageLabel, 
                        "Alias name already exist");

                    // Reshow the edit entry panel.
                    DictionaryEntry_editEntryPanelModalPopupExtender.Show();
                    DictionaryEntry_editEntryUpdatePanel.Update();

                    return;
                }
            }

            // Check if the dictionary type is node or attribute.
            bool isAttribute = false;
            if (DictionaryEntry_dictionaryTypeDropDownList.SelectedValue.Trim() == "2")
                isAttribute = true;

            // Construct alias detail.
            AliasDetail aliasDetail = new AliasDetail();
            aliasDetail.ID = Convert.ToInt32(ViewState["SELECTED_ID"]);
            aliasDetail.Name = DictionaryEntry_editEntryPanel_aliasTextBox.Text.Trim();

            if (base.userID == 0)
            {
                ShowMessage(DictionaryEntry_editEntryPanel_errorMessageLabel,
                    "Your session was expired");
                return;
            }
            aliasDetail.ModifiedBy = base.userID;

            if (isAttribute)
                new ResumeRepositoryBLManager().UpdateAttributeDictionary(aliasDetail);
            else
                new ResumeRepositoryBLManager().UpdateTitleDictionary(aliasDetail);

            // Reload aliases.
            ShowAliases();

            // Show success message.
            ShowMessage(DictionaryEntry_successMessageLabel, "Dictionary entry updated successfully");

            // Update the message & grid view update panel.
            DictionaryEntry_messageUpdatePanel.Update();
            DictionaryEntry_searchResultsUpdatePanel.Update();

            // Set default button to 'add' button.
            Page.Form.DefaultButton = DictionaryEntry_addEntryButton.UniqueID;
        }

        protected void DictionaryEntry_dictionaryTypeDropDownList_SelectedIndexChanged
            (object sender, EventArgs e)
        {
            try
            {
                // Clear message.
                DictionaryEntry_errorMessageLabel.Text = string.Empty;
                DictionaryEntry_successMessageLabel.Text = string.Empty;

                // Clear aliases grid.
                DictionaryEntry_aliasesGridView.DataSource = null;
                DictionaryEntry_aliasesGridView.DataBind();

                // Clear alias list.
                ViewState["ALIAS_LIST"] = null;

                if (Utility.IsNullOrEmpty(DictionaryEntry_dictionaryTypeDropDownList.SelectedValue))
                {
                    DictionaryEntry_headTypeDropDownList.Items.Clear();

                    // Insert the '--Select--' item at 0 index.
                    DictionaryEntry_headTypeDropDownList.Items.Insert(0, new ListItem("--Select--", ""));
                    return;
                }

                DictionaryEntry_headTypeDropDownList.DataSource = new
                    ResumeRepositoryBLManager().GetElements(Convert.ToInt32
                    (DictionaryEntry_dictionaryTypeDropDownList.SelectedValue));

                DictionaryEntry_headTypeDropDownList.DataTextField = "ElementName";
                DictionaryEntry_headTypeDropDownList.DataValueField = "ElementID";
                DictionaryEntry_headTypeDropDownList.DataBind();

                // Insert the '--Select--' item at 0 index.
                DictionaryEntry_headTypeDropDownList.Items.Insert(0, new ListItem("--Select--", ""));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DictionaryEntry_errorMessageLabel, exp.Message);
            }
        }

        protected void DictionaryEntry_headTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Clear message.
                DictionaryEntry_errorMessageLabel.Text = string.Empty;
                DictionaryEntry_successMessageLabel.Text = string.Empty;

                // Show list of aliases.
                ShowAliases();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DictionaryEntry_errorMessageLabel, exp.Message);
            }
        }

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            List<ElementTypeDetail> elementTypes = new ResumeRepositoryBLManager().
                GetElementTypes();

            if (elementTypes == null || elementTypes.Count == 0)
                return;

            // Load drop down items for dictionary type (add alias).
            DictionaryEntry_dictionaryTypeDropDownList.DataSource = elementTypes;
            DictionaryEntry_dictionaryTypeDropDownList.DataTextField = "ElementTypeName";
            DictionaryEntry_dictionaryTypeDropDownList.DataValueField = "ElementTypeID";
            DictionaryEntry_dictionaryTypeDropDownList.DataBind();

            // Insert the '--Select--' item at 0 index for dictionary type.
            DictionaryEntry_dictionaryTypeDropDownList.Items.Insert
                (0, new ListItem("--Select--", ""));

            // Insert the '--Select--' item at 0 index for head type.
            DictionaryEntry_headTypeDropDownList.Items.Insert
                (0, new ListItem("--Select--", ""));
        }

        #endregion Protected Overridden Methods

        private void ShowAliases()
        {
            // Clear aliases grid.
            DictionaryEntry_aliasesGridView.DataSource = null;
            DictionaryEntry_aliasesGridView.DataBind();

            // Clear alias list.
            ViewState["ALIAS_LIST"] = null;

            if (Utility.IsNullOrEmpty(DictionaryEntry_headTypeDropDownList.SelectedValue))
            {
                return;
            }

            // Check if the dictionary type is node or attribute.
            bool isAttribute = false;
            if (DictionaryEntry_dictionaryTypeDropDownList.SelectedValue.Trim() == "2")
                isAttribute = true;

            // Retrieve the element ID.
            int elementID = Convert.ToInt32(DictionaryEntry_headTypeDropDownList.
                SelectedValue.Trim());

            List<AliasDetail> aliases = null;

            if (isAttribute)
                aliases = new ResumeRepositoryBLManager().GetAttributeDictionary(elementID);
            else
                aliases = new ResumeRepositoryBLManager().GetTitleDictionary(elementID);

            DictionaryEntry_aliasesGridView.DataSource = aliases;
            DictionaryEntry_aliasesDiv.DataBind();

            // Keep the alias list in view state.
            ViewState["ALIAS_LIST"] = aliases;

            if (aliases == null || aliases.Count == 0)
            {
                if (DictionaryEntry_successMessageLabel.Text.Trim().Length == 0)
                {
                    ShowMessage(DictionaryEntry_errorMessageLabel,
                        Resources.HCMResource.Common_Empty_Grid);
                }
                else
                {
                    ShowMessage(DictionaryEntry_errorMessageLabel, "<br/>" +
                        Resources.HCMResource.Common_Empty_Grid);
                }
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked in
        /// the delete entry confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will delete the entry from the database.
        /// </remarks>
        protected void DictionaryEntry_deleteEntryPanel_okButtonClick(object sender, EventArgs e)
        {
            try
            {
                // Clear message.
                DictionaryEntry_errorMessageLabel.Text = string.Empty;
                DictionaryEntry_successMessageLabel.Text = string.Empty;

                // Check if the dictionary type is node or attribute.
                bool isAttribute = false;
                if (DictionaryEntry_dictionaryTypeDropDownList.SelectedValue.Trim() == "2")
                    isAttribute = true;

                if (isAttribute)
                {
                    new ResumeRepositoryBLManager().DeleteAttributeDictionary
                        (Convert.ToInt32(ViewState["SELECTED_ID"]));
                }
                else
                {
                    new ResumeRepositoryBLManager().DeleteTitleDictionary
                        (Convert.ToInt32(ViewState["SELECTED_ID"]));
                }

                // Show success message.
                ShowMessage(DictionaryEntry_successMessageLabel, "Dictionary entry deleted successfully");

                // Reload aliases.
                ShowAliases();

                // Update the message & grid view update panel.
                DictionaryEntry_messageUpdatePanel.Update();
                DictionaryEntry_searchResultsUpdatePanel.Update();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DictionaryEntry_errorMessageLabel, exp.Message);
            }            
        }
    }
}