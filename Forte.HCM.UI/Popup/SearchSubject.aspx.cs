﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchSubject.aspx.cs
// File that represents the SearchSubject class that defines the user 
// interface layout and functionalities for the SearchSubject page. This 
// page helps in searching for categories and subject by providing search 
// criteria for category and subject name. This class inherits 
// Forte.HCM.UI.Common.PageBase

#endregion

#region Directives                                                             

using System;
using System.Linq;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the SearchSubject page. This page helps in searching for 
    /// categories and subject by providing search criteria for category and 
    /// subject name. This class inherits the Forte.HCM.UI.Common.PageBase 
    /// class.
    /// </summary>
    public partial class SearchSubject : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "226px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "280px";

        #endregion Private Constants

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Search Subject");
                // Set default button and focus.
                Page.Form.DefaultButton = SearchSubject_searchButton.UniqueID;

                if (!Utility.IsNullOrEmpty(SearchSubject_isMaximizedHiddenField.Value) &&
                        SearchSubject_isMaximizedHiddenField.Value == "Y")
                {
                    SearchSubject_searchCriteriasDiv.Style["display"] = "none";
                    SearchSubject_searchResultsUpSpan.Style["display"] = "block";
                    SearchSubject_searchResultsDownSpan.Style["display"] = "none";
                    SearchSubject_testDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchSubject_searchCriteriasDiv.Style["display"] = "block";
                    SearchSubject_searchResultsUpSpan.Style["display"] = "none";
                    SearchSubject_searchResultsDownSpan.Style["display"] = "block";
                    SearchSubject_testDiv.Style["height"] = RESTORED_HEIGHT;
                }

                SearchSubject_searchTestResultsTR.Attributes.Add("onclick",
                   "ExpandOrRestore('" +
                   SearchSubject_testDiv.ClientID + "','" +
                   SearchSubject_searchCriteriasDiv.ClientID + "','" +
                   SearchSubject_searchResultsUpSpan.ClientID + "','" +
                   SearchSubject_searchResultsDownSpan.ClientID + "','" +
                   SearchSubject_isMaximizedHiddenField.ClientID + "','" +
                   RESTORED_HEIGHT + "','" +
                   EXPANDED_HEIGHT + "')");

                //Subscribe page number click event 
                SearchSubject_pagingNavigator.PageNumberClick +=
                    new PageNavigator.PageNumberClickEventHandler
                        (SearchSubject_bottomPagingNavigator_PageNumberClick);

                if (!IsPostBack)
                {
                    
                    // Set default focus.
                    Page.Form.DefaultFocus = SearchSubject_categoryNameTextBox.UniqueID;
                    SearchSubject_categoryNameTextBox.Focus();

                    SearchSubject_testDiv.Visible = false;

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "CATEGORYNAME";
                }
                else
                {
                    SearchSubject_testDiv.Visible = true;
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(SearchSubject_errorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchSubject_bottomPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                //Load the grid with corresponding page number
                Search(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(SearchSubject_errorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchSubject_resultsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(SearchSubject_errorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchSubject_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                SearchSubject_errorMessageLabel.Text = string.Empty;
                SearchSubject_successMessageLabel.Text = string.Empty;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "CATEGORYNAME";

                // Reset the paging control.
                SearchSubject_pagingNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                Search(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(SearchSubject_errorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the bottom select button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void SearchSubject_selectButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                SearchSubject_errorMessageLabel.Text = string.Empty;
                SearchSubject_successMessageLabel.Text = string.Empty;

                // Check if the scenario is valid.
                if (!IsValidData())
                    return;

                AdminBLManager adminBLManager = new AdminBLManager();

                foreach (GridViewRow row in SearchSubject_resultsGridView.Rows)
                {
                    CheckBox checkBox = ((CheckBox)row.FindControl
                        ("SearchSubject_resultsGridViewSelectCheckbox"));

                    if (!checkBox.Checked)
                    {
                        continue;
                    }

                    HiddenField subjectNameHiddenField = ((HiddenField)row.FindControl
                        ("SearchSubject_resultsGridViewSubjectNameHiddenField"));
                    HiddenField subjectIDHiddenField = ((HiddenField)row.FindControl
                        ("SearchSubject_resultsGridViewSubjectIDHiddenField"));
                    HiddenField categoryIDHiddenField = ((HiddenField)row.FindControl
                        ("SearchSubject_resultsGridViewCategoryIDHiddenField"));

                    Subject subject = new Subject();

                    subject.CategoryID = int.Parse(Request.QueryString["categoryid"]);
                    subject.SubjectID = int.Parse(subjectIDHiddenField.Value);
                    subject.SubjectName = subjectNameHiddenField.Value.Trim();

                    subject.CreatedBy = userID;
                    subject.CreatedDate = DateTime.Now;
                    subject.ModifiedBy = userID;
                    subject.ModifiedDate = DateTime.Now;

                    // Insert the subject.
                    adminBLManager.InsertSubject(subject, base.tenantID);
                }
                
                string closeScript = "<script language ='javascript'>" + 
                    "{CloseSearchSubject('Y');}</script>";

                Page.ClientScript.RegisterStartupScript(this.GetType(), 
                    "CloseSearchSubject", closeScript);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(SearchSubject_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchSubject_resultsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                // Add the sort image to the header column
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchSubject_resultsGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(SearchSubject_errorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchSubject_resultsGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                SearchSubject_pagingNavigator.Reset();

                Search(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(SearchSubject_errorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void SearchSubject_resetLinkButton_Click(object sender, EventArgs e)
        {
            SearchSubject_categoryNameTextBox.Text = string.Empty;
            SearchSubject_subjectNameTextBox.Text = string.Empty;

            SearchSubject_resultsGridView.DataSource = null;
            SearchSubject_resultsGridView.DataBind();

            SearchSubject_pagingNavigator.PageSize = base.GridPageSize;
            SearchSubject_pagingNavigator.TotalRecords = 0;

            SearchSubject_testDiv.Visible = false;

            // Reset default sort field and order keys.
            ViewState["SORT_ORDER"] = SortType.Ascending;
            ViewState["SORT_FIELD"] = "CATEGORYNAME";

            // Reset to the restored state.
            SearchSubject_searchCriteriasDiv.Style["display"] = "block";
            SearchSubject_searchResultsUpSpan.Style["display"] = "none";
            SearchSubject_searchResultsDownSpan.Style["display"] = "block";
            SearchSubject_testDiv.Style["height"] = RESTORED_HEIGHT;
            SearchSubject_isMaximizedHiddenField.Value = "N";

            // Clear message fields.
            SearchSubject_successMessageLabel.Text = string.Empty;
            SearchSubject_errorMessageLabel.Text = string.Empty;

            // Set default focus.
            Page.Form.DefaultFocus = SearchSubject_categoryNameTextBox.UniqueID;
            SearchSubject_categoryNameTextBox.Focus();
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void Search(int pageNumber)
        {
            int totalRecords = 0;

            //List<Subject> subjects = new List<Subject>();

            ////Get the subject from the database
            //subjects = new CommonBLManager().GetCategorySubjects(pagenumber,
            //    SearchSubject_categoryIDTextBox.Text,
            //    SearchSubject_categoryNameTextBox.Text,
            //    SearchSubject_subjectIDTextBox.Text,
            //    SearchSubject_subjectNameTextBox.Text,
            //    base.GridPageSize, out totalRecords,
            //    ViewState["SORT_FIELD"].ToString(), 
            //    ((SortType)ViewState["SORT_ORDER"]));

            List<Subject> subjects = new CommonBLManager().GetCategorySubjects(pageNumber, base.tenantID,
                string.Empty, SearchSubject_categoryNameTextBox.Text.Trim(), string.Empty,
                SearchSubject_subjectNameTextBox.Text.Trim(), base.GridPageSize, out totalRecords,
                ViewState["SORT_FIELD"].ToString(), ((SortType)ViewState["SORT_ORDER"]), string.Empty);

            if (subjects == null || subjects.Count == 0)
            {
                SearchSubject_testDiv.Visible = false;
                ShowMessage(SearchSubject_errorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                SearchSubject_testDiv.Visible = true;
            }

            SearchSubject_resultsGridView.DataSource = subjects;
            SearchSubject_resultsGridView.DataBind();
            SearchSubject_pagingNavigator.PageSize = GridPageSize;
            SearchSubject_pagingNavigator.TotalRecords = totalRecords;
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValid = true;

            bool subjectSelected = false;
            string existingSubjects = string.Empty;

            AdminBLManager adminBLManager = new AdminBLManager();

            if (SearchSubject_resultsGridView.Rows.Count == 0)
            {
                ShowMessage(SearchSubject_errorMessageLabel,
                    "No subjects found to select");

                return false;
            }

            List<string> selectedSubjects = null;

            foreach (GridViewRow row in SearchSubject_resultsGridView.Rows)
            {
                if (selectedSubjects == null)
                    selectedSubjects = new List<string>();

                CheckBox checkBox = ((CheckBox)row.FindControl
                    ("SearchSubject_resultsGridViewSelectCheckbox"));

                if (!checkBox.Checked)
                {
                    continue;
                }

                // Set subject selected flag to true.
                subjectSelected = true;

                HiddenField subjectNameHiddenField = ((HiddenField)row.FindControl
                    ("SearchSubject_resultsGridViewSubjectNameHiddenField"));
                HiddenField subjectIDHiddenField = ((HiddenField)row.FindControl
                    ("SearchSubject_resultsGridViewSubjectIDHiddenField"));
                HiddenField categoryIDHiddenField = ((HiddenField)row.FindControl
                    ("SearchSubject_resultsGridViewCategoryIDHiddenField"));

                // Add the subject name to the selected subjects list.
                selectedSubjects.Add(subjectNameHiddenField.Value.Trim().ToUpper());

                // Checks whether the subject already exists 
                int count = adminBLManager.CheckSubject
                    (int.Parse(Request.QueryString["categoryid"]), 
                    int.Parse(subjectIDHiddenField.Value), 
                    subjectNameHiddenField.Value.Trim());

                if (count != 0)
                {
                    ShowMessage(SearchSubject_errorMessageLabel, 
                        string.Format("Subject '{0}' already exist for category '{1}'",
                        subjectNameHiddenField.Value.Trim(),
                        Request.QueryString["categoryname"]));

                    isValid = false;
                }
            }

            if (subjectSelected == false)
            {
                ShowMessage(SearchSubject_errorMessageLabel,
                    "No subject(s) selected to add");

                return false;
            }

            if (selectedSubjects != null)
            {
                // Generate distinct list from the selected subject list and check
                // the count with the original list. If it varies, then duplicate
                // subjects are selected.
                if (selectedSubjects.Distinct().ToList<string>().Count != selectedSubjects.Count)
                {
                    ShowMessage(SearchSubject_errorMessageLabel,
                        "Duplicate subjects are selected");

                    return false;
                }
            }

            return isValid;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}