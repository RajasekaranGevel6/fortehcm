using System;
using System.Web.UI;
using Forte.HCM.UI.Common;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Data;

namespace Forte.HCM.UI.Popup
{
    public partial class ShowScheduleOnlineInterviewCandidate : PageBase
    {
        #region Event Handlers

        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const int _pageSize = 2;
        
        #endregion Private Constants

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set broser title
                Master.SetPageCaption("Show Schedule Online Candidate");
                // Set default button and focus.
                //Page.Form.DefaultButton = ShowScheduleOnlineInterviewCandidate_topSearchButton.UniqueID;

                if (!IsPostBack)
                {
                    int positionProfileId=0;
                    if(Session["SESSION_POSITION_PROFILE_ID"]!=null)
                        positionProfileId=Convert.ToInt32(Session["SESSION_POSITION_PROFILE_ID"].ToString());
                    ScheduleOnlineInterviewCandidate_candidateNameImageButton.Attributes.Add("onclick", "return LoadCandidate('"
                    + ScheduleOnlineInterviewCandidate_Popup_candidateNameTextBox.ClientID + "','"
                    + ScheduleOnlineInterviewCandidate_Popup_emailTextBox.ClientID + "','"
                    + ScheduleOnlineInterviewCandidate_candidateIdHiddenField.ClientID
                    + "')");

                    // Assign handler for 'load position profile candidate' icon.
                    ScheduleOnlineInterviewCandidate_positionProfileCandidateImageButton.Attributes.Add("onclick", "return LoadPositionProfileCandidates('"
                       + ScheduleOnlineInterviewCandidate_Popup_candidateNameTextBox.ClientID + "','"
                       + ScheduleOnlineInterviewCandidate_Popup_emailTextBox.ClientID + "','"
                       + ScheduleOnlineInterviewCandidate_candidateIdHiddenField.ClientID + "','"
                       + positionProfileId + "')");

                    ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView.AllowSorting = false;
                    // Set default button and focus.
                    //Page.Form.DefaultButton = ShowScheduleOnlineInterviewCandidate_topSearchButton.UniqueID;
                    LoadOnlineInterviewSessionAssessor();
                }
                ScheduleOnlineInterviewCandidate_Popup_createCandidateButton.Attributes.Add("onclick",
                "return ShowCandidatePopup('" + ScheduleOnlineInterviewCandidate_Popup_candidateNameTextBox.ClientID + "','" + ScheduleOnlineInterviewCandidate_Popup_emailTextBox.ClientID + "','" + ScheduleOnlineInterviewCandidate_candidateIdHiddenField.ClientID + "')");

                LoadAssessorGrid();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel,
                    exp.Message.ToString());
            }
        }

        private void LoadOnlineInterviewSessionAssessor()
        {
            try
            {
                if(!IsValidData())
                    return;
                
                string skillSearched = string.Empty;
                string skillSearchedText = string.Empty;
                string selectedSubjectIds = string.Empty;
                string assessorsMatched = string.Empty;
                string assessorTimeSlotMatched = string.Empty;
                string assessorInterviewMatched = string.Empty;
                string interviewSessionKey = string.Empty;

                interviewSessionKey = Request.QueryString["interviewSessionKey"].ToString();
                DataSet assessorAndSkillDataset = new InterviewSessionBLManager().
                    GetOnlineAssessorAndSkillIdBySessionKey(interviewSessionKey);

                DataTable dtFullData = assessorAndSkillDataset.Tables[0];
                //If no matches found
                if (dtFullData.Rows.Count == 0)
                {
                    AssignEmptyGrid();
                    return;
                }

                DataTable dtSkillSet = assessorAndSkillDataset.Tables[1];
                foreach (DataRow row in dtSkillSet.Rows)
                    selectedSubjectIds = selectedSubjectIds + row["SKILL_ID"].ToString() + ",";

                List<Subject> subjects = new List<Subject>();
                subjects = new CommonBLManager().GetSubjects(selectedSubjectIds.ToString().TrimEnd(','));

                foreach (Subject subject in subjects)
                {
                    skillSearched = skillSearched + subject.SubjectID + ",";
                    skillSearchedText = skillSearchedText + subject.SubjectName + ",";
                }

                skillSearched = skillSearched.ToString().TrimEnd(',');
                skillSearchedText = skillSearchedText.ToString().TrimEnd(',');

                //Split the searched skill text
                string[] skillMatrix = skillSearched.Split(',');
                //Split the searched skill id
                string[] skillMatrixText = skillSearchedText.Split(',');
                
                //Get distinctData from retrieved data table
                DataTable distinctData = dtFullData.DefaultView.ToTable(true, "USER_ID","EMAIL","ASSESSOR_NAME");
                distinctData.AcceptChanges();
                //Remove empty rows from distinct data table
                DataView dvFilterEmpty = new DataView(distinctData);
                dvFilterEmpty.RowFilter = "Isnull(EMAIL,'') <> ''";
                DataTable dtAssesorSet = dvFilterEmpty.ToTable();
                dtAssesorSet.AcceptChanges();

                if (dtAssesorSet.Rows.Count == 0)
                {
                    AssignEmptyGrid();
                    return;
                }
                //Construct new data table with filtered data
                DataTable dtGenerateColumn = new DataTable();
                dtGenerateColumn.Columns.Add("Skills/Areas");

                //Get top matches enabled
                int topColumnCnt = 0;
              
                topColumnCnt = dtAssesorSet.Rows.Count;

                for (int k = 0; k <= topColumnCnt - 1; k++)
                    dtGenerateColumn.Columns.Add(string.Concat(dtAssesorSet.Rows[k]["ASSESSOR_NAME"].
                        ToString().Trim(), ":", dtAssesorSet.Rows[k]["EMAIL"].ToString().Trim(),
                        ":", dtAssesorSet.Rows[k]["USER_ID"].ToString().Trim()));
                
                dtGenerateColumn.AcceptChanges();
                int m = 0;
                int columnCount = 0;
                columnCount = dtGenerateColumn.Columns.Count;
                ViewState["columnCount"] = columnCount;
                string[] rowValues = new string[columnCount];
                string rowCloumnName = string.Empty;
                foreach (string skill in skillMatrix)
                {
                    string[] splitColumnName = null;
                    string expression = string.Empty;
                    string skillMatched = string.Empty;
                    string defultCellSelected = string.Empty;
                    defultCellSelected = "N";
                    
                    DataRow drAddNewRow = dtGenerateColumn.NewRow();
                    rowCloumnName = string.Concat(skillMatrixText[m].ToString().Trim().ToUpper(),":",skill.ToString().Trim());
                    rowValues[0] = rowCloumnName;
                    for (int i = 1; i <= columnCount - 1; i++)
                    {
                        splitColumnName = dtGenerateColumn.Columns[i].ColumnName.Split(':');

                        // Construct filter expression.
                        expression = "EMAIL='" + splitColumnName[1].ToString().Trim() + 
                            "' AND SKILL_MATCHED='" + skill.ToString().Trim() +"'";

                        // Get filtered rows.
                        DataRow[] filteredRows = dtFullData.Select(expression);
                        if (filteredRows == null || filteredRows.Length == 0)
                            skillMatched = "N";
                        else
                            skillMatched = "Y";

                        rowValues[i] = string.Concat(skillMatched, ":", defultCellSelected, ":", 
                            skill.ToString().Trim(), ":", splitColumnName[2].ToString().Trim());
                        if (i == dtGenerateColumn.Columns.Count - 1)
                        {
                            drAddNewRow.ItemArray = rowValues;
                            dtGenerateColumn.Rows.Add(drAddNewRow);
                            dtGenerateColumn.AcceptChanges();
                        }
                    }
                    m++;
                }

                //Assign additional skill/areas at the end
                DataRow drAddSkill = dtGenerateColumn.NewRow();
                rowValues[0] = "";
                for (int j = 1; j <= columnCount - 1; j++)
                    rowValues[j] = "N";
                
                drAddSkill.ItemArray = rowValues;
                dtGenerateColumn.Rows.Add(drAddSkill);
                dtGenerateColumn.AcceptChanges();
                ViewState["DATALIST"] = null;
                ViewState["DATALIST"] = dtGenerateColumn;

                //For pageing 
                ViewState["DATALIST_FULL"] = dtGenerateColumn;
                ShowScheduleOnlineInterviewCandidate_pageNumberHiddenField.Value = "1";
                FilterDataTable(string.Empty);
                //LoadAssessorGrid();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel,
                    exception.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods
        /// <summary>
        /// Method that clears the existing and data and grid
        /// </summary>
        private void AssignEmptyGrid()
        {
            ViewState["DATALIST"] = null;
            LoadAssessorGrid();
        }

        /// <summary>
        /// Method to construct the session table;
        /// </summary>
        private  bool ConstructSession()
        {
            // Clear messages.
            ClearMessages();
            int i = 0;
            int rowCnt = 0;
            int colCnt = 0;
            int j = 0;
            bool flagSave = false;
            
            SessionDetail sessionDetail = new SessionDetail();
            sessionDetail.SessionName = "Online Interview Session";
            sessionDetail.SessionKey = string.Empty;

            sessionDetail.Assessors = new List<AssessorDetail>();
            AssessorDetail assessorDetail = null;

            List<AssessorDetail> assessorDetails = null;
            Session["ASSESSOR"] = null;
            assessorDetails = new List<AssessorDetail>();
            
            //Data Table
            DataTable dtAssessor = (DataTable)ViewState["DATALIST_FULL"];
            rowCnt = dtAssessor.Rows.Count;
            colCnt = dtAssessor.Columns.Count;

            for (i = 0; i <= dtAssessor.Columns.Count - 1; i++)
            {
                string rowValues = string.Empty;
                string[] arrayRowValues = new string[4];
                string ColumnName = string.Empty;
                string[] arrColumn = null;
                string skillColumn = string.Empty;
                ColumnName = dtAssessor.Columns[i].ColumnName.ToString();
                arrColumn = ColumnName.ToString().Trim().Split(':');
                if (dtAssessor.Rows[rowCnt-1][dtAssessor.Columns[i].ColumnName].ToString() == "Y")
                {
                    
                    string skillIds = string.Empty;
                    assessorDetail = new AssessorDetail();
                    assessorDetail.Skills = new List<SkillDetail>();
                    for (j = 0; j <= rowCnt - 2; j++)
                    {
                        rowValues = dtAssessor.Rows[j][dtAssessor.Columns[i].ColumnName].ToString();
                        if (rowValues.ToString().Trim().Split(':')[1] == "Y")
                        {
                            flagSave = true;
                            skillColumn = dtAssessor.Rows[j]["Skills/Areas"].ToString();
                            skillIds = skillIds + skillColumn.ToString().Trim().Split(':')[1] + ",";
                            assessorDetail.Skills.Add(new
                                SkillDetail(Convert.ToInt32(skillColumn.ToString().Trim().Split(':')[1].ToString()),
                                skillColumn.ToString().Trim().Split(':')[0].ToString()));
                        }
                    }

                    //For session Construction
                    assessorDetail.UserID = Convert.ToInt32(arrColumn[2].ToString());
                    assessorDetail.FirstName = arrColumn[0].ToString();
                    assessorDetail.UserEmail = arrColumn[1].ToString();
                    assessorDetail.AlternateEmailID = arrColumn[1].ToString();
                    sessionDetail.Assessors.Add(assessorDetail);

                    //To assign to parent grid and to db values
                    assessorDetail.Skill = skillIds.ToString().Trim().TrimEnd(',');
                    assessorDetails.Add(assessorDetail);
                    //End session construction
                  }
            }

            //Launch My Availability
            Session["ASSESSOR"] = assessorDetails;
            Session["SESSION_ASSESSORS"] = sessionDetail;
            return flagSave;
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            return isValidData;
        }

       
        /// <summary>
        /// Method assigns the searched list to grid
        /// </summary>
        private void LoadAssessorGrid()
        {
            ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel.Text = string.Empty;
            if (ViewState["DATALIST"] == null || ((DataTable)ViewState["DATALIST"]).Rows.Count == 0 || ((DataTable)ViewState["DATALIST"]).Rows.Count == 1)
            {
                
                ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView.AutoGenerateColumns = true;
                ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView.DataSource = null;
                ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView.DataBind();
                ShowScheduleOnlineInterviewCandidate_ShowAssesserResultsTR.Visible = false;
                ShowScheduleOnlineInterviewCandidate_ShowAssesserResultsHeaderTR.Visible = false;
                base.ShowMessage(ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel, "No data found to display");
                return;
            }
            ShowScheduleOnlineInterviewCandidate_ShowAssesserResultsTR.Visible = true;
            ShowScheduleOnlineInterviewCandidate_ShowAssesserResultsHeaderTR.Visible = true;

            ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView.AutoGenerateColumns = true;
            ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView.DataSource = ViewState["DATALIST"];
            ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView.DataBind();
        }

        /// <summary>
        /// Method that clears the messages.
        /// </summary>
        private void ClearMessages()
        {
            ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel.Text = string.Empty;
            ShowScheduleOnlineInterviewCandidate_topSuccessMessageLabel.Text = string.Empty;
        }


        #region onNavigatePage

        /// <summary>
        /// Called when [navigate page].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void OnNavigatePage(object sender, EventArgs e)
        {
            try
            {
                ImageButton navigateButton = (ImageButton)sender;
                FilterDataTable(navigateButton.CommandName);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel,
                    exception.Message);
            }
        }
        #endregion

        #region Pageing Method
        /// <summary>
        /// Method handles/filters according to the page navigation and reassigns the data
        /// </summary>
        /// <param name="navigationType">navigationType</param>
        private void FilterDataTable(string navigationType)
        {
            int i = 0;
            int j = 0;
            int k = 0;

            int pageNum = 0;
            int pageSize = 0;
            int pageStartIndex=0;
            int pageEndIndex=0;

            pageSize = 2;
            pageNum = Convert.ToInt32(ShowScheduleOnlineInterviewCandidate_pageNumberHiddenField.Value);

            if (navigationType == "Previous")
                pageNum = pageNum - 1;
            if (navigationType == "Next")
                pageNum = pageNum + 1;
                
            pageStartIndex = ((pageNum - 1) * pageSize) + 1;
            pageEndIndex = pageNum * pageSize;
            ShowScheduleOnlineInterviewCandidate_pageNumberHiddenField.Value = pageNum.ToString();

            //Filter the data table
            DataTable dtFilterForPageing = ((DataTable)ViewState["DATALIST_FULL"]);

            //Check page index exceeds the total records
            if (pageEndIndex >= dtFilterForPageing.Columns.Count)
                pageEndIndex = dtFilterForPageing.Columns.Count-1;

            DataTable dtNewFiltered = new DataTable();
            dtNewFiltered.Columns.Add("Skills/Areas");
            for (i = pageStartIndex; i <= pageEndIndex; i++)
                dtNewFiltered.Columns.Add(dtFilterForPageing.Columns[i].ColumnName);

            int columCnt = 0;
            columCnt = dtNewFiltered.Columns.Count;
            string[] arrayFilteredRow = new string[columCnt];

            //Create row apply data
            for (k = 0; k <= dtFilterForPageing.Rows.Count - 1; k++)
            {
                DataRow drFilter = dtNewFiltered.NewRow();
                for (j = 0; j <= columCnt - 1; j++)
                    arrayFilteredRow[j] = dtFilterForPageing.Rows[k][dtNewFiltered.
                        Columns[j].ColumnName].ToString();
                
                drFilter.ItemArray = arrayFilteredRow;
                dtNewFiltered.Rows.Add(drFilter);
                dtNewFiltered.AcceptChanges();
            }
            ShowScheduleOnlineInterviewCandidate_previousImageButton.Enabled = true;
            ShowScheduleOnlineInterviewCandidate_nextImageButton.Enabled = true;

            if (pageStartIndex == 1)
                ShowScheduleOnlineInterviewCandidate_previousImageButton.Enabled=false;
            if (pageEndIndex >= dtFilterForPageing.Columns.Count-1)
                ShowScheduleOnlineInterviewCandidate_nextImageButton.Enabled = false;

            ViewState["DATALIST"] = dtNewFiltered;
            LoadAssessorGrid();
        }
        #endregion

        #endregion Private Methods

        #region Protected Overridden Methods



        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        /// <summary>
        /// Event handles the rowcommand functionalities
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView_RowCommand(object sender, 
            GridViewCommandEventArgs e)
        {

            #region show skill popup
            if (e.CommandName == "ShowSkillPopup")
            {
                try
                {
                    ShowScheduleOnlineInterviewCandidate_addSkillErrorLabel.Text = null;
                    ShowScheduleOnlineInterviewCandidate_addSkillTextBox.Text = null;
                    ShowScheduleOnlineInterviewCandidate_addSkillModalPopupExtender.Show();
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                    base.ShowMessage(ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel, exception.Message);

                }
            }
            #endregion

            #region select/unselect assessor skills
            if (e.CommandName == "Change")
            {
                string[] arqCollection = null;
                string[] arrCellValue = null;
                string cellValue = null;
                string imgType = string.Empty;
                string newCellValue = string.Empty;

                DataTable dtAssList = (DataTable)ViewState["DATALIST_FULL"];

                if (Convert.ToString(e.CommandArgument) != string.Empty)
                    arqCollection = e.CommandArgument.ToString().Trim().Split(',');

                //Retrieve existing cell value
                cellValue = dtAssList.Rows[Convert.ToInt32(arqCollection[0])]
                                [arqCollection[1]].ToString();

                if(!Utility.IsNullOrEmpty(cellValue))
                    arrCellValue = cellValue.ToString().Trim().Split(':');

                if (arrCellValue[1] == "Y")
                    imgType = "N";
                else
                    imgType = "Y";

                newCellValue = string.Concat(arrCellValue[0], ":", imgType);
                dtAssList.Rows[Convert.ToInt32(arqCollection[0])][arqCollection[1]] = newCellValue;
                dtAssList.AcceptChanges();
                ViewState["DATALIST_FULL"] = dtAssList;
                FilterDataTable(string.Empty);
                LoadAssessorGrid();
            }
            #endregion

            #region delete row from table
            if (e.CommandName == "Delete")
            {
                int delRowNo = Convert.ToInt32(e.CommandArgument);
                DataTable dtAssList = (DataTable)ViewState["DATALIST_FULL"];
                dtAssList.Rows.RemoveAt(delRowNo);
                dtAssList.AcceptChanges();
                ViewState["DATALIST_FULL"] = dtAssList;
                FilterDataTable(string.Empty);
            }
            #endregion

            #region link to profile
            if (e.CommandName == "ProfileLink")
            {
                try
                {
                    int assessorId=Convert.ToInt32(e.CommandArgument);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowAvailabilityForm", "<script language='javascript'>ShowAssessorProfile(" + assessorId + ");</script>", false);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                    base.ShowMessage(ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel, exception.Message);
                }
            }
            
            #endregion

            #region show approved time slots
            if (e.CommandName == "TimeSlotsAvailable")
            {
                try
                {
                    int assessorId = 0;
                    int skillId = 0;
                    string[] selectedAssessors = null;
                    string interviewSessionKey = string.Empty;
                    
                    interviewSessionKey = Request.QueryString["interviewSessionKey"].ToString();
                    selectedAssessors = e.CommandArgument.ToString().Split(':');
                    skillId = Convert.ToInt32(selectedAssessors[0].ToString());
                    assessorId = Convert.ToInt32(selectedAssessors[1].ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowTimeSlots", "<script language='javascript'>ShowSearchTimeSlots('" + assessorId + "','" + interviewSessionKey + "','" + skillId + "','" + ScheduleOnlineInterviewCandidate_selectedTimeSlotIDsHiddenField.ClientID + "');</script>", false);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                    base.ShowMessage(ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel, exception.Message);
                }
            }
            #endregion
        }

        /// <summary>
        /// Handler to generate and apply image and calendar option for each cell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView_RowDataBound(object sender, 
            GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int i = 0;
                string[] assessorData = null;
                for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                {
                    if (i == 0)
                    {
                        Label lblSkill = new Label();
                        lblSkill.Text = e.Row.Cells[i].Text;
                        e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[i].Controls.Add(lblSkill);
                        e.Row.Cells[i].Width = Unit.Pixel(150);
                    }
                    else
                    {
                        LinkButton lnkProfile = new LinkButton();
                        e.Row.Cells[i].Wrap = true;
                        if (!Utility.IsNullOrEmpty(e.Row.Cells[i].Text))
                        {
                            assessorData = e.Row.Cells[i].Text.ToString().Trim().Split(':');
                            lnkProfile.Text = assessorData[0].ToString();
                            lnkProfile.CommandArgument = assessorData[2].ToString();
                            lnkProfile.CommandName = "ProfileLink";
                            lnkProfile.CssClass = "profile_link_text";
                            e.Row.Cells[i].BorderColor = System.Drawing.Color.Red;
                            e.Row.Cells[i].Controls.Add(lnkProfile);
                        }
                        
                        Label lblBreak = new Label();
                        lblBreak.Text = "<br><br>";
                        e.Row.Cells[i].Controls.Add(lblBreak);

                        HyperLink hlkEmalMail = new HyperLink();
                        hlkEmalMail.Target = "_blank";
                        hlkEmalMail.NavigateUrl = "mailto:" + assessorData[1].ToString();
                        hlkEmalMail.Text = assessorData[1];
                        e.Row.Cells[i].Controls.Add(hlkEmalMail);
                        e.Row.Cells[i].Width = Unit.Pixel(150);
                    }
                    e.Row.Cells[i].Height = Unit.Pixel(75);
                }
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                int k = 0;
                string columnName = string.Empty;
                for (k = 0; k <= e.Row.Cells.Count - 1; k++)
                {
                    if (k == 0)
                    {
                        Label lblSpace1 = new Label();
                        lblSpace1.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                        e.Row.Cells[k].Controls.Add(lblSpace1);

                        Label lblSkill = new Label();
                        if (!Utility.IsNullOrEmpty(e.Row.Cells[k].Text))
                        {
                            string[] rowColCollection = null;
                            rowColCollection = e.Row.Cells[k].Text.ToString().Split(':');
                            lblSkill.Text = rowColCollection[0];    
                        }
                        e.Row.Cells[k].Controls.Add(lblSkill);
                        e.Row.Cells[k].HorizontalAlign = HorizontalAlign.Left;
                    }
                    else
                    {
                        
                        columnName = ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView.HeaderRow.Cells[k].Text;
                        if ((((DataTable)ViewState["DATALIST"]).Rows.Count) - 1 == e.Row.RowIndex)
                        {
                            CheckBox chkBox = new CheckBox();
                            chkBox.ToolTip = "Select";
                            chkBox.ID = string.Concat("chkAssessor", e.Row.RowIndex.ToString(), k.ToString());
                            chkBox.Attributes.Add("key", string.Concat(e.Row.RowIndex.ToString(), ",", columnName));
                            chkBox.AutoPostBack = true;
                            chkBox.CheckedChanged += new EventHandler(chkBox_CheckedChanged);
                            if (e.Row.Cells[k].Text.ToString().Trim() == "Y")
                                chkBox.Checked = true;
                            else
                                chkBox.Checked = false;
                            e.Row.Cells[k].Controls.Add(chkBox);
                            e.Row.Cells[k].Height = Unit.Pixel(30);
                            e.Row.Cells[k].HorizontalAlign = HorizontalAlign.Center;
                        }
                        else
                        {
                            string[] cellCollection = null;
                            if (!Utility.IsNullOrEmpty(e.Row.Cells[k].Text))
                            {
                                cellCollection = e.Row.Cells[k].Text.ToString().Trim().Split(':');
                                if (cellCollection[0] == "Y")
                                    e.Row.Cells[k].BackColor = System.Drawing.ColorTranslator.FromHtml("#F2FFE8");
                                else
                                    e.Row.Cells[k].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFEDE1");
                                
                                e.Row.Cells[k].Height = Unit.Pixel(50);
                                e.Row.Cells[k].HorizontalAlign = HorizontalAlign.Center;

                                ImageButton imgCalendar = new ImageButton();
                                imgCalendar.ImageUrl = "~/App_Themes/DefaultTheme/Images/icon_calendar.gif";
                                imgCalendar.ToolTip = "Time Slots Available";
                                imgCalendar.CommandName = "TimeSlotsAvailable";
                                imgCalendar.CommandArgument = string.Concat(cellCollection[2].ToString().Trim(),
                                                                ":", cellCollection[3].ToString().Trim());
                                e.Row.Cells[k].Controls.Add(imgCalendar);
                            }
                        }
                    }
                }
            }
        }
        protected void ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView_RowDeleting(object sender, 
            GridViewDeleteEventArgs e)
        {

        }

        /// <summary>
        /// Handles the Clicked event of the ShowScheduleOnlineInterviewCandidate_addSkillSaveButton_Clicked control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ShowScheduleOnlineInterviewCandidate_addSkillSaveButton_Clicked
                (object sender, EventArgs e)
        {
            try
            {
                if (ShowScheduleOnlineInterviewCandidate_addSkillTextBox.Text.Length == 0)
                {
                    ShowScheduleOnlineInterviewCandidate_addSkillErrorLabel.Text = null;
                    base.ShowMessage(ShowScheduleOnlineInterviewCandidate_addSkillErrorLabel, "Enter the skill");
                    ShowScheduleOnlineInterviewCandidate_addSkillModalPopupExtender.Show();
                    return;
                }
                #region Copy last row, and delete it, then assign the new skill row and append the copied row
                int columnCnt = Convert.ToInt32(ViewState["columnCount"].ToString());
                string[] addSkillRowValue = new string[columnCnt];

                DataTable dtAddSkill = ((DataTable)ViewState["DATALIST_FULL"]);
                DataRow drSkill = dtAddSkill.NewRow();
                DataRow drAddNewSkill = dtAddSkill.NewRow();
                drSkill.ItemArray = dtAddSkill.Rows[dtAddSkill.Rows.Count-1].ItemArray;
                dtAddSkill.Rows.RemoveAt(dtAddSkill.Rows.Count - 1);

                addSkillRowValue[0] = ShowScheduleOnlineInterviewCandidate_addSkillTextBox.Text.ToString().Trim().ToUpper();
                for (int i = 1; i <= columnCnt - 1; i++)
                    addSkillRowValue[i] = "N:N";
                drAddNewSkill.ItemArray = addSkillRowValue;
                
                dtAddSkill.Rows.Add(drAddNewSkill);
                dtAddSkill.Rows.Add(drSkill);
                dtAddSkill.AcceptChanges();
                #endregion

                ViewState["DATALIST_FULL"] = dtAddSkill;
                FilterDataTable(string.Empty);
                LoadAssessorGrid();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Event that handles the save process of selected assessors and its
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowScheduleOnlineInterviewCandidate_saveButton_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (ConstructSession())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(),"ShowAvailabilityForm", "<script language='javascript'>ShowAvailabilityRequest();</script>", false);
                }
                else
                {
                    base.ShowMessage(ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel,
                    "Please select at least one assessor with skill selected");
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel,
                    exp.Message.ToString());
            }
        }
        /// <summary>
        /// Event to handle the check boxes status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkBox_CheckedChanged(object sender, EventArgs e)
        {
            string[] selectedAssessors = null;
            string checkedVal="N";
            CheckBox chkAssessors = (CheckBox)sender;

            if (chkAssessors.Checked)
                checkedVal = "Y";

            string key = (sender as CheckBox).Attributes["key"];
            selectedAssessors = key.Trim().Split(',');

            DataTable dtAssCheckedList = (DataTable)ViewState["DATALIST_FULL"];
            dtAssCheckedList.Rows[Convert.ToInt32(selectedAssessors[0])][selectedAssessors[1]] = checkedVal;
            dtAssCheckedList.AcceptChanges();
            ViewState["DATALIST_FULL"] = dtAssCheckedList;
        }
        /// <summary>
        /// Event launches a availability screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowScheduleOnlineInterviewCandidate_emailAllLinkButton_Click(object sender, EventArgs e)
        {
            if (ConstructSession())
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(),"ShowAvailabilityForm", "<script language='javascript'>ShowAvailabilityRequest();</script>", false);
            }
            else
            {
                base.ShowMessage(ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel,
                "Please select at least one assessor with skill selected");
                return;
            }
        }
        /// <summary>
        /// Event to close the popup buttion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowScheduleOnlineInterviewCandidate_addAssessorButton_Click(object sender, EventArgs e)
        {
            if (ConstructSession())
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Closeform", "<script language='javascript'>self.close();</script>", false);
            }
            else
            {
                base.ShowMessage(ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel,
                "Please select at least one assessor with skill selected");
                return;
            }
        }
        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void ShowAssesserLookup_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadOnlineInterviewSessionAssessor();
                // Set default focus.
                //Page.Form.DefaultFocus = ShowScheduleOnlineInterviewCandidate_topSearchButton.UniqueID;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel, exp.Message);
            }
        }
}
}