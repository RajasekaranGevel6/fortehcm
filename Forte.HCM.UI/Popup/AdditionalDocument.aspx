﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdditionalDocument.aspx.cs"
    Inherits="Forte.HCM.UI.Popup.AdditionalDocument" MasterPageFile="~/MasterPages/PopupMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="SearchPositionProfileCandidate_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript">
        function OpenPopUp(documentname, documentid) {
            window.open('../Common/Download.aspx?document=' + documentname + '&documentid=' + documentid, '', 'toolbar=0,resizable=0,width=1,height=1', '');

        }

        function PageReset() {
            __doPostBack('','');
        }
        function AdditionalDocument_addDocumentError(sender, args) {
            return false;
        }
        //To validate the resume controlCreateCandidate_resumeClientUploadComplete
        function AdditionalDocument_addDocumentComplete(sender, args) {
            var fileName = args.get_fileName();
            var fileExtn = (/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName) : undefined;
            if (fileExtn != "doc" && fileExtn != "DOC" && fileExtn != "pdf" && fileExtn != "PDF" && fileExtn != "docx" && fileExtn != "DOCX"
            && fileExtn != "TXT" && fileExtn != "txt" && fileExtn != "JPG" && fileExtn != "jpg" && fileExtn != "EFX" && fileExtn != "efx") {
                $get("<%=AdditionalDocument_successMessageLabel.ClientID%>").innerHTML = "";
                $get("<%=AdditionalDocument_errorMessageLabel.ClientID%>").innerHTML = "";
                var errMsg = "Please select valid file format.(.doc,.docx,.pdf,txt,jpg,efx and rtf)";
                $get("<%=AdditionalDocument_errorMessageLabel.ClientID%>").innerHTML = errMsg;

                args.set_cancel(true);
                var who1 = document.getElementsByName('<%= AdditionalDocument_addDocumentAsyncFileUpload.UniqueID %>')[0];

                who1.value = "";
                var who3 = who1.cloneNode(false);
                who3.onchange = who1.onchange;
                who1.parentNode.replaceChild(who3, who1);

                return false;
                alert(errMsg);
            }
            else {
                Add_Click();
            }
        }

        function Add_Click() {
            document.getElementById('<%= AdditionalDocument_addDocument_addButton.ClientID %>').click();
            
        }
    </script>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="popup_td_padding_5">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="AdditionalDocument_headerLiteral" runat="server" Text="Candidate Additional Documents"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <img alt="Close" src="../App_Themes/DefaultTheme/Images/close_icon_popup.gif"
                             onclick="javascript:self.close();" title="Close" style="cursor:hand;cursor:pointer"/> 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_5" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:UpdatePanel ID="AdditionalDocument_messageUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="AdditionalDocument_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="AdditionalDocument_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="AdditionalDocument_topResetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header_bg">
                                        Documents
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <div style="height: 170px; overflow: auto;" id="AdditionalDocument_filesDiv">
                                            <asp:UpdatePanel ID="AdditionalDocument_documentsUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="AdditionalDocument_gridView" runat="server" SkinID="sknNewGridView"
                                                        ShowHeader="false" ShowFooter="false" GridLines="Horizontal" BorderColor="white"
                                                        BorderWidth="1px" AutoGenerateColumns="False" OnRowDeleting="AdditionalDocument_gridView_RowDeleting"
                                                        OnRowDataBound="AdditionalDocument_gridView_RowDataBound">
                                                        <RowStyle CssClass="grid_alternate_row" />
                                                        <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                        <HeaderStyle CssClass="grid_header_row" />
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <table cellpadding="1" cellspacing="1" border="0" width="0">
                                                                        <tr>
                                                                            <td width="15PX">
                                                                                <asp:ImageButton ID="AdditionalDocument_gridView_documentDeleteImageButton" SkinID="sknPPDeleteImageButton"
                                                                                    runat="server" CommandName="Delete" ToolTip="Delete" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:HiddenField ID="AdditionalDocument_gridView_documentResumeIdHiddenField" runat="server"
                                                                                    Value='<%# Eval("ResumeID") %>' />
                                                                                <asp:LinkButton ID="AdditionalDocument_gridView_ppfileNameLinkButton" runat="server"
                                                                                    ToolTip="Download document" Text='<%# Eval("ResumeTitle") %>' CommandName="Download"
                                                                                    CommandArgument='<%# Eval("ResumeTitle") %>'></asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="AdditionalDocument_topResetLinkButton" />
                                                    <asp:AsyncPostBackTrigger ControlID="AdditionalDocument_addDocument_addButton" /> 
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td> 
                                         <table width="100%" cellpadding="4" cellspacing="5" border="0" align="left">
                                                    <tr>
                                                        <td class="header_bg" colspan="3">
                                                            Add Document
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">
                                                            <asp:Label ID="AdditionalDocument_documentHeaderLabel" runat="server" Text="Select Document"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td width="50%">
                                                            <asp:UpdatePanel ID="FileUpload_asyncFile_UpdatePanel" runat="server">
                                                                <ContentTemplate > 
                                                                <ajaxToolKit:AsyncFileUpload ID="AdditionalDocument_addDocumentAsyncFileUpload" runat="server"
                                                                    Width="250" Height="24px" ThrobberID="Throbber" FailedValidation="False" PersistFile="false"
                                                                    SkinID="sknCanTextBox" UploaderStyle="Traditional" ClientIDMode="AutoID" OnClientUploadError="AdditionalDocument_addDocumentError"
                                                                    OnClientUploadComplete="AdditionalDocument_addDocumentComplete"  
                                                                    onuploadedcomplete="AdditionalDocument_addDocumentAsyncFileUpload_UploadedComplete"  />
                                                            </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td width="10%">
                                                            <asp:ImageButton ID="AdditionalDocument_addDocumentHelpImageButton" SkinID="sknHelpImageButton"
                                                                runat="server" OnClientClick="javascript:return false;" ToolTip="Click here to select the doc/docx/pdf/rtf/txt/jpg/efx file"
                                                                ImageAlign="Middle" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="popup_td_padding_5" colspan="3">
                                                          <asp:UpdatePanel ID="FileUpload_addButton_UpdatePanel" runat="server">
                                                             <ContentTemplate>
                                                                <div style="display:none">
                                                                    <asp:Button ID="AdditionalDocument_addDocument_addButton"  runat="server" Text="Add"
                                                                        SkinID="sknButtonId" OnClick="AdditionalDocument_addDocument_addButton_Click" />
                                                                 </div>
                                                               </ContentTemplate>
                                                            
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                           
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="popup_td_padding_5" valign="top">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_5">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="AdditionalDocument_topResetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" ToolTip="Reset" OnClick="AdditionalDocument_topResetLinkButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top"> 
                            <label for="Cancel" title="Close" class="popup_link_text" style="cursor:hand;cursor:pointer" onclick="javascript:self.close();">Cancel</label> 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
