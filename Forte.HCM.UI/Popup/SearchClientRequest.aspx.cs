﻿
#region Header

// Copyright (C) Forte. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchClientRequest.aspx.cs
// File that represents the user interface layout and functionalities
// for the SearchClientRequest page. This page helps in searching for 
// position profiles by providing search criteria for position profile name,
// client, position name, created date, etc. This class inherits the 
// Forte.HCM.UI.Common.PageBase class.

# endregion Header

#region Directives

using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the SearchClientRequest page. This page helps in searching for 
    /// position profiles by providing search criteria for position profile 
    /// name, client, position name, created date, etc. This class inherits
    /// inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class SearchClientRequest : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Events Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set title.
                Master.SetPageCaption("Search Position Profile");
                SearchClientRequest_headerLiteral.Text = "Search Position Profile";

                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                // Set default button and focus.
                Page.Form.DefaultButton = SearchClientRequest_searchButton.UniqueID;
                // Subscribes to the page number click event.
                SearchClientRequest_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchClientRequest_pageNavigator_PageNumberClick);
                // 
                if (!Utility.IsNullOrEmpty(SearchClientRequest_isMaximizedHiddenField.Value) &&
                    SearchClientRequest_isMaximizedHiddenField.Value == "Y")
                {
                    SearchClientRequest_searchCriteriasDiv.Style["display"] = "none";
                    SearchClientRequest_searchResultsUpSpan.Style["display"] = "block";
                    SearchClientRequest_searchResultsDownSpan.Style["display"] = "none";
                    SearchClientRequest_searchResultsDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchClientRequest_searchCriteriasDiv.Style["display"] = "block";
                    SearchClientRequest_searchResultsUpSpan.Style["display"] = "none";
                    SearchClientRequest_searchResultsDownSpan.Style["display"] = "block";
                    SearchClientRequest_searchResultsDiv.Style["height"] = RESTORED_HEIGHT;
                }

                if (!IsPostBack)
                {
                    // Set default focus.
                    Page.Form.DefaultFocus = SearchClientRequest_positionProfileTextBox.UniqueID;
                    SearchClientRequest_positionProfileTextBox.Focus();
                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Descending;
                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "CREATEDDATE";
                    SearchClientRequest_searchResultsDiv.Visible = false;
                }
                else
                    SearchClientRequest_searchResultsDiv.Visible = true;

                //Set attribute to expand/reset TR
                SearchClientRequest_searchResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    SearchClientRequest_searchResultsDiv.ClientID + "','" +
                    SearchClientRequest_searchCriteriasDiv.ClientID + "','" +
                    SearchClientRequest_searchResultsUpSpan.ClientID + "','" +
                    SearchClientRequest_searchResultsDownSpan.ClientID + "','" +
                    SearchClientRequest_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchClientRequest_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchClientRequest_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                SearchClientRequest_errorMessageLabel.Text = string.Empty;
                SearchClientRequest_successMessageLabel.Text = string.Empty;

                if (!IsValidData())
                    return;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = "CREATEDDATE";

                // Reset the paging control.
                SearchClientRequest_pageNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchClientRequest_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void SearchClientRequest_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchClientRequest_positionProfileTextBox.Text = string.Empty;
                SearchClientRequest_clientNameTextBox.Text = string.Empty;
                SearchClientRequest_positionNameTextBox.Text = string.Empty;
                SearchClientRequest_createdDateTextBox.Text = string.Empty;
                SearchClientRequest_contactNameTextBox.Text = string.Empty;
                SearchClientRequest_departmentNameTextBox.Text = string.Empty;
                SearchClientRequest_locationTextBox.Text = string.Empty;
                SearchClientRequest_createdByTextBox.Text = string.Empty;

                SearchClientRequest_searchResultsGridView.DataSource = null;
                SearchClientRequest_searchResultsGridView.DataBind();

                SearchClientRequest_pageNavigator.PageSize = base.GridPageSize;
                SearchClientRequest_pageNavigator.TotalRecords = 0;
                SearchClientRequest_searchResultsDiv.Visible = false;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = "CREATEDDATE";

                // Reset to the restored state.
                SearchClientRequest_searchCriteriasDiv.Style["display"] = "block";
                SearchClientRequest_searchResultsUpSpan.Style["display"] = "none";
                SearchClientRequest_searchResultsDownSpan.Style["display"] = "block";
                SearchClientRequest_searchResultsDiv.Style["height"] = RESTORED_HEIGHT;
                SearchClientRequest_isMaximizedHiddenField.Value = "N";

                // Clear message fields.
                SearchClientRequest_successMessageLabel.Text = string.Empty;
                SearchClientRequest_errorMessageLabel.Text = string.Empty;

                // Set default focus.
                Page.Form.DefaultFocus = SearchClientRequest_positionProfileTextBox.UniqueID;
                SearchClientRequest_positionProfileTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchClientRequest_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchClientRequest_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                Search(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchClientRequest_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchClientRequest_searchResultsGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                // Reset and show records for first page.
                SearchClientRequest_pageNavigator.Reset();
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchClientRequest_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchClientRequest_searchResultsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchClientRequest_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchClientRequest_searchResultsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchClientRequest_searchResultsGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchClientRequest_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void Search(int pageNumber)
        {
            int totalRecords = 0;

            // Get the client details and position profile from the database
            ClientRequestSearchCriteria searchCriteria = new ClientRequestSearchCriteria();
            searchCriteria.ClientName = SearchClientRequest_clientNameTextBox.Text;
            searchCriteria.PositionProfileName = SearchClientRequest_positionProfileTextBox.Text;
            searchCriteria.PositionName = SearchClientRequest_positionNameTextBox.Text;
            searchCriteria.ContactName = SearchClientRequest_contactNameTextBox.Text;
            searchCriteria.DepartmentName = SearchClientRequest_departmentNameTextBox.Text;
            searchCriteria.CreatedBy = SearchClientRequest_createdByTextBox.Text;
            searchCriteria.Location = SearchClientRequest_locationTextBox.Text;
            searchCriteria.CreatedDateStr = SearchClientRequest_createdDateTextBox.Text;
            List<ClientRequestDetail> ClientRequestDetail = new CommonBLManager().GetClientRequestInformation(
                 searchCriteria
                 , pageNumber,
                 base.GridPageSize, out totalRecords, ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]), base.userID);

            if (ClientRequestDetail == null || ClientRequestDetail.Count == 0)
            {
                SearchClientRequest_searchResultsDiv.Visible = false;
                ShowMessage(SearchClientRequest_errorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
                SearchClientRequest_searchResultsDiv.Visible = true;

            SearchClientRequest_searchResultsGridView.DataSource = ClientRequestDetail;
            SearchClientRequest_searchResultsGridView.DataBind();
            SearchClientRequest_pageNavigator.PageSize = base.GridPageSize;
            SearchClientRequest_pageNavigator.TotalRecords = totalRecords;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            //int result;

            SearchClientRequest_MaskedEditValidator.Validate();
            if (!SearchClientRequest_MaskedEditValidator.IsValid)
            {
                isValidData = false;
                base.ShowMessage(SearchClientRequest_errorMessageLabel,
                SearchClientRequest_MaskedEditValidator.InvalidValueMessage.ToString());
            }

            //if (SearchClientRequest_clientNameTextBox.Text.Trim().Length != 0)
            //{
            //    if (!int.TryParse(SearchClientRequest_clientNameTextBox.Text, out result))
            //    {
            //        isValidData = false;
            //        base.ShowMessage(SearchClientRequest_errorMessageLabel, "Enter valid integer");
            //        SearchClientRequest_searchResultsDiv.Visible = false;
            //        SearchClientRequest_pageNavigator.TotalRecords = 0;
            //    }
            //}
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}
