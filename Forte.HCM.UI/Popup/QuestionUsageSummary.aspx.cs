﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// QuestionUsageSummary.cs
// File that represents the user interface for showing a question details
// and its test information too. 

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    public partial class QuestionUsageSummary : PageBase
    {
        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Question Usage Summary");

                Page.SetFocus(QuestionUsageSummary_bottomCloseLinkButton.ClientID);
                if (!Utility.IsNullOrEmpty(Request.QueryString["questionKey"]))
                {
                    QuestionUsageSummary_questionKeyValueLabel.Text = 
                        Request.QueryString["questionKey"].ToString().Trim();
                }

                if (!IsPostBack)
                {
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "TESTKEY";

                    if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper().Trim() == "INT")
                    {
                        // Show interview question usage details for default page number 1.
                        LoadInterviewQuestionUsageDetails(1);
                    }
                    else
                    {
                        // Show question usage details for default page number 1.
                        LoadQuestionUsageDetails(1);
                    }
                }

                // Add handlers.
                QuestionUsageSummary_bottomCloseLinkButton.Attributes.Add("onclick", 
                    "Javascript:return CloseMe();");
                QuestionUsageSummary_pageNavigator.PageNumberClick += new PageNavigator.
                    PageNumberClickEventHandler(QuestionUsageSummary_pageNavigator_PageNumberClick);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(QuestionUsageSummary_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void QuestionUsageSummary_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper().Trim() == "INT")
                {
                    // Show interview question usage details for default page number 1.
                    LoadInterviewQuestionUsageDetails(e.PageNumber);
                }
                else
                {
                    // Show question usage details for default page number 1.
                    LoadQuestionUsageDetails(e.PageNumber);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(QuestionUsageSummary_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void QuestionUsageSummary_questionDetailsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }

                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                QuestionUsageSummary_pageNavigator.Reset();

                if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                       Request.QueryString["type"].ToUpper().Trim() == "INT")
                {
                    // Show interview question usage details for default page number 1.
                    LoadInterviewQuestionUsageDetails(1);
                }
                else
                {
                    // Show question usage details for default page number 1.
                    LoadQuestionUsageDetails(1);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(QuestionUsageSummary_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void QuestionUsageSummary_questionDetailsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(QuestionUsageSummary_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void QuestionUsageSummary_questionDetailsGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (QuestionUsageSummary_questionDetailsGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(QuestionUsageSummary_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the question usage details in the grid for
        /// the given page number
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadQuestionUsageDetails(int pageNumber)
        {
            int totalRecords = 0;
            List<TestDetail> tests = new QuestionBLManager().GetQuestionUsageSummary
                (QuestionUsageSummary_questionKeyValueLabel.Text, 
                pageNumber, base.GridPageSize,
                ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]), out totalRecords);

            QuestionUsageSummary_questionDetailsGridView.DataSource = tests;
            QuestionUsageSummary_questionDetailsGridView.DataBind();
            QuestionUsageSummary_pageNavigator.PageSize = base.GridPageSize;
            QuestionUsageSummary_pageNavigator.TotalRecords = totalRecords;

            // Show no data found to display message.
            if (tests == null || tests.Count == 0)
            {
                QuestionUsageSummary_errorMessageLabel.Text = Resources.HCMResource.
                    Common_Empty_Grid;
            }
        }

        /// <summary>
        /// Method that loads the interview question usage details in the grid for
        /// the given page number
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadInterviewQuestionUsageDetails(int pageNumber)
        {
            int totalRecords = 0;
            List<TestDetail> tests = new InterviewQuestionBLManager().GetQuestionUsageSummary
                (QuestionUsageSummary_questionKeyValueLabel.Text,
                pageNumber, base.GridPageSize,
                ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]), out totalRecords);

            QuestionUsageSummary_questionDetailsGridView.DataSource = tests;
            QuestionUsageSummary_questionDetailsGridView.DataBind();
            QuestionUsageSummary_pageNavigator.PageSize = base.GridPageSize;
            QuestionUsageSummary_pageNavigator.TotalRecords = totalRecords;

            // Show no data found to display message.
            if (tests == null || tests.Count == 0)
            {
                QuestionUsageSummary_errorMessageLabel.Text = Resources.HCMResource.
                    Common_Empty_Grid;
            }
        }
 
        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Protected Overridden Methods
    }
}
