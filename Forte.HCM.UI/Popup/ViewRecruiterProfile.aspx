﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewRecruiterProfile.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.ViewRecruiterProfile"
    Title="Recruiter Profile" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ViewRecruiterProfile_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        // Function that handles the profile menu click event.
        function ProfileMenuClick(menuType) 
        {
            // Hide all div.
            document.getElementById('<%=ViewRecruiterProfile_selectedMenuHiddenField.ClientID %>').value = menuType;
            document.getElementById('<%=ViewRecruiterProfile_contactInfoDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=ViewRecruiterProfile_assignmentDetailsDiv.ClientID %>').style.display = 'none';

            // Reset color.
            document.getElementById('<%=ViewRecruiterProfile_contactInfoLinkButton.ClientID %>').className = "assessor_profile_link_button";
            document.getElementById('<%=ViewRecruiterProfile_assignmentDetailsLinkButton.ClientID %>').className = "assessor_profile_link_button";

            if (menuType == 'CI') 
            {
                document.getElementById('<%=ViewRecruiterProfile_contactInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=ViewRecruiterProfile_contactInfoLinkButton.ClientID %>').className = "assessor_profile_link_button_selected";
            }
            else if (menuType == 'AD') 
            {
                document.getElementById('<%=ViewRecruiterProfile_assignmentDetailsDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=ViewRecruiterProfile_assignmentDetailsLinkButton.ClientID %>').className = "assessor_profile_link_button_selected";
            }

            return false;
        }
    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="td_padding_top_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="ViewRecruiterProfile_titleLiteral" runat="server">Recruiter Profile</asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="ViewRecruiterProfile_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <table width="100%" cellpadding="0" cellspacing="3" border="0" class="popupcontrol_view_assessor_profile_bg">
                    <tr>
                        <td style="width: 100%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="ViewRecruiterProfile_messageUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="ViewRecruiterProfile_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknErrorMessage" Width="100%" Text=""></asp:Label>
                                                <asp:Label ID="ViewRecruiterProfile_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknSuccessMessage" Width="100%" Text=""></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 100%">
                                        <asp:UpdatePanel ID="ViewRecruiterProfile_inputControlsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 100%" valign="top" class="popup_td_padding_2">
                                                            <asp:HiddenField runat="server" ID="ViewRecruiterProfile_recruiterIDHiddenField" />
                                                            <asp:HiddenField runat="server" ID="ViewRecruiterProfile_typeHiddenField" />
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" class="view_assessor_profile_assessor_details_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="height: 4px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td rowspan="3" valign="top" align="center" class="view_assessor_profile_photo_border">
                                                                                    <table cellpadding="4" cellspacing="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Image runat="server" ID="ViewRecruiterProfile_photoImage" Width="160px" Height="160px"
                                                                                                    ImageUrl="~/Common/CandidateImageHandler.ashx?source=ASSESSOR_CREA_PAGE" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <table cellpadding="4" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td valign="middle" align="center" class="assessor_profile_left_menu_bg" style="height: 20px">
                                                                                                            <asp:LinkButton ID="ViewRecruiterProfile_sendEmailLinkButton" runat="server" Text="Send Email"
                                                                                                                SkinID="sknAssessorProfileLeftMenuLinkButton" ToolTip="Click here to send an email"></asp:LinkButton>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" colspan="5" style="width: 100%" valign="top" class="view_assessor_profile_title_border">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 90%; height: 50px; padding-left: 10px;" valign="middle">
                                                                                                <asp:Label ID="ViewRecruiterProfile_nameValueLabel" runat="server" SkinID="sknLabelProfileNameText"
                                                                                                    Text=""></asp:Label>
                                                                                            </td>
                                                                                            <td align="right" style="width: 10%; padding-right: 10px;" valign="middle">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" style="height: 42px" valign="top" class="assessor_profile_links_bg">
                                                                                                <table border="0" cellpadding="10px" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:HiddenField ID="ViewRecruiterProfile_selectedMenuHiddenField" runat="server"
                                                                                                                Value="CI" />
                                                                                                            <div style="float: left; padding-top: 5px">
                                                                                                                <asp:LinkButton ID="ViewRecruiterProfile_contactInfoLinkButton" runat="server" Text="Summary"
                                                                                                                    ToolTip="Summary" CssClass="assessor_profile_link_button_selected" OnClientClick="javascript: return ProfileMenuClick('CI')"></asp:LinkButton>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <div style="float: left; padding-top: 5px">
                                                                                                                <asp:LinkButton ID="ViewRecruiterProfile_assignmentDetailsLinkButton" runat="server"
                                                                                                                    ToolTip="Assignments" Text="Assignments" CssClass="assessor_profile_link_button"
                                                                                                                    OnClientClick="javascript: return ProfileMenuClick('AD')"></asp:LinkButton>
                                                                                                            </div>
                                                                                                            <div class="profile_assessments_count_bg">
                                                                                                                <asp:Label ID="ViewRecruiterProfile_assignmentCountLabel" runat="server" Text=""
                                                                                                                    CssClass="profile_assessments_count"></asp:Label></div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="height: 20px">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" style="width: 100%;" class="view_assessor_profile_details_border">
                                                                                    <div style="height: 300px; overflow: auto;">
                                                                                        <div id="ViewRecruiterProfile_contactInfoDiv" runat="server" style="display: block">
                                                                                            <table cellpadding="10" cellspacing="0" style="width: 100%">
                                                                                                <tr>
                                                                                                    <td align="left" style="width: 16%; height: 16px" valign="middle">
                                                                                                        <asp:Label ID="ViewRecruiterProfile_emailLabel" runat="server" SkinID="sknProfileTitleField"
                                                                                                            Text="Email"></asp:Label>
                                                                                                    </td>
                                                                                                    <td align="left" style="width: 28%; padding-left: 10px" valign="middle">
                                                                                                        <asp:Label ID="ViewRecruiterProfile_emailValueLabel" runat="server" SkinID="sknProfileValueField"
                                                                                                            Text=""></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="left" style="width: 16%; height: 16px" valign="middle">
                                                                                                        <asp:Label ID="ViewRecruiterProfile_activePositionAssignmentsLabel" runat="server"
                                                                                                            SkinID="sknProfileTitleField" Text="Current Active Position Assignments"></asp:Label>
                                                                                                    </td>
                                                                                                    <td align="left" style="width: 28%; padding-left: 10px" valign="middle">
                                                                                                        <asp:Label ID="ViewRecruiterProfile_activePositionAssignmentsValueLabel" runat="server"
                                                                                                            SkinID="sknProfileValueField" Text=""></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="left" style="width: 16%; height: 16px" valign="middle">
                                                                                                        <asp:Label ID="ViewRecruiterProfile_recentPositionAssignmentLabel" runat="server"
                                                                                                            SkinID="sknProfileTitleField" Text="Recent Position Assignment Date"></asp:Label>
                                                                                                    </td>
                                                                                                    <td align="left" style="width: 28%; padding-left: 10px" valign="middle">
                                                                                                        <asp:Label ID="ViewRecruiterProfile_recentPositionAssignmentValueLabel" runat="server"
                                                                                                            SkinID="sknProfileValueField" Text=""></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                        <div id="ViewRecruiterProfile_assignmentDetailsDiv" runat="server" style="display: none">
                                                                                            <table cellpadding="10" cellspacing="0" style="width: 100%">
                                                                                                <tr>
                                                                                                    <td align="center" class="empty_border_grid_body_bg">
                                                                                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                                                                            <tr>
                                                                                                                <td align="left" style="width: 100%">
                                                                                                                    <div style="height: 258px; overflow: auto;">
                                                                                                                        <asp:GridView ID="ViewRecruiterProfile_assignmentDetailsGridView" runat="server"
                                                                                                                            AllowSorting="False" AutoGenerateColumns="False" Height="100%" Width="100%" SkinID="sknAssessorProfileGridView"
                                                                                                                            OnRowDataBound="ViewRecruiterProfile_assignmentDetailsGridView_OnRowDataBound">
                                                                                                                            <Columns>
                                                                                                                                <asp:TemplateField HeaderText="Position Profile" ItemStyle-Width="24%" HeaderStyle-HorizontalAlign="Left">
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:HiddenField ID="ViewRecruiterProfile_assignmentDetailsGridView_positionProfileIDHiddenField"
                                                                                                                                            runat="server" Value='<%# Eval("PositionProfileID") %>' />
                                                                                                                                        <asp:LinkButton ID="ViewRecruiterProfile_assignmentDetailsGridView_positionProfileLinkButton"
                                                                                                                                            runat="server" CommandName="view" Text='<%# Eval("PositionProfileName") %>' ToolTip="Click here to view position profile details"></asp:LinkButton>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>
                                                                                                                                <asp:TemplateField HeaderText="Client" ItemStyle-Width="16%" HeaderStyle-HorizontalAlign="Left">
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:HiddenField ID="ViewRecruiterProfile_assignmentDetailsGridView_clientIDHiddenField"
                                                                                                                                            runat="server" Value='<%# Eval("ClientID") %>' />
                                                                                                                                        <asp:LinkButton ID="ViewRecruiterProfile_assignmentDetailsGridView_clientLinkButton"
                                                                                                                                            runat="server" CommandName="view" Text='<%# Eval("ClientName") %>' ToolTip="Click here to view client details"></asp:LinkButton>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>
                                                                                                                                <asp:BoundField DataField="AssignedDateString" HeaderText="Assigned Date" HeaderStyle-HorizontalAlign="Left"
                                                                                                                                    HeaderStyle-Wrap="true" HeaderStyle-Width="10%" />
                                                                                                                                <asp:BoundField DataField="PositionProfileStatusName" HeaderText="Position Status" HeaderStyle-HorizontalAlign="Left"
                                                                                                                                    HeaderStyle-Wrap="true" HeaderStyle-Width="10%" />
                                                                                                                                <asp:BoundField DataField="CandidatesAssociatedByRecruiter" HeaderText="Candidates Associated"
                                                                                                                                    HeaderStyle-HorizontalAlign="Left" HeaderStyle-Wrap="true" HeaderStyle-Width="8%" />
                                                                                                                                <asp:BoundField DataField="CandidatesSubmittedToClientByRecruiter" HeaderText="Candidates Submitted"
                                                                                                                                    HeaderStyle-HorizontalAlign="Left" HeaderStyle-Wrap="true" HeaderStyle-Width="8%" />
                                                                                                                                <asp:BoundField DataField="PositionProfileOwner" HeaderText="Position Profile Owner"
                                                                                                                                    HeaderStyle-HorizontalAlign="Left" HeaderStyle-Wrap="true" HeaderStyle-Width="16%" />
                                                                                                                            </Columns>
                                                                                                                            <EmptyDataTemplate>
                                                                                                                                <table style="width: 100%; height: 100%">
                                                                                                                                    <tr>
                                                                                                                                        <td style="height: 50px">
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td align="center" class="error_message_text_normal" style="height: 100%" valign="middle">
                                                                                                                                            No assignment details available
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </EmptyDataTemplate>
                                                                                                                        </asp:GridView>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <uc1:PageNavigator ID="ViewRecruiterProfile_assignmentDetailsPageNavigator" runat="server" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="ViewRecruiterProfile_bottomControlsUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="left">
                                    <asp:LinkButton ID="ViewRecruiterProfile_cancelLinkButton" runat="server" Text="Cancel"
                                        SkinID="sknPopupLinkButton" OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window"> </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_2">
            </td>
        </tr>
    </table>
</asp:Content>
