﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewAssessorProfile.aspx.cs
// File that represents the user interface layout and functionalities for
// the ViewAssessorProfile page. This will helps to view the assessor 
// profile details.

#endregion Header

#region Directives                                                             

using System;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Configuration;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ViewAssessorProfile page. This will helps to view the assessor 
    /// profile details. This class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ViewAssessorProfile : PageBase
    {
        
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Assessor Profile");

                if (!IsPostBack)
                {
                    // Call and assign query string values to hidden fields.
                    if (RetrieveQueryStringValues() == false)
                        return;

                    // Add handler to view assessor calendar popup.
                    ViewAssessorProfile_viewAssessorCalendar.Attributes.Add("onclick",
                        "return ShowAssessorCalendar('" + ViewAssessorProfile_assessorIDHiddenField.Value + "');");

                    // Set default sort order and field.
                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "CANDIDATE_NAME";

                    // Load assessor details.
                    LoadAssessorDetail(Convert.ToInt32(ViewAssessorProfile_assessorIDHiddenField.Value));

                    // Load assessment details.
                    LoadAssessmentDetails(Convert.ToInt32(ViewAssessorProfile_assessorIDHiddenField.Value), 1);
                }

                // Subscribe to paging event.
                ViewAsssessorProfile_assessmentDetailsPageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler(ViewAsssessorProfile_assessmentDetailsPageNavigator_PageNumberClick);

                // Show menu type.
                ShowMenuType();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewAssessorProfile_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the assessment details grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        private void ViewAsssessorProfile_assessmentDetailsPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                // Load assessment details.
                LoadAssessmentDetails
                    (Convert.ToInt32(ViewAssessorProfile_assessorIDHiddenField.Value), 
                    e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewAssessorProfile_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that retrieves the query string values and assign it to 
        /// hidden fields.
        /// </summary>
        private bool RetrieveQueryStringValues()
        {
            if (Utility.IsNullOrEmpty(Request.QueryString["assessorid"]))
            {
                base.ShowMessage(ViewAssessorProfile_topErrorMessageLabel, "Assessor ID is not passed");
                return false;
            }

            // Get assessor ID.
            int assessorID = 0;
            int.TryParse(Request.QueryString["assessorid"], out assessorID);
            if (assessorID == 0)
            {
                base.ShowMessage(ViewAssessorProfile_topErrorMessageLabel, "Invalid assessor ID");
                return false;
            }

            // Assign assessor ID to hidden field.
            ViewAssessorProfile_assessorIDHiddenField.Value = assessorID.ToString();

            return true;
        }

        /// <summary>
        /// Method that loads the assessor details.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        private void LoadAssessorDetail(int assessorID)
        {
            // Add handler to email assessor image button.
            ViewAssessorProfile_sendEmailLinkButton.Attributes.Add("onclick",
                "return ShowEmailAssessor('" + assessorID + "','PRO');");

            
            // Get assessor detail.
            AssessorDetail assessorDetail = new AssessorBLManager().
                GetAssessorByUserId(assessorID);

            if (assessorDetail == null)
            {
                base.ShowMessage(ViewAssessorProfile_topErrorMessageLabel, "No such assessor exist");
                return;
            }

            // Assign first name.
            ViewAssessorProfile_nameValueLabel.Text = assessorDetail.FirstName;

            // Append last name.
            if (!Utility.IsNullOrEmpty(assessorDetail.LastName))
            {
                ViewAssessorProfile_nameValueLabel.Text = ViewAssessorProfile_nameValueLabel.Text + " " +
                    assessorDetail.LastName;
            }
            ViewAssessorProfile_emailValueLabel.Text = assessorDetail.UserEmail;
            ViewAssessorProfile_alternameEmailValueLabel.Text = assessorDetail.AlternateEmailID;
            ViewAssessorProfile_mobilePhoneValueLabel.Text = assessorDetail.Mobile;
            ViewAssessorProfile_homePhoneValueLabel.Text = assessorDetail.HomePhone;
            ViewAssessorProfile_additionalInformationValueLabel.Text = assessorDetail.AdditionalInfo;

            if (Utility.IsNullOrEmpty(assessorDetail.LinkedInProfile))
                ViewAssessorProfile_linkedInProfileHyperLink.Visible = false;
            else
            {
                ViewAssessorProfile_linkedInProfileHyperLink.Visible = true;
                ViewAssessorProfile_linkedInProfileHyperLink.NavigateUrl = assessorDetail.LinkedInProfile;
            }

            // Load assessor photo
            ViewAssessorProfile_photoImage.ImageUrl = "~/Common/CandidateImageHandler.ashx?source=VIEW_ASS_PHOTO&userid=" + assessorID;


            List<SkillDetail> assessorSkills = null;

            if (assessorDetail.Skills != null && assessorDetail.Skills.Count > 0)
            {
                // Instantiate the list.
                assessorSkills = new List<SkillDetail>();

                // Get distinct categories.
                var distinctCategories = (from skill in assessorDetail.Skills
                                          select new { CategoryID = skill.CategoryID, CategoryName = skill.CategoryName }).Distinct().ToList();

                // Loop through the distict categories and construct the comma 
                // separated skills.
                foreach (var distinctCategory in distinctCategories)
                {
                    string subjects = string.Join(",", (from skill in assessorDetail.Skills where 
                                                            skill.CategoryID == distinctCategory.CategoryID select skill.SkillName).ToArray());

                    // Add to assessor skills.
                    assessorSkills.Add(new SkillDetail(distinctCategory.CategoryID, distinctCategory.CategoryName, subjects));
                }
            }
                
            // Assign skills.
            ViewAssessorProfile_skillsDatalist.DataSource = assessorSkills;
            ViewAssessorProfile_skillsDatalist.DataBind();
        }

        /// <summary>
        /// Method that loads the assessment details.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        private void LoadAssessmentDetails(int assessorID, int pageNumber)
        {
            AssessmentSearchCriteria assessmentSearchCriteria = new AssessmentSearchCriteria();
            assessmentSearchCriteria.InterviewName = null;
            assessmentSearchCriteria.InterviewDescription = null;
            assessmentSearchCriteria.AssessmentStatus = null;
            assessmentSearchCriteria.PositionProfileID = null;

            assessmentSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
            assessmentSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();
            assessmentSearchCriteria.AssessorID = assessorID;
            assessmentSearchCriteria.CurrentPage = pageNumber;

            int totalRecords = 0;

            // Get assessor assessments (completed & in-progress).
            List<AssessorAssessmentDetail> assessmentDetails = new AssessorBLManager().
                GetAssessorAssessments(assessorID,
                pageNumber, base.GridPageSize, out totalRecords);

            ViewAsssessorProfile_assessmentDetailsPageNavigator.TotalRecords = totalRecords;
            ViewAsssessorProfile_assessmentDetailsPageNavigator.PageSize = base.GridPageSize;

            ViewAssessorProfile_assessmentDetailsGridView.DataSource = assessmentDetails;
            ViewAssessorProfile_assessmentDetailsGridView.DataBind();

            // Assign total records to count label.
            ViewAssessorProfile_assessmentCountLabel.Text = totalRecords.ToString();
            ViewAssessorProfile_assessmentCountLabel.ToolTip = string.Format("{0} assessment(s)", totalRecords);
        }

        /// <summary>
        /// Method that shows the menu type and respective section based on the 
        /// selection.
        /// </summary>
        private void ShowMenuType()
        { 
            // Get menu type.
            string menuType = ViewAssessorProfile_selectedMenuHiddenField.Value;

            // Hide all div.
            ViewAssessorProfile_contactInfoDiv.Attributes["style"] = "display:none";
            ViewAssessorProfile_skillsDiv.Attributes["style"] = "display:none";
            ViewAssessorProfile_additionalInfoDiv.Attributes["style"] = "display:none";
            ViewAssessorProfile_assessmentDetailsDiv.Attributes["style"] = "display:none";

            // Reset color.
            ViewAssessorProfile_contactInfoLinkButton.CssClass = "assessor_profile_link_button";
            ViewAssessorProfile_skillsLinkButton.CssClass = "assessor_profile_link_button";
            ViewAssessorProfile_additionalInfoLinkButton.CssClass = "assessor_profile_link_button";
            ViewAssessorProfile_assessmentDetailsLinkButton.CssClass = "assessor_profile_link_button";

            if (menuType == "CI")
            {
                ViewAssessorProfile_contactInfoDiv.Attributes["style"] = "display:block";
                ViewAssessorProfile_contactInfoLinkButton.CssClass = "assessor_profile_link_button_selected";
            }
            else if (menuType == "SK")
            {
                ViewAssessorProfile_skillsDiv.Attributes["style"] = "display:block";
                ViewAssessorProfile_skillsLinkButton.CssClass = "assessor_profile_link_button_selected";
            }
            else if (menuType == "AI")
            {
                ViewAssessorProfile_additionalInfoDiv.Attributes["style"] = "display:block";
                ViewAssessorProfile_additionalInfoLinkButton.CssClass = "assessor_profile_link_button_selected";
            }
            else if (menuType == "AD")
            {
                ViewAssessorProfile_assessmentDetailsDiv.Attributes["style"] = "display:block";
                ViewAssessorProfile_assessmentDetailsLinkButton.CssClass = "assessor_profile_link_button_selected";
            }
        }

        #endregion Private Methods
    }
}