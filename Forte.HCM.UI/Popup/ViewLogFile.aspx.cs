﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.

// ViewScreenShot.cs
// File that represents the ViewScreenShot class that defines the user
// interface layout and functionalities for the tracking details log file
// preview page. This page helps to view the log file taken during a 
// test or interview conduction session.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.IO;
using System.Web.UI;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the ViewLogFile class that defines the user
    /// interface layout and functionalities for the tracking details log file
    /// preview page. This page helps to view the log file taken during a 
    /// test or interview conduction session. This class inherits
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class ViewLogFile : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="string"/> that holds the view type.
        /// </summary>
        private string viewType = null;

        /// <summary>
        /// A <see cref="int"/> that holds the log ID.
        /// </summary>
        private int logID = 0;

        /// <summary>
        /// A <see cref="string"/> that holds the candidate system name, which
        /// helps to construct the file name during download and save.
        /// </summary>
        private string systemName = null;

        #endregion Private Variables                                           

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["viewType"] != null)
                    viewType = Request.QueryString["viewType"];
                if (Request.QueryString["logID"] != null)
                    int.TryParse(Request.QueryString["logID"], out logID);
                if (Request.QueryString["systemName"] != null)
                    systemName = Request.QueryString["systemName"].Trim();

                if (viewType == "CP")
                {
                    Master.SetPageCaption("Cyber Proctor Log File");

                    // Add download log file click handler.
                    ViewLogFile_bottomDownloadLinkButton.Attributes.Add("onclick",
                        "javascript:return DownloadLogFile('CPLOGFILE','" + 
                        + logID + "','" + systemName + "');");
                }
                else if (viewType == "OI")
                {
                    Master.SetPageCaption("Offline Interview Log File");

                    // Add download log file click handler.
                    ViewLogFile_bottomDownloadLinkButton.Attributes.Add("onclick",
                        "javascript:return DownloadLogFile('OILOGFILE','" +
                        +logID + "','" + systemName + "');");
                }

                if (!Page.IsPostBack)
                {
                    // Show preview.
                    ShowPreview();
                }
            }
            catch (Exception ex)
            {
                Logger.TraceLog(ex);
                base.ShowMessage(ViewLogFile_topErrorMessageLabel, ex.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        /// <summary>
        /// Method that shows the preview of the log file.
        /// </summary>
        /// <param name="imageType">
        /// A <see cref="string"/> that holds the image type.
        /// </param>
        private void ShowPreview()
        {
            byte []fileContents = null;

            if (viewType == "CP")
                fileContents = new TrackingBLManager().GetCyberProctorLogFile(logID);
            else if (viewType == "OI")
                fileContents = new TrackingBLManager().GetOfflineInterviewLogFile(logID);

            if (fileContents == null || fileContents.Length == 0)
            {
                ViewLogFile_topErrorMessageLabel.Text = "No log file contents found to display";
                return;
            }

            MemoryStream ms = new MemoryStream(fileContents);
            ViewLogFile_previewTextBox.Text = new StreamReader(ms).ReadToEnd();
        }

        #endregion Private Methods                                             

        #region Overridden Methods                                             

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Overridden Methods                                          
    }
}