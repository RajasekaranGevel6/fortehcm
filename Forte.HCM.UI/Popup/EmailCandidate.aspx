﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailCandidate.aspx.cs"
    Inherits="Forte.HCM.UI.Popup.EmailCandidate" MasterPageFile="~/MasterPages/PopupMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="EmailCandidate_content" ContentPlaceHolderID="PopupMaster_contentPlaceHolder"
    runat="server">
    <script type="text/javascript">

    </script>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <asp:UpdatePanel ID="EmailCandidate_updatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="padding-left: 8px; padding-right: 8px;" valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="height: 10px">
                                        </tr>
                                        <tr>
                                            <td style="width: 50%" class="popup_header_text_grey" align="left">
                                                <asp:Literal ID="EmailCandidate_headerLiteral" runat="server" Text="Email Candidate"></asp:Literal>
                                            </td>
                                            <td style="width: 50%" align="right">
                                                <asp:ImageButton ID="EmailCandidate_cancelImageButton" TabIndex="7" runat="server"  ToolTip="Click here to close the window"
                                                    SkinID="sknCloseImageButton" OnClientClick="javascript:return CloseMe();" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_td_padding_10" valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_email_assessor_inner_bg">
                                        <tr>
                                            <td class="msg_align" style="height: 20px">
                                                <asp:Label ID="EmailCandidate_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="EmailCandidate_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                <asp:HiddenField ID="EmailCandidate_fromHiddenField" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 8px; padding-right: 8px;" valign="top">
                                                <table width="100%" cellpadding="1" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2" >
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="width: 15%" align="left">
                                                                        <asp:Label ID="EmailCandidate_fromLabel" runat="server" Text="From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 55%" align="left">
                                                                        <asp:Label ID="EmailCandidate_fromValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%" align="right">
                                                                        <asp:CheckBox ID="EmailAttachement_sendMeACopyCheckBox" runat="server" Checked="true"
                                                                            TextAlign="Right" Text="Send me a copy" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 15%" align="left">
                                                            <asp:Label ID="EmailCandidate_toLabel" runat="server" Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="width: 97%">
                                                                        <asp:TextBox ID="EmailCandidate_toTextBox" TabIndex="1" runat="server" MaxLength="500"
                                                                            onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)" SkinID="sknMultiLineTextBox"
                                                                            TextMode="MultiLine" Width="99%">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:ImageButton ID="EmailCandidate_toAddressImageButton" runat="server" SkinID="sknMailToImageButton"
                                                                            ToolTip="Click here to select contacts" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td  align="left">
                                                            <asp:Label ID="EmailCandidate_ccLabel" runat="server" Text="CC" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="width: 97%">
                                                                        <asp:TextBox ID="EmailCandidate_ccTextBox" runat="server" Width="99%" TextMode="MultiLine"
                                                                            SkinID="sknMultiLineTextBox" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                                            onchange="CommentsCount(500,this)" TabIndex="2"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:ImageButton ID="EmailCandidate_ccAddressImageButton" runat="server" SkinID="sknMailToImageButton"
                                                                            ToolTip="Click here to select contacts" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td  align="left">
                                                            <asp:Label ID="EmailCandidate_subjectLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="EmailCandidate_subjectTextBox" TabIndex="3" runat="server" MaxLength="100"
                                                                onkeyup="CommentsCount(100,this)" onchange="CommentsCount(100,this)" SkinID="sknMultiLineTextBox"
                                                                Height="23" Width="99%" TextMode="MultiLine">
                                                            </asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 5px" colspan="2">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td valign="top" align="left">
                                                                        <asp:Label ID="EmailCandidate_messageLabel" runat="server" Text="Message" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class="mandatory">*</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 2px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="EmailCandidate_messageTextBox" TabIndex="4" TextMode="MultiLine"
                                                                            Height="284px" runat="server" Width="99%" SkinID="sknMultiLineTextBox" MaxLength="1500"
                                                                            onkeyup="CommentsCount(1500,this)" onchange="CommentsCount(1500,this)" Style="border-bottom: 0px;">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="popup_td_padding_left_8">
                                    <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                <asp:Button ID="EmailCandidate_sendButton" runat="server" Text="Submit" OnClick="EmailCandidate_sendButton_Click"
                                                    SkinID="sknButtonId" TabIndex="5" ToolTip="Click here to send the mail" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="EmailCandidate_cancelLinkButton" runat="server" Text="Cancel" ToolTip="Click here to close the window"
                                                    SkinID="sknPopupLinkButton" TabIndex="6" OnClientClick="javascript:return CloseMe();"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
