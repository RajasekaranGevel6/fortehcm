﻿using System;
using System.IO;
using System.Configuration;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;

namespace Forte.HCM.UI.Popup
{
    public partial class AdditionalDocument : PageBase
    {
        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            Master.SetPageCaption("Candidate Additional Documents");
            try
            {
                ClearControls();
                if (!IsPostBack)
                {
                    //ClearControls();
                    GetDocuments();
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(AdditionalDocument_errorMessageLabel, ex.Message);
            }
        }
        protected void AdditionalDocument_topResetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();

                GetDocuments();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdditionalDocument_errorMessageLabel, exp.Message);
            }

        }
        protected void AdditionalDocument_gridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                AdditionalDocument_successMessageLabel.Text = string.Empty;
                AdditionalDocument_errorMessageLabel.Text = string.Empty;

                HiddenField AdditionalDocument_gridView_documentResumeIdHiddenField =
                            (HiddenField)AdditionalDocument_gridView.Rows[e.RowIndex].FindControl("AdditionalDocument_gridView_documentResumeIdHiddenField");

                new PositionProfileBLManager().DeleteAdditionalDocument(Convert.ToInt32(AdditionalDocument_gridView_documentResumeIdHiddenField.Value));
                base.ShowMessage(AdditionalDocument_successMessageLabel, "Deleted successfully");
                GetDocuments();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdditionalDocument_errorMessageLabel, exp.Message);
            }
        }
        protected void AdditionalDocument_gridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    LinkButton AdditionalDocument_gridView_ppfileNameLinkButton =
                        (LinkButton)e.Row.FindControl("AdditionalDocument_gridView_ppfileNameLinkButton");

                    HiddenField AdditionalDocument_gridView_documentResumeIdHiddenField = (HiddenField)e.Row.FindControl("AdditionalDocument_gridView_documentResumeIdHiddenField");

                    AdditionalDocument_gridView_ppfileNameLinkButton.Attributes.Add("onclick",
                        "return OpenPopUp('" + AdditionalDocument_gridView_ppfileNameLinkButton.Text + "','" + AdditionalDocument_gridView_documentResumeIdHiddenField.Value + "')");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdditionalDocument_errorMessageLabel, exp.Message);
            }
        }
        protected void AdditionalDocument_addDocument_addButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(Session["DOCUMENT_SOURCE"]))
                {
                    ShowMessage(AdditionalDocument_errorMessageLabel, "Select file");
                }

                int candidateId = 0;
                int positionId = 0;
                ClearControls();

                string fileName = string.Empty;

                if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                    positionId = Convert.ToInt32(Request.QueryString["positionprofileid"]);

                if (!Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
                    candidateId = Convert.ToInt32(Request.QueryString["candidateid"]);
                if (Utility.IsNullOrEmpty(Session["FILE_NAME"]))
                {
                    ShowMessage(AdditionalDocument_errorMessageLabel, "Select file");
                    return;
                }
                else
                    fileName = Session["FILE_NAME"].ToString();

                bool flag = new PositionProfileBLManager().InserttPositionProfileAdditionalDocument(
                    positionId, candidateId, base.userID, fileName, (byte[])Session["DOCUMENT_SOURCE"]);

                Session["FILE_NAME"] = "";
                Session["DOCUMENT_SOURCE"] = "";


                if (flag == false)
                {
                    ShowMessage(AdditionalDocument_errorMessageLabel, "File already exists");
                }
                else
                {
                    ShowMessage(AdditionalDocument_successMessageLabel, "File uploaded successfully");
                }
                GetDocuments();

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdditionalDocument_errorMessageLabel, exp.Message);
            }

        }
        protected void AdditionalDocument_addDocumentAsyncFileUpload_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                AdditionalDocument_errorMessageLabel.Text = string.Empty;
                AdditionalDocument_successMessageLabel.Text = string.Empty;

                if (!AdditionalDocument_addDocumentAsyncFileUpload.HasFile)
                {
                    base.ShowMessage(AdditionalDocument_errorMessageLabel, "Select file");
                    return;
                }

                int maxFileSize = Convert.ToInt32
                        (ConfigurationManager.AppSettings["DOCUMENT_MAX_SIZE_IN_BYTES"]);

                if (AdditionalDocument_addDocumentAsyncFileUpload.FileBytes.Length > maxFileSize)
                {
                    base.ShowMessage(AdditionalDocument_errorMessageLabel,
                       string.Format("File size exceeds the maximum limit of {0} bytes", maxFileSize));

                    return;
                }

                Stream documentStream = AdditionalDocument_addDocumentAsyncFileUpload.PostedFile.InputStream;

                byte[] documentSource = StreamToByteArray(documentStream);
                Session["DOCUMENT_SOURCE"] = documentSource;
                Session["FILE_NAME"] = AdditionalDocument_addDocumentAsyncFileUpload.FileName;

                //AdditionalDocument_addDocument_addButton_Click(new object(), new EventArgs());


                //     ClientScript.RegisterStartupScript(this.GetType(), "reg", "javascript:PageReset();", true); 

                /*GetDocuments();
                ClearControls();*/
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdditionalDocument_errorMessageLabel, exp.Message);
            }
        }

        #endregion

        #region Private Methods

        private void GetDocuments()
        {
            int candidateId = 0;
            int positionId = 0;

            if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                positionId = Convert.ToInt32(Request.QueryString["positionprofileid"]);

            if (!Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
                candidateId = Convert.ToInt32(Request.QueryString["candidateid"]);

            AdditionalDocument_gridView.DataSource = null;
            AdditionalDocument_gridView.DataBind();

            AdditionalDocument_gridView.DataSource = new PositionProfileBLManager().
                GetCandidateAdditionalDocuments(positionId, candidateId);
            AdditionalDocument_gridView.DataBind();
        }

        private void ClearControls()
        {
            AdditionalDocument_errorMessageLabel.Text = string.Empty;
            AdditionalDocument_successMessageLabel.Text = string.Empty;
        }

        #endregion

        #region Override Methods
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Public Methdos
        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        #endregion

        #region Static Methods
        static byte[] StreamToByteArray(Stream inputStream)
        {
            if (!inputStream.CanRead)
            {
                throw new ArgumentException();
            }

            // This is optional
            if (inputStream.CanSeek)
            {
                inputStream.Seek(0, SeekOrigin.Begin);
            }

            byte[] output = new byte[inputStream.Length];
            int bytesRead = inputStream.Read(output, 0, output.Length);
            return output;
        }

        #endregion

    }
}