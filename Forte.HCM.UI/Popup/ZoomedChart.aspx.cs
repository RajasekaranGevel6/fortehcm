﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ZoomedChart.cs
// File that represents the user interface for ZoomedChart page.
// This will helps to view the chart in the zoomed mode
//

#endregion

#region Directives                                                             

using System;
using System.Linq;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that defines the user interface layouts 
    /// and functionalities of the zoomed chart page.     
    /// </summary>
    public partial class ZoomedChart : PageBase
    {
        #region Event Handler                                                  
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Page Tittle
                Master.SetPageCaption("Zoomed Chart");
                //Load the chart values 
                LoadValues();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                // Show error messages to the user
                base.ShowMessage(ZoomedChart_topErrorMessageLabel,
                    exception.Message);
            }
        }

        #endregion Event Handler

        #region Override Methods                                               

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>                     
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that is used to bind the chart data
        /// </summary>
        protected override void LoadValues()
        {
            //Defines the chart data
            List<ChartData> chartDatas = null;

            //String that holds the session name
            string sessionId = Request.QueryString["sessionName"];

            if (Session[sessionId] == null)
                return;

            //Initialise the chart data
            chartDatas = new List<ChartData>();

            //Get the chart data source from session
            SingleChartData chartDetails = Session[sessionId] as SingleChartData;

            SetChartHeightAndWidth(chartDetails);

            //Set the header literal text
            ZoomedChart_headerLiteral.Text = chartDetails.ChartTitle;

            ZoomedChart_chart.Series["ZoomedChart_chartPrimarySeries"].ChartType = chartDetails.ChartType;

            chartDatas = chartDetails.ChartData as List<ChartData>;

            ZoomedChart_chart.DataSource = chartDatas;

            //Bind the data to the chart
            ZoomedChart_chart.Series["ZoomedChart_chartPrimarySeries"].Points.
                DataBindXY(chartDatas, "chartXValue", chartDatas, "chartYValue");

            //Set the tool tip 
            ZoomedChart_chart.Series["ZoomedChart_chartPrimarySeries"].ToolTip = "#LABEL  #VAL";

            //Set whether the legend should display or not
            ZoomedChart_chart.Legends[0].Enabled = chartDetails.IsDisplayLegend;


            switch (chartDetails.ChartType)
            {
                case SeriesChartType.Pie:
                    //Disabled the pie label style for the series
                    ZoomedChart_chart.Series["ZoomedChart_chartPrimarySeries"]["PieLabelStyle"] = "Disabled";

                    //Enable the legend
                    ZoomedChart_chart.Legends["ZoomedChart_legend"].Enabled = true;

                    //Assign the legend style
                    ZoomedChart_chart.Legends["ZoomedChart_legend"].LegendStyle = LegendStyle.Column;

                    ZoomedChart_chart.Series["ZoomedChart_chartPrimarySeries"].IsVisibleInLegend = true;

                    //Add first column to the legend
                    LegendCellColumn firstColumn = new LegendCellColumn();

                    firstColumn.ColumnType = LegendCellColumnType.SeriesSymbol;
                    ZoomedChart_chart.Legends["ZoomedChart_legend"].CellColumns.Add(firstColumn);

                    //Add first column to the legend
                    LegendCellColumn secondColumn = new LegendCellColumn();
                    secondColumn.ColumnType = LegendCellColumnType.Text;
                    secondColumn.Text = "#VALX";
                    secondColumn.Alignment = ContentAlignment.MiddleLeft;
                    ZoomedChart_chart.Legends["ZoomedChart_legend"].CellColumns.Add(secondColumn);

                    //Add second column to the legend
                    LegendCellColumn thirdColumn = new LegendCellColumn();
                    thirdColumn.ColumnType = LegendCellColumnType.Text;

                    //Add thrid column to the legend
                    thirdColumn.Text = "#PERCENT";
                    ZoomedChart_chart.Legends["ZoomedChart_legend"].CellColumns.Add(thirdColumn);

                    //Add fourth column to the legend
                    LegendCellColumn fourthColumn = new LegendCellColumn();
                    fourthColumn.ColumnType = LegendCellColumnType.Text;

                    fourthColumn.Text = "(#VAL)";
                    ZoomedChart_chart.Legends["ZoomedChart_legend"].CellColumns.Add(fourthColumn);


                    ZoomedChart_chart.Series["ZoomedChart_chartPrimarySeries"].ToolTip = "#VALX #PERCENT (#VAL)";
                    //ZoomedChart_chart.Series["ZoomedChart_chartPrimarySeries"].Label = "#PERCENT (#VAL)";
                    break;

                case SeriesChartType.Column:
                    //Disabled the pie label style for the series
                    ZoomedChart_chart.Series["ZoomedChart_chartPrimarySeries"]["PieLabelStyle"] = "Enabled";

                    //Show the values as labels in chart
                    ZoomedChart_chart.Series["ZoomedChart_chartPrimarySeries"].IsValueShownAsLabel = true;

                    //Set whether the legend should display or not
                    if (!ZoomedChart_chart.Legends[0].Enabled)
                        ZoomedChart_chart.ChartAreas["ZoomedChart_chartArea"].Position.Width = 100;

                    //Assign the chart x axis title
                    ZoomedChart_chart.ChartAreas["ZoomedChart_chartArea"].AxisX.Title = chartDetails.XAxisTitle;

                    //Assign the chart y axis title
                    ZoomedChart_chart.ChartAreas["ZoomedChart_chartArea"].AxisY.Title = chartDetails.YAxisTitle;

                    //Check if it need to change series 
                    if (chartDetails.IsChangeSeriesColor)
                    {
                        //change the palette name
                        ZoomedChart_chart.Palette = chartDetails.PaletteName;
                    }
                    break;
            }
        }

        #endregion Override Methods

        #region Private Methods                                                
        /// <summary>
        /// Represents the method to set the chart height and width 
        /// </summary>
        /// <param name="chartDetails">
        /// A<see cref="SingleChartData"/>that holds the chart data
        /// </param>
        private void SetChartHeightAndWidth(SingleChartData chartDetails)
        {
            //Initialize the default width
            int defaultLength = Constants.ChartAttributesConstants.CHART_DEFAULT_WIDTH;

            int chartWidth = 0;

            //Initialize the default height
            int defaultHeightCount = Constants.ChartAttributesConstants.CHART_DEFAULT_HEIGHT;

            int chartHeight = 0;

            //Find the maximum length of the x axis variable
            int maxLengthVariable = chartDetails.ChartData.Max(x => x.ChartXValue.Length);

            //if the length of string is greater than 33
            if (maxLengthVariable > defaultLength)
            {
                //Increase the chart Width by 15 px for each character
                chartWidth = (maxLengthVariable - defaultLength) * Constants.ChartAttributesConstants.CHART_INCREASED_WIDTH;

                int defaultChartWidth = int.Parse(ZoomedChart_chart.Width.Value.ToString());

                //set the chart width to new value
                ZoomedChart_chart.Width = Unit.Pixel(chartWidth + defaultChartWidth);
            }

            //check the maximum count of the number of chart data
            if (chartDetails.ChartData.Count > defaultHeightCount)
            {
                defaultHeightCount = chartDetails.ChartData.Count;

                //Increase the chart Width by 20 px for each line
                chartHeight = (defaultHeightCount - Constants.ChartAttributesConstants.CHART_DEFAULT_HEIGHT)
                    * Constants.ChartAttributesConstants.CHART_INCREASED_HEIGHT;

                int defaultChartHeight = int.Parse(ZoomedChart_chart.Height.Value.ToString());

                //set the chart height to new value
                ZoomedChart_chart.Height = Unit.Pixel(chartHeight + defaultChartHeight);
            }
        }

        #endregion Private Methods
    }
}
