﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EmailAttachment.cs
// File that represents the user interface to send emails along with 
// attachments to the recipients. This includes Position profile reivew only.

#endregion Header   

#region Directives                                                             

using System;
using System.IO;
using System.Web;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

using iTextSharp.text;
using iTextSharp.text.pdf;

#endregion Directives

namespace Forte.HCM.UI.Popup
{

    /// <summary>
    /// Clas that represents the user interface to send emails along with 
    /// attachments to the recipients. This includes Position Profile 
    /// Review alone. This class
    /// inherits Forte.HCM.UI.Common.PageBase class.    
    /// </summary>
    public partial class PositionProfileReviewEmail : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="string"/> that holds the chart image path.
        /// </summary>
        private string chartImagePath = string.Empty;

        /// <summary>
        /// A <see cref="string"/> that holds the base URL.
        /// </summary>
        private string baseURL = string.Empty;

        #endregion Private Variables

        #region Private constants                                              
        /// <summary>
        /// A <see cref="int"/> that holds the header detail table index.
        /// </summary>
        private const int HEADER_DETAIL_TABLE_INDEX = 0;
        #endregion Private constants

        #region Event Handlers                                                 

        /// <summary>
        /// Method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set title.
                SetTitle();

                string baseURL = ConfigurationManager.AppSettings["DEFAULT_URL"];

                Response.Cache.SetCacheability(HttpCacheability.NoCache);

                //Load Style Sheets and Skin
                if (!Utility.IsNullOrEmpty(baseURL))
                    chartImagePath = baseURL + "chart/";

                // Set default button field
                Page.Form.DefaultButton = EmailAttachment_submitButton.UniqueID;

                // Validate email address
                EmailAttachment_submitButton.Attributes.Add("onclick", "javascript:return IsValidEmail('"
                    + EmailAttachment_ccTextBox.ClientID + "','"
                    + EmailAttachment_errorMessageLabel.ClientID + "','"
                    + EmailAttachment_successMessageLabel.ClientID + "');");

                EmailAttachment_attachmentImageButton.Attributes.Add("onclick", "javascript:return OpenPopUp('"
                    + Request.QueryString["positionprofileid"] + "','"
                    + Request.QueryString["type"] + "');");

                // Add handler to open 'select contact' popup.
                string positionProfileID = string.Empty;
                if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                    positionProfileID = Request.QueryString["positionprofileid"].Trim();

                EmailAttachment_toAddressImageButton.Attributes.Add
                    ("onclick", "javascript:return ShowSelectContactPopupForReview('" +
                    EmailAttachment_toTextBox.ClientID + "','" + positionProfileID + "','To','N','" + Request.QueryString["usertype"] + "')");

                EmailAttachment_ccAddressImageButton.Attributes.Add
                   ("onclick", "javascript:return ShowSelectContactPopupForReview('" +
                   EmailAttachment_ccTextBox.ClientID + "','" + positionProfileID + "','CC','N','" + Request.QueryString["usertype"] + "')");

                if (!IsPostBack)
                {
                    // Load 
                    // Set default focus field
                    Page.Form.DefaultFocus = EmailAttachment_toTextBox.UniqueID;
                    EmailAttachment_toTextBox.Focus();

                    UserDetail userDetail = new CommonBLManager().GetUserDetail(base.userID);

                    if (userDetail != null)
                    {
                        //EmailAttachment_fromValueLabel.Text = userDetail.FirstName
                        //    + " &lt;" + userDetail.Email + "&gt;";
                        EmailAttachment_fromValueLabel.Text = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                        EmailAttachment_fromHiddenField.Value = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                    }

                    // Retrieve recruiters email list from session.
                    string recruitersSessionKey = "POSITION_PROFILE_REVIEW_RECRUITER_EMAILS_" + positionProfileID;
                    if (!Utility.IsNullOrEmpty(Session[recruitersSessionKey]))
                    {
                        EmailAttachment_toTextBox.Text = Session[recruitersSessionKey].ToString();
                    }

                    // Retrieve position profile name from session.
                    string positionProfileName = string.Empty;
                    string positionProfileNameSessionKey = "POSITION_PROFILE_REVIEW_PROFILE_NAME_" + positionProfileID;
                    if (!Utility.IsNullOrEmpty(Session[positionProfileNameSessionKey]))
                    {
                        positionProfileName = Session[positionProfileNameSessionKey].ToString();
                    }

                    // Call PDF construct method.
                    if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "PP_REVIEW_PDF")
                    {
                        EmailAttachment_HtmlEditor.Visible = false;
                        EmailAttachment_Message_Textbox.Visible = true;
                        EmailAttachment_downloadTypeHiddenField.Value = "PP_REVIEW_PDF";
                        EmailAttachment_attachmentTR.Style.Add("display", "block");
                        EmailAttachment_subjectTextBox.Text = string.Format("Review of position profile '{0}'", positionProfileName);
                        EmailAttachment_messageTextBox.Text = string.Format("Attached: Review of position profile '{0}'", positionProfileName);
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "PP_REVIEW")
                    {
                        string sessionKey = "POSITION_PROFILE_REVIEW_" + positionProfileID;
                        PositionProfilePrint_editor.Content = Session[sessionKey].ToString();
                        EmailAttachment_HtmlEditor.Visible = true;
                        EmailAttachment_Message_Textbox.Visible = false;
                        EmailAttachment_attachmentLabel.Visible = false;
                        EmailAttachment_subjectTextBox.Text = string.Format("Review of position profile '{0}'", positionProfileName);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EmailAttachment_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will be called when the submit button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void EmailAttachment_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear all the error and success labels
                EmailAttachment_errorMessageLabel.Text = string.Empty;
                EmailAttachment_successMessageLabel.Text = string.Empty;
                string fileName = string.Empty;
                bool isPP_Review_PDF_Sent = false;
                bool isPP_Review_Sent = false;

                // Validate the fields
                if (!IsValidData())
                    return;

                // Build Mail Details
                MailDetail mailDetail = new MailDetail();
                MailAttachment mailAttachment = new MailAttachment();
                mailDetail.Attachments = new List<MailAttachment>();
                mailDetail.To = new List<string>();
                mailDetail.CC = new List<string>();

                // Add the logged in user in the CC list when 'send me a copy ' is on
                if (EmailAttachement_sendMeACopyCheckBox.Checked)
                {
                    mailDetail.CC.Add(base.userEmailID);
                }

                if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                   Request.QueryString["type"].ToUpper() == "PP_REVIEW_PDF")
                {
                    int positionProfileID = 0;
                    if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                    {
                        positionProfileID = Convert.ToInt32(Request.QueryString["positionprofileid"].Trim());
                    }
                    
                    // Get raw data.
                    CandidateReportDetail candidateReportDetail;
                    PDFDocumentWriter PdfWriter = new PDFDocumentWriter();
                    string logoPath = Server.MapPath("~/");
                    PDFHeader(out candidateReportDetail);
                    DataSet rawData = new PositionProfileBLManager().GetPositionProfileSummary(positionProfileID, base.userID);
                    PDFHeader(out candidateReportDetail);
                    DataTable TableHeader = rawData.Tables[HEADER_DETAIL_TABLE_INDEX];
                    DataRow dataRow = TableHeader.Rows[0];
                    string fileName_PP = dataRow["POSITION_PROFILE_NAME"].ToString().Trim();
                    mailAttachment.FileContent = PdfWriter.GetPostionprofileReview(rawData, logoPath, candidateReportDetail);
                    mailAttachment.FileName = fileName_PP.Replace(" ", "_") + ".pdf";
                    mailDetail.Attachments.Add(mailAttachment);
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                    isPP_Review_Sent = true;
                }

                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                            Request.QueryString["type"].ToUpper() == "PP_REVIEW")
                {
                    
                    string message = PositionProfilePrint_editor.Content.Trim();
                    
                    // Remove breaks.
                    message = message.Replace("<br />", "");
                    message = message.Replace("<br/>", "");

                    // Assign message.
                    mailDetail.isBodyHTMLPositionProfile = false;
                    mailDetail.Message = message;
                    mailDetail.isBodyHTML = true;
                    isPP_Review_PDF_Sent = true;
                }

                // Add 'To' mail addresses
                string[] toMailIDs = EmailAttachment_toTextBox.Text.Split(new char[] { ',' });
                for (int idx = 0; idx < toMailIDs.Length; idx++)
                    mailDetail.To.Add(toMailIDs[idx].Trim());

                if (!Utility.IsNullOrEmpty(EmailAttachment_ccTextBox.Text.Trim()))
                    mailDetail.CC.Add(EmailAttachment_ccTextBox.Text.Trim());

                //mailDetail.From = EmailAttachment_fromHiddenField.Value.Trim();
                // Set the 'from' address to default address.
                mailDetail.From = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];

                mailDetail.Subject = EmailAttachment_subjectTextBox.Text.Trim();

                // mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                bool isMailSent = false;

                try
                {
                    // Send mail
                    new EmailHandler().SendMail(mailDetail);
                    isMailSent = true;
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    isMailSent = false;
                }
                //Script to close the popup window
                string closeScript = "self.close();";

                if (isMailSent && isPP_Review_Sent)
                    //base.ShowMessage(EmailAttachment_successMessageLabel, "Published URL mailed successfully");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true);
                else if (isMailSent && isPP_Review_PDF_Sent)
                    //base.ShowMessage(EmailAttachment_successMessageLabel,
                    //    Resources.HCMResource.EmailAttachment_TestResultsSentSuccessfully);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true);
                else if (isMailSent)
                    //base.ShowMessage(EmailAttachment_successMessageLabel,
                    //    Resources.HCMResource.EmailAttachment_TestResultsSentSuccessfully);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true);
                else
                    base.ShowMessage(EmailAttachment_errorMessageLabel,
                        Resources.HCMResource.EmailAttachment_MailCouldNotSend);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EmailAttachment_errorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {

            bool isValidData = true;

            if (EmailAttachment_toTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(EmailAttachment_errorMessageLabel,
                    Resources.HCMResource.EmailAttachment_ToAddressCannotBeEmpty);
                isValidData = false;
            }
            else
            {
                string[] toMailIDs = EmailAttachment_toTextBox.Text.Split(new char[] { ',' });

                for (int idx = 0; idx < toMailIDs.Length; idx++)
                {
                    if (!Regex.IsMatch(
                        toMailIDs[idx].Trim(), @"^([\w-]+\.)*?[\w-]+@[\w-]+\.([\w-]+\.)*?[\w]+$"))
                    {
                        base.ShowMessage(EmailAttachment_errorMessageLabel,
                            Resources.HCMResource.EmailAttachment_InvalidEmailAddress);
                        isValidData = false;
                        break;
                    }
                }
            }
            // Validate reason feild
            if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "PP_REVIEW_PDF")
            {
                if (EmailAttachment_messageTextBox.Text.Trim().Length == 0)
                {
                    base.ShowMessage(EmailAttachment_errorMessageLabel,
                        Resources.HCMResource.EmailAttachment_MessageCannotBeEmpty);
                    isValidData = false;
                }
            }

            return isValidData;
        }

        /// <summary>
        /// Overridden protected method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion  Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that get user details from candidate report details
        /// to add footer information
        /// </summary>
        /// <param name="candidateReportDetail"></param>
        private void PDFHeader(out CandidateReportDetail candidateReportDetail)
        {
            candidateReportDetail = new CandidateReportDetail();
            UserDetail userDetail = new UserDetail();
            userDetail = (UserDetail)Session["USER_DETAIL"];
            candidateReportDetail.LoginUser = userDetail.FirstName + " " + userDetail.LastName;
        }

        /// <summary>
        /// Method that sets the page title and caption.
        /// </summary>
        private void SetTitle()
        {
            // Set the page title and caption based on type.
            if ((!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                Request.QueryString["type"].Trim().ToUpper() == "PP_REVIEW_PDF") ||
                (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "PP_REVIEW"))
            {
                Master.SetPageCaption("Email Position Profile Review");
                EmailAttachment_headerLiteral.Text = "Email Position Profile Review";
            }
         
        }

        #endregion Private Methods
    }
}

    
