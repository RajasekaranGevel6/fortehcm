<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ZoomedChart.aspx.cs" MasterPageFile="~/MasterPages/PopupMaster.Master"
    Inherits="Forte.HCM.UI.Popup.ZoomedChart"  %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ZoomedChart_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 90%" class="popup_header_text_grey">
                            <asp:Literal ID="ZoomedChart_headerLiteral" runat="server" Text=""></asp:Literal>
                        </td>
                        <td style="width: 10%" align="right">
                            <asp:ImageButton ID="ZommedChart_topCloseImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="ZoomedChart_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="ZoomedChart_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                    ID="SearchQuestion_stateHiddenField" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table cellpadding="0" cellspacing="0" width="100%" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td align="center" class="popup_td_padding_10">
                            <div style="height: 460px; overflow: auto; width: 900px">
                                <asp:Chart ID="ZoomedChart_chart" runat="server" BackColor="Transparent" Palette="Pastel"
                                    Width="900px" Height="450px" AntiAliasing="Graphics">
                                    <Legends>
                                        <asp:Legend Name="ZoomedChart_legend" BackColor="Transparent" IsEquallySpacedItems="True"
                                            Font="Verdana, 9.25pt" IsTextAutoFit="False" Alignment="Center" ForeColor="114, 142, 192"
                                            Docking="Right" IsDockedInsideChartArea="False" ItemColumnSpacing="80" MaximumAutoSize="100">
                                        </asp:Legend>
                                    </Legends>
                                    <Titles>
                                        <asp:Title Name="ZoomedChart_chartTitle">
                                        </asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="ZoomedChart_chartPrimarySeries" BorderColor="64, 64, 64, 64" IsValueShownAsLabel="false"
                                            Font="Helvetica, 11px">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ZoomedChart_chartArea" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                                            BackColor="Transparent" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                                            <Position Height="91" Width="50" />
                                            <AxisX Interval="1">
                                                <MajorGrid Enabled="false" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid Enabled="false" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_20">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_10">
                <asp:LinkButton ID="ZoomedChart_cancelLinkButton" SkinID="sknPopupLinkButton" runat="server"
                    Text="Cancel" OnClientClick="javascript:CloseMe()" />
            </td>
        </tr>
    </table>
</asp:Content>
