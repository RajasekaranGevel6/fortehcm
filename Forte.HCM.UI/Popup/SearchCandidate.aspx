<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="SearchCandidate.aspx.cs" Inherits="Forte.HCM.UI.Popup.SearchCandidate" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>

<asp:Content ID="SearchClientRequest_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl) {
            
            var nameCtrl = '<%= Request.QueryString["namectrl"] %>';
            var emailCtrl = '<%= Request.QueryString["emailctrl"] %>';
            var idCtrl = '<%= Request.QueryString["idctrl"] %>';
            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Set the user name field value. Replace the 'link button name' with the 
            // 'user name hidden field name' and retrieve the value.
            if (nameCtrl != null && nameCtrl != '') {
                window.opener.document.getElementById(nameCtrl).value
                    = document.getElementById(ctrl.id.replace("SearchCandidate_selectLinkButton", "SearchCandidate_userNameHiddenfield")).value;
            }

            // Set the email hidden field value. Replace the 'link button name' with the 
            // 'eamil hidden field name' and retrieve the value.
            if (emailCtrl != null && emailCtrl != '') {
                window.opener.document.getElementById(emailCtrl).value
                    = document.getElementById(ctrl.id.replace("SearchCandidate_selectLinkButton", "SearchCandidate_emailHiddenfield")).value;
            }

            // Set the user ID hidden field value. Replace the 'link button name' with the 
            // 'user ID hidden field name' and retrieve the value.
            if (idCtrl != null && idCtrl != '') {
                window.opener.document.getElementById(idCtrl).value
                    = document.getElementById(ctrl.id.replace("SearchCandidate_selectLinkButton", "SearchCandidate_userIDHiddenfield")).value;
                
            }
            self.close();
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchCandidate_headerLiteral" runat="server" Text="Search Candidate"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SearchCandidate_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:UpdatePanel ID="SearchCandidate_messageUpdatePanel" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <asp:Label ID="SearchCandidate_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="SearchCandidate_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchCandidate_searchButton" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchCandidate_pageNavigator" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="SearchCandidate_searchCriteriasDiv" runat="server" style="display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="SearchCandidate_searchCriteriaUpdatePanel" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <table width="100%" border="0" cellspacing="3%" cellpadding="2%">
                                                                    <tr>
                                                                       
                                                                        <td>
                                                                            <asp:Label ID="SearchCandidate_userNameLabel" runat="server" Text="User Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchCandidate_userNameTextBox" MaxLength="50"  runat="server"></asp:TextBox>
                                                                        </td>
                                                                         <td>
                                                                            <asp:Label ID="SearchCandidate_nameLabel" runat="server" 
                                                                                SkinID="sknLabelFieldHeaderText" Text="Name"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchCandidate_nameTextBox" MaxLength="50"  runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="SearchCandidate_emailLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchCandidate_emailTextBox" MaxLength="50"  runat="server"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="6">
                                                                            <asp:Button ID="SearchCandidate_searchButton" runat="server" SkinID="sknButtonId"
                                                                                Text="Search" OnClick="SearchCandidate_searchButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="SearchCandidate_searchResultsUpdatePanel" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr id="SearchCandidate_searchTestResultsTR" runat="server">
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 50%">
                                                                        <asp:Literal ID="SearchCandidate_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                            ID="SearchQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 50%" align="right">
                                                                        <span id="SearchCandidate_searchResultsUpSpan" style="display: none;" runat="server">
                                                                            <asp:Image ID="SearchCandidate_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                        </span><span id="SearchCandidate_searchResultsDownSpan" style="display: block;" runat="server">
                                                                            <asp:Image ID="SearchCandidate_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="height: 220px; overflow: auto;" runat="server" id="SearchCandidate_testDiv">
                                                                            <asp:GridView ID="SearchCandidate_testGridView" runat="server" AllowSorting="True"
                                                                                AutoGenerateColumns="False" GridLines="Horizontal" BorderColor="white" BorderWidth="1px"
                                                                                Width="100%" OnSorting="SearchCandidate_testGridView_Sorting" OnRowDataBound="SearchCandidate_testGridView_RowDataBound"
                                                                                OnRowCreated="SearchCandidate_testGridView_RowCreated" OnRowCommand="SearchCandidate_testGridView_RowCommand">
                                                                                <RowStyle CssClass="grid_alternate_row" />
                                                                                <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                                <HeaderStyle CssClass="grid_header_row" />
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-Width="22%">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="SearchCandidate_selectLinkButton" runat="server" Text="Select"
                                                                                                OnClientClick="javascript:return OnSelectClick(this);" ToolTip="Select" Visible='<% # Convert.ToBoolean(GetInverseVisibility(Convert.ToString(Eval("UserName"))))  %>'></asp:LinkButton>
                                                                                            <asp:HiddenField ID="SearchCandidate_userNameHiddenfield" runat="server" Value='<%# Eval("FirstName") %>' />
                                                                                            <asp:HiddenField ID="SearchCandidate_userIDHiddenfield" runat="server" Value='<%# Eval("UserID") %>' />
                                                                                            <asp:HiddenField ID="SearchCandidate_emailHiddenfield" runat="server" Value='<%# Eval("Email") %>' />
                                                                                            <asp:HiddenField ID="SearchCandidate_candidateDetailGridView_candidateIDHiddenField"
                                                                                                runat="server" Value='<%# Eval("CandidateID") %>' />
                                                                                            <asp:ImageButton ID="SearchCandidate_candidateDetailGridView_addNotesImageButton"
                                                                                                runat="server" SkinID="sknAddNotesImageButton" ToolTip="Add Notes" />
                                                                                            <asp:ImageButton ID="SearchCandidate_candidateDetailGridView_viewCandidateActivityImageButton"
                                                                                                runat="server" SkinID="sknViewCandidateActivityLogImageButton" ToolTip="View Candidate Activity Log" />
                                                                                            <asp:ImageButton ID="SearchCandidate_candidateDetailGridView_addCandidateIDImageButton"
                                                                                                runat="server" CommandName="AddCandidateID" CommandArgument='<%# Eval("CandidateID") %>'
                                                                                                SkinID="sknCreateCandidateUserNameImageButton" Visible='<% # Convert.ToBoolean(GetVisibility(Convert.ToString(Eval("UserName"))))  %>'
                                                                                                ToolTip="Create User Name" />                                                                                            
                                                                                            <asp:HyperLink ID="SearchCandidate_viewCandidateActivityDashboardHyperLink" runat="server" Target="_blank" 
                                                                                                ImageUrl="~/App_Themes/DefaultTheme/Images/mail_icon_to.png" ToolTip="View Candidate Activity Dashboard" >
                                                                                                </asp:HyperLink>
                                                                                          </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="User Name" SortExpression="USERNAME" HeaderStyle-Width="14%"
                                                                                        ItemStyle-Width="14%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchCandidate_candidateDetailGridView_usernameLabel" runat="server"
                                                                                                Text='<%# Eval("UserName") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="First Name" SortExpression="FIRSTNAME" HeaderStyle-Width="14%"
                                                                                        ItemStyle-Width="14%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchCandidate_candidateDetailGridView_firstNameLabel" runat="server"
                                                                                                Text='<%# Eval("FirstName") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="User ID" DataField="UserID" Visible="false" />
                                                                                    <asp:BoundField HeaderText="" DataField="" SortExpression="" />
                                                                                    <asp:TemplateField HeaderText="Middle Name" SortExpression="MIDDLENAME" HeaderStyle-Width="14%"
                                                                                        ItemStyle-Width="14%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchCandidate_candidateDetailGridView_middleNameLabel" runat="server"
                                                                                                Text='<%# Eval("MiddleName") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Last Name" SortExpression="LASTNAME" HeaderStyle-Width="14%"
                                                                                        ItemStyle-Width="14%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchCandidate_candidateDetailGridView_lastNameLabel" runat="server"
                                                                                                Text='<%# Eval("LastName") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Email" SortExpression="EMAIL" HeaderStyle-Width="14%"
                                                                                        ItemStyle-Width="14%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchCandidate_candidateDetailGridView_emailLabel" runat="server"
                                                                                                Text='<%# Eval("Email") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <%--<asp:TemplateField HeaderText="Recruited By" SortExpression="RECRUITEDBY" HeaderStyle-Width="18%"
                                                                                        ItemStyle-Width="16%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchCandidate_candidateDetailGridView_recruitedByLabel" runat="server"
                                                                                                Text='<%# Eval("RecruitedBy") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>--%>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <uc1:PageNavigator ID="SearchCandidate_pageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchCandidate_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_10">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="SearchCandidate_topResetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="SearchCandidate_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="SearchCandidate_topCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="SearchCandidate_isMaximizedHiddenField" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="SearchCandidate_modelPopupUpdatePanel">
                    <ContentTemplate>
                        <asp:Panel ID="SearchCandidate_createUserNamePanel" runat="server" Style="display: block"
                            CssClass="popupcontrol_search_candidate_username_detail">
                            <div style="display: none">
                                <asp:Button ID="SearchCandidate_hiddenButton" runat="server" Text="Hidden" /></div>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Label ID="SearchCandidate_createUserHeaderLiteral" runat="server" Text="Create User Name"></asp:Label>
                                                </td>
                                                <td style="width: 25%" align="right">
                                                    <asp:ImageButton ID="SearchCandidate_createUserPanelcancelImageButton" runat="server"
                                                        SkinID="sknCloseImageButton" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                            <tr>
                                                <td class="popup_td_padding_10">
                                                    <table width="100%" cellpadding="2" cellspacing="3">
                                                        <tr>
                                                            <td colspan="2" class="msg_align">
                                                                <asp:Label ID="SearchCandidate_createUserNameSuccessMessageLabel" runat="server"
                                                                    SkinID="sknSuccessMessage"></asp:Label>
                                                                <asp:Label ID="SearchCandidate_createUserNameErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 33%">
                                                                <asp:Label ID="SearchCandidate_createUserNamePanel_userNameLabel" Text="User Name"
                                                                    runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td style="width: 67%">
                                                                <asp:TextBox ID="SearchCandidate_createUserNamePanel_userNameTextBox" runat="server"
                                                                    Width="100%" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <div style="float: left; padding-right: 5px;">
                                                                    <asp:LinkButton ID="SearchCandidate_createUserNamePanel_checkUserEmailIdAvailableButton"
                                                                        CssClass="link_button_hcm" runat="server" Text="Check" OnClick="SearchCandidate_createUserNamePanel_checkUserEmailIdAvailableButton_Click"></asp:LinkButton>
                                                                </div>
                                                                <div style="float: left;">
                                                                    <div id="SearchCandidate_createUserNamePanel_userEmailAvailableStatusDiv" runat="server"
                                                                        style="display: none;">
                                                                        <asp:Label ID="SearchCandidate_createUserNamePanel_inValidEmailAvailableStatus" runat="server"
                                                                            EnableViewState="false" ForeColor="Red"></asp:Label>
                                                                        <asp:Label ID="SearchCandidate_createUserNamePanel_validEmailAvailableStatus" runat="server"
                                                                            EnableViewState="false" ForeColor="Green"></asp:Label>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="SearchCandidate_createUserNamePanel_passwordLabel" Text="Password"
                                                                    runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="SearchCandidate_createUserNamePanel_passwordTextBox" runat="server"
                                                                    Width="100%" MaxLength="50" TextMode="Password"></asp:TextBox>
                                                                <asp:HiddenField ID="SearchCandidate_createUserNamePanel_firstNameHiddenField" runat="server" />
                                                                <asp:HiddenField ID="SearchCandidate_createUserNamePanel_lastNameHiddenField" runat="server" />
                                                                <asp:HiddenField ID="SearchCandidate_createUserNamePanel_candidateIDHiddenField"
                                                                    runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="SearchCandidate_createUserNamePanel_retypePasswordLabel" Text="Retype Password"
                                                                    runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="SearchCandidate_createUserNamePanel_retypePasswordTextBox" runat="server"
                                                                    Width="100%" TextMode="Password"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="left">
                                                    <asp:Button ID="SearchCandidate_createUserNamePanel_saveButton" runat="server" Text="Save"
                                                        SkinID="sknButtonId" OnClick="SearchCandidate_createUserNamePanel_saveButton_Click" />
                                                    &nbsp;
                                                    <asp:LinkButton ID="SearchCandidate_createUserNamePanel_closeButton" runat="server"
                                                        Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_10">
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="SearchCandidate_createUserModalPopupExtender"
                            runat="server" PopupControlID="SearchCandidate_createUserNamePanel" TargetControlID="SearchCandidate_hiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
