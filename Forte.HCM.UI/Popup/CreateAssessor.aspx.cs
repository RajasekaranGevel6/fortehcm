﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreateAssessor.aspx.cs
// File that the represents the user interface layout and functionalities for
// the CreateAssessor page. This will helps to convert an existing 
// corporate user to an assessor. The assessor details such as alternate
// email, skills, linkedin profile, photo, etc are given as input. This
// class inherits Forte.HCM.UI.Common.PageBase.

#endregion Header

#region Directives

using System;
using System.IO;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Threading;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the CreateAssessor page. This will helps to convert an existing 
    /// corporate user to an assessor. The assessor details such as alternate
    /// email, skills, linkedin profile, photo, etc are given as input. This
    /// class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class CreateAssessor : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Create Assessor");

                // Set default button.
                Page.Form.DefaultButton = CreateAssessor_addSkillImageButton.UniqueID;

                if (!IsPostBack)
                {
                    // Set the focus to skills text box.
                    CreateAssessor_skillsTextBox.Focus();

                    // Call and assign query string values to hidden fields.
                    if (RetrieveQueryStringValues() == false)
                        return;

                    // Set default focucs

                    // Clear session.
                    Session["CREATE_ASSESSOR_SKILLS"] = null;

                    CreateAssessor_searchSkillImageButton.Attributes.Add("onclick",
                        "return LoadSkillLookup('" + CreateAssessor_categoryIDHiddenField.ClientID + "','" +
                        CreateAssessor_skillsTextBox.ClientID + "','" +
                        CreateAssessor_skillIDHiddenField.ClientID + "','" +
                        CreateAssessor_addSkillImageButton.ClientID + "');");

                    // Load user detail.
                    LoadUserDetails();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAssessor_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the skills grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CreateAssessor_skillsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAssessor_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the skills grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void CreateAssessor_skillsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "DeleteSkill")
                    return;

                int index = Convert.ToInt32(e.CommandArgument);

                // Retrieve the skill details list from session.
                List<SkillDetail> skillDetails = null;
                if (Session["CREATE_ASSESSOR_SKILLS"] != null)
                {
                    skillDetails = Session["CREATE_ASSESSOR_SKILLS"] as List<SkillDetail>;
                }

                if (skillDetails == null)
                    return;

                // Check if the delete index is present.
                if (index >= skillDetails.Count)
                {
                    base.ShowMessage(CreateAssessor_topErrorMessageLabel, "No skill found to delete");
                    return;
                }

                // Delete the time slot from the list.
                skillDetails.RemoveAt(index);

                // Keep the skills in session.
                if (skillDetails.Count == 0)
                    Session["CREATE_ASSESSOR_SKILLS"] = null;
                else
                    Session["CREATE_ASSESSOR_SKILLS"] = skillDetails;

                // Assign the skills to the grid view.
                CreateAssessor_skillsGridView.DataSource = skillDetails;
                CreateAssessor_skillsGridView.DataBind();

                // Show success message.
                base.ShowMessage(CreateAssessor_topErrorMessageLabel, "Skill deleted successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAssessor_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the add skill image button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        protected void CreateAssessor_addSkillImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            try
            {
                // Check if skill is entered.
                if (Utility.IsNullOrEmpty(CreateAssessor_skillsTextBox.Text))
                {
                    base.ShowMessage(CreateAssessor_topErrorMessageLabel,
                        "No skill entered to add");
                    return;
                }

                List<SkillDetail> skillDetails = null;
                
                if (Session["CREATE_ASSESSOR_SKILLS"] == null)
                {
                    skillDetails = new List<SkillDetail>();
                }
                else
                    skillDetails = Session["CREATE_ASSESSOR_SKILLS"] as List<SkillDetail>;

                string []skills = CreateAssessor_skillsTextBox.Text.Trim().Split(',');

                foreach (string skill in skills)
                {
                    if (skill.Trim().Length == 0)
                        continue;

                    // Retrieve skill detail.
                    SkillDetail skillDetail = new AssessorBLManager().GetSkillDetail
                        (skill.Trim());

                    // Check if skill detail is null.
                    if (skillDetail == null)
                    {
                       continue;
                    }

                    // Check if skill detail already selected.
                    var findSkill = from sk in skillDetails
                               where sk.SkillID == skillDetail.SkillID
                               select sk;

                    if (findSkill != null && findSkill.Count() > 0)
                    {
                        base.ShowMessage(CreateAssessor_topErrorMessageLabel, 
                            string.Format("Skill '{0}' already added", findSkill.First().SkillName));

                        continue;
                    }

                    // Add to the skills list.
                    skillDetails.Add(skillDetail);
                }

                // Keep the skills in session.
                Session["CREATE_ASSESSOR_SKILLS"] = skillDetails;

                // Assign the skills to the grid view.
                CreateAssessor_skillsGridView.DataSource = skillDetails;
                CreateAssessor_skillsGridView.DataBind();

                // Clear skills text box.
                CreateAssessor_skillsTextBox.Text = string.Empty;

                // Set the focus to skills text box.
                CreateAssessor_skillsTextBox.Focus();

                // Clear hidden fields.
                CreateAssessor_categoryIDHiddenField.Value = string.Empty;
                CreateAssessor_skillIDHiddenField.Value = string.Empty;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAssessor_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the create button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void CreateAssessor_createButton_Click(object sender, EventArgs e)
        {
            try
            {
                AssessorDetail assessorDetail = new AssessorDetail();
                assessorDetail.UserID = Convert.ToInt32(CreateAssessor_userIDHiddenField.Value);
                assessorDetail.UserEmail = CreateAssessor_userNameTextBox.Text;
                assessorDetail.Skill = "";
                assessorDetail.AlternateEmailID = CreateAssessor_alternateEmailIDTextBox.Text;
                assessorDetail.FirstName = CreateAssessor_firstNameTextBox.Text;
                assessorDetail.LastName = CreateAssessor_lastNameTextBox.Text;
                assessorDetail.CreatedBy = base.userID;
                assessorDetail.Mobile = CreateAssessor_mobilePhoneTextBox.Text;
                assessorDetail.HomePhone = CreateAssessor_homePhoneTextBox.Text;

                List<SkillDetail> skillDetails = null;

                if (Session["CREATE_ASSESSOR_SKILLS"] != null)
                    skillDetails = Session["CREATE_ASSESSOR_SKILLS"] as List<SkillDetail>;

                if (skillDetails == null || skillDetails.Count == 0)
                {
                    base.ShowMessage(CreateAssessor_topErrorMessageLabel, "Skills cannot be empty");
                    return;
                }
                    
                // Save assessor.
                new AssessorBLManager().SaveAssessor(assessorDetail, skillDetails);

                // Close the window.
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "Closeform", "<script language='javascript'>CloseCreateAssessorWindow();</script>", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreateAssessor_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that retrieves the query string values and assign it to 
        /// hidden fields.
        /// </summary>
        private bool RetrieveQueryStringValues()
        {
            // Check if assessor ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["userid"]))
            {
                base.ShowMessage(CreateAssessor_topErrorMessageLabel, "User ID is not passed");
                return false;
            }

            // Assign user ID.
            CreateAssessor_userIDHiddenField.Value = Request.QueryString
                ["userid"].ToString().Trim();

            return true;
        }

        /// <summary>
        /// Method that loads the basic user details.
        /// </summary>
        private void LoadUserDetails()
        {
            // Retrieve and assign basic user details.
            UserDetail userDetail = new CommonBLManager().GetUserDetail
                (Convert.ToInt32( CreateAssessor_userIDHiddenField.Value));
            CreateAssessor_userNameTextBox.Text = userDetail.UserName;
            CreateAssessor_firstNameTextBox.Text = userDetail.FirstName;
            CreateAssessor_lastNameTextBox.Text = userDetail.LastName;
        }

        /// <summary>
        /// Validates the image.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        private bool IsValidImage(HttpPostedFile file)
        {
            string[] arrFileName = null;
            string fileExt = string.Empty;
            bool retValue = true;
            arrFileName = file.FileName.Split('.');
            fileExt = arrFileName[arrFileName.Length - 1];
            if (fileExt.ToUpper() != "JPG" && fileExt.ToUpper() != "JPEG"
                   && fileExt.ToUpper() != "PNG" && fileExt.ToUpper() != "GIF")
                retValue = false;

            return retValue;
        }

        /// <summary>
        /// Checks the size of the file.
        /// </summary>
        /// <param name="size">The size.</param>
        /// <returns></returns>
        private bool IsValidFileSize(int size)
        {
            int fileSize = 0;
            bool retValue = true;
            const int byteConversion = 1024;
            fileSize = size / byteConversion;
            if (fileSize > 500)
                retValue = false;
            return retValue;
        }

        /// <summary>
        /// Deletes the image.
        /// </summary>
        /// <param name="pathName">Name of the path.</param>
        /// <param name="imageName">Name of the image.</param>
        private void DeleteFile(string pathName, string fileName)
        {
            if (File.Exists(string.Concat(HttpContext.Current.Server.MapPath(pathName), fileName)))
                File.Delete(string.Concat(HttpContext.Current.Server.MapPath(pathName), fileName));
        }

        /// <summary>
        /// Copy the image.
        /// </summary>
        /// <param name="pathName">Name of the path.</param>
        /// <param name="imageName">Name of the image.</param>
        private void CopyFile(string pathName, string fileName)
        {
            File.Copy(HttpContext.Current.Server.MapPath(pathName), fileName, true);
        }

        #endregion Private Methods
    }
}