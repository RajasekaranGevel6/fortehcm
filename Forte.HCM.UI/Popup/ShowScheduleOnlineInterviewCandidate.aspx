<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowScheduleOnlineInterviewCandidate.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.ShowScheduleOnlineInterviewCandidate" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/InterviewQuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc4" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="SearchClientRequest_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">
        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl) {
            var nameCtrl = '<%= Request.QueryString["namectrl"] %>';
            var emailCtrl = '<%= Request.QueryString["emailctrl"] %>';
            var idCtrl = '<%= Request.QueryString["idctrl"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Set the user name field value. Replace the 'link button name' with the 
            // 'user name hidden field name' and retrieve the value.
            if (nameCtrl != null && nameCtrl != '') {
                window.opener.document.getElementById(nameCtrl).value
                    = document.getElementById(ctrl.id.replace("ShowScheduleOnlineInterviewCandidate_selectLinkButton", "ShowScheduleOnlineInterviewCandidate_userNameHiddenfield")).value;
            }

            // Set the email hidden field value. Replace the 'link button name' with the 
            // 'eamil hidden field name' and retrieve the value.
            if (emailCtrl != null && emailCtrl != '') {
                window.opener.document.getElementById(emailCtrl).value
                    = document.getElementById(ctrl.id.replace("ShowScheduleOnlineInterviewCandidate_selectLinkButton", "ShowScheduleOnlineInterviewCandidate_emailHiddenfield")).value;
            }

            // Set the user ID hidden field value. Replace the 'link button name' with the 
            // 'user ID hidden field name' and retrieve the value.
            if (idCtrl != null && idCtrl != '') {
                window.opener.document.getElementById(idCtrl).value
                    = document.getElementById(ctrl.id.replace("ShowScheduleOnlineInterviewCandidate_selectLinkButton", "ShowScheduleOnlineInterviewCandidate_userIDHiddenfield")).value;
            }
            self.close();
        }

    </script>
    <asp:UpdatePanel ID="ShowScheduleOnlineInterviewCandidate_pageUpdatePanel" runat="server">
        <ContentTemplate>
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td class="popup_td_padding_10">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 100%" class="popup_header_text_grey" align="left">
                                    <asp:Literal ID="ShowScheduleOnlineInterviewCandidate_searchAssessor" runat="server" Text="Online Interview Assessor Details"></asp:Literal>
                                </td>
                                <td style="width: 100%" align="right">
                                    <asp:ImageButton ID="ShowScheduleOnlineInterviewCandidate_topCancelImagebutton" runat="server"
                                        SkinID="sknCloseImageButton" OnClientClick="javascript:CloseMe()" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_2">
                    </td>
                </tr>
                <tr>
                    <td class="popup_td_padding_10" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                            <tr>
                                <td class="popup_td_padding_10">
                                    <div id="ShowScheduleOnlineInterviewCandidate_searchCriteriasDiv" runat="server" style="display: block;">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="ShowScheduleOnlineInterviewCandidate_topSuccessMessageLabel" runat="server"
                                                        SkinID="sknSuccessMessage"></asp:Label>
                                                    <asp:Label ID="ShowScheduleOnlineInterviewCandidate_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                    <asp:HiddenField ID="ShowScheduleOnlineInterviewCandidate_pageNumberHiddenField" runat="server"
                                                        Value="1" />
                                                    <asp:HiddenField ID="ScheduleOnlineInterviewCandidate_candidateIdHiddenField" runat="server" />
                                                    <asp:HiddenField ID="ScheduleOnlineInterviewCandidate_selectedTimeSlotIDsHiddenField" runat="server" />
                                                    <asp:HiddenField runat="server" ID="ShowScheduleOnlineInterviewCandidate_isMaximizedHiddenField" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div id="ShowScheduleOnlineInterviewCandidate_searchCriteriDiv" runat="server" style="display: block;">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td width="100%">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <table cellpadding="2" cellspacing="2" border="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_candidateNamePopHeadLabel" runat="server"
                                                                                                Text="Candidate Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            <span class='mandatory'>*</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="ScheduleOnlineInterviewCandidate_Popup_candidateNameTextBox" runat="server"
                                                                                                ReadOnly="true" Text=""></asp:TextBox>&nbsp;<asp:ImageButton ID="ScheduleOnlineInterviewCandidate_candidateNameImageButton"
                                                                                                    SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" ToolTip="Click here to search for candidates" />&nbsp;<asp:ImageButton
                                                                                                        ID="ScheduleOnlineInterviewCandidate_positionProfileCandidateImageButton" SkinID="sknBtnSearchPositinProfileCandidateIcon"
                                                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to search for candidates associated with the position profile" />&nbsp;
                                                                                            <asp:Button ID="ScheduleOnlineInterviewCandidate_Popup_createCandidateButton" Text="New"
                                                                                                runat="server" SkinID="sknButtonId" ToolTip="Click here to create a new candidate" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_Popup_emailHeadLabel" runat="server"
                                                                                                Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="ScheduleOnlineInterviewCandidate_Popup_emailTextBox" runat="server"
                                                                                                Text="" ReadOnly="true"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5" colspan="2"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_8">
                                                                </td>
                                                            </tr>
                                                            <tr id="ShowScheduleOnlineInterviewCandidate_ShowAssesserResultsHeaderTR" runat="server">
                                                                <td class="header_bg">
                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                        <tr>
                                                                            <td style="width: 50%" align="left" class="header_text_bold">
                                                                                <asp:Literal ID="ShowScheduleOnlineInterviewCandidate_searchResultsLiteral" runat="server"
                                                                                    Text="Online Interview Assessor Details">
                                                                                </asp:Literal>
                                                                            </td>
                                                                            <td style="width: 48%" align="left">
                                                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:HiddenField ID="ShowScheduleOnlineInterviewCandidate_stateExpandHiddenField" runat="server"
                                                                                                Value="0" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td style="width: 2%" align="right">
                                                                                <span id="ShowScheduleOnlineInterviewCandidate_searchResultsUpSpan" runat="server" style="display: none;">
                                                                                    <asp:Image ID="ShowScheduleOnlineInterviewCandidate_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                                                        id="ShowScheduleOnlineInterviewCandidate_searchResultsDownSpan" runat="server" style="display: none;"><asp:Image
                                                                                            ID="ShowScheduleOnlineInterviewCandidate_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                                                <asp:HiddenField ID="ShowScheduleOnlineInterviewCandidate_restoreHiddenField" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="ShowScheduleOnlineInterviewCandidate_ShowAssesserResultsTR" runat="server">
                                                                <td class="grid_body_bg">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="td_height_8">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <asp:ImageButton ID="ShowScheduleOnlineInterviewCandidate_previousImageButton" runat="server"
                                                                                                ImageUrl="~/App_Themes/DefaultTheme/Images/nav_btn_prev.gif" CommandName="Previous"
                                                                                                OnClick="OnNavigatePage" ToolTip="Previous" Width="20px" Height="20px" SkinID="sknPreviousDateImage" />
                                                                                        </td>
                                                                                        <td align="right">
                                                                                            <asp:ImageButton ID="ShowScheduleOnlineInterviewCandidate_nextImageButton" runat="server"
                                                                                                ImageUrl="~/App_Themes/DefaultTheme/Images/nav_btn_next.gif" CommandName="Next"
                                                                                                OnClick="OnNavigatePage" ToolTip="Next" Width="20px" Height="20px" SkinID="sknNextDateImage" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <div style="overflow: auto;" runat="server" id="ShowScheduleOnlineInterviewCandidate_assessorDiv">
                                                                                    <asp:GridView ID="ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView" SkinID="sknRecommendAssessorGrid"
                                                                                        AllowSorting="false" runat="server" AutoGenerateColumns="true" OnRowCommand="ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView_RowCommand"
                                                                                        OnRowDataBound="ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView_RowDataBound"
                                                                                        OnRowDeleting="ShowScheduleOnlineInterviewCandidate_assesserDetailsGridView_RowDeleting">
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table cellpadding="3" cellspacing="3">
                            <tr>
                                <td>
                                    <asp:Button ID="ShowScheduleOnlineInterviewCandidate_addAssessorButton" runat="server" Text="Select"
                                        SkinID="sknButtonId" OnClick="ShowScheduleOnlineInterviewCandidate_addAssessorButton_Click" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="ShowAssesserLookup_resetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                        Text="Reset" OnClick="ShowAssesserLookup_resetLinkButton_Click"></asp:LinkButton>
                                </td>
                                <td class="pop_divider_line">
                                    |
                                </td>
                                <td>
                                    <asp:LinkButton ID="ShowScheduleOnlineInterviewCandidate_addAssessorCancel" runat="server"
                                        SkinID="sknPopupLinkButton" Text="Cancel" OnClientClick="javascript:window.close()"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="ShowScheduleOnlineInterviewCandidate_addSkillPanel" runat="server" CssClass="popupcontrol_addSkill">
                            <div style="display: none;">
                                <asp:Button ID="ShowScheduleOnlineInterviewCandidate_addSkillHiddenButton" runat="server"
                                    Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="ShowScheduleOnlineInterviewCandidate_addSkillLiteral" runat="server" Text="Add Skill"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="ShowScheduleOnlineInterviewCandidate_addSkillTopCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                            <tr>
                                                <td align="left" class="popup_td_padding_10">
                                                    <table border="0" cellpadding="0" cellspacing="5" class="tab_body_bg" align="left">
                                                        <tr>
                                                            <td class="msg_align" colspan="4">
                                                                <asp:Label ID="ShowScheduleOnlineInterviewCandidate_addSkillErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="ShowScheduleOnlineInterviewCandidate_addSkillLabel" runat="server" Text="Skill"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class='mandatory'>*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="ShowScheduleOnlineInterviewCandidate_addSkillTextBox" runat="server" MaxLength="100"
                                                                    Columns="75" extMode="SingleLine" Wrap="true">
                                                                </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_5">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="left" style="width: 20px; padding-right: 5px">
                                                    <asp:Button ID="ShowScheduleOnlineInterviewCandidate_addSkillSaveButton" runat="server" SkinID="sknButtonId"
                                                        Text="Save" OnClick="ShowScheduleOnlineInterviewCandidate_addSkillSaveButton_Clicked" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="ShowScheduleOnlineInterviewCandidate_addSkillCloseLinkButton" SkinID="sknPopupLinkButton"
                                                        runat="server" Text="Cancel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="ShowScheduleOnlineInterviewCandidate_addSkillModalPopupExtender"
                            runat="server" PopupControlID="ShowScheduleOnlineInterviewCandidate_addSkillPanel" TargetControlID="ShowScheduleOnlineInterviewCandidate_addSkillHiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
