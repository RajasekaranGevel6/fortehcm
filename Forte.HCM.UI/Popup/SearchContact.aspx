﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/CompactPopupMaster.Master"
    CodeBehind="SearchContact.aspx.cs" Title="Search Contact" Inherits="Forte.HCM.UI.Popup.SearchContact" %>

<%@ MasterType VirtualPath="~/MasterPages/CompactPopupMaster.Master" %>
<asp:Content ID="SearchContact_content" runat="server" ContentPlaceHolderID="CompactPopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        function OnDoneClick()
        {
            var addressControl = '<%= Request.QueryString["addresscontrol"] %>';

            if (window.dialogArguments)
            {
                window.opener = window.dialogArguments;
            }

            // Set the selected emails to the address field.
            if (addressControl != null && addressControl != '')
            {
                if (window.opener.document.getElementById(addressControl).value != null &&
                    window.opener.document.getElementById(addressControl).value != '')
                {
                    // Append contacts.
                    window.opener.document.getElementById(addressControl).value
                        = window.opener.document.getElementById(addressControl).value + "," +
                        document.getElementById('<%= SearchContact_selectedContacts.ClientID %>').value;
                }
                else
                {
                    // Assign contacts.
                    window.opener.document.getElementById(addressControl).value
                        = document.getElementById('<%= SearchContact_selectedContacts.ClientID %>').value;
                }
            }

            // Close the window.
            self.close();

            return false;
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="contact_popup_td_padding_4" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchContact_headerLiteral" runat="server" Text="Search Contact"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SearchContact_topCancelImageButton" ToolTip="Click here to close the window"
                                runat="server" SkinID="sknCloseImageButton" OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="contact_popup_td_padding_4" valign="top" colspan="2">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="contact_popup_td_padding_4">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="msg_align" colspan="2" style="height: 20px">
                                        <asp:UpdatePanel ID="SearchContact_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="SearchContact_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="SearchContact_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchContact_categoryDropDownList" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchContact_goButton" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchContact_doneButton" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchContact_resetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">
                                        <asp:UpdatePanel ID="SearchContact_criteriaUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 80%" align="left">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="SearchContact_keywordTextBox" CssClass="enter_contact_keyword_unwatermark"
                                                                            Height="18px" Columns="108"></asp:TextBox>
                                                                        <ajaxToolKit:TextBoxWatermarkExtender ID="SearchContact_searchForWatermarkExtender"
                                                                            runat="server" Enabled="True" WatermarkCssClass="enter_contact_keyword_watermark"
                                                                            WatermarkText="Enter keywords separated by comma" TargetControlID="SearchContact_keywordTextBox" />
                                                                    </td>
                                                                    <td style="width: 5%">
                                                                        <asp:Button ID="SearchContact_goButton" Text="Go" Height="20px" ToolTip="Click here to search for entered keywords"
                                                                            runat="server" SkinID="sknButtonId" OnClick="SearchContact_goButton_Click" Width="100%" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_2">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 80%" align="left">
                                                            <asp:DropDownList ID="SearchContact_categoryDropDownList" runat="server" AutoPostBack="true"
                                                                Height="20px" SkinID="sknContactCategoryDropDown" Width="100%" OnSelectedIndexChanged="SearchContact_categoryDropDownList_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_2">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:UpdatePanel ID="SearchContact_gridUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2" class="search_contact_grid_header_bg">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="width: 50%" align="left">
                                                                        <asp:Label ID="SearchContact_contactsHeaderLabel" runat="server" Text="Searched Contacts"
                                                                            SkinID="sknContactLabelText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 50%" align="right">
                                                                        <asp:LinkButton ID="SearchContact_selectAllLinkButton" runat="server" SkinID="sknConactLinkButton"
                                                                            Text="Select All" ToolTip="Click here to select all contacts" OnClick="SearchContact_selectAllLinkButton_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="contact_grid_body_bg">
                                                                        <div id="SearchContact_contactsDIV" runat="server" style="height: 161px; overflow: auto;" >
                                                                            <asp:GridView ID="SearchContact_contactsGridView" SkinID="sknMailContactGrid" runat="server" AllowSorting="false"
                                                                                 AutoGenerateColumns="false" ShowHeader="false" 
                                                                                OnRowCommand="SearchContact_contactsGridView_RowCommand" 
                                                                                OnRowDataBound="SearchContact_contactsGridView_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-Width="4%">
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="SearchContact_contactsGridView_defaultImageButton" runat="server"
                                                                                                ToolTip="Click here to select contact" SkinID="sknDefaultMailContact" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                CommandName="Add" Visible='<%# !IsSelected(Eval("Selected").ToString())%>' />
                                                                                            <asp:ImageButton ID="SearchContact_contactsGridView_selectedImageButton" runat="server"
                                                                                                ToolTip="Click here to remove contact" SkinID="sknAddMailContact" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                CommandName="Remove" Visible='<%# IsSelected(Eval("Selected").ToString())%>' />
                                                                                            <asp:HiddenField ID="SearchContact_contactsGridView_selectedFlagHiddenField" runat ="server" Value='<%# Eval("Selected") %>'/>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Full Name" HeaderStyle-Width="30%" ItemStyle-Width="30%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchContact_contactsGridView_fullNameLabel" runat="server" Text='<%# Eval("FullName") %>' >
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Company" HeaderStyle-Width="30%" ItemStyle-Width="30%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchContact_contactsGridView_companyLabel" runat="server" Text='<%# Eval("Company") %>' >
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Email ID" ItemStyle-Width="30%" HeaderStyle-HorizontalAlign="Center"
                                                                                        HeaderStyle-Width="30%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchContact_contactsGridView_emailIDLabel" runat="server" Text='<%# Eval("EmailID") %>' >
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <EmptyDataTemplate>
                                                                                    <table style="width: 100%">
                                                                                        <tr>
                                                                                            <td valign="middle" align="center" style="height: 100%" class="error_message_text_normal">
                                                                                                No contacts found to display
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" class="search_contact_grid_header_bg">
                                                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td style="width: 50%" align="left">
                                                                                    <asp:Label ID="SearchContact_addedContactsLabel" runat="server" Text="Added Contacts"
                                                                                        SkinID="sknContactLabelText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 50%" align="right">
                                                                                    <asp:LinkButton ID="SearchContact_removeAllLinkButton" runat="server" SkinID="sknConactLinkButton"
                                                                                        Text="Remove All" ToolTip="Click here to remove all contacts" OnClick="SearchContact_removeAllLinkButton_Click" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="contact_grid_body_bg" align="left">
                                                                        <div id="SearchContact_addedContactsDIV" runat="server" style="height: 161px; overflow: auto;">
                                                                            <asp:GridView ID="SearchContact_addedContactsGridView" runat="server" AllowSorting="false"
                                                                                SkinID="sknMailContactGrid" AutoGenerateColumns="false" OnRowCommand="SearchContact_addedContactsGridView_RowCommand"
                                                                                ShowHeader="false" ShowFooter="true" 
                                                                                onrowdatabound="SearchContact_addedContactsGridView_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-Width="4%">
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="SearchContact_addedContactsGridView_selectedImageButton" runat="server"
                                                                                                ToolTip="Click here to remove contact" SkinID="sknDeleteMailContact" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                CommandName="Remove" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Full Name" HeaderStyle-Width="30%" ItemStyle-Width="30%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchContact_addedContactsGridView_fullNameLabel" runat="server"
                                                                                                Text='<%# Eval("FullName") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Company" HeaderStyle-Width="30%" ItemStyle-Width="30%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchContact_addedContactsGridView_companyLabel" runat="server" Text='<%# Eval("Company") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Email ID" ItemStyle-Width="30%" HeaderStyle-HorizontalAlign="Center"
                                                                                        HeaderStyle-Width="30%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchContact_addedContactsGridView_emailIDLabel" runat="server" Text='<%# Eval("EmailID") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="search_contact_grid_header_bg">
                                                                        <asp:Label ID="SearchContact_contactsSelectedLabel" runat="server" Text=""
                                                                            SkinID="sknContactCountLabelText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_padding_4"></td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchContact_categoryDropDownList" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchContact_goButton" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchContact_doneButton" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchContact_resetLinkButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="contact_popup_td_padding_4">
            <asp:UpdatePanel ID="SearchContact_bottomControlsUpdatePanel" runat="server">
                <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="SearchContact_doneButton" runat="server" Text="Done" SkinID="sknButtonId"
                                OnClick="SearchContact_doneButton_Click" ToolTip="Click here to move the selected contacts to the parent window"/>
                        </td>
                        <td>
                            <asp:LinkButton ID="SearchContact_resetLinkButton" ToolTip="Click here to reset the window"
                                runat="server" Text="Reset" SkinID="sknPopupLinkButton" OnClick="SearchContact_resetLinkButton_Click" />
                        </td>
                        <td class="pop_divider_line">
                            |
                        </td>
                        <td>
                            <asp:LinkButton ID="SearchContact_cancelLinkButton" ToolTip="Click here to close the window"
                                runat="server" Text="Cancel" SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                            <asp:HiddenField ID="SearchContact_selectedContacts" runat="server" />
                        </td>
                    </tr>
                </table>
                </ContentTemplate>
            </asp:UpdatePanel>
                
            </td>
        </tr>
    </table>
</asp:Content>
