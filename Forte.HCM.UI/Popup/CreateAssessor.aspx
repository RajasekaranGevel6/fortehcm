﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateAssessor.aspx.cs" MasterPageFile="~/MasterPages/PopupMaster.Master"
    Inherits="Forte.HCM.UI.Popup.CreateAssessor" Title="Create Assessor" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="CreateAssessor_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        function CloseCreateAssessorWindow()
        {
            var btncnrl = '<%= Request.QueryString["btncnrl"] %>';

            if (window.dialogArguments)
            {
                window.opener = window.dialogArguments;
            }

            // Trigger the click event of the refresh button in the parent page.
            if (btncnrl != null && window.opener.document.getElementById(btncnrl) != null)
                window.opener.document.getElementById(btncnrl).click();

            self.close();
        }

        function SubmitAddImage()
        {
            alert('hig');
            document.getElementById('<%=CreateAssessor_addSkillImageButton.ClientID %>').click();
            return false;
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="td_padding_top_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="CreateAssessor_titleLiteral" runat="server" Text="Create Assessor"></asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="CreateAssessor_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:self.close();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <table width="100%" cellpadding="0" cellspacing="3" border="0" class="popupcontrol_create_assessor_bg">
                    <tr>
                        <td style="width: 100%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="height: 20px">
                                        <asp:UpdatePanel ID="CreateAssessor_messageUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="CreateAssessor_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknErrorMessage" Width="100%" Text=""></asp:Label>
                                                <asp:Label ID="CreateAssessor_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknSuccessMessage" Width="100%" Text=""></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 100%">
                                        <asp:UpdatePanel ID="CreateAssessor_inputControlsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 50%" valign="top" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 80%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="CreateAssessor_contactInfoLiteral" runat="server" Text="Contact Info"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="create_assessor_grid_body_bg">
                                                                        <table cellpadding="0" cellspacing="4" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 15%" align="left" valign="middle">
                                                                                    <asp:Label ID="CreateAssessor_userNameLabel" runat="server" Text="User" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 85%" align="left" valign="middle">
                                                                                    <asp:TextBox ID="CreateAssessor_userNameTextBox" runat="server" AutoCompleteType="None"
                                                                                        MaxLength="100" ReadOnly="True" Width="98%"></asp:TextBox>
                                                                                    <asp:HiddenField runat="server" ID="CreateAssessor_userIDHiddenField" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 15%" align="left" valign="middle">
                                                                                    <asp:Label ID="CreateAssessor_firstNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="First Name"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 85%" align="left" valign="middle">
                                                                                    <asp:TextBox ID="CreateAssessor_firstNameTextBox" runat="server" MaxLength="100"
                                                                                        ReadOnly="True" Width="98%"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 15%" align="left" valign="middle">
                                                                                    <asp:Label ID="CreateAssessor_lastNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Last Name"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 85%" align="left" valign="middle">
                                                                                    <asp:TextBox ID="CreateAssessor_lastNameTextBox" runat="server" MaxLength="100" ReadOnly="True"
                                                                                        Width="98%"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 15%" align="left" valign="middle">
                                                                                    <asp:Label ID="CreateAssessor_alternateEmailIDLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Alternate Email ID"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 85%" align="left" valign="middle">
                                                                                    <asp:TextBox ID="CreateAssessor_alternateEmailIDTextBox" runat="server" Width="98%"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 15%" align="left" valign="middle">
                                                                                    <asp:Label ID="CreateAssessor_mobilePhoneLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Mobile Phone"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 85%" align="left" valign="middle">
                                                                                    <asp:TextBox ID="CreateAssessor_mobilePhoneTextBox" runat="server" MaxLength="10"
                                                                                        Width="98%">
                                                                                    </asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 15%" align="left" valign="middle">
                                                                                    <asp:Label ID="CreateAssessor_homePhoneLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Home Phone"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 85%" align="left" valign="middle">
                                                                                    <asp:TextBox ID="CreateAssessor_homePhoneTextBox" runat="server" MaxLength="10" Width="98%">
                                                                                    </asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%" valign="top" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 80%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="CreateAssessor_skillsLiteral" runat="server" Text="Skills"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="create_assessor_grid_body_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="CreateAssessor_skillsLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                        Text="Skill(s)"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="CreateAssessor_skillsTextBox" runat="server" Width="150px" Columns="2"></asp:TextBox>
                                                                                    <asp:ImageButton ID="CreateAssessor_addSkillImageButton" runat="server" ImageAlign="AbsMiddle"
                                                                                        SkinID="sknAddVectorGroupImageButton" Style="margin-left: 0px" ToolTip="Click here to add the entered skill"
                                                                                        Width="16px" OnClick="CreateAssessor_addSkillImageButton_Click" />&nbsp;
                                                                                    <asp:ImageButton ID="CreateAssessor_searchSkillImageButton" runat="server" ImageAlign="AbsMiddle"
                                                                                        SkinID="sknbtnSearchIcon" Style="margin-left: 0px" ToolTip="Click here to search & add skill"
                                                                                        Width="16px" />&nbsp;
                                                                                    <asp:ImageButton ID="CreateAssessor_skillHelpImageButton" runat="server" ImageAlign="AbsMiddle"
                                                                                        OnClientClick="javascript:return false;" SkinID="sknHelpImageButton" Style="margin-left: 0px"
                                                                                        ToolTip="Enter multiple skills with comma separated and click the add button"
                                                                                        Width="16px" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <asp:UpdatePanel ID="CreateAssessor_skillsUpdatePanel" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <div style="height: 120px; overflow: auto;">
                                                                                                <asp:GridView ID="CreateAssessor_skillsGridView" runat="server" AutoGenerateColumns="False"
                                                                                                    AllowSorting="False" OnRowCommand="CreateAssessor_skillsGridView_RowCommand" OnRowDataBound="CreateAssessor_skillsGridView_RowDataBound"
                                                                                                    Height="100%">
                                                                                                    <Columns>
                                                                                                        <asp:TemplateField HeaderStyle-Width="24px">
                                                                                                            <ItemTemplate>
                                                                                                                <asp:ImageButton ID="CreateAssessor_skillsGridView_deleteSkillImageButton" runat="server"
                                                                                                                    SkinID="sknDeletePositionProfile" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                                    ToolTip="Delete Skill" CssClass="showCursor" CommandName="DeleteSkill" />&nbsp;
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>
                                                                                                        <asp:TemplateField HeaderText="Category" HeaderStyle-Width="80px">
                                                                                                            <ItemTemplate>
                                                                                                                <asp:Label ID="CreateAssessor_skillsGridView_categoryNameLabel" runat="server" Text='<%# Eval("CategoryName") %>'
                                                                                                                    Width="100%"></asp:Label>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>
                                                                                                        <asp:TemplateField HeaderText="Skill" HeaderStyle-Width="100px">
                                                                                                            <ItemTemplate>
                                                                                                                <asp:Label ID="CreateAssessor_skillsGridView_skillNameLabel" runat="server" Text='<%# Eval("SkillName") %>'
                                                                                                                    Width="100%"></asp:Label>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>
                                                                                                        <asp:TemplateField>
                                                                                                            <ItemTemplate>
                                                                                                                <asp:HiddenField ID="CreateAssessor_skillsGridView_skillIDHiddenField" runat="server"
                                                                                                                    Value='<%# DataBinder.Eval(Container.DataItem, "SkillID")%>' />
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>
                                                                                                    </Columns>
                                                                                                </asp:GridView>
                                                                                            </div>
                                                                                            <asp:HiddenField ID="CreateAssessor_categoryIDHiddenField" runat="server" />
                                                                                            <asp:HiddenField ID="CreateAssessor_skillIDHiddenField" runat="server" />
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 4px">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 4px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="CreateAssessor_buttonControlsUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="td_height_5" colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="left">
                                    <asp:Button ID="CreateAssessor_createButton" runat="server" Text="Create" SkinID="sknButtonId"
                                        Visible="true" ToolTip="Click here to create the assessor" OnClick="CreateAssessor_createButton_Click" />
                                </td>
                                <td valign="middle" align="left">
                                    &nbsp;<asp:LinkButton ID="CreateAssessor_cancelLinkButton" runat="server" Text="Cancel"
                                        SkinID="sknPopupLinkButton" OnClientClick="javascript:self.close();" ToolTip="Click here to close the window"> </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
