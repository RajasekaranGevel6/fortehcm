<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RequestExternalAssessor.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.RequestExternalAssessor" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ID="SearchClientRequest_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">
    // Handler method that will be called when the 'save' button is 
    // clicked in the the show recommend page.
    function OnSaveClick() {
        var btncnrl = '<%= Request.QueryString["ctrlid"] %>';

        if (window.dialogArguments) {
            window.opener = window.dialogArguments;
        }

        // Trigger the click event of the refresh button in the parent page.
        if (btncnrl != null && window.opener.document.getElementById(btncnrl) != null)
            window.opener.document.getElementById(btncnrl).click();

        self.close();
    }
    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="RequestExternalAssessor_headerLiteral" runat="server" Text="Request External Assessor"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="QuestionDetailPreviewControl_topCancelImageButton" runat="server"
                                SkinID="sknCloseImageButton" OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                    <tr>
                        <td class="msg_align" colspan="2">
                            <asp:UpdatePanel ID="RequestExternalAssessor_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="RequestExternalAssessor_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="RequestExternalAssessor_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="RequestExternalAssessor_sendRequestButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="3" cellpadding="2">
                                                <tr>
                                                    <td>
                                                        <table style="width: 100%" cellpadding="2" cellspacing="0">
                                                            <asp:UpdatePanel ID="RequestExternalAssessor_skillUpdatePanel" runat="server"
                                                                UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <tr>
                                                                        <td style="width:130px">
                                                                            <asp:Label ID="RequestExternalAssessor_emailLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <div style="float: left; padding-right: 5px;">
                                                                                <asp:TextBox ID="RequestExternalAssessor_emailTextBox" Width="325px" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                <asp:ImageButton ID="RequestExternalAssessor_candidateSessionHelpImageButton" SkinID="sknHelpImageButton"
                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Enter one or more email ID's separated by commas"
                                                                                    Height="16px" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="RequestExternalAssessor_skillLabel" runat="server" Text="Interview session skill"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="RequestExternalAssessor_skillLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:HiddenField ID="RequestExternalAssessor_attemptIDHiddenField" runat="server" />
                                                                            <asp:HiddenField ID="RequestExternalAssessor_candidateSessionKeyHiddenField" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                    </td>
                                </tr>
                                <tr id="RequestExternalAssessor_skillTR" runat="server">
                                    <td class="header_bg" align="center" runat="server">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 50%">
                                                    <asp:Literal ID="RequestExternalAssessor_selectSkillLiteral" runat="server" Text="Select Skill"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="left">
                                                    <asp:UpdatePanel ID="RequestExternalAssessor_skillGridviewUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <div style="height: 150px; overflow: auto;" runat="server" id="RequestExternalAssessor_skillGridViewDiv">
                                                                <asp:GridView ID="RequestExternalAssessor_skillGridView" runat="server" AllowSorting="True"
                                                                    AutoGenerateColumns="False" GridLines="None" Width="100%">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="RequestExternalAssessor_skillGridView_selectSkillCheckBox" runat="server"
                                                                                    ToolTip="Select" />
                                                                                <asp:HiddenField ID="RequestExternalAssessor_skillGridView_skillIDHiddenField" runat="server"
                                                                                    Value='<%# Eval("SkillID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Skill">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="RequestExternalAssessor_skillGridView_skillNameLabel" runat="server"
                                                                                    Text='<%# Eval("SkillName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <EmptyDataTemplate>
                                                                        <table style="width: 100%; height: 100%">
                                                                            <tr>
                                                                                <td style="height: 80px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="middle" align="center" style="height: 100%" class="error_message_text_normal">
                                                                                    No skills found to display
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="RequestExternalAssessor_pagingControlUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <uc1:PageNavigator ID="RequestExternalAssessor_bottomPagingNavigator" runat="server" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" align="left">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="RequestExternalAssessor_bottomLinksUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Button ID="RequestExternalAssessor_sendRequestButton" runat="server" Text="Send"
                                                    SkinID="sknButtonId" 
                                                    onclick="RequestExternalAssessor_sendRequestButton_Click" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="RequestExternalAssessor_bottomReset" runat="server" Text="Reset"
                                                    SkinID="sknPopupLinkButton" 
                                                    onclick="RequestExternalAssessor_bottomReset_Click"></asp:LinkButton>
                                            </td>
                                            <td class="pop_divider_line">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="RequestExternalAssessor_bottomCancel" runat="server" Text="Cancel"
                                                    SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
