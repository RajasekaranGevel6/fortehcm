<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchCertificateFormat.aspx.cs"
    Inherits="Forte.HCM.UI.Popup.SearchCertificateFormat" MasterPageFile="~/MasterPages/PopupMaster.Master"%>    
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="SearchCertificateFormat_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript">
        function assignCertificateName(certificateName, CertificateId) {
            var ctrlCertificateNameId = '<%= Request.QueryString["ctrlNameId"] %>';
            var ctrlCertificateId = '<%= Request.QueryString["ctrlId"] %>';
            var ctrlCertificateNameHiddenId = '<%= Request.QueryString["ctrlNameHiddendId"] %>';
            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }
            if (ctrlCertificateId != null && ctrlCertificateId != '') {
                window.opener.document.getElementById(ctrlCertificateId).value
                = CertificateId;
            }
            // This is use ful when certificate name text box is read only.
            // if you pass certificate name hidden field this will assign value to the hidden field.
            if (ctrlCertificateNameHiddenId != null && ctrlCertificateNameHiddenId != '') {
                window.opener.document.getElementById(ctrlCertificateNameHiddenId).value
                = certificateName;
            }
            if (ctrlCertificateNameId != null && ctrlCertificateNameId != '') {
                window.opener.document.getElementById(ctrlCertificateNameId).value
                = certificateName;
            }

            self.close();
        }

        function CheckOtherIsCheckedByGVID(CurrentRdbID) {
            Parent = document.getElementById("<%= SearchCertificateFormat_certificatesGridViewDiv.ClientID %>");
            var items = Parent.getElementsByTagName('input');

            for (i = 0; i < items.length; i++) {
                if (items[i].type == 'radio') {
                    if (items[i].id != CurrentRdbID)
                        document.getElementById(items[i].id).checked = false;
                }
            }
            //document.getElementById(CurrentRdbID).checked = true;
            return true;
        }
    </script>

    <div id="SearchCertificate">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="popup_td_padding_10">
                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td style="width: 50%" class="popup_header_text_grey" align="left">
                                <asp:Literal ID="SearchCertificate_headerLiteral" runat="server" Text="Search Certificate"></asp:Literal>
                            </td>
                            <td style="width: 50%" align="right">
                                <asp:ImageButton ID="SearchCertificate_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                    OnClientClick="javascript:CloseMe()" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="popup_td_padding_10" valign="top" colspan="2">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                        <tr>
                            <td class="popup_td_padding_10">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="msg_align">
                                            <asp:Label ID="SearchCertificate_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                            <asp:Label ID="SearchCertificate_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_5">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td style="width: 100%; overflow: auto;" valign="top">
                                                        <div id="SearchCertificateFormat_certificatesGridViewDiv" style="height: 150px; overflow: auto;"
                                                            runat="server">
                                                            <asp:GridView ID="SearchCertificateFormat_certificatesGridView" GridLines="Horizontal"
                                                                BorderColor="white" BorderWidth="1px" runat="server" AutoGenerateColumns="false" Width="100%">
                                                                <RowStyle CssClass="grid_alternate_row" />
                                                                <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                <HeaderStyle CssClass="grid_header_row" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="">
                                                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="SearchCertificateFormat_selectLinkButton" runat="server" Text="Select"
                                                                                OnClientClick='<%#String.Format(" return assignCertificateName(&#39;{0}&#39;,&#39;{1}&#39;)", Eval("CertificateName"),Eval("CertificateID")) %>'></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Certificate Name">
                                                                        <ItemTemplate>
                                                                            <asp:RadioButton ID="ShowCertificateFormat_certificatePreviewRadioButton" runat="server"
                                                                                AutoPostBack="true" Text='<%# Eval("CertificateName") %>' GroupName='<%# Eval("CertificateID") %>'
                                                                                OnCheckedChanged="CertificateFormatRadioButton_CheckedChanged" />
                                                                            <asp:HiddenField ID="ShowCertificateFormat_certificationIDHiddenFiled" Value='<%# Eval("CertificateID") %>'
                                                                                runat="server" />
                                                                            <asp:HiddenField ID="ShowCertificateFormat_htmlTextHiddenField" Value='<%# Eval("HtmlText") %>'
                                                                                runat="server" />
                                                                            <asp:HiddenField ID="ShowCertificateFormat_imageDataHiddenField" Value='<%# Eval("imageData") %>'
                                                                                runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField HeaderText="Description" DataField="CertificateDesc" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <div runat="server" id="ShowCertificateFormat_previewDIV" style="width: 610px; height: 280px;
                                                            vertical-align: top; overflow: auto; display: block;">
                                                            <asp:Image ID="SearchCertificateFormat_certificateImage" runat="server" Height="400px"
                                                                Width="600px" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_5">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <%--<tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 20%">
                                        <asp:Label ID="SearchCertificate_selectFormatLabel" runat="server" Text="Select format"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 80%">
                                        <asp:Label ID="SearchCertificate_selectDropDownListLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                            Text="Preview"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 20%" valign="top">
                                        <asp:ListBox ID="SearchCertificate_selectListBox" runat="server" OnSelectedIndexChanged="SearchCertificate_selectDropDownList_SelectedIndexChanged"
                                            AutoPostBack="True" Height="150px">
                                            <asp:ListItem Value="1" Selected="True">Traditional</asp:ListItem>
                                            <asp:ListItem Value="2">Standard</asp:ListItem>
                                            <asp:ListItem Value="3">Professional</asp:ListItem>
                                            <asp:ListItem Value="4">Modern</asp:ListItem>
                                            <asp:ListItem Value="5">Elegant</asp:ListItem>
                                            <asp:ListItem Value="6">Classic</asp:ListItem>
                                        </asp:ListBox>
                                    </td>
                                    <td style="width: 80%" rowspan="2" align="center">
                                        <asp:Image ID="SearchCertificate_perviewImage" runat="server" ImageAlign="Middle"
                                            ImageUrl="~/CertificateFormats/Traditional.gif" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>--%>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" class="popup_td_padding_5">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td valign="top">
                                <asp:LinkButton ID="SearchCertificate_bottomResetButton" runat="server" SkinID="sknPopupLinkButton"
                                    Text="Reset" OnClick="SearchCertificate_bottomResetButton_Click" />
                            </td>
                            <td class="pop_divider_line" valign="top">
                                |
                            </td>
                            <td valign="top">
                                <asp:LinkButton ID="SearchCertificate_bottomCancelButton" runat="server" Text="Cancel"
                                    SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 10px;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
