﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewExternalAssessorProfile.aspx.cs
// File that represents the user interface layout and functionalities for
// the ViewExternalAssessorProfile page. This will helps to view the assessor 
// profile details.

#endregion Header

#region Directives                                                             

using System;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Configuration;
using System.Data;
using Forte.HCM.Common.DL;
using System.Data.Common;
using Forte.HCM.Utilities;
using Forte.HCM.ExternalService;
using Resources;
using System.Text;
using System.Web.Configuration;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ViewExternalAssessorProfile page. This will helps to view the assessor 
    /// profile details. This class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ViewExternalAssessorProfile : PageBase
    {
        #region Declaration

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="userEmail">
        /// A <see cref="string"/> that holds user email.
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="string"/> that holds confirmation code generated.
        /// </param>
        private delegate void AsyncTaskDelegate(string userEmail, string confirmationCode, string password);

        #endregion Declaration

        #region Enum

        private enum SubscriptionRolesEnum
        {
            SR_COR_AMN = 3,
            SR_FRE = 1,
            SR_STA = 2,
            SR_COR_USR = 4
        }

        #endregion Enum

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("External Assessor Profile");

                if (!IsPostBack)
                {
                    // Call and assign query string values to hidden fields.
                    if (RetrieveQueryStringValues() == false)
                        return;

                    // Add handler to view assessor calendar popup.
                    //ViewExternalAssessorProfile_convertToAssessorImageButton.Attributes.Add("onclick",
                        //"return ShowAssessorCalendar('" + ViewExternalAssessorProfile_assessorIDHiddenField.Value + "');");

                    // Set default sort order and field.
                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "CANDIDATE_NAME";

                    // Load assessor details.
                    LoadAssessorDetail(Convert.ToInt32(ViewExternalAssessorProfile_assessorIDHiddenField.Value));

                    // Load assessment details.
                    LoadAssessmentDetails(Convert.ToInt32(ViewExternalAssessorProfile_assessorIDHiddenField.Value), 1);
                }

                // Subscribe to paging event.
                ViewAsssessorProfile_assessmentDetailsPageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler(ViewAsssessorProfile_assessmentDetailsPageNavigator_PageNumberClick);

                // Show menu type.
                ShowMenuType();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewExternalAssessorProfile_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the assessment details grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        private void ViewAsssessorProfile_assessmentDetailsPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                // Load assessment details.
                LoadAssessmentDetails
                    (Convert.ToInt32(ViewExternalAssessorProfile_assessorIDHiddenField.Value), 
                    e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewExternalAssessorProfile_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when convert to assessor is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/>that holds the event data.
        /// </param>
        protected void ViewExternalAssessorProfile_convertToAssessorImageButton_Click(object sender,
            System.Web.UI.ImageClickEventArgs e)
        {
            IDbTransaction transaction = null;
            try
            {
                int externalAssessorID = Convert.ToInt32(ViewExternalAssessorProfile_assessorIDHiddenField.Value);

                string confirmationCode = new EncryptAndDecrypt().EncryptString(Utility.RandomString(15));
                string password = Utility.RandomString(10);
                int userID = 0;

                transaction = new TransactionManager().Transaction;

                //Insert the external assessor to pras_user/roles/ tables
                InsertCorporateUser(password, confirmationCode, out userID, transaction as DbTransaction);

                //Get external assessor details
                ExternalAssessorDetail assessorDetail = new AssessorBLManager().
                    GetExternalAssessorByID(externalAssessorID);

                //Insert/update assessor details
                InsertAssessorDetail(userID, assessorDetail.Skills, transaction as DbTransaction);

                //Transfer the external assessor assessed details to registered user
                new AssessorBLManager().UpdateExternalAssessorToRegisteredUser(externalAssessorID, userID, assessorDetail.Email,
                    transaction);

                //Commit the transaction
                transaction.Commit();

                try
                {
                    // Sent alert mail to the assessor asynchronously.
                    AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendConfirmationCodeEmail);
                    IAsyncResult result =
                        taskDelegate.BeginInvoke(ViewExternalAssessorProfile_emailValueLabel.Text.Trim(),
                        confirmationCode, password, new AsyncCallback(SendConfirmationCodeEmailCallBack), taskDelegate);
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }

                //if (!Utility.IsNullOrEmpty(transaction))
                //    transaction.Rollback();

                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseViewExternalAssessorProfile","<script language='javascript'>CloseViewAssessorProfile();</script>", false);

            }
            catch (Exception exp)
            {
                if (!Utility.IsNullOrEmpty(transaction))
                    transaction.Rollback();
                
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewExternalAssessorProfile_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that retrieves the query string values and assign it to 
        /// hidden fields.
        /// </summary>
        private bool RetrieveQueryStringValues()
        {
            if (Utility.IsNullOrEmpty(Request.QueryString["assessorid"]))
            {
                base.ShowMessage(ViewExternalAssessorProfile_topErrorMessageLabel, "Assessor ID is not passed");
                return false;
            }

            // Get assessor ID.
            int assessorID = 0;
            int.TryParse(Request.QueryString["assessorid"], out assessorID);
            if (assessorID == 0)
            {
                base.ShowMessage(ViewExternalAssessorProfile_topErrorMessageLabel, "Invalid assessor ID");
                return false;
            }

            // Assign assessor ID to hidden field.
            ViewExternalAssessorProfile_assessorIDHiddenField.Value = assessorID.ToString();

            return true;
        }

        /// <summary>
        /// Method that loads the assessor details.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        private void LoadAssessorDetail(int assessorID)
        {
            // Get assessor detail.
            ExternalAssessorDetail assessorDetail = new AssessorBLManager().
                GetExternalAssessorByID(assessorID);

            if (assessorDetail == null)
            {
                base.ShowMessage(ViewExternalAssessorProfile_topErrorMessageLabel, "No such assessor exist");
                return;
            }

            // Assign first name.
            ViewExternalAssessorProfile_nameValueLabel.Text = assessorDetail.FirstName;
            ViewExternalAssessorProfile_firstNameHiddenField.Value = assessorDetail.FirstName;
            

            // Append last name.
            if (!Utility.IsNullOrEmpty(assessorDetail.LastName))
            {
                ViewExternalAssessorProfile_nameValueLabel.Text = ViewExternalAssessorProfile_nameValueLabel.Text + " " +
                    assessorDetail.LastName;

                ViewExternalAssessorProfile_lastNameHiddenField.Value = assessorDetail.LastName;
            }
            ViewExternalAssessorProfile_emailValueLabel.Text = assessorDetail.Email;
            ViewExternalAssessorProfile_mobilePhoneValueLabel.Text = assessorDetail.ContactNumber;
            
            
            List<SkillDetail> assessorSkills = null;

            if (assessorDetail.Skills != null && assessorDetail.Skills.Count > 0)
            {
                // Instantiate the list.
                assessorSkills = new List<SkillDetail>();

                // Get distinct categories.
                var distinctCategories = (from skill in assessorDetail.Skills
                                          select new { CategoryID = skill.CategoryID, CategoryName = skill.CategoryName }).Distinct().ToList();

                // Loop through the distict categories and construct the comma 
                // separated skills.
                foreach (var distinctCategory in distinctCategories)
                {
                    string subjects = string.Join(",", (from skill in assessorDetail.Skills
                                                        where
                                                            skill.CategoryID == distinctCategory.CategoryID
                                                        select skill.SkillName).ToArray());

                    // Add to assessor skills.
                    assessorSkills.Add(new SkillDetail(distinctCategory.CategoryID, distinctCategory.CategoryName, subjects));
                }
            }

            // Assign skills.
            ViewExternalAssessorProfile_skillsDatalist.DataSource = assessorSkills;
            ViewExternalAssessorProfile_skillsDatalist.DataBind();
        }

        /// <summary>
        /// Method that loads the assessment details.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        private void LoadAssessmentDetails(int assessorID, int pageNumber)
        {
            AssessmentSearchCriteria assessmentSearchCriteria = new AssessmentSearchCriteria();
            assessmentSearchCriteria.InterviewName = null;
            assessmentSearchCriteria.InterviewDescription = null;
            assessmentSearchCriteria.AssessmentStatus = null;
            assessmentSearchCriteria.PositionProfileID = null;

            assessmentSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
            assessmentSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();
            assessmentSearchCriteria.AssessorID = assessorID;
            assessmentSearchCriteria.CurrentPage = pageNumber;

            int totalRecords = 0;

            // Get assessor assessments (completed & in-progress).
            List<AssessorAssessmentDetail> assessmentDetails = new AssessorBLManager().
                GetAssessorAssessments(assessorID,
                pageNumber, base.GridPageSize, out totalRecords);

            ViewAsssessorProfile_assessmentDetailsPageNavigator.TotalRecords = totalRecords;
            ViewAsssessorProfile_assessmentDetailsPageNavigator.PageSize = base.GridPageSize;

            ViewExternalAssessorProfile_assessmentDetailsGridView.DataSource = assessmentDetails;
            ViewExternalAssessorProfile_assessmentDetailsGridView.DataBind();

            // Assign total records to count label.
            ViewExternalAssessorProfile_assessmentCountLabel.Text = totalRecords.ToString();
            ViewExternalAssessorProfile_assessmentCountLabel.ToolTip = string.Format("{0} assessment(s)", totalRecords);
        }

        /// <summary>
        /// Method that shows the menu type and respective section based on the 
        /// selection.
        /// </summary>
        private void ShowMenuType()
        { 
            // Get menu type.
            string menuType = ViewExternalAssessorProfile_selectedMenuHiddenField.Value;

            // Hide all div.
            ViewExternalAssessorProfile_contactInfoDiv.Attributes["style"] = "display:none";
            ViewExternalAssessorProfile_skillsDiv.Attributes["style"] = "display:none";
            ViewExternalAssessorProfile_assessmentDetailsDiv.Attributes["style"] = "display:none";

            // Reset color.
            ViewExternalAssessorProfile_contactInfoLinkButton.CssClass = "assessor_profile_link_button";
            ViewExternalAssessorProfile_skillsLinkButton.CssClass = "assessor_profile_link_button";
            ViewExternalAssessorProfile_assessmentDetailsLinkButton.CssClass = "assessor_profile_link_button";

            if (menuType == "CI")
            {
                ViewExternalAssessorProfile_contactInfoDiv.Attributes["style"] = "display:block";
                ViewExternalAssessorProfile_contactInfoLinkButton.CssClass = "assessor_profile_link_button_selected";
            }
            else if (menuType == "SK")
            {
                ViewExternalAssessorProfile_skillsDiv.Attributes["style"] = "display:block";
                ViewExternalAssessorProfile_skillsLinkButton.CssClass = "assessor_profile_link_button_selected";
            }
            else if (menuType == "AD")
            {
                ViewExternalAssessorProfile_assessmentDetailsDiv.Attributes["style"] = "display:block";
                ViewExternalAssessorProfile_assessmentDetailsLinkButton.CssClass = "assessor_profile_link_button_selected";
            }
        }

        /// <summary>
        /// A Method that insert the new corporate child user
        /// in to the repository
        /// </summary>
        /// <param name="confirmationCode">
        /// A <see cref="System.String"/> that holds the confirmation code to be 
        /// sent to the user 
        /// </param>
        private void InsertCorporateUser(string password, string confirmationCode,
            out int userID, DbTransaction transaction)
        {
            UserRegistrationInfo userRegistrationInfo = null;
            RegistrationConfirmation registrationConfirmation = null;
            try
            {
                password = new EncryptAndDecrypt().EncryptString(password);

                // Get the role id
                int roleID = new UserRegistrationBLManager().
                    GetRoleIDByRoleCode(Constants.RoleCodeConstants.ASSESSOR);

                userRegistrationInfo = new UserRegistrationInfo();
                userRegistrationInfo.UserEmail = ViewExternalAssessorProfile_emailValueLabel.Text.Trim();
                userRegistrationInfo.Password = password.Trim();
                userRegistrationInfo.FirstName = ViewExternalAssessorProfile_firstNameHiddenField.Value.Trim();
                userRegistrationInfo.LastName = ViewExternalAssessorProfile_lastNameHiddenField.Value.Trim();
                
                userRegistrationInfo.IsCustomerAdmin = false;
                userRegistrationInfo.SubscriptionRole = Enum.GetName(typeof(SubscriptionRolesEnum), 4);

                userRegistrationInfo.CorporateRoles = roleID.ToString();
                registrationConfirmation = new RegistrationConfirmation();
                registrationConfirmation.ConfirmationCode = confirmationCode;

                userID = new UserRegistrationBLManager().InsertCorporateUser(base.tenantID, userRegistrationInfo,
                    registrationConfirmation, transaction);
            }
            catch
            {
               throw;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(transaction)) transaction = null;
                if (userRegistrationInfo != null) userRegistrationInfo = null;
                if (registrationConfirmation != null) registrationConfirmation = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }

        }

       
        /// <summary>
        /// A Method that gets the body content of the email
        /// </summary>
        /// A <see cref="System.String"/> that holds the user email
        /// </param>
        /// <param name="Confirmation_Code">
        /// A <see cref="System.String"/> that holds the encrypted 
        /// confirmation code to activate his/her account
        /// </param>
        /// <returns>
        /// A <see cref="System.String"/> that contains the activation mail body
        /// </returns>
        private string GetMailBody(string userEmail, 
            string confirmationCode, string userName, string password)
        {
            StringBuilder sbBody = new StringBuilder();
            try
            {
                sbBody.Append("Dear ");
                sbBody.Append(ViewExternalAssessorProfile_firstNameHiddenField.Value.Trim());
                sbBody.Append(" ");
                sbBody.Append(ViewExternalAssessorProfile_lastNameHiddenField.Value.Trim());
                sbBody.Append(",");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Welcome to ForteHCM !");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This is to acknowledge that your have been created as an assessor.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Your user: " + userName);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Your password: " + password);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("To Activate your account, please copy and paste the ");
                sbBody.Append("below URL in the browser or click on the link");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                string strUri =
                    string.Format(WebConfigurationManager.AppSettings["SIGNUP_ACTIVATIONURL"] + "?" +
                    WebConfigurationManager.AppSettings["SIGNUP_USERNAME"] + "={0}&" +
                    WebConfigurationManager.AppSettings["SIGNUP_CONFIRMATION"] + "={1}",
                    userEmail, confirmationCode);
                sbBody.Append("<a href='" + strUri + "'");
                sbBody.Append(" >" + strUri + "</a>");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Thank you for your interest in ForteHCM.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("============================== Disclaimer ==============================");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("=========================== End Of Disclaimer ============================");
                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        /// <summary>
        /// Method that send mail to the requested external assessor indicating
        /// </summary>
        /// <param name="userEmail">
        /// A <see cref="string"/> that holds the user email
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="string"/> that holds the confirmation code
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendConfirmationCodeEmail(string userEmail, 
            string confirmationCode, string password)
        {
            try
            {
                MailDetail mailDetails = new MailDetail();
                EmailHandler Email = new EmailHandler();
                Email.SendMail(userEmail, HCMResource.UserRegistration_MailActivationCodeSubject,
                    GetMailBody(new EncryptAndDecrypt().EncryptString(userEmail), confirmationCode, userEmail, password));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that inserts the assessor details
        /// </summary>
        private void InsertAssessorDetail(int userID, List<SkillDetail> skillList,
            DbTransaction transaction)
        {
            AssessorDetail assessorDetails = new AssessorDetail();
            assessorDetails.UserID = userID;
            assessorDetails.UserEmail = ViewExternalAssessorProfile_emailValueLabel.Text.Trim();
            assessorDetails.Skill = string.Empty;
            assessorDetails.AlternateEmailID = ViewExternalAssessorProfile_emailValueLabel.Text.Trim();
            assessorDetails.FirstName = ViewExternalAssessorProfile_firstNameHiddenField.Value.Trim();
            assessorDetails.LastName = ViewExternalAssessorProfile_lastNameHiddenField.Value.Trim();
            assessorDetails.CreatedBy = base.userID;
            assessorDetails.NonAvailabilityDates = null;
            assessorDetails.Image = null;
            assessorDetails.Mobile = ViewExternalAssessorProfile_mobilePhoneValueLabel.Text.Trim();
            assessorDetails.HomePhone = ViewExternalAssessorProfile_mobilePhoneValueLabel.Text.Trim();
            assessorDetails.AdditionalInfo = null;
            assessorDetails.LinkedInProfile = null;

            
            //Save assessor details
            new AssessorBLManager().SaveAssessor(assessorDetails, 
                skillList, transaction);
        }

        #endregion Private Methods

        #region Asynchronous Method Handlers

        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendConfirmationCodeEmailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Asynchronous Method Handlers
       
}
}