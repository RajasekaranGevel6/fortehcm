﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewOnlineSchedule.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.ViewOnlineSchedule"
    Title="View Schedule" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ViewOnlineSchedule_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="td_padding_top_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="ViewOnlineSchedule_titleLiteral" runat="server" Text="View Schedule"></asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="ViewOnlineSchedule_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:self.close();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_view_online_schedule_bg">
                    <tr>
                        <td style="width: 100%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="height: 18px">
                                        <asp:UpdatePanel ID="ViewOnlineSchedule_messageUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="ViewOnlineSchedule_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknErrorMessage" Width="100%" Text=""></asp:Label>
                                                <asp:Label ID="ViewOnlineSchedule_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknSuccessMessage" Width="100%" Text=""></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 100%">
                                        <asp:UpdatePanel ID="ViewOnlineSchedule_inputControlsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 100%" valign="top" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="height: 4px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" class="view_online_schedule_candidate_info_bg" valign="middle">
                                                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td style="width: 16%" align="left" class="td_height_20" valign="middle">
                                                                                    <asp:Label ID="ViewOnlineSchedule_assessorNameLabel" runat="server" Text="Candidate Name"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 33%" align="left" valign="middle">
                                                                                    <asp:Label ID="ViewOnlineSchedule_assessorNameValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                        Text=""></asp:Label>
                                                                                </td>
                                                                                <td style="width: 16%" align="left" class="td_height_20" valign="middle">
                                                                                    <asp:Label ID="ViewOnlineSchedule_emailLabel" runat="server" Text="Candidate Email"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 33%" align="left" valign="middle">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="ViewOnlineSchedule_emailValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                                    Text=""></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:ImageButton ID="ViewOnlineSchedule_emailImageButton" runat="server" ToolTip="Click here to email the candidate"
                                                                                                    SkinID="sknMailImageButton" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 4px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="ViewOnlineSchedule_schedulesLiteral" runat="server" Text="Schedules"></asp:Literal>&nbsp;<asp:Label
                                                                                        ID="ViewOnlineSchedule_schedulesSortHelpLabel" runat="server" SkinID="sknLabelText"
                                                                                        Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                                                    <asp:HiddenField ID="ViewOnlineSchedule_candidateSessionKeyHiddenField" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" class="view_online_schedule_grid_body_bg">
                                                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td style="width: 100%">
                                                                                    <div style="height: 260px; overflow: auto;">
                                                                                        <asp:GridView ID="ViewOnlineSchedule_schedulesGridView" runat="server" AllowSorting="True"
                                                                                            AutoGenerateColumns="False" Width="100%" OnRowDataBound="ViewOnlineSchedule_schedulesGridView_RowDataBound"
                                                                                            OnRowCommand="ViewOnlineSchedule_schedulesGridView_RowCommand" OnRowCreated="ViewOnlineSchedule_schedulesGridView_RowCreated"
                                                                                            OnSorting="ViewOnlineSchedule_schedulesGridView_Sorting" Height="100%">
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="ViewOnlineSchedule_schedulesGridView_skillIDHiddenField" runat="server"
                                                                                                            Value='<%# Eval("SkillID") %>' />
                                                                                                        <asp:HiddenField ID="ViewOnlineSchedule_schedulesGridView_timeSlotIDHiddenField"
                                                                                                            runat="server" Value='<%# Eval("TimeSlotID") %>' />
                                                                                                        <asp:HiddenField ID="ViewOnlineSchedule_schedulesGridView_assessorIDHiddenField"
                                                                                                            runat="server" Value='<%# Eval("AssessorID") %>' />
                                                                                                        <asp:HiddenField ID="ViewOnlineSchedule_schedulesGridView_statusHiddenField" runat="server"
                                                                                                            Value='<%# Eval("SessionStatus") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="60px" HeaderStyle-HorizontalAlign="Left"
                                                                                                    ItemStyle-HorizontalAlign="Center">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="ViewOnlineSchedule_schedulesGridView_viewAssessorProfileImageButton"
                                                                                                            SkinID="sknViewAssessorProfileImageButton" ToolTip="View Assessor Profile" runat="server" />
                                                                                                        <asp:ImageButton ID="ViewOnlineSchedule_schedulesGridView_emailAssessorImageButton"
                                                                                                            SkinID="sknMailImageButton" ToolTip="Email Assessor" runat="server" />
                                                                                                        <asp:ImageButton ID="ViewOnlineSchedule_schedulesGridView_unscheduleImageButton"
                                                                                                            SkinID="sknUnscheduleImageButton" ToolTip="Unschedule Candidate" CommandName="Unschedule"
                                                                                                            CommandArgument="<%# Container.DataItemIndex %>" runat="server" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Assessor" SortExpression="ASSESSOR">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="ViewOnlineSchedule_schedulesGridView_assessorLabel" runat="server"
                                                                                                            ToolTip="Click here to view assessor profile" Text='<%# Eval("AssessorName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Skill" SortExpression="SKILL">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="ViewOnlineSchedule_schedulesGridView_skillNameLabel" runat="server"
                                                                                                            Text='<%# Eval("SkillName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Interview Date" SortExpression="INTERVIEW_DATE">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="ViewOnlineSchedule_schedulesGridView_interviewDateLabel" runat="server"
                                                                                                            Text='<%# Eval("InterviewDateString") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Time Slot" SortExpression="TIME_SLOT">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="ViewOnlineSchedule_schedulesGridView_timeSlotLabel" runat="server"
                                                                                                            Text='<%# Eval("TimeSlot") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Interview Status" SortExpression="STATUS">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="ViewOnlineSchedule_schedulesGridView_statusLabel" runat="server" Text='<%# Eval("SessionStatusName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                            <EmptyDataTemplate>
                                                                                                <table style="width: 100%; height: 100%">
                                                                                                    <tr>
                                                                                                        <td style="height: 90px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="middle" align="center" style="height: 100%" class="error_message_text_normal">
                                                                                                            No schedules found to display
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </EmptyDataTemplate>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 8px">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:HiddenField ID="ViewOnlineSchedule_timeSlotIDHiddenField" runat="server" />
                                                            <asp:HiddenField ID="ViewOnlineSchedule_assessorIDHiddenField" runat="server" />
                                                            <asp:HiddenField ID="ViewOnlineSchedule_skillIDHiddenField" runat="server" />
                                                            <asp:Panel ID="ViewOnlineSchedule_unschedulePanel" runat="server" Style="display: none;
                                                                height: 206px" CssClass="popupcontrol_confirm">
                                                                <div id="ViewOnlineSchedule_unscheduleHiddenDIV" style="display: none">
                                                                    <asp:Button ID="ViewOnlineSchedule_unscheduleHiddenButton" runat="server" />
                                                                </div>
                                                                <uc2:ConfirmMsgControl ID="ViewOnlineSchedule_unscheduleConfirmMsgControl" runat="server"
                                                                    OnOkClick="ViewOnlineSchedule_unschedule_OkClick" />
                                                            </asp:Panel>
                                                            <ajaxToolKit:ModalPopupExtender ID="ViewOnlineSchedule_unschedulePopupExtenderControl"
                                                                runat="server" PopupControlID="ViewOnlineSchedule_unschedulePanel" TargetControlID="ViewOnlineSchedule_unscheduleHiddenButton"
                                                                BackgroundCssClass="modalBackground">
                                                            </ajaxToolKit:ModalPopupExtender>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="ViewOnlineSchedule_buttonControlsUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="td_height_5" colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="left">
                                    &nbsp;<asp:LinkButton ID="ViewOnlineSchedule_cancelLinkButton" runat="server" Text="Cancel"
                                        SkinID="sknPopupLinkButton" OnClientClick="javascript:self.close();" ToolTip="Click here to close the window"> </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_2">
            </td>
        </tr>
    </table>
</asp:Content>
