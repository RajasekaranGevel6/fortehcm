﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="SubjectSummary.aspx.cs" Inherits="Forte.HCM.UI.Popup.SubjectSummary" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="SubjectSummary_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <asp:Label ID="SubjectSummary_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SubjectSummary_titleLiteral" runat="server" Text="Subjects Contributed"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SubjectSummary_closeImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td align="center" class="popup_td_padding_10">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="grid_body_bg">
                                        <div id="SubjectSummary_subjectNameListDIV" style="height: 280px; overflow: auto;">
                                            <asp:GridView ID="SubjectSummary_subjectsGridView" runat="server" SkinID="sknWrapHeaderGrid"
                                                AllowSorting="true" OnRowDataBound="SubjectSummary_subjectsGridView_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Subject ID" DataField="SubjectID" ItemStyle-Width="20px" />
                                                    <asp:BoundField HeaderText="Subject Name" DataField="SubjectName" ItemStyle-Width="190px" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:PageNavigator ID="SubjectSummary_pageNavigator" runat="server" />
                        </td>
                    </tr>
                     <tr>
                        <td class="td_height_20">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_left_8">
                <asp:LinkButton ID="SubjectSummary_closeLinkButton" SkinID="sknPopupLinkButton" runat="server"
                    Text="Cancel" OnClientClick="Javascript:CloseMe();" />
            </td>
        </tr>
    </table>
</asp:Content>
