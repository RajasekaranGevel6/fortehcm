﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Forte.HCM.Trace;
using Forte.HCM.EventSupport;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

using OfficeOpenXml;
namespace Forte.HCM.UI.Popup
{
    public partial class ViewPositionProfileActivityLogPopup : PageBase
    {
        #region Events Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Subscribe to message thrown events.
                ViewPositonProfileActivityLogPopup_PositionProfileViewerControl.ControlMessageThrown += new CommonControls.PositionProfileViewerControl.ControlMessageThrownDelegate(ViewPositonProfileActivityLogPopup_PositionProfileViewerControl_ControlMessageThrown);

                // Set page size.
                ViewPositonProfileActivityLogPopup_PositionProfileViewerControl.GridPageSize = base.GridPageSize;

                // Disallow change candidate.
                ViewPositonProfileActivityLogPopup_PositionProfileViewerControl.AllowChangeCandidate = false;
                //Tittle of the page
                 Master.SetPageCaption("Position Profile Activity Log");
                
                if (!IsPostBack)
                {
                    // Check if positionprofile ID is passed.
                    if (!Utility.IsNullOrEmpty(Request.QueryString["positionProfileID"]))
                    {
                        int positioProfileID = 0;
                        if (int.TryParse(Request.QueryString["positionProfileID"].ToString().Trim(), out positioProfileID) == true)
                        {
                            ViewPositonProfileActivityLogPopup_PositionProfileViewerControl.PositionProfileID = positioProfileID;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewPositonProfileActivityLogPopup_errorMessageLabel, exp.Message);
            }
        }

        void ViewPositonProfileActivityLogPopup_PositionProfileViewerControl_ControlMessageThrown
            (object sender, EventSupport.ControlMessageEventArgs c)
        {
            // Check if message is empty. If empty clear all messages.
            if (Utility.IsNullOrEmpty(c.Message))
            {
                ViewPositonProfileActivityLogPopup_errorMessageLabel.Text = string.Empty;
                ViewPositonProfileActivityLogPopup_successMessageLabel.Text = string.Empty;
                return;
            }

            // Show the message.
            if (c.MessageType == MessageType.Error)
            {
                base.ShowMessage(ViewPositonProfileActivityLogPopup_errorMessageLabel, c.Message);
            }
        }

        private void ViewPositonProfileActivityLogPopup_candidateActivityViewerControl_ControlMessageThrown
            (object sender, ControlMessageEventArgs c)
        {
           
        }

        /// <summary>
        /// Handler method that will be called when the reset button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void ViewPositonProfileActivityLogPopup_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ViewPositonProfileActivityLogPopup_errorMessageLabel.Text = string.Empty;
                ViewPositonProfileActivityLogPopup_successMessageLabel.Text = string.Empty;

                // Reset the control.
                ViewPositonProfileActivityLogPopup_PositionProfileViewerControl.Reset();

                // Check if candidate ID is passed and reset it to the control.
                if (!Utility.IsNullOrEmpty(Request.QueryString["positionProfileid"]))
                {
                    int positionProfileID = 0;
                    if (int.TryParse(Request.QueryString["positionProfileid"].ToString().Trim(), out positionProfileID) == true)
                    {
                        ViewPositonProfileActivityLogPopup_PositionProfileViewerControl.PositionProfileID = positionProfileID;
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewPositonProfileActivityLogPopup_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the download link is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will export the activity log to an excel file.
        /// </remarks>
        protected void ViewPositonProfileActivityLogPopup_downloadLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ViewPositonProfileActivityLogPopup_errorMessageLabel.Text = string.Empty;
                ViewPositonProfileActivityLogPopup_successMessageLabel.Text = string.Empty;

                // Get the table that contains the rows for download.
                DataTable table = ViewPositonProfileActivityLogPopup_PositionProfileViewerControl.
                    GetDownloadTable();

                // Check if table is not null and contain rows.
                if (table == null || table.Rows.Count == 0)
                {
                    // Show the error message, only if no error messages is
                    // currently shown in the page. This scenario will occur 
                    // only if 'no candidate was selected'. This error message
                    // was already shown on the page.
                    if (ViewPositonProfileActivityLogPopup_errorMessageLabel.Text.Trim().Length != 0)
                        return;

                    ShowMessage(ViewPositonProfileActivityLogPopup_errorMessageLabel,
                        "No activities found to download");

                    return;
                }

                // Remove the row where 'total' columns is not null (which 
                // contains the total records used for paging).
                DataRow[] foundRows =
                    table.Select("[TOTAL] is not null", "", DataViewRowState.OriginalRows);

                // Check if rows are found with 'total column is not null'
                if (foundRows != null && foundRows.Length > 0)
                {
                    // Remove the 0th row.
                    table.Rows.Remove(foundRows[0]);
                }

                // Remove the unncessary columns from the table.
                table.Columns.Remove("ACTIVITY_BY");
                table.Columns.Remove("ACTIVITY_TYPE");
                table.Columns.Remove("TOTAL");

                // Change the column names.
                table.Columns["ACTIVITY_DATE"].ColumnName = "Activity Date";
                table.Columns["ACTIVITY_BY_NAME"].ColumnName = "Activity By";
                table.Columns["ACTIVITY_TYPE_NAME"].ColumnName = "Activity";
                table.Columns["COMMENTS"].ColumnName = "Comments";

                using (ExcelPackage pck = new ExcelPackage())
                {
                    // Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add
                        (Constants.ExcelExportSheetName.POSITION_PROFILE_ACTIVITY_LOG);

                    // Load the datatable into the sheet, starting from the 
                    // cell A1 and print the column names on row 1.
                    ws.Cells[Constants.ExcelExport.STARTING_CELL].LoadFromDataTable(table, true);

                    string decimalFormat = Constants.ExcelExportFormats.DECIMAL_COMMA;

                    foreach (DataColumn column in table.Columns)
                    {
                        if (column.DataType == typeof(System.DateTime))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                            {
                                col.Style.Numberformat.Format = Constants.ExcelExportFormats.DATE;
                            }
                        }
                        else if (column.DataType == typeof(decimal))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                            {
                                if (decimalFormat == "C")
                                {
                                    for (int row = 2; row <= ws.Dimension.End.Row; row++)
                                    {
                                        ws.Cells[row, column.Ordinal + 1].Value =
                                            ws.Cells[row, column.Ordinal + 1].Value != null ? ws.Cells[row, column.Ordinal + 1].Value.ToString().Replace(".", ",") : string.Empty;
                                    }
                                    col.Style.Numberformat.Format = "0\",\"0000";
                                }
                                else
                                    col.Style.Numberformat.Format = "###0.0000";
                            }
                        }
                    }

                    // Construct file name.
                    string fileName = string.Format("{0}_{1}.xlsx",
                        Constants.ExcelExportFileName.POSITION_PROFILE_ACTIVITY_LOG,
                        Utility.GetValidExportFileNameString(ViewPositonProfileActivityLogPopup_PositionProfileViewerControl.PositionProfileName, 50));

                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                    Response.BinaryWrite(pck.GetAsByteArray());
                    Response.End();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewPositonProfileActivityLogPopup_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}