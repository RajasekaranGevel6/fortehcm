﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="SearchClientRequest.aspx.cs" Inherits="Forte.HCM.UI.Popup.SearchClientRequest" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ID="SearchClientRequest_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">
        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl) 
        {
            var ctrlDisplayField = '<%= Request.QueryString["ctrlId"] %>';
            var ctrlHiddenField = '<%= Request.QueryString["ctrlhidId"] %>';
            var ctrlHiddenTagId = '<%= Request.QueryString["ctrlhiddenTagId"] %>';
            var ctrlHiddenPositionId = '<%= Request.QueryString["ctrlhiddenPositionId"] %>';
            var ctrlTextBoxId = '<%= Request.QueryString["ctrlTextBoxId"] %>';
            var ctrlAppendTextBoxId = '<%= Request.QueryString["ctrlAppendTextBoxId"] %>';
            var crlHiddenTagWithWeightageId = '<%= Request.QueryString["crlHiddenTagWithWeightageId"] %>';
            var parentPage = '<%= Request.QueryString["parent"] %>';
            var ctrlKeyWordHiddenId = '<%= Request.QueryString["ctrlKeyWordHiddenId"] %>';
            

            var from = '<%= Request.QueryString["from"] %>';
            var ctrlId = '<%= Request.QueryString["ctrlId"] %>';
            var ctrlName = '<%= Request.QueryString["ctrlname"] %>';
            var btncnrl = '<%= Request.QueryString["btncnrl"] %>'; 

            if (window.dialogArguments) 
            {
                window.opener = window.dialogArguments;
            }

            if (from == 'ATS')
            {
                window.opener.document.getElementById(ctrlId).value
                    = document.getElementById(ctrl.id.replace("SearchClientRequest_selectLinkButton", "SearchClientRequest_selectClientRequestNumberHiddenfield")).value;
                
                window.opener.document.getElementById(ctrlName).value
                    = document.getElementById(ctrl.id.replace("SearchClientRequest_selectLinkButton", "SearchClientRequest_selectPositionProfileHiddenfield")).value;

                self.close();
            }
            
            // Set the display field value. Replace the 'link button name' with the 
            // 'position profile hidden field name' and retrieve the value.
            if (ctrlDisplayField != null && ctrlDisplayField != '')
            {
                if (parentPage != null && parentPage == 'test')
                {
                    window.opener.document.getElementById(ctrlDisplayField).value
                        = document.getElementById(ctrl.id.replace("SearchClientRequest_selectLinkButton", "SearchClientRequest_selectPositionProfileHiddenfield")).value;
                }
                else
                {
                    window.opener.document.getElementById(ctrlDisplayField).value
                        = document.getElementById(ctrl.id.replace("SearchClientRequest_selectLinkButton", "SearchClientRequest_selectClientRequestNumberHiddenfield")).value;
                }
            }

            // Set the ID hidden field value. Replace the 'link button name' with the 
            // 'tags hidden field name' and retrieve the value.
            if (ctrlHiddenTagId != null && ctrlHiddenTagId != '') {
                window.opener.document.getElementById(ctrlHiddenTagId).value
                                = document.getElementById(ctrl.id.replace("SearchClientRequest_selectLinkButton", "SearchClientRequest_tagsWithNoWeightagesHiddenField")).value;
                //= document.getElementById(ctrl.id.replace("SearchClientRequest_selectLinkButton", "SearchClientRequest_selectTagsHiddenfield")).value;
            }
            // Set the ID hidden field value. Replate the link button name with the
            // tags hidden field name and retrieve the value
            if (crlHiddenTagWithWeightageId != null && crlHiddenTagWithWeightageId != '' && crlHiddenTagWithWeightageId != undefined) {
                window.opener.document.getElementById(crlHiddenTagWithWeightageId).value
                                = document.getElementById(ctrl.id.replace("SearchClientRequest_selectLinkButton", "SearchClientRequest_selectTagsKeyWordsHiddenfield")).value;
            }

            if (ctrlTextBoxId != null && ctrlTextBoxId != '' && ctrlTextBoxId != undefined) {
                window.opener.document.getElementById(ctrlTextBoxId).value
                            = document.getElementById(ctrl.id.replace("SearchClientRequest_selectLinkButton", "SearchClientRequest_tagsWithNoWeightagesHiddenField")).value;

            }

            

            if (ctrlAppendTextBoxId != null && ctrlAppendTextBoxId != '') {
                var WindowValue = window.opener.document.getElementById(ctrlAppendTextBoxId).value;
                if (WindowValue.length == 0) {
                    window.opener.document.getElementById(ctrlAppendTextBoxId).value
                            = document.getElementById(ctrl.id.replace("SearchClientRequest_selectLinkButton", "SearchClientRequest_tagsWithNoWeightagesHiddenField")).value;
                }
                else {
                    var ContentValue = document.getElementById(ctrl.id.replace("SearchClientRequest_selectLinkButton", "SearchClientRequest_tagsWithNoWeightagesHiddenField")).value;
                    var SplitContent = ContentValue.split(',');
                    var ToAppend = '';
                    WindowValue += ',';
                    for (var i = 0; i < SplitContent.length; i++) {
                        if (WindowValue.toLowerCase().indexOf(SplitContent[i].toLowerCase() + ',') < 0) {
                            ToAppend += SplitContent[i];
                            ToAppend += ',';
                        }
                    }
                    if (ToAppend.length > 0) {
                        ToAppend = ToAppend.substring(0, ToAppend.length - 1);
                        window.opener.document.getElementById(ctrlAppendTextBoxId).value = WindowValue + ToAppend;
                    }
                }
            }

            if (ctrlHiddenField != null && ctrlHiddenField != '') {
                window.opener.document.getElementById(ctrlHiddenField).value
                    = document.getElementById(ctrl.id.replace("SearchClientRequest_selectLinkButton", "SearchClientRequest_genIDHiddenField")).value;
            }
            // Set the Position profile display field to the hidden field.
            // This will be help full when the position profile text box is read only.
            if (ctrlHiddenPositionId != null && ctrlHiddenPositionId != '') {
                window.opener.document.getElementById(ctrlHiddenPositionId).value
                    = document.getElementById(ctrl.id.replace("SearchClientRequest_selectLinkButton", "SearchClientRequest_selectClientRequestNumberHiddenfield")).value;
            }

            // Set list of keywords against this position profile
            if (ctrlKeyWordHiddenId != null && ctrlKeyWordHiddenId != '' && ctrlKeyWordHiddenId != undefined) {
                window.opener.document.getElementById(ctrlKeyWordHiddenId).value
                            = document.getElementById(ctrl.id.replace("SearchClientRequest_selectLinkButton", "SearchClientRequest_tagsWithNoWeightagesHiddenField")).value;
            }

            // Trigger the click event of the refresh button in the parent page.
            if (btncnrl != null && window.opener.document.getElementById(btncnrl) != null)
                window.opener.document.getElementById(btncnrl).click();


            self.close();
        }
    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchClientRequest_headerLiteral" runat="server" Text="Search Position Profile"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SearchClientRequest_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" align="left" class="popup_td_padding_10">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="msg_align">
                            <asp:UpdatePanel ID="SearchClientRequest_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="SearchClientRequest_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="SearchClientRequest_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="SearchClientRequest_searchButton" />
                                    <asp:AsyncPostBackTrigger ControlID="SearchClientRequest_pageNavigator" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="popup_td_padding_10">
                            <div id="SearchClientRequest_searchCriteriasDiv" runat="server" style="display: block;">
                                <asp:UpdatePanel ID="SearchClientRequest_searchCriteriaUpdatePanel" runat="server"
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table width="100%" border="0" cellspacing="3%" cellpadding="2%" align="left">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="SearchClientRequest_positionProfileLabel" runat="server" Text="Position Profile Name"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="SearchClientRequest_positionProfileTextBox" MaxLength="200" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="SearchClientRequest_clientNameLabel" runat="server" Text="Client Name"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="SearchClientRequest_clientNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                    <ajaxToolKit:FilteredTextBoxExtender ID="SearchClientRequest_clientNameFileteredTextBoxExtender"
                                                        runat="server" TargetControlID="SearchClientRequest_clientNameTextBox" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers">
                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="SearchClientRequest_positionNameHeadLabel" runat="server" Text="Position Name"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="SearchClientRequest_positionNameTextBox" MaxLength="50" runat="server"></asp:TextBox>
                                                    <ajaxToolKit:FilteredTextBoxExtender ID="SearchClientRequest_positionNameFileteredTextBoxExtender"
                                                        runat="server" TargetControlID="SearchClientRequest_positionNameTextBox" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers"
                                                        FilterMode="ValidChars" FilterInterval="250" ValidChars=" ">
                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                </td>
                                                <td>
                                                    <asp:Label ID="SearchClientRequest_createdDateHeadLabel" runat="server" Text="Created Date"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <table width="65%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 25%">
                                                                <asp:TextBox ID="SearchClientRequest_createdDateTextBox" runat="server" MaxLength="10"
                                                                    AutoCompleteType="None"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 1%">
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="SearchClientRequest_calendarImageButton" SkinID="sknCalendarImageButton"
                                                                    runat="server" ImageAlign="Middle" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <ajaxToolKit:MaskedEditExtender ID="SearchClientRequest_MaskedEditExtender" runat="server"
                                                        TargetControlID="SearchClientRequest_createdDateTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                                        OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Date"
                                                        DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                    <ajaxToolKit:MaskedEditValidator ID="SearchClientRequest_MaskedEditValidator" runat="server"
                                                        ControlExtender="SearchClientRequest_MaskedEditExtender" ControlToValidate="SearchClientRequest_createdDateTextBox"
                                                        EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                        TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                        ValidationGroup="MKE" />
                                                    <ajaxToolKit:CalendarExtender ID="SearchClientRequest_customCalendarExtender" runat="server"
                                                        TargetControlID="SearchClientRequest_createdDateTextBox" CssClass="MyCalendar"
                                                        Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="SearchClientRequest_calendarImageButton" />
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>
                                                    <asp:Label ID="SearchClientRequest_contactNameLabel" runat="server" Text="Contact Name"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="SearchClientRequest_contactNameTextBox" MaxLength="20" runat="server"></asp:TextBox>
                                                    <ajaxToolKit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3"
                                                        runat="server" TargetControlID="SearchClientRequest_contactNameTextBox" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers">
                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                </td>
                                                <td>
                                                    <asp:Label ID="SearchClientRequest_departmentNameLabel" runat="server" Text="Department Name"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="SearchClientRequest_departmentNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                    <ajaxToolKit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4"
                                                        runat="server" TargetControlID="SearchClientRequest_departmentNameTextBox" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers">
                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="SearchClientRequest_locationLabel" runat="server" Text="Location"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="SearchClientRequest_locationTextBox" MaxLength="20" runat="server"></asp:TextBox>
                                                    <ajaxToolKit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1"
                                                        runat="server" TargetControlID="SearchClientRequest_locationTextBox" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers"
                                                        FilterMode="ValidChars" FilterInterval="250" ValidChars="_">
                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                </td>
                                                <td>
                                                    <asp:Label ID="SearchClientRequest_createdByLabel" runat="server" Text="Created By"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="SearchClientRequest_createdByTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                    <ajaxToolKit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2"
                                                        runat="server" TargetControlID="SearchClientRequest_createdByTextBox" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers">
                                                    </ajaxToolKit:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="4">
                                                    <asp:Button ID="SearchClientRequest_searchButton" runat="server" Text="Search" SkinID="sknButtonId"
                                                        OnClick="SearchClientRequest_searchButton_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="popup_td_padding_10">
                            <asp:UpdatePanel ID="SearchClientRequest_searchResultsUpdatePanel" runat="server"
                                UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr id="SearchClientRequest_searchResultsTR" runat="server">
                                            <td class="header_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%">
                                                            <asp:Literal ID="SearchClientRequest_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                ID="SearchQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%" align="right">
                                                            <span id="SearchClientRequest_searchResultsUpSpan" style="display: none;" runat="server">
                                                                <asp:Image ID="SearchClientRequest_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                            </span><span id="SearchClientRequest_searchResultsDownSpan" style="display: block;"
                                                                runat="server">
                                                                <asp:Image ID="SearchClientRequest_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left">
                                                            <div style="height: 180px; overflow: auto;" runat="server" id="SearchClientRequest_searchResultsDiv">
                                                                <asp:GridView ID="SearchClientRequest_searchResultsGridView" runat="server" AutoGenerateColumns="False" SkinID="sknWrapHeaderGrid"
                                                                    GridLines="None" Width="100%" AllowSorting="true" OnSorting="SearchClientRequest_searchResultsGridView_Sorting"
                                                                    OnRowDataBound="SearchClientRequest_searchResultsGridView_RowDataBound" OnRowCreated="SearchClientRequest_searchResultsGridView_RowCreated">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="5%">
                                                                            <ItemTemplate >
                                                                                <asp:LinkButton ID="SearchClientRequest_selectLinkButton" runat="server" Text="Select"
                                                                                    OnClientClick="javascript:return OnSelectClick(this);" ToolTip="<%$ Resources:HCMResource, GridSelectToolTip %>"></asp:LinkButton>
                                                                                <asp:HiddenField ID="SearchClientRequest_selectClientRequestNumberHiddenfield" runat="server"
                                                                                    Value='<%# Eval("ClientRequestNumber") %>' />
                                                                                <asp:HiddenField ID="SearchClientRequest_selectPositionProfileHiddenfield" runat="server"
                                                                                    Value='<%# Eval("PositionProfileName") %>' />
                                                                                <asp:HiddenField ID="SearchClientRequest_selectTagsHiddenfield" runat="server" Value='<%# Eval("Tags") %>' />
                                                                                <asp:HiddenField ID="SearchClientRequest_selectTagsKeyWordsHiddenfield" runat="server"
                                                                                    Value='<% #  Eval("Tags") %>' />
                                                                                <asp:HiddenField ID="SearchClientRequest_genIDHiddenField" runat="server" Value='<%# Eval("genID") %>' />
                                                                                <asp:HiddenField ID="SearchClientRequest_tagsWithNoWeightagesHiddenField" runat="server"
                                                                                    Value='<%# Eval("TagsWithNoWeightage") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField HeaderText="Position Profile Key" DataField="ClientRequestNumber"
                                                                            SortExpression="CLIENTREQUESTNUMBER" ItemStyle-Width="10%" Visible="false"/>
                                                                        <asp:BoundField HeaderText="Position Profile" DataField="PositionProfileName"
                                                                            SortExpression="POSITIONPROFILENAME" ItemStyle-Width="16%" />
                                                                        <asp:BoundField HeaderText="Client Name" DataField="ClientName" SortExpression="CLIENT"
                                                                            ItemStyle-Width="10%" />
                                                                        <asp:BoundField HeaderText="Position Name" DataField="PositionName" SortExpression="POSITIONNAME"
                                                                            ItemStyle-Width="10%" />
                                                                        <asp:BoundField HeaderText="Created Date" DataField="CreatedDate" DataFormatString="{0:MM/dd/yyyy}"
                                                                            HtmlEncode="false" SortExpression="CREATEDDATE" ItemStyle-Width="10%" />
                                                                            <asp:BoundField HeaderText="Department Name" DataField="DepartmentName" SortExpression="DEPARTMENT"
                                                                            ItemStyle-Width="10%" />
                                                                            <asp:BoundField HeaderText="Location" DataField="Street" SortExpression="LOCATION"
                                                                            ItemStyle-Width="10%" />
                                                                            <asp:BoundField HeaderText="Created By" DataField="CreatedBy" SortExpression="CREATEDBY"
                                                                            ItemStyle-Width="10%" />
                                                                            <asp:BoundField HeaderText="Contact Name" DataField="ContactName" SortExpression="CONTACTNAME"
                                                                            ItemStyle-Width="10%" />
                                                                        <asp:BoundField HeaderText="Tags" DataField="TagsWithNoWeightage" SortExpression="TAGS"
                                                                            ItemStyle-Width="12%" />
                                                                            
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <uc1:PageNavigator ID="SearchClientRequest_pageNavigator" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="SearchClientRequest_searchButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_10">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <asp:LinkButton ID="SearchClientRequest_ResetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="SearchClientRequest_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line">
                            |
                        </td>
                        <td>
                            <asp:LinkButton ID="SearchClientRequest_topCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="SearchClientRequest_isMaximizedHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
