﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewAssessorProfile.aspx.cs
// File that represents the user interface layout and functionalities for
// the ViewAssessorProfile page. This will helps to view the assessor 
// profile details.

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ViewAssessorProfile page. This will helps to view the assessor 
    /// profile details. This class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ViewRecruiterProfile : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Recruiter Profile");

                if (!IsPostBack)
                {
                    // Call and assign query string values to hidden fields.
                    if (RetrieveQueryStringValues() == false)
                        return;

                    // Load recruiter details.
                    LoadRecruiterDetail(Convert.ToInt32(ViewRecruiterProfile_recruiterIDHiddenField.Value));

                    // Load assignment details.
                    LoadAssignmentDetails(Convert.ToInt32(ViewRecruiterProfile_recruiterIDHiddenField.Value), 1);
                }

                // Subscribe to paging event.
                ViewRecruiterProfile_assignmentDetailsPageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler(ViewRecruiterProfile_assignmentDetailsPageNavigator_PageNumberClick);

                // Show menu type.
                ShowMenuType();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewRecruiterProfile_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the assessment details grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        private void ViewRecruiterProfile_assignmentDetailsPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                // Load assignment details.
                LoadAssignmentDetails
                    (Convert.ToInt32(ViewRecruiterProfile_recruiterIDHiddenField.Value), 
                    e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewRecruiterProfile_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the data is bound to the grid row.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void ViewRecruiterProfile_assignmentDetailsGridView_OnRowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    #region Assign Position Profile Link Action

                    LinkButton ViewRecruiterProfile_assignmentDetailsGridView_positionProfileLinkButton = (LinkButton)
                        e.Row.FindControl("ViewRecruiterProfile_assignmentDetailsGridView_positionProfileLinkButton");

                    int positionProfileID = 0;
                    HiddenField ViewRecruiterProfile_assignmentDetailsGridView_positionProfileIDHiddenField = (HiddenField)
                        e.Row.FindControl("ViewRecruiterProfile_assignmentDetailsGridView_positionProfileIDHiddenField");

                    if (!Utility.IsNullOrEmpty(ViewRecruiterProfile_assignmentDetailsGridView_positionProfileIDHiddenField.Value))
                        positionProfileID = Convert.ToInt32(ViewRecruiterProfile_assignmentDetailsGridView_positionProfileIDHiddenField.Value);

                    // Add click handler.
                    ViewRecruiterProfile_assignmentDetailsGridView_positionProfileLinkButton.Attributes.Add("onclick",
                        "javascript:return ShowViewPositionProfile('" + positionProfileID + "');");

                    #endregion Assign Position Profile Link Action

                    #region Assign Client Link Action

                    LinkButton ViewRecruiterProfile_assignmentDetailsGridView_clientLinkButton = (LinkButton)
                        e.Row.FindControl("ViewRecruiterProfile_assignmentDetailsGridView_clientLinkButton");

                    int clientID = 0;
                    HiddenField ViewRecruiterProfile_assignmentDetailsGridView_clientIDHiddenField = (HiddenField)
                        e.Row.FindControl("ViewRecruiterProfile_assignmentDetailsGridView_clientIDHiddenField");

                    if (!Utility.IsNullOrEmpty(ViewRecruiterProfile_assignmentDetailsGridView_clientIDHiddenField.Value))
                        clientID = Convert.ToInt32(ViewRecruiterProfile_assignmentDetailsGridView_clientIDHiddenField.Value);

                    // Add click handler.
                    ViewRecruiterProfile_assignmentDetailsGridView_clientLinkButton.Attributes.Add("onclick",
                       "javascript:return ShowViewClient('" + clientID+ "');");

                    #endregion Assign Client Link Action
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ViewRecruiterProfile_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that retrieves the query string values and assign it to 
        /// hidden fields.
        /// </summary>
        private bool RetrieveQueryStringValues()
        {
            #region Retrieve Recruiter ID

            if (Utility.IsNullOrEmpty(Request.QueryString["recruiterid"]))
            {
                base.ShowMessage(ViewRecruiterProfile_topErrorMessageLabel, "Recruiter ID is not passed");
                return false;
            }

            // Get recruiter ID.
            int recruiterID = 0;
            int.TryParse(Request.QueryString["recruiterid"], out recruiterID);
            if (recruiterID == 0)
            {
                base.ShowMessage(ViewRecruiterProfile_topErrorMessageLabel, "Invalid recruiter ID");
                return false;
            }

            // Assign recruiter ID to hidden field.
            ViewRecruiterProfile_recruiterIDHiddenField.Value = recruiterID.ToString();

            #endregion Retrieve Recruiter ID

            #region Retrieve Type

            if (!Utility.IsNullOrEmpty(Request.QueryString["type"]))
            {
                ViewRecruiterProfile_typeHiddenField.Value = Request.QueryString["type"];
            }

            #endregion Retrieve Type

            return true;
        }

        /// <summary>
        /// Method that loads the recruiter details.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the recruiter ID.
        /// </param>
        private void LoadRecruiterDetail(int recruiterID)
        {
            // Add handler to email assessor image button.
            ViewRecruiterProfile_sendEmailLinkButton.Attributes.Add("onclick",
                "return ShowEmailRecruiter('" + recruiterID + "','RR_PRO');");
           
            // Get recruiter detail.
            RecruiterDetail recruiterDetail = new PositionProfileBLManager().
                GetRecruiterByID(recruiterID);

            if (recruiterDetail == null)
            {
                base.ShowMessage(ViewRecruiterProfile_topErrorMessageLabel, "No such recruiter exist");
                return;
            }

            // Assign first name.
            ViewRecruiterProfile_nameValueLabel.Text = recruiterDetail.FirstName;

            // Append last name.
            if (!Utility.IsNullOrEmpty(recruiterDetail.LastName))
            {
                ViewRecruiterProfile_nameValueLabel.Text = ViewRecruiterProfile_nameValueLabel.Text + " " +
                    recruiterDetail.LastName;
            }
            ViewRecruiterProfile_emailValueLabel.Text = recruiterDetail.Email;
            ViewRecruiterProfile_activePositionAssignmentsValueLabel.Text = recruiterDetail.ActivePositionAssignments.ToString();
            ViewRecruiterProfile_recentPositionAssignmentValueLabel.Text = recruiterDetail.RecentPositionAssignmentString;

            // Load recruiter photo
            ViewRecruiterProfile_photoImage.ImageUrl = "~/Common/CandidateImageHandler.ashx?source=VIEW_ASS_PHOTO&userid=" + recruiterID;
        }

        /// <summary>
        /// Method that loads the assignment details.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        private void LoadAssignmentDetails(int recruiterID, int pageNumber)
        {
            int totalRecords = 0;

            string status = null;

            if (!Utility.IsNullOrEmpty(ViewRecruiterProfile_typeHiddenField.Value))
            {
                switch (ViewRecruiterProfile_typeHiddenField.Value.Trim().ToUpper())
                {
                    case "RP":
                        status = null;

                        // Assign values.
                        Master.SetPageCaption("Recruiter Profile");
                        ViewRecruiterProfile_titleLiteral.Text = "Recruiter Profile";
                        ViewRecruiterProfile_assignmentDetailsLinkButton.Text = "Assignments";
                        ViewRecruiterProfile_assignmentDetailsLinkButton.ToolTip = "Assignments";
                        ViewRecruiterProfile_selectedMenuHiddenField.Value = "CI";
                        break;

                    case "WL":
                        status = "PPS_OPEN";

                        // Assign values.
                        Master.SetPageCaption("View Workload");
                        ViewRecruiterProfile_titleLiteral.Text = "View Workload";
                        ViewRecruiterProfile_assignmentDetailsLinkButton.Text = "Active Assignments";
                        ViewRecruiterProfile_assignmentDetailsLinkButton.ToolTip = "Active Assignments";
                        ViewRecruiterProfile_selectedMenuHiddenField.Value = "AD";
                        break;
                }
            }

            // Get recruiter assignment details.
            List<RecruiterAssignmentDetail> assignementDetails = new PositionProfileBLManager().
                GetRecruiterAssignments(recruiterID, status, pageNumber, base.GridPageSize, out totalRecords);

            ViewRecruiterProfile_assignmentDetailsPageNavigator.TotalRecords = totalRecords;
            ViewRecruiterProfile_assignmentDetailsPageNavigator.PageSize = base.GridPageSize;

            ViewRecruiterProfile_assignmentDetailsGridView.DataSource = assignementDetails;
            ViewRecruiterProfile_assignmentDetailsGridView.DataBind();

            // Assign total records to count label.
            ViewRecruiterProfile_assignmentCountLabel.Text = totalRecords.ToString();
            ViewRecruiterProfile_assignmentCountLabel.ToolTip = string.Format("{0} assignment(s)", totalRecords);
        }

        /// <summary>
        /// Method that shows the menu type and respective section based on the 
        /// selection.
        /// </summary>
        private void ShowMenuType()
        { 
            // Get menu type.
            string menuType = ViewRecruiterProfile_selectedMenuHiddenField.Value;

            // Hide all div.
            ViewRecruiterProfile_contactInfoDiv.Attributes["style"] = "display:none";
            ViewRecruiterProfile_assignmentDetailsDiv.Attributes["style"] = "display:none";

            // Reset color.
            ViewRecruiterProfile_contactInfoLinkButton.CssClass = "assessor_profile_link_button";
            ViewRecruiterProfile_assignmentDetailsLinkButton.CssClass = "assessor_profile_link_button";

            if (menuType == "CI")
            {
                ViewRecruiterProfile_contactInfoDiv.Attributes["style"] = "display:block";
                ViewRecruiterProfile_contactInfoLinkButton.CssClass = "assessor_profile_link_button_selected";
            }
            else if (menuType == "AD")
            {
                ViewRecruiterProfile_assignmentDetailsDiv.Attributes["style"] = "display:block";
                ViewRecruiterProfile_assignmentDetailsLinkButton.CssClass = "assessor_profile_link_button_selected";
            }
        }

        #endregion Private Methods
    }
}