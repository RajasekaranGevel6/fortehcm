﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewLogFile.aspx.cs" Inherits="Forte.HCM.UI.Popup.ViewLogFile"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Title="Log File" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ViewLogFile_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">

<script type="text/javascript">

    // Downloads the log file.
    function DownloadLogFile(type, logID, systemName)
    {
        window.open('../Common/Download.aspx?type=' + type 
            + '&logid=' + logID
            + '&systemname=' + systemName, '', 'toolbar=0,resizable=0,width=1,height=1', '');
    }
    </script>

    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="ViewLogFile_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey" align="left">
                            <asp:Literal ID="ViewLogFile_headerLiteral" runat="server" Text="Log File"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="ViewLogFile_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:TextBox runat="server" ID="ViewLogFile_previewTextBox" TextMode="MultiLine"
                                ReadOnly="true" Width="660px" Height="470px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="ViewLogFile_bottomDownloadLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Download" ToolTip="Click here to download the log file" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td>
                            <asp:LinkButton ID="ViewLogFile_bottomCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
