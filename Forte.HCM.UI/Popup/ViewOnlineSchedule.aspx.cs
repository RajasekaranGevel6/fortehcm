﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewOnlineSchedule.aspx.cs
// File that represents the user interface layout and functionalities for
// the ViewOnlineSchedule page. This will helps to view the schedule of a
// candidate against a candidate session. This will also helps a unschedule
// a candidate.

#endregion Header

#region Directives                                                             

using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ViewOnlineSchedule page. This will helps to view the schedule of a
    /// candidate against a candidate session. This will also helps a unschedule
    /// a candidate. This class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ViewOnlineSchedule : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("View Schedule");

                if (!IsPostBack)
                {
                    // Call and assign query string values to hidden fields.
                    if (RetrieveQueryStringValues() == false)
                        return;

                  

                    // Assign message, title and type for unschedule confirm control.
                    ViewOnlineSchedule_unscheduleConfirmMsgControl.Message = "Are you sure you want to cancel the scheduled online interview session? ";
                    ViewOnlineSchedule_unscheduleConfirmMsgControl.Title = "Unschedule ";
                    ViewOnlineSchedule_unscheduleConfirmMsgControl.Type = MessageBoxType.YesNo;

                    // Assign default sort order and field.
                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "ASSESSOR";

                    // Load schedules.
                    LoadSchedules();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewOnlineSchedule_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the schedules grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void ViewOnlineSchedule_schedulesGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    string assessorID = ((HiddenField)e.Row.FindControl
                        ("ViewOnlineSchedule_schedulesGridView_assessorIDHiddenField")).Value;

                    // Add handler for assessor profile icon.
                    ImageButton ViewOnlineSchedule_schedulesGridView_viewAssessorProfileImageButton =
                        (ImageButton)e.Row.FindControl("ViewOnlineSchedule_schedulesGridView_viewAssessorProfileImageButton");
                    ViewOnlineSchedule_schedulesGridView_viewAssessorProfileImageButton.Attributes.Add
                        ("onclick", "javascript:return ShowAssessorProfile('" + assessorID + "');");

                    // Add handler for email assessor icon.
                    ImageButton ViewOnlineSchedule_schedulesGridView_emailAssessorImageButton =
                        (ImageButton)e.Row.FindControl("ViewOnlineSchedule_schedulesGridView_emailAssessorImageButton");
                    ViewOnlineSchedule_schedulesGridView_emailAssessorImageButton.Attributes.Add
                        ("onclick", "javascript:return ShowEmailAssessor('" + assessorID + "','VOS');");

                    // Get status.
                    string status = ((HiddenField)e.Row.FindControl
                        ("ViewOnlineSchedule_schedulesGridView_statusHiddenField")).Value;

                    // Show/hide unschedule icon based on the schedule status.
                    ImageButton ViewOnlineSchedule_schedulesGridView_unscheduleImageButton = 
                        (ImageButton)e.Row.FindControl("ViewOnlineSchedule_schedulesGridView_unscheduleImageButton");

                    // Show the unschedule image button only for the status 'scheduled'
                    if (!Utility.IsNullOrEmpty(status) && status.Trim().ToUpper() ==
                        Constants.CandidateAttemptStatus.SCHEDULED)
                    {
                        ViewOnlineSchedule_schedulesGridView_unscheduleImageButton.Visible = true;
                    }
                    else
                    {
                        ViewOnlineSchedule_schedulesGridView_unscheduleImageButton.Visible = false;
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewOnlineSchedule_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the schedules grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void ViewOnlineSchedule_schedulesGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                //check whether the sort field in view state is same as 
                //the sort expression 
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    //Swap the sort order in the view state
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                //Store the sort field in the view state
                ViewState["SORT_FIELD"] = e.SortExpression;

                // Load schedules.
                LoadSchedules();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewOnlineSchedule_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the schedules grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void ViewOnlineSchedule_schedulesGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (ViewOnlineSchedule_schedulesGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewOnlineSchedule_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void ViewOnlineSchedule_schedulesGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "Unschedule")
                    return;

                // Get row index.
                int index = Convert.ToInt32(e.CommandArgument);

                // Keep the selected details in the hidden fields.
                ViewOnlineSchedule_skillIDHiddenField.Value = ((HiddenField)ViewOnlineSchedule_schedulesGridView.Rows[index].FindControl
                    ("ViewOnlineSchedule_schedulesGridView_skillIDHiddenField")).Value;
                ViewOnlineSchedule_timeSlotIDHiddenField.Value = ((HiddenField)ViewOnlineSchedule_schedulesGridView.Rows[index].FindControl
                    ("ViewOnlineSchedule_schedulesGridView_timeSlotIDHiddenField")).Value;
                ViewOnlineSchedule_assessorIDHiddenField.Value = ((HiddenField)ViewOnlineSchedule_schedulesGridView.Rows[index].FindControl
                    ("ViewOnlineSchedule_schedulesGridView_assessorIDHiddenField")).Value;
                
                // Show the unscheule confirm window.
                ViewOnlineSchedule_unschedulePopupExtenderControl.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewOnlineSchedule_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the unschedule confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void ViewOnlineSchedule_unschedule_OkClick(object sender, EventArgs e)
        {
            try
            {
                // Retrieve the details from the hidden fields.
                int skillID = 0;
                int.TryParse(ViewOnlineSchedule_skillIDHiddenField.Value, out skillID);

                int timeSlotID = 0;
                int.TryParse(ViewOnlineSchedule_timeSlotIDHiddenField.Value, out timeSlotID);

                int assessorID = 0;
                int.TryParse(ViewOnlineSchedule_assessorIDHiddenField.Value, out assessorID);

                // Delete the candidate schedule.
                new AssessorBLManager().DeleteOnlineInterviewCandidateSchedule
                    (ViewOnlineSchedule_candidateSessionKeyHiddenField.Value, 
                    skillID, assessorID, timeSlotID);

                // Reload schedules.
                LoadSchedules();

                // Show success message.
                ViewOnlineSchedule_topSuccessMessageLabel.Text = "Candidate unscheduled successfully";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewOnlineSchedule_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that retrieves the query string values and assign it to 
        /// hidden fields.
        /// </summary>
        private bool RetrieveQueryStringValues()
        {
            // Check if assessor ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["candidatesessionkey"]))
            {
                base.ShowMessage(ViewOnlineSchedule_topErrorMessageLabel, "Candidate session key is not passed");
                return false;
            }

            // Assign candidate session key.
            ViewOnlineSchedule_candidateSessionKeyHiddenField.Value = Request.QueryString
                ["candidatesessionkey"].ToString().Trim();

            return true;
        }

        /// <summary>
        /// Method that loads the schedule for the given candidate session.
        /// </summary>
        private void LoadSchedules()
        {
            int candidateID = 0;
            string candidateName = string.Empty;
            string candidateEmail = string.Empty;

            // Get schedules.
            List<OnlineCandidateSessionDetail> schedules = new AssessorBLManager().
                GetOnlineInterviewCandidateSchedules(
                ViewOnlineSchedule_candidateSessionKeyHiddenField.Value, 
                ViewState["SORT_FIELD"].ToString(),
                (SortType)ViewState["SORT_ORDER"],
                out candidateID,
                out candidateName,
                out candidateEmail);

            // Add handler to email candidate image button.
            ViewOnlineSchedule_emailImageButton.Attributes.Add("onclick",
                "return ShowEmailCandidate('" + candidateID + "');");

            // Assign candidate name and email.
            ViewOnlineSchedule_assessorNameValueLabel.Text = candidateName;
            ViewOnlineSchedule_emailValueLabel.Text = candidateEmail;

            // Assign data source.
            ViewOnlineSchedule_schedulesGridView.DataSource = schedules;
            ViewOnlineSchedule_schedulesGridView.DataBind();
        }

        #endregion Private Methods
    }
}