﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or by any means 
// including electronic or mechanical or otherwise, is prohibited without written 
// permission from Forte.
//
// ViewCertificate.aspx.cs
// File that represents the ViewCertificate class that defines the user interface
// layout and functionalities for the showing the certificate. This page helps to 
// send/download the certificate for the candidates. This class inherits 
// Forte.HCM.UI.Common.PageBase class.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    public partial class ViewCertificate : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler that will call when the page gets loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("View Certificate");
                if (!IsPostBack)
                {
                    Session[Constants.SessionConstants.CERTIFICATE_IMAGE] = null;
                    Session[Constants.SessionConstants.CERTIFICATE_TEMPLATE_IMAGE] = null;
                    Session[Constants.SessionConstants.CERTIFICATE_IMAGE_URL] = null;

                    ViewCertificate_certificatePreviewImage.Visible = false;
                    LoadValues();

                    ViewCertificate_downloadButton.Attributes.Add("onclick", "javascript:OpenPopUp();");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCertificate_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        /// <summary>
        /// Method that will calculate and assign to the expiry date label.
        /// </summary>
        /// <param name="certificateExpiredOn">
        /// A <see cref="string"/> that contains the certificate expired date.
        /// </param>
        /// <param name="testCompletedOn">
        /// A <see cref="DateTime"/> that contains the test completed date.
        /// </param>
        private void ShowCertificateExpiredDate(string certificateExpiredOn, DateTime testCompletedOn)
        {
            switch (certificateExpiredOn)
            {
                case Constants.CertifcateValidityType.CERTIFICATE_VALID_12MONTHS:
                    ViewCertificate_expiryDateLabelValue.Text =
                        GetDateFormat(Convert.ToDateTime(testCompletedOn.AddMonths(12).ToString()));
                    break;
                case Constants.CertifcateValidityType.CERTIFICATE_VALID_18MONTHS:
                    ViewCertificate_expiryDateLabelValue.Text =
                        GetDateFormat(Convert.ToDateTime(testCompletedOn.AddMonths(18).ToString()));
                    break;
                case Constants.CertifcateValidityType.CERTIFICATE_VALID_24MONTHS:
                    ViewCertificate_expiryDateLabelValue.Text =
                        GetDateFormat(Convert.ToDateTime(testCompletedOn.AddMonths(24).ToString()));
                    break;
                case Constants.CertifcateValidityType.CERTIFICATE_VALID_30MONTHS:
                    ViewCertificate_expiryDateLabelValue.Text =
                        GetDateFormat(Convert.ToDateTime(testCompletedOn.AddMonths(30).ToString()));
                    break;
                case Constants.CertifcateValidityType.CERTIFICATE_VALID_6MONTHS:
                    ViewCertificate_expiryDateLabelValue.Text =
                        GetDateFormat(Convert.ToDateTime(testCompletedOn.AddMonths(6).ToString()));
                    break;
                default:
                    ViewCertificate_expiryDateLabelValue.Text = certificateExpiredOn;
                    break;
            }
        }

        /// <summary>
        /// Method that will load completed test certificate.
        /// </summary>
        private void ShowCertificateImage()
        {
            if (Utility.IsNullOrEmpty(Request.QueryString["candidatesession"]))
            {
                base.ShowMessage(ViewCertificate_topErrorMessageLabel, "Candidate session ID is not passed");
                return;
            }

            if (Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
            {
                base.ShowMessage(ViewCertificate_topErrorMessageLabel, "Attempt ID is not passed");
                return;
            }
              
            string candidateSessionKey = Request.QueryString["candidatesession"].Trim();
            int attemptID = Convert.ToInt32(Request.QueryString["attemptid"]);

            // Get certificate expired date, HTML text values are retrieved by passing
            // test key.
            CertificationDetail certificationDetail =
                new AdminBLManager().GetCertificateByCandidateSession(candidateSessionKey, attemptID);

            if (certificationDetail == null)
            {
                base.ShowMessage(ViewCertificate_topErrorMessageLabel, "No completed certificate found to display");
                return;
            }
                
            Session[Constants.SessionConstants.CERTIFICATE_IMAGE] =
                new AdminBLManager().GetCertificateImage(candidateSessionKey, attemptID);

            // Show certificate expired date
            ShowCertificateExpiredDate(certificationDetail.CertificateExpiredOn, 
                certificationDetail.CompletedDate);

            // Show the completed test certificate image.
            ViewCertificate_certificatePreviewImage.ImageUrl
                = @"~/Common/ImageHandler.ashx?ImageID="
                + certificationDetail.CertificateID + "&isThumb=0&source=CERTIFICATE_IMAGE"
                + "&candidatesessionid=" + candidateSessionKey + "&attemptid=" + attemptID;

            // Show certification image.
            ViewCertificate_certificatePreviewImage.Visible = true;

            Session[Constants.SessionConstants.CERTIFICATE_IMAGE_URL] =
                ViewCertificate_certificatePreviewImage.ImageUrl;

            ((Button)ViewCertificate_emailConfirmation_ConfirmMsgControl.FindControl
                ("ConfirmMsgControl_emailAttachmentButton")).Attributes.Add("onclick",
                "javascript:return OpenEmailAttachmentAndCloseConfirmPopUp('CERTIFICATE','N','"
                + ViewCertificate_emailConfirmation_ModalPopupExtender.ID + "','"
                + candidateSessionKey + "','" + attemptID + "');");

            // Body content button
            ((Button)ViewCertificate_emailConfirmation_ConfirmMsgControl.FindControl
                ("ConfirmMsgControl_emailContentButton")).Attributes.Add("onclick",
                "javascript:return OpenEmailAttachmentAndCloseConfirmPopUp('CONTENT','N','"
                + ViewCertificate_emailConfirmation_ModalPopupExtender.ID + "','"
                + candidateSessionKey + "','" + attemptID + "');");
        }

        /// <summary>
        /// Method that will show the certificate template before complete the test.
        /// </summary>
        private void ShowCertificateTemplate()
        {
            string testKey = Request.QueryString["testkey"].Trim();
            string candidateSessionKey = string.Empty;
            int attemptID = 0;

            // Get the HTML data based on the test key.
            CertificationDetail certificationDetail =
                new AdminBLManager().GetTestCertificationDetails(testKey);

            if (certificationDetail == null)
                return;

            // Show the certificate template image.
            ViewCertificate_certificatePreviewImage.ImageUrl
                = @"~/Common/ImageHandler.ashx?ImageID="
                + certificationDetail.CertificateID + "&isThumb=0&source=CERTIFICATE_TEMPLATE_IMAGE"
                + "&testkey=" + testKey;
            ViewCertificate_certificatePreviewImage.Visible = true;

            Session[Constants.SessionConstants.CERTIFICATE_TEMPLATE_IMAGE] = certificationDetail.ImageData;
            Session[Constants.SessionConstants.CERTIFICATE_IMAGE_URL] = ViewCertificate_certificatePreviewImage.ImageUrl;
            
            ((Button)ViewCertificate_emailConfirmation_ConfirmMsgControl.FindControl
              ("ConfirmMsgControl_emailAttachmentButton")).Attributes.Add("onclick",
              "javascript:return OpenEmailAttachmentAndCloseConfirmPopUp('CERTIFICATE_TEMPLATE','N','"
              + ViewCertificate_emailConfirmation_ModalPopupExtender.ID + "','"
              + candidateSessionKey + "','" + attemptID + "');");

            ((Button)ViewCertificate_emailConfirmation_ConfirmMsgControl.FindControl
                    ("ConfirmMsgControl_emailContentButton")).Attributes.Add("onclick",
                    "javascript:return OpenEmailAttachmentAndCloseConfirmPopUp('CONTENT','N','"
                    + ViewCertificate_emailConfirmation_ModalPopupExtender.ID + "','dummy','0');");

            ViewCertificate_expiryDateHeadLabel.Text = "";
        }

        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            if (!Utility.IsNullOrEmpty(Request.QueryString["certificatetype"]) &&
                Request.QueryString["certificatetype"].ToUpper() == "CERTIFICATE")
            {
                EmailAttachment_downloadTypeHiddenField.Value = "CERTIFICATE";
                ShowCertificateImage();
            }
            else
            {
                EmailAttachment_downloadTypeHiddenField.Value = "CERTIFICATE_TEMPLATE";
                ShowCertificateTemplate();
            }
        }

        #endregion Protected Overridden Methods                                
    }
}
