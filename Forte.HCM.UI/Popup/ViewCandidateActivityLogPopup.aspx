﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="ViewCandidateActivityLogPopup.aspx.cs" Inherits="Forte.HCM.UI.Popup.ViewCandidateActivityLogPopup" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/CandidateActivityViewerControl.ascx" TagName="CandidateActivityViewerControl" TagPrefix="uc1" %>   
<asp:Content ID="ViewCandidateActivityLogPopup_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
<script type="text/javascript" language="javascript">

     // Method that opens the add notes popup.
     // candidateID - Refers to the candidate ID (CandidateInformation table).
     function OpenAddNotesPopupWithRefreshForViewCandidateActivityLogPopup(candidateID)
     {
         var height = 260;
         var width = 620;
         var top = (screen.availHeight - parseInt(height)) / 2;
         var left = (screen.availWidth - parseInt(width)) / 2;
         var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:"
                + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

         var queryStringValue = "../Popup/AddNotes.aspx" +
            "?candidateid=" + candidateID;

         window.open(queryStringValue, window.self, sModalFeature);

         document.getElementById('<%=ViewCandidateActivityLogPopup_refreshHiddenField.ClientID %>').value = 'Y';

         __doPostBack('<%=ViewCandidateActivityLogPopup_refreshLinkButton.ClientID %>', "OnClick");
     }

</script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="ViewCandidateActivityLogPopup_headerLiteral" runat="server" Text="Candidate Activity Log"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="ViewCandidateActivityLogPopup_topCancelImageButton" runat="server"
                                SkinID="sknCloseImageButton" OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="ViewCandidateActivityLogPopup_successMessageLabel" runat="server"
                    SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="ViewCandidateActivityLogPopup_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <uc1:CandidateActivityViewerControl ID="ViewCandidateActivityLogPopup_candidateActivityViewerControl"
                    runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_left_8">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="ViewCandidateActivityLogPopup_addNotesLinkButton" runat="server" SkinID="sknPopupLinkButton" Text="Add Notes"
                                OnClick="ViewCandidateActivityLogPopup_addNotesLinkButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="ViewCandidateActivityLogPopup_downloadLinkButton" runat="server" SkinID="sknPopupLinkButton" Text="Download"
                                OnClick="ViewCandidateActivityLogPopup_downloadLinkButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="ViewCandidateActivityLogPopup_refreshLinkButton" runat="server" SkinID="sknPopupLinkButton" Text="Refresh"
                                OnClick="ViewCandidateActivityLogPopup_refreshLinkButton_Click" />

                                <asp:HiddenField ID="ViewCandidateActivityLogPopup_refreshHiddenField" runat="server" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="ViewCandidateActivityLogPopup_resetLinkButton" runat="server"
                                SkinID="sknPopupLinkButton" Text="Reset" OnClick="ViewCandidateActivityLogPopup_resetLinkButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="ViewCandidateActivityLogPopup_cancelLinkButton" runat="server"
                                Text="Cancel" SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="ViewCandidateActivityLogPopup_isMaximizedHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
    