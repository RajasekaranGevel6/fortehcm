
#region Header

// Copyright (C) Forte. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchTestSession.aspx.cs
// This page allows the user to search the existing testSessions 
// and to view all its informations.
#endregion Header

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// This page allows the user to search the existing testSessions 
    /// and to view all its informations.
    /// </summary>
    public partial class SearchOnlineInterviewSession : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "224px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "374px";

        #endregion Private Constants

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set Browser Title
                Master.SetPageCaption("Search Online Interview Session");
                // Set default button
                Page.Form.DefaultButton = SearchOnlineInterviewSession_topSearchButton.UniqueID;

                // Create events for paging control
                SearchOnlineInterviewSession_bottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchOnlineInterviewSession_pagingNavigator_PageNumberClick);

                // Check whether the grid is maximized or not and assign the height accordingly.
                if (!Utility.IsNullOrEmpty(ScheduleCandidate_isMaximizedHiddenField.Value) &&
                        ScheduleCandidate_isMaximizedHiddenField.Value == "Y")
                {
                    SearchOnlineInterviewSession_searchCriteriasDiv.Style["display"] = "none";
                    SearchOnlineInterviewSession_searchResultsUpSpan.Style["display"] = "block";
                    SearchOnlineInterviewSession_searchResultsDownSpan.Style["display"] = "none";
                    SearchOnlineInterviewSession_testDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchOnlineInterviewSession_searchCriteriasDiv.Style["display"] = "block";
                    SearchOnlineInterviewSession_searchResultsUpSpan.Style["display"] = "none";
                    SearchOnlineInterviewSession_searchResultsDownSpan.Style["display"] = "block";
                    SearchOnlineInterviewSession_testDiv.Style["height"] = RESTORED_HEIGHT;
                }

                // Call expand or restore method
                SearchOnlineInterviewSession_searchTestResultsTR.Attributes.Add("onclick",
                        "ExpandOrRestore('" +
                        SearchOnlineInterviewSession_testDiv.ClientID + "','" +
                        SearchOnlineInterviewSession_searchCriteriasDiv.ClientID + "','" +
                        SearchOnlineInterviewSession_searchResultsUpSpan.ClientID + "','" +
                        SearchOnlineInterviewSession_searchResultsDownSpan.ClientID + "','" +
                        ScheduleCandidate_isMaximizedHiddenField.ClientID + "','" +
                        RESTORED_HEIGHT + "','" +
                        EXPANDED_HEIGHT + "')");

                if (!IsPostBack)
                {
                    // Set default focus
                    Page.Form.DefaultFocus = SearchOnlineInterviewSession_sessionIdTextBox.UniqueID;
                    SearchOnlineInterviewSession_sessionIdTextBox.Focus();

                    SearchOnlineInterviewSession_testDiv.Visible = false;

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "TestSessionID";

                    SetAuthorDetails();
                }
                else
                {
                    SearchOnlineInterviewSession_testDiv.Visible = true;
                }

                // Assign handler to scheduler selection popup.
                SearchOnlineInterviewSession_schedulerNameImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                    + SearchOnlineInterviewSession_schedulerHiddenField.ClientID + "','"
                    + SearchOnlineInterviewSession_schedulerIDHiddenField.ClientID + "','"
                    + SearchOnlineInterviewSession_schedulerNameTextBox.ClientID + "','TC')");

                // Assign handler to session author selection popup.
                SearchOnlineInterviewSession_sessionAuthorNameImageButton.Attributes.Add("onclick",
                    "return LoadAdminName('" +SearchOnlineInterviewSession_sessionAuthorNameHiddenField.ClientID + "','" +
                    SearchOnlineInterviewSession_sessionAuthorIDHiddenField.ClientID + "','" +
                    SearchOnlineInterviewSession_sessionAuthorNameTextBox.ClientID + "','TS')");

                // Assign handler to position profile selection popup.
                SearchOnlineInterviewSession_positionProfileImageButton.Attributes.Add("onclick",
                    "return LoadPositionProfileName('" + SearchOnlineInterviewSession_positionProfileTextBox.ClientID + "','"
                    + SearchOnlineInterviewSession_positionProfileIDHiddenField.ClientID + "')");

                // Clear message.
                SearchOnlineInterviewSession_topErrorMessageLabel.Text =
                    SearchOnlineInterviewSession_topSuccessMessageLabel.Text = "";

                // Assign position profile.
                if (!IsPostBack)
                {
                    AssignPositionProfile();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchOnlineInterviewSession_topErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// This button handler will get trigger on clicking the search button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchOnlineInterviewSession_SearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear error message label
                SearchOnlineInterviewSession_topErrorMessageLabel.Text = string.Empty;
                SearchOnlineInterviewSession_positionProfileTextBox.Text =
                    Request[SearchOnlineInterviewSession_positionProfileTextBox.UniqueID].ToString();

                // Reset the paging control.
                SearchOnlineInterviewSession_bottomPagingNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1
                SearchOnlineInterviewSessionDetail(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchOnlineInterviewSession_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the PageNumberClick event of the SearchOnlineInterviewSession_pagingNavigator control.
        /// This binds the data into the grid for the pagenumber.
        /// </summary>
        /// <param name="sender">Source of the event.</param>
        /// <param name="e"></param>
        protected void SearchOnlineInterviewSession_pagingNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                SearchOnlineInterviewSessionDetail(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchOnlineInterviewSession_topErrorMessageLabel,
                    exception.Message);
                SearchOnlineInterviewSession_topErrorMessageLabel.Text = exception.Message;
            }
        }

        /// <summary>
        /// Clicking on Reset button will trigger this method. 
        /// This clears all the informations and reset to empty.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchOnlineInterviewSession_bottomReset_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }
        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method that assigns the position profile and loads the default 
        /// search results in the grid.
        /// </summary>
        private void AssignPositionProfile()
        {
            // Load the position profile name in the search criteria,
            // if position profile is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                return;

            // Get position profile ID.
            int positionProfileID = 0;
            if (int.TryParse(Request.QueryString["positionprofileid"].Trim(),
                out positionProfileID) == false)
            {
                return;
            }
            PositionProfileDetail positionProfile =
                new PositionProfileBLManager().GetPositionProfileInformation(positionProfileID);

            // Assign the position profile ID and name to the corresponding fields.
            SearchOnlineInterviewSession_positionProfileTextBox.Text = positionProfile.PositionProfileName;
            SearchOnlineInterviewSession_positionProfileIDHiddenField.Value = positionProfileID.ToString();

            // Apply the default search.
            // Clear error message label
            SearchOnlineInterviewSession_topErrorMessageLabel.Text = string.Empty;

            if (Request[SearchOnlineInterviewSession_positionProfileTextBox.UniqueID] != null)
            {
                SearchOnlineInterviewSession_positionProfileTextBox.Text =
                    Request[SearchOnlineInterviewSession_positionProfileTextBox.UniqueID].ToString();
            }

            // Reset the paging control.
            SearchOnlineInterviewSession_bottomPagingNavigator.Reset();

            // By default search button click retrieves data for
            // page number 1
            SearchOnlineInterviewSessionDetail(1);

            // Update the search results update panel.
           // SearchCategory_searchResultsUpdatePanel.Update();
            //SearchOnlineInterviewSession_updatePanel.Update();
        }

        /// <summary>
        /// This method creates an instance for search criteria and 
        /// passes to db to get the matched test sessions
        /// </summary>
        /// <param name="pageNumber"></param>
        private void SearchOnlineInterviewSessionDetail(int pageNumber)
        {
            OnlineInterviewSessionSearchCriteria onlineInterviewSearchCriteria = new OnlineInterviewSessionSearchCriteria();
            onlineInterviewSearchCriteria.SessionName = SearchOnlineInterviewSession_sessionNameTextBox.Text.Trim();
            onlineInterviewSearchCriteria.SessionKey = SearchOnlineInterviewSession_sessionIdTextBox.Text.Trim();

            // Since the session author textbox is readonly, get the text value using Request.
            if (Request[SearchOnlineInterviewSession_sessionAuthorNameTextBox.UniqueID] != null)
            {
                SearchOnlineInterviewSession_sessionAuthorNameTextBox.Text =
                  Request[SearchOnlineInterviewSession_sessionAuthorNameTextBox.UniqueID].Trim();
            }
            //onlinerInterviewSearchCriteria.TestSessionCreator = SearchOnlineInterviewSession_sessionAuthorNameTextBox.Text.Trim();

            onlineInterviewSearchCriteria.PositionProfileID =
                (SearchOnlineInterviewSession_positionProfileIDHiddenField.Value == null || SearchOnlineInterviewSession_positionProfileIDHiddenField.Value.Trim().Length == 0) ? 0 :
                Convert.ToInt32(SearchOnlineInterviewSession_positionProfileIDHiddenField.Value.Trim());

            // Since the SchedulerName textbox is readonly, get the text value using Request.
            if (Request[SearchOnlineInterviewSession_schedulerNameTextBox.UniqueID] != null)
            {
                SearchOnlineInterviewSession_schedulerNameTextBox.Text =
                    Request[SearchOnlineInterviewSession_schedulerNameTextBox.UniqueID].Trim();
            }
            onlineInterviewSearchCriteria.SchedulerName = SearchOnlineInterviewSession_schedulerNameTextBox.Text;
            onlineInterviewSearchCriteria.SchedulerNameID =int.Parse(SearchOnlineInterviewSession_schedulerIDHiddenField.Value);
            onlineInterviewSearchCriteria.SessionCreatorID = int.Parse(SearchOnlineInterviewSession_sessionAuthorIDHiddenField.Value);
            onlineInterviewSearchCriteria.Skill = SearchOnlineInterviewSession_sessionSkillTextBox.Text;
            int totalRecords = 0;

            List<OnlineInterviewSessionDetail> onlineInterviewSession =
                new OnlineInterviewBLManager().SearchOnlineInterviewSessionDetails
                (onlineInterviewSearchCriteria, pageNumber, base.GridPageSize,
                out totalRecords, ViewState["SORT_FIELD"].ToString(), ((SortType)ViewState["SORT_ORDER"]));

            // Bind the retrieved test sessions in grid.
            SearchOnlineInterviewSession_testGridView.DataSource = onlineInterviewSession;
            SearchOnlineInterviewSession_testGridView.DataBind();
            if (onlineInterviewSession == null)
            {
                SearchOnlineInterviewSession_testDiv.Visible = false;
                base.ShowMessage(SearchOnlineInterviewSession_topErrorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                SearchOnlineInterviewSession_testDiv.Visible = true;
            }

            SearchOnlineInterviewSession_bottomPagingNavigator.PageSize = base.GridPageSize;
            SearchOnlineInterviewSession_bottomPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method used to set the author name and first name for the first time
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user id 
        /// </param>
        private void SetAuthorDetails()
        {
            //Get the user details from DB and assign it
            UserDetail userDetail = new QuestionBLManager().GetAuthorIDAndName(base.userID);

            if (userDetail == null)
                return;

            SearchOnlineInterviewSession_schedulerIDHiddenField.Value ="0";
            SearchOnlineInterviewSession_sessionAuthorIDHiddenField.Value = base.userID.ToString();
            SearchOnlineInterviewSession_sessionAuthorNameTextBox.Text = userDetail.FirstName;

            //if (base.isAdmin)
            //{
            //    SearchOnlineInterviewSession_sessionAuthorNameImageButton.Visible = true;
            //    SearchOnlineInterviewSession_sessionAuthorNameTextBox.Text = string.Empty;
            //    SearchOnlineInterviewSession_schedulerIDHiddenField.Value = "0";
            //    SearchOnlineInterviewSession_sessionAuthorIDHiddenField.Value = "0";
            //}
        }


        #endregion Private Methods

        #region Sort Columns Related Code

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchOnlineInterviewSession_testGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                SearchOnlineInterviewSession_bottomPagingNavigator.Reset();
                SearchOnlineInterviewSessionDetail(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchOnlineInterviewSession_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchOnlineInterviewSession_testGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchOnlineInterviewSession_testGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchOnlineInterviewSession_topErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchOnlineInterviewSession_testGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchOnlineInterviewSession_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Sort Columns Related Code

        #region Override Methods

        protected override bool IsValidData()
        {
            return true;
        }
        protected override void LoadValues()
        {
            //Do Nothing
        }
        #endregion
    }
}
