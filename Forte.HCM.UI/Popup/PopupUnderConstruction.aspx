﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="PopupUnderConstruction.aspx.cs" Title="Popup UnderConstruction" Inherits="Forte.HCM.UI.Popup.PopupUnderConstruction" %>

<asp:Content ID="PopupUnderConstruction_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
 <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td style="width: 40%" align="left" valign="top">
                <table style="width: 100%">
                    <tr>
                        <td class="unhandled_error_title">
                            Error
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message">
                            Page under construction
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_title">
                            Message
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message">
                            The requested page cannot be accessed at this time
                        </td>
                    </tr>
                     <tr>
                        <td class="unhandled_error_message">
                            The page might under be construction
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message">
                            Please contact your administrator
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 60%" class="under_construction">
            </td>
        </tr>
    </table>   
</asp:Content>