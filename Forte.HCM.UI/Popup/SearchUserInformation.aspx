<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="SearchUserInformation.aspx.cs"  Inherits="Forte.HCM.UI.Popup.SearchUserInformation" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ID="SearchUserInformation_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl) {

            var ctrlDisplayField = '<%= Request.QueryString["ctrlId"] %>';
            var ctrlHiddenField = '<%= Request.QueryString["ctrlhidId"] %>';
            var ctrlFirstNameField = '<%= Request.QueryString["ctrlFirstNameid"] %>';
            var ctrlEmailField = '<%= Request.QueryString["ctrlEmail"] %>';
            var roleLabel = '<%= Request.QueryString["roleLabel"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Set the display field value. Replace the 'link button name' with the 
            // 'user name hidden field name' and retrieve the value.
            if (ctrlDisplayField != null && ctrlDisplayField != '') {
                window.opener.document.getElementById(ctrlDisplayField).value
                    = document.getElementById(ctrl.id.replace("SearchUserInformation_selectLinkButton", "SearchUserInformation_userNameHiddenfield")).value;
            }

            // Set the ID hidden field value. Replace the 'link button name' with the 
            // 'user ID hidden field name' and retrieve the value.
            if (ctrlHiddenField != null && ctrlHiddenField != '') {
                window.opener.document.getElementById(ctrlHiddenField).value
                    = document.getElementById(ctrl.id.replace("SearchUserInformation_selectLinkButton", "SearchUserInformation_userIDHiddenfield")).value;
            }

            if (ctrlFirstNameField != null && ctrlFirstNameField != '') {
                var value = window.opener.document.getElementById(ctrlFirstNameField);
                if (value.type == "text") {
                    window.opener.document.getElementById(ctrlFirstNameField).value =
                    document.getElementById(ctrl.id.replace("SearchUserInformation_selectLinkButton", "SearchUserInformation_userFirstNameHiddenfield")).value;
                }
                else {
                    window.opener.document.getElementById(ctrlFirstNameField).innerHTML =
                    document.getElementById(ctrl.id.replace("SearchUserInformation_selectLinkButton", "SearchUserInformation_userFirstNameHiddenfield")).value;
                }
                window.opener.document.getElementById(ctrlFirstNameField.replace("UserRoleMatrix_userIDTextBox", "UserRoleMatrix_showButton")).click();
            }

            // Set the ID hidden field value. Replace the 'link button name' with the 
            // 'user ID hidden field name' and retrieve the value.
            if (ctrlEmailField != null && ctrlEmailField != '') {
                window.opener.document.getElementById(ctrlEmailField).value
                    = document.getElementById(ctrl.id.replace("SearchUserInformation_selectLinkButton", "SearchUserInformation_userEmailHiddenfield")).value;
            }

            if (roleLabel != null && roleLabel != '') {
                window.opener.document.getElementById(roleLabel.replace("UserRightsMatrix_individualRoleNameLabel", "UserRightsMatrix_hiddenButton")).click();

            }
            self.close();
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchUserInformation_headerLiteral" runat="server" Text="Search User"></asp:Literal>
                            <asp:HiddenField ID="SearchUserInformation_browserHiddenField" runat="server" />
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SearchUserInformation_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:UpdatePanel ID="SearchUserInformation_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="SearchUserInformation_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="SearchUserInformation_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchUserInformation_searchButton" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchUserInformation_pageNavigator" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="SearchUserInformation_searchCriteriasDiv" runat="server" style="display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="SearchUserInformation_searchCriteriaUpdatePanel" runat="server"
                                                            UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <table width="100%" border="0" cellspacing="3%" cellpadding="2%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="SearchUserInformation_userNameLabel" runat="server" Text="User ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchUserInformation_userNameTextBox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="SearchUserInformation_userTypeLabel" runat="server" Text="User Type"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <%--<asp:TextBox ID="SearchUserInformation_firstNameTextBox" runat="server" Visible="false"></asp:TextBox>--%>
                                                                            <asp:DropDownList ID="SearchUserInformation_userTypeDropDownList" runat="server"
                                                                                Width="128px">
                                                                                <asp:ListItem Value="A">All</asp:ListItem>
                                                                                <asp:ListItem Value="I">Internal</asp:ListItem>
                                                                                <asp:ListItem Value="S">Subscription</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="SearchUserInformation_firstNameLabel" runat="server" Text="First Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchUserInformation_firstNameTextBox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="SearchUserInformation_lastNameLabel" runat="server" Text="Last Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchUserInformation_lastNameTextBox" runat="server"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="4">
                                                                            <asp:Button ID="SearchUserInformation_searchButton" runat="server" SkinID="sknButtonId"
                                                                                Text="Search" OnClick="SearchUserInformation_searchButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="SearchUserInformation_searchResultsUpdatePanel" runat="server"
                                            UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr id="SearchUserInformation_searchTestResultsTR" runat="server">
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 50%">
                                                                        <asp:Literal ID="SearchUserInformation_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                            ID="SearchQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 50%" align="right">
                                                                        <span id="SearchUserInformation_searchResultsUpSpan" style="display: none;" runat="server">
                                                                            <asp:Image ID="SearchUserInformation_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                        </span><span id="SearchUserInformation_searchResultsDownSpan" style="display: block;"
                                                                            runat="server">
                                                                            <asp:Image ID="SearchUserInformation_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="height: 220px; overflow: auto;" runat="server" id="SearchUserInformation_testDiv">
                                                                            <asp:GridView ID="SearchUserInformation_testGridView" runat="server" AllowSorting="True"
                                                                                AutoGenerateColumns="False" GridLines="Horizontal" BorderColor="white" BorderWidth="1px"
                                                                                Width="100%" OnSorting="SearchUserInformation_testGridView_Sorting" OnRowDataBound="SearchUserInformation_testGridView_RowDataBound"
                                                                                OnRowCreated="SearchUserInformation_testGridView_RowCreated">
                                                                                <RowStyle CssClass="grid_alternate_row" />
                                                                                <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                                <HeaderStyle CssClass="grid_header_row" />
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="SearchUserInformation_selectLinkButton" runat="server" Text="Select"
                                                                                                OnClientClick="javascript:return OnSelectClick(this);" ToolTip="Select"></asp:LinkButton>
                                                                                            <asp:HiddenField ID="SearchUserInformation_userNameHiddenfield" runat="server" Value='<%# Eval("UserName") %>' />
                                                                                            <asp:HiddenField ID="SearchUserInformation_userIDHiddenfield" runat="server" Value='<%# Eval("UserID") %>' />
                                                                                            <asp:HiddenField ID="SearchUserInformation_userFirstNameHiddenfield" runat="server"
                                                                                                Value='<%# Eval("FIRSTNAME") %>' />
                                                                                            <asp:HiddenField ID="SearchUserInformation_userEmailHiddenfield" runat="server" Value='<%# Eval("Email") %>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="UserID" Visible="false" />
                                                                                    <asp:BoundField HeaderText="User ID" DataField="UserName" SortExpression="USERNAME" />
                                                                                    <asp:BoundField HeaderText="First Name" DataField="FirstName" SortExpression="FIRSTNAME" />
                                                                                    <%-- <asp:BoundField HeaderText="Middle Name" DataField="MiddleName" SortExpression="MIDDLENAME" />--%>
                                                                                    <asp:BoundField HeaderText="Last Name" DataField="LastName" SortExpression="LASTNAME" />
                                                                                    <asp:BoundField HeaderText="User Type" DataField="UserTypeName" SortExpression="USERTYPENAME" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <uc1:PageNavigator ID="SearchUserInformation_pageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchUserInformation_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_5">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="SearchUserInformation_topResetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="SearchUserInformation_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="SearchUserInformation_topCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="SearchUserInformation_isMaximizedHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
