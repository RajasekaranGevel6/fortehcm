﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// FeatureUsageDetails.cs
// File that reprevasents the user interface for showing feature usage dteails.
// This will interact with the SubscriptionFeaturesBLManager.cs to get feature 
// usage details.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface for showing
    /// feature usage details. This class is inherited
    /// from PageBase.
    /// </summary>
    public partial class FeatureUsageDetails : PageBase
    {
        #region Private variable                                               
        private int user = 0;
        private int featureID = 0;
        private int monthYear = 0;
        #endregion Private variable

        #region Event Handler                                                  

        /// <summary>
        /// Event handler will get fired when a page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> object holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Usage Details");
                // Subscribes to the page number click event.
                user = Convert.ToInt32(Request.QueryString["userid"].ToString());
                featureID = Convert.ToInt32(Request.QueryString["featureid"].ToString());
                if (Request.QueryString["mmmyyyid"] != null && Request.QueryString["mmmyyyid"].Trim().Length > 0)
                    monthYear = Convert.ToInt32(Request.QueryString["mmmyyyid"].ToString());
               
                if (!IsPostBack)
                {
                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "NAME";

                    if (monthYear != 0)
                        LoadValues();
                    else
                        ShowMessage(FeatureUsageDetails_errorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(FeatureUsageDetails_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        private void FeatureUsageDetails_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(FeatureUsageDetails_errorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the sorting action takes
        /// place
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FeatureUsageDetails_userFeaturesGridView_Sorting
           (object sender, GridViewSortEventArgs e)
        {
            try
            {


                //Assign the sorting and sort order
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                LoadValues();

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureUsageDetails_errorMessageLabel, exception.Message);
            }
        }
     
        /// <summary>
        /// Handler method that will be called when the sorting action takes
        /// place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FeatureUsageDetails_userFeaturestalentScoutViewProfileImageGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex
                    (FeatureUsage_talentScoutViewProfileImageGridView,
                    (string)ViewState["SORT_FIELD"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureUsageDetails_errorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting action takes
        /// place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FeatureUsageDetails_userFeaturestalentScoutGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex
                    (FeatureUsageDetails_userFeaturesGridView,
                    (string)ViewState["SORT_FIELD"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureUsageDetails_errorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting action takes
        /// place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FeatureUsageDetails_userFeaturesGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex
                    (FeatureUsageDetails_userFeaturesGridView,
                    (string)ViewState["SORT_FIELD"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureUsageDetails_errorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting action takes
        /// place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FeatureUsage_formsUpdatePanelGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex
                    (FeatureUsage_formsUpdatePanelGridView,
                    (string)ViewState["SORT_FIELD"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureUsageDetails_errorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting action takes
        /// place.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FeatureUsage_DetailsTalentScout_cloning_Gridview_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex
                    (FeatureUsage_DetailsTalentScout_cloning_Gridview,
                    (string)ViewState["SORT_FIELD"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(FeatureUsageDetails_errorMessageLabel, exception.Message);
            }
        }

        #endregion Event Handler                                               

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            List<UsageSummary> usageDetails = null;

            string sortOrder = ViewState["SORT_ORDER"].ToString();

            string sortExpression = ViewState["SORT_FIELD"].ToString();

            switch (featureID)
            {
                case Constants.FeatureConstants.NUMBER_OF_POSITION_PROFILES:
                    usageDetails = new AdminBLManager().
                        GetPositionProfileUsageDetails(sortOrder, sortExpression,
                            user, monthYear);

                    FeatureUsage_DetailsDIV.Visible = true;
                    FeatureUsageDetails_userFeaturesGridView.DataSource = usageDetails;
                    FeatureUsageDetails_userFeaturesGridView.DataBind();
                    break;
                case Constants.FeatureConstants.NUMBER_OF_CANDIDATE_SEARCHES:
                    usageDetails = new AdminBLManager().GetTalentSearchUsageDetails
                        (sortOrder, sortExpression,
                      user, monthYear);

                    FeatureUsage_DetailsTalentScoutDIV.Visible = true;
                    FeatureUsage_talentScoutGridView.DataSource = usageDetails;
                    FeatureUsage_talentScoutGridView.DataBind();
                    break;

                case 3:
                    {


                    }
                    break;
                case 4:
                    break;
                case Constants.FeatureConstants.STANDARD_FORMS_FOR_POSITION_PROFILE_GENERATION:
                    usageDetails = new AdminBLManager().GetFormUsageDetails(sortOrder, sortExpression,
                            user, monthYear);

                    FeatureUsage_DetailsFormsDIV.Visible = true;
                    FeatureUsage_formsUpdatePanelGridView.DataSource = usageDetails;
                    FeatureUsage_formsUpdatePanelGridView.DataBind();
                    break;
                case 6:

                    break;
                case 7:

                    break;
                case 8:

                    break;
                case 9:

                    break;
                case Constants.FeatureConstants.VIEW_CANDIDATE_PROFILE_IMAGES:
                    usageDetails = new AdminBLManager().GetViewProfileImageUsageDetails(sortOrder, sortExpression,
                                user, monthYear);

                    FeatureUsage_DetailsTalentScoutViewProfileImageDIV.Visible = true;
                    FeatureUsage_talentScoutViewProfileImageGridView.DataSource = usageDetails;
                    FeatureUsage_talentScoutViewProfileImageGridView.DataBind();
                    break;
                case 11:
                    break;
                case Constants.FeatureConstants.NUMBER_OF_TIMES_CLONING:
                    usageDetails = new AdminBLManager().GetViewCloningUsageDetails(sortOrder, sortExpression,
                                user, monthYear);
                    FeatureUsage_DetailsTalentscout_cloning_DIV.Visible = true;
                    FeatureUsage_DetailsTalentScout_cloning_Gridview.DataSource = usageDetails;
                    FeatureUsage_DetailsTalentScout_cloning_Gridview.DataBind();
                    break;


                default:
                    break;
            }

            if (usageDetails == null || usageDetails.Count == 0)
            {
                ShowMessage(FeatureUsageDetails_errorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
            }
        }
        #endregion Protected Overridden Methods                                
    }
}
