<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewSkillScore.aspx.cs" MasterPageFile="~/MasterPages/PopupMaster.Master"
    Inherits="Forte.HCM.UI.Popup.ViewSkillScore"  %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ViewSkillScore_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="ViewSkillScore_headerLiteral" runat="server" Text="Position Profile Candidate Skill Score"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="ViewSkillScore_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" align="left">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:Label ID="ViewSkillScore_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                        <asp:Label ID="ViewSkillScore_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:Label ID="ViewSkillScore_positionProfileNameLabel" runat="server" Text="Position Profile Name"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 85%">
                                                    <asp:Label ID="ViewSkillScore_positionProfileNameValueLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:Label ID="ViewSkillScore_candidateNameLabel" runat="server" Text="Candidate Name"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 85%">
                                                    <asp:Label ID="ViewSkillScore_candidateNameValueLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="header_bg">
                                        <asp:Literal ID="ViewSkillScore_skillScoreHeaderLabel" runat="server" Text="Skill Score"
                                            SkinID="sknLabelText"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <div style="overflow: auto; height: 264px; width: 100%">
                                            <div id="ViewSkillScore_noSkillMessageDiv" style="width: 98%; height: 98%" runat="server"
                                                visible="false">
                                                <table style="width: 98%; height: 98%">
                                                    <tr>
                                                        <td style="width: 100%; height: 100%" valign="middle" align="center">
                                                            <asp:Label ID="ViewSkillScore_noSkillMessageLabel" runat="server" SkinID="sknSkillEmptyErrorMessage"
                                                                Text="No skill score found to display" Visible="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <asp:Repeater ID="ViewSkillScore_skillsRepeator" runat="server">
                                                <ItemTemplate>
                                                    <table style="width: 80%">
                                                        <tr>
                                                            <td style="width: 70%">
                                                                <asp:Label ID="ViewSkillScore_skillsRepeator_skillValueLabel" runat="server" SkinID="sknSkillNameLabel"
                                                                    Text='<%# DataBinder.Eval(Container.DataItem, "Skill") %>'></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <table style="width: 100%">
                                                                    <tr>
                                                                        <td style="width: 70%">
                                                                            <asp:Label ID="ViewSkillScore_skillsRepeator_testScoreLabel" Text="Test Score" runat="server"
                                                                                SkinID="sknSkillCaptionLabel"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 30%">
                                                                            <asp:Label ID="ViewSkillScore_skillsRepeator_testScoreValueLabel" runat="server"
                                                                                SkinID="sknSkillValueLabel" Text='<%# DataBinder.Eval(Container.DataItem, "TestScore") %>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 70%">
                                                                            <asp:Label ID="ViewSkillScore_skillsRepeator_interviewScoreLabel" Text="Interview Score"
                                                                                runat="server" SkinID="sknSkillCaptionLabel"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 30%">
                                                                            <asp:Label ID="ViewSkillScore_skillsRepeator_interviewScoreValueLabel" runat="server"
                                                                                SkinID="sknSkillValueLabel" Text='<%# DataBinder.Eval(Container.DataItem, "InterviewScore") %>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 70%">
                                                                            <asp:Label ID="ViewSkillScore_skillsRepeator_resumeScoreLabel" Text="Resume Score"
                                                                                runat="server" SkinID="sknSkillCaptionLabel"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 30%">
                                                                            <asp:Label ID="ViewSkillScore_skillsRepeator_resumeScoreValueLabel" runat="server"
                                                                                SkinID="sknSkillValueLabel" Text='<%# DataBinder.Eval(Container.DataItem, "ResumeScore") %>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                                <SeparatorTemplate>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="skill_list_separator" style="width: 100%">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </SeparatorTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header_bg">
                                        <asp:Literal ID="ViewSkillScore_overallSkillScoreLabel" runat="server" Text="Overall Skill Score"
                                            SkinID="sknLabelText"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table width="100%" cellspacing="3" cellpadding="0">
                                            <tr>
                                                <td style="width: 18%">
                                                    <asp:Label ID="ViewSkillScore_overallTestScoreLabel" runat="server" Text="Overall Test Score"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 8%">
                                                    <asp:Label ID="ViewSkillScore_overallTestScoreValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                        Text=""></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="ViewSkillScore_overallInterviewScoreLabel" runat="server" Text="Overall Interview Score"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 8%">
                                                    <asp:Label ID="ViewSkillScore_overallInterviewScoreValueLabel" runat="server" ReadOnly="True"
                                                        SkinID="sknLabelFieldText" Text="" Width="60%"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="ViewSkillScore_overallResumeScoreLabel" runat="server" Text="Overall Resume Score"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 8%">
                                                    <asp:Label ID="ViewSkillScore_overallResumeScoreValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                        Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_left_8" align="left">
                <asp:LinkButton ID="ViewSkillScore_bottomCancelButton" runat="server" Text="Cancel"
                    SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" ToolTip="Click here to close the window"></asp:LinkButton>
            </td>
        </tr>
    </table>
</asp:Content>
