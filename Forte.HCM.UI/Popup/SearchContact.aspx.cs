﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchContact.aspx.cs
// File that represents the SearchContact class that defines the user
// interface layout and functionalities for the search contact popup.
// This popup helps to search for contacts associated with the corporate
// account, clients, etc, that can be used in email window to select
// emails. This class inherits Forte.HCM.UI.Common.PageBase class

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the SearchContact class that defines the user
    /// interface layout and functionalities for the search contact popup.
    /// This popup helps to search for contacts associated with the corporate
    /// account, clients, etc, that can be used in email window to select
    /// emails. This class inherits Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class SearchContact : PageBase
    {
        #region Private Variables 

        /// <summary>
        /// A <see cref="string"/> that that holds the submit type.
        /// </summary>
        private string submitType = null;

        /// <summary>
        /// A <see cref="string"/> that holds the user type.
        /// </summary>
        private string userType = null;

        #endregion Private Variables

        #region Events Handlers

        /// <summary>
        /// Method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button.
                Page.Form.DefaultButton = SearchContact_goButton.UniqueID;

                // Set browser title.
                Master.SetPageCaption("Search Contact");

                // Retrieve submit type.
                if (!Utility.IsNullOrEmpty(Request.QueryString["submittype"]))
                    submitType = Request.QueryString["submittype"].Trim().ToUpper();

                // Retrieve user type.
                if (!Utility.IsNullOrEmpty(Request.QueryString["usertype"]))
                    userType = Request.QueryString["usertype"].Trim().ToUpper();

                if (!IsPostBack)
                {
                    SearchContact_contactsSelectedLabel.Text = string.Format("{0}:({1})",
                        Request.QueryString["addresstype"], 0);

                    // Clear all sessions.
                    Session["SELECTED_CONTACTS"] = null;
                    Session["SEARCHED_CONTACTS"] = null;

                    // Set default focus.
                    SearchContact_keywordTextBox.Focus();

                    // Load category drop down items.
                    LoadCategoryDropDownItems();

                    // Assign keyword.
                    AssignKeyword();

                    // By default search and show the results.
                    SearchContacts();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(SearchContact_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will be called when the done button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will assign the selected contacts to the email window.
        /// </remarks>
        protected void SearchContact_doneButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear message.
                SearchContact_errorMessageLabel.Text = string.Empty;
                SearchContact_successMessageLabel.Text = string.Empty;

                // Check if selected contacts present in session.
                if (Session["SELECTED_CONTACTS"] == null)
                {
                    SearchContact_errorMessageLabel.Text = "No contact has been selected";
                    return;
                }

                // Get selected contacts.
                List<MailContact> selectedMailContacts = Session["SELECTED_CONTACTS"] as List<MailContact>;

                // Do not proceed if selected contacts is empty.
                if (selectedMailContacts == null || selectedMailContacts.Count == 0)
                {
                    SearchContact_errorMessageLabel.Text = "No contact has been selected";
                    return;
                }

                // Compose the selected mail IDs separated by comma.
                string emailIDs = string.Empty;
                foreach (MailContact mailContact in selectedMailContacts)
                {
                    if (!Utility.IsNullOrEmpty(mailContact.EmailID))
                    {
                        if (emailIDs == string.Empty)
                            emailIDs = mailContact.EmailID;
                        else
                            emailIDs = emailIDs + "," + mailContact.EmailID;
                    }
                }

                // Assign the emailIDs to the hidden field.
                SearchContact_selectedContacts.Value = emailIDs;

                // Assing to the parent and close the window.
                ScriptManager.RegisterStartupScript(this, this.GetType(), "OnDoneClick",
                    "javascript: OnDoneClick()", true);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(SearchContact_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will be called when the go button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will search for the contacts to the given search criteria.
        /// </remarks>
        protected void SearchContact_goButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear message.
                SearchContact_errorMessageLabel.Text = string.Empty;
                SearchContact_successMessageLabel.Text = string.Empty;

                // Search contacts.
                SearchContacts();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(SearchContact_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will be called when a item is selected from the 
        /// category drop down list.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will search for the contacts to the given search criteria.
        /// </remarks>
        protected void SearchContact_categoryDropDownList_SelectedIndexChanged
            (object sender, EventArgs e)
        {
            try
            {
                // Clear message.
                SearchContact_errorMessageLabel.Text = string.Empty;
                SearchContact_successMessageLabel.Text = string.Empty;

                // Search contacts.
                SearchContacts();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(SearchContact_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the searched contacts grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        protected void SearchContact_contactsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                // Clear message.
                SearchContact_errorMessageLabel.Text = string.Empty;
                SearchContact_successMessageLabel.Text = string.Empty;

                if (e.CommandName == "Add")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string fullName = (SearchContact_contactsGridView.Rows
                        [index].FindControl("SearchContact_contactsGridView_fullNameLabel") as Label).Text;
                    string company = (SearchContact_contactsGridView.Rows
                        [index].FindControl("SearchContact_contactsGridView_companyLabel") as Label).Text;
                    string emailID = (SearchContact_contactsGridView.Rows
                        [index].FindControl("SearchContact_contactsGridView_emailIDLabel") as Label).Text;

                    // Get the selected contacts list.
                    List<MailContact> selectedContacts = null;

                    if (Session["SELECTED_CONTACTS"] == null)
                        selectedContacts = new List<MailContact>();
                    else
                        selectedContacts = Session["SELECTED_CONTACTS"] as List<MailContact>;

                    // Check if the selected contact already present in the added contact.
                    MailContact findMailContact = selectedContacts.Find(delegate(MailContact current)
                    {
                        return current.FullName == fullName &&
                            current.EmailID == emailID &&
                            current.Company == company;
                    });

                    // Add the item to the selected contacts list, if not 
                    // already found in the selected contacts list.
                    if (findMailContact == null)
                        selectedContacts.Add(new MailContact(fullName, company, emailID));

                    // Update the selected candidate list to session.
                    Session["SELECTED_CONTACTS"] = selectedContacts;

                    // Update selected contacts flag.
                    UpdateSelectedContacts();

                    // Assign search results.
                    if (Session["SEARCHED_CONTACTS"] == null)
                        SearchContact_contactsGridView.DataSource = null;
                    else
                        SearchContact_contactsGridView.DataSource = Session["SEARCHED_CONTACTS"] as List<MailContact>;

                    SearchContact_contactsGridView.DataBind();

                    // Assign selected contacts grid.
                    if (Session["SELECTED_CONTACTS"] == null)
                        SearchContact_addedContactsGridView.DataSource = null;
                    else
                        SearchContact_addedContactsGridView.DataSource = Session["SELECTED_CONTACTS"] as List<MailContact>;

                    SearchContact_addedContactsGridView.DataBind();

                    // Set added contacts count.
                    SetAddedContactsCount();
                }
                else if (e.CommandName == "Remove")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string fullName = (SearchContact_contactsGridView.Rows
                        [index].FindControl("SearchContact_contactsGridView_fullNameLabel") as Label).Text;
                    string company = (SearchContact_contactsGridView.Rows
                        [index].FindControl("SearchContact_contactsGridView_companyLabel") as Label).Text;
                    string emailID = (SearchContact_contactsGridView.Rows
                        [index].FindControl("SearchContact_contactsGridView_emailIDLabel") as Label).Text;

                    // Check if searched contacts present in session.
                    if (Session["SEARCHED_CONTACTS"] == null)
                        return;

                    // Get searched contacts.
                    List<MailContact> searchedMailContacts = Session
                        ["SEARCHED_CONTACTS"] as List<MailContact>;

                    // Find and update contacts list.
                    if (Session["SELECTED_CONTACTS"] != null)
                    {
                        // Get selected contacts.
                        List<MailContact> selectedMailContacts = Session["SELECTED_CONTACTS"] as List<MailContact>;

                        // Loop through the items in the selected contacts list.
                        bool found = false;
                        int findIndex = 0;
                        for (; findIndex < selectedMailContacts.Count; findIndex++)
                        {
                            if (searchedMailContacts[index].FullName == selectedMailContacts[findIndex].FullName &&
                                searchedMailContacts[index].Company == selectedMailContacts[findIndex].Company &&
                                searchedMailContacts[index].EmailID == selectedMailContacts[findIndex].EmailID)
                            {
                                found = true;
                                break;
                            }
                        }

                        if (found == true)
                        {
                            // Remove the item.
                            selectedMailContacts.RemoveAt(findIndex);
                        }

                        // Update the selected contacts list.
                        Session["SELECTED_CONTACTS"] = selectedMailContacts;
                    }

                    // Update the selected flag in the search contacts list.
                    searchedMailContacts[index].Selected = false;

                    // Update the searched contacts list.
                    Session["SEARCHED_CONTACTS"] = searchedMailContacts;

                    // Update selected contacts flag.
                    UpdateSelectedContacts();

                    // Assign search results.
                    if (Session["SEARCHED_CONTACTS"] == null)
                        SearchContact_contactsGridView.DataSource = null;
                    else
                        SearchContact_contactsGridView.DataSource = Session["SEARCHED_CONTACTS"] as List<MailContact>;

                    SearchContact_contactsGridView.DataBind();

                    // Assign selected contacts grid.
                    if (Session["SELECTED_CONTACTS"] == null)
                        SearchContact_addedContactsGridView.DataSource = null;
                    else
                        SearchContact_addedContactsGridView.DataSource = Session["SELECTED_CONTACTS"] as List<MailContact>;

                    SearchContact_addedContactsGridView.DataBind();

                    // Set added contacts count.
                    SetAddedContactsCount();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(SearchContact_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will be called when the row command event is fired in 
        /// the selected contacts grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        protected void SearchContact_addedContactsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                // Clear message.
                SearchContact_errorMessageLabel.Text = string.Empty;
                SearchContact_successMessageLabel.Text = string.Empty;

                if (e.CommandName == "Remove")
                {
                    int index = Convert.ToInt32(e.CommandArgument);

                    // Check if selected contacts present in session.
                    if (Session["SELECTED_CONTACTS"] == null)
                        return;

                    // Get selected contacts.
                    List<MailContact> selectedMailContacts = Session["SELECTED_CONTACTS"] as List<MailContact>;

                    // Remove the item from the selected contacts list.
                    selectedMailContacts.RemoveAt(index);

                    // Update the selected candidate list to session.
                    Session["SELECTED_CONTACTS"] = selectedMailContacts;

                    // Update selected contacts flag.
                    UpdateSelectedContacts();

                    // Assign search results.
                    if (Session["SEARCHED_CONTACTS"] == null)
                        SearchContact_contactsGridView.DataSource = null;
                    else
                        SearchContact_contactsGridView.DataSource = Session["SEARCHED_CONTACTS"] as List<MailContact>;

                    SearchContact_contactsGridView.DataBind();

                    // Assign selected contacts grid.
                    if (Session["SELECTED_CONTACTS"] == null)
                        SearchContact_addedContactsGridView.DataSource = null;
                    else
                        SearchContact_addedContactsGridView.DataSource = Session["SELECTED_CONTACTS"] as List<MailContact>;

                    SearchContact_addedContactsGridView.DataBind();

                    // Set added contacts count.
                    SetAddedContactsCount();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(SearchContact_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will be called when the select all link button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will select all searched contacts to the added contacts list.
        /// </remarks>
        protected void SearchContact_selectAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if searched contacts present in session.
                if (Session["SEARCHED_CONTACTS"] == null)
                    return;

                // Get searched contacts.
                List<MailContact> searchedMailContacts = Session
                    ["SEARCHED_CONTACTS"] as List<MailContact>;

                // Get the selected contacts list.
                List<MailContact> selectedContacts = null;

                if (Session["SELECTED_CONTACTS"] == null)
                    selectedContacts = new List<MailContact>();
                else
                    selectedContacts = Session["SELECTED_CONTACTS"] as List<MailContact>;

                // Loop through all contacts in the searched contacts, check 
                // with the selected contact list, and if not found add to it.
                foreach (MailContact mailContact in searchedMailContacts)
                {
                    // Check if the selected contact already present in the added contact.
                    MailContact findMailContact = selectedContacts.Find(delegate(MailContact current)
                    {
                        return current.FullName == mailContact.FullName &&
                            current.EmailID == mailContact.EmailID &&
                            current.Company == mailContact.Company;
                    });

                    // Add the item to the selected contacts list, if not 
                    // already found in the selected contacts list.
                    if (findMailContact == null)
                    {
                        selectedContacts.Add(new MailContact
                            (mailContact.FullName, mailContact.Company, mailContact.EmailID));
                    }
                }

                // Update the selected candidate list to session.
                Session["SELECTED_CONTACTS"] = selectedContacts;

                // Update selected contacts flag.
                UpdateSelectedContacts();

                // Assign search results.
                if (Session["SEARCHED_CONTACTS"] == null)
                    SearchContact_contactsGridView.DataSource = null;
                else
                    SearchContact_contactsGridView.DataSource = Session["SEARCHED_CONTACTS"] as List<MailContact>;

                SearchContact_contactsGridView.DataBind();

                // Assign selected contacts grid.
                if (Session["SELECTED_CONTACTS"] == null)
                    SearchContact_addedContactsGridView.DataSource = null;
                else
                    SearchContact_addedContactsGridView.DataSource = Session["SELECTED_CONTACTS"] as List<MailContact>;

                SearchContact_addedContactsGridView.DataBind();

                // Set added contacts count.
                SetAddedContactsCount();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(SearchContact_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will be called when the remove all link button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will remove all added contacts.
        /// </remarks>
        protected void SearchContact_removeAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear selected contacts present in session.
                Session["SELECTED_CONTACTS"] = null;

                // Check if searched contacts present in session.
                if (Session["SEARCHED_CONTACTS"] == null)
                    return;

                // Get searched contacts from session.
                List<MailContact> mailContacts = Session["SEARCHED_CONTACTS"] as List<MailContact>;

                // Set all mail contacts selected flag to false.
                foreach (MailContact mailContact in mailContacts)
                    mailContact.Selected = false;

                // Keep searched contacts list in session.
                Session["SEARCHED_CONTACTS"] = mailContacts;

                // Assign search results.
                if (Session["SEARCHED_CONTACTS"] == null)
                    SearchContact_contactsGridView.DataSource = null;
                else
                    SearchContact_contactsGridView.DataSource = Session["SEARCHED_CONTACTS"] as List<MailContact>;

                SearchContact_contactsGridView.DataBind();

                // Assign selected contacts grid.
                if (Session["SELECTED_CONTACTS"] == null)
                    SearchContact_addedContactsGridView.DataSource = null;
                else
                    SearchContact_addedContactsGridView.DataSource = Session["SELECTED_CONTACTS"] as List<MailContact>;

                SearchContact_addedContactsGridView.DataBind();

                // Set added contacts count.
                SetAddedContactsCount();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(SearchContact_errorMessageLabel, exp.Message);
            }
        }
        
        /// <summary>
        /// Method that will be called when the reset link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the window.
        /// </remarks>
        protected void SearchContact_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear message.
                SearchContact_errorMessageLabel.Text = string.Empty;
                SearchContact_successMessageLabel.Text = string.Empty;

                // Clear keywords.
                SearchContact_keywordTextBox.Text = string.Empty;

                // Clear all sessions.
                Session["SELECTED_CONTACTS"] = null;
                Session["SEARCHED_CONTACTS"] = null;

                // Clear grid rows.
                SearchContact_contactsGridView.DataSource = null;
                SearchContact_contactsGridView.DataBind();
                SearchContact_addedContactsGridView.DataSource = null;
                SearchContact_addedContactsGridView.DataBind();

                // Clear selected contacts count.
                SearchContact_contactsSelectedLabel.Text = string.Format("{0}:({1})",
                    Request.QueryString["addresstype"], 0);

                // Set added contacts count.
                SetAddedContactsCount();

                // Set default focus.
                SearchContact_keywordTextBox.Focus();

                LoadCategoryDropDownItems();

                // Assign keyword.
                AssignKeyword();

                // By default search and show the results.
                SearchContacts();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(SearchContact_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will be called when the row data bound event is fired
        /// in the searched contacs grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchContact_contactsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                // Get selected flag.
                bool selected = Convert.ToBoolean((e.Row.FindControl
                    ("SearchContact_contactsGridView_selectedFlagHiddenField") as HiddenField).Value);

                Label fullNameLabel = e.Row.FindControl("SearchContact_contactsGridView_fullNameLabel") as Label;
                e.Row.BorderColor = Color.FromArgb(249, 249, 249);
                e.Row.BorderWidth = new Unit(1, UnitType.Pixel);

                if (selected)
                {
                 //   e.Row.BackColor = Color.FromArgb(238, 242, 244);
                    e.Row.BackColor = Color.FromArgb(230,235,238);
                }
                else
                {
                    e.Row.BackColor = Color.FromArgb(255, 255, 255);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(SearchContact_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will be called when the row data bound event is fired
        /// in the searched contacs grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchContact_addedContactsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                //e.Row.BorderColor = Color.FromArgb(216, 216, 216);
                e.Row.BorderColor = Color.FromArgb(249, 249, 249);
                e.Row.BorderWidth = new Unit(1, UnitType.Pixel);
                e.Row.BackColor = Color.FromArgb(255, 255, 255);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(SearchContact_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden protected method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that assign the client contact names as keywords in the 
        /// search text box.
        /// </summary>
        /// <remarks>
        /// This keyword is retrieved from the contacts associated with the
        /// given position profile
        /// </remarks>
        private void AssignKeyword()
        {
            // Check if position profile ID is passed through query 
            // string. If not present do not proceed further.
            if (Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                return;

            // Check if fill keyword flag is true. If not do not proceed 
            // further.
            if (Utility.IsNullOrEmpty(Request.QueryString["fillkeyword"]) ||
                Request.QueryString["fillkeyword"].Trim().ToUpper() == "N")
                return;

            // Check if position profile ID is valid integer.
            int positionProfileID = 0;
            if (int.TryParse(Request.QueryString["positionprofileid"].Trim(), out positionProfileID) == false)
                return;

            // Check if position profile is a non-zero & non-negative value.
            if (positionProfileID <= 0)
                return;

            if (submitType == "C" || submitType == "I")
            {
                // Fill the client contact names as keyword when the submit type is 'client'
                // Fill the owner & co-owner names as keyword when the submit type is 'internal'
                string keyword = new CommonBLManager().GetPositionProfileContactKeyword
                    (positionProfileID, submitType);

                if (!Utility.IsNullOrEmpty(keyword))
                    SearchContact_keywordTextBox.Text = keyword;
            }
        }

        /// <summary>
        /// Method that searches for the contacts.
        /// </summary>
        private void SearchContacts()
        {
            // Clear session.
            Session["SEARCHED_CONTACTS"] = null;

            // Construct search parameters.
            string keyword = SearchContact_keywordTextBox.Text.Trim();
            int roleID = 0;

            string category = string.Empty;

            if (SearchContact_categoryDropDownList.SelectedItem != null &&
                SearchContact_categoryDropDownList.SelectedItem.Text.Trim().ToUpper() == "ALL CONTACTS")
            {
                // All contacts.
                category = "ALL";
            }
            else if (SearchContact_categoryDropDownList.SelectedItem != null &&
                SearchContact_categoryDropDownList.SelectedItem.Text.Trim().ToUpper() == "ALL INTERNAL USERS")
            {
                category = "AIU";
            }
            else if (SearchContact_categoryDropDownList.SelectedItem != null &&
               SearchContact_categoryDropDownList.SelectedItem.Text.Trim().ToUpper() == "ALL CLIENT CONTACTS")
            {
                category = "ACC";
            }
            else if (SearchContact_categoryDropDownList.SelectedItem != null &&
              SearchContact_categoryDropDownList.SelectedItem.Text.Trim().ToUpper() == "MY CLIENT CONTACTS")
            {
                category = "MCC";
            }
            else if (!Utility.IsNullOrEmpty(SearchContact_categoryDropDownList.SelectedValue))
            {
                category = "ROL";
                int.TryParse(SearchContact_categoryDropDownList.SelectedValue, out roleID);
            }

            // Get mail contacts.
            List<MailContact> mailContacts = new CommonBLManager().GetMailContacts
                (base.tenantID, base.userID, keyword, category, roleID);

            if (mailContacts != null && mailContacts.Count > 0)
            {
                // Keep searched contacts in session.
                Session["SEARCHED_CONTACTS"] = mailContacts;

                // Update selected contacts flag.
                UpdateSelectedContacts();
            }

            // Assign search results grid.
            if (Session["SEARCHED_CONTACTS"] == null)
                SearchContact_contactsGridView.DataSource = null;
            else
                SearchContact_contactsGridView.DataSource = Session["SEARCHED_CONTACTS"] as List<MailContact>;

            SearchContact_contactsGridView.DataBind();
        }

        /// <summary>
        /// Method that updates the 'selected' flag in the searched contacts 
        /// list by comparing the selected contact list maintained in session.      
        /// </summary>
        /// <param name="mailContacts">
        /// A list of <see cref="MailContact"/> that holds the searched contact
        /// list.
        /// </param>
        private void UpdateSelectedContacts()
        {
            // Check if searched contacts present in session.
            if (Session["SEARCHED_CONTACTS"] == null)
                return;

            // Get searched contacts from session.
            List<MailContact> mailContacts = Session["SEARCHED_CONTACTS"] as List<MailContact>;

            // Do not proceed if searched contacts is empty.
            if (mailContacts == null || mailContacts.Count == 0)
                return;

            // Check if selected contacts present in session.
            if (Session["SELECTED_CONTACTS"] == null)
            {
                // Set all mail contacts selected flag to false.
                foreach (MailContact mailContact in mailContacts)
                    mailContact.Selected = false;

                return;
            }

            // Get selected contacts.
            List<MailContact> selectedMailContacts = Session["SELECTED_CONTACTS"] as List<MailContact>;

            // Do not proceed if selected contacts is empty.
            if (selectedMailContacts == null || selectedMailContacts.Count == 0)
            {
                // Set all mail contacts selected flag to false.
                foreach (MailContact mailContact in mailContacts)
                    mailContact.Selected = false;

                return;
            }

            // Compare every item in the searched contacts list with the 
            // selected contacts list and update the 'selected' flag.
            foreach (MailContact mailContact in mailContacts)
            {
                MailContact findMailContact = selectedMailContacts.Find(delegate(MailContact current)
                {
                    return current.FullName == mailContact.FullName &&
                        current.EmailID == mailContact.EmailID &&
                        current.Company == mailContact.Company;
                });

                if (findMailContact != null)
                    mailContact.Selected = true;
                else
                    mailContact.Selected = false;
            }

            // Keep searched contacts list in session.
            Session["SEARCHED_CONTACTS"] = mailContacts;
        }

        /// <summary>
        /// Method that loads the category drop down items.
        /// </summary>
        private void LoadCategoryDropDownItems()
        {
            // Clear items.
            SearchContact_categoryDropDownList.Items.Clear();

            // Add 'all contacts' item.
            SearchContact_categoryDropDownList.Items.Add(new ListItem("All Contacts", "0"));

            int maxRoleID = 0;
            // Get all roles and add.
            List<Roles> roles = new AuthenticationBLManager().GetAssignedCorporateRole(base.tenantID);
            if (roles != null && roles.Count > 0)
            {
                foreach (Roles role in roles)
                {
                    SearchContact_categoryDropDownList.Items.Add
                        (new ListItem(role.RoleName, role.RoleID.ToString()));

                    if (role.RoleID > maxRoleID)
                        maxRoleID = role.RoleID;
                }
            }

            // Add 'others'.
            int internalUsersValue = ++maxRoleID;
            SearchContact_categoryDropDownList.Items.Add(new ListItem("All Internal Users", (maxRoleID).ToString()));
            SearchContact_categoryDropDownList.Items.Add(new ListItem("All Client Contacts", (++maxRoleID).ToString()));
            SearchContact_categoryDropDownList.Items.Add(new ListItem("My Client Contacts", (++maxRoleID).ToString()));

            // Show only the internal users item, for internal submittal or mailing review
            // of internal review.
            if (submitType == "I" || userType == "I")
            {
                if (SearchContact_categoryDropDownList.Items.FindByValue(internalUsersValue.ToString()) != null)
                {
                    SearchContact_categoryDropDownList.SelectedValue = internalUsersValue.ToString();
                    SearchContact_categoryDropDownList.Enabled = false;
                }
            }
        }

        /// <summary>
        /// Method that sets the added contacts count label.
        /// </summary>
        private void SetAddedContactsCount()
        {
            if (Session["SELECTED_CONTACTS"] == null)
            {
                SearchContact_contactsSelectedLabel.Text = string.Format("{0}:({1})", 
                    Request.QueryString["addresstype"], 0);

                return;
            }

            SearchContact_contactsSelectedLabel.Text = string.Format("{0}:({1})",
                Request.QueryString["addresstype"], (Session["SELECTED_CONTACTS"] as List<MailContact>).Count);
        }

        #endregion Private Methods

        #region Protected Methods                                              

        /// <summary>
        /// Method that retrieves the selected contact flag. This helps to 
        /// show or hide icons in grid.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds the selected status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of selected 
        /// status. True represents selected and false not selected.
        /// </returns>
        /// <remarks>
        /// The applicable status for show results to candidates are:
        /// 1. True - Show.
        /// 2. False - Do not show.
        /// </remarks>
        protected bool IsSelected(string status)
        {
            return (status.Trim().ToUpper() == "TRUE" ? true : false);
        }

        #endregion Protected Methods
    }
}
