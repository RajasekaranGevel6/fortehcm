﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchLocation.aspx.cs
// File that represents the user interface layout and functionalities
// for the SearchLocation page. This page helps in searching for 
// categories by providing search criteria for category ID and category
// name. This class inherits the Forte.HCM.UI.Common.PageBase class.

#endregion

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the SearchLocation page. This page helps in searching for 
    /// categories by providing search criteria for category ID and category 
    /// name. This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class SearchLocation : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "224px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "274px";

        #endregion Private Constants

        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Search Location");
                // Set default button and focus.
                Page.Form.DefaultButton = SearchLocation_searchButton.UniqueID;

                // Subscribes to the page number click event.
                SearchLocation_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchLocation_pageNavigator_PageNumberClick);

                // 
                if (!Utility.IsNullOrEmpty(SearchLocation_isMaximizedHiddenField.Value) &&
                    SearchLocation_isMaximizedHiddenField.Value == "Y")
                {
                    SearchLocation_searchCriteriasDiv.Style["display"] = "none";
                    SearchLocation_searchResultsUpSpan.Style["display"] = "block";
                    SearchLocation_searchResultsDownSpan.Style["display"] = "none";
                    SearchLocation_searchResultsDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchLocation_searchCriteriasDiv.Style["display"] = "block";
                    SearchLocation_searchResultsUpSpan.Style["display"] = "none";
                    SearchLocation_searchResultsDownSpan.Style["display"] = "block";
                    SearchLocation_searchResultsDiv.Style["height"] = RESTORED_HEIGHT;
                }

                if (!IsPostBack)
                {
                    // Set default focus.
                    Page.Form.DefaultFocus = SearchLocation_cityTextBox.UniqueID;
                    SearchLocation_cityTextBox.Focus();

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "CATEGORYID";

                    SearchLocation_searchResultsDiv.Visible = false;
                }
                else
                {
                    SearchLocation_searchResultsDiv.Visible = true;
                }

                SearchLocation_searchTestResultsTR.Attributes.Add("onclick", 
                    "ExpandOrRestore('" +
                    SearchLocation_searchResultsDiv.ClientID + "','" +
                    SearchLocation_searchCriteriasDiv.ClientID + "','" +
                    SearchLocation_searchResultsUpSpan.ClientID + "','" +
                    SearchLocation_searchResultsDownSpan.ClientID + "','" +
                    SearchLocation_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" + 
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchLocation_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchLocation_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                SearchLocation_errorMessageLabel.Text = string.Empty;
                SearchLocation_successMessageLabel.Text = string.Empty;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "CATEGORYID";

                // Reset the paging control.
                SearchLocation_pageNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchLocation_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void SearchLocation_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchLocation_cityTextBox.Text = string.Empty;
                SearchLocation_stateTextBox.Text = string.Empty;
                SearchLocation_countryTextbox.Text = string.Empty;

                SearchLocation_searchResultsGridView.DataSource = null;
                SearchLocation_searchResultsGridView.DataBind();

                SearchLocation_pageNavigator.PageSize = base.GridPageSize;
                SearchLocation_pageNavigator.TotalRecords = 0;
                SearchLocation_searchResultsDiv.Visible = false;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "CATEGORYID";

                // Reset to the restored state.
                SearchLocation_searchCriteriasDiv.Style["display"] = "block";
                SearchLocation_searchResultsUpSpan.Style["display"] = "none";
                SearchLocation_searchResultsDownSpan.Style["display"] = "block";
                SearchLocation_searchResultsDiv.Style["height"] = RESTORED_HEIGHT;
                SearchLocation_isMaximizedHiddenField.Value = "N";

                // Clear message fields.
                SearchLocation_successMessageLabel.Text = string.Empty;
                SearchLocation_errorMessageLabel.Text = string.Empty;

                // Set default focus.
                Page.Form.DefaultFocus = SearchLocation_cityTextBox.UniqueID;
                SearchLocation_cityTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchLocation_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchLocation_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                Search(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchLocation_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchLocation_searchResultsGridView_Sorting(object sender, 
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                SearchLocation_pageNavigator.Reset();
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchLocation_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchLocation_searchResultsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchLocation_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchLocation_searchResultsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchLocation_searchResultsGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchLocation_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods                                                

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void Search(int pageNumber)
        {
            int totalRecords = 0;

            // Get the city,state and country
            CandidateInformation candidateLocation = new CandidateInformation();

            candidateLocation.caiCityName = SearchLocation_cityTextBox.Text;
            candidateLocation.caiStateName = SearchLocation_stateTextBox.Text;
            candidateLocation.caiCountryName = SearchLocation_countryTextbox.Text;

          List<CandidateInformation> location = new CommonBLManager().GetLocation(candidateLocation,
              pageNumber, base.GridPageSize, out totalRecords, ViewState["SORT_FIELD"].ToString(),
              ((SortType)ViewState["SORT_ORDER"]));



          if (location == null || location.Count == 0)
          {
              SearchLocation_searchResultsDiv.Visible = false;
              ShowMessage(SearchLocation_errorMessageLabel,
                  Resources.HCMResource.Common_Empty_Grid);
          }
          else
          {
              SearchLocation_searchResultsDiv.Visible = true;
          }

          SearchLocation_searchResultsGridView.DataSource = location;
          SearchLocation_searchResultsGridView.DataBind();
          SearchLocation_pageNavigator.PageSize = base.GridPageSize;
          SearchLocation_pageNavigator.TotalRecords = totalRecords;
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}

