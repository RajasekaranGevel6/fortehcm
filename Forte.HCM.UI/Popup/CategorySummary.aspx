﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="CategorySummary.aspx.cs" Inherits="Forte.HCM.UI.Popup.CategorySummary" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="CategorySummary_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <asp:Label ID="CategorySummary_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="CategorySummary_titleLiteral" runat="server" Text="Categories Contributed"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="CategorySummary_closeImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td align="center" class="popup_td_padding_10">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="grid_body_bg">
                                        <div id="CategorySummary_categoryNameListDIV" style="height: 280px; overflow: auto;">
                                            <asp:GridView ID="CategorySummary_categoriesGridView" runat="server" SkinID="sknWrapHeaderGrid"
                                                AllowSorting="true" GridLines="Horizontal" 
                                                OnRowDataBound="CategorySummary_categoriesGridView_RowDataBound">
                                                <RowStyle CssClass="grid_alternate_row" />
                                                <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                <HeaderStyle CssClass="grid_header_row" />
                                                <Columns>
                                                    <asp:BoundField HeaderText="Category ID" DataField="CategoryID" ItemStyle-Width="20px" />
                                                    <asp:BoundField HeaderText="Category Name" DataField="CategoryName" ItemStyle-Width="190px" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:PageNavigator ID="CategorySummary_pageNavigator" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_20">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_left_8">
                <asp:LinkButton ID="CategorySummary_cancelLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClientClick="javascript:window.close();" />
            </td>
        </tr>
    </table>
</asp:Content>
