﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.Popup {
    
    
    public partial class AvailabilityRequest {
        
        /// <summary>
        /// AvailabilityRequest_titleLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal AvailabilityRequest_titleLiteral;
        
        /// <summary>
        /// AvailabilityRequest_topCancelImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton AvailabilityRequest_topCancelImageButton;
        
        /// <summary>
        /// AvailabilityRequest_messageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel AvailabilityRequest_messageUpdatePanel;
        
        /// <summary>
        /// AvailabilityRequest_topErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label AvailabilityRequest_topErrorMessageLabel;
        
        /// <summary>
        /// AvailabilityRequest_topSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label AvailabilityRequest_topSuccessMessageLabel;
        
        /// <summary>
        /// AvailabilityRequest_inputControlsUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel AvailabilityRequest_inputControlsUpdatePanel;
        
        /// <summary>
        /// AvailabilityRequest_selectAssessorLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal AvailabilityRequest_selectAssessorLiteral;
        
        /// <summary>
        /// AvailabilityRequest_emailImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton AvailabilityRequest_emailImageButton;
        
        /// <summary>
        /// AvailabilityRequest_assessorLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label AvailabilityRequest_assessorLabel;
        
        /// <summary>
        /// AvailabilityRequest_assessorDropDownList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList AvailabilityRequest_assessorDropDownList;
        
        /// <summary>
        /// AvailabilityRequest_selectedAssessorID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField AvailabilityRequest_selectedAssessorID;
        
        /// <summary>
        /// AvailabilityRequest_skillsLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label AvailabilityRequest_skillsLabel;
        
        /// <summary>
        /// AvailabilityRequest_skillsCheckBoxList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBoxList AvailabilityRequest_skillsCheckBoxList;
        
        /// <summary>
        /// AvailabilityRequest_selectedTimeSlotsLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal AvailabilityRequest_selectedTimeSlotsLiteral;
        
        /// <summary>
        /// AvailabilityRequest_addTimeSlotsLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton AvailabilityRequest_addTimeSlotsLinkButton;
        
        /// <summary>
        /// AvailabilityRequest_addTimeSlotsButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button AvailabilityRequest_addTimeSlotsButton;
        
        /// <summary>
        /// AvailabilityRequest_selectedTimeSlotsGridView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView AvailabilityRequest_selectedTimeSlotsGridView;
        
        /// <summary>
        /// AvailabilityRequest_buttonControlsUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel AvailabilityRequest_buttonControlsUpdatePanel;
        
        /// <summary>
        /// AvailabilityRequest_acceptButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button AvailabilityRequest_acceptButton;
        
        /// <summary>
        /// AvailabilityRequest_cancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton AvailabilityRequest_cancelLinkButton;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.PopupMaster Master {
            get {
                return ((Forte.HCM.UI.MasterPages.PopupMaster)(base.Master));
            }
        }
    }
}
