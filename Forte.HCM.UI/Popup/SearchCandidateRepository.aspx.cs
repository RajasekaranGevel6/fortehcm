﻿#region Directives
using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion

namespace Forte.HCM.UI.Popup
{
    public partial class SearchCandidateRepository : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "245px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "294px";

        #endregion Private Constants

        #region Events Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Search Candidate Repository");
                // Set default button and focus.
                Page.Form.DefaultButton = SearchCandidate_searchButton.UniqueID;

                // Subscribes to the page number click event.
                SearchCandidate_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchCandidate_pageNavigator_PageNumberClick);

                // 
                if (!Utility.IsNullOrEmpty(SearchCandidate_isMaximizedHiddenField.Value) &&
                    SearchCandidate_isMaximizedHiddenField.Value == "Y")
                {
                    SearchCandidate_searchCriteriasDiv.Style["display"] = "none";
                    SearchCandidate_searchResultsUpSpan.Style["display"] = "block";
                    SearchCandidate_searchResultsDownSpan.Style["display"] = "none";
                    SearchCandidate_testDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchCandidate_searchCriteriasDiv.Style["display"] = "block";
                    SearchCandidate_searchResultsUpSpan.Style["display"] = "none";
                    SearchCandidate_searchResultsDownSpan.Style["display"] = "block";
                    SearchCandidate_testDiv.Style["height"] = RESTORED_HEIGHT;
                }
                
                if (!IsPostBack)
                {
                    // Set default focus.
                    Page.Form.DefaultFocus = SearchCandidate_firstNameTextBox.UniqueID;
                    SearchCandidate_firstNameTextBox.Focus();

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "USERNAME";

                    SearchCandidate_testDiv.Visible = false;
                }
                else
                {
                    SearchCandidate_testDiv.Visible = true;
                }

                SearchCandidate_searchTestResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    SearchCandidate_testDiv.ClientID + "','" +
                    SearchCandidate_searchCriteriasDiv.ClientID + "','" +
                    SearchCandidate_searchResultsUpSpan.ClientID + "','" +
                    SearchCandidate_searchResultsDownSpan.ClientID + "','" +
                    SearchCandidate_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchCandidate_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                SearchCandidate_errorMessageLabel.Text = string.Empty;
                SearchCandidate_successMessageLabel.Text = string.Empty;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "USERNAME";

                // Reset the paging control.
                SearchCandidate_pageNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void SearchCandidate_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchCandidate_firstNameTextBox.Text = string.Empty;
                SearchCandidate_middleNameTextBox.Text = string.Empty;
                SearchCandidate_lastNameTextBox.Text = string.Empty;
                SearchCandidate_emailTextBox.Text = string.Empty;

                SearchCandidate_testGridView.DataSource = null;
                SearchCandidate_testGridView.DataBind();

                SearchCandidate_pageNavigator.PageSize = base.GridPageSize;
                SearchCandidate_pageNavigator.TotalRecords = 0;
                SearchCandidate_testDiv.Visible = false;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "USERNAME";

                // Reset to the restored state.
                SearchCandidate_searchCriteriasDiv.Style["display"] = "block";
                SearchCandidate_searchResultsUpSpan.Style["display"] = "none";
                SearchCandidate_searchResultsDownSpan.Style["display"] = "block";
                SearchCandidate_testDiv.Style["height"] = RESTORED_HEIGHT;
                SearchCandidate_isMaximizedHiddenField.Value = "N";

                // Clear message fields.
                SearchCandidate_successMessageLabel.Text = string.Empty;
                SearchCandidate_errorMessageLabel.Text = string.Empty;

                // Set default focus.
                Page.Form.DefaultFocus = SearchCandidate_firstNameTextBox.UniqueID;
                SearchCandidate_middleNameTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchCandidate_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                Search(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchCandidate_testGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                SearchCandidate_pageNavigator.Reset();
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchCandidate_testGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                // Set row styles.
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                ImageButton SearchCandidate_candidateDetailGridView_addNotesImageButton = e.Row.FindControl
                    ("SearchCandidate_addNotesImageButton") as ImageButton;

                ImageButton SearchCandidate_viewCandidateActivityLogImageButton = e.Row.FindControl
                    ("SearchCandidate_viewCandidateActivityLogImageButton") as ImageButton;

                int candidateID = Convert.ToInt32((e.Row.FindControl("SearchCandidate_userIDHiddenfield") as HiddenField).Value);

                // Assign events to 'add notes' icon.
                SearchCandidate_candidateDetailGridView_addNotesImageButton.Attributes.Add("OnClick",
                   "javascript:return OpenAddNotesPopup('" + candidateID + "')");

                // Assign events to 'view candidate activity log' icon.
                SearchCandidate_viewCandidateActivityLogImageButton.Attributes.Add
                    ("OnClick", "javascript:return OpenViewCandidateActivityLopPopup('" + candidateID + "')");

                HyperLink SearchCandidate_viewCandidateActivityLogHyperLink = (HyperLink)e.Row.FindControl("SearchCandidate_viewCandidateActivityLogHyperLink");
                if (SearchCandidate_viewCandidateActivityLogHyperLink != null)
                {
                    SearchCandidate_viewCandidateActivityLogHyperLink.NavigateUrl = "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&candidateid=" + candidateID;
                }

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchCandidate_testGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchCandidate_testGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the method to get the return string for 
        /// approved status
        /// </summary>
        /// <param name="isApproved">
        /// A<see cref="string"/>that holds the approved status
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the formatted string 
        /// </returns>
        protected string GetIsApproved(string isApproved)
        {
            string returnString = string.Empty;
            switch (isApproved)
            {
                case "Y":
                    returnString = Resources.HCMResource.ResumeUploader_CandidateResumeAlreadyExistApproved;
                    break;
                case "N":
                    returnString = Resources.HCMResource.ResumeUploader_CandidateResumeAlreadyExistNonApproved;
                    break;
                default:
                    returnString = string.Empty;
                    break;
            }
            return returnString;
        }

        #endregion Events Handlers

        #region Private Methods

        private void Search(int pageNumber)
        {
            int totalRecords = 0;

            List<CandidateDetail> users = new ResumeRepositoryBLManager().GetCandidateInformation(
                base.tenantID,
                SearchCandidate_firstNameTextBox.Text.Trim(),
                SearchCandidate_middleNameTextBox.Text.Trim(),
                SearchCandidate_lastNameTextBox.Text.Trim(),
                SearchCandidate_emailTextBox.Text.Trim(),
                ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]),
                pageNumber,
                base.GridPageSize,
                out totalRecords);

            if (users == null || users.Count == 0)
            {
                SearchCandidate_testDiv.Visible = false;
                SearchCandidate_errorMessageLabel.Text = "No data found to display";
                //Resources.HCMResource.Common_Empty_Grid, MessageType.Error);
            }
            else
            {
                SearchCandidate_testDiv.Visible = true;
            }

            SearchCandidate_testGridView.DataSource = users;
            SearchCandidate_testGridView.DataBind();
            SearchCandidate_pageNavigator.PageSize = base.GridPageSize;
            SearchCandidate_pageNavigator.TotalRecords = totalRecords;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}


