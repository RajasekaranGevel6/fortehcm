﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// RequestTimeSlot.aspx.cs
// File that represents the user interface layout and functionalities for
// the RequestTimeSlot page. This will helps to view scheduled time slots
// and to request new time slots for interview assessment.

#endregion Header

#region Directives                                                             

using System;
using System.Linq;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the RequestTimeSlot page. This will helps to view scheduled time slots
    /// and to request new time slots for interview assessment. This class 
    /// inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class RequestTimeSlot : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Request Time Slot");

                Page.Form.DefaultButton = RequestTimeSlot_addToRequestedTimeSlotsButton.UniqueID;

                if (!IsPostBack)
                {
                    // Call and assign query string values to hidden fields.
                    if (RetrieveQueryStringValues() == false)
                        return;

                    // Clear session.
                    Session["REQUEST_TIME_SLOT_NON_AVAILABLE_DATES"] = null;
                    Session["REQUEST_TIME_SLOT_SCHEDULED_DATES"] = null;

                    // Set default button and focus.
                    Page.Form.DefaultButton = RequestTimeSlot_selectButton.UniqueID;

                    if (Utility.IsNullOrEmpty(RequestTimeSlot_sessionKeyHiddenField.Value))
                    {
                        // If session key is null then it is a non-saved session.

                        // Hide the save button.
                        RequestTimeSlot_saveButton.Visible = false;

                        // Show the select button.
                        RequestTimeSlot_selectButton.Visible = true;

                        // Add handler to select button.
                        RequestTimeSlot_selectButton.Attributes.Add("onclick",
                            "return CloseRequestTimeSlotWindow();");

                        // Show selected assessor name and skills.
                        RequestTimeSlot_skillsLabel.Text = "Skills";
                        if (Session["AVAILABILITY_REQUEST_SELECTED_ASSESSOR"] != null)
                        {
                            RequestTimeSlot_assessorNameValueLabel.Text =
                                Session["AVAILABILITY_REQUEST_SELECTED_ASSESSOR"] as string;
                        }
                        if (Session["AVAILABILITY_REQUEST_SELECTED_SKILLS"] != null)
                        {
                            RequestTimeSlot_skillsValueLabel.Text =
                                Session["AVAILABILITY_REQUEST_SELECTED_SKILLS"] as string;
                        }
                    }
                    else
                    {
                        // If session key is not null then it is a saved session.

                        // Hide the select button.
                        RequestTimeSlot_selectButton.Visible = false;

                        // Show the save button.
                        RequestTimeSlot_saveButton.Visible = true;

                        // Show assessor name and skill.
                        RequestTimeSlot_skillsLabel.Text = "Skill";

                        // Get assessor combined info.
                        AssessorDetail assessorDetail = new AssessorBLManager().
                            GetAssessorCombinedInfo(Convert.ToInt32(RequestTimeSlot_assessorIDHiddenField.Value),
                            Convert.ToInt32(RequestTimeSlot_skillIDHiddenField.Value));

                        if (assessorDetail == null)
                        {
                            base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, "Invalid assessor ID");
                        }
                        else
                        {
                            // Assign details.
                            RequestTimeSlot_assessorNameValueLabel.Text = assessorDetail.FirstName;
                            if (!Utility.IsNullOrEmpty(assessorDetail.LastName))
                            {
                                RequestTimeSlot_assessorNameValueLabel.Text = RequestTimeSlot_assessorNameValueLabel.Text +
                                    " " + assessorDetail.LastName;
                            }
                            RequestTimeSlot_skillsValueLabel.Text = assessorDetail.Skill;
                        }
                    }

                    // Set current date as selected date.
                    RequestTimeSlot_selectDateCalendar.SelectedDates.Clear();

                    // Get current date and time.
                    DateTime currentDate = DateTime.Now;
                    RequestTimeSlot_selectDateCalendar.SelectedDates.Add(currentDate);

                    // Keep the date in session.
                    Session["REQUEST_TIME_SLOT_DATE"] = currentDate;

                    // Clear new slots session.
                    Session["REQUEST_TIME_SLOT_LIST"] = null;

                    RequestTimeSlot_selectedDateLabel.Text = currentDate.ToString("dddd, dd MMMM yyyy");

                    AssessorBLManager blManager = new AssessorBLManager();

                    // Set time slot settings.
                    RequestTimeSlot_fromDropDownList.DataSource = blManager.
                        GetTimeSlotSettings(TimeSlotType.From);
                    RequestTimeSlot_fromDropDownList.DataTextField = "Name";
                    RequestTimeSlot_fromDropDownList.DataValueField = "ID";
                    RequestTimeSlot_fromDropDownList.DataBind();
                    RequestTimeSlot_fromDropDownList.Items.Insert(0, new ListItem("--Select--", ""));

                    RequestTimeSlot_toDropDownList.DataSource = blManager.
                        GetTimeSlotSettings(TimeSlotType.To);
                    RequestTimeSlot_toDropDownList.DataTextField = "Name";
                    RequestTimeSlot_toDropDownList.DataValueField = "ID";
                    RequestTimeSlot_toDropDownList.DataBind();
                    RequestTimeSlot_toDropDownList.Items.Insert(0, new ListItem("--Select--", ""));

                    // Load time slots.
                    LoadTimeSlots();

                    // Highlight monthly slots.
                    HighlightMonthlySlots(currentDate.Month, currentDate.Year);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the submit button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will submit the time slots against the interview session.
        /// </remarks>
        protected void RequestTimeSlot_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if time slots are selected.
                if (Session["REQUEST_TIME_SLOT_LIST"] == null)
                {
                    base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, "No time slots are selected to save");
                    return;
                }

                // Save the time slots.
                new AssessorBLManager().SaveAssessorTimeSlots(null,null,Session["REQUEST_TIME_SLOT_LIST"] as List<AssessorTimeSlotDetail>);

                // Close the window.
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "Closeform", "<script language='javascript'>CloseRequestTimeSlotWindow();</script>", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset link button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void RequestTimeSlot_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                RequestTimeSlot_topSuccessMessageLabel.Text = string.Empty;
                RequestTimeSlot_topErrorMessageLabel.Text = string.Empty;

                // Set default button.
                Page.Form.DefaultButton = RequestTimeSlot_selectButton.UniqueID;

                // Set current date as selected date.
                RequestTimeSlot_selectDateCalendar.SelectedDates.Clear();

                // Get current date and time.
                DateTime currentDate = DateTime.Now;
                RequestTimeSlot_selectDateCalendar.SelectedDates.Add(currentDate);

                // Keep the date in session.
                Session["REQUEST_TIME_SLOT_DATE"] = currentDate;

                RequestTimeSlot_selectedDateLabel.Text = currentDate.ToString("dddd, dd MMMM yyyy");

                // Load time slots.
                LoadTimeSlots();

                // Highlight monthly slots.
                HighlightMonthlySlots(currentDate.Month, currentDate.Year);

                // Clear selected time slots.
                Session["REQUEST_TIME_SLOT_LIST"] = null;

                // Assign emtpy data source to selected time slots.
                RequestTimeSlot_selectedTimeSlotsGridView.DataSource = null;
                RequestTimeSlot_selectedTimeSlotsGridView.DataBind();

                // Clear input fields.
                RequestTimeSlot_fromDropDownList.SelectedIndex = 0;
                RequestTimeSlot_toDropDownList.SelectedIndex = 0;
                RequestTimeSlot_commentsTextBox.Text = string.Empty;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the calender control is 
        /// prerendered.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void RequestTimeSlot_selectDateCalendar_PreRender(object sender, EventArgs e)
        {
            try
            {
                // Clear selected dates.
                RequestTimeSlot_selectDateCalendar.SelectedDates.Clear();

                if (Utility.IsNullOrEmpty(Session["REQUEST_TIME_SLOT_DATE"]))
                    return;

                // Highlight the selected date.
                RequestTimeSlot_selectDateCalendar.SelectedDates.Add
                    ((DateTime)Session["REQUEST_TIME_SLOT_DATE"]);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when a date in the calender control
        /// is selected.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        protected void RequestTimeSlot_selectDateCalendar_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                // Keep the date in session.
                Session["REQUEST_TIME_SLOT_DATE"] = RequestTimeSlot_selectDateCalendar.SelectedDate;

                RequestTimeSlot_selectedDateLabel.Text = RequestTimeSlot_selectDateCalendar.
                    SelectedDate.ToString("dddd, dd MMMM yyyy");

                // Reload time slots.
                LoadTimeSlots();

                // Get the current date.
                DateTime currentDate = (DateTime)Session["REQUEST_TIME_SLOT_DATE"];

                // Set the visible date.
                if (Session["REQUEST_TIME_SLOT_DATE"] != null)
                {
                    RequestTimeSlot_selectDateCalendar.VisibleDate =
                        (DateTime)Session["REQUEST_TIME_SLOT_DATE"];
                }

                // Load time slots.
                LoadTimeSlots();

                // Highlight monthly slots.
                HighlightMonthlySlots(currentDate.Month, currentDate.Year);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the previous date button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will loads the time slots for the previous date.
        /// </remarks>
        protected void RequestTimeSlot_previousDate_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                // Check if selected date is available.
                if (Session["REQUEST_TIME_SLOT_DATE"] == null)
                {
                    base.ShowMessage(RequestTimeSlot_topErrorMessageLabel,
                        "No selected date is present");
                    return;
                }

                DateTime actualDate = (DateTime)Session["REQUEST_TIME_SLOT_DATE"];

                // Check if date is min value.
                if (IsMinDate(actualDate))
                {
                    base.ShowMessage(RequestTimeSlot_topErrorMessageLabel,
                        "Cannot process dates lesser than this");
                    return;
                }

                // Construct new date.
                DateTime newDate = ((DateTime)Session["REQUEST_TIME_SLOT_DATE"]).AddDays(-1);

                // Keep the date in session.
                Session["REQUEST_TIME_SLOT_DATE"] = newDate;

                RequestTimeSlot_selectedDateLabel.Text = newDate.ToString("dddd, dd MMMM yyyy");

                // Reload time slots.
                LoadTimeSlots();

                // Highlight monthly slots.
                HighlightMonthlySlots(newDate.Month, newDate.Year);

                // Set the visible date.
                if (Session["REQUEST_TIME_SLOT_DATE"] != null)
                {
                    RequestTimeSlot_selectDateCalendar.VisibleDate =
                        (DateTime)Session["REQUEST_TIME_SLOT_DATE"];
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the next date button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will loads the time slots for the next date.
        /// </remarks>
        protected void RequestTimeSlot_nextDate_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                // Check if selected date is available.
                if (Session["REQUEST_TIME_SLOT_DATE"] == null)
                {
                    base.ShowMessage(RequestTimeSlot_topErrorMessageLabel,
                        "No selected date is present");
                    return;
                }

                DateTime actualDate = (DateTime)Session["REQUEST_TIME_SLOT_DATE"];

                // Check if date is max value.
                if (IsMaxDate(actualDate))
                {
                    base.ShowMessage(RequestTimeSlot_topErrorMessageLabel,
                        "Cannot process dates greater than this");
                    return;
                }

                // Construct new date.
                DateTime newDate = ((DateTime)Session["REQUEST_TIME_SLOT_DATE"]).AddDays(1);

                // Keep the date in session.
                Session["REQUEST_TIME_SLOT_DATE"] = newDate;

                RequestTimeSlot_selectedDateLabel.Text = newDate.ToString("dddd, dd MMMM yyyy");

                // Reload time slots.
                LoadTimeSlots();

                // Highlight monthly slots.
                HighlightMonthlySlots(newDate.Month, newDate.Year);

                // Set the visible date.
                if (Session["REQUEST_TIME_SLOT_DATE"] != null)
                {
                    RequestTimeSlot_selectDateCalendar.VisibleDate =
                        (DateTime)Session["REQUEST_TIME_SLOT_DATE"];
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the visible month is changed in
        /// the calendar control.
        /// </summary>
        /// <param name="e">
        /// A <see cref="MonthChangedEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will loads the time slots for the next date.
        /// </remarks>
        protected void RequestTimeSlot_selectDateCalendar_VisibleMonthChanged
            (object sender, MonthChangedEventArgs e)
        {
            try
            {
                // Highlight monthly slots.
                HighlightMonthlySlots(e.NewDate.Month, e.NewDate.Year);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the day rendering is happen in 
        /// the calendar control.
        /// </summary>
        /// <param name="e">
        /// A <see cref="DayRenderEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will highlights the non availability dates.
        /// </remarks>
        protected void RequestTimeSlot_selectDateCalendar_DayRender(object sender, DayRenderEventArgs e)
        {
            try
            {
                // Check if the date is present in non availability dates.
                if (base.IsDatePresent(e.Day.Date,
                    Session["REQUEST_TIME_SLOT_NON_AVAILABLE_DATES"] as List<DateTime>))
                {
                    e.Cell.BackColor = Color.FromArgb(250, 148, 118);
                    e.Cell.ForeColor = Color.FromArgb(0, 0, 0);
                }

                // Check if the date is present in the scheduled dates.
                if (base.IsDatePresent(e.Day.Date,
                    Session["REQUEST_TIME_SLOT_SCHEDULED_DATES"] as List<DateTime>))
                {
                    e.Cell.BackColor = Color.FromArgb(141, 222, 153);
                    e.Cell.ForeColor = Color.FromArgb(0, 0, 0);
                }

                // Check if the date is selected date.
                if (Session["REQUEST_TIME_SLOT_DATE"] != null)
                {
                    if (base.IsDatesEqual(e.Day.Date, (DateTime)Session["REQUEST_TIME_SLOT_DATE"]))
                    {
                        e.Cell.BackColor = Color.FromArgb(89, 193, 238);
                        e.Cell.ForeColor = Color.FromArgb(0, 0, 0);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void RequestTimeSlot_timeSlotsGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void RequestTimeSlot_timeSlotsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    LinkButton RequestTimeSlot_timeSlotsGridView_skillLinkButton =
                       (LinkButton)e.Row.FindControl("RequestTimeSlot_timeSlotsGridView_skillLinkButton");

                    HtmlContainerControl RequestTimeSlot_timeSlotsGridView_detailsDiv =
                        (HtmlContainerControl)e.Row.FindControl("RequestTimeSlot_timeSlotsGridView_detailsDiv");

                    HtmlAnchor RequestTimeSlot_timeSlotsGridView_detailsViewFocusDownLink =
                        (HtmlAnchor)e.Row.FindControl("RequestTimeSlot_timeSlotsGridView_detailsViewFocusDownLink");

                    RequestTimeSlot_timeSlotsGridView_skillLinkButton.Attributes.Add("onclick", "return TestHideDetails('" +
                        RequestTimeSlot_timeSlotsGridView_detailsDiv.ClientID + "','" + RequestTimeSlot_timeSlotsGridView_detailsViewFocusDownLink.ClientID + "')");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void RequestTimeSlot_selectedTimeSlotsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "DeleteTimeSlot")
                    return;

                int index = Convert.ToInt32(e.CommandArgument);

                // Check if selected time slot in session is null.
                if (Session["REQUEST_TIME_SLOT_LIST"] == null)
                {
                    base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, "No time slot found to delete");
                    return;
                }

                // Get selected time slots from session.
                List<AssessorTimeSlotDetail> timeSlots = Session["REQUEST_TIME_SLOT_LIST"] as 
                    List<AssessorTimeSlotDetail>;

                // Check if the delete index is present.
                if (index >= timeSlots.Count)
                {
                    base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, "No time slot found to delete");
                    return;
                }

                // Delete the time slot from the list.
                timeSlots.RemoveAt(index);

                // Keep the time slots in session.
                Session["REQUEST_TIME_SLOT_LIST"] = timeSlots;

                // Assign data source.
                RequestTimeSlot_selectedTimeSlotsGridView.DataSource = timeSlots;
                RequestTimeSlot_selectedTimeSlotsGridView.DataBind();

                // Show success message.
                base.ShowMessage(RequestTimeSlot_topSuccessMessageLabel, "Time slot deleted successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void RequestTimeSlot_selectedTimeSlotsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the from time slot dropdown
        /// list item is selected.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        /// <remarks>
        /// This will select the next possible (30 mins) advance time slot in 
        /// the 'to' time slot.
        /// </remarks>
        protected void RequestTimeSlot_fromDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Check if a valid time slot is selected or not.
                if (Utility.IsNullOrEmpty(RequestTimeSlot_fromDropDownList.SelectedValue))
                    return;

                int selectedID = Convert.ToInt32(RequestTimeSlot_fromDropDownList.SelectedValue);
                // Increment the ID to point to the next time slot.
                selectedID++;

                // Set the selected ID to the 'to' time slot.
                if (RequestTimeSlot_toDropDownList.Items.FindByValue(selectedID.ToString()) != null)
                    RequestTimeSlot_toDropDownList.SelectedValue = selectedID.ToString();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the to time slot dropdown
        /// list item is selected.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        /// <remarks>
        /// This will set the focus to the comments field.
        /// </remarks>
        protected void RequestTimeSlot_toDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Set the focus to the comments text box.
                RequestTimeSlot_commentsTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the add time slot link 
        /// button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will add the selected time slot to the selected list.
        /// </remarks>
        protected void RequestTimeSlot_addToRequestedTimeSlotsButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if from & to is selected or not.
                if (!IsValidData())
                    return;

                // Create a new time slot object.
                AssessorTimeSlotDetail timeSlot = new AssessorTimeSlotDetail();
                timeSlot.AssessorID = Convert.ToInt32(RequestTimeSlot_assessorIDHiddenField.Value);
                timeSlot.AvailabilityDate = (DateTime)Session["REQUEST_TIME_SLOT_DATE"];
                timeSlot.TimeSlotIDFrom = Convert.ToInt32(RequestTimeSlot_fromDropDownList.SelectedValue);
                timeSlot.TimeSlotIDTo = Convert.ToInt32(RequestTimeSlot_toDropDownList.SelectedValue);
                timeSlot.TimeSlot = RequestTimeSlot_fromDropDownList.SelectedItem.Text + " - " + RequestTimeSlot_toDropDownList.SelectedItem.Text;
                timeSlot.Comments = RequestTimeSlot_commentsTextBox.Text.Trim();

                if (!Utility.IsNullOrEmpty(RequestTimeSlot_sessionKeyHiddenField.Value))
                {
                    timeSlot.SessionKey = RequestTimeSlot_sessionKeyHiddenField.Value;
                    timeSlot.InitiatedBy = base.userID;
                    timeSlot.SkillID = Convert.ToInt32(RequestTimeSlot_skillIDHiddenField.Value);

                    // Assign request status to accepted.
                    timeSlot.RequestStatus = Constants.TimeSlotRequestStatus.PENDING_CODE;
                }

                List<AssessorTimeSlotDetail> timeSlots = null;

                if (Session["REQUEST_TIME_SLOT_LIST"] == null)
                    timeSlots = new List<AssessorTimeSlotDetail>();
                else
                    timeSlots = Session["REQUEST_TIME_SLOT_LIST"] as List<AssessorTimeSlotDetail>;

                // Add the time slot to the list.
                timeSlots.Add(timeSlot);

                // Keep the time slots in session.
                Session["REQUEST_TIME_SLOT_LIST"] = timeSlots;

                // Assign data source.
                RequestTimeSlot_selectedTimeSlotsGridView.DataSource = timeSlots;
                RequestTimeSlot_selectedTimeSlotsGridView.DataBind();

                // Show success message.
                base.ShowMessage(RequestTimeSlot_topSuccessMessageLabel, "Time slot added successfully");

                // Clear input fields.
                RequestTimeSlot_fromDropDownList.SelectedIndex = 0;
                RequestTimeSlot_toDropDownList.SelectedIndex = 0;
                RequestTimeSlot_commentsTextBox.Text = string.Empty;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValid = true;

            // Check if 'from' time slot is selected or not.
            bool fromSelected = false;
            if (!Utility.IsNullOrEmpty(RequestTimeSlot_fromDropDownList.SelectedValue))
            {
                fromSelected = true;
            }

            // Check if 'to' time slot is selected or not.
            bool toSelected = false;
            if (!Utility.IsNullOrEmpty(RequestTimeSlot_toDropDownList.SelectedValue))
            {
                toSelected = true;
            }

            if (fromSelected == false && toSelected == false)
            {
                isValid = false;
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, "The 'from' and 'to' time slots cannot be empty");
            }
            else if (fromSelected == false)
            {
                isValid = false;
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, "The 'from' time slot cannot be empty");
            }
            else if (toSelected == false)
            {
                isValid = false;
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, "The 'to' time slot cannot be empty");
            }
           
            // Check if 'to' time slot is greater than 'from' time slot.
            if (fromSelected && toSelected)
            {
                int from = Convert.ToInt32(RequestTimeSlot_fromDropDownList.SelectedValue);
                int to = Convert.ToInt32(RequestTimeSlot_toDropDownList.SelectedValue);

                if (from >= to)
                {
                    isValid = false;
                    base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, "The 'to' time slot must be greater than 'from' time slot");
                }
            }
            return isValid;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the time slots for the selected date.
        /// </summary>
        private void LoadTimeSlots()
        {
            if (Session["REQUEST_TIME_SLOT_DATE"] == null)
                return;

            // Clear data.
            RequestTimeSlot_timeSlotsGridView.DataSource = null;
            RequestTimeSlot_timeSlotsGridView.DataBind();

            // Assign default values to summary.
            RequestTimeSlot_summaryDateValueLabel.Text = ((DateTime)Session["REQUEST_TIME_SLOT_DATE"]).ToString("MM/dd/yyyy");
            RequestTimeSlot_totalScheduledTimeSlotsValueLabel.Text = "0";
            RequestTimeSlot_totalScheduledHoursValueLabel.Text = "00:00";

            // Load assessor detail.
            AssessorDetail assesorDetail = new AssessorBLManager().GetAssessorByUserId
                (Convert.ToInt32(RequestTimeSlot_assessorIDHiddenField.Value));

            if (assesorDetail != null && !Utility.IsNullOrEmpty(assesorDetail.NonAvailabilityDates))
            {
                List<DateTime> nonAvailabilityDates = assesorDetail.NonAvailabilityDates.Split
                    (',').Select(n => Convert.ToDateTime(n)).ToList();

                // Keep non availability dates in session.
                Session["REQUEST_TIME_SLOT_NON_AVAILABLE_DATES"] = nonAvailabilityDates;
            }

            // Load assessor time slots.
            List<AssessorTimeSlotDetail> timeSlots = new AssessorBLManager().
                GetAssessorTimeSlots(Convert.ToInt32(RequestTimeSlot_assessorIDHiddenField.Value), (DateTime)Session["REQUEST_TIME_SLOT_DATE"]);

            if (timeSlots == null || timeSlots.Count == 0)
            {
                return;
            }

            RequestTimeSlot_timeSlotsGridView.DataSource = timeSlots;
            RequestTimeSlot_timeSlotsGridView.DataBind();

            // Assign summary.
            RequestTimeSlot_totalScheduledTimeSlotsValueLabel.Text = timeSlots.Count.ToString();
            RequestTimeSlot_totalScheduledHoursValueLabel.Text = GetTotalScheduledHours(timeSlots);
        }

        /// <summary>
        /// Method that calculates and returns the total hours scheduled from
        /// the list of time slots given for a selected date.
        /// </summary>
        /// <param name="timeSlots">
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the time
        /// slots.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the total scheduled hours in a 
        /// time format.
        /// </returns>
        private string GetTotalScheduledHours(List<AssessorTimeSlotDetail> timeSlots)
        {
            string hours = "00:00";

            int minutes = 0;
            foreach (AssessorTimeSlotDetail timeSlot in timeSlots)
            {
                minutes += ((timeSlot.TimeSlotIDTo - timeSlot.TimeSlotIDFrom) * 30);
            }

            if (minutes == 1440)
            {
                // For 24 hrs the system will return 00:00. So manually set
                // the value 24:00.
                hours = "24:00";
            }
            else
            {
                TimeSpan timeSpan = TimeSpan.FromMinutes(minutes);
                hours = string.Format("{0:D2}:{1:D2}",
                    timeSpan.Hours, timeSpan.Minutes);
            }

            return hours;
        }

        /// <summary>
        /// Method that highlights the monthly slot dates.
        /// </summary>
        /// <param name="month">
        /// A <see cref="int"/> that holds the month.
        /// </param>
        /// <param name="year">
        /// A <see cref="int"/> that holds the year.
        /// </param>
        /// <remarks>
        /// This method will be called whenever a month is changed in the 
        /// calendar control.
        /// </remarks>
        private void HighlightMonthlySlots(int month, int year)
        {
            // Clear all selected dates.
            RequestTimeSlot_selectDateCalendar.SelectedDates.Clear();

            // Get slots for current, previous and next months and keep in the session.
            Session["REQUEST_TIME_SLOT_SCHEDULED_DATES"] = new AssessorBLManager().
                GetAssessorTimeSlotDates(
                Convert.ToInt32(RequestTimeSlot_assessorIDHiddenField.Value), month, year, true);
        }

        /// <summary>
        /// Method that retrieves the query string values and assign it to 
        /// hidden fields.
        /// </summary>
        private bool RetrieveQueryStringValues()
        {
            // Check if assessor ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["assessorid"]))
            {
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, "Assessor ID is not passed");
                return false;
            }

            // Check if assessor ID is valid.
            int assessorID = 0;
            int.TryParse(Request.QueryString["assessorid"], out assessorID);

            if (assessorID == 0)
            {
                base.ShowMessage(RequestTimeSlot_topErrorMessageLabel, "Invalid assessor ID");
                return false;
            }

            // Assign assessor ID.
            RequestTimeSlot_assessorIDHiddenField.Value = assessorID.ToString();

            // Check if reference key is passed.
            if (!Utility.IsNullOrEmpty(Request.QueryString["sessionkey"]))
            {
                // Assign session key.
                RequestTimeSlot_sessionKeyHiddenField.Value = Request.QueryString["sessionkey"];
            }

            // Check if skill ID is passed.
            if (!Utility.IsNullOrEmpty(Request.QueryString["skillid"]))
            {
                int skillID = 0;
                int.TryParse(Request.QueryString["skillid"], out skillID);

                // Assign skill ID.
                RequestTimeSlot_skillIDHiddenField.Value = skillID.ToString();
            }

            return true;
        }

        #endregion Private Methods
    }
}