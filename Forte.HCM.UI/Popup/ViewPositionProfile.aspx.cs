﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewPositionProfile.cs
// File that represents the user interface layout and functionalities for
// the ViewPositionProfile page. This will helps to view the position 
// profile details such as name, registered by, registration date, 
// submittal deadline and the list of segments.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ViewPositionProfile page. This will helps to view the position 
    /// profile details such as name, registered by, registration date, 
    /// submittal deadline and the list of segments. This class inherits 
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ViewPositionProfile : PageBase
    {
        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.SetFocus(ViewPositionProfile_bottomCloseLinkButton.ClientID);
                
                // Set browser title.
                Master.SetPageCaption("Position Profile Detail");

                if (!IsPostBack)
                {
                    // Load position profile detail.
                    LoadPositionProfileDetail();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewPositionProfile_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the position profile detail.
        /// </summary>
        public void LoadPositionProfileDetail()
        {
            // Check if client ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
            {
                base.ShowMessage(ViewPositionProfile_errorMessageLabel, "Position profile ID is not passed");
                return;
            }

            // Check if position profile ID is valid.
            int positionProfileID = 0;
            int.TryParse(Request.QueryString["positionprofileid"].ToString(), out positionProfileID);

            if (positionProfileID == 0)
            {
                base.ShowMessage(ViewPositionProfile_errorMessageLabel, "Invalid position profile ID");
                return;
            }

            // Get client detail.
            PositionProfileDetail detail = new PositionProfileBLManager().GetPositionProfile(positionProfileID);

            if (detail == null)
            {
                base.ShowMessage(ViewPositionProfile_errorMessageLabel, "No position profile detail found to display");
                return;
            }

            // Show details.
            ViewPositionProfile_positionProfileNameLabel.Text = detail.PositionName;
            ViewPositionProfile_registeredByLabel.Text = detail.RegisteredByName;
            ViewPositionProfile_dateOfRegistrationLabel.Text = detail.RegistrationDate.ToString("MM/dd/yy");
            ViewPositionProfile_submittalDeadlineLabel.Text = detail.SubmittalDate.ToString("MM/dd/yy");
            ViewPositionProfile_clientNameLabel.Text = detail.ClientName;

            // Assign URL for view more position profile details.
            ViewPositionProfile_moreHyperLink.NavigateUrl = "../PositionProfile/PositionProfileReview.aspx" +
                "?m=1&s=0&positionprofileid=" + positionProfileID + "&parentpage=S_POS_PRO";

            // Assign segments list.
            List<SegmentDetail> segmentDetailList = new List<SegmentDetail>();
            segmentDetailList = detail.Segments;
            ViewState["SEGMENT_DETAILS"] = segmentDetailList;
            Control segmentControl;
            for (int i = 0; i < segmentDetailList.Count; i++)
            {
                switch (segmentDetailList[i].SegmentID)
                {
                    case (int)SegmentType.ClientPositionDetail:
                        segmentControl = null;
                        segmentControl = LoadControl("../" + segmentDetailList[i].segmentPath);
                        ((Segments.ClientPositionDetailsControl)segmentControl).PreviewDataSource = segmentDetailList[i];
                        ViewPositionProfile_segmentsPanel.Controls.Add(segmentControl);
                        ViewPositionProfile_segmentsPanel.Controls.Add(new LiteralControl("<br/>"));
                        break;
                    case (int)SegmentType.Education:
                        segmentControl = null;
                        segmentControl = LoadControl("../" + segmentDetailList[i].segmentPath);
                        ((Segments.EducationRequirementControl)segmentControl).PreviewDataSource = segmentDetailList[i];
                        ViewPositionProfile_segmentsPanel.Controls.Add(segmentControl);
                        ViewPositionProfile_segmentsPanel.Controls.Add(new LiteralControl("<br/>"));

                        break;
                    case (int)SegmentType.Roles:
                        segmentControl = null;
                        segmentControl = LoadControl("../" + segmentDetailList[i].segmentPath);
                        ((Segments.RoleRequirement)segmentControl).PreviewDataSource = segmentDetailList[i];
                        ViewPositionProfile_segmentsPanel.Controls.Add(segmentControl);
                        ViewPositionProfile_segmentsPanel.Controls.Add(new LiteralControl("<br/>"));
                        break;
                    case (int)SegmentType.TechnicalSkills:
                        segmentControl = null;
                        segmentControl = LoadControl("../" + segmentDetailList[i].segmentPath);
                        ((Segments.TechnicalSkillRequirement)segmentControl).PreviewDataSource = segmentDetailList[i];
                        ViewPositionProfile_segmentsPanel.Controls.Add(segmentControl);
                        ViewPositionProfile_segmentsPanel.Controls.Add(new LiteralControl("<br/>"));
                        break;
                    case (int)SegmentType.Verticals:
                        segmentControl = null;
                        segmentControl = LoadControl("../" + segmentDetailList[i].segmentPath);
                        ((Segments.VerticalBackgroundRequirement)segmentControl).PreviewDataSource = segmentDetailList[i];
                        ((Segments.VerticalBackgroundRequirement)segmentControl).DisableControls();
                        ViewPositionProfile_segmentsPanel.Controls.Add(segmentControl);
                        ViewPositionProfile_segmentsPanel.Controls.Add(new LiteralControl("<br/>"));
                        break;
                }
            }
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Protected Overridden Methods
    }
}
