﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AddNotes.aspx.cs
// File that represents the user interface layout and functionalities for
// the AddNotes page. This will helps to add notes against a candidate.

#endregion Header

#region Directives

using System;
using System.Web.UI;


using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the AddNotes page. This will helps to add notes against a candidate.
    /// This class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class AddNotes : PageBase
    {
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Add Notes");
                if (!IsPostBack)
                {
                    // Set default button and focus.
                    Page.Form.DefaultButton = AddNotesControl_addButton.UniqueID;
                    AddNotes_notesTextBox.Focus();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AddNotes_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the add button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void AddNotes_addButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                AddNotes_topSuccessMessageLabel.Text = string.Empty;
                AddNotes_topErrorMessageLabel.Text = string.Empty;

                if (!IsValidData())
                    return;

                // Check if candidate ID is passed.
                if (Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
                {
                    ShowMessage(AddNotes_topErrorMessageLabel, "Candidate ID is not passed");
                    return;
                }

                // Get candidate ID profile ID.
                int candidateID = 0;
                if (int.TryParse(Request.QueryString["candidateid"].ToString().Trim(), out candidateID) == false)
                {
                    ShowMessage(AddNotes_topErrorMessageLabel, "Invalid candidate ID");
                    return;
                }

                // Get position profile ID.
                int positionProfileID = 0;
                if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                {
                    int.TryParse(Request.QueryString["positionprofileid"].ToString().Trim(), out positionProfileID);
                }

                // Insert candidate activity log.
                int activityLogID = new CommonBLManager().InsertCandidateActivityLog
                    (candidateID, 0, AddNotes_notesTextBox.Text.Trim(), positionProfileID, 
                    base.userID, Constants.CandidateActivityLogType.NOTES_ADDED);

                AddNotes_topSuccessMessageLabel.Text = "Notes added successfully";
 
                try
                {
                    // Send mail to the candidate owner.
                    new EmailHandler().SendMail(EntityType.NotesAdded, activityLogID);

                    //Close the popup window
                    string closeScript = "self.close();";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true); 
                }                                                     
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    AddNotes_topErrorMessageLabel.Text = "Send mail to recruiter failed";
                }
               
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AddNotes_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset link button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void AddNotes_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear notes and messages.
                AddNotes_notesTextBox.Text = string.Empty;
                AddNotes_topSuccessMessageLabel.Text = string.Empty;
                AddNotes_topErrorMessageLabel.Text = string.Empty;

                // Set default button and focus.
                Page.Form.DefaultButton = AddNotesControl_addButton.UniqueID;
                AddNotes_notesTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AddNotes_topErrorMessageLabel, exp.Message);
            }
        }

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            // Check if notes is entered or not.
            if (AddNotes_notesTextBox.Text.Trim().Length == 0)
            {
                ShowMessage(AddNotes_topErrorMessageLabel, "Notes cannot be empty");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}