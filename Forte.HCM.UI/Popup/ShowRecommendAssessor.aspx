<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowRecommendAssessor.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.ShowRecommendAssessor" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/InterviewQuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc4" %>
     <%@ Register Src="../CommonControls/CorporateUserSignUp.ascx" TagName="CorporateUserSignUp"
    TagPrefix="uc5" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="SearchClientRequest_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">
        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl) {
            var nameCtrl = '<%= Request.QueryString["namectrl"] %>';
            var emailCtrl = '<%= Request.QueryString["emailctrl"] %>';
            var idCtrl = '<%= Request.QueryString["idctrl"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Set the user name field value. Replace the 'link button name' with the 
            // 'user name hidden field name' and retrieve the value.
            if (nameCtrl != null && nameCtrl != '') {
                window.opener.document.getElementById(nameCtrl).value
                    = document.getElementById(ctrl.id.replace("ShowRecommendAssessor_selectLinkButton", "ShowRecommendAssessor_userNameHiddenfield")).value;
            }

            // Set the email hidden field value. Replace the 'link button name' with the 
            // 'eamil hidden field name' and retrieve the value.
            if (emailCtrl != null && emailCtrl != '') {
                window.opener.document.getElementById(emailCtrl).value
                    = document.getElementById(ctrl.id.replace("ShowRecommendAssessor_selectLinkButton", "ShowRecommendAssessor_emailHiddenfield")).value;
            }

            // Set the user ID hidden field value. Replace the 'link button name' with the 
            // 'user ID hidden field name' and retrieve the value.
            if (idCtrl != null && idCtrl != '') {
                window.opener.document.getElementById(idCtrl).value
                    = document.getElementById(ctrl.id.replace("ShowRecommendAssessor_selectLinkButton", "ShowRecommendAssessor_userIDHiddenfield")).value;
            }
            self.close();
        }


        

        // Handler method that will be called when the 'save' button is 
        // clicked in the the show recommend page.
        function OnSaveClick()
        {
            var btncnrl = '<%= Request.QueryString["ctrlId"] %>';

            if (window.dialogArguments) 
            {
                window.opener = window.dialogArguments;
            }

            // Trigger the click event of the refresh button in the parent page.
            if (btncnrl != null && window.opener.document.getElementById(btncnrl) != null)
                window.opener.document.getElementById(btncnrl).click();

            self.close();
        }
    </script>
    <asp:UpdatePanel ID="ShowRecommendAssessor_pageUpdatePanel" runat="server">
        <ContentTemplate>
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td class="popup_td_padding_10">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 100%" class="popup_header_text_grey" align="left">
                                    <asp:Literal ID="ShowRecommendAssessor_searchAssessor" runat="server"></asp:Literal>
                                </td>
                                <td style="width: 100%" align="right">
                                    <asp:ImageButton ID="ShowRecommendAssessor_topCancelImagebutton" runat="server" SkinID="sknCloseImageButton"
                                        OnClientClick="javascript:CloseMe()" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_2">
                    </td>
                </tr>
                <tr>
                    <td class="popup_td_padding_10" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                            <tr>
                                <td class="popup_td_padding_10">
                                    <div id="ShowRecommendAssessor_searchCriteriasDiv" runat="server" style="display: block;">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="ShowRecommendAssessor_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                    <asp:Label ID="ShowRecommendAssessor_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                    <asp:HiddenField ID="ShowRecommendAssessor_pageNumberHiddenField" runat="server"
                                                        Value="1" />
                                                    <asp:HiddenField runat="server" ID="ShowRecommendAssessor_isMaximizedHiddenField" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div id="ShowRecommendAssessor_searchCriteriDiv" runat="server" style="display: block;">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td width="100%">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" 
                                                                    id="ShowRecommendAssessor_ShowSearchCriteriaTable">
                                                                        <tr>
                                                                            <td align="left">
                                                                                <asp:Label ID="ShowRecommendAssessor_skillsHeadLabel" runat="server" Text="Skills/Areas"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                &nbsp;&nbsp;
                                                                                <asp:Label ID="ShowRecommendAssessor_skillLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                            </td>
                                                                            <td align="left">
                                                                                <%--<asp:TextBox ID="ShowRecommendAssessor_skillsTextBox" runat="server" MaxLength="100"
                                                                                    Columns="100"></asp:TextBox>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5" colspan="2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            <div style="float: left; width: 380px;" class="grouping_border_bg">
                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:CheckBox ID="ShowRecommendAssessor_interviewedPastCheckBox" runat="server" Text="Display only assessors that have interviewed for this client in the past">
                                                                                                            </asp:CheckBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:CheckBox ID="ShowRecommendAssessor_matchesAllAreasCheckBox" runat="server" Text="Display only assessors that can assess all areas">
                                                                                                            </asp:CheckBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>&nbsp;</td>
                                                                                        <td>
                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                <tr>
                                                                                                    <td valign="top">
                                                                                                        <div style="float: left; width: 263px;" class="grouping_border_bg">
                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                <tr>
                                                                                                                    <td colspan="3" align="left">
                                                                                                                        <asp:RadioButton ID="ShowRecommendAssessor_showAllRadioButton" runat="server" Text="Show All"
                                                                                                                            GroupName="Show" Checked="true" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td align="left">
                                                                                                                        <asp:RadioButton ID="ShowRecommendAssessor_showTopRadioButton" runat="server" GroupName="Show"
                                                                                                                            Text="Show Top" />
                                                                                                                    </td>
                                                                                                                    <td align="right">
                                                                                                                        <asp:TextBox ID="ShowRecommendAssessor_topMatchesTextBox" MaxLength="2" runat="server"
                                                                                                                            Width="30px" TabIndex="10"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td align="left">
                                                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                                                            <tr>
                                                                                                                                <td style="vertical-align: bottom">
                                                                                                                                    <asp:ImageButton ID="ShowRecommendAssessor_upImageButton" runat="server" ImageAlign="AbsBottom"
                                                                                                                                        SkinID="sknNumericUpArrowImage" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="vertical-align: top">
                                                                                                                                    <asp:ImageButton ID="ShowRecommendAssessor_downImageButton" runat="server" ImageAlign="Top"
                                                                                                                                        SkinID="sknNumericDownArrowImage" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="ShowRecommendAssessor_matchesLabel" runat="server" Text="Matches"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <ajaxToolKit:NumericUpDownExtender ID="ShowRecommendAssessor_NumericUpDownExtender"
                                                                                                                    Width="60" runat="server" Minimum="0" Maximum="30" Step="1" TargetControlID="ShowRecommendAssessor_topMatchesTextBox"
                                                                                                                    TargetButtonUpID="ShowRecommendAssessor_upImageButton" TargetButtonDownID="ShowRecommendAssessor_downImageButton">
                                                                                                                </ajaxToolKit:NumericUpDownExtender>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="td_height_5" colspan="2">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <div style="float: left; width: 263px;" class="grouping_border_bg">
                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="ShowRecommendAssessor_showPerPageLabel" runat="server" Text="Shows"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td align="right">
                                                                                                                        <asp:TextBox ID="ShowRecommendAssessor_showPerPageTextBox" MaxLength="2" Text="2"
                                                                                                                            runat="server" Width="30px" TabIndex="10"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td align="left">
                                                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                                                            <tr>
                                                                                                                                <td style="vertical-align: bottom">
                                                                                                                                    <asp:ImageButton ID="ShowRecommendAssessor_upImageButtonPerPage" runat="server" ImageAlign="AbsBottom"
                                                                                                                                        SkinID="sknNumericUpArrowImage" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="vertical-align: top">
                                                                                                                                    <asp:ImageButton ID="ShowRecommendAssessor_downImageButtonPerPage" runat="server"
                                                                                                                                        ImageAlign="Top" SkinID="sknNumericDownArrowImage" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="ShowRecommendAssessor_assessorPerPageLabel" runat="server" Text="Assessors Per Page"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <ajaxToolKit:NumericUpDownExtender ID="ShowRecommendAssessor_NumericUpDownExtenderPerPage"
                                                                                                                        Width="60" runat="server" Minimum="0" Maximum="30" Step="1" TargetControlID="ShowRecommendAssessor_showPerPageTextBox"
                                                                                                                        TargetButtonUpID="ShowRecommendAssessor_upImageButtonPerPage" TargetButtonDownID="ShowRecommendAssessor_downImageButtonPerPage">
                                                                                                                    </ajaxToolKit:NumericUpDownExtender>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5" colspan="2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <div style="float: left; width: 655px;" class="grouping_border_bg">
                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="ShowRecommendAssessor_sortOrderHeadLabel" runat="server" Text="Sort Order"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="ShowRecommendAssessor_sortOrderDropDownList" runat="server">
                                                                                                    <asp:ListItem Text="Descending" Value="DESC"></asp:ListItem>
                                                                                                    <asp:ListItem Text="Ascending" Value="ASC"></asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="ShowRecommendAssessor_sortByHeadLabel" runat="server" Text="Sort By"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="ShowRecommendAssessor_sortByDropDownList" runat="server">
                                                                                                    <asp:ListItem Text="Assessors who can assess most areas" Value="CAN_ASSESS_MOST_AREAS"></asp:ListItem>
                                                                                                    <asp:ListItem Text="Assessors that have interviewed for this client in the past"
                                                                                                        Value="INTERVIEWED_PAST"></asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" class="td_padding_top_5">
                                                                                <table cellpadding="2" cellspacing="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                              <asp:LinkButton ID="ShowRecommendAssessor_addNewUserLinkButton" runat="server"
                                                                                                Text="Add New User" SkinID="sknAddLinkButton" 
                                                                                                  onclick="ShowRecommendAssessor_addNewUserLinkButton_Click" ></asp:LinkButton>

                                                                                                  <div id="ShowRecommendAssessor_addNewUser_hiddenDiv" style="display: none;">
                                                                                                        <asp:Button ID="ShowRecommendAssessor_addNewUserHiddenButton" runat="server" />
                                                                                                    </div>
                                                                                                    <ajaxToolKit:ModalPopupExtender ID="ShowRecommendAssessor_addNewUserModalPopupExtender"
                                                                                                        runat="server" PopupControlID="ShowRecommendAssessor_addNewUserPanel"
                                                                                                        TargetControlID="ShowRecommendAssessor_addNewUserHiddenButton" BackgroundCssClass="modalBackground">
                                                                                                    </ajaxToolKit:ModalPopupExtender>
                                                                                                    <asp:Panel ID="ShowRecommendAssessor_addNewUserPanel" runat="server" Style="display: none"
                                                                                                        Height="400px" CssClass="popupcontrol_subscription_types">
                                                                                                        <uc5:CorporateUserSignUp ID="ShowRecommendAssessor_CorporateUserSignUp" runat="server" />
                                                                                                    </asp:Panel>
                                                                                        </td>
                                                                                        <td class="td_height_8">&nbsp;</td>
                                                                                        <td>
                                                                                            <asp:LinkButton ID="ShowRecommendAssessor_addAssessorLinkButton" runat="server" Text="Add Assessor"
                                                                                                SkinID="sknAddLinkButton" ToolTip="Click here to search and add assessors"></asp:LinkButton>
                                                                                            <div style="display: none">
                                                                                                <asp:Button ID="ShowRecommendAssessor_refreshGridButton" runat="server" Text="Refresh"
                                                                                                    SkinID="sknButtonId" OnClick="ShowRecommendAssessor_refreshGridButton_Click" />
                                                                                                <asp:TextBox runat="server" ID="ShowRecommendAssessor_assessorNameTextBox"></asp:TextBox>
                                                                                            </div>
                                                                                            <asp:HiddenField runat="server" ID="ShowRecommendAssessor_addedUserIdHiddenField" />
                                                                                        </td>
                                                                                        <td class="td_height_8">&nbsp;</td>
                                                                                        <td>
                                                                                            <asp:Button ID="ShowRecommendAssessor_topSearchButton" runat="server" Text="Search"
                                                                                                SkinID="sknButtonId" OnClick="ShowRecommendAssessor_topSearchButton_Click" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_8">
                                                                </td>
                                                            </tr>
                                                            <tr id="ShowRecommendAssessor_ShowAssesserResultsHeaderTR" runat="server">
                                                                <td class="header_bg">
                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                        <tr>
                                                                            <td style="width: 50%" align="left" class="header_text_bold">
                                                                                <asp:Literal ID="ShowRecommendAssessor_searchResultsLiteral" runat="server" Text="Search Results">
                                                                                </asp:Literal>
                                                                            </td>
                                                                            <td style="width: 48%" align="left">
                                                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:HiddenField ID="ShowRecommendAssessor_stateExpandHiddenField" runat="server"
                                                                                                Value="0" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td style="width: 2%" align="right">
                                                                                <span id="ShowRecommendAssessor_searchResultsUpSpan" runat="server" style="display: none;">
                                                                                    <asp:Image ID="ShowRecommendAssessor_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                                                        id="ShowRecommendAssessor_searchResultsDownSpan" runat="server" style="display: none;"><asp:Image
                                                                                            ID="ShowRecommendAssessor_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                                                <asp:HiddenField ID="ShowRecommendAssessor_restoreHiddenField" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="ShowRecommendAssessor_ShowAssesserResultsTR" runat="server">
                                                                <td class="grid_body_bg">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <asp:ImageButton ID="ShowRecommendAssessor_previousImageButton" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/nav_btn_prev.gif"
                                                                                                CommandName="Previous" OnClick="OnNavigatePage" ToolTip="Previous" Width="20px"
                                                                                                Height="20px" SkinID="sknPreviousDateImage" />
                                                                                        </td>
                                                                                        <td align="right">
                                                                                            <asp:ImageButton ID="ShowRecommendAssessor_nextImageButton" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/nav_btn_next.gif"
                                                                                                CommandName="Next" OnClick="OnNavigatePage" ToolTip="Next" Width="20px" Height="20px"
                                                                                                SkinID="sknNextDateImage" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                
                                                                                
                                                                                <div style="overflow: auto;" runat="server" id="ShowRecommendAssessor_assessorDiv">
                                                                                    <asp:GridView ID="ShowRecommendAssessor_assesserDetailsGridView" SkinID="sknRecommendAssessorGrid"
                                                                                        AllowSorting="false" runat="server" AutoGenerateColumns="true" OnRowCommand="ShowRecommendAssessor_assesserDetailsGridView_RowCommand"
                                                                                        OnRowDataBound="ShowRecommendAssessor_assesserDetailsGridView_RowDataBound" OnRowDeleting="ShowRecommendAssessor_assesserDetailsGridView_RowDeleting">
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right">
                                                                                <asp:Button ID="ShowRecommendAssessor_addNewAssessorSetButton" runat="server" 
                                                                                    Text="Save" SkinID="sknButtonId" OnClick="ShowRecommendAssessor_addNewAssessorSetButton_Click" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table cellpadding="3" cellspacing="3">
                            <tr>
                                <td>
                                    <asp:Button ID="ShowRecommendAssessor_addAssessorButton" runat="server" Text="Select"
                                        SkinID="sknButtonId" OnClick="ShowRecommendAssessor_addAssessorButton_Click" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="ShowAssesserLookup_resetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                        Text="Reset" OnClick="ShowAssesserLookup_resetLinkButton_Click"></asp:LinkButton>
                                </td>
                                <td class="pop_divider_line">
                                    |
                                </td>
                                <td>
                                    <asp:LinkButton ID="ShowRecommendAssessor_addAssessorCancel" runat="server" SkinID="sknPopupLinkButton"
                                        Text="Cancel" OnClientClick="javascript:window.close()"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
