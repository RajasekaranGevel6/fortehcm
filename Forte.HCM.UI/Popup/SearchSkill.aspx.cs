﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchCategory.aspx.cs
// File that represents the user interface layout and functionalities
// for the SearchCategory page. This page helps in searching for 
// categories by providing search criteria for category ID and category
// name. This class inherits the Forte.HCM.UI.Common.PageBase class.

#endregion

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the SearchCategory page. This page helps in searching for 
    /// categories by providing search criteria for category ID and category 
    /// name. This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class SearchSkill : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "224px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "274px";

        #endregion Private Constants

        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button and focus.
                Page.Form.DefaultButton = SearchSkill_searchButton.UniqueID;

                // Set browser title.
                Master.SetPageCaption("Search Skill");

                // Subscribes to the page number click event.
                SearchSkill_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchSkill_pageNavigator_PageNumberClick);

                // 
                if (!Utility.IsNullOrEmpty(SearchSkill_isMaximizedHiddenField.Value) &&
                    SearchSkill_isMaximizedHiddenField.Value == "Y")
                {
                    SearchSkill_searchCriteriasDiv.Style["display"] = "none";
                    SearchSkill_searchResultsUpSpan.Style["display"] = "block";
                    SearchSkill_searchResultsDownSpan.Style["display"] = "none";
                    SearchSkill_searchResultsDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchSkill_searchCriteriasDiv.Style["display"] = "block";
                    SearchSkill_searchResultsUpSpan.Style["display"] = "none";
                    SearchSkill_searchResultsDownSpan.Style["display"] = "block";
                    SearchSkill_searchResultsDiv.Style["height"] = RESTORED_HEIGHT;
                }

                if (!IsPostBack)
                {
                    // Set default focus.
                    Page.Form.DefaultFocus = SearchSkill_skillTextBox.UniqueID;
                    SearchSkill_skillTextBox.Focus();

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "SKILL_NAME";

                    SearchSkill_searchResultsDiv.Visible = false;
                }
                else
                {
                    SearchSkill_searchResultsDiv.Visible = true;
                }

                SearchSkill_searchTestResultsTR.Attributes.Add("onclick", 
                    "ExpandOrRestore('" +
                    SearchSkill_searchResultsDiv.ClientID + "','" +
                    SearchSkill_searchCriteriasDiv.ClientID + "','" +
                    SearchSkill_searchResultsUpSpan.ClientID + "','" +
                    SearchSkill_searchResultsDownSpan.ClientID + "','" +
                    SearchSkill_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" + 
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchSkill_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchSkill_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                SearchSkill_errorMessageLabel.Text = string.Empty;
                SearchSkill_successMessageLabel.Text = string.Empty;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "SKILL_NAME";

                // Reset the paging control.
                SearchSkill_pageNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchSkill_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void SearchSkill_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchSkill_skillTextBox.Text = string.Empty;

                SearchSkill_searchResultsGridView.DataSource = null;
                SearchSkill_searchResultsGridView.DataBind();

                SearchSkill_pageNavigator.PageSize = base.GridPageSize;
                SearchSkill_pageNavigator.TotalRecords = 0;
                SearchSkill_searchResultsDiv.Visible = false;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "CATEGORYID";

                // Reset to the restored state.
                SearchSkill_searchCriteriasDiv.Style["display"] = "block";
                SearchSkill_searchResultsUpSpan.Style["display"] = "none";
                SearchSkill_searchResultsDownSpan.Style["display"] = "block";
                SearchSkill_searchResultsDiv.Style["height"] = RESTORED_HEIGHT;
                SearchSkill_isMaximizedHiddenField.Value = "N";

                // Clear message fields.
                SearchSkill_successMessageLabel.Text = string.Empty;
                SearchSkill_errorMessageLabel.Text = string.Empty;

                // Set default focus.
                Page.Form.DefaultFocus = SearchSkill_skillTextBox.UniqueID;
                SearchSkill_skillTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchSkill_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchSkill_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                Search(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchSkill_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchSkill_searchResultsGridView_Sorting(object sender, 
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                SearchSkill_pageNavigator.Reset();
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchSkill_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchSkill_searchResultsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchSkill_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchSkill_searchResultsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchSkill_searchResultsGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchSkill_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods                                                

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void Search(int pageNumber)
        {
            int totalRecords = 0;

            // Get the skills.
            List<SkillDetail> skills = new AssessorBLManager().GetSkills(
                SearchSkill_skillTextBox.Text.Trim(), pageNumber, 
                base.GridPageSize, ViewState["SORT_FIELD"].ToString(), 
                (SortType)ViewState["SORT_ORDER"],
                out totalRecords);

            if (skills == null || skills.Count == 0)
            {
                SearchSkill_searchResultsDiv.Visible = false;
                ShowMessage(SearchSkill_errorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                SearchSkill_searchResultsDiv.Visible = true;
            }

            SearchSkill_searchResultsGridView.DataSource = skills;
            SearchSkill_searchResultsGridView.DataBind();
            SearchSkill_pageNavigator.PageSize = base.GridPageSize;
            SearchSkill_pageNavigator.TotalRecords = totalRecords;
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}

