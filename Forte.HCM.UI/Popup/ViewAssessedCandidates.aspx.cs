﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewAssessedCandidates.cs
// File that represents the user interface layout and functionalities for
// the ViewAssessedCandidates page. This will helps to view the assessed
// candidates against position profile and session key.

#endregion Header                                                              

#region Directives                                                             

using System;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;
using System.Web.UI.WebControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ViewAssessedCandidates page. This will helps to view the assessed
    /// candidates against position profile and session key. This class inherits
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ViewAssessedCandidates : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="int"/> that holds the position profile ID.
        /// </summary>
        private int positionProfileID = 0;

        /// <summary>
        /// A <see cref="string"/> that holds the interview session key.
        /// </summary>
        private string interviewSessionKey = null;

        #endregion Private Variables

        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.SetFocus(ViewAssessedCandidates_bottomCloseLinkButton.ClientID);
                
                // Set browser title.
                Master.SetPageCaption("View Assessed Candidates");

                // Set default focus.
                ViewAssessedCandidates_topCloseImageButton.Focus();

                // Retrieve query string values.
                RetriveQueryStringValues();

                if (!IsPostBack)
                {
                    // Load values.
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewAssessedCandidates_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void ViewAssessedCandidates_candidatesGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    HiddenField ViewAssessedCandidates_candidatesGridView_interviewTestKeyHiddenField = (HiddenField)e.Row.FindControl
                        ("ViewAssessedCandidates_candidatesGridView_interviewTestKeyHiddenField");

                    HiddenField ViewAssessedCandidates_candidatesGridView_candidateInterviewSessionKeyHiddenField = (HiddenField)e.Row.FindControl
                        ("ViewAssessedCandidates_candidatesGridView_candidateInterviewSessionKeyHiddenField");
                    
                    HiddenField ViewAssessedCandidates_candidatesGridView_attemptIDHiddenField = (HiddenField)e.Row.FindControl
                       ("ViewAssessedCandidates_candidatesGridView_attemptIDHiddenField");

                    HiddenField ViewAssessedCandidates_candidatesGridView_interviewSessionKeyHiddenField = (HiddenField)e.Row.FindControl
                       ("ViewAssessedCandidates_candidatesGridView_interviewSessionKeyHiddenField");
                    
                    HyperLink ViewAssessedCandidates_candidatesGridView_candidateNameHyperLink = (HyperLink)e.Row.FindControl
                        ("ViewAssessedCandidates_candidatesGridView_candidateNameHyperLink");

                    HyperLink ViewAssessedCandidates_candidatesGridView_sessionKeyHyperLink = (HyperLink)e.Row.FindControl
                       ("ViewAssessedCandidates_candidatesGridView_sessionKeyHyperLink");

                    HyperLink ViewAssessedCandidates_candidatesGridView_interviewNameHyperLink = (HyperLink)e.Row.FindControl
                       ("ViewAssessedCandidates_candidatesGridView_interviewNameHyperLink");

                    // Assign navigate URL to candidate rating summary.
                    ViewAssessedCandidates_candidatesGridView_candidateNameHyperLink.NavigateUrl =
                        "~/Assessments/CandidateAssessorSummary.aspx?m=4&s=0" +
                        "&testkey=" + ViewAssessedCandidates_candidatesGridView_interviewTestKeyHiddenField.Value +
                        "&candidatesessionid=" + ViewAssessedCandidates_candidatesGridView_candidateInterviewSessionKeyHiddenField.Value +
                        "&attemptid=" + ViewAssessedCandidates_candidatesGridView_attemptIDHiddenField.Value +
                        "&positionprofileid=" + Request.QueryString["positionprofileid"] +
                        "&parentpage=PP_REVIEW";

                    // Assign navigate URL to view interview sessions.
                    ViewAssessedCandidates_candidatesGridView_sessionKeyHyperLink.NavigateUrl =
                        "~/InterviewScheduler/InterviewScheduleCandidate.aspx?m=2&s=1&index=3" +
                        "&interviewsessionid=" + ViewAssessedCandidates_candidatesGridView_interviewSessionKeyHiddenField.Value +
                        "&positionprofileid=" + Request.QueryString["positionprofileid"] +
                        "&parentpage=PP_REVIEW";

                    // Assign navigate URL to view interview.
                    ViewAssessedCandidates_candidatesGridView_interviewNameHyperLink.NavigateUrl =
                        "~/InterviewTestMaker/ViewInterviewTest.aspx?m=2&s=1" +
                        "&testkey=" + ViewAssessedCandidates_candidatesGridView_interviewTestKeyHiddenField.Value +
                        "&positionprofileid=" + Request.QueryString["positionprofileid"] +
                        "&parentpage=PP_REVIEW";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewAssessedCandidates_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the show all checkbox state 
        /// is changed.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will show all candidates when checked and show only for the 
        /// interview session key when unchecked.
        /// </remarks>
        protected void ViewAssessedCandidates_showRadioButtonList_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                // Load assessed candidates.
                LoadAssessedCandidates();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewAssessedCandidates_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that retrieve and assign the query string values to the 
        /// local variables. 
        /// </summary>
        private void RetriveQueryStringValues()
        {
            // Check if position profile ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
            {
                base.ShowMessage(ViewAssessedCandidates_errorMessageLabel, "Position profile ID is not passed");
                return;
            }

            // Check if position profile ID is valid.
            int.TryParse(Request.QueryString["positionprofileid"].ToString(), out positionProfileID);

            if (positionProfileID == 0)
            {
                base.ShowMessage(ViewAssessedCandidates_errorMessageLabel, "Invalid position profile ID");
                return;
            }

            // Check if session key is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["interviewsessionkey"]))
            {
                base.ShowMessage(ViewAssessedCandidates_errorMessageLabel, "Interview session key is not passed");
                return;
            }

            interviewSessionKey = Request.QueryString["interviewsessionkey"].Trim();
        }

        /// <summary>
        /// Method that loads the assessed candidates.
        /// </summary>
        private void LoadAssessedCandidates()
        {
            // Assign assessed candidates list to the grid.
            ViewAssessedCandidates_candidatesGridView.DataSource = new PositionProfileBLManager().
                GetAssessedCandidates(positionProfileID, ViewAssessedCandidates_showAllRadioButton.Checked, interviewSessionKey);

            ViewAssessedCandidates_candidatesGridView.DataBind();
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Load position profile detail.
            PositionProfileDetail positionProfileDetail = new PositionProfileBLManager().
                GetPositionProfile(positionProfileID);

            if (positionProfileDetail != null)
            {
                ViewAssessedCandidates_positionProfileNameValueLabel.Text = positionProfileDetail.PositionName;
                ViewAssessedCandidates_clientNameValueLabel.Text = positionProfileDetail.ClientName;

                // Assign interview session key & navigate URL.
                ViewAssessedCandidates_interviewSessionIDValueHyperLink.Text = interviewSessionKey;

                ViewAssessedCandidates_interviewSessionIDValueHyperLink.NavigateUrl =
                    "~/InterviewScheduler/InterviewScheduleCandidate.aspx?m=2&s=1&index=3" +
                    "&interviewsessionid=" + interviewSessionKey + 
                    "&positionprofileid=" + positionProfileID +
                    "&parentpage=PP_REVIEW";
            }

            // Load assessed candidates.
            LoadAssessedCandidates();
        }

        #endregion Protected Overridden Methods
    }   
}
