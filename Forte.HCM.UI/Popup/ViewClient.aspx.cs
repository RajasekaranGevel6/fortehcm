﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewClient.cs
// File that represents the user interface layout and functionalities for
// the ViewClient page. This will helps to view the client details such as
// name, address, website, list of departments, etc. This class inherits 
// Forte.HCM.UI.Common.PageBase.

#endregion Header                                                              

#region Directives                                                             

using System;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ViewClient page. This will helps to view the client details such as
    /// name, address, website, lis of departments, etc. This class inherits 
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ViewClient : PageBase
    {
        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.SetFocus(ViewClient_bottomCloseLinkButton.ClientID);
                
                // Set browser title.
                Master.SetPageCaption("Client Detail");

                if (!IsPostBack)
                {
                    // Load client detail.
                    LoadClientDetail();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewClient_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the client detail.
        /// </summary>
        public void LoadClientDetail()
        {
            // Check if client ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["clientid"]))
            {
                base.ShowMessage(ViewClient_errorMessageLabel, "Client ID is not passed");
                return;
            }

            // Check if client ID is valid.
            int clientID = 0;
            int.TryParse(Request.QueryString["clientid"].ToString(), out clientID);

            if (clientID == 0)
            {
                base.ShowMessage(ViewClient_errorMessageLabel, "Invalid client ID");
                return;
            }

            // Get client detail.
            ClientInformation clientInfo = new ClientBLManager().GetClientInformationByClientId(clientID);

            if (clientInfo == null)
            {
                base.ShowMessage(ViewClient_errorMessageLabel, "No client detail found to display");
                return;
            }

            // Show details.
            ViewClient_clientNameLabel.Text = clientInfo.ClientName;

            // Assign URL for view more client details.
            ViewClient_moreHyperLink.NavigateUrl = "../ClientCenter/ClientManagement.aspx" +
                "?m=0&s=0&parentpage=MENU&viewtype=C";

            // Keep the client name is session. When navigated to client management page by 
            // clicking on 'more...' this client name will be filled into the search criteria,
            // thus during search this client will be loaded.
            Session["CLIENT_MANAGEMENT_CLIENT_NAME"] = clientInfo.ClientName;
            
            if (!Utility.IsNullOrEmpty(clientInfo.ContactInformation.EmailAddress))
            {
                ViewClient_emailIDHyperLink.Text = clientInfo.ContactInformation.EmailAddress;
                ViewClient_emailIDHyperLink.NavigateUrl = string.Format("mailto:{0}", clientInfo.ContactInformation.EmailAddress);
            }

            ViewClient_phoneNoLabel.Text = clientInfo.ContactInformation.Phone.Mobile;
            ViewClient_faxNoLabel.Text = clientInfo.FaxNumber;
            ViewClient_zipLabel.Text = clientInfo.ContactInformation.PostalCode;
            ViewClient_streetAddressLabel.Text = clientInfo.ContactInformation.StreetAddress;
            ViewClient_cityLabel.Text = clientInfo.ContactInformation.City;
            ViewClient_statelabel.Text = clientInfo.ContactInformation.State;
            ViewClient_countryLabel.Text = clientInfo.ContactInformation.Country;
            ViewClient_feinNoLabel.Text = clientInfo.FeinNo;
            ViewClient_websiteURLHyperLink.Text = clientInfo.ContactInformation.WebSiteAddress.Personal;

            if (!Utility.IsNullOrEmpty(clientInfo.ContactInformation.WebSiteAddress.Personal))
            {
                string navigateURL = clientInfo.ContactInformation.WebSiteAddress.Personal;

                if (!navigateURL.ToUpper().Contains("HTTP"))
                    navigateURL = "http://" + navigateURL;

                ViewClient_websiteURLHyperLink.NavigateUrl = navigateURL;
            }

            ViewClient_additionalInfoLabel.Text = clientInfo.AdditionalInfo;
            ViewClient_departmentsLabel.Text = clientInfo.ClientDepartments;
            ViewClient_contactsLabel.Text = clientInfo.ClientContacts;
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Protected Overridden Methods
    }
}
