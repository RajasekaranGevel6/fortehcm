﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddTimeSlot.aspx.cs" MasterPageFile="~/MasterPages/PopupMaster.Master"
    Inherits="Forte.HCM.UI.Popup.AddTimeSlot" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="AddTimeSlot_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        function CloseAddTimeSlotWindow() {
            var btncnrl = '<%= Request.QueryString["btncnrl"] %>';
            var ctrlhiddenid = '<%= Request.QueryString["ctrlhiddenid"] %>';
            var mode = '<%= Request.QueryString["mode"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Trigger the click event of the refresh button in the parent page.
            if (ctrlhiddenid != null && window.opener.document.getElementById(ctrlhiddenid) != null) {
                window.opener.document.getElementById(ctrlhiddenid).value = mode;
            }

            if (btncnrl != null && window.opener.document.getElementById(btncnrl) != null) {
                window.opener.document.getElementById(btncnrl).click();
            }
            self.close();
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_5">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="AddTimeSlot_titleLiteral" runat="server" Text="Select Available Time"></asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="AddTimeSlot_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_5" style="width: 100%">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_add_time_slot_bg">
                    <tr>
                        <td style="width: 100%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="height: 20px">
                                        <asp:UpdatePanel ID="AddTimeSlot_messageUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="AddTimeSlot_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknErrorMessage" Width="100%" Text=""></asp:Label>
                                                <asp:Label ID="AddTimeSlot_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknSuccessMessage" Width="100%" Text=""></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="AddTimeSlot_saveButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 100%; padding-left: 10px; padding-right: 10px;">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 30%" align="left">
                                                    <asp:Label ID="AddTimeSlot_dateLabel" runat="server" Text="Date" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    <span class="mandatory">*</span>
                                                </td>
                                                <td style="width: 70%" align="left">
                                                    <div id="AddTimeSlot_displayRequestedDateDiv" runat="server" style="float: left; padding-right: 5px;display:block;">
                                                        <asp:Label ID="AddTimeSlot_requestedDateLabel" runat="server" SkinID="sknLabelFieldText"
                                                            Width="160px"></asp:Label>
                                                    </div>
                                                    <div id="AddTimeSlot_displayProposeDateDiv" runat="server" style="display:block;">
                                                        <div style="float: left; padding-right: 5px;">
                                                            <asp:TextBox ID="AddTimeSlot_requestedDateTextBox" runat="server" Width="160px"></asp:TextBox>
                                                        </div>
                                                        <div style="float: left">
                                                            <asp:ImageButton ID="AddTimeSlot_requestedDateImageButton" SkinID="sknCalendarImageButton"
                                                                runat="server" ImageAlign="Middle" />
                                                        </div>
                                                        <ajaxToolKit:MaskedEditExtender ID="AddTimeSlot_requestedDateMaskedEditExtender"
                                                            runat="server" TargetControlID="AddTimeSlot_requestedDateTextBox" Mask="99/99/9999"
                                                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                            MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                        <ajaxToolKit:MaskedEditValidator ID="AddTimeSlot_requestedDateMaskedEditValidator"
                                                            runat="server" ControlExtender="AddTimeSlot_requestedDateMaskedEditExtender"
                                                            ControlToValidate="AddTimeSlot_requestedDateTextBox" EmptyValueMessage="Date is required"
                                                            InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                        <ajaxToolKit:CalendarExtender ID="AddTimeSlot_requestedDateCustomCalendarExtender"
                                                            runat="server" TargetControlID="AddTimeSlot_requestedDateTextBox" CssClass="MyCalendar"
                                                            Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="AddTimeSlot_requestedDateImageButton" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 30%" align="left">
                                                    <asp:Label ID="AddTimeSlot_requesterLabel" runat="server" Text="Requester" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 70%" align="left">
                                                    <asp:Label ID="AddTimeSlot_requesterTextBox" runat="server" SkinID="sknLabelFieldText"
                                                        Width="86%"></asp:Label>
                                                    <asp:HiddenField ID="AddTimeSlot_requesterHiddenField" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 30%" align="left">
                                                    <asp:Label ID="AddTimeSlot_availableFullTimeLabel" runat="server" Text="Available Any Time"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 70%" align="left">
                                                    <asp:UpdatePanel ID="AddTimeSlot_availableFullTimeUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <asp:CheckBox ID="AddTimeSlot_availableFullTimeCheckBox" runat="server" OnCheckedChanged="AddTimeSlot_availableFullTimeCheckBox_CheckedChanged"
                                                                AutoPostBack="true" Checked="true" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="popup_td_padding_2" colspan="2">
                                                    <asp:UpdatePanel ID="AddTimeSlot_displayAvailableTimeUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <div id="AddTimeSlot_displayAvailableTimeDiv" runat="server">
                                                                <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="header_bg" align="center">
                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                <tr>
                                                                                    <td align="left" class="header_text_bold">
                                                                                        <asp:Literal ID="AddTimeSlot_addTimeSlotLiteral" runat="server" Text="Select Available Time"></asp:Literal>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" class="request_time_slot_grid_body_bg">
                                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <asp:CheckBoxList ID="AddTimeSlot_availableTimeCheckBoxList" Width="100%" RepeatColumns="2"
                                                                                            runat="server" Enabled="false">
                                                                                        </asp:CheckBoxList>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="AddTimeSlot_availableFullTimeCheckBox" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_10_no_left">
                <asp:UpdatePanel ID="AddTimeSlot_buttonControlsUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="middle" align="left">
                                    <asp:Button ID="AddTimeSlot_saveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                        OnClick="AddTimeSlot_saveButton_Click" />
                                </td>
                                <td valign="middle" align="left">
                                    &nbsp;<asp:LinkButton ID="AddTimeSlot_cancelLinkButton" runat="server" Text="Cancel"
                                        SkinID="sknPopupLinkButton" OnClientClick="javascript:CloseMe();"> </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10">
            </td>
        </tr>
    </table>
</asp:Content>
