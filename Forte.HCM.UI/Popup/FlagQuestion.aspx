﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FlagQuestion.aspx.cs" MasterPageFile="~/MasterPages/PopupMaster.Master"
    Inherits="Forte.HCM.UI.Popup.FlagQuestion" ValidateRequest="false"  %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="FlagQuestion_content" ContentPlaceHolderID="PopupMaster_contentPlaceHolder"
    runat="server">

    <script type="text/javascript" language="javascript">
    // This function helps to close the window
        function CloseMe() {
            self.close();
        }
        
    </script>

    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <asp:UpdatePanel ID="FlagQuestion_updatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="popup_td_padding_10">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="width: 50%" class="popup_header_text_grey" align="left">
                                                <asp:Literal ID="FlagQuestion_headerLiteral" runat="server" Text="Flag Question"></asp:Literal>
                                            </td>
                                            <td style="width: 50%" align="right">
                                                <asp:ImageButton ID="FlagQuestion_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                                    OnClientClick="javascript:CloseMe()" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_td_padding_10" valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_question_inner_bg">
                                        <tr>
                                            <td class="popup_td_padding_10">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td class="msg_align">
                                                            <asp:Label ID="FlagQuestion_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                            <asp:Label ID="FlagQuestion_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            <asp:HiddenField ID="FlagQuestion_toHiddenField" runat="server" />
                                                            <asp:HiddenField ID="FlagQuestion_fromHiddenField" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td style="width: 15%">
                                                                        <asp:Label ID="FlagQuestion_fromLabel" runat="server" Text="From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 85%">
                                                                        <asp:Label ID="FlagQuestion_fromValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="FlagQuestion_toLabel" runat="server" Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="FlagQuestion_toValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="FlagQuestion_ccLabel" runat="server" Text="CC" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="padding: 3px;">
                                                                        <asp:TextBox ID="FlagQuestion_ccTextBox" runat="server" Width="99%" TextMode="MultiLine"
                                                                            SkinID="sknMultiLineTextBox" Height="23" MaxLength="100" onkeyup="CommentsCount(100,this)"
                                                                            onchange="CommentsCount(100,this)" TabIndex="1"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="FlagQuestion_subjectLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="FlagQuestion_subjectValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td valign="top" align="left">
                                                                                    <asp:Label ID="FlagQuestion_reasonLabel" runat="server" Text="Reason" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    <span class="mandatory">*</span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_2">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="FlagQuestion_reasonTextBox" TextMode="MultiLine" Height="200" runat="server"
                                                                                        Width="99%" SkinID="sknMultiLineTextBox" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                                                        onchange="CommentsCount(500,this)" TabIndex="2">
                                                                                    </asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="popup_td_padding_5">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Button ID="FlagQuestion_bottomSubmitButton" runat="server" Text="Submit" OnClick="FlagQuestion_submitButton_Click"
                                                    SkinID="sknButtonId" TabIndex="3" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="FlagQuestion_bottomCancelButton" runat="server" Text="Cancel"
                                                    SkinID="sknPopupLinkButton" TabIndex="4"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
