<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="SearchLocation.aspx.cs"  Inherits="Forte.HCM.UI.Popup.SearchLocation" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ID="SearchCateory_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl) {
            var cityctrlId = '<%= Request.QueryString["cityID"] %>';
            var statectrlId = '<%= Request.QueryString["stateID"] %>';
            var countryctrlId = '<%= Request.QueryString["countryID"] %>';
            var locationctrlId = '<%= Request.QueryString["location"] %>';


            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }


            if (cityctrlId != null && cityctrlId != '') {
                window.opener.document.getElementById(cityctrlId).value
                    = document.getElementById(ctrl.id.replace("SearchLocation_selectButton", "SearchLocation_cityHiddenField")).value;
            }

            if (statectrlId != null && statectrlId != '') {
                window.opener.document.getElementById(statectrlId).value
                    = document.getElementById(ctrl.id.replace("SearchLocation_selectButton", "SearchLocation_stateHiddenField")).value;
            }

            if (countryctrlId != null && countryctrlId != '') {
                window.opener.document.getElementById(countryctrlId).value
                    = document.getElementById(ctrl.id.replace("SearchLocation_selectButton", "SearchLocation_countryHiddenField")).value;
            }
            if (locationctrlId != null && locationctrlId != '') {
                window.opener.document.getElementById(locationctrlId).value
                    = document.getElementById(ctrl.id.replace("SearchLocation_selectButton", "SearchLocation_locationHiddenField")).value;
            }

            self.close();
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchLocation_headerLiteral" runat="server" Text="Search Location"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SearchLocation_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:UpdatePanel ID="SearchLocation_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="SearchLocation_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="SearchLocation_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchLocation_searchButton" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchLocation_pageNavigator" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="SearchLocation_searchCriteriasDiv" runat="server" style="display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="SearchLocation_searchCriteriaUpdatePanel" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <table width="100%" border="0" cellspacing="3%" cellpadding="2%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="SearchLocation_cityLabel" runat="server" Text="City" 
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchLocation_cityTextBox" runat="server" MaxLength="20"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="SearchLocation_stateLabel" runat="server" Text="State" 
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchLocation_stateTextBox" runat="server" MaxLength="20"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="SearchLocation_countryLabel" runat="server" Text="Country" 
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchLocation_countryTextbox" runat="server" MaxLength="20"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="6">
                                                                            <asp:Button ID="SearchLocation_searchButton" runat="server" SkinID="sknButtonId"
                                                                                Text="Search" OnClick="SearchLocation_searchButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="SearchLocation_searchResultsUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr id="SearchLocation_searchTestResultsTR" runat="server">
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 50%">
                                                                        <asp:Literal ID="SearchLocation_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                            ID="SearchQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 50%" align="right">
                                                                        <span id="SearchLocation_searchResultsUpSpan" style="display: none;" runat="server">
                                                                            <asp:Image ID="SearchLocation_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                        </span><span id="SearchLocation_searchResultsDownSpan" style="display: block;" runat="server">
                                                                            <asp:Image ID="SearchLocation_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="height: 220px; overflow: auto;" runat="server" id="SearchLocation_searchResultsDiv">
                                                                            <asp:GridView ID="SearchLocation_searchResultsGridView" runat="server" AutoGenerateColumns="False"
                                                                                Width="100%" OnRowCreated="SearchLocation_searchResultsGridView_RowCreated" OnSorting="SearchLocation_searchResultsGridView_Sorting"
                                                                                AllowSorting="true" OnRowDataBound="SearchLocation_searchResultsGridView_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="left">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="SearchLocation_selectButton" runat="server" OnClientClick="javascript:return OnSelectClick(this);"
                                                                                                Text="Select" SkinID="sknActionLinkButton">
                                                                                            </asp:LinkButton>
                                                                                            <asp:HiddenField ID="SearchLocation_cityHiddenField" runat="server" Value='<%# Eval("caiCity") %>' />
                                                                                            <asp:HiddenField ID="SearchLocation_stateHiddenField" runat="server" Value='<%# Eval("staID") %>' />
                                                                                            <asp:HiddenField ID="SearchLocation_countryHiddenField" runat="server" Value='<%# Eval("couID") %>' />
                                                                                            <asp:HiddenField ID="SearchLocation_locationHiddenField" runat="server" Value='<%# Eval("caiLocation") %>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="City" DataField="caiCityName" SortExpression="CITY" />
                                                                                    <asp:BoundField HeaderText="State" DataField="caiStateName" SortExpression="STATE" />
                                                                                    <asp:BoundField HeaderText="Country" DataField="caiCountryName" SortExpression="COUNTRY" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <uc1:PageNavigator ID="SearchLocation_pageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchLocation_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_left_8">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="SearchLocation_topResetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="SearchLocation_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="SearchLocation_topCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="SearchLocation_isMaximizedHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
