﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="FeatureUsageDetails.aspx.cs" Inherits="Forte.HCM.UI.Popup.FeatureUsageDetails" %>    
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ID="FeatureUsageDetails_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="FeatureUsageDetails_titleLiteral" runat="server" Text="Usage Details"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="FeatureUsageDetails_closeImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td align="center">
                            <asp:Label ID="FeatureUsageDetails_errorMessageLabel" runat="server" SkinID="sknErrorMessage" /><asp:Label
                                ID="FeatureUsageDetails_successMessageLabel" runat="server" SkinID="sknSuccessMessage" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="FeatureUsage_DetailsDIV" runat="server"  style="height:300px; overflow: auto;"
                                visible="false">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="header_bg" align="center">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                        <asp:Literal ID="FeatureUsageDetailsy_HeaderLiteral" runat="server" Text="Usage Details"></asp:Literal>
                                                        <asp:Label ID="FeatureUsageDetails_HeaderHelpLabel" runat="server" SkinID="sknLabelText"
                                                            Text=" - Click column headers to sort">                                                
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="grid_body_bg">
                                            <asp:UpdatePanel ID="FeatureUsageDetails_UpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="FeatureUsageDetails_userFeaturesGridView" runat="server" AllowSorting="True"
                                                        AutoGenerateColumns="false" OnRowCreated="FeatureUsageDetails_userFeaturesGridView_RowCreated"
                                                        OnSorting="FeatureUsageDetails_userFeaturesGridView_Sorting">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Position Profile Name" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="NAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_GridView_PositionProfileNameLabel" runat="server"
                                                                        Text='<%# Eval("PositionProfileName") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_rolenAMEGridViewIDHiddenField" runat="server"
                                                                        Value='<%# Eval("PPID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Client Name" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="CLIENTNAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_GridView_ClientNameLabel" runat="server" Text='<%# Eval("ClientName") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_clientNameGridViewIDHiddenField" runat="server"
                                                                        Value='<%# Eval("PPID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Position Name" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="POSITIONNAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_GridView_PositionNameLabel" runat="server" Text='<%# Eval("PositionName") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_positionNameGridViewIDHiddenField" runat="server"
                                                                        Value='<%# Eval("PPID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Date" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="DATE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_GridView_CreatedOnLabel" runat="server" Text='<%# Eval("CreatedDate") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_CreatedOnGridViewIDHiddenField" runat="server"
                                                                        Value='<%# Eval("PPID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <asp:HiddenField ID="FeatureUsage_checkedStatusHiddenField" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="FeatureUsage_DetailsFormsDIV" runat="server" style=" height:300px;overflow: auto;"
                                visible="false">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="header_bg" align="center">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                        <asp:Literal ID="Literal2" runat="server" Text="Usage Details"></asp:Literal>
                                                        <asp:Label ID="Label2" runat="server" SkinID="sknLabelText" Text=" - Click column headers to sort">                                                
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="grid_body_bg">
                                            <asp:UpdatePanel ID="FeatureUsage_formsUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="FeatureUsage_formsUpdatePanelGridView" runat="server" AllowSorting="True"
                                                        OnSorting="FeatureUsageDetails_userFeaturesGridView_Sorting" OnRowCreated="FeatureUsage_formsUpdatePanelGridView_RowCreated"
                                                        AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Form Name" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="NAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_GridView_FormNameLabel" runat="server" Text='<%# Eval("FormName") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_formNameEGridViewIDHiddenField" runat="server"
                                                                        Value='<%# Eval("FID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Tag" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="FORMTAG">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_GridView_formTagLabel" runat="server" Text='<%# Eval("FormTag") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_formTagGridViewIDHiddenField" runat="server"
                                                                        Value='<%# Eval("FID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Segments" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="SEGMENT">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_GridView_segmentsLabel" runat="server" Text='<%# Eval("FormSegments") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_segmentsGridViewIDHiddenField" runat="server"
                                                                        Value='<%# Eval("FID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Date" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="DATE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_GridView_formsCreatedOnLabel" runat="server" Text='<%# Eval("CreatedDate") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_formsCreatedOnGridViewIDHiddenField" runat="server"
                                                                        Value='<%# Eval("FID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="FeatureUsage_DetailsTalentScoutDIV" runat="server" 
                                style="overflow: auto; height:300px;" visible="false">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="header_bg" align="center">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                        <asp:Literal ID="Literal1" runat="server" Text="Usage Details"></asp:Literal>
                                                        <asp:Label ID="Label1" runat="server" SkinID="sknLabelText" Text=" - Click column headers to sort">                                                
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="grid_body_bg">
                                            <asp:UpdatePanel ID="FeatureUsage_talentScoutUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="FeatureUsage_talentScoutGridView" runat="server" AllowSorting="True"
                                                        OnRowCreated="FeatureUsageDetails_userFeaturestalentScoutGridView_RowCreated"
                                                        OnSorting="FeatureUsageDetails_userFeaturesGridView_Sorting" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Search Terms" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="NAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_GridView_talentScoutNameLabel" runat="server"
                                                                        Text='<%# Eval("SearchTerms") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_talentScoutNameEGridViewIDHiddenField" runat="server"
                                                                        Value='<%# Eval("FID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                                                
                                                            <asp:TemplateField HeaderText="Total Candidates" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="TOTALCANDI">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_GridView_talentScoutLabel" runat="server" Text='<%# Eval("TotalCanditates") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_talentScoutTCGridViewIDHiddenField" runat="server"
                                                                        Value='<%# Eval("FID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField HeaderText="Position Profile Name" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="PPN">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_PPN_GridView_talentScoutLabel" runat="server" Text='<%# Eval("PositionProfileName") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_talentScoutTCPPNGridViewIDHiddenField" runat="server"
                                                                        Value='<%# Eval("FID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                                              
                                                            <asp:TemplateField HeaderText="Date" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="DATE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_GridView_talentScoutCreatedOnLabel" runat="server"
                                                                        Text='<%# Eval("CreatedDate") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_talentScoutCreatedOnGridViewIDHiddenField"
                                                                        runat="server" Value='<%# Eval("FID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <asp:HiddenField ID="HiddenField2" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="FeatureUsage_DetailsTalentScoutViewProfileImageDIV" runat="server" 
                                style="overflow: auto;height:300px;" visible="false">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="header_bg" align="center">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                        <asp:Literal ID="Literal3" runat="server" Text="Usage Details"></asp:Literal>
                                                        <asp:Label ID="Label3" runat="server" SkinID="sknLabelText" Text=" - Click column headers to sort">                                                
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="grid_body_bg">
                                            <asp:UpdatePanel ID="FeatureUsage_talentScoutViewProfileImageUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="FeatureUsage_talentScoutViewProfileImageGridView" runat="server"
                                                        AllowSorting="True" OnRowCreated="FeatureUsageDetails_userFeaturestalentScoutViewProfileImageGridView_RowCreated"
                                                        OnSorting="FeatureUsageDetails_userFeaturesGridView_Sorting" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Candidate Name" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="NAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_GridView_talentScoutViewProfileImageNameLabel"
                                                                        runat="server" Text='<%# Eval("CandidateName") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_talentScoutViewProfileImageNameEGridViewIDHiddenField"
                                                                        runat="server" Value='<%# Eval("FID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- <asp:TemplateField HeaderText="Search Terms" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="SEARCHTERMS">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_GridView_talentScoutViewProfileImageLabel" runat="server"
                                                                        Text='<%# Eval("SearchTerm") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_talentScoutViewProfileImageGridViewIDHiddenField"
                                                                        runat="server" Value='<%# Eval("FID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="Date" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="DATE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsageDetails_GridView_talentScoutViewProfileImageCreatedOnLabel"
                                                                        runat="server" Text='<%# Eval("CreatedDate") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsageDetails_talentScoutViewProfileImageCreatedOnGridViewIDHiddenField"
                                                                        runat="server" Value='<%# Eval("FID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <asp:HiddenField ID="HiddenField3" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div id="FeatureUsage_DetailsTalentscout_cloning_DIV" runat="server" 
                                style="overflow: auto;height:300px;" visible="false">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="header_bg" align="center">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                        <asp:Literal ID="Literal4" runat="server" Text="Usage Details"></asp:Literal>
                                                        <asp:Label ID="Label4" runat="server" SkinID="sknLabelText" Text=" - Click column headers to sort">                                                
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="grid_body_bg">
                                            <asp:UpdatePanel ID="FeatureUsage_DetailsTalentscout_cloning_UpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="FeatureUsage_DetailsTalentScout_cloning_Gridview" runat="server"
                                                        AllowSorting="True" OnRowCreated="FeatureUsage_DetailsTalentScout_cloning_Gridview_RowCreated"
                                                        OnSorting="FeatureUsageDetails_userFeaturesGridView_Sorting" AutoGenerateColumns="false">
                                                        <Columns>
                                                        <asp:TemplateField HeaderText="Candidate Name" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="NAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsage_DetailsTalentScout_cloning_candidateNameLabel" runat="server"
                                                                        Text='<%# Eval("CandidateName") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsage_DetailsTalentScout_cloning_candidateName_FID_HiddenField"
                                                                        runat="server" Value='<%# Eval("FID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Search Terms" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="SEARCH">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsage_DetailsTalentScout_cloning_searchTermLabel"
                                                                        runat="server" Text='<%# Eval("SearchTerm") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsage_DetailsTalentScout_cloning_searchTerm_FID_HiddenField"
                                                                        runat="server" Value='<%# Eval("FID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             
                                                            <asp:TemplateField HeaderText="Date" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="35%" SortExpression="DATE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="FeatureUsage_DetailsTalentScout_cloning_ClonedDateLabel"
                                                                        runat="server" Text='<%# Eval("CreatedDate") %>'>
                                                                    </asp:Label>
                                                                    <asp:HiddenField ID="FeatureUsage_DetailsTalentScout_cloning_clonedDate_FID_HiddenField"
                                                                        runat="server" Value='<%# Eval("FID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <asp:HiddenField ID="HiddenField4" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </td>
                    </tr>
                    <%--<tr>
                        <td>
                            <uc1:PageNavigator ID="FeatureUsageDetails_pageNavigator" runat="server" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="td_height_20">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_left_8">
                <asp:LinkButton ID="FeatureUsageDetails_cancelLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClientClick="javascript:window.close();" />
            </td>
        </tr>
    </table>
</asp:Content>
