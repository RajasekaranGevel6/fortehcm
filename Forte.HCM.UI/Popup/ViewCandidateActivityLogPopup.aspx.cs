﻿#region Header

// Copyright (C) Forte. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewCandidateActivityLogPopup.aspx.cs
// File that represents the ViewCandidateActivityLogPopup class that 
// defines the user interface layout and functionalities for the view 
// candidate activity log page. This page helps to view the list of 
// activity logs for a candidate.

#endregion Header

#region Directives

using System;
using System.Data;
using System.Web.UI;

using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

using OfficeOpenXml;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the ViewCandidateActivityLogPopup class that 
    /// defines the user interface layout and functionalities for the view 
    /// candidate activity log page. This page helps to view the list of 
    /// activity logs for a candidate. This class inherits 
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class ViewCandidateActivityLogPopup : PageBase
    {
        #region Events Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Candidate Activity Log");
                // Subscribe to message thrown events.
                ViewCandidateActivityLogPopup_candidateActivityViewerControl.ControlMessageThrown +=
                    new CandidateActivityViewerControl.ControlMessageThrownDelegate
                    (ViewCandidateActivityLogPopup_candidateActivityViewerControl_ControlMessageThrown);

                // Set page size.
                ViewCandidateActivityLogPopup_candidateActivityViewerControl.GridPageSize = base.GridPageSize;

                // Disallow change candidate.
                ViewCandidateActivityLogPopup_candidateActivityViewerControl.AllowChangeCandidate = false;

                if (!IsPostBack)
                {
                    // Check if candidate ID is passed.
                    if (!Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
                    {
                        int candidateID = 0;
                        if (int.TryParse(Request.QueryString["candidateid"].ToString().Trim(), out candidateID) == true)
                        {
                            ViewCandidateActivityLogPopup_candidateActivityViewerControl.CandidateID = candidateID;
                        }
                    }
                }

                // Check if post back is raised due after adding notes.
                if (ViewCandidateActivityLogPopup_refreshHiddenField.Value != null &&
                    ViewCandidateActivityLogPopup_refreshHiddenField.Value.Trim().ToUpper() == "Y")
                {
                    // Reset the flag value.
                    ViewCandidateActivityLogPopup_refreshHiddenField.Value = "N";

                    // Refresh activities.
                    RefreshActivities();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCandidateActivityLogPopup_errorMessageLabel, exp.Message);
            }
        }

        private void ViewCandidateActivityLogPopup_candidateActivityViewerControl_ControlMessageThrown
            (object sender, ControlMessageEventArgs c)
        {
            // Check if message is empty. If empty clear all messages.
            if (Utility.IsNullOrEmpty(c.Message))
            {
                ViewCandidateActivityLogPopup_errorMessageLabel.Text = string.Empty;
                ViewCandidateActivityLogPopup_successMessageLabel.Text = string.Empty;
                return;
            }

            // Show the message.
            if (c.MessageType == MessageType.Error)
            {
                base.ShowMessage(ViewCandidateActivityLogPopup_errorMessageLabel, c.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void ViewCandidateActivityLogPopup_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ViewCandidateActivityLogPopup_errorMessageLabel.Text = string.Empty;
                ViewCandidateActivityLogPopup_successMessageLabel.Text = string.Empty;

                // Reset the control.
                ViewCandidateActivityLogPopup_candidateActivityViewerControl.Reset();

                // Check if candidate ID is passed and reset it to the control.
                if (!Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
                {
                    int candidateID = 0;
                    if (int.TryParse(Request.QueryString["candidateid"].ToString().Trim(), out candidateID) == true)
                    {
                        ViewCandidateActivityLogPopup_candidateActivityViewerControl.CandidateID = candidateID;
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCandidateActivityLogPopup_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the add notes link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will show the add notes popup.
        /// </remarks>
        protected void ViewCandidateActivityLogPopup_addNotesLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ViewCandidateActivityLogPopup_errorMessageLabel.Text = string.Empty;
                ViewCandidateActivityLogPopup_successMessageLabel.Text = string.Empty;

                // Retrieve and check if candidate ID is present.
                int candidateID = ViewCandidateActivityLogPopup_candidateActivityViewerControl.CandidateID;

                if (candidateID == 0)
                {
                    ShowMessage(ViewCandidateActivityLogPopup_errorMessageLabel,
                        "No candidate was selected");

                    return;
                }

                // Add the handler to the add notes link.
                ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenAddNotesPopupWithRefreshForViewCandidateActivityLogPopup",
                    "javascript: OpenAddNotesPopupWithRefreshForViewCandidateActivityLogPopup('" + candidateID + "')", true);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCandidateActivityLogPopup_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the download link is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will export the activity log to an excel file.
        /// </remarks>
        protected void ViewCandidateActivityLogPopup_downloadLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ViewCandidateActivityLogPopup_errorMessageLabel.Text = string.Empty;
                ViewCandidateActivityLogPopup_successMessageLabel.Text = string.Empty;

                // Check if candidate ID is present.
                if (ViewCandidateActivityLogPopup_candidateActivityViewerControl.CandidateID == 0)
                {
                    ShowMessage(ViewCandidateActivityLogPopup_errorMessageLabel,
                        "No candidate was selected");

                    return;
                }

                // Get the table that contains the rows for download.
                DataTable table = ViewCandidateActivityLogPopup_candidateActivityViewerControl.
                    GetDownloadTable();

                // Check if table is not null and contain rows.
                if (table == null || table.Rows.Count == 0)
                {
                    ShowMessage(ViewCandidateActivityLogPopup_errorMessageLabel,
                        "No activities found to download");

                    return;
                }

                // Remove the row where 'total' columns is not null (which 
                // contains the total records used for paging).
                DataRow[] foundRows =
                    table.Select("[TOTAL] is not null", "", DataViewRowState.OriginalRows);

                // Check if rows are found with 'total column is not null'
                if (foundRows != null && foundRows.Length > 0)
                {
                    // Remove the 0th row.
                    table.Rows.Remove(foundRows[0]);
                }

                // Remove the unncessary columns from the table.
                table.Columns.Remove("ACTIVITY_BY");
                table.Columns.Remove("ACTIVITY_TYPE");
                table.Columns.Remove("TOTAL");

                // Change the column names.
                table.Columns["ACTIVITY_DATE"].ColumnName = "Activity Date";
                table.Columns["ACTIVITY_BY_NAME"].ColumnName = "Activity By";
                table.Columns["ACTIVITY_TYPE_NAME"].ColumnName = "Activity";
                table.Columns["COMMENTS"].ColumnName = "Comments";

                using (ExcelPackage pck = new ExcelPackage())
                {
                    // Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add
                        (Constants.ExcelExportSheetName.CANDIDATE_ACTIVITY_LOG);

                    // Load the datatable into the sheet, starting from the 
                    // cell A1 and print the column names on row 1.
                    ws.Cells[Constants.ExcelExport.STARTING_CELL].LoadFromDataTable(table, true);

                    string decimalFormat = Constants.ExcelExportFormats.DECIMAL_COMMA;

                    foreach (DataColumn column in table.Columns)
                    {
                        if (column.DataType == typeof(System.DateTime))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                            {
                                col.Style.Numberformat.Format = Constants.ExcelExportFormats.DATE;
                            }
                        }
                        else if (column.DataType == typeof(decimal))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                            {
                                if (decimalFormat == "C")
                                {
                                    for (int row = 2; row <= ws.Dimension.End.Row; row++)
                                    {
                                        ws.Cells[row, column.Ordinal + 1].Value =
                                            ws.Cells[row, column.Ordinal + 1].Value != null ? ws.Cells[row, column.Ordinal + 1].Value.ToString().Replace(".", ",") : string.Empty;
                                    }
                                    col.Style.Numberformat.Format = "0\",\"0000";
                                }
                                else
                                    col.Style.Numberformat.Format = "###0.0000";
                            }
                        }
                    }

                    // Construct file name.
                    string fileName = string.Format("{0}_{1}.xlsx",
                        Constants.ExcelExportFileName.CANDIDATE_ACTIVITY_LOG,
                        Utility.GetValidExportFileNameString(ViewCandidateActivityLogPopup_candidateActivityViewerControl.CandidateName, 50));

                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                    Response.BinaryWrite(pck.GetAsByteArray());
                    Response.End();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCandidateActivityLogPopup_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the refresh link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will refresh the page.
        /// </remarks>
        protected void ViewCandidateActivityLogPopup_refreshLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Refresh activities.
                RefreshActivities();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCandidateActivityLogPopup_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods

        /// <summary>
        /// Method that refresh the activities.
        /// </summary>
        private void RefreshActivities()
        {
            ViewCandidateActivityLogPopup_candidateActivityViewerControl.RefreshActivities();
        }

        #endregion Private Methods

    }
}
