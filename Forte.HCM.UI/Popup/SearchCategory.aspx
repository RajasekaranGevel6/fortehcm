<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="SearchCategory.aspx.cs" Inherits="Forte.HCM.UI.Popup.SearchCategory" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ID="SearchCateory_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl) {
            var ctrlId = '<%= Request.QueryString["ctrlcid"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }
            if (ctrlId != null && ctrlId != '') {

                // Set the category name field value. Replace the 'link button name' with the 
                // 'category name hidden field name' and retrieve the value.
                var categoryName = document.getElementById(ctrl.id.replace
                    ("SearchCategory_selectButton", "SearchCategory_categoryNameHiddenField")).value;

                // Set the category ID field value. Replace the 'link button name' with the 
                // 'category ID hidden field name' and retrieve the value.
                var categoryID = document.getElementById(ctrl.id.replace
                    ("SearchCategory_selectButton", "SearchCategory_categoryIDHiddenField")).value;

                window.opener.document.getElementById(ctrlId).value = categoryName + "|" + categoryID;

            }
            self.close();
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchCategory_headerLiteral" runat="server" Text="Search Category"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SearchCategory_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:UpdatePanel ID="SearchCategory_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="SearchCategory_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="SearchCategory_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchCategory_searchButton" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchCategory_pageNavigator" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="SearchCategory_searchCriteriasDiv" runat="server" style="display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="SearchCategory_searchCriteriaUpdatePanel" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <table width="100%" border="0" cellspacing="3%" cellpadding="2%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="SearchCategory_categoryIdLabel" runat="server" Text="Category ID"
                                                                                SkinID="sknLabelText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchCategory_categoryIDTextBox" runat="server" MaxLength="8"></asp:TextBox>
                                                                            <ajaxToolKit:FilteredTextBoxExtender ID="SearchCategory_categoryIDFileteredTextBoxExtender"
                                                                                runat="server" TargetControlID="SearchCategory_categoryIDTextBox" FilterType="Numbers">
                                                                            </ajaxToolKit:FilteredTextBoxExtender>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="SearchCategory_categoryNameLabel" runat="server" Text="Category Name"
                                                                                SkinID="sknLabelText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="SearchCategory_categoryNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="4">
                                                                            <asp:Button ID="SearchCategory_searchButton" runat="server" SkinID="sknButtonId"
                                                                                Text="Search" OnClick="SearchCategory_searchButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="SearchCategory_searchResultsUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr id="SearchCategory_searchTestResultsTR" runat="server">
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 50%">
                                                                        <asp:Literal ID="SearchCategory_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                            ID="SearchQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 50%" align="right">
                                                                        <span id="SearchCategory_searchResultsUpSpan" style="display: none;" runat="server">
                                                                            <asp:Image ID="SearchCategory_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                        </span><span id="SearchCategory_searchResultsDownSpan" style="display: block;" runat="server">
                                                                            <asp:Image ID="SearchCategory_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="height: 220px; overflow: auto;" runat="server" id="SearchCategory_searchResultsDiv">
                                                                            <asp:GridView ID="SearchCategory_searchResultsGridView" runat="server" AutoGenerateColumns="False"
                                                                                Width="100%" OnRowCreated="SearchCategory_searchResultsGridView_RowCreated" OnSorting="SearchCategory_searchResultsGridView_Sorting"
                                                                                AllowSorting="true" OnRowDataBound="SearchCategory_searchResultsGridView_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="left">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="SearchCategory_selectButton" runat="server" OnClientClick="javascript:return OnSelectClick(this);"
                                                                                                Text="Select" SkinID="sknActionLinkButton">
                                                                                            </asp:LinkButton>
                                                                                            <asp:HiddenField ID="SearchCategory_categoryNameHiddenField" runat="server" Value='<%# Eval("CategoryName") %>' />
                                                                                            <asp:HiddenField ID="SearchCategory_categoryIDHiddenField" runat="server" Value='<%# Eval("CategoryID") %>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="Category ID" DataField="CategoryID" SortExpression="CATEGORYID" />
                                                                                    <asp:BoundField HeaderText="Category Name" DataField="CategoryName" SortExpression="CATEGORYNAME" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <uc1:PageNavigator ID="SearchCategory_pageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchCategory_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_left_8">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="SearchCategory_topResetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="SearchCategory_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="SearchCategory_topCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="SearchCategory_isMaximizedHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
