﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewCandidateProfile.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.ViewCandidateProfile"
    Title="Candidate Profile" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ViewAssessorProfile_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        // Function that handles the profile menu click event.
        function ProfileMenuClick(menuType)
        {
            // Hide all div.
            document.getElementById('<%=ViewCandidateProfile_selectedMenuHiddenField.ClientID %>').value = menuType;
            document.getElementById('<%=ViewCandidateProfile_contactInfoDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=ViewCandidateProfile_synopsisDiv.ClientID %>').style.display = 'none';

            // Reset color.
            document.getElementById('<%=ViewCandidateProfile_contactInfoLinkButton.ClientID %>').className = "assessor_profile_link_button";
            document.getElementById('<%=ViewCandidateProfile_synopsisLinkButton.ClientID %>').className = "assessor_profile_link_button";

            if (menuType == 'CI')
            {
                document.getElementById('<%=ViewCandidateProfile_contactInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=ViewCandidateProfile_contactInfoLinkButton.ClientID %>').className = "assessor_profile_link_button_selected";
            }
            else if (menuType == 'SY')
            {
                document.getElementById('<%=ViewCandidateProfile_synopsisDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=ViewCandidateProfile_synopsisLinkButton.ClientID %>').className = "assessor_profile_link_button_selected";
            }

            return false;
        }

        // Function that downloads the candidate resume.
        function DownloadCandidateResume(fileName, mimeType)
        {
            var url = '../Common/Download.aspx?fname=' + fileName + '&mtype=' + mimeType + '&type=DownloadResume';

            window.open(url, '', 'toolbar=0,resizable=0,width=1,height=1', '');
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="td_padding_top_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="ViewCandidateProfile_titleLiteral" runat="server">Candidate Profile</asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="ViewCandidateProfile_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <table width="100%" cellpadding="0" cellspacing="3" border="0" class="popupcontrol_view_assessor_profile_bg">
                    <tr>
                        <td style="width: 100%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="ViewCandidateProfile_messageUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="ViewCandidateProfile_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknErrorMessage" Width="100%" Text=""></asp:Label>
                                                <asp:Label ID="ViewCandidateProfile_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknSuccessMessage" Width="100%" Text=""></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 100%">
                                        <asp:UpdatePanel ID="ViewCandidateProfile_inputControlsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 100%" valign="top" class="popup_td_padding_2">
                                                        <asp:HiddenField runat="server" ID="ViewCandidateProfile_candidateIDHiddenField" />
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" class="view_assessor_profile_assessor_details_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="height: 4px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td rowspan="3" valign="top" align="center" class="view_assessor_profile_photo_border">
                                                                                    <table cellpadding="4" cellspacing="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Image runat="server" ID="ViewCandidateProfile_photoImage" Width="160px" Height="160px" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <table cellpadding="4" cellspacing="5" width="100%">
                                                                                                    <%--<tr>
                                                                                                        <td valign="middle" align="center" class="assessor_profile_left_menu_bg" style="height: 20px">
                                                                                                             <asp:LinkButton ID="ViewCandidateProfile_sendEmailLinkButton" runat="server" Text="Send Email"
                                                                                                                SkinID="sknAssessorProfileLeftMenuLinkButton" ToolTip="Click here to send a mail to the Candidate"></asp:LinkButton>- 
                                                                                                        </td>
                                                                                                    </tr>--%>
                                                                                                     <tr>
                                                                                                        <td valign="middle" align="center" class="assessor_profile_left_menu_bg" style="height: 20px">
                                                                                                            <asp:LinkButton ID="ViewCandidateProfile_activityLogLinkButton" runat="server" Text="Activity Log"
                                                                                                                SkinID="sknAssessorProfileLeftMenuLinkButton" ToolTip="Click here to view activity log"></asp:LinkButton>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                     <tr>
                                                                                                        <td valign="middle" align="center" class="assessor_profile_left_menu_bg" style="height: 20px">
                                                                                                            <asp:LinkButton ID="ViewCandidateProfile_addNotesLinkButton" runat="server" Text="Add Notes"
                                                                                                                SkinID="sknAssessorProfileLeftMenuLinkButton"  ToolTip="Click here to add notes"></asp:LinkButton>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                        <tr>
                                                                                                        <td valign="middle" align="center" class="assessor_profile_left_menu_bg" style="height: 20px"> 
                                                                                                            <asp:HyperLink ID="ViewCandidateProfile_viewCandidateActivityDashboardHyperLink" runat="server" Target="_blank" 
                                                                                                                 Text="Activity Dashboard" Font-Underline="false"  SkinID="sknAssessorProfileLeftMenuLinkButton" ForeColor="White" Font-Bold="true" Font-Size="13px" ToolTip="Click here to view candidate activity dashboard" /> 
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" colspan="5" style="width: 100%" valign="top" class="view_assessor_profile_title_border">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 90%; height: 50px; padding-left: 10px;" valign="middle">
                                                                                                 
                                                                                                <asp:Label ID="ViewCandidateProfile_nameValueLabel" runat="server" SkinID="sknLabelProfileNameText"
                                                                                                    Text=""></asp:Label>
                                                                                            </td>
                                                                                            <td align="right" style="width: 10%; padding-right: 10px;" valign="middle">
                                                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td align="right">
                                                                                                            <asp:ImageButton ID="ViewCandidateProfile_downloadResumeImageButton" runat="server" Visible="false"
                                                                                                                ToolTip="Click here to download resume" AlternateText="Attachment" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_word.gif"/>
                                                                                                        </td>
                                                                                                        <td align="right">
                                                                                                            <asp:HyperLink ID="ViewCandidateProfile_linkedInProfileHyperLink" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/linkedin.png"
                                                                                                                Target="_blank" ToolTip="Click here to view Candidate LinkedID profile">
                                                                                                            </asp:HyperLink>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" style="height: 36px" valign="top" class="assessor_profile_links_bg">
                                                                                                <table border="0" cellpadding="10px" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:HiddenField ID="ViewCandidateProfile_selectedMenuHiddenField" runat="server"
                                                                                                                Value="CI" />
                                                                                                            <asp:LinkButton ID="ViewCandidateProfile_contactInfoLinkButton" runat="server" Text="Contact Info"
                                                                                                                ToolTip="Contact Info" CssClass="assessor_profile_link_button_selected" 
                                                                                                                OnClientClick="javascript: return ProfileMenuClick('CI')"></asp:LinkButton>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:LinkButton ID="ViewCandidateProfile_synopsisLinkButton" runat="server"
                                                                                                                ToolTip="Synopsis" Text="Synopsis" CssClass="assessor_profile_link_button"
                                                                                                                OnClientClick="javascript: return ProfileMenuClick('SY')"></asp:LinkButton>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="height: 20px">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" style="width: 100%;" class="view_assessor_profile_details_border">
                                                                                    <div style="height: 300px; overflow: auto;">
                                                                                        <div id="ViewCandidateProfile_contactInfoDiv" runat="server" style="display: block">
                                                                                            <table cellpadding="10" cellspacing="0" style="width: 100%">
                                                                                             <tr>
                                                                                                    <td align="left" style="width: 10%; height: 16px" valign="middle">
                                                                                                        <asp:Label ID="ViewCandidateProfile_addresslLabel" runat="server" SkinID="sknProfileTitleField"
                                                                                                            Text="Address"></asp:Label>
                                                                                                    </td>
                                                                                                    <td align="left" style="width: 34%; padding-left: 10px" valign="middle">
                                                                                                        <asp:Label ID="ViewCandidateProfile_addressValueLabel" runat="server" SkinID="sknProfileValueField"
                                                                                                            Text=""></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                  <tr>
                                                                                                    <td align="left" style="width: 10%; height: 16px" valign="middle">
                                                                                                        <asp:Label ID="ViewCandidateProfile_locationLabel" runat="server" SkinID="sknProfileTitleField"
                                                                                                            Text="Location"></asp:Label>
                                                                                                    </td>
                                                                                                    <td align="left" style="width: 34%; padding-left: 10px" valign="middle">
                                                                                                        <asp:Label ID="ViewCandidateProfile_locationValueLabel" runat="server" SkinID="sknProfileValueField"
                                                                                                            Text=""></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="left" style="width: 10%; height: 16px" valign="middle">
                                                                                                        <asp:Label ID="ViewCandidateProfile_emailLabel" runat="server" SkinID="sknProfileTitleField"
                                                                                                            Text="Email"></asp:Label>
                                                                                                    </td>
                                                                                                    <td align="left" style="width: 34%; padding-left: 10px" valign="middle">
                                                                                                        <asp:Label ID="ViewCandidateProfile_emailValueLabel" runat="server" SkinID="sknProfileValueField"
                                                                                                            Text=""></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                               
                                                                                                <tr>
                                                                                                    <td align="left" style="width: 10%; height: 16px" valign="middle">
                                                                                                        <asp:Label ID="ViewCandidateProfile_mobilePhoneLabel" runat="server" SkinID="sknProfileTitleField"
                                                                                                            Text="Mobile Phone"></asp:Label>
                                                                                                    </td>
                                                                                                    <td align="left" style="width: 34%; padding-left: 10px" valign="middle">
                                                                                                        <asp:Label ID="ViewCandidateProfile_mobilePhoneValueLabel" runat="server" SkinID="sknProfileValueField"
                                                                                                            Text=""></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="left" style="width: 10%; height: 16px" valign="middle">
                                                                                                        <asp:Label ID="ViewCandidateProfile_homePhoneLabel" runat="server" SkinID="sknProfileTitleField"
                                                                                                            Text="Home Phone"></asp:Label>
                                                                                                    </td>
                                                                                                    <td align="left" style="width: 34%; padding-left: 10px" valign="middle">
                                                                                                        <asp:Label ID="ViewCandidateProfile_homePhoneValueLabel" runat="server" SkinID="sknProfileValueField"
                                                                                                            Text=""></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                        <div id="ViewCandidateProfile_synopsisDiv" runat="server" style="display: none">
                                                                                            <table cellpadding="10" cellspacing="0" style="width: 100%">
                                                                                                <tr>
                                                                                                    <td align="left" style="width: 85%" valign="top">
                                                                                                        <div style="height: 264px; overflow: auto;">
                                                                                                            <asp:Label ID="ViewCandidateProfile_synopsisValueLabel" runat="server"
                                                                                                                SkinID="sknProfileValueField" Text=""></asp:Label>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="ViewCandidateProfile_bottomControlsUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="left">
                                    <asp:LinkButton ID="ViewCandidateProfile_cancelLinkButton" runat="server" Text="Cancel"
                                        SkinID="sknPopupLinkButton" OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window"> </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_2">
            </td>
        </tr>
    </table>
</asp:Content>
