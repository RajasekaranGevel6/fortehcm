﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/PopupMaster.Master"
    AutoEventWireup="True" CodeBehind="BulkCandidateUpload.aspx.cs" Inherits="Forte.HCM.UI.Popup.BulkCandidateUpload" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/DisclaimerControl.ascx" TagName="DisclaimerControl"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PopupMaster_contentPlaceHolder"
    runat="server">
    <script type="text/javascript" language="javascript">

        // Function that will open the download window.
        function DownloadBulkScheduleCandidateTemplate() {
            window.open('../Common/Download.aspx?type=BSCT', '', 'toolbar=0,resizable=0,width=1,height=1', '');
        }

        //Method that is called when the options radio button list is clicked
        function CheckOtherIsCheckedByGVID(spanChk, gridId) {
            var IsChecked = spanChk.checked;
            var CurrentRdbID = spanChk.id;
            var Chk = spanChk;
            Parent = document.getElementById(gridId);
            var items = Parent.getElementsByTagName('input');
            //Loops through the options in list
            for (i = 0; i < items.length; i++) {
                if (items[i].id != CurrentRdbID && items[i].type == "radio") {
                    //If one item is selected then make other item non selectable
                    if (items[i].checked) {
                        items[i].checked = false;
                        items[i].parentElement.parentElement.style.backgroundColor = 'white';
                        items[i].parentElement.parentElement.style.color = 'black';
                    }
                }
            }
        }

        function ShowCandidateImage(path) {
            window.open(path);
            return false;
        }

        // Function that shows the add candidate image popup
        function ShowCandidateImagePopup(path, ctrlId) {
            var height = 250;
            var width = 450;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:"
            + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes;toolbars=no,menubar=no,location=no";

            var queryStringValue = "../popup/AddCandidateImage.aspx?imagePath=" + path + "&ctrlId=" + ctrlId;

            window.open(queryStringValue, window.self, sModalFeature);
            return false;
        }
    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="BulkScheduleCandidates_headerLiteral" runat="server" Text="Bulk Schedule Candidates"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <table width="100%">
                    <tr>
                        <td class="msg_align">
                            <asp:Label ID="BulkScheduleCandidates_topSuccessMessageLabel" runat="server" Tag="12"
                                SkinID="sknSuccessMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="msg_align">
                            <asp:Label ID="BulkScheduleCandidates_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                                ID="Searchcandidate_stateHiddenField" runat="server" Value="0" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <div id="BulkScheduleCandidates_uploadExcelDiv" runat="server" style="display: block">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td class="td_height_8">
                            </td>
                        </tr>
                        <tr>
                            <td class="header_bg">
                                <asp:Literal ID="BulkScheduleCandidates_fileSelectionLiteral" runat="server" Text="File Selection"></asp:Literal>
                            </td>
                        </tr>
                        <tr id="BulkScheduleCandidates_fileUploadTr" runat="server" style="display: block">
                            <td class="grid_body_bg">
                                <table border="0" cellpadding="2" cellspacing="5" width="80%">
                                    <tr>
                                        <td colspan="2" class="batch_upload_instructions_title">
                                            Before performing a batch upload of your candidates, please ensure that
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="batch_upload_instructions">
                                            1. You are using the ForteHCM supplied excel template. Click
                                            <asp:LinkButton runat="server" ID="BulkScheduleCandidates_downloadTemplateLinkButton"
                                                CssClass="batch_upload_download_template_link_btn"> here </asp:LinkButton>
                                            to download the template.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="batch_upload_instructions">
                                            2. The excel sheet name does not contain any special characters and spaces
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="batch_upload_instructions">
                                            3. The excel file is closed before you click on the upload button
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 45%">
                                            <asp:Label ID="BulkScheduleCandidates_fileNameLabel" runat="server" Text="Excel File (that contains the candidates information)"
                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            <span class="mandatory">*</span>
                                        </td>
                                        <td valign="middle">
                                            <div style="float: left; padding-right: 5px;">
                                                <asp:FileUpload ID="BulkScheduleCandidates_fileUpload" runat="server" />
                                            </div>
                                            <div style="float: left;">
                                                <asp:ImageButton ID="BulkScheduleCandidates_helpImageButton" SkinID="sknHelpImageButton"
                                                    runat="server" OnClientClick="javascript:return false;" ToolTip="Click here to select the excel file that contains the candidates" />
                                            </div>
                                        </td>
                                        <%--   <td>
                                            <asp:Button ID="BulkScheduleCandidates_uploadButton" runat="server" Text="Upload" SkinID="sknButtonId"
                                                OnClick="BulkScheduleCandidates_uploadButton_Click" />
                                        </td>--%>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:RegularExpressionValidator ID="BulkScheduleCandidates_regularExpressionValidataor"
                                                runat="server" ControlToValidate="BulkScheduleCandidates_fileUpload" ErrorMessage="Only excel files are allowed"
                                                ValidationExpression="^.+\.((xls|xlsx))$">
                                            </asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: right">
                                            <asp:Button ID="BulkScheduleCandidates_uploadButton" runat="server" Text="Upload"
                                                SkinID="sknButtonId" OnClick="BulkScheduleCandidates_uploadButton_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="BulkScheduleCandidates_fileUploadSuccessTr" runat="server" style="display: none">
                            <td class="grid_body_bg">
                                <table border="0" cellpadding="2" cellspacing="5" width="70%">
                                    <tr>
                                        <td colspan="2" class="batch_upload_instructions">
                                            1. Candidates are Successfully scheduled with the test session . Click
                                            <asp:LinkButton runat="server" ID="BulkScheduleCandidates_downloadCandidateDetails"
                                                CssClass="batch_upload_download_template_link_btn" OnClick="BulkScheduleCandidates_downloadCandidateDetails_Click"> here </asp:LinkButton>
                                            to download the Candidate list in excel format with Candidate session key and credentials.
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="BulkScheduleCandidates_resultDiv" runat="server" visible="false" >
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="msg_align_center">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="BulkScheduleCandidates_inCompleteCandidatesGridView" runat="server"
                                                AutoGenerateColumns="false" OnRowDataBound="BulkScheduleCandidates_inCompleteCandidatesGridView_RowDataBound"
                                                SkinID="sknBulkInvalidCandidates" ShowFooter="false" ShowHeader="false"  >
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <table width="100%" border="0" cellspacing="5" cellpadding="2">
                                                                <tr>
                                                                    <td style="color:Orange; font-size:14px;float:left">
                                                                       <asp:Label runat="server" ID="BulkScheduleCandidates_inCompleteCandidatesErrorLabel" SkinID="sknBulkScheduleInvalidCandidatesError" Text='<%# Eval("caiInvalidCandidatesRemarks")%>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <%--<div id="BulkScheduleCandidates_resultDiv" runat="server" visible="false">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="msg_align_center">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="BulkScheduleCandidates_recordLabel" runat="server" Tag="12" Text="" SkinID="sknRecordCount"></asp:Label>
                                            <span class="signin_text">|</span>
                                            <asp:LinkButton ID="BulkScheduleCandidates_nextLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                Text="Next" OnClick="BulkScheduleCandidates_nextLinkButton_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ajaxToolKit:TabContainer ID="BulkScheduleCandidates_mainTabContainer" runat="server"
                                    ActiveTabIndex="0">
                                    <ajaxToolKit:TabPanel ID="BulkScheduleCandidates_candidatesTabPanel" HeaderText="Candidates"
                                        runat="server">
                                        <HeaderTemplate>
                                            Candidates</HeaderTemplate>
                                        <ContentTemplate>
                                            <table border="0" cellpadding="0" cellspacing="5%" width="100%">
                                                <tr>
                                                    <td class="msg_align">
                                                        <asp:Label ID="BulkScheduleCandidates_candidatesTabPanel_errorMessageLabel" runat="server"
                                                            SkinID="sknErrorMessage"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="BulkScheduleCandidates_removeCandidateLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                        Text="Remove&nbsp;All" OnClick="BulkScheduleCandidates_removeCandidateLinkButton_Click" />
                                                                </td>
                                                                <td style="width: 5%">
                                                                    &nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="BulkScheduleCandidates_saveButton" runat="server" SkinID="sknButtonId"
                                                                        Text="Save" OnClick="BulkScheduleCandidates_saveButton_Click" /><asp:HiddenField ID="BulkScheduleCandidates_isSavedHiddenField"
                                                                            runat="server" Value="0" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:GridView ID="BulkScheduleCandidates_candidateGridView" runat="server" AutoGenerateColumns="False"
                                                OnRowDataBound="BulkScheduleCandidates_candidatesDataList_RowDataBound" ShowHeader="False"
                                                SkinID="sknNewGridView" OnRowCommand="BulkScheduleCandidates_candidateGridView_RowCommand">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                <tr>
                                                                    <td valign="middle" style="width: 8%">
                                                                        <div style="float: left; width: 35%;">
                                                                            <asp:ImageButton ID="BulkScheduleCandidates_candidatesGridView_deleteImageButton" runat="server"
                                                                                SkinID="sknDeleteImageButton" ToolTip="Delete Candidate" CommandName="DeleteCandidate"
                                                                                CommandArgument='<%# Eval("CandidateID")%>' /></div>
                                                                        <div style="float: left;">
                                                                            <asp:Image ID="BulkScheduleCandidates_candidateImage" runat="server" SkinID="sknCandidateImage"
                                                                                ToolTip="Candidate" />
                                                                            <asp:Label ID="BulkScheduleCandidates_candidateNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                                runat="server" ReadOnly="true" Text='<%# Eval("CandidateID") %>'>
                                                                            </asp:Label></div>
                                                                    </td>
                                                                    <td colspan="6" style="width: 92%">
                                                                        <asp:TextBox ID="BulkScheduleCandidates_candidateTextBox" runat="server" SkinID="sknMultiLineTextBox"
                                                                            TextMode="MultiLine" Width="91%" Height="40" Text='<%# Eval("Candidate") %>' MaxLength="500"
                                                                            onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="BulkScheduleCandidates_imageLinkTR">
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BulkScheduleCandidates_imageNameHeadLabel" runat="server" Text="Image Name"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td colspan="5">
                                                                        <asp:LinkButton ID="BulkScheduleCandidates_qstImageLinkButton" SkinID="sknActionLinkButton"
                                                                            runat="server" Text='<%# Eval("ImageName") %>' Visible='<%# Eval("HasImage") %>'> 
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td colspan="6">
                                                                        <asp:DataList ID="BulkScheduleCandidates_subject_categoryDataList" runat="server" RepeatColumns="2"
                                                                            RepeatDirection="Vertical" Width="100%" DataSource='<%# Eval("AnswerChoices") %>'
                                                                            OnItemDataBound="BulkScheduleCandidates_subject_categoryDataList_ItemDataBound">
                                                                            <ItemTemplate>
                                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                                    <tr>
                                                                                        <td style="width: 10px">
                                                                                            <asp:RadioButton ID="BulkScheduleCandidates_selectRadioButton" runat="server" Checked='<%# Eval("IsCorrect") %>' />
                                                                                        </td>
                                                                                        <td style="width: 420px">
                                                                                            <asp:TextBox ID="BulkScheduleCandidates_answerTextBox" SkinID="sknMultiLineTextBox" runat="server"
                                                                                                TextMode="MultiLine" Text='<%# Eval("Choice") %>' Width="80%" Height="30px" MaxLength="200"
                                                                                                onkeyup="CommentsCount(200,this)" onchange="CommentsCount(200,this)"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </asp:DataList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="BulkScheduleCandidates_categoryCandidateLabel" runat="server" Text="Category"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="BulkScheduleCandidates_categoryCandidateReadOnlyLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" Text='<%# Eval("CategoryName") %>'></asp:Label>
                                                                        <asp:HiddenField ID="BulkScheduleCandidates_categoryIDHiddenField" runat="server" Value='<%# Eval("CategoryID") %>' />
                                                                    </td>
                                                                    <td style="width: 6%">
                                                                        <asp:Label ID="BulkScheduleCandidates_subjectCandidateLabel" runat="server" Text="Subject"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="BulkScheduleCandidates_subjectCandidateReadOnlyLabel" runat="server" Text='<%# Eval("SubjectName") %>'
                                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                                        <asp:HiddenField ID="BulkScheduleCandidates_subjectIDHiddenField" runat="server" Value='<%# Eval("SubjectID") %>' />
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="BulkScheduleCandidates_testAreaCandidateHeadLabel" runat="server" Text="Test Area"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BulkScheduleCandidates_testAreaCandidateLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" Text='<%# Eval("TestAreaName") %>' />
                                                                        <asp:HiddenField ID="BulkScheduleCandidates_testAreaIDHiddenField" runat="server" Value='<%# Eval("TestAreaID") %>' />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BulkScheduleCandidates_complexityCandidateHeadLabel" runat="server" Text="Complexity"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BulkScheduleCandidates_complexityCandidateLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" Text='<%# Eval("ComplexityName") %>' />
                                                                        <asp:HiddenField ID="BulkScheduleCandidates_complexityIDHiddenField" runat="server" Value='<%# Eval("Complexity") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BulkScheduleCandidates_tagCandidateHeadLabel" runat="server" Text="Tag"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BulkScheduleCandidates_tagCandidateReadOnlyLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" Text='<%# Eval("Tag") %>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BulkScheduleCandidates_authorCandidateHeadLabel" runat="server" Text="Author"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BulkScheduleCandidates_authorCandidateReadOnlyLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" ReadOnly="true" Text='<%# Eval("AuthorName") %>'>
                                                                        </asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </ajaxToolKit:TabPanel>
                                    <ajaxToolKit:TabPanel ID="BulkScheduleCandidates_inCompleteCandidatesTabPanel" HeaderText="Incomplete Candidates"
                                        runat="server">
                                        <HeaderTemplate>
                                            Incomplete Candidates</HeaderTemplate>
                                        <ContentTemplate>
                                            <table border="0" cellpadding="0" cellspacing="5%" width="100%">
                                                <tr>
                                                    <td class="msg_align">
                                                        <asp:Label ID="BulkScheduleCandidates_inCompleteCandidatesTabPanel_errorMessageLabel"
                                                            runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="BulkScheduleCandidates_removeInvalidCandidateLinkButton" runat="server"
                                                                        SkinID="sknActionLinkButton" Text="Remove&nbsp;All" OnClick="BulkScheduleCandidates_removeInvalidCandidateLinkButton_Click" />
                                                                </td>
                                                                <td style="width: 5%">
                                                                    &nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="BulkScheduleCandidates_invalidSaveButton" runat="server" SkinID="sknButtonId"
                                                                        Text="Save" OnClick="BulkScheduleCandidates_invalidSaveButton_Click" /><asp:HiddenField
                                                                            ID="BulkScheduleCandidates_isInvalidSavedHiddenField" runat="server" Value="0" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:GridView ID="BulkScheduleCandidates_inCompleteCandidatesGridView" runat="server"
                                                AutoGenerateColumns="false" OnRowDataBound="BulkScheduleCandidates_inCompleteCandidatesDataList_RowDataBound"
                                                SkinID="sknNewGridView" ShowFooter="false" ShowHeader="false" OnRowCommand="BulkScheduleCandidates_inCompleteCandidatesGridView_RowCommand">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <table width="100%" border="0" cellspacing="5" cellpadding="2">
                                                                <tr>
                                                                    <td valign="middle" style="width: 8%">
                                                                        <div style="float: left; width: 35%;">
                                                                            <asp:ImageButton ID="BulkScheduleCandidates_candidatesGridView_deleteImageButton" runat="server"
                                                                                SkinID="sknDeleteImageButton" ToolTip="Delete Candidate" CommandName="DeleteCandidate"
                                                                                CommandArgument='<%# Eval("CandidateID")%>' /></div>
                                                                        <div style="float: left;">
                                                                            <asp:Image ID="BulkScheduleCandidates_incompleteCandidateImage" runat="server" SkinID="sknCandidateImage"
                                                                                ToolTip="Candidate" />
                                                                            <asp:Label ID="BulkScheduleCandidates_incompleteCandidateNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                                runat="server" ReadOnly="true" Text='<%# Eval("CandidateID") %>'>
                                                                            </asp:Label></div>
                                                                    </td>
                                                                    <td colspan="3" style="width: 92%">
                                                                        <asp:TextBox ID="BulkScheduleCandidates_incompleteCandidateTextBox" runat="server" TextMode="MultiLine"
                                                                            Width="91%" Height="40" SkinID="sknMultiLineTextBox" Text='<%# Eval("Candidate") %>'
                                                                            MaxLength="500" onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="BulkScheduleCandidates_addImageLinkTR">
                                                                    <td>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <asp:LinkButton ID="BulkScheduleCandidates_addCandidateImageLinkButton" runat="server"
                                                                            Text="Add Image" SkinID="sknActionLinkButton" ToolTip="Add Candidate Image" Visible='<%# Eval("HasImage") %>'>
                                                                        </asp:LinkButton>
                                                                        <asp:HiddenField ID="BulkScheduleCandidates_qstImageHiddenField" runat="server" Value='<%# Eval("ImageName") %>'>
                                                                        </asp:HiddenField>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <asp:DataList ID="BulkScheduleCandidates_subjectcategoryCandidateDataList" runat="server"
                                                                            RepeatColumns="2" RepeatDirection="Vertical" Width="100%" DataSource='<%# Eval("AnswerChoices") %>'
                                                                            OnItemDataBound="BulkScheduleCandidates_subjectcategoryCandidateDataList_ItemDataBound">
                                                                            <ItemTemplate>
                                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                                    <tr>
                                                                                        <td style="width: 10px">
                                                                                            <asp:RadioButton ID="BulkScheduleCandidates_selectCandidateRadioButton" runat="server"
                                                                                                Checked='<%# Eval("IsCorrect") %>' onclick="CheckOtherIsCheckedByGVID(this)" />
                                                                                        </td>
                                                                                        <td style="width: 420px">
                                                                                            <asp:TextBox ID="BulkScheduleCandidates_answerCandidateTextBox" runat="server" TextMode="MultiLine"
                                                                                                Text='<%# Eval("Choice") %>' SkinID="sknMultiLineTextBox" Width="80%" Height="30px"
                                                                                                MaxLength="200" onkeyup="CommentsCount(200,this)" onchange="CommentsCount(200,this)"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </asp:DataList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td style="width: 55%" valign="middle">
                                                                        <div style="float: left; width: 92%;" class="grouping_border_bg">
                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="BulkScheduleCandidates_categoryLabel" runat="server" Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 43%">
                                                                                        <asp:TextBox ID="BulkScheduleCandidates_categoryTextBox" runat="server" Text='<%# Eval("CategoryName") %>'
                                                                                            MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 15%">
                                                                                        <asp:Label ID="BulkScheduleCandidates_subjectLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 22%">
                                                                                        <asp:TextBox ID="BulkScheduleCandidates_subjectTextBox" runat="server" Text='<%# Eval("SubjectName") %>'
                                                                                            MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="BulkScheduleCandidates_subjectIdHiddenField" runat="server" Value='<%# Eval("SubjectID") %>' />
                                                                                        <asp:HiddenField ID="BulkScheduleCandidates_categoryNameHiddenField" runat="server" Value='<%# Eval("CategoryName") %>' />
                                                                                        <asp:HiddenField ID="BulkScheduleCandidates_subjectNameHiddenField" runat="server" Value='<%# Eval("SubjectName") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <div style="float: left; padding-left: 5px; padding-top: 5px;">
                                                                            <asp:ImageButton ID="BulkScheduleCandidates_categoryImageButton" SkinID="sknbtnSearchicon"
                                                                                runat="server" ToolTip="Click here to select category and subject" /></div>
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="BulkScheduleCandidates_testAreaLabel" runat="server" Text="Test Area"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="BulkScheduleCandidates_testAreaDropDownList" runat="server" Width="133px">
                                                                        </asp:DropDownList>
                                                                        <asp:HiddenField ID="BulkScheduleCandidates_testAreaHiddenField" runat="server" Value='<%# Eval("TestAreaName") %>' />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td style="width: 55%">
                                                                        <div style="float: left; padding-right: 5px; width: 92%;" class="grouping_bg">
                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="BulkScheduleCandidates_complexityLabel" runat="server" Text="Complexity"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 43%">
                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                            <asp:DropDownList ID="BulkScheduleCandidates_complexityDropDownList" runat="server" Width="133px">
                                                                                            </asp:DropDownList>
                                                                                            <asp:HiddenField ID="BulkScheduleCandidates_complexityDropDownListHiddenField" runat="server"
                                                                                                Value='<%#Eval("ComplexityName") %>' />
                                                                                        </div>
                                                                                        <div style="float: left;">
                                                                                            <asp:ImageButton ID="BulkScheduleCandidates_complexityImageButton" SkinID="sknHelpImageButton"
                                                                                                runat="server" ImageAlign="Middle" CommandArgument='<%# Eval("Complexity") %>'
                                                                                                OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, ComplexityHelp %>" /></div>
                                                                                    </td>
                                                                                    <td style="width: 16%">
                                                                                        <asp:Label ID="BulkScheduleCandidates_tagHeadLabel" runat="server" Text="Tag" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 22%">
                                                                                        <asp:TextBox ID="BulkScheduleCandidates_tagTextBox" runat="server" Text='<%# Eval("Tag") %>'
                                                                                            MaxLength="100"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            &nbsp;</div>
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="BulkScheduleCandidates_authorHeadLabel" runat="server" Text="Author" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="BulkScheduleCandidates_authorTextBox" runat="server" Text='<%#Eval("AuthorName") %>'>
                                                                            </asp:TextBox>
                                                                            <asp:HiddenField ID="BulkScheduleCandidates_authorHiddenField" runat="server" Value='<%#Eval("Author") %>' />
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="BulkScheduleCandidates_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select the Candidate author" /></div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <asp:Label ID="BulkScheduleCandidates_remarksHeadLabel" runat="server" Text="Remarks "
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="BulkScheduleCandidates_remarksReadOnlyLabel" SkinID="sknLabelFieldText"
                                                                                        runat="server" ReadOnly="true" Text='<%#Eval("InvalidCandidateRemarks") %>'>                                                                                        
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <uc1:PageNavigator ID="BulkScheduleCandidates_pageNavigator" runat="server" />
                                        </ContentTemplate>
                                    </ajaxToolKit:TabPanel>
                                </ajaxToolKit:TabContainer>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="BulkScheduleCandidates_removepopupPanel" runat="server" Style="display: none"
                                    CssClass="popupcontrol_confirm_remove">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="popup_td_padding_10">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                                            <asp:Label ID="BulkScheduleCandidates_confirmMsgControl_titleLabel" runat="server" Text="Remove Confirm"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%" valign="top" align="right">
                                                            <asp:ImageButton ID="BulkScheduleCandidates_confirmMsgControl_CandidateCloseImageButton"
                                                                runat="server" SkinID="sknCloseImageButton" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="popup_td_padding_10">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_candidate_inner_bg">
                                                    <tr>
                                                        <td align="left" class="popup_td_padding_10">
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Label ID="BulkScheduleCandidates_confirmMsgControl_messageValidLabel" runat="server"
                                                                            SkinID="sknLabelFieldText" Text="Are you sure want to remove all the candidates?"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_20">
                                                                    </td>
                                                                </tr>
                                                                <tr valign="bottom">
                                                                    <td align="right" style="text-align: center">
                                                                        <asp:Button ID="BulkScheduleCandidates_confirmMsgControl_yesValidButton" runat="server"
                                                                            SkinID="sknButtonId" Text="Yes" Width="64px" OnClick="BulkScheduleCandidates_confirmMsgControl_yesValidButton_Click" />
                                                                        <asp:Button ID="BulkScheduleCandidates_confirmMsgControl_noValidButton" runat="server"
                                                                            SkinID="sknButtonId" Text="No" Width="45px" OnClientClick="javascript:return false;" />
                                                                        <asp:HiddenField ID="BulkScheduleCandidates_candidateIDHiddenField" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_20">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <div style="display: none">
                                    <asp:Button ID="BulkScheduleCandidates_hiddenButton" runat="server" />
                                </div>
                                <ajaxToolKit:ModalPopupExtender ID="BulkScheduleCandidates_deletepopupExtender" runat="server"
                                    PopupControlID="BulkScheduleCandidates_removepopupPanel" TargetControlID="BulkScheduleCandidates_hiddenButton"
                                    BackgroundCssClass="modalBackground" CancelControlID="BulkScheduleCandidates_confirmMsgControl_noValidButton"
                                    DynamicServicePath="" Enabled="True">
                                </ajaxToolKit:ModalPopupExtender>
                            </td>
                        </tr>
                    </table>
                </div>--%>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <table width="100%">
                    <tr>
                        <td class="msg_align">
                            <asp:Label ID="BulkScheduleCandidates_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="msg_align">
                            <asp:Label ID="BulkScheduleCandidates_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <asp:Panel ID="BulkScheduleCandidates_candidatePanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_confirm_next">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="popup_td_padding_10">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                            <asp:Literal ID="BulkScheduleCandidates_titleLiteral" runat="server" Text="Confirmation Message"></asp:Literal>
                                        </td>
                                        <td style="width: 50%" valign="top" align="right">
                                            <asp:ImageButton ID="candidateDetailPreviewControl_topCancelImageButton" runat="server"
                                                SkinID="sknCloseImageButton" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="popup_td_padding_10">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_candidate_inner_bg">
                                    <tr>
                                        <td class="popup_td_padding_10" align="left">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="BulkScheduleCandidates_messageLabel" SkinID="sknLabelFieldText" runat="server"
                                                            Text="The candidates present in 'candidates' tab is not saved.<br \><br \>Select your option to continue."></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_20">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_padding_left_20">
                                                        <asp:RadioButton ID="BulkScheduleCandidates_saveRadioButton" runat="server" Text="Save and load next"
                                                            GroupName="confirm" Checked="true" ForeColor="#148EC0" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_padding_left_20">
                                                        <asp:RadioButton ID="BulkScheduleCandidates_clearRadioButton" runat="server" Text="Clear all and load next"
                                                            GroupName="confirm" ForeColor="#148EC0" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_8">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_8">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="popup_td_padding_5">
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td>
                                            <asp:Button ID="BulkScheduleCandidates_okButton" runat="server" SkinID="sknButtonId"
                                                Text="OK" Width="64px" OnClick="BulkScheduleCandidates_saveCandidateOkButton_Click" />
                                        </td>
                                        <td style="padding-left: 10px">
                                            <asp:LinkButton ID="BulkScheduleCandidates_cancelLinkButton" runat="server" SkinID="sknCancelLinkButton"
                                                Text="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div id="BulkScheduleCandidates_candidateModalPopupHiddenDiv" runat="server" style="display: none">
                    <asp:Button ID="BulkScheduleCandidates_modalPopUpHiddenButton" runat="server" />
                </div>
                <ajaxToolKit:ModalPopupExtender ID="BulkScheduleCandidates_candidateModalPopupExtender"
                    runat="server" PopupControlID="BulkScheduleCandidates_candidatePanel" TargetControlID="BulkScheduleCandidates_modalPopUpHiddenButton"
                    BackgroundCssClass="modalBackground" CancelControlID="BulkScheduleCandidates_cancelLinkButton">
                </ajaxToolKit:ModalPopupExtender>
                <asp:Panel ID="BulkScheduleCandidates_saveValidPopupPanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_confirm_disclaimer">
                    <uc3:DisclaimerControl ID="BulkScheduleCandidates_savePopupExtenderControl" runat="server"
                        Title="Disclaimer" />
                    <div style="padding-left: 11px">
                        <asp:Button ID="DisclaimerControl_acceptButton" runat="server" SkinID="sknButtonId"
                            Text="Accept" Width="64px" OnClick="DisclaimerControl_acceptButton_Click" />
                        &nbsp;
                        <asp:LinkButton ID="DisclaimerControl_cancelLinkButton" Text="Cancel" runat="server"
                            SkinID="sknPopupLinkButton"></asp:LinkButton>
                    </div>
                </asp:Panel>
                <div id="BulkScheduleCandidates_hiddensavePopupModalDiv" runat="server" style="display: none">
                    <asp:Button ID="BulkScheduleCandidates_hiddensavePopupModalButton" runat="server" />
                </div>
                <ajaxToolKit:ModalPopupExtender ID="BulkScheduleCandidates_disclaimerSavepopupExtender"
                    runat="server" PopupControlID="BulkScheduleCandidates_saveValidPopupPanel" TargetControlID="BulkScheduleCandidates_hiddensavePopupModalButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
                <asp:Panel ID="BulkScheduleCandidates_saveInValidPopupPanel" runat="server" Style="display: none"
                    CssClass="popupcontrol_confirm_disclaimer">
                    <uc3:DisclaimerControl ID="BulkScheduleCandidates_saveInvalidPopupExtenderControl"
                        runat="server" Title="Disclaimer" />
                    <div style="padding-left: 11px">
                        <asp:Button ID="DisclaimerControl_acceptInvalidButton" runat="server" SkinID="sknButtonId"
                            Text="Accept" Width="64px" OnClick="BulkScheduleCandidates_acceptInvalidCandidateButton_Click" />
                        &nbsp;
                        <asp:LinkButton ID="DisclaimerControl_cancelInvalidLinkButton" Text="Cancel" runat="server"
                            SkinID="sknPopupLinkButton"></asp:LinkButton>
                    </div>
                </asp:Panel>
                <div id="BulkScheduleCandidates_hiddensaveInvalidPopupModalDiv" runat="server" style="display: none">
                    <asp:Button ID="BulkScheduleCandidates_hiddensaveInvalidPopupModalButton" runat="server" />
                </div>
                <ajaxToolKit:ModalPopupExtender ID="BulkScheduleCandidates_disclaimerSaveInvalidpopupExtender"
                    runat="server" PopupControlID="BulkScheduleCandidates_saveInValidPopupPanel"
                    TargetControlID="BulkScheduleCandidates_hiddensaveInvalidPopupModalButton" BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
    </table>
</asp:Content>
