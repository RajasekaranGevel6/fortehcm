﻿#region Header

// Copyright (C) Forte. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AssessInterviewResponse.aspx.cs
// File represents the AssessInterviewResponse class that defines
// the user interface layout and functionalities for the assess interview
// response page. This page helps to view the question statistics along
// with the video. This class inherits Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives

using System;
using System.Net;
using System.Web.UI;
using System.Configuration;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Collections.Generic;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the AssessInterviewResponse class that defines
    /// the user interface layout and functionalities for the assess interview
    /// response page. This page helps to view the question statistics along
    /// with the video. This class inherits Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class AssessInterviewResponse : PageBase
    {
        #region Protected Variables
        protected string videoURL;
        protected string questionID;
        #endregion Protected Variables

        #region Events Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Assess Interview Question Response");

                AssessInterviewResponse_successMessageLabel.Text = string.Empty;
                AssessInterviewResponse_errorMessageLabel.Text = string.Empty;

                // Load the skill values.
                if (!IsPostBack)
                {
                    /*string d = Request.QueryString.Get("sessionkey");
                    string Q = Request.QueryString["testquestionid"];*/
                    LoadValues();

                    AssessInterviewResponse_downloadVideoLinkButton.Attributes.Add
                       ("onclick", "javascript:DownloadVideo('" + 
                       Request.QueryString["candidatesessionid"]+ "','" +
                       Request.QueryString["attemptid"]+ "','" +
                       Request.QueryString["testquestionid"]+ "');");

                    string serverURL = ConfigurationManager.AppSettings["VIDEO_STREAMING_SERVER_URL"].ToString();
                    videoURL = string.Concat(serverURL, Request.QueryString["candidatesessionid"], "_" + Request.QueryString["attemptid"]);
                    questionID = Request.QueryString["testquestionid"].ToString();

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(AssessInterviewResponse_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void AssessInterviewResponse_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                SaveRatingComments();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessInterviewResponse_errorMessageLabel, exp.Message);
            }
        }

        private void SaveRatingComments()
        {
            QuestionDetail questionDetails = new QuestionDetail();

            questionDetails.Comments = InterviewCandidateTestDetails_userCommentsTextBox.Text.Trim();
            questionDetails.Rating = Convert.ToDecimal(AssessInterviewResponse_rating.CurrentRating.ToString());
            questionDetails.QuestionKey = Request.QueryString.Get("questionKey").ToString();

            new InterviewReportBLManager().SaveQuestionDetailsWithRatingComments(questionDetails, Request.QueryString.Get("candidatesessionid").ToString(),
                Convert.ToInt32(Request.QueryString.Get("attemptid").ToString()),
                Convert.ToInt32(Request.QueryString.Get("assessorID").ToString()), base.userID);

            base.ShowMessage(AssessInterviewResponse_successMessageLabel, "Comments and rating saved successfully");
        }

        protected void AssessInterviewResponse_cancelLinkButton_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Closeform", "<script language='javascript'>self.close();</script>", false);
        }

        protected void AssessInterviewResponse_downloadVideoLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                string downloadPath = "~/Common/Download.aspx?type=IV" +
                    "&candidatesessionid=" + Request.QueryString["candidatesessionid"] +
                    "&attemptid=" + Request.QueryString["attemptid"] +
                    "&testquestionid=" + Request.QueryString["testquestionid"];

                Response.Redirect(downloadPath, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Events Handlers

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden protected method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
            string candidateSessionID = null;
            string questionKey = string.Empty;
            string interviewKey = string.Empty;

            // Check if candidate session ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]))
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "Candidate session ID is not present");
                return;
            }
            candidateSessionID = Request.QueryString["candidatesessionid"].Trim();

            // Check if attempt ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "Attempt ID is not present");
                return;
            }

            // Check if attempt ID is valid.
            int attemptID = 0;
            if (int.TryParse(Request.QueryString["attemptid"].Trim(), out attemptID) == false)
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "Attempt ID is invalid");
                return;
            }

            // Check if test question ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["testquestionid"]))
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "Test question ID is not present");
                return;
            }

            // Check if test question ID is valid.
            int testQuestionID = 0;
            if (int.TryParse(Request.QueryString["testquestionid"].Trim(), out testQuestionID) == false)
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "Test question ID is invalid");
                return;
            }

            // Check if interview key is given 
            if (Utility.IsNullOrEmpty(Request.QueryString["interviewKey"]))
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "Interview Key is not present");
                return;
            }
            interviewKey = Request.QueryString["interviewKey"].ToString();

            // Check if question key is given 
            if (Utility.IsNullOrEmpty(Request.QueryString["questionKey"]))
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "Question Key is not present");
                return;
            }
            questionKey = Request.QueryString["questionKey"].ToString();

            AssessInterviewResponse_saveButton.Enabled = true;
            /*if (Session["OSTSessionKey"] != null)
            {
                string _ostSessionkey = Convert.ToString(Session["OSTSessionKey"]);
                string _testQuestionID=Convert.ToString(Request.QueryString["testquestionid"]);
                string _assessorID = Convert.ToString(Request.QueryString["assessorID"]);

                //interviewKey, candidateSessionID
                if (new InterviewReportBLManager().GetAssessorSkills(
                    _ostSessionkey, _testQuestionID, _assessorID) > 0)
                {
                    AssessInterviewResponse_saveButton.Enabled = true;
                }
                else
                { AssessInterviewResponse_saveButton.Enabled = false; }
            }*/

            // Get interview question attributes
            QuestionDetail questionAttributes = new InterviewReportBLManager().
                GetInterviewTestQuestionAttributesByKeys(interviewKey, questionKey);
                        
            if (questionAttributes != null)
            {
                //Set maximum rating star
                if (Convert.ToInt32(questionAttributes.Rating) > 0)
                    AssessInterviewResponse_rating.MaxRating = Convert.ToInt32(questionAttributes.Rating);
                else
                    AssessInterviewResponse_rating.MaxRating = 10;
                //Set question comments
                InterviewCandidateTestDetails_commentValueLabel.Text = questionAttributes.Comments;
            }

            // Get interview response
            InterviewResponseDetail logDetail = new InterviewReportBLManager().
                GetInterviewResponseDetail(candidateSessionID, attemptID, testQuestionID, base.userID);

            if (logDetail == null)
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "No data found to display");

                return;
            }
            // Assign details.
            AssessInterviewResponse_questionValueLabel.Text =TrimContent(logDetail.Question,220);
            lblPopupQuestion.Text = logDetail.Question;
            AssessInterviewResponse_candidateCommentsValueLabel.Text =TrimContent( logDetail.CandidateComments,330);
            lblCandidateComments.Text = logDetail.CandidateComments;
            AssessInterviewResponse_timeTakenValueLabel.Text =
                Utility.ConvertSecondsToHoursMinutesSeconds((int)logDetail.ResponseTime);
            AssessInterviewResponse_statusValueLabel.Text = logDetail.Status;
            AssessInterviewResponse_skippedValueLabel.Text = logDetail.Skipped ? "Yes" : "No";
            AssessInterviewResponse_complexityValueLabel.Text = logDetail.Complexity;
            AssessInterviewResponse_testAreaValueLabel.Text = logDetail.TestArea;

            #region Video Commented
            //Validate will be done through Flex
            //// Load video.
            //string path = ConfigurationManager.AppSettings["INTERVIEW_VIDEO_URL"] +
            //    candidateSessionID + "_" + attemptID + "/" + testQuestionID +
            //    "." + ConfigurationManager.AppSettings["VIDEO_FILE_EXTENSION"];

            //// Check if video exist or not. This is a untested code
            //// so anyway assign the video to the player.
            //if (!IsRemoteFileExists(path))
            //{
            //    AssessInterviewResponse_downloadVideoLinkButton.Visible = false;
            //    ShowMessage(AssessInterviewResponse_errorMessageLabel,
            //        "Interview video does not exist");
            //}
            //else
            //{
            //    AssessInterviewResponse_downloadVideoLinkButton.Visible = true;
            //}
            #endregion Video Commented

            // Set the video file path.
            //AssessInterviewResponse_video.FilePath = path;
            if (logDetail.RatingComments == null)
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel, "");
                return;
            }
            for (int i = 0; i < logDetail.RatingComments.Count; i++)
            {
                if (logDetail.RatingComments[i].assessorID == base.userID)
                {
                    if (logDetail.RatingComments[i].CandidateSessionId == candidateSessionID)
                        AssessInterviewResponse_rating.CurrentRating = Convert.ToInt32(logDetail.RatingComments[i].Rating);

                    InterviewCandidateTestDetails_userCommentsTextBox.Text = logDetail.RatingComments[i].userComments;
                    logDetail.RatingComments.RemoveAt(i);
                }
            }
            AssessInterviewResponse_otherAssessorGridview.DataSource = logDetail.RatingComments;
            AssessInterviewResponse_otherAssessorGridview.DataBind(); 

        }

        #endregion Protected Overridden Methods

        #region Private Methods

        /// <summary>
        /// Method that checks if the given remote file exists or not.
        /// </summary>
        /// <param name="url">
        /// A <see cref="string"/> that holds the remote file URL.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the exist status. True if exists
        /// and false otherwise.
        /// </returns>
        private bool IsRemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";

                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                //Returns TURE if the Status code == 200
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                // Any exception will returns false.
                return false;
            }
        }

        #endregion Private Methods
    }
}
