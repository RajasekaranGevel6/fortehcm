﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchUser.aspx.cs
// File that represents the SearchUser class that defines the user interface
// layout and functionalities for the SearchUser page. This page helps in 
// searching for users by providing search criteria for user name, first name, 
// middle name and last name This class inherits Forte.HCM.UI.Common.PageBase
// class.

#endregion

#region Directives

using System;
using System.Text;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

using Resources;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the SearchCandidate page. This page helps in searching for candidates by
    /// providing search criteria for user name, first name, middle name and
    /// last name. This class inherits the Forte.HCM.UI.Common.PageBase 
    /// class.
    /// </summary>
    public partial class SearchCandidate : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "274px";

        #endregion Private Constants

        #region Events Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SearchCandidate_successMessageLabel.Text = "";
                SearchCandidate_errorMessageLabel.Text = "";
                SearchCandidate_createUserNameErrorMessageLabel.Text = "";
                SearchCandidate_createUserNameSuccessMessageLabel.Text = "";

                // Set browser title.
                Master.SetPageCaption("Search Candidate");

                // Set default button and focus.
                Page.Form.DefaultButton = SearchCandidate_searchButton.UniqueID;

                // Subscribes to the page number click event.
                SearchCandidate_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchCandidate_pageNavigator_PageNumberClick);

                // 
                if (!Utility.IsNullOrEmpty(SearchCandidate_isMaximizedHiddenField.Value) &&
                    SearchCandidate_isMaximizedHiddenField.Value == "Y")
                {
                    SearchCandidate_searchCriteriasDiv.Style["display"] = "none";
                    SearchCandidate_searchResultsUpSpan.Style["display"] = "block";
                    SearchCandidate_searchResultsDownSpan.Style["display"] = "none";
                    SearchCandidate_testDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchCandidate_searchCriteriasDiv.Style["display"] = "block";
                    SearchCandidate_searchResultsUpSpan.Style["display"] = "none";
                    SearchCandidate_searchResultsDownSpan.Style["display"] = "block";
                    SearchCandidate_testDiv.Style["height"] = RESTORED_HEIGHT;
                }

                if (!IsPostBack)
                {
                    // Set default focus.
                    Page.Form.DefaultFocus = SearchCandidate_userNameTextBox.UniqueID;
                    SearchCandidate_userNameTextBox.Focus();

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "USERNAME";

                    SearchCandidate_testDiv.Visible = false;
                }
                else
                {
                    SearchCandidate_testDiv.Visible = true;
                }

                SearchCandidate_searchTestResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    SearchCandidate_testDiv.ClientID + "','" +
                    SearchCandidate_searchCriteriasDiv.ClientID + "','" +
                    SearchCandidate_searchResultsUpSpan.ClientID + "','" +
                    SearchCandidate_searchResultsDownSpan.ClientID + "','" +
                    SearchCandidate_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchCandidate_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = "CREATED_DATE";

                // Reset the paging control.
                SearchCandidate_pageNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void SearchCandidate_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchCandidate_userNameTextBox.Text = string.Empty;
                SearchCandidate_emailTextBox.Text = string.Empty;
                SearchCandidate_nameTextBox.Text = string.Empty;

                SearchCandidate_testGridView.DataSource = null;
                SearchCandidate_testGridView.DataBind();

                SearchCandidate_pageNavigator.PageSize = base.GridPageSize;
                SearchCandidate_pageNavigator.TotalRecords = 0;
                SearchCandidate_testDiv.Visible = false;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "USERNAME";

                // Reset to the restored state.
                SearchCandidate_searchCriteriasDiv.Style["display"] = "block";
                SearchCandidate_searchResultsUpSpan.Style["display"] = "none";
                SearchCandidate_searchResultsDownSpan.Style["display"] = "block";
                SearchCandidate_testDiv.Style["height"] = RESTORED_HEIGHT;
                SearchCandidate_isMaximizedHiddenField.Value = "N";

                // Clear message fields.
                

                // Set default focus.
                Page.Form.DefaultFocus = SearchCandidate_userNameTextBox.UniqueID;
                SearchCandidate_userNameTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchCandidate_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                Search(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchCandidate_testGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                SearchCandidate_pageNavigator.Reset();
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchCandidate_testGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                // Set row styles.
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                ImageButton SearchCandidate_candidateDetailGridView_addNotesImageButton = e.Row.FindControl
                    ("SearchCandidate_candidateDetailGridView_addNotesImageButton")as ImageButton;

                ImageButton SearchCandidate_candidateDetailGridView_viewCandidateActivityImageButton = e.Row.FindControl
                    ("SearchCandidate_candidateDetailGridView_viewCandidateActivityImageButton") as ImageButton;

                int candidateID = Convert.ToInt32((e.Row.FindControl("SearchCandidate_candidateDetailGridView_candidateIDHiddenField") as HiddenField).Value);

                // Assign events to 'add notes' icon.
                SearchCandidate_candidateDetailGridView_addNotesImageButton.Attributes.Add
                    ("OnClick", "javascript:return OpenAddNotesPopup('" + candidateID + "')");
                
                // Assign events to 'view candidate activity log' icon.
                SearchCandidate_candidateDetailGridView_viewCandidateActivityImageButton.Attributes.Add
                    ("OnClick", "javascript:return OpenViewCandidateActivityLopPopup('" + candidateID + "')");

                HyperLink SearchCandidate_viewCandidateActivityDashboardHyperLink = (HyperLink)e.Row.FindControl("SearchCandidate_viewCandidateActivityDashboardHyperLink");
                if (SearchCandidate_viewCandidateActivityDashboardHyperLink != null)
                {
                    SearchCandidate_viewCandidateActivityDashboardHyperLink.NavigateUrl = "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&candidateid=" + candidateID;
                }

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchCandidate_testGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchCandidate_testGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCandidate_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods

        private void Search(int pageNumber)
        {
            int totalRecords = 0;

            List<UserDetail> users = new CommonBLManager().GetCandidates
                (SearchCandidate_userNameTextBox.Text.Trim(), SearchCandidate_nameTextBox.Text.Trim(), SearchCandidate_nameTextBox.Text.Trim(),
                SearchCandidate_emailTextBox.Text.Trim(),
                ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]),
                pageNumber,
                base.GridPageSize,
                out totalRecords, base.tenantID);

            if (users == null || users.Count == 0)
            {
                SearchCandidate_testDiv.Visible = false;
                ShowMessage(SearchCandidate_errorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                SearchCandidate_testDiv.Visible = true;
            }

            SearchCandidate_testGridView.DataSource = users;
            SearchCandidate_testGridView.DataBind();
            SearchCandidate_pageNavigator.PageSize = base.GridPageSize;
            SearchCandidate_pageNavigator.TotalRecords = totalRecords;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
        protected void SearchCandidate_createUserNamePanel_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckCandidateUserDetails())
                {
                    if (!string.IsNullOrEmpty
                       (SearchCandidate_createUserNamePanel_passwordTextBox.Text.Trim()) &&
                       (!string.IsNullOrEmpty
                       (SearchCandidate_createUserNamePanel_retypePasswordTextBox.Text.Trim())))
                    {
                        if (SearchCandidate_createUserNamePanel_passwordTextBox.Text.Trim() !=
                            SearchCandidate_createUserNamePanel_retypePasswordTextBox.Text.Trim())
                        {
                            base.ShowMessage(SearchCandidate_createUserNameErrorMessageLabel,
                               HCMResource.SearchCandidateRepository_EnterCorrectPassword);
                            SearchCandidate_createUserModalPopupExtender.Show();

                            return;
                        }
                        if (!IsValidEmailAddress(SearchCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
                        {
                            base.ShowMessage(SearchCandidate_createUserNameErrorMessageLabel,
                                    HCMResource.NewCandidate_EnterValidEmailID);
                            SearchCandidate_createUserModalPopupExtender.Show();
                            return;
                        }

                        if (!CheckForEmailAddressAvailability
                            (SearchCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
                        {
                            base.ShowMessage(SearchCandidate_createUserNameErrorMessageLabel,
                            HCMResource.UserRegistration_UserEmailNotAvailable);
                            SearchCandidate_createUserNamePanel_userEmailAvailableStatusDiv.Style.Add
                                ("display", "block");
                            SearchCandidate_createUserModalPopupExtender.Show();
                            return;
                        }


                        SearchCandidate_pageNavigator.Reset();
                        SaveUserName();

                        ViewState["SORT_FIELD"] = "CREATED_DATE";
                        ViewState["SORT_ORDER"] = SortType.Descending;
                        Search(1);

                        base.ShowMessage(SearchCandidate_successMessageLabel,
                            HCMResource.SearchCandidateRepository_UserNameCreatedSuccessfully);
                    }
                }
                else
                {
                    base.ShowMessage(SearchCandidate_createUserNameErrorMessageLabel,
                                HCMResource.SearchCandidateRepository_EnterMandatoryField);
                    SearchCandidate_createUserModalPopupExtender.Show();
                    return;
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCandidate_errorMessageLabel,
                     exception.Message);
            }
        }
        protected void SearchCandidate_createUserNamePanel_checkUserEmailIdAvailableButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(SearchCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
                {
                    if (!IsValidEmailAddress(SearchCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
                    {
                        base.ShowMessage(SearchCandidate_createUserNameErrorMessageLabel,
                                HCMResource.NewCandidate_EnterValidEmailID);
                        SearchCandidate_createUserModalPopupExtender.Show();
                        return;
                    }

                    if (CheckForEmailAddressAvailability(SearchCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
                        SearchCandidate_createUserNamePanel_validEmailAvailableStatus.Text = Resources.HCMResource.UserRegistration_UserEmailAvailable;
                    else
                        SearchCandidate_createUserNamePanel_inValidEmailAvailableStatus.Text = HCMResource.UserRegistration_UserEmailNotAvailable;
                    SearchCandidate_createUserNamePanel_userEmailAvailableStatusDiv.Style.Add("display", "block");

                    SearchCandidate_createUserModalPopupExtender.Show();
                }
                else
                {
                    base.ShowMessage(SearchCandidate_createUserNameErrorMessageLabel,
                                HCMResource.SearchCandidateRepository_PleaseEnterTheUsername);
                    SearchCandidate_createUserModalPopupExtender.Show();
                    return;
                }

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCandidate_errorMessageLabel,
                     exception.Message);
            }
        }

        /// <summary>
        /// Saves the name of the user.
        /// </summary>
        private void SaveUserName()
        {
            CandidateInformation candidateInformation = new CandidateInformation();

            candidateInformation.caiFirstName = SearchCandidate_createUserNamePanel_firstNameHiddenField.Value;
            candidateInformation.caiLastName = SearchCandidate_createUserNamePanel_lastNameHiddenField.Value;
            candidateInformation.caiID = int.Parse(
           SearchCandidate_createUserNamePanel_candidateIDHiddenField.Value);
            candidateInformation.UserName = SearchCandidate_createUserNamePanel_userNameTextBox.Text.Trim();
            candidateInformation.Password = new EncryptAndDecrypt().EncryptString(SearchCandidate_createUserNamePanel_passwordTextBox.Text.Trim());
            string confirmationCode = GetConfirmationCode();
            new ResumeRepositoryBLManager().InsertCandidateUsers(base.tenantID, candidateInformation,
                base.userID, confirmationCode);
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }

        /// <summary>
        /// A Method that generates a random string and used 
        /// the encrypted string as confirmation code for a user
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that holds the encrypted string
        /// </returns>
        private string GetConfirmationCode()
        {
            return new EncryptAndDecrypt().EncryptString(RandomString(15));
        }
        private bool CheckForEmailAddressAvailability(string User_Email)
        {
            return new UserRegistrationBLManager().CheckUserEmailExists(User_Email, base.tenantID);
        }

        private bool IsValidEmailAddress(string strUserEmailId)
        {
            //Regex regex = new Regex(".+@.+\\.[a-zA-Z]+", RegexOptions.Compiled);
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$", RegexOptions.Compiled);
            return regex.IsMatch(strUserEmailId);
        }
        protected void SearchCandidate_testGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "AddCandidateID")
                {
                    SearchCandidate_createUserNamePanel_userNameTextBox.Text = string.Empty;

                    SearchCandidate_createUserNamePanel_passwordTextBox.Text = string.Empty;

                    SearchCandidate_createUserNamePanel_retypePasswordTextBox.Text = string.Empty;

                    GridViewRow candidateGridViewRow = (GridViewRow)(((ImageButton)e.CommandSource).Parent.Parent);

                    SearchCandidate_createUserNamePanel_firstNameHiddenField.Value =
                        ((Label)candidateGridViewRow.FindControl("SearchCandidate_candidateDetailGridView_firstNameLabel")).Text;
                    SearchCandidate_createUserNamePanel_lastNameHiddenField.Value =
                       ((Label)candidateGridViewRow.FindControl("SearchCandidate_candidateDetailGridView_lastNameLabel")).Text;
                    SearchCandidate_createUserNamePanel_candidateIDHiddenField.Value =
                       ((HiddenField)candidateGridViewRow.FindControl("SearchCandidate_candidateDetailGridView_candidateIDHiddenField")).Value;

                    SearchCandidate_createUserNamePanel_userNameTextBox.Text =
                ((Label)candidateGridViewRow.FindControl("SearchCandidate_candidateDetailGridView_emailLabel")).Text;

                    SearchCandidate_createUserModalPopupExtender.Show();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchCandidate_errorMessageLabel,
                     exception.Message);
            }
        }
        /// <summary>
        /// Gets the visibility.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        protected bool GetVisibility(string username)
        {
            return username.Length == 0 ? true : false;
        }

        /// <summary>
        /// Gets the visibility.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        protected bool GetInverseVisibility(string username)
        {
            return username.Length == 0 ? false : true;
        }


        /// <summary>
        /// Checks the candidate user details.
        /// </summary>
        /// <returns></returns>
        private bool CheckCandidateUserDetails()
        {
            bool value = true;

            if (string.IsNullOrEmpty(SearchCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
            {
                value = false;
            }
            if (string.IsNullOrEmpty(SearchCandidate_createUserNamePanel_passwordTextBox.Text.Trim()))
            {
                value = false;
            }
            if (string.IsNullOrEmpty(SearchCandidate_createUserNamePanel_retypePasswordTextBox.Text.Trim()))
            {
                value = false;
            }
            return value;
        }
    }
}

