﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewInterviewQuestion.cs
// File that represents the user interface layout and functionalities for
// the ViewInterviewQuestion page. This will helps to view interview
// question detail such as question, test area, complexity, correct 
// answer and even question image if associated.

#endregion Header                                                              

#region Directives                                                             

using System;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ViewInterviewQuestion page. This will helps to view interview
    /// question detail such as question, test area, complexity, correct 
    /// answer and even question image if associated. This class inherits 
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ViewInterviewQuestion : PageBase
    {
        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.SetFocus(ViewInterviewQuestion_bottomCloseLinkButton.ClientID);
                
                // Set browser title.
                Master.SetPageCaption("Interview Question Detail");

                if (!IsPostBack)
                {
                    // Load question detail.
                    LoadQuestionDetail();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewInterviewQuestion_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the question detail.
        /// </summary>
        public void LoadQuestionDetail()
        {
            if (Utility.IsNullOrEmpty(Request.QueryString["questionkey"]))
            {
                base.ShowMessage(ViewInterviewQuestion_errorMessageLabel, "Question key is not passed");
                return;
            }

            // Retrieve interview question detail.
            QuestionDetail questionDetail = new QuestionBLManager().
                GetInterviewQuestion(Request.QueryString["questionkey"].Trim());

            // Assign details.
            ViewInterviewQuestion_testAreaValueLabel.Text = questionDetail.TestAreaName;
            ViewInterviewQuestion_complexityValueLabel.Text = questionDetail.ComplexityName;
            ViewInterviewQuestion_tagValueLabel.Text = questionDetail.Tag;
            ViewInterviewQuestion_questionLabel.Text = questionDetail.Question;

            // Assign answer choices.
            ViewInterviewQuestion_answerChoicesPlaceHolder.DataBind();
            ViewInterviewQuestion_answerChoicesPlaceHolder.Controls.Add
                (new ControlUtility().GetAnswerChoices(questionDetail.AnswerChoices, false));

            // Assign question image if available.
            if (questionDetail.HasImage)
            {
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = questionDetail.QuestionImage as byte[];
                ViewInterviewQuestion_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=INTERVIEW_QUESTION_IMAGE&questionKey=" + questionDetail.QuestionKey;
                ViewInterviewQuestion_questionImage.Visible = true;
                ViewInterviewQuestion_questionDiv.Style.Add("height", "220px");
            }
            else
            {
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
                ViewInterviewQuestion_questionImage.Visible = false;
                ViewInterviewQuestion_questionDiv.Style.Add("height", "100%");
            }
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Protected Overridden Methods
    }
}
