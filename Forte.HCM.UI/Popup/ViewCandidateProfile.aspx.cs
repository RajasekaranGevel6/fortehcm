﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewCandidateProfile.aspx.cs
// File that represents the user interface layout and functionalities for
// the ViewCandidateProfile page. This will helps to view the candidate 
// profile details.

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ViewCandidateProfile page. This will helps to view the candidate 
    /// profile details. This class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ViewCandidateProfile : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that is be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Candidate Profile");

                if (!IsPostBack)
                {
                    // Call and assign query string values to hidden fields. OpenAddNotesPopup(
                    if (RetrieveQueryStringValues() == false)
                        return;

                    // Add handler to view candidate activity log popup.
                    ViewCandidateProfile_activityLogLinkButton.Attributes.Add
                        ("onclick", "return OpenViewCandidateActivityLopPopup('" + Request.QueryString["candidateid"].ToString() + "');");

                    // Add handler to add notes popup.
                    ViewCandidateProfile_addNotesLinkButton.Attributes.Add
                        ("onclick", "return OpenAddNotesPopup('" + Request.QueryString["candidateid"].ToString() + "');");

                    // Load Candidate details.
                    LoadCandidateDetail(Convert.ToInt32(ViewCandidateProfile_candidateIDHiddenField.Value));

                    ViewCandidateProfile_viewCandidateActivityDashboardHyperLink.NavigateUrl = "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&candidateid=" + Request.QueryString["candidateid"];
                }

                // Show menu type.
                ShowMenuType();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCandidateProfile_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that retrieves the query string values and assign it to 
        /// hidden fields.
        /// </summary>
        private bool RetrieveQueryStringValues()
        {
            if (Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
            {
                base.ShowMessage(ViewCandidateProfile_topErrorMessageLabel, "Candidate ID is not passed");
                return false;
            }

            // Get Candidate ID.
            int candidateID = 0;
            int.TryParse(Request.QueryString["candidateid"], out candidateID);
            if (candidateID == 0)
            {
                base.ShowMessage(ViewCandidateProfile_topErrorMessageLabel, "Invalid Candidate ID");
                return false;
            }

            // Assign assessor ID to hidden field.
            ViewCandidateProfile_candidateIDHiddenField.Value = candidateID.ToString();

            return true;
        }

        /// <summary>
        /// Method that loads the candidate details.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        private void LoadCandidateDetail(int candidateID)
        {
            string imgCheckPath = string.Empty;
            Session["DummyPath"] = string.Empty;
            Session["DummyName"] = string.Empty;
            
            // Get candidate detail.
            CandidateDetail candidateDetail = new CandidateBLManager().GetCandidateBasicDetail(candidateID); 

            if (candidateDetail == null)
            {
                base.ShowMessage(ViewCandidateProfile_topErrorMessageLabel, "No such candidate exist");
                return;
            }
                 
            // Assign candidate details.
            ViewCandidateProfile_nameValueLabel.Text = candidateDetail.FirstName;
            ViewCandidateProfile_addressValueLabel.Text = candidateDetail.Address;
            ViewCandidateProfile_locationValueLabel.Text = candidateDetail.Location;
            ViewCandidateProfile_emailValueLabel.Text = candidateDetail.EMailID;
            ViewCandidateProfile_mobilePhoneValueLabel.Text = candidateDetail.Mobile;
            ViewCandidateProfile_homePhoneValueLabel.Text = candidateDetail.Phone;

            // Assign LinkedIn profile.
            if (Utility.IsNullOrEmpty(candidateDetail.LinkedInProfile))
            {
                ViewCandidateProfile_linkedInProfileHyperLink.Visible = false;
            }
            else
            {
                ViewCandidateProfile_linkedInProfileHyperLink.Visible = true;
                ViewCandidateProfile_linkedInProfileHyperLink.NavigateUrl = candidateDetail.LinkedInProfile;
            }  
             
            // Assign candidate image.
            ViewCandidateProfile_photoImage.ImageUrl = "~/Common/CandidateImageHandler.ashx?source=CAS_PAGE&candidateid=" + candidateID;

            // Load HRXML to assign executive summary.
            string hrXMLSource = new CandidateBLManager().GetCandidateHRXLDetail(candidateID);

            if (!Utility.IsNullOrEmpty(hrXMLSource))
            {
                try
                {
                    Resume resume = new HRXMLManager(hrXMLSource).Resume;

                    if (resume != null)
                        ViewCandidateProfile_synopsisValueLabel.Text = resume.ExecutiveSummary;
                }
                finally
                {
                }
            }

            // Assign handler for download resume.
            string mimeType;
            string fileName;
            Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT] = null;

            byte[] resumeContent = new CandidateBLManager().GetResumeContentsByCandidiateID(candidateID,
                out mimeType, out fileName);

            // Check if resume contents are present. If not present, hide the download
            // resume option.
            if (resumeContent == null || resumeContent.Length == 0)
            {
                ViewCandidateProfile_downloadResumeImageButton.Visible = false;
                return;
            }
            ViewCandidateProfile_downloadResumeImageButton.Visible = true;

            // Keep resume contents in session.
            Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT] = resumeContent;

            // Add handler to download candidate resume.
            ViewCandidateProfile_downloadResumeImageButton.Attributes.Add
                ("onclick", "javascript:DownloadCandidateResume('" + fileName + "','" + mimeType + "');");
        }

        /// <summary>
        /// Method that shows the menu type and respective section based on the 
        /// selection.
        /// </summary>
        private void ShowMenuType()
        { 
            // Get menu type.
            string menuType = ViewCandidateProfile_selectedMenuHiddenField.Value;

            // Hide all div.
            ViewCandidateProfile_contactInfoDiv.Attributes["style"] = "display:none";
            ViewCandidateProfile_synopsisDiv.Attributes["style"] = "display:none";

            // Reset color.
            ViewCandidateProfile_contactInfoLinkButton.CssClass = "assessor_profile_link_button";
            ViewCandidateProfile_synopsisLinkButton.CssClass = "assessor_profile_link_button";
            
            if (menuType == "CI")
            {
                ViewCandidateProfile_contactInfoDiv.Attributes["style"] = "display:block";
                ViewCandidateProfile_contactInfoLinkButton.CssClass = "assessor_profile_link_button_selected";
            }
            else if (menuType == "SY")
            {
                ViewCandidateProfile_synopsisDiv.Attributes["style"] = "display:block";
                ViewCandidateProfile_synopsisLinkButton.CssClass = "assessor_profile_link_button_selected";
            }
        }

        #endregion Private Methods
    }
}