<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="ViewClient.aspx.cs" Title="View Client" Inherits="Forte.HCM.UI.Popup.ViewClient" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ViewClient_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="ViewClient_headerLiteral" runat="server" Text="Client Detail"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="ViewClient_topCloseImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:window.close();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_view_client_bg">
                    <tr>
                        <td class="msg_align" valign="top">
                            <asp:Label ID="ViewClient_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="ViewClient_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="popup_td_padding_10" style="width: 100%" valign="top">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td colspan="3">
                                        <asp:Label ID="ViewClient_clientNameLabel" runat="server" Text="Client Name" ToolTip="Client Name"
                                            SkinID="sknLabelViewClientNameFieldText"></asp:Label>
                                    </td>
                                    <td valign="bottom" align="right">
                                        <asp:HyperLink ID="ViewClient_moreHyperLink" runat="server" ToolTip="Click here to view more details on the client" 
                                            Text="more..." Target="_blank" SkinID="sknMoreOrangeHyperLink"></asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:HyperLink ID="ViewClient_emailIDHyperLink" runat="server" ToolTip="Click here to send a mail to client"></asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 14%">
                                        <asp:Label ID="ViewClient_phoneNoHeadLabel" runat="server" Text="Phone No" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:Label ID="ViewClient_phoneNoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td style="width: 14%">
                                        <asp:Label ID="ViewClient_faxNoHeadLabel" runat="server" Text="Fax No" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:Label ID="ViewClient_faxNoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="ViewClient_streetAddressHeadLabel" runat="server" Text="Street Address"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClient_streetAddressLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClient_cityHeadLabel" runat="server" Text="City" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClient_cityLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="ViewClient_stateHeadLabel" runat="server" Text="State Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClient_statelabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClient_countryHeadLabel" runat="server" Text="Country" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClient_countryLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="ViewClient_zipHeadLabel" runat="server" Text="Zip Code" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClient_zipLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClient_feinNoHeadLabel" runat="server" Text="FEIN NO" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClient_feinNoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="ViewClient_wesiteURLHeadLabel" runat="server" Text="Website URL" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:HyperLink ID="ViewClient_websiteURLHyperLink" runat="server" Target="_blank" ToolTip="Click here to navigate to the URL" SkinID="sknURLHyperLink"></asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="width: 14%" align="left" valign="top">
                                                    <asp:Label ID="ViewClient_additionalInfoHeadLabel" runat="server" Text="Additional Info"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 85%" align="left" valign="top">
                                                    <div style="height: 50px; width: 100%; overflow: auto;">
                                                        <asp:Label ID="ViewClient_additionalInfoLabel" runat="server" Text="Additional Info"
                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 8px" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 14%" align="left" valign="top">
                                                    <asp:Label ID="ViewClient_departmentsHeadLabel" runat="server" Text="Departments"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 85%" align="left" valign="top">
                                                    <div style="height: 20px; width: 100%; overflow: auto;" >
                                                    <asp:Label ID="ViewClient_departmentsLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 8px" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 14%" align="left" valign="top">
                                                    <asp:Label ID="ViewClient_contactsHeadLabel" runat="server" Text="Contacts"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 85%" align="left" valign="top">
                                                    <div style="height: 20px; width: 100%; overflow: auto;" >
                                                    <asp:Label ID="ViewClient_contactsLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_5">
                <asp:LinkButton ID="ViewClient_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClientClick="javascript:window.close();" ToolTip="Click here to close the window" />
            </td>
        </tr>
    </table>
</asp:Content>
