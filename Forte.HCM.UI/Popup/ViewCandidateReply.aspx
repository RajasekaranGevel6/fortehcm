﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewCandidateReply.aspx.cs" Inherits="Forte.HCM.UI.Popup.ViewCandidateReply"
    MasterPageFile="~/MasterPages/PopupMaster.Master" %>

<asp:Content ID="ViewCandidateReply_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="ViewCandidateReply_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchUser_headerLiteral" runat="server" Text="Preview"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SearchUser_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="ViewCandidateReply_updatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                            <tr>
                                <td class="msg_align">
                                    <asp:UpdatePanel ID="CyberProctorLog_topMessageUpdatePanel" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="CyberProctorLog_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0" runat="server" id="Page_Tab">
                                        <tr>
                                            <td style="width: 46%" align="right">
                                                <asp:ImageButton ID="ViewCandidateReply_prevImageButton" runat="server" SkinID="sknViewPreviousImageButton"
                                                    ToolTip="Previous" OnClick="ViewCandidateReply_prevImageButton_Click" />
                                            </td>
                                            <td style="width: 8%" align="center">
                                                <asp:Label ID="ViewCandidateReply_imagePaging" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                            <td style="width: 46%" align="left">
                                                <asp:ImageButton ID="ViewCandidateReply_nextImageButton" runat="server" SkinID="sknViewNextImageButton"
                                                    ToolTip="Next" OnClick="ViewCandidateReply_nextImageButton_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Image ID="ViewCandidateReply_viewImage" runat="server" Height="400px" Width="600px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:LinkButton ID="SearchUser_topCancelLinkButton" runat="server" Text="Cancel"
                    SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
            </td>
        </tr>
    </table>
</asp:Content>
