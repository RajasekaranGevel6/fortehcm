<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchOnlineInterview.aspx.cs"
    Inherits="Forte.HCM.UI.Popup.SearchOnlineInterview" MasterPageFile="~/MasterPages/PopupMaster.Master" %>    
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ID="SearchOnlineInterview_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">

    <script type="text/javascript">
        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl)
        {
            var from = '<%= Request.QueryString["from"] %>';
            var ctrlId = '<%= Request.QueryString["ctrlid"] %>';
            var ctrlName = '<%= Request.QueryString["ctrlhidId"] %>';
            var btncnrl = '<%= Request.QueryString["btncnrl"] %>';

            if (window.dialogArguments) 
            {
                window.opener = window.dialogArguments;
            }
            if (ctrlId != null && ctrlId != '')
            {
                // Replace the 'link button name' with the 
                // 'hidden field name' and set the value in opener control.
                window.opener.document.getElementById(ctrlId).value
                    = document.getElementById(ctrl.id.replace("SearchOnlineInterview_selectLinkButton", "SearchOnlineInterview_interviewNameHiddenField")).value;

                window.opener.document.getElementById(ctrlName).value
                    = document.getElementById(ctrl.id.replace("SearchOnlineInterview_selectLinkButton", "SearchOnlineInterview_interviewKeyHiddenField")).value;
            }
            // Trigger the click event of the refresh button in the parent page.
            if (btncnrl != null && window.opener.document.getElementById(btncnrl) != null)
                window.opener.document.getElementById(btncnrl).click();

            self.close();
        }
    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchOnlineInterview_headerLiteral" runat="server" Text="Search Online Interview"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="QuestionDetailPreviewControl_topCancelImageButton" runat="server"
                                SkinID="sknCloseImageButton" OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                    <tr>
                        <td class="msg_align" colspan="2">
                            <asp:UpdatePanel ID="SearchOnlineInterview_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="SearchOnlineInterview_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="SearchOnlineInterview_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <div id="SearchOnlineInterview_searchCriteriasDiv" runat="server" style="display: block;">
                                            <table width="100%" border="0" cellspacing="3" cellpadding="2">
                                                <tr>
                                                    <td>
                                                        <table style="width: 100%" cellpadding="2" cellspacing="0">
                                                        <asp:UpdatePanel ID="SearchOnlineInterview_searchCriteriaUpdatePanel" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <tr>
                                                                    <td style="width: 22%">
                                                                        <asp:Label ID="SearchOnlineInterview_interviewKeyLabel" runat="server" Text="Interview Key" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <asp:TextBox ID="SearchOnlineInterview_interviewKeyTextBox" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="SearchOnlineInterview_interviewNameLabel" runat="server" Text="Interview Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="SearchOnlineInterview_interviewNameTextBox" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="SearchOnlineInterview_positionProfileLabel" runat="server" Text="Position Profile"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="SearchOnlineInterview_positionProfileTextBox" ReadOnly="true" runat="server"></asp:TextBox>
                                                                        </div>
                                                                        &nbsp;
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="SearchOnlineInterview_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                runat="server" ImageAlign="Middle" />
                                                                        </div>
                                                                        <asp:HiddenField ID="SearchOnlineInterview_positionProfileIDHiddenField" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="SearchOnlineInterview_sessionAuthorNameHeadLabel" runat="server" Text="Interview Author"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="SearchOnlineInterview_sessionAuthorNameTextBox" runat="server" ReadOnly="true"
                                                                                Text=""></asp:TextBox>
                                                                            <asp:HiddenField ID="SearchOnlineInterview_sessionAuthorNameHiddenField" runat="server" />
                                                                            <asp:HiddenField ID="SearchOnlineInterview_sessionAuthorIDHiddenField" runat="server" />
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="SearchOnlineInterview_sessionAuthorNameImageButton" SkinID="sknbtnSearchicon"
                                                                                Visible="false" runat="server" ImageAlign="Middle" Width="16px" ToolTip="Click here to select the Interview session author" /></div>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="SearchOnlineInterview_schedulerNameHeadLabel" runat="server" Text="Interview Schedule Creator"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="SearchOnlineInterview_schedulerNameTextBox" runat="server" ReadOnly="true"
                                                                                Text=""></asp:TextBox>
                                                                            <asp:HiddenField ID="SearchOnlineInterview_schedulerIDHiddenField" runat="server" />
                                                                            <asp:HiddenField ID="SearchOnlineInterview_schedulerHiddenField" runat="server" />
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="SearchOnlineInterview_schedulerNameImageButton" SkinID="sknbtnSearchicon"
                                                                                runat="server" ImageAlign="Middle" Width="16px" ToolTip="Click here to select the interview schedule creator" /></div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" align="right">
                                                                        <asp:Button ID="SearchOnlineInterview_topSearchButton" runat="server" Text="Search" OnClick="SearchOnlineInterview_SearchButton_Click"
                                                                            SkinID="sknButtonId" />
                                                                    </td>
                                                                </tr>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="SearchOnlineInterview_searchTestResultsTR" runat="server">
                                    <td id="Td1" class="header_bg" align="center" runat="server">
                                        <asp:UpdatePanel ID="SearchCategory_searchResultsUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%">
                                                            <asp:Literal ID="SearchOnlineInterview_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>
                                                            &nbsp;<asp:Label ID="SearchOnlineInterview_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                                Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%" align="right">
                                                            <span id="SearchOnlineInterview_searchResultsUpSpan" style="display: none;" runat="server">
                                                                <asp:Image ID="SearchOnlineInterview_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                            </span><span id="SearchOnlineInterview_searchResultsDownSpan" style="display: block;"
                                                                runat="server">
                                                                <asp:Image ID="SearchOnlineInterview_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="left">
                                                    <asp:UpdatePanel ID="SearchOnlineInterview_updatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <div style="height: 200px; overflow: auto;" runat="server" id="SearchOnlineInterview_testDiv">
                                                                <asp:GridView ID="SearchOnlineInterview_interviewGridView" runat="server" AllowSorting="True"
                                                                    AutoGenerateColumns="False" GridLines="None" Width="100%" OnSorting="SearchOnlineInterview_interviewGridView_Sorting"
                                                                    OnRowCreated="SearchOnlineInterview_interviewGridView_RowCreated" OnRowDataBound="SearchOnlineInterview_interviewGridView_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="SearchOnlineInterview_selectLinkButton" runat="server" Text="Select"
                                                                                    OnClientClick="javascript:return OnSelectClick(this);" ToolTip="Select"></asp:LinkButton>
                                                                                <asp:HiddenField ID="SearchOnlineInterview_interviewKeyHiddenField" runat="server" Value='<%# Eval("Interviewkey") %>' />
                                                                                <asp:HiddenField ID="SearchOnlineInterview_interviewNameHiddenField" runat="server" Value='<%# Eval("InterviewName") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="INTERVIEWKEY" HeaderText="Interview Key">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchOnlineInterview_interviewKeyLabel" runat="server" Text='<%# Eval("Interviewkey") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="INTERVIEW_NAME" HeaderText="Interview&nbsp;Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchOnlineInterview_interviewNameNameLabel" runat="server" Text='<%# TrimContent(Eval("InterviewName").ToString(),20) %>'
                                                                                    ToolTip='<%# Eval("InterviewName")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="INTERVIEWAUTHOR" HeaderText="Interview Author Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchOnlineInterview_interviewAuthor" runat="server" ToolTip='<%# Eval("InterviewAuthorName") %>'
                                                                                    Text='<%# Eval("InterviewAuthorName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Skill Detail">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchOnlineInterview_interviewSkill" runat="server" ToolTip='<%# Eval("Skills") %>'
                                                                                    Text='<%# Eval("Skills") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" SortExpression="NUMBEROFQUESTIONS"
                                                                            HeaderStyle-CssClass="td_padding_right_15" HeaderText="No of Questions" ItemStyle-CssClass="td_padding_right_15">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchOnlineInterview_noOfQuestionsLabel" runat="server" Text='<%# Eval("TotalQuestion") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="CreatedDate" HeaderText="Created Date">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchOnlineInterview_createdDateLabel" runat="server" Text='<%# Convert.ToDateTime(Eval("CreatedDate")).ToString("MM/dd/yyyy") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="SearchOnlineInterview_pagingControlUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <uc1:PageNavigator ID="SearchOnlineInterview_bottomPagingNavigator" runat="server" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" align="left">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="SearchOnlineInterview_bottomLinksUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="SearchOnlineInterview_bottomReset" runat="server" Text="Reset" SkinID="sknPopupLinkButton"
                                                    OnClick="SearchOnlineInterview_bottomReset_Click"></asp:LinkButton>
                                            </td>
                                            <td class="pop_divider_line">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="SearchOnlineInterview_bottomCancel" runat="server" Text="Cancel"
                                                    SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="ScheduleCandidate_isMaximizedHiddenField" runat="server" />
</asp:Content>
