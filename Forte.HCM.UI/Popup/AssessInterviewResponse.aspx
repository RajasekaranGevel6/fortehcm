﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="AssessInterviewResponse.aspx.cs" Title="Assess Interview Question Response"
    Inherits="Forte.HCM.UI.Popup.AssessInterviewResponse" %>

<%@ Register Assembly="Forte.HCM.UI" Namespace="Forte.HCM.UI.CustomControls" TagPrefix="cc1" %>
<%@ Register Src="../CommonControls/CandidateActivityViewerControl.ascx" TagName="CandidateActivityViewerControl"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="AssessInterviewResponse_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        // Function that downloads the video.
        function DownloadVideo(candidateSessionID, attemptID, testQuestionID)
        {
            var url = "../Common/Download.aspx?type=IV" +
                "&candidatesessionid=" + candidateSessionID +
                "&attemptid=" + attemptID +
                "&testquestionid=" + testQuestionID;

            window.open(url, '', 'toolbar=0,resizable=0,width=1,height=1', '');
        }
    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_2" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="height: 8px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="AssessInterviewResponse_headerLiteral" runat="server" Text="Assess Interview Question Response"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="AssessInterviewResponse_topCancelImageButton" ToolTip="Click here to close the window"
                                OnClientClick="javascript:window.close();" runat="server" SkinID="sknCloseImageButton" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_2" valign="top" colspan="2">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_assess_interview_response_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:UpdatePanel ID="AssessInterviewResponse_messageUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="AssessInterviewResponse_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                        <asp:Label ID="AssessInterviewResponse_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="AssessInterviewResponse_saveButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr valign="top">
                                                <td style="width: 20%; height: 30px" valign="top">
                                                    <asp:Label ID="AssessInterviewResponse_questionLabel" runat="server" Text="Question"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 85%" valign="top">
                                                    <div style="width:580px;word-wrap: break-word;">
                                                      <asp:Label ID="AssessInterviewResponse_questionValueLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                    </div>
                                                    <asp:Panel ID="CreateManualTestWithoutAdaptive_hoverPanel" CssClass="table_outline_bg"
                                                        runat="server" Width="550px" Height="100px" Style="margin-left: -10px; overflow: auto;
                                                        z-index: 0">
                                                        <table width="100%" border="0" cellspacing="2" cellpadding="3">
                                                            <tr>
                                                                <th class="popup_question_icon">
                                                                    <div style="height: 72px; overflow: auto">
                                                                        <asp:Label ID="lblPopupQuestion" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </div>
                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <ajaxToolKit:HoverMenuExtender ID="CreateManualTestWithoutAdaptive_hoverMenuExtender"
                                                        runat="server" PopupControlID="CreateManualTestWithoutAdaptive_hoverPanel" PopupPosition="Bottom"
                                                        HoverCssClass="popupHover" TargetControlID="AssessInterviewResponse_questionValueLabel"
                                                        PopDelay="50" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td style="width: 20%; height: 60px" valign="top">
                                                    <asp:Label ID="AssessInterviewResponse_candidateCommentsLabel" runat="server" Text="Candidate Comments"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 85%" valign="top">
                                                <div style="width:580px;word-wrap: break-word;">
                                                    <asp:Label ID="AssessInterviewResponse_candidateCommentsValueLabel" runat="server"
                                                        Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                  </div>
                                                    <asp:Panel ID="AssessInterviewResponsecandidateComments_hoverPanel" CssClass="table_outline_bg"
                                                        runat="server" Width="550px" Height="94px" Style="margin-left: -10px; overflow: auto">
                                                        <table width="100%" border="0" cellspacing="2" cellpadding="3">
                                                            <tr>
                                                                <th class="popup_Comments_icon">
                                                                    <div style="height: 72px; overflow: auto">
                                                                        <asp:Label ID="lblCandidateComments" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </div>
                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <ajaxToolKit:HoverMenuExtender ID="AssessInterviewResponse_candidateComments_HoverMenuExtender"
                                                        runat="server" PopupControlID="AssessInterviewResponsecandidateComments_hoverPanel"
                                                        PopupPosition="Bottom" HoverCssClass="popupHover" TargetControlID="AssessInterviewResponse_candidateCommentsValueLabel"
                                                        PopDelay="50" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:Label ID="AssessInterviewResponse_timeTakenLabel" runat="server" Text="Time Taken"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="AssessInterviewResponse_statusLabel" runat="server" Text="Status"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="AssessInterviewResponse_skippedLabel" runat="server" Text="Skipped"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="AssessInterviewResponse_complexityLabel" runat="server" Text="Complexity"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="AssessInterviewResponse_testAreaLabel" runat="server" Text="Interview Area"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:Label ID="AssessInterviewResponse_timeTakenValueLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="AssessInterviewResponse_statusValueLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="AssessInterviewResponse_skippedValueLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="AssessInterviewResponse_complexityValueLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="AssessInterviewResponse_testAreaValueLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td valign="top">
                                                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td colspan="2" class="header_bg">
                                                                <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Literal ID="AssessInterviewResponse_videoHeaderLabel" runat="server" Text="Video"
                                                                                SkinID="sknLabelText"></asp:Literal>
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:LinkButton ID="AssessInterviewResponse_downloadVideoLinkButton" runat="server"
                                                                                Text="Download Video" SkinID="sknActionLinkButton" ToolTip="Click here to download the video"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="grid_body_bg" align="center">
                                                             <div style="width: 186px; overflow: auto; height: 215px; background-color:Gray;">
                                                                <%--<cc1:WVC ID="AssessInterviewResponse_video" runat="server" BackColor="Black" BorderStyle="Ridge"
                                                                    Height="210px" ShowControls="False" ShowPositionControls="False" ShowStatusBar="False"
                                                                    ShowTracker="False" Width="210px" />--%>
                                                                    
                                                                    <script type="text/javascript" src="../VideoStreaming/AC_OETags.js"  language="javascript"></script>
                                                                    <script type="text/javascript" src="../VideoStreaming/history/history.js" language="javascript"></script>
                                                                     <script language="JavaScript" type="text/javascript">

                                                                         var requiredMajorVersion = 9;
                                                                         var requiredMinorVersion = 0;
                                                                         var requiredRevision = 28;        
                                                                    </script>

                                                                    <script language="JavaScript" type="text/javascript">
                                                                <!--
                                                                        // Version check for the Flash Player that has the ability to start Player Product Install (6.0r65)
                                                                        var hasProductInstall = DetectFlashVer(6, 0, 65);

                                                                        // Version check based upon the values defined in globals
                                                                        var hasRequestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);

                                                                        if (hasProductInstall && !hasRequestedVersion) {
                                                                            // DO NOT MODIFY THE FOLLOWING FOUR LINES
                                                                            // Location visited after installation is complete if installation is required
                                                                            var MMPlayerType = (isIE == true) ? "ActiveX" : "PlugIn";
                                                                            var MMredirectURL = window.location;
                                                                            document.title = document.title.slice(0, 47) + " - Flash Player Installation";
                                                                            var MMdoctitle = document.title;

                                                                            AC_FL_RunContent(
		                                                                "src", "playerProductInstall",
		                                                                "FlashVars", "URL=<%= videoURL %>&MMredirectURL=" + MMredirectURL + '&MMplayerType=' + MMPlayerType + '&MMdoctitle=' + MMdoctitle + "",
		                                                                "width", "186",
		                                                                "height", "215",
		                                                                "align", "middle",
		                                                                "id", "OFIStream",
		                                                                "quality", "high",
		                                                                "bgcolor", "#ffffff",
		                                                                "name", "OFIStream",
		                                                                "allowScriptAccess", "sameDomain",
		                                                                "type", "application/x-shockwave-flash",
		                                                                "pluginspage", "http://www.adobe.com/go/getflashplayer"
	                                                                );
                                                                        } else if (hasRequestedVersion) {
                                                                            // if we've detected an acceptable version
                                                                            // embed the Flash Content SWF when all tests are passed
                                                                            AC_FL_RunContent(
			                                                                "src", "../VideoStreaming/OFIStream",
			                                                                "FlashVars", "URL=<%= videoURL %>&QuestionID=<%= questionID %>",
			                                                                "width", "186",
			                                                                "height", "215",
			                                                                "align", "left",
			                                                                "id", "OFIStream",
			                                                                "quality", "high",
			                                                                "bgcolor", "#ffffff",
			                                                                "name", "OFIStream",
			                                                                "allowScriptAccess", "sameDomain",
			                                                                "type", "application/x-shockwave-flash",
			                                                                "pluginspage", "http://www.adobe.com/go/getflashplayer"
	                                                                );
                                                                        } else {  // flash is too old or we can't detect the plugin
                                                                            var alternateContent = 'Alternate HTML content should be placed here. '
  	                                                                + 'This content requires the Adobe Flash Player. '
   	                                                                + '<a href=http://www.adobe.com/go/getflash/>Get Flash</a>';
                                                                            document.write(alternateContent);  // insert non-flash content
                                                                        }
                                                                // -->
                                                                    </script>
                                                                 <noscript>
                                                                     <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="OFIStream"
                                                                         width="186px" height="215px" codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">
                                                                         <param name="movie" value="OFIStream.swf" />
                                                                         <param name="quality" value="high" />
                                                                         <param name="bgcolor" value="#ffffff" />
                                                                         <param name="allowScriptAccess" value="sameDomain" />
                                                                         <embed src="../VideoStreaming/OFIStream.swf" quality="high" bgcolor="#ffffff"
                                                                             width="186px" height="215px" name="OFIStream" align="middle" play="true"
                                                                             loop="false" quality="high" allowscriptaccess="sameDomain" type="application/x-shockwave-flash"
                                                                             pluginspage="http://www.adobe.com/go/getflashplayer">
			                                                             </embed>
                                                                     </object>
                                                                 </noscript>
                                                            </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="padding-left: 4px">
                                                </td>
                                                <td valign="top">
                                                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td colspan="2" class="header_bg">
                                                                <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Literal ID="AssessInterviewResponse_assessorRating" runat="server" Text="Assessor Rating"
                                                                                SkinID="sknLabelText"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="grid_body_bg" align="center" style="width: 100%">
                                                            <asp:UpdatePanel ID="AssessInterviewResponse_assessorRatingUpdatePanel" runat="server">
                                                                <ContentTemplate>
                                                                    <div style="overflow: auto; height: 220px;">
                                                                <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" colspan="2">
                                                                            <asp:Label ID="InterviewCandidateTestDetails_userCommentsLabel" runat="server" Text="User Comments"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" colspan="2">
                                                                            <asp:TextBox ID="InterviewCandidateTestDetails_userCommentsTextBox" runat="server"
                                                                                MaxLength="1000" Width="98%" Height="40px" TextMode="MultiLine" onchange="CommentsCount(1000,this)"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 8px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="width: 98%" colspan="2">
                                                                            <asp:Label ID="InterviewCandidateTestDetails_commentLabel" runat="server" Text="Interview Question Comments"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="width: 80%" colspan="2">
                                                                             <div style="overflow: auto; height: 60px;">
                                                                                <asp:Label ID="InterviewCandidateTestDetails_commentValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="width: 6%">
                                                                            <asp:Label ID="InterviewCandidateTestDetails_ratingLabel" runat="server" Text="Rating"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <ajaxToolKit:Rating ID="AssessInterviewResponse_rating" runat="server" MaxRating="10"
                                                                                CurrentRating="0" CssClass="rating_star" StarCssClass="rating_item" WaitingStarCssClass="rating_saved"
                                                                                FilledStarCssClass="rating_filled" EmptyStarCssClass="rating_empty" AutoPostBack="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="middle" align="right" colspan="3">
                                                                            <table style="width: 98%" cellpadding="0" cellspacing="3">
                                                                                <tr>
                                                                                    <td valign="middle" align="right" style="width: 80%">
                                                                                        <asp:Button ID="AssessInterviewResponse_saveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                                                            OnClick="AssessInterviewResponse_saveButton_Click" />
                                                                                        <asp:LinkButton ID="AssessInterviewResponse_cancelLinkButton" Visible="true" ToolTip="Click here to close the window"
                                                                                            CssClass="link_btn" runat="server" Text="Cancel" OnClick="AssessInterviewResponse_cancelLinkButton_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                </div>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                                
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 4px">
                                    </td>
                                </tr>
                                 <tr>
                                    <td valign="top">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="header_bg">
                                                    <asp:Literal ID="AssessInterviewResponse_otherAssessorLiteral" runat="server" Text="Other Assessor Comments"
                                                        SkinID="sknLabelText"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="grid_body_bg" align="center" style="height: 100%">
                                                    <asp:UpdatePanel ID="AssessInterviewResponse_otherAssessorUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <div style="width: 95%; overflow: auto; height: 80px;" runat="server" id="AssessInterviewResponse_otherAssessorDiv">
                                                                <asp:GridView ID="AssessInterviewResponse_otherAssessorGridview" runat="server" AutoGenerateColumns="False"
                                                                    SkinID="sknNewGridView">
                                                                    <Columns>
                                                                        <asp:BoundField HeaderText="Assessor Name" DataField="assessorName" />
                                                                        <asp:BoundField HeaderText="Assessor Rating" DataField="Rating" />
                                                                        <asp:BoundField HeaderText="Assessor Comments" DataField="userComments" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="popup_td_padding_left_10" valign="top">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                               
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
       
    </table>
</asp:Content>
