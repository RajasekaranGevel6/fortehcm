<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchCandidateRepository.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.SearchCandidateRepository" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ID="SearchCandidate_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">

    <script type="text/javascript" language="javascript">
     
        function OnSelectClick(ctrl) {
            var nameCtrl = '<%= Request.QueryString["namectrl"] %>';
            var ctrlName = '<%= Request.QueryString["ctrlName"] %>';
            var emailCtrl = '<%= Request.QueryString["emailctrl"] %>';
            var idCtrl = '<%= Request.QueryString["idctrl"] %>';
            var firstnameCtrl = '<%= Request.QueryString["firstnameCtrl"] %>';
            var lastnameCtrl = '<%= Request.QueryString["lastnameCtrl"] %>';
            var isapprovedCtrl = '<%= Request.QueryString["isapproved"] %>';
            var toperrormsgCtrl = '<%= Request.QueryString["toperrormsgLabel"] %>';
            var isaprovedHiddenCtrlId = '<%= Request.QueryString["isaprovedHiddenCtrl"] %>';
            var subctrlname = '<%= Request.QueryString["subctrlname"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Set the user name field value. Replace the 'link button name' with the
            // 'user name hidden field name' and retrieve the value.
            if (nameCtrl != null && nameCtrl != '') 
            {
                window.opener.document.getElementById(nameCtrl).value
                    = document.getElementById(ctrl.id.replace("SearchCandidate_selectLinkButton", "SearchCandidate_userNameHiddenfield")).value;

                window.opener.document.getElementById(ctrlName).value
                    = document.getElementById(ctrl.id.replace("SearchCandidate_selectLinkButton", "SearchCandidate_FirstNameHiddenfield")).value; 

                window.opener.document.getElementById(firstnameCtrl).value
                    = document.getElementById(ctrl.id.replace("SearchCandidate_selectLinkButton", "SearchCandidate_FirstNameHiddenfield")).value;

                if (lastnameCtrl != null && lastnameCtrl != '') {
                    window.opener.document.getElementById(lastnameCtrl).value
                    = document.getElementById(ctrl.id.replace("SearchCandidate_selectLinkButton", "SearchCandidate_LastNameHiddenfield")).value;
                }
            }

            // Set the email hidden field value. Replace the 'link button name' with the 
            // 'eamil hidden field name' and retrieve the value.
            if (emailCtrl != null && emailCtrl != '') {
                window.opener.document.getElementById(emailCtrl).value
                    = document.getElementById(ctrl.id.replace("SearchCandidate_selectLinkButton", "SearchCandidate_emailHiddenfield")).value;
            }

            // Set the user ID hidden field value. Replace the 'link button name' with the 
            // 'user ID hidden field name' and retrieve the value.
            if (idCtrl != null && idCtrl != '') {
                window.opener.document.getElementById(idCtrl).value
                    = document.getElementById(ctrl.id.replace("SearchCandidate_selectLinkButton", "SearchCandidate_userIDHiddenfield")).value;
            }

            // Set the user ID hidden field value. Replace the 'link button name' with the 
            // 'user ID hidden field name' and retrieve the value.
            if (ctrlName != null && ctrlName != '')
            {
                window.opener.document.getElementById(ctrlName).value
                    = document.getElementById(ctrl.id.replace("SearchCandidate_selectLinkButton", "SearchCandidate_FirstNameHiddenfield")).value;
            }

            // If the candidate already has the resume in approved or in unapproved status 
            // Display it to the user
            if (toperrormsgCtrl != null && toperrormsgCtrl != '') {
                window.opener.document.getElementById(toperrormsgCtrl).innerHTML
                    = document.getElementById(ctrl.id.replace("SearchCandidate_selectLinkButton", "SearchCandidate_ResumeIsApprovedHiddenfield")).value;

            }
            // If the candidate already has the resume in approved or in unapproved status 
            // Display it to the user
            if (isaprovedHiddenCtrlId != null && isaprovedHiddenCtrlId != '') {
                window.opener.document.getElementById(isaprovedHiddenCtrlId).value
                    = document.getElementById(ctrl.id.replace("SearchCandidate_selectLinkButton", "SearchCandidate_IsApprovedHiddenfield")).value;
            }

            if (subctrlname != null) 
            {
                if (subctrlname == "CAND_DASHBOARD")
                {
                    window.opener.document.getElementById(ctrlName.replace("CandidateDashboard_candidateNameTextBox", "CandidateDashboard_candidateActvityButton")).click();
                }
                else if (subctrlname == "RESUME_EDITOR")
                {
                    window.opener.document.getElementById(ctrlName.replace("ResumeEditor_candidateIDTextBox", "ResumeEditor_loadDetailsButton")).click();
                }
                else if (subctrlname == "RESUME_UPLOADER")
                {
                    window.opener.document.getElementById(ctrlName.replace("ResumeUploader_candidateIDTextBox", "ResumeUploader_loadDetailsButton")).click();
                }
            }
            
            self.close(); 
        }

    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchCandidate_headerLiteral" runat="server" Text="Search Candidate"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SearchCandidate_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="msg_align">
                            <asp:UpdatePanel ID="SearchCandidate_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="SearchCandidate_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="SearchCandidate_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="SearchCandidate_searchButton" />
                                    <asp:AsyncPostBackTrigger ControlID="SearchCandidate_pageNavigator" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_padding_15">
                            <div id="SearchCandidate_searchCriteriasDiv" runat="server" style="display: block;">
                                <table width="100%">
                                    <tr>
                                        <td valign="top">
                                            <asp:UpdatePanel ID="SearchCandidate_searchCriteriaUpdatePanel" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table width="100%" border="0" cellspacing="3%" cellpadding="2%">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="SearchCandidate_firstNameLabel" runat="server" Text="First Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="SearchCandidate_firstNameTextBox" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="SearchCandidate_middleNameLabel" runat="server" Text="Middle Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="SearchCandidate_middleNameTextBox" runat="server"></asp:TextBox>
                                                            </td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="SearchCandidate_lastNameLabel" runat="server" Text="Last Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="SearchCandidate_lastNameTextBox" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="SearchCandidate_emailLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="SearchCandidate_emailTextBox" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" colspan="4">
                                                                <asp:Button ID="SearchCandidate_searchButton" runat="server" SkinID="sknButtonId"
                                                                    Text="Search" OnClick="SearchCandidate_searchButton_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:UpdatePanel ID="SearchCandidate_searchResultsUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table width="100%">
                                        <tr id="SearchCandidate_searchTestResultsTR" runat="server">
                                            <td class="header_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%">
                                                            <asp:Literal ID="SearchCandidate_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                ID="SearchQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%" align="right">
                                                            <span id="SearchCandidate_searchResultsUpSpan" style="display: none;" runat="server">
                                                                <asp:Image ID="SearchCandidate_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                            </span><span id="SearchCandidate_searchResultsDownSpan" style="display: block;" runat="server">
                                                                <asp:Image ID="SearchCandidate_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left">
                                                            <div style="height: 220px; overflow: auto;" runat="server" id="SearchCandidate_testDiv">
                                                                <asp:GridView ID="SearchCandidate_testGridView" runat="server" AllowSorting="True"
                                                                    AutoGenerateColumns="False" GridLines="None" Width="100%" OnSorting="SearchCandidate_testGridView_Sorting"
                                                                    OnRowDataBound="SearchCandidate_testGridView_RowDataBound" OnRowCreated="SearchCandidate_testGridView_RowCreated">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="100px">
                                                                            <ItemTemplate >
                                                                                <asp:LinkButton ID="SearchCandidate_selectLinkButton" runat="server" Text="Select"
                                                                                    OnClientClick="javascript:return OnSelectClick(this); " ToolTip="Select"></asp:LinkButton>
                                                                                <asp:ImageButton ID="SearchCandidate_addNotesImageButton"
                                                                                    runat="server" SkinID="sknAddNotesImageButton" ToolTip="Add Notes" />
                                                                                <asp:ImageButton ID="SearchCandidate_viewCandidateActivityLogImageButton"
                                                                                    runat="server" SkinID="sknViewCandidateActivityLogImageButton" ToolTip="View Candidate Activity Log" />
                                                                                <asp:HyperLink ID="SearchCandidate_viewCandidateActivityLogHyperLink" runat="server" Target="_blank" 
                                                                                ImageUrl="~/App_Themes/DefaultTheme/Images/mail_icon_to.png" ToolTip="View Candidate Activity Dashboard" >
                                                                                </asp:HyperLink>
                                                                                <asp:HiddenField ID="SearchCandidate_userNameHiddenfield" runat="server" Value='<%# Eval("UserName") %>' />
                                                                                <asp:HiddenField ID="SearchCandidate_userIDHiddenfield" runat="server" Value='<%# Eval("CandidateID") %>' />
                                                                                <asp:HiddenField ID="SearchCandidate_emailHiddenfield" runat="server" Value='<%# Eval("EMailID") %>' />
                                                                                <asp:HiddenField ID="SearchCandidate_FirstNameHiddenfield" runat="server" Value='<%# Eval("FirstName") %>' />
                                                                                <asp:HiddenField ID="SearchCandidate_LastNameHiddenfield" runat="server" Value='<%# Eval("LastName") %>' />
                                                                                <asp:HiddenField ID="SearchCandidate_ResumeIsApprovedHiddenfield" runat="server"
                                                                                    Value='<%# GetIsApproved(Convert.ToString(Eval("ResumeIsApproved"))) %>' />
                                                                                <asp:HiddenField ID="SearchCandidate_IsApprovedHiddenfield" runat="server" Value='<%# Eval("ResumeIsApproved") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField HeaderText="User ID" DataField="UserID" Visible="false" />
                                                                        <asp:BoundField HeaderText="User Name" DataField="UserName" SortExpression="USERNAME" />
                                                                        <asp:BoundField HeaderText="First Name" DataField="FirstName" SortExpression="FIRSTNAME" />
                                                                        <asp:BoundField HeaderText="Middle Name" DataField="MiddleName" SortExpression="MIDDLENAME" />
                                                                        <asp:BoundField HeaderText="Last Name" DataField="LastName" SortExpression="LASTNAME" />
                                                                        <asp:BoundField HeaderText="Email" DataField="EMailID" SortExpression="EMAIL" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <uc1:PageNavigator ID="SearchCandidate_pageNavigator" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="SearchCandidate_searchButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_left_8">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="SearchCandidate_topResetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="SearchCandidate_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="SearchCandidate_topCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="SearchCandidate_isMaximizedHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
