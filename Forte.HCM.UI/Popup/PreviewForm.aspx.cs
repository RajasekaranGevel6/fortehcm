﻿#region Directives                                                   
using System;
using System.Linq;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

using AjaxControlToolkit;

using Microsoft.Practices.EnterpriseLibrary.Logging;
#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that used to preview the form.
    /// </summary>
    public partial class PreviewForm : PageBase
    {
        #region Event Handlers                                       
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Preview Form");
                if (Request.QueryString["formID"] != null &&
                    Request.QueryString["formID"] != string.Empty)
                {
                    List<Segment> segmentDetailList = new PositionProfileBLManager().
                        GetPositionProfileSegmentsForForm
                        (int.Parse(Request.QueryString["formID"].ToString()));

                    segmentDetailList = segmentDetailList.OrderBy(p => p.DisplayOrder).ToList();

                    Session[Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS] = segmentDetailList;
                }
                if (!Support.Utility.IsNullOrEmpty
                    (Session[Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS]))
                {
                    PreviewFormSegemnts();

                    CreateTabs(true);
                }
            }

            catch (Exception exception)
            {
                Logger.Write(exception.Message);
                base.ShowMessage(PreviewForm_errorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the ActiveTabChanged event of the PositionProfileEntry_tabContainer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PreviewForm_tabContainer_ActiveTabChanged(object sender, EventArgs e)
        {
            try
            {
                if (Support.Utility.IsNullOrEmpty(Session[Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS]))
                    return;

                ControlVisibility();

                if (PreviewForm_tabContainer.ActiveTab == null)
                    return;
                ControlVisibility(int.Parse(PreviewForm_tabContainer.ActiveTab.ID.Remove(0, 3)));
            }
            catch (Exception exception)
            {
                Logger.Write(exception.Message);
                base.ShowMessage(PreviewForm_errorMessageLabel, exception.Message);
            }
        }
        #endregion Event Handlers

        #region Private Methods                                      
        /// <summary>
        /// Previews the form segemnts.
        /// </summary>
        private void PreviewFormSegemnts()
        {
            List<Segment> segmentDetailList = (List<Segment>)Session[Support.Constants.SessionConstants.
                PREVIEW_FORM_SEGMENTS];

            if (segmentDetailList == null || segmentDetailList.Count == 0)
                return;
        }


        /// <summary>
        /// Creates the tabs.
        /// </summary>
        /// <param name="activeTabFlag">if set to <c>true</c>
        /// [active tab flag].</param>
        private void CreateTabs(bool activeTabFlag)
        {
            List<Segment> segmentDetailList = null;

            List<int> segmentIdList = new List<int>();

            PreviewForm_tabContainer.Controls.Clear();
            if (Support.Utility.IsNullOrEmpty(Session
                [Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS]))
                return;

            segmentDetailList = (List<Segment>)Session
                [Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS];

            segmentDetailList = segmentDetailList.OrderBy(P => P.DisplayOrder).ToList();

            for (int i = 0; i < segmentDetailList.Count; i++)
            {
                segmentIdList.Add(segmentDetailList[i].SegmentID);
                TabPanel tabPanel = new TabPanel();
                tabPanel.ID = "tab" + segmentDetailList[i].SegmentID.ToString();
                tabPanel.HeaderText = segmentDetailList[i].SegmentName;
                PreviewForm_tabContainer.Tabs.Add(tabPanel);
                if (i == 0 && activeTabFlag)
                    PreviewForm_tabContainer.ActiveTab = tabPanel;
            }
            Session[Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS] = segmentDetailList;

            if (PreviewForm_tabContainer.ActiveTab == null)
                return;
            ControlVisibility(int.Parse(PreviewForm_tabContainer.ActiveTab.ID.Remove(0, 3)));

        }


        /// <summary>
        /// Controls the visibility.
        /// </summary>
        private void ControlVisibility()
        {
            PreviewForm_verticalBackgroundRequirementControl.Visible = false;
            PreviewForm_technicalSkillRequirementControl.Visible = false;
            PreviewForm_roleRequirementControl.Visible = false;
            PreviewForm_educationRequirementControl.Visible = false;
            PreviewForm_clientPositionDetailsControl.Visible = false;
        }

        /// <summary>
        /// Controls the visibility.
        /// </summary>
        /// <param name="segmentId">The segment id.</param>
        private void ControlVisibility(int segmentId)
        {
            switch (segmentId)
            {
                case (int)SegmentType.ClientPositionDetail:
                    PreviewForm_clientPositionDetailsControl.Visible = true;
                    PreviewForm_clientPositionDetailsControl.DisableControls();
                    break;
                case (int)SegmentType.CommunicationSkills:
                    break;
                case (int)SegmentType.CulturalFit:
                    break;
                case (int)SegmentType.DecisionMaking:
                    break;
                case (int)SegmentType.Education:
                    PreviewForm_educationRequirementControl.Visible = true;
                    PreviewForm_educationRequirementControl.DisableControls();
                    break;
                case (int)SegmentType.InterpersonalSkills:
                    break;
                case (int)SegmentType.Roles:
                    PreviewForm_roleRequirementControl.Visible = true;
                    PreviewForm_roleRequirementControl.DisableControls();
                    break;
                case (int)SegmentType.Screening:
                    break;
                case (int)SegmentType.TechnicalSkills:
                    PreviewForm_technicalSkillRequirementControl.Visible = true;
                    PreviewForm_technicalSkillRequirementControl.DisableControls();
                    break;
                case (int)SegmentType.Verticals:
                    PreviewForm_verticalBackgroundRequirementControl.Visible = true;
                    PreviewForm_verticalBackgroundRequirementControl.DisableControls();
                    break;
            }
        }

        #endregion Private Methods

        #region Protected Override Methods                           
        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion Protected Override Methods
    }
}