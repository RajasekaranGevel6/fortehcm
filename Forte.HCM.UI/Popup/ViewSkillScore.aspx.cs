﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewSkillScore.aspx.cs
// File that represents the user interface layout and functionalities 
// for the ViewSkillScore page. This page helps to view the position
// profile skills of a candidate. 

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Collections.Generic;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the ViewSkillScore page. This page helps to view the position
    /// profile skills of a candidate. This class inherits 
    /// Forte.HCM.UI.Common.PageBase class
    /// </summary>
    public partial class ViewSkillScore : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that is called when the page loads.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                
                Page.SetFocus(ViewSkillScore_topCancelImageButton.ClientID);
                //Page Tittle
                Master.SetPageCaption("Candidate Skill Score");
                   
                // Load the skill values.
                if (!IsPostBack)
                {
                    LoadValues();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(ViewSkillScore_topErrorMessageLabel,
                    exception.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Protected Methods                                              

        /// <summary>
        /// Overridden protected method to check the valid data
        /// </summary>
        /// <returns>
        /// A<see cref="bool"/>value whether it is true or false
        /// </returns>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Overridden protected method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
            // Check if position profile ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
            {
                ShowMessage(ViewSkillScore_topErrorMessageLabel, "Position profile ID is not present");
                return;
            }

            // Check if position profile is valid number.
            int positionProfileID = 0;
            if (int.TryParse(Request.QueryString["positionprofileid"].Trim(), out positionProfileID) == false)
            {
                ShowMessage(ViewSkillScore_topErrorMessageLabel, "Position profile ID is invalid");
                return;
            }

            // Check if candidate ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
            {
                ShowMessage(ViewSkillScore_topErrorMessageLabel, "Candidate ID is not present");
                return;
            }

            // Check if candidate is valid number.
            int candidateID = 0;
            if (int.TryParse(Request.QueryString["candidateid"].Trim(), out candidateID) == false)
            {
                ShowMessage(ViewSkillScore_topErrorMessageLabel, "Candidate ID is invalid");
                return;
            }

            // Get position profile candidate skill score detail.
            PositionProfileCandidateSkillScoreDetail scoreDetail = new PositionProfileBLManager().
                GetPositionProfileCandidateSkillScore(positionProfileID, candidateID);

            if (scoreDetail == null)
            {
                ShowMessage(ViewSkillScore_topErrorMessageLabel, "No skill detail found to display");
                return;
            }

            // Assign header detail.
            ViewSkillScore_positionProfileNameValueLabel.Text = scoreDetail.PositionProfileName;
            ViewSkillScore_candidateNameValueLabel.Text = scoreDetail.CandidateName;

            // Show or hide the 'no skill found to display' label, based on
            // skill list is present or not.
            if (scoreDetail.SkillScores == null || scoreDetail.SkillScores.Count == 0)
                ViewSkillScore_noSkillMessageDiv.Visible = true;
            else
                ViewSkillScore_noSkillMessageDiv.Visible = false;

            // Assign skills list.
            ViewSkillScore_skillsRepeator.DataSource = scoreDetail.SkillScores;
            ViewSkillScore_skillsRepeator.DataBind();

            // Assign overall skill detail.
            ViewSkillScore_overallTestScoreValueLabel.Text = scoreDetail.OverallTestScore.ToString();
            ViewSkillScore_overallInterviewScoreValueLabel.Text = scoreDetail.OverallInterviewScore.ToString();
            ViewSkillScore_overallResumeScoreValueLabel.Text = scoreDetail.OverallResumeScore.ToString();
        }

        #endregion Protected Methods                                           
    }
}
