﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchSubject.aspx.cs" MasterPageFile="~/MasterPages/PopupMaster.Master"
    Inherits="Forte.HCM.UI.Popup.SearchSubject" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ID="SearchSubject_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">

    <script type="text/javascript" language="javascript">

        function CloseSearchSubject(status) {
            var statusCtrl = '<%= Request.QueryString["statusctrl"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Set the display field value. Replace the 'link button name' with the 
            // 'user name hidden field name' and retrieve the value.
            if (statusCtrl != null && statusCtrl != '') {
                window.opener.document.getElementById(statusCtrl).value = status;
            }

            self.close();
        }
    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey" align="left">
                            <asp:Literal ID="SearchSubject_headerLiteral" runat="server" Text="Search Subject"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SearchSubject_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <div>
                    <table width="100%" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                        <tr>
                            <td class="msg_align">
                                <asp:Label ID="SearchSubject_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                <asp:Label ID="SearchSubject_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            </td>
                        </tr>
                        <tr id="SearchSubject_searchCriteriaTR" runat="server">
                            <td id="Td2" align="center" runat="server">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="popup_td_padding_10">
                                <div id="SearchSubject_searchCriteriasDiv" runat="server" style="display: block;">
                                    <table width="100%" border="0" cellspacing="3" cellpadding="2">
                                        <tr>
                                            <td>
                                                <asp:Label ID="SearchSubject_categoryNameLabel" runat="server" Text="Category Name"
                                                    SkinID="sknLabelText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="SearchSubject_categoryNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="SearchSubject_subjectNameLabel" runat="server" Text="Subject Name"
                                                    SkinID="sknLabelText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="SearchSubject_subjectNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="right">
                                                <asp:Button ID="SearchSubject_searchButton" runat="server" Text="Search" OnClick="SearchSubject_searchButton_Click"
                                                    SkinID="sknButtonId" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_height_5">
                            </td>
                        </tr>
                        <tr>
                            <td  class="popup_td_padding_10">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr id="SearchSubject_searchTestResultsTR" runat="server">
                                        <td id="Td1" class="header_bg" align="center" runat="server">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td style="width: 50%" align="left">
                                                        <asp:Literal ID="SearchSubject_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>
                                                        &nbsp;<asp:Label ID="SearchSubject_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                            Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 50%" align="right">
                                                        <span id="SearchSubject_searchResultsUpSpan" style="display: none;" runat="server">
                                                            <asp:Image ID="SearchSubject_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                        </span><span id="SearchSubject_searchResultsDownSpan" style="display: block;" runat="server">
                                                            <asp:Image ID="SearchSubject_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="SearchSubject_updatePanel" runat="server">
                                                <ContentTemplate>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td >
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td align="left"  class="grid_body_bg">
                                                                            <div style="height: 225px; width: 478px; overflow: auto;" runat="server" id="SearchSubject_testDiv">
                                                                                <asp:GridView ID="SearchSubject_resultsGridView" runat="server" AutoGenerateColumns="False"
                                                                                    Width="100%" GridLines="Horizontal" BorderColor="white" BorderWidth="1px" OnRowCreated="SearchSubject_resultsGridView_RowCreated"
                                                                                    OnSorting="SearchSubject_resultsGridView_Sorting" AllowSorting="true" OnRowDataBound="SearchSubject_resultsGridView_RowDataBound">
                                                                                    <RowStyle CssClass="grid_alternate_row" />
                                                                                    <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                                    <HeaderStyle CssClass="grid_header_row" />
                                                                                    <Columns>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="SearchSubject_resultsGridViewSelectCheckbox" runat="server" />
                                                                                                <asp:HiddenField ID="SearchSubject_resultsGridViewCategoryIDHiddenField" Value='<%#Eval("CategoryID") %>'
                                                                                                    runat="server" />
                                                                                                <asp:HiddenField ID="SearchSubject_resultsGridViewSubjectIDHiddenField" Value='<%#Eval("SubjectID") %>'
                                                                                                    runat="server" />
                                                                                                <asp:HiddenField ID="SearchSubject_resultsGridViewSubjectNameHiddenField" Value='<%#Eval("SubjectName") %>'
                                                                                                    runat="server" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField HeaderText="Category Name" DataField="CategoryName" SortExpression="CATEGORYNAME" />
                                                                                        <asp:BoundField HeaderText="Subject Name" DataField="SubjectName" SortExpression="SUBJECTNAME" />
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <uc1:PageNavigator ID="SearchSubject_pagingNavigator" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_height_5">
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_10">
                <table border="0" cellpadding="0" cellspacing="0" align="left">
                    <tr>
                        <td>
                            <asp:Button ID="SearchSubject_selectButton" runat="server" SkinID="sknButtonId" Text="Select"
                                OnClick="SearchSubject_selectButton_Click" />
                        </td>
                        <td class="pop_divider_line">
                            |
                        </td>
                        <td>
                            <asp:LinkButton ID="SearchSubject_resetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="SearchSubject_resetLinkButton_Click"></asp:LinkButton>
                        </td>
                        <td class="pop_divider_line">
                            |
                        </td>
                        <td class="td_padding_5">
                            <asp:LinkButton ID="SearchSubject_cancelButton" runat="server" Text="Cancel" SkinID="sknPopupLinkButton"
                                OnClientClick="javascript:window.close();"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField runat="server" ID="SearchSubject_isMaximizedHiddenField" />
            </td>
        </tr>
    </table>
</asp:Content>
