<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="SearchSkill.aspx.cs" Title="Search Skill" Inherits="Forte.HCM.UI.Popup.SearchSkill" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="SearchSkill_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">

    <script type="text/javascript" language="javascript">

        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl)
        {
            var ctrlID = '<%= Request.QueryString["ctrlid"] %>';
            var ctrlName = '<%= Request.QueryString["ctrlname"] %>';

            if (window.dialogArguments)
            {
                window.opener = window.dialogArguments;
            }
            if (ctrlID != null && ctrlID != '')
            {
                // Get skill ID.
                var skillID = document.getElementById(ctrl.id.replace
                    ("SearchSkill_selectButton", "SearchSkill_skillIDHiddenField")).value;

                // Get skill name.
                var skillName = document.getElementById(ctrl.id.replace
                    ("SearchSkill_selectButton", "SearchSkill_skillNameHiddenField")).value;

                window.opener.document.getElementById(ctrlID).value = skillID;
                window.opener.document.getElementById(ctrlName).value = skillName;
            }
            self.close();
        }

    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 80%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchSkill_headerLiteral" runat="server" Text="Search Skill"></asp:Literal>
                        </td>
                        <td style="width: 20%" align="right">
                            <asp:ImageButton ID="SearchSkill_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:UpdatePanel ID="SearchSkill_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="SearchSkill_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="SearchSkill_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchSkill_searchButton" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchSkill_pageNavigator" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="SearchSkill_searchCriteriasDiv" runat="server" style="display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="SearchSkill_searchCriteriaUpdatePanel" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <table width="100%" border="0" cellspacing="3%" cellpadding="2%">
                                                                    <tr>
                                                                        <td style="width: 20%" align="left">
                                                                            <asp:Label ID="SearchSkill_skillLabel" runat="server" Text="Skill"
                                                                                SkinID="sknLabelText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 80%" align="left">
                                                                            <asp:TextBox ID="SearchSkill_skillTextBox" runat="server" MaxLength="50" Width="99%"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="4">
                                                                            <asp:Button ID="SearchSkill_searchButton" runat="server" SkinID="sknButtonId"
                                                                                Text="Search" OnClick="SearchSkill_searchButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="SearchSkill_searchResultsUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                                                    <tr id="SearchSkill_searchTestResultsTR" runat="server">
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 80%">
                                                                        <asp:Literal ID="SearchSkill_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                            ID="SearchQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%" align="right">
                                                                        <span id="SearchSkill_searchResultsUpSpan" style="display: none;" runat="server">
                                                                            <asp:Image ID="SearchSkill_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                        </span><span id="SearchSkill_searchResultsDownSpan" style="display: block;" runat="server">
                                                                            <asp:Image ID="SearchSkill_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="height: 220px; overflow: auto;" runat="server" id="SearchSkill_searchResultsDiv">
                                                                            <asp:GridView ID="SearchSkill_searchResultsGridView" runat="server" AutoGenerateColumns="False"
                                                                                Width="100%" OnRowCreated="SearchSkill_searchResultsGridView_RowCreated" OnSorting="SearchSkill_searchResultsGridView_Sorting"
                                                                                AllowSorting="true" OnRowDataBound="SearchSkill_searchResultsGridView_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="SearchSkill_selectButton" runat="server" OnClientClick="javascript:return OnSelectClick(this);"
                                                                                                Text="Select" SkinID="sknActionLinkButton">
                                                                                            </asp:LinkButton>
                                                                                            <asp:HiddenField ID="SearchSkill_skillNameHiddenField" runat="server" Value='<%# Eval("SkillName") %>' />
                                                                                            <asp:HiddenField ID="SearchSkill_skillIDHiddenField" runat="server" Value='<%# Eval("SkillID") %>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="Skill" DataField="SkillName" SortExpression="SKILL_NAME" HeaderStyle-Width="85%"/>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <uc1:PageNavigator ID="SearchSkill_pageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchSkill_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_left_8">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="SearchSkill_topResetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="SearchSkill_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="SearchSkill_topCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="SearchSkill_isMaximizedHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
