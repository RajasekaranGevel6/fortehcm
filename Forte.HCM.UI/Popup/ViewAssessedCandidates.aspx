<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="ViewAssessedCandidates.aspx.cs" Title="View Assessed Candidates" Inherits="Forte.HCM.UI.Popup.ViewAssessedCandidates" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ViewAssessedCandidates_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey" align="left">
                            <asp:Literal ID="ViewAssessedCandidates_headerLiteral" runat="server" Text="View Assessed Candidates"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="ViewAssessedCandidates_topCloseImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:window.close();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_view_assessed_candidates_bg">
                    <tr>
                        <td class="msg_align" valign="top">
                            <asp:Label ID="ViewAssessedCandidates_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="ViewAssessedCandidates_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="panel_inner_body_bg" valign="top">
                            <table cellpadding="4" cellspacing="0" width="98%" border="0">
                                <tr>
                                    <td style="width: 14%" valign="top">
                                        <asp:Label ID="ViewAssessedCandidates_positionProfileNameHeadLabel" runat="server"
                                            Text="Position Profile" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 85%" valign="top">
                                        <asp:Label ID="ViewAssessedCandidates_positionProfileNameValueLabel" runat="server"
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 14%" valign="top">
                                        <asp:Label ID="ViewAssessedCandidates_clientNameHeadLabel" runat="server" Text="Client"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 85%" valign="top">
                                        <asp:Label ID="ViewAssessedCandidates_clientNameValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                 <tr>
                                    <td style="width: 14%" valign="top">
                                        <asp:Label ID="ViewAssessedCandidates_interviewSessionIDHeadLabel" runat="server" Text="Interview Session ID"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 85%" valign="top">
                                        <asp:HyperLink ID="ViewAssessedCandidates_interviewSessionIDValueHyperLink"
                                        runat="server" Target="_blank" ToolTip="Click here to view interview sessions"
                                        SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 4px">
                        </td>
                    </tr>
                    <tr>
                        <td class="panel_bg">
                            <asp:UpdatePanel ID="ViewAssessedCandidates_criteriaUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <div style="float: left;">
                                                    <asp:RadioButton ID="ViewAssessedCandidates_showOnlyRadioButton" runat="server" Text="Show Candidates Associated Only With This Session"
                                                        AutoPostBack="true" Checked="true" GroupName="ShowCandidates" SkinID="sknGeneralRadioButton"
                                                        OnCheckedChanged="ViewAssessedCandidates_showRadioButtonList_CheckedChanged" />
                                                </div>
                                                <div style="float: left; padding-left: 4px; padding-top: 2px">
                                                    <asp:ImageButton ID="ViewAssessedCandidates_showOnlyHelpImageButton" SkinID="sknHelpImageButton"
                                                        runat="server" ToolTip="Check this to show assessed candidates associated only with this session against the position profile"
                                                        ImageAlign="Middle" OnClientClick="javascript:return false;" /></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="float: left;">
                                                    <asp:RadioButton ID="ViewAssessedCandidates_showAllRadioButton" runat="server" Text="Show All Candidates Across All Sessions"
                                                        AutoPostBack="true" Checked="false" GroupName="ShowCandidates" SkinID="sknGeneralRadioButton"
                                                        OnCheckedChanged="ViewAssessedCandidates_showRadioButtonList_CheckedChanged" />
                                                </div>
                                                <div style="float: left; padding-left: 4px; padding-top: 2px">
                                                    <asp:ImageButton ID="ViewAssessedCandidates_showAllCheckBoxHelpImageButton" SkinID="sknHelpImageButton"
                                                        runat="server" ToolTip="Check this to show all candidates across all sessions against this position profile"
                                                        ImageAlign="Middle" OnClientClick="javascript:return false;" /></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 4px">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="ViewAssessedCandidates_candidateDetailsLiteral" runat="server" Text="Candidate Details"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="grid_body_bg" valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr>
                                    <td>
                                        <div style="height: 200px; overflow: auto;">
                                            <asp:UpdatePanel ID="ViewAssessedCandidates_gridUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="ViewAssessedCandidates_candidatesGridView" runat="server" AllowSorting="False"
                                                        AutoGenerateColumns="False" Width="100%" OnRowDataBound="ViewAssessedCandidates_candidatesGridView_RowDataBound"
                                                        Height="100%">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="ViewAssessedCandidates_candidatesGridView_candidateInterviewSessionKeyHiddenField"
                                                                        runat="server" Value='<%# Eval("CandidateInterviewSessionID") %>' />
                                                                    <asp:HiddenField ID="ViewAssessedCandidates_candidatesGridView_interviewTestKeyHiddenField"
                                                                        runat="server" Value='<%# Eval("InterviewTestKey") %>' />
                                                                    <asp:HiddenField ID="ViewAssessedCandidates_candidatesGridView_attemptIDHiddenField"
                                                                        runat="server" Value='<%# Eval("AttemptID") %>' />
                                                                    <asp:HiddenField ID="ViewAssessedCandidates_candidatesGridView_interviewSessionKeyHiddenField"
                                                                        runat="server" Value='<%# Eval("InterviewSessionKey") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Candidate" HeaderStyle-Width="18%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="ViewAssessedCandidates_candidatesGridView_candidateNameHyperLink"
                                                                        runat="server" Target="_blank" Text='<%# Eval("CandidateFullName") %>' ToolTip="Click here to view candidate interview assessment summary"
                                                                        SkinID="sknLabelFieldTextHyperLink">
                                                                    </asp:HyperLink>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderText="Interview Name" HeaderStyle-Width="37%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="ViewAssessedCandidates_candidatesGridView_interviewNameHyperLink"
                                                                        runat="server" Target="_blank" Text='<%# Eval("InterviewTestName") %>' ToolTip="Click here to view interview"
                                                                        SkinID="sknLabelFieldTextHyperLink">
                                                                    </asp:HyperLink>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Interview<br/>Session ID" HeaderStyle-Width="11%" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="ViewAssessedCandidates_candidatesGridView_sessionKeyHyperLink"
                                                                        runat="server" Target="_blank" Text='<%# Eval("InterviewSessionKey") %>' ToolTip="Click here to view interview sessions"
                                                                        SkinID="sknLabelFieldTextHyperLink">
                                                                    </asp:HyperLink>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Scheduled By" HeaderStyle-Width="20%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ViewAssessedCandidates_candidatesGridView_schedulerNameLabel" runat="server"
                                                                        Text='<%# Eval("SchedulerName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Interview<br/>Completed" HeaderStyle-Width="11%" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ViewAssessedCandidates_candidatesGridView_interviewCompletedDateLabel"
                                                                        runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("DateCompleted")))%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <table style="width: 100%; height: 100%">
                                                                <tr>
                                                                    <td style="height: 80px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="middle" align="center" style="height: 100%" class="error_message_text_normal">
                                                                        No assessed candidates found to display
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ViewAssessedCandidates_showOnlyRadioButton" />
                                                    <asp:AsyncPostBackTrigger ControlID="ViewAssessedCandidates_showAllRadioButton" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 8px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_5">
                <asp:LinkButton ID="ViewAssessedCandidates_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClientClick="javascript:window.close();" ToolTip="Click here to close the window" />
            </td>
        </tr>
    </table>
</asp:Content>
