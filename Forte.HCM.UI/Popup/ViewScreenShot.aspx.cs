﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.

// ViewScreenShot.cs
// File that represents the ViewScreenShot class that defines the user
// interface layout and functionalities for the tracking details preview 
// page. This page helps to view the webcam and images taken during a 
// test or interview conduction session.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the ViewScreenShot class that defines the user
    /// interface layout and functionalities for the tracking details preview 
    /// page. This page helps to view the webcam and images taken during a 
    /// test or interview conduction session. This class inherits
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class ViewScreenShot : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="int"/> that holds the page number.
        /// </summary>
        private int pageNo;

        /// <summary>
        /// A <see cref="string"/> that holds the image type.
        /// </summary>
        private string imageType;

        /// <summary>
        /// A <see cref="string"/> that holds the status which indicates
        /// whether the image is a log or general.
        /// </summary>
        private bool isLog;

        /// <summary>
        /// A <see cref="string"/> that holds the view type.
        /// </summary>
        private string viewType;

        /// <summary>
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </summary>
        private string candidateSessionID = string.Empty;

        /// <summary>
        /// A <see cref="string"/> that holds the attempt ID.
        /// </summary>
        private int attempID = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the log ID.
        /// </summary>
        private int logID = 0;

        #endregion Private Variables                                           

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["imageType"] != null)
                    imageType = Request.QueryString["imageType"];
                if (Request.QueryString["viewType"] != null)
                    viewType = Request.QueryString["viewType"];
                if (Request.QueryString["log"] != null && Request.QueryString["log"].ToUpper().Trim() == "LOG")
                    isLog = true;
                if (Request.QueryString["logID"] != null)
                    int.TryParse(Request.QueryString["logID"], out logID);

                if (imageType == "USER_IMG")
                    Master.SetPageCaption("Webcam Image");
                else if (imageType == "SCREEN_IMG")
                    Master.SetPageCaption("Desktop Image");
                
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["imageIndex"] != null)
                        int.TryParse(Request.QueryString["imageIndex"], out pageNo);
                    if (Request.QueryString["candidateSessionKey"] != null)
                        candidateSessionID = Request.QueryString["candidateSessionKey"];
                    if (Request.QueryString["attempID"] != null)
                        int.TryParse(Request.QueryString["attempID"], out attempID);

                    // Construct image list.
                    ConstructImageList(imageType);

                    // Keep current page number in session.
                    Session["VIEW_SCREEN_SHOT_IMAGE_INDEX"] = pageNo;

                    // Show the image.
                    ShowImage();

                    if (Session["VIEW_SCREEN_SHOT_IMAGE_INDEX"] != null)
                        pageNo = (int)Session["VIEW_SCREEN_SHOT_IMAGE_INDEX"];

                    int totalRecords = 0;
                    if (Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"] != null)
                        totalRecords = (int)Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"];

                    ShowNavigationButtons(totalRecords, pageNo);
                }
            }
            catch (Exception ex)
            {
                Logger.TraceLog(ex);
                base.ShowMessage(ViewScreenShot_topErrorMessageLabel, ex.Message);
            }
        }

        /// <summary>
        /// Handler method that will be the called when the previous image 
        /// button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will load the previous image.
        /// </remarks>
        protected void ViewScreenShot_previousImageButton_Click(object sender, 
            ImageClickEventArgs e)
        {
            try
            {
                // Move to previous image.
                MovePrevious();
            }
            catch (Exception ex)
            {
                Logger.TraceLog(ex);
                base.ShowMessage(ViewScreenShot_topErrorMessageLabel, ex.Message);
            }
        }

        /// <summary>
        /// Handler method that will be the called when the next image 
        /// button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will load the next image.
        /// </remarks>
        protected void ViewScreenShot_nextImageButton_Click(object sender, 
            ImageClickEventArgs e)
        {
            try
            {
                // Move to next image.
                MoveNext();
            }
            catch (Exception ex)
            {
                Logger.TraceLog(ex);
                base.ShowMessage(ViewScreenShot_topErrorMessageLabel, ex.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        /// <summary>
        /// Method the constructs the image list based on the image type.
        /// </summary>
        /// <param name="imageType">
        /// A <see cref="string"/> that holds the image type.
        /// </param>
        private void ConstructImageList(string imageType)
        {
            int totalRecords = 0;

            List<TrackingImageDetail> trackingImages = null;
            if (isLog == false)
            {
                if (viewType == "CP")
                {
                    trackingImages = new TrackingBLManager().GetCyberProctorImage
                        (candidateSessionID, attempID, imageType, 0, 0, out totalRecords);
                }
                else if (viewType == "OI")
                {
                    trackingImages = new TrackingBLManager().GetOfflineInterviewTrackingImageDetails
                        (candidateSessionID, attempID, imageType, 0, 0, out totalRecords);
                }
            }
            else
            {
                if (viewType == "CP")
                {
                    trackingImages = new TrackingBLManager().
                        GetCyberProctorLogImages(logID, imageType, 0, 0, out totalRecords);
                }
                else if (viewType == "OI")
                {
                    trackingImages = new TrackingBLManager().
                        GetOfflineInterviewLogImages(logID, imageType, 0, 0, out totalRecords);
                }
            }

            if (trackingImages != null)
            {
                Session["VIEW_SCREEN_SHOT_IMAGES"] = trackingImages;
                Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"] = trackingImages.Count;
            }
            else
            {
                base.ShowMessage(ViewScreenShot_topErrorMessageLabel,
                    ViewScreenShot_topErrorMessageLabel, "No data found to display");
                ViewScreenShot_previewImage.Visible = false;
                ViewScreenShot_navigationTable.Visible = false;
            }
        }
       
        private void ShowNavigationButtons()
        {
            if (Session["VIEW_SCREEN_SHOT_IMAGE_INDEX"] != null)
                pageNo = (int)Session["VIEW_SCREEN_SHOT_IMAGE_INDEX"];

            int totalRecords = 0;
            if (Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"] != null)
                totalRecords = (int)Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"];

            if (pageNo >= totalRecords - 1)
                ViewScreenShot_nextImageButton.Visible = false;

            if (pageNo <= 0)
                ViewScreenShot_previousImageButton.Visible = false;

            ShowNavigationButtons(totalRecords, pageNo);
        }

        /// <summary>
        /// Method that shows the navigation buttons based on the total records
        /// and current page number.
        /// </summary>
        private void ShowNavigationButtons(int totalRecords, int imageIndex)
        {
            // Set paging label.
            ViewScreenShot_imagePaging.Text = string.Format("{0} of {1}",
                imageIndex + 1, totalRecords);

            if (imageIndex < totalRecords - 1)
                ViewScreenShot_nextImageButton.Visible = true;
            else
                ViewScreenShot_nextImageButton.Visible = false;

            if (imageIndex <= 0)
                ViewScreenShot_previousImageButton.Visible = false;
            else
                ViewScreenShot_previousImageButton.Visible = true;
        }

        /// <summary>
        /// Method that navigates to the next image.
        /// </summary>
        private void MoveNext()
        {
            if (Session["VIEW_SCREEN_SHOT_IMAGE_INDEX"] != null)
                pageNo = (int)Session["VIEW_SCREEN_SHOT_IMAGE_INDEX"];

            int totalRecords = 0;
            if (Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"] != null)
                totalRecords = (int)Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"];

            if (pageNo < totalRecords - 1)
                pageNo++;

            ShowNavigationButtons(totalRecords, pageNo);

            Session["VIEW_SCREEN_SHOT_IMAGE_INDEX"] = pageNo;
            ShowImage();
        }

        /// <summary>
        /// Method that navigates to the previous image.
        /// </summary>
        private void MovePrevious()
        {
            int totalRecords = 0;
            if (Session["VIEW_SCREEN_SHOT_IMAGE_INDEX"] != null)
                pageNo = (int)Session["VIEW_SCREEN_SHOT_IMAGE_INDEX"];

            if (Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"] != null)
                totalRecords = (int)Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"];

            if (pageNo > 0)
                pageNo--;

            ShowNavigationButtons(totalRecords, pageNo);
            Session["VIEW_SCREEN_SHOT_IMAGE_INDEX"] = pageNo;
            ShowImage();
        }

        /// <summary>
        /// Methods that shows and image and message based on the current page 
        /// number.
        /// </summary>
        private void ShowImage()
        {
            if (Session["VIEW_SCREEN_SHOT_IMAGE_INDEX"] != null)
                pageNo = (int)Session["VIEW_SCREEN_SHOT_IMAGE_INDEX"];

            if (Session["VIEW_SCREEN_SHOT_IMAGES"] == null)
                return;

            // Get tracking images list.
            List<TrackingImageDetail> trackingImages = Session["VIEW_SCREEN_SHOT_IMAGES"] 
                as List<TrackingImageDetail>;

            if (trackingImages == null || trackingImages.Count == 0)
                return;

            // Set message.
            ViewScreenShot_messageTextBox.Text = trackingImages[pageNo].Message;

            if (isLog == false)
            {
                if (viewType == "CP")
                {
                    ViewScreenShot_previewImage.ImageUrl = "~/Common/ImageHandler.ashx?source=CP&ImageID="
                      + trackingImages[pageNo].ImageID + "&isThumb=0";
                }
                else if (viewType == "OI")
                {
                    ViewScreenShot_previewImage.ImageUrl = "~/Common/ImageHandler.ashx?source=OI&ImageID="
                      + trackingImages[pageNo].ImageID + "&isThumb=0";
                }
            }
            else 
            {
                if (viewType == "CP")
                {
                    ViewScreenShot_previewImage.ImageUrl = "~/Common/ImageHandler.ashx?source=CPLOG&ImageID="
                      + trackingImages[pageNo].ImageID + "&imageType=" + imageType + "&logID=" + logID;
                }
                else if (viewType == "OI")
                {
                    ViewScreenShot_previewImage.ImageUrl = "~/Common/ImageHandler.ashx?source=OILOG&ImageID="
                      + trackingImages[pageNo].ImageID + "&imageType=" + imageType + "&logID=" + logID;
                }
            }
        }

        #endregion Private Methods                                             

        #region Overridden Methods                                             

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Overridden Methods                                          
    }
}