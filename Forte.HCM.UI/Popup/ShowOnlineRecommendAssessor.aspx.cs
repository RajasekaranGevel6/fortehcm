using System;
using System.Web.UI;
using Forte.HCM.UI.Common;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Data;

namespace Forte.HCM.UI.Popup
{
    public partial class ShowOnlineRecommendAssessor : PageBase
    {
        #region Event Handlers

        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const int _pageSize = 2;
        
        /// <summary>
        /// Gets or sets the current page assessor.
        /// </summary>
        /// <value>
        /// The current page neigh.
        /// </value>
        private int CurrentPageAssessor
        {
            get
            {
                if (this.ViewState["CurrentPageAssessor"] == null)
                    return 0;
                else
                    return Convert.ToInt16(this.ViewState["CurrentPageAssessor"].ToString());
            }
            set
            {
                this.ViewState["CurrentPageAssessor"] = value;
            }
        }


        #endregion Private Constants
        DataTable dttempTable = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button and focus.
                Page.Form.DefaultButton = ShowOnlineRecommendAssessor_topSearchButton.UniqueID;

                if (!IsPostBack)
                {
                    string skill=string.Empty;
                    ShowOnlineRecommendAssessor_assesserDetailsGridView.AllowSorting = false;
                    // Set default button and focus.
                    Page.Form.DefaultButton = ShowOnlineRecommendAssessor_topSearchButton.UniqueID;
                    ShowOnlineRecommendAssessor_showPerPageTextBox.Text = _pageSize.ToString();
                    //ShowOnlineRecommendAssessor_topSearchButton_Click(ShowOnlineRecommendAssessor_topSearchButton, new EventArgs());


                    int positionProfileId = 0;
                    if (Session["SESSION_POSITION_PROFILE_ID"] != null)
                        positionProfileId = Convert.ToInt32(Session["SESSION_POSITION_PROFILE_ID"].ToString());
                    ScheduleOnlineRecommendationAssessor_candidateNameImageButton.Attributes.Add("onclick", "return LoadCandidate('"
                    + ScheduleOnlineRecommendationAssessor_Popup_candidateNameTextBox.ClientID + "','"
                    + ScheduleOnlineRecommendationAssessor_Popup_emailTextBox.ClientID + "','"
                    + ScheduleOnlineRecommendationAssessor_candidateIdHiddenField.ClientID
                    + "')");

                    // Assign handler for 'load position profile candidate' icon.
                    ScheduleOnlineRecommendationAssessor_positionProfileCandidateImageButton.Attributes.Add("onclick", "return LoadPositionProfileCandidates('"
                       + ScheduleOnlineRecommendationAssessor_Popup_candidateNameTextBox.ClientID + "','"
                       + ScheduleOnlineRecommendationAssessor_Popup_emailTextBox.ClientID + "','"
                       + ScheduleOnlineRecommendationAssessor_candidateIdHiddenField.ClientID + "','"
                       + positionProfileId + "')");


                    ShowOnlineRecommendAssessor_SearchSkillButton.Attributes.Add("onclick",
                      "return LoadSkillLookup('" + ShowOnlineRecommendAssessor_CaegoryIDHiddenField.ClientID + "','" +
                      ShowOnlineRecommendAssessor_skillsTextBox.ClientID + "','" +
                      ShowOnlineRecommendAssessor_SkillIDHiddenField.ClientID + "','" + ShowOnlineRecommendAssessor_NewSkillButton.ClientID + "')");

                    ShowOnlineRecommendAssessor_addAssessorLinkButton.Attributes.Add("onclick",
                      "return ShowSearchAssessor('" + ShowOnlineRecommendAssessor_addedUserIdHiddenField.ClientID +
                      "','" + ShowOnlineRecommendAssessor_assessorNameTextBox.ClientID + "','" +
                      ShowOnlineRecommendAssessor_refreshGridButton.ClientID + "','Y');");

                    if (Request.QueryString["flag"].ToString().Trim().ToUpper() == "CANDIDATEKEY")
                    {
                        ScheduleOnlineRecommendationAssessor_viewScheduleImageButton.Visible = true;
                        ScheduleOnlineRecommendationAssessor_viewScheduleImageButton.Attributes.Add("onclick",
                           "return ShowViewOnlineSchedule('" + Request.QueryString["from"].ToString() + "');");
                    }
                    else
                        ScheduleOnlineRecommendationAssessor_viewScheduleImageButton.Visible = false;
                    //Clear selected timeslots sessions
                    Session["ONLINE_INTERVIEW_SCHEDULER_SELECTED_TIME_SLOTS"] = null;
                    //Clear the newly requested list
                    Session["AVAILABILITY_REQUEST_LIST"] = null;
                    AssignCandidateDetails();
                    FormAssessorSkillMatrix();
                }
                ScheduleOnlineRecommendationAssessor_Popup_createCandidateButton.Attributes.Add("onclick",
                "return ShowCandidatePopup('" + ScheduleOnlineRecommendationAssessor_Popup_candidateNameTextBox.ClientID + "','" + ScheduleOnlineRecommendationAssessor_Popup_emailTextBox.ClientID + "','" + ScheduleOnlineRecommendationAssessor_candidateIdHiddenField.ClientID + "')");

                LoadAssessorGrid();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                    exp.Message.ToString());
            }
        }

        private void AssignCandidateDetails()
        {
           if (Request.QueryString["flag"].ToString().ToUpper() == "CANDIDATEKEY")
            {
                //Get candidate details
                OnlineCandidateSessionDetail candidateDetails = new OnlineInterviewBLManager().
                    GetCandidateDetailsByCandidateSessionKey(Request.QueryString["from"]);
                if (candidateDetails != null)
                {
                    ScheduleOnlineRecommendationAssessor_candidateNameLabel.Text =
                       candidateDetails.CandidateName;
                    ScheduleOnlineRecommendationAssessor_emailLabel.Text =
                        candidateDetails.CandidateEmail;

                    ScheduleOnlineRecommendationAssessor_Popup_candidateNameTextBox.Visible = false;
                    ScheduleOnlineRecommendationAssessor_Popup_emailTextBox.Visible = false;
                    ScheduleOnlineRecommendationAssessor_candidateNameImageButton.Visible = false;
                    ScheduleOnlineRecommendationAssessor_positionProfileCandidateImageButton.Visible = false;
                    ScheduleOnlineRecommendationAssessor_Popup_createCandidateButton.Visible = false;
                }
            }
        }

        /// <summary>
        /// Assign selected subject tables
        /// </summary>
        private void AssignSubjectTable()
        {
            // Retrieve and assign selected skills
            ShowOnlineRecommendAssessor_skillGridView.DataSource = (DataTable)Session["SESSION_SUBJECT_TABLE"];
            ShowOnlineRecommendAssessor_skillGridView.DataBind();
        }

        private void FormAssessorSkillMatrix()
        {
            string skillSearched = string.Empty;
            string skillSearchedText = string.Empty;
            string selectedSubjectIds = string.Empty;
            string assessorsMatched = string.Empty;
            string assessorTimeSlotMatched = string.Empty;
            string assessorInterviewMatched = string.Empty;
            string flag = string.Empty;
            DataTable dtFullData = null;
            flag = Request.QueryString["flag"].ToString().ToUpper();
            if (flag == "SEARCH")
            {
                ShowOnlineRecommendAssessor_ShowSearchCriteriaTr.Style["display"] = "block";
                ShowOnlineRecommendAssessor_ShowSortingTr.Style["display"] = "block";
                ShowOnlineRecommendAssessor_ShowCandidateOptionTr.Style["display"] = "none";
                ShowOnlineRecommendAssessor_searchAssessor.Text = "Search and Add Assessors";
                Master.SetPageCaption("Search and Add Assessors");

                ShowOnlineRecommendAssessor_scheduleButton.Visible = false;
                ShowOnlineRecommendAssessor_addAssessorButton.Visible = true;
                ShowOnlineRecommendAssessor_saveSlotsButton.Visible = false;
                AssignSubjectTable();
                DataTable dtSkillSubject = (DataTable)Session["SESSION_SUBJECT_TABLE"];

                if (dtSkillSubject != null && dtSkillSubject.Rows.Count != 0)
                    foreach (DataRow row in dtSkillSubject.Rows)
                        selectedSubjectIds = selectedSubjectIds + row["SkillID"].ToString().Trim() + ",";

                selectedSubjectIds = selectedSubjectIds.ToString().TrimEnd(',');

                List<Subject> subjects = new List<Subject>();
                subjects = new CommonBLManager().GetSubjects(selectedSubjectIds);

                foreach (Subject subject in subjects)
                {
                    skillSearched = skillSearched + subject.SubjectID + ",";
                    skillSearchedText = skillSearchedText + subject.SubjectName + ",";
                }

                skillSearched = skillSearched.ToString().TrimEnd(',');
                skillSearchedText = skillSearchedText.ToString().TrimEnd(',');

                dtFullData = new AssessorBLManager().GetAssessorListBySkill(base.tenantID, skillSearched,0);

                //If no matches found
                if (dtFullData.Rows.Count == 0)
                {
                    AssignEmptyGrid();
                    return;
                }
            }
            else
            {
                ShowOnlineRecommendAssessor_scheduleButton.Visible = true;
                ScheduleOnlineInterviewCandidate_mandatoryLabel.Visible = true;
                ShowOnlineRecommendAssessor_saveSlotsButton.Visible = false;
                if (flag.ToString().ToUpper() == "CANDIDATEKEY")
                {
                    Master.SetPageCaption("Add Assessor/Time Slots");
                    ShowOnlineRecommendAssessor_searchAssessor.Text = "Add Assessor/Time Slots";
                    ScheduleOnlineInterviewCandidate_mandatoryLabel.Visible = false;
                    ShowOnlineRecommendAssessor_saveSlotsButton.Visible = true;
                    ShowOnlineRecommendAssessor_scheduleButton.Visible = false;
                }
                else
                {
                    ShowOnlineRecommendAssessor_searchAssessor.Text = "Schedule Candidate/Change Assessor";
                    Master.SetPageCaption("Schedule Candidate/Change Assessor");
                }

                ShowOnlineRecommendAssessor_ShowSearchCriteriaTr.Style["display"] = "block";
                ShowOnlineRecommendAssessor_ShowSortingTr.Style["display"] = "block";
                ShowOnlineRecommendAssessor_ShowCandidateOptionTr.Style["display"] = "block";
                
                
                ShowOnlineRecommendAssessor_addAssessorButton.Visible = false;
                
                
                string key = string.Empty; /* Interview Session Key or Candidate Session Key */
                key = Request.QueryString["Key"].ToString();

                DataSet assessorAndSkillDataset = new InterviewSessionBLManager().
                            GetOnlineAssessorAndSkillBySessionOrCandidateKey(key,
                                flag.ToString().ToUpper());
                dtFullData = assessorAndSkillDataset.Tables[0];


                //If no matches found
                if (dtFullData.Rows.Count == 0)
                {
                    AssignEmptyGrid();
                    return;
                }

                DataTable dtSessionSubjectTable = new DataTable();
                dtSessionSubjectTable = (DataTable)Session["SESSION_SUBJECT_TABLE"];
                if (dtSessionSubjectTable == null || dtSessionSubjectTable.Rows.Count==0)
                {
                    DataTable dtSkillSet = assessorAndSkillDataset.Tables[1];
                    if (dtSkillSet.Rows.Count > 0)
                        foreach (DataRow row in dtSkillSet.Rows)
                            selectedSubjectIds = selectedSubjectIds + row["SKILL_ID"].ToString() + ",";
                }
                else
                {
                    foreach (DataRow row in dtSessionSubjectTable.Rows)
                        selectedSubjectIds = selectedSubjectIds + row["SkillID"].ToString().Trim() + ",";
                    
                    selectedSubjectIds = selectedSubjectIds.ToString().TrimEnd(',');
                }
                List<Subject> subjects = new List<Subject>();
                subjects = new CommonBLManager().GetSubjects(selectedSubjectIds.ToString().TrimEnd(','));

                DataTable dtConstructSubjectTable = new DataTable();

                dtConstructSubjectTable.Columns.Add("Category", typeof(string));
                dtConstructSubjectTable.Columns.Add("SkillID", typeof(int));
                dtConstructSubjectTable.Columns.Add("Skill", typeof(string));
                dtConstructSubjectTable.AcceptChanges();

                foreach (Subject subject in subjects)
                {
                    skillSearched = skillSearched + subject.SubjectID + ",";
                    skillSearchedText = skillSearchedText + subject.SubjectName + ",";
                    DataRow dr = dtConstructSubjectTable.NewRow();
                    dr[0] = subject.CategoryName;
                    dr[1] = subject.SubjectID;
                    dr[2] = subject.SubjectName;
                    dtConstructSubjectTable.Rows.Add(dr);
                    dtConstructSubjectTable.AcceptChanges();
                }
                Session["SESSION_SUBJECT_TABLE"] = dtConstructSubjectTable;
                AssignSubjectTable();
                skillSearched = skillSearched.ToString().TrimEnd(',');
                skillSearchedText = skillSearchedText.ToString().TrimEnd(',');
            }
            
            //Split the searched skill text
            string[] skillMatrix = skillSearched.Split(',');
            //Split the searched skill id
            string[] skillMatrixText = skillSearchedText.Split(',');
            //Get assessors that have provided time slots
            if (ShowOnlineRecommendAssessor_haveTimeSlotsCheckBox.Checked)
            {
                string status = "A"; // I - Initiated, A - Accepted
                DataTable dtAssListByHaveTimeSlots =
                    new InterviewSessionBLManager().GetAssessorListByHaveProvidedTimeSlots(status);
                if (dtAssListByHaveTimeSlots == null || dtAssListByHaveTimeSlots.Rows.Count == 0)
                {
                    AssignEmptyGrid();
                    return;
                }
                else
                    foreach (DataRow row in dtAssListByHaveTimeSlots.Rows)
                        assessorTimeSlotMatched = assessorTimeSlotMatched + row["ASSESSOR_ID"].ToString().Trim() + ",";
            }

            //Get assessor list who interviewed past for this particular client
            if (ShowOnlineRecommendAssessor_interviewedPastCheckBox.Checked)
            {
                int positionProfileId = 0;
                if (Session["SESSION_POSITION_PROFILE_ID"] != null)
                    positionProfileId = Convert.ToInt32(Session["SESSION_POSITION_PROFILE_ID"].ToString());

                DataTable dtAssListByProfileId = new AssessorBLManager().
                    GetAssessorListByPositionProfile(positionProfileId);

                if (dtAssListByProfileId == null || dtAssListByProfileId.Rows.Count == 0)
                {
                    AssignEmptyGrid();
                    return;
                }
                else
                    foreach (DataRow row in dtAssListByProfileId.Rows)
                        assessorInterviewMatched = assessorInterviewMatched + row["ASSESSOR_ID"].ToString().Trim() + ",";
            }

            if (ShowOnlineRecommendAssessor_matchesAllAreasCheckBox.Checked)
            {
                //Get maximum skill matched for each assessor
                DataView dvFilterMatched = new DataView(dtFullData);
                dvFilterMatched.RowFilter = "MATCHED_COUNT <> 0";
                DataTable dtMaxSkillMatched = dvFilterMatched.ToTable().
                    DefaultView.ToTable(false, "MATCHED_COUNT", "MATCHED_USERID");
                dtMaxSkillMatched.AcceptChanges();

                //Get the searched skill count
                int skillSearchCnt = skillMatrix.Length;

                //To find who matched all
                DataView dvFilterMatchedAllAreas = new DataView(dtMaxSkillMatched);
                dvFilterMatchedAllAreas.RowFilter = "MATCHED_COUNT=" + skillSearchCnt;
                DataTable dtMaxSkillMatchedAssers = dvFilterMatchedAllAreas.ToTable().
                    DefaultView.ToTable(false, "MATCHED_USERID");
                dtMaxSkillMatchedAssers.AcceptChanges();
                if (dtMaxSkillMatchedAssers == null || dtMaxSkillMatchedAssers.Rows.Count == 0)
                {
                    AssignEmptyGrid();
                    return;
                }
                else
                    foreach (DataRow row in dtMaxSkillMatchedAssers.Rows)
                        assessorsMatched = assessorsMatched + row["MATCHED_USERID"].ToString().Trim() + ",";
            }
            //Get distinctData from retrieved data table
            DataTable distinctData = dtFullData.DefaultView.ToTable(true, "USER_ID", "EMAIL", "ASSESSOR_NAME");
            distinctData.AcceptChanges();
            //Remove empty rows from distinct data table
            DataView dvFilterEmpty = new DataView(distinctData);
            dvFilterEmpty.RowFilter = "Isnull(EMAIL,'') <> ''";
            DataTable dtAssesorSet = dvFilterEmpty.ToTable();
            dtAssesorSet.AcceptChanges();

            if (ShowOnlineRecommendAssessor_matchesAllAreasCheckBox.Checked)
            {
                //Filter by matches all reas
                assessorsMatched = assessorsMatched.ToString().Trim().TrimEnd(',');
                DataView dvMatches = new DataView(dtAssesorSet);
                dvMatches.RowFilter = "USER_ID IN (" + assessorsMatched + ")";
                dtAssesorSet = null;
                dtAssesorSet = dvMatches.ToTable();
            }
            if (ShowOnlineRecommendAssessor_haveTimeSlotsCheckBox.Checked)
            {
                //Filter by who have time slots
                assessorTimeSlotMatched = assessorTimeSlotMatched.ToString().Trim().TrimEnd(',');
                DataView dvTimeSlotMatches = new DataView(dtAssesorSet);
                dvTimeSlotMatches.RowFilter = "USER_ID IN (" + assessorTimeSlotMatched + ")";
                dtAssesorSet = null;
                dtAssesorSet = dvTimeSlotMatches.ToTable();
            }
            if (ShowOnlineRecommendAssessor_interviewedPastCheckBox.Checked)
            {
                //Filter by who interviewd past
                assessorInterviewMatched = assessorInterviewMatched.ToString().Trim().TrimEnd(',');
                DataView dvInterviewedMatches = new DataView(dtAssesorSet);
                dvInterviewedMatches.RowFilter = "USER_ID IN (" + assessorInterviewMatched + ")";
                dtAssesorSet = null;
                dtAssesorSet = dvInterviewedMatches.ToTable();
            }

            dtAssesorSet.AcceptChanges();
            if (dtAssesorSet.Rows.Count == 0)
            {
                AssignEmptyGrid();
                return;
            }
            //Construct new data table with filtered data
            DataTable dtGenerateColumn = new DataTable();
            dtGenerateColumn.Columns.Add("Skills/Areas");

            //Get top matches enabled
            int topColumnCnt = 0;
            if (ShowOnlineRecommendAssessor_showTopRadioButton.Checked)
            {
                topColumnCnt = Convert.ToInt32(ShowOnlineRecommendAssessor_topMatchesTextBox.Text);
                if (topColumnCnt > dtAssesorSet.Rows.Count)
                    topColumnCnt = dtAssesorSet.Rows.Count;
            }
            else
                topColumnCnt = dtAssesorSet.Rows.Count;

            for (int k = 0; k <= topColumnCnt - 1; k++)
                dtGenerateColumn.Columns.Add(string.Concat(dtAssesorSet.Rows[k]["ASSESSOR_NAME"].
                    ToString().Trim(), ":", dtAssesorSet.Rows[k]["EMAIL"].ToString().Trim(),
                    ":", dtAssesorSet.Rows[k]["USER_ID"].ToString().Trim()));

            dtGenerateColumn.AcceptChanges();
            int m = 0;
            int columnCount = 0;
            columnCount = dtGenerateColumn.Columns.Count;
            ViewState["columnCount"] = columnCount;
            string[] rowValues = new string[columnCount];
            string rowCloumnName = string.Empty;
            foreach (string skill in skillMatrix)
            {
                string[] splitColumnName = null;
                string expression = string.Empty;
                string skillMatched = string.Empty;
                string defultCellSelected = string.Empty;
                int isExpAssId = 0;
                int isExpSkillId = 0;
                defultCellSelected = "N";

                DataRow drAddNewRow = dtGenerateColumn.NewRow();
                rowCloumnName = string.Concat(skillMatrixText[m].ToString().Trim().ToUpper(), ":", skill.ToString().Trim());
                rowValues[0] = rowCloumnName;
                for (int i = 1; i <= columnCount - 1; i++)
                {
                    splitColumnName = dtGenerateColumn.Columns[i].ColumnName.Split(':');

                    // Construct filter expression.
                    expression = "EMAIL='" + splitColumnName[1].ToString().Trim() +
                        "' AND SKILL_MATCHED='" + skill.ToString().Trim() + "'";

                    DataRow[] filteredRows = null;
                    bool isExpertise;
                    if (flag.ToString().ToUpper() != "SEARCH")
                    {
                        if (splitColumnName[2].ToString().Trim() != string.Empty)
                            isExpAssId = Convert.ToInt32(splitColumnName[2].ToString());

                        if (skill.ToString().Trim() != string.Empty)
                            isExpSkillId = Convert.ToInt32(skill.ToString());
                        isExpertise = new OnlineInterviewBLManager().IsAssessorExpertise(isExpAssId, isExpSkillId);
                        if (isExpertise)
                            skillMatched = "Y";
                        else
                            skillMatched = "N";

                        // Get filtered rows.
                        filteredRows = dtFullData.Select(expression);
                        if (filteredRows == null || filteredRows.Length == 0)
                            defultCellSelected = "N";
                        else
                            defultCellSelected = "Y";
                    }
                    else
                    {
                        // Get filtered rows.
                        filteredRows = dtFullData.Select(expression);
                        if (filteredRows == null || filteredRows.Length == 0)
                            skillMatched = "N";
                        else
                            skillMatched = "Y";
                    }
                    //if (flag.ToString() != "SEARCH")
                     //   defultCellSelected = skillMatched;

                    //rowValues[i] = string.Concat(skillMatched, ":", defultCellSelected);
                    rowValues[i] = string.Concat(skillMatched, ":", defultCellSelected, ":",
                            skill.ToString().Trim(), ":", splitColumnName[2].ToString().Trim());

                    if (i == dtGenerateColumn.Columns.Count - 1)
                    {
                        drAddNewRow.ItemArray = rowValues;
                        dtGenerateColumn.Rows.Add(drAddNewRow);
                        dtGenerateColumn.AcceptChanges();
                    }
                }
                m++;
            }

            //Assign additional skill/areas at the end
            DataRow drAddSkill = dtGenerateColumn.NewRow();
            rowValues[0] = "";
            for (int j = 1; j <= columnCount - 1; j++)
                rowValues[j] = "N";

            drAddSkill.ItemArray = rowValues;
            dtGenerateColumn.Rows.Add(drAddSkill);
            dtGenerateColumn.AcceptChanges();
            ViewState["DATALIST"] = null;
            ViewState["DATALIST"] = dtGenerateColumn;

            //For pageing 
            ViewState["DATALIST_FULL"] = dtGenerateColumn;
            ShowOnlineRecommendAssessor_pageNumberHiddenField.Value = "1";
            FilterDataTable(string.Empty);
        }
        /// <summary>
        /// Handles search option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowOnlineRecommendAssessor_topSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                if(!IsValidData())
                    return;
                FormAssessorSkillMatrix();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler to delete the skill matrix
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowOnlineRecommendAssessor_skillGridView_RowDeleting(object sender, 
            GridViewDeleteEventArgs e)
        {
            HiddenField SubjectValue_HiddenFiled = (HiddenField)ShowOnlineRecommendAssessor_skillGridView.
                Rows[e.RowIndex].FindControl("ShowOnlineRecommendAssessor_skillGridView_skillIDHiddenField");

            if (string.IsNullOrEmpty(SubjectValue_HiddenFiled.Value)) return;

            dttempTable = (DataTable)Session["SESSION_SUBJECT_TABLE"];

            foreach (DataRow _dr in dttempTable.Rows)
            {
                if (SubjectValue_HiddenFiled.Value.ToString().Trim() ==
                    Convert.ToString(_dr["SkillID"]))
                {
                    dttempTable.Rows.Remove(_dr);
                    dttempTable.AcceptChanges();
                    break;
                }
            }

            Session["SESSION_SUBJECT_TABLE"] = dttempTable;

            ShowOnlineRecommendAssessor_skillGridView.DataSource = dttempTable;
            ShowOnlineRecommendAssessor_skillGridView.DataBind();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowOnlineRecommendAssessor_NewSkillButton_Click
          (object sender, ImageClickEventArgs e)
        {
            ShowOnlineRecommendAssessor_topErrorMessageLabel.Text=string.Empty;
            if (ShowOnlineRecommendAssessor_skillsTextBox.Text.Trim() == string.Empty)
            {
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,"No skill entered to add");
                return;
            }
            dttempTable = new DataTable();

            if (Session["SESSION_SUBJECT_TABLE"] == null)
            {
                dttempTable.Columns.Add("Category", typeof(string));
                dttempTable.Columns.Add("SkillID", typeof(int));
                dttempTable.Columns.Add("Skill", typeof(string));
                dttempTable.AcceptChanges();
            }
            else
                dttempTable = (DataTable)Session["SESSION_SUBJECT_TABLE"];

            string[] searchedSkill = null;

            searchedSkill = ShowOnlineRecommendAssessor_skillsTextBox.Text.ToString().Split(',');


            foreach (string skill in searchedSkill)
            {
                bool skillExist = false;
                AddManual_Skill(skill);
                string categoryID_HiddenField = ShowOnlineRecommendAssessor_CaegoryIDHiddenField.Value.ToString().Trim();
                string sbjectID_HiddenField = ShowOnlineRecommendAssessor_SkillIDHiddenField.Value.ToString();

                if (dttempTable.Rows.Count > 0)
                {

                    foreach (DataRow drow in dttempTable.Rows)
                    {
                        if (Convert.ToString(drow[0]) == Convert.ToString(categoryID_HiddenField)
                            && Convert.ToInt32(drow[1]) == Convert.ToInt32(sbjectID_HiddenField))
                        {
                            this.ShowOnlineRecommendAssessor_skillsTextBox.Focus();
                            base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                                string.Concat(skill, " already added"));
                            skillExist = true;
                        }
                    }
                }
                if (!skillExist)
                {
                    DataRow dr = dttempTable.NewRow();
                    dr[0] = categoryID_HiddenField;
                    dr[1] = sbjectID_HiddenField;
                    dr[2] = skill;

                    dttempTable.Rows.Add(dr);
                    dttempTable.AcceptChanges();

                    Session["SESSION_SUBJECT_TABLE"] = dttempTable;
                }
            }


            ShowOnlineRecommendAssessor_skillsTextBox.Text = "";
            ShowOnlineRecommendAssessor_CaegoryIDHiddenField.Value = "";
            ShowOnlineRecommendAssessor_SkillIDHiddenField.Value = "";

            ShowOnlineRecommendAssessor_skillGridView.DataSource = null;
            ShowOnlineRecommendAssessor_skillGridView.DataBind();

            ShowOnlineRecommendAssessor_skillGridView.DataSource = dttempTable;
            ShowOnlineRecommendAssessor_skillGridView.DataBind();
        }
        #region Assessor Next Page
        /// <summary>
        /// Handles the Click event of the ShowOnlineRecommendAssessor_nextImageButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void ShowOnlineRecommendAssessor_nextImageButton_Click(object sender, EventArgs e)
        {
           CurrentPageAssessor += 1;
           LoadAssessorGrid();
        }
        #endregion

        #region Assessor Previous Page
        /// <summary>
        /// Handles the Click event of the ShowOnlineRecommendAssessor_previousImageButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void ShowOnlineRecommendAssessor_previousImageButton_Click(object sender, EventArgs e)
        {
           CurrentPageAssessor -= 1;
           LoadAssessorGrid();
        }
        #endregion
        

        #endregion Event Handlers

        #region Private Methods
        /// <summary>
        /// Method that clears the existing and data and grid
        /// </summary>
        private void AssignEmptyGrid()
        {
            ViewState["DATALIST"] = null;
            LoadAssessorGrid();
        }

        /// <summary>
        /// 
        /// </summary> 
        /// <param name="skill">
        /// <see cref="string"/>
        /// </param>
        private void AddManual_Skill(string skill)
        {
            DataTable dttemp = new InterviewBLManager().getSubjectCategoryID(skill);
            if (dttemp.Rows.Count != 0)
            {
                ShowOnlineRecommendAssessor_CaegoryIDHiddenField.Value = Convert.ToString(dttemp.Rows[0][0]);
                ShowOnlineRecommendAssessor_SkillIDHiddenField.Value = Convert.ToString(dttemp.Rows[0][1]);
                ShowOnlineRecommendAssessor_skillsTextBox.Text = Convert.ToString(dttemp.Rows[0][2]);
            }
        }
        /// <summary>
        /// Method to construct the session table;
        /// </summary>
        private  bool ConstructSession()
        {
            // Clear messages.
            ClearMessages();
            int i = 0;
            int rowCnt = 0;
            int colCnt = 0;
            int j = 0;
            bool flagSave = false;
            
            SessionDetail sessionDetail = new SessionDetail();
            sessionDetail.SessionName = "Online Interview Session";
            sessionDetail.SessionKey = string.Empty;

            sessionDetail.Assessors = new List<AssessorDetail>();
            AssessorDetail assessorDetail = null;

            List<AssessorDetail> assessorDetails = null;
            Session["ASSESSOR"] = null;
            assessorDetails = new List<AssessorDetail>();
            
            //Data Table
            DataTable dtAssessor = (DataTable)ViewState["DATALIST_FULL"];
            rowCnt = dtAssessor.Rows.Count;
            colCnt = dtAssessor.Columns.Count;

            for (i = 0; i <= dtAssessor.Columns.Count - 1; i++)
            {
                string rowValues = string.Empty;
                string[] arrayRowValues = new string[4];
                string ColumnName = string.Empty;
                string[] arrColumn = null;
                string skillColumn = string.Empty;
                ColumnName = dtAssessor.Columns[i].ColumnName.ToString();
                arrColumn = ColumnName.ToString().Trim().Split(':');
                if (dtAssessor.Rows[rowCnt-1][dtAssessor.Columns[i].ColumnName].ToString() == "Y")
                {
                    
                    string skillIds = string.Empty;
                    assessorDetail = new AssessorDetail();
                    assessorDetail.Skills = new List<SkillDetail>();
                    for (j = 0; j <= rowCnt - 2; j++)
                    {
                        rowValues = dtAssessor.Rows[j][dtAssessor.Columns[i].ColumnName].ToString();
                        if (rowValues.ToString().Trim().Split(':')[1] == "Y")
                        {
                            flagSave = true;
                            skillColumn = dtAssessor.Rows[j]["Skills/Areas"].ToString();
                            skillIds = skillIds + skillColumn.ToString().Trim().Split(':')[1] + ",";
                            assessorDetail.Skills.Add(new
                                SkillDetail(Convert.ToInt32(skillColumn.ToString().Trim().Split(':')[1].ToString()),
                                skillColumn.ToString().Trim().Split(':')[0].ToString()));
                        }
                    }

                    //For session Construction
                    assessorDetail.UserID = Convert.ToInt32(arrColumn[2].ToString());
                    assessorDetail.FirstName = arrColumn[0].ToString();
                    assessorDetail.UserEmail = arrColumn[1].ToString();
                    assessorDetail.AlternateEmailID = arrColumn[1].ToString();
                    sessionDetail.Assessors.Add(assessorDetail);

                    //To assign to parent grid and to db values
                    assessorDetail.Skill = skillIds.ToString().Trim().TrimEnd(',');
                    assessorDetails.Add(assessorDetail);
                    //End session construction
                  }
            }

            //Launch My Availability
            Session["ASSESSOR"] = assessorDetails;
            Session["SESSION_ASSESSORS"] = sessionDetail;
            return flagSave;
        }
        /// <summary>
        /// Method to save the scheduled candidate details and selected timeslots against this candidate
        /// </summary>
        private bool SaveScheduleCandidate()
        {
            string candidateSessionKey = string.Empty;
            List<AssessorTimeSlotDetail> timeSlots = null;
            timeSlots = Session["ONLINE_INTERVIEW_SCHEDULER_SELECTED_TIME_SLOTS"] as List<AssessorTimeSlotDetail>;
            if (timeSlots == null)
                return false;

            if (Utility.IsNullOrEmpty(ScheduleOnlineRecommendationAssessor_candidateIdHiddenField.Value))
            {
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                   "Please select candidate name");
                return false;
            }
            candidateSessionKey=Request.QueryString["from"].ToString();
            //Assign candidate id and email id 
            OnlineCandidateSessionDetail onlineCandidateSessionDetail = new OnlineCandidateSessionDetail();
            onlineCandidateSessionDetail.CandidateSessionID = candidateSessionKey;
            onlineCandidateSessionDetail.EmailId = 
                ScheduleOnlineRecommendationAssessor_Popup_emailTextBox.Text.ToString().Trim();
            onlineCandidateSessionDetail.CandidateID = 0;
          
            //Method to call online schedule candidate
            new OnlineInterviewBLManager().ScheduleOnlineInterviewCandidate
                (onlineCandidateSessionDetail, timeSlots, base.userID);
            
            //Save RequestedTimeSlots
            RequestedTimeSlots();
           
            return true;
        }

        /// <summary>
        /// Method to save the requested time slots
        /// </summary>
        private void RequestedTimeSlots()
        {
            string onlineInterviewSessionKey = string.Empty;
            onlineInterviewSessionKey = Request.QueryString["key"].ToString();
            //Add requested time slots if anything included while scheduling
            List<AssessorTimeSlotDetail> slots = Session["AVAILABILITY_REQUEST_LIST"] as List<AssessorTimeSlotDetail>;
            if (slots != null)
                new OnlineInterviewBLManager().InsertAssessorTimeSlot(slots, onlineInterviewSessionKey, base.userID);
        }

        /// <summary>
        /// Method to save the requested time slots
        /// </summary>
        private void InsertCandidateTimeSlot()
        {
            string candidateSessionKey = string.Empty;
            candidateSessionKey = Request.QueryString["from"].ToString();
            List<AssessorTimeSlotDetail> timeSlots = null;
            timeSlots = Session["ONLINE_INTERVIEW_SCHEDULER_SELECTED_TIME_SLOTS"] as List<AssessorTimeSlotDetail>;

            if (timeSlots != null)
                new OnlineInterviewBLManager().InsertCandidateTimeSlot(timeSlots, candidateSessionKey, base.userID);
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            
            if (ShowOnlineRecommendAssessor_showTopRadioButton.Checked && 
                ShowOnlineRecommendAssessor_topMatchesTextBox.Text.Trim()=="0")
            {
                isValidData = false;
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                    "Please select the show top matches");
            }

            if (ShowOnlineRecommendAssessor_showAllRadioButton.Checked &&
                ShowOnlineRecommendAssessor_topMatchesTextBox.Text.Trim() != "0")
                ShowOnlineRecommendAssessor_topMatchesTextBox.Text = "0";

            if (ShowOnlineRecommendAssessor_showPerPageTextBox.Text.Trim() == "0")
            {
                isValidData = false;
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                    "Assessors per page should be greater than zero");
            }
            return isValidData;
        }

       
        /// <summary>
        /// Method assigns the searched list to grid
        /// </summary>
        private void LoadAssessorGrid()
        {
            ShowOnlineRecommendAssessor_topErrorMessageLabel.Text = string.Empty;
            if (ViewState["DATALIST"] == null || ((DataTable)ViewState["DATALIST"]).Rows.Count == 0 || ((DataTable)ViewState["DATALIST"]).Rows.Count == 1)
            {
                
                ShowOnlineRecommendAssessor_assesserDetailsGridView.AutoGenerateColumns = true;
                ShowOnlineRecommendAssessor_assesserDetailsGridView.DataSource = null;
                ShowOnlineRecommendAssessor_assesserDetailsGridView.DataBind();
                ShowOnlineRecommendAssessor_ShowAssesserResultsTR.Visible = false;
                ShowOnlineRecommendAssessor_ShowAssesserResultsHeaderTR.Visible = false;
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel, "No data found to display");
                return;
            }
            ShowOnlineRecommendAssessor_ShowAssesserResultsTR.Visible = true;
            ShowOnlineRecommendAssessor_ShowAssesserResultsHeaderTR.Visible = true;

            ShowOnlineRecommendAssessor_assesserDetailsGridView.AutoGenerateColumns = true;
            ShowOnlineRecommendAssessor_assesserDetailsGridView.DataSource = ViewState["DATALIST"];
            ShowOnlineRecommendAssessor_assesserDetailsGridView.DataBind();
        }

        /// <summary>
        /// Method that clears the messages.
        /// </summary>
        private void ClearMessages()
        {
            ShowOnlineRecommendAssessor_topErrorMessageLabel.Text = string.Empty;
            ShowOnlineRecommendAssessor_topSuccessMessageLabel.Text = string.Empty;
        }


        #region onNavigatePage

        /// <summary>
        /// Called when [navigate page].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void OnNavigatePage(object sender, EventArgs e)
        {
            try
            {
                ImageButton navigateButton = (ImageButton)sender;
                FilterDataTable(navigateButton.CommandName);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                    exception.Message);
            }
        }

        #endregion

        #region Pageing Method
        /// <summary>
        /// Method handles/filters according to the page navigation and reassigns the data
        /// </summary>
        /// <param name="navigationType">navigationType</param>
        private void FilterDataTable(string navigationType)
        {
            int i = 0;
            int j = 0;
            int k = 0;

            int pageNum = 0;
            int pageSize = 0;
            int pageStartIndex=0;
            int pageEndIndex=0;

            pageSize = Convert.ToInt32(ShowOnlineRecommendAssessor_showPerPageTextBox.Text);
            pageNum = Convert.ToInt32(ShowOnlineRecommendAssessor_pageNumberHiddenField.Value);

            if (navigationType == "Previous")
                pageNum = pageNum - 1;
            if (navigationType == "Next")
                pageNum = pageNum + 1;
                
            pageStartIndex = ((pageNum - 1) * pageSize) + 1;
            pageEndIndex = pageNum * pageSize;
            ShowOnlineRecommendAssessor_pageNumberHiddenField.Value = pageNum.ToString();

            //Filter the data table
            DataTable dtFilterForPageing = ((DataTable)ViewState["DATALIST_FULL"]);

            //Check page index exceeds the total records
            if (pageEndIndex >= dtFilterForPageing.Columns.Count)
                pageEndIndex = dtFilterForPageing.Columns.Count-1;

            DataTable dtNewFiltered = new DataTable();
            dtNewFiltered.Columns.Add("Skills/Areas");
            for (i = pageStartIndex; i <= pageEndIndex; i++)
                dtNewFiltered.Columns.Add(dtFilterForPageing.Columns[i].ColumnName);

            int columCnt = 0;
            columCnt = dtNewFiltered.Columns.Count;
            string[] arrayFilteredRow = new string[columCnt];

            //Create row apply data
            for (k = 0; k <= dtFilterForPageing.Rows.Count - 1; k++)
            {
                DataRow drFilter = dtNewFiltered.NewRow();
                for (j = 0; j <= columCnt - 1; j++)
                    arrayFilteredRow[j] = dtFilterForPageing.Rows[k][dtNewFiltered.
                        Columns[j].ColumnName].ToString();
                
                drFilter.ItemArray = arrayFilteredRow;
                dtNewFiltered.Rows.Add(drFilter);
                dtNewFiltered.AcceptChanges();
            }
            ShowOnlineRecommendAssessor_previousImageButton.Enabled = true;
            ShowOnlineRecommendAssessor_nextImageButton.Enabled = true;

            if (pageStartIndex == 1)
                ShowOnlineRecommendAssessor_previousImageButton.Enabled=false;
            if (pageEndIndex >= dtFilterForPageing.Columns.Count-1)
                ShowOnlineRecommendAssessor_nextImageButton.Enabled = false;

            ViewState["DATALIST"] = dtNewFiltered;
            LoadAssessorGrid();
        }
        #endregion

        #endregion Private Methods

        #region Protected Overridden Methods



        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        /// <summary>
        /// Event handles the rowcommand functionalities
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowOnlineRecommendAssessor_assesserDetailsGridView_RowCommand(object sender, 
            GridViewCommandEventArgs e)
        {

            #region show skill popup
            if (e.CommandName == "ShowSkillPopup")
            {
                try
                {
                    ShowOnlineRecommendAssessor_addSkillErrorLabel.Text = null;
                    ShowOnlineRecommendAssessor_addSkillTextBox.Text = null;
                    ShowOnlineRecommendAssessor_addSkillModalPopupExtender.Show();
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                    base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel, exception.Message);

                }
            }
            #endregion

            #region select/unselect assessor skills
            if (e.CommandName == "Change")
            {
                string[] arqCollection = null;
                string[] arrCellValue = null;
                string cellValue = null;
                string imgType = string.Empty;
                string newCellValue = string.Empty;

                DataTable dtAssList = (DataTable)ViewState["DATALIST_FULL"];

                if (Convert.ToString(e.CommandArgument) != string.Empty)
                    arqCollection = e.CommandArgument.ToString().Trim().Split(',');

                //Retrieve existing cell value
                cellValue = dtAssList.Rows[Convert.ToInt32(arqCollection[0])]
                                [arqCollection[1]].ToString();

                if(!Utility.IsNullOrEmpty(cellValue))
                    arrCellValue = cellValue.ToString().Trim().Split(':');

                if (arrCellValue[1] == "Y")
                    imgType = "N";
                else
                    imgType = "Y";

                newCellValue = string.Concat(arrCellValue[0], ":", imgType, ":", arrCellValue[2],":",arrCellValue[3]);
                dtAssList.Rows[Convert.ToInt32(arqCollection[0])][arqCollection[1]] = newCellValue;
                dtAssList.AcceptChanges();
                ViewState["DATALIST_FULL"] = dtAssList;
                FilterDataTable(string.Empty);
                LoadAssessorGrid();
            }
            #endregion

            #region delete row from table
            if (e.CommandName == "Delete")
            {
                int delRowNo = Convert.ToInt32(e.CommandArgument);
                DataTable dtAssList = (DataTable)ViewState["DATALIST_FULL"];
                dtAssList.Rows.RemoveAt(delRowNo);
                dtAssList.AcceptChanges();
                ViewState["DATALIST_FULL"] = dtAssList;
                FilterDataTable(string.Empty);
            }
            #endregion

            #region link to profile
            if (e.CommandName == "ProfileLink")
            {
                try
                {
                    int assessorId=Convert.ToInt32(e.CommandArgument);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowAssessorProfile", "<script language='javascript'>ShowAssessorProfile(" + assessorId + ");</script>", false);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                    base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel, exception.Message);
                }
            }
            
            #endregion

            #region View Assessor Calendar
            if (e.CommandName == "ViewAssessorCalendar")
            {
                try
                {
                    int assessorId = Convert.ToInt32(e.CommandArgument);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowAssessorCalendar", "<script language='javascript'>ShowAssessorCalendar(" + assessorId + ");</script>", false);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                    base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel, exception.Message);
                }
            }

            #endregion
            #region show approved time slots
            if (e.CommandName == "TimeSlotsAvailable")
            {
                try
                {
                    int assessorId = 0;
                    int skillId = 0;
                    string[] selectedAssessors = null;
                    string interviewSessionKey = string.Empty;

                    interviewSessionKey = Request.QueryString["key"].ToString();
                    selectedAssessors = e.CommandArgument.ToString().Split(':');
                    skillId = Convert.ToInt32(selectedAssessors[0].ToString());
                    assessorId = Convert.ToInt32(selectedAssessors[1].ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowTimeSlots", "<script language='javascript'>ShowSearchTimeSlots('" + assessorId + "','" + interviewSessionKey + "','" + skillId + "','" + ShowOnlineRecommendAssessor_addTimeSlotHiddenField.ClientID + "');</script>", false);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                    base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel, exception.Message);
                }
            }
            #endregion
        }

        /// <summary>
        /// Handler method that is called when the refresh button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ShowOnlineRecommendAssessor_refreshGridButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Get the assessor details add assign 
                int userId = 0;
                string gridColumn = string.Empty;
                string skillColumn = string.Empty;
                string skill = string.Empty;
                int skillId = 0;

                if (!string.IsNullOrEmpty(ShowOnlineRecommendAssessor_addedUserIdHiddenField.Value))
                    userId = Convert.ToInt32(ShowOnlineRecommendAssessor_addedUserIdHiddenField.Value);

                //Get the assessor details
                AssessorDetail assessorDetail = new AssessorBLManager().
                    GetAssessorByUserId(userId);

                //Generate new colum
                gridColumn = string.Concat(assessorDetail.FirstName, " ", assessorDetail.LastName, ":",
                    assessorDetail.UserEmail, ":", assessorDetail.UserID);


                DataTable dtAssessorAppend = (DataTable)ViewState["DATALIST_FULL"];

                //Check whether column already exists already or not
                if (dtAssessorAppend.Columns.Contains(gridColumn))
                {
                    base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel, "Assessor is already included");
                    return;
                }

                dtAssessorAppend.Columns.Add(gridColumn);
                dtAssessorAppend.AcceptChanges();

                //Append Default Row Values
                bool isSkillExpertise=false;
                for (int i = 0; i <= dtAssessorAppend.Rows.Count - 1; i++)
                {
                    skillColumn = dtAssessorAppend.Rows[i]["Skills/Areas"].ToString();
                    if (!Utility.IsNullOrEmpty(skillColumn))
                    {
                        skill = skillColumn.ToString().Trim().Split(':')[1];
                        if (!Utility.IsNullOrEmpty(skill.ToString()))
                            skillId = Convert.ToInt32(skill.ToString());
                        isSkillExpertise = new OnlineInterviewBLManager().IsAssessorExpertise(userId,skillId);
                    }
                    if(isSkillExpertise)
                        dtAssessorAppend.Rows[i][gridColumn] = "Y:N:" + skillId + ":" + userId;
                    else
                        dtAssessorAppend.Rows[i][gridColumn] = "N:N:" + skillId + ":" + userId;
                }

                dtAssessorAppend.AcceptChanges();
                ViewState["DATALIST_FULL"] = dtAssessorAppend;
                FilterDataTable(string.Empty);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel, exp.Message);
            }
        }


        
        /// <summary>
        /// Handler to generate and apply image and calendar option for each cell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowOnlineRecommendAssessor_assesserDetailsGridView_RowDataBound(object sender, 
            GridViewRowEventArgs e)
        {
            string interviewSessionKey = string.Empty;
            interviewSessionKey = Request.QueryString["key"].ToString().Trim();
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int i = 0;
                string[] assessorData = null;
                for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                {
                    if (i == 0)
                    {
                        Label lblSkill = new Label();
                        lblSkill.Text = e.Row.Cells[i].Text;
                        e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[i].Controls.Add(lblSkill);
                        e.Row.Cells[i].Width = Unit.Pixel(150);
                    }
                    else
                    {
                        LinkButton lnkProfile = new LinkButton();
                        e.Row.Cells[i].Wrap = true;
                        if (!Utility.IsNullOrEmpty(e.Row.Cells[i].Text))
                        {
                            assessorData = e.Row.Cells[i].Text.ToString().Trim().Split(':');
                            lnkProfile.Text = assessorData[0].ToString();
                            lnkProfile.CommandArgument = assessorData[2].ToString();
                            lnkProfile.ToolTip = "Click here to view assessor profile";
                            lnkProfile.CommandName = "ProfileLink";
                            lnkProfile.CssClass = "profile_link_text";
                            e.Row.Cells[i].BorderColor = System.Drawing.Color.Red;
                            e.Row.Cells[i].Controls.Add(lnkProfile);

                            Label lblSpaceAssessorCalendar = new Label();
                            lblSpaceAssessorCalendar.Text="&nbsp;&nbsp;";
                            e.Row.Cells[i].Controls.Add(lblSpaceAssessorCalendar);

                            ImageButton ShowOnlineRecommendAssessor_calendarImageButton = new ImageButton();
                            ShowOnlineRecommendAssessor_calendarImageButton.SkinID = "sknAssessorCalendarImageButton";
                            ShowOnlineRecommendAssessor_calendarImageButton.ToolTip = "Click here to view assessor calendar";
                            ShowOnlineRecommendAssessor_calendarImageButton.CommandName = "ViewAssessorCalendar";
                            ShowOnlineRecommendAssessor_calendarImageButton.CommandArgument = assessorData[2].ToString();
                            e.Row.Cells[i].Controls.Add(ShowOnlineRecommendAssessor_calendarImageButton);
                        }
                        
                        Label lblBreak = new Label();
                        lblBreak.Text = "<br>";
                        e.Row.Cells[i].Controls.Add(lblBreak);

                        HyperLink hlkEmalMail = new HyperLink();
                        hlkEmalMail.Target = "_blank";
                        hlkEmalMail.NavigateUrl = "mailto:" + assessorData[1].ToString();
                        hlkEmalMail.ToolTip = "Click here to email the assessor";
                        hlkEmalMail.Text = assessorData[1];
                        e.Row.Cells[i].Controls.Add(hlkEmalMail);
                        e.Row.Cells[i].Width = Unit.Pixel(150);
                    }
                    e.Row.Cells[i].Height = Unit.Pixel(50);
                }
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                int k = 0;
                string columnName = string.Empty;
                for (k = 0; k <= e.Row.Cells.Count - 1; k++)
                {
                    if (k == 0)
                    {
                        Label lblSpace1 = new Label();
                        lblSpace1.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                        e.Row.Cells[k].Controls.Add(lblSpace1);

                        if ((((DataTable)ViewState["DATALIST"]).Rows.Count) - 1 == e.Row.RowIndex)
                        {
                            LinkButton lnkAddAdditionalSkill = new LinkButton();
                            lnkAddAdditionalSkill.SkinID = "";
                            lnkAddAdditionalSkill.Text = e.Row.Cells[k].Text;
                            lnkAddAdditionalSkill.CommandArgument = "";
                            lnkAddAdditionalSkill.CommandName = "ShowSkillPopup";
                            lnkAddAdditionalSkill.Enabled = true;
                            e.Row.Cells[k].Text = string.Empty;
                            e.Row.Cells[k].Controls.Add(lnkAddAdditionalSkill);
                            e.Row.Cells[k].Height = Unit.Pixel(25);
                        }
                        else
                        {
                            ImageButton imgDeleteButton = new ImageButton();
                            imgDeleteButton.SkinID = "sknDeletePositionProfile";
                            imgDeleteButton.ID = string.Concat("imgDelete", e.Row.RowIndex.ToString(), k.ToString());
                            imgDeleteButton.ToolTip = "Delete Skill";
                            imgDeleteButton.CommandArgument = e.Row.RowIndex.ToString();
                            imgDeleteButton.CommandName = "Delete";
                            e.Row.Cells[k].Controls.Add(imgDeleteButton);
                            e.Row.Cells[k].Height = Unit.Pixel(50);
                        }
                        Label lblSpace2 = new Label();
                        lblSpace2.Text = "&nbsp;&nbsp;";
                        e.Row.Cells[k].Controls.Add(lblSpace2);

                        Label lblSkill = new Label();
                        if (!Utility.IsNullOrEmpty(e.Row.Cells[k].Text))
                        {
                            string[] rowColCollection = null;
                            rowColCollection = e.Row.Cells[k].Text.ToString().Split(':');
                            lblSkill.Text = rowColCollection[0];    
                        }
                        e.Row.Cells[k].Controls.Add(lblSkill);
                        e.Row.Cells[k].HorizontalAlign = HorizontalAlign.Left;
                    }
                    else
                    {
                        
                        columnName = ShowOnlineRecommendAssessor_assesserDetailsGridView.HeaderRow.Cells[k].Text;
                        if ((((DataTable)ViewState["DATALIST"]).Rows.Count) - 1 == e.Row.RowIndex)
                        {
                            CheckBox chkBox = new CheckBox();
                            chkBox.ToolTip = "Select";
                            chkBox.ID = string.Concat("chkAssessor", e.Row.RowIndex.ToString(), k.ToString());
                            chkBox.Attributes.Add("key", string.Concat(e.Row.RowIndex.ToString(), ",", columnName));
                            chkBox.AutoPostBack = true;
                            chkBox.CheckedChanged += new EventHandler(chkBox_CheckedChanged);
                            if (e.Row.Cells[k].Text.ToString().Trim() == "Y")
                                chkBox.Checked = true;
                            else
                                chkBox.Checked = false;
                            e.Row.Cells[k].Controls.Add(chkBox);
                            e.Row.Cells[k].Height = Unit.Pixel(25);
                            e.Row.Cells[k].HorizontalAlign = HorizontalAlign.Center;
                        }
                        else
                        {
                            if (!Utility.IsNullOrEmpty(e.Row.Cells[k].Text))
                            {
                                string[] cellCollection = null;
                                string slotsCommandArgument = string.Empty;
                                cellCollection = e.Row.Cells[k].Text.ToString().Trim().Split(':');
                                
                                if (Request.QueryString["flag"].ToString().ToUpper() != "SEARCH")
                                {
                                    ImageButton imgCalendar = new ImageButton();
                                    bool slotFlag;
                                    int slotAssessorId=0;
                                    int slotSkillId=0;
                                    if(cellCollection[2].ToString().Trim()!=string.Empty)
                                        slotSkillId = Convert.ToInt32(cellCollection[2].ToString().Trim());

                                    if(cellCollection[3].ToString().Trim()!=string.Empty)
                                        slotAssessorId = Convert.ToInt32(cellCollection[3].ToString().Trim());

                                    slotFlag = new AssessorBLManager().IsAcceptedTimeSlotsAvailable(slotAssessorId, 
                                        interviewSessionKey,slotSkillId);

                                    imgCalendar.ToolTip = "Click here to view confirmed time slots";
                                    if (slotFlag)
                                        imgCalendar.ImageUrl = "~/App_Themes/DefaultTheme/Images/calendar_blue_bright.gif";
                                    else
                                        imgCalendar.ImageUrl = "~/App_Themes/DefaultTheme/Images/calendar_blue_dim.gif";
                                    
                                    imgCalendar.CommandName = "TimeSlotsAvailable";
                                    imgCalendar.CommandArgument = string.Concat(cellCollection[2].ToString().Trim(),
                                                                      ":", cellCollection[3].ToString().Trim());
                                    e.Row.Cells[k].Controls.Add(imgCalendar);
                                }
                                Label lblBreak2 = new Label();
                                lblBreak2.Text = "<br><br>";
                                e.Row.Cells[k].Controls.Add(lblBreak2);

                                ImageButton imgSelected = new ImageButton();
                                if (cellCollection[1] == "Y")
                                {
                                    imgSelected.ImageUrl = "~/App_Themes/DefaultTheme/Images/correct_answer.png";
                                    imgSelected.ToolTip = "Delete";
                                }
                                else
                                {
                                    imgSelected.ImageUrl = "~/App_Themes/DefaultTheme/Images/wrong_answer.png";
                                    imgSelected.ToolTip = "Add";
                                }

                                imgSelected.ID = string.Concat("imgSelected", e.Row.RowIndex.ToString(), k.ToString());
                                imgSelected.CommandName = "Change";

                                if (cellCollection[0] == "Y")
                                    e.Row.Cells[k].BackColor = System.Drawing.ColorTranslator.FromHtml("#F2FFE8");
                                else
                                    e.Row.Cells[k].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFEDE1");
                                
                                imgSelected.CommandArgument = string.Concat(e.Row.RowIndex.ToString(), ",", columnName);
                                e.Row.Cells[k].Controls.Add(imgSelected);
                                e.Row.Cells[k].Height = Unit.Pixel(50);
                                e.Row.Cells[k].HorizontalAlign = HorizontalAlign.Center;
                            }
                        }
                        
                    }
                }
            }
        }
        protected void ShowOnlineRecommendAssessor_assesserDetailsGridView_RowDeleting(object sender, 
            GridViewDeleteEventArgs e)
        {

        }

        /// <summary>
        /// Handles the Clicked event of the ShowOnlineRecommendAssessor_addSkillSaveButton_Clicked control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ShowOnlineRecommendAssessor_addSkillSaveButton_Clicked
                (object sender, EventArgs e)
        {
            try
            {
                if (ShowOnlineRecommendAssessor_addSkillTextBox.Text.Length == 0)
                {
                    ShowOnlineRecommendAssessor_addSkillErrorLabel.Text = null;
                    base.ShowMessage(ShowOnlineRecommendAssessor_addSkillErrorLabel, "Enter the skill");
                    ShowOnlineRecommendAssessor_addSkillModalPopupExtender.Show();
                    return;
                }
                #region Copy last row, and delete it, then assign the new skill row and append the copied row
                int columnCnt = Convert.ToInt32(ViewState["columnCount"].ToString());
                string[] addSkillRowValue = new string[columnCnt];

                DataTable dtAddSkill = ((DataTable)ViewState["DATALIST_FULL"]);
                DataRow drSkill = dtAddSkill.NewRow();
                DataRow drAddNewSkill = dtAddSkill.NewRow();
                drSkill.ItemArray = dtAddSkill.Rows[dtAddSkill.Rows.Count-1].ItemArray;
                dtAddSkill.Rows.RemoveAt(dtAddSkill.Rows.Count - 1);

                addSkillRowValue[0] = ShowOnlineRecommendAssessor_addSkillTextBox.Text.ToString().Trim().ToUpper();
                for (int i = 1; i <= columnCnt - 1; i++)
                    addSkillRowValue[i] = "N:N";
                drAddNewSkill.ItemArray = addSkillRowValue;
                
                dtAddSkill.Rows.Add(drAddNewSkill);
                dtAddSkill.Rows.Add(drSkill);
                dtAddSkill.AcceptChanges();
                #endregion

                ViewState["DATALIST_FULL"] = dtAddSkill;
                FilterDataTable(string.Empty);
                LoadAssessorGrid();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Event that handles the save process of selected assessors and its
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowOnlineRecommendAssessor_saveButton_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (ConstructSession())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(),"ShowAvailabilityForm", "<script language='javascript'>ShowAvailabilityRequest();</script>", false);
                }
                else
                {
                    base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                    "Please select at least one assessor with skill selected");
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                    exp.Message.ToString());
            }
        }

        /// <summary>
        /// Event that handles the candidate scheduling
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowOnlineRecommendAssessor_scheduleButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveScheduleCandidate())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ScheduleCandidate", "<script language='javascript'>OnSelectClick();</script>", false);
                }
                else
                {
                    base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                    "No time slots selected");
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                    exp.Message.ToString());
            }
        }

        /// <summary>
        /// Event that handles to add the newly selected time slots to the existing candidate scheduling
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowOnlineRecommendAssessor_saveSlotsButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["AVAILABILITY_REQUEST_LIST"] != null || 
                    Session["ONLINE_INTERVIEW_SCHEDULER_SELECTED_TIME_SLOTS"] != null)
                {
                    //Insert requested timeslots to TIME SLOTS TABLE
                    RequestedTimeSlots();
                    //Insert time slots against this candidated key in ONLINE_INTERVIEW_CANDIDATE_SESSION_TRACKING table
                    InsertCandidateTimeSlot();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SelectedAndRequestedTimeSlots", "<script language='javascript'>OnSelectClick();</script>", false);
                }
                else
                {
                    base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,"No time slots selected/requested");
                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                    exp.Message.ToString());
            }
        }

        /// <summary>
        /// Event to handle the check boxes status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkBox_CheckedChanged(object sender, EventArgs e)
        {
            string[] selectedAssessors = null;
            string checkedVal="N";
            CheckBox chkAssessors = (CheckBox)sender;

            if (chkAssessors.Checked)
                checkedVal = "Y";

            string key = (sender as CheckBox).Attributes["key"];
            selectedAssessors = key.Trim().Split(',');

            DataTable dtAssCheckedList = (DataTable)ViewState["DATALIST_FULL"];
            dtAssCheckedList.Rows[Convert.ToInt32(selectedAssessors[0])][selectedAssessors[1]] = checkedVal;
            dtAssCheckedList.AcceptChanges();
            ViewState["DATALIST_FULL"] = dtAssCheckedList;
        }
        /// <summary>
        /// Event launches a availability screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowOnlineRecommendAssessor_emailAllLinkButton_Click(object sender, EventArgs e)
        {
            if (ConstructSession())
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(),"ShowAvailabilityForm", "<script language='javascript'>ShowAvailabilityRequest();</script>", false);
            }
            else
            {
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                "Please select at least one assessor with skill selected");
                return;
            }
        }
        /// <summary>
        /// Event to close the popup buttion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowOnlineRecommendAssessor_addAssessorButton_Click(object sender, EventArgs e)
        {
            if (ConstructSession())
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Closeform", "<script language='javascript'>OnSelectClick();</script>", false);
            }
            else
            {
                base.ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel,
                "Please select at least one assessor with skill selected");
                return;
            }
        }
        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void ShowAssesserLookup_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                ShowOnlineRecommendAssessor_interviewedPastCheckBox.Checked=false;
                ShowOnlineRecommendAssessor_haveTimeSlotsCheckBox.Checked=false;
                ShowOnlineRecommendAssessor_matchesAllAreasCheckBox.Checked = false;
                ShowOnlineRecommendAssessor_sortOrderDropDownList.SelectedIndex=0;
                ShowOnlineRecommendAssessor_sortByDropDownList.SelectedIndex = 0;
                ShowOnlineRecommendAssessor_showAllRadioButton.Checked = true;
                ShowOnlineRecommendAssessor_showTopRadioButton.Checked = false;
                ShowOnlineRecommendAssessor_topMatchesTextBox.Text = "0";
                ShowOnlineRecommendAssessor_showPerPageTextBox.Text = _pageSize.ToString();
                //ShowOnlineRecommendAssessor_topSearchButton_Click(ShowOnlineRecommendAssessor_topSearchButton, new EventArgs());
                FormAssessorSkillMatrix();
                // Set default focus.
                Page.Form.DefaultFocus = ShowOnlineRecommendAssessor_topSearchButton.UniqueID;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ShowOnlineRecommendAssessor_topErrorMessageLabel, exp.Message);
            }
        }
}
}