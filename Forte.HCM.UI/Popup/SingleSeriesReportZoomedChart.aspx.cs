﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SingleSeriesReportZoomedChart.cs
// File that represents the user interface for ZoomedChart for the
// single series report page.
// This will helps to view the chart in the zoomed mode

#endregion

#region Directives                                                             
using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Represents the class that displays the chart in the zoomed manner
    /// </summary>
    public partial class SingleSeriesReportZoomedChart : PageBase
    {
        #region Event Handlers                                                 
        /// <summary>
        /// Represents the event handler that is called on page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Set browser title
            Master.SetPageCaption("Zoomed Chart");
            LoadChartDetails();
        }

        #endregion Event Handlers    

        #region Overridden Protected Methods                                   
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        } 
        #endregion

        #region Private Methods                                                
        /// <summary>
        /// Represents the method to set the chart height and width 
        /// </summary>
        /// <param name="chartDetails">
        /// A<see cref="SingleChartData"/>that holds the chart data
        /// </param>
        private void SetChartHeightAndWidth(SingleChartData chartDetails)
        {
            //Initialize the default width
            int defaultLength = 200;

            int chartWidth = 0;

            //Initialize the default height
            //int defaultHeightCount = 23;

            //int chartHeight = 0;

            int maxLength = 0;

            if (chartDetails.ChartData != null)
            {
                //Find the maximum length of the x axis variable
                foreach (ChartData data in chartDetails.ChartData)
                {
                    maxLength += data.ReportChartXValue.Trim().Length;
                }
            }           

            //if the length of string is greater than 33
            if (maxLength > defaultLength)
            {
                //Increase the chart Width by 15 px for each character
                chartWidth = (maxLength - defaultLength) * 15;

                int defaultChartWidth = int.Parse(SingleSeriesReportZoomedChart_chart.Width.Value.ToString());

                //set the chart width to new value
                SingleSeriesReportZoomedChart_chart.Width = Unit.Pixel(chartWidth + defaultChartWidth);
            }           

        }

        /// <summary>
        /// Represents the method to load the chart details
        /// </summary>
        private void LoadChartDetails()
        {
            //String that holds the session name
            string sessionId = Request.QueryString["sessionName"];

            if (Session[sessionId] == null)
                return;

            //Defines the chart data
            List<ChartData> chartdatas = new List<ChartData>();

            //Get the chart data source from session
            SingleChartData chartDetails = Session[sessionId] as SingleChartData;

            //Set the chart type of the control
            SingleSeriesReportZoomedChart_chart.
                Series["SingleSeriesReportZoomedChart_chartPrimarySeries"].ChartType = chartDetails.ChartType;

            //Assign the datasource of the chart
            SingleSeriesReportZoomedChart_chart.DataSource = chartDetails.ChartData;

            //Defines whether the chart title has to display or not
            SingleSeriesReportZoomedChart_chart.Titles[0].Visible =
                chartDetails.IsDisplayChartTitle;

            SingleSeriesReportZoomedChart_headerLiteral.Text = chartDetails.ChartTitle;

            //Assign the chart title to the chart
            if (chartDetails.IsDisplayChartTitle)
            {
                SingleSeriesReportZoomedChart_chart.
                    Titles["SingleSeriesReportZoomedChart_chartTitle"].Text = chartDetails.ChartTitle;
            }

            //Defines whether the axis title has to display or not
            if (chartDetails.IsDisplayAxisTitle)
            {
                SingleSeriesReportZoomedChart_chart.ChartAreas
                    ["SingleSeriesReportZoomedChart_chartArea"].AxisX.Title = chartDetails.XAxisTitle;

                SingleSeriesReportZoomedChart_chart.ChartAreas
                    ["SingleSeriesReportZoomedChart_chartArea"].AxisY.Title = chartDetails.YAxisTitle;
            }

            //Assign the datasource for the chart control
            SingleSeriesReportZoomedChart_chart.Series
                ["SingleSeriesReportZoomedChart_chartPrimarySeries"].Points.DataBindXY
                (chartDetails.ChartData, "ReportChartXValue", chartDetails.ChartData, "ReportChartYValue");

            //Defines whether has the legend has to display or not         
            if (!chartDetails.IsDisplayLegend)
            {
                //If the legend is not displayed then changed the chart area width to 100
                SingleSeriesReportZoomedChart_chart.ChartAreas
                    ["SingleSeriesReportZoomedChart_chartArea"].Position.Width = 100;

                //change the x axis label  angle to - 90 for vertical alignment of label
                SingleSeriesReportZoomedChart_chart.ChartAreas
                   ["SingleSeriesReportZoomedChart_chartArea"].AxisX.LabelStyle.Angle = 0;
            }

            //Assign the tool tip for the chart series point
            SingleSeriesReportZoomedChart_chart.Series
                ["SingleSeriesReportZoomedChart_chartPrimarySeries"].ToolTip = "#VAL";

            //Defines whether the value has to be shown as label in chart
            SingleSeriesReportZoomedChart_chart.Series
                ["SingleSeriesReportZoomedChart_chartPrimarySeries"].IsValueShownAsLabel = chartDetails.IsShowLabel;

            //Assign the value in the point in the series
            if (chartDetails.IsShowLabel)
            {
                SingleSeriesReportZoomedChart_chart.
                    Series["SingleSeriesReportZoomedChart_chartPrimarySeries"]["PieLabelStyle"] = "Inside";
            }
            if ((chartDetails.ChartType == SeriesChartType.Pie) && (!chartDetails.IsShowLabel))
            {
                //Disabled the values in the chart 
                SingleSeriesReportZoomedChart_chart.
                   Series["SingleSeriesReportZoomedChart_chartPrimarySeries"]["PieLabelStyle"] = "Disabled";
            }
            if (chartDetails.IsChangeSeriesColor)
            {
                SingleSeriesReportZoomedChart_chart.Palette = chartDetails.PaletteName;
            }
            if (chartDetails.IsChangePointsColor)
            {
                SingleSeriesReportZoomedChart_chart.Series[0].Points
                    [chartDetails.PointNumber].Color = chartDetails.PointColor;
            }
            if (chartDetails.IsComparisonReport)
            {
                int length = SingleSeriesReportZoomedChart_chart.Series[0].Points.Count;

                SingleSeriesReportZoomedChart_chart.Series[0].Points[length - 1].Color = chartDetails.PointColor;

                SingleSeriesReportZoomedChart_chart.Series[0].Points[length - 2].Color = chartDetails.PointColor;
                SingleSeriesReportZoomedChart_chart.Series[0].Points[length - 3].Color = chartDetails.PointColor;
            }

            SetChartHeightAndWidth(chartDetails);

        }

        #endregion Private Methods
    }
}
