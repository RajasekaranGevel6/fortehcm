﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SubjectSummary.cs
// File that represents the user interface for showing subjects
// which are contributed in the question for particular author.
// This will interact with the QuestionBLManager to get subject 
// list from DB.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface for showing
    /// list of subjects which are contributed in a question
    /// based on the author. This class is inherited from PageBase.
    /// </summary>
    public partial class SubjectSummary : PageBase
    {
        #region Event Handler                                                  

        /// <summary>
        /// Event handler will fire when a page gets loaded.
        /// And it will populate the list of subjects in the 
        /// grid against the author.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Subjects Contributed");

                // Subscribes to the page number click event.
                SubjectSummary_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                    (SubjectSummary_pageNavigator_PageNumberClick);

                if (!IsPostBack)
                {
                    // Check if the querystring contains author id
                    if (!Utility.IsNullOrEmpty(Request.QueryString["authorid"]))
                        LoadSubjects(1);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SubjectSummary_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        void SubjectSummary_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadSubjects(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SubjectSummary_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// triggered in the test inclusion detail gridview.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SubjectSummary_subjectsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
            }
        }

        #endregion Event Handler                                               

        #region Private Methods                                                

        /// <summary>
        /// Method will fetch all the subjects which are contributed in a
        /// question based on the question author.
        /// </summary>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadSubjects(int pageNumber)
        {
            int totalRecords = 0;

            List<Subject> subjects = null;

             if (Request.QueryString["type"] != null)
                if (Request.QueryString["type"].ToString() == "Test")
          subjects =
                new QuestionBLManager().GetSubjectsContributed
                (Convert.ToInt32(Request.QueryString["authorid"].ToString().Trim()),
                pageNumber, base.GridPageSize, out totalRecords);
                else
          subjects =
               new QuestionBLManager().GetInterviewSubjectsContributed
               (Convert.ToInt32(Request.QueryString["authorid"].ToString().Trim()),
               pageNumber, base.GridPageSize, out totalRecords);


            if (subjects != null)
            {
                SubjectSummary_subjectsGridView.DataSource = subjects;
                SubjectSummary_subjectsGridView.DataBind();
                SubjectSummary_pageNavigator.PageSize = base.GridPageSize;
                SubjectSummary_pageNavigator.TotalRecords = totalRecords;
            }
        }

        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods                                
    }
}
