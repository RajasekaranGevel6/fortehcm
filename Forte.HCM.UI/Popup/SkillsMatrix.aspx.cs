﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SkillsMatrix.cs
// File that represents the user interface for HCM Skill matrix.
// This will display the candidates skills based on candidate id.

#endregion Header

#region Directives

using System;

using Forte.HCM.Trace;

#endregion

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface for HCM Skill matrix.
    /// This will display the candidates skills based on candidate id.
    /// </summary>
    public partial class SkillsMatrix : Forte.HCM.UI.Common.PageBase
    {
        #region Event Handlers                                                 
        
        /// <summary>
        /// Event handler that helps to set default button, focus and page title.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Skill Matrix");
                if (!IsPostBack)
                {
                    // Set page title
                    SkillsMatrix_headerLiteral.Text =
                       "Resume & "+ Resources.HCMResource.SkillMatrix_Title;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SkillsMatrix_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            
        }

        #endregion Protected Overridden Methods                                                               
    }
}
