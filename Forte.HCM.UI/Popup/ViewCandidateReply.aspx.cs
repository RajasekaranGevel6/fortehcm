﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.

// QuestionSettings.cs
// File that represents the user interface for tracking user activities
// by capturing screen, desktop images, opened applications, exception and 
// so forth.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Linq;
using System.Web.UI;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    public partial class ViewCandidateReply : PageBase
    {
        //#region Private Variables                                              

        //private int pageNo;
        // private string imageType;
        //private string log;
        //private string viewType;

        //string canSessionKey = string.Empty;
        //int attempid= 0;
        //int logID = 0;

        //#endregion Private Variables                                           

        #region Event Handlers                                                 

        /// <summary>
        /// Event will get fired when the page is loaded and 
        /// performs to show images, desktop captured images,
        /// existing applications.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds a sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event object.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        protected void ViewCandidateReply_prevImageButton_Click(object sender, ImageClickEventArgs e)
        {
         
        }

        protected void ViewCandidateReply_nextImageButton_Click(object sender, ImageClickEventArgs e)
        {
            
        }

        #endregion Event Handlers                                              

        //#region Private Methods                                                

        ///// <summary>
        ///// Method that retrieves the list of image IDs from the list of 
        ///// objects.
        ///// </summary>
        ///// <param name="cyberProctorImages">
        ///// A list of <see cref="CyberProctorImage"/> that holds the cyber
        ///// proctor images.
        ///// </param>
        ///// <returns>
        ///// A list of <see cref="int"/> that holds the image ID.
        ///// </returns>
        //private List<string> GetImagesIDs(List<CyberProctorImage> cyberProctorImages)
        //{
        //    if (cyberProctorImages == null || cyberProctorImages.Count == 0)
        //        return null;

        //    var idList = from n in cyberProctorImages select n.ImageId;
        //    return idList.ToList();
        //}

        ///// <summary>
        ///// Method that retrieves the list of image IDs from the list of 
        ///// objects.
        ///// </summary>
        ///// <param name="trackingImages">
        ///// A list of <see cref="OfflineInterviewTrackingImage"/> that holds
        ///// offline interview desktop capture images.
        ///// </param>
        ///// <returns>
        ///// A list of <see cref="int"/> that holds the image ID.
        ///// </returns>
        //private List<string> GetImagesIDs(List<OfflineInterviewTrackingImage> trackingImages)
        //{
        //    if (trackingImages == null || trackingImages.Count == 0)
        //        return null;

        //    var idList = from n in trackingImages select n.ImageID;
        //    return idList.ToList();
        //}

        //private void GetImageIdList(string imgtype)
        //{
        //    int totalRecords = 0;
        //    if (log == null)
        //    {
        //        if (viewType == "CP")
        //        {
        //            List<CyberProctorImage> cyberProctorImages = new ReportBLManager().
        //                GetCyberProctorImage(canSessionKey, attempid, imgtype, 0, 0, out totalRecords);
        //            if (cyberProctorImages != null)
        //            {
        //                Session["VIEW_SCREEN_SHOT_IMAGE_IDS"] = GetImagesIDs(cyberProctorImages);
        //                Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"] = cyberProctorImages.Count;
        //            }
        //        }
        //        else if (viewType == "OI")
        //        {
        //            List<OfflineInterviewTrackingImage> trackingImages = new InterviewReportBLManager().
        //                GetOfflineInterviewTrackingImageDetails(canSessionKey, attempid, imgtype, 0, 0, out totalRecords);

        //            if (trackingImages != null)
        //            {
        //                Session["VIEW_SCREEN_SHOT_IMAGE_IDS"] = GetImagesIDs(trackingImages);
        //                Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"] = trackingImages.Count;
        //            }
        //        }
        //    }

        //    if (log == "LOG")
        //    {
        //        List<CyberProctorImage> cyberProctorImageDetails = new ReportBLManager().
        //         GetCyberProctorImageDetail(logID, imgtype, 0, 0, out totalRecords);
        //        if (cyberProctorImageDetails != null)
        //        {
        //            Session["VIEW_SCREEN_SHOT_IMAGE_IDS"] = cyberProctorImageDetails;
        //            Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"] = cyberProctorImageDetails.Count;
        //        }
        //    }
        //}

        //private void ShowNavigationButtons()
        //{
        //    if (Session["FullImagepageNo"] != null)
        //        pageNo = (int)Session["FullImagepageNo"];

        //    int totalRecords = 0;
        //    if (Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"] != null)
        //        totalRecords = (int)Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"];

        //    if (pageNo >= totalRecords - 1)
        //        ViewCandidateReply_nextImageButton.Visible = false;

        //    if (pageNo <= 0)
        //        ViewCandidateReply_prevImageButton.Visible = false;

        //    ShowNavigationButtons(totalRecords, pageNo);
        //}

        //private void ShowNavigationButtons(int totalRecords, int imageIndex)
        //{
        //    // Set paging label.
        //    ViewCandidateReply_imagePaging.Text = string.Format("{0} of {1}",
        //        imageIndex + 1, totalRecords);

        //    if (imageIndex < totalRecords - 1)
        //        ViewCandidateReply_nextImageButton.Visible = true;
        //    else
        //        ViewCandidateReply_nextImageButton.Visible = false;

        //    if (imageIndex <= 0)
        //        ViewCandidateReply_prevImageButton.Visible = false;
        //    else
        //        ViewCandidateReply_prevImageButton.Visible = true;
        //}

        //private void MoveNext()
        //{
        //    if (Session["FullImagepageNo"] != null)
        //        pageNo = (int)Session["FullImagepageNo"];

        //    int TotalRec = 0;
        //    if (Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"] != null)
        //        TotalRec = (int)Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"];

        //    if (pageNo < TotalRec - 1)
        //        pageNo++;

        //    ShowNavigationButtons(TotalRec, pageNo);

        //    Session["FullImagepageNo"] = pageNo;
        //    ShowImage();
        //}

        //private void MovePrev()
        //{
        //    int TotalRec = 0;
        //    if (Session["FullImagepageNo"] != null)
        //        pageNo = (int)Session["FullImagepageNo"];
        //    if (Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"] != null)
        //        TotalRec = (int)Session["VIEW_SCREEN_SHOT_TOTAL_RECORDS"];

        //    if (pageNo > 0)
        //        pageNo--;

        //    ShowNavigationButtons(TotalRec, pageNo);
        //    Session["FullImagepageNo"] = pageNo;
        //    ShowImage();
        //}

        //private void ShowImage()
        //{
        //    if (Session["FullImagepageNo"] != null)
        //        pageNo = (int)Session["FullImagepageNo"];

        //    if (Session["VIEW_SCREEN_SHOT_IMAGE_IDS"] == null)
        //        return;

        //    // Get IDs list.
        //    List<string> imageIDs = Session["VIEW_SCREEN_SHOT_IMAGE_IDS"] as List<string>;

        //    if (imageIDs == null || imageIDs.Count == 0)
        //        return;

        //    if (log == null)
        //    {
        //        if (viewType == "CP")
        //        {
        //            ViewCandidateReply_viewImage.ImageUrl = "~/Common/ImageHandler.ashx?source=CP&ImageID="
        //              + imageIDs[pageNo] + "&isThumb=0";
        //        }
        //        else if (viewType == "OI")
        //        {
        //            ViewCandidateReply_viewImage.ImageUrl = "~/Common/ImageHandler.ashx?source=OI&ImageID="
        //              + imageIDs[pageNo] + "&isThumb=0";
        //        }
        //    }
        //    else if (log == "LOG")
        //    {
        //        if (viewType == "CP")
        //        {
        //            ViewCandidateReply_viewImage.ImageUrl = "~/Common/ImageHandler.ashx?source=CPLOG&ImageID="
        //              + imageIDs[pageNo] + "&imageType=" + imageType + "&logID=" + logID;
        //        }
        //        else if (viewType == "OI")
        //        {
        //             ViewCandidateReply_viewImage.ImageUrl = "~/Common/ImageHandler.ashx?source=OILOG&ImageID="
        //              + imageIDs[pageNo] + "&imageType=" + imageType + "&logID=" + logID;
        //        }
        //    }
        //}

        //#endregion Private Methods                                             

        #region Overridden Methods                                             

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Overridden Methods                                          
    }
}