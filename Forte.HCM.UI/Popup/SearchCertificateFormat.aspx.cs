﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchCertificateFormat.cs
// File that represents the user interface to show the certificate details.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.IO;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities for 
    /// show the certificate formats.
    /// </summary>
    /// <remarks>
    /// This class is inherited from the <see cref="PageBase"/> class.
    /// </remarks>
    public partial class SearchCertificateFormat : PageBase
    {
        #region Event Handler                                                  

        /// <summary>
        /// Handler that will call when the page gets loaded. This will load
        /// certification details.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Search Certificate");
                //if (IsPostBack)
                //    return;
                if (!IsPostBack)
                {
                    SearchCertificateFormat_certificateImage.Visible = false;
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCertificate_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void SearchCertificate_bottomResetButton_Click(object sender, EventArgs e)
        {
            try
            {
                ShowCertificateFormat_previewDIV.Style.Add("display", "none");
                SearchCertificateFormat_certificatesGridView.DataSource = new
                    AdminBLManager().GetCertificateDetails();
                SearchCertificateFormat_certificatesGridView.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCertificate_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the radio button is clicked from the gridview.
        /// During that time, it will show the corresponding certificate format in the
        /// provided image control.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CertificateFormatRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                string CurentId = ((RadioButton)sender).ClientID;
                for (int i = 0; i < SearchCertificateFormat_certificatesGridView.Rows.Count; i++)
                {
                    if (((RadioButton)SearchCertificateFormat_certificatesGridView.Rows[i].
                        FindControl("ShowCertificateFormat_certificatePreviewRadioButton")).ClientID == CurentId)
                        continue;
                    if (!(((RadioButton)SearchCertificateFormat_certificatesGridView.Rows[i].
                        FindControl("ShowCertificateFormat_certificatePreviewRadioButton")).Checked))
                        continue;
                    ((RadioButton)SearchCertificateFormat_certificatesGridView.Rows[i]
                        .FindControl("ShowCertificateFormat_certificatePreviewRadioButton")).Checked = false;
                }

                RadioButton radioButton = (RadioButton)sender;

                if (radioButton.Checked)
                {
                    ShowCertificateFormat_previewDIV.Style.Add("display", "block");
                    SearchCertificateFormat_certificateImage.Visible = true;
                    SearchCertificateFormat_certificateImage.ImageUrl = @"~/Common/ImageHandler.ashx?ImageID="
                        + Convert.ToInt32(radioButton.GroupName.ToString()) + "&isThumb=0&source=CERTIFICATE_TEMPLATE";
                    
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCertificate_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handler                                               

        #region Protected Override Methods                                     

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            SearchCertificateFormat_certificatesGridView.DataSource =
                new AdminBLManager().GetCertificateDetails();
            SearchCertificateFormat_certificatesGridView.DataBind();
        }

        #endregion Protected Override Methods                                  
    }
}