﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchTimeSlots.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.SearchTimeSlots"
    Title="Select Time Slots" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="SearchTimeSlots_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl)
        {
            var ctrlID = '<%= Request.QueryString["ctrlid"] %>';
            
            if (window.dialogArguments)
            {
                window.opener = window.dialogArguments;
            }
            if (ctrlID != null && ctrlID != '')
            {
                // Get time slot ID.
                var timeSlotID = document.getElementById(ctrl.id.replace
                    ("SearchTimeSlots_timeSlotsGridView_selectLinkButton", "SearchTimeSlots_timeSlotsGridView_timeSlotIDHiddenField")).value;

                window.opener.document.getElementById(ctrlID).value = timeSlotID;
            }

            self.close();
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="td_padding_top_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="SearchTimeSlots_titleLiteral" runat="server" Text="Select Time Slots"></asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="SearchTimeSlots_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_search_time_slots_bg">
                    <tr>
                        <td style="width: 100%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="height: 18px">
                                        <asp:UpdatePanel ID="SearchTimeSlots_messageUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="SearchTimeSlots_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknErrorMessage" Width="100%" Text=""></asp:Label>
                                                <asp:Label ID="SearchTimeSlots_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknSuccessMessage" Width="100%" Text=""></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 100%">
                                        <asp:UpdatePanel ID="SearchTimeSlots_inputControlsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 78%" valign="top" class="search_time_slots_criteria">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <asp:RadioButton ID="SearchTimeSlots_showAcceptedTimeSlotsRadioButton" Text="Show Accepted Time Slots"
                                                                            runat="server" SkinID="sknCriteriaRadioButton" GroupName="types" Checked="true"
                                                                            AutoPostBack="true" OnCheckedChanged="SearchTimeSlots_timeSlotsStatusRadioButton_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <asp:RadioButton ID="SearchTimeSlots_showRejectedTimeSlotsRadioButton" Text="Show Rejected Time Slots"
                                                                            runat="server" SkinID="sknCriteriaRadioButton" GroupName="types" AutoPostBack="true"
                                                                            OnCheckedChanged="SearchTimeSlots_timeSlotsStatusRadioButton_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <asp:RadioButton ID="SearchTimeSlots_showPendingTimeSlotsRadioButton" Text="Show Pending Time Slots"
                                                                            runat="server" SkinID="sknCriteriaRadioButton" GroupName="types" AutoPostBack="true"
                                                                            OnCheckedChanged="SearchTimeSlots_timeSlotsStatusRadioButton_CheckedChanged" />
                                                                    </td>
                                                                    <td align="right">
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:ImageButton ID="SearchTimeSlots_emailImageButton" runat="server" ToolTip="Click here to email the assessor"
                                                                                        SkinID="sknMailImageButton"/>
                                                                                </td>
                                                                                <td align="right" style="padding-left: 6px">
                                                                                    <asp:ImageButton ID="SearchTimeSlots_requestTimeSlotImageButton" runat="server" ToolTip="Click here to request for time slots"
                                                                                        SkinID="sknRequestTimeSlotImageButton"/>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%" valign="top" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="center" class="search_time_slots_assessor_info_bg" valign="middle">
                                                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td style="width: 14%" align="left" class="td_height_20" valign="middle">
                                                                                    <asp:Label ID="SearchTimeSlots_assessorNameLabel" runat="server" Text="Assessor"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 50%" align="left" valign="middle">
                                                                                    <asp:Label ID="SearchTimeSlots_assessorNameValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                        Text=""></asp:Label>
                                                                                </td>
                                                                                <td style="width: 10%" align="left" class="td_height_20" valign="middle">
                                                                                    <asp:Label ID="SearchTimeSlots_skillLabel" runat="server" Text="Skill" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%" align="left" valign="middle">
                                                                                    <asp:Label ID="SearchTimeSlots_skillValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                        Text=""></asp:Label>
                                                                                    <asp:HiddenField ID="SearchTimeSlots_assessorIDHiddenField" runat="server" />
                                                                                    <asp:HiddenField ID="SearchTimeSlots_skillIDHiddenField" runat="server" />
                                                                                    <asp:HiddenField ID="SearchTimeSlots_sessionKeyHiddenField" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 4px"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="SearchTimeSlots_selectedTimeSlotsLiteral" runat="server" Text="Time Slots"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" class="search_time_slots_grid_body_bg">
                                                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td style="width: 100%">
                                                                                    <div style="height: 200px; overflow: auto;">
                                                                                        <asp:GridView ID="SearchTimeSlots_timeSlotsGridView" runat="server" AllowSorting="False"
                                                                                            AutoGenerateColumns="False" Width="100%" 
                                                                                            OnRowDataBound="SearchTimeSlots_timeSlotsGridView_RowDataBound"
                                                                                            Height="100%" onrowcommand="SearchTimeSlots_timeSlotsGridView_RowCommand">
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="SearchTimeSlots_timeSlotsGridView_requestStatusHiddenField"
                                                                                                            runat="server" Value='<%# Eval("RequestStatus") %>' />
                                                                                                        <asp:HiddenField ID="SearchTimeSlots_timeSlotsGridView_timeSlotIDHiddenField" runat="server"
                                                                                                            Value='<%# Eval("ID") %>' />
                                                                                                        <asp:HiddenField ID="SearchTimeSlots_timeSlotsGridView_isAvailabledHiddenField"
                                                                                                            runat="server" Value='<%# Eval("IsAvailable") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="1%" HeaderStyle-HorizontalAlign="Left"
                                                                                                    ItemStyle-HorizontalAlign="Center">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:LinkButton ID="SearchTimeSlots_timeSlotsGridView_selectLinkButton" runat="server"
                                                                                                            Text="Select" 
                                                                                                            CommandName="SelectSlot" CommandArgument='<%# Eval("ID") %>'>
                                                                                                        </asp:LinkButton>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="40%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="SearchTimeSlots_timeSlotsGridView_requestedDateLabel" runat="server"
                                                                                                            Text='<%# Eval("AvailabilityDateString") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Time Slot" HeaderStyle-Width="35%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="SearchTimeSlots_timeSlotsGridView_timeSlotLabel" runat="server" Text='<%# Eval("TimeSlot") %>'
                                                                                                            Width="90%"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Status" HeaderStyle-Width="34%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="SearchTimeSlots_timeSlotsGridView_requestStatusNameLabel" runat="server"
                                                                                                            Text='<%# Eval("RequestStatusName") %>' Width="90%"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                            <EmptyDataTemplate>
                                                                                                <table style="width: 100%; height: 100%">
                                                                                                    <tr>
                                                                                                        <td style="height: 90px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="middle" align="center" style="height: 100%" class="error_message_text_normal">
                                                                                                            No time slots found to display
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </EmptyDataTemplate>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 10px">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="SearchTimeSlots_buttonControlsUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="td_height_5" colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="left">
                                    &nbsp;<asp:LinkButton ID="SearchTimeSlots_cancelLinkButton" runat="server" Text="Cancel"
                                        SkinID="sknPopupLinkButton" OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window"> </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_2">
            </td>
        </tr>
    </table>
</asp:Content>
