﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="PopupAccessDenied.aspx.cs" Inherits="Forte.HCM.UI.Popup.PopupAccessDenied" %>

<asp:Content ID="PopupAccessDenied_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" border="0" cellspacing="3" cellpadding="0" >
                    <tr>
                        <td style="width: 40%" align="left" valign="top">
                            <table style="width: 100%">
                                <tr>
                                    <td class="access_denied_title">
                                        Error
                                    </td>
                                </tr>
                                <tr>
                                    <td class="access_denied_message">
                                        Access denied
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="access_denied_title">
                                        Message
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="access_denied_message">
                                        You do not have sufficient rights to access to the specific page
                                    </td>
                                </tr>
                                <tr>
                                    <td class="access_denied_message">
                                        Please contact your administrator
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 60%" class="access_denied">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
