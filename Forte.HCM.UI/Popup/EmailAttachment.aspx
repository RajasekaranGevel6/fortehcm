﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailAttachment.aspx.cs"
    Inherits="Forte.HCM.UI.Popup.EmailAttachment" MasterPageFile="~/MasterPages/PopupMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="EmailAttachment_content" ContentPlaceHolderID="PopupMaster_contentPlaceHolder"
    runat="server">
    <script type="text/javascript">

        // This function helps to Open save/open dialog
        function OpenPopUp(candidateSessionID, attemptID)
        {
            window.open('../Common/Download.aspx?candidatesessionid='
            + candidateSessionID + '&attemptid=' + attemptID
            + '&type=<%=EmailAttachment_downloadTypeHiddenField.Value %>', '', 'toolbar=0,resizable=0,width=1,height=1', '');
        }

    </script>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <asp:HiddenField ID="EmailAttachment_creditsEarnedHiddenField" runat="server" />
                <asp:HiddenField ID="EmailAttachment_downloadTypeHiddenField" runat="server" />
                <asp:UpdatePanel ID="EmailAttachment_updatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="popup_td_padding_10">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="width: 50%" class="popup_header_text_grey" align="left">
                                                <asp:Literal ID="EmailAttachment_headerLiteral" runat="server" Text="Email Attachment"></asp:Literal>
                                            </td>
                                            <td style="width: 50%" align="right">
                                                <asp:ImageButton ID="EmailAttachment_cancelImageButton" TabIndex="7" runat="server"
                                                    SkinID="sknCloseImageButton" OnClientClick="javascript:return CloseMe();" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_td_padding_10" valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_question_inner_bg">
                                        <tr>
                                            <td class="msg_align">
                                                <asp:Label ID="EmailAttachment_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="EmailAttachment_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                <asp:HiddenField ID="EmailAttachment_fromHiddenField" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td style="width: 15%">
                                                                        <asp:Label ID="EmailAttachment_fromLabel" runat="server" Text="From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 55%">
                                                                        <asp:Label ID="EmailAttachment_fromValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%" align="right">
                                                                        <asp:CheckBox ID="EmailAttachement_sendMeACopyCheckBox" runat="server" Checked="true"
                                                                            TextAlign="Right" Text="Send me a copy" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 15%">
                                                            <asp:Label ID="EmailAttachment_toLabel" runat="server" Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="width: 97%">
                                                                        <asp:TextBox ID="EmailAttachment_toTextBox" TabIndex="1" runat="server" MaxLength="500"
                                                                            onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)" SkinID="sknMultiLineTextBox"
                                                                            TextMode="MultiLine" Width="99%">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:ImageButton ID="EmailAttachment_toAddressImageButton" runat="server" SkinID="sknMailToImageButton"
                                                                            ToolTip="Click here to select contacts" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="EmailAttachment_ccLabel" runat="server" Text="CC" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="width: 97%">
                                                                        <asp:TextBox ID="EmailAttachment_ccTextBox" runat="server" Width="99%" TextMode="MultiLine"
                                                                            SkinID="sknMultiLineTextBox" Height="23" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                                            onchange="CommentsCount(500,this)" TabIndex="2"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:ImageButton ID="EmailAttachment_ccAddressImageButton" runat="server" SkinID="sknMailToImageButton"
                                                                            ToolTip="Click here to select contacts" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="EmailAttachment_subjectLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="EmailAttachment_subjectTextBox" TabIndex="3" runat="server" MaxLength="100"
                                                                onkeyup="CommentsCount(100,this)" onchange="CommentsCount(100,this)" SkinID="sknMultiLineTextBox"
                                                                Height="23" Width="99%" TextMode="MultiLine">
                                                            </asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <tr id="EmailAttachment_attachmentTR" runat="server" style="display: none;">
                                                        <td>
                                                            <asp:Label ID="EmailAttachment_attachmentLabel" runat="server" Text="Attachment"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td align="left">
                                                            <%--    <a href="~/Common/Download.aspx?type=PDF" runat="server" id="EmailAttachment_attachmentHrefA">
                                                                <asp:Image ID="EmailAttachment_attachmentImage" runat="server" ToolTip="Attachment"
                                                                    AlternateText="Attachment" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_pdf.gif" /></a>--%>
                                                            <%--<a href="Javascript:OpenPopUp()" runat="server" id="EmailAttachment_attachmentHrefA">
                                                                <asp:Image ID="EmailAttachment_attachmentImage" runat="server" ToolTip="Attachment"
                                                                    AlternateText="Attachment" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_pdf.gif" /></a>
                                                            <iframe name="EmailAttachment_attDownloadFrame" id="EmailAttachment_attDownloadFrame"
                                                                width="0" height="0" style="border: 0px"></iframe>--%>
                                                            <asp:ImageButton ID="EmailAttachment_attachmentImageButton" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_pdf.gif"
                                                                AlternateText="Attachment" ToolTip="Attachment" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td valign="top" align="left">
                                                                        <asp:Label ID="EmailAttachment_messageLabel" runat="server" Text="Message" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class="mandatory">*</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="EmailAttachment_messageTextBox" TabIndex="4" TextMode="MultiLine"
                                                                            Height="200" runat="server" Width="99%" SkinID="sknMultiLineTextBox" MaxLength="500"
                                                                            onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)" Style="border-bottom: 0px;">
                                                                        </asp:TextBox>
                                                                        <iframe id="EmailAttachment_contentIframe" runat="server" width="99%" scrolling="auto"
                                                                            style="height: 140px; overflow: auto; display: none; color: #33424B; border: 1px solid #D8D8D8;
                                                                            background-color: #FFFFFF; padding-left: 3px; border-top: 0px;"></iframe>
                                                                        <div id="EmailAttachment_certificateDIV" runat="server" style="height: 180px; width: 716px;
                                                                            overflow: auto;">
                                                                            <img id="EmailAttachment_certificateImage" runat="server" alt="Certificate" />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <%--<tr id="EmailAttachment_contentTR" runat="server" style="display: none;">
                                                        <td colspan="2">
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>--%>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="popup_td_padding_left_8">
                                    <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                <asp:Button ID="EmailAttachment_submitButton" runat="server" Text="Submit" OnClick="EmailAttachment_submitButton_Click"
                                                    SkinID="sknButtonId" TabIndex="5" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="EmailAttachment_cancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknPopupLinkButton" TabIndex="6" OnClientClick="javascript:return CloseMe();"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
