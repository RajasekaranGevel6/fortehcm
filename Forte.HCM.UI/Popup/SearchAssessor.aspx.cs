﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchAssessor.aspx.cs
// File that represents the user interface layout and functionalities
// for the SearchAssessor page. This page helps in searching for corporate
// users as well as assessors associated with the tenant and also provides 
// the facility to convert an existing user to an assessor. 

#endregion

#region Directives

using System;
using System.Text;
using System.Configuration;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.ExternalService;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the SearchAssessor page. This page helps in searching for corporate
    /// users as well as assessors associated with the tenant and also provides 
    /// the facility to convert an existing user to an assessor. This class
    /// inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class SearchAssessor : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "274px";

   
        #endregion Private Constants

        #region Events Handlers                                                
       
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SearchAssessor_successMessageLabel.Text = "";
                SearchAssessor_errorMessageLabel.Text = "";

                // Set browser title.
                Master.SetPageCaption("Search Assessor");

                // Set default button and focus.
                Page.Form.DefaultButton = SearchAssessor_searchButton.UniqueID;

                // Subscribes to the page number click event.
                SearchAssessor_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchAssessor_pageNavigator_PageNumberClick);

                // 
                if (!Utility.IsNullOrEmpty(SearchAssessor_isMaximizedHiddenField.Value) &&
                    SearchAssessor_isMaximizedHiddenField.Value == "Y")
                {
                    SearchAssessor_searchCriteriasDiv.Style["display"] = "none";
                    SearchAssessor_searchResultsUpSpan.Style["display"] = "block";
                    SearchAssessor_searchResultsDownSpan.Style["display"] = "none";
                    SearchAssessor_testDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchAssessor_searchCriteriasDiv.Style["display"] = "block";
                    SearchAssessor_searchResultsUpSpan.Style["display"] = "none";
                    SearchAssessor_searchResultsDownSpan.Style["display"] = "block";
                    SearchAssessor_testDiv.Style["height"] = RESTORED_HEIGHT;
                }

                if (!IsPostBack)
                {
                    // Set default focus.
                    Page.Form.DefaultFocus = SearchAssessor_nameTextBox.UniqueID;
                    SearchAssessor_nameTextBox.Focus();

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "ASSESSOR_NAME";

                    SearchAssessor_testDiv.Visible = false;

                    SearchAssessor_assessorConfirmMsgControl.Message = "Are you sure want to mark the user as an assessor?";
                    SearchAssessor_assessorConfirmMsgControl.Type = MessageBoxType.YesNo;
                    SearchAssessor_assessorConfirmMsgControl.Title = "Mark As An Assessor";
                }
                else
                {
                    SearchAssessor_testDiv.Visible = true;
                }

                SearchAssessor_searchTestResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    SearchAssessor_testDiv.ClientID + "','" +
                    SearchAssessor_searchCriteriasDiv.ClientID + "','" +
                    SearchAssessor_searchResultsUpSpan.ClientID + "','" +
                    SearchAssessor_searchResultsDownSpan.ClientID + "','" +
                    SearchAssessor_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchAssessor_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchAssessor_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "ASSESSOR_NAME";

                // Reset the paging control.
                SearchAssessor_pageNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchAssessor_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void SearchAssessor_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchAssessor_nameTextBox.Text = string.Empty;
                SearchAssessor_emailTextBox.Text = string.Empty;

                SearchAssessor_resultsGridView.DataSource = null;
                SearchAssessor_resultsGridView.DataBind();

                SearchAssessor_pageNavigator.PageSize = base.GridPageSize;
                SearchAssessor_pageNavigator.TotalRecords = 0;
                SearchAssessor_testDiv.Visible = false;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "ASSESSOR_NAME";

                // Reset to the restored state.
                SearchAssessor_searchCriteriasDiv.Style["display"] = "block";
                SearchAssessor_searchResultsUpSpan.Style["display"] = "none";
                SearchAssessor_searchResultsDownSpan.Style["display"] = "block";
                SearchAssessor_testDiv.Style["height"] = RESTORED_HEIGHT;
                SearchAssessor_isMaximizedHiddenField.Value = "N";

                // Set default focus.
                Page.Form.DefaultFocus = SearchAssessor_nameTextBox.UniqueID;
                SearchAssessor_nameTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchAssessor_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchAssessor_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ViewState["PageNo"] = e.PageNumber.ToString();
                Search(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchAssessor_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchAssessor_resultsGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                SearchAssessor_pageNavigator.Reset();
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchAssessor_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchAssessor_resultsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                // Set row styles.
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                string userID = ((HiddenField)e.Row.FindControl
                    ("SearchAssessor_resultsGridView_userIDHiddenField")).Value;

                /*Commented by MKN 22-May-2012 -For creating assessor same in corporate user management*/
                // Add handler for create assessor icon.
                /*  ImageButton SearchAssessor_resultsGridView_createAssessorImageButton =
                     (ImageButton)e.Row.FindControl("SearchAssessor_resultsGridView_createAssessorImageButton");
                
              

                SearchAssessor_resultsGridView_createAssessorImageButton.Attributes.Add
                     ("onclick", "javascript:return ShowCreateAssessor('" + userID + "','" +
                     SearchAssessor_searchButton.ClientID + "');");*/
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchAssessor_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchAssessor_resultsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchAssessor_resultsGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchAssessor_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that search assessors and users for the given page number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void Search(int pageNumber)
        {
            int totalRecords = 0;

            List<AssessorDetail> assessors = new AssessorBLManager().GetAssessorsAndUsers(base.tenantID,
                SearchAssessor_nameTextBox.Text.Trim(), SearchAssessor_emailTextBox.Text.Trim(), 
                IsIncludeUsers(), ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]), pageNumber, base.GridPageSize, out totalRecords);

            if (assessors == null || assessors.Count == 0)
            {
                SearchAssessor_testDiv.Visible = false;
                ShowMessage(SearchAssessor_errorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                SearchAssessor_testDiv.Visible = true;
            }

            SearchAssessor_resultsGridView.DataSource = assessors;
            SearchAssessor_resultsGridView.DataBind();
            SearchAssessor_pageNavigator.PageSize = base.GridPageSize;
            SearchAssessor_pageNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that indicates whether to inculde users (non assessors) 
        /// during the search. This status is taken from the query string
        /// parameter.
        /// </summary>
        /// <returns></returns>
        private bool IsIncludeUsers()
        {
            if (SearchAssessor_showAssessorCheckBox.Checked) 
                 return false;
            else
                return true;
            /*if (!Utility.IsNullOrEmpty(Request.QueryString["includeusers"]) &&
                Request.QueryString["includeusers"].Trim().ToUpper() == "N")
            {
                return false;
            }

            return true;*/
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Protected Methods                                              

        /// <summary>
        /// Method that retrieves the assessor status. This helps in displaying
        /// select link and create assessor icon in the grid.
        /// </summary>
        /// <param name="assessorStatus">
        /// A <see cref="string"/> that holds the assessor status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the assessor status
        /// </returns>
        protected bool IsAssessor(string assessorStatus)
        {
            return assessorStatus.ToUpper() == "TRUE" ? true : false;
        }

        #endregion Protected Methods

        protected void SearchAssessor_resultsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try 
            {

                if (e.CommandName == "CreateAssessor")
                {
                    GridViewRow userGridViewRow = (GridViewRow)(((ImageButton)e.CommandSource).Parent.Parent);

                    if (userGridViewRow == null) return;

                    ViewState["USER_ID"] = e.CommandArgument.ToString();

                    Label SearchAssessor_resultsGridView_emailLabel =
                        (Label)userGridViewRow.FindControl("SearchAssessor_resultsGridView_emailLabel");

                    if (SearchAssessor_resultsGridView_emailLabel == null)
                        ViewState["USER_EMAIL"] = string.Empty;
                    else
                        ViewState["USER_EMAIL"] = SearchAssessor_resultsGridView_emailLabel.Text.ToString().Trim();

                    SearchAssessor_assessorModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchAssessor_errorMessageLabel, exp.Message);
            } 
        }

        protected void SearchAssessor_assessorConfirmMsgControl_OkClick(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["USER_ID"] == null) return;

                CreateNewAssessor(Convert.ToInt32(ViewState["USER_ID"]), ViewState["USER_EMAIL"].ToString());

                 if(ViewState["PageNo"]!=null)
                     Search(Convert.ToInt32(ViewState["PageNo"]));
                 else
                     Search(1);

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(SearchAssessor_errorMessageLabel, ex.Message);
            }
        }

        protected void SearchAssessor_assessorConfirmMsgControl_CancelClick
    (object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(SearchAssessor_errorMessageLabel, ex.Message);
            }
        }

        /// <summary>
        /// Creating a new assessor from existing listed users
        /// </summary>
        /// <param name="userId">
        ///     <see cref="System.String"/>         
        /// </param>
        /// <param name="userEmail">
        ///     <see cref="System.String"/>
        /// </param>
        private void CreateNewAssessor(int userId, string userEmail)
        {
            UserDetail userDetail = new UserDetail();

            userDetail = new CommonBLManager().GetUserDetail(userId);


            AssessorDetail assessorDetails = new AssessorDetail();

            assessorDetails.UserID = userId;
            assessorDetails.UserEmail = userEmail;
            assessorDetails.AlternateEmailID = string.Empty;
            assessorDetails.FirstName = string.Empty;
            assessorDetails.LastName = string.Empty;
            assessorDetails.Mobile = string.Empty;

            if (userDetail != null)
            {
                assessorDetails.UserEmail = userDetail.Email;
                assessorDetails.AlternateEmailID = userDetail.AltEmail;
                assessorDetails.FirstName = userDetail.FirstName;
                assessorDetails.LastName = userDetail.LastName;
                assessorDetails.Mobile = userDetail.Phone;
            }

            assessorDetails.Skill = string.Empty;
            assessorDetails.CreatedBy = base.userID;
            assessorDetails.NonAvailabilityDates = string.Empty;
            assessorDetails.Image = null;
            assessorDetails.HomePhone = string.Empty;
            assessorDetails.AdditionalInfo = string.Empty;
            assessorDetails.LinkedInProfile = string.Empty;

            //Save assessor details
            string retval = new AssessorBLManager().SaveAssessorDetails(assessorDetails);

            try
            {
                new EmailHandler().SendMail(userDetail.Email, ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"], "You have been marked as an assessor", AesssorMailBody(assessorDetails.FirstName, userId));
            }
            catch { }
        }

        private string AesssorMailBody(string assessorName, int assessorId)
        {
            StringBuilder sbBody = new StringBuilder();

            try
            {
                string statusMsg = string.Empty;
                statusMsg = "This is to inform you that you have been marked as an assessor. You are invited to update your profile and skill details";

                sbBody.Append("Dear ");
                sbBody.Append(assessorName);

                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Welcome to ForteHCM !");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(statusMsg);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Click here to proceed   ");
                string strUri = WebConfigurationManager.AppSettings["DEFAULT_URL"] + "Admin/EnrollAssessor.aspx?m=1&s=2&parentpage=MENU&assessorid=" + assessorId;
                sbBody.Append("<a href='" + strUri + "'");
                sbBody.Append(" >" + strUri + "</a>");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("============================== Disclaimer ==============================");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("=========================== End Of Disclaimer ============================");
                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }
 }
}

