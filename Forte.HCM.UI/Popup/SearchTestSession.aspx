<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchTestSession.aspx.cs"
    Inherits="Forte.HCM.UI.Popup.SearchTestSession" MasterPageFile="~/MasterPages/PopupMaster.Master" %>    
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ID="SearchTestSession_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">

    <script type="text/javascript">
        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl) {
            var ctrlId = '<%= Request.QueryString["ctrlId"] %>';

            if (window.dialogArguments) 
            {
                window.opener = window.dialogArguments; 
            }
            if (ctrlId != null && ctrlId != '') {
                // Replace the 'link button name' with the 
                // 'hidden field name' and set the value in opener control.
                window.opener.document.getElementById(ctrlId).value
                = document.getElementById(ctrl.id.replace("SearchTestSession_selectLinkButton", "SearchTestSession_selectIdHiddenfield")).value;

                //Trigger the click event of the show button.
                window.opener.document.getElementById(ctrlId.replace("ScheduleCandidate_testSessionIdTextBox", "ScheduleCandidate_showButton")).click();
            }
            self.close();
        }
    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchTestSession_headerLiteral" runat="server" Text="Search Test Session"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="QuestionDetailPreviewControl_topCancelImageButton" runat="server"
                                SkinID="sknCloseImageButton" OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                    <tr>
                        <td class="msg_align" colspan="2">
                            <asp:UpdatePanel ID="SearchTestSession_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="SearchTestSession_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="SearchTestSession_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <div id="SearchTestSession_searchCriteriasDiv" runat="server" style="display: block;">
                                            <table width="100%" border="0" cellspacing="3" cellpadding="2">
                                                <tr>
                                                    <td>
                                                        <table style="width: 100%" cellpadding="2" cellspacing="0">
                                                        <asp:UpdatePanel ID="SearchTestSession_searchCriteriaUpdatePanel" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <tr>
                                                                    <td style="width: 18%">
                                                                        <asp:Label ID="SearchTestSession_testSessionIdLabel" runat="server" Text="Test Session ID"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="SearchTestSession_testSessionIdTextBox" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 22%">
                                                                        <asp:Label ID="SearchTestSession_testIdLabel" runat="server" Text="Test ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="SearchTestSession_testIdTextBox" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="SearchTestSession_testNameLabel" runat="server" Text="Test Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="SearchTestSession_testNameTextBox" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="SearchTestSession_positionProfileLabel" runat="server" Text="Position Profile"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="SearchTestSession_positionProfileTextBox" ReadOnly="true" runat="server"></asp:TextBox>
                                                                        </div>
                                                                        &nbsp;
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="SearchTestSession_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                runat="server" ImageAlign="Middle" />
                                                                        </div>
                                                                        <asp:HiddenField ID="SearchTestSession_positionProfileIDHiddenField" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="SearchTestSession_sessionAuthorNameHeadLabel" runat="server" Text="Test Session Author"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="SearchTestSession_sessionAuthorNameTextBox" runat="server" ReadOnly="true"
                                                                                Text=""></asp:TextBox>
                                                                            <asp:HiddenField ID="SearchTestSession_sessionAuthorNameHiddenField" runat="server" />
                                                                            <asp:HiddenField ID="SearchTestSession_sessionAuthorIDHiddenField" runat="server" />
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="SearchTestSession_sessionAuthorNameImageButton" SkinID="sknbtnSearchicon"
                                                                                Visible="false" runat="server" ImageAlign="Middle" Width="16px" ToolTip="Click here to select the session author" /></div>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="SearchTestSession_schedulerNameHeadLabel" runat="server" Text="Test Schedule Creator"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="SearchTestSession_schedulerNameTextBox" runat="server" ReadOnly="true"
                                                                                Text=""></asp:TextBox>
                                                                            <asp:HiddenField ID="SearchTestSession_schedulerIDHiddenField" runat="server" />
                                                                            <asp:HiddenField ID="SearchTestSession_schedulerHiddenField" runat="server" />
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="SearchTestSession_schedulerNameImageButton" SkinID="sknbtnSearchicon"
                                                                                runat="server" ImageAlign="Middle" Width="16px" ToolTip="Click here to select the schedule creator" /></div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" align="right">
                                                                        <asp:Button ID="SearchTestSession_topSearchButton" runat="server" Text="Search" OnClick="SearchTestSession_SearchButton_Click"
                                                                            SkinID="sknButtonId" />
                                                                    </td>
                                                                </tr>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="SearchTestSession_searchTestResultsTR" runat="server">
                                    <td id="Td1" class="header_bg" align="center" runat="server">
                                        <asp:UpdatePanel ID="SearchCategory_searchResultsUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%">
                                                            <asp:Literal ID="SearchTestSession_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>
                                                            &nbsp;<asp:Label ID="SearchTestSession_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                                Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%" align="right">
                                                            <span id="SearchTestSession_searchResultsUpSpan" style="display: none;" runat="server">
                                                                <asp:Image ID="SearchTestSession_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                            </span><span id="SearchTestSession_searchResultsDownSpan" style="display: block;"
                                                                runat="server">
                                                                <asp:Image ID="SearchTestSession_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="left">
                                                    <asp:UpdatePanel ID="SearchTestSession_updatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <div style="height: 200px; overflow: auto;" runat="server" id="SearchTestSession_testDiv">
                                                                <asp:GridView ID="SearchTestSession_testGridView" runat="server" AllowSorting="True"
                                                                    AutoGenerateColumns="False" GridLines="None" Width="100%" OnSorting="SearchTestSession_testGridView_Sorting"
                                                                    OnRowCreated="SearchTestSession_testGridView_RowCreated" OnRowDataBound="SearchTestSession_testGridView_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="SearchTestSession_selectLinkButton" runat="server" Text="Select"
                                                                                    OnClientClick="javascript:return OnSelectClick(this);" ToolTip="Select"></asp:LinkButton>
                                                                                <asp:HiddenField ID="SearchTestSession_selectIdHiddenfield" runat="server" Value='<%# Eval("TestSessionID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="TestSessionID" HeaderText="Test Session ID">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchTestSession_testSessionIDLabel" runat="server" Text='<%# Eval("TestSessionID") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="TestName" HeaderText="Test&nbsp;Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchTestSession_testNameLabel" runat="server" Text='<%# TrimContent(Eval("TestName").ToString(),20) %>'
                                                                                    ToolTip='<%# Eval("TestName")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="TestSessionAuthor" HeaderText="Author">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchTestSession_testSessionAuthor" runat="server" ToolTip='<%# Eval("TestSessionAuthorFullName") %>'
                                                                                    Text='<%# Eval("TestSessionAuthor") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" SortExpression="NumberOfQuestions"
                                                                            HeaderStyle-CssClass="td_padding_right_15" HeaderText="No of Questions" ItemStyle-CssClass="td_padding_right_15">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchTestSession_noOfQuestionsLabel" runat="server" Text='<%# Eval("NumberOfQuestions") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="CreatedDate DESC" HeaderText="Created Date">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchTestSession_createdDateLabel" runat="server" Text='<%# Convert.ToDateTime(Eval("CreatedDate")).ToString("MM/dd/yyyy") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField SortExpression="TotalCredit" HeaderText="Cost (in&nbsp;$)" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="SearchTestSession_totalCreditLabel" runat="server" Text='<%# Eval("TotalCredit") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Certification Interview" SortExpression="IsCertification DESC" Visible="false">
                                                                            <ItemTemplate>
                                                                                <%# (Boolean.Parse(Eval("IsCertification").ToString())) ? "Yes" : "No"%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="SearchTestSession_pagingControlUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <uc1:PageNavigator ID="SearchTestSession_bottomPagingNavigator" runat="server" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" align="left">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="SearchTestSession_bottomLinksUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="SearchTestSession_bottomReset" runat="server" Text="Reset" SkinID="sknPopupLinkButton"
                                                    OnClick="SearchTestSession_bottomReset_Click"></asp:LinkButton>
                                            </td>
                                            <td class="pop_divider_line">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="SearchTestSession_bottomCancel" runat="server" Text="Cancel"
                                                    SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="ScheduleCandidate_isMaximizedHiddenField" runat="server" />
</asp:Content>
