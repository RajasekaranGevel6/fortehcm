<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="InterviewQuestionUsageSummary.aspx.cs" Title="Question Usage Summary" Inherits="Forte.HCM.UI.Popup.InterviewQuestionUsageSummary" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="InterviewQuestionUsageSummary_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="InterviewQuestionUsageSummary_headerLiteral" runat="server" Text="Question Usage Summary"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="InterviewQuestionUsageSummary_topCloseImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td align="left" class="popup_td_padding_10">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="msg_align" colspan="2">
                                        <asp:UpdatePanel ID="InterviewQuestionUsageSummary_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="InterviewQuestionUsageSummary_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="InterviewQuestionUsageSummary_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="InterviewQuestionUsageSummary_pageNavigator" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">
                                        <asp:Label ID="InterviewQuestionUsageSummary_questionKeyLabel" runat="server" Text="Question ID"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 85%">
                                        <asp:Label ID="InterviewQuestionUsageSummary_questionKeyValueLabel" runat="server" Text=""
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr id="InterviewQuestionUsageSummary_searchTestResultsTR" runat="server">
                                    <td id="Td1" class="header_bg" align="center" runat="server" colspan="2">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 50%" align="left">
                                                    <asp:Literal ID="InterviewQuestionUsageSummary_searchResultsLiteral" runat="server" Text="Interview Details"></asp:Literal>
                                                    &nbsp;<asp:Label ID="InterviewQuestionUsageSummary_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                        Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                </td>
                                                <td style="width: 50%" align="right">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:UpdatePanel ID="InterviewQuestionUsageSummary_questionsDetailsUpdatePanel" runat="server"
                                            UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left" class="grid_body_bg">
                                                            <div id="InterviewQuestionUsageSummary_questionsDetailsDIV" runat="server" style="height: 250px;
                                                                overflow: auto;">
                                                                <asp:GridView ID="InterviewQuestionUsageSummary_questionDetailsGridView" runat="server" AutoGenerateColumns="False"
                                                                    SkinID="sknWrapHeaderGrid" AllowSorting="True" Width="100%" OnRowCreated="InterviewQuestionUsageSummary_questionDetailsGridView_RowCreated"
                                                                    OnSorting="InterviewQuestionUsageSummary_questionDetailsGridView_Sorting" OnRowDataBound="InterviewQuestionUsageSummary_questionDetailsGridView_RowDataBound">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="TestKey" HeaderText="Interview ID" SortExpression="TESTKEY"
                                                                            ItemStyle-Width="40px" />
                                                                        <asp:TemplateField SortExpression="NAME" ItemStyle-Width="100px" HeaderText="Interview Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="InterviewQuestionUsageSummary_testNameLabel" runat="server" Text='<%# TrimContent(Eval("Name").ToString(),30) %>'
                                                                                    ToolTip='<%# Eval("Name") %>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Created Date" SortExpression="TESTCREATIONDATE DESC" ItemStyle-Width="30px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="InterviewQuestionUsageSummary_testCreatedDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.TestDetail)Container.DataItem).TestCreationDate) %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="NoOfCandidatesAdministered" HeaderText="Candidates Administered"
                                                                            SortExpression="NOOFCANDIDATESADMINISTERED DESC" HeaderStyle-HorizontalAlign="Center"
                                                                            HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="10px"
                                                                            ItemStyle-CssClass="td_padding_right_15"></asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <uc3:PageNavigator ID="InterviewQuestionUsageSummary_pageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_5">
                <asp:LinkButton ID="InterviewQuestionUsageSummary_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClientClick="javascript:CloseMe()" />
            </td>
        </tr>
    </table>
</asp:Content>
