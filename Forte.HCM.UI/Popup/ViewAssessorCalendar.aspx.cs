﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewAssessorCalendar.aspx.cs
// Class that represents the user interface layout and functionalities for
// the ViewAssessorCalendar page. This will helps to view the calendar of
// the assessor that shows the scheduled dates & time slots and non 
// availability dates.

#endregion Header

#region Directives                                                             

using System;
using System.Linq;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ViewAssessorCalendar page. This will helps to view the calendar of
    /// the assessor that shows the scheduled dates & time slots and non 
    /// availability dates. This class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ViewAssessorCalendar : PageBase
    {
        #region Event Handlers                                                 

        private int assessorID = 0;

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Assessor Calendar");

                if (Utility.IsNullOrEmpty(Request.QueryString["assessorid"]))
                {
                    base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel, "Assessor ID is not passed");
                    return;
                }
                int.TryParse(Request.QueryString["assessorid"], out assessorID);
                if (assessorID == 0)
                {
                    base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel, "Assessor ID is invalid");
                    return;
                }

                if (!IsPostBack)
                {
                    // Clear session.
                    Session["ASSESSOR_CALENDAR_NON_AVAILABLE_DATES"] = null;
                    Session["ASSESSOR_CALENDAR_SCHEDULED_DATES"] = null;

                    // Set current date as selected date.
                    ViewAssessorCalendar_selectDateCalendar.SelectedDates.Clear();

                    // Get current date and time.
                    DateTime currentDate = DateTime.Now;
                    ViewAssessorCalendar_selectDateCalendar.SelectedDates.Add(currentDate);

                    // Keep the date in session.
                    Session["ASSESSOR_CALENDAR_DATE"] = currentDate;

                    ViewAssessorCalendar_selectedDateLabel.Text = currentDate.ToString("dddd, dd MMMM yyyy");

                    // Load time slots.
                    LoadTimeSlots();

                    // Highlight monthly slots.
                    HighlightMonthlySlots(currentDate.Month, currentDate.Year);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the calender control is 
        /// prerendered.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void ViewAssessorCalendar_selectDateCalendar_PreRender(object sender, EventArgs e)
        {
            try
            {
                // Clear selected dates.
                ViewAssessorCalendar_selectDateCalendar.SelectedDates.Clear();

                if (Utility.IsNullOrEmpty(Session["ASSESSOR_CALENDAR_DATE"]))
                    return;

                // Highlight the selected date.
                ViewAssessorCalendar_selectDateCalendar.SelectedDates.Add
                    ((DateTime)Session["ASSESSOR_CALENDAR_DATE"]);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when a date in the calender control
        /// is selected.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        protected void ViewAssessorCalendar_selectDateCalendar_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                // Keep the date in session.
                Session["ASSESSOR_CALENDAR_DATE"] = ViewAssessorCalendar_selectDateCalendar.SelectedDate;

                ViewAssessorCalendar_selectedDateLabel.Text = ViewAssessorCalendar_selectDateCalendar.
                    SelectedDate.ToString("dddd, dd MMMM yyyy");

                // Reload time slots.
                LoadTimeSlots();

                // Get the current date.
                DateTime currentDate = (DateTime)Session["ASSESSOR_CALENDAR_DATE"];

                // Set the visible date.
                if (Session["ASSESSOR_CALENDAR_DATE"] != null)
                {
                    ViewAssessorCalendar_selectDateCalendar.VisibleDate =
                        (DateTime)Session["ASSESSOR_CALENDAR_DATE"];
                }

                // Load time slots.
                LoadTimeSlots();

                // Highlight monthly slots.
                HighlightMonthlySlots(currentDate.Month, currentDate.Year);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the previous date button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will loads the time slots for the previous date.
        /// </remarks>
        protected void ViewAssessorCalendar_previousDate_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                // Check if selected date is available.
                if (Session["ASSESSOR_CALENDAR_DATE"] == null)
                {
                    base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel,
                        "No selected date is present");
                    return;
                }

                DateTime actualDate = (DateTime)Session["ASSESSOR_CALENDAR_DATE"];

                // Check if date is min value.
                if (IsMinDate(actualDate))
                {
                    base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel,
                        "Cannot process dates lesser than this");
                    return;
                }

                // Construct new date.
                DateTime newDate = ((DateTime)Session["ASSESSOR_CALENDAR_DATE"]).AddDays(-1);

                // Keep the date in session.
                Session["ASSESSOR_CALENDAR_DATE"] = newDate;

                ViewAssessorCalendar_selectedDateLabel.Text = newDate.ToString("dddd, dd MMMM yyyy");

                // Reload time slots.
                LoadTimeSlots();

                // Highlight monthly slots.
                HighlightMonthlySlots(newDate.Month, newDate.Year);

                // Set the visible date.
                if (Session["ASSESSOR_CALENDAR_DATE"] != null)
                {
                    ViewAssessorCalendar_selectDateCalendar.VisibleDate =
                        (DateTime)Session["ASSESSOR_CALENDAR_DATE"];
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the next date button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will loads the time slots for the next date.
        /// </remarks>
        protected void ViewAssessorCalendar_nextDate_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                // Check if selected date is available.
                if (Session["ASSESSOR_CALENDAR_DATE"] == null)
                {
                    base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel,
                        "No selected date is present");
                    return;
                }

                DateTime actualDate = (DateTime)Session["ASSESSOR_CALENDAR_DATE"];

                // Check if date is max value.
                if (IsMaxDate(actualDate))
                {
                    base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel,
                        "Cannot process dates greater than this");
                    return;
                }

                // Construct new date.
                DateTime newDate = ((DateTime)Session["ASSESSOR_CALENDAR_DATE"]).AddDays(1);

                // Keep the date in session.
                Session["ASSESSOR_CALENDAR_DATE"] = newDate;

                ViewAssessorCalendar_selectedDateLabel.Text = newDate.ToString("dddd, dd MMMM yyyy");

                // Reload time slots.
                LoadTimeSlots();

                // Highlight monthly slots.
                HighlightMonthlySlots(newDate.Month, newDate.Year);

                // Set the visible date.
                if (Session["ASSESSOR_CALENDAR_DATE"] != null)
                {
                    ViewAssessorCalendar_selectDateCalendar.VisibleDate =
                        (DateTime)Session["ASSESSOR_CALENDAR_DATE"];
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the visible month is changed in
        /// the calendar control.
        /// </summary>
        /// <param name="e">
        /// A <see cref="MonthChangedEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will loads the time slots for the next date.
        /// </remarks>
        protected void ViewAssessorCalendar_selectDateCalendar_VisibleMonthChanged
            (object sender, MonthChangedEventArgs e)
        {
            try
            {
                // Highlight monthly slots.
                HighlightMonthlySlots(e.NewDate.Month, e.NewDate.Year);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the day rendering is happen in 
        /// the calendar control.
        /// </summary>
        /// <param name="e">
        /// A <see cref="DayRenderEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will highlights the non availability dates.
        /// </remarks>
        protected void ViewAssessorCalendar_selectDateCalendar_DayRender(object sender, DayRenderEventArgs e)
        {
            try
            {
                // Check if the date is present in non availability dates.
                if (base.IsDatePresent(e.Day.Date,
                    Session["ASSESSOR_CALENDAR_NON_AVAILABLE_DATES"] as List<DateTime>))
                {
                    e.Cell.BackColor = Color.FromArgb(250, 148, 118);
                    e.Cell.ForeColor = Color.FromArgb(0, 0, 0);
                }

                // Check if the date is present in the scheduled dates.
                if (base.IsDatePresent(e.Day.Date,
                    Session["ASSESSOR_CALENDAR_SCHEDULED_DATES"] as List<DateTime>))
                {
                    e.Cell.BackColor = Color.FromArgb(141, 222, 153);
                    e.Cell.ForeColor = Color.FromArgb(0, 0, 0);
                }

                // Check if the date is selected date.
                if (Session["ASSESSOR_CALENDAR_DATE"] != null)
                {
                    if (base.IsDatesEqual(e.Day.Date, (DateTime)Session["ASSESSOR_CALENDAR_DATE"]))
                    {
                        e.Cell.BackColor = Color.FromArgb(89, 193, 238);
                        e.Cell.ForeColor = Color.FromArgb(0, 0, 0);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void ViewAssessorCalendar_timeSlotsGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void ViewAssessorCalendar_timeSlotsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    LinkButton ViewAssessorCalendar_timeSlotsGridView_skillLinkButton =
                        (LinkButton)e.Row.FindControl("ViewAssessorCalendar_timeSlotsGridView_skillLinkButton");

                    HtmlContainerControl ViewAssessorCalendar_timeSlotsGridView_detailsDiv =
                        (HtmlContainerControl)e.Row.FindControl("ViewAssessorCalendar_timeSlotsGridView_detailsDiv");

                    HtmlAnchor ViewAssessorCalendar_timeSlotsGridView_detailsViewFocusDownLink =
                        (HtmlAnchor)e.Row.FindControl("ViewAssessorCalendar_timeSlotsGridView_detailsViewFocusDownLink");

                    ViewAssessorCalendar_timeSlotsGridView_skillLinkButton.Attributes.Add("onclick", "return TestHideDetails('" +
                        ViewAssessorCalendar_timeSlotsGridView_detailsDiv.ClientID + "','" + ViewAssessorCalendar_timeSlotsGridView_detailsViewFocusDownLink.ClientID + "')");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel, exp.Message);
            }
        }
        
        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the time slots for the selected date.
        /// </summary>
        private void LoadTimeSlots()
        {
            if (Session["ASSESSOR_CALENDAR_DATE"] == null)
                return;

            // Clear data.
            ViewAssessorCalendar_timeSlotsGridView.DataSource = null;
            ViewAssessorCalendar_timeSlotsGridView.DataBind();

            // Assign default values to summary.
            ViewAssessorCalendar_summaryDateValueLabel.Text = ((DateTime)Session["ASSESSOR_CALENDAR_DATE"]).ToString("MM/dd/yyyy");
            ViewAssessorCalendar_totalScheduledTimeSlotsValueLabel.Text = "0";
            ViewAssessorCalendar_totalScheduledHoursValueLabel.Text = "00:00";

            // Load assessor detail.
            AssessorDetail assessorDetail = new AssessorBLManager().GetAssessorByUserId(assessorID);

            if (assessorDetail == null)
            {
                base.ShowMessage(ViewAssessorCalendar_topErrorMessageLabel, "No such assessor exist");
                return;
            }

            // Assign first name.
            ViewAssessorCalendar_assessorNameValueLabel.Text = assessorDetail.FirstName;

            // Append last name.
            if (!Utility.IsNullOrEmpty(assessorDetail.LastName))
            {
                ViewAssessorCalendar_assessorNameValueLabel.Text = ViewAssessorCalendar_assessorNameValueLabel.Text + " " +
                    assessorDetail.LastName;
            }

            if (!Utility.IsNullOrEmpty(assessorDetail.NonAvailabilityDates))
            {
                List<DateTime> nonAvailabilityDates = assessorDetail.NonAvailabilityDates.Split
                    (',').Select(n => Convert.ToDateTime(n)).ToList();

                // Keep non availability dates in session.
                Session["ASSESSOR_CALENDAR_NON_AVAILABLE_DATES"] = nonAvailabilityDates;
            }

            // Load assessor time slots.
            List<AssessorTimeSlotDetail> timeSlots = new AssessorBLManager().
                GetAssessorTimeSlots(assessorID, (DateTime)Session["ASSESSOR_CALENDAR_DATE"]);

            if (timeSlots == null || timeSlots.Count == 0)
            {
                return;
            }

            ViewAssessorCalendar_timeSlotsGridView.DataSource = timeSlots;
            ViewAssessorCalendar_timeSlotsGridView.DataBind();

            // Assign summary.
            ViewAssessorCalendar_totalScheduledTimeSlotsValueLabel.Text = timeSlots.Count.ToString();
            ViewAssessorCalendar_totalScheduledHoursValueLabel.Text = GetTotalScheduledHours(timeSlots);
        }

        /// <summary>
        /// Method that calculates and returns the total hours scheduled from
        /// the list of time slots given for a selected date.
        /// </summary>
        /// <param name="timeSlots">
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the time
        /// slots.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the total scheduled hours in a 
        /// time format.
        /// </returns>
        private string GetTotalScheduledHours(List<AssessorTimeSlotDetail> timeSlots)
        {
            string hours = "00:00";

            int minutes = 0;
            foreach (AssessorTimeSlotDetail timeSlot in timeSlots)
            {
                minutes += ((timeSlot.TimeSlotIDTo - timeSlot.TimeSlotIDFrom) * 30);
            }

            if (minutes == 1440)
            {
                // For 24 hrs the system will return 00:00. So manually set
                // the value 24:00.
                hours = "24:00";
            }
            else
            {
                TimeSpan timeSpan = TimeSpan.FromMinutes(minutes);
                hours = string.Format("{0:D2}:{1:D2}",
                    timeSpan.Hours, timeSpan.Minutes);
            }

            return hours;
        }

        /// <summary>
        /// Method that highlights the monthly slot dates.
        /// </summary>
        /// <param name="month">
        /// A <see cref="int"/> that holds the month.
        /// </param>
        /// <param name="year">
        /// A <see cref="int"/> that holds the year.
        /// </param>
        /// <remarks>
        /// This method will be called whenever a month is changed in the 
        /// calendar control.
        /// </remarks>
        private void HighlightMonthlySlots(int month, int year)
        {
            // Clear all selected dates.
            ViewAssessorCalendar_selectDateCalendar.SelectedDates.Clear();

            // Get slots for current, previous and next months and keep in the session.
            Session["ASSESSOR_CALENDAR_SCHEDULED_DATES"] = new AssessorBLManager().
                GetAssessorTimeSlotDates(assessorID, month, year, true);
        }
        #endregion Private Methods
    }
}