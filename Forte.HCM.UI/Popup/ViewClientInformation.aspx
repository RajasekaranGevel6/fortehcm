<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="ViewClientInformation.aspx.cs" Title="View Client Information" Inherits="Forte.HCM.UI.Popup.ViewClientInformation" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ViewClientInformation_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="ViewClientInformation_headerLiteral" runat="server" Text="Client Detail"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="ViewClientInformation_topCloseImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:window.close();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_view_client_bg">
                    <tr>
                        <td class="msg_align" valign="top">
                            <asp:Label ID="ViewClientInformation_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="ViewClientInformation_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="popup_td_padding_10" style="width: 100%" valign="top">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td colspan="3">
                                        <asp:Label ID="ViewClientInformation_clientNameLabel" runat="server"
                                            SkinID="sknLabelViewClientNameFieldText"></asp:Label>
                                    </td>
                                    <td valign="bottom" align="right">
                                        <asp:HyperLink ID="ViewClientInformation_moreHyperLink" runat="server" ToolTip="Click here to view more details on the client" 
                                            Text="more..." Target="_blank" SkinID="sknMoreOrangeHyperLink"></asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr id="trDepartmentDescription" runat="server" style="display:none">
                                    <td colspan="4">
                                        <div style="height: 20px; width: 100%; overflow: auto;">
                                            <asp:Label ID="ViewClientInformation_departmentDescriptionLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4"></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:HyperLink ID="ViewClientInformation_emailIDHyperLink" runat="server"></asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 14%">
                                        <asp:Label ID="ViewClientInformation_phoneNoHeadLabel" runat="server" Text="Phone No" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:Label ID="ViewClientInformation_phoneNoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td style="width: 14%">
                                        <asp:Label ID="ViewClientInformation_faxNoHeadLabel" runat="server" Text="Fax No" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:Label ID="ViewClientInformation_faxNoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="ViewClientInformation_streetAddressHeadLabel" runat="server" Text="Street Address"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClientInformation_streetAddressLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClientInformation_cityHeadLabel" runat="server" Text="City" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClientInformation_cityLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="ViewClientInformation_stateHeadLabel" runat="server" Text="State Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClientInformation_statelabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClientInformation_countryHeadLabel" runat="server" Text="Country" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClientInformation_countryLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="ViewClientInformation_zipHeadLabel" runat="server" Text="Zip Code" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClientInformation_zipLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClientInformation_feinNoHeadLabel" runat="server" Text="FEIN NO" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ViewClientInformation_feinNoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="ViewClientInformation_wesiteURLHeadLabel" runat="server" Text="Website URL" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:HyperLink ID="ViewClientInformation_websiteURLHyperLink" runat="server" Target="_blank" ToolTip="Click here to navigate to the URL" SkinID="sknURLHyperLink"></asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 8px" colspan="2">
                                    </td>
                                </tr>
                                <tr id="trClientDepartmentContact" runat="server" style="display:none">
                                    <td colspan="4">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="width: 14%" align="left" valign="top">
                                                    <asp:Label ID="ViewClientInformation_departmentsHeadLabel" runat="server" Text="Departments"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 85%" align="left" valign="top">
                                                    <div style="height: 20px; width: 100%; overflow: auto;">
                                                        <asp:Label ID="ViewClientInformation_departmentsLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 8px" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 14%" align="left" valign="top">
                                                    <asp:Label ID="ViewClientInformation_contactsHeadLabel" runat="server" Text="Contacts"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 85%" align="left" valign="top">
                                                    <div style="height: 20px; width: 100%; overflow: auto;">
                                                        <asp:Label ID="ViewClientInformation_contactsLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- Client Additional Information -->
                                <tr>
                                    <td colspan="4">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="width: 14%" align="left" valign="top">
                                                    <asp:Label ID="ViewClientInformation_additionalInfoHeadLabel" runat="server" Text="Client Additional Info"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 5px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="height: 75px; width: 100%; overflow: auto;">
                                                        <asp:Label ID="ViewClientInformation_additionalInfoLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- Department Additional Information -->
                                <tr id="trDepartmentAdditionalInformation" runat="server" style="display:none">
                                    <td colspan="4">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="width: 14%" align="left" valign="top">
                                                    <asp:Label ID="ViewClientInformation_departmentAdditionalInfoLabel" runat="server" Text="Department Additional Info"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 5px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="height: 75px; width: 100%; overflow: auto;" >
                                                        <asp:Label ID="ViewClientInformation_departmentAdditionalInfoLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- Contact Additional Information -->
                                <tr id="trContactAdditionalInformation" runat="server" style="display:none">
                                    <td colspan="4">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="width: 14%" align="left" valign="top">
                                                    <asp:Label ID="ViewClientInformation_contactAdditionalInfoLabel" runat="server" Text="Contact Additional Info"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 5px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="height: 75px; width: 100%; overflow: auto;">
                                                        <asp:Label ID="ViewClientInformation_contactAdditionalInfoLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_5">
                <asp:LinkButton ID="ViewClientInformation_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClientClick="javascript:window.close();" ToolTip="Click here to close the window" />
            </td>
        </tr>
    </table>
</asp:Content>
