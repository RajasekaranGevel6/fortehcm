﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EmailCandidate.aspx.cs
// File that represents the user interface layout and functionalities for
// the EmailCandidate page. This helps in sending emails to the candidate
// in various scenarios like viewing schedules, during online interview 
// etc.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Configuration;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the EmailCandidate page. This helps in sending emails to the candidate
    /// in various scenarios like viewing schedules, during online interview 
    /// etc. This class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class EmailCandidate : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Email Candidate");

                Response.Cache.SetCacheability(HttpCacheability.NoCache);

                // Set default button field
                Page.Form.DefaultButton = EmailCandidate_sendButton.UniqueID;

                // Validate email address.
                EmailCandidate_sendButton.Attributes.Add("onclick", "javascript:return IsValidEmails('"
                    + EmailCandidate_toTextBox.ClientID + "','"
                    + EmailCandidate_ccTextBox.ClientID + "','"
                    + EmailCandidate_errorMessageLabel.ClientID + "','"
                    + EmailCandidate_successMessageLabel.ClientID + "');");
                
                // Add handler to open 'select contact' popup.
                string positionProfileID = string.Empty;
                if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                    positionProfileID = Request.QueryString["positionprofileid"].Trim();

                EmailCandidate_toAddressImageButton.Attributes.Add
                    ("onclick", "javascript:return ShowSelectContactPopup('" +
                    EmailCandidate_toTextBox.ClientID + "','" + positionProfileID + "','To','N')");

                EmailCandidate_ccAddressImageButton.Attributes.Add
                   ("onclick", "javascript:return ShowSelectContactPopup('" +
                   EmailCandidate_ccTextBox.ClientID + "','" + positionProfileID + "','CC','N')");

                if (!IsPostBack)
                {
                    // Set default focus field
                    Page.Form.DefaultFocus = EmailCandidate_toTextBox.UniqueID;
                    EmailCandidate_toTextBox.Focus();

                    EmailCandidate_fromValueLabel.Text = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                    EmailCandidate_fromHiddenField.Value = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];

                    // Check if candidate ID is passed.
                    if (Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
                    {
                        base.ShowMessage(EmailCandidate_errorMessageLabel, "Candidate ID is not passed");
                        return;
                    }

                    // Get candidate ID.
                    int candidateID = 0;
                    int.TryParse(Request.QueryString["candidateid"], out candidateID);

                    // Check if candidate ID is valid.
                    if (candidateID == 0)
                    {
                        base.ShowMessage(EmailCandidate_errorMessageLabel, "Candidate ID is invalid");
                        return;
                    }

                    // Get candidate detail.
                    UserDetail userDetail = new CommonBLManager().GetUserDetail(candidateID);

                    if (userDetail == null)
                    {
                        base.ShowMessage(EmailCandidate_errorMessageLabel, "No such assessor exist");
                        return;
                    }

                    // Assign to to and cc.
                    EmailCandidate_toTextBox.Text = userDetail.Email;

                    // Compose email.
                    ComposeEmail(userDetail);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EmailCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will be called when the send button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void EmailCandidate_sendButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear all the error and success labels
                EmailCandidate_errorMessageLabel.Text = string.Empty;
                EmailCandidate_successMessageLabel.Text = string.Empty;

                // Validate the fields
                if (!IsValidData())
                    return;

                // Build mail detail.
                MailDetail mailDetail = new MailDetail();
                mailDetail.To = new List<string>();
                mailDetail.CC = new List<string>();

                // Add the logged in user in the CC list when 'send me a copy ' is on
                if (EmailAttachement_sendMeACopyCheckBox.Checked)
                {
                    mailDetail.CC.Add(base.userEmailID);
                }

                // Set to address.
                string[] toMailIDs = EmailCandidate_toTextBox.Text.Split(new char[] { ',' });
                for (int idx = 0; idx < toMailIDs.Length; idx++)
                    mailDetail.To.Add(toMailIDs[idx].Trim());

                // Set CC address.
                if (!Utility.IsNullOrEmpty(EmailCandidate_ccTextBox.Text.Trim()))
                {
                    string[] ccMailIDs = EmailCandidate_ccTextBox.Text.Split(new char[] { ',' });
                    for (int idx = 0; idx < ccMailIDs.Length; idx++)
                        mailDetail.CC.Add(ccMailIDs[idx].Trim());
                }

                // Set from address.
                mailDetail.From = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                
                // Set subject.
                mailDetail.Subject = EmailCandidate_subjectTextBox.Text.Trim();

                // Set message.
                mailDetail.Message = EmailCandidate_messageTextBox.Text.Trim();
                mailDetail.isBodyHTML = true;
                bool isMailSent = false;

                try
                {
                    // Send mail.
                    new EmailHandler().SendMail(mailDetail);
                    isMailSent = true;
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    isMailSent = false;
                }
                //Script that close the popup window
                string closeScript = "self.close();";

                if (isMailSent)
                {
                    //base.ShowMessage(EmailCandidate_successMessageLabel,
                    //    Resources.HCMResource.EmailCandidate_EmailSent);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Closescript", closeScript, true);
                }
                else
                {
                    base.ShowMessage(EmailCandidate_errorMessageLabel,
                        Resources.HCMResource.EmailCandidate_MailCouldNotSend);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EmailCandidate_errorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool isMandatoryMissing = false;

            // Validate to address field.
            if (EmailCandidate_toTextBox.Text.Trim().Length == 0)
            {
                isMandatoryMissing = true;
                isValidData = false;
            }
            
            // Validate message field.
            if (EmailCandidate_messageTextBox.Text.Trim().Length == 0)
            {
                isMandatoryMissing = true;
                isValidData = false;
            }

            if (isMandatoryMissing)
            {
                base.ShowMessage(EmailCandidate_errorMessageLabel,
                   Resources.HCMResource.EmailCandidate_MandatoryCannotBeEmpty);
            }

            return isValidData;
        }

        /// <summary>
        /// Overridden protected method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        private void AssignAssessorDetail()
        {

        }

        /// <summary>
        /// Method that compose the basic skeleton for for the email.
        /// </summary>
        /// <param name="userDetail">
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </param>
        private void ComposeEmail(UserDetail userDetail)
        {
            string message = string.Empty;

            message += "Dear " + userDetail.FirstName + ",";
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += "This is a system generated message";
            message += Environment.NewLine;
            message += "Do not reply to this message";
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += "============================== Disclaimer ==============================";
            message += Environment.NewLine;
            message += "This message is for the designated recipient only and may contain privileged, proprietary, or ";
            message += Environment.NewLine;
            message += "otherwise private information. If you have received it in error, please notify the sender";
            message += Environment.NewLine;
            message += "immediately and delete the original. Any other use of the email by you is prohibited.";
            message += Environment.NewLine;
            message += "=========================== End Of Disclaimer ============================";

            EmailCandidate_messageTextBox.Text = message;
        }

        #endregion Private Methods
    }
}