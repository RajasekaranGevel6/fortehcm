﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.Popup {
    
    
    public partial class SearchInterviewTestSession {
        
        /// <summary>
        /// SearchInterviewTestSession_headerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SearchInterviewTestSession_headerLiteral;
        
        /// <summary>
        /// QuestionDetailPreviewControl_topCancelImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton QuestionDetailPreviewControl_topCancelImageButton;
        
        /// <summary>
        /// SearchInterviewTestSession_messageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchInterviewTestSession_messageUpdatePanel;
        
        /// <summary>
        /// SearchInterviewTestSession_topSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchInterviewTestSession_topSuccessMessageLabel;
        
        /// <summary>
        /// SearchInterviewTestSession_topErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchInterviewTestSession_topErrorMessageLabel;
        
        /// <summary>
        /// SearchInterviewTestSession_searchCriteriasDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl SearchInterviewTestSession_searchCriteriasDiv;
        
        /// <summary>
        /// SearchInterviewTestSession_searchCriteriaUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchInterviewTestSession_searchCriteriaUpdatePanel;
        
        /// <summary>
        /// SearchInterviewTestSession_testSessionIdLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchInterviewTestSession_testSessionIdLabel;
        
        /// <summary>
        /// SearchInterviewTestSession_testSessionIdTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchInterviewTestSession_testSessionIdTextBox;
        
        /// <summary>
        /// SearchInterviewTestSession_testIdLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchInterviewTestSession_testIdLabel;
        
        /// <summary>
        /// SearchInterviewTestSession_testIdTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchInterviewTestSession_testIdTextBox;
        
        /// <summary>
        /// SearchInterviewTestSession_testNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchInterviewTestSession_testNameLabel;
        
        /// <summary>
        /// SearchInterviewTestSession_testNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchInterviewTestSession_testNameTextBox;
        
        /// <summary>
        /// SearchInterviewTestSession_positionProfileLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchInterviewTestSession_positionProfileLabel;
        
        /// <summary>
        /// SearchInterviewTestSession_positionProfileTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchInterviewTestSession_positionProfileTextBox;
        
        /// <summary>
        /// SearchInterviewTestSession_positionProfileImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton SearchInterviewTestSession_positionProfileImageButton;
        
        /// <summary>
        /// SearchInterviewTestSession_positionProfileIDHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchInterviewTestSession_positionProfileIDHiddenField;
        
        /// <summary>
        /// SearchInterviewTestSession_sessionAuthorNameHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchInterviewTestSession_sessionAuthorNameHeadLabel;
        
        /// <summary>
        /// SearchInterviewTestSession_sessionAuthorNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchInterviewTestSession_sessionAuthorNameTextBox;
        
        /// <summary>
        /// SearchInterviewTestSession_sessionAuthorNameHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchInterviewTestSession_sessionAuthorNameHiddenField;
        
        /// <summary>
        /// SearchInterviewTestSession_sessionAuthorIDHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchInterviewTestSession_sessionAuthorIDHiddenField;
        
        /// <summary>
        /// SearchInterviewTestSession_sessionAuthorNameImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton SearchInterviewTestSession_sessionAuthorNameImageButton;
        
        /// <summary>
        /// SearchInterviewTestSession_schedulerNameHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchInterviewTestSession_schedulerNameHeadLabel;
        
        /// <summary>
        /// SearchInterviewTestSession_schedulerNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchInterviewTestSession_schedulerNameTextBox;
        
        /// <summary>
        /// SearchInterviewTestSession_schedulerIDHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchInterviewTestSession_schedulerIDHiddenField;
        
        /// <summary>
        /// SearchInterviewTestSession_schedulerHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchInterviewTestSession_schedulerHiddenField;
        
        /// <summary>
        /// SearchInterviewTestSession_schedulerNameImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton SearchInterviewTestSession_schedulerNameImageButton;
        
        /// <summary>
        /// SearchInterviewTestSession_topSearchButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button SearchInterviewTestSession_topSearchButton;
        
        /// <summary>
        /// SearchInterviewTestSession_searchTestResultsTR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow SearchInterviewTestSession_searchTestResultsTR;
        
        /// <summary>
        /// Td1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableCell Td1;
        
        /// <summary>
        /// SearchCategory_searchResultsUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchCategory_searchResultsUpdatePanel;
        
        /// <summary>
        /// SearchInterviewTestSession_searchResultsLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SearchInterviewTestSession_searchResultsLiteral;
        
        /// <summary>
        /// SearchInterviewTestSession_sortHelpLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchInterviewTestSession_sortHelpLabel;
        
        /// <summary>
        /// SearchInterviewTestSession_searchResultsUpSpan control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl SearchInterviewTestSession_searchResultsUpSpan;
        
        /// <summary>
        /// SearchInterviewTestSession_searchResultsUpImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image SearchInterviewTestSession_searchResultsUpImage;
        
        /// <summary>
        /// SearchInterviewTestSession_searchResultsDownSpan control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl SearchInterviewTestSession_searchResultsDownSpan;
        
        /// <summary>
        /// SearchInterviewTestSession_searchResultsDownImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image SearchInterviewTestSession_searchResultsDownImage;
        
        /// <summary>
        /// SearchInterviewTestSession_updatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchInterviewTestSession_updatePanel;
        
        /// <summary>
        /// SearchInterviewTestSession_testDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl SearchInterviewTestSession_testDiv;
        
        /// <summary>
        /// SearchInterviewTestSession_testGridView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView SearchInterviewTestSession_testGridView;
        
        /// <summary>
        /// SearchInterviewTestSession_pagingControlUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchInterviewTestSession_pagingControlUpdatePanel;
        
        /// <summary>
        /// SearchInterviewTestSession_bottomPagingNavigator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.PageNavigator SearchInterviewTestSession_bottomPagingNavigator;
        
        /// <summary>
        /// SearchInterviewTestSession_bottomLinksUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchInterviewTestSession_bottomLinksUpdatePanel;
        
        /// <summary>
        /// SearchInterviewTestSession_bottomReset control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton SearchInterviewTestSession_bottomReset;
        
        /// <summary>
        /// SearchInterviewTestSession_bottomCancel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton SearchInterviewTestSession_bottomCancel;
        
        /// <summary>
        /// ScheduleCandidate_isMaximizedHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ScheduleCandidate_isMaximizedHiddenField;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.PopupMaster Master {
            get {
                return ((Forte.HCM.UI.MasterPages.PopupMaster)(base.Master));
            }
        }
    }
}
