﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.Popup {
    
    
    public partial class ViewCandidateProfile {
        
        /// <summary>
        /// ViewCandidateProfile_titleLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ViewCandidateProfile_titleLiteral;
        
        /// <summary>
        /// ViewCandidateProfile_topCancelImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ViewCandidateProfile_topCancelImageButton;
        
        /// <summary>
        /// ViewCandidateProfile_messageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ViewCandidateProfile_messageUpdatePanel;
        
        /// <summary>
        /// ViewCandidateProfile_topErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewCandidateProfile_topErrorMessageLabel;
        
        /// <summary>
        /// ViewCandidateProfile_topSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewCandidateProfile_topSuccessMessageLabel;
        
        /// <summary>
        /// ViewCandidateProfile_inputControlsUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ViewCandidateProfile_inputControlsUpdatePanel;
        
        /// <summary>
        /// ViewCandidateProfile_candidateIDHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ViewCandidateProfile_candidateIDHiddenField;
        
        /// <summary>
        /// ViewCandidateProfile_photoImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image ViewCandidateProfile_photoImage;
        
        /// <summary>
        /// ViewCandidateProfile_activityLogLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ViewCandidateProfile_activityLogLinkButton;
        
        /// <summary>
        /// ViewCandidateProfile_addNotesLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ViewCandidateProfile_addNotesLinkButton;
        
        /// <summary>
        /// ViewCandidateProfile_viewCandidateActivityDashboardHyperLink control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink ViewCandidateProfile_viewCandidateActivityDashboardHyperLink;
        
        /// <summary>
        /// ViewCandidateProfile_nameValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewCandidateProfile_nameValueLabel;
        
        /// <summary>
        /// ViewCandidateProfile_downloadResumeImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ViewCandidateProfile_downloadResumeImageButton;
        
        /// <summary>
        /// ViewCandidateProfile_linkedInProfileHyperLink control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink ViewCandidateProfile_linkedInProfileHyperLink;
        
        /// <summary>
        /// ViewCandidateProfile_selectedMenuHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ViewCandidateProfile_selectedMenuHiddenField;
        
        /// <summary>
        /// ViewCandidateProfile_contactInfoLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ViewCandidateProfile_contactInfoLinkButton;
        
        /// <summary>
        /// ViewCandidateProfile_synopsisLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ViewCandidateProfile_synopsisLinkButton;
        
        /// <summary>
        /// ViewCandidateProfile_contactInfoDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ViewCandidateProfile_contactInfoDiv;
        
        /// <summary>
        /// ViewCandidateProfile_addresslLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewCandidateProfile_addresslLabel;
        
        /// <summary>
        /// ViewCandidateProfile_addressValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewCandidateProfile_addressValueLabel;
        
        /// <summary>
        /// ViewCandidateProfile_locationLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewCandidateProfile_locationLabel;
        
        /// <summary>
        /// ViewCandidateProfile_locationValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewCandidateProfile_locationValueLabel;
        
        /// <summary>
        /// ViewCandidateProfile_emailLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewCandidateProfile_emailLabel;
        
        /// <summary>
        /// ViewCandidateProfile_emailValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewCandidateProfile_emailValueLabel;
        
        /// <summary>
        /// ViewCandidateProfile_mobilePhoneLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewCandidateProfile_mobilePhoneLabel;
        
        /// <summary>
        /// ViewCandidateProfile_mobilePhoneValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewCandidateProfile_mobilePhoneValueLabel;
        
        /// <summary>
        /// ViewCandidateProfile_homePhoneLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewCandidateProfile_homePhoneLabel;
        
        /// <summary>
        /// ViewCandidateProfile_homePhoneValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewCandidateProfile_homePhoneValueLabel;
        
        /// <summary>
        /// ViewCandidateProfile_synopsisDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ViewCandidateProfile_synopsisDiv;
        
        /// <summary>
        /// ViewCandidateProfile_synopsisValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ViewCandidateProfile_synopsisValueLabel;
        
        /// <summary>
        /// ViewCandidateProfile_bottomControlsUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel ViewCandidateProfile_bottomControlsUpdatePanel;
        
        /// <summary>
        /// ViewCandidateProfile_cancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton ViewCandidateProfile_cancelLinkButton;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.PopupMaster Master {
            get {
                return ((Forte.HCM.UI.MasterPages.PopupMaster)(base.Master));
            }
        }
    }
}
