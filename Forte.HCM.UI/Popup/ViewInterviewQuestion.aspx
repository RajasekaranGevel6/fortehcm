<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="ViewInterviewQuestion.aspx.cs" Title="View Interview Question" Inherits="Forte.HCM.UI.Popup.ViewInterviewQuestion" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ViewInterviewQuestion_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="ViewInterviewQuestion_headerLiteral" runat="server" Text="Interview Question Detail"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="ViewInterviewQuestion_topCloseImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_view_interview_question_bg">
                    <tr>
                        <td align="left" class="popup_td_padding_10" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="msg_align" colspan="2">
                                        <asp:Label ID="ViewInterviewQuestion_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                        <asp:Label ID="ViewInterviewQuestion_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="popup_td_padding_10" valign="top">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popup_panel_inner_bg" valign="top">
                                                    <table width="100%" cellpadding="0" cellspacing="2" border="0">
                                                        <tr>
                                                            <td style="width: 12%">
                                                                <asp:Label ID="ViewInterviewQuestion_testAreaHeadLabel" runat="server" Text="Interview Area"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 15%">
                                                                <asp:Label ID="ViewInterviewQuestion_testAreaValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                            <td style="width: 14%">
                                                                <asp:Label ID="ViewInterviewQuestion_complexityHeadLabel" runat="server" Text="Complexity"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 15%">
                                                                <asp:Label ID="ViewInterviewQuestion_complexityValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                            <td style="width: 5%">
                                                                <asp:Label ID="ViewInterviewQuestion_tagHeadLabel" runat="server" Text="Tag" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 37%">
                                                                <asp:Label ID="ViewInterviewQuestion_tagValueLabel" class="label_multi_field_text"
                                                                    runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popup_question_icon">
                                                    <asp:Label ID="ViewInterviewQuestion_questionLabel" runat="server" class="label_multi_field_text"></asp:Label><br /><br />
                                                    <div style="overflow: auto;" id="ViewInterviewQuestion_questionDiv" runat="server">
                                                        <asp:Image runat="server" ID="ViewInterviewQuestion_questionImage" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 4px" valign="top">
                                                    <div style="overflow: auto;">
                                                        <asp:PlaceHolder ID="ViewInterviewQuestion_answerChoicesPlaceHolder" runat="server">
                                                        </asp:PlaceHolder>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_5">
                <asp:LinkButton ID="ViewInterviewQuestion_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClientClick="javascript:window.close();" ToolTip="Click here to close the window" />
            </td>
        </tr>
    </table>
</asp:Content>
