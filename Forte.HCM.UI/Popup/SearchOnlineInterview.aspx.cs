
#region Header

// Copyright (C) Forte. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchInterviewTestSession.aspx.cs
// This page allows the user to search the existing testSessions 
// and to view all its informations.
#endregion Header

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// This page allows the user to search the existing testSessions 
    /// and to view all its informations.
    /// </summary>
    public partial class SearchOnlineInterview : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "224px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "374px";

        #endregion Private Constants

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set Browser Tittle
                Master.SetPageCaption("Search Online Interview");
                // Set default button
                Page.Form.DefaultButton = SearchOnlineInterview_topSearchButton.UniqueID;

                // Create events for paging control
                SearchOnlineInterview_bottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchOnlineInterview_pagingNavigator_PageNumberClick);

                // Check whether the grid is maximized or not and assign the height accordingly.
                if (!Utility.IsNullOrEmpty(ScheduleCandidate_isMaximizedHiddenField.Value) &&
                        ScheduleCandidate_isMaximizedHiddenField.Value == "Y")
                {
                    SearchOnlineInterview_searchCriteriasDiv.Style["display"] = "none";
                    SearchOnlineInterview_searchResultsUpSpan.Style["display"] = "block";
                    SearchOnlineInterview_searchResultsDownSpan.Style["display"] = "none";
                    SearchOnlineInterview_testDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchOnlineInterview_searchCriteriasDiv.Style["display"] = "block";
                    SearchOnlineInterview_searchResultsUpSpan.Style["display"] = "none";
                    SearchOnlineInterview_searchResultsDownSpan.Style["display"] = "block";
                    SearchOnlineInterview_testDiv.Style["height"] = RESTORED_HEIGHT;
                }

                // Call expand or restore method
                SearchOnlineInterview_searchTestResultsTR.Attributes.Add("onclick",
                        "ExpandOrRestore('" +
                        SearchOnlineInterview_testDiv.ClientID + "','" +
                        SearchOnlineInterview_searchCriteriasDiv.ClientID + "','" +
                        SearchOnlineInterview_searchResultsUpSpan.ClientID + "','" +
                        SearchOnlineInterview_searchResultsDownSpan.ClientID + "','" +
                        ScheduleCandidate_isMaximizedHiddenField.ClientID + "','" +
                        RESTORED_HEIGHT + "','" +
                        EXPANDED_HEIGHT + "')");

                if (!IsPostBack)
                {
                    // Set default focus
                    Page.Form.DefaultFocus = SearchOnlineInterview_topSearchButton.UniqueID;
                    SearchOnlineInterview_interviewKeyTextBox.Focus();

                    SearchOnlineInterview_testDiv.Visible = false;

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "TestSessionID";

                    SetAuthorDetails();
                }
                else
                {
                    SearchOnlineInterview_testDiv.Visible = true;
                }

                // Assign handler to scheduler selection popup.
                SearchOnlineInterview_schedulerNameImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                    + SearchOnlineInterview_schedulerHiddenField.ClientID + "','"
                    + SearchOnlineInterview_schedulerIDHiddenField.ClientID + "','"
                    + SearchOnlineInterview_schedulerNameTextBox.ClientID + "','TC')");

                // Assign handler to session author selection popup.
                SearchOnlineInterview_sessionAuthorNameImageButton.Attributes.Add("onclick",
                    "return LoadAdminName('" +SearchOnlineInterview_sessionAuthorNameHiddenField.ClientID + "','" +
                    SearchOnlineInterview_sessionAuthorIDHiddenField.ClientID + "','" +
                    SearchOnlineInterview_sessionAuthorNameTextBox.ClientID + "','TS')");

                // Assign handler to position profile selection popup.
                SearchOnlineInterview_positionProfileImageButton.Attributes.Add("onclick",
                    "return LoadPositionProfileName('" + SearchOnlineInterview_positionProfileTextBox.ClientID + "','"
                    + SearchOnlineInterview_positionProfileIDHiddenField.ClientID + "')");

                // Clear message.
                SearchOnlineInterview_topErrorMessageLabel.Text =
                    SearchOnlineInterview_topSuccessMessageLabel.Text = "";

                // Assign position profile.
                if (!IsPostBack)
                {
                    AssignPositionProfile();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchOnlineInterview_topErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// This button handler will get trigger on clicking the search button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchOnlineInterview_SearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear error message label
                SearchOnlineInterview_topErrorMessageLabel.Text = string.Empty;
                SearchOnlineInterview_positionProfileTextBox.Text =
                    Request[SearchOnlineInterview_positionProfileTextBox.UniqueID].ToString();

                // Reset the paging control.
                SearchOnlineInterview_bottomPagingNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1
                SearchInterviewTestSessionDetail(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchOnlineInterview_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the PageNumberClick event of the SearchOnlineInterview_pagingNavigator control.
        /// This binds the data into the grid for the pagenumber.
        /// </summary>
        /// <param name="sender">Source of the event.</param>
        /// <param name="e"></param>
        protected void SearchOnlineInterview_pagingNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                SearchInterviewTestSessionDetail(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchOnlineInterview_topErrorMessageLabel,
                    exception.Message);
                SearchOnlineInterview_topErrorMessageLabel.Text = exception.Message;
            }
        }

        /// <summary>
        /// Clicking on Reset button will trigger this method. 
        /// This clears all the informations and reset to empty.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchOnlineInterview_bottomReset_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }
        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method that assigns the position profile and loads the default 
        /// search results in the grid.
        /// </summary>
        private void AssignPositionProfile()
        {
            // Load the position profile name in the search criteria,
            // if position profile is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                return;

            // Get position profile ID.
            int positionProfileID = 0;
            if (int.TryParse(Request.QueryString["positionprofileid"].Trim(),
                out positionProfileID) == false)
            {
                return;
            }
            PositionProfileDetail positionProfile =
                new PositionProfileBLManager().GetPositionProfileInformation(positionProfileID);

            // Assign the position profile ID and name to the corresponding fields.
            SearchOnlineInterview_positionProfileTextBox.Text = positionProfile.PositionProfileName;
            SearchOnlineInterview_positionProfileIDHiddenField.Value = positionProfileID.ToString();

            // Apply the default search.
            // Clear error message label
            SearchOnlineInterview_topErrorMessageLabel.Text = string.Empty;

            if (Request[SearchOnlineInterview_positionProfileTextBox.UniqueID] != null)
            {
                SearchOnlineInterview_positionProfileTextBox.Text =
                    Request[SearchOnlineInterview_positionProfileTextBox.UniqueID].ToString();
            }

            // Reset the paging control.
            SearchOnlineInterview_bottomPagingNavigator.Reset();

            // By default search button click retrieves data for
            // page number 1
            SearchInterviewTestSessionDetail(1);

            // Update the search results update panel.
           // SearchCategory_searchResultsUpdatePanel.Update();
            //SearchOnlineInterview_updatePanel.Update();
        }

        /// <summary>
        /// This method creates an instance for search criteria and 
        /// passes to db to get the matched test sessions
        /// </summary>
        /// <param name="pageNumber"></param>
        private void SearchInterviewTestSessionDetail(int pageNumber)
        {
            OnlineInterviewSessionSearchCriteria onlineInterviewCriteria = new OnlineInterviewSessionSearchCriteria();
            onlineInterviewCriteria.Interviewkey = SearchOnlineInterview_interviewKeyTextBox.Text.Trim();
            onlineInterviewCriteria.InterviewName = SearchOnlineInterview_interviewNameTextBox.Text.Trim();
            
            // Since the session author textbox is readonly, get the text value using Request.
            //if (Request[SearchOnlineInterview_sessionAuthorNameTextBox.UniqueID] != null)
            //{
            //    SearchOnlineInterview_sessionAuthorNameTextBox.Text =
            //      Request[SearchOnlineInterview_sessionAuthorNameTextBox.UniqueID].Trim();
            //}
            onlineInterviewCriteria.InterviewCreator = SearchOnlineInterview_sessionAuthorNameTextBox.Text.Trim();

            onlineInterviewCriteria.PositionProfileID =
                (SearchOnlineInterview_positionProfileIDHiddenField.Value == null || SearchOnlineInterview_positionProfileIDHiddenField.Value.Trim().Length == 0) ? 0 :
                Convert.ToInt32(SearchOnlineInterview_positionProfileIDHiddenField.Value.Trim());

            // Since the SchedulerName textbox is readonly, get the text value using Request.
            if (Request[SearchOnlineInterview_schedulerNameTextBox.UniqueID] != null)
            {
                SearchOnlineInterview_schedulerNameTextBox.Text =
                    Request[SearchOnlineInterview_schedulerNameTextBox.UniqueID].Trim();
            }

            if (!Utility.IsNullOrEmpty(SearchOnlineInterview_sessionAuthorIDHiddenField.Value))
            {
                onlineInterviewCriteria.InterviewCreatorID = int.Parse(SearchOnlineInterview_sessionAuthorIDHiddenField.Value);
            }

            onlineInterviewCriteria.SearchTenantID = base.tenantID;

            int totalRecords = 0;

            List<OnlineInterviewSessionDetail> onlineInterview =
                new OnlineInterviewBLManager().GetOnlineInterviewDetail(onlineInterviewCriteria, pageNumber, base.GridPageSize,
                out totalRecords, ViewState["SORT_FIELD"].ToString(), ((SortType)ViewState["SORT_ORDER"]));

            // Bind the retrieved test sessions in grid.
            SearchOnlineInterview_interviewGridView.DataSource = onlineInterview;
            SearchOnlineInterview_interviewGridView.DataBind();
            if (onlineInterview == null)
            {
                SearchOnlineInterview_testDiv.Visible = false;
                base.ShowMessage(SearchOnlineInterview_topErrorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                SearchOnlineInterview_testDiv.Visible = true;
            }

            SearchOnlineInterview_bottomPagingNavigator.PageSize = base.GridPageSize;
            SearchOnlineInterview_bottomPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method used to set the author name and first name for the first time
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user id 
        /// </param>
        private void SetAuthorDetails()
        {
            //Get the user details from DB and assign it
            UserDetail userDetail = new QuestionBLManager().GetAuthorIDAndName(base.userID);

            if (userDetail == null)
                return;

            SearchOnlineInterview_schedulerIDHiddenField.Value ="0";
            //SearchOnlineInterview_sessionAuthorIDHiddenField.Value = base.userID.ToString();
            //SearchOnlineInterview_sessionAuthorNameTextBox.Text = userDetail.FirstName;
        }


        #endregion Private Methods

        #region Sort Columns Related Code

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchOnlineInterview_interviewGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                SearchOnlineInterview_bottomPagingNavigator.Reset();
                SearchInterviewTestSessionDetail(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchOnlineInterview_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchOnlineInterview_interviewGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchOnlineInterview_interviewGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchOnlineInterview_topErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchOnlineInterview_interviewGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchOnlineInterview_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Sort Columns Related Code

        #region Override Methods

        protected override bool IsValidData()
        {
            return true;
        }
        protected override void LoadValues()
        {
            //Do Nothing
        }
        #endregion
    }
}
