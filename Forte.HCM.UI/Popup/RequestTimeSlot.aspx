﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RequestTimeSlot.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.RequestTimeSlot"
    Title="Request Time Slot" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="RequestTimeSlot_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        function CollapseAllRows(targetControl)
        {
            var gridCtrl = document.getElementById("<%= RequestTimeSlot_timeSlotsGridView_resultsDiv.ClientID %>");
            if (gridCtrl != null)
            {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++)
                {
                    if (rowItems[indexRow].id.indexOf("RequestTimeSlot_timeSlotsGridView_detailsDiv") != "-1")
                    {
                        rowItems[indexRow].style.display = "none";
                    }
                }
            }

            return false;
        }

        function CloseRequestTimeSlotWindow()
        {
            var btncnrl = '<%= Request.QueryString["btncnrl"] %>';

            if (window.dialogArguments)
            {
                window.opener = window.dialogArguments;
            }

            // Trigger the click event of the refresh button in the parent page.
            if (btncnrl != null && window.opener.document.getElementById(btncnrl) != null)
                window.opener.document.getElementById(btncnrl).click();

            self.close();
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="td_padding_top_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="RequestTimeSlot_titleLiteral" runat="server">Request Time Slot</asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="RequestTimeSlot_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <table width="100%" cellpadding="0" cellspacing="3" border="0" class="popupcontrol_request_time_slot_bg">
                    <tr>
                        <td style="width: 100%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="td_height_20">
                                        <asp:UpdatePanel ID="RequestTimeSlot_messageUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="RequestTimeSlot_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknErrorMessage" Width="100%" Text=""></asp:Label>
                                                <asp:Label ID="RequestTimeSlot_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknSuccessMessage" Width="100%" Text=""></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="RequestTimeSlot_selectButton" />
                                                <asp:AsyncPostBackTrigger ControlID="RequestTimeSlot_resetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="RequestTimeSlot_addToRequestedTimeSlotsButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 100%">
                                        <asp:UpdatePanel ID="RequestTimeSlot_inputControlsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2" style="width: 100%" valign="middle" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="center" class="request_time_slot_details_bg" valign="middle">
                                                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td style="width: 13%" align="left" class="td_height_20" valign="middle">
                                                                                    <asp:Label ID="RequestTimeSlot_assessorNameLabel" runat="server" Text="Assessor"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 37%" align="left" valign="middle">
                                                                                    <asp:Label ID="RequestTimeSlot_assessorNameValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                        Text=""></asp:Label>
                                                                                </td>
                                                                                <td style="width: 13%" align="left" class="td_height_20" valign="middle">
                                                                                    <asp:Label ID="RequestTimeSlot_skillsLabel" runat="server" Text="Skills" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 37%" align="left" valign="middle">
                                                                                    <asp:Label ID="RequestTimeSlot_skillsValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                        Text=""></asp:Label>
                                                                                    <asp:HiddenField ID="RequestTimeSlot_assessorIDHiddenField" runat="server" />
                                                                                    <asp:HiddenField ID="RequestTimeSlot_skillIDHiddenField" runat="server" />
                                                                                    <asp:HiddenField ID="RequestTimeSlot_sessionKeyHiddenField" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 26%" valign="top" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="RequestTimeSlot_calendarLiteral" runat="server" Text="Calendar"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" class="request_time_slot_grid_body_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Calendar ID="RequestTimeSlot_selectDateCalendar" runat="server" BackColor="White"
                                                                                        BorderColor="Black" BorderWidth="1px" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black"
                                                                                        Height="168px" Width="170px" SelectionMode="Day" OtherMonthDayStyle-Font-Underline="false"
                                                                                        DayHeaderStyle-Font-Underline="false" ShowGridLines="false" DayHeaderStyle-Font-Overline="false"
                                                                                        DayStyle-Font-Underline="false" DayStyle-Font-Overline="false" Font-Underline="false"
                                                                                        TodayDayStyle-Font-Underline="false" OnPreRender="RequestTimeSlot_selectDateCalendar_PreRender"
                                                                                        OnSelectionChanged="RequestTimeSlot_selectDateCalendar_SelectionChanged" OnVisibleMonthChanged="RequestTimeSlot_selectDateCalendar_VisibleMonthChanged"
                                                                                        OnDayRender="RequestTimeSlot_selectDateCalendar_DayRender">
                                                                                        <DayStyle Font-Underline="false" Wrap="false" CssClass="no_underline" />
                                                                                        <SelectedDayStyle Font-Underline="false" BackColor="#59C1EE" ForeColor="Black"></SelectedDayStyle>
                                                                                        <OtherMonthDayStyle Font-Underline="false" BackColor="#EEEEEE" ForeColor="Black" />
                                                                                        <TodayDayStyle Font-Underline="false" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                                                                    </asp:Calendar>
                                                                                </td>
                                                                            </tr>
                                                                             <tr>
                                                                                <td style="height: 2px">
                                                                                </td>
                                                                            </tr>
                                                                             <tr>
                                                                                <td align="center" style="padding-left: 1px">
                                                                                    <table cellpadding="0" cellspacing="1" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td style="width: 10%; background-color: #59c1ee; height: 18px;" align="center">
                                                                                            </td>
                                                                                            <td style="width: 90%; padding-left: 4px" align="left">
                                                                                                <asp:Label ID="RequestTimeSlot_legendSelectedDateCaptionLabel" runat="server"
                                                                                                    Text="Selected Date" SkinID="sknLabelFieldLegendText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 10%; background-color: #8dde99; height: 18px" align="center">
                                                                                            </td>
                                                                                            <td style="width: 90%; padding-left: 4px" align="left">
                                                                                                <asp:Label ID="RequestTimeSlot_legendWithScheduleCaptionLabel" runat="server"
                                                                                                    Text="Dates With Schedule" SkinID="sknLabelFieldLegendText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 10%; background-color: #fa9476; height: 18px" align="center">
                                                                                            </td>
                                                                                            <td style="width: 90%; padding-left: 4px" align="left">
                                                                                                <asp:Label ID="RequestTimeSlot_legendVacationDatesCaptionLabel" runat="server"
                                                                                                    Text="Vacation Dates" SkinID="sknLabelFieldLegendText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 73%" valign="top" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="RequestTimeSlot_timeSlotsLiteral" runat="server" Text="Time Slots"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" class="request_time_slot_grid_body_bg">
                                                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <div style="height: 26px; overflow: auto;">
                                                                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td align="left">
                                                                                                    <asp:ImageButton ID="RequestTimeSlot_previousDate" runat="server" ToolTip="Go to the previous date"
                                                                                                        SkinID="sknPreviousDateImage" OnClick="RequestTimeSlot_previousDate_Click" Height="18px"
                                                                                                        Width="18px" />
                                                                                                </td>
                                                                                                <td align="center">
                                                                                                    <asp:Label ID="RequestTimeSlot_selectedDateLabel" runat="server" Text="" SkinID="sknLabelSelectedDateTextSmall"></asp:Label>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <asp:ImageButton ID="RequestTimeSlot_nextDate" runat="server" ToolTip="Go to the next date"
                                                                                                        SkinID="sknNextDateImage" OnClick="RequestTimeSlot_nextDate_Click" Height="18px"
                                                                                                        Width="18px" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="height: 202px; overflow: auto;" id="RequestTimeSlot_timeSlotsGridView_resultsDiv" runat="server">
                                                                                        <asp:GridView ID="RequestTimeSlot_timeSlotsGridView" runat="server" AllowSorting="True"
                                                                                            AutoGenerateColumns="False" Width="100%" OnRowCreated="RequestTimeSlot_timeSlotsGridView_RowCreated"
                                                                                            OnRowDataBound="RequestTimeSlot_timeSlotsGridView_RowDataBound" Height="100%">
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="RequestTimeSlot_timeSlotsGridView_timeSlotIDHiddenField" Value='<%# Eval("ID") %>'
                                                                                                            runat="server" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Skill">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:LinkButton ID="RequestTimeSlot_timeSlotsGridView_skillLinkButton" runat="server"
                                                                                                            PostBackUrl="#" Text='<%# Eval("Skill") %>' ToolTip="Click here to show/hide expanded view"></asp:LinkButton>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Initiated By">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="RequestTimeSlot_timeSlotsGridView_initiatedByLabel" runat="server"
                                                                                                            Text='<%# Eval("InitiatedByName") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Initiated Date">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="RequestTimeSlot_timeSlotsGridView_initiatedDateLabel" runat="server"
                                                                                                            Text='<%# Eval("InitiatedDateString") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Time Slot">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="RequestTimeSlot_timeSlotsGridView_timeSlotLabel" runat="server" Text='<%# Eval("TimeSlot") %>'></asp:Label>
                                                                                                        <tr>
                                                                                                            <td class="grid_padding_right" colspan="7" style="width: 100%">
                                                                                                                <div id="RequestTimeSlot_timeSlotsGridView_detailsDiv" runat="server" style="display: none;"
                                                                                                                    class="table_outline_bg_request_time_slot">
                                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                        <tr>
                                                                                                                            <td style="width: 17%; height: 20px" align="left" valign="top">
                                                                                                                                <asp:Label ID="RequestTimeSlot_timeSlotsGridView_sessionLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                                    Text="Session"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="width: 82%" align="left" valign="top">
                                                                                                                                <div style="height: 30px; overflow: auto;">
                                                                                                                                    <asp:Label ID="RequestTimeSlot_timeSlotsGridView_sessionValueLabel" runat="server"
                                                                                                                                        SkinID="sknLabelFieldText" Text='<%# Eval("SessionDesc") %>'></asp:Label>
                                                                                                                                </div>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="height: 6px">
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 17%; height: 20px" align="left" valign="top">
                                                                                                                                <asp:Label ID="RequestTimeSlot_timeSlotsGridView_commentsLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                                    Text="Comments"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="width: 82%" align="left" valign="top">
                                                                                                                                <div style="height: 30px; overflow: auto;">
                                                                                                                                    <asp:Label ID="RequestTimeSlot_timeSlotsGridView_commentsValueLabel" runat="server"
                                                                                                                                        SkinID="sknLabelFieldText" Text='<%# Eval("Comments") %>'></asp:Label>
                                                                                                                                </div>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </div>
                                                                                                                <%-- popup DIV --%>
                                                                                                                <a href="#RequestTimeSlot_timeSlotsGridView_detailsViewFocusMeLink" id="RequestTimeSlot_timeSlotsGridView_detailsViewFocusDownLink"
                                                                                                                    runat="server"></a><a href="#" id="RequestTimeSlot_timeSlotsGridView_detailsViewFocusMeLink"
                                                                                                                        runat="server"></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                            <EmptyDataTemplate>
                                                                                                <table style="width: 100%; height: 100%">
                                                                                                    <tr>
                                                                                                        <td style="height: 80px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="middle" align="center" style="height: 100%" class="error_message_text_normal">
                                                                                                            No time slots found to display
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </EmptyDataTemplate>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="RequestTimeSlot_summaryLiteral" runat="server" Text="Date Summary"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" class="request_time_slot_grid_body_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 68%; height: 23px" align="left" valign="middle">
                                                                                    <asp:Label ID="RequestTimeSlot_summaryDateLabel" runat="server" Text="Selected Date"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 31%" align="left" valign="middle">
                                                                                    <asp:Label ID="RequestTimeSlot_summaryDateValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                        Text=""></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 68%; height: 23px" align="left" valign="middle">
                                                                                    <asp:Label ID="RequestTimeSlot_totalScheduledTimeSlotsLabel" runat="server" Text="Total Scheduled Slots"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 31%" align="left" valign="middle">
                                                                                    <asp:Label ID="RequestTimeSlot_totalScheduledTimeSlotsValueLabel" runat="server"
                                                                                        SkinID="sknLabelFieldText" Text=""></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 68%; height: 23px" align="left" valign="middle">
                                                                                    <asp:Label ID="RequestTimeSlot_totalScheduledHoursLabel" runat="server" Text="Total Scheduled Hours"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 31%" align="left" valign="middle">
                                                                                    <asp:Label ID="RequestTimeSlot_totalScheduledHoursValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                                                        Text=""></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="RequestTimeSlot_addTimeSlotLiteral" runat="server" Text="Request Time Slot"></asp:Literal>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <asp:Button ID="RequestTimeSlot_addToRequestedTimeSlotsButton" runat="server" Text="Add To Requested Time Slots"
                                                                                        SkinID="sknButtonId" OnClick="RequestTimeSlot_addToRequestedTimeSlotsButton_Click"
                                                                                        ToolTip="Click here to add the entered time slot to the selected time slots" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" class="request_time_slot_grid_body_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 15%; height: 23px" align="left">
                                                                                    <asp:Label ID="RequestTimeSlot_fromLabel" runat="server" Text="From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    <span class="mandatory">*</span>
                                                                                </td>
                                                                                <td style="width: 34%" align="left">
                                                                                    <asp:DropDownList ID="RequestTimeSlot_fromDropDownList" runat="server" Width="90%"
                                                                                        AutoPostBack="true" OnSelectedIndexChanged="RequestTimeSlot_fromDropDownList_SelectedIndexChanged">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td style="width: 15%" align="right">
                                                                                    <asp:Label ID="RequestTimeSlot_toLabel" runat="server" Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label><span
                                                                                        class="mandatory">*</span>
                                                                                </td>
                                                                                <td style="width: 34%" align="right">
                                                                                    <asp:DropDownList ID="RequestTimeSlot_toDropDownList" runat="server" Width="90%"
                                                                                        AutoPostBack="true" OnSelectedIndexChanged="RequestTimeSlot_toDropDownList_SelectedIndexChanged">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 15%; height: 46px" align="left" valign="top">
                                                                                    <asp:Label ID="RequestTimeSlot_commentsLabel" runat="server" Text="Comments" Width="100%"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 84%" align="left" colspan="3" valign="middle">
                                                                                    <asp:TextBox ID="RequestTimeSlot_commentsTextBox" runat="server" MaxLength="200"
                                                                                        TextMode="MultiLine" Height="41px" Width="99%" onkeyup="CommentsCount(200,this)"
                                                                                        onchange="CommentsCount(200,this)"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="width: 100%" valign="top" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="RequestTimeSlot_selectedTimeSlotsLiteral" runat="server" Text="Requested Time Slots"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" class="request_time_slot_grid_body_bg">
                                                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="height: 114px; overflow: auto;">
                                                                                        <asp:GridView ID="RequestTimeSlot_selectedTimeSlotsGridView" runat="server" AllowSorting="False"
                                                                                            AutoGenerateColumns="False" Width="100%" OnRowDataBound="RequestTimeSlot_selectedTimeSlotsGridView_RowDataBound"
                                                                                            OnRowCommand="RequestTimeSlot_selectedTimeSlotsGridView_RowCommand" Height="100%">
                                                                                            <Columns>
                                                                                                <asp:TemplateField HeaderStyle-Width="5%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="RequestTimeSlot_selectedTimeSlotsGridView_deleteTimeSlotImageButton"
                                                                                                            runat="server" SkinID="sknDeletePositionProfile" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                            ToolTip="Delete Time Slot" CssClass="showCursor" CommandName="DeleteTimeSlot" />&nbsp;
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="25%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="RequestTimeSlot_selectedTimeSlotsGridView_dateLabel" runat="server"
                                                                                                            Text='<%# Eval("AvailabilityDateString") %>' Width="100%"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Time Slot" HeaderStyle-Width="25%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="RequestTimeSlot_selectedTimeSlotsGridView_timeSlotLabel" runat="server"
                                                                                                            Text='<%# Eval("TimeSlot") %>' Width="100%"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Comments" HeaderStyle-Width="48%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="RequestTimeSlot_selectedTimeSlotsGridView_commentsLabel" runat="server"
                                                                                                            Text='<%# Eval("Comments") %>' Width="100%"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                            <EmptyDataTemplate>
                                                                                                <table style="width: 100%; height: 100%">
                                                                                                    <tr>
                                                                                                        <td style="height: 40px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="middle" align="center" style="height: 100%" class="error_message_text_normal">
                                                                                                            No time slots selected
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </EmptyDataTemplate>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="RequestTimeSlot_selectButton" />
                                                <asp:AsyncPostBackTrigger ControlID="RequestTimeSlot_resetLinkButton" />
                                                <asp:AsyncPostBackTrigger ControlID="RequestTimeSlot_addToRequestedTimeSlotsButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="RequestTimeSlot_buttonControlsUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="left">
                                    <asp:Button ID="RequestTimeSlot_selectButton" runat="server" Text="Select" SkinID="sknButtonId"
                                        ToolTip="Click here accept the selected time slots and close the window" />
                                    <asp:Button ID="RequestTimeSlot_saveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                        ToolTip="Click here save the selected time slots and close the window" OnClick="RequestTimeSlot_saveButton_Click" />
                                </td>
                                <td valign="middle" align="left">
                                    &nbsp;<asp:LinkButton ID="RequestTimeSlot_resetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                        Text="Reset" OnClick="RequestTimeSlot_resetButton_Click" ToolTip="Click here to reset the window" />
                                </td>
                                <td class="pop_divider_line" valign="middle">
                                    |
                                </td>
                                <td valign="middle" align="left">
                                    <asp:LinkButton ID="RequestTimeSlot_cancelLinkButton" runat="server" Text="Cancel"
                                        SkinID="sknPopupLinkButton" OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window"> </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_2">
            </td>
        </tr>
    </table>
</asp:Content>
