﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.Admin {
    
    
    public partial class AddCertificate {
        
        /// <summary>
        /// AddCertificate_isMaximizedHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField AddCertificate_isMaximizedHiddenField;
        
        /// <summary>
        /// AddCertificate_headerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal AddCertificate_headerLiteral;
        
        /// <summary>
        /// AddCertificate_closeImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton AddCertificate_closeImageButton;
        
        /// <summary>
        /// AddCertificate_successMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label AddCertificate_successMessageLabel;
        
        /// <summary>
        /// AddCertificate_errorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label AddCertificate_errorMessageLabel;
        
        /// <summary>
        /// AddCertificate_certificateNameHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label AddCertificate_certificateNameHeadLabel;
        
        /// <summary>
        /// AddCertificate_certificateNameTextbox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox AddCertificate_certificateNameTextbox;
        
        /// <summary>
        /// AddCertificate_descriptionHeadLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label AddCertificate_descriptionHeadLabel;
        
        /// <summary>
        /// AddCertificate_descriptionTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox AddCertificate_descriptionTextBox;
        
        /// <summary>
        /// AddCertificate_certificateBackgroundLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label AddCertificate_certificateBackgroundLabel;
        
        /// <summary>
        /// AddCertificate_certificateBackgroundFileUpload control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.FileUpload AddCertificate_certificateBackgroundFileUpload;
        
        /// <summary>
        /// BatchQuestionEntry_helpImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton BatchQuestionEntry_helpImageButton;
        
        /// <summary>
        /// AddCertificate_uploadButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button AddCertificate_uploadButton;
        
        /// <summary>
        /// AddCertificate_availableBackgroundLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton AddCertificate_availableBackgroundLinkButton;
        
        /// <summary>
        /// AddCertificate_htmlTextboxDIV control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl AddCertificate_htmlTextboxDIV;
        
        /// <summary>
        /// AddCertificate_htmlTextbox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox AddCertificate_htmlTextbox;
        
        /// <summary>
        /// AddCertificate_previewButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button AddCertificate_previewButton;
        
        /// <summary>
        /// AddCertificate_previewDIV control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl AddCertificate_previewDIV;
        
        /// <summary>
        /// AddCertificate_submitButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button AddCertificate_submitButton;
        
        /// <summary>
        /// AddCertificate_cancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton AddCertificate_cancelLinkButton;
        
        /// <summary>
        /// AddCertificate_previewCertificatesPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel AddCertificate_previewCertificatesPanel;
        
        /// <summary>
        /// AddCertificate_previewCertificates_Button control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button AddCertificate_previewCertificates_Button;
        
        /// <summary>
        /// AddCertificate_previewPanel_headerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal AddCertificate_previewPanel_headerLiteral;
        
        /// <summary>
        /// AddCertificate_previewPanel_closeImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton AddCertificate_previewPanel_closeImageButton;
        
        /// <summary>
        /// AddCertificate_availableBackgroundTextbox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox AddCertificate_availableBackgroundTextbox;
        
        /// <summary>
        /// AddCertificate_previewCertificates_cancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton AddCertificate_previewCertificates_cancelLinkButton;
        
        /// <summary>
        /// AddCertificate_previewCertificates_modalpPopupExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender AddCertificate_previewCertificates_modalpPopupExtender;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.PopupMaster Master {
            get {
                return ((Forte.HCM.UI.MasterPages.PopupMaster)(base.Master));
            }
        }
    }
}
