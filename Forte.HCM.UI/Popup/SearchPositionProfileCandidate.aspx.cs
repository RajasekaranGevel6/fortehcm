﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchPositionProfileCandidate.aspx.cs
// File that represents the SearchUser class that defines the user interface
// layout and functionalities for the SearchPositionProfileCandidate page. This 
// page helps in searching for candidates assoicated with a position profile by
// providing search criteria for user name, first name, middle name and
// last name. This class inherits Forte.HCM.UI.Common.PageBase class.

#endregion

#region Directives                                                             

using System;
using System.Text;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

using Resources;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the SearchPositionProfileCandidate page. This page helps in 
    /// searching for candidates assoicated with a position profile by
    /// providing search criteria for user name, first name, middle name and
    /// last name. This class inherits the Forte.HCM.UI.Common.PageBase 
    /// class.
    /// </summary>
    public partial class SearchPositionProfileCandidate : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "274px";

        #endregion Private Constants

        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SearchPositionProfileCandidate_successMessageLabel.Text = "";
                SearchPositionProfileCandidate_errorMessageLabel.Text = "";
                SearchPositionProfileCandidate_createUserNameErrorMessageLabel.Text = "";
                SearchPositionProfileCandidate_createUserNameSuccessMessageLabel.Text = "";

                // Set browser title.
                Master.SetPageCaption("Search Candidate");

                // Set default button and focus.
                Page.Form.DefaultButton = SearchPositionProfileCandidate_searchButton.UniqueID;

                // Subscribes to the page number click event.
                SearchPositionProfileCandidate_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchPositionProfileCandidate_pageNavigator_PageNumberClick);

                // 
                if (!Utility.IsNullOrEmpty(SearchPositionProfileCandidate_isMaximizedHiddenField.Value) &&
                    SearchPositionProfileCandidate_isMaximizedHiddenField.Value == "Y")
                {
                    SearchPositionProfileCandidate_searchCriteriasDiv.Style["display"] = "none";
                    SearchPositionProfileCandidate_searchResultsUpSpan.Style["display"] = "block";
                    SearchPositionProfileCandidate_searchResultsDownSpan.Style["display"] = "none";
                    SearchPositionProfileCandidate_testDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchPositionProfileCandidate_searchCriteriasDiv.Style["display"] = "block";
                    SearchPositionProfileCandidate_searchResultsUpSpan.Style["display"] = "none";
                    SearchPositionProfileCandidate_searchResultsDownSpan.Style["display"] = "block";
                    SearchPositionProfileCandidate_testDiv.Style["height"] = RESTORED_HEIGHT;
                }

                if (!IsPostBack)
                {
                    // Set default focus.
                    Page.Form.DefaultFocus = SearchPositionProfileCandidate_userNameTextBox.UniqueID;
                    SearchPositionProfileCandidate_userNameTextBox.Focus();

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "USERNAME";

                    SearchPositionProfileCandidate_testDiv.Visible = false;

                    List<AttributeDetail> attributes =  new AttributeBLManager().
                        GetAttributesByType(Constants.AttributeTypes.CANDIDATE_SESSION_STATUS, "A");
                    SearchPositionProfileCandidate_statusDropDownList.DataSource = attributes;
                    SearchPositionProfileCandidate_statusDropDownList.DataBind();
                    SearchPositionProfileCandidate_statusDropDownList.DataTextField = "AttributeName";
                    SearchPositionProfileCandidate_statusDropDownList.DataValueField = "AttributeID";
                    SearchPositionProfileCandidate_statusDropDownList.DataBind();
                    SearchPositionProfileCandidate_statusDropDownList.Items.Insert
                        (0, new ListItem("--Select--", string.Empty));

                    // Load position profile detail.
                    LoadPositionProfileDetail();
                }
                else
                {
                    SearchPositionProfileCandidate_testDiv.Visible = true;
                }

                SearchPositionProfileCandidate_searchTestResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    SearchPositionProfileCandidate_testDiv.ClientID + "','" +
                    SearchPositionProfileCandidate_searchCriteriasDiv.ClientID + "','" +
                    SearchPositionProfileCandidate_searchResultsUpSpan.ClientID + "','" +
                    SearchPositionProfileCandidate_searchResultsDownSpan.ClientID + "','" +
                    SearchPositionProfileCandidate_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchPositionProfileCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchPositionProfileCandidate_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "USERNAME";

                // Reset the paging control.
                SearchPositionProfileCandidate_pageNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchPositionProfileCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void SearchPositionProfileCandidate_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchPositionProfileCandidate_userNameTextBox.Text = string.Empty;
                SearchPositionProfileCandidate_emailTextBox.Text = string.Empty;

                SearchPositionProfileCandidate_testGridView.DataSource = null;
                SearchPositionProfileCandidate_testGridView.DataBind();

                SearchPositionProfileCandidate_pageNavigator.PageSize = base.GridPageSize;
                SearchPositionProfileCandidate_pageNavigator.TotalRecords = 0;
                SearchPositionProfileCandidate_testDiv.Visible = false;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "USERNAME";

                // Reset to the restored state.
                SearchPositionProfileCandidate_searchCriteriasDiv.Style["display"] = "block";
                SearchPositionProfileCandidate_searchResultsUpSpan.Style["display"] = "none";
                SearchPositionProfileCandidate_searchResultsDownSpan.Style["display"] = "block";
                SearchPositionProfileCandidate_testDiv.Style["height"] = RESTORED_HEIGHT;
                SearchPositionProfileCandidate_isMaximizedHiddenField.Value = "N";

                // Set default focus.
                Page.Form.DefaultFocus = SearchPositionProfileCandidate_userNameTextBox.UniqueID;
                SearchPositionProfileCandidate_userNameTextBox.Focus();

                // Set the status dropdown to default selected.
                SearchPositionProfileCandidate_statusDropDownList.SelectedIndex = 0;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchPositionProfileCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchPositionProfileCandidate_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                Search(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchPositionProfileCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchPositionProfileCandidate_testGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                SearchPositionProfileCandidate_pageNavigator.Reset();
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchPositionProfileCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchPositionProfileCandidate_testGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                // Set row styles.
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                ImageButton SearchPositionProfileCandidate_candidateDetailGridView_addNotesImageButton = e.Row.FindControl
                    ("SearchPositionProfileCandidate_candidateDetailGridView_addNotesImageButton") as ImageButton;

                ImageButton SearchPositionProfileCandidate_candidateDetailGridView_viewCandidateActivityLogImageButton = e.Row.FindControl
                   ("SearchPositionProfileCandidate_candidateDetailGridView_viewCandidateActivityLogImageButton") as ImageButton;

                int candidateID = Convert.ToInt32((e.Row.FindControl("SearchPositionProfileCandidate_candidateDetailGridView_candidateIDHiddenField") as HiddenField).Value);

                // Assign events to 'add notes' icon.
                SearchPositionProfileCandidate_candidateDetailGridView_addNotesImageButton.Attributes.Add("OnClick",
                   "javascript:return OpenAddNotesPopup('" + candidateID + "')");

                // Assign events to 'view candidate activity log' icon.
                SearchPositionProfileCandidate_candidateDetailGridView_viewCandidateActivityLogImageButton.Attributes.Add
                    ("OnClick", "javascript:return OpenViewCandidateActivityLopPopup('" + candidateID + "')");

                HyperLink SearchPositionProfileCandidate_viewCandidateActivityDashboardHyperLink = (HyperLink)e.Row.FindControl("SearchPositionProfileCandidate_viewCandidateActivityDashboardHyperLink");
                if (SearchPositionProfileCandidate_viewCandidateActivityDashboardHyperLink != null)
                {
                    SearchPositionProfileCandidate_viewCandidateActivityDashboardHyperLink.NavigateUrl = "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&candidateid=" + candidateID;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchPositionProfileCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchPositionProfileCandidate_testGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                int sortColumnIndex = GetSortColumnIndex
                    (SearchPositionProfileCandidate_testGridView,
                    (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchPositionProfileCandidate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when any user action is performed 
        /// on a row.
        /// </remarks>
        protected void SearchPositionProfileCandidate_testGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "AddCandidateID")
                {
                    SearchPositionProfileCandidate_createUserNamePanel_userNameTextBox.Text = string.Empty;

                    SearchPositionProfileCandidate_createUserNamePanel_passwordTextBox.Text = string.Empty;

                    SearchPositionProfileCandidate_createUserNamePanel_retypePasswordTextBox.Text = string.Empty;

                    GridViewRow candidateGridViewRow = (GridViewRow)(((ImageButton)e.CommandSource).Parent.Parent);

                    SearchPositionProfileCandidate_createUserNamePanel_firstNameHiddenField.Value =
                        ((Label)candidateGridViewRow.FindControl("SearchPositionProfileCandidate_candidateDetailGridView_firstNameLabel")).Text;

                    SearchPositionProfileCandidate_createUserNamePanel_lastNameHiddenField.Value =
                       ((Label)candidateGridViewRow.FindControl("SearchPositionProfileCandidate_candidateDetailGridView_lastNameLabel")).Text;

                    SearchPositionProfileCandidate_createUserNamePanel_candidateIDHiddenField.Value =
                       ((HiddenField)candidateGridViewRow.FindControl("SearchPositionProfileCandidate_candidateDetailGridView_candidateIDHiddenField")).Value;

                    SearchPositionProfileCandidate_createUserNamePanel_userNameTextBox.Text =
                      ((Label)candidateGridViewRow.FindControl("SearchPositionProfileCandidate_candidateDetailGridView_emailLabel")).Text;

                    SearchPositionProfileCandidate_createUserModalPopupExtender.Show();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfileCandidate_errorMessageLabel,
                     exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save button is present
        /// in the create user panel.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will create the candidate login credentials.
        /// </remarks>
        protected void SearchPositionProfileCandidate_createUserNamePanel_saveButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                if (CheckCandidateUserDetails())
                {
                    if (!string.IsNullOrEmpty
                       (SearchPositionProfileCandidate_createUserNamePanel_passwordTextBox.Text.Trim()) &&
                       (!string.IsNullOrEmpty
                       (SearchPositionProfileCandidate_createUserNamePanel_retypePasswordTextBox.Text.Trim())))
                    {
                        if (SearchPositionProfileCandidate_createUserNamePanel_passwordTextBox.Text.Trim() !=
                            SearchPositionProfileCandidate_createUserNamePanel_retypePasswordTextBox.Text.Trim())
                        {
                            base.ShowMessage(SearchPositionProfileCandidate_createUserNameErrorMessageLabel,
                               HCMResource.SearchCandidateRepository_EnterCorrectPassword);
                            SearchPositionProfileCandidate_createUserModalPopupExtender.Show();

                            return;
                        }
                        if (!IsValidEmailAddress(SearchPositionProfileCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
                        {
                            base.ShowMessage(SearchPositionProfileCandidate_createUserNameErrorMessageLabel,
                                    HCMResource.NewCandidate_EnterValidEmailID);
                            SearchPositionProfileCandidate_createUserModalPopupExtender.Show();
                            return;
                        }


                        if (!CheckForEmailAddressAvailability
                            (SearchPositionProfileCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
                        {
                            base.ShowMessage(SearchPositionProfileCandidate_createUserNameErrorMessageLabel,
                            HCMResource.UserRegistration_UserEmailNotAvailable);
                            SearchPositionProfileCandidate_createUserNamePanel_userEmailAvailableStatusDiv.Style.Add
                                ("display", "block");
                            SearchPositionProfileCandidate_createUserModalPopupExtender.Show();
                            return;
                        }
                        SearchPositionProfileCandidate_pageNavigator.Reset();
                        SaveUserName();

                        ViewState["SORT_FIELD"] = "CREATED_DATE";
                        ViewState["SORT_ORDER"] = SortType.Descending;
                        Search(1);

                        base.ShowMessage(SearchPositionProfileCandidate_successMessageLabel,
                            HCMResource.SearchCandidateRepository_UserNameCreatedSuccessfully);
                    }
                }
                else
                {
                    base.ShowMessage(SearchPositionProfileCandidate_createUserNameErrorMessageLabel,
                            HCMResource.SearchCandidateRepository_EnterMandatoryField);
                    SearchPositionProfileCandidate_createUserModalPopupExtender.Show();
                    return;
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfileCandidate_errorMessageLabel,
                     exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the availability link is 
        /// clicked on the email in the create user panel.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will check if the entered email is available or not
        /// </remarks>
        protected void SearchPositionProfileCandidate_createUserNamePanel_checkUserEmailIdAvailableButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(SearchPositionProfileCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
                {
                    if (!IsValidEmailAddress(SearchPositionProfileCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
                    {
                        base.ShowMessage(SearchPositionProfileCandidate_createUserNameErrorMessageLabel,
                                HCMResource.NewCandidate_EnterValidEmailID);
                        SearchPositionProfileCandidate_createUserModalPopupExtender.Show();
                        return;
                    }

                    if (CheckForEmailAddressAvailability(SearchPositionProfileCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
                        SearchPositionProfileCandidate_createUserNamePanel_validEmailAvailableStatus.Text = Resources.HCMResource.UserRegistration_UserEmailAvailable;
                    else
                        SearchPositionProfileCandidate_createUserNamePanel_inValidEmailAvailableStatus.Text = HCMResource.UserRegistration_UserEmailNotAvailable;
                    SearchPositionProfileCandidate_createUserNamePanel_userEmailAvailableStatusDiv.Style.Add("display", "block");

                    SearchPositionProfileCandidate_createUserModalPopupExtender.Show();
                }
                else
                {
                    base.ShowMessage(SearchPositionProfileCandidate_createUserNameErrorMessageLabel,
                                HCMResource.SearchCandidateRepository_PleaseEnterTheUsername);
                    SearchPositionProfileCandidate_createUserModalPopupExtender.Show();
                    return;
                }

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfileCandidate_errorMessageLabel,
                     exception.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that search for candidates for the given page number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void Search(int pageNumber)
        {
            int totalRecords = 0;

            List<UserDetail> users = new CommonBLManager().GetPositionProfileCandidates
                (SearchPositionProfileCandidate_userNameTextBox.Text.Trim(), SearchPositionProfileCandidate_nameTextBox.Text.Trim(), SearchPositionProfileCandidate_nameTextBox.Text.Trim(),
                SearchPositionProfileCandidate_emailTextBox.Text.Trim(),
                base.tenantID,
                Request.QueryString["positionprofileid"] == null ? 0 : Convert.ToInt32(Request.QueryString["positionprofileid"]),
                SearchPositionProfileCandidate_statusDropDownList.SelectedValue,
                SearchPositionProfileCandidate_showMyCandidateOnlyCheckBox.Checked ? base.userID : 0,
                ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]),
                pageNumber,
                base.GridPageSize,
                out totalRecords);

            if (users == null || users.Count == 0)
            {
                SearchPositionProfileCandidate_testDiv.Visible = false;
                ShowMessage(SearchPositionProfileCandidate_errorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                SearchPositionProfileCandidate_testDiv.Visible = true;
            }

            SearchPositionProfileCandidate_testGridView.DataSource = users;
            SearchPositionProfileCandidate_testGridView.DataBind();
            SearchPositionProfileCandidate_pageNavigator.PageSize = base.GridPageSize;
            SearchPositionProfileCandidate_pageNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that generates a random string that can be used as 
        /// encrypted value for confirmation code.
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that holds the encrypted string
        /// </returns>
        private string GetConfirmationCode()
        {
            return new EncryptAndDecrypt().EncryptString(RandomString(15));
        }

        /// <summary>
        /// Method that checks if the email is already available.
        /// </summary>
        /// <param name="emailID">
        /// A <see cref="string"/> that holds the email ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. Returns true if 
        /// available else returns false.
        /// </returns>
        private bool CheckForEmailAddressAvailability(string emailID)
        {
            return new UserRegistrationBLManager().CheckUserEmailExists(emailID, base.tenantID);
        }

        /// <summary>
        /// Method that checks if the email is valid or not.
        /// </summary>
        /// <param name="emailID">
        /// A <see cref="string"/> that holds the email ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. Returns true if 
        /// valid else returns false.
        /// </returns>
        private bool IsValidEmailAddress(string emailID)
        {
            //Regex regex = new Regex(".+@.+\\.[a-zA-Z]+", RegexOptions.Compiled);
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$", RegexOptions.Compiled);
            return regex.IsMatch(emailID);
        }

        /// <summary>
        /// Method that checks the candidate user details.
        /// </summary>
        /// <returns></returns>
        private bool CheckCandidateUserDetails()
        {
            bool value = true;

            if (string.IsNullOrEmpty(SearchPositionProfileCandidate_createUserNamePanel_userNameTextBox.Text.Trim()))
            {
                value = false;
            }
            if (string.IsNullOrEmpty(SearchPositionProfileCandidate_createUserNamePanel_passwordTextBox.Text.Trim()))
            {
                value = false;
            }
            if (string.IsNullOrEmpty(SearchPositionProfileCandidate_createUserNamePanel_retypePasswordTextBox.Text.Trim()))
            {
                value = false;
            }
            return value;
        }

        /// <summary>
        /// Method that loads the position profile detail.
        /// </summary>
        private void LoadPositionProfileDetail()
        {
            // Check if position profile ID parameter is null.
            if (Request.QueryString["positionprofileid"] == null)
                return;

            // Get position profile ID from the query string.
            int positionProfileID = Convert.ToInt32(Request.QueryString["positionprofileid"]);

            // Check if position profile ID parameter is zero.
            if (positionProfileID == 0)
                return;

            PositionProfileDetail positionProfileDetail = new PositionProfileBLManager().
                GetPositionProfileKeyWord(positionProfileID);

            // Check if position profile object is null.
            if (Support.Utility.IsNullOrEmpty(positionProfileDetail))
                return;

            // Set the position profile name into the textbox.
            SearchPositionProfileCandidate_positionProfileNameValueLabel.Text = positionProfileDetail.PositionProfileName;
        }

        /// <summary>
        /// Method that saves the name of the user.
        /// </summary>
        private void SaveUserName()
        {
            CandidateInformation candidateInformation = new CandidateInformation();

            candidateInformation.caiFirstName = SearchPositionProfileCandidate_createUserNamePanel_firstNameHiddenField.Value;
            candidateInformation.caiLastName = SearchPositionProfileCandidate_createUserNamePanel_lastNameHiddenField.Value;
            candidateInformation.caiID = int.Parse(
           SearchPositionProfileCandidate_createUserNamePanel_candidateIDHiddenField.Value);
            candidateInformation.UserName = SearchPositionProfileCandidate_createUserNamePanel_userNameTextBox.Text.Trim();
            candidateInformation.Password = new EncryptAndDecrypt().EncryptString(SearchPositionProfileCandidate_createUserNamePanel_passwordTextBox.Text.Trim());
            string confirmationCode = GetConfirmationCode();
            new ResumeRepositoryBLManager().InsertCandidateUsers(base.tenantID, candidateInformation,
                base.userID, confirmationCode);
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }

        #endregion Private Methods

        #region Protected Methods                                              

        /// <summary>
        /// Gets the visibility.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        protected bool GetVisibility(string username)
        {
            return username.Length == 0 ? true : false;
        }

        /// <summary>
        /// Method that retrieves the select icon visibility status.
        /// </summary>
        /// <param name="username">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that hold the visibility status. Returns if 
        /// is user name is present else returns false.
        /// </returns>
        protected bool GetSelectIconVisibility(string username)
        {
            return username.Length == 0 ? false : true;
        }

        #endregion Protected Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}

