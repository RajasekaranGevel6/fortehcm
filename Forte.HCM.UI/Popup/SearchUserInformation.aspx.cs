﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchUser.aspx.cs
// File that represents the SearchUser class that defines the user interface
// layout and functionalities for the SearchUser page. This page helps in 
// searching for users by providing search criteria for user name, first name, 
// middle name and last name This class inherits Forte.HCM.UI.Common.PageBase
// class.

#endregion

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the SearchUser page. This page helps in searching for users by
    /// providing search criteria for user name, first name, middle name and
    /// last name. This class inherits the Forte.HCM.UI.Common.PageBase 
    /// class.
    /// </summary>
    public partial class SearchUserInformation : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Search User");
                // Set default button and focus.
                Page.Form.DefaultButton = SearchUserInformation_searchButton.UniqueID;

                // Subscribes to the page number click event.
                SearchUserInformation_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchUserInformation_pageNavigator_PageNumberClick);

                if (!Utility.IsNullOrEmpty(SearchUserInformation_isMaximizedHiddenField.Value) &&
                    SearchUserInformation_isMaximizedHiddenField.Value == "Y")
                {
                    SearchUserInformation_searchCriteriasDiv.Style["display"] = "none";
                    SearchUserInformation_searchResultsUpSpan.Style["display"] = "block";
                    SearchUserInformation_searchResultsDownSpan.Style["display"] = "none";
                    SearchUserInformation_testDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchUserInformation_searchCriteriasDiv.Style["display"] = "block";
                    SearchUserInformation_searchResultsUpSpan.Style["display"] = "none";
                    SearchUserInformation_searchResultsDownSpan.Style["display"] = "block";
                    SearchUserInformation_testDiv.Style["height"] = RESTORED_HEIGHT;
                }

                // This validation is done for focusing the linkbutton of the input
                // fields such as category, subject, test area.
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchUserInformation_browserHiddenField.Value))
                {
                    ValidateEnterKey(SearchUserInformation_browserHiddenField.Value.Trim());
                    SearchUserInformation_browserHiddenField.Value = string.Empty;
                }

                if (!IsPostBack)
                {
                    // Set default focus.
                    Page.Form.DefaultFocus = SearchUserInformation_userNameTextBox.UniqueID;
                    SearchUserInformation_userNameTextBox.Focus();

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "USERNAME";

                    SearchUserInformation_testDiv.Visible = false;

                    // Select the 'User Type' as 'Subscription' by default and make it disabled
                    // when launched from 'Feature Usage' page.
                    if (Request.QueryString["launchedfrom"] != null && 
                        Request.QueryString["launchedfrom"].ToLower() == "featureusage")
                    {
                        SearchUserInformation_userTypeDropDownList.SelectedValue = "S";
                        SearchUserInformation_userTypeDropDownList.Enabled = false;
                    }
                }
                else
                {
                    SearchUserInformation_testDiv.Visible = true;
                }

                SearchUserInformation_searchTestResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    SearchUserInformation_testDiv.ClientID + "','" +
                    SearchUserInformation_searchCriteriasDiv.ClientID + "','" +
                    SearchUserInformation_searchResultsUpSpan.ClientID + "','" +
                    SearchUserInformation_searchResultsDownSpan.ClientID + "','" +
                    SearchUserInformation_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchUserInformation_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchUserInformation_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                SearchUserInformation_errorMessageLabel.Text = string.Empty;
                SearchUserInformation_successMessageLabel.Text = string.Empty;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "USERNAME";

                // Reset the paging control.
                SearchUserInformation_pageNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchUserInformation_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void SearchUserInformation_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchUserInformation_userNameTextBox.Text = string.Empty;
                SearchUserInformation_firstNameTextBox.Text = string.Empty;
                SearchUserInformation_lastNameTextBox.Text = string.Empty;

                SearchUserInformation_testGridView.DataSource = null;
                SearchUserInformation_testGridView.DataBind();

                SearchUserInformation_pageNavigator.PageSize = base.GridPageSize;
                SearchUserInformation_pageNavigator.TotalRecords = 0;
                SearchUserInformation_testDiv.Visible = false;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "USERNAME";

                // Reset to the restored state.
                SearchUserInformation_searchCriteriasDiv.Style["display"] = "block";
                SearchUserInformation_searchResultsUpSpan.Style["display"] = "none";
                SearchUserInformation_searchResultsDownSpan.Style["display"] = "block";
                SearchUserInformation_testDiv.Style["height"] = RESTORED_HEIGHT;
                SearchUserInformation_isMaximizedHiddenField.Value = "N";

                // Clear message fields.
                SearchUserInformation_successMessageLabel.Text = string.Empty;
                SearchUserInformation_errorMessageLabel.Text = string.Empty;

                // Set default focus.
                Page.Form.DefaultFocus = SearchUserInformation_userNameTextBox.UniqueID;
                SearchUserInformation_userNameTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchUserInformation_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchUserInformation_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                Search(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchUserInformation_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchUserInformation_testGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                SearchUserInformation_pageNavigator.Reset();
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchUserInformation_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchUserInformation_testGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchUserInformation_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchUserInformation_testGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchUserInformation_testGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchUserInformation_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods                                                

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void Search(int pageNumber)
        {
            int totalRecords = 0;

            List<UserDetail> users = new CommonBLManager().GetUserInformation
                (SearchUserInformation_userNameTextBox.Text.Trim(),
                SearchUserInformation_firstNameTextBox.Text.Trim(),
                SearchUserInformation_userTypeDropDownList.SelectedValue.ToString().Trim(),
                SearchUserInformation_lastNameTextBox.Text.Trim(),
                ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]),
                pageNumber,
                base.GridPageSize, Request.QueryString["rolecodes"],
                out totalRecords);

            if (users == null || users.Count == 0)
            {
                SearchUserInformation_testDiv.Visible = false;
                ShowMessage(SearchUserInformation_errorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                SearchUserInformation_testDiv.Visible = true;
            }

            SearchUserInformation_testGridView.DataSource = users;
            SearchUserInformation_testGridView.DataBind();
            SearchUserInformation_pageNavigator.PageSize = base.GridPageSize;
            SearchUserInformation_pageNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that will call when the page gets loaded. This will perform
        /// to call the event handlers based parameter.
        /// </summary>
        /// <param name="stringValue">
        /// A <see cref="string"/> that contains the string.
        /// </param>
        private void ValidateEnterKey(string stringValue)
        {
            switch (stringValue.Trim())
            {
                case "dummy":
                    foreach (GridViewRow row in SearchUserInformation_testGridView.Rows)
                    {
                        LinkButton SearchUserInformation_selectLinkButton = (LinkButton)
                            row.FindControl("SearchUserInformation_selectLinkButton");

                        System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "OnSelectClickMethod",
                            "javascript:OnSelectClick('" + SearchUserInformation_selectLinkButton.ClientID + "');", true);
                    }
                    break;
            }
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}

