﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateActivitySummary.aspx.cs
// File that represents the CandidateActivitySummary class that 
// defines the user interface layout and functionalities for the view 
// candidate activity summary page. This page helps to view the list of 
// activity logs for a candidate that are associated with the specific 
// position profile.

#endregion Header

#region Directives                                                             

using System;
using System.Data;
using System.Web.UI;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

using OfficeOpenXml;
using System.Web;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the CandidateActivitySummary class that defines
    /// the user interface layout and functionalities for the view candidate
    /// activity summary page. This page helps to view the list of activity 
    /// logs for a candidate that are associated with the specific position
    /// profile. This class inherits Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class CandidateActivitySummary : PageBase
    {
        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Candidate Activity Summary");

                // Load the skill values.
                if (!IsPostBack)
                {
                    LoadValues();
                }

                // Check if post back is raised due after adding notes.
                if (CandidateActivitySummary_refreshHiddenField.Value != null &&
                    CandidateActivitySummary_refreshHiddenField.Value.Trim().ToUpper() == "Y")
                {
                    // Reset the flag value.
                    CandidateActivitySummary_refreshHiddenField.Value = "N";

                    // Refresh activities.
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(CandidateActivitySummary_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the download link is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will export the activity log to an excel file.
        /// </remarks>
        protected void CandidateActivitySummary_downloadLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                CandidateActivitySummary_errorMessageLabel.Text = string.Empty;
                CandidateActivitySummary_successMessageLabel.Text = string.Empty;

                // Check if position profile ID is given.
                if (Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                {
                    ShowMessage(CandidateActivitySummary_errorMessageLabel,
                        "Position profile ID is not present");
                    return;
                }

                // Check if position profile is valid number.
                int positionProfileID = 0;
                if (int.TryParse(Request.QueryString["positionprofileid"].Trim(), 
                    out positionProfileID) == false)
                {
                    ShowMessage(CandidateActivitySummary_errorMessageLabel,
                        "Position profile ID is invalid");
                    return;
                }

                // Check if candidate ID is given.
                if (Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
                {
                    ShowMessage(CandidateActivitySummary_errorMessageLabel,
                        "Candidate ID is not present");
                    return;
                }

                // Check if candidate is valid number.
                int candidateID = 0;
                if (int.TryParse(Request.QueryString["candidateid"].Trim(), out candidateID) == false)
                {
                    ShowMessage(CandidateActivitySummary_errorMessageLabel,
                        "Candidate ID is invalid");
                    return;
                }

                // Get candidate activity summary table.
                DataTable table = new PositionProfileBLManager().
                    GetCandidateActivitySummaryTable(positionProfileID, candidateID);

                // Check if table is not null and contain rows.
                if (table == null || table.Rows.Count == 0)
                {
                    ShowMessage(CandidateActivitySummary_errorMessageLabel,
                       "No activities found to download");
                    return;
                }

                // Remove the unncessary columns from the table.
                table.Columns.Remove("ACTIVITY_BY");
                table.Columns.Remove("ACTIVITY_TYPE");

                // Change the column names.
                table.Columns["ACTIVITY_DATE"].ColumnName = "Activity Date";
                table.Columns["ACTIVITY_BY_NAME"].ColumnName = "Activity By";
                table.Columns["ACTIVITY_TYPE_NAME"].ColumnName = "Activity";
                table.Columns["COMMENTS"].ColumnName = "Comments";

                using (ExcelPackage pck = new ExcelPackage())
                {
                    // Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add
                        (Constants.ExcelExportSheetName.CANDIDATE_ACTIVITY_LOG);

                    // Load the datatable into the sheet, starting from the 
                    // cell A1 and print the column names on row 1.
                    ws.Cells[Constants.ExcelExport.STARTING_CELL].LoadFromDataTable(table, true);

                    string decimalFormat = Constants.ExcelExportFormats.DECIMAL_COMMA;

                    foreach (DataColumn column in table.Columns)
                    {
                        if (column.DataType == typeof(System.DateTime))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                            {
                                col.Style.Numberformat.Format = Constants.ExcelExportFormats.DATE;
                            }
                        }
                        else if (column.DataType == typeof(decimal))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                            {
                                if (decimalFormat == "C")
                                {
                                    for (int row = 2; row <= ws.Dimension.End.Row; row++)
                                    {
                                        ws.Cells[row, column.Ordinal + 1].Value =
                                            ws.Cells[row, column.Ordinal + 1].Value != null ? ws.Cells[row, column.Ordinal + 1].Value.ToString().Replace(".", ",") : string.Empty;
                                    }
                                    col.Style.Numberformat.Format = "0\",\"0000";
                                }
                                else
                                    col.Style.Numberformat.Format = "###0.0000";
                            }
                        }
                    }

                    // Construct file name.
                    string fileName = string.Format("{0}_{1}_{2}.xlsx",
                        Constants.ExcelExportFileName.CANDIDATE_ACTIVITY_LOG,
                        Utility.GetValidExportFileNameString(CandidateActivitySummary_positionProfileNameValueLabel.Text, 50),
                        Utility.GetValidExportFileNameString(CandidateActivitySummary_candidateNameValueLabel.Text, 50));

                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                    Response.BinaryWrite(pck.GetAsByteArray());
                    Response.End();
                    //HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(CandidateActivitySummary_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the add notes link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will show the add notes popup.
        /// </remarks>
        protected void CandidateActivitySummary_addNotesLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                CandidateActivitySummary_errorMessageLabel.Text = string.Empty;
                CandidateActivitySummary_successMessageLabel.Text = string.Empty;

                // Check if position profile ID is given.
                if (Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                {
                    ShowMessage(CandidateActivitySummary_errorMessageLabel,
                        "Position profile ID is not present");
                    return;
                }

                // Check if position profile is valid number.
                int positionProfileID = 0;
                if (int.TryParse(Request.QueryString["positionprofileid"].Trim(), out positionProfileID) == false)
                {
                    ShowMessage(CandidateActivitySummary_errorMessageLabel,
                        "Position profile ID is invalid");
                    return;
                }

                // Check if candidate ID is given.
                if (Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
                {
                    ShowMessage(CandidateActivitySummary_errorMessageLabel,
                        "Candidate ID is not present");
                    return;
                }

                // Check if candidate is valid number.
                int candidateID = 0;
                if (int.TryParse(Request.QueryString["candidateid"].Trim(), out candidateID) == false)
                {
                    ShowMessage(CandidateActivitySummary_errorMessageLabel,
                        "Candidate ID is invalid");
                    return;
                }

                // Add the handler to the add notes link.
                ScriptManager.RegisterStartupScript(this, this.GetType(), 
                    "OpenPositionProfileAddNotesPopupWithRefresh",
                    "javascript: OpenPositionProfileAddNotesPopupWithRefresh('" +
                    positionProfileID + "','" + candidateID + "')", true);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(CandidateActivitySummary_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the refresh link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will refresh the page.
        /// </remarks>
        protected void CandidateActivitySummary_refreshLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Refresh activities.
                LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(CandidateActivitySummary_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden protected method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
            // Check if position profile ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
            {
                ShowMessage(CandidateActivitySummary_errorMessageLabel, 
                    "Position profile ID is not present");
                return;
            }

            // Check if position profile is valid number.
            int positionProfileID = 0;
            if (int.TryParse(Request.QueryString["positionprofileid"].Trim(), out positionProfileID) == false)
            {
                ShowMessage(CandidateActivitySummary_errorMessageLabel, 
                    "Position profile ID is invalid");
                return;
            }

            // Check if candidate ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["candidateid"]))
            {
                ShowMessage(CandidateActivitySummary_errorMessageLabel, 
                    "Candidate ID is not present");
                return;
            }

            // Check if candidate is valid number.
            int candidateID = 0;
            if (int.TryParse(Request.QueryString["candidateid"].Trim(), out candidateID) == false)
            {
                ShowMessage(CandidateActivitySummary_errorMessageLabel, 
                    "Candidate ID is invalid");
                return;
            }

            // Get candidate activity summary.
            CandidateActivitySummaryDetail logDetail = new PositionProfileBLManager().
                GetCandidateActivitySummary(positionProfileID, candidateID);

            if (logDetail == null)
            {
                // This will never happen
                return;
            }

            // Assign header detail.
            CandidateActivitySummary_positionProfileNameValueLabel.Text = logDetail.PositionProfileName;
            CandidateActivitySummary_candidateNameValueLabel.Text = logDetail.CandidateName;

            // Assign activities.
            CandidateActivitySummary_activityLogDetailGridView.DataSource = logDetail.Activities;
            CandidateActivitySummary_activityLogDetailGridView.DataBind();

            if (logDetail.Activities == null || logDetail.Activities.Count == 0)
            {
                ShowMessage(CandidateActivitySummary_errorMessageLabel,
                    "No activities found to display");
                return;
            }
        }

        #endregion Protected Overridden Methods
    }
}
