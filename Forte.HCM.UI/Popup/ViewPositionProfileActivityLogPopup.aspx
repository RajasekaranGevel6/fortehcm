﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewPositionProfileActivityLogPopup.aspx.cs" MasterPageFile="~/MasterPages/PopupMaster.Master"
Inherits="Forte.HCM.UI.Popup.ViewPositionProfileActivityLogPopup"  %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PositionProfileViewerControl.ascx" TagName="PositionProfileViewerControl"
    TagPrefix="uc1" %>
<asp:Content ID="ViewPositonProfileActivityLogPopup_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="ViewPositonProfileActivityLogPopup_headerLiteral" runat="server" Text="View Position Profile Activity Log "></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="ViewPositonProfileActivityLogPopup_topCancelImageButton" runat="server"
                                SkinID="sknCloseImageButton" OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="ViewPositonProfileActivityLogPopup_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="ViewPositonProfileActivityLogPopup_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <uc1:PositionProfileViewerControl ID="ViewPositonProfileActivityLogPopup_PositionProfileViewerControl"
                    runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_left_8">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="ViewPositonProfileActivityLogPopup_downloadLinkButton" runat="server"
                                Text="Download" SkinID="sknPopupLinkButton" 
                                ToolTip="Click here to download the activity log"                                
                                OnClick="ViewPositonProfileActivityLogPopup_downloadLinkButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="ViewPositonProfileActivityLogPopup_resetLinkButton" runat="server" ToolTip="Click here to reset the window"
                                SkinID="sknPopupLinkButton" Text="Reset" OnClick="ViewPositonProfileActivityLogPopup_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="ViewPositonProfileActivityLogPopup_cancelLinkButton" runat="server" ToolTip="Click here to close the window"
                                Text="Cancel" SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="ViewPositonProfileActivityLogPopup_isMaximizedHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>