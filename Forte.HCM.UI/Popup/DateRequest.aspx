﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/PopupMaster.Master"
    AutoEventWireup="true" CodeBehind="DateRequest.aspx.cs" Inherits="Forte.HCM.UI.Popup.DateRequest" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PopupMaster_contentPlaceHolder"
    runat="server">
    <script type="text/javascript" language="javascript">
        function CloseAssessorRequestWindow() {
            var btncnrl = '<%= Request.QueryString["btncnrl"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Trigger the click event of the refresh button in the parent page.
            if (btncnrl != null && window.opener.document.getElementById(btncnrl) != null)
                window.opener.document.getElementById(btncnrl).click();

            self.close();
        }

        function ShowTimeSlotDiv(lnk) {
            var slotDiv = document.getElementById("TimeSlotDiv");
            if (slotDiv != null) {
                if (slotDiv.style.display == 'none') {
                    slotDiv.style.display = 'block';
                    lnk.title = 'Click here to collapse the time slot.';
                }
                else {
                    slotDiv.style.display = 'none';
                    lnk.title = 'Click here to expand the time slot.';
                }

            }
            return false;
        }

        function ShowInterviewDiv(lnk) {
            var InterviewDiv = document.getElementById("InterviewDiv");
            if (InterviewDiv != null) {
                if (InterviewDiv.style.display == 'none') {
                    InterviewDiv.style.display = 'block';
                    lnk.title = 'Click here to collapse the assigned interviews.';
                }
                else {
                    InterviewDiv.style.display = 'none';
                    lnk.title = 'Click here to expand the assigned interviews.';
                }

            }
            return false;
        }
    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="OnlineInterview_assessorCalendar_titleLiteral" runat="server" Text="Request Date"></asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="OnlineInterview_assessorCalendar_topCancelImageButton" runat="server"
                                SkinID="sknCloseImageButton" OnClientClick="javascript:CloseAssessorRequestWindow();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" align="center" valign="top">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="popupcontrol_view_assessor_profile_bg"
                    style="height: 530px">
                    <tr>
                        <td style="padding: 10px" align="center" valign="top">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td  colspan ="3" valign="top">
                                        <asp:UpdatePanel ID="DateRequest_messageUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="DateRequest_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknErrorMessage" Width="100%" Text=""></asp:Label>
                                                <asp:Label ID="DateRequest_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknSuccessMessage" Width="100%" Text=""></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="OnlineInterview_assessorCalendar_saveButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan ="3" align="left" valign="top">
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td style ="padding:4px;text-align:left">
                                                        <asp:Label ID="OnlineInterview_assessorCalendar_Input_option1" runat="server" SkinID="sknHomePageLabel" Text="1. Select a date first "></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style ="padding:4px;text-align:left">
                                                        <asp:Label ID="OnlineInterview_assessorCalendar_Input_option2" runat="server" SkinID="sknHomePageLabel" Text="2. Select one or more timeslots "></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                     <td style ="padding:4px;text-align:left">
                                                        <asp:Label ID="OnlineInterview_assessorCalendar_Input_option3" runat="server" SkinID="sknHomePageLabel" Text="3. Click on Request Timeslot to send out a requisition to the assessor "></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr><td style ="padding:4px;text-align:left"></td></tr>
                                            </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="25%" valign="top" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td colspan="2">
                                                    <asp:UpdatePanel ID="OnlineInterview_assessorCalendar_CalendarUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Calendar ID="OnlineInterview_assessorCalendar_Calendar" runat="server" BackColor="White"
                                                                BorderColor="Black" BorderWidth="1px" Font-Names="Verdana" Font-Size="10pt" ForeColor="Black"
                                                                Height="200px" Width="200px" SelectionMode="Day" OtherMonthDayStyle-Font-Underline="false"
                                                                DayHeaderStyle-Font-Underline="false" ShowGridLines="false" DayHeaderStyle-Font-Overline="false"
                                                                DayStyle-Font-Underline="false" DayStyle-Font-Overline="false" Font-Underline="false"
                                                                TodayDayStyle-Font-Underline="false" OnDayRender="OnlineInterview_assessorCalendar_Calendar_DayRender"
                                                                OnSelectionChanged="OnlineInterview_assessorCalendar_Calendar_SelectionChanged"
                                                                OnVisibleMonthChanged="OnlineInterview_assessorCalendar_Calendar_VisibleMonthChanged">
                                                                <DayStyle Font-Underline="false" Wrap="false" CssClass="no_underline" />
                                                                <SelectedDayStyle Font-Underline="false" BackColor="#59C1EE" ForeColor="Black"></SelectedDayStyle>
                                                                <OtherMonthDayStyle Font-Underline="false" BackColor="#EEEEEE" ForeColor="Black" />
                                                                <TodayDayStyle Font-Underline="false" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                                            </asp:Calendar>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="width: 15px; height: 15px; background-color: #E1D455" title="Request Initiated Dates">
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:Label ID="DateRequest_intiatedDateLabel" runat="server" Text="Request Initiated Dates"
                                                        SkinID="sknHomePageLabel"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="width: 15px; height: 15px; background-color: #7092BE" title="Available Dates">
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:Label ID="DateRequest_approvedDateLabel" runat="server" Text="Available Dates"
                                                        SkinID="sknHomePageLabel"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="width: 15px; height: 15px; background-color: #99DD96" title="Dates With Schedule">
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:Label ID="DateRequest_scheduleDateLabel" runat="server" Text="Dates With Schedule"
                                                        SkinID="sknHomePageLabel"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="width: 15px; height: 15px; background-color: #F29378" title="Vacation Dates">
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:Label ID="DateRequest_vacationDateLabel" runat="server" Text="Vacation Dates"
                                                        SkinID="sknHomePageLabel"></asp:Label>
                                                    <asp:HiddenField ID="DateRequest_genIdHiddenField" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="15px">
                                        &nbsp;
                                    </td>
                                    <td valign="top" align="left">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                           <%-- <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td valign="top" class="popup_td_padding_2" colspan="2">
                                                    <asp:UpdatePanel ID="DateRequest_displayAvailableTimeUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <div id="DateRequest_displayAvailableTimeDiv" runat="server">
                                                                <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="header_bg" align="center">
                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                <tr>
                                                                                    <td align="left" class="header_text_bold">
                                                                                        <asp:Literal ID="DateRequest_addTimeSlotLiteral" runat="server" Text="Select Available Time"></asp:Literal>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" class="request_time_slot_grid_body_bg">
                                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <asp:CheckBoxList ID="DateRequest_availableTimeCheckBoxList" Width="100%" RepeatColumns="3"
                                                                                            runat="server" Enabled="false">
                                                                                        </asp:CheckBoxList>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="OnlineInterview_assessorCalendar_Calendar" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" valign="top">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_text" valign="middle" align="left" style="padding-top:15px"> 
                                                    <a onclick="return ShowTimeSlotDiv(this);" title="Click here to expand the time slot." style="cursor:pointer">Time Slots</a>
                                                </td>
                                                <td align="right">
                                                    <asp:Button ID="OnlineInterview_assessorCalendar_saveButton" runat="server" Text="Request Timeslot"
                                                        SkinID="sknButtonId" OnClick="OnlineInterview_assessorCalendar_saveButton_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left">
                                                    <asp:UpdatePanel ID="DateRequest_assessorTimeSlotUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <div style="overflow: auto; height: 200px;display:none" id="TimeSlotDiv">
                                                                <asp:GridView ID="AssessorTimeSlot_PopupGridView" runat="server" SkinID="sknWrapHeaderGrid"
                                                                    AutoGenerateColumns="False">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="From">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="OnlineInterview_assessorTimeSlot_GridView_PopupFromTimeLabel" runat="server"
                                                                                    SkinID="sknHomePageLabel" Text='<%# Eval("TimeSlotTextFrom") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="To">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="OnlineInterview_assessorTimeSlot_GridView_PopupToTimeLabel" runat="server"
                                                                                    SkinID="sknHomePageLabel" Text='<%# Eval("TimeSlotTextTo") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Status">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="OnlineInterview_assessorTimeSlot_GridView_PopupStatusLabel" runat="server"
                                                                                    SkinID="sknHomePageLabel" Text='<%# Eval("RequestStatus") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="OnlineInterview_assessorCalendar_Calendar" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_text" valign="middle" align="left"  style="padding-top:15px"> 
                                                    <a  onclick="return ShowInterviewDiv(this);" title="Click here to expand the assigned interviews." style="cursor:pointer">Assigned Interviews</a>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:UpdatePanel ID="DateRequest_scheduledInterview_GridViewUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <div style="overflow: auto; height: 200px;display:none" id="InterviewDiv">
                                                                <asp:GridView ID="DateRequest_scheduledInterview_GridView" runat="server" SkinID="sknWrapHeaderGrid"
                                                                    AutoGenerateColumns="False" Width="100%">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Interview Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="DateRequest_scheduledInterview_GridView_InterviewName_Label" runat="server"
                                                                                    Text='<%# Eval("InterviewName")  %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Initiated By">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="DateRequest_scheduledInterview_GridView_InitiatedBy_Label" runat="server"
                                                                                    Text='<%# Eval("TestAuthor")  %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="From">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="DateRequest_scheduledInterview_GridView_InitiatedBy_Label" runat="server"
                                                                                    Text='<%# Eval("TimeSlotTextFrom")  %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="To">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="DateRequest_scheduledInterview_GridView_InitiatedBy_Label" runat="server"
                                                                                    Text='<%# Eval("TimeSlotTextTo")  %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="OnlineInterview_assessorCalendar_Calendar" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_18">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_2" align="center">
            </td>
        </tr>
        <tr valign="bottom">
            <td align="left" style="text-align: left; padding-left: 12px">
                <asp:LinkButton ID="OnlineInterview_assessorCalendar_cancelLinkButton" runat="server"
                    Text="Cancel" SkinID="sknCancelLinkButton" OnClientClick="javascript:self.close();"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_2" align="left">
            </td>
        </tr>
    </table>
</asp:Content>
