﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CategorySubjectLookup.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.CategorySubjectLookup"
    Title="Search Category/Subject" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="CategorySubjectLookup_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl)
        {
            var ctrlId = '<%= Request.QueryString["ctrlcid"] %>';
            var ctrlsid = '<%= Request.QueryString["ctrlsid"] %>';
            var ctrlhiddid = '<%= Request.QueryString["ctrlhiddid"] %>';
            var ctrlhidcatname = '<%= Request.QueryString["ctrlhidcatname"] %>';
            var ctrlhidsubname = '<%= Request.QueryString["ctrlhidsubname"] %>';
            var btncnrl = '<%= Request.QueryString["ctrlbtn"] %>';
            var ctrlskill = '<% = Request.QueryString["ctrhiddenskill"]  %>'
            
            if (window.dialogArguments)
            {
                window.opener = window.dialogArguments;
            }
            if (ctrlId != null && ctrlId != '') {
             
                window.opener.document.getElementById(ctrlId).value
                    = document.getElementById(ctrl.id.replace("CategorySubjectLookup_resultsGridViewSelectButton", "CategorySubjectLookup_resultsGridViewCategoryNameHiddenField")).value;

                window.opener.document.getElementById(ctrlsid).value
                    = document.getElementById(ctrl.id.replace("CategorySubjectLookup_resultsGridViewSelectButton", "CategorySubjectLookup_resultsGridViewSubjectNameHiddenField")).value;

                if (ctrlskill != null && ctrlskill != '') {
                    
                    /* var grid = document.getElementById("<%=CategorySubjectLookup_resultsGridView.ClientID%>");

                    var inputs = grid.getElementsByTagName("input");
                    var TargetCheckBoxChildControl = "CategorySubjectLookup_resultsGridViewCheckBox";

                    var TargetHiddenChildControl = "CategorySubjectLookup_resultsGridViewSubjectNameHiddenField";

                    var skillval = "";
                    var singleval = "";

                    if (grid.rows.length > 0)  
                    {
                    for (Row = 1; Row < grid.rows.length; Row++) 
                    {
                    cell = grid.rows[Row].cells[0];
                    for (j = 0; j < cell.childNodes.length; j++) { 
                                            
                    if (cell.childNodes[j].type == "checkbox" && cell.childNodes[j].checked) {

                    for (k = 0; k < cell.childNodes.length; k++) {
                    if (cell.childNodes[k].type == "hidden" && cell.childNodes[k].id.indexOf(TargetHiddenChildControl, 0) >= 0) {
                    singleval = cell.childNodes[k].value +",";
                    skillval += singleval;
                    }
                    }
                    }
                    }
                    }
                    } */
                    if (document.getElementById("<%=CategorySubjectLookup_selectedSubjectHiddenField.ClientID%>") == null) return;
                    
                    var subjectHiddenField = document.getElementById("<%=CategorySubjectLookup_selectedSubjectHiddenField.ClientID%>")
                    
                    window.opener.document.getElementById(ctrlskill).value = subjectHiddenField.value;
                }
                 

                window.opener.document.getElementById(ctrlhiddid).value
                    = document.getElementById(ctrl.id.replace("CategorySubjectLookup_resultsGridViewSelectButton", "CategorySubjectLookup_resultsGridViewSubjectIDHiddenField")).value;

                if (ctrlhidcatname != null && ctrlhidcatname != '' && ctrlhidcatname != 'undefined')
                {
                    window.opener.document.getElementById(ctrlhidcatname).value
                        = document.getElementById(ctrl.id.replace("CategorySubjectLookup_resultsGridViewSelectButton", "CategorySubjectLookup_resultsGridViewCategoryNameHiddenField")).value;
                }
                if (ctrlhidsubname != null && ctrlhidsubname != '' && ctrlhidsubname != 'undefinded')
                {
                    window.opener.document.getElementById(ctrlhidsubname).value
                        = document.getElementById(ctrl.id.replace("CategorySubjectLookup_resultsGridViewSelectButton", "CategorySubjectLookup_resultsGridViewSubjectNameHiddenField")).value;
                }
            }

            // Trigger the click event of the refresh button in the parent page.
            if (btncnrl != null && window.opener.document.getElementById(btncnrl) != null)
                window.opener.document.getElementById(btncnrl).click();

            self.close();
        }

        function SelectSkills(ctrl) {
            var skill = ctrl.value.trim();
            
            skill = skill + ",";
            var subjectHiddenField = document.getElementById("<%=CategorySubjectLookup_selectedSubjectHiddenField.ClientID%>")

            if (subjectHiddenField != null)
                subjectHiddenField.value += skill; 
        }
    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey" align="left">
                            <asp:Literal ID="CategorySubjectLookup_headerLiteral" runat="server" Text="Search Category / Subject"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="CategorySubjectLookup_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="msg_align">
                            <asp:Label ID="CategorySubjectLookup_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="CategorySubjectLookup_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr id="CategorySubjectLookup_searchCriteriaTR" runat="server">
                        <td id="Td2" align="center" runat="server">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="CategorySubjectLookup_searchCriteriasDiv" runat="server" style="display: block;">
                                <table width="100%" border="0" cellspacing="3" cellpadding="2">
                                    <tr>
                                        <td>
                                            <asp:Label ID="CategorySubjectLookup_categoryNameLabel" runat="server" Text="Category Name"
                                                SkinID="sknLabelText"></asp:Label>
                                        </td>
                                        <td>
                                            <div style="display: none">
                                                <asp:TextBox ID="CategorySubjectLookup_categoryIDTextBox" runat="server" MaxLength="11"></asp:TextBox>
                                                <ajaxToolKit:FilteredTextBoxExtender ID="CategorySubjectLookup_categoryIDFileteredTextBoxExtender"
                                                    runat="server" TargetControlID="CategorySubjectLookup_categoryIDTextBox" FilterType="Numbers">
                                                </ajaxToolKit:FilteredTextBoxExtender>
                                            </div>
                                            <asp:TextBox ID="CategorySubjectLookup_categoryNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="CategorySubjectLookup_subjectNameLabel" runat="server" Text="Subject Name"
                                                SkinID="sknLabelText"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="CategorySubjectLookup_subjectNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                            <div style="display: none">
                                                <asp:TextBox ID="CategorySubjectLookup_subjectIDTextBox" runat="server" MaxLength="11"></asp:TextBox>
                                                <ajaxToolKit:FilteredTextBoxExtender ID="CategorySubjectLookup_subjectIDFilteredTextBoxExtender"
                                                    runat="server" TargetControlID="CategorySubjectLookup_subjectIDTextBox" FilterType="Numbers">
                                                </ajaxToolKit:FilteredTextBoxExtender>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="right">
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="CategorySubjectLookup_searchButton" runat="server" Text="Search"
                                                OnClick="CategorySubjectLookup_searchButton_Click" SkinID="sknButtonId" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="popup_td_padding_10">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr id="CategorySubjectLookup_searchTestResultsTR" runat="server">
                                    <td id="Td1" class="header_bg" align="center" runat="server">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 50%" align="left">
                                                    <asp:Literal ID="CategorySubjectLookup_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>
                                                    &nbsp;<asp:Label ID="CategorySubjectLookup_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                        Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                </td>
                                                <td style="width: 50%" align="right">
                                                    <span id="CategorySubjectLookup_searchResultsUpSpan" style="display: none;" runat="server">
                                                        <asp:Image ID="CategorySubjectLookup_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                    </span><span id="CategorySubjectLookup_searchResultsDownSpan" style="display: block;"
                                                        runat="server">
                                                        <asp:Image ID="CategorySubjectLookup_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="CategorySubjectLookup_updatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="height: 225px; width: 610px; overflow: auto;" runat="server" id="CategorySubjectLookup_testDiv">
                                                                         <asp:HiddenField ID="CategorySubjectLookup_selectedSubjectHiddenField" runat="server" />
                                                                            <asp:GridView ID="CategorySubjectLookup_resultsGridView" runat="server" AutoGenerateColumns="False"
                                                                                Width="100%" OnRowCreated="CategorySubjectLookup_resultsGridView_RowCreated"
                                                                                OnSorting="CategorySubjectLookup_resultsGridView_Sorting" AllowSorting="true"
                                                                                OnRowDataBound="CategorySubjectLookup_resultsGridView_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="left">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="CategorySubjectLookup_resultsGridViewSelectButton" runat="server"
                                                                                                OnClientClick="javascript:return OnSelectClick(this);" Text="Select"></asp:LinkButton>

                                                                                           <input type="checkbox" id="CategorySubjectLookup_resultsGridViewInput" runat="server" name="CategorySubjectLookup_resultsGridViewInput" 
                                                                                                value='<%# Eval("SubjectName") %>' onclick="javascript:return SelectSkills(this);" visible="false"  /> 

                                                                                            <asp:HiddenField ID="CategorySubjectLookup_resultsGridViewCategoryNameHiddenField"
                                                                                                runat="server" Value='<%# Eval("CategoryName") %>' />
                                                                                            <asp:HiddenField ID="CategorySubjectLookup_resultsGridViewSubjectNameHiddenField"
                                                                                                runat="server" Value='<%# Eval("SubjectName") %>' />
                                                                                            <asp:HiddenField ID="CategorySubjectLookup_resultsGridViewSubjectIDHiddenField" runat="server"
                                                                                                Value='<%# Eval("SubjectID") %>' /> 
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="Category ID" DataField="CategoryID" SortExpression="CATEGORYID" Visible="false"/>
                                                                                    <asp:BoundField HeaderText="Category Name" DataField="CategoryName" SortExpression="CATEGORYNAME" />
                                                                                    <asp:BoundField HeaderText="Subject ID" DataField="SubjectID" SortExpression="SUBJECTID" Visible="false"/>
                                                                                    <asp:BoundField HeaderText="Subject Name" DataField="SubjectName" SortExpression="SUBJECTNAME" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <uc1:PageNavigator ID="CategorySubjectLookup_pagingNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_right_10">
                <table border="0" cellpadding="0" cellspacing="0" align="left">
                    <tr>
                        <td  class="td_padding_5">
                            <asp:Button ID="CategorySubjectLookup_OkButton" runat="server" Text="Select"
                                                OnClientClick="javascript:return OnSelectClick(this);" SkinID="sknButtonId" />
                        </td>
                        <td class="pop_divider_line">|</td>
                        <td>
                            <asp:LinkButton ID="CategorySubjectLookup_resetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="CategorySubjectLookup_resetLinkButton_Click"></asp:LinkButton>
                        </td>
                        <td class="pop_divider_line">
                            |
                        </td>
                        <td class="td_padding_5">
                            <asp:LinkButton ID="CategorySubjectLookup_cancelButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField runat="server" ID="CategorySubjectLookup_isMaximizedHiddenField" />
            </td>
        </tr>
    </table>
</asp:Content>
