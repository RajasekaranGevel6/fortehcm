﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Forte.HCM.UI.Common;
using Forte.HCM.Support;
using Forte.HCM.Trace;
using Forte.HCM.DataObjects;
using System.IO;
using Forte.HCM.ExternalService;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Forte.HCM.UI.Popup
{
    public partial class ReportEmailAttachment : PageBase
    {
        #region Event Handlers

        /// <summary>
        /// This event handler helps to set default button, focus and page title.
        /// Also, it retrieves the currently logged in user email address by calling
        /// BL method then, admin email address too.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Report Email Attachment");
                // Set default button field
                Page.Form.DefaultButton = EmailAttachment_submitButton.UniqueID;

                // Validate email address
                EmailAttachment_submitButton.Attributes.Add("onclick", "javascript:return IsValidEmail('"
                    + EmailAttachment_ccTextBox.ClientID + "','"
                    + EmailAttachment_errorMessageLabel.ClientID + "','"
                    + EmailAttachment_successMessageLabel.ClientID + "');");

                if (!IsPostBack)
                {
                    // Set default focus field
                    Page.Form.DefaultFocus = EmailAttachment_toTextBox.UniqueID;
                    EmailAttachment_toTextBox.Focus();
                    UserDetail userDetail = new UserDetail();
                    userDetail = (UserDetail)Session["USER_DETAIL"];
                    EmailAttachment_fromValueLabel.Text = userDetail.FirstName +","+ userDetail.LastName
                        + " &lt;" + userDetail.UserName+ "&gt;";
                    EmailAttachment_adminUserIdHiddenField.Value = userDetail.UserName;
                }
                EmailAttachment_attachmentImageButton.ToolTip = Request.QueryString["filename"];
                EmailAttachment_attachmentImageButton.Attributes.Add("onclick", "javascript:return OpenPopUp('" + Request.QueryString["filename"]+ "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EmailAttachment_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This event handler helps to send an alert email
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EmailAttachment_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear all the error and success labels
                EmailAttachment_errorMessageLabel.Text = string.Empty;
                EmailAttachment_successMessageLabel.Text = string.Empty;
                string fileName = string.Empty;

                // Validate the fields
                if (!IsValidData())
                    return;

                // Check if the querystrings are not empty.

                fileName = Request.QueryString["filename"];

                // Build Mail Details
                MailDetail mailDetail = new MailDetail();
                MailAttachment mailAttachment = new MailAttachment();
                mailDetail.Attachments = new List<MailAttachment>();
                mailDetail.To = new List<string>();
                mailDetail.CC = new List<string>();

                byte[] attachementContent = (byte[])Session[Constants.SessionConstants.REPORT_RESULTS_PDF];
                mailAttachment.FileContent = attachementContent;  
                mailAttachment.FileName = fileName;
                mailDetail.Attachments.Add(mailAttachment);
                mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
               

                // Add 'To' mail addresses
                string[] toMailIDs = EmailAttachment_toTextBox.Text.Split(new char[] { ',' });
                for (int idx = 0; idx < toMailIDs.Length; idx++)
                    mailDetail.To.Add(toMailIDs[idx].Trim());

                if (!Utility.IsNullOrEmpty(EmailAttachment_ccTextBox.Text.Trim()))
                    mailDetail.CC.Add(EmailAttachment_ccTextBox.Text.Trim());

                mailDetail.From = EmailAttachment_adminUserIdHiddenField.Value.Trim();
                mailDetail.Subject = EmailAttachment_subjectTextBox.Text.Trim();
                bool isMailSent = false;
                try
                {
                    // Send mail
                    new EmailHandler().SendMail(mailDetail);
                    isMailSent = true;
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    isMailSent = false;
                }

                  //Close the popup window
                    string closeScript = "self.close();";
                if (isMailSent)
                    //base.ShowMessage(EmailAttachment_successMessageLabel,
                    //    Resources.HCMResource.Report_Common_Mail_successfully);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true); 
                else
                    base.ShowMessage(EmailAttachment_errorMessageLabel,
                        Resources.HCMResource.EmailAttachment_MailCouldNotSend);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EmailAttachment_errorMessageLabel, exp.Message);
            }
            finally
            {
              //  Session["CAND_SESSION_KEY"] = null;
             //   Session["ATTEMPT_ID"] = null;
            }
        }

        #endregion Event Handlers

        #region Protected Methods

        /// <summary>
        /// This override method helps to validate input fields
        /// </summary>
        /// <returns>Returns FALSE if the condition meets TRUE</returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            if (EmailAttachment_toTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(EmailAttachment_errorMessageLabel,
                    Resources.HCMResource.EmailAttachment_ToAddressCannotBeEmpty);
                isValidData = false;
            }
            else
            {
                string[] toMailIDs = EmailAttachment_toTextBox.Text.Split(new char[] { ',' });

                for (int idx = 0; idx < toMailIDs.Length; idx++)
                {
                    if (!Regex.IsMatch(
                        toMailIDs[idx].Trim(), @"^([\w-]+\.)*?[\w-]+@[\w-]+\.([\w-]+\.)*?[\w]+$"))
                    {
                        base.ShowMessage(EmailAttachment_errorMessageLabel,
                            Resources.HCMResource.EmailAttachment_InvalidEmailAddress);
                        isValidData = false;
                        break;
                    }
                }
            }
            // Validate reason feild
            if (EmailAttachment_messageTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(EmailAttachment_errorMessageLabel,
                    Resources.HCMResource.EmailAttachment_MessageCannotBeEmpty);
                isValidData = false;
            }
            return isValidData;
        }

        /// <summary>
        /// This protected method is used to load the default values when the page is loaded.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion Protected Methods
    }
}
