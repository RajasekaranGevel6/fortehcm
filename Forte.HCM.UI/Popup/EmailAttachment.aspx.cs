﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EmailAttachment.cs
// File that represents the user interface to send emails along with 
// attachments to the recipients. This includes position profile, 
// position profile status, test results, certificates etc.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.IO;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Configuration;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

using iTextSharp.text;
using iTextSharp.text.pdf;




#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Clas that represents the user interface to send emails along with 
    /// attachments to the recipients. This includes position profile, 
    /// position profile status, test results, certificates etc. This class
    /// inherits Forte.HCM.UI.Common.PageBase class.    
    /// </summary>
    public partial class EmailAttachment : PageBase
    {
        #region Static Variables                                               

        /// <summary>
        /// A <see cref="BaseColor"/> that holds the item color.
        /// </summary>
        static BaseColor itemColor = new BaseColor(20, 142, 192);

        /// <summary>
        /// A <see cref="BaseColor"/> that holds the header color.
        /// </summary>
        static BaseColor headerColor = new BaseColor(40, 48, 51);


        /// <summary>
        /// A <see cref="BaseColor"/> that holds the item black color.
        /// </summary>
        static BaseColor itemBlackColor = new BaseColor(0, 0, 0); 

        #endregion Static Variables

        #region Private Variables                                              

        /// <summary>
        /// A <see cref="string"/> that holds the chart image path.
        /// </summary>
        private string chartImagePath = string.Empty;

        /// <summary>
        /// A <see cref="string"/> that holds the base URL.
        /// </summary>
        private string baseURL = string.Empty;
          
        #endregion Private Variables

        #region Private constants                                              
        /// <summary>
        /// A <see cref="int"/> that holds the header detail table index.
        /// </summary>
        private const int HEADER_DETAIL_TABLE_INDEX = 0;
        #endregion Private constants
        #region Event Handlers

        /// <summary>
        /// Method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set title.
                SetTitle();

                string baseURL = ConfigurationManager.AppSettings["DEFAULT_URL"];

                Response.Cache.SetCacheability(HttpCacheability.NoCache);

                //Load Style Sheets and Skin
                if (!Utility.IsNullOrEmpty(baseURL))
                    chartImagePath = baseURL + "chart/";

                // Set default button field
                Page.Form.DefaultButton = EmailAttachment_submitButton.UniqueID;

                // Validate email address
                EmailAttachment_submitButton.Attributes.Add("onclick", "javascript:return IsValidEmail('"
                    + EmailAttachment_ccTextBox.ClientID + "','"
                    + EmailAttachment_errorMessageLabel.ClientID + "','"
                    + EmailAttachment_successMessageLabel.ClientID + "');");
                
                EmailAttachment_attachmentImageButton.Attributes.Add("onclick", "javascript:return OpenPopUp('"
                    + Request.QueryString["candidatesession"]+ "','" 
                    + Request.QueryString["attemptid"]+ "');");

                // Add handler to open 'select contact' popup.
                string positionProfileID = string.Empty;
                if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                    positionProfileID = Request.QueryString["positionprofileid"].Trim();

                EmailAttachment_toAddressImageButton.Attributes.Add
                    ("onclick", "javascript:return ShowSelectContactPopup('" +
                    EmailAttachment_toTextBox.ClientID + "','" + positionProfileID + "','To','N')");

                EmailAttachment_ccAddressImageButton.Attributes.Add
                   ("onclick", "javascript:return ShowSelectContactPopup('" +
                   EmailAttachment_ccTextBox.ClientID + "','" + positionProfileID + "','CC','N')");

                if (!IsPostBack)
                {
                    // Load 
                    // Set default focus field
                    Page.Form.DefaultFocus = EmailAttachment_toTextBox.UniqueID;
                    EmailAttachment_toTextBox.Focus();

                    UserDetail userDetail = new CommonBLManager().GetUserDetail(base.userID);

                    if (userDetail != null)
                    {
                        //EmailAttachment_fromValueLabel.Text = userDetail.FirstName
                        //    + " &lt;" + userDetail.Email + "&gt;";
                        EmailAttachment_fromValueLabel.Text = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                        EmailAttachment_fromHiddenField.Value = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                    }

                    // Call PDF construct method

                    if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "Y")
                    {
                        EmailAttachment_downloadTypeHiddenField.Value = "PDF";
                        EmailAttachment_attachmentTR.Style.Add("display", "block");
                        EmailAttachment_certificateDIV.Visible = false; 
                        ConstructPDF();
                    }
                    else   if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "CIAS")
                    {
                        EmailAttachment_downloadTypeHiddenField.Value = "PDF";
                        EmailAttachment_attachmentTR.Style.Add("display", "block");
                        EmailAttachment_certificateDIV.Visible = false;

                        if (Request.QueryString["candidatesession"] != null &&
                            Request.QueryString["attemptid"] != null)
                        {
                            GeneretePDF_AssessorSummaryRating(Request.QueryString["candidatesession"].ToString(),
                                Convert.ToInt32(Request.QueryString["attemptid"]));
                        }
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "N" &&
                        !Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "CERTIFICATE")
                    {
                        // Certificate for completed test
                        EmailAttachment_attachmentTR.Style.Add("display", "block");
                        EmailAttachment_certificateDIV.Visible = false;
                        EmailAttachment_contentIframe.Style.Add("display", "none");

                        EmailAttachment_attachmentImageButton.ImageUrl
                            = "~/App_Themes/DefaultTheme/Images/icon_view_certificate.gif";
                        EmailAttachment_downloadTypeHiddenField.Value = "CERTIFICATE";
                        ConstructCertificate();
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "N" &&
                        !Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "CERTIFICATE_TEMPLATE")
                    {
                        // Certificate template for certificate test
                        EmailAttachment_attachmentTR.Style.Add("display", "block");
                        EmailAttachment_contentIframe.Style.Add("display", "none");
                        EmailAttachment_certificateDIV.Visible = false;

                        EmailAttachment_attachmentImageButton.ImageUrl
                            = "~/App_Themes/DefaultTheme/Images/icon_view_certificate.gif";
                        EmailAttachment_downloadTypeHiddenField.Value = "CERTIFICATE_TEMPLATE";
                        ConstructCertificateTemplate();
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "N" &&
                        !Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "CONTENT")
                    {
                        // Send email as content
                        string pageName = string.Empty;

                        EmailAttachment_contentIframe.Style.Add("display", "none");
                        EmailAttachment_messageTextBox.Style.Add("height", "100px");
                        EmailAttachment_certificateDIV.Visible = true;

                        string certificateTemplateURL =
                            Session[Constants.SessionConstants.CERTIFICATE_IMAGE_URL].ToString().Replace("~/", "");
                        baseURL += certificateTemplateURL.Trim();

                        EmailAttachment_certificateImage.Attributes.Add("src", baseURL.Trim());
                        ViewState["CERTIFICATE_TEMPLATE_CONTENT_URL"] = baseURL.Trim();
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "PP")
                    {
                        EmailAttachment_downloadTypeHiddenField.Value = "PP";
                        EmailAttachment_attachmentTR.Style.Add("display", "block");
                        EmailAttachment_certificateDIV.Visible = false;
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "PP_REVIEW_PDF")
                    {
                        EmailAttachment_downloadTypeHiddenField.Value = "PP_REVIEW_PDF";
                        EmailAttachment_attachmentTR.Style.Add("display", "block");
                        EmailAttachment_certificateDIV.Visible = false;
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "EMAIL_TEST_SESSION")
                    {
                        EmailAttachment_downloadTypeHiddenField.Value = "";
                        EmailAttachment_attachmentTR.Style.Add("display", "none");
                        EmailAttachment_certificateDIV.Visible = false;

                        // Set subject.
                        if (Session["EMAIL_TEST_SESSION_SUBJECT"] != null)
                        {
                            EmailAttachment_subjectTextBox.Text = 
                                Session["EMAIL_TEST_SESSION_SUBJECT"].ToString();
                        }

                        // Set message.
                        if (Session["EMAIL_TEST_SESSION_MESSAGE"] != null)
                        {
                            EmailAttachment_messageTextBox.Text =
                                Session["EMAIL_TEST_SESSION_MESSAGE"].ToString();
                        }
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "EMAIL_INTERVIEW_SESSION")
                    {
                        EmailAttachment_downloadTypeHiddenField.Value = "";
                        EmailAttachment_attachmentTR.Style.Add("display", "none");
                        EmailAttachment_certificateDIV.Visible = false;

                        // Set subject.
                        if (Session["EMAIL_INTERVIEW_SESSION_SUBJECT"] != null)
                        {
                            EmailAttachment_subjectTextBox.Text =
                                Session["EMAIL_INTERVIEW_SESSION_SUBJECT"].ToString();
                        }

                        // Set message.
                        if (Session["EMAIL_INTERVIEW_SESSION_MESSAGE"] != null)
                        {
                            EmailAttachment_messageTextBox.Text =
                                Session["EMAIL_INTERVIEW_SESSION_MESSAGE"].ToString();
                        }
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "EMAIL_ONLINE_INTERVIEW_SESSION")
                    {
                        EmailAttachment_downloadTypeHiddenField.Value = "";
                        EmailAttachment_attachmentTR.Style.Add("display", "none");
                        EmailAttachment_certificateDIV.Visible = false;

                        // Set subject.
                        if (Session["EMAIL_ONLINE_INTERVIEW_SESSION_SUBJECT"] != null)
                        {
                            EmailAttachment_subjectTextBox.Text =
                                Session["EMAIL_ONLINE_INTERVIEW_SESSION_SUBJECT"].ToString();
                        }

                        // Set message.
                        if (Session["EMAIL_ONLINE_INTERVIEW_SESSION_MESSAGE"] != null)
                        {
                            EmailAttachment_messageTextBox.Text =
                                Session["EMAIL_ONLINE_INTERVIEW_SESSION_MESSAGE"].ToString();
                        }
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "PCIRSS")
                    {
                        EmailAttachment_downloadTypeHiddenField.Value = "";
                        EmailAttachment_attachmentTR.Style.Add("display", "none");
                        EmailAttachment_certificateDIV.Visible = false;
                        EmailAttachment_headerLiteral.Text = "Email";
                        Master.SetPageCaption("Email");

                        // Check if URL present in session.
                        if (Session["INTERVIEW_SUMMARY_PUBLISH_URL_WITH_SCORE"] != null && Session["INTERVIEW_SUMMARY_PUBLISH_URL_CANDIDATE_NAME"] != null)
                        {
                            // Set subject.
                            EmailAttachment_subjectTextBox.Text = string.Format
                                ("Published URL for interview assessment summary of candidate '{0}'", 
                                Session["INTERVIEW_SUMMARY_PUBLISH_URL_CANDIDATE_NAME"]);

                            // Set message.
                            string message = Environment.NewLine + Environment.NewLine;
                            message += string.Format("Here is the published URL for interview assessment summary of candidate '{0}'", 
                                Session["INTERVIEW_SUMMARY_PUBLISH_URL_CANDIDATE_NAME"]);
                            message += Environment.NewLine;
                            message += Environment.NewLine;
                            message += "You can click the following link to view the candidate interview assessment summary"; 
                            message += Environment.NewLine;
                            message += Session["INTERVIEW_SUMMARY_PUBLISH_URL_WITH_SCORE"];

                            EmailAttachment_messageTextBox.Text = message;

                        }
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "PCIRHS")
                    {
                        EmailAttachment_downloadTypeHiddenField.Value = "";
                        EmailAttachment_attachmentTR.Style.Add("display", "none");
                        EmailAttachment_certificateDIV.Visible = false;
                        EmailAttachment_headerLiteral.Text = "Email";
                        Master.SetPageCaption("Email");

                        // Check if URL present in session.
                        if (Session["INTERVIEW_SUMMARY_PUBLISH_URL_WITHOUT_SCORE"] != null && Session["INTERVIEW_SUMMARY_PUBLISH_URL_CANDIDATE_NAME"] != null)
                        {
                            // Set subject.
                            EmailAttachment_subjectTextBox.Text = string.Format
                               ("Published URL for interview assessment summary of candidate '{0}'",
                               Session["INTERVIEW_SUMMARY_PUBLISH_URL_CANDIDATE_NAME"]);

                            // Set message.
                            string message = Environment.NewLine + Environment.NewLine;
                            message += string.Format("Here is the published URL for interview assessment summary of candidate '{0}'",
                               Session["INTERVIEW_SUMMARY_PUBLISH_URL_CANDIDATE_NAME"]);
                            message += Environment.NewLine;
                            message += Environment.NewLine;
                            message += "You can click the following link to view the candidate interview assessment summary";
                            message += Environment.NewLine;
                            message += Session["INTERVIEW_SUMMARY_PUBLISH_URL_WITHOUT_SCORE"];

                            EmailAttachment_messageTextBox.Text = message;
                        }
                    }
                    else
                    {
                        EmailAttachment_certificateDIV.Visible = false;
                        string pageName = "TestResultPrint.aspx";
                        EmailAttachment_contentIframe.Style.Add("display", "block");
                        EmailAttachment_messageTextBox.Style.Add("height", "100px");

                        if (!Utility.IsNullOrEmpty(baseURL))
                        {
                            EmailAttachment_contentIframe.Attributes["src"] = baseURL +
                                "PrintPages/" + pageName + "?m=2&candidatesession=" +
                                Request.QueryString["candidatesession"] + "&attemptid=" +
                                Request.QueryString["attemptid"] + "&testkey=" +
                                Request.QueryString["testkey"];
                        }
                    }

                     if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesession"]) &&
                        !Utility.IsNullOrEmpty(Request.QueryString["attemptid"]) &&
                        !Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
                    {
                        Session["CAND_SESSION_KEY"] = Request.QueryString["candidatesession"].ToString();
                        Session["ATTEMPT_ID"] = Request.QueryString["attemptid"].ToString();

                        if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]))
                        {
                            if (Request.QueryString["emailtype"].ToUpper() == "CIAS")
                            {
                                EmailAttachment_attachmentImageButton.ToolTip =
                                    ViewState["AssessmentSummaryFileName"].ToString();
                            }
                            else
                            {
                                EmailAttachment_attachmentImageButton.ToolTip =
                               Request.QueryString["candidatesession"].ToString() + "_"
                               + Request.QueryString["attemptid"].ToString() + ".pdf";
                            }
                        }
                        else
                        {
                            // Set the tool tip for the attachment icon.
                            EmailAttachment_attachmentImageButton.ToolTip =
                                Request.QueryString["candidatesession"].ToString() + "_"
                                + Request.QueryString["attemptid"].ToString() + ".pdf";
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EmailAttachment_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will be called when the submit button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void EmailAttachment_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear all the error and success labels
                EmailAttachment_errorMessageLabel.Text = string.Empty;
                EmailAttachment_successMessageLabel.Text = string.Empty;
                string fileName = string.Empty;
                bool isCertificateSent = false;
                bool isPositionProfileSent = false;
                bool isTestSessionSent = false;
                bool isInterviewSessionSent = false;
                bool isOnlineInterviewSessionSent = false;
                bool isCandidateAssessmentSummary = false;
                bool isPublishURLSent = false;
                bool isPP_ReviewSent = false;

                // Validate the fields
                if (!IsValidData())
                    return;

                // Check if the querystrings are not empty.
                if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesession"]) &&
                    !Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
                    fileName = Request.QueryString["candidatesession"].ToString() + "_"
                    + Request.QueryString["attemptid"].ToString() + ".pdf";

                // Build Mail Details
                MailDetail mailDetail = new MailDetail();
                MailAttachment mailAttachment = new MailAttachment();
                mailDetail.Attachments = new List<MailAttachment>();
                mailDetail.To = new List<string>();
                mailDetail.CC = new List<string>();

                // Add the logged in user in the CC list when 'send me a copy ' is on
                if (EmailAttachement_sendMeACopyCheckBox.Checked)
                {
                    mailDetail.CC.Add(base.userEmailID);
                }

                if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "Y")
                {
                    byte[] attachementContent = (byte[])
                        Session[Constants.SessionConstants.CANDIDATE_RESULTS_PDF];
                    mailAttachment.FileContent = attachementContent;
                    mailAttachment.FileName = fileName;
                    mailDetail.Attachments.Add(mailAttachment);
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "CIAS")
                {
                    byte[] attachementContent = (byte[])
                        Session[Constants.SessionConstants.CANDIDATE_RESULTS_PDF];
                    mailAttachment.FileContent = attachementContent;
                    if (ViewState["AssessmentSummaryFileName"]!=null)
                        mailAttachment.FileName = ViewState["AssessmentSummaryFileName"].ToString();
                    else
                        mailAttachment.FileName = fileName;

                    mailDetail.Attachments.Add(mailAttachment);
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                    isCandidateAssessmentSummary = true;
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "N" &&
                        !Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "CERTIFICATE")
                {
                    byte[] certificateAttachment = Session[Constants.SessionConstants.CERTIFICATE_IMAGE] as byte[];
                    mailAttachment.FileContent = certificateAttachment;
                    mailAttachment.FileName = "MyCertificate.JPG";
                    mailDetail.Attachments.Add(mailAttachment);
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                    isCertificateSent = true;
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "N" &&
                        !Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "CERTIFICATE_TEMPLATE")
                {
                    byte[] certificateAttachment = Session[Constants.SessionConstants.CERTIFICATE_TEMPLATE_IMAGE] as byte[];
                    mailAttachment.FileContent = certificateAttachment;
                    mailAttachment.FileName = "CertificateTemplate.JPG";
                    mailDetail.Attachments.Add(mailAttachment);
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                    isCertificateSent = true;
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "N" &&
                        !Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "CONTENT")
                {
                    string certificateTemplatePath =
                        "<img src='" + ViewState["CERTIFICATE_TEMPLATE_CONTENT_URL"].ToString().Trim()
                        + "' alt='Certificate'><br/>";
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim() + certificateTemplatePath;
                    mailDetail.isBodyHTML = true;
                    isCertificateSent = true;
                }
                else if(!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "PP")
                {
                    if (!Utility.IsNullOrEmpty(Session[Constants.SessionConstants.PP_PDF_CONTENT]) || !Utility.IsNullOrEmpty(Session[Constants.SessionConstants.PP_PDF_HEADER]))
                    {
                        CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
                        candidateReportDetail = (CandidateReportDetail)Session[Constants.SessionConstants.PP_PDF_HEADER];
                        List<Dictionary<object, object>> dictinaryList = (List<Dictionary<object, object>>)Session[Constants.SessionConstants.PP_PDF_CONTENT];
                     
                        byte[] attachementContent = new WidgetReport().GetPDFPPByteArray("POSITION PROFILE", dictinaryList, candidateReportDetail);
                        mailAttachment.FileContent = attachementContent;
                        mailAttachment.FileName = candidateReportDetail.TestName.Replace(" ", "_")+".pdf";
                        mailDetail.Attachments.Add(mailAttachment);
                        mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                        isPositionProfileSent = true;
                    }
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "PP_REVIEW")
                {

                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "EMAIL_TEST_SESSION")
                {
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                    mailDetail.isBodyHTML = true;
                    isTestSessionSent = true;
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                   Request.QueryString["type"].ToUpper() == "EMAIL_INTERVIEW_SESSION")
                {
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                    mailDetail.isBodyHTML = true;
                    isInterviewSessionSent = true;
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "EMAIL_ONLINE_INTERVIEW_SESSION")
                {
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                    mailDetail.isBodyHTML = true;
                    isOnlineInterviewSessionSent = true;
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "PCIRSS" || Request.QueryString["type"].ToUpper() == "PCIRHS")
                {
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                    mailDetail.isBodyHTML = true;
                    isPublishURLSent = true;
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "PP_REVIEW_PDF")
                {
                    int positionProfileID=0;
                    if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                    {
                        positionProfileID = Convert.ToInt32(Request.QueryString["positionprofileid"].Trim());
                    }
                    // Get raw data.
                    CandidateReportDetail candidateReportDetail;
                    PDFDocumentWriter PdfWriter = new PDFDocumentWriter();
                    string logoPath = Server.MapPath("~/");
                    PDFHeader(out candidateReportDetail);
                    DataSet rawData = new PositionProfileBLManager().GetPositionProfileSummary(positionProfileID, base.userID);
                    PDFHeader(out candidateReportDetail);
                    DataTable TableHeader = rawData.Tables[HEADER_DETAIL_TABLE_INDEX];
                    DataRow dataRow = TableHeader.Rows[0];                   
                    string fileName_PP = dataRow["POSITION_PROFILE_NAME"].ToString().Trim();                   
                    mailAttachment.FileContent = PdfWriter.GetPostionprofileReview(rawData, logoPath,candidateReportDetail);
                    mailAttachment.FileName = fileName_PP.Replace(" ", "_") + ".pdf";
                    mailDetail.Attachments.Add(mailAttachment);
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                    isPP_ReviewSent = true;
                }
                else
                {
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesession"]) &&
                            !Utility.IsNullOrEmpty(Request.QueryString["attemptid"]) &&
                            !Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
                    {
                        string pageName = "TestResultPrint.aspx";

                        Server.Execute("../PrintPages/" + pageName + "?m=2&candidatesession="
                                    + Request.QueryString["candidatesession"] + "&attemptid="
                                    + Request.QueryString["attemptid"] + "&testkey="
                                    + Request.QueryString["testkey"], htw);
                        mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim() + sw.ToString().Trim();
                        mailDetail.isBodyHTML = true;
                    }
                }

                // Add 'To' mail addresses
                string[] toMailIDs = EmailAttachment_toTextBox.Text.Split(new char[] { ',' });
                for (int idx = 0; idx < toMailIDs.Length; idx++)
                    mailDetail.To.Add(toMailIDs[idx].Trim());

                if (!Utility.IsNullOrEmpty(EmailAttachment_ccTextBox.Text.Trim()))
                    mailDetail.CC.Add(EmailAttachment_ccTextBox.Text.Trim());

                //mailDetail.From = EmailAttachment_fromHiddenField.Value.Trim();
                // Set the 'from' address to default address.
                mailDetail.From = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                
                mailDetail.Subject = EmailAttachment_subjectTextBox.Text.Trim();

                // mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                bool isMailSent = false;

                try
                {
                    // Send mail
                    new EmailHandler().SendMail(mailDetail);
                    isMailSent = true;
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    isMailSent = false;
                }
                    //Script to close the popup window
                    string closeScript = "self.close();";

                    if (isMailSent && isCertificateSent)
                        base.ShowMessage(EmailAttachment_successMessageLabel,
                            Resources.HCMResource.EmailAttachment_CertificateSentSuccessfully);
                    else if (isMailSent && isPositionProfileSent)
                        //base.ShowMessage(EmailAttachment_successMessageLabel,"Position profile sent successfully");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true);
                    else if (isMailSent && isTestSessionSent)
                        //base.ShowMessage(EmailAttachment_successMessageLabel, "Test sessions sent successfully");
                         ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true);
                    else if (isMailSent && isInterviewSessionSent)
                        //base.ShowMessage(EmailAttachment_successMessageLabel, "Interview sessions sent successfully");
                         ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true);
                    else if (isMailSent && isOnlineInterviewSessionSent)
                        //base.ShowMessage(EmailAttachment_successMessageLabel, "Online interview sessions sent successfully");
                         ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true);
                    else if (isMailSent && isCandidateAssessmentSummary)
                        //base.ShowMessage(EmailAttachment_successMessageLabel, "Candidate's interview assessment summary mailed successfully");
                         ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true);
                    else if (isMailSent && isPublishURLSent)
                        //base.ShowMessage(EmailAttachment_successMessageLabel, "Published URL mailed successfully");
                         ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true);
                    else if (isMailSent && isPP_ReviewSent)
                        //base.ShowMessage(EmailAttachment_successMessageLabel, "Published URL mailed successfully");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true);
                    else if (isMailSent)
                        //base.ShowMessage(EmailAttachment_successMessageLabel,
                        //    Resources.HCMResource.EmailAttachment_TestResultsSentSuccessfully);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true);
                    else
                        base.ShowMessage(EmailAttachment_errorMessageLabel,
                            Resources.HCMResource.EmailAttachment_MailCouldNotSend);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EmailAttachment_errorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;

            if (EmailAttachment_toTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(EmailAttachment_errorMessageLabel,
                    Resources.HCMResource.EmailAttachment_ToAddressCannotBeEmpty);
                isValidData = false;
            }
            else
            {
                string[] toMailIDs = EmailAttachment_toTextBox.Text.Split(new char[] { ',' });

                for (int idx = 0; idx < toMailIDs.Length; idx++)
                {
                    if (!Regex.IsMatch(
                        toMailIDs[idx].Trim(), @"^([\w-]+\.)*?[\w-]+@[\w-]+\.([\w-]+\.)*?[\w]+$"))
                    {
                        base.ShowMessage(EmailAttachment_errorMessageLabel,
                            Resources.HCMResource.EmailAttachment_InvalidEmailAddress);
                        isValidData = false;
                        break;
                    }
                }
            }
            // Validate reason feild
            if (EmailAttachment_messageTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(EmailAttachment_errorMessageLabel,
                    Resources.HCMResource.EmailAttachment_MessageCannotBeEmpty);
                isValidData = false;
            }
            return isValidData;
        }

        /// <summary>
        /// Overridden protected method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that sets the page title and caption.
        /// </summary>
        private void SetTitle()
        {
            // Set the page title and caption based on type.
            if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                Request.QueryString["type"].Trim().ToUpper() == "PP")
            {
                Master.SetPageCaption("Email Position Profile");
                EmailAttachment_headerLiteral.Text = "Email Position Profile";
            }
            else
            {
                Master.SetPageCaption("Email Attachment");
                EmailAttachment_headerLiteral.Text = "Email Attachment";
            }
        } 
         
        private string GetCandidateImageFile(int candidateID)
        {
            string path = null;
             
            bool isExists = false;
            if (candidateID>0)
            { 
                path = Server.MapPath("~/") +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                            "\\" + candidateID + "." +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];
                if (File.Exists(path))
                    isExists = true;
                else
                    isExists = false;
            }

            if (isExists)
                return path;
            else
            {    
                return Server.MapPath("~/") +
                      ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                      "\\" + "photo_not_available.png";
            }
        }

        private void GeneretePDF_AssessorSummaryRating(string candidateSessionID, int attemptID)
        {

            CandidateInterviewSessionDetail sessionDetail = new InterviewSchedulerBLManager().
          GetCandidateInterviewSessionDetail(candidateSessionID, attemptID);

            PDFDocumentWriter pdfWriter = new PDFDocumentWriter();

            DataTable assessmentTable = new DataTable();

            string fileName = sessionDetail.CandidateName + "_" + "InterviewAssessmentSummary.pdf";

            ViewState["AssessmentSummaryFileName"] = fileName;

            Document doc = new Document(PDFDocumentWriter.paper,
                    PDFDocumentWriter.leftMargin, PDFDocumentWriter.rightMargin,
                    PDFDocumentWriter.topMargin, PDFDocumentWriter.bottomMargin);

            string logoPath = Server.MapPath("~/");

            iTextSharp.text.Image imgBackground = iTextSharp.text.Image.GetInstance(logoPath + "Images/AssessmentBackground.jpg");

            imgBackground.ScaleToFit(1000, 132);
            imgBackground.Alignment = iTextSharp.text.Image.UNDERLYING;
            imgBackground.SetAbsolutePosition(50, 360);

            PdfPTable pageHeader = new PdfPTable(pdfWriter.PageHeader(logoPath, "Candidate Interview Assessment Report"));
            pageHeader.SpacingAfter = 10f;

            //Construct Table Structures    
            PdfPTable tableHeaderDetail = new PdfPTable(1);
            PdfPCell cellBorder = new PdfPCell(new Phrase(""));
            cellBorder.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            cellBorder.Border = 0;
            tableHeaderDetail.WidthPercentage = 90;
            cellBorder.PaddingTop = 20f;
            cellBorder.AddElement(pdfWriter.TableHeader(sessionDetail, candidateSessionID,
                attemptID, GetCandidateImageFile(sessionDetail.CandidateInformationID), out assessmentTable));
            tableHeaderDetail.AddCell(cellBorder);
            tableHeaderDetail.SpacingAfter = 30f;

            //Response Output
            MemoryStream memoryStream = new MemoryStream();

            PdfWriter writer =  PdfWriter.GetInstance(doc, memoryStream);
            doc.Open();
            doc.Add(pageHeader);
            doc.Add(imgBackground);
            doc.Add(tableHeaderDetail); 
            doc.Add(pdfWriter.TableDetail(assessmentTable));
            writer.CloseStream = false;
            doc.Close(); 
            memoryStream.Position = 0; 
            // Convert stream to byte array.
            BinaryReader reader = new BinaryReader(memoryStream);
            byte[] dataAsByteArray = new byte[memoryStream.Length];
            dataAsByteArray = reader.ReadBytes(Convert.ToInt32(memoryStream.Length));

            // Store the byte array in Session
            Session[Constants.SessionConstants.CANDIDATE_RESULTS_PDF] = dataAsByteArray; 
        }

        /// <summary>
        /// Method that constructs the PDF document.
        /// </summary>
        private void ConstructPDF()
        {
            string candSessionID = string.Empty;
            string attemptID = string.Empty;
            string testKey = string.Empty;
            decimal averageTimeTaken = 0;
            string fileName = string.Empty;
            string histogramSessionKey = string.Empty;

            if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesession"]) &&
              !Utility.IsNullOrEmpty(Request.QueryString["attemptid"]) &&
              !Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
            {
                candSessionID = Request.QueryString["candidatesession"].ToString();
                testKey = Request.QueryString["testkey"].ToString();
                attemptID = Request.QueryString["attemptid"].ToString();
            }

            string testName = string.Empty;
            testName = new ReportBLManager().GetTestName(testKey);

            string candidateName = string.Empty;
            candidateName = new CandidateBLManager().GetCandidateName(candSessionID);

            fileName = candSessionID + "_" + attemptID + ".pdf";

            //Response.ContentType = "application/pdf";
            //if (fileName != string.Empty)
            //    Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            //else
            //    Response.AddHeader("content-disposition", "attachment;filename=TestResult.pdf");

            //Response.Cache.SetCacheability(HttpCacheability.NoCache);

            // Set Styles
            iTextSharp.text.Font fontCaptionStyle = iTextSharp.text.FontFactory.GetFont
                ("Arial,sans-serif", 8, iTextSharp.text.Font.NORMAL, headerColor);
            iTextSharp.text.Font fontHeaderStyle = iTextSharp.text.FontFactory.GetFont
                ("Arial,sans-serif", 7, iTextSharp.text.Font.NORMAL, headerColor);
            iTextSharp.text.Font fontItemStyle = iTextSharp.text.FontFactory.GetFont
                ("Arial,sans-serif", 7, iTextSharp.text.Font.NORMAL, itemColor);

            TestStatistics testStatistics = new ReportBLManager().GetTestSummaryDetails(testKey);

            //Set Document Size as A4.
            var doc = new Document(iTextSharp.text.PageSize.A4);

            //Construct Table Structures            
            PdfPTable tableCandidateInfo = new PdfPTable(8);

            tableCandidateInfo.SpacingBefore = 10f;
            tableCandidateInfo.SpacingAfter = 20f;

            //Candidate Info table Structure
            PdfPCell cellTestIDHeader = new PdfPCell(new Phrase("Test ID", fontHeaderStyle));
            tableCandidateInfo.AddCell(cellTestIDHeader);
            PdfPCell cellTestID = new PdfPCell(new Phrase(testKey, fontItemStyle));
            tableCandidateInfo.AddCell(cellTestID);
            PdfPCell cellTestNameHeader = new PdfPCell(new Phrase("Test Name", fontHeaderStyle));
            tableCandidateInfo.AddCell(cellTestNameHeader);
            PdfPCell cellTestName = new PdfPCell(new Phrase(testName.Trim(), fontItemStyle));
            tableCandidateInfo.AddCell(cellTestName);
            PdfPCell cellCandidateNameHeader = new PdfPCell(new Phrase("Candidate Name", fontHeaderStyle));
            tableCandidateInfo.AddCell(cellCandidateNameHeader);
            PdfPCell cellCandidateName = new PdfPCell(new Phrase(candidateName, fontItemStyle));
            tableCandidateInfo.AddCell(cellCandidateName);
            PdfPCell cellCrerditsEarnedHeader = new PdfPCell(new Phrase("Credits Earned in ($)", fontHeaderStyle));
            tableCandidateInfo.AddCell(cellCrerditsEarnedHeader);
            PdfPCell cellCrerditsEarned = new PdfPCell(new Phrase("0.00", fontItemStyle));
            tableCandidateInfo.AddCell(cellCrerditsEarned);

            PdfPTable tableTestStatisticsHeader = new PdfPTable(1);
            tableTestStatisticsHeader.SpacingAfter = 8f;
            PdfPCell tableTestStatisticsHeaderCell = new PdfPCell(new Phrase("Test Statistics", fontCaptionStyle));
            tableTestStatisticsHeaderCell.BackgroundColor = new BaseColor(235, 239, 241);
            PdfPCell tableTestStatisticsItems = new PdfPCell(new Phrase("Items", fontItemStyle));
            tableTestStatisticsHeader.AddCell(tableTestStatisticsHeaderCell);

            //Summary table structure
            PdfPTable tableTestStatistics = new PdfPTable(8);
            tableTestStatistics.SpacingAfter = 8f;
            tableTestStatistics.WidthPercentage = 100;
            PdfPCell cellTestSummaryHeader = new PdfPCell(new Phrase("Test Summary", fontCaptionStyle));
            cellTestSummaryHeader.Colspan = 4;
            cellTestSummaryHeader.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            tableTestStatistics.AddCell(cellTestSummaryHeader);
            PdfPCell cellTestCategoryHeader = new PdfPCell(new Phrase("Categories / Subjects", fontCaptionStyle));
            cellTestCategoryHeader.Colspan = 4;
            cellTestCategoryHeader.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            tableTestStatistics.AddCell(cellTestCategoryHeader);
            PdfPCell cellTestAuthorHeader = new PdfPCell(new Phrase("Test Author", fontHeaderStyle));
            tableTestStatistics.AddCell(cellTestAuthorHeader);
            PdfPCell cellTestAuthor = new PdfPCell(new Phrase(testStatistics.TestAuthor.Trim(), fontItemStyle));
            tableTestStatistics.AddCell(cellTestAuthor);
            PdfPCell cellTestScoreHeader = new PdfPCell(new Phrase("Highest Score", fontHeaderStyle));
            tableTestStatistics.AddCell(cellTestScoreHeader);
            PdfPCell cellTestScore = new PdfPCell(new Phrase(testStatistics.HighestScore.ToString(), fontItemStyle));
            tableTestStatistics.AddCell(cellTestScore);

            PdfPCell cellCategoryOrSubjects = new PdfPCell(new Phrase("Category", fontHeaderStyle));
            cellCategoryOrSubjects.Colspan = 4;
            cellCategoryOrSubjects.Rowspan = 6;
            cellCategoryOrSubjects.Table = new PdfPTable(2);

            //cellCategoryOrSubjects.Table.SpacingAfter = 20f;
            cellCategoryOrSubjects.Table.AddCell(new Phrase("Category", fontItemStyle));
            cellCategoryOrSubjects.Table.AddCell(new Phrase("Subject", fontItemStyle));

            // Category/Subject Details
            foreach (Subject subject in testStatistics.SubjectStatistics)
            {
                if (testStatistics.SubjectStatistics == null)
                    break;

                PdfPCell cellInnerCategory = new PdfPCell(new Phrase
                    (subject.CategoryName.Trim(), fontHeaderStyle));

                PdfPCell cellInnerSubject = new PdfPCell(new Phrase
                    (subject.SubjectName.Trim(), fontHeaderStyle));

                cellCategoryOrSubjects.Table.AddCell(cellInnerCategory);
                cellCategoryOrSubjects.Table.AddCell(cellInnerSubject);
            }

            tableTestStatistics.AddCell(cellCategoryOrSubjects);
            PdfPCell cellNOofQuestionsHeader = new PdfPCell(new Phrase("Number Of Questions", fontHeaderStyle));
            tableTestStatistics.AddCell(cellNOofQuestionsHeader);
            PdfPCell cellNOofQuestions = new PdfPCell(new Phrase
                (testStatistics.NoOfQuestions.ToString(), fontItemStyle));
            tableTestStatistics.AddCell(cellNOofQuestions);
            PdfPCell cellLowestScoreHeader = new PdfPCell(new Phrase
                ("Lowest Score", fontHeaderStyle));
            tableTestStatistics.AddCell(cellLowestScoreHeader);
            PdfPCell cellLowestScore = new PdfPCell(new Phrase
                (testStatistics.LowestScore.ToString(), fontItemStyle));
            tableTestStatistics.AddCell(cellLowestScore);
            PdfPCell cellNOofCandidatesHeader = new PdfPCell
                (new Phrase("Number Of Candidates", fontHeaderStyle));
            tableTestStatistics.AddCell(cellNOofCandidatesHeader);
            PdfPCell cellNOofCandidates = new PdfPCell(new Phrase
                (testStatistics.NoOfCandidates.ToString(), fontItemStyle));
            tableTestStatistics.AddCell(cellNOofCandidates);
            PdfPCell cellTestMeanScoreHeader = new PdfPCell(new Phrase("Mean Score", fontHeaderStyle));
            tableTestStatistics.AddCell(cellTestMeanScoreHeader);
            PdfPCell cellTestMeanScore = new PdfPCell(new Phrase
                (testStatistics.MeanScore.ToString(), fontItemStyle));
            tableTestStatistics.AddCell(cellTestMeanScore);
            PdfPCell cellAverageTimeHeader = new PdfPCell(new Phrase
                ("Average Time Taken By Candidates", fontHeaderStyle));
            tableTestStatistics.AddCell(cellAverageTimeHeader);

            if (!Utility.IsNullOrEmpty(testStatistics.AverageTimeTakenByCandidates)
                && testStatistics.AverageTimeTakenByCandidates != 0)
            {
                averageTimeTaken = testStatistics.AverageTimeTakenByCandidates;
            }

            PdfPCell cellAverageTime = new PdfPCell(new Phrase
                (Utility.ConvertSecondsToHoursMinutesSeconds
                (Convert.ToInt32(testStatistics.AverageTimeTakenByCandidates)), fontItemStyle));

            tableTestStatistics.AddCell(cellAverageTime);
            PdfPCell cellStandardDeviationHeader = new PdfPCell(new Phrase("Standard Deviation", fontHeaderStyle));
            tableTestStatistics.AddCell(cellStandardDeviationHeader);
            PdfPCell cellStandardDeviation = new PdfPCell(new Phrase
                (testStatistics.StandardDeviation.ToString(), fontItemStyle));
            tableTestStatistics.AddCell(cellStandardDeviation);
            PdfPCell cellScoreRangeHeader = new PdfPCell(new Phrase("Score Range", fontHeaderStyle));
            tableTestStatistics.AddCell(cellScoreRangeHeader);
            PdfPCell cellScoreRange = new PdfPCell(new Phrase("TBD", fontItemStyle));
            tableTestStatistics.AddCell(cellScoreRange);
            PdfPCell cellEmptyHeader = new PdfPCell(new Phrase("", fontHeaderStyle));
            tableTestStatistics.AddCell(cellEmptyHeader);
            PdfPCell cellEmptyItem = new PdfPCell(new Phrase("", fontItemStyle));
            tableTestStatistics.AddCell(cellEmptyItem);

            int rowCount = 0;
            if (testStatistics.SubjectStatistics.Count > 6)
                rowCount = testStatistics.SubjectStatistics.Count - 6;
            for (int j = 0; j < rowCount; j++)
            {
                PdfPCell cellEmptyItemforRowSpan = new PdfPCell(new Phrase("", fontItemStyle));
                cellEmptyItemforRowSpan.Colspan = 4;
                tableTestStatistics.AddCell(cellEmptyItemforRowSpan);
            }

            tableTestStatisticsItems.AddElement(tableTestStatistics);

            //Category and Subject Statistics table structure
            PdfPTable tableCategoryStatistics = new PdfPTable(2);
            tableCategoryStatistics.SpacingAfter = 8f;
            tableCategoryStatistics.WidthPercentage = 100;
            PdfPCell cellCategoryStatistics = new PdfPCell(new Phrase("Category Statistics", fontCaptionStyle));
            PdfPCell cellSubjectStatistics = new PdfPCell(new Phrase("Subject Statistics", fontCaptionStyle));
            cellCategoryStatistics.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            cellSubjectStatistics.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            tableCategoryStatistics.AddCell(cellCategoryStatistics);
            tableCategoryStatistics.AddCell(cellSubjectStatistics);
            iTextSharp.text.Image imageCategory = iTextSharp.text.Image.GetInstance(chartImagePath
                + Constants.ChartConstants.CHART_CATEGORY + "-" + testKey + ".png");
            imageCategory.ScaleToFit(205f, 300f);
            imageCategory.Border = Rectangle.BOX;
            imageCategory.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellCategoryImage = new PdfPCell(imageCategory);
            cellCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableCategoryStatistics.AddCell(cellCategoryImage);
            iTextSharp.text.Image imageSubject = iTextSharp.text.Image.GetInstance(chartImagePath
                + Constants.ChartConstants.CHART_SUBJECT + "-" + testKey + ".png");
            imageSubject.ScaleToFit(205f, 300f);
            imageSubject.Border = Rectangle.BOX;
            imageSubject.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellSubjectImage = new PdfPCell(imageSubject);
            cellSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableCategoryStatistics.AddCell(cellSubjectImage);

            tableTestStatisticsItems.AddElement(tableCategoryStatistics);

            //Test Area and complexity table structure
            PdfPTable tableTestAreaStatistics = new PdfPTable(2);
            //tableTestAreaStatistics.SpacingAfter = 3f;
            tableTestAreaStatistics.WidthPercentage = 100;
            PdfPCell cellAreStatisticsHeader = new PdfPCell(new Phrase("Test Area Statistics", fontCaptionStyle));
            PdfPCell cellComplexityStatisticsHeader = new PdfPCell(new Phrase("Complexity Statistics", fontCaptionStyle));
            tableTestAreaStatistics.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            tableTestAreaStatistics.AddCell(cellAreStatisticsHeader);
            tableTestAreaStatistics.AddCell(cellComplexityStatisticsHeader);
            iTextSharp.text.Image imageArea = iTextSharp.text.Image.GetInstance(chartImagePath
                + Constants.ChartConstants.CHART_TESTAREA + "-" + testKey + ".png");
            imageArea.ScaleToFit(205f, 300f);
            imageArea.Border = Rectangle.BOX;
            imageArea.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellAreaImage = new PdfPCell(imageArea);
            cellAreaImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableTestAreaStatistics.AddCell(cellAreaImage);
            iTextSharp.text.Image imageComplexity = iTextSharp.text.Image.GetInstance(chartImagePath
                + Constants.ChartConstants.CHART_COMPLEXITY + "-" + testKey + ".png");
            imageComplexity.ScaleToFit(205f, 300f);
            imageComplexity.Border = Rectangle.BOX;
            imageComplexity.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellComplexityImage = new PdfPCell(imageComplexity);
            cellComplexityImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableTestAreaStatistics.AddCell(cellComplexityImage);

            tableTestStatisticsItems.AddElement(tableTestAreaStatistics);
            tableTestStatisticsHeader.AddCell(tableTestStatisticsItems);

            PdfPTable tableCandidateStatisticsHeader = new PdfPTable(1);
            tableCandidateStatisticsHeader.SpacingAfter = 8f;
            PdfPCell tableCandidateStatisticsHeaderCell = new PdfPCell(new Phrase
                ("Candidate Statistics", fontCaptionStyle));
            tableCandidateStatisticsHeaderCell.BackgroundColor = new BaseColor(235, 239, 241);
            PdfPCell tableCandidateStatisticsItemCell = new PdfPCell(new Phrase("", fontItemStyle));
            tableCandidateStatisticsHeader.AddCell(tableCandidateStatisticsHeaderCell);

            CandidateStatisticsDetail candidateStatisticsDetail =
                Session["CANDIDATE_STATISTICS"] as CandidateStatisticsDetail;

            //Candidate Statistics
            PdfPTable tableCandidateStatistics = new PdfPTable(4);
            tableCandidateStatistics.SpacingAfter = 8f;
            tableCandidateStatistics.WidthPercentage = 100;
            PdfPCell cellCandidateNoOfQuestionsHeader = new PdfPCell(new Phrase
                ("Number Of Questions", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateNoOfQuestionsHeader);
            PdfPCell cellCandidateNoOfQuestion = new PdfPCell(new Phrase
                (testStatistics.NoOfQuestions.ToString(), fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateNoOfQuestion);
            PdfPCell cellCandidateMyScoreHeader = new PdfPCell(new Phrase("My Score", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateMyScoreHeader);
            PdfPCell cellCandidateMyScore = new PdfPCell(new Phrase
                (candidateStatisticsDetail.MyScore.ToString(), fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateMyScore);
            PdfPCell cellCandidateTotalQuestionsAttendedHeader = new PdfPCell(new Phrase
                ("Total Questions Attended", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateTotalQuestionsAttendedHeader);
            PdfPCell cellCandidateTotalQuestionsAttended = new PdfPCell(new Phrase
                (candidateStatisticsDetail.TotalQuestionAttended.ToString(), fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateTotalQuestionsAttended);
            PdfPCell cellCCandidateAbsoluteScoreHeader = new PdfPCell(new Phrase
                ("Absolute Score", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCCandidateAbsoluteScoreHeader);
            PdfPCell cellCandidateAbsolute = new PdfPCell(new Phrase
                (candidateStatisticsDetail.AbsoluteScore.ToString(), fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAbsolute);
            PdfPCell cellCandidateTotalQuestionsSkippedHeader = new PdfPCell(new Phrase
                ("Total Questions Skipped", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateTotalQuestionsSkippedHeader);
            PdfPCell cellCandidateTotalQuestionsSkipped = new PdfPCell(new Phrase
                (candidateStatisticsDetail.TotalQuestionSkipped.ToString(), fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateTotalQuestionsSkipped);
            PdfPCell cellCandidateRelativeScoreHeader = new PdfPCell(new Phrase
                ("Relative Score", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateRelativeScoreHeader);
            PdfPCell cellCandidateRelativeScore = new PdfPCell(new Phrase(candidateStatisticsDetail.RelativeScore.ToString(), fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateRelativeScore);
            PdfPCell cellCandidateAnsweredCorrectlyHeader = new PdfPCell(new Phrase
                ("Questions Answered Correctly", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateAnsweredCorrectlyHeader);
            PdfPCell cellCandidateAnsweredCorrectly = new PdfPCell(new Phrase
                (candidateStatisticsDetail.QuestionsAnsweredCorrectly.ToString(), fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAnsweredCorrectly);
            PdfPCell cellCandidatePercentileHeader = new PdfPCell(new Phrase("Percentile", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidatePercentileHeader);
            PdfPCell cellCandidateercentile = new PdfPCell(new Phrase
                (candidateStatisticsDetail.Percentile.ToString(), fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateercentile);
            PdfPCell cellCandidateAnsweredWronglyHeader = new PdfPCell(new Phrase
                ("Questions Answered Wrongly", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateAnsweredWronglyHeader);
            PdfPCell cellCandidateAnsweredWrongly = new PdfPCell(new Phrase
                (candidateStatisticsDetail.QuestionsAnsweredWrongly.ToString(), fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAnsweredWrongly);
            PdfPCell cellCandidateTotalTimeTakenHeader = new PdfPCell(new Phrase
                ("Total Time Taken", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateTotalTimeTakenHeader);
            PdfPCell cellCandidateTotalTimeTaken = new PdfPCell(new Phrase
                (candidateStatisticsDetail.TimeTaken.ToString(), fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateTotalTimeTaken);
            PdfPCell cellCandidateAnsweredCorrectlyPercentageHeader = new PdfPCell
                (new Phrase("Questions Answered Correctly (in %)", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateAnsweredCorrectlyPercentageHeader);
            PdfPCell cellCandidateAnsweredCorrectlyPercentage = new PdfPCell(new Phrase
                (candidateStatisticsDetail.AnsweredCorrectly.ToString(), fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAnsweredCorrectlyPercentage);
            PdfPCell cellCandidateAverageTimeTakenToAnswerEachQuestionHeader = new PdfPCell
                (new Phrase("Average Time Taken To Answer Each Question", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateAverageTimeTakenToAnswerEachQuestionHeader);


            
            if (!Utility.IsNullOrEmpty(candidateStatisticsDetail.AverageTimeTaken)
                && candidateStatisticsDetail.AverageTimeTaken.Length != 0)
            {
                averageTimeTaken = decimal.Parse(candidateStatisticsDetail.AverageTimeTaken);
            }

            PdfPCell cellCandidateAverageTimeTakenToAnswerEachQuestion = new PdfPCell
                (new Phrase(Utility.ConvertSecondsToHoursMinutesSeconds
                    (decimal.ToInt32(averageTimeTaken)), fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAverageTimeTakenToAnswerEachQuestion);
            PdfPCell cellCandidateTestStatusHeader = new PdfPCell(
              new Phrase("Test Status", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateTestStatusHeader);
            PdfPCell cellCandidateTestStatus = new PdfPCell(
                new Phrase(candidateStatisticsDetail.TestStatus, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateTestStatus);
            PdfPCell cellCandidateAverageTimeTakenToAnswerEachQuestionHeader1 = new PdfPCell(
                new Phrase("", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateAverageTimeTakenToAnswerEachQuestionHeader1);
            PdfPCell cellCandidateAverageTimeTakenToAnswerEachQuestion1 = new PdfPCell(
                new Phrase("", fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAverageTimeTakenToAnswerEachQuestion1);

            tableCandidateStatisticsItemCell.AddElement(tableCandidateStatistics);

            //Test Score and category table structure
            PdfPTable tableTestScore = new PdfPTable(2);
            tableTestScore.SpacingAfter = 8f;
            tableTestScore.WidthPercentage = 100;
            PdfPCell cellScoreHeader = new PdfPCell(new Phrase("Test Scores", fontCaptionStyle));
            PdfPCell cellScoreCategoryHeader = new PdfPCell(new Phrase
                ("Score Distribution Amongst Categories", fontCaptionStyle));
            tableTestScore.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            tableTestScore.AddCell(cellScoreHeader);
            tableTestScore.AddCell(cellScoreCategoryHeader);

            //iTextSharp.text.Image imageTestScore;

            //if (!Utility.IsNullOrEmpty(Request.QueryString["parentpage"]) &&
            //    Request.QueryString["parentpage"].ToUpper() == "CAND_HOME" ||
            //     Request.QueryString["parentpage"].ToUpper() == "MY_TST")
            //{
            //    imageTestScore = iTextSharp.text.Image.GetInstance(
            //        chartImagePath + Constants.ChartConstants.CHART_HISTOGRAM + "-"
            //        + testKey + "-" + candSessionID + "-" + attemptID + ".png");
            //}
            //else
            //{
            //    imageTestScore = iTextSharp.text.Image.GetInstance(
            //      chartImagePath + Constants.ChartConstants.CHART_HISTOGRAM + "-Adm" + "-"
            //      + testKey + "-" + candSessionID + "-" + attemptID + ".png");
            //}

            iTextSharp.text.Image imageTestScore = iTextSharp.text.Image.GetInstance(
                   chartImagePath + Constants.ChartConstants.CHART_HISTOGRAM + "-"
                   + testKey + "-" + candSessionID + "-" + attemptID + ".png");

            imageTestScore.ScaleToFit(205f, 300f);
            imageTestScore.Border = Rectangle.BOX;
            imageTestScore.Alignment = iTextSharp.text.Image.ALIGN_LEFT;

            PdfPCell cellTestScoreImage = new PdfPCell(imageTestScore);
            cellCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableTestScore.AddCell(cellTestScoreImage);

            iTextSharp.text.Image imageScoreCategory = iTextSharp.text.Image.GetInstance(chartImagePath
                + Constants.ChartConstants.CHART_CATEGORY + "-" + testKey + "-" + candSessionID + "-"
                + attemptID + ".png");
            imageScoreCategory.ScaleToFit(205f, 300f);
            imageScoreCategory.Border = Rectangle.BOX;
            imageScoreCategory.Alignment = iTextSharp.text.Image.ALIGN_LEFT;

            PdfPCell cellScoreCategoryImage = new PdfPCell(imageScoreCategory);
            cellSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableTestScore.AddCell(cellScoreCategoryImage);

            tableCandidateStatisticsItemCell.AddElement(tableTestScore);

            //Score subject and score distribution area table structure
            PdfPTable tableScoreSubject = new PdfPTable(2);
            //tableScoreSubject.SpacingAfter = 3f;
            tableScoreSubject.WidthPercentage = 100;
            PdfPCell cellScoreSubjectHeader = new PdfPCell(new Phrase
                ("Score Distribution Amongst Subjects", fontCaptionStyle));
            PdfPCell cellScoreTestAreaHeader = new PdfPCell(new Phrase
                ("Score Distribution Amongst Test Areas", fontCaptionStyle));
            tableScoreSubject.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            tableScoreSubject.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            tableScoreSubject.AddCell(cellScoreSubjectHeader);
            tableScoreSubject.AddCell(cellScoreTestAreaHeader);

            iTextSharp.text.Image imageScoreSubject = iTextSharp.text.Image.GetInstance(chartImagePath
                + Constants.ChartConstants.CHART_SUBJECT + "-" + testKey + "-"
                + candSessionID + "-" + attemptID + ".png");
            imageScoreSubject.ScaleToFit(205f, 300f);
            imageScoreSubject.Border = Rectangle.BOX;
            imageArea.Alignment = iTextSharp.text.Image.ALIGN_LEFT;

            PdfPCell cellScoreSubjectImage = new PdfPCell(imageScoreSubject);
            cellAreaImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableScoreSubject.AddCell(cellScoreSubjectImage);
            iTextSharp.text.Image imageScoreArea = iTextSharp.text.Image.GetInstance(chartImagePath
                + Constants.ChartConstants.CHART_TESTAREA + "-" + testKey + "-"
                + candSessionID + "-" + attemptID + ".png");
            imageScoreArea.ScaleToFit(205f, 300f);
            imageScoreArea.Border = Rectangle.BOX;
            imageScoreArea.Alignment = iTextSharp.text.Image.ALIGN_LEFT;

            PdfPCell cellScoreAreaImage = new PdfPCell(imageScoreArea);
            cellComplexityImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableScoreSubject.AddCell(cellScoreAreaImage);

            tableCandidateStatisticsItemCell.AddElement(tableScoreSubject);
            tableCandidateStatisticsHeader.AddCell(tableCandidateStatisticsItemCell);

            //Response Output
            //PdfWriter.GetInstance(doc, Response.OutputStream);

            //Response Output
            MemoryStream memoryStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);

            doc.Open();
            doc.Add(tableCandidateInfo);
            doc.Add(tableTestStatisticsHeader);
            doc.Add(tableCandidateStatisticsHeader);

            writer.CloseStream = false;
            doc.Close();
            memoryStream.Position = 0;

            // Convert stream to byte array.
            BinaryReader reader = new BinaryReader(memoryStream);
            byte[] dataAsByteArray = new byte[memoryStream.Length];
            dataAsByteArray = reader.ReadBytes(Convert.ToInt32(memoryStream.Length));

            // Store the byte array in Session
            Session[Constants.SessionConstants.CANDIDATE_RESULTS_PDF] = dataAsByteArray;
        }

        /// <summary>
        /// Method that will retrieve the certificate image as bytes from the DB
        /// against the candidate session id and attempt id.
        /// </summary>
        private void ConstructCertificate()
        {
            if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesession"]) &&
                !Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
                Session[Constants.SessionConstants.CERTIFICATE_IMAGE] = new AdminBLManager().GetCertificateImage
                    (Request.QueryString["candidatesession"].Trim(),
                    Convert.ToInt32(Request.QueryString["attemptid"]));
        }


        private void PDFHeader(out CandidateReportDetail candidateReportDetail)
        {
            candidateReportDetail = new CandidateReportDetail();
            UserDetail userDetail = new UserDetail();
            userDetail = (UserDetail)Session["USER_DETAIL"];
            candidateReportDetail.LoginUser = userDetail.FirstName + " " + userDetail.LastName;
        }

        /// <summary>
        /// Method that will check the tempate session is null or not.
        /// </summary>
        private void ConstructCertificateTemplate()
        {
            if (Session[Constants.SessionConstants.CERTIFICATE_TEMPLATE_IMAGE] == null)
                return;
        }

        #endregion Private Methods                                             
    }
}