<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CertificationTitleEntry.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.CertificationTitleEntry" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<asp:Content ID="CertificationTitleEntry_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">

    <script type="text/javascript" language="javascript">

    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="CertificationTitleEntry_headerLiteral" runat="server" Text="Certification Titles"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="CertificationTitleEntry_topCancelImageButton" runat="server"
                                SkinID="sknCloseImageButton" OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="msg_align">
                            <asp:UpdatePanel ID="CertificationTitleEntry_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="CertificationTitleEntry_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="CertificationTitleEntry_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="CertificationTitleEntry_addEntryButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="CertificationTitleEntry_selectHeaderUpdatePanel" runat="server"
                                UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr runat="server">
                                            <td class="header_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 100%">
                                                            <asp:Literal ID="CertificationTitleEntry_vectorDetailsHeader" runat="server" Text="Vector Details"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <table cellpadding="3" cellspacing="3" width="100%">
                                                    <tr>
                                                        <td style="width: 15%">
                                                            <asp:Label runat="server" ID="CertificationTitleEntry_vectorGroupLabel" Text="Group"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%">
                                                            <asp:Label ID="CertificationTitleEntry_vectorGroupValueLabel" runat="server" Text=""
                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:Label runat="server" ID="CertificationTitleEntry_vectorLabel" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%">
                                                            <asp:Label ID="CertificationTitleEntry_vectorValueLabel" runat="server" Text=""
                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_5">
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:UpdatePanel ID="CertificationTitleEntry_searchResultsUpdatePanel" runat="server"
                                UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr id="CertificationTitleEntry_searchTestResultsTR" runat="server">
                                            <td class="header_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 100%">
                                                            <asp:Literal ID="CertificationTitleEntry_searchResultsLiteral" runat="server" Text="Certification Titles"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left">
                                                            <div style="height: 226px; overflow: auto;" runat="server" id="CertificationTitleEntry_certification_titleDiv">
                                                                <asp:GridView ID="CertificationTitleEntry_titlesGridView" runat="server" AllowSorting="True"
                                                                    AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowCommand="CertificationTitleEntry_titlesGridView_RowCommand">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="CertificationTitleEntry_titlesGridView_idHiddenField" runat="server"
                                                                                    Value='<%# Eval("ID") %>' />
                                                                                <asp:ImageButton ID="CertificationTitleEntry_titlesGridView_editImageButton" runat="server"
                                                                                    SkinID="sknEditDictionaryEntryImageButton" ToolTip="Edit Certification Title"
                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="EditEntry" />&nbsp;
                                                                                <asp:ImageButton ID="CertificationTitleEntry_titlesGridView_deleteImageButton" runat="server"
                                                                                    SkinID="sknDeleteDictionaryEntryImageButton" ToolTip="Delete Certification Title"
                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="DeleteEntry" />&nbsp;
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-Width="88%" HeaderText="Alias">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="CertificationTitleEntry_titlesGridView_titleLabel" runat="server"
                                                                                    Text='<%# Eval("Title") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="CertificationTitleEntry_addEntryButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="CertificationTitleEntry_addAliasUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="header_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 100%">
                                                            <asp:Literal ID="CertificationTitleEntry_addEntryLiteral" runat="server" Text="Add Certification Title"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <table cellpadding="3" cellspacing="3" width="100%">
                                                    <tr>
                                                        <td style="width: 15%">
                                                            <asp:Label runat="server" ID="CertificationTitleEntry_addCertificationTitleLabel"
                                                                Text="Certification Title" SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">*</span>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <asp:TextBox runat="server" ID="CertificationTitleEntry_addCertificationTitleTextBox"
                                                                Width="100%" MaxLength="255"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 1%">
                                                            <asp:ImageButton ID="CertificationTitleEntry_addCertificationTitleHelpImageButton"
                                                                SkinID="sknHelpImageButton" runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                ToolTip="Enter the certification title here" />
                                                        </td>
                                                        <td style="width: 8%" align="left">
                                                            <asp:Button runat="server" Text="Add" SkinID="sknButtonId" ID="CertificationTitleEntry_addEntryButton"
                                                                OnClick="CertificationTitleEntry_addEntryButton_Click" ToolTip="Add" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_5">
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_left_8">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="CertificationTitleEntry_resetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="CertificationTitleEntry_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="CertificationTitleEntry_cancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="CertificationTitleEntry_editEntryUpdatePanel" runat="server"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="CertificationTitleEntry_editEntryPanel" runat="server" CssClass="popupcontrol_edit_dictionary_entry"
                            Style="display: none">
                            <div style="display: none;">
                                <asp:Button ID="CertificationTitleEntry_editEntryPanel_hiddenButton" runat="server"
                                    Text="Hidden" />
                            </div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="CertificationTitleEntry_editEntryPanel_headerLiteral" runat="server"
                                                        Text="Edit Certification Title"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="CertificationTitleEntry_editEntryPanel_closeImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popupcontrol_edit_dictionary_entry_inner_bg" style="width: 100%" align="left"
                                                    valign="top" colspan="2">
                                                    <table border="0" cellpadding="2" cellspacing="5" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="2" cellspacing="3" width="100%" class="tab_body_bg"
                                                                    align="left">
                                                                    <tr>
                                                                        <td class="msg_align" colspan="3">
                                                                            <asp:Label ID="CertificationTitleEntry_editEntryPanel_errorMessageLabel" runat="server"
                                                                                SkinID="sknErrorMessage"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 8%">
                                                                            <asp:Label ID="CertificationTitleEntry_editEntryPanel_aliasLabel" runat="server"
                                                                                Text="Alias" SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">*</span>
                                                                        </td>
                                                                        <td style="width: 91%">
                                                                            <asp:TextBox runat="server" ID="CertificationTitleEntry_editEntryPanel_certificationTitleTextBox"
                                                                                Width="98%" MaxLength="255"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 1%">
                                                                            <asp:ImageButton ID="CertificationTitleEntry_editEntryPanel_aliasHelpImageButton"
                                                                                SkinID="sknHelpImageButton" runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                                ToolTip="Enter or modify the alias here" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5" colspan="2">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top">
                                                <td colspan="2">
                                                    <asp:Button ID="CertificationTitleEntry_editEntryPanel_updateButton" runat="server"
                                                        Text="Update" SkinID="sknButtonId" OnClick="CertificationTitleEntry_editEntryPanel_updateButton_Click" />
                                                    <asp:LinkButton ID="CertificationTitleEntry_editEntryPanel_cancelButton" runat="server"
                                                        Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5" colspan="2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="CertificationTitleEntry_editEntryPanelModalPopupExtender"
                            runat="server" TargetControlID="CertificationTitleEntry_editEntryPanel_hiddenButton"
                            PopupControlID="CertificationTitleEntry_editEntryPanel" BackgroundCssClass="modalBackground"
                            CancelControlID="CertificationTitleEntry_editEntryPanel_cancelButton">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="CertificationTitleEntry_deleteEntryUpdatePanel" runat="server"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="CertificationTitleEntry_deleteEntryPanel" runat="server" Style="display: none;
                            height: 202px" CssClass="dictionary_entry_delete_confirm">
                            <div style="display: none">
                                <asp:Button ID="CertificationTitleEntry_deleteEntryPanel_hiddenButton" runat="server" />
                            </div>
                            <uc1:ConfirmMsgControl ID="CertificationTitleEntry_deleteEntryPanel_confirmMessageControl"
                                runat="server" OnOkClick="CertificationTitleEntry_deleteEntryPanel_okButtonClick" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="CertificationTitleEntry_deleteEntryPanel_confirmModalPopupExtender"
                            runat="server" PopupControlID="CertificationTitleEntry_deleteEntryPanel" TargetControlID="CertificationTitleEntry_deleteEntryPanel_hiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
