﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchTimeSlots.aspx.cs
// Class that represents the user interface layout and functionalities for
// the SearchTimeSlots page. This will helps to view and select accepted
// time slots. Additionally it helps to view pending and rejected time slots
// too. This class inherits Forte.HCM.UI.Common.PageBase.

#endregion Header

#region Directives                                                             

using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the SearchTimeSlots page. This will helps to view and select accepted
    /// time slots. Additionally it helps to view pending and rejected time 
    /// slots too. This class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class SearchTimeSlots : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Select Time Slots");

                if (!IsPostBack)
                {
                    // Call and assign query string values to hidden fields.
                    if (RetrieveQueryStringValues() == false)
                        return;

                    // Add handler to email assessor image button.
                    SearchTimeSlots_emailImageButton.Attributes.Add("onclick",
                        "return ShowEmailAssessor('" + SearchTimeSlots_assessorIDHiddenField.Value + "','STS');");

                    // Assign handler to request timne slot image button.
                    SearchTimeSlots_requestTimeSlotImageButton.Attributes.Add("onclick",
                        "return ShowRequestSessionTimeSlot('" + SearchTimeSlots_assessorIDHiddenField.Value + 
                        "','" + SearchTimeSlots_skillIDHiddenField.Value + "','" + 
                        SearchTimeSlots_sessionKeyHiddenField.Value + "');");

                    // Load time slots.
                    LoadTimeSlots();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTimeSlots_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchTimeSlots_timeSlotsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    HiddenField SearchTimeSlots_timeSlotsGridView_requestStatusHiddenField = (HiddenField)e.Row.FindControl
                        ("SearchTimeSlots_timeSlotsGridView_requestStatusHiddenField");
                    HiddenField SearchTimeSlots_timeSlotsGridView_timeSlotIDHiddenField = (HiddenField)e.Row.FindControl
                        ("SearchTimeSlots_timeSlotsGridView_timeSlotIDHiddenField");
                    LinkButton SearchTimeSlots_timeSlotsGridView_selectLinkButton = (LinkButton)e.Row.FindControl
                        ("SearchTimeSlots_timeSlotsGridView_selectLinkButton");
                    bool isAvailable = Convert.ToBoolean(((HiddenField)e.Row.FindControl
                        ("SearchTimeSlots_timeSlotsGridView_isAvailabledHiddenField")).Value);

                    string requestStatus = string.Empty;
                    if (!Utility.IsNullOrEmpty(SearchTimeSlots_timeSlotsGridView_requestStatusHiddenField.Value))
                        requestStatus = SearchTimeSlots_timeSlotsGridView_requestStatusHiddenField.Value.Trim();

                    // Show the select link only if the time slot is accepted and the
                    // time slot is not already assigned to any candidate.
                    if (requestStatus == Constants.TimeSlotRequestStatus.ACCEPTED_CODE && isAvailable == true)
                        SearchTimeSlots_timeSlotsGridView_selectLinkButton.Visible = true;
                    else
                        SearchTimeSlots_timeSlotsGridView_selectLinkButton.Visible = false;

                    // Check if the time slot ID is already selected in the
                    // online interview schedule page.
                    if (Session["ONLINE_INTERVIEW_SCHEDULER_SELECTED_TIME_SLOTS"] != null)
                    {
                        List<AssessorTimeSlotDetail> selectedTimeSlots = Session
                            ["ONLINE_INTERVIEW_SCHEDULER_SELECTED_TIME_SLOTS"] as List<AssessorTimeSlotDetail>;

                        // Check if the time slot ID present in the list.
                        if (selectedTimeSlots.FindIndex(delegate(AssessorTimeSlotDetail id)
                        {
                            return id.TimeSlotID == Convert.ToInt32(SearchTimeSlots_timeSlotsGridView_timeSlotIDHiddenField.Value); }
                            ) != -1)
                        {
                            // Disable the select link.
                            SearchTimeSlots_timeSlotsGridView_selectLinkButton.Enabled = false;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTimeSlots_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void SearchTimeSlots_timeSlotsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "SelectSlot")
            {
                int timeSlotId = 0;
                if (!Utility.IsNullOrEmpty(e.CommandArgument))
                    timeSlotId = Convert.ToInt32(e.CommandArgument);

                AssessorTimeSlotDetail timeSlot = new AssessorTimeSlotDetail();
                timeSlot.TimeSlotID = timeSlotId;
                timeSlot.AssessorID = Convert.ToInt32(Request.QueryString["assessorid"].ToString());
                timeSlot.SkillID = Convert.ToInt32(Request.QueryString["skillid"].ToString());

                List<AssessorTimeSlotDetail> timeSlots = null;

                if (Session["ONLINE_INTERVIEW_SCHEDULER_SELECTED_TIME_SLOTS"] == null)
                    timeSlots = new List<AssessorTimeSlotDetail>();
                else
                    timeSlots = Session["ONLINE_INTERVIEW_SCHEDULER_SELECTED_TIME_SLOTS"] as List<AssessorTimeSlotDetail>;

                // Add the time slot to the list.
                timeSlots.Add(timeSlot);

                // Keep the time slots in session.
                Session["ONLINE_INTERVIEW_SCHEDULER_SELECTED_TIME_SLOTS"] = timeSlots;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseTimeSlots",
                    "<script language='javascript'>self.close();</script>", false);
            }
        }
        /// <summary>
        /// Handler method that is called when the critera radio button checked
        /// state is changed.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        /// <remarks>
        /// This will loads the time slots based on the selected status.
        /// </remarks>
        protected void SearchTimeSlots_timeSlotsStatusRadioButton_CheckedChanged
            (object sender, EventArgs e)
        {
            try
            {
                // Load time slots.
                LoadTimeSlots();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTimeSlots_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that retrieves the query string values and assign it to 
        /// hidden fields.
        /// </summary>
        private bool RetrieveQueryStringValues()
        {
            // Check if assessor ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["assessorid"]))
            {
                base.ShowMessage(SearchTimeSlots_topErrorMessageLabel, "Assessor ID is not passed");
                return false;
            }

            // Check if assessor ID is valid.
            int assessorID = 0;
            int.TryParse(Request.QueryString["assessorid"], out assessorID);

            if (assessorID == 0)
            {
                base.ShowMessage(SearchTimeSlots_topErrorMessageLabel, "Invalid assessor ID");
                return false;
            }

            // Assign assessor ID.
            SearchTimeSlots_assessorIDHiddenField.Value = assessorID.ToString();

            // Check if reference key is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["referencekey"]))
            {
                base.ShowMessage(SearchTimeSlots_topErrorMessageLabel, "Reference key is passed");
                return false;
            }

            // Assign session key.
            SearchTimeSlots_sessionKeyHiddenField.Value = Request.QueryString["referencekey"];

            // Check if skill ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["skillid"]))
            {
                base.ShowMessage(SearchTimeSlots_topErrorMessageLabel, "Skill ID is not passed");
                return false;
            }

            // Check if skill ID is valid.
            int skillID = 0;
            int.TryParse(Request.QueryString["skillid"], out skillID);

            if (skillID == 0)
            {
                base.ShowMessage(SearchTimeSlots_topErrorMessageLabel, "Invalid skill ID");
                return false;
            }

            // Assign skill ID.
            SearchTimeSlots_skillIDHiddenField.Value = skillID.ToString();

            return true;
        }

        /// <summary>
        /// Method that loads the time slots for the given parameters.
        /// </summary>
        private void LoadTimeSlots()
        {
            // Get assessor combined info.
            AssessorDetail assessorDetail = new AssessorBLManager().
                GetAssessorCombinedInfo(Convert.ToInt32(SearchTimeSlots_assessorIDHiddenField.Value), 
                Convert.ToInt32(SearchTimeSlots_skillIDHiddenField.Value));

            if (assessorDetail == null)
            {
                base.ShowMessage(SearchTimeSlots_topErrorMessageLabel, "Invalid assessor ID");
            }
            else
            {
                // Assign details.
                SearchTimeSlots_assessorNameValueLabel.Text = assessorDetail.FirstName;
                if (!Utility.IsNullOrEmpty(assessorDetail.LastName))
                {
                    SearchTimeSlots_assessorNameValueLabel.Text = SearchTimeSlots_assessorNameValueLabel.Text +
                        " " + assessorDetail.LastName;
                }
                SearchTimeSlots_skillValueLabel.Text = assessorDetail.Skill;
            }

            // Get request status.
            string requestStatus = string.Empty;
            if (SearchTimeSlots_showAcceptedTimeSlotsRadioButton.Checked)
                requestStatus = Constants.TimeSlotRequestStatus.ACCEPTED_CODE;
            else if (SearchTimeSlots_showRejectedTimeSlotsRadioButton.Checked)
                requestStatus = Constants.TimeSlotRequestStatus.REJECTED_CODE;
            else if (SearchTimeSlots_showPendingTimeSlotsRadioButton.Checked)
                requestStatus = Constants.TimeSlotRequestStatus.PENDING_CODE;

            List<AssessorTimeSlotDetail> timeSlots = new AssessorBLManager().
                GetProcesssedTimeSlots(Convert.ToInt32(SearchTimeSlots_assessorIDHiddenField.Value),
                    SearchTimeSlots_sessionKeyHiddenField.Value, 
                    Convert.ToInt32(SearchTimeSlots_skillIDHiddenField.Value), requestStatus);

            // Assign data source.
            SearchTimeSlots_timeSlotsGridView.DataSource = timeSlots;
            SearchTimeSlots_timeSlotsGridView.DataBind();
        }

        #endregion Private Methods
       
}
}