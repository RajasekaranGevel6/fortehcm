﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="CandidateActivitySummary.aspx.cs" Inherits="Forte.HCM.UI.Popup.CandidateActivitySummary"%>
    
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/CandidateActivityViewerControl.ascx" TagName="CandidateActivityViewerControl"
    TagPrefix="uc1" %>
<asp:Content ID="CandidateActivitySummary_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">

    <script type="text/javascript" language="javascript">

     // Method that opens the add notes popup.
     // positionProfileID - Refers to the position profile ID
     // candidateID - Refers to the candidate ID (CandidateInformation table).
     function OpenPositionProfileAddNotesPopupWithRefresh(positionProfileID, candidateID)
     {
         var height = 260;
         var width = 620;
         var top = (screen.availHeight - parseInt(height)) / 2;
         var left = (screen.availWidth - parseInt(width)) / 2;
         var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:"
                + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

         var queryStringValue = "../Popup/AddNotes.aspx" +
            "?candidateid=" + candidateID +
            "&positionprofileid=" + positionProfileID;

         window.open(queryStringValue, window.self, sModalFeature);

         document.getElementById('<%=CandidateActivitySummary_refreshHiddenField.ClientID %>').value = 'Y';
         
         __doPostBack('<%=CandidateActivitySummary_refreshLinkButton.ClientID %>', "OnClick");
     }

 </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="CandidateActivitySummary_headerLiteral" runat="server" Text="Candidate Activity Log"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="CandidateActivitySummary_topCancelImageButton" ToolTip="Click here to close the window"
                                runat="server" SkinID="sknCloseImageButton" OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:Label ID="CandidateActivitySummary_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                        <asp:Label ID="CandidateActivitySummary_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:Label ID="CandidateActivitySummary_positionProfileNameLabel" runat="server"
                                                        Text="Position Profile Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 85%">
                                                    <asp:Label ID="CandidateActivitySummary_positionProfileNameValueLabel" runat="server"
                                                        Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:Label ID="CandidateActivitySummary_candidateNameLabel" runat="server" Text="Candidate Name"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 85%">
                                                    <asp:Label ID="CandidateActivitySummary_candidateNameValueLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="header_bg">
                                        <asp:Literal ID="CandidateActivitySummary_activitiesHeaderLabel" runat="server" Text="Activities"
                                            SkinID="sknLabelText"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <div id="CandidateActivitySummary_activityLogDetailDIV" runat="server" style="height: 232px;
                                            overflow: auto;">
                                            <asp:GridView ID="CandidateActivitySummary_activityLogDetailGridView" runat="server"
                                                AllowSorting="false" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Activity Date" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="CandidateActivitySummary_activityLogDetailGridView_activityDateLabel"
                                                                runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("ActivityDate")))%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Activity By" HeaderStyle-Width="18%" ItemStyle-Width="18%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="CandidateActivitySummary_activityLogDetailGridView_activityByLabel"
                                                                runat="server" Text='<%# Eval("ActivityByName") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Activity" ItemStyle-Width="18%" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-Width="18%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="CandidateActivitySummary_activityLogDetailGridView_activityTypeLabel"
                                                                runat="server" Text='<%# Eval("ActivityTypeName") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Comments" ItemStyle-Width="32%" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-Width="32%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="CandidateActivitySummary_activityLogDetailGridView_commentsLabel"
                                                                runat="server" Text='<%# Eval("Comments") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_left_8">
                <table>
                    <tr>
                        <td>
                            <asp:LinkButton ID="CandidateActivitySummary_addNotesLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Add Notes" OnClick="CandidateActivitySummary_addNotesLinkButton_Click"
                                ToolTip="Click here to add notes"></asp:LinkButton>
                        </td>
                        <td class="pop_divider_line">
                            |
                        </td>
                        <td>
                            <asp:LinkButton ID="CandidateActivitySummary_downloadLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Download" OnClick="CandidateActivitySummary_downloadLinkButton_Click" ToolTip="Click here to download the activity log"></asp:LinkButton>
                        </td>
                        <td class="pop_divider_line">
                            |
                        </td>
                        <td>
                            <asp:LinkButton ID="CandidateActivitySummary_refreshLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Refresh" OnClick="CandidateActivitySummary_refreshLinkButton_Click" ToolTip="Click here to refresh the activity log"></asp:LinkButton>
                                <asp:HiddenField ID="CandidateActivitySummary_refreshHiddenField" runat="server" />
                        </td>
                        <td class="pop_divider_line">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="CandidateActivitySummary_cancelLinkButton" ToolTip="Click here to close the window"
                                runat="server" Text="Cancel" SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
