﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.BL;
using Forte.HCM.UI.CommonControls;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using System.IO;
using System.Web.UI.HtmlControls;
using Forte.HCM.Utilities;
using Forte.HCM.EventSupport;
using System.Text;
using System.Data;
using OfficeOpenXml;
using System.Security.Cryptography;

namespace Forte.HCM.UI.Popup
{
    /// <summary> 
    /// Class that defines the user interface layout and functionalities for
    /// BatchcandidateEntry page. This page helps to insert bulk of candidates in 
    /// to the database.      
    /// </summary>
    public partial class BulkCandidateUpload : PageBase
    {
        #region Events Handlers
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set default button and focus
                Page.Form.DefaultButton = BulkScheduleCandidates_uploadButton.UniqueID;

                Page.Form.DefaultFocus = BulkScheduleCandidates_fileUpload.UniqueID;

                BulkScheduleCandidates_fileUpload.Focus();

                Master.SetPageCaption("Bulk Schedule Candidate");

                if (!IsPostBack)
                {
                    if (Request.QueryString["tKey"] != null && Request.QueryString["expDate"] != null)
                    {
                        ViewState["TEST_SESSION_KEY"] = Request.QueryString["tKey"].ToString();
                        ViewState["TEST_SESSION_EXPDATE"] = Request.QueryString["expDate"].ToString();
                    }
                    //Assign the page number in viewstate
                    ViewState["PAGE_NUMBER"] = 1;

                    UserDetail userDetail = new CommonBLManager().GetUserDetail(userID);

                    Session["USERNAME"] = userDetail.FirstName;

                    // Add handler to download template.
                    BulkScheduleCandidates_downloadTemplateLinkButton.Attributes.Add
                        ("onclick", "javascript:DownloadBulkScheduleCandidateTemplate();");
                }
                BulkScheduleCandidates_topSuccessMessageLabel.Attributes["Tag"] = "0";

                BulkScheduleCandidates_topErrorMessageLabel.Text = string.Empty;
                BulkScheduleCandidates_topSuccessMessageLabel.Text = string.Empty;
                BulkScheduleCandidates_bottomSuccessMessageLabel.Text = string.Empty;
                BulkScheduleCandidates_bottomErrorMessageLabel.Text = string.Empty;

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
                    BulkScheduleCandidates_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when page number is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event data.
        /// </param>
        //void BulkScheduleCandidates_pageNavigator_PageNumberClick
        //    (object sender, PageNumberEventArgs e)
        //{
        //    try
        //    {
        //        UpdateOldInvalidCandidatesToSession(Convert.ToInt32(ViewState["INVALID_CANDIDATES_PAGE_NUMBER"]));
        //        ViewState["INVALID_CANDIDATES_PAGE_NUMBER"] = e.PageNumber;
        //        BindInvalidCandidates(e.PageNumber);
        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.ExceptionLog(exception);

        //        // Show error messages to the user
        //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
        //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
        //    }
        //}

        /// <summary>
        /// Handler method that will be called on datalist row data bound
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event data.
        /// </param>
        //protected void BulkScheduleCandidates_candidatesDataList_RowDataBound
        //    (object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Row.RowType != DataControlRowType.DataRow)
        //            return;
        //        LinkButton BulkScheduleCandidates_qstImageLinkButton =
        //            (LinkButton)e.Row.FindControl("BulkScheduleCandidates_qstImageLinkButton");
        //        HtmlTableRow tr = (HtmlTableRow)e.Row.FindControl("BulkScheduleCandidates_imageLinkTR");
        //        if (BulkScheduleCandidates_qstImageLinkButton.Visible)
        //        {

        //            string xlsFilePath = ViewState["PATH_NAME"].ToString();
        //            string imageAbsolutePath = Path.Combine("../BatchcandidateExcels/", Path.GetFileNameWithoutExtension(xlsFilePath)) + "/" + BulkScheduleCandidates_qstImageLinkButton.Text;
        //            BulkScheduleCandidates_qstImageLinkButton.Attributes.Add("onclick",
        //                "javascript: return ShowcandidateImage('" + imageAbsolutePath + "')");

        //            tr.Style.Add("display", "");
        //        }
        //        else
        //            tr.Style.Add("display", "none");


        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.ExceptionLog(exception);

        //        // Show error messages to the user
        //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
        //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
        //    }
        //}
        /// <summary>
        /// Handler method that will be called on datalist row data bound
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event data.
        /// </param>
        //protected void BulkScheduleCandidates_inCompletecandidatesDataList_RowDataBound
        //    (object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Row.RowType != DataControlRowType.DataRow)
        //            return;
        //        HiddenField BulkScheduleCandidates_testAreaHiddenField =
        //            (HiddenField)e.Row.FindControl("BulkScheduleCandidates_testAreaHiddenField");
        //        DropDownList BulkScheduleCandidates_testAreaDropDownList =
        //            (DropDownList)e.Row.FindControl("BulkScheduleCandidates_testAreaDropDownList");
        //        DropDownList BulkScheduleCandidates_complexityDropDownList =
        //            (DropDownList)e.Row.FindControl("BulkScheduleCandidates_complexityDropDownList");
        //        ImageButton BulkScheduleCandidates_complexityImageButton =
        //            (ImageButton)e.Row.FindControl("BulkScheduleCandidates_complexityImageButton");
        //        HiddenField BulkScheduleCandidates_complexityHiddenField =
        //             (HiddenField)e.Row.FindControl("BulkScheduleCandidates_complexityDropDownListHiddenField");

        //        if (BulkScheduleCandidates_testAreaDropDownList != null)
        //        {
        //            BulkScheduleCandidates_testAreaDropDownList.DataSource =
        //       new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
        //            BulkScheduleCandidates_testAreaDropDownList.DataTextField = "AttributeName";
        //            BulkScheduleCandidates_testAreaDropDownList.DataValueField = "AttributeID";
        //            BulkScheduleCandidates_testAreaDropDownList.DataBind();
        //            BulkScheduleCandidates_testAreaDropDownList.Items.Insert(0, "--Select--");

        //            BulkScheduleCandidates_testAreaDropDownList.ClearSelection();
        //            if (BulkScheduleCandidates_testAreaDropDownList.Items.FindByText(
        //                BulkScheduleCandidates_testAreaHiddenField.Value.Trim()) != null)
        //                BulkScheduleCandidates_testAreaDropDownList.Items.FindByText(
        //                    BulkScheduleCandidates_testAreaHiddenField.Value.Trim()).Selected = true;
        //        }
        //        if (BulkScheduleCandidates_complexityDropDownList != null)
        //        {
        //            BulkScheduleCandidates_complexityDropDownList.DataSource =
        //                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
        //            BulkScheduleCandidates_complexityDropDownList.DataTextField = "AttributeName";
        //            BulkScheduleCandidates_complexityDropDownList.DataValueField = "AttributeID";
        //            BulkScheduleCandidates_complexityDropDownList.DataBind();
        //            BulkScheduleCandidates_complexityDropDownList.Items.Insert(0, "--Select--");

        //            BulkScheduleCandidates_complexityDropDownList.ClearSelection();

        //            if (BulkScheduleCandidates_complexityDropDownList.Items.FindByText(
        //                BulkScheduleCandidates_complexityHiddenField.Value.Trim()) != null)
        //                BulkScheduleCandidates_complexityDropDownList.Items.FindByText(
        //                    BulkScheduleCandidates_complexityHiddenField.Value.Trim()).Selected = true;
        //        }

        //        ImageButton BulkScheduleCandidates_categoryImageButton = ((ImageButton)e.Row.FindControl
        //            ("BulkScheduleCandidates_categoryImageButton"));
        //        TextBox BulkScheduleCandidates_categoryTextBox = ((TextBox)e.Row.FindControl
        //            ("BulkScheduleCandidates_categoryTextBox"));
        //        TextBox BulkScheduleCandidates_subjectTextBox = ((TextBox)e.Row.FindControl
        //            ("BulkScheduleCandidates_subjectTextBox"));
        //        HiddenField BulkScheduleCandidates_subjectIdHiddenField = ((HiddenField)e.Row.FindControl
        //            ("BulkScheduleCandidates_subjectIdHiddenField"));
        //        BulkScheduleCandidates_categoryImageButton.Attributes.Add("onclick",
        //            "return LoadCategorySubjectLookUpforReadOnly('" + BulkScheduleCandidates_categoryTextBox.ClientID +
        //            "','" + BulkScheduleCandidates_subjectTextBox.ClientID + "','"
        //                + BulkScheduleCandidates_subjectIdHiddenField.ClientID + "','" +
        //                ((HiddenField)e.Row.FindControl("BulkScheduleCandidates_categoryNameHiddenField")).ClientID + "','" +
        //                ((HiddenField)e.Row.FindControl("BulkScheduleCandidates_subjectNameHiddenField")).ClientID + "');");

        //        ImageButton BulkScheduleCandidates_authorImageButton = ((ImageButton)e.Row.FindControl
        //            ("BulkScheduleCandidates_authorImageButton"));
        //        TextBox BulkScheduleCandidates_authorTextBox = ((TextBox)e.Row.FindControl
        //            ("BulkScheduleCandidates_authorTextBox"));
        //        HiddenField BulkScheduleCandidates_authorHiddenField = ((HiddenField)e.Row.FindControl
        //            ("BulkScheduleCandidates_authorHiddenField"));
        //        BulkScheduleCandidates_authorImageButton.Attributes.Add("onclick",
        //            "return LoadUserName('" + BulkScheduleCandidates_authorHiddenField.ClientID + "','"
        //            + BulkScheduleCandidates_authorHiddenField.ClientID + "','"
        //            + BulkScheduleCandidates_authorTextBox.ClientID + "')");
        //        BulkScheduleCandidates_authorTextBox.Attributes.Add("onclick", "javascript:return false;");
        //        BulkScheduleCandidates_authorTextBox.Attributes.Add("onkeydown", "javascript:return false;");

        //        HiddenField BulkScheduleCandidates_qstImageHiddenField = ((HiddenField)e.Row.FindControl
        //            ("BulkScheduleCandidates_qstImageHiddenField"));

        //        string xlsFilePath = ViewState["PATH_NAME"].ToString();
        //        string imageAbsolutePath = Path.Combine("/BatchcandidateExcels/", Path.GetFileNameWithoutExtension(xlsFilePath)) + "/" + BulkScheduleCandidates_qstImageHiddenField.Value;

        //        LinkButton BulkScheduleCandidates_addcandidateImageLinkButton =
        //            (LinkButton)e.Row.FindControl("BulkScheduleCandidates_addcandidateImageLinkButton");
        //        BulkScheduleCandidates_addcandidateImageLinkButton.Attributes.Add
        //            ("onclick", "javascript:return ShowcandidateImagePopup('" + imageAbsolutePath + "','" + BulkScheduleCandidates_addcandidateImageLinkButton.ClientID+ "');");

        //        HtmlTableRow BulkScheduleCandidates_addImageLinkTR =
        //            (HtmlTableRow)e.Row.FindControl("BulkScheduleCandidates_addImageLinkTR");

        //        if(BulkScheduleCandidates_addcandidateImageLinkButton.Visible)
        //        {
        //            BulkScheduleCandidates_addImageLinkTR.Style.Add("display", "");
        //        }
        //        else
        //            BulkScheduleCandidates_addImageLinkTR.Style.Add("display", "none");
        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.ExceptionLog(exception);

        //        // Show error messages to the user
        //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
        //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
        //    }
        //}

        /// <summary>
        /// Handler method that is called when upload button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void BulkScheduleCandidates_uploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear the candidates in the valid candidate sessions
                Session["VALID_CANDIDATES"] = null;

                //Clear the candidates in the invalid candidate sessions
                Session["INVALID_CANDIDATES"] = null;

                //Make the total candidate count as 0
                ViewState["TOTAL_CANDIDATES_COUNT"] = 0;

                //Make the page number as 1
                ViewState["PAGE_NUMBER"] = 1;

                //Clear the path name from view state
                ViewState["PATH_NAME"] = string.Empty;

                //Save file in the server and get the file name
                string fileName = SaveFileAs();

                //If file name is empty return
                if (fileName == string.Empty)
                    return;

                //Save the file name in viewstate
                ViewState["PATH_NAME"] = fileName;

                //Get the sheet name and the total candidate in each sheet
                GetSheetName(fileName);

                //Load the candidates from excel
                LoadExcelFile(fileName);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
                    BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);

                BulkScheduleCandidates_uploadExcelDiv.Style["display"] = "block";

                // BulkScheduleCandidates_resultDiv.Style["display"] = "none";

                //BulkScheduleCandidates_resultDiv.Visible = false;
                BulkScheduleCandidates_uploadExcelDiv.Visible = true;
            }
        }

        /// <summary>
        /// This method is called to extract the images in BatchcandidateExcels folder
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        private void ExtractcandidateImages(string xlsFilePath)
        {
            string zipFileName = Path.GetFileNameWithoutExtension(xlsFilePath);
            string zipFilePath = Path.Combine(Path.GetDirectoryName(xlsFilePath), zipFileName);
            Directory.CreateDirectory(zipFilePath);

        }



        /// <summary>
        /// Handler that will be called when the datalist is binded
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        //protected void BulkScheduleCandidates_subject_categoryDataList_ItemDataBound
        //    (object sender, DataListItemEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Item.ItemType != ListItemType.Item
        //           && e.Item.ItemType != ListItemType.AlternatingItem)
        //            return;

        //        RadioButton selectRadioButton = (RadioButton)
        //            e.Item.FindControl("BulkScheduleCandidates_selectRadioButton");

        //        //To add a javascript that will make only one item selectable
        //        selectRadioButton.Attributes.Add("onclick", "return CheckOtherIsCheckedByGVID(this,'" +
        //            e.Item.Parent.ClientID + "')");
        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.ExceptionLog(exception);

        //        // Show error messages to the user
        //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
        //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
        //    }
        //}

        /// <summary>
        /// Handler that will be called on subjectcategorycandidateDataList is binded
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        //protected void BulkScheduleCandidates_subjectcategorycandidateDataList_ItemDataBound
        //    (object sender, DataListItemEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
        //            return;

        //        RadioButton Subject_selectcandidateRadioButton = (RadioButton)
        //            e.Item.FindControl("BulkScheduleCandidates_selectcandidateRadioButton");

        //        Subject_selectcandidateRadioButton.Attributes.
        //            Add("onclick", "return CheckOtherIsCheckedByGVID(this,'" +
        //            e.Item.Parent.ClientID + "')");
        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.ExceptionLog(exception);

        //        // Show error messages to the user
        //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
        //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
        //    }
        //}

        /// <summary>
        /// Handler method that is called when the next link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// 
        //protected void BulkScheduleCandidates_nextLinkButton_Click
        //    (object sender, EventArgs e)
        //{
        //    try
        //    {
        //        UpdateOldInvalidCandidatesToSession(Convert.ToInt32(ViewState["INVALID_CANDIDATES_PAGE_NUMBER"]));
        //        LoadNextBatchcandidates();
        //        ViewState["INVALID_CANDIDATES_PAGE_NUMBER"] = 1;
        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.ExceptionLog(exception);

        //        // Show error messages to the user
        //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
        //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
        //    }
        //}

        /// <summary>
        /// Handler method that is called on row command on BulkScheduleCandidates_candidateGridView
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewCommandEventArgs"/> that holds the event data
        /// </param>
        //protected void BulkScheduleCandidates_candidateGridView_RowCommand
        //    (object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName == "Deletecandidate")
        //        {
        //            BulkScheduleCandidates_confirmMsgControl_messageValidLabel.Text =
        //                Resources.HCMResource.BulkScheduleCandidates_SinglecandidateDelete;

        //            BulkScheduleCandidates_deletepopupExtender.Show();

        //            BulkScheduleCandidates_candidateIDHiddenField.Value = "V" + e.CommandArgument.ToString();
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.ExceptionLog(exception);

        //        // Show error messages to the user
        //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
        //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
        //    }
        //}

        /// <summary>
        /// Handler that is called on row command on
        /// BulkScheduleCandidates_inCompletecandidatesGridView
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewCommandEventArgs"/>that holds the event args
        /// </param>
        //protected void BulkScheduleCandidates_inCompletecandidatesGridView_RowCommand
        //    (object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName == "Deletecandidate")
        //        {
        //            BulkScheduleCandidates_confirmMsgControl_messageValidLabel.Text =
        //                Resources.HCMResource.BulkScheduleCandidates_SinglecandidateDelete;

        //            BulkScheduleCandidates_deletepopupExtender.Show();

        //            BulkScheduleCandidates_candidateIDHiddenField.Value = "I" + e.CommandArgument.ToString();
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.ExceptionLog(exception);

        //        // Show error messages to the user
        //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
        //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
        //    }
        //}

        /// <summary>
        /// Handler that is called when yes button of the confirm msg is clicked to 
        /// delete a candidate or set of candidates
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event args
        /// </param>
        //protected void BulkScheduleCandidates_confirmMsgControl_yesValidButton_Click
        //    (object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //Get the candidates details as in the grid 
        //        Session["VALID_CANDIDATES"] = saveBatchcandidate();

        //        //Checks which candidate has to be removed.
        //        //V denotes to remove a valid single candidate
        //        if (BulkScheduleCandidates_candidateIDHiddenField.Value.ToString().Substring(0, 1) == "V")
        //        {
        //            List<CandidateInformation> candidates = Session["VALID_CANDIDATES"] as List<CandidateInformation>;
        //            CandidateInformation candidateDetail = candidates.Find(delegate(CandidateInformation candidate)
        //            {
        //                return candidate.candidateID.ToString() == BulkScheduleCandidates_candidateIDHiddenField.Value.ToString().Substring(1);
        //            });

        //            candidates.Remove(candidateDetail);
        //            BulkScheduleCandidates_candidateGridView.DataSource = candidates;
        //            BulkScheduleCandidates_candidateGridView.DataBind();
        //            Session["VALID_CANDIDATES"] = candidates;
        //        }

        //        //I denotes the option to delete a invalid single candidate
        //        else if (BulkScheduleCandidates_candidateIDHiddenField.Value.
        //            ToString().Substring(0, 1) == "I")
        //        {
        //            List<CandidateInformation> candidates = GetCurrentInvalidcandidates();

        //            CandidateInformation candidateDetail = candidates.Find(delegate(CandidateInformation candidate)
        //            {
        //                return candidate.candidateID.ToString() == BulkScheduleCandidates_candidateIDHiddenField.Value.ToString().Substring(1);
        //            });

        //            candidates.Remove(candidateDetail);

        //            Session["INVALID_CANDIDATES"] = candidates;

        //            BulkScheduleCandidates_pageNavigator.Reset();

        //            ViewState["INVALID_CANDIDATES_PAGE_NUMBER"] = 1;

        //            BindInvalidCandidates(1);

        //            BulkScheduleCandidates_pageNavigator.TotalRecords = candidates.Count;
        //            BulkScheduleCandidates_pageNavigator.PageSize = GridPageSize;
        //        }
        //        //AV denote the option to delet all valid candidates
        //        else if (BulkScheduleCandidates_candidateIDHiddenField.Value == "AV")
        //        {
        //            List<CandidateInformation> candidates = Session["VALID_CANDIDATES"] as List<CandidateInformation>;

        //            candidates.Clear();

        //            BulkScheduleCandidates_candidateGridView.DataSource = candidates;

        //            BulkScheduleCandidates_candidateGridView.DataBind();

        //            Session["VALID_CANDIDATES"] = candidates;

        //            LoadNextBatchcandidates();
        //        }
        //        //AI denote the option to delete all invalid candidates      
        //        else if (BulkScheduleCandidates_candidateIDHiddenField.Value == "AI")
        //        {
        //            List<CandidateInformation> candidates = Session["INVALID_CANDIDATES"] as List<CandidateInformation>;

        //            int lastNumber = int.Parse(ViewState["INVALID_CANDIDATES_PAGE_NUMBER"].ToString()) * GridPageSize;

        //            int index = lastNumber - GridPageSize;

        //            candidates.RemoveRange(index, BulkScheduleCandidates_inCompletecandidatesGridView.Rows.Count);
        //            Session["INVALID_CANDIDATES"] = candidates;

        //            BulkScheduleCandidates_pageNavigator.TotalRecords = candidates.Count;

        //            BulkScheduleCandidates_pageNavigator.PageSize = GridPageSize;

        //            BulkScheduleCandidates_pageNavigator.Reset();

        //            ViewState["INVALID_CANDIDATES_PAGE_NUMBER"] = 1;

        //            BindInvalidCandidates(1);

        //            if (candidates.Count != 0)
        //                return;

        //            LoadNextBatchcandidates();
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.ExceptionLog(exception);

        //        // Show error messages to the user
        //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
        //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
        //    }
        //}

        /// <summary>
        /// Handler method that is called when valid candidates,
        /// remove all link button is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        //protected void BulkScheduleCandidates_removecandidateLinkButton_Click
        //    (object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (BulkScheduleCandidates_candidateGridView.Rows.Count == 0)
        //        {
        //            BulkScheduleCandidates_candidatesTabPanel_errorMessageLabel.Text =
        //                    "There are no more records to remove";
        //            return;
        //        }

        //        BulkScheduleCandidates_candidateIDHiddenField.Value = "AV";

        //        BulkScheduleCandidates_confirmMsgControl_messageValidLabel.Text =
        //            Resources.HCMResource.BulkScheduleCandidates_MultiValidcandidateDelete;

        //        BulkScheduleCandidates_deletepopupExtender.Show();
        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.ExceptionLog(exception);
        //        // Show error messages to the user
        //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
        //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
        //    }
        //}

        /// <summary>
        /// Handler method that is called when invalid 
        /// candidates remove all button is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        //protected void BulkScheduleCandidates_removeInvalidcandidateLinkButton_Click
        //    (object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (BulkScheduleCandidates_inCompletecandidatesGridView.Rows.Count == 0)
        //        {
        //            BulkScheduleCandidates_inCompletecandidatesTabPanel_errorMessageLabel.Text =
        //                         "There are no more records to remove";
        //            return;
        //        }

        //        BulkScheduleCandidates_candidateIDHiddenField.Value = "AI";

        //        BulkScheduleCandidates_confirmMsgControl_messageValidLabel.Text =
        //               Resources.HCMResource.BulkScheduleCandidates_MultiInValidcandidateDelete;

        //        BulkScheduleCandidates_deletepopupExtender.Show();

        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.ExceptionLog(exception);

        //        // Show error messages to the user
        //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
        //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
        //    }
        //}

        /// <summary>
        /// Handler method that is called when save button is clicked 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        //protected void BulkScheduleCandidates_saveButton_Click
        //    (object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (BulkScheduleCandidates_candidateGridView.Rows.Count == 0)
        //        {
        //            base.ShowMessage(BulkScheduleCandidates_bottomErrorMessageLabel,
        //                BulkScheduleCandidates_topErrorMessageLabel,
        //            Resources.HCMResource.BulkScheduleCandidates_PleaseEnterAcandidate);
        //            return;
        //        }

        //        List<CandidateInformation> candidateDetails = new List<CandidateInformation>();

        //        //Get the candidate details from the grid
        //        candidateDetails = saveBatchcandidate();

        //        //Check whether the candidates details are valid
        //        if (CheckIsValidData(candidateDetails, "Valid"))
        //        {
        //            // Assign Disclaimer message to the disclaimer popup
        //            BulkScheduleCandidates_savePopupExtenderControl.Message =
        //                Resources.HCMResource.BulkScheduleCandidates_DisclaimerMessage;
        //            BulkScheduleCandidates_disclaimerSavepopupExtender.Show();
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.ExceptionLog(exception);

        //        // Show error messages to the user
        //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
        //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
        //    }
        //}

        /// <summary>
        /// Handler that is called when confirm message ok Button is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        //protected void BulkScheduleCandidates_savecandidateOkButton_Click
        //    (object sender, EventArgs e)
        //{
        //    try
        //    {
        //        BulkScheduleCandidates_isSavedHiddenField.Value = "1";

        //        if (BulkScheduleCandidates_saveRadioButton.Checked)
        //        {
        //            BulkScheduleCandidates_saveButton_Click(sender, e);
        //            return;
        //        }

        //        LoadNextBatchcandidates();

        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.ExceptionLog(exception);

        //        // Show error messages to the user
        //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
        //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
        //    }
        //}

        /// <summary>
        /// Handler method that is called when invalid 
        /// candidates save button is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        //protected void BulkScheduleCandidates_invalidSaveButton_Click
        //    (object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (BulkScheduleCandidates_inCompletecandidatesGridView.Rows.Count == 0)
        //        {
        //            base.ShowMessage(BulkScheduleCandidates_bottomErrorMessageLabel, BulkScheduleCandidates_topErrorMessageLabel, Resources.HCMResource.BulkScheduleCandidates_PleaseEnterAcandidate);
        //            return;
        //        }

        //        List<CandidateInformation> invalidcandidate = new List<CandidateInformation>();

        //        invalidcandidate = GetInvalidcandidate();

        //        if (CheckIsValidData(invalidcandidate, "Invalid"))
        //        {
        //            // Assign Disclaimer message to the disclaimer popup
        //            BulkScheduleCandidates_saveInvalidPopupExtenderControl.Message =
        //                Resources.HCMResource.BulkScheduleCandidates_DisclaimerMessage;
        //            BulkScheduleCandidates_disclaimerSaveInvalidpopupExtender.Show();
        //        }
        //        BulkScheduleCandidates_inCompletecandidatesGridView.DataSource = invalidcandidate;
        //        BulkScheduleCandidates_inCompletecandidatesGridView.DataBind();
        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.ExceptionLog(exception);

        //        // Show error messages to the user
        //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
        //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
        //    }
        //}

        /// <summary>
        /// Handler method that is called when save button is clicked        
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void DisclaimerControl_acceptButton_Click(object sender, EventArgs e)
        {
            //    try
            //    {
            //        List<CandidateInformation> candidateDetails = new List<CandidateInformation>();

            //        //Get the candidate details from the grid
            //        candidateDetails = saveBatchcandidate();

            //        //Check whether there is invalid candidates . if yes avtivate that tab


            //        //Save the candidates to the database
            //        SavecandidatesToDatabase(candidateDetails);

            //        //Assign the hidden value to 1, as the candidate is saved
            //        BulkScheduleCandidates_isSavedHiddenField.Value = "1";

            //        candidateDetails = new List<CandidateInformation>();

            //        BulkScheduleCandidates_candidateGridView.DataSource = candidateDetails;
            //        BulkScheduleCandidates_candidateGridView.DataBind();


            //        if (BulkScheduleCandidates_inCompletecandidatesGridView.Rows.Count > 0)
            //        {
            //            base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
            //           BulkScheduleCandidates_bottomErrorMessageLabel, "Incomplete candidates are still pending");
            //            BulkScheduleCandidates_mainTabContainer.ActiveTabIndex = 1;
            //        }
            //        else
            //        {
            //            //Load next set of candidates
            //            BulkScheduleCandidates_nextLinkButton_Click(sender, e);
            //        }
            //    }
            //    catch (Exception exception)
            //    {
            //        Logger.ExceptionLog(exception);
            //        // Show error messages to the user
            //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
            //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
            //    }
        }

        /// <summary>
        /// Handler method that is called when save button is clicked        
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void BulkScheduleCandidates_acceptInvalidCandidateButton_Click(object sender, EventArgs e)
        {
            //    try
            //    {
            //        List<CandidateInformation> candidateDetails = new List<CandidateInformation>();

            //        //Get the candidate details from the grid
            //        candidateDetails = GetInvalidcandidate();

            //        //Save the candidates to the database
            //        SavecandidatesToDatabase(candidateDetails);

            //        List<CandidateInformation> inValidcandidates = new List<CandidateInformation>();

            //        inValidcandidates = Session["INVALID_CANDIDATES"] as List<CandidateInformation>;

            //        foreach (CandidateInformation candidates in candidateDetails)
            //        {
            //            inValidcandidates.RemoveAll(delegate(CandidateInformation candidate)
            //            {
            //                return candidate.candidateID == candidates.candidateID;
            //            });
            //        }

            //        Session["INVALID_CANDIDATES"] = inValidcandidates;

            //        //Load next set of candidates
            //        LoadNextBatchcandidates();
            //    }
            //    catch (Exception exception)
            //    {
            //        Logger.ExceptionLog(exception);

            //        // Show error messages to the user
            //        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
            //            BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
            //    }
        }

        /// <summary>
        /// Handler method that is called when save button is clicked        
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void BulkScheduleCandidates_saveCandidateOkButton_Click(object sender, EventArgs e)
        {

        }

        #endregion Events Handlers

        #region Private Methods

        /// <summary>
        /// Method that used to save the batch candidates into the database
        /// </summary>
        /// <param name="CandidateInformation">
        /// A<see cref="List<CandidateInformation>"/>that holds the list of candidates
        /// </param>
        private void SavecandidatesToDatabase(List<CandidateInformation> candidateDetails)
        {
            // can be used in candidate list download
            string strUserIDs = "";
            List<CandidateInformation> savedCandidateInformationList = new List<CandidateInformation>();
            CandidateInformation savedCandidateInformation;
            foreach (CandidateInformation candidateInformation in candidateDetails)
            {
                // check whether candidate already exists in db.
                savedCandidateInformation = new CandidateInformation();
                int userID = new ResumeRepositoryBLManager().CheckIfUserExist(candidateInformation, base.tenantID);
                if (userID == 0)
                {
                    int candidateID = new ResumeRepositoryBLManager().InsertNewCandidate
                       (candidateInformation, base.userID, base.tenantID);
                    candidateInformation.caiID = candidateID;
                    // Save user.
                    string confirmationCode = GetConfirmationCode();
                    userID = new ResumeRepositoryBLManager().
                       InsertCandidateUsers(base.tenantID, candidateInformation,
                       base.userID, confirmationCode);

                    //SaveUserName(candidateInformation);
                }
                savedCandidateInformation.caiID = userID;
                savedCandidateInformation.caiEmail = candidateInformation.caiEmail;
                savedCandidateInformation.caiFirstName = candidateInformation.caiFirstName + " " + candidateInformation.caiLastName;
                savedCandidateInformationList.Add(savedCandidateInformation);

                strUserIDs = strUserIDs == "" ? userID.ToString() : (strUserIDs + "," + userID.ToString());
            }

            if (savedCandidateInformationList.Count > 0)
            {
                CreateCandidateSession(savedCandidateInformationList.Count);
                if (ViewState["CANDIDATE_SESSIONID"] != null)
                {
                    string SessionIDs = ViewState["CANDIDATE_SESSIONID"].ToString();
                    string[] candidateSessionIDs = SessionIDs.Split(',');

                    TestScheduleDetail testScheduleDetail;
                    string isCandidateAlreadyAssignedCandidateName1 = "";

                    for (int i = 0; i < savedCandidateInformationList.Count; i++)
                    {
                        string candidateName = new TestSchedulerBLManager().CheckCandidateAlreadyAssigned(ViewState["TEST_SESSION_KEY"].ToString(), savedCandidateInformationList[i].caiID.ToString());
                        if (candidateName == null || candidateName == "")
                        {
                            testScheduleDetail = new TestScheduleDetail();
                            testScheduleDetail.AttemptID = 1;
                            testScheduleDetail.CandidateID = savedCandidateInformationList[i].caiID.ToString().Trim();
                            testScheduleDetail.EmailId = savedCandidateInformationList[i].caiEmail.Trim();
                            testScheduleDetail.CandidateTestSessionID = candidateSessionIDs[i].Trim();
                            // Set the time to maximum in expiry date.
                            testScheduleDetail.ExpiryDate = Convert.ToDateTime(ViewState["TEST_SESSION_EXPDATE"].ToString()).Add(new TimeSpan(23, 59, 59));
                            testScheduleDetail.EmailReminder = "N";
                            testScheduleDetail.IsRescheduled = false;
                            bool isMailSent = false;
                            new TestSchedulerBLManager().ScheduleCandidate(testScheduleDetail, base.userID, base.tenantID, out isMailSent);
                        }
                        else
                            isCandidateAlreadyAssignedCandidateName1 = isCandidateAlreadyAssignedCandidateName1 == "" ? savedCandidateInformationList[i].caiFirstName :
                                (isCandidateAlreadyAssignedCandidateName1 + "," + savedCandidateInformationList[i].caiFirstName);
                    }

                    ViewState["CANDIDATE_USER_IDs"] = strUserIDs;
                    BulkScheduleCandidates_fileUploadTr.Style["display"] = "none";
                    BulkScheduleCandidates_fileUploadSuccessTr.Style["display"] = "block";
                    if (isCandidateAlreadyAssignedCandidateName1 != "")
                    {
                        BulkScheduleCandidates_topErrorMessageLabel.Style["display"] = "block";
                        base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
                          "Already Candidate " + isCandidateAlreadyAssignedCandidateName1 +
                          " has been scheduled in this test session");

                    }
                }
            }
        }



        /// <summary>
        /// create candidate sessions based on no of candidate session count .
        /// </summary>
        /// <param name="noOfCandidateSession">
        /// A <see cref="int"/> that contains the total number of candidate session has to be created.
        /// </param>
        /// <returns>
        ///
        /// </returns>
        private void CreateCandidateSession(int noOfCandidateSession)
        {
            string candidateSessionIDs = null;
            new TestBLManager().CreateCandidateTestSession(noOfCandidateSession, base.userID, ViewState["TEST_SESSION_KEY"].ToString(), out candidateSessionIDs);
            ViewState["CANDIDATE_SESSIONID"] = candidateSessionIDs;
            ViewState["ATTEMPT_ID"] = 1;
        }
        /// <summary>
        /// Saves the name of the user.
        /// </summary>
        private void SaveUserName(CandidateInformation candidateInformation)
        {
            string confirmationCode = GetConfirmationCode();
            int userID = new ResumeRepositoryBLManager().
                InsertCandidateUsers(base.tenantID, candidateInformation,
                base.userID, confirmationCode);
        }

        /// <summary>
        /// A Method that generates a random string and used 
        /// the encrypted string as confirmation code for a user
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that holds the encrypted string
        /// </returns>
        private string GetConfirmationCode()
        {
            return new EncryptAndDecrypt().EncryptString(RandomString(15));
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }
        /// <summary>
        /// This method gets the value from the grid view and updates
        /// the session of invalid candidate details
        /// </summary>
        /// <param name="PreviousPageNumber">
        /// A <see cref="integer"/> that contains the page number
        /// of previous records.
        /// </param>
        //private void UpdateOldInvalidCandidatesToSession(int PreviousPageNumber)
        //{
        //    List<CandidateInformation> inValidcandidates = null;
        //    inValidcandidates = Session["INVALID_CANDIDATES"] as List<CandidateInformation>;
        //    if (inValidcandidates == null || inValidcandidates.Count == 0)
        //        return;
        //    if (PreviousPageNumber == 0)
        //        PreviousPageNumber = 1;
        //    int firstRecord = (PreviousPageNumber * GridPageSize) - GridPageSize;
        //    if (BulkScheduleCandidates_inCompletecandidatesGridView.Rows.Count != 0)
        //    {
        //        inValidcandidates.RemoveRange(firstRecord,
        //            BulkScheduleCandidates_inCompletecandidatesGridView.Rows.Count);
        //        inValidcandidates.InsertRange(firstRecord, GetInvalidcandidate());
        //        Session["INVALID_CANDIDATES"] = null;
        //        Session["INVALID_CANDIDATES"] = inValidcandidates;
        //    }
        //}

        /// <summary>
        /// Method that is used to read the candidates from the excel
        /// </summary>
        /// <param name="fileName">
        /// A <see cref="string"/>that holds the file name
        /// </param>
        private void LoadExcelFile(string fileName)
        {
            if (Session["USERNAME"] == null)
            {
                ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
                    BulkScheduleCandidates_bottomErrorMessageLabel, Resources.HCMResource.BulkScheduleCandidates_InvalidUserName);

                return;
            }
            CandidateExcelBatchReader excelBatchReader = new CandidateExcelBatchReader(fileName);

            //ViewState["TOTAL_CANDIDATE_COUNT"] = excelBatchReader.GetCount("Sheet1$");

            List<CandidateInformation> candidateDetails =
             excelBatchReader.GetCandidates
             (int.Parse(ViewState["PAGE_NUMBER"].ToString()), GridPageSize,
             Session["USERNAME"].ToString().Trim(), ViewState["CURRENT_SHEET_NAME"].ToString(), userID, fileName);

            bool isInValidDataExists= GetValidCandidates(candidateDetails);

            if(!isInValidDataExists)            
                SavecandidatesToDatabase(candidateDetails);
        }

        /// <summary>
        /// Method that is used to separate valid and invalid 
        /// candidates taken from the excel.
        /// </summary>
        /// <param name="candidates">
        /// A<see cref="List<CandidateInformation>"/>
        /// that holds the list of candidates.
        /// </param>
        private bool GetValidCandidates(List<CandidateInformation> candidates)
        {
            List<CandidateInformation> validcandidates = new List<CandidateInformation>();

            List<CandidateInformation> inValidcandidates = new List<CandidateInformation>();

            //For the first time store the list of invalid candidates in session    
            if (Session["INVALID_CANDIDATES"] != null)
            {
                inValidcandidates = Session["INVALID_CANDIDATES"] as List<CandidateInformation>;
            }

            inValidcandidates.AddRange(candidates.FindAll(delegate(CandidateInformation candidate)
            {
                return candidate.caiIsValid = false;
            }));

            //validcandidates = candidates.FindAll(delegate(CandidateInformation candidate)
            //{
            //    return candidate.UserName != null || candidate.UserName != "" ||
            //       candidate.caiEmail != "" || candidate.caiEmail != null ||
            //       candidate.Password != null || candidate.Password != "" ||
            //       candidate.caiFirstName != "" || candidate.caiFirstName != null ||
            //        candidate.caiLastName != "" || candidate.caiLastName != null; ;
            //});

            ////BulkScheduleCandidates_candidateGridView.DataSource = validcandidates;
            ////BulkScheduleCandidates_candidateGridView.DataBind();
            //Session["VALID_CANDIDATES"] = validcandidates;


            Session["INVALID_CANDIDATES"] = inValidcandidates;
            //BulkScheduleCandidates_pageNavigator.TotalRecords = inValidcandidates.Count;
            //BulkScheduleCandidates_pageNavigator.PageSize = GridPageSize;

            //BulkScheduleCandidates_pageNavigator.Reset();

            if (inValidcandidates.Count == 0)
                return false;
            else
            {
                BindInvalidCandidates(1);
                return true;
            }
                     
        }

        /// <summary>
        /// Method that is used to bind the invalid candidates in the grid
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the current page number
        /// </param>
        private void BindInvalidCandidates(int pageNumber)
        {
            List<CandidateInformation> inValidcandidates = new List<CandidateInformation>();
            //int lastRecord = pageNumber * GridPageSize;
            //int firstRecord = lastRecord - GridPageSize;
            inValidcandidates = Session["INVALID_CANDIDATES"] as List<CandidateInformation>;
            if (inValidcandidates.Count == 0)
            {
                //BulkScheduleCandidates_inCompletecandidatesGridView.DataSource = inValidcandidates;
                //BulkScheduleCandidates_inCompletecandidatesGridView.DataBind();
                return;
            }
            BulkScheduleCandidates_resultDiv.Visible = true;
            //int remainingRecord = inValidcandidates.Count - ((pageNumber - 1) * GridPageSize);

            //lastRecord = lastRecord - firstRecord;

            //lastRecord = lastRecord > remainingRecord ?
            //    inValidcandidates.Count % GridPageSize : lastRecord;

            //Get the certain number of candidates from invalid candidates
            //BulkScheduleCandidates_inCompletecandidatesGridView.DataSource =
            //inValidcandidates.GetRange(firstRecord, lastRecord);

            ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
                   BulkScheduleCandidates_bottomErrorMessageLabel, "Unable to Schedule candidates to test session.Incompleted candidate details");

            BulkScheduleCandidates_inCompleteCandidatesGridView.DataSource = inValidcandidates;
            BulkScheduleCandidates_inCompleteCandidatesGridView.DataBind();

        }

        /// <summary>
        /// Method that is used to display the candidate record details
        /// </summary>
        /// <param name="totalCount">
        /// A<see cref="string"/>that holds the totalCount of the candidate
        /// </param>
        /// <param name="candidateCount">
        /// A<see cref="int"/>that holds the candidateCount displayed per page
        /// </param>
        private void DisplayRecordLabel(string totalCount, int candidateCount)
        {
            int firstNumber;

            int lastNumber;

            if ((int.Parse(ViewState["PAGE_NUMBER"].ToString()) == 1))
            {
                firstNumber = ((int.Parse(ViewState["PAGE_NUMBER"].ToString())
                                    - 1) * GridPageSize) + 1;
                lastNumber = candidateCount;
            }
            else
            {
                firstNumber = ((int.Parse(ViewState["PAGE_NUMBER"].ToString())
                                    - 1) * GridPageSize) + 1;

                lastNumber = firstNumber - 1 + candidateCount;
            }

            //To display the number of candidates displayed in the page
            //BulkScheduleCandidates_recordLabel.Text = firstNumber + " to "
            //                + lastNumber + " of " + totalCount + " Records";
        }

        /// <summary>
        /// This method helps to save the file in the server.
        /// </summary>
        private string SaveFileAs()
        {
            string fileName = BulkScheduleCandidates_fileUpload.FileName;

            // Check if file name is null or empty.
            if (fileName == null || fileName.Trim().Length == 0)
            {
                base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
                    BulkScheduleCandidates_bottomErrorMessageLabel,
                    Resources.HCMResource.BulkScheduleCandidates_PleaseProvideAExcelFile);
                return string.Empty;
            }

            // Check if file type is not of the desired one.
            string extension = Path.GetExtension(fileName);
            if (extension == null || extension.Trim().Length == 0 ||
                (extension.ToLower().Trim() != ".xls" && extension.ToLower().Trim() != ".xlsx"))
            {
                base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
                    BulkScheduleCandidates_bottomErrorMessageLabel,
                    Resources.HCMResource.BulkScheduleCandidates_PleaseProvideAExcelFile);
                return string.Empty;
            }

            // Check if content type is not of the desired one.
            string contentType = BulkScheduleCandidates_fileUpload.PostedFile.ContentType;
            if (contentType == null || contentType.Trim().Length == 0 ||
                (contentType.ToLower().Trim() != "application/vnd.ms-excel" &&
                contentType.ToLower().Trim() != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" &&
                contentType.ToLower().Trim() != "application/ms-excel"))
            {
                base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
                    BulkScheduleCandidates_bottomErrorMessageLabel,
                    Resources.HCMResource.BulkScheduleCandidates_PleaseProvideAExcelFile);
                return string.Empty;
            }

            // Create a directory in the server to keep the uploaded files.
            Directory.CreateDirectory(Server.MapPath("../") + "\\BulkScheduleCandidateExcels");

            //To save excel file as 
            string saveAsPath = Server.MapPath("../") + "BulkScheduleCandidateExcels\\"
                + userID + "_" + DateTime.Now.TimeOfDay.Milliseconds + "_" + fileName;

            BulkScheduleCandidates_fileUpload.SaveAs(saveAsPath);

            BulkScheduleCandidates_uploadExcelDiv.Style["display"] = "block";

            //BulkScheduleCandidates_resultDiv.Style["display"] = "block";

            //BulkScheduleCandidates_resultDiv.Visible = true;

            return saveAsPath;
        }

        /// <summary>
        /// Method to save the invalid candidates
        /// </summary>
        //private List<CandidateInformation> GetInvalidcandidate()
        //{
        //    CandidateInformation candidate;

        //    List<CandidateInformation> candidateDetails = new List<CandidateInformation>();

        //    foreach (GridViewRow row in BulkScheduleCandidates_inCompleteCandidatesGridView.Rows)
        //    {
        //        candidate = new CandidateInformation();

        //        Label candidateIDLabel = (Label)row.FindControl
        //            ("BulkScheduleCandidates_incompletecandidateNoLabel");

        //        if (!Utility.IsNullOrEmpty(candidateIDLabel.Text))
        //        {
        //            candidate.candidateID = int.Parse(candidateIDLabel.Text.Trim());
        //        }

        //        candidate.candidate = ((TextBox)row.FindControl
        //            ("BulkScheduleCandidates_incompletecandidateTextBox")).Text.Trim();

        //        //candidate.CategoryID = int.Parse(((HiddenField)row.FindControl
        //        //    ("BulkScheduleCandidates_categoryIDHiddenField")).Value);
        //        candidate.CategoryName = ((HiddenField)row.FindControl("BulkScheduleCandidates_categoryNameHiddenField")).Value.Trim();
        //        candidate.SubjectName = ((HiddenField)row.FindControl("BulkScheduleCandidates_subjectNameHiddenField")).Value.Trim();
        //        //if (!Utility.IsNullOrEmpty(((TextBox)
        //        //   row.FindControl("BulkScheduleCandidates_categoryTextBox")).Text.Trim()))
        //        //{
        //        //    candidate.CategoryName = ((TextBox)
        //        //    row.FindControl("BulkScheduleCandidates_categoryTextBox")).Text.Trim();
        //        //}

        //        //if (!Utility.IsNullOrEmpty(((TextBox)
        //        //    row.FindControl("BulkScheduleCandidates_subjectTextBox")).Text.Trim()))
        //        //{
        //        //    candidate.SubjectName = ((TextBox)
        //        //    row.FindControl("BulkScheduleCandidates_subjectTextBox")).Text.Trim();
        //        //}


        //        candidate.Subjects = new List<Subject>();
        //        candidate.Subjects.Add(new Subject());

        //        if (!Utility.IsNullOrEmpty(((HiddenField)
        //            row.FindControl("BulkScheduleCandidates_subjectIdHiddenField")).Value.Trim()))
        //        {
        //            candidate.Subjects[0].SubjectID = int.Parse(((HiddenField)
        //           row.FindControl("BulkScheduleCandidates_subjectIdHiddenField")).Value.Trim());

        //            candidate.Subjects[0].IsSelected = true;

        //            candidate.SubjectID = int.Parse(((HiddenField)
        //           row.FindControl("BulkScheduleCandidates_subjectIdHiddenField")).Value.Trim());
        //        }

        //        DropDownList testAreaDropDownList = (DropDownList)row.
        //            FindControl("BulkScheduleCandidates_testAreaDropDownList");
        //        candidate.TestAreaID = testAreaDropDownList.SelectedValue.Trim();
        //        candidate.TestAreaName = testAreaDropDownList.SelectedItem.Text.Trim();

        //        DropDownList complexityDropDown = (DropDownList)row.
        //            FindControl("BulkScheduleCandidates_complexityDropDownList");

        //        candidate.Complexity = complexityDropDown.SelectedValue.Trim();
        //        candidate.ComplexityName = complexityDropDown.SelectedItem.Text.Trim();

        //        candidate.Tag = ((TextBox)row.FindControl
        //            ("BulkScheduleCandidates_tagTextBox")).Text.Trim();

        //        candidate.CreditsEarned = 0.00M;

        //        TextBox userTextbox = (TextBox)
        //          row.FindControl("BulkScheduleCandidates_authorTextBox");

        //        HiddenField hiddenField = (HiddenField)
        //            row.FindControl("BulkScheduleCandidates_authorHiddenField");

        //        if (!Utility.IsNullOrEmpty(userTextbox))
        //        {
        //            candidate.AuthorName = userTextbox.Text;

        //            candidate.Author = int.Parse(hiddenField.Value);

        //            candidate.CreatedBy = int.Parse(hiddenField.Value);

        //            candidate.ModifiedBy = int.Parse(hiddenField.Value);
        //        }

        //        DataList answerDataList = (DataList)row.
        //            FindControl("BulkScheduleCandidates_subjectcategorycandidateDataList");
        //        candidate.NoOfChoices = Convert.ToInt16(answerDataList.Controls.Count);
        //        int options = 1;

        //        candidate.AnswerChoices = new List<AnswerChoice>();

        //        foreach (Control control in answerDataList.Controls)
        //        {
        //            RadioButton radioButton =
        //                (RadioButton)control.FindControl("BulkScheduleCandidates_selectcandidateRadioButton");
        //            TextBox textBox =
        //                (TextBox)control.FindControl("BulkScheduleCandidates_answercandidateTextBox");

        //            if (radioButton.Checked)
        //            {
        //                candidate.Answer = Convert.ToInt16(options);
        //            }

        //            //if (textBox.Text.Trim().Length == 0)
        //            //    continue;

        //            candidate.AnswerChoices.Add(new AnswerChoice
        //                (textBox.Text.Trim(), options, radioButton.Checked));

        //            options = options + 1;
        //            candidate.CreatedDate = DateTime.Now.Date;
        //            candidate.ModifiedDate = DateTime.Now.Date;
        //        }
        //        //candidate.NoOfChoices = Convert.ToInt16(candidate.AnswerChoices.Count);
        //        HiddenField BulkScheduleCandidates_qstImageHiddenField =
        //                (HiddenField)row.FindControl("BulkScheduleCandidates_qstImageHiddenField");

        //        string xlsFilePath = Path.GetDirectoryName(ViewState["PATH_NAME"].ToString());
        //        string imagesFolder = Path.Combine(xlsFilePath, Path.GetFileNameWithoutExtension(ViewState["PATH_NAME"].ToString()));

        //        string candidateImageName = BulkScheduleCandidates_qstImageHiddenField.Value;
        //        candidate.HasImage = false;
        //        candidate.ImageName = candidateImageName;
        //        if (candidate.ImageName != "")
        //        {
        //            candidate.HasImage = true;
        //            if (File.Exists(Path.Combine(imagesFolder, candidate.ImageName)))
        //            {
        //                try
        //                {
        //                    FileStream fs = File.OpenRead(Path.Combine(imagesFolder, candidate.ImageName));
        //                    if (fs.Length > 102400) // if file size greater than 100 KB
        //                    {
        //                        candidate.IsValid = false;
        //                        candidate.InvalidcandidateRemarks += "MAXIMUM OF 100KB IMAGE CAN BE UPLOADED, ";
        //                    }
        //                    else
        //                    {
        //                        byte[] data = new byte[fs.Length];
        //                        fs.Read(data, 0, data.Length);
        //                        candidate.candidateImage = data;
        //                    }
        //                    fs.Close();
        //                }
        //                catch
        //                {
        //                    candidate.IsValid = false;
        //                    candidate.InvalidcandidateRemarks += "IMAGE NOT PRESENT, ";

        //                }
        //            }
        //            else
        //            {
        //                candidate.IsValid = false;
        //                candidate.InvalidcandidateRemarks += "IMAGE NOT PRESENT, ";
        //            }
        //        }

        //        candidate.InvalidcandidateRemarks = ((Label)row.FindControl
        //   ("BulkScheduleCandidates_remarksReadOnlyLabel")).Text.Trim();

        //        candidateDetails.Add(candidate);
        //    }

        //    return candidateDetails;
        //}

        /// <summary>
        /// Method to check whether the candidates are valid or not
        /// </summary>
        /// <param name="candidates">
        /// A<see cref="List<CandidateInformation>"/>list of candidate details
        /// </param>
        /// <param name="options">
        /// A<see cref="string"/>that holds the options to check
        /// </param>
        //private bool CheckIsValidData(List<CandidateInformation> candidates, string options)
        //{
        //    string errorMessage = string.Empty;
        //    System.Text.StringBuilder errorText = null;
        //    try
        //    {
        //        if (options != "Invalid")
        //        {
        //            foreach (CandidateInformation candidate in candidates)
        //            {
        //                if (Utility.IsNullOrEmpty(errorText))
        //                    errorText = new System.Text.StringBuilder();
        //                //To check whether the candidate is null
        //                if (candidate.candidate.Trim().Length == 0)
        //                {
        //                    errorText.Append(Resources.HCMResource.BulkScheduleCandidates_PleaseEnterCandidate);
        //                    errorText.Append(", ");
        //                    //ShowMessage(BulkScheduleCandidates_bottomErrorMessageLabel,
        //                    //BulkScheduleCandidates_topErrorMessageLabel, string.Format("candidate - " + candidate.candidateID +
        //                    //" " + Resources.HCMResource.BulkScheduleCandidates_PleaseEntercandidate));
        //                }

        //                //To check whether the anser options are valid or not 
        //                List<AnswerChoice> answers = new List<AnswerChoice>();
        //                answers = (candidate.AnswerChoices.FindAll(delegate(AnswerChoice answer)
        //                {
        //                    return answer.Choice.Trim().Length == 0;
        //                }));

        //                if ((answers.Count > 2 && candidate.NoOfChoices > 2)
        //                    || (answers.Count >= 1 && candidate.NoOfChoices == 2))
        //                {
        //                    errorText.Append(Resources.HCMResource.BulkScheduleCandidates_PleaseEnterChoices);
        //                    errorText.Append(", ");
        //                    //ShowMessage(BulkScheduleCandidates_bottomErrorMessageLabel,
        //                    //BulkScheduleCandidates_topErrorMessageLabel, string.Format("candidate - " + candidate.candidateID
        //                    //+ " " + Resources.HCMResource.BulkScheduleCandidates_PleaseEnterChoices));
        //                }
        //                if (candidate.AnswerChoices.Count == 1)
        //                {
        //                    errorText.Append("Quesion with single option cannot be added., ");
        //                    //ShowMessage(BulkScheduleCandidates_bottomErrorMessageLabel,
        //                    //BulkScheduleCandidates_topErrorMessageLabel, string.Format("candidate - " + candidate.candidateID
        //                    //+ " " + "Quesion with single option cannot be added. "));
        //                }

        //                if (answers.Count >= 2 && candidate.NoOfChoices == 3)
        //                {
        //                    errorText.Append(Resources.HCMResource.BulkScheduleCandidates_PleaseEnterChoices);
        //                    errorText.Append(", ");
        //                    //ShowMessage(BulkScheduleCandidates_bottomErrorMessageLabel,
        //                    //BulkScheduleCandidates_topErrorMessageLabel, string.Format("candidate - " + candidate.candidateID
        //                    //+ " " + Resources.HCMResource.BulkScheduleCandidates_PleaseEnterChoices));
        //                }

        //                AnswerChoice correctAnswer = candidate.AnswerChoices.Find(delegate(AnswerChoice answer)
        //                {
        //                    return answer.ChoiceID == candidate.Answer;
        //                });
        //                if (correctAnswer != null && correctAnswer.Choice.Trim().Length == 0)
        //                {
        //                    errorText.Append(Resources.HCMResource.BulkScheduleCandidates_PleaseEnterCorrectChoice);
        //                    errorText.Append(", ");
        //                    //ShowMessage(BulkScheduleCandidates_bottomErrorMessageLabel,
        //                    //    BulkScheduleCandidates_topErrorMessageLabel, string.Format("candidate - " +
        //                    //  candidate.candidateID + " " + Resources.HCMResource.
        //                    //  BulkScheduleCandidates_PleaseEnterCorrectChoice));
        //                }
        //                if (candidate.CategoryName == null || candidate.SubjectName == null ||
        //                    candidate.CategoryName.Trim().Length == 0 || candidate.SubjectName.Trim().Length == 0)
        //                {
        //                    errorText.Append("Please enter category  and subject, ");
        //                    //ShowMessage(BulkScheduleCandidates_bottomErrorMessageLabel,
        //                    //    BulkScheduleCandidates_topErrorMessageLabel, string.Format("candidate - " +
        //                    //  candidate.candidateID + " " + "Please enter category  and subject"));
        //                }
        //                if (errorText.ToString().Length > 1)
        //                    ShowMessage(BulkScheduleCandidates_bottomErrorMessageLabel,
        //                        BulkScheduleCandidates_topErrorMessageLabel, string.Format("candidate - " +
        //                      candidate.candidateID + " " + errorText.ToString().TrimEnd(' ').TrimEnd(',')));
        //                errorText = null;
        //            }
        //        }
        //        //To check the selected subjects and categories
        //        if (options == "Invalid")
        //        {
        //            errorMessage = new candidateBLManager().CheckValidcandidates(candidates);
        //            BulkScheduleCandidates_bottomErrorMessageLabel.Text = "";
        //            BulkScheduleCandidates_bottomErrorMessageLabel.Text = "";
        //            ShowMessage(BulkScheduleCandidates_bottomErrorMessageLabel, errorMessage);
        //            ShowMessage(BulkScheduleCandidates_topErrorMessageLabel, errorMessage);
        //        }
        //        //If the candidates are valid and no error message is there to display
        //        //then save all candidates
        //        if (BulkScheduleCandidates_topSuccessMessageLabel.Text.Trim().Length == 0 &&
        //            BulkScheduleCandidates_bottomErrorMessageLabel.Text.Trim().Length == 0 &&
        //            errorMessage.Trim().Length == 0)
        //            return true;
        //        else
        //            return false;
        //    }
        //    finally
        //    {
        //        if (!Utility.IsNullOrEmpty(errorText)) errorText = null;
        //    }
        //}

        /// <summary>
        /// Method to save the list of candidates
        /// </summary>
        //private List<CandidateInformation> saveBatchcandidate()
        //{
        //    CandidateInformation candidate;
        //    List<CandidateInformation> validcandidates = new List<CandidateInformation>();

        //    foreach (GridViewRow row in BulkScheduleCandidates_candidateGridView.Rows)
        //    {
        //        candidate = new CandidateInformation();

        //        Label candidateIdLabel = ((Label)row.FindControl("BulkScheduleCandidates_candidateNoLabel"));

        //        if (candidateIdLabel.Text != "")
        //        {
        //            candidate.candidateID = int.Parse(candidateIdLabel.Text);
        //        }

        //        candidate.candidate = ((TextBox)row.FindControl
        //            ("BulkScheduleCandidates_candidateTextBox")).Text.Trim();

        //        candidate.CategoryName = ((Label)row.FindControl
        //            ("BulkScheduleCandidates_categorycandidateReadOnlyLabel")).Text.Trim();

        //        candidate.CategoryID = int.Parse(((HiddenField)row.FindControl
        //            ("BulkScheduleCandidates_categoryIDHiddenField")).Value.Trim());

        //        candidate.SubjectName = ((Label)row.FindControl(
        //        "BulkScheduleCandidates_subjectcandidateReadOnlyLabel")).Text.Trim();

        //        candidate.SubjectID = int.Parse(((HiddenField)row.FindControl
        //            ("BulkScheduleCandidates_subjectIDHiddenField")).Value.Trim());

        //        Subject subject = new Subject();
        //        candidate.Subjects = new List<Subject>();
        //        candidate.Subjects.Add(subject);

        //        candidate.Subjects[0].IsSelected = true;
        //        candidate.Subjects[0].SubjectID = int.Parse(((HiddenField)
        //            row.FindControl("BulkScheduleCandidates_subjectIDHiddenField")).Value.Trim());

        //        candidate.TestAreaID = ((HiddenField)row.FindControl
        //            ("BulkScheduleCandidates_testAreaIDHiddenField")).Value.Trim();

        //        candidate.ComplexityName = ((Label)row.FindControl
        //            ("BulkScheduleCandidates_complexitycandidateLabel")).Text.Trim();

        //        candidate.Complexity = ((HiddenField)row.FindControl
        //            ("BulkScheduleCandidates_complexityIDHiddenField")).Value.Trim();
        //        candidate.Tag = ((Label)row.FindControl
        //            ("BulkScheduleCandidates_tagcandidateReadOnlyLabel")).Text.Trim();

        //        candidate.TestAreaName = ((Label)row.FindControl
        //            ("BulkScheduleCandidates_testAreacandidateLabel")).Text.Trim();

        //        candidate.AuthorName = ((Label)row.FindControl
        //            ("BulkScheduleCandidates_authorcandidateReadOnlyLabel")).Text.Trim();

        //        candidate.CreditsEarned = 0.00M;

        //        candidate.Author = userID;
        //        candidate.CreatedBy = userID;
        //        candidate.ModifiedBy = userID;

        //        DataList answerDataList = (DataList)row.
        //            FindControl("BulkScheduleCandidates_subject_categoryDataList");
        //        candidate.NoOfChoices = Convert.ToInt16(answerDataList.Controls.Count);

        //        int options = 1;

        //        candidate.AnswerChoices = new List<AnswerChoice>();

        //        foreach (Control control in answerDataList.Controls)
        //        {
        //            RadioButton radioButton =
        //                (RadioButton)control.FindControl("BulkScheduleCandidates_selectRadioButton");
        //            TextBox textBox =
        //                (TextBox)control.FindControl("BulkScheduleCandidates_answerTextBox");

        //            if (radioButton.Checked)
        //            {
        //                candidate.Answer = Convert.ToInt16(options);
        //            }
        //            //if (textBox.Text.Trim().Length == 0)
        //            //    continue;

        //            candidate.AnswerChoices.Add(new AnswerChoice(textBox.Text.Trim()
        //                , options, radioButton.Checked));

        //            options = options + 1;
        //            candidate.CreatedDate = DateTime.Now.Date;
        //            candidate.ModifiedDate = DateTime.Now.Date;

        //        }
        //        //candidate.NoOfChoices = Convert.ToInt16(candidate.AnswerChoices.Count);
        //        string xlsFilePath = Path.GetDirectoryName(ViewState["PATH_NAME"].ToString());
        //        string imagesFolder = Path.Combine(xlsFilePath, Path.GetFileNameWithoutExtension(ViewState["PATH_NAME"].ToString()));

        //        string candidateImageName = ((LinkButton)row.FindControl
        //            ("BulkScheduleCandidates_qstImageLinkButton")).Text.Trim();
        //        candidate.HasImage = false;
        //        candidate.ImageName = candidateImageName;
        //        if (candidate.ImageName != "")
        //        {
        //            candidate.HasImage = true;
        //            if (File.Exists(Path.Combine(imagesFolder, candidate.ImageName)))
        //            {
        //                try
        //                {
        //                    FileStream fs = File.OpenRead(Path.Combine(imagesFolder, candidate.ImageName));
        //                    if (fs.Length > 102400) // if file size greater than 100 KB
        //                    {
        //                        candidate.IsValid = false;
        //                        candidate.InvalidcandidateRemarks += "IMAGE SIZE EXCEEDED ABOVE 100KB, ";
        //                    }
        //                    else
        //                    {
        //                        byte[] data = new byte[fs.Length];
        //                        fs.Read(data, 0, data.Length);
        //                        candidate.candidateImage = data;
        //                    }
        //                    fs.Close();
        //                }
        //                catch
        //                {
        //                    candidate.IsValid = false;
        //                    candidate.InvalidcandidateRemarks += "IMAGE NOT PRESENT, ";

        //                }
        //            }
        //            else
        //            {
        //                candidate.IsValid = false;
        //                candidate.InvalidcandidateRemarks += "IMAGE NOT PRESENT, ";
        //            }
        //        }
        //        validcandidates.Add(candidate);
        //    }
        //    return validcandidates;
        //}

        /// <summary>
        /// Method to load the next batch of candidates
        /// </summary>
        private void LoadNextBatchcandidates()
        {
            // If the valid candidates are not saved , it will show
            //a modal popup extender to save the valid candidates
            //if (BulkScheduleCandidates_candidateGridView.Rows.Count > 0 &&
            //    BulkScheduleCandidates_isSavedHiddenField.Value != "1")
            //{
            //    BulkScheduleCandidates_candidateModalPopupExtender.Show();
            //    return;
            //}

            //To check the last candidate of the file
            double records = Convert.ToDouble(ViewState["TOTAL_CANDIDATE_COUNT"].ToString()) / GridPageSize;

            //If particular quesiton is last candidate , 
            //gets the candidate from the next sheet
            if (records <= int.Parse(ViewState["PAGE_NUMBER"].ToString()))
            {
                List<string> sheetNames = Session["SHEET_NAME"] as List<string>;

                //Store the current sheet name
                string sheetName = ViewState["CURRENT_SHEET_NAME"].ToString();

                //Gets the total candidate count for the sheet
                //Get the index of the current sheet
                int sheetIndex = sheetNames.FindIndex(delegate(string sheet)
                {
                    return sheet == sheetName;
                });

                if (sheetIndex + 1
                    != sheetNames.Count)
                {
                    //Get the next sheet name
                    GetSheetCandidateCount(sheetNames[sheetIndex + 1],
                        ViewState["PATH_NAME"].ToString());
                    //Reset page number
                    ViewState["PAGE_NUMBER"] = 1;
                }

                if (int.Parse(ViewState["TOTAL_CANDIDATE_COUNT"].ToString()) == 0
                    || sheetIndex + 1 == sheetNames.Count)
                {
                    //BulkScheduleCandidates_pageNavigator.MoveToPage(1);
                    ShowMessage(BulkScheduleCandidates_bottomErrorMessageLabel,
                        Resources.HCMResource.BulkScheduleCandidates_LastRecord);
                    ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
                        Resources.HCMResource.BulkScheduleCandidates_LastRecord);
                    List<CandidateInformation> nullDataSource = new List<CandidateInformation>();
                    //BulkScheduleCandidates_candidateGridView.DataSource
                    //= nullDataSource;
                    //BulkScheduleCandidates_candidateGridView.DataBind();

                    //List<CandidateInformation> invalidcandidates = Session["INVALID_CANDIDATES"]
                    //    as List<CandidateInformation>;

                    //BulkScheduleCandidates_inCompletecandidatesGridView.DataSource = invalidcandidates;
                    //BulkScheduleCandidates_inCompletecandidatesGridView.DataBind();

                    ViewState["INVALID_CANDIDATES_PAGE_NUMBER"] = 1;

                    BindInvalidCandidates(1);

                    //if (BulkScheduleCandidates_inCompletecandidatesGridView.Rows.Count > 0)
                    //{
                    //    return;
                    //}

                    ResetValues();

                    BulkScheduleCandidates_bottomErrorMessageLabel.Text = string.Empty;

                    BulkScheduleCandidates_topErrorMessageLabel.Text = string.Empty;

                    ShowMessage(BulkScheduleCandidates_bottomSuccessMessageLabel,
                    BulkScheduleCandidates_topSuccessMessageLabel,
                    Resources.HCMResource.
                    BulkScheduleCandidates_AllCandidatesProcessed);

                    return;
                }
            }
            else
            {
                ViewState["PAGE_NUMBER"] = int.Parse(ViewState["PAGE_NUMBER"].ToString()) + 1;
            }
            LoadExcelFile(ViewState["PATH_NAME"].ToString());

            //BulkScheduleCandidates_isSavedHiddenField.Value = "0";
        }

        /// <summary>
        /// Method to get the current sheet name 
        /// </summary>
        /// <param name="fileName">
        /// A<see cref="string"/>that stores the name of the file
        /// </param>
        private void GetSheetName(string fileName)
        {
            //Get the list of sheet name in the excel 
            List<string> sheetName = new ExcelBatchReader(fileName).
                GetSheetNames();

            //Stores the sheet details in session
            Session["SHEET_NAME"] = sheetName;

            //Store the current sheet name
            ViewState["CURRENT_SHEET_NAME"] = sheetName[0];

            //Gets the total candidate count for the sheet
            GetSheetCandidateCount(sheetName[0], fileName);
        }

        /// <summary>
        /// Method to get the candidate count in current sheet
        /// </summary>
        /// <param name="sheetName">
        /// A<see cref="string"/>that holds the sheet name
        /// </param>
        /// <param name="fileName">
        /// A<see cref="string"/>that holds the file name
        /// </param>
        private void GetSheetCandidateCount(string sheetName, string fileName)
        {
            //Get the total candidate count of the first sheet
            //and store it in view state
            ViewState["TOTAL_CANDIDATE_COUNT"] = new ExcelBatchReader
                                              (fileName).GetCount(sheetName);

            //Checks if the number of candidate in the current sheet is zero,
            //if it is not then it will save the candidate count in session

            if (int.Parse(ViewState["TOTAL_CANDIDATE_COUNT"].ToString()) != 0)
            {
                ViewState["CURRENT_SHEET_NAME"] = sheetName;
                return;
            }

            //If the current sheet total candidate is zero.it checks the next sheet

            //Gets the list of sheet name from session 
            List<string> sheetNames = Session["SHEET_NAME"] as List<string>;

            //Get the index of the current sheet
            int sheetIndex = sheetNames.FindIndex(delegate(string sheet)
            {
                return sheet == sheetName;
            });
            //if the current sheet is the last sheet , it will return
            if (sheetNames.Count - 1 == sheetIndex)
                return;

            //else it will find count for next sheet
            GetSheetCandidateCount(sheetNames[sheetIndex + 1], fileName);
        }

        /// <summary>
        /// Mathod that is used to get the current invalid candidates
        /// </summary>
        /// <returns>
        /// A<see cref=List<CandidateInformation>/>that has
        /// the invalid candidates
        /// </returns>
        //private List<CandidateInformation> GetCurrentInvalidcandidates()
        //{
        //    List<CandidateInformation> candidates = Session["INVALID_CANDIDATES"] as List<CandidateInformation>;

        //    List<CandidateInformation> currentcandidate = new List<CandidateInformation>();

        //    currentcandidate = GetInvalidcandidate();

        //    int lastNumber = int.Parse(ViewState
        //         ["INVALID_CANDIDATES_PAGE_NUMBER"].ToString()) * GridPageSize;

        //    int index = lastNumber - GridPageSize;

        //    // candidates.RemoveRange(index, BulkScheduleCandidates_inCompletecandidatesGridView.Rows.Count);

        //    candidates.InsertRange(index, currentcandidate);

        //    return candidates;
        //}

        /// <summary>
        /// Method to reset the values 
        /// </summary>
        private void ResetValues()
        {
            //Clear the candidates in the valid candidate sessions
            Session["VALID_CANDIDATES"] = null;

            //Clear the candidates in the invalid candidate sessions
            Session["INVALID_CANDIDATES"] = null;

            //Make the upload excel div as visible true
            BulkScheduleCandidates_uploadExcelDiv.Style["display"] = "block";

            //Make the upload result div as visible false
            //BulkScheduleCandidates_resultDiv.Style["display"] = "none";

            //Clear the record label text
            // BulkScheduleCandidates_recordLabel.Text = string.Empty;

            //Make the total candidate count as 0
            ViewState["TOTAL_CANDIDATE_COUNT"] = 0;

            //Make the page number as 1
            ViewState["PAGE_NUMBER"] = 1;

            //Clear the path name from view state
            ViewState["PATH_NAME"] = string.Empty;
        }

        private string Decrypt(string encryptedText)
        {
            if ((encryptedText == "") || (encryptedText == null))
                throw new Exception("Please Provide Some Encrypted Text For Decryption");

            const string SALTVALUE = "FORT";
            const string STRINGIVVALUE = "FORTE-HCMENCRYPTI";
            const string PASSWORD = "SRALT";
            const string HASHALGORITHMNAME = "SHA1";

            byte[] bytIV = null;
            byte[] bytSalt = null;
            byte[] bytText = null;
            byte[] bytKey = null;
            byte[] bytOriginal = null;
            byte[] bytReturn = null;
            int OriginalDataLenght;
            MemoryStream objMememoryStream = null;
            CryptoStream objCryptptoStream = null;
            try
            {
                bytIV = Encoding.ASCII.GetBytes(STRINGIVVALUE);
                bytSalt = Encoding.ASCII.GetBytes(SALTVALUE);
                bytText = Convert.FromBase64String(encryptedText);
                PasswordDeriveBytes objPass = new PasswordDeriveBytes(PASSWORD, bytSalt, HASHALGORITHMNAME, 5);
                RijndaelManaged objRinjandel = new RijndaelManaged();
                bytKey = objPass.GetBytes(objRinjandel.KeySize / 8);
                objMememoryStream = new MemoryStream(bytText);
                objCryptptoStream = new CryptoStream(objMememoryStream, objRinjandel.CreateDecryptor(bytKey, bytIV), CryptoStreamMode.Read);
                bytOriginal = new byte[bytText.Length];
                OriginalDataLenght = objCryptptoStream.Read(bytOriginal, 0, bytOriginal.Length);
                objCryptptoStream.Close();
                objMememoryStream.Close();
                bytReturn = new byte[OriginalDataLenght];
                for (int i = 0; i < bytOriginal.Length; i++)
                {
                    if (Convert.ToString(bytOriginal[i]) == "0")
                    {
                        break;
                    }
                    bytReturn[i] = bytOriginal[i];
                }
                return Encoding.ASCII.GetString(bytReturn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objMememoryStream != null)
                    objMememoryStream = null;
                if (objCryptptoStream != null)
                    objCryptptoStream = null;
                bytIV = null;
                bytKey = null;
                bytOriginal = null;
                bytReturn = null;
                bytSalt = null;
                bytText = null;
            }
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Handler method that will be called when the export to excel link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will export and download candidate details in excel format.
        /// </remarks>
        protected void BulkScheduleCandidates_downloadCandidateDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CANDIDATE_USER_IDs"] != null)
                {
                    string candidateIDs = ViewState["CANDIDATE_USER_IDs"].ToString();
                    if (candidateIDs != null || candidateIDs != "")
                    {
                        DataTable table = new TestSchedulerBLManager().GetCandidateInformationByIDs(candidateIDs, ViewState["TEST_SESSION_KEY"].ToString());
                        if (table == null)
                        {
                            return;
                        }

                        // Change the column names.
                        table.Columns["S_NO"].ColumnName = "S.No";
                        table.Columns["CANDIDATE_FULL_NAME"].ColumnName = "Candidate Name";
                        table.Columns["CANDIDATE_SESSION_KEY"].ColumnName = "Candidate Session Key";
                        table.Columns["USERNAME"].ColumnName = "User Name";
                        table.Columns["PASSWORD"].ColumnName = "Password";
                        table.Columns["EXPIRY_DATE"].ColumnName = "Expiry Date";

                        //Decrypt password
                        foreach (DataRow drRow in table.Rows)
                        {
                            drRow["Password"] = Decrypt(drRow["Password"].ToString());
                        }

                        using (ExcelPackage pck = new ExcelPackage())
                        {
                            // Create the worksheet
                            ExcelWorksheet ws = pck.Workbook.Worksheets.Add(Constants.ExcelExportSheetName.CANDIDATE_DETAILS);

                            // Load the datatable into the sheet, starting from the 
                            // cell A1 and print the column names on row 1.
                            ws.Cells[Constants.ExcelExport.STARTING_CELL].LoadFromDataTable(table, true);

                            string decimalFormat = Constants.ExcelExportFormats.DECIMAL_COMMA;

                            foreach (DataColumn column in table.Columns)
                            {
                                if (column.DataType == typeof(System.DateTime))
                                {
                                    using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                                    {
                                        col.Style.Numberformat.Format = Constants.ExcelExportFormats.DATE;
                                    }
                                }
                                else if (column.DataType == typeof(decimal))
                                {
                                    using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + table.Rows.Count, column.Ordinal + 1])
                                    {
                                        if (decimalFormat == "C")
                                        {
                                            /*for (int row = 2; row <= ws.Dimension.End.Row; row++)
                                            {
                                                ws.Cells[row, column.Ordinal + 1].Value =
                                                    ws.Cells[row, column.Ordinal + 1].Value != null ? ws.Cells[row, column.Ordinal + 1].Value.ToString().Replace(".", ",") : string.Empty;
                                            }*/
                                            col.Style.Numberformat.Format = "0.00";
                                        }
                                        else
                                            col.Style.Numberformat.Format = "###0.0000";
                                    }
                                }
                            }

                            // Construct file name.
                            string fileName = string.Format("{0}_{1}.xlsx",
                                Constants.ExcelExportFileName.CANDIDATE_DETAILS, ViewState["TEST_SESSION_KEY"].ToString());

                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                            Response.BinaryWrite(pck.GetAsByteArray());
                            //Response.End();
                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                            //Response.End();
                            Response.Flush();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
                    BulkScheduleCandidates_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called on gridview row data bound
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void BulkScheduleCandidates_inCompleteCandidatesGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                Label BulkScheduleCandidates_inCompleteCandidatesErrorLabel = (Label)e.Row.FindControl("BulkScheduleCandidates_inCompleteCandidatesErrorLabel");

            }
            catch (Exception exception)
            {
                
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(BulkScheduleCandidates_topErrorMessageLabel,
                    BulkScheduleCandidates_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Protected Overridden Methods


    }
}
