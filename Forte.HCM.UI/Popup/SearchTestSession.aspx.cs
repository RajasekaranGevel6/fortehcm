
#region Header

// Copyright (C) Forte. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchTestSession.aspx.cs
// This page allows the user to search the existing testSessions 
// and to view all its informations.
#endregion Header

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// This page allows the user to search the existing testSessions 
    /// and to view all its informations.
    /// </summary>
    public partial class SearchTestSession : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "224px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "374px";

        #endregion Private Constants

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Search Test Session");

                // Set default button
                Page.Form.DefaultButton = SearchTestSession_topSearchButton.UniqueID;

            
                // Create events for paging control
                SearchTestSession_bottomPagingNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchTestSession_pagingNavigator_PageNumberClick);

                // Check whether the grid is maximized or not and assign the height accordingly.
                if (!Utility.IsNullOrEmpty(ScheduleCandidate_isMaximizedHiddenField.Value) &&
                        ScheduleCandidate_isMaximizedHiddenField.Value == "Y")
                {
                    SearchTestSession_searchCriteriasDiv.Style["display"] = "none";
                    SearchTestSession_searchResultsUpSpan.Style["display"] = "block";
                    SearchTestSession_searchResultsDownSpan.Style["display"] = "none";
                    SearchTestSession_testDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchTestSession_searchCriteriasDiv.Style["display"] = "block";
                    SearchTestSession_searchResultsUpSpan.Style["display"] = "none";
                    SearchTestSession_searchResultsDownSpan.Style["display"] = "block";
                    SearchTestSession_testDiv.Style["height"] = RESTORED_HEIGHT;
                }

                // Call expand or restore method
                SearchTestSession_searchTestResultsTR.Attributes.Add("onclick",
                        "ExpandOrRestore('" +
                        SearchTestSession_testDiv.ClientID + "','" +
                        SearchTestSession_searchCriteriasDiv.ClientID + "','" +
                        SearchTestSession_searchResultsUpSpan.ClientID + "','" +
                        SearchTestSession_searchResultsDownSpan.ClientID + "','" +
                        ScheduleCandidate_isMaximizedHiddenField.ClientID + "','" +
                        RESTORED_HEIGHT + "','" +
                        EXPANDED_HEIGHT + "')");

                if (!IsPostBack)
                {
                    // Set default focus
                    Page.Form.DefaultFocus = SearchTestSession_testSessionIdTextBox.UniqueID;
                    SearchTestSession_testSessionIdTextBox.Focus();

                    SearchTestSession_testDiv.Visible = false;

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Descending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "TestSessionID";

                    SetAuthorDetails();
                }
                else
                {
                    SearchTestSession_testDiv.Visible = true;
                }

                // Assign handler to scheduler selection popup.
                SearchTestSession_schedulerNameImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                    + SearchTestSession_schedulerHiddenField.ClientID + "','"
                    + SearchTestSession_schedulerIDHiddenField.ClientID + "','"
                    + SearchTestSession_schedulerNameTextBox.ClientID + "','TC')");

                // Assign handler to session author selection popup.
                SearchTestSession_sessionAuthorNameImageButton.Attributes.Add("onclick",
                    "return LoadAdminName('" +SearchTestSession_sessionAuthorNameHiddenField.ClientID + "','" +
                    SearchTestSession_sessionAuthorIDHiddenField.ClientID + "','" +
                    SearchTestSession_sessionAuthorNameTextBox.ClientID + "','TS')");

                // Assign handler to position profile selection popup.
                SearchTestSession_positionProfileImageButton.Attributes.Add("onclick",
                    "return LoadPositionProfileName('" + SearchTestSession_positionProfileTextBox.ClientID + "','"
                    + SearchTestSession_positionProfileIDHiddenField.ClientID + "')");

                // Clear message.
                SearchTestSession_topErrorMessageLabel.Text =
                    SearchTestSession_topSuccessMessageLabel.Text = "";

                // Assign position profile.
                if (!IsPostBack)
                {
                    AssignPositionProfile();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTestSession_topErrorMessageLabel, exp.Message);

            }
        }

        /// <summary>
        /// This button handler will get trigger on clicking the search button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchTestSession_SearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear error message label
                SearchTestSession_topErrorMessageLabel.Text = string.Empty;
                SearchTestSession_positionProfileTextBox.Text =
                    Request[SearchTestSession_positionProfileTextBox.UniqueID].ToString();

                // Reset the paging control.
                SearchTestSession_bottomPagingNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1
                SearchTestSessionDetail(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchTestSession_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the PageNumberClick event of the SearchTestSession_pagingNavigator control.
        /// This binds the data into the grid for the pagenumber.
        /// </summary>
        /// <param name="sender">Source of the event.</param>
        /// <param name="e"></param>
        protected void SearchTestSession_pagingNavigator_PageNumberClick(object sender,
            PageNumberEventArgs e)
        {
            try
            {
                SearchTestSessionDetail(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchTestSession_topErrorMessageLabel,
                    exception.Message);
                SearchTestSession_topErrorMessageLabel.Text = exception.Message;
            }
        }

        /// <summary>
        /// Clicking on Reset button will trigger this method. 
        /// This clears all the informations and reset to empty.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchTestSession_bottomReset_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }
        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method that assigns the position profile and loads the default 
        /// search results in the grid.
        /// </summary>
        private void AssignPositionProfile()
        {
            // Load the position profile name in the search criteria,
            // if position profile is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                return;

            // Get position profile ID.
            int positionProfileID = 0;
            if (int.TryParse(Request.QueryString["positionprofileid"].Trim(),
                out positionProfileID) == false)
            {
                return;
            }
            PositionProfileDetail positionProfile =
                new PositionProfileBLManager().GetPositionProfileInformation(positionProfileID);

            // Assign the position profile ID and name to the corresponding fields.
            SearchTestSession_positionProfileTextBox.Text = positionProfile.PositionProfileName;
            SearchTestSession_positionProfileIDHiddenField.Value = positionProfileID.ToString();

            // Apply the default search.
            // Clear error message label
            SearchTestSession_topErrorMessageLabel.Text = string.Empty;

            if (Request[SearchTestSession_positionProfileTextBox.UniqueID] != null)
            {
                SearchTestSession_positionProfileTextBox.Text =
                    Request[SearchTestSession_positionProfileTextBox.UniqueID].ToString();
            }

            // Reset the paging control.
            SearchTestSession_bottomPagingNavigator.Reset();

            // By default search button click retrieves data for
            // page number 1
            SearchTestSessionDetail(1);

            // Update the search results update panel.
           // SearchCategory_searchResultsUpdatePanel.Update();
            //SearchTestSession_updatePanel.Update();
        }

        /// <summary>
        /// This method creates an instance for search criteria and 
        /// passes to db to get the matched test sessions
        /// </summary>
        /// <param name="pageNumber"></param>
        private void SearchTestSessionDetail(int pageNumber)
        {
            TestSessionSearchCriteria tsSearchCriteria = new TestSessionSearchCriteria();
            tsSearchCriteria.TestKey = SearchTestSession_testIdTextBox.Text.Trim();
            tsSearchCriteria.TestName = SearchTestSession_testNameTextBox.Text.Trim();
            tsSearchCriteria.SessionKey = SearchTestSession_testSessionIdTextBox.Text.Trim();

            // Since the session author textbox is readonly, get the text value using Request.
            if (Request[SearchTestSession_sessionAuthorNameTextBox.UniqueID] != null)
            {
                SearchTestSession_sessionAuthorNameTextBox.Text =
                  Request[SearchTestSession_sessionAuthorNameTextBox.UniqueID].Trim();
            }
            tsSearchCriteria.TestSessionCreator = SearchTestSession_sessionAuthorNameTextBox.Text.Trim();

            tsSearchCriteria.PositionProfileID =
                (SearchTestSession_positionProfileIDHiddenField.Value == null || SearchTestSession_positionProfileIDHiddenField.Value.Trim().Length == 0) ? 0 :
                Convert.ToInt32(SearchTestSession_positionProfileIDHiddenField.Value.Trim());

            // Since the SchedulerName textbox is readonly, get the text value using Request.
            if (Request[SearchTestSession_schedulerNameTextBox.UniqueID] != null)
            {
                SearchTestSession_schedulerNameTextBox.Text =
                    Request[SearchTestSession_schedulerNameTextBox.UniqueID].Trim();
            }
            tsSearchCriteria.SchedulerName = SearchTestSession_schedulerNameTextBox.Text;
            tsSearchCriteria.SchedulerNameID =int.Parse(SearchTestSession_schedulerIDHiddenField.Value);
            tsSearchCriteria.TestSessionCreatorID = int.Parse(SearchTestSession_sessionAuthorIDHiddenField.Value);

            int totalRecords = 0;

            List<TestSessionDetail> testSession =
                new TestSchedulerBLManager().SearchTestSessionDetails
                (tsSearchCriteria, pageNumber, base.GridPageSize,
                out totalRecords, ViewState["SORT_FIELD"].ToString(), ((SortType)ViewState["SORT_ORDER"]));

            // Bind the retrieved test sessions in grid.
            SearchTestSession_testGridView.DataSource = testSession;
            SearchTestSession_testGridView.DataBind();
            if (testSession == null)
            {
                SearchTestSession_testDiv.Visible = false;
                base.ShowMessage(SearchTestSession_topErrorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                SearchTestSession_testDiv.Visible = true;
            }

            SearchTestSession_bottomPagingNavigator.PageSize = base.GridPageSize;
            SearchTestSession_bottomPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method used to set the author name and first name for the first time
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user id 
        /// </param>
        private void SetAuthorDetails()
        {
            //Get the user details from DB and assign it
            UserDetail userDetail = new QuestionBLManager().GetAuthorIDAndName(base.userID);

            if (userDetail == null)
                return;

            SearchTestSession_schedulerIDHiddenField.Value ="0";
            SearchTestSession_sessionAuthorIDHiddenField.Value = base.userID.ToString();
            SearchTestSession_sessionAuthorNameTextBox.Text = userDetail.FirstName;

            //if (base.isAdmin)
            //{
            //    SearchTestSession_sessionAuthorNameImageButton.Visible = true;
            //    SearchTestSession_sessionAuthorNameTextBox.Text = string.Empty;
            //    SearchTestSession_schedulerIDHiddenField.Value = "0";
            //    SearchTestSession_sessionAuthorIDHiddenField.Value = "0";
            //}
        }


        #endregion Private Methods

        #region Sort Columns Related Code

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchTestSession_testGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                SearchTestSession_bottomPagingNavigator.Reset();
                SearchTestSessionDetail(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTestSession_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchTestSession_testGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchTestSession_testGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTestSession_topErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchTestSession_testGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchTestSession_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Sort Columns Related Code

        #region Override Methods

        protected override bool IsValidData()
        {
            return true;
        }
        protected override void LoadValues()
        {
            //Do Nothing
        }
        #endregion
    }
}
