﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="TestInclusionDetail.aspx.cs" Inherits="Forte.HCM.UI.Popup.TestInclusionDetail"%>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="TestInclusionDetail_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="TestInclusionDetail_titleLiteral" runat="server" Text="Test Inclusion Details"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="TestInclusionDetail_closeImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:Label ID="TestInclusionDetail_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr id="TestInclusionDetail_searchResultsTR" runat="server">
                                    <td id="Td1" class="header_bg" runat="server">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 50%">
                                                    <asp:Literal ID="TestInclusionDetail_searchResultsLiteral" runat="server" Text="Test Inclusion Details"></asp:Literal>
                                                    &nbsp;<asp:Label ID="TestInclusionDetail_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                        Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                </td>
                                                <td style="width: 50%" align="right">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="left">
                                                    <div style="height: 281px; width: 650px; overflow: auto;" runat="server" id="TestInclusionDetail_testInclusionDetailDiv">
                                                        <asp:UpdatePanel ID="TestInclusionDetail_testInclusionDetailGridView_updatePanel"
                                                            runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView ID="TestInclusionDetail_detailGridView" runat="server"
                                                                    SkinID="sknWrapHeaderGrid" AllowSorting="true" OnRowDataBound="TestInclusionDetail_detailGridView_RowDataBound"
                                                                    OnRowCreated="TestInclusionDetail_detailGridView_RowCreated" OnSorting="TestInclusionDetail_detailGridView_Sorting">
                                                                    <Columns>
                                                                        <asp:BoundField HeaderText="Test ID" DataField="TestKey" SortExpression="TestKey"
                                                                            ItemStyle-Width="15%" />
                                                                        <asp:BoundField HeaderText="Test Name" DataField="Name" ItemStyle-Width="30%" SortExpression="Name" />
                                                                        <asp:TemplateField HeaderText="Created Date" SortExpression="TestCreationDate" ItemStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="TestInclusionDetail_detailGridView_createdDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.TestDetail)Container.DataItem).TestCreationDate) %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Test Author" SortExpression="TestAuthorName" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="TestInclusionDetail_detailGridView_testAuthorNameLabel" runat="server" Text='<%# Eval("TestAuthorName") %>'
                                                                                    ToolTip='<%# Eval("TestAuthorFullName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField HeaderText="Test Administered" DataField="NoOfCandidatesAdministered"
                                                                            SortExpression="NoOfCandidatesAdministered" ItemStyle-CssClass="td_padding_right_20"
                                                                            ItemStyle-HorizontalAlign="right" ItemStyle-Width="15%" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc1:PageNavigator ID="TestInclusionDetail_pageNavigator" runat="server" />
                                        <asp:HiddenField ID="TestInclusionDetail_questionKeyHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_5">
                <asp:LinkButton ID="TestInclusionDetail_closeLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClientClick="javascript:CloseMe()" />
            </td>
        </tr>
    </table>
</asp:Content>
