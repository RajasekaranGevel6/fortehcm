﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowAssesser.aspx.cs" MasterPageFile="~/MasterPages/PopupMaster.Master"
    Inherits="Forte.HCM.UI.Popup.ShowAssesser" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/InterviewQuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc4" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="SearchClientRequest_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl) {
            var nameCtrl = '<%= Request.QueryString["namectrl"] %>';
            var emailCtrl = '<%= Request.QueryString["emailctrl"] %>';
            var idCtrl = '<%= Request.QueryString["idctrl"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Set the user name field value. Replace the 'link button name' with the 
            // 'user name hidden field name' and retrieve the value.
            if (nameCtrl != null && nameCtrl != '') {
                window.opener.document.getElementById(nameCtrl).value
                    = document.getElementById(ctrl.id.replace("ShowAssesser_selectLinkButton", "ShowAssesser_userNameHiddenfield")).value;
            }

            // Set the email hidden field value. Replace the 'link button name' with the 
            // 'eamil hidden field name' and retrieve the value.
            if (emailCtrl != null && emailCtrl != '') {
                window.opener.document.getElementById(emailCtrl).value
                    = document.getElementById(ctrl.id.replace("ShowAssesser_selectLinkButton", "ShowAssesser_emailHiddenfield")).value;
            }

            // Set the user ID hidden field value. Replace the 'link button name' with the 
            // 'user ID hidden field name' and retrieve the value.
            if (idCtrl != null && idCtrl != '') {
                window.opener.document.getElementById(idCtrl).value
                    = document.getElementById(ctrl.id.replace("ShowAssesser_selectLinkButton", "ShowAssesser_userIDHiddenfield")).value;
            }
            self.close();
        }

    </script>
    <table width="50%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey" align="left">
                            <asp:Literal ID="ShowAssesser_searchAssessor" runat="server" Text="Search Assessor"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="ShowAssesser_topCancelImagebutton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_2">
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">


                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <div id="ShowAssesser_searchCriteriasDiv" runat="server" style="display: block;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center">
                                            <asp:UpdatePanel runat="server" ID="ShowAssesser_simpleLinkUpdatePanel">
                                                <ContentTemplate>
                                                    <asp:Label ID="ShowAssesser_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                    <asp:Label ID="ShowAssesser_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                    <asp:HiddenField ID="ShowAssesser_stateHiddenField" runat="server" Value="0" />

                                                     <asp:HiddenField runat="server" ID="ShowAssesser_isMaximizedHiddenField" />

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="ShowAssesser_searchCriteriDiv" runat="server" style="display: block;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td width="100%">
                                                            <asp:UpdatePanel runat="server" ID="ShowAssesser_SearchDivUpdatePanel">
                                                                <ContentTemplate>
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td style="width: 18%">
                                                                                <asp:Label ID="ShowAssesser_firstNameHeadLabel" runat="server" Text="First Name"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 1%">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="ShowAssesser_firstNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 18%">
                                                                                <asp:Label ID="ShowAssesser_lastNametHeadLabel" runat="server" Text="Last Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 1%">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="ShowAssesser_lastNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="ShowAssesser_skillHeadLabel" runat="server" Text="Skill" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 1%">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td style="width: 16%">
                                                                                <asp:TextBox ID="ShowAssesser_skillTextBox" runat="server" MaxLength="100"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="ShowAssesser_fromdateLabel" SkinID="sknLabelFieldHeaderText" runat="server"
                                                                                    Text="From Date"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 1%">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 165px">
                                                                                    <tr>
                                                                                        <td style="width: 37%">
                                                                                            <asp:TextBox ID="ShowAssesser_fromdateTextbox" runat="server" MaxLength="10" AutoCompleteType="None"
                                                                                                Text=""></asp:TextBox>
                                                                                        </td>
                                                                                        <td align="left">
                                                                                            <asp:ImageButton ID="ShowAssesser_fromdateImageButton" SkinID="sknCalendarImageButton"
                                                                                                runat="server" ImageAlign="Middle" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <ajaxToolKit:MaskedEditExtender ID="ShowAssesser_fromdateMaskedEditExtender" runat="server"
                                                                                    TargetControlID="ShowAssesser_fromdateTextbox" Mask="99/99/9999" MessageValidatorTip="true"
                                                                                    OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Date"
                                                                                    DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                                <ajaxToolKit:MaskedEditValidator ID="ShowAssesser_MaskedEditValidator" runat="server"
                                                                                    ControlExtender="ShowAssesser_fromdateMaskedEditExtender" ControlToValidate="ShowAssesser_fromdateTextbox"
                                                                                    EmptyValueMessage="Date is required" InvalidValueMessage="From date is invalid"
                                                                                    Display="None" TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                                    ValidationGroup="MKE" />
                                                                                <ajaxToolKit:CalendarExtender ID="ShowAssesser_fromdateCustomCalendarExtender" runat="server"
                                                                                    TargetControlID="ShowAssesser_fromdateTextbox" CssClass="MyCalendar" Format="MM/dd/yyyy"
                                                                                    PopupPosition="BottomLeft" PopupButtonID="ShowAssesser_fromdateImageButton" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="ShowAsseser_todateLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                    Text="To Date"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 1%">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <table width="100" cellpadding="0" cellspacing="0" border="0">
                                                                                    <tr>
                                                                                        <td style="width: 37%">
                                                                                            <asp:TextBox ID="ShowAssesser_todateTextbox" runat="server" MaxLength="10" AutoCompleteType="None"
                                                                                                Text=""></asp:TextBox>
                                                                                        </td>
                                                                                        <td align="left">
                                                                                            <asp:ImageButton ID="ShowAssesser_todateImageButton" SkinID="sknCalendarImageButton"
                                                                                                runat="server" ImageAlign="Middle" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <ajaxToolKit:MaskedEditExtender ID="ShowAssesser_todateMaskedEditExtender" runat="server"
                                                                                    TargetControlID="ShowAssesser_todateTextbox" Mask="99/99/9999" MessageValidatorTip="true"
                                                                                    OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Date"
                                                                                    DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                                <ajaxToolKit:MaskedEditValidator ID="ShowAssesser_todateMaskedEditValidator" runat="server"
                                                                                    ControlExtender="ShowAssesser_todateMaskedEditExtender" ControlToValidate="ShowAssesser_todateTextbox"
                                                                                    EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                                                    TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                                    ValidationGroup="MKE" />
                                                                                <ajaxToolKit:CalendarExtender ID="ShowAssesser_todateCustomCalendarExtender" runat="server"
                                                                                    TargetControlID="ShowAssesser_todateTextbox" CssClass="MyCalendar" Format="MM/dd/yyyy"
                                                                                    PopupPosition="BottomLeft" PopupButtonID="ShowAssesser_todateImageButton" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="td_padding_top_5">
                                                            <asp:Button ID="ShowAssesser_topSearchButton" runat="server" Text="Search" SkinID="sknButtonId"
                                                                OnClick="ShowAssesser_topSearchButton_Click" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                    <tr id="ShowAssesser_ShowAssesserResultsTR" runat="server">
                                                        <td class="header_bg">
                                                            <asp:UpdatePanel ID="ShowAssesser_expandAllUpdatePanel" runat="server">
                                                                <ContentTemplate>
                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                        <tr>
                                                                            <td style="width: 50%" align="left" class="header_text_bold">
                                                                                <asp:Literal ID="ShowAssesser_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                                    ID="ShowAssesser_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 48%" align="left">
                                                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:HiddenField ID="ShowAssesser_stateExpandHiddenField" runat="server" Value="0" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td style="width: 2%" align="right">
                                                                                <span id="ShowAssesser_searchResultsUpSpan" runat="server" style="display: none;">
                                                                                    <asp:Image ID="ShowAssesser_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                                                        id="ShowAssesser_searchResultsDownSpan" runat="server" style="display: block;"><asp:Image
                                                                                            ID="ShowAssesser_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                                                <asp:HiddenField ID="ShowAssesser_restoreHiddenField" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                     <div style="height: 225px; width: 610px; overflow: auto;" runat="server" id="ShowAssesser_questionDiv">                                                                       
                                                                            <asp:GridView ID="ShowAssesser_assesserDetailsGridView" runat="server" AutoGenerateColumns="false">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="20px">
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="ShowAssesser_selectCheckbox" runat="server" Width="15px" />
                                                                                            <asp:HiddenField ID="ShowAssesser_assessorIDHiddenField" runat="server" Value='<%# Eval("AssessorID") %>' />
                                                                                            <asp:HiddenField ID="ShowAssesser_userIDHiddenField" runat="server" Value='<%# Eval("UserID") %>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="First Name">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ShowAssesser_firstNameLabel" runat="server" Text='<%# Eval("AssessorFirstName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Last Name">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ShowAssesser_lastNameLabel" runat="server" Text='<%# Eval("AssessorLastName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Email">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ShowAssesser_emailLabel" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <uc2:PageNavigator ID="ShowAssesser_bottomPagingNavigator" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                   
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <table cellpadding="3" cellspacing="3">
                    <tr>
                        <td>
                            <asp:Button ID="ShowAssesser_addAssessorButton" runat="server" Text="Select"
                                OnClick="ShowAssesser_addAssessorButton_Click" SkinID="sknButtonId" />
                        </td>
                        <td>
                            <asp:LinkButton ID="ShowAssesserLookup_resetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="ShowAssesserLookup_resetLinkButton_Click"></asp:LinkButton>
                        </td>
                        <td class="pop_divider_line">
                            |
                        </td>
                        <td>
                            <asp:LinkButton ID="ShowAssesser_addAssessorCancel" runat="server" SkinID="sknPopupLinkButton"
                                Text="Cancel" OnClientClick="javascript:window.close()"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
                
            </td>
        </tr>
    </table>

</asp:Content>
