﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AvailabilityRequest.aspx.cs
// File that represents the user interface layout and functionalities for
// the AvailabilityRequest page. This will helps to submit date and time 
// slot request to assessors for interview assessment. 

#endregion Header

#region Directives

using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the AvailabilityRequest page. This will helps to submit date and time 
    /// slot request to assessors for interview assessment. This class inherits
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class AvailabilityRequest : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Assessor Availability Request");

                if (!IsPostBack)
                {
                    // Clear temp time slots session.
                    Session["AVAILABILITY_REQUEST_LIST_TEMP"] = null;
                    //Session["AVAILABILITY_REQUEST_LIST"] = null;

                    // Append the existing selected values to the temp collection.
                    if (Session["AVAILABILITY_REQUEST_LIST"] != null)
                    {
                        // Get existing time slots from the session.
                        List<AssessorTimeSlotDetail> existingSlots = Session["AVAILABILITY_REQUEST_LIST"] as
                            List<AssessorTimeSlotDetail>;

                        // Add to the temp time slots.
                        List<AssessorTimeSlotDetail> tempSlots = new List<AssessorTimeSlotDetail>();
                        tempSlots.AddRange(existingSlots);

                        // Keep temp time slots in session.
                        Session["AVAILABILITY_REQUEST_LIST_TEMP"] = tempSlots;
                    }

                    // Load assessors.
                    LoadAssessors();

                    // Load selected time slots.
                    LoadTimeSlots();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AvailabilityRequest_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the assessment details grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void AvailabilityRequest_selectedTimeSlotsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AvailabilityRequest_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the time slots grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void AvailabilityRequest_selectedTimeSlotsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "DeleteTimeSlot")
                    return;

                int index = Convert.ToInt32(e.CommandArgument);

                // Check if selected time slot in session is null.
                if (Session["AVAILABILITY_REQUEST_LIST_TEMP"] == null)
                {
                    base.ShowMessage(AvailabilityRequest_topErrorMessageLabel, "No time slot found to delete");
                    return;
                }

                // Get selected time slots from session.
                List<AssessorTimeSlotDetail> oldSlots = Session["AVAILABILITY_REQUEST_LIST_TEMP"] as
                    List<AssessorTimeSlotDetail>;

                // Check if the delete index is present.
                if (index >= oldSlots.Count)
                {
                    base.ShowMessage(AvailabilityRequest_topErrorMessageLabel, "No time slot found to delete");
                    return;
                }

                // Delete the time slot from the list.
                oldSlots.RemoveAt(index);

                // Keep the time slots in session.
                Session["AVAILABILITY_REQUEST_LIST_TEMP"] = oldSlots;

                // Assign data source.
                AvailabilityRequest_selectedTimeSlotsGridView.DataSource = oldSlots;
                AvailabilityRequest_selectedTimeSlotsGridView.DataBind();

                // Show success message.
                base.ShowMessage(AvailabilityRequest_topErrorMessageLabel, "Time slot deleted successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AvailabilityRequest_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the assessor dropdown list item
        /// is selected.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        protected void AvailabilityRequest_assessorDropDownList_SelectedIndexChanged
            (object sender, EventArgs e)
        {
            try
            {
                // Load skills.
                LoadSkills();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AvailabilityRequest_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the skill check box list item
        /// is selected.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        protected void AvailabilityRequest_skillsCheckBoxList_SelectedIndexChanged
            (object sender, EventArgs e)
        {
            try
            {
                // Initiate request time slot.
                InitiateRequestTimeSlot();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AvailabilityRequest_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the select button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void AvailabilityRequest_addTimeSlotsButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                AvailabilityRequest_topErrorMessageLabel.Text = string.Empty;

                // Check if assessor is selected.
                if (Utility.IsNullOrEmpty(AvailabilityRequest_selectedAssessorID.Value))
                    return;

                // Get the existing selected list.
                List<AssessorTimeSlotDetail> oldSlots = null;

                if (Session["AVAILABILITY_REQUEST_LIST_TEMP"] == null)
                    oldSlots = new List<AssessorTimeSlotDetail>();
                else
                    oldSlots = Session["AVAILABILITY_REQUEST_LIST_TEMP"] as List<AssessorTimeSlotDetail>;

                // Get the newly selected list.
                List<AssessorTimeSlotDetail> newSlots = null;

                if (Session["REQUEST_TIME_SLOT_LIST"] != null)
                {
                    newSlots = Session["REQUEST_TIME_SLOT_LIST"] as List<AssessorTimeSlotDetail>;

                    // Loop through the list of selected skills and add the 
                    // newly added time slots against them.
                    for (int skillIndex = 0; skillIndex < AvailabilityRequest_skillsCheckBoxList.Items.Count;
                        skillIndex++)
                    {
                        if (!AvailabilityRequest_skillsCheckBoxList.Items[skillIndex].Selected)
                            continue;

                        int skillID = Convert.ToInt32(AvailabilityRequest_skillsCheckBoxList.Items[skillIndex].Value);
                        string skill = AvailabilityRequest_skillsCheckBoxList.Items[skillIndex].Text;

                        // Loop through the list of selected time slots and add
                        // it against every skill and assign the assessor too.
                        foreach (AssessorTimeSlotDetail newSlot in newSlots)
                        {
                            // Create a new time slot object.
                            AssessorTimeSlotDetail timeSlot = new AssessorTimeSlotDetail();

                            // Assign assessor.
                            timeSlot.Assessor = AvailabilityRequest_assessorDropDownList.SelectedItem.Text;
                            timeSlot.AssessorID = Convert.ToInt32(AvailabilityRequest_assessorDropDownList.SelectedItem.Value);

                            // Assign other details from the newly added time slot.
                            timeSlot.AvailabilityDate = newSlot.AvailabilityDate;
                            timeSlot.TimeSlotIDFrom = newSlot.TimeSlotIDFrom;
                            timeSlot.TimeSlotIDTo = newSlot.TimeSlotIDTo;
                            timeSlot.TimeSlot = newSlot.TimeSlot;
                            timeSlot.Comments = newSlot.Comments;
                            timeSlot.Skill = skill;
                            timeSlot.SkillID = skillID;

                            // Assign requester.
                            timeSlot.InitiatedBy = base.userID;

                            // Assign request status
                            timeSlot.RequestStatus = Constants.TimeSlotRequestStatus.PENDING_CODE;

                            // Add the item to the old slots.
                            oldSlots.Add(timeSlot);
                        }
                    }
                }

                // Keep the old slots in the session.
                Session["AVAILABILITY_REQUEST_LIST_TEMP"] = oldSlots;

                // Load selected time slots.
                LoadTimeSlots();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AvailabilityRequest_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the accept button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void AvailabilityRequest_acceptButton_Click(object sender, EventArgs e)
        {
            try
            {
                /*
                // Get new time slots from the temporary session.
                List<AssessorTimeSlotDetail> tempSlots = null;
                if (Session["AVAILABILITY_REQUEST_LIST_TEMP"] != null)
                {
                    tempSlots = Session["AVAILABILITY_REQUEST_LIST_TEMP"] as
                        List<AssessorTimeSlotDetail>;
                }

                // Get existing time slots from the original session.
                List<AssessorTimeSlotDetail> existingSlots = null;
                if (Session["AVAILABILITY_REQUEST_LIST"] != null)
                {
                    existingSlots = Session["AVAILABILITY_REQUEST_LIST"] as
                        List<AssessorTimeSlotDetail>;
                }

                // Instantiate existing time slots.
                if (existingSlots == null)
                    existingSlots = new List<AssessorTimeSlotDetail>();

                // Append the new slots to the existing time slots.
                if (tempSlots != null)
                {
                    existingSlots.AddRange(tempSlots);
                }

                // Keep the append existing time slots to the session.
                Session["AVAILABILITY_REQUEST_LIST"] = existingSlots;

                */
                // Keep the append existing time slots to the original session.
                Session["AVAILABILITY_REQUEST_LIST"] = Session["AVAILABILITY_REQUEST_LIST_TEMP"];

                // Close the window.
                ScriptManager.RegisterStartupScript(this, this.GetType(), 
                    "Closeform", "<script language='javascript'>self.close();</script>", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AvailabilityRequest_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the list of assessors.
        /// </summary>
        private void LoadAssessors()
        {
            if (Session["SESSION_ASSESSORS"] == null)
            {
                base.ShowMessage(AvailabilityRequest_topErrorMessageLabel, 
                    "No assessors found to process");
                return;
            }

            // Get assessors from session.
            SessionDetail sessionDetail = Session["SESSION_ASSESSORS"] as SessionDetail;
            if (sessionDetail.Assessors == null || sessionDetail.Assessors.Count == 0)
            {
                base.ShowMessage(AvailabilityRequest_topErrorMessageLabel, 
                    "No assessors found to process");
                return;
            }

            // Load assessors.
            AvailabilityRequest_assessorDropDownList.DataSource = sessionDetail.Assessors;
            AvailabilityRequest_assessorDropDownList.DataTextField = "FirstName";
            AvailabilityRequest_assessorDropDownList.DataValueField = "UserID";
            AvailabilityRequest_assessorDropDownList.DataBind();
            AvailabilityRequest_assessorDropDownList.Items.Insert(0, new ListItem("--Select--", "0"));

            // Show the alert message to the user.
            base.ShowMessage(AvailabilityRequest_topErrorMessageLabel,
                "Select assessor to request time slots");
        }

        /// <summary>
        /// Method that loads the time slots.
        /// </summary>
        private void LoadTimeSlots()
        {
            // Get the temp time slots list.
            List<AssessorTimeSlotDetail> tempSlots = null;

            if (Session["AVAILABILITY_REQUEST_LIST_TEMP"] != null)
                tempSlots = Session["AVAILABILITY_REQUEST_LIST_TEMP"] as List<AssessorTimeSlotDetail>;

            // Assign time slots to the grid.
            AvailabilityRequest_selectedTimeSlotsGridView.DataSource = tempSlots;
            AvailabilityRequest_selectedTimeSlotsGridView.DataBind();
        }

        /// <summary>
        /// Method that loads the skills for the selected assessor.
        /// </summary>
        private void LoadSkills()
        {
            // Get index.
            int index = AvailabilityRequest_assessorDropDownList.SelectedIndex;

            // Check if assessor is selected.
            if (index == 0)
            {
                // Clear skills list.
                AvailabilityRequest_skillsCheckBoxList.Items.Clear();
                AvailabilityRequest_skillsCheckBoxList.DataSource = null;
                AvailabilityRequest_skillsCheckBoxList.DataBind();

                AvailabilityRequest_selectedAssessorID.Value = null;

                // Hide the add time slots link.
                AvailabilityRequest_addTimeSlotsLinkButton.Visible = false;

                // Hide the email assessor image button.
                AvailabilityRequest_emailImageButton.Visible = false;

                base.ShowMessage(AvailabilityRequest_topErrorMessageLabel,
                   "Select assessor to request time slots");

                return;
            }

            // Decrement index to point to correct assessor.
            index--;

            // Get assessor record.
            AssessorDetail assessorDetail = (Session["SESSION_ASSESSORS"] as SessionDetail).
                Assessors[index];

            AvailabilityRequest_selectedAssessorID.Value = assessorDetail.UserID.ToString();

            // Show the email assessor image button.
            AvailabilityRequest_emailImageButton.Visible = true;

            // Add handler to email assessor image button.
            AvailabilityRequest_emailImageButton.Attributes.Add("onclick",
                "return ShowEmailAssessor('" + AvailabilityRequest_selectedAssessorID.Value + "','REQ');");

            // Keep selected assessor name in session.
            Session["AVAILABILITY_REQUEST_SELECTED_ASSESSOR"] = assessorDetail.FirstName;

            // Load assessor skills.
            AvailabilityRequest_skillsCheckBoxList.DataSource = assessorDetail.Skills;
            AvailabilityRequest_skillsCheckBoxList.DataTextField = "SkillName";
            AvailabilityRequest_skillsCheckBoxList.DataValueField = "SkillID";
            AvailabilityRequest_skillsCheckBoxList.DataBind();

            // Check if skills is selected.
            bool skillSelected = false;
            for (int skillIndex = 0; skillIndex < AvailabilityRequest_skillsCheckBoxList.Items.Count;
                skillIndex++)
            {
                if (AvailabilityRequest_skillsCheckBoxList.Items[skillIndex].Selected)
                {
                    skillSelected = true;
                }
            }

            if (skillSelected == false)
            {
                // Hide the add time slots link.
                AvailabilityRequest_addTimeSlotsLinkButton.Visible = false;

                base.ShowMessage(AvailabilityRequest_topErrorMessageLabel,
                   "Select skills to request time slots");

                return;
            }

            // Show the add time slot link button.
            AvailabilityRequest_addTimeSlotsLinkButton.Visible = true;

            // Add handler to add/edit time slot button.
            AvailabilityRequest_addTimeSlotsLinkButton.Attributes.Add("onclick",
                "return ShowRequestTimeSlot('" + AvailabilityRequest_selectedAssessorID.Value + "','" +
                AvailabilityRequest_addTimeSlotsButton.ClientID + "');");
        }

        /// <summary>
        /// Method that initiates the request time slot process by checking 
        /// whether skills are selected, and assign handler to the add time 
        /// slot button.
        /// </summary>
        private void InitiateRequestTimeSlot()
        {
            // Check if skills is selected.
            bool skillSelected = false;
            string skills = string.Empty;
            for (int skillIndex = 0; skillIndex < AvailabilityRequest_skillsCheckBoxList.Items.Count;
                skillIndex++)
            {
                if (AvailabilityRequest_skillsCheckBoxList.Items[skillIndex].Selected)
                {
                    skillSelected = true;
                    if (skills == string.Empty)
                        skills = AvailabilityRequest_skillsCheckBoxList.Items[skillIndex].Text;
                    else
                        skills = skills + "," + AvailabilityRequest_skillsCheckBoxList.Items[skillIndex].Text;
                }
            }
           
            if (skillSelected == false)
            {
                // Hide the add time slots link.
                AvailabilityRequest_addTimeSlotsLinkButton.Visible = false;

                base.ShowMessage(AvailabilityRequest_topErrorMessageLabel,
                   "Select skills to request time slots");

                return;
            }

            // Show the add time slot link button.
            AvailabilityRequest_addTimeSlotsLinkButton.Visible = true;

            // Show the help message to the user to select time slots.
            base.ShowMessage(AvailabilityRequest_topSuccessMessageLabel,
                "Click on 'Request Time Slots' to request time slots for the selected assessor and skills");

            // Keep selected skills in session as comma separated.
            Session["AVAILABILITY_REQUEST_SELECTED_SKILLS"] = skills;

            // Add handler to add time slot button.
            AvailabilityRequest_addTimeSlotsLinkButton.Attributes.Add("onclick",
                "return ShowRequestTimeSlot('" + AvailabilityRequest_selectedAssessorID.Value + "','" +
                AvailabilityRequest_addTimeSlotsButton.ClientID + "');");
        }

        #endregion Private Methods

    }
}