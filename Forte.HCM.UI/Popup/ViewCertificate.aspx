﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewCertificate.aspx.cs"
    Inherits="Forte.HCM.UI.Popup.ViewCertificate" MasterPageFile="~/MasterPages/PopupMaster.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>   
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<asp:Content ID="ViewCertificate_content" ContentPlaceHolderID="PopupMaster_contentPlaceHolder"
    runat="server">

    <script type="text/javascript" language="javascript">
        // Function that will close the popup window.
        function CloseMe() {
            self.close();
            return false;
        }
        
        // Function that will show the email confirm message extender.
        function ShowEmailConfirm() {
            //displays email confirmation pop up extender
            $find("<%= ViewCertificate_emailConfirmation_ModalPopupExtender.ID %>").show();
            return false;
        }
        
        // Function that will open the download window.
        function OpenPopUp() {
            window.open('../Common/Download.aspx?type=<%=EmailAttachment_downloadTypeHiddenField.Value %>', '', 'toolbar=0,resizable=0,width=1,height=1', '');
        }
    </script>

    <table width="100%" border="0" cellspacing="1" cellpadding="1">
        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="width: 50%" class="popup_header_text_grey" align="left">
                                        <asp:Literal ID="ViewCertificate_headerLiteral" runat="server" Text="View Certificate"></asp:Literal>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <asp:ImageButton ID="ViewCertificate_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                            OnClientClick="javascript:CloseMe()" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="popup_td_padding_10" valign="top">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_question_inner_bg">
                                <tr>
                                    <td class="msg_align">
                                        <asp:Label ID="ViewCertificate_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                        <asp:Label ID="ViewCertificate_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ViewCertificate_expiryDateHeadLabel" runat="server" Text="Expiry Date"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    &nbsp; &nbsp;
                                                    <asp:Label ID="ViewCertificate_expiryDateLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                    <asp:HiddenField ID="ViewCertificate_htmlTextHiddenField" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" align="center">
                                                    <div id="ViewCertificate_previewDiv" runat="server" style="height: 395px; width: 595px;
                                                        overflow: auto;">
                                                        <asp:Image ID="ViewCertificate_certificatePreviewImage" runat="server" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%" border="0" cellpadding="4" cellspacing="4">
                    <tr>
                        <td>
                            <asp:Button ID="ViewCertificate_downloadButton" runat="server" Text="Download" SkinID="sknButtonId" />
                            <asp:HiddenField ID="EmailAttachment_downloadTypeHiddenField" runat="server" />
                            <asp:Button ID="ViewCertificate_emailButton" runat="server" Text="Email" SkinID="sknButtonId"
                                OnClientClick="javascript:return ShowEmailConfirm();" />
                            <asp:LinkButton ID="ViewCertificate_cancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:return self.close();"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="ViewCertificate_emailConfirmationPanel" runat="server" Style="display: none;
                    height: 205px; width: 250px;" CssClass="popupcontrol_confirm">
                    <div id="ViewCertificate_emailConfirmationDiv" style="display: none">
                        <asp:Button ID="ViewCertificate_emailConfirmation_hiddenButton" runat="server" />
                    </div>
                    <uc3:confirmmsgcontrol id="ViewCertificate_emailConfirmation_ConfirmMsgControl" runat="server"
                        title="Email Confirmation" type="EmailConfirmType" message="How do you want to email your certificate?" />
                </asp:Panel>
                <ajaxtoolkit:modalpopupextender id="ViewCertificate_emailConfirmation_ModalPopupExtender"
                    behaviorid="ViewCertificate_emailConfirmation_ModalPopupExtender" runat="server"
                    popupcontrolid="ViewCertificate_emailConfirmationPanel" targetcontrolid="ViewCertificate_emailConfirmation_hiddenButton"
                    backgroundcssclass="modalBackground">
                </ajaxtoolkit:modalpopupextender>
            </td>
        </tr>
    </table>
</asp:Content>
