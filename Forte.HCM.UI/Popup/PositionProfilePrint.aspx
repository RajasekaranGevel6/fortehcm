﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PositionProfilePrint.aspx.cs"
    Inherits="Forte.HCM.UI.Popup.PositionProfilePrint" MasterPageFile="~/MasterPages/PopupMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="HTMLEditor" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PopupMaster_contentPlaceHolder"
    runat="server">
    <script type="text/javascript">
        // This function helps to Open save/open dialog
        function OpenDownloadPopUp(fileName, type) {
            window.open('../Common/Download.aspx?documentId='
            + fileName + '&type=' + type
            + '', '', 'toolbar=0,resizable=0,width=1,height=1', '');
        }

        function DownloadResume(candidateid) {
            window.open('../Common/Download.aspx?candidateid=' + candidateid + '&type=DownloadProfilePrintCandidateResume' + '', '', 'toolbar=0,resizable=0,width=1,height=1', '');
        }
        function ReloadParentWindow() {
            window.opener.document.getElementById('PositionProfileStatus_topResetLinkButton').click();

        }

        function CallParentWindowFunction() {
            window.opener.ParentWindowFunction();
            //self.opener.window.document.getElementById(buttonid).click();
            return false;
        }

        function CloseUpdate() {
            var buttonid = '<%= Request.QueryString["buttonid"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }
            if (buttonid != null) {
                window.opener.document.getElementById(buttonid).click();
            }
            self.close();
        }

    </script>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <asp:HiddenField ID="PositionProfilePrint_adminUserIdHiddenField" runat="server" />
                <asp:HiddenField ID="PositionProfilePrint_creditsEarnedHiddenField" runat="server" />
                <%--  <asp:UpdatePanel ID="PositionProfilePrint_updatePanel" runat="server">
                    <ContentTemplate>--%>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="width: 50%" class="popup_header_text_grey" align="left">
                                        <asp:Literal ID="PositionProfilePrint_headerLiteral" runat="server" Text="Email Position Profile"></asp:Literal>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <asp:ImageButton ID="PositionProfilePrint_cancelImageButton" TabIndex="7" runat="server"
                                            SkinID="sknCloseImageButton" OnClientClick="javascript:return CloseMe();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="popup_td_padding_10" valign="top">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_question_inner_bg">
                                <tr>
                                    <td class="msg_align">
                                        <asp:Label ID="PositionProfilePrint_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                        <asp:Label ID="PositionProfilePrint_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                        <asp:HiddenField ID="PositionProfilePrint_fromHiddenField" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td colspan="2">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 15%">
                                                                <asp:Label ID="PositionProfilePrint_fromLabel" runat="server" Text="From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 55%">
                                                                <asp:Label ID="PositionProfilePrint_fromValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                            <td style="width: 55%" align="right">
                                                                <asp:CheckBox ID="PositionProfilePrint_sendMeACopyCheckBox" runat="server" Checked="true"
                                                                    TextAlign="Right" Text="Send me a copy" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%">
                                                    <asp:Label ID="PositionProfilePrint_toLabel" runat="server" Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    <span class="mandatory">*</span>
                                                </td>
                                                <td>
                                                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="width: 97%">
                                                                <asp:TextBox ID="PositionProfilePrint_toTextBox" TabIndex="1" runat="server" MaxLength="500"
                                                                    onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)" SkinID="sknMultiLineTextBox"
                                                                    TextMode="MultiLine" Width="99%">
                                                                </asp:TextBox>
                                                            </td>
                                                            <td style="width: 3%">
                                                                <asp:ImageButton ID="PositionProfilePrint_toAddressImageButton" runat="server" SkinID="sknMailToImageButton"
                                                                    ToolTip="Click here to select addresses" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="PositionProfilePrint_ccLabel" runat="server" Text="CC" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="width: 97%">
                                                                <asp:TextBox ID="PositionProfilePrint_ccTextBox" runat="server" Width="99%" TextMode="MultiLine"
                                                                    SkinID="sknMultiLineTextBox" Height="18" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                                    onchange="CommentsCount(500,this)" TabIndex="2"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 3%">
                                                                <asp:ImageButton ID="PositionProfilePrint_ccAddressImageButton" runat="server" SkinID="sknMailToImageButton"
                                                                    ToolTip="Click here to select addresses" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="PositionProfilePrint_subjectLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="PositionProfilePrint_subjectTextBox" TabIndex="3" runat="server"
                                                        MaxLength="100" onkeyup="CommentsCount(100,this)" onchange="CommentsCount(100,this)"
                                                        SkinID="sknMultiLineTextBox" Height="18" Width="99%" TextMode="MultiLine">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr id="PositionProfilePrint_attachmentTR" runat="server" visible="false">
                                                <td>
                                                    <asp:Label ID="PositionProfilePrint_attachmentLabel" runat="server" Text="Attachment"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    <div style="height: 90px; width: 570px; overflow: auto;">
                                                        <asp:UpdatePanel ID="PositionProfilePrint_attachmentUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView ID="PositionProfilePrint_attachmentGridView" runat="server" OnRowCommand="PositionProfilePrint_attachmentGridView_RowCommand"
                                                                    OnRowDataBound="PositionProfilePrint_attachmentGridView_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Candidate Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="PositionProfilePrint_attachmentGridViewCandidateNameLabel" runat="server"
                                                                                    Text='<%# Eval("CandidateName") %>'></asp:Label>
                                                                                <asp:HiddenField ID="PositionProfilePrint_attachmentGridViewCandidateIDHiddenField"
                                                                                    runat="server" Value='<%# Eval("canID") %>' />
                                                                                <asp:HiddenField ID="PositionProfilePrint_attachmentGridViewPositionProfileCandidateIDHiddenField"
                                                                                    runat="server" Value='<%# Eval("PositionProfileCandidateID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Download Resume">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="PositionProfilePrint_resumeAttachmentImageButton" runat="server"
                                                                                    ToolTip="Attachment" AlternateText="Attachment" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_word.gif"
                                                                                    Visible='<%# Eval("ISResume") %>' CommandArgument='<%# Eval("canID") %>' />
                                                                                <asp:CheckBox ID="PositionProfilePrint_attachmentGridViewResumeCheckBox" Text="Resume"
                                                                                    runat="server" Visible='<%# Eval("ISResume") %>' Checked="true" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Download Test Details">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="PositionProfilePrint_testDetailsAttachmentImageButton" runat="server"
                                                                                    ToolTip="Attachment" AlternateText="Attachment" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_pdf.gif"
                                                                                    Visible='<%# Eval("ISTestResult") %>' CommandArgument='<%# Eval("CandidateSessionKey")+ "," + Eval("TestKey")+ "," + Eval("AttemptID")%>'
                                                                                    CommandName="DownloadTestResult" />
                                                                                <asp:CheckBox ID="PositionProfilePrint_attachmentGridViewTestDetailsCheckBox" Text="Test Details"
                                                                                    runat="server" Visible='<%# Eval("ISTestResult") %>' Checked="true" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Download Interview Assessment Details">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="PositionProfilePrint_interviewDetailsAttachmentImageButton"
                                                                                    runat="server" ToolTip="Attachment" AlternateText="Attachment" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_pdf.gif"
                                                                                    Visible='<%# Eval("ISInterviewResult") %>' CommandArgument='<%# Eval("CandidateInterviewSessionKey")+ "," + Eval("CandidateInterviewSessionAttemptID")%>'
                                                                                    CommandName="DownloadInterviewResult" />
                                                                                <asp:CheckBox ID="PositionProfilePrint_attachmentGridViewInterviewDetailsCheckBox"
                                                                                    Text="Interview Details" Checked="true" runat="server" Visible='<%# Eval("ISInterviewResult") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr id="PositionProfilePrint_additionaldocuments" runat="server" visible="true">
                                                <td>
                                                    <asp:Label ID="PositionProfileadditionaldocument_lable" runat="server" Text="Additional Documents"
                                                        SkinID="sknLabelFieldHeaderText" Visible="true"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    <div style="height: 130px; width: 570px; overflow: auto;">
                                                        <asp:GridView ID="Positionprofileprint_additionaldocument" runat="server" OnRowDataBound="AdditionalDocument_gridView_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Candidate Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="PositionProfilePrint_additionalattachmentGridViewCandidateNameLabel"
                                                                            runat="server" Text='<%# Eval("CandidateFirstName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Additional Documents">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="PositionProfilePrint_additionalattachmentGridViewCandidateIDHiddenField"
                                                                            runat="server" Value='<%# Eval("ResumeID") %>' />
                                                                        <asp:ImageButton ID="PositionProfilePrint_documentAttachmentImageButton" runat="server"
                                                                            ToolTip="Attachment" AlternateText="Attachment" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_word.gif"
                                                                            Visible='<%# Eval("ISDocument") %>' CommandName="Downloadadditionaldocuments"
                                                                            CommandArgument='<%# Eval("ResumeID") %>' />
                                                                        <asp:CheckBox ID="PositionProfilePrint_additionaldocumentGridViewResumeCheckBox"
                                                                            Text='<%# Eval("ResumeTitle") %>' runat="server" Checked="true" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="PositionProfilePrint_messageLabel" runat="server" Text="Message" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class="mandatory">*</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <%--   <asp:TextBox ID="PositionProfilePrint_messageTextBox" TabIndex="4" TextMode="MultiLine"
                                                                    Height="200" runat="server" Width="99%" SkinID="sknMultiLineTextBox" MaxLength="500"
                                                                    onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)" Style="border-bottom: 0px;">
                                                                </asp:TextBox>--%>
                                                                <HTMLEditor:Editor ID="PositionProfilePrint_editor" runat="server" Height="260px"
                                                                    Width="100%" AutoFocus="true" ActiveMode="Design" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <%--<tr id="PositionProfilePrint_contentTR" runat="server" style="display: none;">
                                                        <td colspan="2">
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>--%>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="popup_td_padding_left_8">
                            <table border="0" cellpadding="0" cellspacing="3">
                                <tr>
                                    <td>
                                        <asp:Button ID="PositionProfilePrint_submitButton" runat="server" Text="Submit" OnClick="PositionProfilePrint_submitButton_Click"
                                            SkinID="sknButtonId" TabIndex="5" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="PositionProfilePrint_cancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknPopupLinkButton" TabIndex="6" OnClientClick="javascript:return CloseMe();"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%--   </ContentTemplate>
                </asp:UpdatePanel>--%>
            </td>
        </tr>
    </table>
</asp:Content>
