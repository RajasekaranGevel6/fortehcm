<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DictionaryEntry.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.DictionaryEntry" %>
 <%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>   
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<asp:Content ID="DictionaryEntry_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">

    <script type="text/javascript" language="javascript">

    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="DictionaryEntry_headerLiteral" runat="server" Text="Edit Dictionary"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="DictionaryEntry_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="msg_align">
                            <asp:UpdatePanel ID="DictionaryEntry_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="DictionaryEntry_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="DictionaryEntry_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="DictionaryEntry_dictionaryTypeDropDownList" />
                                    <asp:AsyncPostBackTrigger ControlID="DictionaryEntry_headTypeDropDownList" />
                                    <asp:AsyncPostBackTrigger ControlID="DictionaryEntry_addEntryButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="DictionaryEntry_selectHeaderUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr runat="server">
                                            <td class="header_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 100%">
                                                            <asp:Literal ID="DictionaryEntry_selectDictionaryHeader" runat="server" Text="Select Dictionary Header"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <table cellpadding="3" cellspacing="3" width="100%">
                                                    <tr>
                                                        <td style="width: 20%">
                                                            <asp:Label runat="server" ID="DictionaryEntry_addDictionaryTypeLabel" Text="Dictionary Type"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 29%">
                                                            <asp:DropDownList runat="server" ID="DictionaryEntry_dictionaryTypeDropDownList"
                                                                Width="90%" AutoPostBack="true" OnSelectedIndexChanged="DictionaryEntry_dictionaryTypeDropDownList_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:Label runat="server" ID="DictionaryEntry_headTypeLabel" Text="Head Type" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 36%">
                                                            <asp:DropDownList runat="server" ID="DictionaryEntry_headTypeDropDownList" Width="90%"
                                                                AutoPostBack="true" OnSelectedIndexChanged="DictionaryEntry_headTypeDropDownList_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_5">
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:UpdatePanel ID="DictionaryEntry_searchResultsUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr id="DictionaryEntry_searchTestResultsTR" runat="server">
                                            <td class="header_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 100%">
                                                            <asp:Literal ID="SearchCandidate_searchResultsLiteral" runat="server" Text="Dictionary Details"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left">
                                                            <div style="height: 226px; overflow: auto;" runat="server" id="DictionaryEntry_aliasesDiv">
                                                                <asp:GridView ID="DictionaryEntry_aliasesGridView" runat="server" AllowSorting="True"
                                                                    AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowCommand="DictionaryEntry_aliasesGridView_RowCommand">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="DictionaryEntry_aliasesGridView_idHiddenField" runat="server"
                                                                                    Value='<%# Eval("ID") %>' />
                                                                                <asp:ImageButton ID="DictionaryEntry_aliasesGridView_editImageButton" runat="server"
                                                                                    SkinID="sknEditDictionaryEntryImageButton" ToolTip="Edit Dictionary Entry" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                    CommandName="EditEntry" />&nbsp;
                                                                                <asp:ImageButton ID="DictionaryEntry_aliasesGridView_deleteImageButton" runat="server"
                                                                                    SkinID="sknDeleteDictionaryEntryImageButton" ToolTip="Delete Dictionary Entry"
                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="DeleteEntry" />&nbsp;
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-Width="88%" HeaderText="Alias">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="DictionaryEntry_aliasesGridView_aliasNameLabel" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="DictionaryEntry_dictionaryTypeDropDownList" />
                                    <asp:AsyncPostBackTrigger ControlID="DictionaryEntry_headTypeDropDownList" />
                                    <asp:AsyncPostBackTrigger ControlID="DictionaryEntry_addEntryButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="DictionaryEntry_addAliasUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="header_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 100%">
                                                            <asp:Literal ID="DictionaryEntry_addEntryLiteral" runat="server" Text="Add Dictionary Entry"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <table cellpadding="3" cellspacing="3" width="100%">
                                                    <tr>
                                                        <td style="width: 8%">
                                                            <asp:Label runat="server" ID="DictionaryEntry_addAliasLabel" Text="Alias" SkinID="sknLabelFieldHeaderText"></asp:Label><span
                                                                class="mandatory">*</span>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <asp:TextBox runat="server" ID="DictionaryEntry_addAliasTextBox" Width="100%" MaxLength="255"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 1%">
                                                            <asp:ImageButton ID="DictionaryEntry_addAliasHelpImageButton" SkinID="sknHelpImageButton"
                                                                runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                ToolTip="Enter the alias here" />
                                                        </td>
                                                        <td style="width: 8%" align="left">
                                                            <asp:Button runat="server" Text="Add" SkinID="sknButtonId" ID="DictionaryEntry_addEntryButton"
                                                                OnClick="DictionaryEntry_addEntryButton_Click" ToolTip="Add" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_5">
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_left_8">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="DictionaryEntry_resetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="DictionaryEntry_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="DictionaryEntry_cancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="DictionaryEntry_editEntryUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="DictionaryEntry_editEntryPanel" runat="server" CssClass="popupcontrol_edit_dictionary_entry"
                            Style="display: none">
                            <div style="display: none;">
                                <asp:Button ID="DictionaryEntry_editEntryPanel_hiddenButton" runat="server" Text="Hidden" />
                            </div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="DictionaryEntry_editEntryPanel_headerLiteral" runat="server" Text="Edit Dictionary Entry"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="DictionaryEntry_editEntryPanel_closeImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_8" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popupcontrol_edit_dictionary_entry_inner_bg" style="width: 100%" align="left"
                                                    valign="top" colspan="2">
                                                    <table border="0" cellpadding="2" cellspacing="5" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="2" cellspacing="3" width="100%" class="tab_body_bg"
                                                                    align="left">
                                                                    <tr>
                                                                        <td class="msg_align" colspan="3">
                                                                            <asp:Label ID="DictionaryEntry_editEntryPanel_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 8%">
                                                                            <asp:Label ID="DictionaryEntry_editEntryPanel_aliasLabel" runat="server" Text="Alias"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">*</span>
                                                                        </td>
                                                                        <td style="width: 91%">
                                                                            <asp:TextBox runat="server" ID="DictionaryEntry_editEntryPanel_aliasTextBox" Width="98%"
                                                                                MaxLength="255"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 1%">
                                                                            <asp:ImageButton ID="DictionaryEntry_editEntryPanel_aliasHelpImageButton" SkinID="sknHelpImageButton"
                                                                                runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                                ToolTip="Enter or modify the alias here" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5" colspan="2">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top">
                                                <td colspan="2">
                                                    <asp:Button ID="DictionaryEntry_editEntryPanel_updateButton" runat="server" Text="Update"
                                                        SkinID="sknButtonId" OnClick="DictionaryEntry_editEntryPanel_updateButton_Click" />
                                                    <asp:LinkButton ID="DictionaryEntry_editEntryPanel_cancelButton" runat="server" Text="Cancel"
                                                        SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5" colspan="2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="DictionaryEntry_editEntryPanelModalPopupExtender"
                            runat="server" TargetControlID="DictionaryEntry_editEntryPanel_hiddenButton"
                            PopupControlID="DictionaryEntry_editEntryPanel" BackgroundCssClass="modalBackground"
                            CancelControlID="DictionaryEntry_editEntryPanel_cancelButton">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="DictionaryEntry_deleteEntryUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="DictionaryEntry_deleteEntryPanel" runat="server" Style="display: none;
                            height: 202px" CssClass="dictionary_entry_delete_confirm">
                            <div style="display: none">
                                <asp:Button ID="DictionaryEntry_deleteEntryPanel_hiddenButton" runat="server" />
                            </div>
                            <uc1:ConfirmMsgControl ID="DictionaryEntry_deleteEntryPanel_confirmMessageControl"
                                runat="server" OnOkClick="DictionaryEntry_deleteEntryPanel_okButtonClick" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="DictionaryEntry_deleteEntryPanel_confirmModalPopupExtender"
                            runat="server" PopupControlID="DictionaryEntry_deleteEntryPanel" TargetControlID="DictionaryEntry_deleteEntryPanel_hiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
