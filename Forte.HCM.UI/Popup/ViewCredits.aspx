﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="ViewCredits.aspx.cs" Inherits="Forte.HCM.UI.Popup.ViewCredits" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<asp:Content ID="ViewCredits_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="ViewCredits_headerLiteral" runat="server" Text="View Credits"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="ViewCredits_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="msg_align">
                            <asp:UpdatePanel ID="ViewCredits_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="ViewCredits_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="ViewCredits_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ViewCredits_creditsTypeDropDownList" />
                                    <asp:AsyncPostBackTrigger ControlID="ViewCredits_pageNavigator" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="ViewCredits_creditsDiv" runat="server" style="display: block;">
                                <table width="100%">
                                    <tr>
                                        <td valign="top">
                                            <asp:UpdatePanel ID="ViewCredits_creditsUpdatePanel" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table width="100%" border="0"  >
                                                        <tr>                                                           
                                                            <td style="width: 12%;">
                                                                <asp:Label ID="ViewCredits_userIdLabel" runat="server" Text="User Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 16%;">
                                                                <asp:Label ID="ViewCredits_userIdValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                            <td style="width: 16%;">
                                                                <asp:Label ID="ViewCredits_creditsLabel" runat="server" Text="Available Credits (in $)"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 9%;">
                                                                <asp:Label ID="ViewCredits_availableCreditsValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>                                                          
                                                            <td colspan="4">
                                                                <div id="ViewCredits_DIV" runat="server" style=" display:none; width:100%;padding-right:1px">
                                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                                                                        <tr>
                                                                            <td style="width:11%;" align="left">
                                                                                <asp:Label ID="ViewCredits_creditsEarnedLabel" runat="server" Text="Credits Earned (in $)"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 15%;">
                                                                                <asp:Label ID="ViewCredits_creditsEarnedValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 15%;">
                                                                                <asp:Label ID="ViewCredits_creditsRedeemedLabel" runat="server" Text="Credits Redeemed (in $)"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 8%;">
                                                                                <asp:Label ID="ViewCredits_creditsRedeemedValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:UpdatePanel ID="ViewCredits_searchResultsUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table width="100%">
                                        <tr id="ViewCredits_creditsUsageHistoryResultsTR" runat="server">
                                            <td class="header_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 50%">
                                                            <asp:Literal ID="ViewCredits_searchResultsLiteral" runat="server" Text="Credits Usage History"></asp:Literal>&nbsp;<asp:Label
                                                                ID="ViewCredits_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%" align="right">
                                                            <span id="ViewCredits_searchResultsUpSpan" style="display: none;" runat="server">
                                                                <asp:Image ID="ViewCredits_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                            </span><span id="ViewCredits_searchResultsDownSpan" style="display: block;" runat="server">
                                                                <asp:Image ID="ViewCredits_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="td_height_2">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Label ID="ViewCredits_creditsTypeLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="Credit Type"></asp:Label>
                                                            <asp:DropDownList ID="ViewCredits_creditsTypeDropDownList" runat="server" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ViewCredits_creditsTypeDropDownList_SelectedIndexChanged"
                                                                Width="15%">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_2">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <div style="height: 220px; overflow: auto;" runat="server" id="ViewCredits_creditsUsageHistoryDiv">
                                                                <asp:GridView ID="ViewCredits_creditsUsageHistoryGridView" runat="server" AllowSorting="True"
                                                                    AutoGenerateColumns="False" GridLines="None" Width="100%" OnSorting="ViewCredits_creditsUsageHistoryGridView_Sorting"
                                                                    OnRowDataBound="ViewCredits_creditsUsageHistoryGridView_RowDataBound" OnRowCreated="ViewCredits_creditsUsageHistoryGridView_RowCreated">
                                                                    <Columns>
                                                                        <asp:BoundField HeaderText="Date" DataField="Date" SortExpression="DATE" DataFormatString="{0:MM/dd/yyyy}" />
                                                                        <asp:BoundField HeaderText="Credits (in $)" DataField="Credit" SortExpression="CREDIT" />
                                                                        <asp:BoundField HeaderText="Credit Type" DataField="UsageHistoryCreditType" SortExpression="USAGEHISTORYCREDITTYPE" />
                                                                        <asp:BoundField HeaderText="Description" DataField="Description" SortExpression="DESCRIPTION" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <uc1:PageNavigator ID="ViewCredits_pageNavigator" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ViewCredits_creditsTypeDropDownList" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_left_8">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="ViewCredits_topResetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="ViewCredits_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="ViewCredits_topCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="ViewCredits_isMaximizedHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
