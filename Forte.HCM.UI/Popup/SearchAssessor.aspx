<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="SearchAssessor.aspx.cs" Inherits="Forte.HCM.UI.Popup.SearchAssessor" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="AddAssessorConfirmMsgControl"
    TagPrefix="uc2" %> 
   
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>

<asp:Content ID="SearchAssessor_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl)
        {
            var idCtrl = '<%= Request.QueryString["idctrl"] %>';
            var nameCtrl = '<%= Request.QueryString["namectrl"] %>';
            var btncnrl = '<%= Request.QueryString["btncnrl"] %>';

            if (window.dialogArguments) 
            {
                window.opener = window.dialogArguments;
            }

            // Set assessor ID.
            if (idCtrl != null && idCtrl != '')
            {
                window.opener.document.getElementById(idCtrl).value
                    = document.getElementById(ctrl.id.replace("SearchAssessor_resultsGridView_selectLinkButton", "SearchAssessor_resultsGridView_userIDHiddenField")).value;
            }

            // Set assessor name.
            if (nameCtrl != null && nameCtrl != '')
            {
                window.opener.document.getElementById(nameCtrl).value
                    = document.getElementById(ctrl.id.replace("SearchAssessor_resultsGridView_selectLinkButton", "SearchAssessor_resultsGridView_nameHiddenField")).value;
            }

            // Trigger the click event of the refresh button in the parent page.
            if (btncnrl != null && window.opener.document.getElementById(btncnrl) != null)
                window.opener.document.getElementById(btncnrl).click();

            self.close();
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey" align="left">
                            <asp:Literal ID="SearchAssessor_headerLiteral" runat="server" Text="Search Assessor"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SearchAssessor_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" ToolTip="Click here to close the window"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="msg_align" style="height: 20px">
                                        <asp:UpdatePanel ID="SearchAssessor_messageUpdatePanel" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <asp:Label ID="SearchAssessor_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="SearchAssessor_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchAssessor_searchButton" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchAssessor_pageNavigator" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="SearchAssessor_searchCriteriasDiv" runat="server" style="display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="SearchAssessor_searchCriteriaUpdatePanel" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <table width="100%" border="0" cellspacing="3%" cellpadding="2%">
                                                                    <tr>
                                                                        <td align="left" style="width: 11%">
                                                                            <asp:Label ID="SearchAssessor_nameLabel" runat="server" 
                                                                                SkinID="sknLabelFieldHeaderText" Text="Name"></asp:Label>
                                                                        </td>
                                                                        <td align="left" style="width: 36%">
                                                                            <asp:TextBox ID="SearchAssessor_nameTextBox" MaxLength="50"  runat="server" Width="96%"></asp:TextBox>
                                                                        </td>
                                                                        <td align="left" style="width: 15%">
                                                                            <asp:Label ID="SearchAssessor_emailLabel" runat="server" Text="Email / Alt Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="left" style="width: 35%">
                                                                            <asp:TextBox ID="SearchAssessor_emailTextBox" MaxLength="50"  runat="server" Width="98%"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" align="left"> 
                                                                        </td>
                                                                        
                                                                        <td align="right" colspan="2" >
                                                                            <asp:CheckBox ID="SearchAssessor_showAssessorCheckBox" TextAlign="Right" Text="Show Assessor" SkinID="sknMyRecordCheckBox" runat="server" /> &nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <asp:Button ID="SearchAssessor_searchButton" runat="server" SkinID="sknButtonId"
                                                                                Text="Search" OnClick="SearchAssessor_searchButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 2px">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="SearchAssessor_searchResultsUpdatePanel" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr id="SearchAssessor_searchTestResultsTR" runat="server">
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 80%" align="left">
                                                                        <asp:Literal ID="SearchAssessor_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                            ID="SearchQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%" align="right">
                                                                        <span id="SearchAssessor_searchResultsUpSpan" style="display: none;" runat="server">
                                                                            <asp:Image ID="SearchAssessor_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                        </span><span id="SearchAssessor_searchResultsDownSpan" style="display: block;" runat="server">
                                                                            <asp:Image ID="SearchAssessor_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="height: 220px; overflow: auto;" runat="server" id="SearchAssessor_testDiv">
                                                                            <asp:GridView ID="SearchAssessor_resultsGridView" runat="server" AllowSorting="True"
                                                                                AutoGenerateColumns="False" GridLines="Horizontal" BorderColor="white" BorderWidth="1px"
                                                                                Width="100%" OnSorting="SearchAssessor_resultsGridView_Sorting" OnRowDataBound="SearchAssessor_resultsGridView_RowDataBound"
                                                                                OnRowCreated="SearchAssessor_resultsGridView_RowCreated" 
                                                                                onrowcommand="SearchAssessor_resultsGridView_RowCommand">
                                                                                <RowStyle CssClass="grid_alternate_row" />
                                                                                <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                                                <HeaderStyle CssClass="grid_header_row" />
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-Width="50px">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="SearchAssessor_resultsGridView_selectLinkButton" runat="server" Text="Select"
                                                                                                OnClientClick="javascript:return OnSelectClick(this);" ToolTip="Select"  Visible='<%# IsAssessor(Eval("IsAssessor").ToString())%>'></asp:LinkButton>
                                                                                            <asp:ImageButton ID="SearchAssessor_resultsGridView_createAssessorImageButton" runat="server"  CommandName="CreateAssessor" CommandArgument='<%# Eval("UserID") %>'
                                                                                                ToolTip="Mark As An Assessor" SkinID="sknCreateAssessorImageButton"  Visible='<%# !IsAssessor(Eval("IsAssessor").ToString())%>'/>
                                                                                            <asp:HiddenField ID="SearchAssessor_resultsGridView_nameHiddenField" runat="server" Value='<%# Eval("FirstName") %>' />
                                                                                            <asp:HiddenField ID="SearchAssessor_resultsGridView_userIDHiddenField" runat="server" Value='<%# Eval("UserID") %>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Name" SortExpression="ASSESSOR_NAME" HeaderStyle-Width="140px"
                                                                                        ItemStyle-Width="140px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchAssessor_resultsGridView_nameLabel" runat="server"
                                                                                                Text='<%# Eval("FirstName") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Email" SortExpression="EMAIL" HeaderStyle-Width="140px"
                                                                                        ItemStyle-Width="140px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchAssessor_resultsGridView_emailLabel" runat="server"
                                                                                                Text='<%# Eval("UserEmail") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Alt Email" SortExpression="ALTERNATE_EMAIL" HeaderStyle-Width="140px"
                                                                                        ItemStyle-Width="140px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SearchAssessor_resultsGridView_alternateEmailLabel" runat="server"
                                                                                                Text='<%# Eval("AlternateEmailID") %>'>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <uc1:PageNavigator ID="SearchAssessor_pageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchAssessor_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:HiddenField ID="SearchAssessor_assessorIdHiddenField" runat="server" />
                <asp:Panel ID="SearchAssessor_assessorPopupPanel" runat="server" Style="display: none"
                    CssClass="client_confirm_message_box">
                    <uc2:AddAssessorConfirmMsgControl ID="SearchAssessor_assessorConfirmMsgControl"
                        runat="server" OnCancelClick="SearchAssessor_assessorConfirmMsgControl_CancelClick" OnOkClick="SearchAssessor_assessorConfirmMsgControl_OkClick" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="SearchAssessor_assessorModalPopupExtender"
                    runat="server" PopupControlID="SearchAssessor_assessorPopupPanel" TargetControlID="SearchAssessor_assessorIdHiddenField"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_10">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="SearchAssessor_topResetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="SearchAssessor_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="SearchAssessor_topCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" ToolTip="Click here to close the window"/>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="SearchAssessor_isMaximizedHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
