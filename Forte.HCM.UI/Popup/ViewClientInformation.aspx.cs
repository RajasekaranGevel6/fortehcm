﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewClient.cs
// File that represents the user interface layout and functionalities for
// the ViewClient page. This will helps to view the client details such as
// name, address, website, list of departments, etc. This class inherits 
// Forte.HCM.UI.Common.PageBase.

#endregion Header                                                              

#region Directives                                                             

using System;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;
using System.Collections.Generic;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ViewClient page. This will helps to view the client details such as
    /// name, address, website, lis of departments, etc. This class inherits 
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ViewClientInformation : PageBase
    {
        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.SetFocus(ViewClientInformation_bottomCloseLinkButton.ClientID);
                
                // Set browser title.

                if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) && 
                    Request.QueryString["type"] == "CI")
                {
                    ViewClientInformation_headerLiteral.Text = "Client Detail";
                    Master.SetPageCaption("Client Detail");
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"] == "CD")
                {
                    ViewClientInformation_headerLiteral.Text = "Client Department";
                    Master.SetPageCaption("Client Department");
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"] == "CC")
                {
                    ViewClientInformation_headerLiteral.Text = "Client Contact";
                    Master.SetPageCaption("Client Contact");
                }
                else
                {
                    ViewClientInformation_headerLiteral.Text = "Client Information";
                    Master.SetPageCaption("Client Information");
                }

                if (!IsPostBack)
                {
                    ViewClientInformation_departmentAdditionalInfoLabel.Visible = false;
                    ViewClientInformation_departmentAdditionalInfoLabelValue.Visible = false;

                    ViewClientInformation_contactAdditionalInfoLabel.Visible = false;
                    ViewClientInformation_contactAdditionalInfoLabelValue.Visible = false;

                    ViewClientInformation_departmentsHeadLabel.Visible = false;
                    ViewClientInformation_departmentsLabel.Visible = false;

                    ViewClientInformation_contactsHeadLabel.Visible = false;
                    ViewClientInformation_contactsLabel.Visible = false;

                    ViewClientInformation_feinNoHeadLabel.Visible=false;
                    ViewClientInformation_feinNoLabel.Visible = false;

                    ViewClientInformation_wesiteURLHeadLabel.Visible=false;
                    ViewClientInformation_websiteURLHyperLink.Visible = false;
                    // Load client detail.
                    LoadClientDetail();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewClientInformation_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the client detail.
        /// </summary>
        public void LoadClientDetail()
        {
            // Check if client ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["id"]))
            {
                base.ShowMessage(ViewClientInformation_errorMessageLabel, "ID is not passed");
                return;
            }

            // Check if client ID is valid.
            int ID = 0;
            int.TryParse(Request.QueryString["id"].ToString(), out ID);

            if (ID == 0)
            {
                base.ShowMessage(ViewClientInformation_errorMessageLabel, "Invalid ID");
                return;
            }

            #region Declaration

            string clientName = string.Empty;
            string email = string.Empty;
            string phoneNo = string.Empty;
            string faxNo = string.Empty;
            string zipCode = string.Empty;
            string street = string.Empty;
            string city = string.Empty;
            string state = string.Empty;
            string country = string.Empty;
            string feinNo = string.Empty;
            string webSiteURL = string.Empty;
            string departments = string.Empty;
            string contacts = string.Empty;
            string clientAdditionalInfo = string.Empty;
            string departmentAdditionalInfo = string.Empty;
            string contactAdditionalInfo = string.Empty;

            string departmentName=string.Empty;
            string departmentDescription=string.Empty;

            string firstName = string.Empty;
            string lastName = string.Empty;
            string middleName = string.Empty;


            #endregion Declaration

            // Get client information.
            
            if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"] == "CI")
            {
                ClientInformation clientInfo = new ClientBLManager().GetClientInformationByClientId(ID);
                clientName = clientInfo.ClientName;
                email = clientInfo.ContactInformation.EmailAddress;
                phoneNo = clientInfo.ContactInformation.Phone.Mobile;
                faxNo = clientInfo.FaxNumber;
                zipCode = clientInfo.ContactInformation.PostalCode;
                street = clientInfo.ContactInformation.StreetAddress;
                city = clientInfo.ContactInformation.City;
                state = clientInfo.ContactInformation.State;
                country = clientInfo.ContactInformation.Country;
                feinNo = clientInfo.FeinNo;
                webSiteURL = clientInfo.ContactInformation.WebSiteAddress.Personal;

                departments = clientInfo.ClientDepartments;
                contacts = clientInfo.ClientContacts;

                clientAdditionalInfo = clientInfo.AdditionalInfo;

                ViewClientInformation_departmentsHeadLabel.Visible = true;
                ViewClientInformation_departmentsLabel.Visible = true;

                ViewClientInformation_contactsHeadLabel.Visible = true;
                ViewClientInformation_contactsLabel.Visible = true;

                ViewClientInformation_feinNoHeadLabel.Visible = true;
                ViewClientInformation_feinNoLabel.Visible = true;

                ViewClientInformation_wesiteURLHeadLabel.Visible = true;
                ViewClientInformation_websiteURLHyperLink.Visible = true;

                // Show client name
                ViewClientInformation_clientNameLabel.Text = clientName;
                ViewClientInformation_clientNameLabel.ToolTip = "Client Name";

                ViewClientInformation_emailIDHyperLink.ToolTip = "Click here to send a mail to client";

                trClientDepartmentContact.Style["display"] = "block";
            }

            // Get department information
            Department department = null;
            if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                Request.QueryString["type"] == "CD")
            {
                ViewClientInformation_departmentAdditionalInfoLabel.Visible = true;
                ViewClientInformation_departmentAdditionalInfoLabelValue.Visible = true;

                department = new ClientBLManager().GetDepartment(ID);

                departmentName = department.DepartmentName;
                departmentDescription = department.DepartmentDescription;
                departmentAdditionalInfo = department.AdditionalInfo;
                faxNo = department.FaxNumber;

                email = department.ContactInformation.EmailAddress;
                phoneNo = department.ContactInformation.Phone.Mobile;
                zipCode = department.ContactInformation.PostalCode;
                street = department.ContactInformation.StreetAddress;
                city = department.ContactInformation.City;
                state = department.ContactInformation.State;
                country = department.ContactInformation.Country;

                ViewClientInformation_departmentDescriptionLabel.Text = departmentDescription;
                trDepartmentDescription.Style["display"] = "block";

                // Get client additional information
                clientAdditionalInfo = department.DepartmentAdditionalInfo;

                // Show department name
                ViewClientInformation_clientNameLabel.Text = departmentName;
                ViewClientInformation_clientNameLabel.ToolTip = "Department Name";

                ViewClientInformation_emailIDHyperLink.ToolTip = "Click here to send a mail to department";

                ViewClientInformation_departmentAdditionalInfoLabelValue.Text = departmentAdditionalInfo;
                trDepartmentAdditionalInformation.Style["display"] = "block";
            }

            // Get contact information
            ClientContactInformation contactInformation = null;
            if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                Request.QueryString["type"] == "CC")
            {
                ViewClientInformation_departmentAdditionalInfoLabel.Visible = true;
                ViewClientInformation_departmentAdditionalInfoLabelValue.Visible = true;

                ViewClientInformation_contactAdditionalInfoLabel.Visible = true;
                ViewClientInformation_contactAdditionalInfoLabelValue.Visible = true;

                contactInformation = new ClientBLManager().GetContact(ID);

                firstName = contactInformation.FirstName;
                middleName = contactInformation.MiddleName;
                lastName = contactInformation.LastName;
                faxNo = contactInformation.FaxNo;

                email = contactInformation.ContactInformation.EmailAddress;
                phoneNo = contactInformation.ContactInformation.Phone.Mobile;
                zipCode = contactInformation.ContactInformation.PostalCode;
                street = contactInformation.ContactInformation.StreetAddress;
                city = contactInformation.ContactInformation.City;
                state = contactInformation.ContactInformation.State;
                country = contactInformation.ContactInformation.Country;
                
                // Get client additional information
                clientAdditionalInfo = contactInformation.ClientAdditionalInfo;

                // Show contact name
                ViewClientInformation_clientNameLabel.Text = string.Format("{0} {1} {2}",
                    firstName,middleName,lastName);
                ViewClientInformation_clientNameLabel.ToolTip = "Contact Name";

                ViewClientInformation_emailIDHyperLink.ToolTip = "Click here to send a mail to contact";

                trContactAdditionalInformation.Style["display"] = "block";
                ViewClientInformation_contactAdditionalInfoLabelValue.Text = contactInformation.AdditionalInfo;

                // Get list of contacts department details


                string depAdditionalInfo = string.Empty;
                List<Department> lstDepartments = new ClientBLManager().GetDepartmentsByContactID(ID);

                if (!Utility.IsNullOrEmpty(lstDepartments) && 
                    lstDepartments.Count > 0)
                {
                    foreach(Department depart in lstDepartments)
                    {
                        depAdditionalInfo += depart.AdditionalInfo + "<br><br>";
                    }
                }

                ViewClientInformation_departmentAdditionalInfoLabelValue.Text = depAdditionalInfo;
                trDepartmentAdditionalInformation.Style["display"] = "block";
            }

            // Assign URL for view more client details.
            ViewClientInformation_moreHyperLink.NavigateUrl = "../ClientCenter/ClientManagement.aspx" +
                "?m=0&s=0&parentpage=MENU&viewtype=C";

            // Keep the client name is session. When navigated to client management page by 
            // clicking on 'more...' this client name will be filled into the search criteria,
            // thus during search this client will be loaded.
            Session["CLIENT_MANAGEMENT_CLIENT_NAME"] = clientName;

            if (!Utility.IsNullOrEmpty(email))
            {
                ViewClientInformation_emailIDHyperLink.Text = email;
                ViewClientInformation_emailIDHyperLink.NavigateUrl = string.Format("mailto:{0}", email);
            }

            ViewClientInformation_phoneNoLabel.Text = phoneNo;
            ViewClientInformation_faxNoLabel.Text = faxNo;
            ViewClientInformation_zipLabel.Text = zipCode;
            ViewClientInformation_streetAddressLabel.Text = street;
            ViewClientInformation_cityLabel.Text = city;
            ViewClientInformation_statelabel.Text = state;
            ViewClientInformation_countryLabel.Text = country;
            ViewClientInformation_feinNoLabel.Text = feinNo;
            ViewClientInformation_websiteURLHyperLink.Text = webSiteURL;

            if (!Utility.IsNullOrEmpty(webSiteURL))
            {
                string navigateURL = webSiteURL;

                if (!navigateURL.ToUpper().Contains("HTTP"))
                    navigateURL = "http://" + navigateURL;

                ViewClientInformation_websiteURLHyperLink.NavigateUrl = navigateURL;
            }

            ViewClientInformation_additionalInfoLabel.Text = clientAdditionalInfo;
            ViewClientInformation_departmentsLabel.Text = departments;
            ViewClientInformation_contactsLabel.Text = contacts;
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Protected Overridden Methods
    }
}
