﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Geval6.
//
// CertificationTitleEntry.cs
// File that represents the User Interface and 
// fucntionalities of the Certification entry page.

#endregion Header

#region Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Represents the class that holds the User Interface and 
    /// fucntionalities of the certification titlepage.
    /// This page helps to view the  various certificates managed in the resume parser application
    /// </summary>    
    public partial class CertificationTitleEntry : PageBase
    {
        #region Event Handlers

        /// <summary>
        /// Method that will be called when the page is loads.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Certification Title Entry");
                // Set default button to 'add' button.
                Page.Form.DefaultButton = CertificationTitleEntry_addEntryButton.UniqueID;

                if (!IsPostBack)
                {
                    //Load group and vector name
                    LoadValues();

                    // Show the certificate titles.
                    ShowCertificateTitles();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CertificationTitleEntry_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called add button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will add the entry into database.
        /// </remarks>
        protected void CertificationTitleEntry_addEntryButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear message.
                CertificationTitleEntry_errorMessageLabel.Text = string.Empty;
                CertificationTitleEntry_successMessageLabel.Text = string.Empty;

                if (Utility.IsNullOrEmpty(Request.QueryString["vectorid"]))
                    return;

                // Check if the certification title is entered.
                if (CertificationTitleEntry_addCertificationTitleTextBox.Text.Trim().Length == 0)
                {
                    ShowMessage(CertificationTitleEntry_errorMessageLabel, "Certification title cannot be empty");
                    CertificationTitleEntry_addCertificationTitleTextBox.Focus();
                    return;
                }

                // Check if the certification title name already exist.
                if (ViewState["TITLES_LIST"] != null)
                {
                    if ((ViewState["TITLES_LIST"] as List<CertificationTitle>).Find(item => item.Title.ToUpper().
                        Trim() == CertificationTitleEntry_addCertificationTitleTextBox.Text.Trim().ToUpper()) != null)
                    {
                        ShowMessage(CertificationTitleEntry_errorMessageLabel, "Certification title already exist");
                        return;
                    }
                }

                if (base.userID == 0)
                {
                    ShowMessage(CertificationTitleEntry_errorMessageLabel,
                        "Your session was expired");
                    return;
                }

                // Construct certification title detail.
                CertificationTitle certificationDetail = new CertificationTitle();
                //certificationDetail.ID = 0;
                certificationDetail.Title = CertificationTitleEntry_addCertificationTitleTextBox.Text.Trim();
                certificationDetail.VectorID = Convert.ToInt32(Request.QueryString["vectorid"]);
                certificationDetail.CreatedBy = base.userID;

                // TODO call your bl method here.
                new CompetencyVectorBLManager().InserCertification(certificationDetail);

                // Clear text boxes.
                CertificationTitleEntry_addCertificationTitleTextBox.Text = string.Empty;

                // Refresh the certificate titles.
                ShowCertificateTitles();

                // Show success message.
                ShowMessage(CertificationTitleEntry_successMessageLabel, "Certification title added successfully");

                // Set the focus to add certification title text box.
                CertificationTitleEntry_addCertificationTitleTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CertificationTitleEntry_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when edit/delete is clicked in grid view
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        protected void CertificationTitleEntry_titlesGridView_RowCommand(object sender,
            GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandSource is ImageButton))
                    return;

                // Clear message.
                CertificationTitleEntry_errorMessageLabel.Text = string.Empty;
                CertificationTitleEntry_successMessageLabel.Text = string.Empty;

                // Update the message update panel.
                CertificationTitleEntry_messageUpdatePanel.Update();

                // Keep the selected values in view state.
                ViewState["SELECTED_ID"] = (((ImageButton)e.CommandSource).Parent.
                    FindControl("CertificationTitleEntry_titlesGridView_idHiddenField") as HiddenField).Value;
                ViewState["SELECTED_TITLES"] = (((ImageButton)e.CommandSource).Parent.
                     FindControl("CertificationTitleEntry_titlesGridView_titleLabel") as Label).Text;

                if (e.CommandName == "EditEntry")
                {
                    // Clear any previous messages.
                    CertificationTitleEntry_editEntryPanel_errorMessageLabel.Text = string.Empty;

                    // Assign values and show the edit entry panel.
                    CertificationTitleEntry_editEntryPanel_certificationTitleTextBox.Text =
                        (ViewState["SELECTED_TITLES"] == null ? string.Empty : ViewState["SELECTED_TITLES"].ToString());
                    CertificationTitleEntry_editEntryPanel_certificationTitleTextBox.Focus();
                    CertificationTitleEntry_editEntryPanelModalPopupExtender.Show();
                    CertificationTitleEntry_editEntryUpdatePanel.Update();
                }
                else if (e.CommandName == "DeleteEntry")
                {
                    // Set message, title and type for the confirmation control for edit entry.
                    CertificationTitleEntry_deleteEntryPanel_confirmMessageControl.Message =
                        string.Format("Are you sure to delete the certification title '{0}'?", ViewState["SELECTED_TITLES"]);
                    CertificationTitleEntry_deleteEntryPanel_confirmMessageControl.Title =
                        "Delete Certification Title";
                    CertificationTitleEntry_deleteEntryPanel_confirmMessageControl.Type =
                        MessageBoxType.YesNo;

                    CertificationTitleEntry_deleteEntryPanel_confirmModalPopupExtender.Show();
                    CertificationTitleEntry_deleteEntryUpdatePanel.Update();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CertificationTitleEntry_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the modal pop up update button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        ///  <remarks>
        /// This will update the entry into database.
        /// </remarks>
        protected void CertificationTitleEntry_editEntryPanel_updateButton_Click(object sender, EventArgs e)
        {
            // Clear any previous messages in the edit entry panel.
            CertificationTitleEntry_editEntryPanel_errorMessageLabel.Text = string.Empty;

            // Check if the certification title name is entered.
            if (CertificationTitleEntry_editEntryPanel_certificationTitleTextBox.Text.Trim().Length == 0)
            {
                ShowMessage(CertificationTitleEntry_editEntryPanel_errorMessageLabel,
                    "Certification title cannot be empty");

                // Reshow the edit entry panel.
                CertificationTitleEntry_editEntryPanelModalPopupExtender.Show();
                CertificationTitleEntry_editEntryUpdatePanel.Update();

                return;
            }

            // Check if the certification title name already exist.
            if (ViewState["TITLES_LIST"] != null)
            {
                if ((ViewState["TITLES_LIST"] as List<CertificationTitle>).Find(item => item.Title.ToUpper().
                    Trim() == CertificationTitleEntry_editEntryPanel_certificationTitleTextBox.Text.Trim().ToUpper() &&
                    item.ID != Convert.ToInt32(ViewState["SELECTED_ID"])) != null)
                {
                    ShowMessage(CertificationTitleEntry_editEntryPanel_errorMessageLabel,
                        "Certification title already exist");

                    // Reshow the edit entry panel.
                    CertificationTitleEntry_editEntryPanelModalPopupExtender.Show();
                    CertificationTitleEntry_editEntryUpdatePanel.Update();

                    return;
                }
            }

            // Construct certification title detail.
            CertificationTitle certificationDetail = new CertificationTitle();
            certificationDetail.ID = Convert.ToInt32(ViewState["SELECTED_ID"]);
            certificationDetail.Title = CertificationTitleEntry_editEntryPanel_certificationTitleTextBox.Text.Trim();

            if (base.userID == 0)
            {
                ShowMessage(CertificationTitleEntry_errorMessageLabel,
                    "Your session was expired");
                return;
            }
            certificationDetail.ModifiedBy = base.userID;

            new CompetencyVectorBLManager().UpdateCertification(certificationDetail);

            // Refresh the certificate titles.
            ShowCertificateTitles();

            // Show success message.
            ShowMessage(CertificationTitleEntry_successMessageLabel,
                "Certification title updated successfully");

            // Update the message & grid view update panel.
            CertificationTitleEntry_messageUpdatePanel.Update();
            CertificationTitleEntry_searchResultsUpdatePanel.Update();

            // Set default button to 'add' button.
            Page.Form.DefaultButton = CertificationTitleEntry_addEntryButton.UniqueID;
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked in
        /// the delete entry confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will delete the entry from the database.
        /// </remarks>
        protected void CertificationTitleEntry_deleteEntryPanel_okButtonClick(object sender, EventArgs e)
        {
            try
            {
                // Clear message.
                CertificationTitleEntry_errorMessageLabel.Text = string.Empty;
                CertificationTitleEntry_successMessageLabel.Text = string.Empty;

                // TODO call bl method here to delete the record.
                new CompetencyVectorBLManager().DeleteCertification
                        (Convert.ToInt32(ViewState["SELECTED_ID"]));

                // Show success message.
                ShowMessage(CertificationTitleEntry_successMessageLabel, "Certification title entry deleted successfully");

                // Refresh the certificate titles.
                ShowCertificateTitles();

                // Update the message & grid view update panel.
                CertificationTitleEntry_messageUpdatePanel.Update();
                CertificationTitleEntry_searchResultsUpdatePanel.Update();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CertificationTitleEntry_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the page to the defaut stage.
        /// </remarks>
        protected void CertificationTitleEntry_resetButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        #endregion Event Handlers

        #region Private Methods

        private void ShowCertificateTitles()
        {
            // Clear certification title grid.
            CertificationTitleEntry_titlesGridView.DataSource = null;
            CertificationTitleEntry_titlesGridView.DataBind();

            // Clear certification title list.
            ViewState["TITLES_LIST"] = null;

            if (Utility.IsNullOrEmpty(Request.QueryString["vectorid"]))
                return;

            List<CertificationTitle> titles = null;
            titles = new CompetencyVectorBLManager().GetCertificationTitle(Convert.ToInt32(Request.QueryString["vectorid"]));
            // TODO get from bl

            CertificationTitleEntry_titlesGridView.DataSource = titles;
            CertificationTitleEntry_certification_titleDiv.DataBind();

            // Keep the titles in view state.
            ViewState["TITLES_LIST"] = titles;

            if (titles == null || titles.Count == 0)
            {
                if (CertificationTitleEntry_successMessageLabel.Text.Trim().Length == 0)
                {
                    ShowMessage(CertificationTitleEntry_errorMessageLabel,
                        Resources.HCMResource.Common_Empty_Grid);
                }
                else
                {
                    ShowMessage(CertificationTitleEntry_errorMessageLabel, "<br/>" +
                        Resources.HCMResource.Common_Empty_Grid);
                }
            }
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            if (Utility.IsNullOrEmpty(Request.QueryString["vectorid"]))
                return;
            List<CertificationTitle> groupName = null;
            groupName = new CompetencyVectorBLManager().GetGroupName(Convert.ToInt32(Request.QueryString["vectorid"]));
            CertificationTitleEntry_vectorGroupValueLabel.Text = groupName[0].GroupName;
            CertificationTitleEntry_vectorValueLabel.Text = groupName[0].VectorName;
        }

        #endregion Protected Overridden Methods
    }
}