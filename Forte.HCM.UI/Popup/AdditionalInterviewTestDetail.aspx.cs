
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AdditionalTestDetail.cs
// File that represents the additional test details page.This page helps in 
// viewing the various test statistics details of the test
// 

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the additional test details page. This page helps in providing 
    /// information to the  user about the category subject and test area 
    /// distributions for the question.
    /// </summary>
    public partial class AdditionalInterviewTestDetail : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that is called when the page loads.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set Browser Tittle
                Master.SetPageCaption("Interview Statistics");
                string interviewTestKey = string.Empty;
                Page.SetFocus(AdditionalInterviewTestDetail_topCancelImageButton.ClientID);
                //Load the chart values
                if (!IsPostBack)
                {
                    LoadValues();
                }

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                ShowMessage(AdditionalInterviewTestDetail_topErrorMessageLabel,
                    exception.Message);
            }

        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        /// <summary>
        /// Method that will call the javascript function based on the input parameters.
        /// </summary>
        /// <param name="candidateTestDetail">
        /// A <see cref="CandidateTestDetail"/> that contains the candidate test detail.
        /// </param>
        /// <param name="interviewTestKey">
        /// A <see cref="string"/> that contains the test key.
        /// </param>
        private void ViewCertificateTemplateOrImage
            (CandidateTestDetail candidateTestDetail, string interviewTestKey)
        {
            if (candidateTestDetail != null)
            {
                AdditionalInterviewTestDetail_viewCertificateLinkButton.Attributes.Add("onclick",
                    "return OpenViewCertificate('" + candidateTestDetail.CandidateSessionID
                    + "','" + candidateTestDetail.AttemptID + "','" + interviewTestKey
                    + "','" + candidateTestDetail.CompletedOn + "','CERTIFICATE')");
            }
            else
            {
                AdditionalInterviewTestDetail_viewCertificateLinkButton.Attributes.Add("onclick",
                    "return OpenViewCertificate('dummy'" + ",'0'" + ",'" + interviewTestKey
                    + "','dummy'" + ",'TEMPLATE')");
            }
        }

        /// <summary>
        /// Method to load the interview summary details
        /// </summary>
        /// <param name="interviewTestKey">
        /// A<see cref="string"/>that holds the test id
        /// </param>
        private void LoadInterviewTestSummary(string interviewTestKey)
        {
            TestStatistics testStatistics = new InterviewReportBLManager()
                .GetAdditionalInterviewTestDetails(interviewTestKey);

            if (testStatistics == null)
                return;

            AdditionalInterviewTestDetail_noOfAdministeredValueLabel.Text =
                testStatistics.NoOfQuestions.ToString();

            AdditionalInterviewTestDetail_avgTimeTakenValueLabel.Text =
                Utility.ConvertSecondsToHoursMinutesSeconds
                (Convert.ToInt32(testStatistics.AverageTimeTakenByCandidates));

            AdditionalInterviewTestDetail_minScoreValueLabel.Text =
                testStatistics.LowestScore.ToString();

            AdditionalInterviewTestDetail_maxScoreValueLabel.Text =
                testStatistics.HighestScore.ToString();

            AdditionalInterviewTestDetail_avgScoreValueLabel.Text =
                testStatistics.AverageScore.ToString();

            AdditionalInterviewTestDetail_costValueLabel.Text =
                testStatistics.TestCost.ToString();
        }

        #endregion Private Methods                                             

        #region Protected Methods                                              

        /// <summary>
        /// Overridden protected method to check the valid data
        /// </summary>
        /// <returns>
        /// A<see cref="bool"/>value whether it is true or false
        /// </returns>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Overridden protected method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
            if (Utility.IsNullOrEmpty(Request.QueryString["interviewTestkey"]))
                return;

            string interviewTestKey = Request.QueryString["interviewTestkey"].ToString().Trim();

            TestDetail testDetail = new InterviewBLManager().GetInterviewTestDetail(interviewTestKey);
            bool isCertificateTest = Convert.ToBoolean(testDetail.IsCertification);

            // This will get candidate session id, attempt id, and test completed date infromation
            // based on the test key.
            // CandidateTestDetail candidateTestDetail = new TestBLManager().GetCandidateTestDetail(testKey);

            if (isCertificateTest)
            {
                AdditionalInterviewTestDetail_viewCertificationTR.Visible = true;
                //ViewCertificateTemplateOrImage(candidateTestDetail, testKey);
                AdditionalInterviewTestDetail_viewCertificateLinkButton.Attributes.Add("onclick",
                    "return OpenViewCertificate('dummy'" + ",'0'" + ",'" + interviewTestKey
                    + "','dummy'" + ",'TEMPLATE')");
            }

            //Assign the test id 
            AdditionalInterviewTestDetail_testIDValueLabel.Text = interviewTestKey;

            //Get the test statistics details for the test
            TestStatistics testStatisticsDatasource = new InterviewReportBLManager().
                GetInterviewTestSummaryDetails(interviewTestKey);

            if (testStatisticsDatasource == null)
                return;

            //Assign the test name            
            AdditionalInterviewTestDetail_testNameValueLabel.Text
                = testStatisticsDatasource.TestName;

            //Assign the data for the category chart control
            testStatisticsDatasource.CategoryStatisticsChartData.
                ChartType = SeriesChartType.Pie;

            //Assign the chart width
            testStatisticsDatasource.CategoryStatisticsChartData.
                ChartWidth = 350;

            //Assign the chart length
            testStatisticsDatasource.CategoryStatisticsChartData.
                ChartLength = 165;

            testStatisticsDatasource.CategoryStatisticsChartData.
                IsDisplayChartTitle = true;

            //Assign the chart title
            testStatisticsDatasource.CategoryStatisticsChartData.ChartTitle =
                Resources.HCMResource.AdditionalTestDetail_CategoryTitle;

            //Assign the chart type
            testStatisticsDatasource.CategoryStatisticsChartData.
               ChartType = SeriesChartType.Pie;

            //Assign the chart datasource
            AdditionalInterviewTestDetail_categoryChartControl.DataSource =
                testStatisticsDatasource.CategoryStatisticsChartData;

            //Assign the chart type
            testStatisticsDatasource.SubjectStatisticsChartData.
                ChartType = SeriesChartType.Pie;

            //Assign the chart width
            testStatisticsDatasource.SubjectStatisticsChartData.
                ChartWidth = 375;

            //Assign the chart Length
            testStatisticsDatasource.SubjectStatisticsChartData.
                ChartLength = 165;

            testStatisticsDatasource.SubjectStatisticsChartData.
                IsDisplayChartTitle = true;

            //Assign the chart title
            testStatisticsDatasource.SubjectStatisticsChartData.
                ChartTitle = Resources.HCMResource.AdditionalTestDetail_SubjectTitle;

            //Assign the data for the subject chart control
            AdditionalInterviewTestDetail_subjectChartControl.DataSource
                = testStatisticsDatasource.SubjectStatisticsChartData;

            //Assign the data for the testarea  chart control
            testStatisticsDatasource.TestAreaStatisticsChartData.
           ChartType = SeriesChartType.Pie;

            //Assign the chart width
            testStatisticsDatasource.TestAreaStatisticsChartData.
                ChartWidth = 350;

            //Assign the chart length
            testStatisticsDatasource.TestAreaStatisticsChartData.
                ChartLength = 165;

            testStatisticsDatasource.TestAreaStatisticsChartData.
                IsDisplayChartTitle = true;

            //Assign the chart title
            testStatisticsDatasource.TestAreaStatisticsChartData.
                ChartTitle = Resources.HCMResource.AdditionalInterviewTestDetail_TestAreaTitle;

            //Assign the test area chart data source
            AdditionalInterviewTestDetail_testAreaChartControl.DataSource
                = testStatisticsDatasource.TestAreaStatisticsChartData;

            //Assign the data for the complexity  chart control
            testStatisticsDatasource.ComplexityStatisticsChartData.
           ChartType = SeriesChartType.Pie;

            //Assign the chart width
            testStatisticsDatasource.ComplexityStatisticsChartData.
                ChartWidth = 350;

            //Assign the chart length
            testStatisticsDatasource.ComplexityStatisticsChartData.
                ChartLength = 165;

            testStatisticsDatasource.ComplexityStatisticsChartData.
                IsDisplayChartTitle = true;

            //Assign the chart title
            testStatisticsDatasource.ComplexityStatisticsChartData.
                ChartTitle = Resources.HCMResource.AdditionalTestDetail_ComplexityTitle;

            //Assign the data source
            AdditionalInterviewTestDetail_complexityChartControl.DataSource
                = testStatisticsDatasource.ComplexityStatisticsChartData;

            //Load the test summary details
            LoadInterviewTestSummary(interviewTestKey);
        }

        #endregion Protected Methods                                           
    }
}
