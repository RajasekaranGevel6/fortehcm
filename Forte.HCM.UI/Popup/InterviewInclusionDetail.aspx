﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="InterviewInclusionDetail.aspx.cs" Inherits="Forte.HCM.UI.Popup.InterviewInclusionDetail"%>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="InterviewInclusionDetail_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="InterviewInclusionDetail_titleLiteral" runat="server" Text="Interview Inclusion Details"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="InterviewInclusionDetail_closeImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:Label ID="InterviewInclusionDetail_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr id="InterviewInclusionDetail_searchResultsTR" runat="server">
                                    <td id="Td1" class="header_bg" runat="server">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 50%">
                                                    <asp:Literal ID="InterviewInclusionDetail_searchResultsLiteral" runat="server" Text="Interview Inclusion Details"></asp:Literal>
                                                    &nbsp;<asp:Label ID="InterviewInclusionDetail_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                        Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                </td>
                                                <td style="width: 50%" align="right">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="left">
                                                    <div style="height: 281px; width: 650px; overflow: auto;" runat="server" id="InterviewInclusionDetail_testInclusionDetailDiv">
                                                        <asp:UpdatePanel ID="InterviewInclusionDetail_testInclusionDetailGridView_updatePanel"
                                                            runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView ID="InterviewInclusionDetail_detailGridView" runat="server"
                                                                    SkinID="sknWrapHeaderGrid" AllowSorting="true" OnRowDataBound="InterviewInclusionDetail_detailGridView_RowDataBound"
                                                                    OnRowCreated="InterviewInclusionDetail_detailGridView_RowCreated" OnSorting="InterviewInclusionDetail_detailGridView_Sorting">
                                                                    <Columns>
                                                                        <asp:BoundField HeaderText="Interview ID" DataField="TestKey" SortExpression="TestKey"
                                                                            ItemStyle-Width="15%" />
                                                                        <asp:BoundField HeaderText="Interview Name" DataField="Name" ItemStyle-Width="30%" SortExpression="Name" />
                                                                        <asp:TemplateField HeaderText="Created Date" SortExpression="TestCreationDate" ItemStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="InterviewInclusionDetail_detailGridView_createdDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.TestDetail)Container.DataItem).TestCreationDate) %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Interview Author" SortExpression="TestAuthorName" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="InterviewInclusionDetail_detailGridView_testAuthorNameLabel" runat="server" Text='<%# Eval("TestAuthorName") %>'
                                                                                    ToolTip='<%# Eval("TestAuthorFullName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField HeaderText="Interview Administered" DataField="NoOfCandidatesAdministered"
                                                                            SortExpression="NoOfCandidatesAdministered" ItemStyle-CssClass="td_padding_right_20"
                                                                            ItemStyle-HorizontalAlign="right" ItemStyle-Width="15%" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc1:PageNavigator ID="InterviewInclusionDetail_pageNavigator" runat="server" />
                                        <asp:HiddenField ID="InterviewInclusionDetail_questionKeyHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_5">
                <asp:LinkButton ID="InterviewInclusionDetail_closeLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClientClick="javascript:CloseMe()" />
            </td>
        </tr>
    </table>
</asp:Content>
