﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;


namespace Forte.HCM.UI.Popup
{
    public partial class DateRequest : PageBase
    {
        #region Declaration

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="assessorDetail">
        /// A <see cref="UserDetail"/> that holds the assessor detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegate(UserDetail assessorDetail, EntityType entityType);

        #endregion Declaration

        #region Handler Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Master.SetPageCaption("Assessor Date Request");

                    Session["SELECATED_DATES"] = string.Empty;
                    ViewState["CURRENT_DATE"] = string.Empty;
                    ViewState["ASSESSOR_AVAILABLE_DATES"] = string.Empty;

                    ViewState["ASSESSOR_VACATION_DATE"] = string.Empty;
                    OnlineInterview_assessorCalendar_Calendar.VisibleDate = DateTime.Today;

                    LoadTimes();
                    //LoadEditTimeSlot();
                    GetAvailableDate();
                }
            }
            catch (Exception Exp)
            {
                Logger.ExceptionLog(Exp);
            }
        }

        protected void OnlineInterview_assessorCalendar_Calendar_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                DateRequest_availableTimeCheckBoxList.Enabled = true;
                int assDateGenId = new AssessorBLManager().GetAssessorAvailableDateGenId(Convert.ToInt32(Request.QueryString["userid"]),
                    OnlineInterview_assessorCalendar_Calendar.SelectedDate);

                GetAsssessorInterviews(Convert.ToInt32(Request.QueryString["userid"]), OnlineInterview_assessorCalendar_Calendar.VisibleDate.Month,
                    OnlineInterview_assessorCalendar_Calendar.SelectedDate);

                ClearCheckBoxSelection();

                LoadTimeSlots(assDateGenId);

                if (assDateGenId > 0)
                {
                    ViewState["DATE_REQUEST_GENID"] = assDateGenId;

                    LoadEditTimeSlot();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void OnlineInterview_assessorCalendar_Calendar_DayRender(object sender, DayRenderEventArgs e)
        {
            try
            {
                if (!Utility.IsNullOrEmpty(ViewState["ASSESSOR_AVAILABLE_DATES"]))
                {
                    List<AssessorTimeSlotDetail> availabilityDates = null;

                    availabilityDates = ViewState["ASSESSOR_AVAILABLE_DATES"] as List<AssessorTimeSlotDetail>;

                    foreach (AssessorTimeSlotDetail ass in availabilityDates)
                    {
                        if (e.Day.Date == ass.AvailabilityDate && ass.RequestStatus == "Approved" && ass.Scheduled == "S")
                        {
                            e.Cell.BackColor = System.Drawing.Color.FromArgb(153, 221, 150);
                            e.Cell.ToolTip = "Scheduled date";
                            //e.Day.IsSelectable = false;
                            //89, 193, 238
                        }
                        else if (e.Day.Date == ass.AvailabilityDate && ass.RequestStatus == "Approved" && ass.Scheduled == "N")
                        {
                            e.Cell.BackColor = System.Drawing.Color.FromArgb(112, 146, 190);
                            e.Cell.ToolTip = "Request Approved but not yet scheduled";
                            // e.Day.IsSelectable = false;
                        }
                        else if (e.Day.Date == ass.AvailabilityDate && ass.RequestStatus == "Initiated" && (ass.Scheduled == "N" || ass.Scheduled == "S"))
                        {
                            e.Cell.BackColor = System.Drawing.Color.FromArgb(255, 212, 85);
                            e.Cell.ToolTip = "Request initiated";
                            //e.Day.IsSelectable = false;
                        }
                    }
                }


                if (!Utility.IsNullOrEmpty(ViewState["ASSESSOR_VACATION_DATE"]))
                {

                    string[] vacationDates = ViewState["ASSESSOR_VACATION_DATE"].ToString().Split(',');

                    if (vacationDates == null) return;

                    foreach (string vDate in vacationDates)
                    {
                        if (e.Day.Date == DateTime.Parse(vDate, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None))
                        {
                            e.Cell.BackColor = System.Drawing.Color.FromArgb(242, 147, 120);
                            e.Day.IsSelectable = false;
                        }
                    }
                }

                if (!Utility.IsNullOrEmpty(Session["SELECATED_DATES"]))
                {
                    string[] selectionDates = Session["SELECATED_DATES"].ToString().Split(',');

                    if (selectionDates == null) return;

                    foreach (string sDate in selectionDates)
                    {
                        if (Utility.IsNullOrEmpty(sDate) || sDate == ",") break;

                        /*if (e.Day.Date == DateTime.Parse(sDate, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None))
                        {*/
                        if (e.Day.Date.ToString() == sDate.ToString())
                        {
                            e.Cell.BackColor = System.Drawing.Color.FromArgb(89, 193, 238);
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void OnlineInterview_assessorCalendar_saveButton_Click(object sender, EventArgs e)
        {
            // Clear messages.
            DateRequest_topSuccessMessageLabel.Text = string.Empty;
            DateRequest_topErrorMessageLabel.Text = string.Empty;

            if (!IsValidData()) return;

            List<AssessorTimeSlotDetail> selectedTimes = new List<AssessorTimeSlotDetail>();
            selectedTimes = ConstructSelectedTimes();

            AssessorTimeSlotDetail proposedDateDetail = new AssessorTimeSlotDetail();

            proposedDateDetail.InitiatedBy = base.userID;
            proposedDateDetail.AssessorID = Convert.ToInt32(Request.QueryString["userid"]);
            proposedDateDetail.RequestStatus = Constants.TimeSlotRequestStatus.PENDING_CODE;
            proposedDateDetail.IsAvailableFullTime = "N";

            proposedDateDetail.AvailabilityDate = OnlineInterview_assessorCalendar_Calendar.SelectedDate;

            // Save the time slots.
            if (Utility.IsNullOrEmpty(ViewState["DATE_REQUEST_GENID"]) || Convert.ToInt32(ViewState["DATE_REQUEST_GENID"]) == 0)
            {
                new AssessorBLManager().SaveAssessorAvailableDate("A", proposedDateDetail, selectedTimes);
            }
            else
            {
                proposedDateDetail.RequestDateGenID = Convert.ToInt32(ViewState["DATE_REQUEST_GENID"]);
                new AssessorBLManager().SaveAssessorAvailableDate("U", proposedDateDetail, selectedTimes);
            }

            ViewState["DATE_REQUEST_GENID"] = null;

            DateRequest_topErrorMessageLabel.Text = string.Empty;
            DateRequest_topSuccessMessageLabel.Text = string.Empty;

            base.ShowMessage(DateRequest_topSuccessMessageLabel, "Requested date saved successfully.");

            //LoadTimes();
            //LoadEditTimeSlot();
            GetAvailableDate();

            //Page_Load(new object(), new EventArgs());

            /*AssessorTimeSlotDetail assessorAvailDates = null;
            if (!Utility.IsNullOrEmpty(Session["SELECATED_DATES"]))
            {
                string[] selectionDates = Session["SELECATED_DATES"].ToString().Split(',');
                if (selectionDates == null) return;

                int requestDateGenID = 0;
                foreach (string sDate in selectionDates)
                {
                    if (Utility.IsNullOrEmpty(sDate) || sDate == ",") continue;

                    assessorAvailDates = new AssessorTimeSlotDetail();
                    assessorAvailDates.AssessorID = Convert.ToInt32(Request.QueryString["userid"]);
                    assessorAvailDates.AvailabilityDate = Convert.ToDateTime(sDate);
                    assessorAvailDates.InitiatedBy = base.userID;
                    assessorAvailDates.RequestStatus = Constants.TimeSlotRequestStatus.PENDING_CODE;
                    assessorAvailDates.IsAvailableFullTime = "N";
                    requestDateGenID = new OnlineInterviewAssessorBLManager().InsertAssessorAvailabilityDates(assessorAvailDates);
                } 

            }*/

            UserDetail assessorDetail = new UserDetail();

            //UserDetail GetUserDetail
            assessorDetail = new CommonBLManager().GetUserDetail(Convert.ToInt32(Request.QueryString["userid"]));

            //Set the requester name
            assessorDetail.RequestedBy = string.Format("{0} {1}",
                ((UserDetail)Session["USER_DETAIL"]).FirstName,((UserDetail)Session["USER_DETAIL"]).LastName); 

            //Send mail to the requested user
            // Sent alert mail to the associated user asynchronously.
            AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendRequestAvailabilityDateEmail);
            IAsyncResult result = taskDelegate.BeginInvoke(assessorDetail,
                EntityType.RequestAvailabilityDate, new AsyncCallback(SendRequestAvailabilityDateEmailCallBack), taskDelegate);
        }

        /// <summary>
        /// Method that send email to position profile associates indicating
        /// the status change of position profile.
        /// </summary>
        /// <param name="assessorDetail">
        /// A <see cref="UserDetail"/> that holds the requested assessor 
        /// detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type to send request mail
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendRequestAvailabilityDateEmail(UserDetail assessorDetail, EntityType entityType)
        {
            try
            {
                // Send email.
                new EmailHandler().SendMail(entityType, assessorDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void OnlineInterview_assessorCalendar_Calendar_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {
            try
            {
                GetAvailableDate();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion

        #region Protected Override Methods
        protected override bool IsValidData()
        {
            bool isValid = true;

            // Check if data is present.
            if (OnlineInterview_assessorCalendar_Calendar.SelectedDate.ToString() == "01-01-0001 00:00:00")
                isValid = false;

            if (isValid == false)
            {
                base.ShowMessage(DateRequest_topErrorMessageLabel, "Mandatory fields cannot be empty");
                return isValid;
            }

            if (ConstructSelectedTimes() == null || ConstructSelectedTimes().Count == 0)
            {
                base.ShowMessage(DateRequest_topErrorMessageLabel, "Select time");
                isValid = false;
                return isValid;
            }

            return isValid;
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Private Methods

        private void LoadTimeSlots(int genId)
        {
            AssessorTimeSlot_PopupGridView.DataSource = null;
            AssessorTimeSlot_PopupGridView.DataBind();

            if (genId == 0) return;

            List<AssessorTimeSlotDetail> assessorTimeSlots =
                          new OnlineInterviewAssessorBLManager().GetAssessorTimeSlots(genId);

            AssessorTimeSlot_PopupGridView.DataSource = assessorTimeSlots;
            AssessorTimeSlot_PopupGridView.DataBind();
        }

        /// <summary>
        /// Method that construct and return the list of selected times
        /// </summary>
        /// <returns>Returns the list of selected times</returns>
        private List<AssessorTimeSlotDetail> ConstructSelectedTimes()
        {
            List<AssessorTimeSlotDetail> selectedTimes = new List<AssessorTimeSlotDetail>();
            foreach (ListItem cBox in DateRequest_availableTimeCheckBoxList.Items)
            {
                AssessorTimeSlotDetail selectedTime = new AssessorTimeSlotDetail();
                if (cBox.Selected)
                {
                    selectedTime.TimeSlotIDFrom = Convert.ToInt32(cBox.Value);
                    selectedTime.TimeSlotIDTo = Convert.ToInt32(cBox.Value) + 1;
                    selectedTime.RequestStatus = Constants.TimeSlotRequestStatus.PENDING_CODE;
                    selectedTimes.Add(selectedTime);
                }
            }
            return selectedTimes;
        }

        private void GetAvailableDate()
        {
            int totalRecords = 0;

            List<AssessorTimeSlotDetail> assessorAvailabilityDates =
                       new OnlineInterviewAssessorBLManager().GetAssessorAvailableDates(Convert.ToInt32(Request.QueryString["userid"]),
                       OnlineInterview_assessorCalendar_Calendar.VisibleDate.Month, 1, base.GridPageSize, out totalRecords);

            if (Utility.IsNullOrEmpty(assessorAvailabilityDates)) return;

            ViewState["ASSESSOR_AVAILABLE_DATES"] = assessorAvailabilityDates;

            if (Utility.IsNullOrEmpty(assessorAvailabilityDates[0].VacationDates)) return;

            ViewState["ASSESSOR_VACATION_DATE"] = assessorAvailabilityDates[0].VacationDates.ToString();
        }

        private void GetAsssessorInterviews(int assessorId, int monthId, DateTime requestDate)
        {

            DateRequest_scheduledInterview_GridView.DataSource = null;
            DateRequest_scheduledInterview_GridView.DataBind();

            List<InterviewDetail> interviewDetailList = new OnlineInterviewAssessorBLManager().
                   GetAssessorInterviewDetail(assessorId, requestDate);

            if (interviewDetailList == null) return;

            foreach (InterviewDetail idetail in interviewDetailList)
            {
                if (idetail.TimeSlotTextFrom == null)
                    idetail.TimeSlotTextFrom = "Any Time";
            }


            DateRequest_scheduledInterview_GridView.DataSource = interviewDetailList;
            DateRequest_scheduledInterview_GridView.DataBind();

        }

        /// <summary>
        /// Method to load the available times
        /// </summary>
        private void LoadTimes()
        {
            AssessorBLManager blManager = new AssessorBLManager();

            // Set time slot settings.
            DateRequest_availableTimeCheckBoxList.DataSource = blManager.
                GetTimeSlotSettings(TimeSlotType.From);
            DateRequest_availableTimeCheckBoxList.DataTextField = "FromTo";
            DateRequest_availableTimeCheckBoxList.DataValueField = "ID";
            DateRequest_availableTimeCheckBoxList.DataBind();
        }

        /// <summary>
        /// Method that loads the data for edit mode.
        /// </summary>
        private void LoadEditTimeSlot()
        {
            //LoadTimes();

            // Check if time slot id is present.
            if (Utility.IsNullOrEmpty(Request.QueryString["userid"]))
            {
                base.ShowMessage(DateRequest_topErrorMessageLabel, "Assessor Id cannot be blank");
                return;
            }

            int id = 0;
            int.TryParse(Request.QueryString["userid"], out id);

            if (id == 0)
            {
                base.ShowMessage(DateRequest_topErrorMessageLabel, "No valid time slot ID is passed");
                return;
            }

            // Get time slot detail.  
            AssessorTimeSlotDetail timeSlot = null;


            if (!Utility.IsNullOrEmpty(ViewState["DATE_REQUEST_GENID"]) && Convert.ToInt32(ViewState["DATE_REQUEST_GENID"]) > 0)
                timeSlot = new AssessorBLManager().GetAssessorTimeSlot(id, OnlineInterview_assessorCalendar_Calendar.SelectedDate);

            if (timeSlot == null)
            {
                base.ShowMessage(DateRequest_topErrorMessageLabel, "No such time slot found");
                return;
            }

            /*AddTimeSlot_requestedDateLabel.Text = timeSlot.AvailabilityDate.ToString("ddd, dd MMM yyyy");  
            AddTimeSlot_requesterTextBox.Text = timeSlot.InitiatedByName;
              */

            ///ViewState["DATE_REQUEST_GENID"] = timeSlot.ID.ToString(); 
            DateRequest_genIdHiddenField.Value = ViewState["DATE_REQUEST_GENID"].ToString();

            if (!Utility.IsNullOrEmpty(timeSlot.IsAvailableFullTime) &&
                timeSlot.IsAvailableFullTime.Trim() == "Y")
            {
                //DateRequest_availableFullTimeCheckBox.Checked = true;
                DateRequest_availableTimeCheckBoxList.Enabled = false;
            }
            else
            {
                DateRequest_availableTimeCheckBoxList.Enabled = true;
                //DateRequest_availableFullTimeCheckBox.Checked = false;
                if (timeSlot.assessorTimeSlotDetails != null && timeSlot.assessorTimeSlotDetails.Count > 0)
                {
                    foreach (ListItem cBox in DateRequest_availableTimeCheckBoxList.Items)
                    {
                        int index = timeSlot.assessorTimeSlotDetails.
                            FindIndex(item => item.TimeSlotIDFrom == Convert.ToInt32(cBox.Value));

                        if (index >= 0)
                        {
                            cBox.Selected = true;
                            var obj = timeSlot.assessorTimeSlotDetails.
                                Find(item => item.TimeSlotIDFrom == Convert.ToInt32(cBox.Value));

                            if (!Utility.IsNullOrEmpty(obj.RequestStatus) && obj.RequestStatus == "A")
                                cBox.Enabled = false;
                        }
                    }
                }
            }
        }

        private void ClearCheckBoxSelection()
        {
            foreach (ListItem cBox in DateRequest_availableTimeCheckBoxList.Items)
            {
                cBox.Selected = false;
                cBox.Enabled = true;

            }
        }

        #endregion

        #region Asynchronous Method Handlers

        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendRequestAvailabilityDateEmailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Asynchronous Method Handlers

        /*protected void DateRequest_availableFullTimeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (DateRequest_availableFullTimeCheckBox.Checked) 
                    DateRequest_availableTimeCheckBoxList.Enabled = false; 
                else 
                    DateRequest_availableTimeCheckBoxList.Enabled = true; 
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp); 
            }
        }*/
    }
}