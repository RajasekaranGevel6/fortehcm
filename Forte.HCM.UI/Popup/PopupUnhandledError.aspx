﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master" 
CodeBehind="PopupUnhandledError.aspx.cs" Inherits="Forte.HCM.UI.Popup.PopupUnhandledError" %>

<asp:Content ID="PopupUnhandledError_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
        <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td style="width: 40%" align="left" valign="top">
                <table style="width: 100%">
                    <tr>
                        <td class="unhandled_error_title">
                            Error
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message">
                            Unhandled error
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_title">
                            Message
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message">
                            An unhandled error has occured in the application
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message">
                            Please contact your administrator
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_title">
                            Details
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="unhandled_error_message">
                            <asp:Label ID="UnhandledError_detailsLabel" runat="server" Text="None"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 60%" class="unhandled_error">
            </td>
        </tr>
    </table>
</asp:Content>
