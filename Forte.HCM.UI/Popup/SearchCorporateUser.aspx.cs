﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchCorporateUser.aspx.cs
// File that represents the SearchCorporateUser class that defines the user interface
// layout and functionalities for the SearchCorporateUser page. This page helps in 
// searching for users by providing search criteria for user name, first name, 
// middle name and last name This class inherits Forte.HCM.UI.Common.PageBase
// class.

#endregion

#region Directives

using System;
using System.Web.UI;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the SearchCorporateUser page. This page helps in searching for users by
    /// providing search criteria for user name, first name, middle name and
    /// last name. This class inherits the Forte.HCM.UI.Common.PageBase 
    /// class.
    /// </summary>
    public partial class SearchCorporateUser : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        /// <summary>
        /// A <see cref="int"/> that hold the position profile ID.
        /// </summary>
        private int positionProfileID = 0;

        /// <summary>
        /// A <see cref="String"/> that hold the owner type.
        /// </summary>
        private string ownerType = null;

        #endregion Private Constants

        #region Events Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Assign page & browser title based on action.
                string action = string.Empty;
                if (!Utility.IsNullOrEmpty(Request.QueryString["action"]))
                    action = Request.QueryString["action"].ToUpper().Trim();

                if (action == "CO")
                {
                    SearchCorporateUser_headerLiteral.Text = "Select Co-Owners";
                    Master.SetPageCaption("Select Co-Owners");
                }
                else if (action == "TO")
                {
                    SearchCorporateUser_headerLiteral.Text = "Select Task Owners";
                    Master.SetPageCaption("Select Task Owners");
                }
                else
                {
                    SearchCorporateUser_headerLiteral.Text = "Select Users";
                    Master.SetPageCaption("Select Users");
                }

                // Set default button and focus.
                Page.Form.DefaultButton = SearchCorporateUser_searchButton.UniqueID;

                // Subscribes to the page number click event.
                SearchCorporateUser_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchCorporateUser_pageNavigator_PageNumberClick);

                if (!Utility.IsNullOrEmpty(SearchCorporateUser_isMaximizedHiddenField.Value) &&
                    SearchCorporateUser_isMaximizedHiddenField.Value == "Y")
                {
                    SearchCorporateUser_searchCriteriasDiv.Style["display"] = "none";
                    SearchCorporateUser_searchResultsUpSpan.Style["display"] = "block";
                    SearchCorporateUser_searchResultsDownSpan.Style["display"] = "none";
                    SearchCorporateUser_testDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchCorporateUser_searchCriteriasDiv.Style["display"] = "block";
                    SearchCorporateUser_searchResultsUpSpan.Style["display"] = "none";
                    SearchCorporateUser_searchResultsDownSpan.Style["display"] = "block";
                    SearchCorporateUser_testDiv.Style["height"] = RESTORED_HEIGHT;
                }

                // This validation is done for focusing the linkbutton of the input
                // fields such as category, subject, test area.
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchCorporateUser_browserHiddenField.Value))
                {
                    ValidateEnterKey(SearchCorporateUser_browserHiddenField.Value.Trim());
                    SearchCorporateUser_browserHiddenField.Value = string.Empty;
                }

                // Get position profile ID.
                int.TryParse(Request.QueryString["positionprofileid"], out positionProfileID);

                // Set the owner type
                if (!Utility.IsNullOrEmpty(Request.QueryString["ownertype"]))
                    ownerType = Request.QueryString["ownertype"].Trim();

                if (!IsPostBack)
                {
                    // Set default focus.
                    Page.Form.DefaultFocus = SearchCorporateUser_userNameTextBox.UniqueID;
                    SearchCorporateUser_userNameTextBox.Focus();

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "USERNAME";

                    SearchCorporateUser_testDiv.Visible = false;

                    SearchCorporateUser_createAssessorConfirmMsgControl.Message = "Are you sure want to mark the user as an assessor?";
                    SearchCorporateUser_createAssessorConfirmMsgControl.Type = MessageBoxType.YesNo;
                    SearchCorporateUser_createAssessorConfirmMsgControl.Title = "Mark As An Assessor";

                    if (Utility.IsNullOrEmpty(Request.QueryString["multipleSelection"]))
                        SearchCorporateUser_selectButton.Visible = false;

                    //Reset session value
                    Session["SELECTED_USER_LIST"] = null;
                }
                else
                {
                    SearchCorporateUser_testDiv.Visible = true;
                }

                SearchCorporateUser_searchTestResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    SearchCorporateUser_testDiv.ClientID + "','" +
                    SearchCorporateUser_searchCriteriasDiv.ClientID + "','" +
                    SearchCorporateUser_searchResultsUpSpan.ClientID + "','" +
                    SearchCorporateUser_searchResultsDownSpan.ClientID + "','" +
                    SearchCorporateUser_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCorporateUser_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchCorporateUser_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                SearchCorporateUser_errorMessageLabel.Text = string.Empty;
                SearchCorporateUser_successMessageLabel.Text = string.Empty;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "USERNAME";

                // Reset the paging control.
                SearchCorporateUser_pageNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCorporateUser_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void SearchCorporateUser_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchCorporateUser_userNameTextBox.Text = string.Empty;
                SearchCorporateUser_firstNameTextBox.Text = string.Empty;
                SearchCorporateUser_middleNameTextBox.Text = string.Empty;
                SearchCorporateUser_lastNameTextBox.Text = string.Empty;

                SearchCorporateUser_testGridView.DataSource = null;
                SearchCorporateUser_testGridView.DataBind();

                SearchCorporateUser_pageNavigator.PageSize = base.GridPageSize;
                SearchCorporateUser_pageNavigator.TotalRecords = 0;
                SearchCorporateUser_testDiv.Visible = false;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "USERNAME";

                // Reset to the restored state.
                SearchCorporateUser_searchCriteriasDiv.Style["display"] = "block";
                SearchCorporateUser_searchResultsUpSpan.Style["display"] = "none";
                SearchCorporateUser_searchResultsDownSpan.Style["display"] = "block";
                SearchCorporateUser_testDiv.Style["height"] = RESTORED_HEIGHT;
                SearchCorporateUser_isMaximizedHiddenField.Value = "N";

                // Clear message fields.
                SearchCorporateUser_successMessageLabel.Text = string.Empty;
                SearchCorporateUser_errorMessageLabel.Text = string.Empty;

                // Set default focus.
                Page.Form.DefaultFocus = SearchCorporateUser_userNameTextBox.UniqueID;
                SearchCorporateUser_userNameTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCorporateUser_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchCorporateUser_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ViewState["CURRENT_PAGE_NO"] = e.PageNumber.ToString();
                Search(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCorporateUser_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchCorporateUser_testGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                SearchCorporateUser_pageNavigator.Reset();
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCorporateUser_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchCorporateUser_testGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    LinkButton SearchCorporateUser_selectLinkButton = (LinkButton)e.Row.FindControl
                        ("SearchCorporateUser_selectLinkButton");

                    CheckBox SearchCorporateUser_userSelectCheckbox = (CheckBox)e.Row.FindControl
                            ("SearchCorporateUser_userSelectCheckbox");

                    HiddenField SearchCorporateUser_userIsOwnerHiddenfield = (HiddenField)e.Row.FindControl
                            ("SearchCorporateUser_userIsOwnerHiddenfield");

                    bool singleSelect = true;
                    bool multipleSelect = false;

                    if ((!Utility.IsNullOrEmpty(Request.QueryString["multipleSelection"])) &&
                               Request.QueryString["multipleSelection"].ToString() == "Y")
                    {
                        singleSelect = false;
                        multipleSelect = true;
                    }

                    SearchCorporateUser_selectButton.Visible = multipleSelect;
                    SearchCorporateUser_selectLinkButton.Visible = singleSelect;

                    if (!Utility.IsNullOrEmpty(SearchCorporateUser_userIsOwnerHiddenfield.Value)
                        && SearchCorporateUser_userIsOwnerHiddenfield.Value.Trim() == "Y")
                        SearchCorporateUser_userSelectCheckbox.Visible = false;
                    else
                        SearchCorporateUser_userSelectCheckbox.Visible = multipleSelect;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCorporateUser_errorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Handler method that is called when the bottom select button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void SearchCorporateUser_selectButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                SearchCorporateUser_errorMessageLabel.Text = string.Empty;
                SearchCorporateUser_successMessageLabel.Text = string.Empty;

                // Check if the scenario is valid.
                if (!IsValidData())
                    return;

                List<UserDetail> userDetails = null;

                if (Session["SELECTED_USER_LIST"] == null)
                    userDetails = new List<UserDetail>();
                else
                    userDetails = Session["SELECTED_USER_LIST"] as List<UserDetail>;

                foreach (GridViewRow row in SearchCorporateUser_testGridView.Rows)
                {
                    CheckBox checkBox = ((CheckBox)row.FindControl
                        ("SearchCorporateUser_userSelectCheckbox"));

                    if (!checkBox.Checked)
                    {
                        continue;
                    }

                    HiddenField SearchCorporateUser_userIDHiddenfield = ((HiddenField)row.FindControl
                        ("SearchCorporateUser_userIDHiddenfield"));
                    HiddenField SearchCorporateUser_userFirstNameHiddenfield = ((HiddenField)row.FindControl
                        ("SearchCorporateUser_userFirstNameHiddenfield"));
                    HiddenField SearchCorporateUser_userLastNameHiddenfield = ((HiddenField)row.FindControl
                        ("SearchCorporateUser_userLastNameHiddenfield"));
                    HiddenField SearchCorporateUser_userEmailHiddenfield = ((HiddenField)row.FindControl
                        ("SearchCorporateUser_userEmailHiddenfield"));

                    UserDetail userDetail = new UserDetail();

                    userDetail.UserID = int.Parse(SearchCorporateUser_userIDHiddenfield.Value);
                    userDetail.FirstName = SearchCorporateUser_userFirstNameHiddenfield.Value;
                    userDetail.LastName = SearchCorporateUser_userLastNameHiddenfield.Value;
                    userDetail.Email = SearchCorporateUser_userEmailHiddenfield.Value;

                    userDetails.Add(userDetail);
                }

                Session["SELECTED_USER_LIST"] = userDetails;
                //Script to close the popup window
                string closeScript = "self.close();";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Closeform", "<script language='javascript'>OnSelectClick();</script>", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(SearchCorporateUser_errorMessageLabel, exp.Message);
            }
        }

       
        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchCorporateUser_testGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchCorporateUser_testGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchCorporateUser_errorMessageLabel, exp.Message);
            }
        }

        protected void SearchCorporateUser_createAssessorConfirmMsgControl_CancelClick
     (object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);   
                base.ShowMessage(SearchCorporateUser_errorMessageLabel, ex.Message);
            }
        }

        protected void SearchCorporateUser_createAssessorConfirmMsgControl_OkClick(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["USER_ID"] == null) return;

                CreateNewAssessor(Convert.ToInt32(ViewState["USER_ID"]), ViewState["USER_EMAIL"].ToString());


                /*Type t = typeof(Button);
                object[] p = new object[1];
                p[0] = EventArgs.Empty;
                MethodInfo m = t.GetMethod("OnClick", BindingFlags.NonPublic | BindingFlags.Instance);
                m.Invoke(SearchCorporateUser_searchButton, p);*/

                if(ViewState["CURRENT_PAGE_NO"]  ==null)
                    Search(1);
                else
                    Search(Convert.ToInt32(ViewState["CURRENT_PAGE_NO"]));

            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(SearchCorporateUser_errorMessageLabel, ex.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods

        /// <summary>
        /// Creating a new assessor from existing listed users
        /// </summary>
        /// <param name="userId">
        ///     <see cref="System.String"/>         
        /// </param>
        /// <param name="userEmail">
        ///     <see cref="System.String"/>
        /// </param>
        private void CreateNewAssessor(int userId, string userEmail)
        {
            UserDetail userDetail = new UserDetail();

            userDetail = new CommonBLManager().GetUserDetail(userId);

            AssessorDetail assessorDetails = new AssessorDetail();

            assessorDetails.UserID = userId;
            assessorDetails.UserEmail = userEmail;
            assessorDetails.AlternateEmailID = string.Empty;
            assessorDetails.FirstName = string.Empty;
            assessorDetails.LastName = string.Empty;
            assessorDetails.Mobile = string.Empty;    

            if (userDetail != null)
            {
                assessorDetails.UserEmail = userDetail.Email;                
                assessorDetails.AlternateEmailID = userDetail.AltEmail;
                assessorDetails.FirstName = userDetail.FirstName;
                assessorDetails.LastName = userDetail.LastName;
                assessorDetails.Mobile = userDetail.Phone;    
            }

            assessorDetails.Skill = string.Empty;
            assessorDetails.CreatedBy = base.userID;
            assessorDetails.NonAvailabilityDates = string.Empty;
            assessorDetails.Image = null; 
            assessorDetails.HomePhone = string.Empty;
            assessorDetails.AdditionalInfo = string.Empty;
            assessorDetails.LinkedInProfile = string.Empty;

            //Save assessor details
            string retval = new AssessorBLManager().SaveAssessorDetails(assessorDetails);
        }
        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void Search(int pageNumber)
        {
            List<UserDetail> users = null;

            int totalRecords = 0;

            users = new CommonBLManager().GetCorporateUsers
                (SearchCorporateUser_userNameTextBox.Text.Trim(),
                SearchCorporateUser_firstNameTextBox.Text.Trim(),
                SearchCorporateUser_middleNameTextBox.Text.Trim(),
                SearchCorporateUser_lastNameTextBox.Text.Trim(),
                ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]),
                pageNumber,
                base.GridPageSize, out totalRecords, base.tenantID, positionProfileID,ownerType
);


            if (users == null || users.Count == 0)
            {
                SearchCorporateUser_testDiv.Visible = false;
                ShowMessage(SearchCorporateUser_errorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                SearchCorporateUser_testDiv.Visible = true;
            }

            SearchCorporateUser_testGridView.DataSource = users;
            SearchCorporateUser_testGridView.DataBind();
            SearchCorporateUser_pageNavigator.PageSize = base.GridPageSize;
            SearchCorporateUser_pageNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that will call when the page gets loaded. This will perform
        /// to call the event handlers based parameter.
        /// </summary>
        /// <param name="stringValue">
        /// A <see cref="string"/> that contains the string.
        /// </param>
        private void ValidateEnterKey(string stringValue)
        {
            switch (stringValue.Trim())
            {
                case "dummy":
                    foreach (GridViewRow row in SearchCorporateUser_testGridView.Rows)
                    {
                        LinkButton SearchCorporateUser_selectLinkButton = (LinkButton)
                            row.FindControl("SearchCorporateUser_selectLinkButton");

                        System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "OnSelectClickMethod",
                            "javascript:OnSelectClick('" + SearchCorporateUser_selectLinkButton.ClientID + "');", true);
                    }
                    break;
            }
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValid = true;

            bool userSelected = false;
            string existingSubjects = string.Empty;

            if (SearchCorporateUser_testGridView.Rows.Count == 0)
            {
                ShowMessage(SearchCorporateUser_errorMessageLabel,
                    "No users found to select");

                return false;
            }

            List<string> selectedUsers = null;

            foreach (GridViewRow row in SearchCorporateUser_testGridView.Rows)
            {
                if (selectedUsers == null)
                    selectedUsers = new List<string>();

                CheckBox checkBox = ((CheckBox)row.FindControl
                    ("SearchCorporateUser_userSelectCheckbox"));

                if (!checkBox.Checked)
                {
                    continue;
                }

                // Set subject selected flag to true.
                userSelected = true;
            }

            if (userSelected == false)
            {
                ShowMessage(SearchCorporateUser_errorMessageLabel,
                    "No user(s) selected to add");

                return false;
            }

            return isValid;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
        //protected void SearchCorporateUser_testGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "Select")
        //    {
        //        LinkButton SearchCorporateUser_selectLinkButton =
        //            (LinkButton)((LinkButton)e.CommandSource).FindControl("SearchCorporateUser_selectLinkButton");
        //        SearchCorporateUser_selectLinkButton.Click += new EventHandler(SearchCorporateUser_selectLinkButton_Click);
        //    }
        //}
        protected void SearchCorporateUser_testGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try {
                if (e.CommandName == "CreateAssessor")
                {
                    GridViewRow userGridViewRow = (GridViewRow)(((ImageButton)e.CommandSource).Parent.Parent);

                    if (userGridViewRow == null) return;

                    ViewState["USER_ID"] = e.CommandArgument.ToString();

                    HiddenField SearchCorporateUser_userEmailHiddenfield =
                        (HiddenField)userGridViewRow.FindControl("SearchCorporateUser_userEmailHiddenfield");

                    if (SearchCorporateUser_userEmailHiddenfield == null)
                        ViewState["USER_EMAIL"] = string.Empty;
                    else
                        ViewState["USER_EMAIL"] = SearchCorporateUser_userEmailHiddenfield.Value.ToString();

                    SearchCorporateUser_createAssessorModalPopupExtender.Show();
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(SearchCorporateUser_errorMessageLabel, ex.Message);
            } 
        }

        /// <summary>
        /// Gets the visibility.
        /// </summary>
        /// <param name="assessor">The assessor.</param>
        /// <returns></returns>
        protected bool IsAssessor(string assessor)
        {
            return assessor == "Y" ? true : false;             

            /*if (assessor == "Y")
                return true;
            else
                return false;*/
        }

        protected bool IsNotAssessor(string assessor)
        {
            return assessor == "N" ? true : false;
            /*if (assessor == "N")
                return true;
            else
                return false;*/
        }

}
}

