#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// RequestExternalAssessor.aspx.cs
// File that represents the user interface layout and functionalities for
// the RequestExternalAssessor page. This will helps to send request to external assessor
// to assess interview assessment.

#endregion Header

#region Directives

using System;
using Resources;
using System.Web;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
 

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Net.Mail;
using System.ComponentModel;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the RequestExternalAssessor page. This will helps to send request to external assessor
    /// to assess interview assessment. This class 
    /// inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class RequestExternalAssessor : PageBase
    {
        #region Declaration

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="ExternalAssessorDetail">
        /// A <see cref="ExternalAssessorDetail"/> that holds the external assessor details.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegate(List<ExternalAssessorDetail> externalAssessorDetails);

        #endregion Declaration

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Request External Assessor");
                Page.Form.DefaultButton = RequestExternalAssessor_sendRequestButton.UniqueID;

                ClearControls();

                if (!IsPostBack)
                {
                    // Call and assign query string values to hidden fields.
                    if (RetrieveQueryStringValues() == false)
                        return;

                    LoadSkills();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestExternalAssessor_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the send request button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will send mail to the requested external assessor
        /// </remarks>
        protected void RequestExternalAssessor_sendRequestButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;

                //Construct candidation interview session details
                CandidateInterviewSessionDetail candidateInterviewSessionDetail = 
                    new CandidateInterviewSessionDetail();

                string candiateSessionKey = RequestExternalAssessor_candidateSessionKeyHiddenField.Value;
                int attemptID = Convert.ToInt32(RequestExternalAssessor_attemptIDHiddenField.Value);

                //Construct list of selected skill to list
                List<int> skillList = GetRequestedSkillID();

                //Save external assessor details
                List<ExternalAssessorDetail> externalAssessorDetails = 
                    new InterviewSessionBLManager().SaveExternalAssessorDetail(skillList,candiateSessionKey,attemptID,
                    RequestExternalAssessor_emailTextBox.Text.Trim(), base.userID);

                try
                {
                    // Sent alert mail to the associated assessor asynchronously.
                    AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendMailToAssessor);
                    IAsyncResult result = taskDelegate.BeginInvoke(externalAssessorDetails,
                        new AsyncCallback(SendMailToAssessorCallBack), taskDelegate);
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseRequestExternalForm", "<script language='javascript'>OnSaveClick();</script>", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestExternalAssessor_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset
        /// button clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the form inputs
        /// </remarks>
        protected void RequestExternalAssessor_bottomReset_Click(object sender, EventArgs e)
        {
            try
            {
                RequestExternalAssessor_emailTextBox.Text = string.Empty;
                foreach (GridViewRow row in RequestExternalAssessor_skillGridView.Rows)
                {
                    CheckBox checkBox = ((CheckBox)row.FindControl
                        ("RequestExternalAssessor_skillGridView_selectSkillCheckBox"));
                    checkBox.Checked = false;
                }
                ClearControls();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(RequestExternalAssessor_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method that retrieves the query string values and assign it to 
        /// hidden fields.
        /// </summary>
        private bool RetrieveQueryStringValues()
        {
            // Check if assessor ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
            {
                base.ShowMessage(RequestExternalAssessor_topErrorMessageLabel, "Assessor ID is not passed");
                return false;
            }

            // Check if attempt ID is valid.
            int attemptID = 0;
            int.TryParse(Request.QueryString["attemptid"], out attemptID);

            if (attemptID == 0)
            {
                base.ShowMessage(RequestExternalAssessor_topErrorMessageLabel, "Invalid attempt ID");
                return false;
            }

            // Assign attempt ID.
            RequestExternalAssessor_attemptIDHiddenField.Value = attemptID.ToString();

            // Check if candidate session key is passed.
            if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]))
            {
                // Assign session key.
                RequestExternalAssessor_candidateSessionKeyHiddenField.Value = Request.QueryString["candidatesessionid"];
            }

            return true;
        }

        /// <summary>
        /// Metod to load the list of skills against this candidate session key
        /// </summary>
        private void LoadSkills()
        {
            // Candidate session key
            string candidateSessionKey=RequestExternalAssessor_candidateSessionKeyHiddenField.Value;

            // Get the skills.
            List<SkillDetail> skills = new InterviewSessionBLManager().
                GetSkillsByCandidateSessionKey(candidateSessionKey);

            if (skills == null || skills.Count == 0)
            {
                RequestExternalAssessor_skillGridView.DataSource = null;
                RequestExternalAssessor_skillGridView.DataBind();
            }
            else
            {
                string skillName = string.Join(", ", skills.Select(s => s.SkillName.Trim()).ToArray());
                RequestExternalAssessor_skillLabelValue.Text = skillName;
                RequestExternalAssessor_skillGridView.DataSource = skills;
                RequestExternalAssessor_skillGridView.DataBind();
            }
        }

        /// <summary>
        /// Method to get the list of selected skill id
        /// </summary>
        /// <returns>List that returns the set of skill</returns>
        private List<int> GetRequestedSkillID()
        {
            List<int> skillList = new List<int>();
            foreach (GridViewRow row in RequestExternalAssessor_skillGridView.Rows)
            {
                CheckBox checkBox = ((CheckBox)row.FindControl
                    ("RequestExternalAssessor_skillGridView_selectSkillCheckBox"));

                if (!checkBox.Checked)
                {
                    continue;
                }
                HiddenField RequestExternalAssessor_skillGridView_skillIDHiddenField = ((HiddenField)row.FindControl
                    ("RequestExternalAssessor_skillGridView_skillIDHiddenField"));

                if (!Utility.IsNullOrEmpty(RequestExternalAssessor_skillGridView_skillIDHiddenField.Value))
                {
                    skillList.Add(int.Parse(RequestExternalAssessor_skillGridView_skillIDHiddenField.Value));
                }
            }

            return skillList;
        }

        /// <summary>
        /// Method to clear display message controls
        /// </summary>
        private void ClearControls()
        {
            RequestExternalAssessor_topSuccessMessageLabel.Text = string.Empty;
            RequestExternalAssessor_topErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Method that send mail to the requested external assessor indicating
        /// </summary>
        /// <param name="ExternalAssessorDetail">
        /// A <see cref="ExternalAssessorDetail"/> that holds the external assessor details 
        /// detail.
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendMailToAssessor(List<ExternalAssessorDetail> externalAssessorDetails)
        {
            try
            {
                // Send email to the requsted external assessor
                if (externalAssessorDetails != null && externalAssessorDetails.Count > 0)
                {
                    foreach (ExternalAssessorDetail externalAssessorDetail in externalAssessorDetails)
                    {
                        if (Utility.IsNullOrEmpty(externalAssessorDetail.ExternalKey))
                        {
                            new EmailHandler().SendMail
                                (EntityType.RequestToAssessor, externalAssessorDetail);
                        }
                        else
                        {
                            new EmailHandler().SendMail
                                (EntityType.RequestToExternalAssessor, externalAssessorDetail);
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Private Methods

        #region Asynchronous Method Handlers

        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendMailToAssessorCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Asynchronous Method Handlers

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValid = true;

            if (Utility.IsNullOrEmpty(RequestExternalAssessor_emailTextBox.Text))
            {
                base.ShowMessage(RequestExternalAssessor_topErrorMessageLabel, "Please enter email");
                isValid = false;
            }
            else
            {
                string[] emailIDs = RequestExternalAssessor_emailTextBox.Text.
                    ToString().Trim().Split(new char[] { ',' });
                if (emailIDs.Count() == 1)
                {
                    if (!Utility.IsValidEmailAddress(emailIDs[0].ToString().Trim()))
                    {
                        base.ShowMessage(RequestExternalAssessor_topErrorMessageLabel,
                            HCMResource.UserRegistration_InvalidEmailAddress);
                        isValid = false;
                    }
                }
                else
                {
                    string errMsg = string.Empty;
                    foreach (string emailId in emailIDs)
                    {
                        if (!Utility.IsValidEmailAddress(emailId.ToString().Trim()))
                            errMsg = errMsg + string.Format("`{0}`", emailId.ToString().Trim()) + " ,";
                    }
                    if (!Utility.IsNullOrEmpty(errMsg))
                    {
                        base.ShowMessage(RequestExternalAssessor_topErrorMessageLabel,
                            string.Format("Email {0} is invalid", errMsg.Trim().TrimEnd(',')));
                        isValid = false;
                    }
                }
            }

            List<int> skillList = GetRequestedSkillID();
            if (skillList == null || skillList.Count == 0)
            {
                base.ShowMessage(RequestExternalAssessor_topErrorMessageLabel, "Please select atleast one skill to send request");
                isValid = false;
            }

            return isValid;
        }

        #endregion Protected Overridden Methods

       
}
}