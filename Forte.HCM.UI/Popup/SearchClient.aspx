<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="SearchClient.aspx.cs" Title="Select Client" Inherits="Forte.HCM.UI.Popup.SearchClient" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="SearchClient_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">

    <script type="text/javascript" language="javascript">

        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl)
        {
            var ctrlID = '<%= Request.QueryString["ctrlid"] %>';
            var ctrlName = '<%= Request.QueryString["ctrlname"] %>';
            var btnCnrl = '<%= Request.QueryString["btncnrl"] %>';

            if (window.dialogArguments)
            {
                window.opener = window.dialogArguments;
            }
            if (ctrlID != null && ctrlID != '')
            {
                // Get client ID.
                var clientID = document.getElementById(ctrl.id.replace
                    ("SearchClient_selectButton", "SearchClient_clientIDHiddenField")).value;

                // Get client name.
                var clientName = document.getElementById(ctrl.id.replace
                    ("SearchClient_selectButton", "SearchClient_clientNameHiddenField")).value;

                window.opener.document.getElementById(ctrlID).value = clientID;
                window.opener.document.getElementById(ctrlName).value = clientName + " | " + clientID;
            }
            // Trigger the click event of the refresh button in the parent page.
            if (btnCnrl != null && window.opener.document.getElementById(btnCnrl) != null)
                window.opener.document.getElementById(btnCnrl).click();

            self.close();
        }

    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 80%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchClient_headerLiteral" runat="server" Text="Select Client"></asp:Literal>
                        </td>
                        <td style="width: 20%" align="right">
                            <asp:ImageButton ID="SearchClient_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:UpdatePanel ID="SearchClient_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="SearchClient_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="SearchClient_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchClient_searchButton" />
                                                <asp:AsyncPostBackTrigger ControlID="SearchClient_pageNavigator" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="SearchClient_searchCriteriasDiv" runat="server" style="display: block;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="SearchClient_searchCriteriaUpdatePanel" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <table width="100%" border="0" cellspacing="3%" cellpadding="2%">
                                                                    <tr>
                                                                        <td style="width: 20%" align="left">
                                                                            <asp:Label ID="SearchClient_clientLabel" runat="server" Text="Client"
                                                                                SkinID="sknLabelText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 80%" align="left">
                                                                            <asp:TextBox ID="SearchClient_clientTextBox" runat="server" MaxLength="50" Width="99%"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="4">
                                                                            <asp:Button ID="SearchClient_searchButton" runat="server" SkinID="sknButtonId"
                                                                                Text="Search" OnClick="SearchClient_searchButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="SearchClient_searchResultsUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                                                    <tr id="SearchClient_searchTestResultsTR" runat="server">
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 80%">
                                                                        <asp:Literal ID="SearchClient_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                                            ID="SearchQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%" align="right">
                                                                        <span id="SearchClient_searchResultsUpSpan" style="display: none;" runat="server">
                                                                            <asp:Image ID="SearchClient_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                                        </span><span id="SearchClient_searchResultsDownSpan" style="display: block;" runat="server">
                                                                            <asp:Image ID="SearchClient_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="grid_body_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="height: 220px; overflow: auto;" runat="server" id="SearchClient_searchResultsDiv">
                                                                            <asp:GridView ID="SearchClient_searchResultsGridView" runat="server" AutoGenerateColumns="False"
                                                                                Width="100%" OnRowCreated="SearchClient_searchResultsGridView_RowCreated" 
                                                                                OnSorting="SearchClient_searchResultsGridView_Sorting"
                                                                                AllowSorting="true" OnRowDataBound="SearchClient_searchResultsGridView_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="SearchClient_selectButton" runat="server" OnClientClick="javascript:return OnSelectClick(this);"
                                                                                                Text="Select" SkinID="sknActionLinkButton">
                                                                                            </asp:LinkButton>
                                                                                            <asp:HiddenField ID="SearchClient_clientNameHiddenField" runat="server" Value='<%# Eval("ClientName") %>' />
                                                                                            <asp:HiddenField ID="SearchClient_clientIDHiddenField" runat="server" Value='<%# Eval("ClientID") %>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="Client" DataField="ClientName" SortExpression="CLIENT_NAME" HeaderStyle-Width="85%"/>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <uc1:PageNavigator ID="SearchClient_pageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="SearchClient_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_left_8">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:LinkButton ID="SearchClient_topResetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Reset" OnClick="SearchClient_resetButton_Click" />
                        </td>
                        <td class="pop_divider_line" valign="top">
                            |
                        </td>
                        <td valign="top">
                            <asp:LinkButton ID="SearchClient_topCancelLinkButton" runat="server" Text="Cancel"
                                SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="SearchClient_isMaximizedHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
