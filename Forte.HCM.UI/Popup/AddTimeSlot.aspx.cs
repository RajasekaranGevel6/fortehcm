﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AddTimeSlot.aspx.cs
// File that represents the user interface layout and functionalities for
// the AddTimeSlot page. This will helps to view scheduled time slots
// and to add/edit time slots for online interview.

#endregion Header

#region Directives                                                             

using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Collections.Generic;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the AddTimeSlot page. This will helps to view scheduled time slots
    /// and to add/edit time slots for online interview. This class inherits
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class AddTimeSlot : PageBase
    {
        #region Private Variables          
        
        /// <summary>
        /// A <see cref="string"/> that holds the mode.
        /// </summary>
        private string mode = string.Empty;

        /// <summary>
        /// A <see cref="bool"/> that represents the status whether the fire 
        /// the from time slot selected event, which will auto select the 
        /// to time slot in 30 minutes advance.
        /// </summary>
        private bool fireFromSelected = true;

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="assessorDetail">
        /// A <see cref="AssessorDetail"/> that holds the assessor detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegate(AssessorDetail assessorDetail, EntityType entityType);

        #endregion Private Variables

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Check if mode is edit.
                if (!Utility.IsNullOrEmpty(Request.QueryString["mode"]))
                    mode = Request.QueryString["mode"].Trim().ToUpper();

                // Set title.
                SetTitle(mode);

                if (!IsPostBack)
                {
                    // Set default button and focus.
                    Page.Form.DefaultButton = AddTimeSlot_saveButton.UniqueID;

                    LoadTimes();

                    if (!Utility.IsNullOrEmpty(Request.QueryString["source"]) &&
                        Request.QueryString["source"].Trim().ToUpper() == "MYAVB")
                    {
                        // Set date.
                        if (Session["MY_AVAILABILITY_DATE"] == null)
                        {
                            base.ShowMessage(AddTimeSlot_topErrorMessageLabel, "Date is not passed from the parent page");
                            AddTimeSlot_saveButton.Visible = false;
                        }
                        else
                        {
                            AddTimeSlot_requestedDateTextBox.Text = Convert.ToDateTime
                                (Session["MY_AVAILABILITY_DATE"]).ToString();
                        }
                    }

                    if (mode == "A" || mode == "V")
                    {
                        // Load time slot.
                        LoadEditTimeSlot();
                        AddTimeSlot_displayRequestedDateDiv.Style["display"] = "block";
                        AddTimeSlot_displayProposeDateDiv.Style["display"] = "none";
                    }
                    else if (mode == "P")
                    {
                        // Load interview session, requester, skill, date, etc based on
                        // the time slot ID.
                        LoadProposeTimeSlot();

                        AddTimeSlot_displayRequestedDateDiv.Style["display"] = "none";
                        AddTimeSlot_displayProposeDateDiv.Style["display"] = "block";
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AddTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void AddTimeSlot_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                AddTimeSlot_topSuccessMessageLabel.Text = string.Empty;
                AddTimeSlot_topErrorMessageLabel.Text = string.Empty;

                if (!IsValidData())
                    return;

                List<AssessorTimeSlotDetail> selectedTimes = new List<AssessorTimeSlotDetail>();
                selectedTimes = ConstructSelectedTimes();

                AssessorTimeSlotDetail proposedDateDetail = new AssessorTimeSlotDetail();

                proposedDateDetail.InitiatedBy = base.userID;
                proposedDateDetail.AssessorID = base.userID;
                proposedDateDetail.RequestStatus = Constants.TimeSlotRequestStatus.ACCEPTED_CODE;
                proposedDateDetail.IsAvailableFullTime = AddTimeSlot_availableFullTimeCheckBox.Checked==true ? "Y" : "N";
                
                if (mode == "P")
                {
                    proposedDateDetail.AvailabilityDate =
                        Convert.ToDateTime(AddTimeSlot_requestedDateTextBox.Text.Trim());
                }
                else
                {
                    proposedDateDetail.RequestDateGenID = Convert.ToInt32(Request.QueryString["id"].Trim());
                }

                // Save the time slots.
                new AssessorBLManager().UpdateAssessorAvailableDateTimeSlots(mode, proposedDateDetail, selectedTimes);

                if (mode == "A")
                {
                    AssessorDetail assessorMailDetail = new AssessorDetail();
                    assessorMailDetail.FirstName = ((UserDetail)Session["USER_DETAIL"]).FirstName;
                    assessorMailDetail.LastName = ((UserDetail)Session["USER_DETAIL"]).LastName;
                    assessorMailDetail.RequestedSlotID = proposedDateDetail.RequestDateGenID;

                    //Send mail to the requested user
                    // Sent alert mail to the associated user asynchronously.
                    AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendAcceptedSlotMailToRequester);
                    IAsyncResult result = taskDelegate.BeginInvoke(assessorMailDetail,
                        EntityType.AcceptedAvailabilityDate, new AsyncCallback(SendAcceptedSlotMailToRequesterCallBack),
                        taskDelegate);
                }

                // Close popup window.
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "Closeform", "<script language='javascript'>CloseAddTimeSlotWindow();</script>", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AddTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that send email to the requested assessor
        /// </summary>
        /// <param name="assessorDetail">
        /// A <see cref="AssessorDetail"/> that holds the interview details
        /// detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type to send request mail
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendAcceptedSlotMailToRequester(AssessorDetail assessorDetail,
            EntityType entityType)
        {
            try
            {
                // Send email.
                new EmailHandler().SendMail(entityType, assessorDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendAcceptedSlotMailToRequesterCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that construct and return the list of selected times
        /// </summary>
        /// <returns>Returns the list of selected times</returns>
        private List<AssessorTimeSlotDetail> ConstructSelectedTimes()
        {
            List<AssessorTimeSlotDetail> selectedTimes = new List<AssessorTimeSlotDetail>();
            foreach (ListItem cBox in AddTimeSlot_availableTimeCheckBoxList.Items)
            {
                AssessorTimeSlotDetail selectedTime = new AssessorTimeSlotDetail();
                if (cBox.Selected)
                {
                    selectedTime.TimeSlotIDFrom = Convert.ToInt32(cBox.Value);
                    selectedTime.TimeSlotIDTo = Convert.ToInt32(cBox.Value) + 1;
                    selectedTime.RequestStatus = Constants.TimeSlotRequestStatus.ACCEPTED_CODE;
                    selectedTimes.Add(selectedTime);
                }
            }
            return selectedTimes;
        }

        /// <summary>
        /// Handler method that is called when the from time slot dropdown
        /// list item is selected.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        protected void AddTimeSlot_fromDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Do not fire the event, if fire status is false.
                if (fireFromSelected == false)
                    return;

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AddTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when available 
        /// full time check box is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        protected void AddTimeSlot_availableFullTimeCheckBox_CheckedChanged(object sender,
            EventArgs e)
        {
            try
            {
                if (AddTimeSlot_availableFullTimeCheckBox.Checked)
                {
                    AddTimeSlot_availableTimeCheckBoxList.Enabled = false;
                }
                else
                {
                    AddTimeSlot_availableTimeCheckBoxList.Enabled = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AddTimeSlot_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValid = true;

            // Check if data is present.
            if (mode == "P" && AddTimeSlot_requestedDateTextBox.Text.Trim().Length == 0)
                isValid = false;

            // Check if requester is selected
            if (Utility.IsNullOrEmpty(AddTimeSlot_requesterHiddenField.Value))
            {
                isValid = false;
            }

            if (isValid == false)
            {
                base.ShowMessage(AddTimeSlot_topErrorMessageLabel, "Mandatory fields cannot be empty");
                return isValid;
            }

            if (AddTimeSlot_availableFullTimeCheckBox.Checked==false && 
                (ConstructSelectedTimes() == null || ConstructSelectedTimes().Count==0))
            {
                base.ShowMessage(AddTimeSlot_topErrorMessageLabel, "Select time");
                isValid = false;
                return isValid;
            }

            return isValid;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that sets the title based on the mode.
        /// </summary>
        /// <param name="mode">
        /// A <see cref="string"/> that holds the mode. Value 'A' represents
        /// 'Add', 'E' represents 'Edit' and 'P' represent 'Propose'.
        /// </param>
        private void SetTitle(string mode)
        {
            //Approve
            if (mode == "A")
            {
                Master.SetPageCaption("Approve Time Slot");
                AddTimeSlot_titleLiteral.Text = "Approve Time Slot";
            }
            else if (mode == "V") //View/Edit 
            {
                Master.SetPageCaption("View/Edit Time Slot");
                AddTimeSlot_titleLiteral.Text = "View/Edit Time Slot";
            }
            else if (mode == "P") //Propose new date
            {
                Master.SetPageCaption("Propose Time Slot");
                AddTimeSlot_titleLiteral.Text = "Propose Time Slot";
            }
        }

        /// <summary>
        /// Method that loads the data for edit mode.
        /// </summary>
        private void LoadEditTimeSlot()
        {
            // Check if time slot id is present.
            if (Utility.IsNullOrEmpty(Request.QueryString["id"]))
            {
                base.ShowMessage(AddTimeSlot_topErrorMessageLabel, "No time slot ID is passed");
                return;
            }

            int id = 0;
            int.TryParse(Request.QueryString["id"], out id);

            if (id == 0)
            {
                base.ShowMessage(AddTimeSlot_topErrorMessageLabel, "No valid time slot ID is passed");
                return;
            }

            // Get time slot detail.
            AssessorTimeSlotDetail timeSlot = new AssessorBLManager().GetAssessorTimeSlot(id);

            if (timeSlot == null)
            {
                base.ShowMessage(AddTimeSlot_topErrorMessageLabel, "No such time slot found");
                return;
            }

            AddTimeSlot_requestedDateLabel.Text = timeSlot.AvailabilityDate.ToString("ddd, MMM dd yyyy");
            
           
            AddTimeSlot_requesterTextBox.Text = timeSlot.InitiatedByName;
            AddTimeSlot_requesterHiddenField.Value = timeSlot.InitiatedBy.ToString();

            if (!Utility.IsNullOrEmpty(timeSlot.IsAvailableFullTime) && timeSlot.IsAvailableFullTime.Trim() == "Y" && mode=="A")
            {
                AddTimeSlot_availableFullTimeCheckBox.Checked = true;
                AddTimeSlot_availableTimeCheckBoxList.Enabled = false;
            }
            else
            {
                AddTimeSlot_availableTimeCheckBoxList.Enabled = true;
                AddTimeSlot_availableFullTimeCheckBox.Checked = false;
                if (timeSlot.assessorTimeSlotDetails != null && timeSlot.assessorTimeSlotDetails.Count > 0)
                {
                    foreach (ListItem cBox in AddTimeSlot_availableTimeCheckBoxList.Items)
                    {
                        int index = timeSlot.assessorTimeSlotDetails.
                            FindIndex(item => item.TimeSlotIDFrom == Convert.ToInt32(cBox.Value));
                        if (index >= 0)
                        {
                            cBox.Selected = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Method that loads the data for propose time slot mode.
        /// </summary>
        private void LoadProposeTimeSlot()
        {
            
            AddTimeSlot_requesterTextBox.Text = (((UserDetail)Session["USER_DETAIL"]).LastName == null) ? 
                ((UserDetail)Session["USER_DETAIL"]).FirstName : string.Format("{0} {1}", ((UserDetail)Session["USER_DETAIL"]).FirstName, 
                ((UserDetail)Session["USER_DETAIL"]).LastName);

            AddTimeSlot_requesterHiddenField.Value = ((UserDetail)Session["USER_DETAIL"]).UserID.ToString();

            // Set date.
            if (Session["MY_AVAILABILITY_DATE"] != null)
            {

                /*AddTimeSlot_requestedDateTextBox.Text =Convert.ToString(DateTime.ParseExact(Session["MY_AVAILABILITY_DATE"].ToString(),"MM/dd/yyyy",null));*/
                AddTimeSlot_requestedDateTextBox.Text = Convert.ToDateTime
                    (Session["MY_AVAILABILITY_DATE"]).ToString();
            }
        }

        /// <summary>
        /// Method to load the available times
        /// </summary>
        private void LoadTimes()
        {
            AssessorBLManager blManager = new AssessorBLManager();

            // Set time slot settings.
            AddTimeSlot_availableTimeCheckBoxList.DataSource = blManager.
                GetTimeSlotSettings(TimeSlotType.From);
            AddTimeSlot_availableTimeCheckBoxList.DataTextField = "FromTo";
            AddTimeSlot_availableTimeCheckBoxList.DataValueField = "ID";
            AddTimeSlot_availableTimeCheckBoxList.DataBind();
        }

        #endregion Private Methods
        
}
}