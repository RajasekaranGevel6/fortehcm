﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CategorySubjectLookup.aspx.cs
// File that represents the CategorySubjectLookup class that defines the user 
// interface layout and functionalities for the CategorySubjectLookup page. 
// This page helps in searching for categories and subject by providing search
// criteria for category and subject. 

#endregion

#region Directives                                                             

using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Web.UI.HtmlControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the CategorySubjectLookup page. This page helps in searching for 
    /// categories and subject by providing search criteria for category and
    /// subject. This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class CategorySubjectLookup : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button and focus.
                Page.Form.DefaultButton = CategorySubjectLookup_searchButton.UniqueID;

                // Set browser title.
                Master.SetPageCaption("Search Category/Subject");

                if (Request.QueryString["subjectmode"] != null)
                    CategorySubjectLookup_OkButton.Visible = true;
                else
                    CategorySubjectLookup_OkButton.Visible = false;

                if (!Utility.IsNullOrEmpty(CategorySubjectLookup_isMaximizedHiddenField.Value) &&
                        CategorySubjectLookup_isMaximizedHiddenField.Value == "Y")
                {
                    CategorySubjectLookup_searchCriteriasDiv.Style["display"] = "none";
                    CategorySubjectLookup_searchResultsUpSpan.Style["display"] = "block";
                    CategorySubjectLookup_searchResultsDownSpan.Style["display"] = "none";
                    CategorySubjectLookup_testDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    CategorySubjectLookup_searchCriteriasDiv.Style["display"] = "block";
                    CategorySubjectLookup_searchResultsUpSpan.Style["display"] = "none";
                    CategorySubjectLookup_searchResultsDownSpan.Style["display"] = "block";
                    CategorySubjectLookup_testDiv.Style["height"] = RESTORED_HEIGHT;
                }

                CategorySubjectLookup_searchTestResultsTR.Attributes.Add("onclick",
                   "ExpandOrRestore('" +
                   CategorySubjectLookup_testDiv.ClientID + "','" +
                   CategorySubjectLookup_searchCriteriasDiv.ClientID + "','" +
                   CategorySubjectLookup_searchResultsUpSpan.ClientID + "','" +
                   CategorySubjectLookup_searchResultsDownSpan.ClientID + "','" +
                   CategorySubjectLookup_isMaximizedHiddenField.ClientID + "','" +
                   RESTORED_HEIGHT + "','" +
                   EXPANDED_HEIGHT + "')");

                //Subscribe page number click event 
                CategorySubjectLookup_pagingNavigator.PageNumberClick +=
                    new PageNavigator.PageNumberClickEventHandler
                        (CategorySubjectLookup_bottomPagingNavigator_PageNumberClick);

                if (!IsPostBack)
                {
                    // Set default focus.
                    Page.Form.DefaultFocus = CategorySubjectLookup_categoryNameTextBox.UniqueID;
                    CategorySubjectLookup_categoryNameTextBox.Focus();

                    CategorySubjectLookup_testDiv.Visible = false;

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "CATEGORYNAME";

                    if ((!Utility.IsNullOrEmpty(Request.QueryString["source"])) &&
                        Request.QueryString["source"].Trim() == "lookup")
                    {
                        CategorySubjectLookup_categoryNameTextBox.Text = Request.QueryString["categoryname"].Trim();
                        CategorySubjectLookup_subjectNameTextBox.Text = Request.QueryString["subjectname"].Trim();
                        Search(1);
                    }
                    else if ((!Utility.IsNullOrEmpty(Request.QueryString["categoryname"]))
                        && (Request.QueryString["categoryname"].ToString().Trim().Length != 0))
                    {
                        CategorySubjectLookup_categoryNameTextBox.Text = Request.QueryString["categoryname"].ToString().Trim();
                    }
                }
                else
                {
                    CategorySubjectLookup_testDiv.Visible = true;
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(CategorySubjectLookup_errorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void CategorySubjectLookup_bottomPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                //Load the grid with corresponding page number
                Search(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(CategorySubjectLookup_errorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CategorySubjectLookup_resultsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    //object obj = e.Row.FindControl("CategorySubjectLookup_resultsGridViewInput");
                }

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(CategorySubjectLookup_errorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void CategorySubjectLookup_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                CategorySubjectLookup_errorMessageLabel.Text = string.Empty;
                CategorySubjectLookup_successMessageLabel.Text = string.Empty;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "CATEGORYID";

                // Reset the paging control.
                CategorySubjectLookup_pagingNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                Search(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(CategorySubjectLookup_errorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void CategorySubjectLookup_resultsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                // Add the sort image to the header column
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (CategorySubjectLookup_resultsGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    } 
                }

                if (e.Row.RowType != DataControlRowType.DataRow) return;

                if (Request.QueryString["subjectmode"] != null)
                {
                    LinkButton CategorySubjectLookup_resultsGridViewSelectButton = (LinkButton)e.Row.FindControl("CategorySubjectLookup_resultsGridViewSelectButton");
                    if (CategorySubjectLookup_resultsGridViewSelectButton != null)
                        CategorySubjectLookup_resultsGridViewSelectButton.Visible = false;

                    HtmlInputCheckBox inputCheckBox = (HtmlInputCheckBox)e.Row.FindControl("CategorySubjectLookup_resultsGridViewInput");
                    if (inputCheckBox != null)
                        inputCheckBox.Visible = true;

                    if (CategorySubjectLookup_selectedSubjectHiddenField.Value != null && CategorySubjectLookup_selectedSubjectHiddenField.Value != "")
                    {
                        object datarow = e.Row.DataItem;
                        Subject subject = (Subject)datarow;
                        string[] selectedSubject = CategorySubjectLookup_selectedSubjectHiddenField.Value.Split(',');

                        foreach (string skill in selectedSubject)
                        {
                            if (subject.SubjectName.Trim() == skill.Trim())
                            {
                                inputCheckBox.Checked = true;
                            }
                        } 
                    }
                    //subject.SubjectName=
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(CategorySubjectLookup_errorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void CategorySubjectLookup_resultsGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }

                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] = ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                CategorySubjectLookup_pagingNavigator.Reset();

                Search(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(CategorySubjectLookup_errorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void CategorySubjectLookup_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                CategorySubjectLookup_categoryIDTextBox.Text = string.Empty;
                CategorySubjectLookup_categoryNameTextBox.Text = string.Empty;
                CategorySubjectLookup_subjectNameTextBox.Text = string.Empty;
                CategorySubjectLookup_subjectIDTextBox.Text = string.Empty;

                CategorySubjectLookup_resultsGridView.DataSource = null;
                CategorySubjectLookup_resultsGridView.DataBind();

                CategorySubjectLookup_pagingNavigator.PageSize = base.GridPageSize;
                CategorySubjectLookup_pagingNavigator.TotalRecords = 0;

                CategorySubjectLookup_testDiv.Visible = false;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "CATEGORYID";

                // Reset to the restored state.
                CategorySubjectLookup_searchCriteriasDiv.Style["display"] = "block";
                CategorySubjectLookup_searchResultsUpSpan.Style["display"] = "none";
                CategorySubjectLookup_searchResultsDownSpan.Style["display"] = "block";
                CategorySubjectLookup_testDiv.Style["height"] = RESTORED_HEIGHT;
                CategorySubjectLookup_isMaximizedHiddenField.Value = "N";

                // Clear message fields.
                CategorySubjectLookup_successMessageLabel.Text = string.Empty;
                CategorySubjectLookup_errorMessageLabel.Text = string.Empty;

                // Set default focus.
                Page.Form.DefaultFocus = CategorySubjectLookup_categoryNameTextBox.UniqueID;
                CategorySubjectLookup_categoryNameTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(CategorySubjectLookup_errorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void Search(int pageNumber)
        {
            int totalRecords = 0;

            List<Subject> subjects = new List<Subject>();

            //Get the subject from the database
            subjects = new CommonBLManager().GetCategorySubjects(pageNumber, base.tenantID,
                CategorySubjectLookup_categoryIDTextBox.Text,
                CategorySubjectLookup_categoryNameTextBox.Text,
                CategorySubjectLookup_subjectIDTextBox.Text,
                CategorySubjectLookup_subjectNameTextBox.Text,
                base.GridPageSize, out totalRecords,
                ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]), string.Empty);

            if (subjects == null || subjects.Count == 0)
            {
                CategorySubjectLookup_testDiv.Visible = false;
                ShowMessage(CategorySubjectLookup_errorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                CategorySubjectLookup_testDiv.Visible = true;
            }

            CategorySubjectLookup_resultsGridView.DataSource = subjects;
            CategorySubjectLookup_resultsGridView.DataBind();
            CategorySubjectLookup_pagingNavigator.PageSize = GridPageSize;
            CategorySubjectLookup_pagingNavigator.TotalRecords = totalRecords;
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}