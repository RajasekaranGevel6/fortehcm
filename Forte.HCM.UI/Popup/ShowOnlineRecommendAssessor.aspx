<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowOnlineRecommendAssessor.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.ShowOnlineRecommendAssessor" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/InterviewQuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc4" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="SearchClientRequest_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">
        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl) {
            var nameCtrl = '<%= Request.QueryString["namectrl"] %>';
            var emailCtrl = '<%= Request.QueryString["emailctrl"] %>';
            var idCtrl = '<%= Request.QueryString["idctrl"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Set the user name field value. Replace the 'link button name' with the 
            // 'user name hidden field name' and retrieve the value.
            if (nameCtrl != null && nameCtrl != '') {
                window.opener.document.getElementById(nameCtrl).value
                    = document.getElementById(ctrl.id.replace("ShowOnlineRecommendAssessor_selectLinkButton", "ShowOnlineRecommendAssessor_userNameHiddenfield")).value;
            }

            // Set the email hidden field value. Replace the 'link button name' with the 
            // 'eamil hidden field name' and retrieve the value.
            if (emailCtrl != null && emailCtrl != '') {
                window.opener.document.getElementById(emailCtrl).value
                    = document.getElementById(ctrl.id.replace("ShowOnlineRecommendAssessor_selectLinkButton", "ShowOnlineRecommendAssessor_emailHiddenfield")).value;
            }

            // Set the user ID hidden field value. Replace the 'link button name' with the 
            // 'user ID hidden field name' and retrieve the value.
            if (idCtrl != null && idCtrl != '') {
                window.opener.document.getElementById(idCtrl).value
                    = document.getElementById(ctrl.id.replace("ShowOnlineRecommendAssessor_selectLinkButton", "ShowOnlineRecommendAssessor_userIDHiddenfield")).value;
            }
            self.close();
        }
        // Handler method that will be called when the 'save' button is 
        // clicked in the the show recommend page.
        function OnSelectClick() {
            var btncnrl = '<%= Request.QueryString["ctrlId"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Trigger the click event of the refresh button in the parent page.
            if (btncnrl != null && window.opener.document.getElementById(btncnrl) != null)
                window.opener.document.getElementById(btncnrl).click();

            self.close();
        }
    </script>
    <asp:UpdatePanel ID="ShowOnlineRecommendAssessor_pageUpdatePanel" runat="server">
        <ContentTemplate>
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td class="popup_td_padding_10">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 100%" class="popup_header_text_grey" align="left">
                                    <asp:Literal ID="ShowOnlineRecommendAssessor_searchAssessor" runat="server" Text="Search and Add Assessors"></asp:Literal>
                                </td>
                                <td style="width: 100%" align="right">
                                    <asp:ImageButton ID="ShowOnlineRecommendAssessor_topCancelImagebutton" runat="server"
                                        SkinID="sknCloseImageButton" OnClientClick="javascript:CloseMe()" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_2">
                    </td>
                </tr>
                <tr>
                    <td class="popup_td_padding_10" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                            <tr>
                                <td class="popup_td_padding_10">
                                    <div id="ShowOnlineRecommendAssessor_searchCriteriasDiv" runat="server" style="display: block;">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center">
                                                    <%--<asp:UpdatePanel runat="server" ID="ShowOnlineRecommendAssessor_simpleLinkUpdatePanel">
                                                <ContentTemplate>--%>
                                                    <asp:Label ID="ShowOnlineRecommendAssessor_topSuccessMessageLabel" runat="server"
                                                        SkinID="sknSuccessMessage"></asp:Label>
                                                    <asp:Label ID="ShowOnlineRecommendAssessor_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                    <asp:HiddenField ID="ShowOnlineRecommendAssessor_pageNumberHiddenField" runat="server"
                                                        Value="1" />
                                                    <asp:HiddenField runat="server" ID="ShowOnlineRecommendAssessor_isMaximizedHiddenField" />
                                                    <%--</ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div id="ShowOnlineRecommendAssessor_searchCriteriDiv" runat="server" style="display: block;">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td width="100%">
                                                                    <%--<asp:UpdatePanel runat="server" ID="ShowOnlineRecommendAssessor_SearchDivUpdatePanel">
                                                                <ContentTemplate>--%>
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="td_height_5" colspan="2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="ShowOnlineRecommendAssessor_ShowCandidateOptionTr" runat="server">
                                                                            <td colspan="2">
                                                                                <table cellpadding="2" cellspacing="2" border="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_candidateNamePopHeadLabel" runat="server"
                                                                                                Text="Candidate Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            <span class='mandatory'><asp:Label ID="ScheduleOnlineInterviewCandidate_mandatoryLabel" Text="*" runat="server" ></asp:Label></span>
                                                                                        </td>
                                                                                        <td align="left">
                                                                                            <asp:Label ID="ScheduleOnlineRecommendationAssessor_candidateNameLabel" runat="server"
                                                                                                SkinID="sknLabelFieldText"> </asp:Label>
                                                                                            <asp:TextBox ID="ScheduleOnlineRecommendationAssessor_Popup_candidateNameTextBox"
                                                                                                runat="server" ReadOnly="true" Text=""></asp:TextBox>
                                                                                            &nbsp;<asp:ImageButton ID="ScheduleOnlineRecommendationAssessor_candidateNameImageButton"
                                                                                                SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" ToolTip="Click here to search for candidates" />&nbsp;
                                                                                            <asp:ImageButton ID="ScheduleOnlineRecommendationAssessor_positionProfileCandidateImageButton"
                                                                                                SkinID="sknBtnSearchPositinProfileCandidateIcon" runat="server" ImageAlign="Middle"
                                                                                                ToolTip="Click here to search for candidates associated with the position profile" />&nbsp;
                                                                                            <asp:Button ID="ScheduleOnlineRecommendationAssessor_Popup_createCandidateButton"
                                                                                                Text="New" runat="server" SkinID="sknButtonId" ToolTip="Click here to create a new candidate" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleOnlineInterviewCandidate_Popup_emailHeadLabel" runat="server"
                                                                                                Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="ScheduleOnlineRecommendationAssessor_emailLabel" runat="server" SkinID="sknLabelFieldText"> </asp:Label>
                                                                                            <asp:TextBox ID="ScheduleOnlineRecommendationAssessor_Popup_emailTextBox" runat="server"
                                                                                                Text="" ReadOnly="true"></asp:TextBox>
                                                                                            <asp:HiddenField ID="ScheduleOnlineRecommendationAssessor_candidateIdHiddenField"
                                                                                                runat="server" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:ImageButton ToolTip="View Schedule" ID="ScheduleOnlineRecommendationAssessor_viewScheduleImageButton"
                                                                                                runat="server" SkinID="sknViewScheduleImageButton"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5" colspan="2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" class="grid_body_bg">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="td_height_5" colspan="2">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="CreateOnlineInterviewSession_skillHeadLabel" runat="server" Text="Skills/Areas"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">&nbsp;*</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="ShowOnlineRecommendAssessor_skillsTextBox" runat="server" Width="350px"
                                                                                                Columns="70" MaxLength="50"></asp:TextBox>
                                                                                            <asp:ImageButton ID="ShowOnlineRecommendAssessor_NewSkillButton" runat="server" ImageAlign="AbsMiddle"
                                                                                                SkinID="sknAddVectorGroupImageButton" Style="margin-left: 0px" ToolTip="Click here to add the entered skill"
                                                                                                Width="16px" OnClick="ShowOnlineRecommendAssessor_NewSkillButton_Click" />&nbsp;&nbsp;
                                                                                            <asp:ImageButton ID="ShowOnlineRecommendAssessor_SearchSkillButton" runat="server"
                                                                                                ImageAlign="AbsMiddle" SkinID="sknbtnSearchIcon" Style="margin-left: 0px" ToolTip="Click here to search & add skill"
                                                                                                Width="16px" />&nbsp;&nbsp;
                                                                                            <asp:ImageButton ID="ShowOnlineRecommendAssessor_newAssessorSkillsImageButton" runat="server"
                                                                                                ImageAlign="AbsMiddle" OnClientClick="javascript:return false;" SkinID="sknHelpImageButton"
                                                                                                Style="margin-left: 0px" ToolTip="Enter multiple skills with comma separated and click the add button" Width="16px" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            <asp:UpdatePanel ID="ShowOnlineRecommendAssessor_skillGridViewUpdatePanel" runat="server">
                                                                                                <ContentTemplate>
                                                                                                    <asp:GridView ID="ShowOnlineRecommendAssessor_skillGridView" runat="server" AutoGenerateColumns="False"
                                                                                                        EnableModelValidation="True" GridLines="Horizontal" BorderColor="white" BorderWidth="1px"
                                                                                                        OnRowDeleting="ShowOnlineRecommendAssessor_skillGridView_RowDeleting">
                                                                                                        <Columns>
                                                                                                            <asp:BoundField DataField="Category" HeaderText="Category" ItemStyle-Width="100px" />
                                                                                                            <asp:BoundField DataField="Skill" HeaderText="Skill" ItemStyle-Width="150px" />
                                                                                                            <asp:CommandField ButtonType="Image" DeleteImageUrl="~/Images/delete.png" ShowDeleteButton="True"
                                                                                                                ItemStyle-Width="50px" />
                                                                                                            <asp:TemplateField>
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:HiddenField ID="ShowOnlineRecommendAssessor_skillGridView_skillIDHiddenField"
                                                                                                                        runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "SkillID")%>' />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                    </asp:GridView>
                                                                                                    <asp:HiddenField ID="ShowOnlineRecommendAssessor_CaegoryIDHiddenField" runat="server" />
                                                                                                    <asp:HiddenField ID="ShowOnlineRecommendAssessor_SkillIDHiddenField" runat="server" />
                                                                                                </ContentTemplate>
                                                                                            </asp:UpdatePanel>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5" colspan="2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr runat="server" id="ShowOnlineRecommendAssessor_ShowSearchCriteriaTr">
                                                                            <td colspan="2">
                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            <div style="float: left; width: 92%;" class="grouping_border_bg">
                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:CheckBox ID="ShowOnlineRecommendAssessor_interviewedPastCheckBox" runat="server"
                                                                                                                Text="Display only assessors that have interviewed for this client in the past">
                                                                                                            </asp:CheckBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:CheckBox ID="ShowOnlineRecommendAssessor_haveTimeSlotsCheckBox" runat="server"
                                                                                                                Text="Display only assessors that have provided time slots"></asp:CheckBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:CheckBox ID="ShowOnlineRecommendAssessor_matchesAllAreasCheckBox" runat="server"
                                                                                                                Text="Display only assessors that can assess all areas"></asp:CheckBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                <tr>
                                                                                                    <td valign="top">
                                                                                                        <div style="float: left; width: 97%;" class="grouping_border_bg">
                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                <tr>
                                                                                                                    <td colspan="3" align="left">
                                                                                                                        <asp:RadioButton ID="ShowOnlineRecommendAssessor_showAllRadioButton" runat="server"
                                                                                                                            Text="Show All" GroupName="Show" Checked="true" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td align="left">
                                                                                                                        <asp:RadioButton ID="ShowOnlineRecommendAssessor_showTopRadioButton" runat="server"
                                                                                                                            GroupName="Show" Text="Show Top" />
                                                                                                                    </td>
                                                                                                                    <td align="right">
                                                                                                                        <asp:TextBox ID="ShowOnlineRecommendAssessor_topMatchesTextBox" MaxLength="2" runat="server"
                                                                                                                            Width="30px" TabIndex="10"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td align="left">
                                                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                                                            <tr>
                                                                                                                                <td style="vertical-align: bottom">
                                                                                                                                    <asp:ImageButton ID="ShowOnlineRecommendAssessor_upImageButton" runat="server" ImageAlign="AbsBottom"
                                                                                                                                        SkinID="sknNumericUpArrowImage" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="vertical-align: top">
                                                                                                                                    <asp:ImageButton ID="ShowOnlineRecommendAssessor_downImageButton" runat="server"
                                                                                                                                        ImageAlign="Top" SkinID="sknNumericDownArrowImage" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="ShowOnlineRecommendAssessor_matchesLabel" runat="server" Text="Matches"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <ajaxToolKit:NumericUpDownExtender ID="ShowOnlineRecommendAssessor_NumericUpDownExtender"
                                                                                                                    Width="60" runat="server" Minimum="0" Maximum="30" Step="1" TargetControlID="ShowOnlineRecommendAssessor_topMatchesTextBox"
                                                                                                                    TargetButtonUpID="ShowOnlineRecommendAssessor_upImageButton" TargetButtonDownID="ShowOnlineRecommendAssessor_downImageButton">
                                                                                                                </ajaxToolKit:NumericUpDownExtender>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="td_height_5" colspan="2">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <div style="float: left; width: 97%;" class="grouping_border_bg">
                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="ShowOnlineRecommendAssessor_showPerPageLabel" runat="server" Text="Shows"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td align="right">
                                                                                                                        <asp:TextBox ID="ShowOnlineRecommendAssessor_showPerPageTextBox" MaxLength="2" Text="2"
                                                                                                                            runat="server" Width="30px" TabIndex="10"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td align="left">
                                                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                                                            <tr>
                                                                                                                                <td style="vertical-align: bottom">
                                                                                                                                    <asp:ImageButton ID="ShowOnlineRecommendAssessor_upImageButtonPerPage" runat="server"
                                                                                                                                        ImageAlign="AbsBottom" SkinID="sknNumericUpArrowImage" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="vertical-align: top">
                                                                                                                                    <asp:ImageButton ID="ShowOnlineRecommendAssessor_downImageButtonPerPage" runat="server"
                                                                                                                                        ImageAlign="Top" SkinID="sknNumericDownArrowImage" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="ShowOnlineRecommendAssessor_assessorPerPageLabel" runat="server" Text="Assessors Per Page"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <ajaxToolKit:NumericUpDownExtender ID="ShowOnlineRecommendAssessor_NumericUpDownExtenderPerPage"
                                                                                                                        Width="60" runat="server" Minimum="0" Maximum="30" Step="1" TargetControlID="ShowOnlineRecommendAssessor_showPerPageTextBox"
                                                                                                                        TargetButtonUpID="ShowOnlineRecommendAssessor_upImageButtonPerPage" TargetButtonDownID="ShowOnlineRecommendAssessor_downImageButtonPerPage">
                                                                                                                    </ajaxToolKit:NumericUpDownExtender>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5" colspan="2">
                                                                            </td>
                                                                        </tr>
                                                                        <tr runat="server" id="ShowOnlineRecommendAssessor_ShowSortingTr">
                                                                            <td colspan="2">
                                                                                <div style="float: left; width: 99%;" class="grouping_border_bg">
                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="ShowOnlineRecommendAssessor_sortOrderHeadLabel" runat="server" Text="Sort Order"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="ShowOnlineRecommendAssessor_sortOrderDropDownList" runat="server">
                                                                                                    <asp:ListItem Text="Ascending" Value="A"></asp:ListItem>
                                                                                                    <asp:ListItem Text="Descending" Value="D"></asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="ShowOnlineRecommendAssessor_sortByHeadLabel" runat="server" Text="Sort By"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="ShowOnlineRecommendAssessor_sortByDropDownList" runat="server">
                                                                                                    <asp:ListItem Text="Assessors that have interviewed for this client in the past"
                                                                                                        Value="1"></asp:ListItem>
                                                                                                    <asp:ListItem Text="Assessors that have provided time slots" Value="2"></asp:ListItem>
                                                                                                    <asp:ListItem Text="Assessors who can assess most areas" Value="3"></asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <%--</ContentTemplate>
                                                            </asp:UpdatePanel>--%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" class="td_padding_top_5">
                                                                    <%-- <asp:UpdatePanel ID="ShowOnlineRecommendAssessor_searchButtonUpdatePanel" runat="server">
                                                                <ContentTemplate>--%>
                                                                    <asp:Button ID="ShowOnlineRecommendAssessor_topSearchButton" runat="server" Text="Search"
                                                                        SkinID="sknButtonId" OnClick="ShowOnlineRecommendAssessor_topSearchButton_Click" />
                                                                    <%--</ContentTemplate>
                                                            </asp:UpdatePanel>--%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_8">
                                                                </td>
                                                            </tr>
                                                            <tr id="ShowOnlineRecommendAssessor_ShowAssesserResultsHeaderTR" runat="server">
                                                                <td class="header_bg">
                                                                    <%--<asp:UpdatePanel ID="ShowOnlineRecommendAssessor_expandAllUpdatePanel" runat="server">
                                                                <ContentTemplate>--%>
                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                        <tr>
                                                                            <td style="width: 50%" align="left" class="header_text_bold">
                                                                                <asp:Literal ID="ShowOnlineRecommendAssessor_searchResultsLiteral" runat="server"
                                                                                    Text="Search Results"> </asp:Literal>
                                                                            </td>
                                                                            <td style="width: 48%" align="left">
                                                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:HiddenField ID="ShowOnlineRecommendAssessor_stateExpandHiddenField" runat="server"
                                                                                                Value="0" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td style="width: 2%" align="right">
                                                                                <span id="ShowOnlineRecommendAssessor_searchResultsUpSpan" runat="server" style="display: none;">
                                                                                    <asp:Image ID="ShowOnlineRecommendAssessor_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                                                        id="ShowOnlineRecommendAssessor_searchResultsDownSpan" runat="server" style="display: none;"><asp:Image
                                                                                            ID="ShowOnlineRecommendAssessor_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                                                <asp:HiddenField ID="ShowOnlineRecommendAssessor_restoreHiddenField" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <%--</ContentTemplate>
                                                            </asp:UpdatePanel>--%>
                                                                </td>
                                                            </tr>
                                                            <tr id="ShowOnlineRecommendAssessor_ShowAssesserResultsTR" runat="server">
                                                                <td class="grid_body_bg">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td align="right">
                                                                                <asp:LinkButton ID="ShowOnlineRecommendAssessor_addAssessorLinkButton" runat="server"
                                                                                    Text="Add Assessor" SkinID="sknAddLinkButton"> </asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_8">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <%--<asp:UpdatePanel ID="ShowOnlineRecommendAssessor_pagingControlUpdatePanel" runat="server">
                                                                            <ContentTemplate>--%>
                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <asp:ImageButton ID="ShowOnlineRecommendAssessor_previousImageButton" runat="server"
                                                                                                ImageUrl="~/App_Themes/DefaultTheme/Images/nav_btn_prev.gif" CommandName="Previous"
                                                                                                OnClick="OnNavigatePage" ToolTip="Previous" Width="20px" Height="20px" SkinID="sknPreviousDateImage" />
                                                                                        </td>
                                                                                        <td align="right">
                                                                                            <asp:ImageButton ID="ShowOnlineRecommendAssessor_nextImageButton" runat="server"
                                                                                                ImageUrl="~/App_Themes/DefaultTheme/Images/nav_btn_next.gif" CommandName="Next"
                                                                                                OnClick="OnNavigatePage" ToolTip="Next" Width="20px" Height="20px" SkinID="sknNextDateImage" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <%--</ContentTemplate>
                                                                        </asp:UpdatePanel>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_5">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <div style="display: none">
                                                                                    <asp:Button ID="ShowOnlineRecommendAssessor_refreshGridButton" runat="server" Text="Refresh"
                                                                                        SkinID="sknButtonId" OnClick="ShowOnlineRecommendAssessor_refreshGridButton_Click" />
                                                                                    <asp:Button ID="ShowOnlineRecommendAssessor_addTimeSlotButton" runat="server" Text="Refresh"
                                                                                        SkinID="sknButtonId" />
                                                                                </div>
                                                                                <asp:HiddenField runat="server" ID="ShowOnlineRecommendAssessor_addedUserIdHiddenField" />
                                                                                <asp:HiddenField runat="server" ID="ShowOnlineRecommendAssessor_addTimeSlotHiddenField" />
                                                                                <div style="display: none">
                                                                                    <asp:TextBox runat="server" ID="ShowOnlineRecommendAssessor_assessorNameTextBox"></asp:TextBox>
                                                                                </div>
                                                                                <div style="overflow: auto;" runat="server" id="ShowOnlineRecommendAssessor_assessorDiv">
                                                                                    <asp:GridView ID="ShowOnlineRecommendAssessor_assesserDetailsGridView" SkinID="sknRecommendAssessorGrid"
                                                                                        AllowSorting="false" runat="server" AutoGenerateColumns="true" OnRowCommand="ShowOnlineRecommendAssessor_assesserDetailsGridView_RowCommand"
                                                                                        OnRowDataBound="ShowOnlineRecommendAssessor_assesserDetailsGridView_RowDataBound"
                                                                                        OnRowDeleting="ShowOnlineRecommendAssessor_assesserDetailsGridView_RowDeleting">
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" class="td_padding_top_5">
                                                                                <asp:Button ID="ShowOnlineRecommendAssessor_bottomSaveButton" runat="server" Text="Request For Availability"
                                                                                    SkinID="sknButtonId" OnClick="ShowOnlineRecommendAssessor_saveButton_Click" />
                                                                                &nbsp;&nbsp;<asp:Button ID="ShowOnlineRecommendAssessor_saveSlotsButton" runat="server"
                                                                                    Text="Save" SkinID="sknButtonId" OnClick="ShowOnlineRecommendAssessor_saveSlotsButton_Click" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table cellpadding="3" cellspacing="3">
                            <tr>
                                <td>
                                    <asp:Button ID="ShowOnlineRecommendAssessor_scheduleButton" runat="server" Text="Schedule Candidate"
                                        SkinID="sknButtonId" OnClick="ShowOnlineRecommendAssessor_scheduleButton_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="ShowOnlineRecommendAssessor_addAssessorButton" runat="server" Text="Select"
                                        SkinID="sknButtonId" OnClick="ShowOnlineRecommendAssessor_addAssessorButton_Click" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="ShowAssesserLookup_resetLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                        Text="Reset" OnClick="ShowAssesserLookup_resetLinkButton_Click"></asp:LinkButton>
                                </td>
                                <td class="pop_divider_line">
                                    |
                                </td>
                                <td>
                                    <asp:LinkButton ID="ShowOnlineRecommendAssessor_addAssessorCancel" runat="server"
                                        SkinID="sknPopupLinkButton" Text="Cancel" OnClientClick="javascript:window.close()"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="ShowOnlineRecommendAssessor_addSkillPanel" runat="server" CssClass="popupcontrol_addSkill">
                            <div style="display: none;">
                                <asp:Button ID="ShowOnlineRecommendAssessor_addSkillHiddenButton" runat="server"
                                    Text="Hidden" /></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="ShowOnlineRecommendAssessor_addSkillLiteral" runat="server" Text="Add Skill"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="ShowOnlineRecommendAssessor_addSkillTopCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_10">
                                        <table cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                            <tr>
                                                <td align="left" class="popup_td_padding_10">
                                                    <table border="0" cellpadding="0" cellspacing="5" class="tab_body_bg" align="left">
                                                        <tr>
                                                            <td class="msg_align" colspan="4">
                                                                <asp:Label ID="ShowOnlineRecommendAssessor_addSkillErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="ShowOnlineRecommendAssessor_addSkillLabel" runat="server" Text="Skill"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                <span class='mandatory'>*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="ShowOnlineRecommendAssessor_addSkillTextBox" runat="server" MaxLength="100"
                                                                    Columns="75" extMode="SingleLine" Wrap="true"> </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_td_padding_5">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="left" style="width: 20px; padding-right: 5px">
                                                    <asp:Button ID="ShowOnlineRecommendAssessor_addSkillSaveButton" runat="server" SkinID="sknButtonId"
                                                        Text="Save" OnClick="ShowOnlineRecommendAssessor_addSkillSaveButton_Clicked" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="ShowOnlineRecommendAssessor_addSkillCloseLinkButton" SkinID="sknPopupLinkButton"
                                                        runat="server" Text="Cancel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="ShowOnlineRecommendAssessor_addSkillModalPopupExtender"
                            runat="server" PopupControlID="ShowOnlineRecommendAssessor_addSkillPanel" TargetControlID="ShowOnlineRecommendAssessor_addSkillHiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
