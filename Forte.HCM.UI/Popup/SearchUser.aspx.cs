﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SearchUser.aspx.cs
// File that represents the SearchUser class that defines the user interface
// layout and functionalities for the SearchUser page. This page helps in 
// searching for users by providing search criteria for user name, first name, 
// middle name and last name This class inherits Forte.HCM.UI.Common.PageBase
// class.

#endregion

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the SearchUser page. This page helps in searching for users by
    /// providing search criteria for user name, first name, middle name and
    /// last name. This class inherits the Forte.HCM.UI.Common.PageBase 
    /// class.
    /// </summary>
    public partial class SearchUser : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Events Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //set browser Title
                Master.SetPageCaption("Search User");

                // Set default button and focus.
                Page.Form.DefaultButton = SearchUser_searchButton.UniqueID;

                // Subscribes to the page number click event.
                SearchUser_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (SearchUser_pageNavigator_PageNumberClick);

                if (!Utility.IsNullOrEmpty(SearchUser_isMaximizedHiddenField.Value) &&
                    SearchUser_isMaximizedHiddenField.Value == "Y")
                {
                    SearchUser_searchCriteriasDiv.Style["display"] = "none";
                    SearchUser_searchResultsUpSpan.Style["display"] = "block";
                    SearchUser_searchResultsDownSpan.Style["display"] = "none";
                    SearchUser_testDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    SearchUser_searchCriteriasDiv.Style["display"] = "block";
                    SearchUser_searchResultsUpSpan.Style["display"] = "none";
                    SearchUser_searchResultsDownSpan.Style["display"] = "block";
                    SearchUser_testDiv.Style["height"] = RESTORED_HEIGHT;
                }

                // This validation is done for focusing the linkbutton of the input
                // fields such as category, subject, test area.
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SearchUser_browserHiddenField.Value))
                {
                    ValidateEnterKey(SearchUser_browserHiddenField.Value.Trim());
                    SearchUser_browserHiddenField.Value = string.Empty;
                }

                if (!IsPostBack)
                {
                    // Set default focus.
                    Page.Form.DefaultFocus = SearchUser_userNameTextBox.UniqueID;
                    SearchUser_userNameTextBox.Focus();

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "USERNAME";

                    SearchUser_testDiv.Visible = false;
                }
                else
                {
                    SearchUser_testDiv.Visible = true;
                }

                SearchUser_searchTestResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    SearchUser_testDiv.ClientID + "','" +
                    SearchUser_searchCriteriasDiv.ClientID + "','" +
                    SearchUser_searchResultsUpSpan.ClientID + "','" +
                    SearchUser_searchResultsDownSpan.ClientID + "','" +
                    SearchUser_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchUser_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply the search filters and display the results in the
        /// grid.
        /// </remarks>
        protected void SearchUser_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                SearchUser_errorMessageLabel.Text = string.Empty;
                SearchUser_successMessageLabel.Text = string.Empty;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "USERNAME";

                // Reset the paging control.
                SearchUser_pageNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchUser_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void SearchUser_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchUser_userNameTextBox.Text = string.Empty;
                SearchUser_firstNameTextBox.Text = string.Empty;
                SearchUser_middleNameTextBox.Text = string.Empty;
                SearchUser_lastNameTextBox.Text = string.Empty;

                SearchUser_testGridView.DataSource = null;
                SearchUser_testGridView.DataBind();

                SearchUser_pageNavigator.PageSize = base.GridPageSize;
                SearchUser_pageNavigator.TotalRecords = 0;
                SearchUser_testDiv.Visible = false;

                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "USERNAME";

                // Reset to the restored state.
                SearchUser_searchCriteriasDiv.Style["display"] = "block";
                SearchUser_searchResultsUpSpan.Style["display"] = "none";
                SearchUser_searchResultsDownSpan.Style["display"] = "block";
                SearchUser_testDiv.Style["height"] = RESTORED_HEIGHT;
                SearchUser_isMaximizedHiddenField.Value = "N";

                // Clear message fields.
                SearchUser_successMessageLabel.Text = string.Empty;
                SearchUser_errorMessageLabel.Text = string.Empty;

                // Set default focus.
                Page.Form.DefaultFocus = SearchUser_userNameTextBox.UniqueID;
                SearchUser_userNameTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchUser_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void SearchUser_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                Search(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchUser_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void SearchUser_testGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                SearchUser_pageNavigator.Reset();
                Search(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchUser_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void SearchUser_testGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchUser_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void SearchUser_testGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (SearchUser_testGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchUser_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods

        /// <summary>
        /// Represents the method that applies the search filters and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void Search(int pageNumber)
        {
            List<UserDetail> users = null;

            int totalRecords = 0;

            //If the popup is called from the credit management page 
            // then need to take all candidates
            if (Request.QueryString["home"] != null &&
                Request.QueryString["home"].ToString().Length > 0 &&
                Request.QueryString["home"].ToString().Trim() == "CreditManagement")
            {
                users = new CommonBLManager().GetUsersAndCandidates(SearchUser_userNameTextBox.Text.Trim(),
                SearchUser_firstNameTextBox.Text.Trim(),
                SearchUser_middleNameTextBox.Text.Trim(),
                SearchUser_lastNameTextBox.Text.Trim(),
                ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]),
                pageNumber,
                base.GridPageSize,base.tenantID,
                out totalRecords);
            }
            else
            {
                string roleCode = "CA";
                if (Request.QueryString["rolecodes"] == "TS" ||
                    Request.QueryString["rolecodes"] == "TC" ||
                    Request.QueryString["rolecodes"] == "TR") // Test Report 
                    roleCode = "TS";
                users = new CommonBLManager().GetUsers
                (SearchUser_userNameTextBox.Text.Trim(),
                SearchUser_firstNameTextBox.Text.Trim(),
                SearchUser_middleNameTextBox.Text.Trim(),
                SearchUser_lastNameTextBox.Text.Trim(),
                base.userID,
                ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]),
                pageNumber,
                base.GridPageSize, roleCode,
                out totalRecords);
            }

            if ((Request.QueryString["home"] != null && Request.QueryString["home"].ToString().Length > 0) &&
                (Request.QueryString["home"].ToString().Trim() == "PO_PRO_FORM_CREAT" ||
                Request.QueryString["home"].ToString().Trim() == "PO_PRO_CREA" ||
                Request.QueryString["home"].ToString().Trim() == "CLNT_MGMT" || Request.QueryString["home"].ToString().Trim() == "REVIEW_RESUME"))
            {
                users = new CommonBLManager().GetTenantUsers
                    (SearchUser_userNameTextBox.Text.Trim(),
                    SearchUser_firstNameTextBox.Text.Trim(),
                    SearchUser_middleNameTextBox.Text.Trim(),
                    SearchUser_lastNameTextBox.Text.Trim(),
                    ViewState["SORT_FIELD"].ToString(),
                    ((SortType)ViewState["SORT_ORDER"]),
                    pageNumber,
                    base.GridPageSize, out totalRecords, base.tenantID, base.userID);
            }
            else if ((Request.QueryString["home"] != null && Request.QueryString["home"].ToString().Length > 0) &&
                (Request.QueryString["home"].ToString().Trim().ToUpper() == "PPMOD" || Request.QueryString["home"].ToString().Trim() == "ACTI_LOG"
                 ))
            {
                users = new CommonBLManager().GetPositionProfileFormTenantUsers
                    (SearchUser_userNameTextBox.Text.Trim(),
                    SearchUser_firstNameTextBox.Text.Trim(),
                    SearchUser_middleNameTextBox.Text.Trim(),
                    SearchUser_lastNameTextBox.Text.Trim(),
                    ViewState["SORT_FIELD"].ToString(),
                    ((SortType)ViewState["SORT_ORDER"]),
                    pageNumber,
                    base.GridPageSize, out totalRecords, base.tenantID);
            }

            if (users == null || users.Count == 0)
            {
                SearchUser_testDiv.Visible = false;
                ShowMessage(SearchUser_errorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                SearchUser_testDiv.Visible = true;
            }

            SearchUser_testGridView.DataSource = users;
            SearchUser_testGridView.DataBind();
            SearchUser_pageNavigator.PageSize = base.GridPageSize;
            SearchUser_pageNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that will call when the page gets loaded. This will perform
        /// to call the event handlers based parameter.
        /// </summary>
        /// <param name="stringValue">
        /// A <see cref="string"/> that contains the string.
        /// </param>
        private void ValidateEnterKey(string stringValue)
        {
            switch (stringValue.Trim())
            {
                case "dummy":
                    foreach (GridViewRow row in SearchUser_testGridView.Rows)
                    {
                        LinkButton SearchUser_selectLinkButton = (LinkButton)
                            row.FindControl("SearchUser_selectLinkButton");

                        System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "OnSelectClickMethod",
                            "javascript:OnSelectClick('" + SearchUser_selectLinkButton.ClientID + "');", true);
                    }
                    break;
            }
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}

