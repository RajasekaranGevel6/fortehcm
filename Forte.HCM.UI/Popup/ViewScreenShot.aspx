﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewScreenShot.aspx.cs" Inherits="Forte.HCM.UI.Popup.ViewScreenShot"
    MasterPageFile="~/MasterPages/PopupMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ViewScreenShot_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2"  align="center">
                            <asp:Label ID="ViewScreenShot_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey" align="left">
                            <asp:Literal ID="ViewScreenShot_headerLiteral" runat="server" Text="Preview"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="ViewScreenShot_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" ToolTip="Click here to close the window"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="ViewScreenShot_updatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0" runat="server" id="ViewScreenShot_navigationTable">
                                        <tr>
                                            <td style="width: 46%" align="right">
                                                <asp:ImageButton ID="ViewScreenShot_previousImageButton" runat="server" SkinID="sknViewPreviousImageButton"
                                                    ToolTip="Previous" OnClick="ViewScreenShot_previousImageButton_Click" />
                                            </td>
                                            <td style="width: 8%" align="center">
                                                <asp:Label ID="ViewScreenShot_imagePaging" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                            </td>
                                            <td style="width: 46%" align="left">
                                                <asp:ImageButton ID="ViewScreenShot_nextImageButton" runat="server" SkinID="sknViewNextImageButton"
                                                    ToolTip="Next" OnClick="ViewScreenShot_nextImageButton_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Image ID="ViewScreenShot_previewImage" runat="server" Height="440px" Width="620px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:TextBox ID="ViewScreenShot_messageTextBox" runat="server" ReadOnly="true" Width="617px"
                                        TextMode="MultiLine" SkinID="sknMultiLineDisplayTextBox"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_8">
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:LinkButton ID="ViewScreenShot_bottomCancelLinkButton" runat="server" Text="Cancel"
                    SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" ToolTip="Click here to close the window"/>
            </td>
        </tr>
    </table>
</asp:Content>
