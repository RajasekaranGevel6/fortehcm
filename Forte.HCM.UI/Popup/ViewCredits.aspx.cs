﻿#region Header

// Copyright (C) Forte. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ViewCredits.aspx.cs
// File that represents the user interface layout and functionalities 
// for the View Credits page. This page helps to  view user credits.
// This class inherits Forte.HCM.UI.Common.PageBase class.

#endregion Header

#region Directives

using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the ViewCredits class that defines the user interface
    /// layout and functionalities for the View Credits page. This page helps to 
    /// view user credits.This class inherits Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class ViewCredits : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "250px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "275px";

        #endregion Private Constants

        #region Events Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { 
                //Set browser title
                Master.SetPageCaption("View Credits");
                //Clear cache
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                // Subscribes to the page number click event.
                ViewCredits_pageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                       (ViewCredits_pageNavigator_PageNumberClick);
                // Set Expanded or Restored height
                if (!Utility.IsNullOrEmpty(ViewCredits_isMaximizedHiddenField.Value) &&
                   ViewCredits_isMaximizedHiddenField.Value == "Y")
                {
                    ViewCredits_creditsDiv.Style["display"] = "none";
                    ViewCredits_searchResultsUpSpan.Style["display"] = "block";
                    ViewCredits_searchResultsDownSpan.Style["display"] = "none";
                    ViewCredits_creditsUsageHistoryDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    ViewCredits_creditsDiv.Style["display"] = "block";
                    ViewCredits_searchResultsUpSpan.Style["display"] = "none";
                    ViewCredits_searchResultsDownSpan.Style["display"] = "block";
                    ViewCredits_creditsUsageHistoryDiv.Style["height"] = RESTORED_HEIGHT;
                }

                //Get the values from quesry string
                if (Request.QueryString["qstring"] != null &&
                     Request.QueryString["qstring"] == "CreditSearch")
                {
                    ViewCredits_DIV.Style["display"] = "block";
                }

                if (Request.QueryString["userID"] != null)
                {
                    ViewState["CANDIDATE_ID"] = Request.QueryString["userID"].ToString();
                }


                //Load values
                if (!IsPostBack)
                {
                    ViewCredits_creditsUsageHistoryDiv.Visible = false;
                    LoadValues();
                }
                else
                    ViewCredits_creditsUsageHistoryDiv.Visible = true;


               
                //Set attribute to purchase credit button   
                ViewCredits_creditsUsageHistoryResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    ViewCredits_creditsUsageHistoryDiv.ClientID + "','" +
                    ViewCredits_creditsDiv.ClientID + "','" +
                    ViewCredits_searchResultsUpSpan.ClientID + "','" +
                    ViewCredits_searchResultsDownSpan.ClientID + "','" +
                    ViewCredits_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCredits_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the selected indexis changed
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will display appropriate selected index results in the
        /// grid.
        /// </remarks>
        protected void ViewCredits_creditsTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ViewCredits_errorMessageLabel.Text = string.Empty;
                ViewCredits_successMessageLabel.Text = string.Empty;
                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "DATE";
                // Reset the paging control.
                ViewCredits_pageNavigator.Reset();
                // drop down button retrieves data for selected option.
                LoadMyCreditsGrid(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCredits_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void ViewCredits_resetButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ViewCredits_creditsTypeDropDownList.SelectedIndex = 0;
                ViewCredits_errorMessageLabel.Text = string.Empty;
                ViewCredits_successMessageLabel.Text = string.Empty;

                // Reset default sort field .
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "DATE";

                // Reset to restored height
                ViewCredits_creditsDiv.Style["display"] = "block";
                ViewCredits_searchResultsUpSpan.Style["display"] = "none";
                ViewCredits_searchResultsDownSpan.Style["display"] = "block";
                ViewCredits_creditsUsageHistoryDiv.Style["height"] = RESTORED_HEIGHT;
                ViewCredits_isMaximizedHiddenField.Value = "N";

                // Reset the paging control.
                ViewCredits_pageNavigator.Reset();

                // Reset default datas in grid
                LoadMyCreditsGrid(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCredits_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void ViewCredits_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadMyCreditsGrid(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCredits_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void ViewCredits_creditsUsageHistoryGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                // Reset and show records for first page.
                ViewCredits_pageNavigator.Reset();
                LoadMyCreditsGrid(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCredits_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void ViewCredits_creditsUsageHistoryGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCredits_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void ViewCredits_creditsUsageHistoryGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (ViewCredits_creditsUsageHistoryGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ViewCredits_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods

        /// <summary>
        /// Represents the method that applies the default or selected item from dropdownlist and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadMyCreditsGrid(int pageNumber)
        {
            int totalRecords = 0;
            List<CreditUsageDetail> creditUsageHistory = new CreditBLManager().GetCreditsUsageHistory(
                int.Parse(ViewState["CANDIDATE_ID"].ToString()), ViewCredits_creditsTypeDropDownList.SelectedValue, pageNumber, base.GridPageSize,
                out totalRecords, ViewState["SORT_FIELD"].ToString(), ((SortType)ViewState["SORT_ORDER"]));

            if (creditUsageHistory == null || creditUsageHistory.Count == 0)
            {
                ViewCredits_creditsUsageHistoryDiv.Visible = false;
                ShowMessage(ViewCredits_errorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
                ViewCredits_creditsUsageHistoryDiv.Visible = true;

            ViewCredits_creditsUsageHistoryGridView.DataSource = creditUsageHistory;
            ViewCredits_creditsUsageHistoryGridView.DataBind();
            ViewCredits_pageNavigator.PageSize = base.GridPageSize;
            ViewCredits_pageNavigator.TotalRecords = totalRecords;
            ViewCredits_creditsTypeDropDownList.Focus();
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            CreditsSummary creditSummary = new CreditBLManager().GetCreditSummary(int.Parse(ViewState["CANDIDATE_ID"].ToString()));
            if (creditSummary != null)
            {
                ViewCredits_userIdValueLabel.Text = creditSummary.UserID;
                ViewCredits_availableCreditsValueLabel.Text = creditSummary.AvailableCredits.ToString();
                ViewCredits_creditsEarnedValueLabel.Text = creditSummary.CreditsEarned.ToString();
                ViewCredits_creditsRedeemedValueLabel.Text = creditSummary.CreditsRedeemed.ToString();
            }
            List<AttributeDetail> attributeDetail = new AttributeBLManager().GetCreditTypeAttributes();
            if (attributeDetail != null && attributeDetail.Count > 0)
            {
                ViewCredits_creditsTypeDropDownList.DataSource = attributeDetail;
                ViewCredits_creditsTypeDropDownList.DataTextField = "AttributeName";
                ViewCredits_creditsTypeDropDownList.DataValueField = "AttributeID";
                ViewCredits_creditsTypeDropDownList.DataBind();
                ViewCredits_creditsTypeDropDownList.Items.Insert(0, new ListItem("Both", "0"));
                ViewCredits_creditsTypeDropDownList.Items[0].Selected = true;
            }
            // Clear messages.
            ViewCredits_errorMessageLabel.Text = string.Empty;
            ViewCredits_successMessageLabel.Text = string.Empty;

            // Reset default sort field and order keys.
            if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                ViewState["SORT_ORDER"] = SortType.Ascending;
            if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                ViewState["SORT_FIELD"] = "DATE";

            // Reset the paging control.
            ViewCredits_pageNavigator.Reset();

            // By default on load retrieves data for  page number 1.
            LoadMyCreditsGrid(1);
        }

        #endregion Protected Overridden Methods
    }
}
