﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchInterviewQuestion.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.SearchInterviewQuestion" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/InterviewQuestionDetailPreviewControl.ascx" TagName="QuestionDetailPreviewControl"
    TagPrefix="uc4" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="SearchClientRequest_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">
        // Handler method that will be called when the 'Select' link is 
        // clicked in the grid row.
        function OnSelectClick(ctrl) {
            var nameCtrl = '<%= Request.QueryString["namectrl"] %>';
            var emailCtrl = '<%= Request.QueryString["emailctrl"] %>';
            var idCtrl = '<%= Request.QueryString["idctrl"] %>';
            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Set the user name field value. Replace the 'link button name' with the 
            // 'user name hidden field name' and retrieve the value.
            if (nameCtrl != null && nameCtrl != '') {
                window.opener.document.getElementById(nameCtrl).value
                    = document.getElementById(ctrl.id.replace("SearchInterviewQuestion_selectLinkButton", "SearchInterviewQuestion_userNameHiddenfield")).value;
            }

            // Set the email hidden field value. Replace the 'link button name' with the 
            // 'eamil hidden field name' and retrieve the value.
            if (emailCtrl != null && emailCtrl != '') {
                window.opener.document.getElementById(emailCtrl).value
                    = document.getElementById(ctrl.id.replace("SearchInterviewQuestion_selectLinkButton", "SearchInterviewQuestion_emailHiddenfield")).value;
            }

            // Set the user ID hidden field value. Replace the 'link button name' with the 
            // 'user ID hidden field name' and retrieve the value.
            if (idCtrl != null && idCtrl != '') {
                window.opener.document.getElementById(idCtrl).value
                    = document.getElementById(ctrl.id.replace("SearchInterviewQuestion_selectLinkButton", "SearchInterviewQuestion_userIDHiddenfield")).value;
            }
            self.close();
        }

    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10" colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="SearchInterviewQuestions_headerLiteral" runat="server" Text="Search Interview Question"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SearchInterviewQuestion_closeImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="SearchInterviewQuestion_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="SearchInterviewQuestion_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchInterviewQuestion_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                            ID="SearchInterviewQuestion_stateHiddenField" runat="server" Value="0" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div id="SearchInterviewQuestion_searchCriteriasDiv" runat="server" style="display: block;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="right" class="td_padding_bottom_5">
                                            <asp:UpdatePanel runat="server" ID="SearchInterviewQuestion_simpleLinkUpdatePanel">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="SearchInterviewQuestion_simpleLinkButton" runat="server" Text="Advanced"
                                                        SkinID="sknActionLinkButton" OnClick="SearchInterviewQuestion_simpleLinkButton_Click"
                                                        Visible="false" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="panel_bg">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="100%">
                                                        <asp:UpdatePanel runat="server" ID="SearchInterviewQuestion_SearchDivUpdatePanel">
                                                            <ContentTemplate>
                                                                <div id="SearchInterviewQuestion_simpleSearchDiv" runat="server">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="SearchInterviewQuestion_categoryHeadLabel" runat="server" Text="Category"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="SearchInterviewQuestion_categoryTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="SearchInterviewQuestion_subjectHeadLabel" runat="server" Text="Subject"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="SearchInterviewQuestion_subjectTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="SearchInterviewQuestion_keywordsHeadLabel" runat="server" Text="Keyword"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <div style="float: left; padding-right: 5px;">
                                                                                    <asp:TextBox ID="SearchInterviewQuestion_keywordsTextBox" runat="server" MaxLength="100"></asp:TextBox></div>
                                                                                <div style="float: left;">
                                                                                    <asp:ImageButton ID="SearchInterviewQuestion_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" /></div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div id="SearchInterviewQuestion_advanceSearchDiv" runat="server" style="width: 100%"
                                                                    visible="false">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <uc1:CategorySubjectControl ID="SearchInterviewQuestion_searchCategorySubjectControl"
                                                                                    runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_8">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="panel_inner_body_bg">
                                                                                <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="SearchInterviewQuestion_testAreaHeadLabel" runat="server" Text="Interview Area"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="7" class="checkbox_list_bg" align="left" style="width: 90%">
                                                                                            <asp:CheckBoxList ID="SearchInterviewQuestion_testAreaCheckBoxList" runat="server"
                                                                                                RepeatColumns="3" RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                            </asp:CheckBoxList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="SearchInterviewQuestion_complexityLabel" runat="server" Text="Complexity"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="3" align="left" valign="middle">
                                                                                            <div style="float: left; padding-right: 5px; width: 90%; vertical-align: middle;"
                                                                                                class="checkbox_list_bg">
                                                                                                <asp:CheckBoxList ID="SearchInterviewQuestion_complexityCheckBoxList" runat="server"
                                                                                                    RepeatColumns="3" RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                </asp:CheckBoxList>
                                                                                            </div>
                                                                                            <div style="float: left; vertical-align: middle; padding-left: 5px; padding-top: 5px;">
                                                                                                <asp:ImageButton ID="SearchInterviewQuestion_complexityImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, ComplexityHelp %>" />
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchInterviewQuestion_keywordHeadLabel" runat="server" Text="Keyword"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="3">
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="SearchInterviewQuestion_keywordTextBox" runat="server" Columns="50"
                                                                                                    MaxLength="100"></asp:TextBox></div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="SearchInterviewQuestion_keywordImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, KeywordHelp %>" />
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchInterviewQuestion_questionLabel" runat="server" Text="Question ID"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="SearchInterviewQuestion_questionIDTextBox" runat="server" MaxLength="15"></asp:TextBox>
                                                                                        </td>
                                                                                        <%--  <td>
                                                                                            <asp:Label ID="SearchInterviewQuestion_creditHeadLabel" runat="server" Text="Credit Earned (in $)"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="SearchInterviewQuestion_creditTextBox" runat="server"></asp:TextBox>
                                                                                            <ajaxToolKit:MaskedEditExtender ID="Options_simpleMaskedEditExtender" runat="server"
                                                                                                TargetControlID="SearchInterviewQuestion_creditTextBox" Mask="99.99" MessageValidatorTip="true"
                                                                                                InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                                MaskType="Number" AcceptNegative="None" ErrorTooltipEnabled="True" />
                                                                                        </td>--%>
                                                                                        <td style="width: 8%">
                                                                                            <asp:Label ID="SearchInterviewQuestion_statusHeadLabel" runat="server" Text="Status"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:DropDownList ID="SearchInterviewQuestion_statusDropDownList" runat="server"
                                                                                                    Width="132px">
                                                                                                    <asp:ListItem Text="All" Value=""></asp:ListItem>
                                                                                                    <asp:ListItem Text="Active" Value="Y"></asp:ListItem>
                                                                                                    <asp:ListItem Text="Inactive" Value="N"></asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="SearchInterviewQuestion_statusImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, StatusHelp %>" /></div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchInterviewQuestion_authorHeadLabel" runat="server" Text="Author"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="SearchInterviewQuestion_authorTextBox" runat="server"></asp:TextBox>
                                                                                                <asp:HiddenField ID="SearchInterviewQuestion_dummyAuthorID" runat="server" />
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="SearchInterviewQuestion_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                                    runat="server" ImageAlign="Middle" ToolTip="Click here to select the question author" /></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="td_padding_top_5">
                                                        <asp:Button ID="SearchInterviewQuestion_topSearchButton" runat="server" Text="Search"
                                                            SkinID="sknButtonId" OnClick="SearchInterviewQuestion_topSearchButton_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr id="SearchInterviewQuestion_SearchInterviewQuestionResultsTR" runat="server">
                        <td class="header_bg">
                            <asp:UpdatePanel ID="SearchInterviewQuestion_expandAllUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 50%" align="left" class="header_text_bold">
                                                <asp:Literal ID="SearchInterviewQuestion_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                    ID="SearchInterviewQuestion_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                    Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                            </td>
                                            <td style="width: 48%" align="left">
                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                    <tr>
                                                        <td>
                                                            <asp:HiddenField ID="SearchInterviewQuestion_stateExpandHiddenField" runat="server"
                                                                Value="0" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 2%" align="right">
                                                <span id="SearchInterviewQuestion_searchResultsUpSpan" runat="server" style="display: none;">
                                                    <asp:Image ID="SearchInterviewQuestion_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                        id="SearchInterviewQuestion_searchResultsDownSpan" runat="server" style="display: block;"><asp:Image
                                                            ID="SearchInterviewQuestion_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                <asp:HiddenField ID="SearchInterviewQuestion_restoreHiddenField" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <asp:UpdatePanel runat="server" ID="SearchInterviewQuestion_questionGridViewUpdatePanel">
                                <ContentTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left">
                                                <div style="height: 300px; overflow: auto;" runat="server" id="SearchInterviewQuestion_questionDiv"
                                                    visible="false">
                                                    <asp:GridView ID="SearchInterviewQuestion_questionGridView" runat="server" OnRowCommand="SearchInterviewQuestion_questionGridView_RowCommand"
                                                        OnSorting="SearchInterviewQuestion_questionGridView_Sorting" OnRowDataBound="SearchInterviewQuestion_questionGridView_RowDataBound"
                                                        OnRowCreated="SearchInterviewQuestion_questionGridView_RowCreated">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="12%" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="SearchInterviewQuestion_editImageButton" runat="server" SkinID="sknEditImageButton"
                                                                        ToolTip="Edit Question" CommandArgument='<%# Eval("QuestionKey") %>' CommandName="editquestion" />
                                                                    <asp:ImageButton ID="SearchInterviewQuestion_activeImageButton" runat="server" SkinID="sknInactiveImageButton"
                                                                        Visible="false" ToolTip="Deactivate Question" CommandName="inactive" CommandArgument='<%# Eval("QuestionKey") %>' />
                                                                    <asp:ImageButton ID="SearchInterviewQuestion_inactiveImageButton" runat="server"
                                                                        SkinID="sknActiveImageButton" ToolTip="Activate Question" Visible="false" CommandName="active"
                                                                        CommandArgument='<%# Eval("QuestionKey") %>' />
                                                                    <asp:ImageButton ID="SearchInterviewQuestion_questionUsageSummaryImageButton" runat="server"
                                                                        SkinID="sknDashboardImageButton" ToolTip="Question Summary" OnClientClick=<%# "OpenUsageSummary('" + Eval("QuestionKey") +"');return false;"%> />
                                                                    <asp:ImageButton ID="SearchInterviewQuestion_questionInfoImageButton" runat="server"
                                                                        ToolTip="Question Info" SkinID="sknPreviewImageButton" CommandName="view" CommandArgument='<%# Eval("QuestionKey") %>' />
                                                                    <asp:ImageButton ID="SearchInterviewQuestion_deleteImageButton" runat="server" SkinID="sknDeleteImageButton"
                                                                        ToolTip="Delete Question" CommandName="deletes" CommandArgument='<%# Eval("QuestionKey") %>' /><asp:HiddenField
                                                                            ID="SearchInterviewQuestion_statusHiddenField" runat="server" Value='<%# Eval("Status") %>' />
                                                                    <asp:HiddenField ID="TestIncludedHiddenField" runat="server" Value='<%# Eval("TestIncluded") %>' />
                                                                    <asp:HiddenField ID="DataAccessRightsHiddenField" runat="server" Value='<%# Eval("DataAccessRights") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Select" HeaderStyle-Width="20px">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="SearchInterviewQuestion_selectCheckbox" runat="server" Width="15px" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Question ID" SortExpression="QUESTIONKEY" HeaderStyle-Width="75px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="SearchInterviewQuestion_questionIdLinkButton" Visible="false"
                                                                        runat="server" Text='<%# Eval("QuestionKey") %>'></asp:LinkButton>
                                                                    <asp:Label ID="SearchInterviewQuestion_questionIdLabel" runat="server" Text='<%# Eval("QuestionKey") %>'></asp:Label>
                                                                    <asp:HiddenField ID="SearchInterviewQuestion_complexityIdHiddenField" runat="server"
                                                                        Value='<%# Eval("ComplexityID") %>' />
                                                                    <asp:HiddenField ID="HiddenFSearchInterviewQuestion_complexityHiddenFieldield" runat="server"
                                                                        Value='<%# Eval("Complexity") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Question" SortExpression="QUESTIONDESC" HeaderStyle-Width="250px">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="SearchInterviewQuestion_questionGridLiteral" runat="server" Text='<%# Eval("Question") %>'></asp:Literal>
                                                                    <asp:HiddenField ID="SearchInterviewQuestion_questGridHiddenfield" runat="server"
                                                                        Value='<%# Eval("Question") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Complexity" DataField="Complexity" HeaderStyle-Width="80px"
                                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" SortExpression="COMPLEXITYNAME">
                                                            </asp:BoundField>
                                                            <asp:BoundField HeaderText="Status" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                                                HeaderStyle-VerticalAlign="Middle" DataField="Status" SortExpression="STATUS" />
                                                            <asp:TemplateField HeaderText="Modified Date" SortExpression="MODIFIEDDATE" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SearchInterviewQuestion_modifiedDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.QuestionDetail)Container.DataItem).ModifiedDate) %>'
                                                                        Width="100px"></asp:Label>
                                                                    <tr>
                                                                        <td class="grid_padding_right" colspan="7">
                                                                            <div id="SearchInterviewQuestion_detailsDiv" runat="server" style="display: none;"
                                                                                class="table_outline_bg">
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="popup_question_icon" style="width: 35%" colspan="4">
                                                                                            <asp:Label ID="SearchInterviewQuestion_questionLabel" runat="server" Text='<%# Eval("Question").ToString()%>'
                                                                                                Width="810px"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4">
                                                                                            <div style="height: 60px; overflow: auto; width: 100%">
                                                                                                <asp:PlaceHolder ID="SearchInterviewQuestion_answerChoicesPlaceHolder" runat="server">
                                                                                                </asp:PlaceHolder>
                                                                                                <asp:HiddenField ID="CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="SearchInterviewQuestion_testAreaHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                Text="Interview Area"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 40%">
                                                                                            <asp:Label ID="SearchInterviewQuestion_testAreaTextBox" runat="server" ReadOnly="true"
                                                                                                SkinID="sknLabelFieldText" Text='<%# Eval("TestAreaName") %>'></asp:Label>
                                                                                            <asp:HiddenField ID="SearchInterviewQuestion_testAreaIdHiddenfield" runat="server"
                                                                                                Value='<%# Eval("TestAreaID") %>' />
                                                                                        </td>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="SearchInterviewQuestion_complexityHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                Text="Complexity"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="SearchInterviewQuestion_complexityTextBox" runat="server" ReadOnly="true"
                                                                                                SkinID="sknLabelFieldText" Text='<%# Eval("Complexity") %>'></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                            <%-- popup DIV --%>
                                                                            <a href="#SearchInterviewQuestion_focusmeLink" id="SearchInterviewQuestion_focusDownLink"
                                                                                runat="server"></a><a href="#" id="SearchInterviewQuestion_focusmeLink" runat="server">
                                                                                </a>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <uc2:PageNavigator ID="SearchInterviewQuestion_bottomPagingNavigator" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="SearchInterviewQuestion_topSearchButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align" runat="server" visible="false">
                <asp:UpdatePanel runat="server" ID="SearchInterviewQuestion_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="SearchInterviewQuestion_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchInterviewQuestion_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="right">
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="SearchInterviewQuestion_AddToQuestionsButton" runat="server" SkinID="sknButtonId"
                                Text="Add Questions" OnClick="SearchInterviewQuestion_AddToQuestionsButton_Click" />
                        </td>
                        <td>
                            <asp:LinkButton ID="SearchInterviewQuestion_AddToQuestionsCancel" runat="server"
                                SkinID="sknPopupLinkButton" Text="Cancel" OnClientClick="javascript:window.close()"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="SearchInterviewQuestion_modelPopupUpdatePanel">
                    <ContentTemplate>
                        <asp:Panel ID="SearchInterviewQuestion_questionPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_question_detail">
                            <div style="display: none">
                                <asp:Button ID="SearchInterviewQuestion_hiddenButton" runat="server" Text="Hidden" /></div>
                            <uc4:QuestionDetailPreviewControl ID="SearchInterviewQuestion_QuestionDetailPreviewControl"
                                runat="server" OnCancelClick="SearchInterviewQuestion_cancelClick" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="SearchInterviewQuestion_questionModalPopupExtender"
                            runat="server" PopupControlID="SearchInterviewQuestion_questionPanel" TargetControlID="SearchInterviewQuestion_hiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                        <asp:Panel ID="SearchInterviewQuestion_ConfirmPopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc3:ConfirmMsgControl ID="SearchInterviewQuestiont_confirmPopupExtenderControl"
                                runat="server" OnOkClick="SearchInterviewQuestiont_okClick" OnCancelClick="SearchInterviewQuestion_cancelClick" />
                        </asp:Panel>
                        <div id="SearchInterviewQuestion_hiddenDIV" runat="server" style="display: none">
                            <asp:Button ID="SearchInterviewQuestion_hiddenPopupModalButton" runat="server" />
                        </div>
                        <ajaxToolKit:ModalPopupExtender ID="SearchInterviewQuestion_ConfirmPopupExtender"
                            runat="server" PopupControlID="SearchInterviewQuestion_ConfirmPopupPanel" TargetControlID="SearchInterviewQuestion_hiddenPopupModalButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                        <asp:HiddenField ID="SearchInterviewQuestion_QuestionKey" runat="server" />
                        <asp:HiddenField ID="SearchInterviewQuestion_Action" runat="server" />
                        <asp:HiddenField ID="SearchInterviewQuestion_authorIdHiddenField" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
