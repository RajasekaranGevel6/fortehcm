﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EmailAssessor.aspx.cs
// File that represents the user interface layout and functionalities for
// the EmailAssessor page. This helps in sending emails to the assessor 
// along with the requested dates and time slots. 

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Configuration;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the EmailAssessor page. This helps in sending emails to the assessor 
    /// along with the requested dates and time slots. This class inherits
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class EmailAssessor : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Email Assessor");

                Response.Cache.SetCacheability(HttpCacheability.NoCache);

                // Set default button field
                Page.Form.DefaultButton = EmailAssessor_sendButton.UniqueID;

                // Validate email address.
                EmailAssessor_sendButton.Attributes.Add("onclick", "javascript:return IsValidEmails('"
                    + EmailAssessor_toTextBox.ClientID + "','"
                    + EmailAssessor_ccTextBox.ClientID + "','"
                    + EmailAssessor_errorMessageLabel.ClientID + "','"
                    + EmailAssessor_successMessageLabel.ClientID + "');");
                
                // Add handler to open 'select contact' popup.
                string positionProfileID = string.Empty;
                if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                    positionProfileID = Request.QueryString["positionprofileid"].Trim();

                EmailAssessor_toAddressImageButton.Attributes.Add
                    ("onclick", "javascript:return ShowSelectContactPopup('" +
                    EmailAssessor_toTextBox.ClientID + "','" + positionProfileID + "','To','N')");

                EmailAssessor_ccAddressImageButton.Attributes.Add
                   ("onclick", "javascript:return ShowSelectContactPopup('" +
                   EmailAssessor_ccTextBox.ClientID + "','" + positionProfileID + "','CC','N')");

                if (!IsPostBack)
                {
                    // Set default focus field
                    Page.Form.DefaultFocus = EmailAssessor_toTextBox.UniqueID;
                    EmailAssessor_toTextBox.Focus();

                    EmailAssessor_fromValueLabel.Text = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                    EmailAssessor_fromHiddenField.Value = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];

                    if (!Utility.IsNullOrEmpty(Request.QueryString["from"]) &&
                        Request.QueryString["from"] == "RR_PRO")
                    {
                        // Set browser title.
                        Master.SetPageCaption("Email Recruiter");
                        EmailAssessor_headerLiteral.Text = "Email Recruiter";

                        // Check if recruiter ID is passed.
                        if (Utility.IsNullOrEmpty(Request.QueryString["recruiterid"]))
                        {
                            base.ShowMessage(EmailAssessor_errorMessageLabel, "Recruiter ID is not passed");
                            return;
                        }

                        // Get recruiter ID.
                        int recruiterID = 0;
                        int.TryParse(Request.QueryString["recruiterid"], out recruiterID);

                        // Check if recruiter ID is valid.
                        if (recruiterID == 0)
                        {
                            base.ShowMessage(EmailAssessor_errorMessageLabel, "Recruiter ID is invalid");
                            return;
                        }

                        // Get recruiter detail.
                        RecruiterDetail recruiterDetail = new PositionProfileBLManager().
                            GetRecruiterByID(recruiterID);

                        if (recruiterDetail == null)
                        {
                            base.ShowMessage(EmailAssessor_errorMessageLabel, "No such recruiter exist");
                            return;
                        }

                        // Assign to address.
                        EmailAssessor_toTextBox.Text = recruiterDetail.Email;
                    }
                    else
                    {
                        // Check if assessor ID is passed.
                        if (Utility.IsNullOrEmpty(Request.QueryString["assessorid"]))
                        {
                            base.ShowMessage(EmailAssessor_errorMessageLabel, "Assessor ID is not passed");
                            return;
                        }

                        // Get assessor ID.
                        int assessorID = 0;
                        int.TryParse(Request.QueryString["assessorid"], out assessorID);

                        // Check if assessor ID is valid.
                        if (assessorID == 0)
                        {
                            base.ShowMessage(EmailAssessor_errorMessageLabel, "Assessor ID is invalid");
                            return;
                        }

                        // Get assessor detail.
                        AssessorDetail assessorDetail = new AssessorBLManager().GetAssessorByUserId(assessorID);

                        if (assessorDetail == null)
                        {
                            base.ShowMessage(EmailAssessor_errorMessageLabel, "No such assessor exist");
                            return;
                        }

                        // Assign to to and cc.
                        EmailAssessor_toTextBox.Text = assessorDetail.UserEmail;
                        EmailAssessor_ccTextBox.Text = assessorDetail.AlternateEmailID;

                        if (Request.QueryString["from"] != null &&
                            Request.QueryString["from"].Trim().ToUpper() == "REQ")
                        {
                            // Compose time slots email.
                            ComposeTimeSlotsRequestEmail(assessorDetail);
                        }
                        else
                        {
                            // Compose general email.
                            ComposeGeneralEmail(assessorDetail);
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EmailAssessor_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will be called when the send button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void EmailAssessor_sendButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear all the error and success labels
                EmailAssessor_errorMessageLabel.Text = string.Empty;
                EmailAssessor_successMessageLabel.Text = string.Empty;

                // Validate the fields
                if (!IsValidData())
                    return;

                // Build mail detail.
                MailDetail mailDetail = new MailDetail();
                mailDetail.To = new List<string>();
                mailDetail.CC = new List<string>();

                // Add the logged in user in the CC list when 'send me a copy ' is on
                if (EmailAttachement_sendMeACopyCheckBox.Checked)
                {
                    mailDetail.CC.Add(base.userEmailID);
                }

                // Set to address.
                string[] toMailIDs = EmailAssessor_toTextBox.Text.Split(new char[] { ',' });
                for (int idx = 0; idx < toMailIDs.Length; idx++)
                    mailDetail.To.Add(toMailIDs[idx].Trim());

                // Set CC address.
                if (!Utility.IsNullOrEmpty(EmailAssessor_ccTextBox.Text.Trim()))
                {
                    string[] ccMailIDs = EmailAssessor_ccTextBox.Text.Split(new char[] { ',' });
                    for (int idx = 0; idx < ccMailIDs.Length; idx++)
                        mailDetail.CC.Add(ccMailIDs[idx].Trim());
                }

                // Set from address.
                mailDetail.From = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                
                // Set subject.
                mailDetail.Subject = EmailAssessor_subjectTextBox.Text.Trim();

                // Set message.
                mailDetail.Message = EmailAssessor_messageTextBox.Text.Trim();
                mailDetail.isBodyHTML = true;
                bool isMailSent = false;

                try
                {
                    // Send mail.
                    new EmailHandler().SendMail(mailDetail);
                    isMailSent = true;
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    isMailSent = false;
                }
                //Script that close the popup window
                string closeScript = "self.close();";

                if (isMailSent)
                {
                    //base.ShowMessage(EmailAssessor_successMessageLabel,
                    //    Resources.HCMResource.EmailAssessor_EmailSent);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true); 
                }
                else
                {
                    base.ShowMessage(EmailAssessor_errorMessageLabel,
                        Resources.HCMResource.EmailAssessor_MailCouldNotSend);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EmailAssessor_errorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            bool isMandatoryMissing = false;

            // Validate to address field.
            if (EmailAssessor_toTextBox.Text.Trim().Length == 0)
            {
                isMandatoryMissing = true;
                isValidData = false;
            }
            
            // Validate message field.
            if (EmailAssessor_messageTextBox.Text.Trim().Length == 0)
            {
                isMandatoryMissing = true;
                isValidData = false;
            }

            if (isMandatoryMissing)
            {
                base.ShowMessage(EmailAssessor_errorMessageLabel,
                   Resources.HCMResource.EmailAssessor_MandatoryCannotBeEmpty);
            }

            return isValidData;
        }

        /// <summary>
        /// Overridden protected method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that compose the time slots request email.
        /// </summary>
        /// <param name="assessorDetail">
        /// A <see cref="AssessorDetail"/> that holds the assessor detail.
        /// </param>
        private void ComposeTimeSlotsRequestEmail(AssessorDetail assessorDetail)
        {
            // Get the selected time slots from the session.
            List<AssessorTimeSlotDetail> timeSlots = null;
            if (Session["AVAILABILITY_REQUEST_LIST_TEMP"] != null)
            {
                timeSlots = Session["AVAILABILITY_REQUEST_LIST_TEMP"] as List<AssessorTimeSlotDetail>;
            }

            if (timeSlots == null)
            {
                base.ShowMessage(EmailAssessor_errorMessageLabel, "No requested time slots found for the assessor");
                return;
            }

            // Check if selected time slots found for the assessor.
            var filteredSlots = from n in timeSlots
                                where n.AssessorID == assessorDetail.UserID
                                select n;

            if (filteredSlots == null || filteredSlots.Count() == 0)
            {
                base.ShowMessage(EmailAssessor_errorMessageLabel, "No selected time slots found for the assessor");
                return;
            }

            EmailAssessor_subjectTextBox.Text = "Request for time slot to assess interview";

            string message = string.Empty;

            message += "Dear " + assessorDetail.FirstName + ",";
            message += Environment.NewLine;
            message += Environment.NewLine;

            message += "The following time slots has been requested to assess interview with the corresponding skills";
            message += Environment.NewLine;
            message += "---------------------------------------------------------------------------------";
            message += Environment.NewLine;

            int row = 1;
            // Loop through the filtered time slots and compose the mail.
            foreach (AssessorTimeSlotDetail timeSlot in filteredSlots.ToList())
            {
                message += row++ + ". " + timeSlot.Skill + " - " + timeSlot.AvailabilityDateString + "(" + timeSlot.TimeSlot + ")";
                message += Environment.NewLine;
            }

            message += "---------------------------------------------------------------------------------";
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += "Review the details and process the request";

            message += Environment.NewLine;
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += "This is a system generated message";
            message += Environment.NewLine;
            message += "Do not reply to this message";
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += "============================== Disclaimer ==============================";
            message += Environment.NewLine;
            message += "This message is for the designated recipient only and may contain privileged, proprietary, or ";
            message += Environment.NewLine;
            message += "otherwise private information. If you have received it in error, please notify the sender";
            message += Environment.NewLine;
            message += "immediately and delete the original. Any other use of the email by you is prohibited.";
            message += Environment.NewLine;
            message += "=========================== End Of Disclaimer ============================";

            EmailAssessor_messageTextBox.Text = message;
        }

        /// <summary>
        /// Method that compose the general email.
        /// </summary>
        /// <param name="assessorDetail">
        /// A <see cref="AssessorDetail"/> that holds the assessor detail.
        /// </param>
        private void ComposeGeneralEmail(AssessorDetail assessorDetail)
        {
            string message = string.Empty;

            message += "Dear " + assessorDetail.FirstName + ",";
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += "This is a system generated message";
            message += Environment.NewLine;
            message += "Do not reply to this message";
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += "============================== Disclaimer ==============================";
            message += Environment.NewLine;
            message += "This message is for the designated recipient only and may contain privileged, proprietary, or ";
            message += Environment.NewLine;
            message += "otherwise private information. If you have received it in error, please notify the sender";
            message += Environment.NewLine;
            message += "immediately and delete the original. Any other use of the email by you is prohibited.";
            message += Environment.NewLine;
            message += "=========================== End Of Disclaimer ============================";

            EmailAssessor_messageTextBox.Text = message;
        }


        #endregion Private Methods
    }
}