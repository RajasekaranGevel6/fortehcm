﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddQuestionImage.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.AddQuestionImage" %>

<asp:Content ID="AddQuestionImage_ContentID" ContentPlaceHolderID="PopupMaster_contentPlaceHolder"
    runat="server">
    <script type="text/javascript" language="javascript">
        // This function helps to close the window
        function CloseMe() {

            self.close();
            return false;
        }
        function DisplayImageName() {

            var ctrlDisplayField = '<%= Request.QueryString["ctrlId"] %>';
            var imagePath = '<%= Request.QueryString["ctrlId"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Set the display field value. 
            if (ctrlDisplayField != null && ctrlDisplayField != '') {
                window.opener.document.getElementById(ctrlDisplayField).innerHTML
                    = window.opener.document.getElementById(ctrlDisplayField.replace("BatchQuestionEntry_addQuestionImageLinkButton", "BatchQuestionEntry_qstImageHiddenField")).value;
            }
            self.close();
            return false;
        }
     
    </script>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="width: 50%" class="popup_header_text_grey" align="left">
                                        <asp:Literal ID="AddQuestionImage_headerLiteral" Text="Add Question Image" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <asp:ImageButton ID="AddQuestionImage_closeImageButton" runat="server" SkinID="sknCloseImageButton"
                                            OnClientClick="javascript:CloseMe();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td valign="top" class="popup_td_padding_10">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:Label ID="AddQuestionImage_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                        <asp:Label ID="AddQuestionImage_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            <asp:RegularExpressionValidator ID="AddQuestionImage_regularExpressionValidataor"
                                                runat="server" ControlToValidate="AddQuestionImage_fileUpload" ErrorMessage="Only image files are allowed"
                                                ValidationExpression="^.+\.((jpg|png|bmp|jpeg))$">
                                            </asp:RegularExpressionValidator>
                                        </div>
                                        <div>
                                            <asp:RequiredFieldValidator ID="AddQuestionImage_requiredFieldValidator" runat="server"
                                                ControlToValidate="AddQuestionImage_fileUpload" ErrorMessage="Select image file"></asp:RequiredFieldValidator>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center">
                                        <asp:FileUpload runat="server" ID="AddQuestionImage_fileUpload" Width="100%" />
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td colspan="4">
                                        * JPG, GIF, PNG files are supported<br />
                                        * Maximum of 100KB can be uploaded
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td align="left">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left">
                                        <asp:Button ID="AddQuestionImage_uploadButton" runat="server" Text="Upload" SkinID="sknButtonId"
                                            OnClick="AddQuestionImage_uploadButtonClick" />
                                    </td>
                                    <td align="left">
                                        &nbsp;&nbsp;<asp:LinkButton ID="AddQuestionImage_cancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknPopupLinkButton" OnClientClick="javascript:CloseMe();"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
