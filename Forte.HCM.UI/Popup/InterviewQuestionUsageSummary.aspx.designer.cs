//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.Popup {
    
    
    public partial class InterviewQuestionUsageSummary {
        
        /// <summary>
        /// InterviewQuestionUsageSummary_headerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal InterviewQuestionUsageSummary_headerLiteral;
        
        /// <summary>
        /// InterviewQuestionUsageSummary_topCloseImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton InterviewQuestionUsageSummary_topCloseImageButton;
        
        /// <summary>
        /// InterviewQuestionUsageSummary_messageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel InterviewQuestionUsageSummary_messageUpdatePanel;
        
        /// <summary>
        /// InterviewQuestionUsageSummary_successMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewQuestionUsageSummary_successMessageLabel;
        
        /// <summary>
        /// InterviewQuestionUsageSummary_errorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewQuestionUsageSummary_errorMessageLabel;
        
        /// <summary>
        /// InterviewQuestionUsageSummary_questionKeyLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewQuestionUsageSummary_questionKeyLabel;
        
        /// <summary>
        /// InterviewQuestionUsageSummary_questionKeyValueLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewQuestionUsageSummary_questionKeyValueLabel;
        
        /// <summary>
        /// InterviewQuestionUsageSummary_searchTestResultsTR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow InterviewQuestionUsageSummary_searchTestResultsTR;
        
        /// <summary>
        /// Td1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableCell Td1;
        
        /// <summary>
        /// InterviewQuestionUsageSummary_searchResultsLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal InterviewQuestionUsageSummary_searchResultsLiteral;
        
        /// <summary>
        /// InterviewQuestionUsageSummary_sortHelpLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label InterviewQuestionUsageSummary_sortHelpLabel;
        
        /// <summary>
        /// InterviewQuestionUsageSummary_questionsDetailsUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel InterviewQuestionUsageSummary_questionsDetailsUpdatePanel;
        
        /// <summary>
        /// InterviewQuestionUsageSummary_questionsDetailsDIV control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl InterviewQuestionUsageSummary_questionsDetailsDIV;
        
        /// <summary>
        /// InterviewQuestionUsageSummary_questionDetailsGridView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView InterviewQuestionUsageSummary_questionDetailsGridView;
        
        /// <summary>
        /// InterviewQuestionUsageSummary_pageNavigator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.PageNavigator InterviewQuestionUsageSummary_pageNavigator;
        
        /// <summary>
        /// InterviewQuestionUsageSummary_bottomCloseLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton InterviewQuestionUsageSummary_bottomCloseLinkButton;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.PopupMaster Master {
            get {
                return ((Forte.HCM.UI.MasterPages.PopupMaster)(base.Master));
            }
        }
    }
}
