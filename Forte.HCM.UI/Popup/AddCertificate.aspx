﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCertificate.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Admin.AddCertificate" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="AddCertificate_contentID" ContentPlaceHolderID="PopupMaster_contentPlaceHolder"
    runat="server">

    <script type="text/javascript" language="javascript">
        // This function helps to close the window
        function CloseMe() {
            self.close();
            return false;
        }

        // Function calls when user clicks on the available backgrounds linkbutton.
        function CertificateBackgroundModalPopup() {
            $find("<%= AddCertificate_previewCertificates_modalpPopupExtender.ClientID %>").show();
            return false;
        }
       
    </script>

    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <asp:HiddenField ID="AddCertificate_isMaximizedHiddenField" runat="server" />
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="width: 50%" class="popup_header_text_grey" align="left">
                                        <asp:Literal ID="AddCertificate_headerLiteral" Text="Add Certificate" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <asp:ImageButton ID="AddCertificate_closeImageButton" runat="server" SkinID="sknCloseImageButton"
                                            OnClientClick="javascript:CloseMe();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td valign="top" class="popup_td_padding_10">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:Label ID="AddCertificate_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                        <asp:Label ID="AddCertificate_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="100%" cellpadding="2" cellspacing="5" border="0">
                                            <tr>
                                                <td align="left" style="width: 20%;">
                                                    <asp:Label ID="AddCertificate_certificateNameHeadLabel" runat="server" Text="Certificate Name"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">&nbsp;*</span>
                                                </td>
                                                <td align="left" style="width: 25%;">
                                                    <asp:TextBox ID="AddCertificate_certificateNameTextbox" runat="server" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td align="left" style="width: 15%;">
                                                    <asp:Label ID="AddCertificate_descriptionHeadLabel" runat="server" Text="Description"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">&nbsp;*</span>
                                                </td>
                                                <td align="left" style="width: 40%;">
                                                    <asp:TextBox ID="AddCertificate_descriptionTextBox" runat="server" SkinID="sknMultiLineTextBox"
                                                        MaxLength="100" Width="240px" onkeyup="CommentsCount(100,this)" onchange="CommentsCount(100,this)"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="AddCertificate_certificateBackgroundLabel" runat="server" Text="Certificate Background"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>&nbsp;
                                                </td>
                                                <td colspan="3">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="left">
                                                                <div style="float: left; padding-right: 5px;">
                                                                    <asp:FileUpload ID="AddCertificate_certificateBackgroundFileUpload" runat="server" />
                                                                    <asp:ImageButton ID="BatchQuestionEntry_helpImageButton" SkinID="sknHelpImageButton"
                                                                        runat="server" OnClientClick="javascript:return false;" ToolTip="Click here to select image file (png, gif or jpg) for background" />
                                                                </div>
                                                                <div>
                                                                    <asp:Button ID="AddCertificate_uploadButton" SkinID="sknButtonId" runat="server"
                                                                        Text="Upload" OnClick="AddCertificate_uploadButton_Click" />
                                                                    <span style="float: right;">
                                                                        <asp:LinkButton ID="AddCertificate_availableBackgroundLinkButton" runat="server"
                                                                            Text="Available Backgrounds" ToolTip="Click here to select available backgrounds"
                                                                            SkinID="sknActionLinkButton" OnClientClick="javascript:return CertificateBackgroundModalPopup();">
                                                                        </asp:LinkButton>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <table border="0" width="100%">
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <div id="AddCertificate_htmlTextboxDIV" runat="server" style="display: block;">
                                                                    <asp:TextBox Width="99%" Height="170px" ID="AddCertificate_htmlTextbox" runat="server"
                                                                        SkinID="sknMultiLineTextBox" TextMode="MultiLine"></asp:TextBox>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <table>
                                                                    <tr>
                                                                        <td class="help_button">
                                                                            <a href="#">Help</a>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Button ID="AddCertificate_previewButton" Text="Preview" runat="server" SkinID="sknButtonId"
                                                                                OnClick="AddCertificate_previewButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <div id="AddCertificate_previewDIV" style="text-align: center; width: 690px; height: 200px;
                                                                    overflow: auto; display: block;" runat="server">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td align="left">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left">
                                        <asp:Button ID="AddCertificate_submitButton" runat="server" Text="Save" SkinID="sknButtonId"
                                            OnClick="AddCertificate_submitButton_Click" />
                                    </td>
                                    <td align="left">
                                        &nbsp;&nbsp;<asp:LinkButton ID="AddCertificate_cancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknPopupLinkButton" OnClientClick="javascript:CloseMe();"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="AddCertificate_previewCertificatesPanel" runat="server" CssClass="popupcontrol_available_backgrounds"
                    Style="display: none">
                    <div style="display: none">
                        <asp:Button ID="AddCertificate_previewCertificates_Button" runat="server" />
                    </div>
                    <table width="100%" border="0" cellspacing="2" cellpadding="2">
                        <tr>
                            <td class="td_height_8">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                            <asp:Literal ID="AddCertificate_previewPanel_headerLiteral" runat="server" Text="Available Backgrounds"></asp:Literal>
                                        </td>
                                        <td style="width: 50%" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                <tr>
                                                    <td>
                                                        <asp:ImageButton ID="AddCertificate_previewPanel_closeImageButton" runat="server"
                                                            SkinID="sknCloseImageButton" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_20" colspan="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupcontrol_question_inner_bg" align="center" colspan="2">
                                            <table border="0" cellpadding="2" cellspacing="2" width="90%" class="tab_body_bg"
                                                align="left">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox runat="server" TextMode="MultiLine" Width="270px" Height="100px" ID="AddCertificate_availableBackgroundTextbox"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table>
                                                <tr>
                                                    <td class="td_height_8">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_8" colspan="2">
                                        </td>
                                    </tr>
                                    <tr align="left">
                                        <td colspan="2">
                                            <asp:LinkButton ID="AddCertificate_previewCertificates_cancelLinkButton" runat="server"
                                                Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="AddCertificate_previewCertificates_modalpPopupExtender"
                    runat="server" TargetControlID="AddCertificate_previewCertificates_Button" PopupControlID="AddCertificate_previewCertificatesPanel"
                    BackgroundCssClass="modalBackground" CancelControlID="AddCertificate_previewCertificates_cancelLinkButton">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
        <%--<tr>
            <td valign="top">
                <asp:Panel ID="AddCertificate_previewCertificate_Panel" runat="server" Style="display: none">
                    <div style="display: none">
                        <asp:Button ID="AddCertificate_previewCertificate_Panel_hiddenButton" runat="server" />
                    </div>
                    <table width="100%" border="0" cellspacing="2" cellpadding="2" class="popupcontrol_preview_certificate">
                        <tr>
                            <td class="td_height_8">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="100%" cellpadding="4" cellspacing="4" border="0">
                                    <tr>
                                        <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                            <asp:Literal ID="AddCertificate_previewCertificate_Panel_Literal" runat="server"
                                                Text="Preview"></asp:Literal>
                                        </td>
                                        <td style="width: 50%" valign="top" align="right">
                                            <table border="0" cellpadding="5" cellspacing="2">
                                                <tr>
                                                    <td align="right">
                                                        <asp:ImageButton ID="AddCertificate_previewCertificate_Panel_closeImageButton" runat="server"
                                                            SkinID="sknCloseImageButton" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_5" colspan="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupcontrol_question_inner_bg" align="center" colspan="2">
                                            <table border="0" cellpadding="2" cellspacing="2" width="80%" class="tab_body_bg"
                                                align="left">
                                                <tr>
                                                    <td>
                                                        <div id="AddCertificate_previewDIV" style="width: 597px; height: 395px; overflow: auto;
                                                            display: block;" runat="server">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table>
                                                <tr>
                                                    <td class="td_height_8">&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_8" colspan="2">
                                        </td>
                                    </tr>
                                    <tr align="left">
                                        <td colspan="2">
                                            <asp:LinkButton ID="AddCertificate_previewCertificate_Panel_cancelLinkButton" runat="server"
                                                Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <ajaxToolKit:ModalPopupExtender ID="AddCertificate_previewCertificate_Panel_modalPopupExtender"
                        runat="server" TargetControlID="AddCertificate_previewCertificate_Panel_hiddenButton"
                        PopupControlID="AddCertificate_previewCertificate_Panel" BackgroundCssClass="modalBackground"
                        CancelControlID="AddCertificate_previewCertificate_Panel_cancelLinkButton">
                    </ajaxToolKit:ModalPopupExtender>
                </asp:Panel>
            </td>
        </tr>--%>
    </table>
</asp:Content>
