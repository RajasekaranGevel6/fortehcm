<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="QuestionUsageSummary.aspx.cs" Title="Question Usage Summary" Inherits="Forte.HCM.UI.Popup.QuestionUsageSummary" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="QuestionUsageSummary_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="QuestionUsageSummary_headerLiteral" runat="server" Text="Question Usage Summary"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="QuestionUsageSummary_topCloseImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td align="left" class="popup_td_padding_10">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="msg_align" colspan="2">
                                        <asp:UpdatePanel ID="QuestionUsageSummary_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="QuestionUsageSummary_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="QuestionUsageSummary_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="QuestionUsageSummary_pageNavigator" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">
                                        <asp:Label ID="QuestionUsageSummary_questionKeyLabel" runat="server" Text="Question ID"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </td>
                                    <td style="width: 85%">
                                        <asp:Label ID="QuestionUsageSummary_questionKeyValueLabel" runat="server" Text=""
                                            SkinID="sknLabelFieldText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_8">
                                    </td>
                                </tr>
                                <tr id="QuestionUsageSummary_searchTestResultsTR" runat="server">
                                    <td id="Td1" class="header_bg" align="center" runat="server" colspan="2">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 50%" align="left">
                                                    <asp:Literal ID="QuestionUsageSummary_searchResultsLiteral" runat="server" Text="Test Details"></asp:Literal>
                                                    &nbsp;<asp:Label ID="QuestionUsageSummary_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                        Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                </td>
                                                <td style="width: 50%" align="right">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:UpdatePanel ID="QuestionUsageSummary_questionsDetailsUpdatePanel" runat="server"
                                            UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left" class="grid_body_bg">
                                                            <div id="QuestionUsageSummary_questionsDetailsDIV" runat="server" style="height: 250px;
                                                                overflow: auto;">
                                                                <asp:GridView ID="QuestionUsageSummary_questionDetailsGridView" runat="server" AutoGenerateColumns="False"
                                                                    SkinID="sknWrapHeaderGrid" AllowSorting="True" Width="100%" OnRowCreated="QuestionUsageSummary_questionDetailsGridView_RowCreated"
                                                                    OnSorting="QuestionUsageSummary_questionDetailsGridView_Sorting" OnRowDataBound="QuestionUsageSummary_questionDetailsGridView_RowDataBound">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="TestKey" HeaderText="Test ID" SortExpression="TESTKEY"
                                                                            ItemStyle-Width="40px" />
                                                                        <asp:TemplateField SortExpression="NAME" ItemStyle-Width="100px" HeaderText="Test Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="QuestionUsageSummary_testNameLabel" runat="server" Text='<%# TrimContent(Eval("Name").ToString(),30) %>'
                                                                                    ToolTip='<%# Eval("Name") %>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Created Date" SortExpression="TESTCREATIONDATE DESC" ItemStyle-Width="30px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="QuestionUsageSummary_testCreatedDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.TestDetail)Container.DataItem).TestCreationDate) %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="NoOfCandidatesAdministered" HeaderText="Candidates Administered"
                                                                            SortExpression="NOOFCANDIDATESADMINISTERED DESC" HeaderStyle-HorizontalAlign="Center"
                                                                            HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="10px"
                                                                            ItemStyle-CssClass="td_padding_right_15"></asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <uc3:PageNavigator ID="QuestionUsageSummary_pageNavigator" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_5">
                <asp:LinkButton ID="QuestionUsageSummary_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClientClick="javascript:CloseMe()" />
            </td>
        </tr>
    </table>
</asp:Content>
