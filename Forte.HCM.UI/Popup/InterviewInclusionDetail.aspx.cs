﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewInclusionDetail.cs
// File that represents the user interface layout and functionalities for
// the InterviewInclusionDetail page. This will helps to view the list of
// of interviews the given question was included.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the InterviewInclusionDetail page. This will helps to view the list of
    /// of interviews the given question was included. This class inherits 
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class InterviewInclusionDetail : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler that will call when a page gets loaded and
        /// populate the test details in the appropriate grid.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> object holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Interview Inclusion Details");

                // Subscribes to the page number click event.
                InterviewInclusionDetail_pageNavigator.PageNumberClick +=
                    new PageNavigator.PageNumberClickEventHandler
                        (InterviewInclusionDetail_pageNavigator_PageNumberClick);

                if (!IsPostBack)
                {
                    if (!Utility.IsNullOrEmpty(Request.QueryString["questionkey"]))
                    {
                        InterviewInclusionDetail_questionKeyHiddenField.Value = 
                            Request.QueryString["questionkey"].ToString().Trim();
                    }

                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "TestKey";

                    // Show test details by passing 1 as page number.
                    LoadInterviewInclusionDetail(1);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewInclusionDetail_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of test inclusion detail gridview.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/> that contains event data.
        /// </param>
        protected void InterviewInclusionDetail_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadInterviewInclusionDetail(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewInclusionDetail_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the grid column header link
        /// is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will sort the grid data.
        /// </remarks>
        protected void InterviewInclusionDetail_detailGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                InterviewInclusionDetail_pageNavigator.Reset();
                LoadInterviewInclusionDetail(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewInclusionDetail_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// triggered in the details gridview.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void InterviewInclusionDetail_detailGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewInclusionDetail_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the details gridview.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void InterviewInclusionDetail_detailGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (InterviewInclusionDetail_detailGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewInclusionDetail_errorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        /// <summary>
        /// Method will load the interview inclusion detail
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="pageNumber"/> that contains the current page number.
        /// </param>
        private void LoadInterviewInclusionDetail(int pageNumber)
        {
            List<TestDetail> testDetails = null;

            int totalRecords = 0;
            testDetails = new InterviewBLManager().GetInterviewTestInclusionDetail
                (InterviewInclusionDetail_questionKeyHiddenField.Value.Trim(),
                pageNumber, base.GridPageSize, ViewState["SORT_FIELD"].ToString(),
                ((SortType)ViewState["SORT_ORDER"]), out totalRecords);

            InterviewInclusionDetail_detailGridView.DataSource = testDetails;
            InterviewInclusionDetail_detailGridView.DataBind();
            
            InterviewInclusionDetail_pageNavigator.PageSize = base.GridPageSize;
            InterviewInclusionDetail_pageNavigator.TotalRecords = totalRecords;

            // Show no data found to display message.
            if (testDetails == null || testDetails.Count == 0)
            {
                InterviewInclusionDetail_errorMessageLabel.Text = Resources.HCMResource.Common_Empty_Grid;
                InterviewInclusionDetail_testInclusionDetailDiv.Visible = false;
            }
        }

        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Protected Overridden Methods                                
    }
}