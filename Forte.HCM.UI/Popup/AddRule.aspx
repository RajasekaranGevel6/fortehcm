﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddRule.aspx.cs" MasterPageFile="~/MasterPages/PopupMaster.Master"
    Inherits="Forte.HCM.UI.Popup.AddRule" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="AddRule_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">
        function CloseAddRuleWindow() {
            var btncnrl = '<%= Request.QueryString["btncnrl"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Trigger the click event of the refresh button in the parent page.
            if (btncnrl != null && window.opener.document.getElementById(btncnrl) != null)
                window.opener.document.getElementById(btncnrl).click();

            self.close();
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="AddRule_titleLiteral" runat="server" Text="Add Rule"></asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="AddRule_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_add_time_slot_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <div id="AddRule_searchCriteriasDiv" runat="server" style="display: block;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center">
                                            <asp:UpdatePanel ID="AddRule_messageUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="AddRule_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                        SkinID="sknErrorMessage" Width="100%" Text=""></asp:Label>
                                                    <asp:Label ID="AddRule_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                                        SkinID="sknSuccessMessage" Width="100%" Text=""></asp:Label>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="AddRule_saveButton" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="AddRule_searchCriteriDiv" runat="server" style="display: block;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td width="100%">
                                                            <table width="100%" border="0" cellspacing="2" cellpadding="2" runat="server">
                                                                <tr>
                                                                    <td style="width: 20%" align="left">
                                                                        <asp:Label ID="AddRule_nameLabel" runat="server" Text="Rule Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class="mandatory" visible="false">*</span>
                                                                    </td>
                                                                    <td style="width: 80%" align="left">
                                                                        <asp:TextBox ID="AddRule_nameTextBox" runat="server" onkeyup="CommentsCount(200,this)"
                                                                            onchange="CommentsCount(200,this)" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 20%" align="left">
                                                                        <asp:Label ID="AddRule_reasonLabel" runat="server" Text="Reason" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class="mandatory" visible="false">*</span>
                                                                    </td>
                                                                    <td style="width: 80%" align="left">
                                                                        <asp:TextBox ID="AddRule_reasonTextBox" runat="server" TextMode="MultiLine" Width="100%"
                                                                            onkeyup="CommentsCount(200,this)" onchange="CommentsCount(200,this)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                                        <asp:Literal ID="AddRule_conditionLiteral" runat="server" Text="Condition"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server">
                                                        <td class="grid_body_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <div id="AddRule_conditionDetailDIV" runat="server" style="height: 170px; overflow: auto;">
                                                                            <asp:UpdatePanel ID="AddRule_conditionUpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <div id="Rule_referenceInfoMainDiv">
                                                                                        <div style="clear: both; overflow: auto; height: 150px" runat="server" id="AddRule_controlsDiv"
                                                                                            class="resume_Table_Bg">
                                                                                            <asp:HiddenField ID="AddRule_deletedRowHiddenField" runat="server" />
                                                                                            <asp:ListView ID="AddRule_listView" runat="server"
                                                                                                OnItemCommand="AddRule_listView_ItemCommand">
                                                                                                <LayoutTemplate>
                                                                                                    <div id="itemPlaceholderContainer" runat="server">
                                                                                                        <div runat="server" id="itemPlaceholder">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </LayoutTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <div class="resume_panel_list_view">
                                                                                                        <asp:Panel runat="server" ID="AddRule_conditionPanel" CssClass="can_resume_panel_text">
                                                                                                            <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="AddRule_rowIndexLabel" runat="server" 
                                                                                                                            Text='<%# Container.DataItemIndex + 1 %>' 
                                                                                                                            SkinID="sknRecordCount"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:DropDownList ID="AddRule_sourceNodeDropDown" runat="server">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:DropDownList ID="AddRule_expressionTypeDropDown" runat="server">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:DropDownList ID="AddRule_destinationNodeDropDown"
                                                                                                                            runat="server" AutoPostBack="true" 
                                                                                                                            OnSelectedIndexChanged="AddRule_destinationNodeDropDown_SelectedIndexChanged" >
                                                                                                                        </asp:DropDownList>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="AddRule_destinationValueTextBox" MaxLength="50"
                                                                                                                            Text='<%# Eval("DestinationValue") %>'
                                                                                                                            runat="server" 
                                                                                                                            Enabled='<%# Eval("DestinationElementID") != null &&  Eval("DestinationElementID") != string.Empty  && Eval("DestinationElementID").ToString()=="1" %>'>
                                                                                                                            </asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:DropDownList ID="AddRule_logicalOperatorDropDown" runat="server">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:ImageButton ID="AddRule_deleteConditionImageButton" runat="server" CommandName="deleteRole"
                                                                                                                            SkinID="sknDeleteImageButton" ToolTip="Delete Rule" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </asp:Panel>
                                                                                                    </div>
                                                                                                </ItemTemplate>
                                                                                                <EmptyDataTemplate>
                                                                                                <table style="width: 100%">
                                                                                                    <tr>
                                                                                                        <td style="height: 50px">
                                                                                                            <asp:Label ID="AddRule_noRecordLabel" 
                                                                                                            Text="No condition found to display" CssClass="error_message_text" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                </EmptyDataTemplate>
                                                                                            </asp:ListView>
                                                                                        </div>
                                                                                        <div>
                                                                                            <asp:LinkButton ID="AddRule_addConditionLinkButton" runat="server" SkinID="sknAddLinkButton"
                                                                                                ToolTip="Add Condition" Text="Add Condition" OnClick="AddRule_addConditionLinkButton_Click">
                                                                                            </asp:LinkButton>
                                                                                        </div>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                    <tr runat="server">
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 50%" align="left" class="header_text_bold">
                                                                        <asp:Literal ID="AddRule_actionLiteral" runat="server" Text="Action"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server">
                                                        <td class="grid_body_bg">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="width:200px">
                                                                        <asp:RadioButton ID="AddRule_addToExistingRecordRadioButton" runat="server" 
                                                                            Text="Add To Existing Candidate Record" GroupName="AddRule_actionGroupName" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:RadioButton ID="AddRule_markAsDuplicateCheckBoxRadioButton" runat="server"
                                                                            GroupName="AddRule_actionGroupName" Text="Mark As Duplicate"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_8">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <table cellpadding="3" cellspacing="3">
                    <tr>
                        <td style="padding-left: 5px">
                            <asp:Button ID="AddRule_saveButton" runat="server" Text="Save" SkinID="sknButtonId" 
                                OnClick="AddRule_saveButton_Click" />
                        </td>
                        <td>
                            <asp:LinkButton ID="AddRule_cancelLinkButton" runat="server" SkinID="sknPopupLinkButton"
                                Text="Cancel" OnClientClick="javascript:window.close()"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
