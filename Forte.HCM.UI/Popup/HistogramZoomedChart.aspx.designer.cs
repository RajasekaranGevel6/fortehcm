﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.Popup {
    
    
    public partial class HistogramZoomedChart {
        
        /// <summary>
        /// HistogramZoomedChart_headerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal HistogramZoomedChart_headerLiteral;
        
        /// <summary>
        /// HistogramZoomedChart_topCloseImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton HistogramZoomedChart_topCloseImageButton;
        
        /// <summary>
        /// HistogramZoomedChart_topSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label HistogramZoomedChart_topSuccessMessageLabel;
        
        /// <summary>
        /// HistogramZoomedChart_topErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label HistogramZoomedChart_topErrorMessageLabel;
        
        /// <summary>
        /// SearchQuestion_stateHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchQuestion_stateHiddenField;
        
        /// <summary>
        /// HistogramZoomedChart_chart control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.DataVisualization.Charting.Chart HistogramZoomedChart_chart;
        
        /// <summary>
        /// HistogramZoomedChart_cancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton HistogramZoomedChart_cancelLinkButton;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.PopupMaster Master {
            get {
                return ((Forte.HCM.UI.MasterPages.PopupMaster)(base.Master));
            }
        }
    }
}
