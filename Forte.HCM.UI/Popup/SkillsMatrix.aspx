﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/PopupMaster.Master" AutoEventWireup="true"
    CodeBehind="SkillsMatrix.aspx.cs" Inherits="Forte.HCM.UI.Popup.SkillsMatrix" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="~/CommonControls/SkillsMatrixControl.ascx" TagName="SkillsMatrixControl"
    TagPrefix="uc1" %>
<asp:Content ID="SkillsMatrix_Content" ContentPlaceHolderID="PopupMaster_contentPlaceHolder"
    runat="server">
    
    <script type="text/javascript">
    /* To close the pop up */
        function CloseMe() {
            self.close();
        }
    </script>
    <table width="96%" border="0" cellspacing="0" cellpadding="0" style="text-align:center">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey" align="left">
                            <asp:Literal ID="SkillsMatrix_headerLiteral" runat="server" Text="Resume & Competency Vector"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="SkillsMatrix_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" align="center" style="width: 100%">
                <table width="100%" cellpadding="0" cellspacing="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td align="center" class="popup_td_padding_10">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:Label ID="SkillsMatrix_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                        <asp:Label ID="SkillsMatrix_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" valign="top" align="center">
                                        <div style="height: auto;text-align:center; width: 100%;" >
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                <td align="center">
                                                <uc1:SkillsMatrixControl ID="SkillsMatrix_Control" runat="server" />
                                                </td>
                                                </tr>                                                
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td >
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" align="left">
                <asp:LinkButton ID="SkillsMatrix_topCancelButton" runat="server" Text="Cancel"
                    SkinID="sknPopupLinkButton" OnClientClick="javascript:return CloseMe();"></asp:LinkButton>
            </td>
        </tr>
    </table>
</asp:Content>
