﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AvailabilityRequest.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.AvailabilityRequest"
    Title="Assessor Availability Request" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="AvailabilityRequest_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="td_padding_top_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="AvailabilityRequest_titleLiteral" runat="server" Text="Assessor Availability Request"></asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="AvailabilityRequest_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <table width="100%" cellpadding="0" cellspacing="3" border="0" class="popupcontrol_request_time_slot_bg">
                    <tr>
                        <td style="width: 100%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="height: 20px">
                                        <asp:UpdatePanel ID="AvailabilityRequest_messageUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="AvailabilityRequest_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknErrorMessage" Width="100%" Text=""></asp:Label>
                                                <asp:Label ID="AvailabilityRequest_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknSuccessMessage" Width="100%" Text=""></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 100%">
                                        <asp:UpdatePanel ID="AvailabilityRequest_inputControlsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2" style="width: 78%" valign="top" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 80%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="AvailabilityRequest_selectAssessorLiteral" runat="server" Text="Select Assessor/Skills"></asp:Literal>
                                                                                </td>
                                                                                 <td style="width: 20%" align="right" valign="middle">
                                                                                    <asp:ImageButton ID="AvailabilityRequest_emailImageButton" runat="server" ToolTip="Click here to email the assessor"
                                                                                        SkinID="sknMailImageButton" Visible="false"/>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="availability_request_grid_body_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 15%; height: 24px" align="left" valign="middle">
                                                                                    <asp:Label ID="AvailabilityRequest_assessorLabel" runat="server" Text="Assessor"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    <span class="mandatory">*</span>
                                                                                </td>
                                                                                <td style="width: 85%" align="left" valign="middle">
                                                                                    <asp:DropDownList ID="AvailabilityRequest_assessorDropDownList" runat="server" AutoPostBack="true" SkinID="sknAssessorDropDownList"
                                                                                        Width="530px" OnSelectedIndexChanged="AvailabilityRequest_assessorDropDownList_SelectedIndexChanged">
                                                                                    </asp:DropDownList>
                                                                                    <asp:HiddenField ID="AvailabilityRequest_selectedAssessorID" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 15%; height: 24px; padding-top: 2px" align="left" valign="top">
                                                                                    <asp:Label ID="AvailabilityRequest_skillsLabel" runat="server" Text="Skills/Areas"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    <span class="mandatory">*</span></td>
                                                                                <td style="width: 85%" align="left" valign="middle" class="checkbox_list_bg">
                                                                                    <div style="height: 60px; overflow: auto;">
                                                                                        <asp:CheckBoxList ID="AvailabilityRequest_skillsCheckBoxList" runat="server" RepeatColumns="4" SkinID="sknSkillsCheckBoxList"
                                                                                            RepeatDirection="Horizontal" CellSpacing="5" Width="300px" AutoPostBack="True"
                                                                                            OnSelectedIndexChanged="AvailabilityRequest_skillsCheckBoxList_SelectedIndexChanged">
                                                                                        </asp:CheckBoxList>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="width: 100%" valign="top" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="AvailabilityRequest_selectedTimeSlotsLiteral" runat="server" Text="Requested Time Slots"></asp:Literal>
                                                                                </td>
                                                                                <td style="width: 50%" align="right">
                                                                                     <asp:LinkButton ID="AvailabilityRequest_addTimeSlotsLinkButton" runat="server"
                                                                                        Text="Request Time Slots" SkinID="sknAddLinkButton" ToolTip="Click here request time slots"
                                                                                        Visible="false" />
                                                                                    <div style="display: none">
                                                                                        <asp:Button ID="AvailabilityRequest_addTimeSlotsButton" runat="server" Text="Refresh"
                                                                                            SkinID="sknButtonId" OnClick="AvailabilityRequest_addTimeSlotsButton_Click"
                                                                                            Visible="true" ToolTip="Click here add or edit selected time slots" />
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" class="availability_request_grid_body_bg">
                                                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="height: 270px; overflow: auto;">
                                                                                        <asp:GridView ID="AvailabilityRequest_selectedTimeSlotsGridView" runat="server" AllowSorting="False"
                                                                                            AutoGenerateColumns="False" Width="100%" OnRowDataBound="AvailabilityRequest_selectedTimeSlotsGridView_RowDataBound"
                                                                                            OnRowCommand="AvailabilityRequest_selectedTimeSlotsGridView_RowCommand" Height="100%">
                                                                                            <Columns>
                                                                                                <asp:TemplateField HeaderStyle-Width="5%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="RequestTimeSlot_selectedTimeSlotsGridView_deleteTimeSlotImageButton"
                                                                                                            runat="server" SkinID="sknDeletePositionProfile" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                            ToolTip="Delete Time Slot" CssClass="showCursor" CommandName="DeleteTimeSlot" />&nbsp;
                                                                                                        <asp:HiddenField ID="RequestTimeSlot_selectedTimeSlotsGridView_assessorIDHiddenField"
                                                                                                            runat="server" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Assessor" HeaderStyle-Width="15%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="RequestTimeSlot_selectedTimeSlotsGridView_assessorLabel" runat="server"
                                                                                                            Text='<%# Eval("Assessor") %>' Width="100%"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Skill" HeaderStyle-Width="15%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="RequestTimeSlot_selectedTimeSlotsGridView_skillLabel" runat="server"
                                                                                                            Text='<%# Eval("Skill") %>' Width="100%"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="20%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="RequestTimeSlot_selectedTimeSlotsGridView_dateLabel" runat="server"
                                                                                                            Text='<%# Eval("AvailabilityDateString") %>' Width="100%"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Time Slot" HeaderStyle-Width="20%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="RequestTimeSlot_selectedTimeSlotsGridView_timeSlotLabel" runat="server"
                                                                                                            Text='<%# Eval("TimeSlot") %>' Width="100%"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Comments" HeaderStyle-Width="30%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="RequestTimeSlot_selectedTimeSlotsGridView_commentsLabel" runat="server"
                                                                                                            Text='<%# Eval("Comments") %>' Width="100%"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                            <EmptyDataTemplate>
                                                                                                <table style="width: 100%; height: 100%">
                                                                                                    <tr>
                                                                                                        <td style="height: 110px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="middle" align="center" style="height: 100%" class="error_message_text_normal">
                                                                                                            No time slots selected
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </EmptyDataTemplate>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="AvailabilityRequest_buttonControlsUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="td_height_5" colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="left">
                                    <asp:Button ID="AvailabilityRequest_acceptButton" runat="server" Text="Accept"
                                        SkinID="sknButtonId" Visible="true" 
                                        ToolTip="Click here accept the selected time slots" 
                                        onclick="AvailabilityRequest_acceptButton_Click" />
                                </td>
                                <td valign="middle" align="left">
                                    &nbsp;<asp:LinkButton ID="AvailabilityRequest_cancelLinkButton" runat="server" Text="Cancel"
                                        SkinID="sknPopupLinkButton" OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window"> </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_2">
            </td>
        </tr>
    </table>
</asp:Content>
