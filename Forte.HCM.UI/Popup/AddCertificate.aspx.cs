﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AddCertificate.cs
// File that represents the user interface to add certificate details.
// This page will provide the functionality for adding or modfiying
// certificate information.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.IO;
using System.Web.UI;
using System.Drawing;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives                                                          

namespace Forte.HCM.UI.Admin
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities for 
    /// add/modify the certificates.
    /// </summary>
    /// <remarks>
    /// This class is inherited from the <see cref="PageBase"/> class.
    /// </remarks>
    public partial class AddCertificate : PageBase
    {
        #region Event Handler                                                  

        /// <summary>
        /// Handler that will call when the page gets loaded. This will load
        /// certification detail in the appropriate text fields.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            //set Browser tittle
            Master.SetPageCaption("Add Certificate");
            if (!IsPostBack)
            {
                // Show the default HTML information in the editor.
                if (!Utility.IsNullOrEmpty(Request.QueryString["certificateid"]) &&
                    Request.QueryString["certificateid"].ToUpper() == "ADD")
                {
                    AddCertificate_htmlTextbox.Text = Resources.HCMResource.AddCertificate_DefaultHtmlContent;
                    Page.Title = "Add Certificate";
                    Page.Form.DefaultButton = AddCertificate_submitButton.UniqueID;
                    AddCertificate_certificateNameTextbox.Focus();

                    AddCertificate_headerLiteral.Text = "Add Certificate";
                }
                else
                {
                    LoadValues();
                }
                ListAllCertificates();
            }
        }

        /// <summary>
        /// Handler that will call when the upload button is clicked.
        /// During that time, it uploads the file in the working folder.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void AddCertificate_uploadButton_Click(object sender, EventArgs e)
        {
            ClearLabelMessage();

            if (AddCertificate_certificateBackgroundFileUpload.FileName != "")
            {
                try
                {
                    string fileExtension = 
                        Path.GetExtension(AddCertificate_certificateBackgroundFileUpload.FileName);
                    double fileSize = 
                        AddCertificate_certificateBackgroundFileUpload.PostedFile.ContentLength / 1024f;

                    if (fileExtension.ToUpper() == ".JPEG" || fileExtension.ToUpper() == ".JPG"
                        || fileExtension.ToUpper() == ".GIF" || fileExtension.ToUpper() == ".PNG")
                    {
                        if (fileSize <= 1024)
                        {
                            string fileName = AddCertificate_certificateBackgroundFileUpload.FileName;
                            string certificateImagePath = Server.MapPath("~") + @"\CertificateFormats\" + fileName;
                            //Server.MapPath(@"../CertificateFormats/" + fileName);
                            //Server.MapPath("/HCM/CertificateFormats/" + fileName);

                            AddCertificate_certificateBackgroundFileUpload.SaveAs(certificateImagePath);

                            base.ShowMessage(AddCertificate_successMessageLabel, "File uploaded successfully");
                            AddCertificate_availableBackgroundTextbox.Text = string.Empty;
                            ListAllCertificates();
                        }
                        else
                            base.ShowMessage(AddCertificate_errorMessageLabel,
                                "File size should not be exceeded more than 1MB");
                    }
                    else
                        base.ShowMessage(AddCertificate_errorMessageLabel, fileExtension
                            + " file should not be uploaded");
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    base.ShowMessage(AddCertificate_errorMessageLabel, exp.Message);
                }
            }
            else
            {
                base.ShowMessage(AddCertificate_errorMessageLabel,
                    Resources.HCMResource.AddCertificate_SelectAFileToUpload);
            }
        }

        /// <summary>
        /// Handler that will call when the submit button is clicked. This will
        /// submit all the certificate related information to the database.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void AddCertificate_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();
                CertificationDetail certificationDetail = null;
                string certificatePath = string.Empty;

                // Validate the data
                if (!IsValidData())
                    return;

                if (!Utility.IsNullOrEmpty(Request.QueryString["certificateid"]) &&
                    Request.QueryString["certificateid"].ToUpper() == "ADD")
                {
                    certificationDetail = ConstructCertificationDetail();

                    //new AdminBLManager().InsertCertificate(certificationDetail, base.userID);

                    base.ShowMessage(AddCertificate_successMessageLabel,
                        Resources.HCMResource.AddCertificate_CertificateAddedSuccessfully);
                    AddCertificate_submitButton.Enabled = false;

                    //Close the popup window
                    string closeScript = "self.close();";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true); 
                }
                else
                {
                    certificationDetail = ConstructCertificationDetail();
                    new AdminBLManager().UpdateCertificate(certificationDetail, base.userID,
                        Convert.ToInt32(Request.QueryString["certificateid"].ToString().Trim()));
                    base.ShowMessage(AddCertificate_successMessageLabel,
                        Resources.HCMResource.AddCertificate_CertificateUpdatedSuccessfully);

                    //Close the popup window
                    string closeScript = "self.close();";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true); 
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AddCertificate_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the preview button is clicked. This will show
        /// the preview of the HTML code.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void AddCertificate_previewButton_Click(object sender, EventArgs e)
        {
            try
            {
                AddCertificate_previewDIV.InnerHtml = AddCertificate_htmlTextbox.Text;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AddCertificate_errorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handler                                               

        #region Private Methods                                                

        /// <summary>
        /// Method that will construct the certification detail.
        /// </summary>
        /// <returns>
        /// A <see cref="CertificationDetail"/> that contains the certification detail.
        /// </returns>
        private CertificationDetail ConstructCertificationDetail()
        {
            CertificationDetail certificationDetail = new CertificationDetail();
            certificationDetail.CertificateName = AddCertificate_certificateNameTextbox.Text.Trim();
            certificationDetail.CertificateDesc = AddCertificate_descriptionTextBox.Text.Trim();
            certificationDetail.HtmlText = AddCertificate_htmlTextbox.Text.Trim();

            // Make an HTML file by using the HTML textbox content.
            string filePath = Server.MapPath("~") + @"\CertificateFormats\Certificate.htm";
                //Server.MapPath(@"../CertificateFormats/Certificate.htm"); 
            
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                writer.WriteLine(AddCertificate_htmlTextbox.Text.Trim());
                writer.Close();
            }

            string address = filePath;
            int width = 620;
            int height = 420;

            Bitmap bmp = WebPageCaptureManager.GetWebPageCaptureImage
                (address, 620, 420, width, height);

            certificationDetail.ImageData = ImageToByteArray(bmp);

            return certificationDetail;
        }

        /// <summary>
        /// Method that will convert an image to byte array.
        /// </summary>
        /// <param name="img">
        /// An <see cref="Image"/> that contains the image instance.
        /// </param>
        /// <returns>
        /// A <see cref="byte[]"/> that contains the byte array.
        /// </returns>
        private byte[] ImageToByteArray(Image img)
        {
            ImageConverter convertor = new ImageConverter();
            return ((byte[])convertor.ConvertTo(img, typeof(byte[])));
        }

        /// <summary>
        /// Method that will clear the error/success label messages.
        /// </summary>
        private void ClearLabelMessage()
        {
            AddCertificate_successMessageLabel.Text = string.Empty;
            AddCertificate_errorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Method that will show the existing certificate formats.
        /// </summary>
        private void ListAllCertificates()
        {
            DirectoryInfo di = new DirectoryInfo(Server.MapPath("~") + @"\CertificateFormats\"); 

            FileInfo[] rgFiles = di.GetFiles();
            foreach (FileInfo fi in rgFiles)
            {
                if (fi.Extension.ToUpper() == ".JPG" || fi.Extension.ToUpper() == ".GIF" 
                    || fi.Extension.ToUpper() == ".PNG" || fi.Extension.ToUpper() == ".JPEG")
                {
                    AddCertificate_availableBackgroundTextbox.Text =
                        AddCertificate_availableBackgroundTextbox.Text + fi.Name + "\n";
                }
            }
        }

        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;

            // Validate session description text field
            if (AddCertificate_certificateNameTextbox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(AddCertificate_errorMessageLabel,
                    Resources.HCMResource.AddCertificate_CertificateNameCannotBeBlank);
            }

            // Validate test instructions field
            if (AddCertificate_descriptionTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(AddCertificate_errorMessageLabel,
                    Resources.HCMResource.AddCertificate_DescriptionCannotBeBlank);
            }
            if (AddCertificate_htmlTextbox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(AddCertificate_errorMessageLabel,
                    Resources.HCMResource.AddCertificate_HtmlTextboxCannotBeBlank);
            }
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            AddCertificate_submitButton.Focus();
            Page.Title = "Edit Certificate";
            AddCertificate_headerLiteral.Text = "Edit Certificate";

            CertificationDetail certificationDetail = new AdminBLManager().GetCertificateDetails
                (Convert.ToInt32(Request.QueryString["certificateid"].ToString()));

            AddCertificate_certificateNameTextbox.Text = certificationDetail.CertificateName.Trim();
            AddCertificate_descriptionTextBox.Text = certificationDetail.CertificateDesc.Trim();
            AddCertificate_htmlTextbox.Text = certificationDetail.HtmlText.Trim();
        }

        #endregion Protected Overridden Methods                                
    }
}