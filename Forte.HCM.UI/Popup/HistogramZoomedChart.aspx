﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HistogramZoomedChart.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.HistogramZoomedChart" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="HistogramZoomedChart_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 90%" class="popup_header_text_grey">
                            <asp:Literal ID="HistogramZoomedChart_headerLiteral" runat="server" Text=""></asp:Literal>
                        </td>
                        <td style="width: 10%" align="right">
                            <asp:ImageButton ID="HistogramZoomedChart_topCloseImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="HistogramZoomedChart_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="HistogramZoomedChart_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                    ID="SearchQuestion_stateHiddenField" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" class="popupcontrol_question_inner_bg">
                            <div style="height: 460px; overflow: auto; width: 900px">
                                <asp:Chart ID="HistogramZoomedChart_chart" runat="server" BackColor="Transparent"
                                    Palette="Pastel" Width="900px" Height="450px" AntiAliasing="Graphics">
                                    <Titles>
                                        <asp:Title Name="HistogramZoomedChart_chartTitle">
                                        </asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="HistogramZoomedChart_chartPrimarySeries" BorderColor="64, 64, 64, 64"
                                            IsValueShownAsLabel="true" Font="Helvetica, 11px">
                                            <SmartLabelStyle AllowOutsidePlotArea="Yes" IsMarkerOverlappingAllowed="True" MaxMovingDistance="200" />
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="HistogramZoomedChart_chartArea" BorderColor="64, 64, 64, 64"
                                            BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent"
                                            BackGradientStyle="TopBottom">
                                            <AxisX TitleFont="Trebuchet MS, 8pt" LineColor="64, 64, 64, 64" IsLabelAutoFit="False"
                                                IsStartedFromZero="true">
                                                <LabelStyle ForeColor="180, 65, 140, 240" Angle="0" />
                                                <MajorGrid Enabled="false" />
                                            </AxisX>
                                            <AxisY Interval = "1">
                                                <MajorGrid Enabled="false" />
                                                <LabelStyle ForeColor="180, 65, 140, 240" />
                                            </AxisY>
                                            <Position Height="91" Width="100" Y="5" />
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_10">
                <asp:LinkButton ID="HistogramZoomedChart_cancelLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClientClick="javascript:CloseMe()" />
            </td>
        </tr>
    </table>
</asp:Content>
