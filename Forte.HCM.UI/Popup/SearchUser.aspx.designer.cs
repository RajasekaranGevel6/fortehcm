﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.Popup {
    
    
    public partial class SearchUser {
        
        /// <summary>
        /// SearchUser_headerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SearchUser_headerLiteral;
        
        /// <summary>
        /// SearchUser_browserHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchUser_browserHiddenField;
        
        /// <summary>
        /// SearchUser_topCancelImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton SearchUser_topCancelImageButton;
        
        /// <summary>
        /// SearchUser_messageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchUser_messageUpdatePanel;
        
        /// <summary>
        /// SearchUser_successMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchUser_successMessageLabel;
        
        /// <summary>
        /// SearchUser_errorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchUser_errorMessageLabel;
        
        /// <summary>
        /// SearchUser_searchCriteriasDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl SearchUser_searchCriteriasDiv;
        
        /// <summary>
        /// SearchUser_searchCriteriaUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchUser_searchCriteriaUpdatePanel;
        
        /// <summary>
        /// SearchUser_userNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchUser_userNameLabel;
        
        /// <summary>
        /// SearchUser_userNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchUser_userNameTextBox;
        
        /// <summary>
        /// SearchUser_firstNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchUser_firstNameLabel;
        
        /// <summary>
        /// SearchUser_firstNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchUser_firstNameTextBox;
        
        /// <summary>
        /// SearchUser_middleNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchUser_middleNameLabel;
        
        /// <summary>
        /// SearchUser_middleNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchUser_middleNameTextBox;
        
        /// <summary>
        /// SearchUser_lastNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchUser_lastNameLabel;
        
        /// <summary>
        /// SearchUser_lastNameTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchUser_lastNameTextBox;
        
        /// <summary>
        /// SearchUser_searchButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button SearchUser_searchButton;
        
        /// <summary>
        /// SearchUser_searchResultsUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchUser_searchResultsUpdatePanel;
        
        /// <summary>
        /// SearchUser_searchTestResultsTR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow SearchUser_searchTestResultsTR;
        
        /// <summary>
        /// SearchUser_searchResultsLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SearchUser_searchResultsLiteral;
        
        /// <summary>
        /// SearchQuestion_sortHelpLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SearchQuestion_sortHelpLabel;
        
        /// <summary>
        /// SearchUser_searchResultsUpSpan control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl SearchUser_searchResultsUpSpan;
        
        /// <summary>
        /// SearchUser_searchResultsUpImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image SearchUser_searchResultsUpImage;
        
        /// <summary>
        /// SearchUser_searchResultsDownSpan control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl SearchUser_searchResultsDownSpan;
        
        /// <summary>
        /// SearchUser_searchResultsDownImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image SearchUser_searchResultsDownImage;
        
        /// <summary>
        /// SearchUser_testDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl SearchUser_testDiv;
        
        /// <summary>
        /// SearchUser_testGridView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView SearchUser_testGridView;
        
        /// <summary>
        /// SearchUser_pageNavigator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.PageNavigator SearchUser_pageNavigator;
        
        /// <summary>
        /// SearchUser_topResetLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton SearchUser_topResetLinkButton;
        
        /// <summary>
        /// SearchUser_topCancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton SearchUser_topCancelLinkButton;
        
        /// <summary>
        /// SearchUser_isMaximizedHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchUser_isMaximizedHiddenField;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.PopupMaster Master {
            get {
                return ((Forte.HCM.UI.MasterPages.PopupMaster)(base.Master));
            }
        }
    }
}
