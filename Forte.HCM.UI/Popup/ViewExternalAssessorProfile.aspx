﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewExternalAssessorProfile.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.ViewExternalAssessorProfile"
    Title="Assessor Profile" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ViewExternalAssessorProfile_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        // Function that handles the profile menu click event.
        function ProfileMenuClick(menuType)
        {
            // Hide all div.
            document.getElementById('<%=ViewExternalAssessorProfile_selectedMenuHiddenField.ClientID %>').value = menuType;
            document.getElementById('<%=ViewExternalAssessorProfile_contactInfoDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=ViewExternalAssessorProfile_skillsDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=ViewExternalAssessorProfile_assessmentDetailsDiv.ClientID %>').style.display = 'none';

            // Reset color.
            document.getElementById('<%=ViewExternalAssessorProfile_contactInfoLinkButton.ClientID %>').className = "assessor_profile_link_button";
            document.getElementById('<%=ViewExternalAssessorProfile_skillsLinkButton.ClientID %>').className = "assessor_profile_link_button";
            document.getElementById('<%=ViewExternalAssessorProfile_assessmentDetailsLinkButton.ClientID %>').className = "assessor_profile_link_button";

            if (menuType == 'CI')
            {
                document.getElementById('<%=ViewExternalAssessorProfile_contactInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=ViewExternalAssessorProfile_contactInfoLinkButton.ClientID %>').className = "assessor_profile_link_button_selected";
            }
            else if (menuType == 'SK')
            {
                document.getElementById('<%=ViewExternalAssessorProfile_skillsDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=ViewExternalAssessorProfile_skillsLinkButton.ClientID %>').className = "assessor_profile_link_button_selected";
            }
            else if (menuType == 'AD')
            {
                document.getElementById('<%=ViewExternalAssessorProfile_assessmentDetailsDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=ViewExternalAssessorProfile_assessmentDetailsLinkButton.ClientID %>').className = "assessor_profile_link_button_selected";
            }

            return false;
        }

        // Handler method that will be called when the 'save' button is 
        // clicked in the the show recommend page.
        function CloseViewAssessorProfile() {
            var btncnrl = '<%= Request.QueryString["ctrlid"] %>';

            if (window.dialogArguments) {
                window.opener = window.dialogArguments;
            }

            // Trigger the click event of the refresh button in the parent page.
            if (btncnrl != null && window.opener.document.getElementById(btncnrl) != null)
                window.opener.document.getElementById(btncnrl).click();

            self.close();
        }
    

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="td_padding_top_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="ViewExternalAssessorProfile_titleLiteral" runat="server">External Assessor Profile</asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="ViewExternalAssessorProfile_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <table width="100%" cellpadding="0" cellspacing="3" border="0" class="popupcontrol_view_assessor_profile_bg">
                    <tr>
                        <td style="width: 100%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="ViewExternalAssessorProfile_messageUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="ViewExternalAssessorProfile_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknErrorMessage" Width="100%" Text=""></asp:Label>
                                                <asp:Label ID="ViewExternalAssessorProfile_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknSuccessMessage" Width="100%" Text=""></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 100%">
                                        <asp:UpdatePanel ID="ViewExternalAssessorProfile_inputControlsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 100%" valign="top" class="popup_td_padding_2">
                                                            <asp:HiddenField runat="server" ID="ViewExternalAssessorProfile_assessorIDHiddenField" />
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" class="view_assessor_profile_assessor_details_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="height: 4px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" colspan="5" style="width: 100%" valign="top" class="view_external_assessor_profile_title_border">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 90%; height: 50px; padding-left: 10px;" valign="middle">
                                                                                                <asp:Label ID="ViewExternalAssessorProfile_nameValueLabel" runat="server" SkinID="sknLabelProfileNameText"
                                                                                                    Text=""></asp:Label>
                                                                                                <asp:HiddenField ID="ViewExternalAssessorProfile_firstNameHiddenField" runat="server" />
                                                                                                <asp:HiddenField ID="ViewExternalAssessorProfile_lastNameHiddenField" runat="server" />
                                                                                            </td>
                                                                                            <td align="right" style="width: 10%; padding-right: 10px;" valign="middle">
                                                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td align="right">
                                                                                                            <asp:ImageButton ID="ViewExternalAssessorProfile_convertToAssessorImageButton" runat="server" 
                                                                                                                SkinID="sknAssessorCalendarImageButton" 
                                                                                                                ToolTip="Click here to convert to registered user" 
                                                                                                                onclick="ViewExternalAssessorProfile_convertToAssessorImageButton_Click" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" style="height: 42px" valign="top" class="external_assessor_profile_links_bg">
                                                                                                <table border="0" cellpadding="10px" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:HiddenField ID="ViewExternalAssessorProfile_selectedMenuHiddenField" runat="server"
                                                                                                                Value="CI" />
                                                                                                            <div style="float: left; padding-top: 5px">
                                                                                                                <asp:LinkButton ID="ViewExternalAssessorProfile_contactInfoLinkButton" runat="server" Text="Contact Info"
                                                                                                                    ToolTip="Contact Info" CssClass="assessor_profile_link_button_selected" OnClientClick="javascript: return ProfileMenuClick('CI')"></asp:LinkButton>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <div style="float: left; padding-top: 5px">
                                                                                                                <asp:LinkButton ID="ViewExternalAssessorProfile_skillsLinkButton" runat="server" Text="Skills"
                                                                                                                    ToolTip="Skills" CssClass="assessor_profile_link_button" OnClientClick="javascript: return ProfileMenuClick('SK')"></asp:LinkButton>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <div style="float: left; padding-top: 5px">
                                                                                                                <asp:LinkButton ID="ViewExternalAssessorProfile_assessmentDetailsLinkButton" runat="server"
                                                                                                                    ToolTip="Assessments" Text="Assessments" CssClass="assessor_profile_link_button"
                                                                                                                    OnClientClick="javascript: return ProfileMenuClick('AD')"></asp:LinkButton>
                                                                                                            </div>
                                                                                                            <div class="profile_assessments_count_bg">
                                                                                                                <asp:Label ID="ViewExternalAssessorProfile_assessmentCountLabel" runat="server" 
                                                                                                                    Text="" CssClass="profile_assessments_count"></asp:Label></div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" style="width: 100%;" class="view_external_assessor_profile_details_border">
                                                                                    <div style="height: 300px; overflow: auto;">
                                                                                        <div id="ViewExternalAssessorProfile_contactInfoDiv" runat="server" style="display: block">
                                                                                            <table cellpadding="10" cellspacing="0" style="width: 100%">
                                                                                                <tr>
                                                                                                    <td align="left" style="width: 10%; height: 16px" valign="middle">
                                                                                                        <asp:Label ID="ViewExternalAssessorProfile_emailLabel" runat="server" SkinID="sknProfileTitleField"
                                                                                                            Text="Email"></asp:Label>
                                                                                                    </td>
                                                                                                    <td align="left" style="width: 34%; padding-left: 10px" valign="middle">
                                                                                                        <asp:Label ID="ViewExternalAssessorProfile_emailValueLabel" runat="server" SkinID="sknProfileValueField"
                                                                                                            Text=""></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="left" style="width: 10%; height: 16px" valign="middle">
                                                                                                        <asp:Label ID="ViewExternalAssessorProfile_mobilePhoneLabel" runat="server" SkinID="sknProfileTitleField"
                                                                                                            Text="Contact Number"></asp:Label>
                                                                                                    </td>
                                                                                                    <td align="left" style="width: 34%; padding-left: 10px" valign="middle">
                                                                                                        <asp:Label ID="ViewExternalAssessorProfile_mobilePhoneValueLabel" runat="server" SkinID="sknProfileValueField"
                                                                                                            Text=""></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                        <div id="ViewExternalAssessorProfile_skillsDiv" runat="server" style="display: none">
                                                                                            <table cellpadding="10" cellspacing="0" style="width: 100%">
                                                                                                <tr>
                                                                                                    <td align="left" style="width: 100%" valign="top">
                                                                                                        <div style="height: 190px; overflow: auto;">
                                                                                                            <asp:DataList ID="ViewExternalAssessorProfile_skillsDatalist" runat="server" RepeatColumns="1"
                                                                                                                RepeatDirection="Horizontal" RepeatLayout="Table">
                                                                                                                <ItemTemplate>
                                                                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <asp:Label ID="ViewExternalAssessorProfile_skillsDatalistCategory" runat="server" SkinID="sknProfileTitleField"
                                                                                                                                    Text='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 90%; padding-left: 30px" align="left">
                                                                                                                                <asp:Label ID="ViewExternalAssessorProfile_skillsDatalistSkills" runat="server" SkinID="sknProfileValueField"
                                                                                                                                    Text='<%# Eval("Skills") %>'></asp:Label>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <td style="height: 8px">
                                                                                                                        </td>
                                                                                                                    </table>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:DataList>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                        <div id="ViewExternalAssessorProfile_assessmentDetailsDiv" runat="server" style="display: none">
                                                                                            <table cellpadding="10" cellspacing="0" style="width: 100%">
                                                                                                <tr>
                                                                                                    <td align="center" class="empty_border_grid_body_bg">
                                                                                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                                                                            <tr>
                                                                                                                <td align="left" style="width: 100%">
                                                                                                                    <div style="height: 258px; overflow: auto;">
                                                                                                                        <asp:GridView ID="ViewExternalAssessorProfile_assessmentDetailsGridView" runat="server" AllowSorting="False"
                                                                                                                            AutoGenerateColumns="False" Height="100%" Width="100%" SkinID="sknAssessorProfileGridView">
                                                                                                                            <Columns>
                                                                                                                                <asp:BoundField DataField="ClientName" HeaderText="Client" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                                <asp:BoundField DataField="CompletedDateString" HeaderText="Date" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                                <asp:BoundField DataField="InterviewName" HeaderText="Interview" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                                <asp:BoundField DataField="CandidateName" HeaderText="Candidate" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                                <asp:BoundField DataField="AssessmentStatus" HeaderText="Status" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                            </Columns>
                                                                                                                            <EmptyDataTemplate>
                                                                                                                                <table style="width: 100%; height: 100%">
                                                                                                                                    <tr>
                                                                                                                                        <td style="height: 50px">
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td align="center" class="error_message_text_normal" style="height: 100%" valign="middle">
                                                                                                                                            No assessment details available
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </EmptyDataTemplate>
                                                                                                                        </asp:GridView>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <uc1:PageNavigator ID="ViewAsssessorProfile_assessmentDetailsPageNavigator" runat="server" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="ViewExternalAssessorProfile_bottomControlsUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="left">
                                    <asp:LinkButton ID="ViewExternalAssessorProfile_cancelLinkButton" runat="server" Text="Cancel"
                                        SkinID="sknPopupLinkButton" OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window"> </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_2">
            </td>
        </tr>
    </table>
</asp:Content>
