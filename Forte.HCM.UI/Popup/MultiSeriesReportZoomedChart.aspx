﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MultiSeriesReportZoomedChart.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.MultiSeriesReportZoomedChart" %>   
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="MultiSeriesReportZoomedChart_content" ContentPlaceHolderID="PopupMaster_contentPlaceHolder"
    runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 90%" class="popup_header_text_grey">
                            <asp:Literal ID="MultiSeriesReportZoomedChart_headerLiteral" runat="server" Text=""></asp:Literal>
                        </td>
                        <td style="width: 10%" align="right">
                            <asp:ImageButton ID="MultiSeriesReportZoomedChart_topCloseImageButton" runat="server"
                                SkinID="sknCloseImageButton" OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="MultiSeriesReportZoomedChart_topSuccessMessageLabel" runat="server"
                    SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="MultiSeriesReportZoomedChart_topErrorMessageLabel" runat="server"
                    SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" class="popupcontrol_question_inner_bg">
                            <div style="height: 460px; overflow: auto; width: 900px">
                                <asp:Chart ID="MultiSeriesReportZoomedChart_chart" runat="server" BackColor="Transparent"
                                    Palette="Pastel" Width="900px" Height="450px" AntiAliasing="Graphics">
                                    <Legends>
                                        <asp:Legend Name="MultiSeriesReportZoomedChart_legend" BackColor="Transparent" IsEquallySpacedItems="True"
                                            Font="Verdana, 9.25pt" IsTextAutoFit="False" Alignment="Center" ForeColor="114, 142, 192"
                                            Docking="Top" IsDockedInsideChartArea="False" ItemColumnSpacing="80" MaximumAutoSize="100"
                                            TextWrapThreshold="100">
                                        </asp:Legend>
                                    </Legends>
                                    <Titles>
                                        <asp:Title Name="MultiSeriesReportZoomedChart_chartTitle">
                                        </asp:Title>
                                    </Titles>
                                    <Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="MultiSeriesReportZoomedChart_chartArea" BorderColor="64, 64, 64, 64"
                                            BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent"
                                            BackGradientStyle="TopBottom">
                                            <AxisY>
                                                <MajorGrid Enabled="False" LineDashStyle="DashDotDot" />
                                                <LabelStyle ForeColor="180, 65, 140, 240" Angle="90" />
                                            </AxisY>
                                            <AxisX Interval="1">
                                                <MajorGrid Enabled="False" LineDashStyle="DashDotDot" />
                                                <LabelStyle ForeColor="180, 65, 140, 240" />
                                            </AxisX>
                                            <AxisX2 TitleForeColor="180, 65, 140, 240">
                                            </AxisX2>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_10">
                <asp:LinkButton ID="MultiSeriesZoomedChart_cancelLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClientClick="javascript:CloseMe()" />
            </td>
        </tr>
    </table>
</asp:Content>
