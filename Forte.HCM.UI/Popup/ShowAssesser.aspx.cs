﻿using System;
using System.Web.UI;
using Forte.HCM.UI.Common;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

namespace Forte.HCM.UI.Popup
{
    public partial class ShowAssesser : PageBase
    {
        #region Event Handlers

        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Show Assesser");
                
                // Set default button and focus.
                Page.Form.DefaultButton = ShowAssesser_topSearchButton.UniqueID;

                if (!Utility.IsNullOrEmpty(ShowAssesser_isMaximizedHiddenField.Value) &&
                        ShowAssesser_isMaximizedHiddenField.Value == "Y")
                {
                    ShowAssesser_searchCriteriasDiv.Style["display"] = "none";
                    ShowAssesser_searchResultsUpSpan.Style["display"] = "block";
                    ShowAssesser_searchResultsDownSpan.Style["display"] = "none";
                    ShowAssesser_questionDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    ShowAssesser_searchCriteriasDiv.Style["display"] = "block";
                    ShowAssesser_searchResultsUpSpan.Style["display"] = "none";
                    ShowAssesser_searchResultsDownSpan.Style["display"] = "block";
                    ShowAssesser_questionDiv.Style["height"] = RESTORED_HEIGHT;
                }


                //CategorySubjectLookup_searchTestResultsTR.Attributes.Add("onclick",
                //  "ExpandOrRestore('" +
                //  .ClientID + "','" +
                //  CategorySubjectLookup_searchCriteriasDiv.ClientID + "','" +
                //  CategorySubjectLookup_searchResultsUpSpan.ClientID + "','" +
                //  CategorySubjectLookup_searchResultsDownSpan.ClientID + "','" +
                //  CategorySubjectLookup_isMaximizedHiddenField.ClientID + "','" +
                //  RESTORED_HEIGHT + "','" +
                //  EXPANDED_HEIGHT + "')");
                
                ShowAssesser_ShowAssesserResultsTR.Attributes.Add("onclick",
                   "ExpandOrRestore('" +
                   ShowAssesser_questionDiv.ClientID + "','" +
                   ShowAssesser_SearchDivUpdatePanel.ClientID + "','" +
                   ShowAssesser_searchResultsUpSpan.ClientID + "','" +
                   ShowAssesser_searchResultsDownSpan.ClientID + "','" +
                   ShowAssesser_isMaximizedHiddenField.ClientID + "','" +
                   RESTORED_HEIGHT + "','" +
                   EXPANDED_HEIGHT + "')");

                //Subscribe page number click event 
                ShowAssesser_bottomPagingNavigator.PageNumberClick += new PageNavigator.PageNumberClickEventHandler(ShowAssesser_bottomPagingNavigator_PageNumberClick);

                if (!IsPostBack)
                {
                   
                    // Set default button and focus.
                    Page.Form.DefaultButton = ShowAssesser_topSearchButton.UniqueID;
                    ShowAssesser_firstNameTextBox.Focus();

                    Session["ASSESSOR"] = null;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "AssessorID";
               
                
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ShowAssesser_topErrorMessageLabel,
                    exp.Message.ToString());
            }
        }


        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void ShowAssesser_bottomPagingNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            try
            {
                //Load the grid with corresponding page number
                SearchAssessorCriteria(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(ShowAssesser_topErrorMessageLabel,exception.Message);
            }
        }

        protected void ShowAssesser_topSearchButton_Click(object sender, EventArgs e)
        {
          
            try
            {
                // Clear messages.
                //ShowAssesser_topErrorMessageLabel.Text = string.Empty;
                //ShowAssesser_topSuccessMessageLabel.Text = string.Empty;
               
                ClearMessages();
                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "CATEGORYID";

                // Reset the paging control.
              //  CategorySubjectLookup_pagingNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                SearchAssessorCriteria(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(ShowAssesser_topErrorMessageLabel,
                    exception.Message);
            }


        }

        protected void ShowAssesser_addAssessorButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ClearMessages();

                List<TestSessionSearchCriteria> assessorDetails = null;

                if (Session["ASSESSOR"] == null)
                    assessorDetails = new List<TestSessionSearchCriteria>();
                else
                    assessorDetails = Session["ASSESSOR"] as List<TestSessionSearchCriteria>;

                for (int i = 0; i < ShowAssesser_assesserDetailsGridView.Rows.Count; i++)
                {
                    TestSessionSearchCriteria searchAssessor = new TestSessionSearchCriteria();
                    CheckBox ShowAssesser_selectCheckbox = (CheckBox)ShowAssesser_assesserDetailsGridView.Rows[i].FindControl("ShowAssesser_selectCheckbox");
                    if (ShowAssesser_selectCheckbox.Checked == true)
                    {
                        searchAssessor.AssessorFirstName = ((Label)ShowAssesser_assesserDetailsGridView.Rows[i].FindControl("ShowAssesser_firstNameLabel")).Text;
                        searchAssessor.AssessorLastName = ((Label)ShowAssesser_assesserDetailsGridView.Rows[i].FindControl("ShowAssesser_lastNameLabel")).Text;
                        searchAssessor.Email = ((Label)ShowAssesser_assesserDetailsGridView.Rows[i].FindControl("ShowAssesser_emailLabel")).Text;
                        searchAssessor.AssessorID = Convert.ToInt32(((HiddenField)ShowAssesser_assesserDetailsGridView.Rows[i].FindControl("ShowAssesser_assessorIDHiddenField")).Value);
                        searchAssessor.UserID = Convert.ToInt32(((HiddenField)ShowAssesser_assesserDetailsGridView.Rows[i].FindControl("ShowAssesser_userIDHiddenField")).Value);
                        assessorDetails.Add(searchAssessor);
                    }
                }
                if (assessorDetails.Count == 0)
                {
                    base.ShowMessage(ShowAssesser_topErrorMessageLabel,
                         "Select atleast one assessor");
                    return;
                }
                Session["ASSESSOR"] = assessorDetails;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Closeform", "<script language='javascript'>self.close();</script>", false);

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ShowAssesser_topErrorMessageLabel,
                    exp.Message.ToString());
            }
        }


        /// <summary>
        /// Handler method that will be called when the reset button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void ShowAssesserLookup_resetLinkButton_Click(object sender, EventArgs e)
        {

            try
            {
                ShowAssesser_firstNameTextBox.Text = string.Empty;
                ShowAssesser_lastNameTextBox.Text = string.Empty;
                ShowAssesser_skillTextBox.Text = string.Empty;
                ShowAssesser_fromdateTextbox.Text = string.Empty;
                ShowAssesser_todateTextbox.Text = string.Empty;


                ShowAssesser_assesserDetailsGridView.DataSource = null;
                ShowAssesser_assesserDetailsGridView.DataBind();

                ShowAssesser_bottomPagingNavigator.PageSize = base.GridPageSize;
                ShowAssesser_bottomPagingNavigator.TotalRecords = 0;

                ShowAssesser_questionDiv.Visible = false;

               //  Reset default sort field and order keys.

                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "CATEGORYID";

               //   Reset to the restored state.

                if (ShowAssesser_isMaximizedHiddenField.Value == "Y")
                {
                    ShowAssesser_searchCriteriasDiv.Style["display"] = "block";
                    ShowAssesser_searchResultsUpSpan.Style["display"] = "none";
                    ShowAssesser_searchResultsDownSpan.Style["display"] = "block";
                   // ShowAssesser_searchCriteriasDiv.Style["height"] = RESTORED_HEIGHT;  
                    ShowAssesser_isMaximizedHiddenField.Value = "N";
                }

                // Clear message fields.
                ShowAssesser_topSuccessMessageLabel.Text = string.Empty;
                ShowAssesser_topErrorMessageLabel.Text = string.Empty;

               // Set default focus.

                Page.Form.DefaultFocus = ShowAssesser_firstNameTextBox.UniqueID;
                ShowAssesser_firstNameTextBox.Focus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(ShowAssesser_topErrorMessageLabel, exp.Message);
            }



        }


        #endregion Event Handlers

        #region Private Methods

        private void SearchAssessorCriteria(int pageNumber)
        {
            TestSessionSearchCriteria searchAssessor = new TestSessionSearchCriteria();

            searchAssessor.AssessorFirstName = ShowAssesser_firstNameTextBox.Text;
            searchAssessor.AssessorLastName = ShowAssesser_lastNameTextBox.Text;
            searchAssessor.Skill = ShowAssesser_skillTextBox.Text;
            ShowAssesser_MaskedEditValidator.Validate();
            ShowAssesser_todateMaskedEditValidator.Validate();

            if (ShowAssesser_fromdateTextbox.Text != "")
            {
                if (!ShowAssesser_MaskedEditValidator.IsValid)
                {
                    base.ShowMessage(ShowAssesser_topErrorMessageLabel,
                        ShowAssesser_todateMaskedEditValidator.ErrorMessage);
                    return;
                }
                searchAssessor.FromDate = Convert.ToDateTime(ShowAssesser_fromdateTextbox.Text).
                    AddDays(1).ToString("MM/dd/yyyy");
            }
            else
                searchAssessor.FromDate = "1/1/1753";

            if (ShowAssesser_todateTextbox.Text != "")
            {
                if (!ShowAssesser_todateMaskedEditValidator.IsValid)
                {
                    base.ShowMessage(ShowAssesser_topErrorMessageLabel,
                        ShowAssesser_todateMaskedEditValidator.ErrorMessage);
                    return;
                }
                searchAssessor.ToDate = Convert.ToDateTime(ShowAssesser_todateTextbox.Text).
                    AddDays(1).ToString("MM/dd/yyyy");
            }
            else
                searchAssessor.ToDate = "12/31/9999";
            searchAssessor.UserID = base.userID;
            int totalRecords = 0;

            List<TestSessionSearchCriteria> assessorDetails =
               new InterviewSchedulerBLManager().SearchAssessorDetails
               (searchAssessor, pageNumber, base.GridPageSize,
               out totalRecords, ViewState["SORT_FIELD"].ToString(), ((SortType)ViewState["SORT_ORDER"]));


            if (assessorDetails != null)
            {

                if (assessorDetails.Count == 0)
                {
                    base.ShowMessage(ShowAssesser_topErrorMessageLabel, "No data found to display");
                    return;
                }

            
                ShowAssesser_searchCriteriasDiv.Style["display"] = "block";
                ShowAssesser_searchResultsUpSpan.Style["display"] = "none";
                ShowAssesser_searchResultsDownSpan.Style["display"] = "block";
                ShowAssesser_questionDiv.Style["height"] = RESTORED_HEIGHT;


                ShowAssesser_assesserDetailsGridView.DataSource = assessorDetails;
                ShowAssesser_assesserDetailsGridView.DataBind();

                ShowAssesser_bottomPagingNavigator.PageSize = GridPageSize;
                ShowAssesser_bottomPagingNavigator.TotalRecords = totalRecords;

                ShowAssesser_questionDiv.Visible = true;
                
               

            }
            else {
                base.ShowMessage(ShowAssesser_topErrorMessageLabel, "No data found to display");
                return;
            }
        }

        /// <summary>
        /// Method that clears the messages.
        /// </summary>
        private void ClearMessages()
        {
            ShowAssesser_topErrorMessageLabel.Text = string.Empty;
            ShowAssesser_topSuccessMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}