<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PopupMaster.Master"
    CodeBehind="ApproveCandidateSelfSkill.aspx.cs" Title="Approve Candidate Self Skill"
    Inherits="Forte.HCM.UI.Popup.ApproveCandidateSelfSkill" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ApproveCandidateSelfSkill_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript">
        function GridRadioCheck(rb)
        {
            var gv = document.getElementById("<%=ApproveCandidateSelfSkill_skillGridView.ClientID%>");
            var rbs = gv.getElementsByTagName("input");

            var row = rb.parentNode.parentNode;
            for (var i = 0; i < rbs.length; i++)
            {
                if (rbs[i].type == "radio")
                {
                    if (rbs[i].checked && rbs[i] != rb)
                    {
                        rbs[i].checked = false;
                        break;
                    }
                }
            }
        }

        // Handler method that will be called when the 'add/rejected' button is 
        // clicked in the page.
        function OnClick()
        {
            var btncnrl = '<%= Request.QueryString["ctrlId"] %>';

            if (window.dialogArguments)
            {
                window.opener = window.dialogArguments;
            }

            // Trigger the click event of the refresh button in the parent page.
            if (btncnrl != null && window.opener.document.getElementById(btncnrl) != null)
                window.opener.document.getElementById(btncnrl).click();

            self.close();
        }

    </script>
    <asp:UpdatePanel ID="ShowRecommendAssessor_pageUpdatePanel" runat="server">
        <ContentTemplate>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="popup_td_padding_10" colspan="2">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="width: 50%" class="popup_header_text_grey">
                                    <asp:Literal ID="ApproveCandidateSelfSkill_headerLiteral" runat="server" Text="Approve/Reject Skill"></asp:Literal>
                                </td>
                                <td style="width: 50%" align="right">
                                    <asp:ImageButton ID="ApproveCandidateSelfSkill_topCancelImageButton" runat="server"
                                        SkinID="sknCloseImageButton" OnClientClick="javascript:CloseMe()" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="popup_td_padding_10" valign="top" colspan="2">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="popupcontrol_question_inner_bg">
                            <tr>
                                <td class="msg_align" style="height: 20px">
                                    <asp:UpdatePanel ID="ApproveCandidateSelfSkill_messageUpdatePanel" runat="server"
                                        UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Label ID="ApproveCandidateSelfSkill_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                            <asp:Label ID="ApproveCandidateSelfSkill_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-left: 5px;
                                        padding-right: 5px">
                                        <!-- Related Skills -->
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:UpdatePanel ID="ApproveCandidateSelfSkill_searchResultsUpdatePanel" runat="server"
                                                    UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="header_bg" style="width: 100%">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="width: 100%" align="left" class="header_text_bold">
                                                                                <asp:Literal ID="ApproveCandidateSelfSkill_UpdateLiteral" runat="server" Text="Update"></asp:Literal>&nbsp;<asp:Label
                                                                                    ID="ApproveCandidateSelfSkill_updateLabelHelp" runat="server" SkinID="sknLabelText"
                                                                                    Text="- Select the skill and click update to relate with an existing skill"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" class="grid_body_bg">
                                                                    <div id="CandidateSelfSkill_Div" runat="server">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <div id="ApproveCandidateSelfSkill_resultDiv" runat="server" style="height: 120px;
                                                                                        overflow: auto;">
                                                                                        <asp:GridView ID="ApproveCandidateSelfSkill_skillGridView" runat="server" AllowSorting="True"
                                                                                            AutoGenerateColumns="False" GridLines="None" Width="25%" OnRowDataBound="ApproveCandidateSelfSkill_skillGridView_RowDataBound">
                                                                                            <EmptyDataRowStyle CssClass="error_message_text" />
                                                                                            <EmptyDataTemplate>
                                                                                                <table style="width: 100%">
                                                                                                    <tr>
                                                                                                        <td style="height: 110px">    
                                                                                                            No related skills found to display
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                
                                                                                            </EmptyDataTemplate>
                                                                                            <Columns>
                                                                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="10px" ItemStyle-Width="10px">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:RadioButton ID="ApproveCandidateSelfSkill_skillRadioButton" runat="server" onclick="GridRadioCheck(this);" />
                                                                                                        <asp:HiddenField ID="ApproveCandidateSelfSkill_skillIDHiddenField" runat="server"
                                                                                                            Value='<%# Eval("SkillID") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField HeaderText="Skill" DataField="SkillName" HeaderStyle-HorizontalAlign="Left" />
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:Button ID="ApproveCandidateSelfSkill_changeSkillButton" runat="server" Text="Update"
                                                                                        SkinID="sknButtonId" OnClick="ApproveCandidateSelfSkill_changeSkillButton_Click" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <!-- End Related Skills -->
                                        <tr>
                                            <td class="td_height_5">
                                            </td>
                                        </tr>
                                        <!-- New Skill -->
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="ApproveCandidateSelfSkill_newItemUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="header_bg">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="width: 100%" align="left" class="header_text_bold">
                                                                                <asp:Literal ID="ApproveCandidateSelfSkill_newItemHeader" runat="server" Text="Add"></asp:Literal>&nbsp;<asp:Label
                                                                                    ID="ApproveCandidateSelfSkill_newItemHelpLabel" runat="server" SkinID="sknLabelText"
                                                                                    Text="- Click add new to add as a new skill"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" class="grid_body_bg">
                                                                    <div id="Div1" runat="server">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td align="left" style="width: 190px">
                                                                                    <asp:TextBox ID="ApproveCandidateSelfSkill_newSkillNameTextBox" runat="server" MaxLength="50"
                                                                                        Width="230px"></asp:TextBox>
                                                                                </td>
                                                                                <td style="padding-left: 18px; width: 100px;" align="left">
                                                                                    <asp:Button ID="ApproveCandidateSelfSkill_addNewSkillButton" runat="Server" Text="Add New"
                                                                                        SkinID="sknButtonId" OnClick="ApproveCandidateSelfSkill_addNewSkillButton_Click" />
                                                                                </td>
                                                                                <td align="left">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <%--<asp:AsyncPostBackTrigger ControlID="CandidateSelfSkill_searchButton" />--%>
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <!--End New Skill -->
                                        <tr>
                                            <td style="height: 10px">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="popup_td_padding_left_8">
                        <table>
                            <tr>
                                <td>
                                    <asp:Button ID="ApproveCandidateSelfSkill_rejectSkillButton" runat="server" Text="Reject"
                                        SkinID="sknButtonId" OnClick="ApproveCandidateSelfSkill_rejectSkillButton_Click" />
                                </td>
                                <td >
                                    <asp:LinkButton ID="ApproveCandidateSelfSkill_topResetLinkButton" runat="server"
                                        SkinID="sknPopupLinkButton" Text="Reset" OnClick="ApproveCandidateSelfSkill_resetButton_Click" />
                                </td>
                                <td class="pop_divider_line">
                                    |
                                </td>
                                <td>
                                    <asp:LinkButton ID="ApproveCandidateSelfSkill_topCancelLinkButton" runat="server"
                                        Text="Cancel" SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();" />
                                </td>
                            </tr>
                        </table>
                        <asp:HiddenField ID="ApproveCandidateSelfSkill_isMaximizedHiddenField" runat="server" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
