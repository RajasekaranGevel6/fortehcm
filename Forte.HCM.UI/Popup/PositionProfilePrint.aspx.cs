﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PositionProfilePrint.cs
// File that represents the user interface to send emails along with 
// attachments to the recipients. This includes position profile, 
// position profile status, etc

#endregion Header

#region Directives                                                             

using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data;
using Forte.HCM.Utilities;


#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface to send emails along with 
    /// attachments to the recipients. This includes position profile, 
    /// position profile status, etc. This class inherits 
    /// Forte.HCM.UI.Common.PageBase class.    
    /// </summary>
    public partial class PositionProfilePrint : PageBase
    {
        #region Static Variables                                               

        /// <summary>
        /// A <see cref="BaseColor"/> that holds the item color.
        /// </summary>
        static BaseColor itemColor = new BaseColor(20, 142, 192);

        /// <summary>
        /// A <see cref="BaseColor"/> that holds the header color.
        /// </summary>
        static BaseColor headerColor = new BaseColor(40, 48, 51);

        /// <summary>
        /// A <see cref="BaseColor"/> that holds the item black color.
        /// </summary>
        static BaseColor itemBlackColor = new BaseColor(0, 0, 0); 


        #endregion Static Variables

        #region Private Variables                                              

        /// <summary>
        /// A <see cref="string"/> that holds the chart image path.
        /// </summary>
        private string chartImagePath = string.Empty;
        /// <summary>
        /// A <see cref="int"/> that holds the header detail table index.
        /// </summary>
        private const int ADDITIONAL_DOCUMENT_TABLE_INDEX = 0;
        /// <summary>
        /// A <see cref="string"/> that holds the base URL.
        /// </summary>
        private string baseURL = string.Empty;
        /// <summary>
        /// A <see cref="int"/> that hold the position profile ID.
        /// </summary>
        private int positionProfileID = 0;

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing of
        /// position profile status change email alert.
        /// </summary>
        private delegate void PositionProfileStatusDelegate();

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing of
        /// candidate status change email alert.
        /// </summary>
        private delegate void CandidateStatusDelegate(int ID);
        #endregion Private Variables

        #region Event Handlers                                                 

        /// <summary>
        /// Method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set title.
                SetTitle();
                 
                PositionProfilePrint_submitButton.Attributes.Add("onclick", "javascript:return IsValidEmail('"
                       + PositionProfilePrint_ccTextBox.ClientID + "','"
                       + PositionProfilePrint_errorMessageLabel.ClientID + "','"
                       + PositionProfilePrint_successMessageLabel.ClientID + "');");

                // Add handler to open 'select contact' popup.
                string positionProfileID = string.Empty;
                if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                    positionProfileID = Request.QueryString["positionprofileid"].Trim();

                string fillKeyword = string.Empty;
                if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].Trim().ToUpper() == "PP_STAT")
                {
                    fillKeyword = "Y";
                }

                if (Utility.IsNullOrEmpty(Request.QueryString["submittype"]))
                {
                    PositionProfilePrint_toAddressImageButton.Attributes.Add
                        ("onclick", "javascript:return ShowSelectContactPopup('" +
                        PositionProfilePrint_toTextBox.ClientID + "','" + positionProfileID + "','To','" + fillKeyword + "')");

                    PositionProfilePrint_ccAddressImageButton.Attributes.Add
                        ("onclick", "javascript:return ShowSelectContactPopup('" +
                        PositionProfilePrint_ccTextBox.ClientID + "','" + positionProfileID + "','CC','" + fillKeyword + "')");
                }
                else
                {
                    PositionProfilePrint_toAddressImageButton.Attributes.Add
                        ("onclick", "javascript:return ShowSelectContactPopupForSubmit('" +
                        PositionProfilePrint_toTextBox.ClientID + "','" + positionProfileID + "','To','" + fillKeyword + "','" + Request.QueryString["submittype"] + "')");

                    PositionProfilePrint_ccAddressImageButton.Attributes.Add
                        ("onclick", "javascript:return ShowSelectContactPopupForSubmit('" +
                        PositionProfilePrint_ccTextBox.ClientID + "','" + positionProfileID + "','CC','" + fillKeyword + "','" + Request.QueryString["submittype"] + "')");
                }

                if (!IsPostBack)
                {
                    if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "PP_STAT")
                    {
                        PositionProfileDashboard positionProfileDashboard = new PositionProfileDashboard();
                        positionProfileDashboard = new PositionProfileBLManager().GetPositionProfileStatus(Convert.ToInt32( Request.QueryString["positionprofileid"]));

                        PositionProfilePrint_subjectTextBox.Text = positionProfileDashboard.PositionProfileName;
                        PositionProfilePrint_attachmentTR.Visible = false;
                        BindAttachement();
                        PositionProfilePrint_editor.Content =
                        Session[Constants.SessionConstants.PP_STATUS_EMAIL_CONTENT].ToString().Replace
                           ("none", "block").Replace("  ", "").Replace
                           ("    ", "").Replace("Cancel</a>", "</a>");
                       
                        PositionProfilePrint_additionaldocuments.Visible = true;
                        BindAdditionalDocument();
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "PP_REVIEW")
                    {
                        // Construct the session key.
                        string sessionKey = "POSITION_PROFILE_REVIEW_" + positionProfileID;

                        PositionProfilePrint_editor.Content = Session[sessionKey].ToString();
                        //Session[Constants.SessionConstants.PP_STATUS_EMAIL_CONTENT].ToString().Replace
                         //  ("none", "block").Replace("  ", "").Replace
                          // ("    ", "").Replace("Cancel</a>", "</a>");
                        PositionProfilePrint_attachmentTR.Visible = false;
                    }
                    else
                    {
                        PositionProfilePrint_editor.Content =
                            Session[Constants.SessionConstants.PP_EMAIL_CONTENT].ToString().Replace
                            ("none", "block").Replace("  ", "").Replace
                            ("    ", "").Replace("border-width:0px;", "display:none").Replace("Cancel</a>", "</a>");
                    }

                    Page.Form.DefaultFocus = PositionProfilePrint_toTextBox.UniqueID;
                    PositionProfilePrint_toTextBox.Focus();
                    UserDetail userDetail = new CommonBLManager().GetUserDetail(base.userID);
                    if (userDetail != null)
                    {
                        //PositionProfilePrint_fromValueLabel.Text = userDetail.FirstName
                        //    + " &lt;" + userDetail.Email + "&gt;";
                        PositionProfilePrint_fromValueLabel.Text = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                        PositionProfilePrint_fromHiddenField.Value = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                    }
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(PositionProfilePrint_errorMessageLabel, exp.Message);
                Forte.HCM.Trace.Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that will be called when the submit button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void PositionProfilePrint_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                string baseURL = ConfigurationManager.AppSettings["DEFAULT_URL"];

                Response.Cache.SetCacheability(HttpCacheability.NoCache);

                //Load Style Sheets and Skin
                if (!Utility.IsNullOrEmpty(baseURL))
                    chartImagePath = baseURL + "chart/";
                // Clear all the error and success labels
                PositionProfilePrint_errorMessageLabel.Text = string.Empty;
                PositionProfilePrint_successMessageLabel.Text = string.Empty;
                // Validate the fields
                if (!IsValidData())
                    return;
                // Build Mail Details


                MailDetail mailDetail = new MailDetail();
                List<MailAttachment> mailAttachmentLists = new List<MailAttachment>();
                if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "PP_STAT")
                {
                    foreach (GridViewRow row in PositionProfilePrint_attachmentGridView.Rows)
                    {
                        var resumeCheckBox = row.FindControl("PositionProfilePrint_attachmentGridViewResumeCheckBox") as CheckBox;
                        var testDetailsCheckBox = row.FindControl("PositionProfilePrint_attachmentGridViewTestDetailsCheckBox") as CheckBox;

                        var candidateID =
                           (HiddenField)row.FindControl("PositionProfilePrint_attachmentGridViewCandidateIDHiddenField");

                        var poistionProfileCandidateID =
                            (HiddenField)row.FindControl("PositionProfilePrint_attachmentGridViewPositionProfileCandidateIDHiddenField");


                        var interviewDetailsCheckBox = 
                            row.FindControl("PositionProfilePrint_attachmentGridViewInterviewDetailsCheckBox") as CheckBox;

                        CandidateInformation candidateInformation = new CandidateInformation();

                        candidateInformation = new PositionProfileBLManager().GetPositionProfileCandidateInformation(int.Parse(poistionProfileCandidateID.Value));
                        if (candidateInformation != null)
                        {
                            if (testDetailsCheckBox.Checked)
                            {
                                if (candidateInformation.TestKey != null)
                                {
                                    MailAttachment mailAttachment = new MailAttachment();
                                    mailAttachment.FileContent = ConstructPDF(candidateInformation.candidateSessionID, candidateInformation.TestKey, candidateInformation.AttemptID.ToString());
                                    mailAttachment.FileName = candidateInformation.candidateSessionID + "_" + candidateInformation.TestKey + candidateInformation.AttemptID + ".pdf";
                                    mailAttachmentLists.Add(mailAttachment);
                                }
                            }
                            if (resumeCheckBox.Checked)
                            {
                                string mimeType;
                                string fileName;
                                byte[] resumeContent = new CandidateBLManager().GetResumeContentsByCandidiateID(int.Parse(candidateID.Value), out mimeType, out fileName);
                                MailAttachment mailAttachment = new MailAttachment();
                                mailAttachment.FileContent = resumeContent;
                                mailAttachment.FileName = fileName;
                                mailAttachmentLists.Add(mailAttachment);
                            }

                            if (interviewDetailsCheckBox != null)
                                if (interviewDetailsCheckBox.Checked)
                                {
                                    if (candidateInformation.InterviewCandidateSessionID != null && candidateInformation.InterviewAttemptID > 0)
                                    {
                                        MailAttachment mailAttachment_interviewDetail = new MailAttachment();
                                        mailAttachment_interviewDetail.FileContent = GeneretePDF_AssessorSummaryRating(candidateInformation.InterviewCandidateSessionID,
                                            candidateInformation.InterviewAttemptID);
                                        mailAttachment_interviewDetail.FileName = candidateInformation.caiFirstName + "_" + "Interview Result" + ".pdf";
                                        mailAttachmentLists.Add(mailAttachment_interviewDetail);
                                    }
                                }
                             
                        }
                    }


                    foreach (GridViewRow row in Positionprofileprint_additionaldocument.Rows)
                    {
                        var documentCheckBox = row.FindControl("PositionProfilePrint_additionaldocumentGridViewResumeCheckBox") as CheckBox;                      

                        var documentId =
                           (HiddenField)row.FindControl("PositionProfilePrint_additionalattachmentGridViewCandidateIDHiddenField");                 

                        if(!Utility.IsNullOrEmpty(documentId))
                            if(documentCheckBox.Checked)
                            {                               
                                string fileName = string.Empty;                               
                                int documentId2 = int.Parse(documentId.Value);
                                byte[] resumeContent = new CandidateBLManager().GetDocumentContentsByDocumentId(documentId2, out fileName);
                                MailAttachment mailAttachment = new MailAttachment();
                                mailAttachment.FileContent = resumeContent;
                                mailAttachment.FileName = fileName;
                                mailAttachmentLists.Add(mailAttachment);
                            }
                    }
                }
                mailDetail.Attachments = mailAttachmentLists;
                mailDetail.To = new List<string>();
                mailDetail.CC = new List<string>();

                // Add the logged in user in the CC list when 'send me a copy ' is on
                if (PositionProfilePrint_sendMeACopyCheckBox.Checked)
                {
                    mailDetail.CC.Add(base.userEmailID);
                }

                mailDetail.Message = PositionProfilePrint_editor.Content.Trim().Replace("<br />", "");
                mailDetail.isBodyHTML = true;
                // Add 'To' mail addresses
                string[] toMailIDs = PositionProfilePrint_toTextBox.Text.Split(new char[] { ',' });
                for (int idx = 0; idx < toMailIDs.Length; idx++)
                    mailDetail.To.Add(toMailIDs[idx].Trim());

                if (!Utility.IsNullOrEmpty(PositionProfilePrint_ccTextBox.Text.Trim()))
                    mailDetail.CC.Add(PositionProfilePrint_ccTextBox.Text.Trim());

                //mailDetail.From = PositionProfilePrint_fromHiddenField.Value;
                // Set the 'from' address to default address.
                mailDetail.From = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];

                mailDetail.Subject = PositionProfilePrint_subjectTextBox.Text.Trim();
                mailDetail.isBodyHTMLPositionProfile = false;

                // Send mail
                bool isMailSent = false;
                try
                {
                    // Send mail
                    new EmailHandler().SendMail(mailDetail);
                    isMailSent = true;

                    string[] candidateIds = null;

                    if (!Utility.IsNullOrEmpty(Session["SELECTED_CANDIDATES"]))
                        candidateIds = Session["SELECTED_CANDIDATES"].ToString().Split(',');

                    if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]) &&
                        !Utility.IsNullOrEmpty(Request.QueryString["submittype"]))
                    {
                        if (Request.QueryString["submittype"].ToString() == "C" && candidateIds.Length>0)
                        {
                            foreach(string candidateid in candidateIds)
                            {
                                new PositionProfileBLManager().UpdatePositionProfileStatus("PCS_SUB", null, 
                                base.userID, Convert.ToInt32(Request.QueryString["positionprofileid"]), Convert.ToInt32(candidateid), "POSITION_ID");

                                CandidateStatusDelegate taskDelegate = new CandidateStatusDelegate(SendCandidateStatusChangeAlertEmail);
                                IAsyncResult result = taskDelegate.BeginInvoke(Convert.ToInt32(candidateid),
                                    new AsyncCallback(SendCandidateStatusChangeAlertEmailCallBack), taskDelegate);
                            } 
                        }
                    }
                   
                }
                catch (Exception exp)
                {
                    Forte.HCM.Trace.Logger.ExceptionLog(exp);
                    isMailSent = false;
                }
                // Script  that close the popup window
                //string closeScript = "self.close();"; 
                 

                if (isMailSent)
                { 
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", "javascript:CloseUpdate();", true); 
                   
                }
                else
                    base.ShowMessage(PositionProfilePrint_errorMessageLabel,
                        Resources.HCMResource.EmailAttachment_MailCouldNotSend);
            }
            catch (Exception exp)
            {
                base.ShowMessage(PositionProfilePrint_errorMessageLabel, exp.Message);
                Forte.HCM.Trace.Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that will be called when the row data bound event is fired in 
        /// the attachment grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfilePrint_attachmentGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //Download resume details
                    ImageButton PositionProfilePrint_resumeAttachmentImageButton =
                            (ImageButton)e.Row.FindControl("PositionProfilePrint_resumeAttachmentImageButton");

                    HiddenField PositionProfilePrint_attachmentGridViewCandidateIDHiddenField = 
                        (HiddenField)e.Row.FindControl("PositionProfilePrint_attachmentGridViewCandidateIDHiddenField");

                    PositionProfilePrint_resumeAttachmentImageButton.Attributes.Add("onclick",
                        "return DownloadResume('" + PositionProfilePrint_attachmentGridViewCandidateIDHiddenField.Value + "')");
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(PositionProfilePrint_errorMessageLabel, exp.Message);
                Forte.HCM.Trace.Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that will be called when the row command event is fired in 
        /// the attachment grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfilePrint_attachmentGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DownloadTestResult")
                {
                    string[] candidateTestDetail = e.CommandArgument.ToString().Split(',');
                    Response.Redirect("../PrintPages/TestResultPrint.aspx?candidatesession="
                   + candidateTestDetail[0]
                   + "&attemptid=" + candidateTestDetail[2]
                   + "&testkey=" + candidateTestDetail[1]
                   + "&downloadoremail=D", false);
                }
                else if (e.CommandName == "DownloadInterviewResult")
                {
                    string[] candidateTestDetail = e.CommandArgument.ToString().Split(',');
                    Response.Redirect("../PrintPages/TestResultPrint.aspx?candidatesession="
                   + candidateTestDetail[0]
                   + "&attemptid=" + candidateTestDetail[1] 
                   + "&downloadoremail=INT", false);
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(PositionProfilePrint_errorMessageLabel, exp.Message);
                Forte.HCM.Trace.Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// On row databound method to download the documents when clicking them
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AdditionalDocument_gridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    ImageButton AdditionalDocument_gridView_ppdocumentImageButton =
                        (ImageButton)e.Row.FindControl("PositionProfilePrint_documentAttachmentImageButton");

                    HiddenField AdditionalDocument_gridView_documentIdHiddenField = (HiddenField)e.Row.FindControl("PositionProfilePrint_additionalattachmentGridViewCandidateIDHiddenField");
                    string type="DownloadDocuments";
                    AdditionalDocument_gridView_ppdocumentImageButton.Attributes.Add("onclick",
                        "return OpenDownloadPopUp('" + AdditionalDocument_gridView_documentIdHiddenField.Value + "','" +type + "')");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfilePrint_errorMessageLabel, exp.Message);
            }
        }


    

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            if (PositionProfilePrint_toTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(PositionProfilePrint_errorMessageLabel,
                    Resources.HCMResource.EmailAttachment_ToAddressCannotBeEmpty);
                isValidData = false;
            }
            else
            {
                string[] toMailIDs = PositionProfilePrint_toTextBox.Text.Split(new char[] { ',' });

                for (int idx = 0; idx < toMailIDs.Length; idx++)
                {
                    if (!Regex.IsMatch(
                        toMailIDs[idx].Trim(), @"^([\w-]+\.)*?[\w-]+@[\w-]+\.([\w-]+\.)*?[\w]+$"))
                    {
                        base.ShowMessage(PositionProfilePrint_errorMessageLabel,
                            Resources.HCMResource.EmailAttachment_InvalidEmailAddress);
                        isValidData = false;
                        break;
                    }
                }
            }
            return isValidData;
        }

        /// <summary>
        /// Overridden protected method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                
        /// <summary>
        /// Method that send email to recruiter indicating the status change
        /// of candidate against position profile.
        /// </summary>
        /// <param name="ID">
        /// A <see cref="int"/> that holds the gen ID.
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendCandidateStatusChangeAlertEmail(int genID)
        {
            try
            {
                // Send email.
                new EmailHandler().SendMail(EntityType.PositionProfileCandidateStatusChanged, genID.ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }
        /// <summary>
        /// Method that binds the attachments.
        /// </summary>
        private void BindAttachement()
        {
            try
            {
                IList<string> test = new List<string>();
                test = (IList<string>)Session[Constants.SessionConstants.PP_SELECTED_CANDIATE_ID];
                List<DownloadDetails> downloadDetailsList = new List<DownloadDetails>();
                CandidateInformation candidateInformation = new CandidateInformation();
                if (!Utility.IsNullOrEmpty(test))
                {
                    PositionProfilePrint_attachmentTR.Visible = true;
                    foreach (string item in test)
                    {
                        candidateInformation = new PositionProfileBLManager().GetPositionProfileCandidateInformation(int.Parse(item));
                        if (candidateInformation != null)
                        {
                            DownloadDetails downloadDetails = new DownloadDetails();
                            downloadDetails.CandidateName = candidateInformation.caiFirstName;
                            downloadDetails.CanID = candidateInformation.caiID.ToString();
                            downloadDetails.PositionProfileCandidateID = int.Parse(item);
                            downloadDetails.CandidateSessionKey = candidateInformation.candidateSessionID;
                            downloadDetails.AttemptID = candidateInformation.AttemptID;
                            downloadDetails.TestKey = candidateInformation.TestKey;

                            downloadDetails.CandidateInterviewSessionKey = candidateInformation.InterviewCandidateSessionID;
                            downloadDetails.CandidateInterviewSessionAttemptID = candidateInformation.InterviewAttemptID;

                            if (downloadDetails.CandidateInterviewSessionKey != null)
                                downloadDetails.ISInterviewResult = true;
                            else
                                downloadDetails.ISInterviewResult = false;

                            if (candidateInformation.TestKey != null)
                                downloadDetails.ISTestResult = true;
                            else
                                downloadDetails.ISTestResult = false;

                            string mimeType;
                            string fileName;
                            byte[] resumeContent = new CandidateBLManager().GetResumeContentsByCandidiateID(candidateInformation.caiID, out mimeType, out fileName);
                            if (resumeContent == null)
                            {
                                downloadDetails.ISResume = false;
                            }
                            else
                            {
                                downloadDetails.ISResume = true;
                                downloadDetails.mimeType = mimeType;
                                downloadDetails.FileName = fileName;
                            }
                            downloadDetailsList.Add(downloadDetails);
                        }

                    }
                    PositionProfilePrint_attachmentGridView.DataSource = downloadDetailsList;
                    PositionProfilePrint_attachmentGridView.DataBind();
                    foreach (GridViewRow row in PositionProfilePrint_attachmentGridView.Rows)
                    {
                        int i = 0;
                        var imgMimeType = row.FindControl("PositionProfilePrint_resumeAttachmentImageButton") as ImageButton;
                        if (downloadDetailsList[i].mimeType != null)
                        {
                            if (downloadDetailsList.Exists(X => X.mimeType.ToLower() == "doc"))
                            {
                                imgMimeType.ImageUrl = "~/App_Themes/DefaultTheme/Images/icon_word.gif";
                            }
                            else if (downloadDetailsList.Exists(X => X.mimeType.ToLower() == "txt"))
                            {
                                imgMimeType.ImageUrl = "~/App_Themes/DefaultTheme/Images/add_notes.gif";
                            }
                            imgMimeType.ToolTip = downloadDetailsList[i].FileName;
                        }
                        i++;
                    }
                }
                else
                {
                    PositionProfilePrint_attachmentTR.Visible = false;
                }
         
            }
            catch (Exception exp)
            {
                base.ShowMessage(PositionProfilePrint_errorMessageLabel, exp.Message);
                Forte.HCM.Trace.Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that adds the additional documents to
        /// the email attachments for the selected candidates
        /// </summary>
        private void BindAdditionalDocument()
        {
            try
            {
                // Get position profile ID.
                int.TryParse(Request.QueryString["positionprofileid"], out positionProfileID);
             
                IList<string> test = new List<string>();
                test = (IList<string>)Session[Constants.SessionConstants.PP_SELECTED_CANDIATE_ID];
                
                string candidateIds = string.Empty;
                foreach (string item in test)
                {
                    candidateIds += item + ',';
                }

                if (!Utility.IsNullOrEmpty(candidateIds))
                {
                    candidateIds = candidateIds.Remove(candidateIds.LastIndexOf(','));
                    Positionprofileprint_additionaldocument.DataSource = new CandidateBLManager().GetAdditionalDocumentContent(candidateIds, positionProfileID);                   
                    Positionprofileprint_additionaldocument.DataBind();
                    if (Positionprofileprint_additionaldocument.Rows.Count > 0)
                    {
                        GenerateUniqueData(0);
                        foreach (GridViewRow row in Positionprofileprint_additionaldocument.Rows)
                        {
                            int i = 0;
                            var imgMimeType = row.FindControl("PositionProfilePrint_documentAttachmentImageButton") as ImageButton;
                            var checkbox = row.FindControl("PositionProfilePrint_additionaldocumentGridViewResumeCheckBox") as CheckBox;
                            string fileName = checkbox.Text.ToString();
                            string mimeType = Path.GetExtension(fileName);
                            if (mimeType != null)
                            {
                                if (mimeType.ToLower() == ".doc")
                                {
                                    imgMimeType.ImageUrl = "~/App_Themes/DefaultTheme/Images/icon_word.gif";
                                }
                                else if (mimeType.ToLower() == ".txt")
                                {
                                    imgMimeType.ImageUrl = "~/App_Themes/DefaultTheme/Images/add_notes.gif";
                                }
                                else if (mimeType.ToLower() == ".pdf")
                                {
                                    imgMimeType.ImageUrl = "~/App_Themes/DefaultTheme/Images/icon_pdf.gif";
                                }
                                imgMimeType.ToolTip = fileName.ToString();
                            }
                            i++;
                        }
                    }
                    else
                    {
                        PositionProfilePrint_additionaldocuments.Visible = false;
                    }
                  
                }
                else
                {
                    PositionProfilePrint_additionaldocuments.Visible = false;
                }
               
            }
            catch (Exception exp)
            {
                base.ShowMessage(PositionProfilePrint_errorMessageLabel, exp.Message);
                Forte.HCM.Trace.Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that generates unique candidate name in gridview rows
        /// </summary>
        /// <param name="cellno"></param>
        private void GenerateUniqueData(int cellno)
        {
            //Logic for unique names

            //Step 1:
           var intialNameLabel =  Positionprofileprint_additionaldocument.Rows[0].Cells[cellno].FindControl
               ("PositionProfilePrint_additionalattachmentGridViewCandidateNameLabel") as Label;

           string initialNamevalue = intialNameLabel.Text.ToString();

            //Step 2:        

            for (int i = 1; i < Positionprofileprint_additionaldocument.Rows.Count; i++)
            {
                var rowLabelValue = Positionprofileprint_additionaldocument.Rows[i].Cells[cellno].FindControl
               ("PositionProfilePrint_additionalattachmentGridViewCandidateNameLabel") as Label;
                if (rowLabelValue.Text == initialNamevalue)
                    rowLabelValue.Text = string.Empty;
                else
                    initialNamevalue = rowLabelValue.Text;
            }
        }

        /// <summary>
        /// Method that constructs the PDF document.
        /// </summary>
        private byte[] ConstructPDF(string candSessionID, string testKey, string attemptID)
        {
            try
            {
                decimal averageTimeTaken = 0;
                string fileName = string.Empty;
                string histogramSessionKey = string.Empty;
                string imagePath = string.Empty;
                string testName = string.Empty;
                bool flag = false;

                testName = new ReportBLManager().GetTestName(testKey);

                string candidateName = string.Empty;
                candidateName = new CandidateBLManager().GetCandidateName(candSessionID);

                fileName = candSessionID + "_" + attemptID + ".pdf";

                //Response.ContentType = "application/pdf";
                //if (fileName != string.Empty)
                //    Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                //else
                //    Response.AddHeader("content-disposition", "attachment;filename=TestResult.pdf");

                //Response.Cache.SetCacheability(HttpCacheability.NoCache);

                // Set Styles
                iTextSharp.text.Font fontCaptionStyle = iTextSharp.text.FontFactory.GetFont
                    ("Arial,sans-serif", 8, iTextSharp.text.Font.NORMAL, headerColor);
                iTextSharp.text.Font fontHeaderStyle = iTextSharp.text.FontFactory.GetFont
                    ("Arial,sans-serif", 7, iTextSharp.text.Font.NORMAL, headerColor);
                iTextSharp.text.Font fontItemStyle = iTextSharp.text.FontFactory.GetFont
                    ("Arial,sans-serif", 7, iTextSharp.text.Font.NORMAL, itemColor);

                TestStatistics testStatistics = new ReportBLManager().GetTestSummaryDetails(testKey);

                //Set Document Size as A4.
                var doc = new Document(iTextSharp.text.PageSize.A4);

                //Construct Table Structures            
                PdfPTable tableCandidateInfo = new PdfPTable(8);

                tableCandidateInfo.SpacingBefore = 10f;
                tableCandidateInfo.SpacingAfter = 20f;

                //Candidate Info table Structure
                PdfPCell cellTestIDHeader = new PdfPCell(new Phrase("Test ID", fontHeaderStyle));
                tableCandidateInfo.AddCell(cellTestIDHeader);
                PdfPCell cellTestID = new PdfPCell(new Phrase(testKey, fontItemStyle));
                tableCandidateInfo.AddCell(cellTestID);
                PdfPCell cellTestNameHeader = new PdfPCell(new Phrase("Test Name", fontHeaderStyle));
                tableCandidateInfo.AddCell(cellTestNameHeader);
                PdfPCell cellTestName = new PdfPCell(new Phrase(testName.Trim(), fontItemStyle));
                tableCandidateInfo.AddCell(cellTestName);
                PdfPCell cellCandidateNameHeader = new PdfPCell(new Phrase("Candidate Name", fontHeaderStyle));
                tableCandidateInfo.AddCell(cellCandidateNameHeader);
                PdfPCell cellCandidateName = new PdfPCell(new Phrase(candidateName, fontItemStyle));
                tableCandidateInfo.AddCell(cellCandidateName);
                PdfPCell cellCrerditsEarnedHeader = new PdfPCell(new Phrase("Credits Earned in ($)", fontHeaderStyle));
                tableCandidateInfo.AddCell(cellCrerditsEarnedHeader);
                PdfPCell cellCrerditsEarned = new PdfPCell(new Phrase("0.00", fontItemStyle));
                tableCandidateInfo.AddCell(cellCrerditsEarned);

                PdfPTable tableTestStatisticsHeader = new PdfPTable(1);
                tableTestStatisticsHeader.SpacingAfter = 8f;
                PdfPCell tableTestStatisticsHeaderCell = new PdfPCell(new Phrase("Test Statistics", fontCaptionStyle));
                tableTestStatisticsHeaderCell.BackgroundColor = new BaseColor(235, 239, 241);
                PdfPCell tableTestStatisticsItems = new PdfPCell(new Phrase("Items", fontItemStyle));
                tableTestStatisticsHeader.AddCell(tableTestStatisticsHeaderCell);

                //Summary table structure
                PdfPTable tableTestStatistics = new PdfPTable(8);
                tableTestStatistics.SpacingAfter = 8f;
                tableTestStatistics.WidthPercentage = 100;
                PdfPCell cellTestSummaryHeader = new PdfPCell(new Phrase("Test Summary", fontCaptionStyle));
                cellTestSummaryHeader.Colspan = 4;
                cellTestSummaryHeader.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                tableTestStatistics.AddCell(cellTestSummaryHeader);
                PdfPCell cellTestCategoryHeader = new PdfPCell(new Phrase("Categories / Subjects", fontCaptionStyle));
                cellTestCategoryHeader.Colspan = 4;
                cellTestCategoryHeader.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                tableTestStatistics.AddCell(cellTestCategoryHeader);
                PdfPCell cellTestAuthorHeader = new PdfPCell(new Phrase("Test Author", fontHeaderStyle));
                tableTestStatistics.AddCell(cellTestAuthorHeader);
                PdfPCell cellTestAuthor = new PdfPCell(new Phrase(testStatistics.TestAuthor.Trim(), fontItemStyle));
                tableTestStatistics.AddCell(cellTestAuthor);
                PdfPCell cellTestScoreHeader = new PdfPCell(new Phrase("Highest Score", fontHeaderStyle));
                tableTestStatistics.AddCell(cellTestScoreHeader);
                PdfPCell cellTestScore = new PdfPCell(new Phrase(testStatistics.HighestScore.ToString(), fontItemStyle));
                tableTestStatistics.AddCell(cellTestScore);

                PdfPCell cellCategoryOrSubjects = new PdfPCell(new Phrase("Category", fontHeaderStyle));
                cellCategoryOrSubjects.Colspan = 4;
                cellCategoryOrSubjects.Rowspan = 6;
                cellCategoryOrSubjects.Table = new PdfPTable(2);

                //cellCategoryOrSubjects.Table.SpacingAfter = 20f;
                cellCategoryOrSubjects.Table.AddCell(new Phrase("Category", fontItemStyle));
                cellCategoryOrSubjects.Table.AddCell(new Phrase("Subject", fontItemStyle));

                // Category/Subject Details
                foreach (Subject subject in testStatistics.SubjectStatistics)
                {
                    if (testStatistics.SubjectStatistics == null)
                        break;

                    PdfPCell cellInnerCategory = new PdfPCell(new Phrase
                        (subject.CategoryName.Trim(), fontHeaderStyle));

                    PdfPCell cellInnerSubject = new PdfPCell(new Phrase
                        (subject.SubjectName.Trim(), fontHeaderStyle));

                    cellCategoryOrSubjects.Table.AddCell(cellInnerCategory);
                    cellCategoryOrSubjects.Table.AddCell(cellInnerSubject);
                }

                tableTestStatistics.AddCell(cellCategoryOrSubjects);
                PdfPCell cellNOofQuestionsHeader = new PdfPCell(new Phrase("Number Of Questions", fontHeaderStyle));
                tableTestStatistics.AddCell(cellNOofQuestionsHeader);
                PdfPCell cellNOofQuestions = new PdfPCell(new Phrase
                    (testStatistics.NoOfQuestions.ToString(), fontItemStyle));
                tableTestStatistics.AddCell(cellNOofQuestions);
                PdfPCell cellLowestScoreHeader = new PdfPCell(new Phrase
                    ("Lowest Score", fontHeaderStyle));
                tableTestStatistics.AddCell(cellLowestScoreHeader);
                PdfPCell cellLowestScore = new PdfPCell(new Phrase
                    (testStatistics.LowestScore.ToString(), fontItemStyle));
                tableTestStatistics.AddCell(cellLowestScore);
                PdfPCell cellNOofCandidatesHeader = new PdfPCell
                    (new Phrase("Number Of Candidates", fontHeaderStyle));
                tableTestStatistics.AddCell(cellNOofCandidatesHeader);
                PdfPCell cellNOofCandidates = new PdfPCell(new Phrase
                    (testStatistics.NoOfCandidates.ToString(), fontItemStyle));
                tableTestStatistics.AddCell(cellNOofCandidates);
                PdfPCell cellTestMeanScoreHeader = new PdfPCell(new Phrase("Mean Score", fontHeaderStyle));
                tableTestStatistics.AddCell(cellTestMeanScoreHeader);
                PdfPCell cellTestMeanScore = new PdfPCell(new Phrase
                    (testStatistics.MeanScore.ToString(), fontItemStyle));
                tableTestStatistics.AddCell(cellTestMeanScore);
                PdfPCell cellAverageTimeHeader = new PdfPCell(new Phrase
                    ("Average Time Taken By Candidates", fontHeaderStyle));
                tableTestStatistics.AddCell(cellAverageTimeHeader);

                if (!Utility.IsNullOrEmpty(testStatistics.AverageTimeTakenByCandidates)
                    && testStatistics.AverageTimeTakenByCandidates != 0)
                {
                    averageTimeTaken = testStatistics.AverageTimeTakenByCandidates;
                }

                PdfPCell cellAverageTime = new PdfPCell(new Phrase
                    (Utility.ConvertSecondsToHoursMinutesSeconds
                    (Convert.ToInt32(testStatistics.AverageTimeTakenByCandidates)), fontItemStyle));

                tableTestStatistics.AddCell(cellAverageTime);
                PdfPCell cellStandardDeviationHeader = new PdfPCell(new Phrase("Standard Deviation", fontHeaderStyle));
                tableTestStatistics.AddCell(cellStandardDeviationHeader);
                PdfPCell cellStandardDeviation = new PdfPCell(new Phrase
                    (testStatistics.StandardDeviation.ToString(), fontItemStyle));
                tableTestStatistics.AddCell(cellStandardDeviation);
                PdfPCell cellScoreRangeHeader = new PdfPCell(new Phrase("Score Range", fontHeaderStyle));
                tableTestStatistics.AddCell(cellScoreRangeHeader);
                PdfPCell cellScoreRange = new PdfPCell(new Phrase("TBD", fontItemStyle));
                tableTestStatistics.AddCell(cellScoreRange);
                PdfPCell cellEmptyHeader = new PdfPCell(new Phrase("", fontHeaderStyle));
                tableTestStatistics.AddCell(cellEmptyHeader);
                PdfPCell cellEmptyItem = new PdfPCell(new Phrase("", fontItemStyle));
                tableTestStatistics.AddCell(cellEmptyItem);

                int rowCount = 0;
                if (testStatistics.SubjectStatistics.Count > 6)
                    rowCount = testStatistics.SubjectStatistics.Count - 6;
                for (int j = 0; j < rowCount; j++)
                {
                    PdfPCell cellEmptyItemforRowSpan = new PdfPCell(new Phrase("", fontItemStyle));
                    cellEmptyItemforRowSpan.Colspan = 4;
                    tableTestStatistics.AddCell(cellEmptyItemforRowSpan);
                }

                tableTestStatisticsItems.AddElement(tableTestStatistics);

                //Category and Subject Statistics table structure
                PdfPTable tableCategoryStatistics = new PdfPTable(2);
                tableCategoryStatistics.SpacingAfter = 8f;
                tableCategoryStatistics.WidthPercentage = 100;
                PdfPCell cellCategoryStatistics = new PdfPCell(new Phrase("Category Statistics", fontCaptionStyle));
                PdfPCell cellSubjectStatistics = new PdfPCell(new Phrase("Subject Statistics", fontCaptionStyle));
                cellCategoryStatistics.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                cellSubjectStatistics.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                tableCategoryStatistics.AddCell(cellCategoryStatistics);
                tableCategoryStatistics.AddCell(cellSubjectStatistics);

               /* if (File.Exists(chartImagePath
                    + Constants.ChartConstants.CHART_CATEGORY + "-" + testKey + ".png"))
                {
                     imageCategory = iTextSharp.text.Image.GetInstance(chartImagePath
                        + Constants.ChartConstants.CHART_CATEGORY + "-" + testKey + ".png");
                }*/
                iTextSharp.text.Image imageCategory =null;

                imagePath = Server.MapPath("~/chart/")  + Constants.ChartConstants.CHART_CATEGORY + "-" + testKey + ".png";

                if (!File.Exists(imagePath))
                {
                    imageCategory = iTextSharp.text.Image.GetInstance(Server.MapPath("~/chart/empty.png"));
                }
                else
                {
                    flag = true;
                    imageCategory = iTextSharp.text.Image.GetInstance(chartImagePath
                           + Constants.ChartConstants.CHART_CATEGORY + "-" + testKey + ".png");
                    imageCategory.ScaleToFit(205f, 300f);
                    imageCategory.Border = Rectangle.BOX;
                }  
                imageCategory.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
                PdfPCell cellCategoryImage = new PdfPCell(imageCategory);
                cellCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                if (!flag)
                {
                    imageCategory.Alignment = iTextSharp.text.Image.ALIGN_JUSTIFIED;
                    cellCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    cellCategoryImage.FixedHeight = 50f;
                }

                tableCategoryStatistics.AddCell(cellCategoryImage);
                flag = false;

                imagePath = Server.MapPath("~/chart/")  +Constants.ChartConstants.CHART_SUBJECT + "-" + testKey + ".png";

                iTextSharp.text.Image imageSubject =null;

                if(!File.Exists(imagePath) )
                {
                    imageSubject = iTextSharp.text.Image.GetInstance(Server.MapPath("~/chart/empty.png"));
                }
                else
                {
                    flag=true;
                   imageSubject= iTextSharp.text.Image.GetInstance(chartImagePath
                    + Constants.ChartConstants.CHART_SUBJECT + "-" + testKey + ".png");
                    imageSubject.ScaleToFit(205f, 300f);
                    imageSubject.Border = Rectangle.BOX;
                }
                
                imageSubject.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
                PdfPCell cellSubjectImage = new PdfPCell(imageSubject);
                cellSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                if(!flag)
                {
                    imageSubject.Alignment = iTextSharp.text.Image.ALIGN_JUSTIFIED;
                    cellSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    cellSubjectImage.FixedHeight =50f; 
                }
                tableCategoryStatistics.AddCell(cellSubjectImage);
                flag=false ;

                tableTestStatisticsItems.AddElement(tableCategoryStatistics);

                //Test Area and complexity table structure
                PdfPTable tableTestAreaStatistics = new PdfPTable(2);
                //tableTestAreaStatistics.SpacingAfter = 3f;
                tableTestAreaStatistics.WidthPercentage = 100;
                PdfPCell cellAreStatisticsHeader = new PdfPCell(new Phrase("Test Area Statistics", fontCaptionStyle));
                PdfPCell cellComplexityStatisticsHeader = new PdfPCell(new Phrase("Complexity Statistics", fontCaptionStyle));
                tableTestAreaStatistics.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                tableTestAreaStatistics.AddCell(cellAreStatisticsHeader);
                tableTestAreaStatistics.AddCell(cellComplexityStatisticsHeader);

                imagePath = Server.MapPath("~/chart/") 
                    + Constants.ChartConstants.CHART_TESTAREA + "-" + testKey + ".png";

                iTextSharp.text.Image imageArea = null;
                if (!File.Exists(imagePath))
                {
                    imageArea = iTextSharp.text.Image.GetInstance(Server.MapPath("~/chart/empty.png"));
                }
                else
                {
                    flag=true;
                    imageArea =iTextSharp.text.Image.GetInstance(chartImagePath
                         + Constants.ChartConstants.CHART_TESTAREA + "-" + testKey + ".png");
                    imageArea.ScaleToFit(205f, 300f);
                    imageArea.Border = Rectangle.BOX;
                }
              
                imageArea.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
                PdfPCell cellAreaImage = new PdfPCell(imageArea);
                cellAreaImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                if (!flag)
                {
                    imageArea.Alignment = iTextSharp.text.Image.ALIGN_JUSTIFIED;
                    cellAreaImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    cellAreaImage.FixedHeight = 50f; 
                }
                flag = false;
                tableTestAreaStatistics.AddCell(cellAreaImage);

                imagePath = Server.MapPath("~/chart/") 
                    + Constants.ChartConstants.CHART_COMPLEXITY + "-" + testKey + ".png";

                iTextSharp.text.Image imageComplexity = null;
                if (!File.Exists(imagePath))
                {
                    imageComplexity = iTextSharp.text.Image.GetInstance(Server.MapPath("~/chart/empty.png"));
                }
                else
                {
                    flag = true;
                   imageComplexity= iTextSharp.text.Image.GetInstance(chartImagePath
                        + Constants.ChartConstants.CHART_COMPLEXITY + "-" + testKey + ".png");
                   imageComplexity.ScaleToFit(205f, 300f);
                   imageComplexity.Border = Rectangle.BOX;
                }
               
                imageComplexity.Alignment = iTextSharp.text.Image.ALIGN_LEFT; 
                PdfPCell cellComplexityImage = new PdfPCell(imageComplexity);
                cellComplexityImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                if (!flag)
                {
                    imageComplexity.Alignment = iTextSharp.text.Image.ALIGN_JUSTIFIED;
                    cellComplexityImage.FixedHeight = 50f;
                    cellComplexityImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER; 
                }
                flag = false;

                tableTestAreaStatistics.AddCell(cellComplexityImage);

                tableTestStatisticsItems.AddElement(tableTestAreaStatistics);
                tableTestStatisticsHeader.AddCell(tableTestStatisticsItems);

                PdfPTable tableCandidateStatisticsHeader = new PdfPTable(1);
                tableCandidateStatisticsHeader.SpacingAfter = 8f;
                PdfPCell tableCandidateStatisticsHeaderCell = new PdfPCell(new Phrase
                    ("Candidate Statistics", fontCaptionStyle));
                tableCandidateStatisticsHeaderCell.BackgroundColor = new BaseColor(235, 239, 241);
                PdfPCell tableCandidateStatisticsItemCell = new PdfPCell(new Phrase("", fontItemStyle));
                tableCandidateStatisticsHeader.AddCell(tableCandidateStatisticsHeaderCell);

                CandidateStatisticsDetail candidateStatisticsDetail =
                     new ReportBLManager().GetCandidateTestResultStatisticsDetails(candSessionID, testKey, int.Parse(attemptID));

                //Candidate Statistics
                PdfPTable tableCandidateStatistics = new PdfPTable(4);
                tableCandidateStatistics.SpacingAfter = 8f;
                tableCandidateStatistics.WidthPercentage = 100;
                PdfPCell cellCandidateNoOfQuestionsHeader = new PdfPCell(new Phrase
                    ("Number Of Questions", fontHeaderStyle));
                tableCandidateStatistics.AddCell(cellCandidateNoOfQuestionsHeader);
                PdfPCell cellCandidateNoOfQuestion = new PdfPCell(new Phrase
                    (testStatistics.NoOfQuestions.ToString(), fontItemStyle));
                tableCandidateStatistics.AddCell(cellCandidateNoOfQuestion);
                PdfPCell cellCandidateMyScoreHeader = new PdfPCell(new Phrase("My Score", fontHeaderStyle));
                tableCandidateStatistics.AddCell(cellCandidateMyScoreHeader);
                PdfPCell cellCandidateMyScore = new PdfPCell(new Phrase
                    (candidateStatisticsDetail.MyScore.ToString(), fontItemStyle));
                tableCandidateStatistics.AddCell(cellCandidateMyScore);
                PdfPCell cellCandidateTotalQuestionsAttendedHeader = new PdfPCell(new Phrase
                    ("Total Questions Attended", fontHeaderStyle));
                tableCandidateStatistics.AddCell(cellCandidateTotalQuestionsAttendedHeader);
                PdfPCell cellCandidateTotalQuestionsAttended = new PdfPCell(new Phrase
                    (candidateStatisticsDetail.TotalQuestionAttended.ToString(), fontItemStyle));
                tableCandidateStatistics.AddCell(cellCandidateTotalQuestionsAttended);
                PdfPCell cellCCandidateAbsoluteScoreHeader = new PdfPCell(new Phrase
                    ("Absolute Score", fontHeaderStyle));
                tableCandidateStatistics.AddCell(cellCCandidateAbsoluteScoreHeader);
                PdfPCell cellCandidateAbsolute = new PdfPCell(new Phrase
                    (candidateStatisticsDetail.AbsoluteScore.ToString(), fontItemStyle));
                tableCandidateStatistics.AddCell(cellCandidateAbsolute);
                PdfPCell cellCandidateTotalQuestionsSkippedHeader = new PdfPCell(new Phrase
                    ("Total Questions Skipped", fontHeaderStyle));
                tableCandidateStatistics.AddCell(cellCandidateTotalQuestionsSkippedHeader);
                PdfPCell cellCandidateTotalQuestionsSkipped = new PdfPCell(new Phrase
                    (candidateStatisticsDetail.TotalQuestionSkipped.ToString(), fontItemStyle));
                tableCandidateStatistics.AddCell(cellCandidateTotalQuestionsSkipped);
                PdfPCell cellCandidateRelativeScoreHeader = new PdfPCell(new Phrase
                    ("Relative Score", fontHeaderStyle));
                tableCandidateStatistics.AddCell(cellCandidateRelativeScoreHeader);
                PdfPCell cellCandidateRelativeScore = new PdfPCell(new Phrase(candidateStatisticsDetail.RelativeScore.ToString(), fontItemStyle));
                tableCandidateStatistics.AddCell(cellCandidateRelativeScore);
                PdfPCell cellCandidateAnsweredCorrectlyHeader = new PdfPCell(new Phrase
                    ("Questions Answered Correctly", fontHeaderStyle));
                tableCandidateStatistics.AddCell(cellCandidateAnsweredCorrectlyHeader);
                PdfPCell cellCandidateAnsweredCorrectly = new PdfPCell(new Phrase
                    (candidateStatisticsDetail.QuestionsAnsweredCorrectly.ToString(), fontItemStyle));
                tableCandidateStatistics.AddCell(cellCandidateAnsweredCorrectly);
                PdfPCell cellCandidatePercentileHeader = new PdfPCell(new Phrase("Percentile", fontHeaderStyle));
                tableCandidateStatistics.AddCell(cellCandidatePercentileHeader);
                PdfPCell cellCandidateercentile = new PdfPCell(new Phrase
                    (candidateStatisticsDetail.Percentile.ToString(), fontItemStyle));
                tableCandidateStatistics.AddCell(cellCandidateercentile);
                PdfPCell cellCandidateAnsweredWronglyHeader = new PdfPCell(new Phrase
                    ("Questions Answered Wrongly", fontHeaderStyle));
                tableCandidateStatistics.AddCell(cellCandidateAnsweredWronglyHeader);
                PdfPCell cellCandidateAnsweredWrongly = new PdfPCell(new Phrase
                    (candidateStatisticsDetail.QuestionsAnsweredWrongly.ToString(), fontItemStyle));
                tableCandidateStatistics.AddCell(cellCandidateAnsweredWrongly);
                PdfPCell cellCandidateTotalTimeTakenHeader = new PdfPCell(new Phrase
                    ("Total Time Taken", fontHeaderStyle));
                tableCandidateStatistics.AddCell(cellCandidateTotalTimeTakenHeader);
                PdfPCell cellCandidateTotalTimeTaken = new PdfPCell(new Phrase
                    (candidateStatisticsDetail.TimeTaken.ToString(), fontItemStyle));
                tableCandidateStatistics.AddCell(cellCandidateTotalTimeTaken);
                PdfPCell cellCandidateAnsweredCorrectlyPercentageHeader = new PdfPCell
                    (new Phrase("Questions Answered Correctly (in %)", fontHeaderStyle));
                tableCandidateStatistics.AddCell(cellCandidateAnsweredCorrectlyPercentageHeader);
                PdfPCell cellCandidateAnsweredCorrectlyPercentage = new PdfPCell(new Phrase
                    (candidateStatisticsDetail.AnsweredCorrectly.ToString(), fontItemStyle));
                tableCandidateStatistics.AddCell(cellCandidateAnsweredCorrectlyPercentage);
                PdfPCell cellCandidateAverageTimeTakenToAnswerEachQuestionHeader = new PdfPCell
                    (new Phrase("Average Time Taken To Answer Each Question", fontHeaderStyle));
                tableCandidateStatistics.AddCell(cellCandidateAverageTimeTakenToAnswerEachQuestionHeader);



                if (!Utility.IsNullOrEmpty(candidateStatisticsDetail.AverageTimeTaken)
                    && candidateStatisticsDetail.AverageTimeTaken.Length != 0)
                {
                    averageTimeTaken = decimal.Parse(candidateStatisticsDetail.AverageTimeTaken);
                }

                PdfPCell cellCandidateAverageTimeTakenToAnswerEachQuestion = new PdfPCell
                    (new Phrase(Utility.ConvertSecondsToHoursMinutesSeconds
                        (decimal.ToInt32(averageTimeTaken)), fontItemStyle));
                tableCandidateStatistics.AddCell(cellCandidateAverageTimeTakenToAnswerEachQuestion);
                PdfPCell cellCandidateTestStatusHeader = new PdfPCell(
                  new Phrase("Test Status", fontHeaderStyle));
                tableCandidateStatistics.AddCell(cellCandidateTestStatusHeader);
                PdfPCell cellCandidateTestStatus = new PdfPCell(
                    new Phrase(candidateStatisticsDetail.TestStatus, fontItemStyle));
                tableCandidateStatistics.AddCell(cellCandidateTestStatus);
                PdfPCell cellCandidateAverageTimeTakenToAnswerEachQuestionHeader1 = new PdfPCell(
                    new Phrase("", fontHeaderStyle));
                tableCandidateStatistics.AddCell(cellCandidateAverageTimeTakenToAnswerEachQuestionHeader1);
                PdfPCell cellCandidateAverageTimeTakenToAnswerEachQuestion1 = new PdfPCell(
                    new Phrase("", fontItemStyle));
                tableCandidateStatistics.AddCell(cellCandidateAverageTimeTakenToAnswerEachQuestion1);

                tableCandidateStatisticsItemCell.AddElement(tableCandidateStatistics);


                //Test Score and category table structure
                PdfPTable tableTestScore = new PdfPTable(2);
                tableTestScore.SpacingAfter = 8f;
                tableTestScore.WidthPercentage = 100;
                PdfPCell cellScoreHeader = new PdfPCell(new Phrase("Test Scores", fontCaptionStyle));
                PdfPCell cellScoreCategoryHeader = new PdfPCell(new Phrase
                    ("Score Distribution Amongst Categories", fontCaptionStyle));
                tableTestScore.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                tableTestScore.AddCell(cellScoreHeader);
                tableTestScore.AddCell(cellScoreCategoryHeader);

                //iTextSharp.text.Image imageTestScore;

                //if (!Utility.IsNullOrEmpty(Request.QueryString["parentpage"]) &&
                //    Request.QueryString["parentpage"].ToUpper() == "CAND_HOME" ||
                //     Request.QueryString["parentpage"].ToUpper() == "MY_TST")
                //{
                //    imageTestScore = iTextSharp.text.Image.GetInstance(
                //        chartImagePath + Constants.ChartConstants.CHART_HISTOGRAM + "-"
                //        + testKey + "-" + candSessionID + "-" + attemptID + ".png");
                //}
                //else
                //{
                //    imageTestScore = iTextSharp.text.Image.GetInstance(
                //      chartImagePath + Constants.ChartConstants.CHART_HISTOGRAM + "-Adm" + "-"
                //      + testKey + "-" + candSessionID + "-" + attemptID + ".png");
                //}

                imagePath = Server.MapPath("~/chart/") + Constants.ChartConstants.CHART_HISTOGRAM + "-"
                       + testKey + "-" + candSessionID + "-" + attemptID + ".png";

                iTextSharp.text.Image imageTestScore = null;
                if (!File.Exists(imagePath))
                {
                    imageTestScore = iTextSharp.text.Image.GetInstance(Server.MapPath("~/chart/empty.png"));
                }
                else
                {
                    flag =true;
                    imageTestScore = iTextSharp.text.Image.GetInstance(
                            chartImagePath + Constants.ChartConstants.CHART_HISTOGRAM + "-"
                            + testKey + "-" + candSessionID + "-" + attemptID + ".png");
                    imageTestScore.ScaleToFit(205f, 300f);
                    imageTestScore.Border = Rectangle.BOX;
                }
                
                imageTestScore.Alignment = iTextSharp.text.Image.ALIGN_LEFT;

                PdfPCell cellTestScoreImage = new PdfPCell(imageTestScore);
                cellCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                if (!flag)
                {
                    imageTestScore.Alignment = iTextSharp.text.Image.ALIGN_JUSTIFIED;
                    cellTestScoreImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    cellTestScoreImage.FixedHeight = 50f;
                }
                flag = false;
                tableTestScore.AddCell(cellTestScoreImage);

                imagePath = Server.MapPath("~/chart/") 
                    + Constants.ChartConstants.CHART_CATEGORY + "-" + testKey + "-" + candSessionID + "-"
                    + attemptID + ".png";

                iTextSharp.text.Image imageScoreCategory = null;
                if (!File.Exists(imagePath))
                {
                    imageScoreCategory = iTextSharp.text.Image.GetInstance(Server.MapPath("~/chart/empty.png"));
                }
                else
                {
                    flag = true;
                   imageScoreCategory= iTextSharp.text.Image.GetInstance(chartImagePath
                        + Constants.ChartConstants.CHART_CATEGORY + "-" + testKey + "-" + candSessionID + "-"
                        + attemptID + ".png");
                    imageScoreCategory.ScaleToFit(205f, 300f);
                    imageScoreCategory.Border = Rectangle.BOX;
                }
                
                imageScoreCategory.Alignment = iTextSharp.text.Image.ALIGN_LEFT;

                PdfPCell cellScoreCategoryImage = new PdfPCell(imageScoreCategory);
                cellScoreCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                if (!flag)
                {
                    imageScoreCategory.Alignment = iTextSharp.text.Image.ALIGN_JUSTIFIED;
                    cellScoreCategoryImage.FixedHeight = 50f;
                    cellScoreCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                }
                flag = false;
                tableTestScore.AddCell(cellScoreCategoryImage);                
                tableCandidateStatisticsItemCell.AddElement(tableTestScore);

                //Score subject and score distribution area table structure
                PdfPTable tableScoreSubject = new PdfPTable(2);
                //tableScoreSubject.SpacingAfter = 3f;
                tableScoreSubject.WidthPercentage = 100;
                PdfPCell cellScoreSubjectHeader = new PdfPCell(new Phrase
                    ("Score Distribution Amongst Subjects", fontCaptionStyle));
                PdfPCell cellScoreTestAreaHeader = new PdfPCell(new Phrase
                    ("Score Distribution Amongst Test Areas", fontCaptionStyle));
                tableScoreSubject.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                tableScoreSubject.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                tableScoreSubject.AddCell(cellScoreSubjectHeader);
                tableScoreSubject.AddCell(cellScoreTestAreaHeader);

                imagePath = Server.MapPath("~/chart/") 
                    + Constants.ChartConstants.CHART_SUBJECT + "-" + testKey + "-"
                    + candSessionID + "-" + attemptID + ".png";

                iTextSharp.text.Image imageScoreSubject = null;
                if (!File.Exists(imagePath))
                {
                    imageScoreSubject = iTextSharp.text.Image.GetInstance(Server.MapPath("~/chart/empty.png"));
                }
                else
                {
                    flag = true;
                    imageScoreSubject = iTextSharp.text.Image.GetInstance(chartImagePath
                         + Constants.ChartConstants.CHART_SUBJECT + "-" + testKey + "-"
                         + candSessionID + "-" + attemptID + ".png");
                    imageScoreSubject.ScaleToFit(205f, 300f);
                    imageScoreSubject.Border = Rectangle.BOX;
                }

                imageScoreSubject.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
                PdfPCell cellScoreSubjectImage = new PdfPCell(imageScoreSubject);
                cellScoreSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                if (!flag)
                {
                    imageScoreSubject.Alignment = iTextSharp.text.Image.ALIGN_JUSTIFIED;
                    cellScoreSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    cellScoreSubjectImage.FixedHeight = 50f;
                }
                flag = false;

                tableScoreSubject.AddCell(cellScoreSubjectImage);

                imagePath = Server.MapPath("~/chart/")  + Constants.ChartConstants.CHART_TESTAREA + "-" + testKey + "-"
                    + candSessionID + "-" + attemptID + ".png";

                iTextSharp.text.Image imageScoreArea = null;

                if (!File.Exists(imagePath))
                {
                    imageScoreArea = iTextSharp.text.Image.GetInstance(Server.MapPath("~/chart/empty.png"));
                }
                else
                {
                    flag = true;
                      imageScoreArea = iTextSharp.text.Image.GetInstance(chartImagePath
                        + Constants.ChartConstants.CHART_TESTAREA + "-" + testKey + "-"
                        + candSessionID + "-" + attemptID + ".png");
                      imageScoreArea.ScaleToFit(205f, 300f);
                      imageScoreArea.Border = Rectangle.BOX;
                } 
                 
                imageScoreArea.Alignment = iTextSharp.text.Image.ALIGN_LEFT; 
                PdfPCell cellScoreAreaImage = new PdfPCell(imageScoreArea);
                cellComplexityImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                if (!flag)
                {
                    imageScoreArea.Alignment = iTextSharp.text.Image.ALIGN_JUSTIFIED;
                    cellScoreAreaImage.FixedHeight = 50f;
                    cellComplexityImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                }
                tableScoreSubject.AddCell(cellScoreAreaImage);

                tableCandidateStatisticsItemCell.AddElement(tableScoreSubject);
                tableCandidateStatisticsHeader.AddCell(tableCandidateStatisticsItemCell);

                //Response Output
                //PdfWriter.GetInstance(doc, Response.OutputStream);

                //Response Output
                MemoryStream memoryStream = new MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);

                doc.Open();
                doc.Add(tableCandidateInfo);
                doc.Add(tableTestStatisticsHeader);
                doc.Add(tableCandidateStatisticsHeader);

                writer.CloseStream = false;
                doc.Close();
                memoryStream.Position = 0;

                // Convert stream to byte array.
                BinaryReader reader = new BinaryReader(memoryStream);
                byte[] dataAsByteArray = new byte[memoryStream.Length];
                dataAsByteArray = reader.ReadBytes(Convert.ToInt32(memoryStream.Length));
                return dataAsByteArray;
            }
            catch (Exception exp)
            { 
                throw exp;
            }
        }

        /// <summary>
        /// Method that sets the page title and caption.
        /// </summary>
        private void SetTitle()
        {
            // Set the page title and caption based on type.
            if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                Request.QueryString["type"].Trim().ToUpper() == "PP_STAT")
            {
                Master.SetPageCaption("Email Position Profile Status");
                PositionProfilePrint_headerLiteral.Text = "Email Position Profile Status";
            }
            else if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
            {
                Master.SetPageCaption("Email Position Profile");
                PositionProfilePrint_headerLiteral.Text = "Email Position Profile";
            }
            else
            {
                Master.SetPageCaption("Email Position Profile");
                PositionProfilePrint_headerLiteral.Text = "Email Position Profile";
            }
        }

        #endregion Private Methods

        #region Interview Assessment Details                                   
      
        private string GetCandidateImageFile(int candidateID)
        {
            string path = null;

            bool isExists = false;
            if (candidateID > 0)
            {
                path = Server.MapPath("~/") +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                            "\\" + candidateID + "." +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];
                if (File.Exists(path))
                    isExists = true;
                else
                    isExists = false;
            }

            if (isExists)
                return path;
            else
            {
                return Server.MapPath("~/") +
                      ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                      "\\" + "photo_not_available.png";
            }
        }

        private byte[] GeneretePDF_AssessorSummaryRating(string candidateSessionID, int attemptID)
        {
            CandidateInterviewSessionDetail sessionDetail = new InterviewSchedulerBLManager().
          GetCandidateInterviewSessionDetail(candidateSessionID, attemptID);

            PDFDocumentWriter pdfWriter = new PDFDocumentWriter();

            DataTable assessmentTable = new DataTable();

            string fileName = sessionDetail.CandidateName + "_" + "InterviewDetail.pdf"; 

            Document doc = new Document(PDFDocumentWriter.paper,
                    PDFDocumentWriter.leftMargin, PDFDocumentWriter.rightMargin,
                    PDFDocumentWriter.topMargin, PDFDocumentWriter.bottomMargin);

            string logoPath = Server.MapPath("~/");

            iTextSharp.text.Image imgBackground = iTextSharp.text.Image.GetInstance(logoPath + "Images/AssessmentBackground.jpg");

            imgBackground.ScaleToFit(1000, 132);
            imgBackground.Alignment = iTextSharp.text.Image.UNDERLYING;
            imgBackground.SetAbsolutePosition(50, 360);

            PdfPTable pageHeader = new PdfPTable(pdfWriter.PageHeader(logoPath, "Candidate Interview Assessment Report"));
            pageHeader.SpacingAfter = 10f;

            //Construct Table Structures    
            PdfPTable tableHeaderDetail = new PdfPTable(1);
            PdfPCell cellBorder = new PdfPCell(new Phrase(""));
            cellBorder.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            cellBorder.Border = 0;
            tableHeaderDetail.WidthPercentage = 90;
            cellBorder.PaddingTop = 20f;
            cellBorder.AddElement(pdfWriter.TableHeader(sessionDetail, candidateSessionID,
                attemptID, GetCandidateImageFile(sessionDetail.CandidateInformationID), out assessmentTable));
            tableHeaderDetail.AddCell(cellBorder);
            tableHeaderDetail.SpacingAfter = 30f;

            //Response Output
            MemoryStream memoryStream = new MemoryStream();

            PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
            doc.Open();
            doc.Add(pageHeader);
            doc.Add(imgBackground);
            doc.Add(tableHeaderDetail); 
            doc.Add(pdfWriter.TableDetail(assessmentTable));
            writer.CloseStream = false;
            doc.Close();
            memoryStream.Position = 0; 

            // Convert stream to byte array.
            BinaryReader reader = new BinaryReader(memoryStream);
            byte[] dataAsByteArray = new byte[memoryStream.Length];
            dataAsByteArray = reader.ReadBytes(Convert.ToInt32(memoryStream.Length));

            return dataAsByteArray;
            // Store the byte array in Session
            //Session[Constants.SessionConstants.CANDIDATE_RESULTS_PDF] = dataAsByteArray;
        }

#endregion

        #region Private Classes                                                

        /// <summary>
        /// Class that holds the download details.
        /// </summary>
        private class DownloadDetails
        {
            public string CandidateName { get; set; }
            public string mimeType { get; set; }
            public bool ISResume { get; set; }
            public bool ISTestResult { get; set; }
            public string FileName { get; set; }
            public string CanID { get; set; }
            public string CandidateSessionKey { get; set; }
            public int AttemptID { get; set; }
            public string TestKey { get; set; }
            public string CandidateInterviewSessionKey { get; set; }
            public int CandidateInterviewSessionAttemptID { get; set; }
            public bool ISInterviewResult { get; set; }
            public int PositionProfileCandidateID { get; set; }
        }

        #endregion Private Classes

        #region Asynchronous Method Handlers

        /// <summary>
        /// Handler method that acts as the callback method for position profile 
        /// status change email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendStatusChangeAlertEmailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                PositionProfileStatusDelegate caller = (PositionProfileStatusDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that acts as the callback method for candidate status
        /// change email alert against the position profile.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendCandidateStatusChangeAlertEmailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                CandidateStatusDelegate caller = (CandidateStatusDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Asynchronous Method Handlers

       
}
}