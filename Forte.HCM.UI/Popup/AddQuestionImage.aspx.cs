﻿using System;
using System.IO;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;


namespace Forte.HCM.UI.Popup
{
    public partial class AddQuestionImage : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AddQuestionImage_errorMessageLabel.Text
                    = string.Empty;
        }
        /// <summary>
        /// Handler method that is called when upload button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void AddQuestionImage_uploadButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (IsValidData())
                {
                    string imagePath = Request.QueryString["imagePath"].ToString();
                    if (!Directory.Exists(Path.GetDirectoryName(Server.MapPath("~/") + imagePath)))
                        Directory.CreateDirectory(Path.GetDirectoryName(Server.MapPath("~/") + imagePath));

                    AddQuestionImage_fileUpload.PostedFile.SaveAs(Server.MapPath("~/") + imagePath);
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "OnClose",
                            "javascript:DisplayImageName();", true);

                }
                
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(AddQuestionImage_errorMessageLabel, exception.Message);
            }
        }
        protected override void LoadValues()
        {
            
        }
        protected override bool IsValidData()
        {
            if (!AddQuestionImage_fileUpload.HasFile)
            {
                base.ShowMessage(AddQuestionImage_errorMessageLabel, Resources.HCMResource.BatchQuestionEntry_uploadFile);
                return false;
            }
            else if(AddQuestionImage_fileUpload.PostedFile.ContentLength>102400) // above 100 kb
            {
                base.ShowMessage(AddQuestionImage_errorMessageLabel, Resources.HCMResource.BatchQuestionEntry_imageSizeExceeded);
                return false;
            }
            return true;
        }
    }
}