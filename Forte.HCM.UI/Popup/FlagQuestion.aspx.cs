﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// FlagQuestion.cs
// File that represents the user interface to send a mail to the admin
// from the user regarding to flag a question.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI;
using System.Configuration;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities
    /// for the sending mails to the user who is going to flag a question.
    /// </summary>
    public partial class FlagQuestion : PageBase
    {
        #region Event Handler                                                  

        /// <summary>
        /// This event handler helps to set default button, focus and page title.
        /// Also, it retrieves the currently logged in user email address by calling
        /// BL method then, admin email address too.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Flag Question");
                // Set default button field
                Page.Form.DefaultButton = FlagQuestion_bottomSubmitButton.UniqueID;
                FlagQuestion_bottomCancelButton.Attributes.Add("onclick", "Javascript:return CloseMe();");
                // Call validate email javascript function
                FlagQuestion_bottomSubmitButton.Attributes.Add("onclick", "javascript:return IsValidEmail('"
                    + FlagQuestion_ccTextBox.ClientID + "','"
                    + FlagQuestion_topErrorMessageLabel.ClientID + "','"
                    + FlagQuestion_topSuccessMessageLabel.ClientID + "');");

                if (!IsPostBack)
                {
                    // Set default focus field
                    Page.Form.DefaultFocus = FlagQuestion_reasonTextBox.UniqueID;
                    FlagQuestion_reasonTextBox.Focus();

                    UserDetail userDetail = new CommonBLManager().GetUserDetail(base.userID);

                    if (userDetail != null)
                    {
                        FlagQuestion_fromValueLabel.Text = userDetail.FirstName
                            + " &lt;" + userDetail.Email + "&gt;";

                        FlagQuestion_toValueLabel.Text = "Admin &lt;" +
                            ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"].ToString() + "&gt;";

                        FlagQuestion_toHiddenField.Value = ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"].ToString();
                        FlagQuestion_fromHiddenField.Value = userDetail.Email.Trim();
                    }

                    // Check if the querystring is not equal to empty.
                    if (Request.QueryString["questionkey"] != null)
                    {
                        FlagQuestion_subjectValueLabel.Text =
                            string.Format(Resources.HCMResource.FlagQuestion_SubjectInRegardsToQuestion,
                            Request.QueryString["questionkey"].ToString());
                    }
                }
            }
            catch (Exception exp)
            {
                // Catch the error
                Logger.ExceptionLog(exp);
                base.ShowMessage(FlagQuestion_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This event handler helps to send an alert email
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FlagQuestion_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear all the error and success labels
                FlagQuestion_topErrorMessageLabel.Text = string.Empty;
                FlagQuestion_topSuccessMessageLabel.Text = string.Empty;

                // Validate the fields
                if (!IsValidData())
                    return;

                // Send email
                new EmailHandler().SendMail(FlagQuestion_fromHiddenField.Value.Trim(),
                    FlagQuestion_toHiddenField.Value.Trim(), FlagQuestion_ccTextBox.Text.Trim(),
                    FlagQuestion_subjectValueLabel.Text.Trim(), FlagQuestion_reasonTextBox.Text.Trim());

                // Show successful message
                base.ShowMessage(FlagQuestion_topSuccessMessageLabel,
                    Resources.HCMResource.FlagQuestion_AlertSentSuccessfully);

                //Script that close the popup window
                string closeScript = "self.close();";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Closescript", closeScript, true); 

            }
            catch (Exception exp)
            {
                // Catch the error
                Logger.ExceptionLog(exp);
                base.ShowMessage(FlagQuestion_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handler                                               

        #region Protected Methods                                              

        /// <summary>
        /// This override method helps to validate input fields
        /// </summary>
        /// <returns>Returns FALSE if the condition meets TRUE</returns>
        protected override bool IsValidData()
        {
            // Validate reason feild
            if (FlagQuestion_reasonTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(FlagQuestion_topErrorMessageLabel,
                    Resources.HCMResource.FlagQuestion_Reasoncannotbeempty);
                return false;
            }
            else
                return true;
        }

        /// <summary>
        /// This protected method is used to load the default values when the page is loaded.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Methods                                           
    }
}