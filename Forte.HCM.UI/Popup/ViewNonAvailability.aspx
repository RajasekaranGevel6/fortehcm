﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewNonAvailability.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.ViewNonAvailability"
    Title="Set Vacation Dates" %>

<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ViewNonAvailability_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <script type="text/javascript" language="javascript">

        function CloseViewNonAvailabilityWindow()
        {
            var btncnrl = '<%= Request.QueryString["btncnrl"] %>';

            if (window.dialogArguments)
            {
                window.opener = window.dialogArguments;
            }

            // Trigger the click event of the refresh button in the parent page.
            window.opener.document.getElementById(btncnrl).click();

            self.close();
        }

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="td_padding_top_10">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                            <asp:Literal ID="ViewNonAvailability_titleLiteral" runat="server" Text="Set Vacation Dates"></asp:Literal>
                        </td>
                        <td style="width: 25%" valign="top" align="right">
                            <asp:ImageButton ID="ViewNonAvailability_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <table width="100%" cellpadding="0" cellspacing="3" border="0" class="popupcontrol_view_assessor_non_availability_bg">
                    <tr>
                        <td style="width: 100%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="ViewNonAvailability_messageUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="ViewNonAvailability_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknErrorMessage" Width="100%" Text=""></asp:Label>
                                                <asp:Label ID="ViewNonAvailability_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknSuccessMessage" Width="100%" Text=""></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 100%" valign="top">
                                        <asp:UpdatePanel ID="ViewNonAvailability_inputControlsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 26%" valign="top" class="popup_td_padding_2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="header_bg" align="center">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="width: 50%" align="left" class="header_text_bold">
                                                                                    <asp:Literal ID="ViewNonAvailability_calendarLiteral" runat="server" Text="Calendar"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" class="view_assessor_calendar_grid_body_bg" valign="top">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td class="set_vacation_dates_instructions">
                                                                                    1. Click on the desired date to either set or reset vacation date
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="set_vacation_dates_instructions">
                                                                                    2. Click 'Save' button to update your changes
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="height: 2px">
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:HiddenField ID="ViewNonAvailability_assessorIDHiddenField" runat="server" />
                                                                                    <asp:Calendar ID="ViewNonAvailability_nonAvailabilityCalendar" runat="server" BackColor="White"
                                                                                        BorderColor="Black" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black"
                                                                                        Height="250px" Width="340px" SelectionMode="Day" OtherMonthDayStyle-Font-Underline="false"
                                                                                        DayHeaderStyle-Font-Underline="false" ShowGridLines="false" DayHeaderStyle-Font-Overline="false"
                                                                                        DayStyle-Font-Underline="false" DayStyle-Font-Overline="false" Font-Underline="false"
                                                                                        TodayDayStyle-Font-Underline="false" OnPreRender="ViewNonAvailability_nonAvailabilityCalendar_PreRender"
                                                                                        OnSelectionChanged="ViewNonAvailability_nonAvailabilityCalendar_SelectionChanged">
                                                                                        <DayStyle Font-Underline="false" Wrap="false" CssClass="no_underline" />
                                                                                        <SelectedDayStyle Font-Underline="false" BackColor="#FA9476" ForeColor="#000000">
                                                                                        </SelectedDayStyle>
                                                                                        <OtherMonthDayStyle Font-Underline="false" BackColor="#EEEEEE" ForeColor="Black" />
                                                                                        <TodayDayStyle Font-Underline="false" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                                                                    </asp:Calendar>
                                                                                </td>
                                                                            </tr>
                                                                             <tr>
                                                                                <td style="height: 3px">
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 8px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="ViewNonAvailability_buttonControlsUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="td_height_5" colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="ViewNonAvailability_saveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                        Visible="true" ToolTip="Click here save the selected non availability dates"
                                        OnClick="ViewNonAvailability_saveButton_Click" />
                                </td>
                                <td valign="middle" align="left">
                                    &nbsp;<asp:LinkButton ID="ViewNonAvailability_cancelLinkButton" runat="server" Text="Cancel"
                                        SkinID="sknPopupLinkButton" OnClientClick="javascript:CloseMe();" ToolTip="Click here to close the window"> </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_2">
            </td>
        </tr>
    </table>
</asp:Content>
