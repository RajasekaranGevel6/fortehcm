﻿var _countQuestIncrementContainer = 0;
var _currentQuestIncrementSeconds = 0;
function ActivateQuestIncrementCountDown(strContainerID, initialValue) {
    _countQuestIncrementContainer = document.getElementById(strContainerID);

    if (!_countQuestIncrementContainer) {
        alert("count down error: container does not exist: " + strContainerID +
            "\nmake sure html element with this ID exists");
        return;
    }

    SetCountQuestIncrementText(parseInt(initialValue));
    window.setTimeout("CountQuestIncrementTick()", 1000);

}

function CountQuestIncrementTick() {
    SetCountQuestIncrementText(_currentQuestIncrementSeconds + 1);
    window.setTimeout("CountQuestIncrementTick()", 1000);
}

function SetCountQuestIncrementText(seconds) {
    document.getElementById("TestConductor_questionTimeEllapsedHiddenField").value = seconds;
    //store:
    _currentQuestIncrementSeconds = seconds;

    //get minutes:
    var minutes = parseInt(seconds / 60);

    //shrink:
    seconds = (seconds % 60);

    //get hours:
    var hours = parseInt(minutes / 60);

    //shrink:
    minutes = (minutes % 60);

    //build text:
    var strText = AddZero(hours) + ":" + AddZero(minutes) + ":" + AddZero(seconds);

    //apply:
    _countQuestIncrementContainer.innerHTML = strText;
}

var _countIncrementContainer = 0;
var _currentIncrementSeconds = 0;
var _currentMaximumSeconds = 0;
function ActivateIncrementCountDown(strContainerID, initialValue, maximumValue) {
    _countIncrementContainer = document.getElementById(strContainerID);
    _currentMaximumSeconds = maximumValue;

    if (!_countIncrementContainer) {
        alert("count down error: container does not exist: " + strContainerID +
            "\nmake sure html element with this ID exists");
        return;
    }

    SetCountIncrementText(initialValue);
    window.setTimeout("CountIncrementTick()", 1000);
}

function CountIncrementTick() {
    if (parseInt(_currentIncrementSeconds) >= parseInt(_currentMaximumSeconds)) {
        __doPostBack("TestConductor_quitTestLinkButton", "SESS_ELPS");
    }
    else {
        SetCountIncrementText(parseInt(_currentIncrementSeconds) + 1);
        window.setTimeout("CountIncrementTick()", 1000);
    }
}

function SetCountIncrementText(seconds) {
    document.getElementById("TestConductor_timeEllapsedHiddenField").value = seconds;

    //store:
    _currentIncrementSeconds = seconds;

    //get minutes:
    var minutes = parseInt(seconds / 60);

    //shrink:
    seconds = (seconds % 60);

    //get hours:
    var hours = parseInt(minutes / 60);

    //shrink:
    minutes = (minutes % 60);

    //build text:
    var strText = AddZero(hours) + ":" + AddZero(minutes) + ":" + AddZero(seconds);

    //apply:
    _countIncrementContainer.innerHTML = strText;
}
var _countDowncontainer = 0;
var _currentSeconds = 0;

function ActivateCountDown(strContainerID, initialValue) {
    _countDowncontainer = document.getElementById(strContainerID);

    if (!_countDowncontainer) {
        alert("count down error: container does not exist: " + strContainerID +
            "\nmake sure html element with this ID exists");
        return;
    }

    SetCountdownText(initialValue);
    window.setTimeout("CountDownTick()", 1000);
}

function CountDownTick() {
    
    if (_currentSeconds > 0) {
        SetCountdownText(_currentSeconds - 1);
        window.setTimeout("CountDownTick()", 1000);
    }
    
}

function SetCountdownText(seconds) {
    //store:
    _currentSeconds = seconds;

    //get minutes:
    var minutes = parseInt(seconds / 60);

    //shrink:
    seconds = (seconds % 60);

    //get hours:
    var hours = parseInt(minutes / 60);

    //shrink:
    minutes = (minutes % 60);

    if (seconds != -1) {
        //build text:
        var strText = AddZero(hours) + ":" + AddZero(minutes) + ":" + AddZero(seconds);

        //apply:
        _countDowncontainer.innerHTML = strText;

        document.getElementById("TestConductor_timeRemainsLabel").innerHTML = strText;
    }
}

function AddZero(num) {
    return ((num >= 0) && (num < 10)) ? "0" + num : num + "";
}
function DockRightPanel(dockPanelId, unDockPanelId) {
    var dockPanel = document.getElementById(dockPanelId);
    var unDockPanel = document.getElementById(unDockPanelId);
    if (dockPanel.style.display == "none") {
        dockPanel.style.display = "";
        unDockPanel.style.display = "none";
        document.getElementById("TestConductor_maintainDockHiddenField").value = "undock";
    }
    else {
        unDockPanel.style.display = "";
        dockPanel.style.display = "none";
        document.getElementById("TestConductor_maintainDockHiddenField").value = "dock";
    }
}
function WindowLoad() {
    var elapsedStatus = document.getElementById("TestConductor_timeElapsedStatus").value;
    if (elapsedStatus != "Elapsed") {
        var elapsedTime = document.getElementById("TestConductor_timeEllapsedHiddenField").value;
        var recommendedTime = document.getElementById("TestConductor_recommendedTimeHiddenField").value;
        var questionElapsedTime = document.getElementById("TestConductor_questionTimeEllapsedHiddenField").value;
        ActivateCountDown("TestConductor_testTimeRemainSpan", (recommendedTime - elapsedTime));
        ActivateIncrementCountDown("TestConductor_testTimeEllapsedSpan", elapsedTime, recommendedTime);
        ActivateQuestIncrementCountDown("TestConductor_qstTimeEllapsedSpan", questionElapsedTime);
    }
}
function OpenTestCompletedPage(pageName) {
    if (window.dialogArguments) {
        window.opener = window.dialogArguments;
    }
    window.opener.location = pageName;
    window.close();
}