﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Threading;
using Kalitte.Dashboard.Framework;
using Kalitte.Dashboard.Framework.Types;
using System.Web.Configuration;

namespace Forte.HCM.UI
{
    public partial class DefaultMaster : System.Web.UI.MasterPage
    {
        protected string pageCaption;
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        public void SetPageCaption(string pageTitle)
        {
            if (pageTitle != null)
            {

                pageCaption = pageTitle.Trim();
            }


        }
    }
}
