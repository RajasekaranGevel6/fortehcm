﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/HomeMaster.Master"
CodeBehind="Help.aspx.cs" Inherits="Forte.HCM.UI.Help" %>

<%@ MasterType VirtualPath="~/MasterPages/HomeMaster.Master" %>
<asp:Content ID="Help_bodyContent" ContentPlaceHolderID="HomeMaster_body"
    runat="server">
    <table width="100%" cellpadding="0" cellspacing="0" class="td_pading_logo_10">
        <tr>
            <td class="header_bg">
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td width="90%" class="header_text_bold">
                            <asp:Literal ID="Help_headerLiteral" runat="server" Text="Help"></asp:Literal>
                        </td>
                        <td width="10%" align="right">
                            <table cellpadding="3" cellspacing="4" width="100%">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="Help_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="Help_cancelLinkButton_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="Help_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="Help_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" style="color: #81868A; font-size: small">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top" >
                            <br />
                            <b><u>Online Testing Module</u></b><br/>
                            Allows create questions, compose test, create test sessions, schedule candidates, view reports etc.<br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" >
                            <b><u>TalentScout</u></b><br/>
                            Allows to search candidates from the repository through the search parameters and weightages. <br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" >
                            <b><u>Position Profile</u></b><br/>
                            Allows to create forms and position profiles that defines the criteria for searching candidates. <br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" >
                            <b><u>Resume Repository</u></b><br/>
                            Allows to search candidates from job boards, upload resumes against for parsing, edit resumes, etc. <br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" >
                            <b><u>Site Admin</u></b><br/>
                            Allows to setup subscription & features, enroll customers, manage & assign rights for roles and users. <br/><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" >
                            <b><u>Customer Admin</u></b><br/>
                            Allows to manage corporate users and account details. <br/><br/><br/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
