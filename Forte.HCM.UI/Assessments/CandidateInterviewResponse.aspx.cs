﻿using System;
using System.Net;
using System.Web.UI;
using System.Configuration;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.IO;

namespace Forte.HCM.UI.Assessments
{
    public partial class CandidateInterviewResponse : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Protected Variables
        protected string videoURL;
        protected string questionID;
        #endregion Protected Variables

        #region Events Handlers
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                AssessInterviewResponse_successMessageLabel.Text = string.Empty;
                AssessInterviewResponse_errorMessageLabel.Text = string.Empty;

                // Load the skill values.
                if (!IsPostBack)
                {
                    LoadValues();

                    
                    string serverURL = ConfigurationManager.AppSettings["VIDEO_STREAMING_SERVER_URL"].ToString();
                    videoURL = string.Concat(serverURL, Request.QueryString["candidatesessionid"], "_" + Request.QueryString["attemptid"]);
                    questionID = Request.QueryString["testquestionid"].ToString();

                    string folderName =Request.QueryString["candidatesessionid"] + "_" + Request.QueryString["attemptid"];
                    string fileName = Request.QueryString["testquestionid"].ToString();

                    /*AssessInterviewResponse_downloadVideoImageButton.Attributes.Add("onclick",
                        "javascript:return DownloadVideo('" + folderName +"','" + fileName + "');");*/
                }
                ExpandRestore();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(AssessInterviewResponse_errorMessageLabel, exp.Message);
            }
        }
          /// <summary>
        /// Handler method that will be called when the save button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void AssessInterviewResponse_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                SaveRatingComments();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessInterviewResponse_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler to rating values to percentage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CandidateInterviewResponse_otherAssessorRatingCommentsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                decimal ratingPercentage = 0;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    HiddenField CandidateInterviewResponse_ratingPercentageHiddenField = (HiddenField)e.Row.FindControl(
                        "CandidateInterviewResponse_ratingPercentageHiddenField");

                    Label CandidateInterviewResponse_ratingPercentageLabel = (Label)e.Row.FindControl(
                        "CandidateInterviewResponse_ratingPercentageLabel");

                    if (!string.IsNullOrEmpty(Convert.ToString(CandidateInterviewResponse_ratingPercentageHiddenField.Value)))
                        ratingPercentage = Convert.ToDecimal(CandidateInterviewResponse_ratingPercentageHiddenField.Value);


                    if (ratingPercentage <= 0)
                        CandidateInterviewResponse_ratingPercentageLabel.Visible = false;
                    else
                    {
                        CandidateInterviewResponse_ratingPercentageLabel.Visible = true;
                        CandidateInterviewResponse_ratingPercentageLabel.Text = string.Concat("Rating(", string.Format("{0}%", string.Format("{0:0.00}", ratingPercentage)), ")");
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessInterviewResponse_errorMessageLabel, exp.Message);
            }
        }

 

        #endregion Events Handlers

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden protected method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
            string candidateSessionID = null;
            string questionKey = string.Empty;
            string interviewKey = string.Empty;

            // Check if candidate session ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]))
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "Candidate session ID is not present");
                return;
            }
            candidateSessionID = Request.QueryString["candidatesessionid"].Trim();

            // Check if attempt ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "Attempt ID is not present");
                return;
            }

            // Check if attempt ID is valid.
            int attemptID = 0;
            if (int.TryParse(Request.QueryString["attemptid"].Trim(), out attemptID) == false)
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "Attempt ID is invalid");
                return;
            }

            // Check if test question ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["testquestionid"]))
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "Test question ID is not present");
                return;
            }

            // Check if test question ID is valid.
            int testQuestionID = 0;
            if (int.TryParse(Request.QueryString["testquestionid"].Trim(), out testQuestionID) == false)
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "Test question ID is invalid");
                return;
            }

            // Check if interview key is given 
            if (Utility.IsNullOrEmpty(Request.QueryString["interviewKey"]))
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "Interview Key is not present");
                return;
            }
            interviewKey = Request.QueryString["interviewKey"].ToString();

            // Check if question key is given 
            if (Utility.IsNullOrEmpty(Request.QueryString["questionKey"]))
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "Question Key is not present");
                return;
            }
            questionKey = Request.QueryString["questionKey"].ToString();

            AssessInterviewResponse_saveButton.Enabled = true;
            

            // Get interview question attributes
            QuestionDetail questionAttributes = new InterviewReportBLManager().
                GetInterviewTestQuestionAttributesByKeys(interviewKey, questionKey);
                        
            if (questionAttributes != null)
            {
                //Set maximum rating star
                if (Convert.ToInt32(questionAttributes.Rating) > 0)
                    AssessInterviewResponse_rating.MaxRating = Convert.ToInt32(questionAttributes.Rating);
                else
                    AssessInterviewResponse_rating.MaxRating = 10;
                //Set question comments
                InterviewCandidateTestDetails_commentValueLabel.Text = questionAttributes.Comments == null ? questionAttributes.Comments : 
                    questionAttributes.Comments.ToString().ToString().Replace(Environment.NewLine, "<br />");
            }

            // Get interview response
            InterviewResponseDetail logDetail = new InterviewReportBLManager().
                GetInterviewResponseDetail(candidateSessionID, attemptID, testQuestionID, base.userID);

            if (logDetail == null || logDetail.Question==null)
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel,
                    "No data found to display");

                return;
            }
            // Assign details.
            AssessInterviewResponse_questionValueLabel.Text = logDetail.Question == null ? logDetail.Question :
                logDetail.Question.ToString().ToString().Replace(Environment.NewLine, "<br />");
            if (logDetail.HasImage)
            {
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = logDetail.QuestionImage;
                CandidateInterviewResponse_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=INTERVIEW_QUESTION_IMAGE&questionKey=" + logDetail.QuestionKey;
                CandidateInterviewResponse_questionImageDiv.Style["display"] = "block";
            }
            else
            {
                CandidateInterviewResponse_questionImageDiv.Style["display"] = "none";
            }
            
            if(!Utility.IsNullOrEmpty(logDetail.ChoiceDesc))
            {
                string choiceDesc = logDetail.ChoiceDesc == null ? logDetail.ChoiceDesc : logDetail.ChoiceDesc.ToString().ToString().Replace("&#x0D;", "<br />");
                CandidateResponse_questionAnswerLabel.Text = choiceDesc == null ? choiceDesc : choiceDesc.ToString().ToString().Replace(Environment.NewLine, "<br />");
            }

            if (!Utility.IsNullOrEmpty(logDetail.CandidateComments))
            {
                string candidateComments =  logDetail.CandidateComments == null ? logDetail.CandidateComments : logDetail.CandidateComments.ToString().ToString().Replace(Environment.NewLine, "<br />");
                AssessInterviewResponse_candidateCommentsValueLabel.Text = candidateComments;
            }
            CandidateInterviewResponse_weightageValueLabel.Text = logDetail.Weightage.ToString();
            CandidateInterviewResponse_categoryValueLabel.Text = logDetail.CategoryName;
            CandidateInterviewResponse_subjectValueLabel.Text = logDetail.SubjectName;
            AssessInterviewResponse_skippedValueLabel.Text = logDetail.Skipped ? "Yes" : "No";
            AssessInterviewResponse_complexityValueLabel.Text = logDetail.Complexity;
            AssessInterviewResponse_testAreaValueLabel.Text = logDetail.TestArea;

            
            if (logDetail.RatingComments == null)
            {
                ShowMessage(AssessInterviewResponse_errorMessageLabel, "");
                return;
            }
            for (int i = 0; i < logDetail.RatingComments.Count; i++)
            {
                if (logDetail.RatingComments[i].assessorID == base.userID)
                {
                    if (logDetail.RatingComments[i].CandidateSessionId == candidateSessionID)
                        AssessInterviewResponse_rating.CurrentRating = Convert.ToInt32(logDetail.RatingComments[i].Rating);

                    InterviewCandidateTestDetails_userCommentsTextBox.Text = logDetail.RatingComments[i].userComments;
                    logDetail.RatingComments.RemoveAt(i);
                }
            }
            
            CandidateInterviewResponse_otherAssessorRatingCommentsGridView.DataSource = logDetail.RatingComments;
            CandidateInterviewResponse_otherAssessorRatingCommentsGridView.DataBind();
        }

        #endregion Protected Overridden Methods

        #region Private Methods

        /// <summary>
        /// Method that checks if the given remote file exists or not.
        /// </summary>
        /// <param name="url">
        /// A <see cref="string"/> that holds the remote file URL.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the exist status. True if exists
        /// and false otherwise.
        /// </returns>
        private bool IsRemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";

                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                //Returns TURE if the Status code == 200
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                // Any exception will returns false.
                return false;
            }
        }

        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>
        private void ExpandRestore()
        {
            if (CandiateInterviewResponse_restoreHiddenField.Value == "Y")
            {
                CandidateInterviewResponse_otherCommentsDiv.Style["display"] = "block";
                CandiateInterviewResponse_otherCommentsUpSpan.Style["display"] = "none";
                CandiateInterviewResponse_otherCommentsDownSpan.Style["display"] = "block";
                CandiateInterviewResponse_ratingDiv.Style["height"] = RESTORED_HEIGHT;
            }
            else
            {
                CandidateInterviewResponse_otherCommentsDiv.Style["display"] = "none";
                CandiateInterviewResponse_otherCommentsUpSpan.Style["display"] = "block";
                CandiateInterviewResponse_otherCommentsDownSpan.Style["display"] = "none";
                CandiateInterviewResponse_ratingDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            CandiateInterviewResponse_otherCommentsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                CandiateInterviewResponse_ratingDiv.ClientID + "','" +
                CandidateInterviewResponse_otherCommentsDiv.ClientID + "','" +
                CandiateInterviewResponse_otherCommentsUpSpan.ClientID + "','" +
                CandiateInterviewResponse_otherCommentsDownSpan.ClientID + "','" +
                CandiateInterviewResponse_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
        }

        /// <summary>
        /// Method to save the rating comments
        /// </summary>
        private void SaveRatingComments()
        {
            QuestionDetail questionDetails = new QuestionDetail();

            questionDetails.Comments = InterviewCandidateTestDetails_userCommentsTextBox.Text.Trim();
            questionDetails.Rating = Convert.ToDecimal(AssessInterviewResponse_rating.CurrentRating.ToString());
            questionDetails.QuestionKey = Request.QueryString.Get("questionKey").ToString();

            new InterviewReportBLManager().SaveQuestionDetailsWithRatingComments(questionDetails, Request.QueryString.Get("candidatesessionid").ToString(),
                Convert.ToInt32(Request.QueryString.Get("attemptid").ToString()),
                Convert.ToInt32(Request.QueryString.Get("assessorID").ToString()), base.userID);

            base.ShowMessage(AssessInterviewResponse_successMessageLabel, "Comments and rating saved successfully");
        }
        #endregion Private Methods

        protected void AssessInterviewResponse_downloadVideoImageButton_Click(object sender, ImageClickEventArgs e)
        {

            
            string filePath = ConfigurationManager.AppSettings["OFFLINE_INTERVIEW_VIDEO_FILE"].ToString();

            string folderName = Request.QueryString["candidatesessionid"] + "_" + Request.QueryString["attemptid"];
            string fileName = Request.QueryString["testquestionid"].ToString() + ".flv";

            filePath += folderName + "//" + fileName;

             if (File.Exists(Server.MapPath(filePath)))
             {
                 AssessInterviewResponse_downloadVideoHyperLink.NavigateUrl = "../Common/CandidateImageHandler.ashx?filepath=" + filePath + "&filetype=FLV";
                 string downloadScript = "<script>window.location.href = document.getElementById('AssessInterviewResponse_downloadVideoHyperLink').href;</script>";
                 ScriptManager.RegisterStartupScript(this, this.GetType(), "videodownload", downloadScript, false); 
             }
             else
             {
                 string script = string.Format("alert('{0}');", "File not found");     
                 ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "key_name", script, true);
             } 
           
        }
    }
}