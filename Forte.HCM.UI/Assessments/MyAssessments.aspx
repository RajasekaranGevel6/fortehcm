﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyAssessments.aspx.cs" Inherits="Forte.HCM.UI.Assessments.MyAssessments"
    MasterPageFile="~/MasterPages/InterviewMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/ConfirmInterviewScoreMsgControl.ascx" TagName="ConfirmInterviewScoreMsgControl"
    TagPrefix="uc4" %>
<asp:Content ID="MyAssessments_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="MyAssessments_headerLiteral" runat="server" Text="My Assessments"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="Myassessments_topDownloadImageButton" runat="server" ToolTip="Export to Excel"
                                            SkinID="sknExportExcelImageButton" CommandName="Export" OnClick="Myassessments_topDownloadImageButton_Click" />
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;<asp:LinkButton ID="MyAssessments_topResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="MyAssessments_resetLinkButton_Click"></asp:LinkButton>
                                        &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="MyAssessments_topCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="MyAssessments_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="MyAssessments_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="MyAssessments_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="MyAssessments_searchButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" valign="middle">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="MyAssessments_searchCriteriasDiv" runat="server" style="display: block;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="panel_bg">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                                                            <tr>
                                                                <td style="width: 14%">
                                                                    <asp:Label ID="MyAssessments_interviewNameHeadLabel" runat="server" Text="Interview Name"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 30%">
                                                                    <asp:TextBox ID="MyAssessments_interviewNameTextBox" MaxLength="50" runat="server"
                                                                        Width="60%"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 4%">
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="MyAssessments_interviewDescriptionHeadLabel" runat="server" Text="Interview Description"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <asp:TextBox ID="MyAssessments_interviewDescriptionTextBox" MaxLength="500" runat="server"
                                                                        Width="98%"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_5">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 14%">
                                                                    <asp:Label ID="MyAssessments_assessmentStatusHeadLabel" runat="server" Text="Assessment    Status"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 30%">
                                                                    <div style="float: left; padding-right: 5px;">
                                                                        <asp:DropDownList ID="MyAssessments_assessmentStatusDropDownList" runat="server"
                                                                            Width="173px" DataTextField="AttributeValue" DataValueField="AttributeID">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div style="float: left;">
                                                                        <asp:ImageButton ID="MyAssessments_assessmentStatusImageButton" SkinID="sknHelpImageButton"
                                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the interview assessment status here" />
                                                                    </div>
                                                                </td>
                                                                <td style="width: 4%">
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="MyAssessments_positionProfileHeadLabel" runat="server" Text="Position Profile"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <div style="float: left; padding-right: 5px; width: 92%">
                                                                        <asp:TextBox ID="MyAssessments_positionProfileTextBox" MaxLength="50" runat="server"
                                                                            Width="100%"></asp:TextBox>
                                                                        <asp:HiddenField ID="MyAssessments_positionProfileHiddenField" runat="server" />
                                                                    </div>
                                                                    <div style="float: left; padding-left: 2px">
                                                                        <asp:ImageButton ID="MyAssessments_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                            runat="server" ImageAlign="Middle" ToolTip="Click here to select the position profile" />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table>
                                                            <tr>
                                                                <td class="td_height_2">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RadioButtonList ID="MyAssessments_interviewListOptionRadioButtonList" runat="server"
                                                                        RepeatDirection="Horizontal" SkinID="sknSkillRadioButtonList" OnSelectedIndexChanged="MyAssessments_interviewListOptionRadioButtonList_SelectedIndexChanged"
                                                                        AutoPostBack="True">
                                                                        <asp:ListItem Text="Offline Interview" Value="OFFINT"></asp:ListItem>
                                                                        <asp:ListItem Text="Interview" Value="ONINT"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table align="right">
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="MyAssessments_searchButton" runat="server" SkinID="sknButtonId" Text="Search"
                                                                        OnClick="MyAssessments_searchButton_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr id="MyAssessments_searchResultsTR" runat="server">
                        <td class="header_bg" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="MyAssessments_searchResultsLiteral" runat="server" Text="Assessment Details"></asp:Literal>
                                        &nbsp;<asp:Label ID="MyAssessments_sortHelpLabel" runat="server" SkinID="sknLabelText"></asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="MyAssessments_searchResultsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="MyAssessments_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span>
                                        <span id="MyAssessments_searchResultsDownSpan" style="display: block;" runat="server">
                                            <asp:Image ID="MyAssessments_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                        <asp:HiddenField ID="MyAssessments_restoreHiddenField" runat="server" />
                                        <asp:HiddenField ID="MyAssessments_isMaximizedHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="grid_body_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="MyAssessments_assessmentDetailsGridViewUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <div style="height: 200px; width: 100%; overflow: auto" runat="server" id="MyAssessments_schedulerDiv">
                                                    <asp:GridView ID="MyAssessments_assessmentDetailsGridView" runat="server" AllowSorting="True"
                                                        AutoGenerateColumns="False" OnSorting="MyAssessments_assessmentDetailsGridView_Sorting"
                                                        OnRowCommand="MyAssessments_assessmentDetailsGridView_RowCommand" OnRowCreated="MyAssessments_assessmentDetailsGridView_RowCreated"
                                                        OnRowDataBound="MyAssessments_assessmentDetailsGridView_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="75px">
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="MyAssessments_viewInterviewSummaryHyperLink" runat="server" Target="_blank"
                                                                        ToolTip="View Interview Summary" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_test_result.gif">
                                                                    </asp:HyperLink>
                                                                    <asp:HiddenField ID="MyAssessments_assessmentDetailsTestStatusHiddenField" Value='<%# Eval("TestStatus") %>'
                                                                        runat="server" />
                                                                    <asp:ImageButton ID="MyAssessments_assessmentDetailsAssessorRatingImageButton" runat="server"
                                                                        ImageUrl="~/App_Themes/DefaultTheme/Images/show_credit_remarks.gif" ToolTip="Assessor Rating"
                                                                        Visible="true" CommandName="AssessorRating" />
                                                                    <asp:HyperLink ID="MyAssessments_candidateRatingSummaryHyperLink" runat="server"
                                                                        Target="_blank" ToolTip="Candidate Rating Summary" ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif">
                                                                    </asp:HyperLink>
                                                                    <asp:ImageButton ID="MyAssessments_assessmentDetailsViewTrackingDetailsImageButton"
                                                                        runat="server" SkinID="sknTrackingImageButton" ToolTip="Offline Interview Tracking Details"
                                                                        Visible="false" CommandName="Tracking" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="CandidateName" HeaderText="Candidate Name" SortExpression="CANDIDATE_NAME">
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Completed Date" SortExpression="COMPLETED_DATE DESC">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="MyAssessments_assessmentDetailsCompletedDateLabel"
                                                                        Text='<%# GetDateFormat(Convert.ToDateTime(Eval("CompletedDate"))) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="InterviewName" HeaderText="Interview Name" SortExpression="INTERVIEW_NAME">
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="PositionProfileName" HeaderText="Position Profile Name"
                                                                SortExpression="POSITION_PROFILE_NUMBER"></asp:BoundField>
                                                            <asp:BoundField DataField="AssessmentStatus" HeaderText="Assessment Status" SortExpression="ASSESSMENT_STATUS">
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TestStatus" HeaderText="Interview Status" SortExpression="TEST_STATUS">
                                                            </asp:BoundField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="MyAssessments_assessmentDetailsGridView_CandidateInterviewSessionKey"
                                                                        runat="server" Value='<%# Eval("CandidateInterviewSessionKey") %>' />
                                                                    <asp:HiddenField ID="MyAssessments_assessmentDetailsGridView_AssessorId" runat="server"
                                                                        Value='<%# Eval("AssessorId") %>' />
                                                                    <asp:HiddenField ID="MyAssessments_assessmentDetailsGridView_InterviewSessionKey"
                                                                        runat="server" Value='<%# Eval("InterviewSessionKey") %>' />
                                                                    <asp:HiddenField ID="MyAssessments_assessmentDetailsGridView_attemptIDHiddenField"
                                                                        runat="server" Value='<%# Eval("AttemptID") %>' />
                                                                    <asp:HiddenField ID="MyAssessments_assessmentDetailsGridView_testKeyHiddenField"
                                                                        runat="server" Value='<%# Eval("InterviewTestKey") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div style="display: block; height: 200px; width: 100%; overflow: auto" runat="server"
                                                    id="MyAssessments_onlineInterviewSchedulerDiv">
                                                    <asp:GridView ID="MyAssessments_onlineSchedulerList_GridView" runat="server" SkinID="sknWrapHeaderGrid"
                                                        AutoGenerateColumns="False" EnableModelValidation="True" Width="100%" 
                                                        OnRowDataBound="MyAssessments_onlineSchedulerList_GridView_RowDataBound" 
                                                        onsorting="MyAssessments_onlineSchedulerList_GridView_Sorting" 
                                                        onrowcommand="MyAssessments_onlineSchedulerList_GridView_RowCommand">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="MyAssessments_onlineSchedulerList_GridViewImageButton" runat="server"
                                                                        ImageUrl="~/App_Themes/DefaultTheme/Images/istart.png" ToolTip="Interview Conduction"
                                                                        Visible="true" CommandName="IConduction" />
                                                                    <asp:HyperLink ID="MyAssessments_onlineSchedulerList_GridView_candidateRatingSummaryHyperLink"
                                                                        runat="server" Target="_blank" ToolTip="Candidate Rating Summary" ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif">
                                                                    </asp:HyperLink>
                                                                    <asp:HiddenField ID="MyAssessments_onlineSchedulerList_GridView_slotKey_Label" Value='<%# Eval("SlotKey") %>'
                                                                        runat="server" />
                                                                    <asp:HiddenField ID="MyAssessments_onlineSchedulerList_GridView_firstName_Label"
                                                                        Value='<%# Eval("FirstName") %>' runat="server" />
                                                                    <asp:HiddenField ID="MyAssessments_onlineSchedulerList_GridView_emailId_Label" Value='<%# Eval("EmailId") %>'
                                                                        runat="server" />
                                                                    <asp:HiddenField ID="MyAssessments_onlineSchedulerList_GridView_GenId_HiddenField"
                                                                        Value='<%# Eval("CandidateInterviewID") %>' runat="server" />
                                                                    <asp:HiddenField ID="MyAssessments_onlineSchedulerList_GridView_sessionKey_HiddenField"
                                                                        Value='<%# Eval("CandidateSessionID") %>' runat="server" />
                                                                    <asp:HiddenField ID="MyAssessments_onlineSchedulerList_GridView_chatId_HiddenField"
                                                                        Value='<%# Eval("ChatRoomName") %>' runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Candidate Name" SortExpression="CANDIDATE_NAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="MyAssessments_onlineSchedulerList_GridView_CandidateName_Label" runat="server"
                                                                        Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Completed Date" SortExpression="ONLINE_INTERVIEW_DATE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="MyAssessments_onlineSchedulerList_GridView_InterviewDate_Label" runat="server"
                                                                        Text='<%# GetDateFormat(Convert.ToDateTime(Eval("InterviewDate"))) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Interview Name" SortExpression="ONLINE_INTERVIEW_NAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="MyAssessments_onlineSchedulerList_GridView_ScheduledBy_Label" runat="server"
                                                                        Text='<%# Eval("InterviewName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Position Profile Name" SortExpression="POSITION_PROFILE_NAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="MyAssessments_onlineSchedulerList_GridView_positionProfileName_Label"
                                                                        runat="server" Text='<%# Eval("PositionProfileName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Assessmet/Interview Status" SortExpression="ONLINE_INTERVIEW_STATUS">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="MyAssessments_onlineSchedulerList_GridView_Status_Label" runat="server"
                                                                        Text='<%# Eval("SessionStatus") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="MyAssessments_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="MyAssessments_showPublishUrlUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <div style="float: left;">
                                                    <asp:Panel ID="MyAssessments_emailConfirmationPanel" runat="server" Style="display: none;
                                                        height: 254px;" CssClass="popupcontrol_confirm_publish_candidate_interview_response">
                                                        <div id="MyAssessments_emailConfirmationDiv" style="display: none">
                                                            <asp:Button ID="MyAssessments_emailConfirmation_hiddenButton" runat="server" />
                                                        </div>
                                                        <uc4:ConfirmInterviewScoreMsgControl ID="MyAssessments_emailConfirmation_ConfirmMsgControl"
                                                            runat="server" Title="Publish Candidate Interview Response" Type="InterviewScoreConfirmType" />
                                                    </asp:Panel>
                                                    <ajaxToolKit:ModalPopupExtender ID="MyAssessments_emailConfirmation_ModalPopupExtender"
                                                        BehaviorID="MyAssessments_emailConfirmation_ModalPopupExtender" runat="server"
                                                        PopupControlID="MyAssessments_emailConfirmationPanel" TargetControlID="MyAssessments_emailConfirmation_hiddenButton"
                                                        BackgroundCssClass="modalBackground">
                                                    </ajaxToolKit:ModalPopupExtender>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="MyAssessments_pagingNavigatorUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                        <uc3:PageNavigator ID="MyAssessments_pagingNavigator" runat="server" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="MyAssessments_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="MyAssessments_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="MyAssessments_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="MyAssessments_searchButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="0" align="right">
                    <tr>
                        <td>
                            <asp:ImageButton ID="Myassessments_bottomDownloadImageButton" runat="server" ToolTip="Export to Excel"
                                SkinID="sknExportExcelImageButton" CommandName="Export" OnClick="Myassessments_bottomDownloadImageButton_Click" />
                        </td>
                        <td>
                            &nbsp;<asp:LinkButton ID="MyAssessments_bottomResetLinkButton" runat="server" Text="Reset"
                                SkinID="sknActionLinkButton" OnClick="MyAssessments_resetLinkButton_Click"></asp:LinkButton>
                            &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="MyAssessments_bottomCancelLinkButton" runat="server"
                                Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
