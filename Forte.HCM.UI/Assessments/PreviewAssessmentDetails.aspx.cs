﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PreviewAssessmentDetails.aspx.cs
// File that represents the user interface layout and functionalities
// for the PreviewAssessmentDetails page. This page helps in viewing 
// the candidate interview videos and provide rating. Also helps in 
// viewing other assessor rating and consolidated rating. 

#endregion Header

using System;
using System.Data;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.Assessments
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the PreviewAssessmentDetails page. This page helps in viewing 
    /// the candidate interview videos and provide rating. Also helps in 
    /// viewing other assessor rating and consolidated rating. This class
    /// inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class PreviewAssessmentDetails : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Preview Assessment Summary");

                PreviewAssessmentDetails_topErrorMessageLabel.Text = string.Empty;
                PreviewAssessmentDetails_topSuccessMessageLabel.Text = string.Empty;
                if (!IsPostBack)
                {
                    PreviewAssessmentDetails_completedDateTextBox.Text = string.Empty;

                    // Session["OSTSessionKey"] = Request.QueryString.Get("sessionkey");
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PreviewAssessmentDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the question list.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void PreviewAssessmentDetails_questionAreaDataList_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                // Find the row type is DataRow
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    object dr = e.Row.DataItem;

                    decimal _rating=((Forte.HCM.DataObjects.QuestionDetail)(dr)).Rating;
                    int _questionID = ((Forte.HCM.DataObjects.QuestionDetail)(dr)).QuestionID;

                    if (((Forte.HCM.DataObjects.QuestionDetail)(dr)).RatingEdit == "Y")
                    {
                        //e.Row.BackColor = System.Drawing.Color.LightGreen;
                        e.Row.CssClass = "RatingEditable";

                        if (_rating > 0)
                            e.Row.CssClass = "RatingFinished";

                       /* if (_rating < 0)
                            e.Row.CssClass = "NotRating";
                        //e.Row.BackColor = System.Drawing.Color.LightBlue;
                        else if (_rating > 0)
                            e.Row.CssClass = "RatingFinished";
                        //e.Row.BackColor = System.Drawing.Color.LightGray;*/
                    }
                    else
                        e.Row.CssClass = "RatingNotAllowed";
                        //e.Row.BackColor = System.Drawing.Color.LightYellow;
                    

                    // Find a label from the current row.
                    Label PreviewAssessmentDetails_rowNoLabel = (Label)e.Row.FindControl("PreviewAssessmentDetails_rowNoLabel");

                    // Assign the row no to the label.
                    PreviewAssessmentDetails_rowNoLabel.Text = (e.Row.RowIndex + 1).ToString();

                    LinkButton PreviewAssessmentDetails_questionAreaQuestionsLabel =
                        (LinkButton)e.Row.FindControl("PreviewAssessmentDetails_questionAreaQuestionsLabel");

                    HiddenField PreviewAssessmentDetails_questionAreaQuestionIDHiddenField =
                        (HiddenField)e.Row.FindControl("PreviewAssessmentDetails_questionAreaQuestionIDHiddenField");

                    HiddenField PreviewAssessmentDetails_questionAreaQuestionKeyHiddenField =
                        (HiddenField)e.Row.FindControl("PreviewAssessmentDetails_questionAreaQuestionKeyHiddenField");


                    if (((Forte.HCM.DataObjects.QuestionDetail)(dr)).RatingEdit == "Y")
                    {
                        PreviewAssessmentDetails_questionAreaQuestionsLabel.Attributes.Add("onclick",
                            "return OpenInterviewResponse('" + Request.QueryString["ISK"] +
                                "','" + Request.QueryString["CISK"] +
                                "','" + int.Parse(Request.QueryString["ATMPID"]) + "','" +
                                Convert.ToInt32(PreviewAssessmentDetails_questionAreaQuestionIDHiddenField.Value) + "','" +
                                PreviewAssessmentDetails_questionAreaQuestionKeyHiddenField.Value + "','" +
                                Convert.ToInt32((Request.QueryString.Get("assessorId"))) + "','" + Request.QueryString.Get("sessionkey") + "');");
                    }
                    else
                        PreviewAssessmentDetails_questionAreaQuestionsLabel.Style.Add("cursor", "default");

                    Label PreviewAssessmentDetails_questionAreaRatingLabel =
                        (Label)e.Row.FindControl("PreviewAssessmentDetails_questionAreaRatingLabel");
                    if (PreviewAssessmentDetails_questionAreaRatingLabel.Text == "-1")
                        PreviewAssessmentDetails_questionAreaRatingLabel.Text = "N/R";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PreviewAssessmentDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the question link button is 
        /// clicked in the question list.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PreviewAssessmentDetails_questionAreaQuestionsLabel_Click
            (object sender, EventArgs e)
        {
            try
            {
                LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PreviewAssessmentDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the save button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PreviewAssessmentDetails_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                AssessmentSummary assessmentSummary = new AssessmentSummary();

                if(PreviewAssessmentDetails_completedDateTextBox.Text!=string.Empty)
                    assessmentSummary.CompletedOn = Convert.ToDateTime(PreviewAssessmentDetails_completedDateTextBox.Text);

                assessmentSummary.OverallRating = Convert.ToInt32(PreviewAssessmentDetails_userRatingRating.CurrentRating);
                assessmentSummary.Comments = PreviewAssessmentDetails_commentsTextbox.Text;
                assessmentSummary.CreatedBy = base.userID;
                assessmentSummary.AssessmentStatus = "ASS_INPRG";
                new AssessmentSummaryBLManager().SaveCandidateAssessmentRatngComments(assessmentSummary,
                    int.Parse(Request.QueryString["assessorId"]),
                    Request.QueryString.Get("CISK"),
                    int.Parse(Request.QueryString["ATMPID"]));

                base.ShowMessage(PreviewAssessmentDetails_topSuccessMessageLabel, "Candidate status saved successfully");
                LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PreviewAssessmentDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the submit button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PreviewAssessmentDetails_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                AssessmentSummary assessmentSummary = new AssessmentSummary();

                if(PreviewAssessmentDetails_completedDateTextBox.Text!=string.Empty)
                    assessmentSummary.CompletedOn = Convert.ToDateTime(PreviewAssessmentDetails_completedDateTextBox.Text);

                assessmentSummary.OverallRating = Convert.ToInt32(PreviewAssessmentDetails_userRatingRating.CurrentRating);
                assessmentSummary.Comments = PreviewAssessmentDetails_commentsTextbox.Text;
                assessmentSummary.CreatedBy = base.userID;
                assessmentSummary.AssessmentStatus = "ASS_COMP";
                new AssessmentSummaryBLManager().SaveCandidateAssessmentRatngComments(assessmentSummary,
                    int.Parse(Request.QueryString["assessorId"]),
                    Request.QueryString.Get("CISK"),
                    int.Parse(Request.QueryString["ATMPID"]));
                LoadValues();
                base.ShowMessage(PreviewAssessmentDetails_topSuccessMessageLabel, "Candidate status completed successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PreviewAssessmentDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when question group by drop down list
        /// item is seleted.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PreviewAssessmentDetails_questiongroupByDropDownList_SelectedIndexChanged
            (object sender, EventArgs e)
        {
            try
            {
                if (PreviewAssessmentDetails_questiongroupByDropDownList.SelectedValue != "--Select--")
                {
                    BindAssessmentSummary(Request.QueryString.Get("CISK"), int.Parse(Request.QueryString["ATMPID"]),
                       int.Parse(Request.QueryString["assessorId"]),
                       Convert.ToChar(PreviewAssessmentDetails_questiongroupByDropDownList.SelectedValue));
                }
                else
                {
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PreviewAssessmentDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the item data bound event is 
        /// fired in the assessor details list.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="DataListItemEventArgs"/> that holds the event data.
        /// </param>
        protected void PreviewAssessmentDetails_assessorDetailsDatalist_ItemDataBound
            (object sender, DataListItemEventArgs e)
        {
            try
            {
                HiddenField PreviewAssessmentDetails_assessorDetailsRatingHiddenField = (HiddenField)e.Item.FindControl
                    ("PreviewAssessmentDetails_assessorDetailsRatingHiddenField");

                Image PreviewAssessmentDetails_assessorDetailsRatingImage = (Image)e.Item.FindControl
                    ("PreviewAssessmentDetails_assessorDetailsRatingImage");

                Label PreviewAssessmentDetails_assessorDetailsRatingLabel = (Label)e.Item.FindControl
                    ("PreviewAssessmentDetails_assessorDetailsRatingLabel");

                if (PreviewAssessmentDetails_assessorDetailsRatingHiddenField.Value == "0")
                {
                    PreviewAssessmentDetails_assessorDetailsRatingImage.Visible = false;
                    PreviewAssessmentDetails_assessorDetailsRatingLabel.Text = "N/R";
                }
                else
                {
                    if (string.IsNullOrEmpty(base.GetSummaryRatingImage(
                        Convert.ToDecimal(PreviewAssessmentDetails_assessorDetailsRatingHiddenField.Value))))
                        PreviewAssessmentDetails_assessorDetailsRatingImage.Visible = false;
                    else
                    {
                        PreviewAssessmentDetails_assessorDetailsRatingImage.Visible = true;
                        PreviewAssessmentDetails_assessorDetailsRatingImage.ImageUrl = base.GetSummaryRatingImage(
                            Convert.ToDecimal(PreviewAssessmentDetails_assessorDetailsRatingHiddenField.Value));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PreviewAssessmentDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the overall subject rating grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void PreviewAssessmentDetails_overallsubjectRatingGridview_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                decimal _overAllScore = 0;
                decimal _overAllWeightageScore = 0;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    HiddenField PreviewAssessmentDetails_overallsubjectRatingGridviewHiddenField = (HiddenField)e.Row.FindControl(
                        "PreviewAssessmentDetails_overallsubjectRatingGridviewHiddenField");

                    HiddenField PreviewAssessmentDetails_overallWeightageScoreHiddenField = (HiddenField)e.Row.FindControl(
                        "PreviewAssessmentDetails_overallWeightageScoreHiddenField");


                   /* Image PreviewAssessmentDetails_overallsubjectRatingGridviewImage = (Image)e.Row.FindControl(
                        "PreviewAssessmentDetails_overallsubjectRatingGridviewImage");*/

                    Label PreviewAssessmentDetails_overallScoreLabel = (Label)e.Row.FindControl(
                        "PreviewAssessmentDetails_overallScoreLabel");

                    Label PreviewAssessmentDetails_overallWeightageScoreLabel=(Label)e.Row.FindControl(
                        "PreviewAssessmentDetails_overallWeightageScoreLabel");

                    if (!string.IsNullOrEmpty(Convert.ToString(PreviewAssessmentDetails_overallsubjectRatingGridviewHiddenField.Value)))
                    _overAllScore= 
                        Convert.ToDecimal(PreviewAssessmentDetails_overallsubjectRatingGridviewHiddenField.Value);

                    if (!string.IsNullOrEmpty(Convert.ToString(PreviewAssessmentDetails_overallWeightageScoreHiddenField.Value)))
                    _overAllWeightageScore = 
                        Convert.ToDecimal(PreviewAssessmentDetails_overallWeightageScoreHiddenField.Value);

                    if (_overAllScore<=0)
                        PreviewAssessmentDetails_overallScoreLabel.Visible = false;                 
                    else
                    {
                        PreviewAssessmentDetails_overallScoreLabel.Visible = true;
                        PreviewAssessmentDetails_overallScoreLabel.Text=String.Format("{0:0.00}", _overAllScore); 
                    }

                    if (_overAllWeightageScore<=0)
                        PreviewAssessmentDetails_overallWeightageScoreLabel.Visible = false;
                    else
                    {
                        PreviewAssessmentDetails_overallWeightageScoreLabel.Visible = true;
                        PreviewAssessmentDetails_overallWeightageScoreLabel.Text =String.Format("{0:0.00}", _overAllWeightageScore);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PreviewAssessmentDetails_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindAssessmentSummary(Request.QueryString.Get("CISK"), int.Parse(Request.QueryString["ATMPID"]),
                int.Parse(Request.QueryString["assessorId"]), 'Q');
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that binds the assessment summary.
        /// </summary>
        /// <param name="candidateInterviewKey">
        /// A <see cref="string"/> that holds the candidate interview key.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="char"/> that holds the sort order.
        /// </param>
        private void BindAssessmentSummary(string candidateInterviewKey, 
            int attemptID, int assessorID, char orderBy)
        {
            AssessmentSummary assessmentSummary = new AssessmentSummary();
            assessmentSummary =
                new AssessmentSummaryBLManager().GetAssessorSummary(candidateInterviewKey, attemptID, assessorID, orderBy);

            LoadCandidateDetails(assessmentSummary);

            DataSet overallRatingDataset = new AssessmentSummaryBLManager().
               GetRatingSummary(Request.QueryString.Get("CISK"), int.Parse(Request.QueryString["ATMPID"]));


            if (overallRatingDataset.Tables[0].Rows.Count == 0)
            {
                base.ShowMessage(PreviewAssessmentDetails_topErrorMessageLabel, "");
            }

            decimal _overAllScore = 0;
            decimal _overWeightageScore = 0;

            if (overallRatingDataset.Tables[5].Rows.Count != 0)
                if (!string.IsNullOrEmpty(Convert.ToString(overallRatingDataset.Tables[5].Rows[0]["OVERALL_SCORE"])))
                    _overAllScore = Convert.ToDecimal(overallRatingDataset.Tables[5].Rows[0]["OVERALL_SCORE"].ToString());


            if (overallRatingDataset.Tables[6].Rows.Count != 0)
                if (!string.IsNullOrEmpty(Convert.ToString(overallRatingDataset.Tables[6].Rows[0]["OVERALL_WEIGHTAGE_SCORE"])))
                    _overWeightageScore =
                        Convert.ToDecimal(overallRatingDataset.Tables[6].Rows[0]["OVERALL_WEIGHTAGE_SCORE"].ToString());

            if (overallRatingDataset.Tables[3].Rows.Count != 0)
            {
                PreviewAssessmentDetails_overallScore.Visible = true;
                PreviewAssessmentDetails_OverallWeightageScore.Visible = true;

                if (_overAllScore <= 0)
                    PreviewAssessmentDetails_overallScore.Text = "N/R";
                else
                    PreviewAssessmentDetails_overallScore.Text = String.Format("{0:0.00}", _overAllScore);


                if (_overWeightageScore <= 0)
                    PreviewAssessmentDetails_OverallWeightageScore.Text = "N/R";
                else
                    PreviewAssessmentDetails_OverallWeightageScore.Text = String.Format("{0:0.00}", _overWeightageScore);
            }

            if (overallRatingDataset.Tables[2].Rows.Count == 0)
            {
                base.ShowMessage(PreviewAssessmentDetails_topErrorMessageLabel, "");
            }
            else
            {
                PreviewAssessmentDetails_overallsubjectRatingGridview.DataSource = overallRatingDataset.Tables[2];
                PreviewAssessmentDetails_overallsubjectRatingGridview.DataBind();
            }
            if (assessmentSummary.QuestionDetails == null)
            {
                base.ShowMessage(PreviewAssessmentDetails_topErrorMessageLabel, "");
            }
            else
            {
                PreviewAssessmentDetails_questionAreaDataList.DataSource = assessmentSummary.QuestionDetails;
                PreviewAssessmentDetails_questionAreaDataList.DataBind();
            }
            if (assessmentSummary.AssessorDetails == null)
            {
                base.ShowMessage(PreviewAssessmentDetails_topErrorMessageLabel, "");
            }
            else
            {
                PreviewAssessmentDetails_assessorDetailsDatalist.DataSource = assessmentSummary.AssessorDetails;
                PreviewAssessmentDetails_assessorDetailsDatalist.DataBind();
            }
        }

        /// <summary>
        /// Method that loads the candidate details.
        /// </summary>
        /// <param name="assessmentSummary">
        /// A <see cref="AssessmentSummary"/> that holds the assessment summary.
        /// </param>
        private void LoadCandidateDetails(AssessmentSummary assessmentSummary)
        {
            PreviewAssessmentDetails_candidateNameLabelValue.Text = assessmentSummary.CanidateName;
            PreviewAssessmentDetails_clientNameLinkButton.Text = assessmentSummary.ClientName;

            // Add handler for client name link button.
            if (assessmentSummary.ClientID != 0)
            {
                PreviewAssessmentDetails_clientNameLinkButton.Attributes.Add("onclick",
                    "javascript:return ShowViewClient('" + assessmentSummary.ClientID + "');");
            }

            PreviewAssessmentDetails_positionProfileLinkButton.Text = assessmentSummary.PositionProfileName;

            // Add handler for position profile link button.
            if (assessmentSummary.PositionProfileID != 0)
            {
                PreviewAssessmentDetails_positionProfileLinkButton.Attributes.Add("onclick",
                    "javascript:return ShowViewPositionProfile('" + assessmentSummary.PositionProfileID + "');");
            }

            if ((assessmentSummary.CompletedOn == Convert.ToDateTime("01/01/1901")) ||
                (assessmentSummary.CompletedOn == Convert.ToDateTime("1/1/0001 12:00:00 AM")) ||
                (Convert.ToString(assessmentSummary.CompletedOn) == string.Empty))
            {
                PreviewAssessmentDetails_completedDateTextBox.Text = string.Empty;
            }
            else
                PreviewAssessmentDetails_completedDateTextBox.Text = Convert.ToDateTime(assessmentSummary.CompletedOn).ToShortDateString();
            if (!Utility.IsNullOrEmpty(assessmentSummary.OverallRating))
                PreviewAssessmentDetails_userRatingRating.CurrentRating = Convert.ToInt32(assessmentSummary.OverallRating);
            PreviewAssessmentDetails_commentsTextbox.Text = assessmentSummary.Comments;

            // Assign candidate image handler.
            PreviewAssessmentDetails_candidatePhotoImage.ImageUrl = "~/Common/CandidateImageHandler.ashx?source=VIEW_CAND&candidateid=" +
                assessmentSummary.CandidateInfoID;
        }

        #endregion Private Methods
    }
}