﻿#region Directives

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

using OfficeOpenXml;
using System.Configuration;
using System.Globalization;
#endregion Directives

namespace Forte.HCM.UI.Assessments
{
    public partial class MyAssessments : PageBase
    {

        #region Private Constants
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";
        #endregion Private Constants

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();
                MyAssessments_positionProfileImageButton.Attributes.Add("onclick",
                   "return LoadPositionProfileName('" + MyAssessments_positionProfileTextBox.ClientID + "','" +
                    MyAssessments_positionProfileHiddenField.ClientID + "');");

                CheckAndSetExpandorRestore();

                MyAssessments_searchResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    MyAssessments_schedulerDiv.ClientID + "','" +
                    MyAssessments_searchCriteriasDiv.ClientID + "','" +
                    MyAssessments_searchResultsUpSpan.ClientID + "','" +
                    MyAssessments_searchResultsDownSpan.ClientID + "','" +
                    MyAssessments_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");

                Master.SetPageCaption("My Assessments");
                Page.Form.DefaultButton = MyAssessments_searchButton.UniqueID;

                if (!IsPostBack)
                {
                    ViewState["SORT_ORDER"] = SortType.Descending;
                    ViewState["SORT_FIELD"] = MyAssessments_interviewListOptionRadioButtonList.SelectedIndex == 0 ? "COMPLETED_DATE" : "ONLINE_INTERVIEW_DATE";

                    MyAssessments_onlineInterviewSchedulerDiv.Style.Add("display", "none");
                    MyAssessments_schedulerDiv.Style.Add("display", "block");

                    MyAssessments_interviewListOptionRadioButtonList.SelectedIndex = 0;

                    MyAssessments_positionProfileImageButton.Attributes.Add("onclick",
                        "javascript:return LoadPositionProfileName('" + MyAssessments_positionProfileTextBox.ClientID + "','" +
                        MyAssessments_positionProfileHiddenField.ClientID + "');");

                    LoadAssessmentDropDown();

                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                       (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU
                        || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_LANDING
                        || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_ASSESSMENT))
                    {
                        base.ClearSearchCriteriaSession();

                        // Load the candidate session details
                        LoadAssessmentDetails(1);
                    }
                    else if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"]))
                        && (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"])))
                    {
                        MyAssessments_assessmentStatusDropDownList.SelectedIndex = 1;
                        MyAssessments_positionProfileHiddenField.Value = Request.QueryString["positionprofileid"];

                        PositionProfileDetail positionProfile = new PositionProfileBLManager().
                            GetPositionProfile(Convert.ToInt32(MyAssessments_positionProfileHiddenField.Value));

                        MyAssessments_positionProfileTextBox.Text = positionProfile.PositionName;
                        // Load the candidate session details
                        LoadAssessmentDetails(1);

                    }
                    else if (Session[Constants.SearchCriteriaSessionKey.SEARCH_ASSESOR_ASSESSMENT] != null)
                    {
                        // Check if page is redirected from any child page. If the page
                        // if redirected from any child page, fill the search criteria
                        // and apply the search.
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_ASSESOR_ASSESSMENT]
                            as AssessmentSearchCriteria);

                        // Reload the maximize hidden value.
                        CheckAndSetExpandorRestore();
                    }
                }

                MyAssessments_pagingNavigator.PageNumberClick += new CommonControls.
                    PageNavigator.PageNumberClickEventHandler(MyAssessments_pagingNavigator_PageNumberClick);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                    MyAssessments_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void MyAssessments_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                base.ClearSearchCriteriaSession();
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                    MyAssessments_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void MyAssessments_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearLabelMessage();
                MyAssessments_pagingNavigator.Reset();
                MyAssessments_pagingNavigator.TotalRecords = 1;
                ViewState["PAGENUMBER"] = "1";
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = MyAssessments_interviewListOptionRadioButtonList.SelectedIndex == 0 ? "COMPLETED_DATE" : "ONLINE_INTERVIEW_DATE";
                if (MyAssessments_interviewListOptionRadioButtonList.SelectedIndex == 0)
                    LoadAssessmentDetails(1);
                else
                {
                    SetSearchInterviewParam(Convert.ToInt32(ViewState["PAGENUMBER"]), ViewState["SORT_FIELD"].ToString(), (SortType)ViewState["SORT_ORDER"]);
                    LoadOnlineInterviews(Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                    MyAssessments_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void MyAssessments_assessmentDetailsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                int sortColumnIndex = GetSortColumnIndex
                    (MyAssessments_assessmentDetailsGridView,
                    (string)ViewState["SORT_FIELD"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
                /*  HiddenField MyAssessments_assessmentDetailsTestStatusHiddenField=(HiddenField)e.Row.FindControl
                      ("MyAssessments_assessmentDetailsTestStatusHiddenField");
                  ImageButton MyAssessments_assessmentDetailsSelectLinkButton=(ImageButton)e.Row.FindControl
                      ("MyAssessments_assessmentDetailsSelectLinkButton");                
                  if (MyAssessments_assessmentDetailsTestStatusHiddenField.Value == "Completed")
                  {
                      MyAssessments_assessmentDetailsSelectLinkButton.Visible = true;
                      MyAssessments_assessmentDetailsPreviewAssessmentSummaryImageButton.Visible = true;
                  }*/

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                    MyAssessments_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void MyAssessments_assessmentDetailsGridView_RowCommand
           (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                //Check whether the command name is view 
                if (e.CommandName == "Tracking")
                {
                    string value = ((HiddenField)((ImageButton)e.CommandSource).Parent.
                        FindControl("MyAssessments_assessmentDetailsGridView_CandidateInterviewSessionKey")).Value;
                    string attemptID = ((HiddenField)((ImageButton)e.CommandSource).Parent.
                        FindControl("MyAssessments_assessmentDetailsGridView_attemptIDHiddenField")).Value;
                    Response.Redirect("~/InterviewReportCenter/OfflineInterviewTrackingDetails.aspx?candidatesession="
                       + value.Trim() + "&attemptid=" + attemptID.Trim() + "&parentpage=MY_ASMT", false);
                }
                else if (e.CommandName == "AssessorRating")
                {
                    string value = ((HiddenField)((ImageButton)e.CommandSource).Parent.
                       FindControl("MyAssessments_assessmentDetailsGridView_CandidateInterviewSessionKey")).Value;
                    string assessorId = ((HiddenField)((ImageButton)e.CommandSource).Parent.
                     FindControl("MyAssessments_assessmentDetailsGridView_AssessorId")).Value;
                    string interviewSessionvalue = ((HiddenField)((ImageButton)e.CommandSource).Parent.
                     FindControl("MyAssessments_assessmentDetailsGridView_InterviewSessionKey")).Value;
                    string attemptID = ((HiddenField)((ImageButton)e.CommandSource).Parent.
                     FindControl("MyAssessments_assessmentDetailsGridView_attemptIDHiddenField")).Value;
                    string interviewTestKey = ((HiddenField)((ImageButton)e.CommandSource).Parent.
                     FindControl("MyAssessments_assessmentDetailsGridView_testKeyHiddenField")).Value;
                    Response.Redirect("~/Assessments/AssessorRating.aspx?CISK="
                        + value.Trim() + "&ISK=" + interviewTestKey.Trim() + "&sessionkey=" + interviewSessionvalue.Trim() +
                        "&assessorId=" + assessorId + "&ATMPID=" + attemptID.Trim() + "&parentpage=MY_ASMT", false);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                    MyAssessments_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called on the sorting of the data grid view
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewSortEventArgs"/> that holds the event data.
        /// </param>
        protected void MyAssessments_assessmentDetailsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                //check whether the sort field in view state is same as 
                //the sort expression 
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    //Swap the sort order in the view state
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                //Store the sort field in the view state
                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                MyAssessments_pagingNavigator.TotalRecords = 1;
                MyAssessments_pagingNavigator.Reset();

                //Set question details based on the values 
                LoadAssessmentDetails(1);
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                    MyAssessments_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void MyAssessments_assessmentDetailsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField MyAssessments_assessmentDetailsTestStatusHiddenField = (HiddenField)e.Row.FindControl
                            ("MyAssessments_assessmentDetailsTestStatusHiddenField");
                    ImageButton MyAssessments_assessmentDetailsAssessorRatingImageButton = (ImageButton)e.Row.FindControl
                        ("MyAssessments_assessmentDetailsAssessorRatingImageButton");
                    ImageButton MyAssessments_assessmentDetailsViewTrackingDetailsImageButton = (ImageButton)e.Row.FindControl
                        ("MyAssessments_assessmentDetailsViewTrackingDetailsImageButton");

                    HiddenField MyAssessments_assessmentDetailsGridView_CandidateInterviewSessionKey = (HiddenField)e.Row.FindControl
                        ("MyAssessments_assessmentDetailsGridView_CandidateInterviewSessionKey");

                    HiddenField MyAssessments_assessmentDetailsGridView_AssessorId = (HiddenField)e.Row.FindControl
                        ("MyAssessments_assessmentDetailsGridView_AssessorId");

                    HiddenField MyAssessments_assessmentDetailsGridView_InterviewSessionKey = (HiddenField)e.Row.FindControl
                        ("MyAssessments_assessmentDetailsGridView_InterviewSessionKey");

                    HiddenField MyAssessments_assessmentDetailsGridView_attemptIDHiddenField = (HiddenField)e.Row.FindControl
                        ("MyAssessments_assessmentDetailsGridView_attemptIDHiddenField");

                    HiddenField MyAssessments_assessmentDetailsGridView_testKeyHiddenField = (HiddenField)e.Row.FindControl
                        ("MyAssessments_assessmentDetailsGridView_testKeyHiddenField");


                    string candidateInterviewSessionKey = MyAssessments_assessmentDetailsGridView_CandidateInterviewSessionKey.Value;
                    string assessorId = MyAssessments_assessmentDetailsGridView_AssessorId.Value;
                    string interviewSessionvalue = MyAssessments_assessmentDetailsGridView_InterviewSessionKey.Value;
                    string attemptID = MyAssessments_assessmentDetailsGridView_attemptIDHiddenField.Value;
                    string interviewTestKey = MyAssessments_assessmentDetailsGridView_testKeyHiddenField.Value;

                    HyperLink MyAssessments_viewInterviewSummaryHyperLink =
                        (HyperLink)e.Row.FindControl("MyAssessments_viewInterviewSummaryHyperLink");

                    MyAssessments_viewInterviewSummaryHyperLink.NavigateUrl = "~/InterviewReportCenter/InterviewCandidateTestDetails.aspx?m=4&s=0&candidatesession="
                        + candidateInterviewSessionKey.Trim() + "&testkey=" + interviewTestKey.Trim() + "&sessionkey=" + interviewSessionvalue.Trim() +
                        "&assessorId=" + assessorId + "&attemptid=" + attemptID.Trim() + "&parentpage=MY_ASMT";

                    HyperLink MyAssessments_candidateRatingSummaryHyperLink =
                        (HyperLink)e.Row.FindControl("MyAssessments_candidateRatingSummaryHyperLink");

                    MyAssessments_candidateRatingSummaryHyperLink.NavigateUrl = "~/Assessments/CandidateAssessorSummary.aspx?m=4&s=0&candidatesessionid="
                       + candidateInterviewSessionKey + "&attemptid=" + attemptID.Trim() + "&assessorId=" + assessorId + "&ISK=" + interviewTestKey.Trim() + "&parentpage=MY_ASMT";


                    if (MyAssessments_assessmentDetailsTestStatusHiddenField.Value == "Completed")
                    {
                        MyAssessments_viewInterviewSummaryHyperLink.Visible = true;
                        MyAssessments_assessmentDetailsAssessorRatingImageButton.Visible = true;
                        MyAssessments_candidateRatingSummaryHyperLink.Visible = true;
                        MyAssessments_assessmentDetailsViewTrackingDetailsImageButton.Visible = false;
                    }
                    else
                    {
                        MyAssessments_viewInterviewSummaryHyperLink.Visible = true;
                        MyAssessments_assessmentDetailsAssessorRatingImageButton.Visible = false;
                        MyAssessments_candidateRatingSummaryHyperLink.Visible = false;
                        MyAssessments_assessmentDetailsViewTrackingDetailsImageButton.Visible = false;
                    }
                }
            }
            catch (Exception exception)
            {
                //Log the exception and show message to user
                Logger.ExceptionLog(exception);
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                    MyAssessments_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void Myassessments_topDownloadImageButton_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {

            try
            {
                ClearLabelMessage();
                MyAssessments_pagingNavigator.TotalRecords = 1;
                MyAssessments_pagingNavigator.Reset();
                ViewState["PAGENUMBER"] = "1";
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = MyAssessments_interviewListOptionRadioButtonList.SelectedIndex == 0 ? "COMPLETED_DATE" : "ONLINE_INTERVIEW_DATE";
                ExportAssessmentDetails(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                    MyAssessments_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void Myassessments_bottomDownloadImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ClearLabelMessage();
                MyAssessments_pagingNavigator.TotalRecords = 1;
                MyAssessments_pagingNavigator.Reset();
                ViewState["PAGENUMBER"] = "1";
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = MyAssessments_interviewListOptionRadioButtonList.SelectedIndex == 0 ? "COMPLETED_DATE" : "ONLINE_INTERVIEW_DATE";
                ExportAssessmentDetails(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                    MyAssessments_bottomErrorMessageLabel, exp.Message);
            }
        }

        void MyAssessments_pagingNavigator_PageNumberClick(object sender, EventSupport.PageNumberEventArgs e)
        {
            try
            {
                ViewState["PAGENUMBER"] = e.PageNumber;

                if (MyAssessments_interviewListOptionRadioButtonList.SelectedIndex == 0)
                    LoadAssessmentDetails(e.PageNumber);
                else
                {
                    SetSearchInterviewParam(Convert.ToInt32(ViewState["PAGENUMBER"]), ViewState["SORT_FIELD"].ToString(), (SortType)ViewState["SORT_ORDER"]);
                    LoadOnlineInterviews(Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                    MyAssessments_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Online Interview Event Handlers
        protected void MyAssessments_interviewListOptionRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (MyAssessments_interviewListOptionRadioButtonList.SelectedIndex == 0)
                {
                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_ASSESOR_ASSESSMENT] != null)
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_ASSESOR_ASSESSMENT]
                                as AssessmentSearchCriteria);

                    MyAssessments_onlineInterviewSchedulerDiv.Style.Add("display", "none");
                    MyAssessments_schedulerDiv.Style.Add("display", "block");

                    return;
                }

                MyAssessments_onlineInterviewSchedulerDiv.Style.Add("display", "block");
                MyAssessments_schedulerDiv.Style.Add("display", "none");

                SetSearchInterviewParam(1, ViewState["SORT_FIELD"].ToString(), SortType.Ascending);

                LoadOnlineInterviews(Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                MyAssessments_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void MyAssessments_onlineSchedulerList_GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    OnlineCandidateSessionDetail dataRowItem = (OnlineCandidateSessionDetail)e.Row.DataItem;

                    ImageButton MyAssessments_onlineSchedulerList_GridViewImageButton = (ImageButton)
                        e.Row.FindControl("MyAssessments_onlineSchedulerList_GridViewImageButton");

                    HyperLink MyAssessments_onlineSchedulerList_GridView_candidateRatingSummaryHyperLink =
                   (HyperLink)e.Row.FindControl("MyAssessments_onlineSchedulerList_GridView_candidateRatingSummaryHyperLink");

                    if (MyAssessments_onlineSchedulerList_GridView_candidateRatingSummaryHyperLink != null)
                        MyAssessments_onlineSchedulerList_GridView_candidateRatingSummaryHyperLink.NavigateUrl
                            = "~/OnlineInterview/OnlineInterviewAssessorSummary.aspx?m=3&s=0&parentpage=MENU&interviewkey=" +
                            dataRowItem.CandidateSessionID + "&interviewid=" +
                            dataRowItem.CandidateInterviewID.ToString() + "&chatroomid=" + dataRowItem.ChatRoomName;

                    MyAssessments_onlineSchedulerList_GridViewImageButton.Visible = false;

                    if (dataRowItem.SessionStatus != "Completed")
                    {
                        if(dataRowItem.InterviewDate == DateTime.Today) 
                            MyAssessments_onlineSchedulerList_GridViewImageButton.Visible = true; 
                    } 
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                MyAssessments_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void MyAssessments_onlineSchedulerList_GridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }
                //check whether the sort field in view state is same as 
                //the sort expression 
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    //Swap the sort order in the view state
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                //Store the sort field in the view state
                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                MyAssessments_pagingNavigator.TotalRecords = 1;
                MyAssessments_pagingNavigator.Reset();
                if (MyAssessments_interviewListOptionRadioButtonList.SelectedIndex == 1)
                {
                    if (Convert.ToInt32(ViewState["PAGENUMBER"]) == 0) ViewState["PAGENUMBER"] = 1;

                    SetSearchInterviewParam(Convert.ToInt32(ViewState["PAGENUMBER"]), ViewState["SORT_FIELD"].ToString(), (SortType)ViewState["SORT_ORDER"]);
                    LoadOnlineInterviews(Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
              MyAssessments_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void MyAssessments_onlineSchedulerList_GridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Trim() == "IConduction")
                {
                    string chatRoomValue = ((HiddenField)((ImageButton)e.CommandSource).Parent.
                     FindControl("MyAssessments_onlineSchedulerList_GridView_chatId_HiddenField")).Value;

                    //Checking candidate have online interview for current date & time
                    OnlineCandidateSessionDetail onlineInterviewDetail =
                        new CandidateBLManager().GetOnlineInterviewDetail(0, base.userID, chatRoomValue);

                    if (onlineInterviewDetail == null)
                    {
                        base.ShowMessage(MyAssessments_topErrorMessageLabel, MyAssessments_bottomErrorMessageLabel, "There is no such a interview");
                        return;
                    }

                    if (onlineInterviewDetail.InterviewDate != null)
                    {
                        if (onlineInterviewDetail.InterviewDate < DateTime.Today)
                        {
                            base.ShowMessage(MyAssessments_topErrorMessageLabel, MyAssessments_bottomErrorMessageLabel, "Interview has expired");
                            return;
                        }
                        else if (onlineInterviewDetail.InterviewDate > DateTime.Today)
                        {
                            base.ShowMessage(MyAssessments_topErrorMessageLabel, MyAssessments_bottomErrorMessageLabel,
                                "Interview scheduled on " + base.GetDateFormat(onlineInterviewDetail.InterviewDate));
                            return;
                        }
                    }

                    if (onlineInterviewDetail != null)
                    {
                        if (new Utility().GetOnlineInterviewAvailable(onlineInterviewDetail.InterviewDate,
                            onlineInterviewDetail.TimeSlotFrom, onlineInterviewDetail.TimeSlotTo))
                        {
                            Response.Redirect(ConfigurationManager.AppSettings["ONLINE_INTERVIEW_URL"].ToString() + "?candidateid=" + onlineInterviewDetail.CandidateID.ToString()
                                + "&userrole=A" + "&userid=" + base.userID.ToString().Trim() + "&interviewkey=" + onlineInterviewDetail.CandidateSessionID
                                + "&chatroom=" + onlineInterviewDetail.ChatRoomName, false);
                            return;
                        }
                        else
                        {
                            base.ShowMessage(MyAssessments_topErrorMessageLabel, MyAssessments_bottomErrorMessageLabel, "Interview has expired");
                            return;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
              MyAssessments_bottomErrorMessageLabel, exp.Message);
            }
        }
        #endregion

        #region Protected Overridden Methods
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion Protected Overridden Methods

        #region Private Methods

        private void FillSearchCriteria(AssessmentSearchCriteria assessmentSearchCriteria)
        {
            if (assessmentSearchCriteria.InterviewName != null)
                MyAssessments_interviewNameTextBox.Text = assessmentSearchCriteria.InterviewName;

            if (assessmentSearchCriteria.InterviewDescription != null)
                MyAssessments_interviewDescriptionTextBox.Text = assessmentSearchCriteria.InterviewDescription;

            if (assessmentSearchCriteria.PositionProfileID != null)
            {
                MyAssessments_positionProfileHiddenField.Value = assessmentSearchCriteria.PositionProfileID;
                MyAssessments_positionProfileTextBox.Text = assessmentSearchCriteria.PositionProfileName;
            }

            if (assessmentSearchCriteria.AssessmentStatus != null)
            {
                if (MyAssessments_assessmentStatusDropDownList.Items.FindByValue(assessmentSearchCriteria.AssessmentStatus) != null)
                {
                    MyAssessments_assessmentStatusDropDownList.SelectedValue = assessmentSearchCriteria.AssessmentStatus;
                }

                //foreach (ListItem item in MyAssessments_assessmentStatusDropDownList.Items)
                //{
                //    int i = 0;
                //    if (item.Text.Trim().ToLower() == assessmentSearchCriteria.AssessmentStatus.Trim().ToLower())
                //    {
                //        MyAssessments_assessmentStatusDropDownList.SelectedIndex = i;
                //    }
                //    i++;
                //}
            }

            MyAssessments_isMaximizedHiddenField.Value =
                    assessmentSearchCriteria.IsMaximized == true ? "Y" : "N";


            ViewState["SORT_ORDER"] = assessmentSearchCriteria.SortDirection;
            ViewState["SORT_FIELD"] = assessmentSearchCriteria.SortExpression;

            // Apply search
            LoadAssessmentDetails(assessmentSearchCriteria.CurrentPage);
            MyAssessments_pagingNavigator.MoveToPage(assessmentSearchCriteria.CurrentPage);
        }
        private void LoadAssessmentDropDown()
        {
            MyAssessments_assessmentStatusDropDownList.DataSource = new AssessorBLManager().GetAssessmentTypes();
            MyAssessments_assessmentStatusDropDownList.DataBind();

            MyAssessments_assessmentStatusDropDownList.Items.Insert(0, new ListItem("--Select--", ""));
        }
        private void LoadAssessmentDetails(int pageNumber)
        {
            AssessmentSearchCriteria assessmentSearchCriteria = new AssessmentSearchCriteria();

            assessmentSearchCriteria.InterviewName = MyAssessments_interviewNameTextBox.Text.Trim();
            assessmentSearchCriteria.InterviewDescription = MyAssessments_interviewDescriptionTextBox.Text.Trim();
            assessmentSearchCriteria.AssessmentStatus = MyAssessments_assessmentStatusDropDownList.SelectedItem.Value;
            assessmentSearchCriteria.PositionProfileID = MyAssessments_positionProfileHiddenField.Value;
            assessmentSearchCriteria.PositionProfileName = MyAssessments_positionProfileTextBox.Text;
            assessmentSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
            assessmentSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();
            assessmentSearchCriteria.AssessorID = base.userID;
            assessmentSearchCriteria.CurrentPage = pageNumber;
            assessmentSearchCriteria.IsMaximized =
                MyAssessments_isMaximizedHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

            // Keep the search criteria in Session if the page is launched from 
            // somewhere else.
            Session[Constants.SearchCriteriaSessionKey.SEARCH_ASSESOR_ASSESSMENT] = assessmentSearchCriteria;

            int totalRecords = 0;

            List<AssessorAssessmentDetail> assessmentDetails = new AssessorBLManager().
               GetAssessmentDetailsForAssessor(assessmentSearchCriteria, pageNumber, base.GridPageSize, out totalRecords);

            if (assessmentDetails == null || assessmentDetails.Count == 0)
            {
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                    MyAssessments_bottomErrorMessageLabel,
                    Resources.HCMResource.TestScheduler_NoDataFoundToDisplay);

                // Invisible paging control and gridview DIV.
                MyAssessments_schedulerDiv.Visible = false;
                MyAssessments_pagingNavigator.Visible = false;
            }
            else
            {
                MyAssessments_schedulerDiv.Visible = true;
                MyAssessments_pagingNavigator.Visible = true;
                MyAssessments_assessmentDetailsGridView.DataSource = assessmentDetails;
                MyAssessments_assessmentDetailsGridView.DataBind();
                MyAssessments_pagingNavigator.TotalRecords = totalRecords;
                MyAssessments_pagingNavigator.PageSize = base.GridPageSize;
            }

        }
        /// <summary>
        /// Method that will clear all the success/error label message.
        /// </summary>
        private void ClearLabelMessage()
        {
            MyAssessments_topSuccessMessageLabel.Text = string.Empty;
            MyAssessments_bottomSuccessMessageLabel.Text = string.Empty;
            MyAssessments_topErrorMessageLabel.Text = string.Empty;
            MyAssessments_bottomErrorMessageLabel.Text = string.Empty;
        }
        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            // Maintain the expand/restore gridview.
            if (!Utility.IsNullOrEmpty(MyAssessments_isMaximizedHiddenField.Value) &&
                MyAssessments_isMaximizedHiddenField.Value == "Y")
            {
                MyAssessments_searchCriteriasDiv.Style["display"] = "none";
                MyAssessments_searchResultsUpSpan.Style["display"] = "block";
                MyAssessments_searchResultsDownSpan.Style["display"] = "none";
                MyAssessments_schedulerDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                MyAssessments_searchCriteriasDiv.Style["display"] = "block";
                MyAssessments_searchResultsUpSpan.Style["display"] = "none";
                MyAssessments_searchResultsDownSpan.Style["display"] = "block";
                MyAssessments_schedulerDiv.Style["height"] = RESTORED_HEIGHT;
            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_ASSESOR_ASSESSMENT]))
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(MyAssessments_isMaximizedHiddenField.Value))
                    ((AssessmentSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_ASSESOR_ASSESSMENT]).IsMaximized =
                        MyAssessments_isMaximizedHiddenField.Value == "Y" ? true : false;
        }

        /// <summary>
        /// Method that gets the my assessments details collections in list
        /// </summary>
        /// <param name="pageNumber"></param>
        private void ExportAssessmentDetails(int pageNumber)
        {
            AssessmentSearchCriteria assessmentSearchCriteria = new AssessmentSearchCriteria();

            assessmentSearchCriteria.InterviewName = MyAssessments_interviewNameTextBox.Text.Trim();
            assessmentSearchCriteria.InterviewDescription = MyAssessments_interviewDescriptionTextBox.Text.Trim();
            assessmentSearchCriteria.AssessmentStatus = MyAssessments_assessmentStatusDropDownList.SelectedItem.Value;
            assessmentSearchCriteria.PositionProfileID = MyAssessments_positionProfileHiddenField.Value;
            assessmentSearchCriteria.PositionProfileName = MyAssessments_positionProfileTextBox.Text;
            assessmentSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
            assessmentSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();
            assessmentSearchCriteria.AssessorID = base.userID;
            assessmentSearchCriteria.CurrentPage = pageNumber;
            assessmentSearchCriteria.IsMaximized =
                MyAssessments_isMaximizedHiddenField.Value.Trim().ToUpper() == "Y" ? true : false;

            // Keep the search criteria in Session if the page is launched from 
            // somewhere else.
            Session[Constants.SearchCriteriaSessionKey.SEARCH_ASSESOR_ASSESSMENT] = assessmentSearchCriteria;

            int totalRecords = 0;

            List<AssessorAssessmentDetail> assessmentDetails = new AssessorBLManager().
               GetAssessmentDetailsForAssessor(assessmentSearchCriteria, pageNumber, base.GridPageSize, out totalRecords);

            if (assessmentDetails == null || assessmentDetails.Count == 0)
            {
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                    MyAssessments_bottomErrorMessageLabel,
                    Resources.HCMResource.TestScheduler_NoDataFoundToDisplay);

                // Invisible paging control and gridview DIV.
                MyAssessments_schedulerDiv.Visible = false;
                MyAssessments_pagingNavigator.Visible = false;
            }
            else
            {
                //Sending the assessment details collection list to xportToExcel_AssessmentDetails methods
                ExportToExcel_AssessmentDetails(assessmentDetails);
            }
        }
        /// <summary>
        /// Method that exports the my assessments details to Excel
        /// </summary>
        /// <param name="assessmentDetails"></param>
        private void ExportToExcel_AssessmentDetails(List<AssessorAssessmentDetail> assessmentDetails)
        {
            GridView AssessmentDetailTemp = new GridView();
            AssessmentDetailTemp.DataSource = assessmentDetails;
            AssessmentDetailTemp.DataBind();
            for (int i = 0; i < AssessmentDetailTemp.Rows.Count; i++)
            {
                string TempVar = AssessmentDetailTemp.Rows[i].Cells[2].Text.ToString();
                DateTime TempDate = Convert.ToDateTime(TempVar);
                if (DateTime.MinValue != TempDate)
                {
                    AssessmentDetailTemp.Rows[i].Cells[2].Text = TempDate.ToString("d");
                }
                else
                {
                    AssessmentDetailTemp.Rows[i].Cells[2].Text = "";
                }
            }
            DataTable Table = new DataTable();
            // Fill The ColumnHeaders
            for (int i = 0; i < AssessmentDetailTemp.HeaderRow.Cells.Count; i++)
            {

                Table.Columns.Add(AssessmentDetailTemp.HeaderRow.Cells[i].Text.ToString(), typeof(string));
            }

            Table.AcceptChanges();

            // fill rows             
            foreach (GridViewRow row in AssessmentDetailTemp.Rows)
            {
                DataRow dr;
                dr = Table.NewRow();
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    dr[i] = row.Cells[i].Text.Replace("&nbsp;", "");
                }
                Table.Rows.Add(dr);
            }
            Table.AcceptChanges();

            // Check if table is not null and contain rows
            if (Table == null || Table.Rows.Count == 0)
            {
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                  MyAssessments_bottomErrorMessageLabel, "No assessment details found");
            }

            //Remove Unnecessary Columns from Table
            DataColumn[] ExcludeColumnList = new[]
             {
                        Table.Columns[1], Table.Columns[7],Table.Columns[8], Table.Columns[9], 
                        Table.Columns[10],Table.Columns[11],Table.Columns[12], Table.Columns[13],
                        Table.Columns[14],Table.Columns[15],Table.Columns[16],Table.Columns[17],
                        Table.Columns[18],Table.Columns[19],Table.Columns[20],Table.Columns[21],
                        Table.Columns[22]
             };
            foreach (DataColumn col in ExcludeColumnList)
            {
                Table.Columns.Remove(col);
            }
            Table.AcceptChanges();

            // Change the column names.
            Table.Columns["CandidateName"].ColumnName = "Candidate Name";
            Table.Columns["CompletedDate"].ColumnName = "Completed Date";
            Table.Columns["InterviewName"].ColumnName = "Interview Name";
            Table.Columns["PositionProfileName"].ColumnName = "Position Profile Name";
            Table.Columns["AssessmentStatus"].ColumnName = "Assessment Status";
            Table.Columns["Teststatus"].ColumnName = "Interview Status";
            Table.AcceptChanges();

            //Creating excel package and adding worksheets
            ExcelPackage pack = new ExcelPackage();
            ExcelWorksheet ws = pack.Workbook.Worksheets.Add("My Assessment");

            // Load the datatable into the sheet, starting from the 
            // cell A1 and print the column names on row 1.
            ws.Cells[Constants.ExcelExport.STARTING_CELL].LoadFromDataTable(Table, true);
            string fileName = "My_Assessment.xlsx";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
            Response.BinaryWrite(pack.GetAsByteArray());
            Response.End();
        }

        #endregion Private Methods

        #region Online Interview Functions

        private void SetSearchInterviewParam(int pageNumber, string sortExpression, SortType sortType)
        {
            Session["INTERVIEW_SEARCH_CRITERIA"] = null;

            InterviewSearchCriteria interviewSearch = new InterviewSearchCriteria(); 
             
            interviewSearch.InterviewSessionKey = null;
            
            interviewSearch.InterviewSessionStatus = SelectedSesstionStatus();
            interviewSearch.Keyword = SearchText();
            interviewSearch.SearchTenantID = base.tenantID;
            interviewSearch.CurrentPage = pageNumber;
            interviewSearch.PageSize = base.GridPageSize;
            interviewSearch.SortExpression = sortExpression;
            interviewSearch.SortDirection = sortType;

            if (!Utility.IsNullOrEmpty(MyAssessments_positionProfileHiddenField.Value))
                interviewSearch.PositionProfileId = Convert.ToInt32(MyAssessments_positionProfileHiddenField.Value);

            Session["INTERVIEW_SEARCH_CRITERIA"] = interviewSearch;
        }

        /// <summary>
        /// Returns the search text
        /// </summary>
        /// <returns></returns>
        private string SearchText()
        {
            string searchText = string.Empty;

            if (!Utility.IsNullOrEmpty(MyAssessments_interviewNameTextBox.Text.Trim()))
                searchText += MyAssessments_interviewNameTextBox.Text.Trim() + ",";

            if (!Utility.IsNullOrEmpty(MyAssessments_interviewDescriptionTextBox.Text.Trim()))
                searchText += MyAssessments_interviewDescriptionTextBox.Text.Trim() + ",";

            if (!Utility.IsNullOrEmpty(searchText))
                return searchText.Remove(searchText.Length - 1);

            return null;
        }

        /// <summary>
        /// Returns the status code of selecting appropritate text
        /// </summary>
        /// <returns></returns>
        private string SelectedSesstionStatus()
        {
            string sessionStatus = null;

            if (MyAssessments_assessmentStatusDropDownList.SelectedIndex == -1) return null;

            if (MyAssessments_assessmentStatusDropDownList.SelectedItem.Text.Trim() == "--Select--") return null;

            switch (MyAssessments_assessmentStatusDropDownList.SelectedValue.Trim())
            {
                case "ASS_CSCH":
                    sessionStatus = "SESS_SCHD";
                    break;
                case "ASS_INIT":
                    sessionStatus = "SESS_INPR";
                    break;
                case "ASS_INPRG":
                    sessionStatus = "SESS_INPR";
                    break;
                case "ASS_COMP":
                    sessionStatus = "SESS_COMP";
                    break;
            } 

            return sessionStatus;
        }

        /// <summary>
        /// Loading the interview session aganist the interview key
        /// </summary>
        /// <param name="interviewKey">
        /// A<see cref="System.String"/> that holds the online interview key
        /// </param>
        /// <param name="status">
        /// A <see cref="System.String"/> that holds the session status
        /// </param>
        /// <param name="searchText">
        /// A <see cref="System.String"/> that holds the searhc criteria
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="System.Int32"/> that holds the page no
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="System.Int32"/>that holds the page size
        /// </param>
        /// <param name="sortingKey">
        /// A <see cref="System.String"/> that holds the ascendin/desending value
        /// </param>
        /// <param name="sortByDirection">
        /// A <see cref="DataObjects.SortType"/> that holds the ascendin/desending flag
        /// </param>
        private void LoadOnlineInterviews(InterviewSearchCriteria interviewSearch)
        {
            int totalRecords = 0;

            List<OnlineCandidateSessionDetail> onlineInterviewCandidateList =
                new OnlineInterviewAssessorBLManager().GetOnlineInterviews(interviewSearch, out totalRecords);
 
            MyAssessments_onlineSchedulerList_GridView.DataSource = null;
            MyAssessments_onlineSchedulerList_GridView.DataBind();
             

            MyAssessments_pagingNavigator.TotalRecords = 1;
            MyAssessments_pagingNavigator.Reset();

            if (onlineInterviewCandidateList == null)
            {
                base.ShowMessage(MyAssessments_topErrorMessageLabel,
                    MyAssessments_bottomErrorMessageLabel, "No records found to display");
                return;
            } 

            MyAssessments_pagingNavigator.TotalRecords = totalRecords;
            MyAssessments_pagingNavigator.PageSize = base.GridPageSize;
            MyAssessments_pagingNavigator.MoveToPage(interviewSearch.CurrentPage);

            MyAssessments_onlineSchedulerList_GridView.DataSource = onlineInterviewCandidateList;
            MyAssessments_onlineSchedulerList_GridView.DataBind();

        }
        #endregion

    }
}