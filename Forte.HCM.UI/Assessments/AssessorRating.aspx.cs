#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AssessorRating.aspx.cs
// File that represents the user interface layout and functionalities
// for the PreviewAssessmentDetails page. This page helps in viewing 
// the candidate interview videos and provide rating. Also helps in 
// viewing other assessor rating and consolidated rating. 

#endregion Header

using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Configuration;
using System.Web.UI.HtmlControls;
using Forte.HCM.Utilities;


namespace Forte.HCM.UI.Assessments
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the AssessorRating.aspx page. This page helps in viewing 
    /// the candidate interview videos and provide rating. Also helps in 
    /// viewing other assessor rating and consolidated rating. This class
    /// inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class AssessorRating : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "130px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "105px";

        /// <summary>
        /// /// A <see cref="string"/> constant that holds the restored height of otehr assessor score all score
        /// </summary>
        public const string OVERALLSCORE_RESTORED_HEIGHT = "130px";

        /// <summary>
        /// /// /// A <see cref="string"/> constant that holds the expanded height of otehr assessor score all score
        /// </summary>
        public const string OVERALLSCORE_EXPANDED_HEIGHT = "105px";

        #endregion Private Constants

        #region Protected Variables
        /// <summary>
        /// Holding video url
        /// </summary>
        protected string videoURL;
        /// <summary>
        /// Holding question id
        /// </summary>
        protected string questionID;
        
        #endregion Protected Variables

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Assessment Rating");
                AssessorRating_topErrorMessageLabel.Text = string.Empty;
                AssessorRating_topSuccessMessageLabel.Text = string.Empty;

                if (!IsPostBack)
                {
                    LoadValues();
                }
                ExpandRestoreOtherAssessorSubjectScore();
                ExpandRestoreOtherOverallScore();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the question list.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void AssessorRating_questionAreaDataList_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                // Find the row type is DataRow
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    object dr = e.Row.DataItem;

                    decimal _rating = ((Forte.HCM.DataObjects.QuestionDetail)(dr)).Rating;
                    int _questionID = ((Forte.HCM.DataObjects.QuestionDetail)(dr)).QuestionID;

                    if (((Forte.HCM.DataObjects.QuestionDetail)(dr)).RatingEdit == "Y")
                    {
                        e.Row.CssClass = "RatingEditable";
                        if (_rating > 0)
                            e.Row.CssClass = "RatingFinished";
                    }
                    else
                    {
                        e.Row.CssClass = "RatingNotAllowed";
                    }
                    
                    // Find a label from the current row.
                    Label AssessorRating_rowNoLabel = (Label)e.Row.FindControl("AssessorRating_rowNoLabel");

                    // Assign the row no to the label.
                    AssessorRating_rowNoLabel.Text = (e.Row.RowIndex + 1).ToString();

                    LinkButton AssessorRating_questionAreaQuestionsLabel =
                        (LinkButton)e.Row.FindControl("AssessorRating_questionAreaQuestionsLabel");

                    HiddenField AssessorRating_questionAreaQuestionIDHiddenField =
                        (HiddenField)e.Row.FindControl("AssessorRating_questionAreaQuestionIDHiddenField");

                    HiddenField AssessorRating_questionAreaQuestionKeyHiddenField =
                        (HiddenField)e.Row.FindControl("AssessorRating_questionAreaQuestionKeyHiddenField");


                    if (((Forte.HCM.DataObjects.QuestionDetail)(dr)).RatingEdit != "Y")
                    {
                        AssessorRating_questionAreaQuestionsLabel.Style.Add("cursor", "default");
                        AssessorRating_questionAreaQuestionsLabel.Enabled = false;
                    }
                    
                    Label AssessorRating_questionAreaRatingLabel = (Label)e.Row.FindControl("AssessorRating_questionAreaRatingLabel");

                    if (!Utility.IsNullOrEmpty(AssessorRating_questionAreaRatingLabel.Text))
                    {
                        int questRating = 0;
                        questRating = (int)Math.Ceiling(Convert.ToDouble(AssessorRating_questionAreaRatingLabel.Text.ToString()));
                        if(questRating < 0)
                            AssessorRating_questionAreaRatingLabel.Text = "N/R";    
                        else
                            AssessorRating_questionAreaRatingLabel.Text = questRating.ToString();
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler to format the score
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AssessorRating_othersOverAllScore_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                decimal _overAllWeightageScore = 0;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField AssessorRating_overallAssessorWeightedGridviewHiddenField = (HiddenField)e.Row.FindControl(
                        "AssessorRating_overallAssessorWeightedGridviewHiddenField");

                    Label AssessorRating_overallAssessorWeightedGridviewLabel = (Label)e.Row.FindControl(
                        "AssessorRating_overallAssessorWeightedGridviewLabel");


                    if (!string.IsNullOrEmpty(Convert.ToString(AssessorRating_overallAssessorWeightedGridviewHiddenField.Value)))
                        _overAllWeightageScore =
                            Convert.ToDecimal(AssessorRating_overallAssessorWeightedGridviewHiddenField.Value);

                    if (_overAllWeightageScore < 0)
                        AssessorRating_overallAssessorWeightedGridviewLabel.Visible = false;
                    else
                    {
                        AssessorRating_overallAssessorWeightedGridviewLabel.Visible = true;
                        AssessorRating_overallAssessorWeightedGridviewLabel.Text =
                            string.Format("{0}%", String.Format("{0:0.00}", _overAllWeightageScore));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessorRating_topErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Hanlder to format the asssessor sore
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AssessorRating_assessorSubjectRatingGridview_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                decimal _assessorWeightageScore = 0;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField AssessorRating_assessorWeightageScoreGridviewHiddenField = (HiddenField)e.Row.FindControl(
                        "AssessorRating_assessorWeightageScoreGridviewHiddenField");

                    Label AssessorRating_assessorWeightageScoreGridviewLabel = (Label)e.Row.FindControl(
                        "AssessorRating_assessorWeightageScoreGridviewLabel");

                    if (!string.IsNullOrEmpty(Convert.ToString(AssessorRating_assessorWeightageScoreGridviewHiddenField.Value)))
                        _assessorWeightageScore =
                            Convert.ToDecimal(AssessorRating_assessorWeightageScoreGridviewHiddenField.Value);

                    if (_assessorWeightageScore < 0)
                        AssessorRating_assessorWeightageScoreGridviewLabel.Visible = false;
                    else
                    {
                        AssessorRating_assessorWeightageScoreGridviewLabel.Visible = true;
                        AssessorRating_assessorWeightageScoreGridviewLabel.Text = 
                            string.Format("{0}%", String.Format("{0:0.00}", _assessorWeightageScore));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessorRating_topErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler to format the other assessor subject rating
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AssessorRating_otherAssessorSubjectRatingGridview_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                decimal _otherAssessorWeightageScore = 0;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    HiddenField AssessorRating_otherAssessorWeightageScoreGridviewHiddenField = (HiddenField)e.Row.FindControl(
                        "AssessorRating_otherAssessorWeightageScoreGridviewHiddenField");

                    Label AssessorRating_otherAssessorWeightageScoreGridviewLabel = (Label)e.Row.FindControl(
                        "AssessorRating_otherAssessorWeightageScoreGridviewLabel");

                    if (!string.IsNullOrEmpty(Convert.ToString(AssessorRating_otherAssessorWeightageScoreGridviewHiddenField.Value)))
                        _otherAssessorWeightageScore =
                            Convert.ToDecimal(AssessorRating_otherAssessorWeightageScoreGridviewHiddenField.Value);

                    if (_otherAssessorWeightageScore < 0)
                        AssessorRating_otherAssessorWeightageScoreGridviewLabel.Visible = false;
                    else
                    {
                        AssessorRating_otherAssessorWeightageScoreGridviewLabel.Visible = true;
                        AssessorRating_otherAssessorWeightageScoreGridviewLabel.Text = 
                            string.Format("{0}%", String.Format("{0:0.00}", _otherAssessorWeightageScore));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the question link button is 
        /// clicked in the question list.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void AssessorRating_questionAreaQuestionsLabel_Click
            (object sender, EventArgs e)
        {
            try
            {
                LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the save button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void AssessorRating_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                AssessmentSummary assessmentSummary = new AssessmentSummary();
                assessmentSummary.Comments = AssessorRating_commentsTextbox.Text;
                assessmentSummary.CreatedBy = base.userID;
                assessmentSummary.AssessmentStatus = "ASS_INPRG";
                new AssessmentSummaryBLManager().SaveCandidateAssessmentRatngComments(assessmentSummary,
                    int.Parse(Request.QueryString["assessorId"]),
                    Request.QueryString.Get("CISK"),
                    int.Parse(Request.QueryString["ATMPID"]));

                base.ShowMessage(AssessorRating_topSuccessMessageLabel, "Candidate status saved successfully");
                LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the submit button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void AssessorRating_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                AssessmentSummary assessmentSummary = new AssessmentSummary();
                assessmentSummary.Comments = AssessorRating_commentsTextbox.Text;
                assessmentSummary.CreatedBy = base.userID;
                assessmentSummary.AssessmentStatus = "ASS_COMP";
                new AssessmentSummaryBLManager().SaveCandidateAssessmentRatngComments(assessmentSummary,
                    int.Parse(Request.QueryString["assessorId"]),
                    Request.QueryString.Get("CISK"),
                    int.Parse(Request.QueryString["ATMPID"]));

                //Update Candidate Interview Score
                DataSet overallCandidateRatingDataset = new AssessmentSummaryBLManager().
                GetRatingSummary(Request.QueryString.Get("CISK"), int.Parse(Request.QueryString["ATMPID"]));

                decimal overAllSubjectScore = 0;
                decimal overAllSubjectWeightageScore = 0;

                if (overallCandidateRatingDataset.Tables[5].Rows.Count != 0)
                    if (!string.IsNullOrEmpty(Convert.ToString(overallCandidateRatingDataset.Tables[5].Rows[0]["OVERALL_SCORE"])))
                        overAllSubjectScore = Convert.ToDecimal(overallCandidateRatingDataset.Tables[5].Rows[0]["OVERALL_SCORE"].ToString());


                if (overallCandidateRatingDataset.Tables[6].Rows.Count != 0)
                    if (!string.IsNullOrEmpty(Convert.ToString(overallCandidateRatingDataset.Tables[6].Rows[0]["OVERALL_WEIGHTAGE_SCORE"])))
                        overAllSubjectWeightageScore =
                            Convert.ToDecimal(overallCandidateRatingDataset.Tables[6].Rows[0]["OVERALL_WEIGHTAGE_SCORE"].ToString());

                new AssessmentSummaryBLManager().UpdateCanidateInterviewScore(Request.QueryString.Get("ISK"),
                    Request.QueryString.Get("CISK"), int.Parse(Request.QueryString["ATMPID"]), 
                    overAllSubjectScore, overAllSubjectWeightageScore, base.userID);

                LoadValues();
                base.ShowMessage(AssessorRating_topSuccessMessageLabel, "Candidate status completed successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when question group by drop down list
        /// item is seleted.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void AssessorRating_questiongroupByDropDownList_SelectedIndexChanged
            (object sender, EventArgs e)
        {
            try
            {
                if (AssessorRating_questiongroupByDropDownList.SelectedValue != "--Select--")
                {
                    BindAssessmentSummary(Request.QueryString.Get("CISK"), int.Parse(Request.QueryString["ATMPID"]),
                       int.Parse(Request.QueryString["assessorId"]),
                       Convert.ToChar(AssessorRating_questiongroupByDropDownList.SelectedValue));
                }
                else
                {
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the item data bound event is 
        /// fired in the assessor details list.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="DataListItemEventArgs"/> that holds the event data.
        /// </param>
        protected void AssessorRating_assessorDetailsDatalist_ItemDataBound
            (object sender, DataListItemEventArgs e)
        {
            try
            {
                HiddenField AssessorRating_assessorDetailsRatingHiddenField = (HiddenField)e.Item.FindControl
                    ("AssessorRating_assessorDetailsRatingHiddenField");

                Image AssessorRating_assessorDetailsRatingImage = (Image)e.Item.FindControl
                    ("AssessorRating_assessorDetailsRatingImage");

                Label AssessorRating_assessorDetailsRatingLabel = (Label)e.Item.FindControl
                    ("AssessorRating_assessorDetailsRatingLabel");

                if (AssessorRating_assessorDetailsRatingHiddenField.Value == "0")
                {
                    AssessorRating_assessorDetailsRatingImage.Visible = false;
                    AssessorRating_assessorDetailsRatingLabel.Text = "N/R";
                }
                else
                {
                    if (string.IsNullOrEmpty(base.GetSummaryRatingImage(
                        Convert.ToDecimal(AssessorRating_assessorDetailsRatingHiddenField.Value))))
                        AssessorRating_assessorDetailsRatingImage.Visible = false;
                    else
                    {
                        AssessorRating_assessorDetailsRatingImage.Visible = true;
                        AssessorRating_assessorDetailsRatingImage.ImageUrl = base.GetSummaryRatingImage(
                            Convert.ToDecimal(AssessorRating_assessorDetailsRatingHiddenField.Value));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event to handle row command events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AssessorRating_questionAreaDataList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "QuestionDetail")
                {
                    LinkButton lnk = (LinkButton)e.CommandSource;
                    HiddenField AssessorRating_questionAreaQuestionIDHiddenField =
                        (HiddenField)lnk.Parent.FindControl("AssessorRating_questionAreaQuestionIDHiddenField");
                    int testquestionID = Convert.ToInt32(AssessorRating_questionAreaQuestionIDHiddenField.Value);

                    HiddenField AssessorRating_questionAreaQuestionKeyHiddenField = 
                        (HiddenField)lnk.Parent.FindControl("AssessorRating_questionAreaQuestionKeyHiddenField");
                    string questionKey = AssessorRating_questionAreaQuestionKeyHiddenField.Value;

                    HiddenField AssessorRating_questionRatingEditHiddenField = 
                        (HiddenField)lnk.Parent.FindControl("AssessorRating_questionRatingEditHiddenField");
                    string ratingEdit = AssessorRating_questionRatingEditHiddenField.Value;

                    LoadCandidateResponse(testquestionID, questionKey, ratingEdit);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Refresh the page details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AssessorRating_refreshButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method to show the publish url popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AssessorRating_publishInterviewImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                InterviewScoreParamDetail interviewScoreParamDetail = null;
                if (interviewScoreParamDetail == null)
                    interviewScoreParamDetail = new InterviewScoreParamDetail();

                int attemptID = 0;
                if(!Utility.IsNullOrEmpty(Request.QueryString.Get("ATMPID").ToString()))
                    attemptID = Convert.ToInt32(Request.QueryString.Get("ATMPID").ToString());

                interviewScoreParamDetail.CandidateInterviewSessionKey = Request.QueryString.Get("CISK").ToString();
                interviewScoreParamDetail.AttemptID = attemptID;
                interviewScoreParamDetail.CandidateName = AssessorRating_candidateNameLinkButton.Text.ToString();
                AssessorRating_emailConfirmation_ConfirmMsgControl.InterviewParams = interviewScoreParamDetail;
                AssessorRating_emailConfirmation_ModalPopupExtender.Show();
                AssessorRating_emailConfirmation_ConfirmMsgControl.ShowScoreUrl();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AssessorRating_topErrorMessageLabel, exp.Message);
            }
        }
        #endregion Event Handlers

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindAssessmentSummary(Request.QueryString.Get("CISK"), int.Parse(Request.QueryString["ATMPID"]),
                int.Parse(Request.QueryString["assessorId"]), 'Q');
        }


        #endregion Protected Overridden Methods

        #region Private Methods

        /// <summary>
        /// Method that binds the assessment summary.
        /// </summary>
        /// <param name="candidateInterviewKey">
        /// A <see cref="string"/> that holds the candidate interview key.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="char"/> that holds the sort order.
        /// </param>
        private void BindAssessmentSummary(string candidateInterviewKey,
            int attemptID, int assessorID, char orderBy)
        {
            AssessmentSummary assessmentSummary = new AssessmentSummary();
            assessmentSummary =
                new AssessmentSummaryBLManager().GetAssessorSummary(candidateInterviewKey, attemptID, assessorID, orderBy);

            LoadCandidateDetails(assessmentSummary);

            DataSet overallRatingDataset = new AssessmentSummaryBLManager().
               GetRatingSummary(Request.QueryString.Get("CISK"), int.Parse(Request.QueryString["ATMPID"]));


            if (overallRatingDataset.Tables[0].Rows.Count == 0)
            {
                base.ShowMessage(AssessorRating_topErrorMessageLabel, "");
            }
            else
            {
                decimal assessorWeightedScore = 0;

                DataView dvOverallWeightedFilterByAssessor = new DataView(overallRatingDataset.Tables[0]);
                dvOverallWeightedFilterByAssessor.RowFilter = "ASSESSOR_ID = " + assessorID;
                DataTable dtOverallWeightedAssessorScore = dvOverallWeightedFilterByAssessor.ToTable();
                dtOverallWeightedAssessorScore.AcceptChanges();

                if (dtOverallWeightedAssessorScore.Rows.Count > 0)
                {
                    assessorWeightedScore = Convert.ToDecimal(dtOverallWeightedAssessorScore.Rows[0]["OVERALL_ASSESSOR_WEIGHTED"].ToString());
                    AssessorRating_assessorOverallWeightageScoreValueLabel.Text = 
                        string.Format("{0}%", String.Format("{0:0.00}", assessorWeightedScore));
                }
            }

            decimal _overAllScore = 0;
            decimal _overWeightageScore = 0;

            if (overallRatingDataset.Tables[5].Rows.Count != 0)
                if (!string.IsNullOrEmpty(Convert.ToString(overallRatingDataset.Tables[5].Rows[0]["OVERALL_SCORE"])))
                    _overAllScore = Convert.ToDecimal(overallRatingDataset.Tables[5].Rows[0]["OVERALL_SCORE"].ToString());


            if (overallRatingDataset.Tables[6].Rows.Count != 0)
                if (!string.IsNullOrEmpty(Convert.ToString(overallRatingDataset.Tables[6].Rows[0]["OVERALL_WEIGHTAGE_SCORE"])))
                    _overWeightageScore =
                        Convert.ToDecimal(overallRatingDataset.Tables[6].Rows[0]["OVERALL_WEIGHTAGE_SCORE"].ToString());

            AssessorRating_overallWeightageScoreLabelValue.Text = string.Format("{0}%", String.Format("{0:0.00}", _overWeightageScore));

           
            DataTable dtOtherAssessorSubjectScore = new DataTable();
            //Assign subjec score
            if (overallRatingDataset.Tables[1].Rows.Count == 0)
            {
                base.ShowMessage(AssessorRating_topErrorMessageLabel, "");
            }
            else
            {
                //Set current assessor subject score                
                DataView dvFilterByAssessor = new DataView(overallRatingDataset.Tables[1]);
                dvFilterByAssessor.RowFilter = "ASSESSOR = " + assessorID;
                DataTable dtAssessorSubjectScore = dvFilterByAssessor.ToTable();
                dtAssessorSubjectScore.AcceptChanges();

                AssessorRating_assessorSubjectRatingGridview.DataSource = dtAssessorSubjectScore;
                AssessorRating_assessorSubjectRatingGridview.DataBind();

                DataView dvFilterByOtherAssessor = new DataView(overallRatingDataset.Tables[1]);
                dvFilterByOtherAssessor.RowFilter = "ASSESSOR <> " + assessorID;
                dtOtherAssessorSubjectScore = dvFilterByOtherAssessor.ToTable();
                dtOtherAssessorSubjectScore.AcceptChanges();

                dtOtherAssessorSubjectScore.DefaultView.Sort = "CAT_SUB_ID ASC";

                //Set other assessor subject score 
                AssessorRating_otherAssessorSubjectRatingGridview.DataSource = dtOtherAssessorSubjectScore.DefaultView.ToTable();
                AssessorRating_otherAssessorSubjectRatingGridview.DataBind();
            }

            //Set other assesor overall score and comment
            if (overallRatingDataset.Tables[0].Rows.Count == 0)
            {
                base.ShowMessage(AssessorRating_topErrorMessageLabel, "");
            }
            else
            {
                DataView dvOtherAssessor = new DataView(overallRatingDataset.Tables[0]);
                dvOtherAssessor.RowFilter = "ASSESSOR_ID <> " + assessorID;
                DataTable dtOtherAssessor = dvOtherAssessor.ToTable();
                dtOtherAssessor.AcceptChanges();

                AssessorRating_othersOverAllScore.DataSource = dtOtherAssessor;
                AssessorRating_othersOverAllScore.DataBind();
            }

            if (assessmentSummary.QuestionDetails == null)
            {
                base.ShowMessage(AssessorRating_topErrorMessageLabel, "");
            }
            else
            {
                AssessorRating_questionAreaDataList.DataSource = assessmentSummary.QuestionDetails;
                AssessorRating_questionAreaDataList.DataBind();
            }
        }
        
        
        /// <summary>
        /// Method that loads the candidate details.
        /// </summary>
        /// <param name="assessmentSummary">
        /// A <see cref="AssessmentSummary"/> that holds the assessment summary.
        /// </param>
        private void LoadCandidateDetails(AssessmentSummary assessmentSummary)
        {
            //Get test completed date
            DateTime testCompletedDate = DateTime.Now;
            testCompletedDate = new ReportBLManager().GetInterviewTestCompletedDateByCandidate(Request.QueryString["CISK"],
                Convert.ToInt32(Request.QueryString["ATMPID"]));

            string interviewName = new InterviewReportBLManager().GetInterviewTestName(Request.QueryString["ISK"].ToString());
            AssessorRating_interviewNameLabel.Text = interviewName;

            if (!Utility.IsNullOrEmpty(testCompletedDate))
            {
                AssessorRating_testCompletedDateLabel.Text =
                  "Interview Completed on " + testCompletedDate.ToString("MMMM dd, yyyy");
            }
            AssessorRating_candidateNameLinkButton.Text = assessmentSummary.CanidateName;

              // Add handler for candidate name link button.
            if (assessmentSummary.CandidateID != 0)
            {
                AssessorRating_candidateNameLinkButton.Attributes.Add("onclick",
                    "javascript:return OpenViewCandidateProfilePopup('" + assessmentSummary.CandidateInfoID + "');");
            }

            AssessorRating_clientNameLinkButton.Text = assessmentSummary.ClientName;

            // Add handler for client name link button.
            if (assessmentSummary.ClientID != 0)
            {
                AssessorRating_clientNameLinkButton.Attributes.Add("onclick",
                    "javascript:return ShowViewClient('" + assessmentSummary.ClientID + "');");
            }
            
            // Get client details
            if (assessmentSummary.PositionProfileID != 0)
            {
                ViewState["POSITION_PROFILE_ID"] = assessmentSummary.PositionProfileID;
                ClientInformation clientInfo = 
                    new ClientBLManager().GetClientInfoPositionProfileID(assessmentSummary.PositionProfileID);

                if (!Utility.IsNullOrEmpty(clientInfo))
                {
                    // Get client departments
                    new ClientInfoDataManager().GetClientDepartmentContactInfo(AssessorRating_showClientDepartmentsLabel,
                        clientInfo.ClientDepartments, "CD", true);
                    // Get client contacts
                    new ClientInfoDataManager().GetClientDepartmentContactInfo(AssessorRating_showClientContactsLabel,
                        clientInfo.ClientContacts, "CC", true);
                }
            }
           

            AssessorRating_positionProfileLinkButton.Text = assessmentSummary.PositionProfileName;

            // Add handler for position profile link button.
            if (assessmentSummary.PositionProfileID != 0)
            {
                AssessorRating_positionProfileLinkButton.Attributes.Add("onclick",
                    "javascript:return ShowViewPositionProfile('" + assessmentSummary.PositionProfileID + "');");
            }

            if ((assessmentSummary.CompletedOn == Convert.ToDateTime("01/01/1901")) ||
                (assessmentSummary.CompletedOn == Convert.ToDateTime("1/1/0001 12:00:00 AM")) ||
                (Convert.ToString(assessmentSummary.CompletedOn) == string.Empty))
            {
                ////AssessorRating_completedDateTextBox.Text = string.Empty;
            }
            ////else
                ////AssessorRating_completedDateTextBox.Text = Convert.ToDateTime(assessmentSummary.CompletedOn).ToShortDateString();

            ////if (!Utility.IsNullOrEmpty(assessmentSummary.OverallRating))
                ////AssessorRating_userRatingRating.CurrentRating = Convert.ToInt32(assessmentSummary.OverallRating);

            AssessorRating_commentsTextbox.Text = assessmentSummary.Comments;

            // Assign candidate image handler.
            AssessorRating_candidatePhotoImage.ImageUrl = "~/Common/CandidateImageHandler.ashx?source=VIEW_CAND&candidateid=" +
                assessmentSummary.CandidateInfoID;
        }

        /// <summary>
        /// Method to load question detail when question is loaded
        /// </summary>
        /// <param name="questionID"></param>
        /// <param name="questionKey"></param>
        private void LoadCandidateResponse(int testQuestionID, string questionKey, string ratingEdit)
        {
            if (!Utility.IsNullOrEmpty(ViewState["POSITION_PROFILE_ID"]))
            {
                ClientInformation clientInfo =
                    new ClientBLManager().GetClientInfoPositionProfileID(Convert.ToInt32(ViewState["POSITION_PROFILE_ID"]));

                if (!Utility.IsNullOrEmpty(clientInfo))
                {
                    // Get client departments
                    new ClientInfoDataManager().GetClientDepartmentContactInfo(AssessorRating_showClientDepartmentsLabel,
                        clientInfo.ClientDepartments, "CD", true);
                    // Get client contacts
                    new ClientInfoDataManager().GetClientDepartmentContactInfo(AssessorRating_showClientContactsLabel,
                        clientInfo.ClientContacts, "CC", true);
                }
            }

            string candidateSessionID = null;
            string interviewKey = string.Empty;
            string iFrameSrc = string.Empty;
            string baseURL = ConfigurationManager.AppSettings["DEFAULT_URL"];

            // Check if candidate session ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["CISK"]))
            {
                ShowMessage(AssessorRating_topErrorMessageLabel,
                    "Candidate session ID is not present");
                return;
            }
            candidateSessionID = Request.QueryString["CISK"].Trim();

            // Check if attempt ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["ATMPID"]))
            {
                ShowMessage(AssessorRating_topErrorMessageLabel,
                    "Attempt ID is not present");
                return;
            }

            // Check if attempt ID is valid.
            int attemptID = 0;
            if (int.TryParse(Request.QueryString["ATMPID"].Trim(), out attemptID) == false)
            {
                ShowMessage(AssessorRating_topErrorMessageLabel,
                    "Attempt ID is invalid");
                return;
            }

            // Check if interview key is given 
            if (Utility.IsNullOrEmpty(Request.QueryString["ISK"]))
            {
                ShowMessage(AssessorRating_topErrorMessageLabel,
                    "Interview Key is not present");
                return;
            }
            interviewKey = Request.QueryString["ISK"].ToString();

            AssessorRating_saveButton.Enabled = true;
            AsssessorRating_candidateResponseIframe.Visible = true;

            if (ratingEdit.ToString().Trim().ToUpper() == "Y")
            {
                iFrameSrc = baseURL + "Assessments/CandidateInterviewResponse.aspx";
                iFrameSrc += "?interviewKey=" + interviewKey;
                iFrameSrc += "&candidatesessionid=" + candidateSessionID;
                iFrameSrc += "&attemptid=" + attemptID;
                iFrameSrc += "&testquestionid=" + testQuestionID;
                iFrameSrc += "&questionKey=" + questionKey;
                iFrameSrc += "&assessorID=" + Request.QueryString["assessorId"];
                iFrameSrc += "&sessionKey=" + Request.QueryString["sessionkey"];

                AsssessorRating_candidateResponseIframe.Attributes["src"] = iFrameSrc;
            }
            else
                AsssessorRating_candidateResponseIframe.Attributes["src"] = null;
        }

        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>
        private void ExpandRestoreOtherAssessorSubjectScore()
        {
            if (AssessorRating_otherAssessorSubjectRatingRestoreHiddenField.Value == "Y")
            {
                AssessorRating_OtherAssessorSubjectScoreDiv.Style["display"] = "block";
                AssessorRating_otherAssessorSubjectRatingUpSpan.Style["display"] = "none";
                AssessorRating_otherAssessorSubjectRatingDownSpan.Style["display"] = "block";
                AssessorRating_AssessorSubjectScoreDiv.Style["height"] = RESTORED_HEIGHT;
            }
            else
            {
                AssessorRating_OtherAssessorSubjectScoreDiv.Style["display"] = "none";
                AssessorRating_otherAssessorSubjectRatingUpSpan.Style["display"] = "block";
                AssessorRating_otherAssessorSubjectRatingDownSpan.Style["display"] = "none";
                AssessorRating_AssessorSubjectScoreDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            AssessorRating_assessorSubjectScoreTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                AssessorRating_AssessorSubjectScoreDiv.ClientID + "','" +
                AssessorRating_OtherAssessorSubjectScoreDiv.ClientID + "','" +
                AssessorRating_otherAssessorSubjectRatingUpSpan.ClientID + "','" +
                AssessorRating_otherAssessorSubjectRatingDownSpan.ClientID + "','" +
                AssessorRating_otherAssessorSubjectRatingRestoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
        }


        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>
        private void ExpandRestoreOtherOverallScore()
        {
            if (AssessorRating_overallScoreRestoreHiddenField.Value == "Y")
            {
                AssessorRating_othersOverAllScoreDiv.Style["display"] = "block";
                AssessorRating_overallScoreUpSpan.Style["display"] = "none";
                AssessorRating_overallScoreDownSpan.Style["display"] = "block";
                AssessorRating_overAllScoreDiv.Style["height"] = OVERALLSCORE_RESTORED_HEIGHT;
            }
            else
            {
                AssessorRating_othersOverAllScoreDiv.Style["display"] = "none";
                AssessorRating_overallScoreUpSpan.Style["display"] = "block";
                AssessorRating_overallScoreDownSpan.Style["display"] = "none";
                AssessorRating_overAllScoreDiv.Style["height"] = OVERALLSCORE_EXPANDED_HEIGHT;
            }
            AssessorRating_otherOverallAssessorScoreTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                AssessorRating_overAllScoreDiv.ClientID + "','" +
                AssessorRating_othersOverAllScoreDiv.ClientID + "','" +
                AssessorRating_overallScoreUpSpan.ClientID + "','" +
                AssessorRating_overallScoreDownSpan.ClientID + "','" +
                AssessorRating_overallScoreRestoreHiddenField.ClientID + "','" +
                OVERALLSCORE_RESTORED_HEIGHT + "','" +
                OVERALLSCORE_EXPANDED_HEIGHT + "')");
        }

        
        #endregion Private Methods
    }
}