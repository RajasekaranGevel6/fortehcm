using System;
using System.Net;
using System.Web.UI;
using System.Configuration;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Forte.HCM.UI.Assessments
{
    public partial class PublishCandidateInterviewResponse : PageBase
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Protected Variables
        protected string videoURL;
        protected string questionID;
        #endregion Protected Variables

        #region Events Handlers
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                PublishCandidateInterviewResponse_successMessageLabel.Text = string.Empty;
                PublishCandidateInterviewResponse_errorMessageLabel.Text = string.Empty;
                // Load the skill values.
                if (!IsPostBack)
                {
                    /*string d = Request.QueryString.Get("sessionkey");
                    string Q = Request.QueryString["testquestionid"];*/
                    LoadValues();

                    PublishCandidateInterviewResponse_downloadVideoLinkButton.Attributes.Add
                       ("onclick", "javascript:DownloadVideo('" +
                       Request.QueryString["candidatesessionid"] + "','" +
                       Request.QueryString["attemptid"] + "','" +
                       Request.QueryString["testquestionid"] + "');");

                    string serverURL = ConfigurationManager.AppSettings["VIDEO_STREAMING_SERVER_URL"].ToString();
                    videoURL = string.Concat(serverURL, Request.QueryString["candidatesessionid"], "_" + Request.QueryString["attemptid"]);
                    questionID = Request.QueryString["testquestionid"].ToString();

                    string showComment = Request.QueryString["showcomment"].ToString();
                    if (showComment.ToString().ToUpper() == "Y")
                    {
                        CandiateInterviewResponse_ratingDiv.Style["display"] = "block";
                    }
                    else
                    {
                        CandiateInterviewResponse_ratingDiv.Style["display"] = "none";
                    }
                }
                ExpandRestore();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PublishCandidateInterviewResponse_errorMessageLabel, exp.Message);
            }
        }
        
        
        protected void PublishCandidateInterviewResponse_cancelLinkButton_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Closeform", "<script language='javascript'>self.close();</script>", false);
        }

        protected void PublishCandidateInterviewResponse_downloadVideoLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                string downloadPath = "~/Common/Download.aspx?type=IV" +
                    "&candidatesessionid=" + Request.QueryString["candidatesessionid"] +
                    "&attemptid=" + Request.QueryString["attemptid"] +
                    "&testquestionid=" + Request.QueryString["testquestionid"];

                Response.Redirect(downloadPath, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }


        /// <summary>
        /// Handler to rating values to percentage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PublishCandidateInterviewResponse_otherAssessorRatingCommentsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                decimal ratingPercentage = 0;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    HiddenField PublishCandidateInterviewResponse_ratingPercentageHiddenField = (HiddenField)e.Row.FindControl(
                        "PublishCandidateInterviewResponse_ratingPercentageHiddenField");

                    Label PublishCandidateInterviewResponse_ratingPercentageLabel = (Label)e.Row.FindControl(
                        "PublishCandidateInterviewResponse_ratingPercentageLabel");

                    if (!string.IsNullOrEmpty(Convert.ToString(PublishCandidateInterviewResponse_ratingPercentageHiddenField.Value)))
                        ratingPercentage = Convert.ToDecimal(PublishCandidateInterviewResponse_ratingPercentageHiddenField.Value);


                    if (ratingPercentage <= 0)
                        PublishCandidateInterviewResponse_ratingPercentageLabel.Visible = false;
                    else
                    {
                        PublishCandidateInterviewResponse_ratingPercentageLabel.Visible = true;
                        PublishCandidateInterviewResponse_ratingPercentageLabel.Text = string.Concat("Rating(", string.Format("{0}%", string.Format("{0:0.00}", ratingPercentage)), ")");
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PublishCandidateInterviewResponse_errorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden protected method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
            string candidateSessionID = null;
            string questionKey = string.Empty;
            string interviewKey = string.Empty;

            // Check if candidate session ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]))
            {
                ShowMessage(PublishCandidateInterviewResponse_errorMessageLabel,
                    "Candidate session ID is not present");
                return;
            }
            candidateSessionID = Request.QueryString["candidatesessionid"].Trim();

            // Check if attempt ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
            {
                ShowMessage(PublishCandidateInterviewResponse_errorMessageLabel,
                    "Attempt ID is not present");
                return;
            }

            // Check if attempt ID is valid.
            int attemptID = 0;
            if (int.TryParse(Request.QueryString["attemptid"].Trim(), out attemptID) == false)
            {
                ShowMessage(PublishCandidateInterviewResponse_errorMessageLabel,
                    "Attempt ID is invalid");
                return;
            }

            // Check if test question ID is given.
            if (Utility.IsNullOrEmpty(Request.QueryString["testquestionid"]))
            {
                ShowMessage(PublishCandidateInterviewResponse_errorMessageLabel,
                    "Test question ID is not present");
                return;
            }

            // Check if test question ID is valid.
            int testQuestionID = 0;
            if (int.TryParse(Request.QueryString["testquestionid"].Trim(), out testQuestionID) == false)
            {
                ShowMessage(PublishCandidateInterviewResponse_errorMessageLabel,
                    "Test question ID is invalid");
                return;
            }

            // Check if interview key is given 
            if (Utility.IsNullOrEmpty(Request.QueryString["interviewKey"]))
            {
                ShowMessage(PublishCandidateInterviewResponse_errorMessageLabel,
                    "Interview Key is not present");
                return;
            }
            interviewKey = Request.QueryString["interviewKey"].ToString();

            // Check if question key is given 
            if (Utility.IsNullOrEmpty(Request.QueryString["questionKey"]))
            {
                ShowMessage(PublishCandidateInterviewResponse_errorMessageLabel,
                    "Question Key is not present");
                return;
            }
            questionKey = Request.QueryString["questionKey"].ToString();

            
            // Get interview question attributes
            QuestionDetail questionAttributes = new InterviewReportBLManager().
                GetInterviewTestQuestionAttributesByKeys(interviewKey, questionKey);
                        
            if (questionAttributes != null)
            {
                //Set question comments
                InterviewCandidateTestDetails_commentValueLabel.Text = questionAttributes.Comments == null ? questionAttributes.Comments :
                    questionAttributes.Comments.ToString().Replace(Environment.NewLine, "<br />");
            }

            //
            // Get interview response
            InterviewResponseDetail logDetail = new InterviewReportBLManager().
                GetInterviewResponseDetail(candidateSessionID, attemptID, testQuestionID, base.userID);

            if (logDetail == null || logDetail.Question==null)
            {
                ShowMessage(PublishCandidateInterviewResponse_errorMessageLabel,
                    "No data found to display");

                return;
            }
            // Assign details.
            PublishCandidateInterviewResponse_questionValueLabel.Text = logDetail.Question == null ? logDetail.Question :
                logDetail.Question.ToString().Replace(Environment.NewLine, "<br />");

            if (logDetail.HasImage)
            {
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = logDetail.QuestionImage;
                PublishCandidateInterviewResponse_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=INTERVIEW_QUESTION_IMAGE&questionKey=" + logDetail.QuestionKey;
                PublishCandidateInterviewResponse_questionImageDiv.Style["display"] = "block";
            }
            else
            {
                PublishCandidateInterviewResponse_questionImageDiv.Style["display"] = "none";
            }
            if (!Utility.IsNullOrEmpty(logDetail.ChoiceDesc))
            {
                string choiceDesc = logDetail.ChoiceDesc == null ? logDetail.ChoiceDesc : logDetail.ChoiceDesc.ToString().ToString().Replace("&#x0D;", "");
                CandidateResponse_questionAnswerLabel.Text = choiceDesc == null ? choiceDesc : choiceDesc.ToString().ToString().Replace(Environment.NewLine, "<br />");
            }

            PublishCandidateInterviewResponse_candidateCommentsValueLabel.Text = logDetail.CandidateComments == null ? logDetail.CandidateComments :
                logDetail.CandidateComments.ToString().Replace(Environment.NewLine, "<br />");

            CandidateInterviewResponse_weightageValueLabel.Text = logDetail.Weightage.ToString();
            CandidateInterviewResponse_categoryValueLabel.Text = logDetail.CategoryName;
            CandidateInterviewResponse_subjectValueLabel.Text = logDetail.SubjectName;
            PublishCandidateInterviewResponse_skippedValueLabel.Text = logDetail.Skipped ? "Yes" : "No";
            PublishCandidateInterviewResponse_complexityValueLabel.Text = logDetail.Complexity;
            PublishCandidateInterviewResponse_testAreaValueLabel.Text = logDetail.TestArea;

            
            if (logDetail.RatingComments == null)
            {
                ShowMessage(PublishCandidateInterviewResponse_errorMessageLabel, "");
                return;
            }
            
            CandidateInterviewResponse_otherAssessorRatingCommentsGridView.DataSource = logDetail.RatingComments;
            CandidateInterviewResponse_otherAssessorRatingCommentsGridView.DataBind();
        }

        #endregion Protected Overridden Methods

        #region Private Methods

        /// <summary>
        /// Method that checks if the given remote file exists or not.
        /// </summary>
        /// <param name="url">
        /// A <see cref="string"/> that holds the remote file URL.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the exist status. True if exists
        /// and false otherwise.
        /// </returns>
        private bool IsRemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";

                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                //Returns TURE if the Status code == 200
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                // Any exception will returns false.
                return false;
            }
        }

        /// <summary>
        /// Expand & Restore the Result Grid 
        /// </summary>
        private void ExpandRestore()
        {
            if (CandiateInterviewResponse_restoreHiddenField.Value == "Y")
            {
                CandidateInterviewResponse_otherCommentsDiv.Style["display"] = "none";
                CandiateInterviewResponse_otherCommentsUpSpan.Style["display"] = "block";
                CandiateInterviewResponse_otherCommentsDownSpan.Style["display"] = "none";
                CandiateInterviewResponse_ratingDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                CandidateInterviewResponse_otherCommentsDiv.Style["display"] = "block";
                CandiateInterviewResponse_otherCommentsUpSpan.Style["display"] = "none";
                CandiateInterviewResponse_otherCommentsDownSpan.Style["display"] = "block";
                CandiateInterviewResponse_ratingDiv.Style["height"] = RESTORED_HEIGHT;
            }
            CandiateInterviewResponse_otherCommentsTR.Attributes.Add("onclick",
                "ExpandOrRestore('" +
                CandiateInterviewResponse_ratingDiv.ClientID + "','" +
                CandidateInterviewResponse_otherCommentsDiv.ClientID + "','" +
                CandiateInterviewResponse_otherCommentsUpSpan.ClientID + "','" +
                CandiateInterviewResponse_otherCommentsDownSpan.ClientID + "','" +
                CandiateInterviewResponse_restoreHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" +
                EXPANDED_HEIGHT + "')");
        }

        #endregion Private Methods


    }
}