﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateAssessorSummary.aspx.cs
// File that represents the user interface layout and functionalities
// for the CandidateAssessorSummary page. This page helps in viewing the 
// assessor's rating summary of a candidate. This class inherits the 
// Forte.HCM.UI.Common.PageBase class.

#endregion Header 

#region Directives                                                             

using System;
using System.IO;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;

using iTextSharp.text;
using iTextSharp.text.pdf;

#endregion Directives

namespace Forte.HCM.UI.Assessments
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the CandidateAssessorSummary page. This page helps in viewing the 
    /// assessor's rating summary of a candidate. This class inherits the 
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class CandidateAssessorSummary : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "200px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Constants

        #region Private Variables                                              

        /// <summary>
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </summary>
        private string candidateSessionID = null;

        /// <summary>
        /// A <see cref="int"/> that holds the attempt ID.
        /// </summary>
        private int attemptID = 0;

        /// <summary>
        /// A <see cref="BaseColor"/> that holds the item color.
        /// </summary>
        static BaseColor itemColor = new BaseColor(20, 142, 192);

        /// <summary>
        /// A <see cref="BaseColor"/> that holds the header color.
        /// </summary>
        static BaseColor headerColor = new BaseColor(40, 48, 51);

        /// <summary>
        /// A <see cref="BaseColor"/> that holds the item black color.
        /// </summary>
        static BaseColor itemBlackColor = new BaseColor(0, 0, 0); 
         
    
        #endregion Private Variables

        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set page caption.
                Master.SetPageCaption("Candidate's Interview Assessment Summary");

                // Assign handler to maximize/restore icon.
                CandidateAssessorSummary_searchResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    CandidateAssessorSummary_tableDiv.ClientID + "','" +
                     CandidateAssessorSummary_searchCriteriasDiv.ClientID + "','" +
                    CandidateAssessorSummary_searchResultsUpSpan.ClientID + "','" +
                    CandidateAssessorSummary_searchResultsDownSpan.ClientID + "','" +
                    CandidateAssessorSummary_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");

                // Set maximize/restore state based.
                CheckAndSetExpandorRestore();

                // Assign values.
                AssignValues();

                if (!IsPostBack)
                {
                    // Load header.
                    LoadHeader();
                }

                // Load the report table.
                LoadTable();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateAssessorSummary_topErrorMessageLabel,
                    CandidateAssessorSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when convert to assessor icon is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reload the data.
        /// </remarks>
        protected void CandidateAssessorSummary_convertToRegisteredUserButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Set successfull message
                base.ShowMessage(CandidateAssessorSummary_topSuccessMessageLabel, "External assessor is converted to registered user");

                // Load the report table.
                LoadTable();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateAssessorSummary_topErrorMessageLabel,
                    CandidateAssessorSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the refresh button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reload the data.
        /// </remarks>
        protected void CandidateAssessorSummary_refreshButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear the message controls
                ClearControls();

                // Load the report table.
                LoadTable();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateAssessorSummary_topErrorMessageLabel,
                    CandidateAssessorSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the email button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reload the data.
        /// </remarks>
        protected void CandidateAssessorSummary_emailButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear the message controls
                ClearControls();

                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(),"script", 
                    "javascript:OpenEmailAttachment('" + Request.QueryString["candidatesessionid"].ToString().Trim() + "','"
                    + Request.QueryString["attemptid"].ToString().Trim() + "','" + 0 + "','"
                    + Request.QueryString["parentpage"].ToString().Trim() + "','"
                    + "CIAS" + "')", true);  //CIAS -> Candidate's Interview Assessment Summary 
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateAssessorSummary_topErrorMessageLabel,
                    CandidateAssessorSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the publish button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will load the publish popup window with the URLs in it.
        /// </remarks>
        protected void CandidateAssessorSummary_publishImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Clear the message controls
                ClearControls();

                InterviewScoreParamDetail interviewScoreParamDetail = null;
                if (interviewScoreParamDetail == null)
                    interviewScoreParamDetail = new InterviewScoreParamDetail();
                int attemptID = 0;
                if (!Utility.IsNullOrEmpty(Request.QueryString.Get("attemptid").ToString()))
                    attemptID = Convert.ToInt32(Request.QueryString.Get("attemptid").ToString());
                
                interviewScoreParamDetail.CandidateInterviewSessionKey = Request.QueryString.Get("candidatesessionid").ToString();
                interviewScoreParamDetail.AttemptID = attemptID;
                interviewScoreParamDetail.CandidateName = CandidateAssessorSummary_candidateNameValueLinkButton.Text.ToString();
                CandidateAssessorSummary_emailConfirmation_ConfirmMsgControl.InterviewParams = interviewScoreParamDetail;
                CandidateAssessorSummary_emailConfirmation_ModalPopupExtender.Show();
                CandidateAssessorSummary_emailConfirmation_ConfirmMsgControl.ShowScoreUrl();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateAssessorSummary_topErrorMessageLabel,
                    CandidateAssessorSummary_bottomErrorMessageLabel, exp.Message);
            }
        }
        
        /// <summary>
        /// Handler method that will be called when the download button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will reload the data.
        /// </remarks>
        protected void CandidateAssessorSummary_downloadButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear the message controls
                ClearControls();

                string candidateSessionID="";
                int attemptID=0;
                DataTable assessmentTable = new DataTable();

                if (Request.QueryString["candidatesessionid"]== null &&  Request.QueryString["attemptid"]==null)
                {
                     base.ShowMessage(CandidateAssessorSummary_topErrorMessageLabel,
                    CandidateAssessorSummary_bottomErrorMessageLabel, "Assessment data not found.");
                    return;
                }

                candidateSessionID =Convert.ToString(Request.QueryString["candidatesessionid"]);
                attemptID =Convert.ToInt32(Request.QueryString["attemptid"]);

                CandidateInterviewSessionDetail sessionDetail = new InterviewSchedulerBLManager().
                    GetCandidateInterviewSessionDetail(candidateSessionID, attemptID);
    
                PDFDocumentWriter pdfWriter= new PDFDocumentWriter();

                string fileName = sessionDetail.CandidateName + "_" + "InterviewAssessmentSummary.pdf"; 

                Response.ContentType = "application/pdf"; 
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName); 
                Response.Cache.SetCacheability(HttpCacheability.NoCache);

                Document doc = new Document(PDFDocumentWriter.paper,
                    PDFDocumentWriter.leftMargin, PDFDocumentWriter.rightMargin,
                    PDFDocumentWriter.topMargin, PDFDocumentWriter.bottomMargin);

                string logoPath = Server.MapPath("~/");

                iTextSharp.text.Image imgBackground = iTextSharp.text.Image.GetInstance(logoPath + "Images/AssessmentBackground.jpg"); 
               
                imgBackground.ScaleToFit(1000,132); 
                imgBackground.Alignment = iTextSharp.text.Image.UNDERLYING;
                imgBackground.SetAbsolutePosition(50, 360);

                PdfPTable pageHeader = new PdfPTable(pdfWriter.PageHeader(logoPath, "Candidate Interview Assessment Report"));  
                pageHeader.SpacingAfter =10f; 

                //Construct Table Structures    
                PdfPTable tableHeaderDetail = new PdfPTable(1);
                PdfPCell cellBorder = new PdfPCell(new Phrase(""));
                cellBorder.BorderColor  = iTextSharp.text.BaseColor.LIGHT_GRAY ;
                cellBorder.Border = 0; 
                tableHeaderDetail.WidthPercentage  = 90;
                cellBorder.PaddingTop = 20f;
                cellBorder.AddElement(pdfWriter.TableHeader(sessionDetail,candidateSessionID,
                    attemptID, GetCandidateImageFile(sessionDetail.CandidateInformationID), out assessmentTable)); 
                tableHeaderDetail.AddCell(cellBorder);
                tableHeaderDetail.SpacingAfter = 30f;
                //Response Output
                PdfWriter.GetInstance(doc, Response.OutputStream); 
                 
                doc.Open();
                doc.Add(pageHeader);
                doc.Add(imgBackground);
                //doc.Add(pdfWriter.PageTitle("Candidate's Interview Assessment Summary")); 
                doc.Add(tableHeaderDetail);
                //doc.Add(pdfWriter.PageTitle("Assessment Details"));
                doc.Add(pdfWriter.TableDetail(assessmentTable)); 
                doc.Close(); 
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateAssessorSummary_topErrorMessageLabel,
                    CandidateAssessorSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that retrieves the image path for the given candidate ID.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the candidate image path.
        /// </returns>
        private string GetCandidateImageFile(int candidateID)
        {
            string path = null;

            bool isExists = false;
            if (candidateID > 0)
            {
                path = Server.MapPath("~/") +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                            "\\" + candidateID + "." +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];
                if (File.Exists(path))
                    isExists = true;
                else
                    isExists = false;
            }

            if (isExists)
                return path;
            else
            {     // Show the 'not available image'.
                return Server.MapPath("~/") +
                      ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                      "\\" + "photo_not_available.jpg";
            }
        }

        /// <summary>
        /// Method that that assign values to the required parameters.
        /// </summary>
        private void AssignValues()
        {
            // Check if candidate session ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]))
            {
                base.ShowMessage(CandidateAssessorSummary_topErrorMessageLabel,
                    CandidateAssessorSummary_bottomErrorMessageLabel, "Candidate session ID is not passed");
                return;
            }
            candidateSessionID = Request.QueryString["candidatesessionid"].Trim();

            // Check if attempt ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
            {
                base.ShowMessage(CandidateAssessorSummary_topErrorMessageLabel,
                    CandidateAssessorSummary_bottomErrorMessageLabel, "Attempt ID is not passed");
                return;
            }

            // Convert attempt ID to number.
            if (int.TryParse(Request.QueryString["attemptid"].Trim(), out attemptID) == false)
            {
                base.ShowMessage(CandidateAssessorSummary_topErrorMessageLabel,
                    CandidateAssessorSummary_bottomErrorMessageLabel, "Attempt ID is invalid");
                return;
            }
        }

        /// <summary>
        /// Method that clears all messages.
        /// </summary>
        private void ClearMessages()
        {
            CandidateAssessorSummary_topSuccessMessageLabel.Text = string.Empty;
            CandidateAssessorSummary_bottomSuccessMessageLabel.Text = string.Empty;
            CandidateAssessorSummary_topErrorMessageLabel.Text = string.Empty;
            CandidateAssessorSummary_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// method that checks and sets expanded or restored state for the 
        /// table.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            // Maintain the expand/restore gridview.
            if (!Utility.IsNullOrEmpty(CandidateAssessorSummary_isMaximizedHiddenField.Value) &&
                CandidateAssessorSummary_isMaximizedHiddenField.Value == "Y")
            {
                CandidateAssessorSummary_searchCriteriasDiv.Style["display"] = "none";
                CandidateAssessorSummary_searchResultsUpSpan.Style["display"] = "block";
                CandidateAssessorSummary_searchResultsDownSpan.Style["display"] = "none";
                CandidateAssessorSummary_tableDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                CandidateAssessorSummary_searchCriteriasDiv.Style["display"] = "block";
                CandidateAssessorSummary_searchResultsUpSpan.Style["display"] = "none";
                CandidateAssessorSummary_searchResultsDownSpan.Style["display"] = "block";
                CandidateAssessorSummary_tableDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// Method that loads the header information.
        /// </summary>
        private void LoadHeader()
        {
            if (Utility.IsNullOrEmpty(candidateSessionID) || attemptID == 0)
                return;

            // Get candidate interview session detail.
            CandidateInterviewSessionDetail sessionDetail = new InterviewSchedulerBLManager().
                GetCandidateInterviewSessionDetail(candidateSessionID, attemptID);
            ViewState["INTERVIEW_SESSION_DETAIL"] = sessionDetail;
            if (sessionDetail == null)
                return;

            CandidateAssessorSummary_interviewNameValueLabel.Text = sessionDetail.InterviewTestName;
            CandidateAssessorSummary_interviewDescriptionValueLabel.Text = sessionDetail.InterviewTestDescription;

            if (sessionDetail.PositionProfileID != 0)
            {
                CandidateAssessorSummary_positionProfileValueLinkButton.Visible = true;
                CandidateAssessorSummary_positionProfileValueLinkButton.Text = sessionDetail.PositionProfileName;

                // Assign handler to view position profile page.
                CandidateAssessorSummary_positionProfileValueLinkButton.Attributes.Add("onclick",
                   "javascript:return ShowViewPositionProfile('" + sessionDetail.PositionProfileID + "');");

                // Get the client details associated with position profile id
                ClientInformation clientInfo =
                    new ClientBLManager().GetClientInfoPositionProfileID(sessionDetail.PositionProfileID);

                if (!Utility.IsNullOrEmpty(clientInfo))
                {
                    // Get client departments
                    new ClientInfoDataManager().GetClientDepartmentContactInfo(CandidateAssessorSummary_showClientDepartmentsLabel,
                        clientInfo.ClientDepartments, "CD", true);
                    // Get client contacts
                    new ClientInfoDataManager().GetClientDepartmentContactInfo(CandidateAssessorSummary_showClientContactsLabel,
                        clientInfo.ClientContacts, "CC", true);
                }
            }
            if (sessionDetail.ClientID != 0)
            {
                CandidateAssessorSummary_clientNameValueLinkButton.Visible = true;

                CandidateAssessorSummary_clientNameValueLinkButton.Text = sessionDetail.ClientName;

                // Assign handler to view client detail page.
                CandidateAssessorSummary_clientNameValueLinkButton.Attributes.Add("onclick",
                   "javascript:return ShowViewClient('" + sessionDetail.ClientID + "');");
            }

            CandidateAssessorSummary_candidateNameValueLinkButton.Text = sessionDetail.CandidateName;

            // Assign handler to candidate profile page.
            CandidateAssessorSummary_candidateNameValueLinkButton.Attributes.Add("onclick",
                    "javascript:return OpenViewCandidateProfilePopup('" + sessionDetail.CandidateInformationID + "');");

            ViewState["CandidateID"] = sessionDetail.CandidateInformationID;

            // Load candidate image.
            CandidateAssessorSummary_candidateImage.ImageUrl =
                "~/Common/CandidateImageHandler.ashx?source=CAS_PAGE&candidateid=" + 
                sessionDetail.CandidateInformationID;
        }

        /// <summary>
        /// Method that constructs and load the table.
        /// </summary>
        private void LoadTable()
        {
            // Clear error message.
            CandidateAssessorSummary_topErrorMessageLabel.Text = string.Empty;
            CandidateAssessorSummary_bottomErrorMessageLabel.Text = string.Empty;

            if (Utility.IsNullOrEmpty(candidateSessionID) || attemptID == 0)
                return;

            // Get rating summary.
            DataSet rawData = new AssessmentSummaryBLManager().
                GetRatingSummary(candidateSessionID, attemptID);

            // Check if data set is empty or table count is invalid.
            if (rawData == null || rawData.Tables.Count != 7)
            {
                base.ShowMessage(CandidateAssessorSummary_topErrorMessageLabel,
                    CandidateAssessorSummary_bottomErrorMessageLabel, "No assessor summary data found to display");
                return;
            }

            // Check if all tables from index 1 to 6 is empty. This indicates
            // empty data set.
            if (rawData.Tables[0].Rows.Count == 0 &&
                rawData.Tables[1].Rows.Count == 0 &&
                rawData.Tables[2].Rows.Count == 0 &&
                rawData.Tables[3].Rows.Count == 0 &&
                rawData.Tables[4].Rows.Count == 0)
            {
                base.ShowMessage(CandidateAssessorSummary_topErrorMessageLabel,
                    CandidateAssessorSummary_bottomErrorMessageLabel, "No assessor summary data found to display");
                return;
            }

            decimal totalScore = 0m;
            decimal totalWeightedScore = 0m;

            // Get rating summary table.
            DataTable table = new DataManager().GetRatingSummaryTable(rawData, out totalScore, out totalWeightedScore);
            ViewState["AssessmentTable"] = table;

            // Assign total weighted score.
            CandidateAssessorSummary_totalWeightedScoreValueLabel.Text = string.Format("{0:N2}%", totalWeightedScore);

            // Assign rating summary table.
            CandidateAssessorSummary_tablePlaceHolder.Controls.Clear();
            CandidateAssessorSummary_tablePlaceHolder.Controls.Add(GetTable(table));
        }

        /// <summary>
        /// Method that retrieves the table that comprises of the header and
        /// rows.
        /// </summary>
        /// <param name="table">
        /// A <see cref=""/> that holds the data for the table to be 
        /// constructed.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the table.
        /// </returns>
        private Table GetTable(DataTable table)
        {
            Table htmlTable = new Table();

            htmlTable.CssClass = "candidate_assessor_summary_table";
            htmlTable.CellPadding = 0;
            htmlTable.CellSpacing = 0;

            // Create header.
            TableRow headerRow = new TableRow();

            // Add the header row cells.
            headerRow.Cells.AddRange(GetTableHeaderCells(table.Columns));

            // Add the header row to the table.
            htmlTable.Rows.Add(headerRow);

            // Create rows.
            foreach (DataRow row in table.Rows)
            {
                TableRow tableRow = new TableRow();

                // Create row.
                tableRow.Cells.AddRange(GetTableRowCells(row, table.Columns.Count - 1));

                // Add the row to the table.
                htmlTable.Rows.Add(tableRow);
            }

            return htmlTable;
        }

        /// <summary>
        /// Method that retrieves the tables header that comprises of the 
        /// header cells collection.
        /// </summary>
        /// <param name="columns">
        /// A <see cref="DataColumnCollection"/> that holds the data for the 
        /// columns to be constructed.
        /// </param>
        /// <returns>
        /// An array of <see cref="TableHeaderCell"/> that holds the table 
        /// header row.
        /// </returns>
        private TableHeaderCell[] GetTableHeaderCells(DataColumnCollection columns)
        {
            TableHeaderCell[] tableHeaderCells = new TableHeaderCell[columns.Count - 1];

            int accessorColumnWidth = 80 / (tableHeaderCells.Length - 2);

            for (int columnIndex = 0; columnIndex < tableHeaderCells.Length; columnIndex++)
            {
                tableHeaderCells[columnIndex] = new TableHeaderCell();
                tableHeaderCells[columnIndex].VerticalAlign = VerticalAlign.Middle;
                tableHeaderCells[columnIndex].HorizontalAlign = HorizontalAlign.Center;

                if (columnIndex == 0)
                {
                    // Title column.
                    tableHeaderCells[columnIndex].Width = new Unit(350, UnitType.Pixel);
                    tableHeaderCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_title_header";
                    tableHeaderCells[columnIndex].Text = "Subject/Question";
                }
                else if (columnIndex == tableHeaderCells.Length - 1)
                {
                    // Weighted subject score column.
                    tableHeaderCells[columnIndex].Width = new Unit(100, UnitType.Pixel);
                    tableHeaderCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_weighted_subject_score_header";
                    tableHeaderCells[columnIndex].Text = columns[columnIndex].Caption;
                }
                else if (columnIndex == tableHeaderCells.Length - 2)
                {
                    // Weight column.
                    tableHeaderCells[columnIndex].Width = new Unit(100, UnitType.Pixel);
                    tableHeaderCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_weight_header";
                    tableHeaderCells[columnIndex].Text = columns[columnIndex].Caption;
                }
                else if (columnIndex == tableHeaderCells.Length - 3)
                {
                    // Score.
                    tableHeaderCells[columnIndex].Width = new Unit(100, UnitType.Pixel);
                    tableHeaderCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_score_header";
                    tableHeaderCells[columnIndex].Text = columns[columnIndex].Caption;
                }
                else
                {
                    // Assessor column.
                    tableHeaderCells[columnIndex].Width = new Unit(100, UnitType.Pixel);
                    tableHeaderCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_assessor_header";

                    LinkButton linkButton = new LinkButton();
                    linkButton.CssClass = "candidate_assessor_summary_table_cell_assessor_header_link_button";
                    linkButton.Text = columns[columnIndex].Caption;
                    

                    if (!Utility.IsNullOrEmpty(columns[columnIndex].ColumnName) &&
                      Convert.ToInt32(columns[columnIndex].ColumnName) >= 100000)
                    {
                        linkButton.ToolTip = "Click here to view external assessor profile";
                        linkButton.Attributes.Add("onclick", "javascript:return ShowExternalAssessorProfile('" + columns[columnIndex].ColumnName + "','" + CandidateAssessorSummary_convertToRegisteredUserButton.ClientID + "');");
                    }
                    else
                    {
                        linkButton.ToolTip = "Click here to view assessor profile";
                        linkButton.Attributes.Add("onclick", "javascript:return ShowAssessorProfile('" + columns[columnIndex].ColumnName + "');");
                    }

                    // Add the link to the cell.
                    tableHeaderCells[columnIndex].Controls.Add(linkButton);
                }
            }

            return tableHeaderCells;
        }

        /// <summary>
        /// Method that retrieves the tables row that comprises of the cells
        /// collection.
        /// </summary>
        /// <param name="row">
        /// A <see cref="DataRow"/> that holds data for the row to be
        /// constructed.
        /// </param>
        /// <param name="columnsCount">
        /// A <see cref="int"/> that holds the columns count.
        /// </param>
        /// <returns>
        /// An array of <see cref="TableCell"/> that holds the table row.
        /// </returns>
        private TableCell[] GetTableRowCells(DataRow row, int columnsCount)
        {
            TableCell[] tableCells = new TableCell[columnsCount];

            for (int columnIndex = 0; columnIndex < columnsCount; columnIndex++)
            {
                tableCells[columnIndex] = new TableCell();

                if (columnIndex == 0)
                {
                    if (row["FLAG"].ToString().Trim().ToUpper() == "S")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_subject";
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "Q")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_question";
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "O")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_overall_summary";
                }
                else if (columnIndex == columnsCount - 1)
                {
                    // Assign weighted subject score column.
                    if (row["FLAG"].ToString().Trim().ToUpper() == "S")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_summary";
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "Q")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_summary";
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "O")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_overall_summary";
                }
                else if (columnIndex == columnsCount - 2)
                {
                    // Assign weight column.
                    if (row["FLAG"].ToString().Trim().ToUpper() == "S")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_summary";
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "Q")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_summary";
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "O")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_overall_summary";
                }
                else if (columnIndex == columnsCount - 3)
                {
                    // Score.
                    if (row["FLAG"].ToString().Trim().ToUpper() == "S")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_summary";
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "Q")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_summary";
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "O")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_overall_summary";
                }
                else
                {
                    // Assessor column.
                    if (row["FLAG"].ToString().Trim().ToUpper() == "S")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_assessor";
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "Q")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_assessor";
                    else if (row["FLAG"].ToString().Trim().ToUpper() == "O")
                        tableCells[columnIndex].CssClass = "candidate_assessor_summary_table_cell_overall_summary";
                }

                tableCells[columnIndex].VerticalAlign = VerticalAlign.Middle;
                tableCells[columnIndex].HorizontalAlign = HorizontalAlign.Left;

                if (columnIndex == 0)
                {
                    // Question column.
                    if (Utility.IsNullOrEmpty(row[columnIndex]))
                    {
                        tableCells[columnIndex].Text = string.Empty;
                        tableCells[columnIndex].ToolTip = string.Empty;
                    }
                    else
                    {
                        // Check if the row is a question.
                        if (row["FLAG"].ToString().Trim().ToUpper() == "Q")
                        {
                            // Construct question and question key
                            string question = row[columnIndex].ToString().Trim();
                            string questionKey = null;

                            int dollarIndex = row[columnIndex].ToString().Trim().LastIndexOf('$');
                            if (dollarIndex != -1)
                            {
                                try
                                {
                                    question = row[columnIndex].ToString().Trim().Substring(0, dollarIndex);
                                    questionKey = row[columnIndex].ToString().Trim().Substring(dollarIndex + 1);
                                }
                                catch (Exception exp)
                                {
                                    Logger.ExceptionLog(exp);
                                }
                            }

                            LinkButton linkButton = new LinkButton();
                            linkButton.CssClass = "candidate_assessor_summary_table_cell_question_link_button";
                            linkButton.Text = TrimContent(question, Convert.ToInt32(ConfigurationManager.AppSettings["QUESTION_DISPLAY_LENGTH"]));
                            linkButton.ToolTip = question;
                            linkButton.Attributes.Add("onclick", "javascript:return ShowViewInterviewQuestion('" + questionKey + "');");

                            // Add the link to the cell.
                            tableCells[columnIndex].Controls.Add(linkButton);
                        }
                        else
                        {
                            tableCells[columnIndex].Text = row[columnIndex].ToString().Trim();
                            tableCells[columnIndex].ToolTip = row[columnIndex].ToString().Trim();
                        }
                    }
                }
                else if (columnIndex == columnsCount - 1)
                {
                    // Weighted subject score column.
                    if (!Utility.IsNullOrEmpty(row[columnIndex]))
                    {
                        // Assign values only for subject lines.
                        if (row["FLAG"].ToString().Trim().ToUpper() == "S")
                        {
                            decimal value = 0m;
                            decimal.TryParse(row[columnIndex].ToString().Trim(), out value);
                            tableCells[columnIndex].HorizontalAlign = HorizontalAlign.Center;
                            tableCells[columnIndex].Text = string.Format("{0:N2}%", value);
                        }

                        // Assign total weighted subject score.
                        if (row["FLAG"].ToString().Trim().ToUpper() == "O")
                        {
                            decimal value = 0m;
                            decimal.TryParse(row[columnIndex].ToString().Trim(), out value);
                            tableCells[columnIndex].HorizontalAlign = HorizontalAlign.Center;
                            tableCells[columnIndex].Text = string.Format("{0:N2}%", value);
                        }
                    }
                }
                else if (columnIndex == columnsCount - 2)
                {
                    // Weight column.

                    // Show caption.
                    if (row["FLAG"].ToString().Trim().ToUpper() == "O")
                    {
                        tableCells[columnIndex].Text = "Total Weighted Score";
                        tableCells[columnIndex].HorizontalAlign = HorizontalAlign.Right;
                    }

                    if (!Utility.IsNullOrEmpty(row[columnIndex]))
                    {
                        // Assign weight.
                        if (row["FLAG"].ToString().Trim().ToUpper() == "Q")
                        {
                            int value = 0;
                            Int32.TryParse(row[columnIndex].ToString().Trim(), out value);
                            tableCells[columnIndex].HorizontalAlign = HorizontalAlign.Center;

                            if (value == 0)
                                tableCells[columnIndex].Text = string.Empty;
                            else
                                tableCells[columnIndex].Text = value.ToString();
                        }
                    }
                }
                else if (columnIndex == columnsCount - 3)
                {
                    // Score.

                    // Show caption.
                    if (row["FLAG"].ToString().Trim().ToUpper() == "O")
                    {
                        tableCells[columnIndex].HorizontalAlign = HorizontalAlign.Right;
                    }

                    if (!Utility.IsNullOrEmpty(row[columnIndex]))
                    {
                        // Assign values only for question lines.
                        if (row["FLAG"].ToString().Trim().ToUpper() == "Q")
                        {
                            decimal value = 0m;
                            decimal.TryParse(row[columnIndex].ToString().Trim(), out value);
                            tableCells[columnIndex].HorizontalAlign = HorizontalAlign.Center;

                            if (value == -01m)
                                tableCells[columnIndex].Text = string.Empty;
                            else
                                tableCells[columnIndex].Text = string.Format("{0:N2}%", value);
                        }
                    }
                }
                else
                {
                    if (!Utility.IsNullOrEmpty(row[columnIndex]))
                    {
                        // Assign values only for question lines.
                        if (row["FLAG"].ToString().Trim().ToUpper() == "Q")
                        {
                            decimal value = 0m;
                            decimal.TryParse(row[columnIndex].ToString().Trim(), out value);

                            if (value == -1)
                            {
                                tableCells[columnIndex].Text = string.Empty;
                            }
                            else
                            {
                                tableCells[columnIndex].HorizontalAlign = HorizontalAlign.Center;
                                tableCells[columnIndex].Text = string.Format("{0:N2}%", value);
                            }
                        }
                    }
                }
            }

            return tableCells;
        }

        /// <summary>
        /// Method to clear the error message controls
        /// </summary>
        private void ClearControls()
        {
            CandidateAssessorSummary_topSuccessMessageLabel.Text=string.Empty;
            CandidateAssessorSummary_topErrorMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Overridden method that loads default values into controls.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Overridden Methods
    }
}