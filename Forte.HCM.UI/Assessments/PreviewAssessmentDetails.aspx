﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreviewAssessmentDetails.aspx.cs"
    Inherits="Forte.HCM.UI.Assessments.PreviewAssessmentDetails" MasterPageFile="~/MasterPages/InterviewMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="PreviewAssessmentDetails_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="PreviewAssessmentDetails_headerLiteral" runat="server" Text="Preview Assessment Details"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        &nbsp;
                                        <asp:Button ID="PreviewAssessmentDetails_refreshButton" runat="server" SkinID="sknButtonId"
                                            Text="Refresh" />&nbsp; &nbsp;<asp:LinkButton ID="PreviewAssessmentDetails_topResetLinkButton"
                                                runat="server" Text="Reset" SkinID="sknActionLinkButton"></asp:LinkButton>
                                        &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="PreviewAssessmentDetails_topCancelLinkButton"
                                            runat="server" Text="Cancel" SkinID="sknActionLinkButton" 
                                            onclick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="PreviewAssessmentDetails_messagesUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="PreviewAssessmentDetails_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="PreviewAssessmentDetails_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" border="0" cellspacing="3" cellpadding="0">
                    <tr>
                        <td style="width: 33%" valign="top" >
                            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td class="header_bg">
                                                    <asp:Literal ID="PreviewAssessmentDetails_candidateDetailsLiteral" runat="server"
                                                        Text="Candidate Details"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="panel_body_bg">
                                                    <asp:UpdatePanel runat="server" ID="PreviewAssessmentDetails_candidateDetailsUpdatePanel">
                                                        <ContentTemplate>
                                                            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="PreviewAssessmentDetails_candidateNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                            Text="Candidate Name"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="PreviewAssessmentDetails_candidateNameLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="PreviewAssessmentDetails_clientNameLabel" runat="server" Text="Client Name"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="PreviewAssessmentDetails_clientNameLinkButton" runat="server"
                                                                            Text="Client Name" CssClass="link_btn"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="PreviewAssessmentDetails_positionProfileNameLabel" runat="server"
                                                                            Text="Position Profile" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="PreviewAssessmentDetails_positionProfileLinkButton" runat="server"
                                                                            Text="Position Profile" CssClass="link_btn" ></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                             
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <div style="height: 440px; vertical-align: middle">
                                                                            <table>
                                                                                <tr>
                                                                                    <td colspan="2" align="center">
                                                                                        <asp:Image ID="PreviewAssessmentDetails_candidatePhotoImage" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_height_5">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="height: 40px">
                                                                                        <asp:Label ID="PreviewAssessmentDetails_userRatingLabel" runat="server" Text="User Rating"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td >
                                                                                        <ajaxToolKit:Rating runat="server" ID="PreviewAssessmentDetails_userRatingRating"
                                                                                            MaxRating="10" CurrentRating="0" CssClass="rating_star" StarCssClass="rating_item"
                                                                                            WaitingStarCssClass="rating_saved" FilledStarCssClass="rating_filled" EmptyStarCssClass="rating_empty">
                                                                                        </ajaxToolKit:Rating>
                                                                                    </td>
                                                                                </tr>
                                                                                   <tr>
                                                                    <td>
                                                                        <asp:Label ID="PreviewAssessmentDetails_completedDateLabel" runat="server" Text="Assessment<br/> Completed On"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 165px">
                                                                            <tr>
                                                                                <td style="width: 37%">
                                                                                    <asp:TextBox ID="PreviewAssessmentDetails_completedDateTextBox" runat="server" MaxLength="10" Text=""
                                                                                        AutoCompleteType="None" ></asp:TextBox>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:ImageButton ID="PreviewAssessmentDetails_completedDateImageButton" SkinID="sknCalendarImageButton"
                                                                                        runat="server" ImageAlign="Middle" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <ajaxToolKit:MaskedEditExtender ID="PreviewAssessmentDetails_completedDateMaskedEditExtender"
                                                                            runat="server" TargetControlID="PreviewAssessmentDetails_completedDateTextBox"
                                                                            Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                            OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                            ErrorTooltipEnabled="True" />
                                                                        <ajaxToolKit:CalendarExtender ID="PreviewAssessmentDetails_completedDateCalendarExtender"
                                                                            runat="server" TargetControlID="PreviewAssessmentDetails_completedDateTextBox"
                                                                            CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="PreviewAssessmentDetails_completedDateImageButton" />
                                                                    </td>
                                                                </tr>

                                                                                <caption>
                                                                                  
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="PreviewAssessmentDetails_commentsLabel" runat="server" 
                                                                                                SkinID="sknLabelFieldHeaderText" Text="Comment"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="PreviewAssessmentDetails_commentsTextbox" runat="server" 
                                                                                                Height="70px" TextMode="MultiLine" Width="201px" Wrap="true"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_height_8">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td align="right">
                                                                                            <asp:UpdatePanel ID="PreviewAssessmentDetails_SaveSubmitUpdatePanel" 
                                                                                                runat="server">
                                                                                                <ContentTemplate>
                                                                                                    <asp:Button ID="PreviewAssessmentDetails_saveButton" runat="server" 
                                                                                                        OnClick="PreviewAssessmentDetails_saveButton_Click" SkinID="sknButtonId" 
                                                                                                        Text="Save" />
                                                                                                    <asp:Button ID="PreviewAssessmentDetails_submitButton" runat="server" 
                                                                                                        OnClick="PreviewAssessmentDetails_submitButton_Click" SkinID="sknButtonId" 
                                                                                                        Text="Submit" />
                                                                                                </ContentTemplate>
                                                                                            </asp:UpdatePanel>
                                                                                        </td>
                                                                                    </tr>
                                                                                </caption>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 33%" valign="top">
                            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="header_bg">
                                                    <asp:Literal ID="ViewContributorSummary_questionSummaryLiteral" runat="server" Text="Rating Summary"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="panel_body_bg" valign="top">
                                                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td valign="top">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td>
                                                                <asp:Label ID="PreviewAssessmentDetails_OverallScoreLabel" runat="server" Text="Total Score"
                                                                    SkinID="sknLabelText"></asp:Label></td>
                                                                        <td style="width:8px"></td>
                                                                        <td>
                                                                <asp:Label ID="PreviewAssessmentDetails_OverallWeightageScoreLabel" runat="server" Text="Total Weighted Score"
                                                                    SkinID="sknLabelText"></asp:Label></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 40%">
                                                                <asp:Label ID="PreviewAssessmentDetails_overallRatingLabel" runat="server" Text="Overall Rating"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td valign="top" align="center" style="width:60%">
                                                                 <table cellpadding="0" cellspacing="0" border="0"
                                                                 style="width:100%">
                                                                        <tr>
                                                                            <td align="justify" style="width:40%">&nbsp;
                                                                                <asp:Label ID="PreviewAssessmentDetails_overallScore" runat="server" Text=""
                                                                                    SkinID="sknLabelFieldText"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                                            </td>
                                                                            <td style="width:20%"></td>
                                                                            <td align="left" style="width:40%">
                                                                            <asp:Label ID="PreviewAssessmentDetails_OverallWeightageScore" runat="server" Text=""
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        </tr>
                                                                    </table>
                                                                <%--<asp:Image ID="PreviewAssessmentDetails_overallRatingImage" runat="server" />
                                                                <asp:Label ID="PreviewAssessmentDetails_overallAssessorssujectRatingLabel" runat="server" Text=""></asp:Label>--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:UpdatePanel ID="PreviewAssessmentDetails_overallsubjectRatingUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <div style="width: 100%; overflow: auto; height: 100px;" runat="server" id="PreviewAssessmentDetails_overallsubjectRatingDiv">
                                                                            <asp:GridView ID="PreviewAssessmentDetails_overallsubjectRatingGridview" runat="server"
                                                                                AutoGenerateColumns="False" SkinID="sknNewGridView" ShowHeader="false" OnRowDataBound="PreviewAssessmentDetails_overallsubjectRatingGridview_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Subject" ItemStyle-Width="35%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="PreviewAssessmentDetails_overallSubjectGridViewLabel" runat="server"
                                                                                                SkinID="sknLabelFieldHeaderText" Text='<%# Eval("SUBJECT_NAME") %>'></asp:Label>
                                                                                                <asp:HiddenField ID="PreviewAssessmentDetails_overallSubjectGridViewHiddenField" Value='<%# Eval("CAT_SUB_ID") %>'
                                                                                                 runat="server" />

                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Rating"  ItemStyle-Width="21%">
                                                                                        <ItemTemplate>
                                                                                            <%--<asp:Image ID="PreviewAssessmentDetails_overallsubjectRatingGridviewImage" runat="server" />--%>
                                                                                                <asp:Label ID="PreviewAssessmentDetails_overallScoreLabel" runat="server"
                                                                                                SkinID="sknLabelFieldText" Text='<%# Eval("OVERALL_SUBJECT_RATING") %>'></asp:Label>

                                                                                            <asp:HiddenField ID="PreviewAssessmentDetails_overallsubjectRatingGridviewHiddenField"
                                                                                                Value='<%# Eval("OVERALL_SUBJECT_RATING") %>' runat="server" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                       <asp:TemplateField HeaderText="OVERALL WEIGHTAGE SCORE"  ItemStyle-Width="20%">
                                                                                        <ItemTemplate>&nbsp;
                                                                                            <asp:Label ID="PreviewAssessmentDetails_overallWeightageScoreLabel" runat="server"
                                                                                                SkinID="sknLabelFieldText" Text='<%# Eval("OVERALL_SUBJECT_WEIGHTAGE_RATING") %>'></asp:Label>

                                                                                            <asp:HiddenField ID="PreviewAssessmentDetails_overallWeightageScoreHiddenField"
                                                                                                Value='<%# Eval("OVERALL_SUBJECT_WEIGHTAGE_RATING") %>' runat="server" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header_bg">
                                        <asp:Literal ID="PreviewAssessmentDetails_assessorDetailsLiteral" runat="server"
                                            Text="Other Assessor Rating"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tab_body_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td>
                                                    <div style="width: 100%; overflow: auto; height: 375px;" runat="server" id="PreviewAssessmentDetails_assessorDetailsDiv">
                                                        <asp:DataList ID="PreviewAssessmentDetails_assessorDetailsDatalist" runat="server"
                                                            RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Flow" OnItemDataBound="PreviewAssessmentDetails_assessorDetailsDatalist_ItemDataBound">
                                                            <ItemTemplate>
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="td_height_5">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="PreviewAssessmentDetails_assessorDetailsDateLable" runat="server"
                                                                                SkinID="sknLabelFieldHeaderText" Text="Date"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="PreviewAssessmentDetails_assessorDetailsDateLableValue" runat="server"
                                                                                SkinID="sknLabelFieldText" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("CompletedOn")))%>'></asp:Label>
                                                                        </td>
                                                                        <td style="width: 40px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="PreviewAssessmentDetails_assessorDetailsAssessorNameLable" runat="server"
                                                                                SkinID="sknLabelFieldHeaderText" Text="Assessor Name"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="PreviewAssessmentDetails_assessorDetailsAssessorNameLableValue" runat="server"
                                                                                SkinID="sknLabelFieldText" Text='<%# Eval("FirstName") %>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="PreviewAssessmentDetails_assessorRatingLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                Text="Rating"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Image ID="PreviewAssessmentDetails_assessorDetailsRatingImage" runat="server" />
                                                                            <asp:HiddenField ID="PreviewAssessmentDetails_assessorDetailsRatingHiddenField" runat="server"
                                                                                Value='<%# Eval("assessorRating") %>' />
                                                                            <asp:Label ID="PreviewAssessmentDetails_assessorDetailsRatingLabel" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <asp:Label ID="PreviewAssessmentDetails_assessorDetailsCommentsLabel" runat="server"
                                                                                Text="Comment" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 50%">
                                                                            <asp:Label ID="PreviewAssessmentDetails_assessorDetailsCommentsLabelValue" runat="server"
                                                                                Text='<%# Eval("Comments") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 33%" valign="top">
                            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td class="header_bg">
                                                    <asp:Literal ID="PreviewAssessmentDetails_assessorDetailsQuestionsLiteral" runat="server"
                                                        Text="Questions"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="panel_body_bg" valign="top">
                                                    <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 55%" align="right">
                                                                <asp:Label ID="PreviewAssessmentDetails_questiongroupByLabel" runat="server" Text="Group By"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td align="right" style="width: 25%">
                                                                <asp:UpdatePanel ID="PreviewAssessmentDetails_questiongroupByDropDownListUpdatePanel"
                                                                    runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:DropDownList ID="PreviewAssessmentDetails_questiongroupByDropDownList" runat="server"
                                                                            AutoPostBack="true" OnSelectedIndexChanged="PreviewAssessmentDetails_questiongroupByDropDownList_SelectedIndexChanged">
                                                                            <asp:ListItem Text="--Select--"></asp:ListItem>
                                                                            <asp:ListItem Text="Question" Value="Q"></asp:ListItem>
                                                                            <asp:ListItem Text="Subject" Value="S"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:UpdatePanel ID="PreviewAssessmentDetails_questiongroupByUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <div style="width: 100%; overflow: auto; height: 400px;" runat="server" id="PreviewAssessmentDetails_questionAreaDiv">
                                                                            <asp:GridView ID="PreviewAssessmentDetails_questionAreaDataList" runat="server" AutoGenerateColumns="False"
                                                                                SkinID="sknNewGridView" ShowHeader="false" OnRowDataBound="PreviewAssessmentDetails_questionAreaDataList_RowDataBound">
                                                                                <Columns>
                                                                                
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                                                <tr>
                                                                                                    <td valign="middle" style="width: 15%">
                                                                                                        <asp:Image ID="PreviewAssessmentDetails_questionImage" runat="server" SkinID="sknQuestionImage"
                                                                                                            ToolTip="Question" />
                                                                                                        <asp:Label ID="PreviewAssessmentDetails_rowNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                                                            runat="server">
                                                                                                        </asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 50%" align="left">
                                                                                                        <div class="label_multi_field_text">
                                                                                                            <asp:LinkButton ID="PreviewAssessmentDetails_questionAreaQuestionsLabel" runat="server"
                                                                                                                Text='<%# TrimContent(Eval("Question").ToString(),25) %>' 
                                                                                                                OnClick="PreviewAssessmentDetails_questionAreaQuestionsLabel_Click"></asp:LinkButton>
                                                                                                            <asp:HiddenField ID="PreviewAssessmentDetails_questionAreaQuestionIDHiddenField"
                                                                                                                runat="server" Value='<%# Eval("TestQuestionID") %>' />
                                                                                                            <asp:HiddenField ID="PreviewAssessmentDetails_questionAreaQuestionKeyHiddenField"
                                                                                                                runat="server" Value='<%# Eval("QuestionKey") %>' />
                                                                                                        </div>
                                                                                                    </td>
                                                                                                    <td style="width: 25%" align="left">
                                                                                                        <asp:Label ID="PreviewAssessmentDetails_questionAreaSubjectLabel" runat="server"
                                                                                                            Text='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                                        <asp:HiddenField ID="PreviewAssessmentDetails_questionAreaSubjectHidddenField" runat="server"
                                                                                                            Value='<%# Eval("SubjectID") %>' />
                                                                                                    </td>
                                                                                                    <td style="width: 10%" align="left">
                                                                                                        <asp:Label ID="PreviewAssessmentDetails_questionAreaRatingLabel" runat="server" Text='<%# Eval("Rating") %>'>
                                                                                                        </asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                     <asp:TemplateField>
                                                                                            <ItemTemplate> 
                                                                                                <asp:Panel ID="CreateManualTestWithoutAdaptive_hoverPanel" CssClass="table_outline_bg"
                                                                                                    runat="server" Width="550px" Height="100px" style="margin-left:-400px; overflow:auto">
                                                                                                    <table width="100%" border="0" cellspacing="2" cellpadding="3">
                                                                                                        <tr>
                                                                                                           <th class="popup_question_icon" >
                                                                                                                <div style="height:72px;overflow:auto">
                                                                                                                <%# Eval("Question") %> 
                                                                                                                </div>
                                                                                                            </th> 
                                                                                                        </tr> 
                                                                                                    </table>
                                                                                                </asp:Panel>
                                                                                                <ajaxToolKit:HoverMenuExtender ID="CreateManualTestWithoutAdaptive_hoverMenuExtender"
                                                                                                    runat="server" PopupControlID="CreateManualTestWithoutAdaptive_hoverPanel" PopupPosition="Bottom"
                                                                                                    HoverCssClass="popupHover" TargetControlID="PreviewAssessmentDetails_questionAreaQuestionsLabel"
                                                                                                    PopDelay="50" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                </Columns>
                                                                                <%--<HeaderStyle CssClass="grid_header" />--%>
                                                                                <AlternatingRowStyle CssClass="grid_001" />
                                                                                <RowStyle CssClass="grid" />
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div style="vertical-align:top">
                                                                         <table cellpadding="0" cellspacing="0"  border="0">
                                                                            <tr>
                                                                                <td  width="20" class=RatingEditable></td>
                                                                                <td width="140"  class=label_field_bold_text>&nbsp;&nbsp; Rating Applicable
                                                                                     </td>
                                                                            </tr>
                                                                            <tr><td height="3px"></td></tr>
                                                                            <tr>
                                                                                <td  width="20" class=RatingFinished></td>
                                                                                <td width="140"  class=label_field_bold_text>&nbsp;&nbsp; Rating Completed
                                                                             </td>
                                                                            </tr>
                                                                            <tr><td height="3px"></td></tr>
                                                                            <tr>
                                                                                <td  width="20" class=RatingNotAllowed></td>
                                                                                <td width="160"  class=label_field_bold_text>&nbsp;&nbsp; Rating Not Allowed
                                                                             </td>
                                                                            </tr>
                                                                         </table>
                                                                        </div>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" Text="Reset" SkinID="sknActionLinkButton"></asp:LinkButton>
                                        &nbsp;|&nbsp;&nbsp;<asp:LinkButton ID="LinkButton2" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
