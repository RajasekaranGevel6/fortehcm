﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CandidateAssessorSummary.aspx.cs"
    Inherits="Forte.HCM.UI.Assessments.CandidateAssessorSummary" MasterPageFile="~/MasterPages/InterviewMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<%@ Register Src="~/CommonControls/ConfirmInterviewScoreMsgControl.ascx" TagName="ConfirmInterviewScoreMsgControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<asp:Content ID="CandidateAssessorSummary_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="CandidateAssessorSummary_headerLiteral" runat="server" Text="Candidate's Interview Assessment Summary"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="CandidateAssessorSummary_showPublishUrlUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <div>
                                                    <asp:ImageButton ID="CandidateAssessorSummary_topPublishImageButton" runat="server"
                                                        ImageUrl="~/App_Themes/DefaultTheme/Images/publish.png" ToolTip="Publish Candidate Interview Response"
                                                        Visible="true" Width="18px" Height="18px" OnClick="CandidateAssessorSummary_publishImageButton_Click" />
                                                </div>
                                                <div style="float: left;">
                                                    <asp:Panel ID="CandidateAssessorSummary_emailConfirmationPanel" runat="server" Style="display: none;
                                                        height: 254px;" CssClass="popupcontrol_confirm_publish_candidate_interview_response">
                                                        <div id="CandidateAssessorSummary_emailConfirmationDiv" style="display: none">
                                                            <asp:Button ID="CandidateAssessorSummary_emailConfirmation_hiddenButton" runat="server" />
                                                        </div>
                                                        <uc1:ConfirmInterviewScoreMsgControl ID="CandidateAssessorSummary_emailConfirmation_ConfirmMsgControl"
                                                            runat="server" Title="Publish Candidate Interview Response" Type="InterviewScoreConfirmType" />
                                                    </asp:Panel>
                                                    <ajaxToolKit:ModalPopupExtender ID="CandidateAssessorSummary_emailConfirmation_ModalPopupExtender"
                                                        BehaviorID="CandidateAssessorSummary_emailConfirmation_ModalPopupExtender" runat="server"
                                                        PopupControlID="CandidateAssessorSummary_emailConfirmationPanel" TargetControlID="CandidateAssessorSummary_emailConfirmation_hiddenButton"
                                                        BackgroundCssClass="modalBackground">
                                                    </ajaxToolKit:ModalPopupExtender>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <div style="display: none">
                                            <asp:Button ID="CandidateAssessorSummary_convertToRegisteredUserButton" OnClick="CandidateAssessorSummary_convertToRegisteredUserButton_Click"
                                                runat="server" />
                                        </div>
                                        <asp:Button ID="CandidateAssessorSummary_topRefreshButton" runat="server" Text="Refresh"
                                            ToolTip="Click here to refresh" SkinID="sknButtonId" OnClick="CandidateAssessorSummary_refreshButton_Click" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:Button ID="CandidateAssessorSummary_topEmailButton" runat="server" Text="Email"
                                            ToolTip="Click here to email" SkinID="sknButtonId" OnClick="CandidateAssessorSummary_emailButton_Click" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:Button ID="CandidateAssessorSummary_topDownloadButton" runat="server"
                                            Text="Download" ToolTip="Click here to download" SkinID="sknButtonId" OnClick="CandidateAssessorSummary_downloadButton_Click" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:LinkButton ID="CandidateAssessorSummary_topCancelLinkButton" runat="server"
                                            ToolTip="Click here to go back to parent" Text="Cancel" SkinID="sknActionLinkButton"
                                            OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="CandidateAssessorSummary_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="CandidateAssessorSummary_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" valign="middle">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="CandidateAssessorSummary_searchCriteriasDiv" runat="server" style="display: block;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="panel_bg">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="panel_inner_body_bg">
                                                            <tr>
                                                                <td style="width: 15%" rowspan="8" valign="top">
                                                                    <asp:Image runat="server" ID="CandidateAssessorSummary_candidateImage" AlternateText="Photo not available" />
                                                                </td>
                                                                <td style="width: 14%" valign="top">
                                                                    <asp:Label ID="CandidateAssessorSummary_interviewNameHeadLabel" runat="server" Text="Interview Name"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 23%" valign="top">
                                                                    <asp:Label ID="CandidateAssessorSummary_interviewNameValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                </td>
                                                                <td style="width: 13%" rowspan="5" valign="top">
                                                                    <asp:Label ID="CandidateAssessorSummary_interviewDescriptionHeadLabel" runat="server"
                                                                        Text="Interview Description" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td style="width: 29%" rowspan="5" valign="top">
                                                                    <div style="height: 120px; width: 100%; overflow: auto" runat="server" id="CandidateAssessorSummary_interviewDescriptionDiv">
                                                                        <asp:Label ID="CandidateAssessorSummary_interviewDescriptionValueLabel" runat="server"
                                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="CandidateAssessorSummary_positionProfileHeadLabel" runat="server"
                                                                        Text="Position Profile" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:LinkButton ID="CandidateAssessorSummary_positionProfileValueLinkButton" runat="server"
                                                                        SkinID="sknLabelFieldTextLinkButton" Visible="false" ToolTip="Click here to view position profile details"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="CandidateAssessorSummary_clientNameHeadLabel" runat="server" Text="Client"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:LinkButton ID="CandidateAssessorSummary_clientNameValueLinkButton" runat="server"
                                                                        SkinID="sknLabelFieldTextLinkButton" Visible="false" ToolTip="Click here to view client details"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="CandidateAssessorSummary_showClientDepartmentsHeaderLabel" runat="server"
                                                                        Text="Department(s)" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:Label ID="CandidateAssessorSummary_showClientDepartmentsLabel" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="CandidateAssessorSummary_showClientContactsHeaderLabel" runat="server"
                                                                        Text="Contact(s)" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:Label ID="CandidateAssessorSummary_showClientContactsLabel" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="CandidateAssessorSummary_candidateNameHeadLabel" runat="server" Text="Candidate Name"
                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:LinkButton ID="CandidateAssessorSummary_candidateNameValueLinkButton" runat="server"
                                                                        SkinID="sknLabelFieldTextLinkButton" ToolTip="Click here to view candidate profile"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_8">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="CandidateAssessorSummary_totalWeightedScoreHeadLabel" runat="server"
                                                                        Text="Total Weighted Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:Label ID="CandidateAssessorSummary_totalWeightedScoreValueLabel" runat="server"
                                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table>
                                                            <tr>
                                                                <td class="td_height_2">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr id="CandidateAssessorSummary_searchResultsTR" runat="server">
                        <td class="header_bg" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 50%" align="left" class="header_text_bold">
                                        <asp:Literal ID="CandidateAssessorSummary_searchResultsLiteral" runat="server" Text="Assessment Details"></asp:Literal>
                                        &nbsp;<asp:Label ID="CandidateAssessorSummary_sortHelpLabel" runat="server" SkinID="sknLabelText"></asp:Label>
                                    </td>
                                    <td style="width: 50%" align="right">
                                        <span id="CandidateAssessorSummary_searchResultsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="CandidateAssessorSummary_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" /></span>
                                        <span id="CandidateAssessorSummary_searchResultsDownSpan" style="display: block;"
                                            runat="server">
                                            <asp:Image ID="CandidateAssessorSummary_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                        <asp:HiddenField ID="CandidateAssessorSummary_restoreHiddenField" runat="server" />
                                        <asp:HiddenField ID="CandidateAssessorSummary_isMaximizedHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="grid_body_bg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr>
                                    <td align="left" id="CandidateAssessorSummary_tableDiv" runat="server" valign="top">
                                        <%--<div style="width: 100%;" runat="server" id="CandidateAssessorSummary_tableDiv">--%>
                                        <asp:PlaceHolder ID="CandidateAssessorSummary_tablePlaceHolder" runat="server"></asp:PlaceHolder>
                                        <%--</div>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="CandidateAssessorSummary_bottomSuccessMessageLabel" runat="server"
                    SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="CandidateAssessorSummary_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="0" align="right">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="CandidateAssessorSummary_bottomPublishUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton ID="CandidateAssessorSummary_bottomPublishImageButton" runat="server"
                                        ImageUrl="~/App_Themes/DefaultTheme/Images/publish.png" ToolTip="Publish Candidate Interview Response"
                                        Width="18px" Height="18px" OnClick="CandidateAssessorSummary_publishImageButton_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            &nbsp;<asp:Button ID="CandidateAssessorSummary_bottomRefreshButton" runat="server"
                                Text="Refresh" SkinID="sknButtonId" ToolTip="Click here to refresh" OnClick="CandidateAssessorSummary_refreshButton_Click" />
                        </td>
                        <td>
                            &nbsp;<asp:Button ID="CandidateAssessorSummary_bottomEmailButton" runat="server"
                                Text="Email" ToolTip="Click here to email" SkinID="sknButtonId" OnClick="CandidateAssessorSummary_emailButton_Click" />
                        </td>
                        <td>
                            &nbsp;<asp:Button ID="CandidateAssessorSummary_bottomDownloadButton" runat="server"
                                Text="Download" ToolTip="Click here to download" SkinID="sknButtonId" OnClick="CandidateAssessorSummary_downloadButton_Click" />
                        </td>
                        <td>
                            &nbsp;<asp:LinkButton ID="CandidateAssessorSummary_bottomCancelLinkButton" runat="server"
                                ToolTip="Click here to go back to parent" Text="Cancel" SkinID="sknActionLinkButton"
                                OnClick="ParentPageRedirect"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
