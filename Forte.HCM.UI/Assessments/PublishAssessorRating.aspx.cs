#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PublishAssessorRating.aspx.cs
// File that represents the user interface layout and functionalities
// for the PreviewAssessmentDetails page. This page helps in viewing 
// the candidate interview videos and provide rating. Also helps in 
// viewing other assessor rating and consolidated rating. 

#endregion Header

using System;
using System.Data;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Configuration;
using System.Web.UI.HtmlControls;
using Forte.HCM.Utilities;

namespace Forte.HCM.UI.Assessments
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the PublishAssessorRating.aspx.cs page. This page helps in viewing 
    /// the candidate interview videos and provide rating. Also helps in 
    /// viewing other assessor rating and consolidated rating. This class
    /// inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class PublishAssessorRating : PageBase
    {
       
        #region Protected Variables
        /// <summary>
        /// Holding video url
        /// </summary>
        protected string videoURL;
        /// <summary>
        /// Holding question id
        /// </summary>
        protected string questionID;

        
        #endregion Protected Variables

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Candidate Interview Response");
                PublishAssessorRating_topErrorMessageLabel.Text = string.Empty;
                PublishAssessorRating_topSuccessMessageLabel.Text = string.Empty;
                if (!IsPostBack)
                {
                    ViewState["INTERVIEW_KEY"] = null;
                    ViewState["CANDIDATE_SESSION_KEY"] = null;
                    ViewState["ATTEMPT_ID"] = null;
                    ViewState["SHOW_SCORE"] = null;
                    ViewState["POSITION_PROFILE_ID"] = null;

                    string displayType = string.Empty;
                    string scoreCode = string.Empty;
                    string queryString = string.Empty;

                    if (Utility.IsNullOrEmpty(Request.QueryString["r"]))
                    {
                        PublishAssessorRating_candidatePhotoImage.ImageUrl = "~/CandidatePhotos/photo_not_available.jpg";
                        PublishAssessorRating_topErrorMessageLabel.Text = "No data found to display";
                        return;
                    }
                    queryString = Request.QueryString["r"].ToString();
                    InterviewScoreParamDetail interviewScoreParamDetail = null;
                    if (interviewScoreParamDetail == null)
                        interviewScoreParamDetail = new InterviewScoreParamDetail();

                    scoreCode = queryString.Remove(queryString.Length - 1, 1); // Returns the string without last character
                    displayType = queryString.Substring(queryString.Length - 1); // Returns last last character

                    interviewScoreParamDetail = new AssessmentSummaryBLManager().
                        GetCandidateInterviewPublishDetails(scoreCode);

                    if (interviewScoreParamDetail == null)
                    {
                        PublishAssessorRating_candidatePhotoImage.ImageUrl = "~/CandidatePhotos/photo_not_available.jpg";
                        PublishAssessorRating_topErrorMessageLabel.Text = "No data found to display";
                        return;
                    }

                    ViewState["INTERVIEW_KEY"] = interviewScoreParamDetail.InterviewKey;
                    ViewState["CANDIDATE_SESSION_KEY"] = interviewScoreParamDetail.CandidateInterviewSessionKey;
                    ViewState["ATTEMPT_ID"] = interviewScoreParamDetail.AttemptID;
                   

                    if (displayType.ToString().Trim() == "S")
                        ViewState["SHOW_SCORE"] = "Y";
                    else
                        ViewState["SHOW_SCORE"] = "N";
                    
                        
                    ShowInterviewScore();
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PublishAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the question list.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void PublishAssessorRating_questionAreaDataList_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                // Find the row type is DataRow
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    object dr = e.Row.DataItem;

                    decimal _rating = ((Forte.HCM.DataObjects.QuestionDetail)(dr)).Rating;
                    int _questionID = ((Forte.HCM.DataObjects.QuestionDetail)(dr)).QuestionID;

                    e.Row.CssClass = "RatingEditable";
                    
                    // Find a label from the current row.
                    Label PublishAssessorRating_rowNoLabel = (Label)e.Row.FindControl("PublishAssessorRating_rowNoLabel");

                    // Assign the row no to the label.
                    PublishAssessorRating_rowNoLabel.Text = (e.Row.RowIndex + 1).ToString();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PublishAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler to format the score
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PublishAssessorRating_othersOverAllScore_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                decimal _overAllWeightageScore = 0;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField PublishAssessorRating_othersAssessorOverallWeightageRatingGridviewHiddenField = (HiddenField)e.Row.FindControl(
                        "PublishAssessorRating_othersAssessorOverallWeightageRatingGridviewHiddenField");

                    Label PublishAssessorRating_otherAssessorOverallSubjectWeightageRatingLabel = (Label)e.Row.FindControl(
                        "PublishAssessorRating_otherAssessorOverallSubjectWeightageRatingLabel");

                    if (!string.IsNullOrEmpty(Convert.ToString(PublishAssessorRating_othersAssessorOverallWeightageRatingGridviewHiddenField.Value)))
                        _overAllWeightageScore =
                            Convert.ToDecimal(PublishAssessorRating_othersAssessorOverallWeightageRatingGridviewHiddenField.Value);

                    if (_overAllWeightageScore < 0)
                        PublishAssessorRating_otherAssessorOverallSubjectWeightageRatingLabel.Visible = false;
                    else
                    {
                        PublishAssessorRating_otherAssessorOverallSubjectWeightageRatingLabel.Visible = true;
                        PublishAssessorRating_otherAssessorOverallSubjectWeightageRatingLabel.Text = 
                            string.Format("{0}%", string.Format("{0:0.00}", _overAllWeightageScore));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PublishAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Hanlder to format the asssessor sore
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PublishAssessorRating_assessorSubjectRatingGridview_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                decimal _assessorWeightageScore = 0;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    HiddenField PublishAssessorRating_assessorWeightageScoreGridviewHiddenField = (HiddenField)e.Row.FindControl(
                        "PublishAssessorRating_assessorWeightageScoreGridviewHiddenField");

                    Label PublishAssessorRating_assessorWeightageScoreGridviewLabel = (Label)e.Row.FindControl(
                        "PublishAssessorRating_assessorWeightageScoreGridviewLabel");

                    if (!string.IsNullOrEmpty(Convert.ToString(PublishAssessorRating_assessorWeightageScoreGridviewHiddenField.Value)))
                        _assessorWeightageScore =
                            Convert.ToDecimal(PublishAssessorRating_assessorWeightageScoreGridviewHiddenField.Value);

                    if (_assessorWeightageScore < 0)
                        PublishAssessorRating_assessorWeightageScoreGridviewLabel.Visible = false;
                    else
                    {
                        PublishAssessorRating_assessorWeightageScoreGridviewLabel.Visible = true;
                        PublishAssessorRating_assessorWeightageScoreGridviewLabel.Text = 
                            string.Format("{0}%", string.Format("{0:0.00}", _assessorWeightageScore));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PublishAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }
        
        /// <summary>
        /// Handler method that is called when the question link button is 
        /// clicked in the question list.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PublishAssessorRating_questionAreaQuestionsLabel_Click
            (object sender, EventArgs e)
        {
            try
            {
                LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PublishAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when question group by drop down list
        /// item is seleted.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PublishAssessorRating_questiongroupByDropDownList_SelectedIndexChanged
            (object sender, EventArgs e)
        {
            try
            {
                if (PublishAssessorRating_questiongroupByDropDownList.SelectedValue != "--Select--")
                {
                    BindAssessmentSummary(ViewState["CANDIDATE_SESSION_KEY"].ToString(),
                        int.Parse(ViewState["ATTEMPT_ID"].ToString()),
                       Convert.ToChar(PublishAssessorRating_questiongroupByDropDownList.SelectedValue));
                }
                else
                {
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PublishAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the item data bound event is 
        /// fired in the assessor details list.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="DataListItemEventArgs"/> that holds the event data.
        /// </param>
        protected void PublishAssessorRating_assessorDetailsDatalist_ItemDataBound
            (object sender, DataListItemEventArgs e)
        {
            try
            {
                HiddenField PublishAssessorRating_assessorDetailsRatingHiddenField = (HiddenField)e.Item.FindControl
                    ("PublishAssessorRating_assessorDetailsRatingHiddenField");

                Image PublishAssessorRating_assessorDetailsRatingImage = (Image)e.Item.FindControl
                    ("PublishAssessorRating_assessorDetailsRatingImage");

                Label PublishAssessorRating_assessorDetailsRatingLabel = (Label)e.Item.FindControl
                    ("PublishAssessorRating_assessorDetailsRatingLabel");

                if (PublishAssessorRating_assessorDetailsRatingHiddenField.Value == "0")
                {
                    PublishAssessorRating_assessorDetailsRatingImage.Visible = false;
                    PublishAssessorRating_assessorDetailsRatingLabel.Text = "N/R";
                }
                else
                {
                    if (string.IsNullOrEmpty(base.GetSummaryRatingImage(
                        Convert.ToDecimal(PublishAssessorRating_assessorDetailsRatingHiddenField.Value))))
                        PublishAssessorRating_assessorDetailsRatingImage.Visible = false;
                    else
                    {
                        PublishAssessorRating_assessorDetailsRatingImage.Visible = true;
                        PublishAssessorRating_assessorDetailsRatingImage.ImageUrl = base.GetSummaryRatingImage(
                            Convert.ToDecimal(PublishAssessorRating_assessorDetailsRatingHiddenField.Value));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PublishAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event to handle row command events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PublishAssessorRating_questionAreaDataList_RowCommand(object sender, 
            GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "QuestionDetail")
                {
                    LinkButton lnk = (LinkButton)e.CommandSource;
                    HiddenField PublishAssessorRating_questionAreaQuestionIDHiddenField = 
                        (HiddenField)lnk.Parent.FindControl("PublishAssessorRating_questionAreaQuestionIDHiddenField");
                    int testquestionID = Convert.ToInt32(PublishAssessorRating_questionAreaQuestionIDHiddenField.Value);

                    HiddenField PublishAssessorRating_questionAreaQuestionKeyHiddenField = 
                        (HiddenField)lnk.Parent.FindControl("PublishAssessorRating_questionAreaQuestionKeyHiddenField");
                    string questionKey = PublishAssessorRating_questionAreaQuestionKeyHiddenField.Value;

                    LoadCandidateResponse(testquestionID, questionKey);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PublishAssessorRating_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindAssessmentSummary(ViewState["CANDIDATE_SESSION_KEY"].ToString(), int.Parse(ViewState["ATTEMPT_ID"].ToString()), 'Q');
        }


        #endregion Protected Overridden Methods

        #region Private Methods

        /// <summary>
        /// Method to show score list
        /// </summary>
        private void ShowInterviewScore()
        {
            if (ViewState["SHOW_SCORE"].ToString()=="Y")
            {
                PublishAssessorRating_displayOverallScoreDiv.Style["display"] = "block";
                PublishAssessorRating_displayAssessorScoreDiv.Style["display"] = "block";
            }
            else
            {
                PublishAssessorRating_displayOverallScoreDiv.Style["display"] = "none";
                PublishAssessorRating_displayAssessorScoreDiv.Style["display"] = "none";
            }
        }

        /// <summary>
        /// Method that binds the assessment summary.
        /// </summary>
        /// <param name="candidateInterviewKey">
        /// A <see cref="string"/>that holds the candidate interview key.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/>that holds the attempt ID.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="char"/>that holds the sort order.
        /// </param>
        private void BindAssessmentSummary(string candidateInterviewKey,
            int attemptID, char orderBy)
        {
            AssessmentSummary assessmentSummary = new AssessmentSummary();
            assessmentSummary =
                new AssessmentSummaryBLManager().GetAssessorSummaryPublish(candidateInterviewKey, attemptID, orderBy);

            LoadCandidateDetails(assessmentSummary);

            DataSet overallRatingDataset = new AssessmentSummaryBLManager().
               GetRatingSummary(candidateInterviewKey, attemptID);


            if (overallRatingDataset.Tables[0].Rows.Count == 0)
            {
                base.ShowMessage(PublishAssessorRating_topErrorMessageLabel, "");
            }

            decimal _overWeightageScore = 0;
            if (overallRatingDataset.Tables[6].Rows.Count != 0)
                if (!string.IsNullOrEmpty(Convert.ToString(overallRatingDataset.Tables[6].Rows[0]["OVERALL_WEIGHTAGE_SCORE"])))
                    _overWeightageScore =
                        Convert.ToDecimal(overallRatingDataset.Tables[6].Rows[0]["OVERALL_WEIGHTAGE_SCORE"].ToString());

            PublishAssessorRating_assessorOverallWeightageScoreValueLabel.Text = 
                string.Format("{0}%", String.Format("{0:0.00}", _overWeightageScore));

            if (overallRatingDataset.Tables[1].Rows.Count == 0)
            {
                base.ShowMessage(PublishAssessorRating_topErrorMessageLabel, "");
            }
            else
            {
                //Set assessor weighted score         
                DataTable dtAssessorSubjectScore = overallRatingDataset.Tables[1];
                dtAssessorSubjectScore.AcceptChanges();

                PublishAssessorRating_assessorSubjectRatingGridview.DataSource = dtAssessorSubjectScore;
                PublishAssessorRating_assessorSubjectRatingGridview.DataBind();
            }

            //Set assessor comment and weighted score
            if (overallRatingDataset.Tables[0].Rows.Count == 0)
            {
                base.ShowMessage(PublishAssessorRating_topErrorMessageLabel, "");
            }
            else
            {
                DataTable dtOtherAssessor = overallRatingDataset.Tables[0];
                if (dtOtherAssessor!=null && dtOtherAssessor.Rows.Count > 0)
                {
                    PublishAssessorRating_othersOverAllScore.DataSource = dtOtherAssessor;
                    PublishAssessorRating_othersOverAllScore.DataBind();
                }
            }

            if (assessmentSummary.QuestionDetails == null)
            {
                base.ShowMessage(PublishAssessorRating_topErrorMessageLabel, "");
            }
            else
            {
                PublishAssessorRating_questionAreaDataList.DataSource = assessmentSummary.QuestionDetails;
                PublishAssessorRating_questionAreaDataList.DataBind();
            }
        }
        
       
        /// <summary>
        /// Method that loads the candidate details.
        /// </summary>
        /// <param name="assessmentSummary">
        /// A <see cref="AssessmentSummary"/> that holds the assessment summary.
        /// </param>
        private void LoadCandidateDetails(AssessmentSummary assessmentSummary)
        {
            //Get test completed date
            DateTime testCompletedDate = DateTime.Now;
            string testName = string.Empty;

            testCompletedDate = new ReportBLManager().GetInterviewTestCompletedDateByCandidate(ViewState["CANDIDATE_SESSION_KEY"].ToString(),
                Convert.ToInt32(ViewState["ATTEMPT_ID"].ToString()));

            testName = new InterviewReportBLManager().GetInterviewTestName(ViewState["INTERVIEW_KEY"].ToString());
            PublishAssessorRating_interviewTestNameLabel.Text = testName;
            if (!Utility.IsNullOrEmpty(testCompletedDate))
            {
                PublishAssessorRating_testCompletedDateLabel.Text =
                  "Interview Completed on " + testCompletedDate.ToString("MMMM dd, yyyy");
            }
            PublishAssessorRating_candidateNameLabelValue.Text = assessmentSummary.CanidateName;
            PublishAssessorRating_clientNameLabel.Text = assessmentSummary.ClientName;
            PublishAssessorRating_positionProfileLabel.Text = assessmentSummary.PositionProfileName;

            // Get client details
            if (assessmentSummary.PositionProfileID != 0)
            {
                ViewState["POSITION_PROFILE_ID"] = assessmentSummary.PositionProfileID;
                ClientInformation clientInfo =
                    new ClientBLManager().GetClientInfoPositionProfileID(assessmentSummary.PositionProfileID);

                if (!Utility.IsNullOrEmpty(clientInfo))
                {
                    // Get client departments
                    new ClientInfoDataManager().GetClientDepartmentContactInfo(PublishAssessorRating_showClientDepartmentsLabel,
                        clientInfo.ClientDepartments, "CD", false);
                    // Get client contacts
                    new ClientInfoDataManager().GetClientDepartmentContactInfo(PublishAssessorRating_showClientContactsLabel,
                        clientInfo.ClientContacts, "CC", false);
                }
            }

            // Assign candidate image handler.
            PublishAssessorRating_candidatePhotoImage.ImageUrl = "~/Common/CandidateImageHandler.ashx?source=VIEW_CAND&candidateid=" +
                assessmentSummary.CandidateInfoID;
        }

        /// <summary>
        /// Method to load question detail when question is loaded
        /// </summary>
        /// <param name="testQuestionID"></param>
        /// <param name="questionKey"></param>
        private void LoadCandidateResponse(int testQuestionID, string questionKey)
        {
            if (!Utility.IsNullOrEmpty(ViewState["POSITION_PROFILE_ID"]))
            {
                ClientInformation clientInfo =
                    new ClientBLManager().GetClientInfoPositionProfileID(Convert.ToInt32(ViewState["POSITION_PROFILE_ID"]));

                if (!Utility.IsNullOrEmpty(clientInfo))
                {
                    // Get client departments
                    new ClientInfoDataManager().GetClientDepartmentContactInfo(PublishAssessorRating_showClientDepartmentsLabel,
                        clientInfo.ClientDepartments, "CD", false);
                    // Get client contacts
                    new ClientInfoDataManager().GetClientDepartmentContactInfo(PublishAssessorRating_showClientContactsLabel,
                        clientInfo.ClientContacts, "CC", false);
                }
            }

            string interviewKey = string.Empty;
            string iFrameSrc = string.Empty;
            string baseURL = ConfigurationManager.AppSettings["DEFAULT_URL"];

            AsssessorRating_candidateResponseIframe.Visible = true;

            iFrameSrc = baseURL + "Assessments/PublishCandidateInterviewResponse.aspx";
            iFrameSrc += "?interviewKey=" + ViewState["INTERVIEW_KEY"].ToString();
            iFrameSrc += "&candidatesessionid=" + ViewState["CANDIDATE_SESSION_KEY"].ToString();
            iFrameSrc += "&attemptid=" + ViewState["ATTEMPT_ID"].ToString();
            iFrameSrc += "&testquestionid=" + testQuestionID;
            iFrameSrc += "&questionKey=" + questionKey;
            iFrameSrc += "&showcomment=" + ViewState["SHOW_SCORE"].ToString();
            AsssessorRating_candidateResponseIframe.Attributes["src"] = iFrameSrc;

        }
        #endregion Private Methods

    }
}