<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssessorRating.aspx.cs" Inherits="Forte.HCM.UI.Assessments.AssessorRating"
    MasterPageFile="~/MasterPages/InterviewMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<%@ Register Src="~/CommonControls/ConfirmInterviewScoreMsgControl.ascx" TagName="ConfirmInterviewScoreMsgControl" TagPrefix="uc1" %>
<asp:Content ID="AssessorRating_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <asp:UpdatePanel ID="tset" runat="server">
        <ContentTemplate>
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td class="header_bg">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 30%" class="header_text_bold">
                                    <asp:Literal ID="AssessorRating_headerLiteral" runat="server" Text="Assessment Rating"></asp:Literal>
                                </td>
                                <td style="width: 30%">
                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="AssessorRating_topPublishInterviewImageButton"
                                                    runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/publish.png" ToolTip="Publish Candidate Interview Response"
                                                    Visible="true" Width="18px" Height="18px" OnClick="AssessorRating_publishInterviewImageButton_Click" />
                                                <asp:UpdatePanel ID="AssessorRating_showPublishUrlUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <div style="float: left;">
                                                            <asp:Panel ID="AssessorRating_emailConfirmationPanel" runat="server" Style="display: none;
                                                                height: 254px;" CssClass="popupcontrol_confirm_publish_candidate_interview_response">
                                                                <div id="AssessorRating_emailConfirmationDiv" style="display: none">
                                                                    <asp:Button ID="AssessorRating_emailConfirmation_hiddenButton" runat="server" />
                                                                </div>
                                                                <uc1:ConfirmInterviewScoreMsgControl ID="AssessorRating_emailConfirmation_ConfirmMsgControl"
                                                                    runat="server" Title="Publish Candidate Interview Response" Type="InterviewScoreConfirmType" />
                                                            </asp:Panel>
                                                            <ajaxToolKit:ModalPopupExtender ID="AssessorRating_emailConfirmation_ModalPopupExtender"
                                                                BehaviorID="AssessorRating_emailConfirmation_ModalPopupExtender" runat="server"
                                                                PopupControlID="AssessorRating_emailConfirmationPanel" TargetControlID="AssessorRating_emailConfirmation_hiddenButton"
                                                                BackgroundCssClass="modalBackground">
                                                            </ajaxToolKit:ModalPopupExtender>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                                &nbsp;
                                                <asp:Button ID="AssessorRating_topRefreshButton" runat="server" SkinID="sknButtonId"
                                                    Text="Refresh" OnClick="AssessorRating_refreshButton_Click" />
                                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                                <asp:LinkButton ID="AssessorRating_topCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="AssessorRating_messagesUpdatePanel" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="AssessorRating_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                <asp:Label ID="AssessorRating_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="tab_body_bg_assessor">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top" colspan="3">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="2" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 60px">
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Image ID="AssessorRating_candidatePhotoImage" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 225px;  padding-top:10px" valign="top" class="border_right">
                                                            <table cellpadding="4" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <asp:LinkButton ID="AssessorRating_candidateNameLinkButton" runat="server" SkinID="sknAssessorActionLinkButton" ToolTip="Click here to view candidate profile"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                         <asp:Label ID="AssessorRating_interviewNameLabel" runat="server" SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <asp:Label ID="AssessorRating_testCompletedDateLabel" runat="server" SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                 <tr>
                                                                    <td align="left">
                                                                        <div runat="server" id="PublishAssessorRating_displayOverallScoreDiv">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td style="width:130px">
                                                                                    <asp:Label ID="AssessorRating_overallWeightageScoreLabel" runat="server"
                                                                                        Text="Total Weighted Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="AssessorRating_overallWeightageScoreLabelValue" SkinID="sknLabelRatingScoreFieldText"
                                                                                        runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            
                                                                        </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 10px;">
                                                        
                                                        </td>
                                                        <td valign="top" style="width: 350px; padding-top:10px">
                                                            <table cellpadding="4" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top">
                                                                        <div style="width:400px; word-wrap: break-word;white-space:normal">
                                                                        <asp:LinkButton ID="AssessorRating_positionProfileLinkButton" runat="server" Text="" 
                                                                        SkinID="sknAssessorActionLinkButton" style="word-wrap:break-word;white-space:normal" ToolTip="Click here to view position profile details"></asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <asp:LinkButton ID="AssessorRating_clientNameLinkButton" runat="server" Text="Client Name" SkinID="sknLabelFieldTextLinkButton" ToolTip="Click here to view client details"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <asp:Label ID="AssessorRating_showClientDepartmentsLabel" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <asp:Label ID="AssessorRating_showClientContactsLabel" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_5">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td style="width: 410px" valign="top">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="header_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:Literal ID="AssessorRating_subjectScoreLiteral" runat="server" Text="Assessor Subject Score"></asp:Literal>
                                                                                </td>
                                                                                <td align="right" >
                                                                                    <table cellpadding="2" cellspacing="0">
                                                                                        <tr id="AssessorRating_assessorSubjectScoreTR" runat="server">
                                                                                            <td>
                                                                                                <div style="float: left">
                                                                                                    <asp:Label ID="AssessorRating_otherAssessorTitleLable" runat="server" SkinID="sknLabelRatingText" Text="Other Assessor(s)"></asp:Label>
                                                                                                </div>
                                                                                                <div style="float: left">&nbsp;</div>
                                                                                                <div style="float: left">
                                                                                                    <span id="AssessorRating_otherAssessorSubjectRatingUpSpan" runat="server" style="display: none;">
                                                                                                        <asp:Image ID="AssessorRating_otherAssessorSubjectRatingUpImage" runat="server" ToolTip="Click here to view other assessor(s) subject score"
                                                                                                            SkinID="sknMinimizeImage" />
                                                                                                    </span><span id="AssessorRating_otherAssessorSubjectRatingDownSpan" runat="server"
                                                                                                        style="display: block;">
                                                                                                        <asp:Image ID="AssessorRating_otherAssessorSubjectRatingDownImage" runat="server"
                                                                                                            SkinID="sknMaximizeImage" />
                                                                                                    </span>
                                                                                                    <asp:HiddenField ID="AssessorRating_otherAssessorSubjectRatingRestoreHiddenField"
                                                                                                        runat="server" />
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="assessor_panel_body_bg_small" valign="top">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="width: 100%;overflow: auto;height:100px;" runat="server" id="AssessorRating_AssessorSubjectScoreDiv">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td valign="top" align="left" style="width: 100%">
                                                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                                                        <tr>
                                                                                                            <td style="width: 210px">
                                                                                                                <asp:Label ID="AssessorRating_assesorSkillNameLabel" runat="server" Text="Skill"
                                                                                                                    SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 150px">
                                                                                                                <asp:Label ID="AssessorRating_assesorNameLabel" runat="server" Text="Name" SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 150px; text-align: right">
                                                                                                                <asp:Label ID="AssessorRating_assesorSubjectWeightedScoreLabel" runat="server" Text="Subject Weighted Score"
                                                                                                                    SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:UpdatePanel ID="AssessorRating_assessorSubjectRatingUpdatePanel" runat="server">
                                                                                                        <ContentTemplate>
                                                                                                            <asp:GridView ID="AssessorRating_assessorSubjectRatingGridview" runat="server" AutoGenerateColumns="False"
                                                                                                                SkinID="sknNewGridView" ShowHeader="false" OnRowDataBound="AssessorRating_assessorSubjectRatingGridview_RowDataBound">
                                                                                                                <Columns>
                                                                                                                    <asp:TemplateField HeaderText="Subject" ItemStyle-Width="31%">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="AssessorRating_subjectNameLabel" runat="server" SkinID="sknLabelAssessorFieldNameText"
                                                                                                                                Text='<%# Eval("SUBJECT_NAME") %>'></asp:Label>
                                                                                                                            <asp:HiddenField ID="AssessorRating_overallSubjectGridViewHiddenField" Value='<%# Eval("CAT_SUB_ID") %>'
                                                                                                                                runat="server" />
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField HeaderText="Assessor Name" ItemStyle-Width="25%">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="AssessorRating_AssessorNameLabel" runat="server" SkinID="sknLabelAssessorFieldNameText"
                                                                                                                                Text='<%# Eval("ASSESSOR_NAME") %>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField HeaderText="OVERALL WEIGHTED SCORE" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Right">
                                                                                                                        <ItemTemplate>
                                                                                                                            &nbsp;
                                                                                                                            <asp:Label ID="AssessorRating_assessorWeightageScoreGridviewLabel" runat="server"
                                                                                                                                SkinID="sknLabelAssessorFieldscoreText" Text='<%# Eval("SUBJECT_WEIGHTAGE_RATING") %>'></asp:Label>
                                                                                                                            <asp:HiddenField ID="AssessorRating_assessorWeightageScoreGridviewHiddenField" Value='<%# Eval("SUBJECT_WEIGHTAGE_RATING") %>'
                                                                                                                                runat="server" />
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                </Columns>
                                                                                                                <HeaderStyle CssClass="grid_header" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                                <AlternatingRowStyle CssClass="grid_001" BorderColor="#CBDAEA" BorderStyle="Solid"
                                                                                                                    BorderWidth="1" />
                                                                                                                <RowStyle CssClass="grid" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                            </asp:GridView>
                                                                                                        </ContentTemplate>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <asp:UpdatePanel ID="AssessorRating_otherAssessorSubjectRatingUpdatePanel" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <div style="width: 100%; overflow: auto;height: 120px; display: block;" runat="server" id="AssessorRating_OtherAssessorSubjectScoreDiv">
                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="AssessorRating_otherAssessorTitle" runat="server" SkinID="sknLabelRatingText"
                                                                                                                Text="Other Assessor(s)"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td style="width: 210px">
                                                                                                                        <asp:Label ID="AssessorRating_otherAssesorSkillNameLabel" runat="server" Text="Skill"
                                                                                                                            SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 150px">
                                                                                                                        <asp:Label ID="AssessorRating_otherAssesorNameLabel" runat="server" Text="Name" SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 150px; text-align: right">
                                                                                                                        <asp:Label ID="AssessorRating_otherAssesorWeightedScoreLabel" runat="server" Text="Subject Weighted Score"
                                                                                                                            SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:GridView ID="AssessorRating_otherAssessorSubjectRatingGridview" runat="server"
                                                                                                                AutoGenerateColumns="False" SkinID="sknNewGridView" ShowHeader="false" OnRowDataBound="AssessorRating_otherAssessorSubjectRatingGridview_RowDataBound">
                                                                                                                <Columns>
                                                                                                                    <asp:TemplateField HeaderText="Subject" ItemStyle-Width="31%" ItemStyle-Wrap="true">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="AssessorRating_otherAssessorSubjectNameLabel" runat="server" SkinID="sknLabelAssessorFieldNameText"
                                                                                                                                Text='<%# Eval("SUBJECT_NAME") %>'></asp:Label>
                                                                                                                            <asp:HiddenField ID="AssessorRating_otherAssessorOverallSubjectGridViewHiddenField"
                                                                                                                                Value='<%# Eval("CAT_SUB_ID") %>' runat="server" />
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField HeaderText="Assessor Name" ItemStyle-Width="25%">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="AssessorRating_otherAssessorNameLabel" runat="server" SkinID="sknLabelAssessorFieldNameText"
                                                                                                                                Text='<%# Eval("ASSESSOR_NAME") %>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField HeaderText="OVERALL WEIGHTED SCORE" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Right">
                                                                                                                        <ItemTemplate>
                                                                                                                            &nbsp;
                                                                                                                            <asp:Label ID="AssessorRating_otherAssessorWeightageScoreGridviewLabel" runat="server"
                                                                                                                                SkinID="sknLabelAssessorFieldscoreText" Text='<%# Eval("SUBJECT_WEIGHTAGE_RATING") %>'></asp:Label>
                                                                                                                            <asp:HiddenField ID="AssessorRating_otherAssessorWeightageScoreGridviewHiddenField"
                                                                                                                                Value='<%# Eval("SUBJECT_WEIGHTAGE_RATING") %>' runat="server" />
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                </Columns>
                                                                                                                <HeaderStyle CssClass="grid_header" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                                <AlternatingRowStyle CssClass="grid_001" BorderColor="#CBDAEA" BorderStyle="Solid"
                                                                                                                    BorderWidth="1" />
                                                                                                                <RowStyle CssClass="grid" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                            </asp:GridView>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 2px">
                                                        </td>
                                                        <td valign="top" style="width: 410px">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="header_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:Literal ID="AssessorRating_overallScoreLiteral" runat="server" Text="Assessor Score"></asp:Literal>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <table cellpadding="2" cellspacing="0">
                                                                                        <tr id="AssessorRating_otherOverallAssessorScoreTR" runat="server">
                                                                                            <td>
                                                                                                <div style="float: left">
                                                                                                    <asp:Label ID="AssessorRating_otherOverAllScoreTitleLable" runat="server" 
                                                                                                    SkinID="sknLabelRatingText" Text="Other Assessor(s)"></asp:Label>
                                                                                                </div>
                                                                                                <div style="float: left">&nbsp;</div>
                                                                                                <div style="float: left">
                                                                                                    <span id="AssessorRating_overallScoreUpSpan" runat="server" style="display: none;">
                                                                                                        <asp:Image ID="AssessorRating_overallScoreUpImage" runat="server" ToolTip="Click here to view other assessor(s) score"
                                                                                                            SkinID="sknMinimizeImage" />
                                                                                                    </span><span id="AssessorRating_overallScoreDownSpan" runat="server" style="display: block;">
                                                                                                        <asp:Image ID="AssessorRating_overallScoreDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                                                    </span>
                                                                                                    <asp:HiddenField ID="AssessorRating_overallScoreRestoreHiddenField" runat="server" />
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="assessor_panel_body_bg_small" valign="top">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="width: 100%; overflow: auto;height:100px;" runat="server" id="AssessorRating_overAllScoreDiv">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="4" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="AssessorRating_assessorOverallWeightageScoreLabel" runat="server"
                                                                                                                    Text="Weighted Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="AssessorRating_assessorOverallWeightageScoreValueLabel" SkinID="sknLabelRatingScoreFieldText"
                                                                                                                    runat="server"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td align="right">
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td align="right">
                                                                                                                <asp:Label ID="AssessorRating_commentsLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                                                    Text="Comment"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="AssessorRating_commentsTextbox" runat="server" Height="60px" TextMode="MultiLine"
                                                                                                                    Width="300px" Wrap="true" MaxLength="500" onchange="CommentsCount(500,this)"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="td_height_5">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">
                                                                                                                <asp:UpdatePanel ID="AssessorRating_SaveSubmitUpdatePanel" runat="server">
                                                                                                                    <ContentTemplate>
                                                                                                                        <asp:Button ID="AssessorRating_saveButton" runat="server" OnClick="AssessorRating_saveButton_Click"
                                                                                                                            SkinID="sknButtonId" Text="Save" />
                                                                                                                        <asp:Button ID="AssessorRating_submitButton" runat="server" OnClick="AssessorRating_submitButton_Click"
                                                                                                                            SkinID="sknButtonId" Text="Submit" />
                                                                                                                    </ContentTemplate>
                                                                                                                </asp:UpdatePanel>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <asp:UpdatePanel ID="AssessorRating_overallAssesorScoreUpdtePanel" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <div style="width: 100%; overflow: auto; height: 120px;" runat="server" id="AssessorRating_othersOverAllScoreDiv">
                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="AssessorRating_otherAssessorOverallScoreTitleLabel" runat="server"
                                                                                                                SkinID="sknLabelRatingText" Text="Other Assessor(s)"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:GridView ID="AssessorRating_othersOverAllScore" runat="server" AutoGenerateColumns="False"
                                                                                                                SkinID="sknNewGridView" ShowHeader="false" OnRowDataBound="AssessorRating_othersOverAllScore_RowDataBound">
                                                                                                                <Columns>
                                                                                                                    <asp:TemplateField>
                                                                                                                        <ItemTemplate>
                                                                                                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <asp:Label ID="AssessorRating_assessorNameLabel" runat="server" Text='<%# Eval("ASSESSOR_NAME") %>'
                                                                                                                                            SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td align="left" style="width: 100px">
                                                                                                                                                    <asp:Label ID="AssessorRating_otherAssessorWeightageScoreLabel" Text="Weighted Score"
                                                                                                                                                        runat="server"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                                <td align="left">
                                                                                                                                                    <asp:Label ID="AssessorRating_overallAssessorWeightedGridviewLabel" runat="server"
                                                                                                                                                        Text='<%# Eval("OVERALL_ASSESSOR_WEIGHTED") %>' SkinID="sknLabelAssessorFieldscoreText"></asp:Label>
                                                                                                                                                    <asp:HiddenField ID="AssessorRating_overallAssessorWeightedGridviewHiddenField"
                                                                                                                                                        Value='<%# Eval("OVERALL_ASSESSOR_WEIGHTED") %>' runat="server" />
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td colspan="2">
                                                                                                                                        <div class="label_multi_field_text">
                                                                                                                                            <asp:Label ID="AssessorRating_otherAssessorCommentsLabel" SkinID="sknLabelRatingCommentsFieldText"
                                                                                                                                                runat="server" Text='<%# Eval("COMMENTS") %>'></asp:Label>
                                                                                                                                        </div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                </Columns>
                                                                                                                <HeaderStyle CssClass="grid_header" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                                <AlternatingRowStyle CssClass="grid_001" BorderColor="#CBDAEA" BorderStyle="Solid"
                                                                                                                    BorderWidth="1" />
                                                                                                                <RowStyle CssClass="grid" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                            </asp:GridView>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5" colspan="3">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 270px" valign="top">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td valign="top">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Literal ID="AssessorRating_assessorDetailsQuestionsLiteral" runat="server" Text="Questions"></asp:Literal>
                                                                    </td>
                                                                    <td align="right" style="padding-left: 80px">
                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:Label ID="AssessorRating_questiongroupByLabel" runat="server" Text="Group By"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <asp:UpdatePanel ID="AssessorRating_questiongroupByDropDownListUpdatePanel" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <asp:DropDownList ID="AssessorRating_questiongroupByDropDownList" runat="server"
                                                                                                AutoPostBack="true" OnSelectedIndexChanged="AssessorRating_questiongroupByDropDownList_SelectedIndexChanged">
                                                                                                <asp:ListItem Text="--Select--"></asp:ListItem>
                                                                                                <asp:ListItem Text="Question" Value="Q"></asp:ListItem>
                                                                                                <asp:ListItem Text="Subject" Value="S"></asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="assessor_panel_body_bg" valign="top">
                                                            <table cellpadding="3" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellpadding="3" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:UpdatePanel ID="AssessorRating_questiongroupByUpdatePanel" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <div style="vertical-align: top">
                                                                                                <table cellpadding="0" cellspacing="3" border="0">
                                                                                                    <tr>
                                                                                                        <td width="70" class="RatingEditable">
                                                                                                            Rating Required
                                                                                                        </td>
                                                                                                        <td width="70" class="RatingFinished">
                                                                                                            Rating Completed
                                                                                                        </td>
                                                                                                        <td width="70" class="RatingNotAllowed">
                                                                                                            Rating Not Required
                                                                                                        </td>
                                                                                                </table>
                                                                                            </div>
                                                                                            <div>
                                                                                                &nbsp;
                                                                                            </div>
                                                                                            <div style="width: 100%; overflow: auto; height: 400px;" runat="server" id="AssessorRating_questionAreaDiv">
                                                                                                <asp:GridView ID="AssessorRating_questionAreaDataList" runat="server" AutoGenerateColumns="False"
                                                                                                    SkinID="sknNewGridView" ShowHeader="false" OnRowDataBound="AssessorRating_questionAreaDataList_RowDataBound"
                                                                                                    OnRowCommand="AssessorRating_questionAreaDataList_RowCommand">
                                                                                                    <Columns>
                                                                                                        <asp:TemplateField>
                                                                                                            <ItemTemplate>
                                                                                                                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                                                                    <tr>
                                                                                                                        <td valign="middle" style="width: 5%">
                                                                                                                            <%-- <asp:Image ID="AssessorRating_questionImage" runat="server" SkinID="sknQuestionImage"
                                                                                                                                ToolTip="Question" />--%>
                                                                                                                            <asp:Label ID="AssessorRating_rowNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                                                                                runat="server">
                                                                                                                            </asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="width: 60%" align="left">
                                                                                                                            <div class="label_multi_field_text">
                                                                                                                                <asp:LinkButton ID="AssessorRating_questionAreaQuestionsLabel" runat="server" Text='<%# TrimContent(Eval("Question").ToString(),25) %>'
                                                                                                                                    CommandName="QuestionDetail">
                                                                                                                                </asp:LinkButton>
                                                                                                                                <asp:HiddenField ID="AssessorRating_questionAreaQuestionIDHiddenField" runat="server"
                                                                                                                                    Value='<%# Eval("TestQuestionID") %>' />
                                                                                                                                <asp:HiddenField ID="AssessorRating_questionAreaQuestionKeyHiddenField" runat="server"
                                                                                                                                    Value='<%# Eval("QuestionKey") %>' />
                                                                                                                                <asp:HiddenField ID="AssessorRating_questionRatingEditHiddenField" runat="server"
                                                                                                                                    Value='<%# Eval("RatingEdit") %>' />
                                                                                                                            </div>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" align="left">
                                                                                                                            <asp:Label ID="AssessorRating_questionAreaSubjectLabel" runat="server" Text='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                                                            <asp:HiddenField ID="AssessorRating_questionAreaSubjectHidddenField" runat="server"
                                                                                                                                Value='<%# Eval("SubjectID") %>' />
                                                                                                                        </td>
                                                                                                                        <td style="width: 10%" align="left">
                                                                                                                            <asp:Label ID="AssessorRating_questionAreaRatingLabel" runat="server" Text='<%# Eval("Rating") %>'>
                                                                                                                            </asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>
                                                                                                    </Columns>
                                                                                                    <AlternatingRowStyle CssClass="grid_001" BorderColor="#CBDAEA" BorderStyle="Solid"
                                                                                                        BorderWidth="1" />
                                                                                                    <RowStyle CssClass="grid" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                </asp:GridView>
                                                                                            </div>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_20">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 4px">
                                </td>
                                <td style="width: 726px" valign="top">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="candidateResponseUpdatePanel" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <iframe id="AsssessorRating_candidateResponseIframe" runat="server" height="635"
                                                            style="border-style: none" width="100%"></iframe>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="AssessorRating_questionAreaDataList" EventName="RowCommand" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="header_bg">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 50%">
                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="AssessorRating_bottomPublishUpdatePanel" runat="server">
                                                    <ContentTemplate>
                                                        <asp:ImageButton ID="AssessorRating_bottomPublishInterviewImageButton" runat="server"
                                                            ImageUrl="~/App_Themes/DefaultTheme/Images/publish.png" ToolTip="Publish Candidate Interview Response"
                                                            Visible="true" Width="18px" Height="18px" OnClick="AssessorRating_publishInterviewImageButton_Click" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                                &nbsp;
                                                <asp:Button ID="AssessorRating_bottomRefreshButton" runat="server" SkinID="sknButtonId"
                                                    Text="Refresh" OnClick="AssessorRating_refreshButton_Click" />
                                                &nbsp;|&nbsp;&nbsp;
                                                <asp:LinkButton ID="AssessorRating_bottomCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
