<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PublishAssessorRating.aspx.cs" Inherits="Forte.HCM.UI.Assessments.PublishAssessorRating"
    MasterPageFile="~/MasterPages/OverviewMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/OverviewMaster.Master" %>
<asp:Content ID="PublishAssessorRating_bodyContent" runat="server" ContentPlaceHolderID="OverviewMaster_body">
    <asp:UpdatePanel ID="tset" runat="server">
        <ContentTemplate>
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td class="header_bg">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="width: 30%" class="header_text_bold">
                                    <asp:Literal ID="PublishAssessorRating_headerLiteral" runat="server" Text="Candidate Interview Response"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="PublishAssessorRating_messagesUpdatePanel" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="PublishAssessorRating_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                <asp:Label ID="PublishAssessorRating_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="tab_body_bg_assessor">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top" align="left" colspan="3">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="2" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 60px">
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Image ID="PublishAssessorRating_candidatePhotoImage" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 235px;  padding-top:10px" valign="top" class="border_right">
                                                            <table cellpadding="4" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <asp:Label ID="PublishAssessorRating_candidateNameLabelValue" runat="server" CssClass="assessor_name_label_text">
                                                                        </asp:Label>
                                                                    </td>
                                                                </tr>
                                                                 <tr>
                                                                    <td align="left" valign="top">
                                                                         <asp:Label ID="PublishAssessorRating_interviewTestNameLabel" runat="server" SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <asp:Label ID="PublishAssessorRating_testCompletedDateLabel" runat="server" SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <div runat="server" id="PublishAssessorRating_displayOverallScoreDiv">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="PublishAssessorRating_assessorOverallWeightageScoreLabel" runat="server"
                                                                                        Text="Total Weighted Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="PublishAssessorRating_assessorOverallWeightageScoreValueLabel" SkinID="sknLabelRatingScoreFieldText"
                                                                                        runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 10px;">
                                                        
                                                        </td>
                                                        <td valign="top" style="width: 350px; padding-top:10px">
                                                            <table cellpadding="2" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top">
                                                                        <div style="width:450px; word-wrap: break-word;white-space: normal">
                                                                            <asp:Label ID="PublishAssessorRating_positionProfileLabel" runat="server" CssClass="assessor_name_label_text"
                                                                             style="word-wrap:break-word;white-space:normal" ></asp:Label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <asp:Label ID="PublishAssessorRating_clientNameLabel" runat="server" CssClass="client_name_label_text"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <asp:Label ID="PublishAssessorRating_showClientDepartmentsLabel" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <asp:Label ID="PublishAssessorRating_showClientContactsLabel" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_5">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <div runat="server" id="PublishAssessorRating_displayAssessorScoreDiv">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td style="width: 410px" valign="top">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="header_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:Literal ID="PublishAssessorRating_subjectScoreLiteral" runat="server" Text="Assessor(s) Subject Score"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <td class="assessor_panel_body_bg_small" valign="top">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td valign="top" align="left" style="width: 100%">
                                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                                        <tr>
                                                                                            <td style="width: 185px">
                                                                                                <asp:Label ID="PublishAssessorRating_assesorSkillNameLabel" runat="server" Text="Skill"
                                                                                                    SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 150px">
                                                                                                <asp:Label ID="PublishAssessorRating_assesorNameLabel" runat="server" Text="Name"
                                                                                                    SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 150px; text-align: right">
                                                                                                <asp:Label ID="PublishAssessorRating_assesorSubjectWeightedScoreLabel" runat="server"
                                                                                                    Text="Subject Weighted Score" SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="width: 100%;overflow: auto;height:150px;" runat="server" 
                                                                                        id="PublishAssessorRating_AssessorSubjectScoreDiv">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:UpdatePanel ID="PublishAssessorRating_assessorSubjectRatingUpdatePanel" runat="server">
                                                                                                        <ContentTemplate>
                                                                                                            <asp:GridView ID="PublishAssessorRating_assessorSubjectRatingGridview" runat="server" 
                                                                                                                AutoGenerateColumns="False"
                                                                                                                SkinID="sknNewGridView" ShowHeader="false" 
                                                                                                                OnRowDataBound="PublishAssessorRating_assessorSubjectRatingGridview_RowDataBound">
                                                                                                                <Columns>
                                                                                                                    <asp:TemplateField HeaderText="Subject" ItemStyle-Width="31%">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="PublishAssessorRating_subjectNameLabel" runat="server" SkinID="sknLabelAssessorFieldNameText"
                                                                                                                                Text='<%# Eval("SUBJECT_NAME") %>'></asp:Label>
                                                                                                                            <asp:HiddenField ID="PublishAssessorRating_overallSubjectGridViewHiddenField" Value='<%# Eval("CAT_SUB_ID") %>'
                                                                                                                                runat="server" />
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField HeaderText="Assessor Name" ItemStyle-Width="25%">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="PublishAssessorRating_AssessorNameLabel" runat="server" SkinID="sknLabelAssessorFieldNameText"
                                                                                                                                Text='<%# Eval("ASSESSOR_NAME") %>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField HeaderText="OVERALL WEIGHTED SCORE" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Right">
                                                                                                                        <ItemTemplate>
                                                                                                                            &nbsp;
                                                                                                                            <asp:Label ID="PublishAssessorRating_assessorWeightageScoreGridviewLabel" runat="server"
                                                                                                                                SkinID="sknLabelAssessorFieldscoreText" Text='<%# Eval("SUBJECT_WEIGHTAGE_RATING") %>'></asp:Label>
                                                                                                                            <asp:HiddenField ID="PublishAssessorRating_assessorWeightageScoreGridviewHiddenField" Value='<%# Eval("SUBJECT_WEIGHTAGE_RATING") %>'
                                                                                                                                runat="server" />
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                </Columns>
                                                                                                                <HeaderStyle CssClass="grid_header" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                                <AlternatingRowStyle CssClass="grid_001" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                                <RowStyle CssClass="grid" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                            </asp:GridView>
                                                                                                        </ContentTemplate>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 2px">
                                                        </td>
                                                        <td valign="top" style="width: 410px">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="header_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <asp:Literal ID="PublishAssessorRating_otherAssessorOverallScoreTitleLiteral" runat="server" Text="Assessor(s) Score"></asp:Literal>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="assessor_panel_body_bg_small" valign="top">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <asp:UpdatePanel ID="PublishAssessorRating_overallAssesorScoreUpdtePanel" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <div style="width: 100%; overflow: auto; height: 164px;" runat="server" id="PublishAssessorRating_othersOverAllScoreDiv">
                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:GridView ID="PublishAssessorRating_othersOverAllScore" runat="server" AutoGenerateColumns="False"
                                                                                                                SkinID="sknNewGridView" ShowHeader="false" OnRowDataBound="PublishAssessorRating_othersOverAllScore_RowDataBound">
                                                                                                                <Columns>
                                                                                                                    <asp:TemplateField>
                                                                                                                        <ItemTemplate>
                                                                                                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td  style="width:150px;">
                                                                                                                                                    <asp:Label ID="PublishAssessorRating_assessorNameLabel" runat="server" Text='<%# Eval("ASSESSOR_NAME") %>'
                                                                                                                                                        SkinID="sknLabelAssessorFieldHeaderText"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                                <td style="width: 100px">
                                                                                                                                                    <asp:Label ID="PublishAssessorRating_otherAssessorWeightageScoreLabel" Text="Weighted Score"
                                                                                                                                                        runat="server"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                                <td align="left">
                                                                                                                                                    <asp:Label ID="PublishAssessorRating_otherAssessorOverallSubjectWeightageRatingLabel"
                                                                                                                                                        runat="server" Text='<%# Eval("OVERALL_ASSESSOR_WEIGHTED") %>' SkinID="sknLabelAssessorFieldscoreText"></asp:Label>
                                                                                                                                                    <asp:HiddenField ID="PublishAssessorRating_othersAssessorOverallWeightageRatingGridviewHiddenField"
                                                                                                                                                        Value='<%# Eval("OVERALL_ASSESSOR_WEIGHTED") %>' runat="server" />
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td colspan="2">
                                                                                                                                        <div class="label_multi_field_text">
                                                                                                                                            <asp:Label ID="PublishAssessorRating_otherAssessorCommentsLabel" SkinID="sknLabelRatingCommentsFieldText"
                                                                                                                                                runat="server" Text='<%# Eval("COMMENTS") %>'></asp:Label>
                                                                                                                                        </div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                </Columns>
                                                                                                                <HeaderStyle CssClass="grid_header" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                                <AlternatingRowStyle CssClass="grid_001" BorderColor="#CBDAEA" BorderStyle="Solid"
                                                                                                                    BorderWidth="1" />
                                                                                                                <RowStyle CssClass="grid" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                            </asp:GridView>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5" colspan="3">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 270px" valign="top">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td valign="top">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="header_bg">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Literal ID="PublishAssessorRating_assessorDetailsQuestionsLiteral" runat="server" Text="Questions"></asp:Literal>
                                                                    </td>
                                                                    <td align="right" style="padding-left: 80px">
                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:Label ID="PublishAssessorRating_questiongroupByLabel" runat="server" Text="Group By"
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <asp:UpdatePanel ID="PublishAssessorRating_questiongroupByDropDownListUpdatePanel" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <asp:DropDownList ID="PublishAssessorRating_questiongroupByDropDownList" runat="server"
                                                                                                AutoPostBack="true" OnSelectedIndexChanged="PublishAssessorRating_questiongroupByDropDownList_SelectedIndexChanged">
                                                                                                <asp:ListItem Text="--Select--"></asp:ListItem>
                                                                                                <asp:ListItem Text="Question" Value="Q"></asp:ListItem>
                                                                                                <asp:ListItem Text="Subject" Value="S"></asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="assessor_panel_body_bg" valign="top">
                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td class="td_height_5">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:UpdatePanel ID="PublishAssessorRating_questiongroupByUpdatePanel" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <div>
                                                                                                &nbsp;
                                                                                            </div>
                                                                                            <div style="width: 100%; overflow: auto; height: 400px;" runat="server" id="PublishAssessorRating_questionAreaDiv">
                                                                                                <asp:GridView ID="PublishAssessorRating_questionAreaDataList" runat="server" AutoGenerateColumns="False"
                                                                                                    SkinID="sknNewGridView" ShowHeader="false" OnRowDataBound="PublishAssessorRating_questionAreaDataList_RowDataBound"
                                                                                                    OnRowCommand="PublishAssessorRating_questionAreaDataList_RowCommand">
                                                                                                    <Columns>
                                                                                                        <asp:TemplateField>
                                                                                                            <ItemTemplate>
                                                                                                                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                                                                    <tr>
                                                                                                                        <td valign="middle" style="width: 5%">
                                                                                                                            <asp:Label ID="PublishAssessorRating_rowNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                                                                                runat="server">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                            </asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="width: 70%" align="left">
                                                                                                                            <div class="label_multi_field_text">
                                                                                                                                <asp:LinkButton ID="PublishAssessorRating_questionAreaQuestionsLabel" runat="server" 
                                                                                                                                    Text='<%# TrimContent(Eval("Question").ToString(),32) %>'
                                                                                                                                    CommandName="QuestionDetail">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                                </asp:LinkButton>
                                                                                                                                <asp:HiddenField ID="PublishAssessorRating_questionAreaQuestionIDHiddenField" runat="server"
                                                                                                                                    Value='<%# Eval("TestQuestionID") %>' />
                                                                                                                                <asp:HiddenField ID="PublishAssessorRating_questionAreaQuestionKeyHiddenField" runat="server"
                                                                                                                                    Value='<%# Eval("QuestionKey") %>' />
                                                                                                                            </div>
                                                                                                                        </td>
                                                                                                                        <td style="width: 20%" align="left">
                                                                                                                            <asp:Label ID="PublishAssessorRating_questionAreaSubjectLabel" runat="server" Text='<%# Eval("SubjectName") %>'></asp:Label>
                                                                                                                            <asp:HiddenField ID="PublishAssessorRating_questionAreaSubjectHidddenField" runat="server"
                                                                                                                                Value='<%# Eval("SubjectID") %>' />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>
                                                                                                    </Columns>
                                                                                                    <AlternatingRowStyle CssClass="grid_001" BorderColor="#CBDAEA" BorderStyle="Solid"
                                                                                                        BorderWidth="1" />
                                                                                                    <RowStyle CssClass="grid" BorderColor="#CBDAEA" BorderStyle="Solid" BorderWidth="1" />
                                                                                                </asp:GridView>
                                                                                            </div>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_20">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 4px">
                                </td>
                                <td style="width: 726px" valign="top">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="candidateResponseUpdatePanel" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <iframe id="AsssessorRating_candidateResponseIframe" runat="server" height="635"
                                                            style="border-style: none" width="100%"></iframe>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="PublishAssessorRating_questionAreaDataList" EventName="RowCommand" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
