﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewCandidateTestDetails.cs
// File that represents the Candidate Test question Details with answers.
// y.
//

#endregion Header

#region Directives

using System;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;
using Forte.HCM.Utilities;



#endregion Directives

namespace Forte.HCM.UI.InterviewReportCenter
{
    public partial class InterviewCandidateTestDetails : PageBase
    {
        #region Event Handler

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Candidate Interview Result Summary"); 

                if (!IsPostBack)
                {
                    LoadValues();
                }

                if (Request.QueryString["parentpage"] != null &&
                    Request.QueryString["parentpage"] == "mail")
                {
                    // Hide menus.
                    Master.HideMenu();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewCandidateTestDetails_topErrorMessageLabel,
                InterviewCandidateTestDetails_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the RowDataBound event of the InterviewCandidateTestDetails_testDrftGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void InterviewCandidateTestDetails_testDrftGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                // Find the row type is DataRow
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField questionKey = (HiddenField)e.Row.FindControl("InterviewCandidateTestDetails_questionKey");
                    QuestionDetail questionDetail = new InterviewReportBLManager().GetSingleInterviewQuestionDetails(
                           Request.QueryString["testkey"], questionKey.Value, int.Parse(Request.QueryString["attemptid"]),
                           Request.QueryString["candidatesession"]);
                    // Find a label from the current row.
                    Label InterviewCandidateTestDetails_rowNoLabel = (Label)e.Row.FindControl("InterviewCandidateTestDetails_rowNoLabel");

                    // Find a image control for the current row.
                    Image InterviewCandidateTestDetails_questionImageDisplay = (Image)e.Row.FindControl("InterviewCandidateTestDetails_questionImageDisplay");

                    // Assign the row no to the label.
                    InterviewCandidateTestDetails_rowNoLabel.Text = (e.Row.RowIndex + 1).ToString();

                    if (!Utility.IsNullOrEmpty(questionDetail.QuestionImage))
                    {
                        InterviewCandidateTestDetails_questionImageDisplay.Visible = true;
                        Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = questionDetail.QuestionImage;
                        InterviewCandidateTestDetails_questionImageDisplay.ImageUrl = @"~/Common/ImageHandler.ashx?source=INTERVIEW_QUESTION_IMAGE&questionKey=" + questionDetail.QuestionKey;
                    }
                    else
                    {
                        InterviewCandidateTestDetails_questionImageDisplay.Visible = false;
                    }

                    ImageButton InterviewCandidateTestDetails_candidateResponseImageButton =
                        (ImageButton)e.Row.FindControl("InterviewCandidateTestDetails_candidateResponseImageButton");

                    HiddenField InterviewCandidateTestDetails_questionID =
                        (HiddenField)e.Row.FindControl("InterviewCandidateTestDetails_questionID");

                    HiddenField InterviewCandidateTestDetails_questionKey =
                        (HiddenField)e.Row.FindControl("InterviewCandidateTestDetails_questionKey");

                    ImageButton InterviewCandidateTestDetails_hideVideoImageButton =
                        (ImageButton)e.Row.FindControl("InterviewCandidateTestDetails_hideVideoImageButton");

                    InterviewCandidateTestDetails_hideVideoImageButton.Visible = false;

                    InterviewCandidateTestDetails_candidateResponseImageButton.Attributes.Add("onclick",
                        "return OpenInterviewResponse('" + Request.QueryString["testkey"] +
                            "','" + Request.QueryString["candidatesession"] +
                            "','" + int.Parse(Request.QueryString["attemptid"]) + "','" +
                            Convert.ToInt32(InterviewCandidateTestDetails_questionID.Value) + "','" +
                            InterviewCandidateTestDetails_questionKey.Value + "','" +
                            Convert.ToInt32((Request.QueryString.Get("assessorId"))) + "');");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewCandidateTestDetails_topErrorMessageLabel,
                InterviewCandidateTestDetails_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler to redirect to offline interview tracking details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InterviewCandidateTestDetails_viewTestDetailsLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/InterviewReportCenter/OfflineInterviewTrackingDetails.aspx?"
                + "candidatesession=" + Request.QueryString.Get("candidatesession") + "&attemptid=" + int.Parse(Request.QueryString["attemptid"]), false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(InterviewCandidateTestDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method to show the publish interview popup extender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InterviewCandidateTestDetails_publishInterviewImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                InterviewScoreParamDetail interviewScoreParamDetail = null;
                if (interviewScoreParamDetail == null)
                    interviewScoreParamDetail = new InterviewScoreParamDetail();

                int attemptID = 0;
                if (!Utility.IsNullOrEmpty(Request.QueryString.Get("attemptid").ToString()))
                    attemptID = Convert.ToInt32(Request.QueryString.Get("attemptid").ToString());

                interviewScoreParamDetail.InterviewKey = Request.QueryString.Get("testkey").ToString();
                interviewScoreParamDetail.CandidateInterviewSessionKey = Request.QueryString.Get("candidatesession").ToString();
                interviewScoreParamDetail.AttemptID = attemptID;
                interviewScoreParamDetail.CandidateName = InterviewCandidateTestDetails_candidateNameLinkButton.Text.ToString();

                InterviewCandidateTestDetails_emailConfirmation_ConfirmMsgControl.InterviewParams = interviewScoreParamDetail;
                InterviewCandidateTestDetails_emailConfirmation_ModalPopupExtender.Show();
                InterviewCandidateTestDetails_emailConfirmation_ConfirmMsgControl.ShowScoreUrl();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(InterviewCandidateTestDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method to display video for the current question and disable other questions videos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InterviewCandidateTestDetails_testDrftGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string iFrameSrc = string.Empty;
                string baseURL = ConfigurationManager.AppSettings["DEFAULT_URL"];

                //Check whether the command name is ShowVideo 
                if (e.CommandName == "ShowVideo")
                {
                    string questionId = ((HiddenField)((ImageButton)e.CommandSource).
                        Parent.FindControl("InterviewCandidateTestDetails_questionID")).Value;

                    iFrameSrc = baseURL + "InterviewReportCenter/InterviewQuestionVideoDisplay.aspx?";
                    iFrameSrc += "candidatesessionid=" + Request.QueryString["candidatesession"];
                    iFrameSrc += "&attemptid=" + Request.QueryString["attemptid"];
                    iFrameSrc += "&questionid=" + questionId;

                    //Reset other question videos
                    foreach (GridViewRow row in InterviewCandidateTestDetails_testDrftGridView.Rows)
                    {
                        HtmlControl InterviewCandidateTestDetails_resetDisplayVideoIframe = 
                            (HtmlControl)row.FindControl("InterviewCandidateTestDetails_displayVideoIframe");
                        ImageButton InterviewCandidateTestDetails_showVideoImageButton =
                            (ImageButton)row.FindControl("InterviewCandidateTestDetails_showVideoImageButton");
                        ImageButton InterviewCandidateTestDetails_hideVideoImageButton =
                            (ImageButton)row.FindControl("InterviewCandidateTestDetails_hideVideoImageButton");

                        InterviewCandidateTestDetails_resetDisplayVideoIframe.Attributes["src"] = string.Empty;
                        InterviewCandidateTestDetails_resetDisplayVideoIframe.Visible = false;
                        InterviewCandidateTestDetails_showVideoImageButton.Visible = true;
                        InterviewCandidateTestDetails_hideVideoImageButton.Visible = false;
                    }

                    GridViewRow rowIndex = (GridViewRow)((ImageButton)e.CommandSource).NamingContainer;

                    HtmlControl InterviewCandidateTestDetails_displayVideoIframe =
                       (HtmlControl)InterviewCandidateTestDetails_testDrftGridView.Rows[rowIndex.RowIndex].
                       FindControl("InterviewCandidateTestDetails_displayVideoIframe");

                    ImageButton InterviewCandidateTestDetails_hideVideoRowImageButton =
                       (ImageButton)InterviewCandidateTestDetails_testDrftGridView.Rows[rowIndex.RowIndex].
                        FindControl("InterviewCandidateTestDetails_hideVideoImageButton");

                    ImageButton InterviewCandidateTestDetails_showVideoRowImageButton =
                        (ImageButton)InterviewCandidateTestDetails_testDrftGridView.Rows[rowIndex.RowIndex].
                        FindControl("InterviewCandidateTestDetails_showVideoImageButton");

                    InterviewCandidateTestDetails_hideVideoRowImageButton.Visible = true;
                    InterviewCandidateTestDetails_showVideoRowImageButton.Visible = false;
                    InterviewCandidateTestDetails_displayVideoIframe.Visible=true;
                    InterviewCandidateTestDetails_displayVideoIframe.Attributes["src"] = string.Empty;
                    InterviewCandidateTestDetails_displayVideoIframe.Attributes["src"] = iFrameSrc;
                }

                //Check whether the command name is HideVideo 
                if (e.CommandName == "HideVideo")
                {
                    GridViewRow rowIndex = (GridViewRow)((ImageButton)e.CommandSource).NamingContainer;
                    HtmlControl InterviewCandidateTestDetails_displayVideoIframe =
                       (HtmlControl)InterviewCandidateTestDetails_testDrftGridView.Rows[rowIndex.RowIndex].
                       FindControl("InterviewCandidateTestDetails_displayVideoIframe");

                    ImageButton InterviewCandidateTestDetails_showVideoImageButton =
                       (ImageButton)InterviewCandidateTestDetails_testDrftGridView.Rows[rowIndex.RowIndex].
                        FindControl("InterviewCandidateTestDetails_showVideoImageButton");

                    ImageButton InterviewCandidateTestDetails_hideVideoImageButton =
                       (ImageButton)InterviewCandidateTestDetails_testDrftGridView.Rows[rowIndex.RowIndex].
                        FindControl("InterviewCandidateTestDetails_hideVideoImageButton");

                    InterviewCandidateTestDetails_displayVideoIframe.Attributes["src"] = string.Empty;
                    InterviewCandidateTestDetails_displayVideoIframe.Visible = false;
                    InterviewCandidateTestDetails_showVideoImageButton.Visible = true;
                    InterviewCandidateTestDetails_hideVideoImageButton.Visible = false;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(InterviewCandidateTestDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler to redirect to candidate rating summary
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InterviewCandidateTestDetails_candidateRatingSummaryImageButton_Click(object sender, ImageClickEventArgs e)
        {
            int attemptID = 0;
            if (!Utility.IsNullOrEmpty(Request.QueryString.Get("attemptid").ToString()))
                attemptID = Convert.ToInt32(Request.QueryString.Get("attemptid").ToString());

            Response.Redirect("~/Assessments/CandidateAssessorSummary.aspx?m=4&s=0" +
                "&testkey=" + Request.QueryString["testkey"] +
                "&candidatesessionid=" + Request.QueryString.Get("candidatesession").ToString() +
                "&attemptid=" + attemptID +
                "&tab=CS" +
                "&parentpage=" + Constants.ParentPage.INTERVIEW_TEST_STATISTICS_INFO, false);
        }

        /// <summary>
        /// Handler to redirect to candidate rating summary
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InterviewCandidateTestDetails_requestExternalAssessorImageButton_Click(object sender,
            ImageClickEventArgs e)
        {
            int attemptID = 0;
            if (!Utility.IsNullOrEmpty(Request.QueryString.Get("attemptid").ToString()))
                attemptID = Convert.ToInt32(Request.QueryString.Get("attemptid").ToString());

            string candidateSessionId = Request.QueryString.Get("candidatesession").ToString();

            ScriptManager.RegisterStartupScript(InterviewCandidateTestDetails_showPublishUrlUpdatePanel,
                        InterviewCandidateTestDetails_showPublishUrlUpdatePanel.GetType(),
                        "RequestExternalAssessorWindow", "RequestExternalAssessor(" + attemptID + ",'" + candidateSessionId + "', " +
                        "'" + InterviewCandidateTestDetails_refreshButton.ClientID + "')", true);
        }

        #endregion Event Handler

        #region Private methods
        /// <summary>
        /// Represents the method to get the answer choices of the question 
        /// </summary>
        /// <param name="answerChoice">
        /// A<see cref="List<AnswerChoice>"/>that holds the list of answerchoices
        /// </param>
        /// <returns>
        /// A<see cref="Table"/>a table that contain the answer choice
        /// </returns>
        private Table GetAnswerChoices(List<AnswerChoice> answerChoice)
        {
            //This method will return the table containing the answer choices
            return (new ControlUtility().GetAnswerChoices(answerChoice, false));
        }

        /// <summary>
        /// Loads the candidate statistics details.
        /// </summary>
        /// <param name="CandidateStatisticsDataSource">The candidate statistics data source.</param>
        private void LoadCandidateStatisticsDetails(CandidateStatisticsDetail CandidateStatisticsDataSource)
        {
            InterviewCandidateTestDetails_noOfQuestionLabelValue.Text =
                CandidateStatisticsDataSource.TotalQuestion.ToString();
            InterviewCandidateTestDetails_noOfQuestionattenedLabelValue.Text =
                CandidateStatisticsDataSource.TotalQuestionAttended.ToString();
            InterviewCandidateTestDetails_noOfQuestionSkippedLabelValue.Text =
                CandidateStatisticsDataSource.TotalQuestionSkipped.ToString();
            InterviewCandidateTestDetails_noOfQuestionTimeTakenLabelValue.Text= CandidateStatisticsDataSource.TimeTaken;
            InterviewCandidateTestDetails_InterviewTestNameLabel.Text = CandidateStatisticsDataSource.TestName;
            

            InterviewCandidateTestDetails_candidateNameLinkButton.Text = CandidateStatisticsDataSource.CandidateFullName;
            InterviewCandidateTestDetails_ClientNameLinkButton.Text = CandidateStatisticsDataSource.ClientName;
            InterviewCandidateTestDetails_positionProfileNameLinkButton.Text = CandidateStatisticsDataSource.PositionProfileName;
            // Add handler for candidate name link button.
            if (!Utility.IsNullOrEmpty(CandidateStatisticsDataSource.CandidateID))
            {
                InterviewCandidateTestDetails_candidateNameLinkButton.Attributes.Add("onclick",
                    "javascript:return OpenViewCandidateProfilePopup('" + CandidateStatisticsDataSource.CandidateID + "');");
            }

            // Add handler for client name link button.
            if (CandidateStatisticsDataSource.ClientID != 0)
            {
                InterviewCandidateTestDetails_ClientNameLinkButton.Attributes.Add("onclick",
                    "javascript:return ShowViewClient('" + CandidateStatisticsDataSource.ClientID + "');");
            }

            // Get client department / contact information
            if (CandidateStatisticsDataSource.PositionProfileID != 0)
            {
                ClientInformation clientInfo =
                    new ClientBLManager().GetClientInfoPositionProfileID(CandidateStatisticsDataSource.PositionProfileID);

                if (!Utility.IsNullOrEmpty(clientInfo))
                {
                    // Get client departments
                    new ClientInfoDataManager().GetClientDepartmentContactInfo(InterviewCandidateTestDetails_showClientDepartmentsLabel,
                        clientInfo.ClientDepartments, "CD", true);
                    // Get client contacts
                    new ClientInfoDataManager().GetClientDepartmentContactInfo(InterviewCandidateTestDetails_showClientContactsLabel,
                        clientInfo.ClientContacts, "CC", true);
                }
            }

            // Add handler for position profile link button.
            if (CandidateStatisticsDataSource.PositionProfileID!=0)
            {
                InterviewCandidateTestDetails_positionProfileNameLinkButton.Attributes.Add("onclick",
                    "javascript:return ShowViewPositionProfile('" + CandidateStatisticsDataSource.PositionProfileID + "');");
            }

            //Set interview completed date
            DateTime interviewCompletedDate = DateTime.Now;
            interviewCompletedDate = new ReportBLManager().
                GetInterviewTestCompletedDateByCandidate(Request.QueryString["candidatesession"].ToString(),
                    Convert.ToInt32(Request.QueryString["attemptid"]));

            if (!Utility.IsNullOrEmpty(interviewCompletedDate))
            {
                InterviewCandidateTestDetails_interviewCompletedDateLabelValue.Text =
                  "Interview Completed on " + interviewCompletedDate.ToString("MMMM dd, yyyy");
            }

            // Assign candidate image handler.
            InterviewCandidateTestDetails_candidatePhotoImage.ImageUrl = "~/Common/CandidateImageHandler.ashx?source=VIEW_CAND&candidateid=" +
                CandidateStatisticsDataSource.CandidateID;

            decimal averageTimeTaken = 0;
            if (!Utility.IsNullOrEmpty(CandidateStatisticsDataSource.AverageTimeTaken)
                && CandidateStatisticsDataSource.AverageTimeTaken.Length != 0)
            {
                averageTimeTaken = decimal.Parse(CandidateStatisticsDataSource.AverageTimeTaken);
            }
            //InterviewCandidateTestDetails_avgTimeTakenLabelValue.Text =
            //    Utility.ConvertSecondsToHoursMinutesSeconds
            //    (decimal.ToInt32(averageTimeTaken));
            InterviewCandidateTestDetails_testStatusLabelValue.Text = CandidateStatisticsDataSource.TestStatus;
        }

        /// <summary>
        /// Binds the test questions.
        /// </summary>
        /// <param name="testKey">The test key.</param>
        /// <param name="sortExpression">The sort expression.</param>
        private void BindTestQuestions(string testKey, string candidateSessionKey, int attemptID)
        {
            List<QuestionDetail> interviewquestionDetails = new List<QuestionDetail>();

            // Call a method to build question detail object
            // by passing testkey and sortexpression

            interviewquestionDetails =
                new InterviewReportBLManager().GetInterviewCandidateTestQuestionDetail(testKey, candidateSessionKey, attemptID);

            if (interviewquestionDetails.Count==0)
            {
                 base.ShowMessage(InterviewCandidateTestDetails_topErrorMessageLabel,
                   InterviewCandidateTestDetails_bottomErrorMessageLabel, "Interview not completed");
                 InterviewCandidateTestDetails_topPublishInterviewImageButton.Visible = false;
                 InterviewCandidateTestDetails_bottomPublishInterviewImageButton.Visible = false;
                 InterviewCandidateTestDetails_interviewCompletedDateLabelValue.Text = string.Empty;
                return;
            }
            // Bind gridview
            InterviewCandidateTestDetails_testDrftGridView.DataSource = interviewquestionDetails;
            InterviewCandidateTestDetails_testDrftGridView.DataBind();
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            BindTestQuestions(Request.QueryString.Get("testkey"), Request.QueryString.Get("candidatesession"), int.Parse(Request.QueryString["attemptid"]));

            CandidateStatisticsDetail candidateStatisticsDetail = new CandidateStatisticsDetail();
            candidateStatisticsDetail =
            new InterviewReportBLManager().GetInterviewCandidateTestResultStatisticsDetails
          (Request.QueryString.Get("candidatesession"), Request.QueryString.Get("testkey"), int.Parse(Request.QueryString["attemptid"]),
          (Request.QueryString.Get("sessionkey")), base.userID);
            if (candidateStatisticsDetail == null)
            {
                base.ShowMessage(InterviewCandidateTestDetails_topErrorMessageLabel,
               InterviewCandidateTestDetails_bottomErrorMessageLabel, "Interview not completed");
                InterviewCandidateTestDetails_topPublishInterviewImageButton.Visible = false;
                InterviewCandidateTestDetails_bottomPublishInterviewImageButton.Visible = false;
                InterviewCandidateTestDetails_interviewCompletedDateLabelValue.Text = string.Empty;
                return;
            }
            LoadCandidateStatisticsDetails(candidateStatisticsDetail);
        }

        protected static string ConvertSecondsToHoursMinutesSeconds(int seconds)
        {
            TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);

            string hoursMinutesSeconds = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                    timeSpan.Hours,
                                    timeSpan.Minutes,
                                    timeSpan.Seconds);

            return hoursMinutesSeconds;
        }

        #endregion Protected Overridden Methods
   }
}