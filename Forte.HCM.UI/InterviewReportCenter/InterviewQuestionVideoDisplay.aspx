﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InterviewQuestionVideoDisplay.aspx.cs" Inherits="Forte.HCM.UI.InterviewReportCenter.InterviewQuestionVideoDisplay" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--  BEGIN Browser History required section -->
    <link rel="stylesheet" type="text/css" href="../VideoStreaming/history/history.css"  />
    <!--  END Browser History required section -->
    <script type="text/javascript" src="../VideoStreaming/AC_OETags.js" language="javascript"></script>
    <!--  BEGIN Browser History required section -->
    <script type="text/javascript" src="../VideoStreaming/history/history.js" language="javascript"></script>
    <!--  END Browser History required section -->
    <style type="text/css">
        body
        {
            margin: 0px;
            padding:10px;
            overflow: auto;
        }
    </style>
     <script language="JavaScript" type="text/javascript">

         var requiredMajorVersion = 9;
         var requiredMinorVersion = 0;
         var requiredRevision = 28;

         // Handler for onchange and onkeyup to check for max characters.
         function CommentsCount(characterLength, clientId) {
             var maxlength = new Number(characterLength);
             if (clientId.value.length > maxlength) {
                 clientId.value = clientId.value.substring(0, maxlength);
             }
         }    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <script language="JavaScript" type="text/javascript">                                         
         // Version check for the Flash Player that has the ability to start Player Product Install (6.0r65)
         var hasProductInstall = DetectFlashVer(6, 0, 65);

         // Version check based upon the values defined in globals
         var hasRequestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);

         if (hasProductInstall && !hasRequestedVersion) {
             // DO NOT MODIFY THE FOLLOWING FOUR LINES
             // Location visited after installation is complete if installation is required
             var MMPlayerType = (isIE == true) ? "ActiveX" : "PlugIn";
             var MMredirectURL = window.location;
             document.title = document.title.slice(0, 47) + " - Flash Player Installation";
             var MMdoctitle = document.title;

             AC_FL_RunContent(
		                    "src", "playerProductInstall",
		                    "FlashVars", "URL=<%= videoURL %>&MMredirectURL=" + MMredirectURL + '&MMplayerType=' + MMPlayerType + '&MMdoctitle=' + MMdoctitle + "",
		                    "width", "186",
		                    "height", "215",
		                    "align", "middle",
		                    "id", "OFIStream",
		                    "quality", "high",
		                    "bgcolor", "#ffffff",
		                    "name", "OFIStream",
		                    "allowScriptAccess", "sameDomain",
		                    "type", "application/x-shockwave-flash",
		                    "pluginspage", "http://www.adobe.com/go/getflashplayer"
	                                        );
         } else if (hasRequestedVersion) {
             // if we've detected an acceptable version
             // embed the Flash Content SWF when all tests are passed
             AC_FL_RunContent(
			                "src", "../VideoStreaming/OFIStream",
			                "FlashVars", "URL=<%= videoURL %>&QuestionID=<%= questionID %>",
			                "width", "186",
			                "height", "215",
			                "align", "left",
			                "id", "OFIStream",
			                "quality", "high",
			                "bgcolor", "#ffffff",
			                "name", "OFIStream",
			                "allowScriptAccess", "sameDomain",
			                "type", "application/x-shockwave-flash",
			                "pluginspage", "http://www.adobe.com/go/getflashplayer"
	                                        );
         } else {  // flash is too old or we can't detect the plugin
             var alternateContent = 'Alternate HTML content should be placed here. '
  	                                        + 'This content requires the Adobe Flash Player. '
   	                                        + '<a href=http://www.adobe.com/go/getflash/>Get Flash</a>';
             document.write(alternateContent);  // insert non-flash content
         }
                                         
        </script>
        <noscript>
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="OFIStream"
                width="186px" height="215px" codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">
                <param name="movie" value="OFIStream.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#ffffff" />
                <param name="allowScriptAccess" value="sameDomain" />
                <embed src="../VideoStreaming/OFIStream.swf" quality="high" bgcolor="#ffffff"
                    width="186px" height="215px" name="OFIStream" align="middle" play="true"
                    loop="false" quality="high" allowscriptaccess="sameDomain" type="application/x-shockwave-flash"
                    pluginspage="http://www.adobe.com/go/getflashplayer">
			    </embed>
            </object>
        </noscript>
    </div>
    </form>
</body>
</html>
