﻿using System;
using System.Configuration;

namespace Forte.HCM.UI.InterviewReportCenter
{
    public partial class InterviewQuestionVideoDisplay : System.Web.UI.Page
    {
        protected string videoURL;
        protected string questionID;
        protected void Page_Load(object sender, EventArgs e)
        {

            string serverURL = ConfigurationManager.AppSettings["VIDEO_STREAMING_SERVER_URL"].ToString();
            videoURL = string.Concat(serverURL, Request.QueryString["candidatesessionid"], "_" + Request.QueryString["attemptid"]);
            questionID = Request.QueryString["questionId"].ToString();
             
        }
    }
}