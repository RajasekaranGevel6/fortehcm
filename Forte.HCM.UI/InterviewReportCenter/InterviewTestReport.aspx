<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    CodeBehind="InterviewTestReport.aspx.cs" Inherits="Forte.HCM.UI.InterviewReportCenter.InterviewTestReport" %>

<%@ Register Src="~/CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="InterviewTestReport_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="InterviewTestReport_headerLiteral" runat="server" Text="Interview Report"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="InterviewTestReport_topResetLinkButton" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                            OnClick="InterviewTestReport_resetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="InterviewTestReport_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="InterviewTestReport_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="InterviewTestReport_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="InterviewTestReport_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div id="InterviewTestReport_searchByTestDiv" runat="server" style="display: block;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="right" class="td_padding_bottom_5">
                                            <asp:UpdatePanel runat="server" ID="InterviewTestReport_simpleLinkUpdatePanel">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="InterviewTestReport_simpleLinkButton" runat="server" Text="Advanced" SkinID="sknActionLinkButton"
                                                        OnClick="InterviewTestReport_simpleLinkButton_Click" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="panel_bg">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="100%">
                                                        <asp:UpdatePanel runat="server" ID="InterviewTestReport_searchDivUpdatePanel">
                                                            <ContentTemplate>
                                                                <div id="InterviewTestReport_simpleSearchDiv" runat="server" style="width: 100%">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="InterviewTestReport_categoryHeadLabel" runat="server" Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="InterviewTestReport_categoryTextBox" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="InterviewTestReport_subjectHeadLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="InterviewTestReport_subjectTextBox" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="InterviewTestReport_keywordsHeadLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <div style="float: left; padding-right: 5px;">
                                                                                    <asp:TextBox ID="InterviewTestReport_simpleKeywordTextBox" runat="server"></asp:TextBox></div>
                                                                                <div style="float: left;">
                                                                                    <asp:ImageButton ID="InterviewTestReport_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, Search_Interview_KeywordHelp %>" /></div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div id="InterviewTestReport_advanceSearchDiv" runat="server" visible="false" style="width: 100%">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <uc1:CategorySubjectControl ID="InterviewTestReport_searchCategorySubjectControl" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_8">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="panel_inner_body_bg">
                                                                                <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="InterviewTestReport_testAreaHeadLabel" runat="server" Text="Interview Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="7" class="checkbox_list_bg" align="left" style="width: 90%">
                                                                                            <asp:CheckBoxList ID="InterviewTestReport_testAreaCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                            </asp:CheckBoxList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 10%">
                                                                                            <asp:Label ID="InterviewTestReport_complexityLabel" runat="server" Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="3" align="left" valign="middle">
                                                                                            <div style="float: left; padding-right: 5px; width: 90%; vertical-align: middle;"
                                                                                                class="checkbox_list_bg">
                                                                                                <asp:CheckBoxList ID="InterviewTestReport_complexityCheckBoxList" runat="server" RepeatColumns="3"
                                                                                                    RepeatDirection="Horizontal" CellSpacing="5" Width="100%">
                                                                                                </asp:CheckBoxList>
                                                                                            </div>
                                                                                            <div style="float: left; vertical-align: middle; padding-left: 5px; padding-top: 5px;">
                                                                                                <asp:ImageButton ID="InterviewTestReport_complexityImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, ComplexityHelp %>" />
                                                                                            </div>
                                                                                        </td>
                                                                                        <td colspan="4">
                                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td style="width: 50%">
                                                                                                        <asp:Label ID="InterviewTestReport_positionProfileLabel" runat="server" Text="Position Profile"
                                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                                            <asp:TextBox ID="InterviewTestReport_positionProfileTextBox" runat="server" ReadOnly="true"></asp:TextBox>
                                                                                                            <asp:HiddenField ID="InterviewTestReport_positionProfileIDHiddenField" runat="server" />
                                                                                                        </div>
                                                                                                        <div style="float: left;">
                                                                                                            <asp:ImageButton ID="InterviewTestReport_positionProfileImageButton" SkinID="sknbtnSearchicon"
                                                                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select the position profile" />
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="InterviewTestReport_testIdLabel" runat="server" Text="Interview ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="InterviewTestReport_testIdTextBox" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="InterviewTestReport_testNameLabel" runat="server" Text="Interview Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="InterviewTestReport_testNameTextBox" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td style="width: 8%">
                                                                                            <asp:Label ID="InterviewTestReport_keywordHeadLabel" runat="server" Text="Keyword" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="InterviewTestReport_advancedKeywordTextBox" runat="server"></asp:TextBox></div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="InterviewTestReport_keywordImageButton" SkinID="sknHelpImageButton" runat="server"
                                                                                                    ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, Search_Interview_KeywordHelp %>" /></div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="InterviewTestReport_authorHeadLabel" runat="server" Text="Author" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="InterviewTestReport_authorTextBox" runat="server"></asp:TextBox>
                                                                                                <asp:HiddenField ID="InterviewTestReport_dummyAuthorID" runat="server" />
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="InterviewTestReport_authorImageButton" SkinID="sknbtnSearchicon" runat="server"
                                                                                                    ImageAlign="Middle" ToolTip="Click here to select the interview author" /></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="td_padding_top_5">
                                                        <asp:Button ID="InterviewTestReport_topSearchButton" runat="server" Text="Search" SkinID="sknButtonId"
                                                            OnClick="InterviewTestReport_topSearchButton_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr id="InterviewTestReport_searchTestResultsTR" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 98%" align="left" class="header_text_bold">
                                        <asp:Literal ID="InterviewTestReport_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                            ID="InterviewTestReport_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                    </td>
                                    <td style="width: 2%" align="right">
                                        <span id="InterviewTestReport_searchResultsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="InterviewTestReport_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                        </span><span id="InterviewTestReport_searchResultsDownSpan" runat="server" style="display: block;">
                                            <asp:Image ID="InterviewTestReport_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                        </span>
                                        <asp:HiddenField ID="InterviewTestReport_restoreHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <asp:UpdatePanel runat="server" ID="InterviewTestReport_testGridViewUpdatePanel">
                                <ContentTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left">
                                                <div style="height: 200px; overflow: auto;" runat="server" id="InterviewTestReport_testDiv"
                                                    visible="false">
                                                    <asp:GridView ID="InterviewTestReport_testGridView" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                        GridLines="Horizontal" BorderColor="White" BorderWidth="1px" Width="100%" OnSorting="InterviewTestReport_testGridView_Sorting"
                                                        OnRowDataBound="InterviewTestReport_testGridView_RowDataBound" OnRowCreated="InterviewTestReport_testGridView_RowCreated"
                                                        OnRowCommand="InterviewTestReport_testGridView_RowCommand">
                                                        <RowStyle CssClass="grid_alternate_row" />
                                                        <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                        <HeaderStyle CssClass="grid_header_row" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Interview&nbsp;ID" SortExpression="TESTKEY">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="InterviewTestReport_testIdLinkButton" CommandName="InterviewTestStatisticsInfo"
                                                                        runat="server" Text='<%# Eval("TestKey") %>'>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="9%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Interview Name" SortExpression="TESTNAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="InterviewTestReport_testNameLabel" runat="server" Text='<%# TrimContent(Eval("Name").ToString(),35) %>'
                                                                        ToolTip='<%# Eval("Name") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Author" SortExpression="USERNAME" ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="InterviewTestReport_testAuthorFullNameLabel" runat="server" Text='<%# Eval("TestAuthorName") %>'
                                                                        ToolTip='<%# Eval("TestAuthorFullName") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20%" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderStyle-CssClass="td_padding_right_20" HeaderText="No of Questions"
                                                                DataField="NoOfQuestions" SortExpression="TOTALQUESTION DESC">
                                                                <ItemStyle Width="1%" CssClass="td_padding_right_20" HorizontalAlign="right" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Created Date" SortExpression="CREATEDDATE DESC" ItemStyle-Width="60px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="InterviewTestReport_testCreatedDateLabel" runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("CreatedDate"))) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Cost (in&nbsp;$)" HeaderStyle-CssClass="td_padding_right_20"
                                                                DataField="TestCost" SortExpression="TESTCOST">
                                                                <ItemStyle Width="1%" CssClass="td_padding_right_20" HorizontalAlign="right" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Certification Interview" SortExpression="CERTIFICATION">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="InterviewTestReport_testCertificationLabel" runat="server" Text='<%# GetExpansion(Convert.ToBoolean(Eval("IsCertification")))%>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="10%" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <uc2:PageNavigator ID="InterviewTestReport_bottomPagingNavigator" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="InterviewTestReport_topSearchButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="InterviewTestReport_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="InterviewTestReport_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label><asp:Label
                            ID="InterviewTestReport_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                                ID="InterviewTestReport_authorIdHiddenField" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%">
                            &nbsp;
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="InterviewTestReport_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="InterviewTestReport_resetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="InterviewTestReport_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
