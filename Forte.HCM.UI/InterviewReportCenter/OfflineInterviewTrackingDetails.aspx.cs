﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SerachQuestion.cs
// File that represents the user Serach the Question by various Key filed.
// This will helps Search a question From the question repository.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.ReportCenter
{
    public partial class OfflineInterviewTrackingDetails : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="int"/> that holds the current page number of  desktop ThumbImage tab page
        /// /// </summary>
        private int desktopThumbImagepageNo;

        /// <summary>
        /// A <see cref="int"/> that holds the Candidate session key
        /// /// </summary>
        private string canSessionKey = string.Empty;

        /// <summary>
        /// A <see cref="int"/> that holds the Candidate cuurent attemp id
        /// /// </summary>
        private int attempID = 0;

        #endregion Private Variables                                           

        #region Event Handler                                                  

        /// <summary>
        /// Handler will get fired when the page is loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SubscriptEvents();
                GetQueryString();

                Master.SetPageCaption("Offline Interview Tracking Details");

                if (!IsPostBack)
                {
                    desktopThumbImagepageNo = 1;
                    Session["OfflineInterviewTrackingDetailsDesktopThumbImagePageNo"] = desktopThumbImagepageNo;
                    LoadDesktopThumbnails(desktopThumbImagepageNo);
                    LoadApplicationLogs(1);
                    LoadOperatingSystemEvents(1);
                    LoadCyberProctorExceptions(1);
                    LoadRunningApplications(1);
                }
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp);
                base.ShowMessage(OfflineInterviewTrackingDetails_bottomErrorMessageLabel, exp.Message);
                base.ShowMessage(OfflineInterviewTrackingDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of application logs section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void OfflineInterviewTrackingDetails_applicationLogsPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadApplicationLogs(e.PageNumber);
            }
            catch (Exception exp)
            {
                base.ShowMessage(OfflineInterviewTrackingDetails_topErrorMessageLabel,
                    OfflineInterviewTrackingDetails_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of running applications section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void OfflineInterviewTrackingDetails_runningApplicationsPageNavigator_PageNumberClick
            (object sender,PageNumberEventArgs e)
        {
            try
            {
                LoadRunningApplications(e.PageNumber);
            }
            catch (Exception exp)
            {
                base.ShowMessage(OfflineInterviewTrackingDetails_topErrorMessageLabel,
                    OfflineInterviewTrackingDetails_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of exceptions section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void OfflineInterviewTrackingDetails_exceptionPageNavigator_PageNumberClick
            (object sender,PageNumberEventArgs e)
        {
            try
            {
                LoadCyberProctorExceptions(e.PageNumber);
            }
            catch (Exception exp)
            {
                base.ShowMessage(OfflineInterviewTrackingDetails_topErrorMessageLabel,
                    OfflineInterviewTrackingDetails_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of operating system events section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void OfflineInterviewTrackingDetails_operatingSystemEventsPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadOperatingSystemEvents(e.PageNumber);
            }
            catch (Exception exp)
            {
                base.ShowMessage(OfflineInterviewTrackingDetails_topErrorMessageLabel,
                    OfflineInterviewTrackingDetails_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handle the Click Event of the OfflineInterviewTrackingDetails_DesktopNextImageButton
        /// this binds the cyberproctoring desktop thumnail images to the Datalist
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/></param>
        /// <param name="e">
        /// A<see cref="ImageClickEventArgs"/></param>
        protected void OfflineInterviewTrackingDetails_desktopCaptureNextImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            try
            {
                desktopThumbImagepageNo = (int)Session["OfflineInterviewTrackingDetailsDesktopThumbImagePageNo"];
                desktopThumbImagepageNo = desktopThumbImagepageNo + 1;
                Session["OfflineInterviewTrackingDetailsDesktopThumbImagePageNo"] = desktopThumbImagepageNo;
                LoadDesktopThumbnails(desktopThumbImagepageNo);
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp);
                base.ShowMessage(OfflineInterviewTrackingDetails_bottomErrorMessageLabel, exp.Message);
                base.ShowMessage(OfflineInterviewTrackingDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handle the Click Event of the OfflineInterviewTrackingDetails_DescktopPrevImageButton
        /// this binds the cyberproctoring desktop thumnail images to the Datalist
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/></param>
        /// <param name="e">
        /// A<see cref="ImageClickEventArgs"/></param>
        protected void OfflineInterviewTrackingDetails_desktopCapturePreviousImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            try
            {
                desktopThumbImagepageNo = (int)Session["OfflineInterviewTrackingDetailsDesktopThumbImagePageNo"];
                desktopThumbImagepageNo = desktopThumbImagepageNo - 1;
                Session["OfflineInterviewTrackingDetailsDesktopThumbImagePageNo"] = desktopThumbImagepageNo;
                LoadDesktopThumbnails(desktopThumbImagepageNo);
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp);
                base.ShowMessage(OfflineInterviewTrackingDetails_bottomErrorMessageLabel, exp.Message);
                base.ShowMessage(OfflineInterviewTrackingDetails_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handle the item databount event of the OfflineInterviewTrackingDetails_imageDatalist 
        /// is add the onclick attribute for the OfflineInterviewTrackingDetails_ScreenShotImageButton 
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/></param>
        /// <param name="e">
        /// A<see cref="DataListItemEventArgs"/></param>
        protected void OfflineInterviewTrackingDetails_desktopCaptureDatalist_ItemDataBound
            (object sender, DataListItemEventArgs e)
        {
            Image img = null;
            int deskTopPageIndex = 0;
            try
            {
                img = ((Image)e.Item.FindControl("OfflineInterviewTrackingDetails_desktopCaptureImage"));

                if (img == null)
                    return;

                HiddenField messageHiddenField = ((HiddenField)e.Item.FindControl("OfflineInterviewTrackingDetails_messageHiddenField"));

                if (messageHiddenField == null)
                    return;

                desktopThumbImagepageNo = (int)Session["OfflineInterviewTrackingDetailsDesktopThumbImagePageNo"];
                    int.TryParse(e.Item.ItemIndex.ToString(), out deskTopPageIndex);

                // Assign image handler to show zoomed image.
                img.Attributes.Add("onclick", "javascript:return OpenScreenShot('OI','" +
                    (deskTopPageIndex + ((desktopThumbImagepageNo - 1) * base.GridPageSize)).ToString() 
                    + "','SCREEN_IMG','" + canSessionKey +"','"+ attempID + "');");

                // Assign message as tooltip.
                img.ToolTip = messageHiddenField.Value;
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp);
                base.ShowMessage(OfflineInterviewTrackingDetails_bottomErrorMessageLabel, exp.Message);
                base.ShowMessage(OfflineInterviewTrackingDetails_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handler                                               

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the application logs.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadApplicationLogs(int pageNumber)
        {
            int totalRecords = 0;

            List<ApplicationLogDetail> applicationLogs = new TrackingBLManager().
                GetApplicationLogs(canSessionKey, attempID, pageNumber,
                 base.GridPageSize, out totalRecords);

            OfflineInterviewTrackingDetails_applicationLogsPageNavigator.PageSize = base.GridPageSize;
            OfflineInterviewTrackingDetails_applicationLogsPageNavigator.TotalRecords = totalRecords;

            if (applicationLogs == null || applicationLogs.Count == 0)
            {
                OfflineInterviewTrackingDetails_applicationLogsGridView.DataSource = null;
                OfflineInterviewTrackingDetails_applicationLogsGridView.DataBind();
                OfflineInterviewTrackingDetails_applicationLogsDiv.Visible = false;
                return;
            }
            OfflineInterviewTrackingDetails_applicationLogsDiv.Visible = true;
            OfflineInterviewTrackingDetails_applicationLogsGridView.DataSource = applicationLogs;
            OfflineInterviewTrackingDetails_applicationLogsGridView.DataBind();
        }

        /// <summary>
        /// To Load CyberProctor Events 
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/></param>
        private void LoadOperatingSystemEvents(int pageNumber)
        {
            int totalRecords = 0;

            List<ApplicationEventDetail> applicationEvents = new TrackingBLManager().
                GetApplicationEvents(canSessionKey, attempID, pageNumber, 
                base.GridPageSize, out totalRecords);

            OfflineInterviewTrackingDetails_operatingSystemEventsPageNavigator.PageSize = base.GridPageSize;
            OfflineInterviewTrackingDetails_operatingSystemEventsPageNavigator.TotalRecords = totalRecords;

            if (applicationEvents == null || applicationEvents.Count == 0)
            {
                OfflineInterviewTrackingDetails_operatingSystemEventsGridView.DataSource = null;
                OfflineInterviewTrackingDetails_operatingSystemEventsGridView.DataBind();
                OfflineInterviewTrackingDetails_operatingSystemEventsDiv.Visible = false;
                return;
            }
            OfflineInterviewTrackingDetails_operatingSystemEventsDiv.Visible = true;
            OfflineInterviewTrackingDetails_operatingSystemEventsGridView.DataSource = applicationEvents;
            OfflineInterviewTrackingDetails_operatingSystemEventsGridView.DataBind();
        }

        /// <summary>
        /// To Load CyberProctor Exceptions
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/></param>
        private void LoadCyberProctorExceptions(int pageNumber)
        {
            int totalRecords = 0;
            List<ApplicationExceptionDetail> applicationExceptions = new TrackingBLManager().
                GetApplicationExceptions(canSessionKey, attempID, pageNumber, base.GridPageSize,
                out totalRecords);

            OfflineInterviewTrackingDetails_exceptionsPageNavigator.PageSize = base.GridPageSize;
            OfflineInterviewTrackingDetails_exceptionsPageNavigator.TotalRecords = totalRecords;

            if (applicationExceptions == null || applicationExceptions.Count == 0)
            {
                OfflineInterviewTrackingDetails_exceptionsGridView.DataSource = applicationExceptions;
                OfflineInterviewTrackingDetails_exceptionsGridView.DataBind();
                OfflineInterviewTrackingDetails_exceptionsGridView.Visible = true;
                return;
            }

            OfflineInterviewTrackingDetails_exceptionsGridView.Visible = true;
            OfflineInterviewTrackingDetails_exceptionsGridView.DataSource = applicationExceptions;
            OfflineInterviewTrackingDetails_exceptionsGridView.DataBind();
        }

        /// <summary>
        /// to Load Desktop Thumbnails
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/></param>
        private void LoadDesktopThumbnails(int pageNumber)
        {
            int totalRec = 0;
            List<TrackingImageDetail> trackingImages =
                new TrackingBLManager().GetOfflineInterviewTrackingImageDetails(
                canSessionKey, attempID, "SCREEN_IMG", pageNumber, 
                base.GridPageSize, out totalRec);

            OfflineInterviewTrackingDetails_desktopCaptureDatalist.DataSource = trackingImages;
            OfflineInterviewTrackingDetails_desktopCaptureDatalist.DataBind();

            if (pageNumber > 1)
                OfflineInterviewTrackingDetails_desktopCapturePreviousImageButton.Visible = true;
            else
                OfflineInterviewTrackingDetails_desktopCapturePreviousImageButton.Visible = false;

            if ((pageNumber * base.GridPageSize) >= totalRec)
                OfflineInterviewTrackingDetails_desktopCaptureNextImageButton.Visible = false;
            else
                OfflineInterviewTrackingDetails_desktopCaptureNextImageButton.Visible = true;
        }

        /// <summary>
        /// to Load Existing Applications
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/></param>
        private void LoadRunningApplications(int pageNumber)
        {
            int totalRecords = 0;
            List<ApplicationDetail> runningApplications = null;
            runningApplications = new TrackingBLManager().GetRunningApplications(
                canSessionKey, attempID, pageNumber, base.GridPageSize, out totalRecords);

            OfflineInterviewTrackingDetails_runningApplicationsPageNavigator.PageSize = base.GridPageSize;
            OfflineInterviewTrackingDetails_runningApplicationsPageNavigator.TotalRecords = totalRecords;
            if (runningApplications == null || runningApplications.Count == 0)
            {
                OfflineInterviewTrackingDetails_runningApplicationsGridView.DataSource = null;
                OfflineInterviewTrackingDetails_runningApplicationsGridView.DataBind();
                OfflineInterviewTrackingDetails_runningApplicationsDiv.Visible = false;
                return;
            }

            OfflineInterviewTrackingDetails_runningApplicationsDiv.Visible = true;
            OfflineInterviewTrackingDetails_runningApplicationsGridView.DataSource = runningApplications;
            OfflineInterviewTrackingDetails_runningApplicationsGridView.DataBind();
        }
        
        /// <summary>
        /// to Subscript the Events
        /// </summary>
        private void SubscriptEvents()
        {
            OfflineInterviewTrackingDetails_applicationLogsPageNavigator.PageNumberClick +=
                new PageNavigator.PageNumberClickEventHandler
                (OfflineInterviewTrackingDetails_applicationLogsPageNavigator_PageNumberClick);
            OfflineInterviewTrackingDetails_operatingSystemEventsPageNavigator.PageNumberClick +=
                new PageNavigator.PageNumberClickEventHandler
                (OfflineInterviewTrackingDetails_operatingSystemEventsPageNavigator_PageNumberClick);
            OfflineInterviewTrackingDetails_exceptionsPageNavigator.PageNumberClick += 
                new PageNavigator.PageNumberClickEventHandler(
                OfflineInterviewTrackingDetails_exceptionPageNavigator_PageNumberClick);
            OfflineInterviewTrackingDetails_runningApplicationsPageNavigator.PageNumberClick +=
                new PageNavigator.PageNumberClickEventHandler(
                OfflineInterviewTrackingDetails_runningApplicationsPageNavigator_PageNumberClick);
        }
        
        /// <summary>
        /// to read the query string
        /// </summary>
        private void GetQueryString()
        {
            if (Request.QueryString["candidatesession"] == null)
                throw new Exception(Resources.HCMResource.OfflineInterviewTrackingDetails_QueryStringNotFound);
            canSessionKey = canSessionKey = Request.QueryString["candidatesession"];
            if (Request.QueryString["attemptid"] == null)
                throw new Exception(Resources.HCMResource.OfflineInterviewTrackingDetails_QueryStringNotFound);
            int.TryParse(Request.QueryString["attemptid"], out attempID);
        }

        #endregion Private Methods                                             

        #region Overridden Methods                                             

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Overridden Methods                                          
    }
}
