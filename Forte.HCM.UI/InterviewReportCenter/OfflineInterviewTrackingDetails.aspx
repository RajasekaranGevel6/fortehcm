<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OfflineInterviewTrackingDetails.aspx.cs"
    MasterPageFile="~/MasterPages/InterviewMaster.Master" Inherits="Forte.HCM.UI.ReportCenter.OfflineInterviewTrackingDetails" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="OfflineInterviewTrackingDetails_bodyContent" ContentPlaceHolderID="InterviewMaster_body"
    runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="OfflineInterviewTrackingDetails_headerLiteral" runat="server" Text="Offline Interview Tracking Details"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td class="td_padding_5">
                                        <asp:LinkButton ID="OfflineInterviewTrackingDetails_topCancelButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="OfflineInterviewTrackingDetails_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="OfflineInterviewTrackingDetails_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <ajaxToolKit:TabContainer ID="OfflineInterviewTrackingDetails_mainTabContainer" runat="server" Width="100%"
                    ActiveTabIndex="0" CssClass="">
                    <ajaxToolKit:TabPanel ID="OfflineInterviewTrackingDetails_logsTabPanel" HeaderText="Application Logs"
                        runat="server">
                        <HeaderTemplate>
                            Application Logs
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="OfflineInterviewTrackingDetails_applicationLogsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <div style="height: 300px; overflow: auto;" id="OfflineInterviewTrackingDetails_applicationLogsDiv"
                                                    runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="OfflineInterviewTrackingDetails_applicationLogsGridView" runat="server" AutoGenerateColumns="False"
                                                                    Width="100%" AllowSorting="False">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Message" HeaderText="Message" />
                                                                        <asp:BoundField DataField="Date" HeaderText="Date & Time" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div>
                                                    <uc1:PageNavigator ID="OfflineInterviewTrackingDetails_applicationLogsPageNavigator" runat="server" />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="OfflineInterviewTrackingDetails_operatingSystemEventsTabPanel" HeaderText="Operating System Events"
                        runat="server">
                        <HeaderTemplate>
                            Operating System Events
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="OfflineInterviewTrackingDetails_operatingSystemEventsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <div style="height: 300px; overflow: auto;" id="OfflineInterviewTrackingDetails_operatingSystemEventsDiv"
                                                    runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="OfflineInterviewTrackingDetails_operatingSystemEventsGridView" runat="server" AutoGenerateColumns="False"
                                                                    Width="100%" AllowSorting="False">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="ApplicationName" HeaderText="Application Name" />
                                                                        <asp:BoundField DataField="ApplicationPath" HeaderText="File Path" />
                                                                        <asp:BoundField DataField="EventStatus" HeaderText="Status" />
                                                                        <asp:BoundField DataField="Date" HeaderText="Date & Time" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div>
                                                    <uc1:PageNavigator ID="OfflineInterviewTrackingDetails_operatingSystemEventsPageNavigator" runat="server" />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="OfflineInterviewTrackingDetails_desktopCaptureTabPanel" runat="server">
                        <HeaderTemplate>
                            Desktop Capture
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="OfflineInterviewTrackingDetails_desktopCaptureUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <div id="OfflineInterviewTrackingDetails_desktopCaptureDiv" runat="server">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td class="td_height_5" colspan="3">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 6%" align="center">
                                                    <asp:ImageButton ID="OfflineInterviewTrackingDetails_desktopCapturePreviousImageButton" runat="server" SkinID="sknViewPreviousImageButton"
                                                        ToolTip="Previous" OnClick="OfflineInterviewTrackingDetails_desktopCapturePreviousImageButton_Click" Style="width: 14px;
                                                        height: 16px" />
                                                </td>
                                                <td style="width: 88%; height: 260px;" align="center" valign="top">
                                                    <asp:DataList ID="OfflineInterviewTrackingDetails_desktopCaptureDatalist" runat="server" RepeatColumns="5"
                                                        RepeatLayout="Table" GridLines="None" RepeatDirection="Horizontal" Width="100%"
                                                        OnItemDataBound="OfflineInterviewTrackingDetails_desktopCaptureDatalist_ItemDataBound">
                                                        <ItemTemplate>
                                                            <div style="float: left; height: 140px; width: 95%;">
                                                                <table width="95%" cellpadding="0" cellspacing="5" border="0" align="center">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Image ID="OfflineInterviewTrackingDetails_desktopCaptureImage" runat="server" Height="120px"
                                                                                Style="cursor: pointer" Width="120px" ImageUrl='<%# "../Common/ImageHandler.ashx?source=OI&ImageID="+ Eval("ImageID") + "&isThumb=1"  %>' />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <asp:HiddenField ID="OfflineInterviewTrackingDetails_messageHiddenField" Value='<% #Eval("Message") %>' runat="server"/>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </td>
                                                <td style="width: 6%" align="center">
                                                    <asp:ImageButton ID="OfflineInterviewTrackingDetails_desktopCaptureNextImageButton" runat="server" SkinID="sknViewNextImageButton"
                                                        ToolTip="Next" OnClick="OfflineInterviewTrackingDetails_desktopCaptureNextImageButton_Click" Style="width: 14px;
                                                        height: 16px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="OfflineInterviewTrackingDetails_exceptionsTabPanel" HeaderText="Exceptions" runat="server">
                        <HeaderTemplate>
                            Exceptions
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="OfflineInterviewTrackingDetails_exceptionsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <div style="height: 300px; overflow: auto;" id="OfflineInterviewTrackingDetails_exceptionsDiv"
                                                    runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="OfflineInterviewTrackingDetails_exceptionsGridView" runat="server" AutoGenerateColumns="False"
                                                                    Width="100%" AllowSorting="False">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Exception" HeaderText="Exception" />
                                                                        <asp:BoundField DataField="Date" HeaderText="Date and Time" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div>
                                                    <uc1:PageNavigator ID="OfflineInterviewTrackingDetails_exceptionsPageNavigator" runat="server" />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="OfflineInterviewTrackingDetails_runningApplicationsTabPanel" HeaderText="Search By Interview"
                        runat="server">
                        <HeaderTemplate>
                            Running Applications
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table width="100%" cellpadding="0" cellspacing="0" runat="server">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="OfflineInterviewTrackingDetails_runningApplicationsUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <div style="height: 300px; overflow: auto;" id="OfflineInterviewTrackingDetails_runningApplicationsDiv"
                                                    runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="OfflineInterviewTrackingDetails_runningApplicationsGridView" runat="server" AutoGenerateColumns="False"
                                                                    Width="100%" AllowSorting="False">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Application" HeaderText="Process Name" />
                                                                        <asp:BoundField DataField="ExePath" HeaderText="File Path" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div>
                                                    <uc1:PageNavigator ID="OfflineInterviewTrackingDetails_runningApplicationsPageNavigator" runat="server" />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                </ajaxToolKit:TabContainer>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="OfflineInterviewTrackingDetails_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="OfflineInterviewTrackingDetails_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="header_bg" align="right">
                <table width="100%" cellpadding="0" cellspacing="0" align="right" border="0">
                    <tr>
                        <td style="width: 50%;">
                        </td>
                        <td style="width: 50%;" align="right">
                            <asp:LinkButton ID="OfflineInterviewTrackingDetails_bottomCancelButton" runat="server" Text="Cancel"
                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
