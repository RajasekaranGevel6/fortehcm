<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    CodeBehind="InterviewTestStatisticsInfo.aspx.cs" Inherits="Forte.HCM.UI.InterviewReportCenter.InterviewTestStatisticsInfo" %>

<%@ Register Src="../CommonControls/GeneralInterviewTestStatisticsControl.ascx" TagName="GeneralInterviewTestStatisticsControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/QuestionStatisticsControl.ascx" TagName="QuestionStatisticsControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="InterviewTestStatisticsInfo_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        function ExpORCollapseQuestRows(targetControl) {

            var ctrl = document.getElementById('<%=InterviewTestStatisticsInfo_questionStatisticsStateExpandHiddenField.ClientID %>');
            targetValue = ctrl.value;
            if (targetValue == "0") {
                targetControl.innerHTML = "Collapse All";
                ExpandAllQuestRows(targetControl);
                ctrl.value = 1;
            }
            else {
                targetControl.innerHTML = "Expand All";
                CollapseAllRows(targetControl);
                ctrl.value = 0;
            }
            return false;
        }


        function ExpandAllQuestRows(targetControl) {
            var gridCtrl = document.getElementById("<%= InterviewTestStatisticsInfo_questionStatistics_questionDiv.ClientID %>");
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (rowItems[indexRow].id.indexOf("InterviewTestStatisticsInfo_questionStatisticsGridView_detailsDiv") != "-1") {
                        rowItems[indexRow].style.display = "block";
                    }
                }
            }
            return false;
        }
        function CollapseAllRows(targetControl) {
            var gridCtrl = document.getElementById("<%= InterviewTestStatisticsInfo_questionStatistics_questionDiv.ClientID %>");
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (rowItems[indexRow].id.indexOf("InterviewTestStatisticsInfo_questionStatisticsGridView_detailsDiv") != "-1") {
                        rowItems[indexRow].style.display = "none";
                    }
                }
            }
            var linkcontrol = document.getElementById
            ("<%=InterviewTestStatisticsInfo_questionStatisticsTestDraftExpandLinkButton.ClientID %>");
            if (linkcontrol.innerHTML = "Collapse All") {
                linkcontrol.innerHTML = "Expand All";
                var ctrl = document.getElementById('<%=InterviewTestStatisticsInfo_questionStatisticsStateExpandHiddenField.ClientID %>');
                ctrl.value = 0;

            }

            return false;
        }

        function HideQuestDetails(ctrlId) {
            if (document.getElementById(ctrlId).style.display == "none") {
                CollapseAllRows(ctrlId);
                document.getElementById(ctrlId).style.display = "block";
            }
            else {
                CollapseAllRows(ctrlId);
                document.getElementById(ctrlId).style.display = "none";
            }
            return false;
        }

        function GetMouseClickedPos(ctrlID) {
            if (document.getElementById(ctrlID) != null) {
                document.getElementById(ctrlID).focus();
            }
            return true;
        }

        // Handler method that will be called when the maximize or restore
        // button is clicked. This will maximize or restore the panel to
        // show or hide full view.
        function ExpandOrRestore(ctrlExpand, ctrlHide, expandImage, compressImage,
            isMaximizedHidden, restoredHeight, expandedHeight) {
            if (document.getElementById(ctrlExpand) != null) {
                document.getElementById(expandImage).style.display = "none";
                document.getElementById(compressImage).style.display = "none";
                if (document.getElementById(ctrlExpand).style.height.toString() == restoredHeight) {
                    document.getElementById(ctrlExpand).style.height = expandedHeight;
                    document.getElementById(ctrlHide).style.display = "none";
                    document.getElementById(expandImage).style.display = "block";
                    document.getElementById(isMaximizedHidden).value = "Y";
                }
                else {
                    document.getElementById(ctrlExpand).style.height = restoredHeight;
                    document.getElementById(ctrlHide).style.display = "block";
                    document.getElementById(compressImage).style.display = "block";
                    document.getElementById(isMaximizedHidden).value = "N";
                }
            }
            return false;
        }

        function selectedCanidates(a, b) {
            try {
                var candidates = document.getElementById('<%=InterviewTestStatisticsInfo_selectedcanidates.ClientID %>').value;
                if (a.checked) {
                    if (candidates.length == 0)
                        candidates = b;
                    else
                        candidates = candidates + "," + b;
                }
                else {
                    candidates = candidates.replace(b, "");
                }
                document.getElementById('<%=InterviewTestStatisticsInfo_selectedcanidates.ClientID %>').value = candidates;
            }
            catch (exp) {
               // alert(exp);
            }
        }
    </script>
    <asp:HiddenField ID="InterviewTestStatisticsInfo_selectedcanidates" runat="server" />
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="InterviewTestStatisticsInfo_headerLiteral" runat="server" Text="Interview Statistics Information"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td width="18%" align="right">
                                        <asp:LinkButton ID="InterviewTestStatisticsInfo_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="InterviewTestStatisticsInfo_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="InterviewTestStatisticsInfo_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="InterviewTestStatisticsInfo_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <div id="InterviewTestStatisticsInfo_searchByTestSessionDiv" runat="server" style="display: block;">
                    <table width="90%" border="0" cellspacing="3" cellpadding="0">
                        <tr>
                            <td style="width: 7%">
                                <asp:Label ID="InterviewTestStatisticsInfo_testIdHeadLabel" runat="server" Text="Interview ID"
                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="InterviewTestStatisticsInfo_testIdLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                            </td>
                            <td style="width: 10%">
                                <asp:Label ID="InterviewTestStatisticsInfo_testNameHeadLabel" runat="server" Text="Interview Name"
                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="InterviewTestStatisticsInfo_testNameLabel" runat="server" Text="Net Interview - 3 years"
                                    SkinID="sknLabelFieldText"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="height: 100%; overflow: auto; display: block;" runat="server" id="InterviewTestStatisticsInfo_testSessionDiv">
                    <ajaxToolKit:TabContainer ID="InterviewTestStatisticsInfo_mainTabContainer" runat="server"
                        Width="100%" ActiveTabIndex="0">
                        <ajaxToolKit:TabPanel ID="InterviewTestStatisticsInfo_generalTabPanel" HeaderText="Search By Interview"
                            runat="server">
                            <HeaderTemplate>
                                General Statistics
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <uc1:GeneralInterviewTestStatisticsControl ID="InterviewTestStatisticsInfo_generalTestStatisticsControl"
                                                runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                        <ajaxToolKit:TabPanel ID="InterviewTestStatisticsInfo_questionTabPanel" runat="server">
                            <HeaderTemplate>
                                Question Statistics
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:UpdatePanel ID="InterviewTestStatisticsInfo_questionStatisticsUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:LinkButton ID="InterviewTestStatisticsInfo_questionStatisticsTestDraftExpandLinkButton"
                                                                                runat="server" Text="Expand All" SkinID="sknActionLinkButton" OnClientClick="javascript:return ExpORCollapseQuestRows(this);"></asp:LinkButton>
                                                                            <asp:HiddenField ID="InterviewTestStatisticsInfo_questionStatisticsStateExpandHiddenField"
                                                                                runat="server" Value="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <div style="height: 320px; overflow: auto;" runat="server" id="InterviewTestStatisticsInfo_questionStatistics_questionDiv">
                                                                                <asp:GridView ID="InterviewTestStatisticsInfo_questionStatisticsGridView" runat="server" AllowSorting="true"
                                                                                    SkinID="sknWrapHeaderGrid" AutoGenerateColumns="false" Width="100%" OnSorting="InterviewTestStatisticsInfo_questionStatisticsGridView_Sorting"
                                                                                    OnRowDataBound="InterviewTestStatisticsInfo_questionStatisticsGridView_RowDataBound" OnRowCreated="InterviewTestStatisticsInfo_questionStatisticsGridView_RowCreated">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Question ID" Visible="false">
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="InterviewTestStatisticsInfo_questionStatisticsGridView_questionIdGridLinkButton"
                                                                                                    runat="server" Text='<%# Eval("QuestionKey") %>' ToolTip="Question Details"></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Font-Underline="False" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Question" SortExpression="Question DESC" HeaderStyle-CssClass="td_padding_right_20"
                                                                                            ItemStyle-CssClass="td_padding_right_20" ItemStyle-Width="350px">
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="InterviewTestStatisticsInfo_questionStatisticsGridView_questionGridLinkButton"
                                                                                                    runat="server"></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--<asp:TemplateField HeaderText="Avg&nbsp;Time&nbsp;Taken (Within Interview in Sec)" SortExpression="LocalTime"
                                                                                            ItemStyle-CssClass="td_padding_right_20" HeaderStyle-CssClass="td_padding_right_20"
                                                                                            ItemStyle-HorizontalAlign="right" ItemStyle-Width="14%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="InterviewTestStatisticsInfo_questionStatisticsGridView_WithinTest" runat="server"
                                                                                                    Text='<%# GetCorrectDate(Convert.ToDecimal(Eval("AverageTimeTakenWithinTest"))) %>'>
                                                                                                </asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>--%>
                                                                                        <%--<asp:TemplateField HeaderText="Avg&nbsp;Time&nbsp;Taken (Across All Interview in Sec)"
                                                                                            SortExpression="GlobalTime" ItemStyle-CssClass="td_padding_right_20" HeaderStyle-CssClass="td_padding_right_20"
                                                                                            ItemStyle-HorizontalAlign="right" ItemStyle-Width="16%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="InterviewTestStatisticsInfo_questionStatisticsGridView_acrossTest" runat="server"
                                                                                                    Text='<%# GetCorrectDate(Convert.ToDecimal(Eval("AverageTimeTakenAcrossTest"))) %>'>
                                                                                                </asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>--%>
<%--                                                                                        <asp:BoundField HeaderText="Right Answer : Administered" DataField="Ratio" HeaderStyle-CssClass="td_padding_right_20"
                                                                                            SortExpression="Ratio" ItemStyle-HorizontalAlign="center" ItemStyle-Width="15%" />
--%>                                                                                        <asp:TemplateField HeaderText="Author" SortExpression="Author" ItemStyle-Width="16%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="InterviewTestStatisticsInfo_questionStatisticsGridView_testAuthorNameLabel"
                                                                                                    runat="server"></asp:Label>
                                                                                                <tr>
                                                                                                    <td colspan="8">
                                                                                                        <div id="InterviewTestStatisticsInfo_questionStatisticsGridView_detailsDiv" runat="server"
                                                                                                            style="display: none;">
                                                                                                            <table border="0" cellpadding="3" cellspacing="3" class="table_outline_bg" width="850px">
                                                                                                                <tr>
                                                                                                                    <td class="popup_question_icon" style="word-wrap: break-word;white-space:normal;">
                                                                                                                        <asp:Label ID="InterviewTestStatisticsInfo_questionStatisticsGridView_questionLabel" runat="server"
                                                                                                                            SkinID="sknLabelFieldMultiText"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="td_padding_left_20">
                                                                                                                        <asp:PlaceHolder ID="InterviewTestStatisticsInfo_questionStatisticsGridView_answerChoicesPlaceHolder"
                                                                                                                            runat="server"></asp:PlaceHolder>
                                                                                                                        <asp:HiddenField ID="CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="td_padding_10">
                                                                                                                        <table border="0" cellpadding="0" cellspacing="4" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td style="width: 8%">
                                                                                                                                    <asp:Label ID="InterviewTestStatisticsInfo_questionStatisticsGridView_categoryLabel" runat="server"
                                                                                                                                        SkinID="sknLabelFieldHeaderText" Text="Category"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 15%">
                                                                                                                                    <asp:Label ID="InterviewTestStatisticsInfo_questionStatisticsGridView_categoryValueLabel"
                                                                                                                                        runat="server" ReadOnly="true" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 6%">
                                                                                                                                    <asp:Label ID="InterviewTestStatisticsInfo_questionStatisticsGridView_subjectLabel" runat="server"
                                                                                                                                        SkinID="sknLabelFieldHeaderText" Text="Subject"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 20%">
                                                                                                                                    <asp:Label ID="InterviewTestStatisticsInfo_questionStatisticsGridView_subjectValueLabel" runat="server"
                                                                                                                                        ReadOnly="true" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 8%">
                                                                                                                                    <asp:Label ID="InterviewTestStatisticsInfo_questionStatisticsGridView_testAreaLabel" runat="server"
                                                                                                                                        SkinID="sknLabelFieldHeaderText" Text="Interview Area"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 12%">
                                                                                                                                    <asp:Label ID="InterviewTestStatisticsInfo_questionStatisticsGridView_testAreaValueLabel"
                                                                                                                                        runat="server" ReadOnly="true" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 10%">
                                                                                                                                    <asp:Label ID="InterviewTestStatisticsInfo_questionStatisticsGridView_complexityLabel" runat="server"
                                                                                                                                        SkinID="sknLabelFieldHeaderText" Text="Complexity"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 21%">
                                                                                                                                    <asp:Label ID="InterviewTestStatisticsInfo_questionStatisticsGridView_complexityValueLabel"
                                                                                                                                        runat="server" ReadOnly="true" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                        <%-- popup DIV --%>
                                                                                                        <a href="#SearchQuestion_focusmeLink" id="InterviewTestStatisticsInfo_focusDownLink" runat="server">
                                                                                                        </a><a href="#" id="InterviewTestStatisticsInfo_focusmeLink" runat="server"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                        <ajaxToolKit:TabPanel ID="InterviewTestStatisticsInfo_candidateTabPanel" runat="server">
                            <HeaderTemplate>
                                Candidate Statistics
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td class="msg_align">
                                            <asp:Label ID="InterviewTestStatisticsInfo_candidateTabPanelErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_5">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="panel_bg">
                                            <div id="InterviewTestStatisticsInfo_candidateTabPanel_searchCriteriasDiv" runat="server"
                                                style="display: block;">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td>
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="panel_inner_body_bg">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="InterviewTestStatisticsInfo_candidateTabPanel_candidateNameLabel" runat="server"
                                                                            Text="Candidate Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="InterviewTestStatisticsInfo_candidateTabPanel_candidateNameTextBox" runat="server"
                                                                                Text="" MaxLength="50"></asp:TextBox></div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="InterviewTestStatisticsInfo_candidateTabPanel_candidateNameImageButton"
                                                                                SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" /></div>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="InterviewTestStatisticsInfo_candidateTabPanel_createdByLabel" runat="server"
                                                                            Text="Interview<br>Interview Session Creator" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="InterviewTestStatisticsInfo_candidateTabPanel_createdByTextBox" runat="server"
                                                                                Text="" MaxLength="50">
                                                                            </asp:TextBox>
                                                                            <asp:HiddenField ID="InterviewTestStatisticsInfo_candidateTabPanel_dummyCreatedBy" runat="server" />
                                                                            <asp:HiddenField ID="InterviewTestStatisticsInfo_candidateTabPanel_createdBy_HiddenField"
                                                                                runat="server" />
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="InterviewTestStatisticsInfo_candidateTabPanel_createdByImageButton" SkinID="sknbtnSearchicon"
                                                                                runat="server" ImageAlign="Middle" /></div>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="InterviewTestStatisticsInfo_candidateTabPanel_scheduledByHeadLabel" runat="server"
                                                                            Text="Interview<br>Schedule Creator" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="InterviewTestStatisticsInfo_candidateTabPanel_scheduledByTextBox" runat="server"
                                                                                MaxLength="50"></asp:TextBox></div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="InterviewTestStatisticsInfo_candidateTabPanel_scheduledByImageButton"
                                                                                SkinID="sknbtnSearchicon" runat="server" ImageAlign="Middle" /></div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2" colspan="6">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="InterviewTestStatisticsInfo_candidateTabPanel_testDateHeadLabel" runat="server"
                                                                            Text="Interview Date From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td style="width: 40%; padding-right: 2px">
                                                                                    <asp:TextBox ID="InterviewTestStatisticsInfo_candidateTabPanel_testDateTextBox" runat="server"
                                                                                        MaxLength="10" AutoCompleteType="None" Columns="17"></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:ImageButton ID="InterviewTestStatisticsInfo_candidateTabPanel_calendarImageButton" SkinID="sknCalendarImageButton"
                                                                                        runat="server" ImageAlign="Middle" />
                                                                                </td>
                                                                                <td>
                                                                                    <ajaxToolKit:MaskedEditExtender ID="InterviewTestStatisticsInfo_candidateTabPanel_maskedEditExtender"
                                                                                        runat="server" TargetControlID="InterviewTestStatisticsInfo_candidateTabPanel_testDateTextBox"
                                                                                        Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                        OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                        ErrorTooltipEnabled="True" />
                                                                                    <ajaxToolKit:MaskedEditValidator ID="InterviewTestStatisticsInfo_candidateTabPanel_maskedEditValidator"
                                                                                        runat="server" ControlExtender="InterviewTestStatisticsInfo_candidateTabPanel_maskedEditExtender"
                                                                                        ControlToValidate="InterviewTestStatisticsInfo_candidateTabPanel_testDateTextBox" EmptyValueMessage="Date is required"
                                                                                        InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                        EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                                    <ajaxToolKit:CalendarExtender ID="InterviewTestStatisticsInfo_candidateTabPanel_customCalendarExtender"
                                                                                        runat="server" TargetControlID="InterviewTestStatisticsInfo_candidateTabPanel_testDateTextBox"
                                                                                        CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="InterviewTestStatisticsInfo_candidateTabPanel_calendarImageButton" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="InterviewTestStatisticsInfo_candidateTabPanel_testToDateHeadLabel" runat="server"
                                                                            Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td style="width: 40%; padding-right: 2px">
                                                                                    <asp:TextBox ID="InterviewTestStatisticsInfo_candidateTabPanel_testToDateTextBox" runat="server"
                                                                                        MaxLength="10" AutoCompleteType="None" Columns="17"></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:ImageButton ID="InterviewTestStatisticsInfo_candidateTabPanel_calendarToImageButton"
                                                                                        SkinID="sknCalendarImageButton" runat="server" ImageAlign="Middle" />
                                                                                </td>
                                                                                <td>
                                                                                    <ajaxToolKit:MaskedEditExtender ID="InterviewTestStatisticsInfo_candidateTabPanel_MaskedEditExtender1"
                                                                                        runat="server" TargetControlID="InterviewTestStatisticsInfo_candidateTabPanel_testToDateTextBox"
                                                                                        Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                        OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                        ErrorTooltipEnabled="True" />
                                                                                    <ajaxToolKit:MaskedEditValidator ID="InterviewTestStatisticsInfo_candidateTabPanel_toDateMaskedEditValidator"
                                                                                        runat="server" ControlExtender="InterviewTestStatisticsInfo_candidateTabPanel_MaskedEditExtender"
                                                                                        ControlToValidate="InterviewTestStatisticsInfo_candidateTabPanel_testToDateTextBox" EmptyValueMessage="Date is required"
                                                                                        InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                        EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                                    <ajaxToolKit:CalendarExtender ID="InterviewTestStatisticsInfo_candidateTabPanel_customCalendarExtender1"
                                                                                        runat="server" TargetControlID="InterviewTestStatisticsInfo_candidateTabPanel_testToDateTextBox"
                                                                                        CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="InterviewTestStatisticsInfo_candidateTabPanel_calendarToImageButton" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2" colspan="6">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <table border="0" cellpadding="0" cellspacing="5" align="right">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Button ID="InterviewTestStatisticsInfo_candidateTabPanel_searchButton" runat="server"
                                                                            SkinID="sknButtonId" Text="Search" OnClick="InterviewTestStatisticsInfo_candidateTabPanel_searchButton_Click" />
                                                                    </td>
                                                                    <td class="link_button">
                                                                        |
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="InterviewTestStatisticsInfo_candidateTabPanel_resetLinkButton" runat="server"
                                                                            SkinID="sknActionLinkButton" Text="Reset" OnClick="InterviewTestStatisticsInfo_candidateTabPanel_resetLinkButton_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_5">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="header_bg" align="center">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr id="InterviewTestStatisticsInfo_candidateTabPanel_searchCandidateResultsTR" runat="server">
                                                    <td style="width: 50%" align="left">
                                                        <asp:Literal ID="InterviewTestStatisticsInfo_candidateTabPanel_searchResultsLiteral" runat="server"
                                                            Text="Candidate List"></asp:Literal>&nbsp;<asp:Label ID="CandidateStatisticsControl_sortHelpLabel"
                                                                runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 50%" align="right">
                                                        <span id="InterviewTestStatisticsInfo_candidateTabPanel_searchResultsUpSpan" runat="server"
                                                            style="display: none;">
                                                            <asp:Image ID="InterviewTestStatisticsInfo_candidateTabPanel_searchResultsUpImage" runat="server"
                                                                SkinID="sknMinimizeImage" /></span><span id="InterviewTestStatisticsInfo_candidateTabPanel_searchResultsDownSpan"
                                                                    runat="server" style="display: block;"><asp:Image ID="InterviewTestStatisticsInfo_candidateTabPanel_searchResultsDownImage"
                                                                        runat="server" SkinID="sknMaximizeImage" /></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="grid_body_bg">
                                            <asp:UpdatePanel ID="TestStatistics_candidateDetailsUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr id="TestStatistics_candidateDetailsReportLinkTR" runat="server" visible="false">
                                                            <td align="right" class="td_height_20" valign="middle">
                                                                <%--<table align="right" border="0" cellspacing="2" cellpadding="2">
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:LinkButton ID="InterviewTestStatisticsInfo_candidateTabPanel_groupAnalysisLinkButton"
                                                                                runat="server" Text="Group Analysis Report" SkinID="sknActionLinkButton" CommandName="Group"
                                                                                OnClick="InterviewTestStatisticsInfo_candidateTabPanel_groupAnalysisReportLinkButton_Click" />
                                                                        </td>
                                                                        <td class="link_button" align="center">
                                                                            |
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:LinkButton ID="InterviewTestStatisticsInfo_candidateTabPanel_comparisonReportLinkButton"
                                                                                runat="server" Text="Comparison Report" SkinID="sknActionLinkButton" CommandName="Comparison"
                                                                                OnClick="InterviewTestStatisticsInfo_candidateTabPanel_comparisonReportLinkButton_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <table border="0" cellspacing="0" cellpadding="0" width="100%" style="table-layout: fixed">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="height: 200px; overflow: auto" runat="server" id="InterviewTestStatisticsInfo_candidateTabPanel_questionDiv"
                                                                                visible="false">
                                                                                <asp:GridView ID="InterviewTestStatisticsInfo_candidateTabPanel_candidateGridView" runat="server"
                                                                                    AllowSorting="true" AutoGenerateColumns="false" Width="100%" SkinID="sknWrapHeaderGrid"
                                                                                    OnSorting="InterviewTestStatisticsInfo_candidateTabPanel_candidateGridView_Sorting" OnRowDataBound="InterviewTestStatisticsInfo_candidateTabPanel_candidateGridView_RowDataBound"
                                                                                    OnRowCreated="InterviewTestStatisticsInfo_candidateTabPanel_candidateGridView_RowCreated"
                                                                                    OnRowCommand="InterviewTestStatisticsInfo_candidateTabPanel_candidateGridView_RowCommand">
                                                                                    <Columns>
                                                                                        <asp:TemplateField ItemStyle-Wrap="false">
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="InterviewTestStatisticsInfo_candidateTabPanel_candidateGridView_selectCheckBox"
                                                                                                    runat="server" />&nbsp;
                                                                                                <asp:ImageButton ID="InterviewTestStatisticsInfo_candidateTabPanel_candidateGridView_designReportImageButton"
                                                                                                    runat="server" ToolTip="Design Report" SkinID="sknDesignReportImageButton" CommandName="DesignReport"
                                                                                                    CommandArgument="<%# Container.DataItemIndex %>" Visible="false" />
                                                                                                <asp:HyperLink ID="InterviewTestStatisticsInfo_viewInterviewSummaryHyperLink" runat="server"
                                                                                                    Target="_blank" ToolTip="View Interview Summary" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_test_result.gif">
                                                                                                </asp:HyperLink>
                                                                                                <asp:HyperLink ID="InterviewTestStatisticsInfo_candidateRatingSummaryHyperLink" runat="server"
                                                                                                    Target="_blank" ToolTip="Candidate Rating Summary" ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif">
                                                                                                </asp:HyperLink>
                                                                                                <asp:ImageButton ID="InterviewTestStatisticsInfo_candidateTabPanel_candidateGridView_trackingImageButton"
                                                                                                    runat="server" ToolTip="Offline Interview Tracking Details" SkinID="sknTrackingImageButton"
                                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="OfflineInterviewTrackingDetails"
                                                                                                    Visible="false" />
                                                                                                <asp:HiddenField ID="InterviewTestStatisticsInfo_candidateStatisticsTab_candidatesGridViewCandidateSessionIDHiddenField"
                                                                                                    runat="server" Value='<%# Eval("CandidateSessionID") %>' />
                                                                                                <asp:HiddenField ID="InterviewTestStatisticsInfo_candidateStatisticsTab_candidatesGridViewIsCyberProctoringHiddenField"
                                                                                                    runat="server" Value='<%# Eval("IsCyberProctoring") %>' />
                                                                                                <asp:HiddenField ID="InterviewTestStatisticsInfo_candidateStatisticsTab_candidatesGridViewAttemptIDHiddenField"
                                                                                                    runat="server" Value='<%# Eval("AttemptNumber") %>' />
                                                                                                <asp:HiddenField ID="InterviewTestStatisticsInfo_candidateStatisticsTab_candidateGridViewCandidateIDHiddenField"
                                                                                                    runat="server" Value='<%# Eval("CandidateID") %>' />
                                                                                                <asp:HiddenField ID="InterviewTestStatisticsInfo_candidateStatisticsTab_candidateGridViewFirstNameHiddenField"
                                                                                                    runat="server" Value='<%# Eval("CandidateFullName") %>' />
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Width="80px" />
                                                                                        </asp:TemplateField>
                                                                                        <%--<asp:BoundField HeaderText="Candidate&nbsp;Name" DataField="FirstName" SortExpression="CANDIDATE_NAME"
                                                                                ItemStyle-Wrap="false" ItemStyle-Width="150px" />--%>
                                                                                        <asp:TemplateField HeaderText="Candidate&nbsp;Name" SortExpression="CANDIDATE_NAME"
                                                                                            ItemStyle-Wrap="false" ItemStyle-Width="150px">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="InterviewTestStatisticsInfo_candidateStatisticsTab_candidateFullnameLabel"
                                                                                                    runat="server" Text='<%# Eval("FirstName") %>' ToolTip='<%# Eval("CandidateFullName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField HeaderText="Attempt&nbsp;Number" DataField="AttemptNumber" SortExpression="ATTEMPT_NUMBER" />
                                                                                        <%--<asp:BoundField HeaderText="Interview Session Creator" DataField="SessionCreator" SortExpression="TEST_SESSION_CREATOR"
                                                                                ItemStyle-Wrap="false" ItemStyle-Width="450px" HeaderStyle-CssClass="grid_header_row_wrap"
                                                                                HeaderStyle-Width="100px"></asp:BoundField>--%>
                                                                                        <asp:TemplateField HeaderText="Interview<br>Session&nbsp;Creator" SortExpression="TEST_SESSION_CREATOR"
                                                                                            ItemStyle-Wrap="false" ItemStyle-Width="450px" HeaderStyle-CssClass="grid_header_row_wrap"
                                                                                            HeaderStyle-Width="100px">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="InterviewTestStatisticsInfo_candidateStatisticsTab_testSessionCreatorFullnameLabel"
                                                                                                    runat="server" Text='<%# Eval("SessionCreator") %>' ToolTip='<%# Eval("SessionCreatorFullName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--<asp:BoundField HeaderText="Interview Schedule Creator" DataField="ScheduleCreator" ItemStyle-Wrap="false"
                                                                                SortExpression="TEST_SCHEDULE_CREATOR" ItemStyle-Width="200px" HeaderStyle-CssClass="grid_header_row_wrap"
                                                                                HeaderStyle-Width="450px"></asp:BoundField>--%>
                                                                                        <asp:TemplateField HeaderText="Interview<br>Schedule&nbsp;Creator" ItemStyle-Wrap="false"
                                                                                            SortExpression="TEST_SCHEDULE_CREATOR" ItemStyle-Width="200px" HeaderStyle-CssClass="grid_header_row_wrap"
                                                                                            HeaderStyle-Width="450px">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="InterviewTestStatisticsInfo_candidateStatisticsTab_testScheduleCreatorFullnameLabel"
                                                                                                    runat="server" Text='<%# Eval("ScheduleCreator") %>' ToolTip='<%# Eval("ScheduleCreatorFullName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Interview Date" SortExpression="DATE" ItemStyle-Width="120px">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="TestReport_testCreatedDateLabel" runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("TestDate"))) %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField HeaderText="Over All Rating" DataField="Percentile" ItemStyle-CssClass="td_padding_right_20"
                                                                                            ItemStyle-Width="15px" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="td_padding_right_20" />
                                                                                        <%--<asp:BoundField HeaderText="Recruiter" DataField="Recruiter" SortExpression="Recruiter"
                                                                                HeaderStyle-Width="100px" />--%>
                                                                                        <asp:TemplateField HeaderText="Recruiter" SortExpression="Recruiter" HeaderStyle-Width="100px">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="InterviewTestStatisticsInfo_candidateStatisticsTab_recruiterFullnameLabel"
                                                                                                    runat="server" Text='<%# Eval("ScheduleCreator") %>' ToolTip='<%# Eval("ScheduleCreatorFullName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <uc3:PageNavigator ID="InterviewTestStatisticsInfo_candidateTabPanel_bottomPagingNavigator"
                                                                    runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolKit:TabPanel>
                    </ajaxToolKit:TabContainer>
                </div>
            </td>
        </tr>
        <tr>
            <td class="header_bg" align="right" style="width: 100%;">
                <table border="0" cellpadding="0" cellspacing="2" width="100%">
                    <tr>
                        <td align="right">
                            <asp:LinkButton ID="InterviewTestStatisticsInfo_bottomCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                Text="Cancel" OnClick="ParentPageRedirect" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="InterviewTestStatisticsInfo_bottomMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="InterviewTestStatisticsInfo_bottomSuccessLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="InterviewTestStatisticsInfo_bottonErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="InterviewTestStatisticsInfo_heightHiddenField" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
