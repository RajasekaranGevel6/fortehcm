﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    AutoEventWireup="true" CodeBehind="InterviewCandidateTestDetails.aspx.cs" Inherits="Forte.HCM.UI.InterviewReportCenter.InterviewCandidateTestDetails" %>

<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<%@ Register Src="~/CommonControls/ConfirmInterviewScoreMsgControl.ascx" TagName="ConfirmInterviewScoreMsgControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="InterviewMaster_body" runat="server">
    <script language="javascript" type="text/javascript">
    function RequestExternalAssessor(attemptId, candidatSessionId, ctrlId) {
        var height = 450;
        var width = 800;
        var top = (screen.availHeight - parseInt(height)) / 2;
        var left = (screen.availWidth - parseInt(width)) / 2;
        var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";
        var queryStringValue = "../popup/RequestExternalAssessor.aspx?attemptid=" + attemptId + "&candidatesessionid=" + candidatSessionId + "&ctrlid=" + ctrlId;
        //window.open(queryStringValue, window.self, sModalFeature);
        window.open(queryStringValue, window.self, sModalFeature);
        return false;
    }
    </script>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td class="header_bg">
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Label ID="InterviewCandidateTestDetails_headerLiteral" runat="server" Text="Interview Candidate Result Summary"></asp:Label>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="2" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="InterviewCandidateTestDetails_showPublishUrlUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <div style="float:left;width:25px;">
                                                    <asp:ImageButton ID="InterviewCandidateTestDetails_requestExternalAssessorImageButton" 
                                                        runat="server"
                                                        ToolTip="Request External Assessor" 
                                                        OnClick="InterviewCandidateTestDetails_requestExternalAssessorImageButton_Click"
                                                        SkinID="sknMailImageButton"/>
                                                    <div style="display:none">
                                                        <asp:Button ID="InterviewCandidateTestDetails_refreshButton" runat="server" />
                                                    </div>
                                                </div>
                                                <div style="float:left;width:25px;">
                                                    <asp:ImageButton ID="InterviewCandidateTestDetails_topCandidateRatingSummaryImageButton" runat="server" 
                                                        ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif" 
                                                        ToolTip="Candidate Rating Summary" 
                                                        onclick="InterviewCandidateTestDetails_candidateRatingSummaryImageButton_Click" />
                                                </div>
                                                <div style="float:left">
                                                    <asp:ImageButton ID="InterviewCandidateTestDetails_topPublishInterviewImageButton"
                                                        runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/publish.png" ToolTip="Publish Candidate Interview Response"
                                                        Visible="true" Width="18px" Height="18px" OnClick="InterviewCandidateTestDetails_publishInterviewImageButton_Click" />
                                                </div>
                                                <div style="float: left;">
                                                    <asp:Panel ID="InterviewCandidateTestDetails_emailConfirmationPanel" runat="server"
                                                        Style="display: none; height: 254px;" CssClass="popupcontrol_confirm_publish_candidate_interview_response">
                                                        <div id="InterviewCandidateTestDetails_emailConfirmationDiv" style="display: none">
                                                            <asp:Button ID="InterviewCandidateTestDetails_emailConfirmation_hiddenButton" runat="server" />
                                                        </div>
                                                        <uc1:ConfirmInterviewScoreMsgControl ID="InterviewCandidateTestDetails_emailConfirmation_ConfirmMsgControl"
                                                            runat="server" Title="Publish Candidate Interview Response" Type="InterviewScoreConfirmType" />
                                                    </asp:Panel>
                                                    <ajaxToolKit:ModalPopupExtender ID="InterviewCandidateTestDetails_emailConfirmation_ModalPopupExtender"
                                                        BehaviorID="InterviewCandidateTestDetails_emailConfirmation_ModalPopupExtender"
                                                        runat="server" PopupControlID="InterviewCandidateTestDetails_emailConfirmationPanel"
                                                        TargetControlID="InterviewCandidateTestDetails_emailConfirmation_hiddenButton"
                                                        BackgroundCssClass="modalBackground">
                                                    </ajaxToolKit:ModalPopupExtender>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td>
                                        <asp:Button ID="InterviewCandidateTestDetails_topDownloadButton" runat="server" Text="Download"
                                            SkinID="sknButtonId" Visible="false" />
                                    </td>
                                    <td>
                                        <asp:Button ID="InterviewCandidateTestDetails_topEmailButton" OnClientClick="javascript:return ShowEmailConfirm();"
                                            runat="server" Text="Email" SkinID="sknButtonId" Visible="false" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="InterviewCandidateTestDetails_topCancelButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="InterviewCandidateTestDetails_topSuccessMessageLabel" runat="server"
                    SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="InterviewCandidateTestDetails_topErrorMessageLabel" runat="server"
                    SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="4" border="0">
                    <tr>
                        <td colspan="2">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top">
                                        <table cellpadding="2" cellspacing="2" width="100%" border="0">
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Image ID="InterviewCandidateTestDetails_candidatePhotoImage" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top">
                                                    <table width="100%" cellpadding="2" cellspacing="2" border="0">
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table cellpadding="2" cellspacing="2" width="100%">
                                                                    <tr>
                                                                        <td align="left" valign="top" colspan="2">
                                                                            <asp:LinkButton ID="InterviewCandidateTestDetails_candidateNameLinkButton" runat="server"
                                                                                SkinID="sknAssessorActionLinkButton" ToolTip="Click here to view candidate profile"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top" colspan="2">
                                                                            <asp:Label ID="InterviewCandidateTestDetails_InterviewTestNameLabel" runat="server"
                                                                                SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top" colspan="2">
                                                                            <asp:Label ID="InterviewCandidateTestDetails_interviewCompletedDateLabelValue" runat="server" SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="InterviewCandidateTestDetails_noOfQuestionHeadLabel" runat="server"
                                                                                Text="Number Of Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="InterviewCandidateTestDetails_noOfQuestionLabelValue" runat="server"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="InterviewCandidateTestDetails_noOfQuestionTimeTakenLabel" runat="server"
                                                                                Text="Total Time Taken " SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="InterviewCandidateTestDetails_noOfQuestionTimeTakenLabelValue" runat="server"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top">
                                        <table cellpadding="2" cellspacing="2" width="100%" border="0">
                                            <tr>
                                                <td class="td_height_2"></td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <div style="width: 400px; word-wrap: break-word; white-space: normal">
                                                        &nbsp;<asp:LinkButton ID="InterviewCandidateTestDetails_positionProfileNameLinkButton"
                                                            runat="server" SkinID="sknAssessorActionLinkButton" Style="word-wrap: break-word;
                                                            white-space: normal" ToolTip="Click here to view position profile details">
                                                        </asp:LinkButton>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                    <asp:LinkButton ID="InterviewCandidateTestDetails_ClientNameLinkButton" ToolTip="Click here to view client details"
                                                        runat="server" SkinID="sknLabelFieldTextLinkButton">
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding-left:8px">
                                                    <asp:Label ID="InterviewCandidateTestDetails_showClientDepartmentsLabel" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding-left:8px">
                                                    <asp:Label ID="InterviewCandidateTestDetails_showClientContactsLabel" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <table cellpadding="2" cellspacing="2" width="100%" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="InterviewCandidateTestDetails_noOfQuestionattenedLabel" runat="server"
                                                                    Text="Total Questions Attended" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="InterviewCandidateTestDetails_noOfQuestionattenedLabelValue" runat="server"
                                                                    SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="InterviewCandidateTestDetails_noOfQuestionSkippedLabel" runat="server"
                                                                    Text="Total Questions Skipped" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="InterviewCandidateTestDetails_noOfQuestionSkippedLabelValue" runat="server"
                                                                    SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="InterviewCandidateTestDetails_testStatusLabel" runat="server" Text="Interview Status"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="InterviewCandidateTestDetails_testStatusLabelValue" runat="server"
                                                                    SkinID="sknLabelFieldTextBold"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="InterviewCandidateTestDetails_questionGridViewUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="InterviewCandidateTestDetails_testDrftGridView" runat="server"
                                                    AutoGenerateColumns="False" SkinID="sknNewGridView" OnRowDataBound="InterviewCandidateTestDetails_testDrftGridView_RowDataBound"
                                                    OnRowCommand="InterviewCandidateTestDetails_testDrftGridView_RowCommand">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                    <tr>
                                                                        <td valign="top" style="width: 5%">
                                                                            <asp:Image ID="InterviewCandidateTestDetails_questionImage" runat="server" SkinID="sknQuestionImage"
                                                                                ToolTip="Question" />
                                                                            <asp:Label ID="InterviewCandidateTestDetails_rowNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                                runat="server">
                                                                            </asp:Label>
                                                                        </td>
                                                                        <td style="width: 92%">
                                                                            <div>
                                                                                <div style="word-wrap: break-word;white-space:normal;">
                                                                                    <asp:Literal ID="InterviewCandidateTestDetails_questionLiteral" runat="server" 
                                                                                    Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'></asp:Literal>
                                                                                </div>
                                                                                <div style="text-align: center;" id="InterviewCandidateTestDetails_questionImageDiv"
                                                                                    runat="server">
                                                                                    <asp:Image runat="server" ID="InterviewCandidateTestDetails_questionImageDisplay" />
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%">
                                                                            <asp:Image ID="InterviewCandidateTestDetails_answerImage" ImageUrl="~/App_Themes/DefaultTheme/Images/question_answer.gif"
                                                                                runat="server" ToolTip="Answer" Visible='<%# Eval("Choice_Desc") != null &&  Eval("Choice_Desc") != string.Empty %>' />
                                                                        </td>
                                                                        <td>
                                                                            <div class="label_multi_field_text">
                                                                                <div style="word-wrap: break-word;white-space:normal;">
                                                                                <asp:Label ID="InterviewCandidateTestDetails_choiceDescLabelValue" SkinID="sknLabelFieldText"
                                                                                    runat="server" Text='<%# Eval("Choice_Desc")==null ? Eval("Choice_Desc") : Eval("Choice_Desc").ToString().Replace("\n", "<br />") %>'></asp:Label>
                                                                                </div>
                                                                                <asp:HiddenField ID="InterviewCandidateTestDetails_questionKey" runat="server" Value='<%# Eval("QuestionKey") %>' />
                                                                                <asp:HiddenField ID="InterviewCandidateTestDetails_questionID" runat="server" Value='<%# Eval("QuestionID") %>' />
                                                                                <asp:HiddenField ID="CorrectAnswerHiddenField" runat="server" Value='<%# Eval("Answer") %>' />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%">
                                                                            <asp:Image ID="InterviewCandidateTestDetails_interviewQuestionImage" ImageUrl="~/App_Themes/DefaultTheme/Images/question_comments.gif"
                                                                                runat="server" ToolTip="Comments" Visible='<%# Eval("Comments") != null &&  Eval("Comments") != string.Empty %>' />
                                                                        </td>
                                                                        <td>
                                                                            <div class="label_multi_field_text">
                                                                                <div style="word-wrap: break-word;white-space:normal;">
                                                                                <asp:Label ID="InterviewCandidateTestDetails_interviewQuestionAnswerLabelValue" runat="server"
                                                                                    Text='<%# Eval("Comments")==null ? Eval("Comments") : Eval("Comments").ToString().Replace("\n", "<br />") %>' ></asp:Label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%" >
                                                                            <asp:Image ID="InterviewCandidateTestDetails_candidateCommentsImage" 
                                                                                ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_comments.gif"
                                                                                runat="server" ToolTip="Candidate Comments" Visible='<%# Eval("UserComments") != null &&  Eval("UserComments") != string.Empty %>' />
                                                                        </td>
                                                                        <td>
                                                                            <div style="word-wrap: break-word;white-space:normal;">
                                                                            <asp:Label ID="InterviewCandidateTestDetails_candidateCommentsLabelValue" SkinID="sknLabelFieldText"
                                                                                runat="server" Text='<%# Eval("UserComments")==null ? Eval("UserComments") : Eval("UserComments").ToString().Replace("\n", "<br />") %>'> </asp:Label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="padding-left: 50px">
                                                                            <table cellpadding="2" cellspacing="2" width="100%">
                                                                                <tr>
                                                                                    <td style="width: 8%">
                                                                                        <asp:Label ID="InterviewCandidateTestDetails_categoryQuestionLabel" runat="server"
                                                                                            Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 6%">
                                                                                        <asp:Label ID="InterviewCandidateTestDetails_subjectQuestionLabel" runat="server"
                                                                                            Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 8%">
                                                                                        <asp:Label ID="InterviewCandidateTestDetails_weightageLabel" runat="server" Text="Weightage"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 8%">
                                                                                        <asp:Label ID="InterviewCandidateTestDetails_skippedLabel" runat="server" Text="Skipped"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 8%">
                                                                                        <asp:Label ID="InterviewCandidateTestDetails_testAreaQuestionHeadLabel" runat="server"
                                                                                            Text="Interview Area" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 8%">
                                                                                        <asp:Label ID="InterviewCandidateTestDetails_complexityQuestionHeadLabel" runat="server"
                                                                                            Text="Complexity" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 10%">
                                                                                        <asp:Label ID="InterviewCandidateTestDetails_categoryQuestionTextBox" SkinID="sknLabelFieldText"
                                                                                            runat="server" Text='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 10%">
                                                                                        <asp:Label ID="InterviewCandidateTestDetails_subjectQuestionTextBox" runat="server"
                                                                                            Text='<%# Eval("SubjectName") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 8%">
                                                                                        <asp:Label ID="InterviewCandidateTestDetails_weightageLabelValue" runat="server"
                                                                                            Text='<%# Eval("Weightage") %>' SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="InterviewCandidateTestDetails_skippedLabelValue" SkinID="sknLabelFieldText"
                                                                                            runat="server" Text='<%# Eval("Skipped") %>' />
                                                                                    </td>
                                                                                    <td style="width: 10%">
                                                                                        <asp:Label ID="InterviewCandidateTestDetails_testAreaQuestionLabel" SkinID="sknLabelFieldText"
                                                                                            runat="server" Text='<%# Eval("TestAreaName") %>' />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="InterviewCandidateTestDetails_complexityQuestionLabel" SkinID="sknLabelFieldText"
                                                                                            runat="server" Text='<%# Eval("Complexity") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <div style="text-align: center;">
                                                                                <iframe id="InterviewCandidateTestDetails_displayVideoIframe" runat="server" height="220px"
                                                                                    style="border-style: none" width="190px" visible="false"></iframe>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" align="right">
                                                                            <asp:UpdatePanel ID="InterviewCandidateTestDetails_displayVideoImageUpdatePanel"
                                                                                runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:ImageButton ID="InterviewCandidateTestDetails_showVideoImageButton" 
                                                                                        ImageUrl="~/App_Themes/DefaultTheme/Images/video_display_icon.gif"
                                                                                        ToolTip="Click here to play the video" runat="server" CommandName="ShowVideo"
                                                                                        Height="15px" Width="15px" />
                                                                                    <asp:ImageButton ID="InterviewCandidateTestDetails_hideVideoImageButton" 
                                                                                        ImageUrl="~/App_Themes/DefaultTheme/Images/video_hide_icon.gif"
                                                                                        ToolTip="Click here to hide the video" runat="server" CommandName="HideVideo"
                                                                                        Height="15px" Width="15px" />
                                                                                    <asp:ImageButton ID="InterviewCandidateTestDetails_candidateResponseImageButton"
                                                                                        ImageUrl="~/App_Themes/DefaultTheme/Images/flag_icon.gif" ToolTip="Click here to view the candidate response"
                                                                                        runat="server" CommandName="CandidateResponse" Visible="false" />
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="grid_header" />
                                                    <AlternatingRowStyle CssClass="grid_001" />
                                                    <RowStyle CssClass="grid" />
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="InterviewCandidateTestDetails_bottomSuccessMessageLabel" runat="server"
                    SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="InterviewCandidateTestDetails_bottomErrorMessageLabel" runat="server"
                    SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="header_bg">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td style="width: 50%">
                        </td>
                        <td style="width: 50%" align="right">
                            <table>
                                <tr>
                                    <%--<td>
                                        <asp:Button ID="InterviewCandidateTestDetails_bottomDownloadButton" runat="server" Text="Download" OnClick="InterviewCandidateTestDetails_downloadButton_Click"
                                            SkinID="sknButtonId" />
                                    </td>
                                    <td>
                                        <asp:Button ID="InterviewCandidateTestDetails_bottomEmailButton" OnClientClick="javascript:return ShowEmailConfirm();"
                                            runat="server" Text="Email" SkinID="sknButtonId" />
                                    </td>--%>
                                    <td>
                                        <asp:ImageButton ID="InterviewCandidateTestDetails_bottomCandidateRatingSummaryImageButton"
                                            runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif"
                                            ToolTip="Candidate Rating Summary" OnClick="InterviewCandidateTestDetails_candidateRatingSummaryImageButton_Click" />
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="InterviewCandidateTestDetails_bottomPublishInterviewUpdatePanel"
                                            runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="InterviewCandidateTestDetails_bottomPublishInterviewImageButton"
                                                    runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/publish.png" ToolTip="Publish Candidate Interview Response"
                                                    Visible="true" Width="18px" Height="18px" OnClick="InterviewCandidateTestDetails_publishInterviewImageButton_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="InterviewCandidateTestDetails_bottomCancelButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
