﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// UserDashboard.cs
// File that represents the user dashboard for recently added test,testsession etc...
//

#endregion Header

#region Directives                                                             
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Forte.HCM.Trace;
using Forte.HCM.BL;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;
using System.Collections.Generic;
#endregion Directives
namespace Forte.HCM.UI
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the UserDashboard page. This page helps in List the recently created Test ,position profile etc.. 
    /// based on the looged in user
    /// This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class UserDashboard : PageBase
    {
        #region Event Handlers                                                 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("User Dashboard");
                if (IsPostBack)
                    return;

                HCMDashboard hcmDashboard = new HCMDashboard();
                hcmDashboard = new DashBoardBLManager().GetHCMDashboard(base.userID, 4);
                if (!Support.Utility.IsNullOrEmpty(hcmDashboard.PositionProfile))
                    UserDashboard_positionProfileGridView.DataSource = hcmDashboard.PositionProfile;
                else
                    UserDashboard_positionProfileGridView.DataSource = null;
                UserDashboard_positionProfileGridView.DataBind();

                if (!Support.Utility.IsNullOrEmpty(hcmDashboard.Test))
                    UserDashboard_testGridView.DataSource = hcmDashboard.Test;
                else
                    UserDashboard_testGridView.DataSource = null;
                UserDashboard_testGridView.DataBind();

                if (!Support.Utility.IsNullOrEmpty(hcmDashboard.TalentScout))
                    UserDashboard_talentSearchGridView.DataSource = hcmDashboard.TalentScout;
                else
                    UserDashboard_talentSearchGridView.DataSource = null;
                UserDashboard_talentSearchGridView.DataBind();

                if (!Support.Utility.IsNullOrEmpty(hcmDashboard.TestSession))
                    UserDashboard_testSessionGridView.DataSource = hcmDashboard.TestSession;
                else
                    UserDashboard_testSessionGridView.DataSource = null;
                UserDashboard_testSessionGridView.DataBind();

                if (!Support.Utility.IsNullOrEmpty(hcmDashboard.TestReport))
                    UserDashboard_testReportGridView.DataSource = hcmDashboard.TestReport;
                else
                    UserDashboard_testReportGridView.DataSource = null;
                UserDashboard_testReportGridView.DataBind();

                if (!Support.Utility.IsNullOrEmpty(hcmDashboard.ResumeUpload))
                    UserDashboard_uploadedResumeGridView.DataSource = hcmDashboard.ResumeUpload;
                else
                    UserDashboard_uploadedResumeGridView.DataSource = null;
                UserDashboard_uploadedResumeGridView.DataBind();

                if (!Support.Utility.IsNullOrEmpty(hcmDashboard.ResumeDownload))
                    UserDashboard_downloadedResumeGridView.DataSource = hcmDashboard.ResumeDownload;
                else
                    UserDashboard_downloadedResumeGridView.DataSource = null;
                UserDashboard_downloadedResumeGridView.DataBind();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(UserDashboard_topErrorMessageLabel, exp.Message);
            }
        }

        protected void UserDashboard_moreLinkButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                redirectPage(e.CommandName.ToString(), e.CommandArgument.ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(UserDashboard_topErrorMessageLabel, exp.Message);
            }
        }

        protected void UserDashboard_icon_Command(object sender, CommandEventArgs e)
        {
            try
            {
                redirectPage(e.CommandName.ToString(), e.CommandArgument.ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(UserDashboard_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the UserDashboard_cancelLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void UserDashboard_cancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Default.aspx", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(UserDashboard_topErrorMessageLabel, exp.Message);
            }
        }
        
        protected void UserDashboard_gridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                redirectPage(e.CommandName.ToString(), e.CommandArgument.ToString());
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(UserDashboard_topErrorMessageLabel, exception.Message);
            }
        }
        #endregion Event Handlers

        #region Private Methods                                                
        /// <summary>
        /// Previews the position profile.
        /// </summary>
        /// <param name="positionProfileID">The position profile ID.</param>
        private void PreviewPositionProfile(string positionProfileID)
        {
            if (positionProfileID == "")
            {
                return;
            }
            PositionProfileDetail positionProfileDetail = new PositionProfileDetail();
            positionProfileDetail = new PositionProfileBLManager().GetPositionProfile(Convert.ToInt32(positionProfileID));
            List<SegmentDetail> segmentDetailList = new List<SegmentDetail>();
            segmentDetailList = positionProfileDetail.Segments;
            ViewState["SEGMENT_DETAILS"] = segmentDetailList;
            //(List<SegmentDetail>)ViewState[SEGMENT_DETAILS];          
            Control segmentControl;
            for (int i = 0; i < segmentDetailList.Count; i++)
            {
                switch (segmentDetailList[i].SegmentID)
                {
                    case (int)SegmentType.ClientPositionDetail:
                        segmentControl = null;
                        segmentControl = LoadControl(segmentDetailList[i].segmentPath);
                        ((Segments.ClientPositionDetailsControl)segmentControl).PreviewDataSource = segmentDetailList[i];
                        UserDashboard_previewPopupInnerPanel.Controls.Add(segmentControl);
                        break;
                    case (int)SegmentType.Education:
                        segmentControl = null;
                        segmentControl = LoadControl(segmentDetailList[i].segmentPath);
                        ((Segments.EducationRequirementControl)segmentControl).PreviewDataSource = segmentDetailList[i];
                        UserDashboard_previewPopupInnerPanel.Controls.Add(segmentControl);
                        break;
                    case (int)SegmentType.Roles:
                        segmentControl = null;
                        segmentControl = LoadControl(segmentDetailList[i].segmentPath);
                        ((Segments.RoleRequirement)segmentControl).PreviewDataSource = segmentDetailList[i];
                        UserDashboard_previewPopupInnerPanel.Controls.Add(segmentControl);
                        break;
                    case (int)SegmentType.TechnicalSkills:
                        segmentControl = null;
                        segmentControl = LoadControl(segmentDetailList[i].segmentPath);
                        ((Segments.TechnicalSkillRequirement)segmentControl).PreviewDataSource = segmentDetailList[i];
                        UserDashboard_previewPopupInnerPanel.Controls.Add(segmentControl);
                        break;
                    case (int)SegmentType.Verticals:
                        segmentControl = null;
                        segmentControl = LoadControl(segmentDetailList[i].segmentPath);
                        ((Segments.VerticalBackgroundRequirement)segmentControl).PreviewDataSource = segmentDetailList[i];
                        ((Segments.VerticalBackgroundRequirement)segmentControl).DisableControls();
                        UserDashboard_previewPopupInnerPanel.Controls.Add(segmentControl);
                        break;
                }
            }
        }
        private void redirectPage(string name, string id)
        {
            string url = "";
            switch (name)
            {
                case "CreatePositionProfile":
                    url = "~/PositionProfile/PositionProfileBasicInfo.aspx?m=1&s=0";
                    break;
                case "EditPositionProfile":
                    url = "~/PositionProfile/PositionProfileBasicInfo.aspx?m=0&s=0&" + Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING + "=" + id;
                    break;
                case "SearchPositionProfile":
                    url = "~/PositionProfile/SearchPositionProfile.aspx?m=1&s=1";
                    break;
                case "ViewPositionProfile":
                    PreviewPositionProfile(id);
                    UserDashboard_previewPopupExtender.Show();
                    break;
                case "SearchTerm":
                    url = @"~/TalentScoutHome.aspx?key=" + id;
                    break;
                case "TalentSearch":
                    url = @"~/TalentScoutHome.aspx?";
                    if (id.Length > 0)
                        url = url + Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING + "=" + id;
                    break;
                case "SearchTest":
                    url = "~/TestMaker/SearchTest.aspx?m=1&s=2";
                    break;
                case "CreateManualTest":
                    url = "~/TestMaker/CreateManualTestWithoutAdaptive.aspx?m=1&s=1";
                    break;
                case "CreateAutomatedTest":
                    url = "~/TestMaker/CreateAutomaticTest.aspx?m=1&s=0";
                    break;
                case "EditTest":
                    url = "~/TestMaker/EditManualTestWithoutAdaptive.aspx?m=1&s=2&testkey=" + id;
                    break;
                case "CreateTestSession":
                    url = "~/TestMaker/CreateTestSession.aspx?m=1&s=2&testkey=" + id;
                    break;
                case "SearchTestSession":
                    url = "~/TestMaker/TestSession.aspx?m=1&s=3";
                    break;
                case "TestScheduler":
                    url = "~/Scheduler/TestScheduler.aspx?m=2&s=0";
                    break;
                case "TestReport":
                    url = "~/ReportCenter/TestReport.aspx?m=3&s=0";
                    break;
                case "CandidateReport":
                    url = "~/ReportCenter/CandidateReport.aspx?m=3&s=1";
                    break;
                case "ReportStatistics":
                    string[] candidateDetails = id.Split('|');
                    url = "~/TestMaker/TestResult.aspx?m=2&s=0&candidatesession=" + candidateDetails[1] + "&attemptid=" + candidateDetails[2] + "&testkey=" + candidateDetails[0];
                    break;
                case "ViewCandidate":
                    url = "~/ResumeRepository/CreateCandidate.aspx?m=0&s=0&candidateID=" + id;
                    break;
                case "ResumeRepository":
                    url = "~/ResumeRepository/ResumeEditor.aspx?m=1&s=2";
                    break;
                case "UploadResume":
                    url = "~/ResumeRepository/ResumeUploader.aspx?m=1&s=1";
                    break;
                case "SearchCandidate":
                    url = "~/ResumeRepository/SearchCandidateRepositoryr.aspx?m=0&s=1";
                    break;
                case "ViewScheldule":
                    candidateDetails = id.Split('|');
                    UserDashboard_viewTestSchedule.CandidateSessionId = candidateDetails[1];
                    TestSessionDetail testSession = new TestSchedulerBLManager().GetTestSessionDetail(candidateDetails[0], candidateDetails[1],base.userID);
                    UserDashboard_viewTestSchedule.TestSession = testSession;
                    UserDashboard_viewScheduleModalPopupExtender.Show();
                    break;
            }
            if (url.Length > 0)
                Response.Redirect(url + "&ParentPage=" + Constants.ParentPage.USER_DASHBOARD, false);
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   
        protected override bool IsValidData()
        {
            return true;
        }

        protected override void LoadValues()
        {

        }
        #endregion Protected Overridden Methods
    }
}