﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PositionProfileWorkflowSelection.aspx.cs
// File that represents the user interface layout and functionalities
// for the PositionProfileWorkflowSelection page. This page helps in 
// grouping all the position profile activities 
// This class inherits the Forte.HCM.UI.Common.PageBase class.

#endregion

#region Directives

using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Web.UI;
using System.Linq;
using System.Reflection;
using System.Configuration;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.Segments;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

using Resources;
using Forte.HCM.ExternalService;


#endregion

namespace Forte.HCM.UI.PositionProfile
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the PositionProfileWorkflowSelection page. This page helps in 
    /// grouping all the position profile activities 
    /// This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class PositionProfileWorkflowSelection : PageBase
    {
        #region Private Variables

        /// <summary>
        /// A <see cref="int"/> that hold the position profile ID.
        /// </summary>
        private int positionProfileID = 0;

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="positionProfileDetail">
        /// A <see cref="PositionProfileDetail"/> that holds the position profile detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegate(PositionProfileDetail positionProfileDetail, EntityType entityType);

        #endregion Private Variables

        #region Event Handler

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set the page title
                Master.SetPageCaption("Position Profile Workflow Selection");

                // Check if position profile ID is passed.
                if (Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                {
                    base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                        PositionProfileWorkflowSelection_bottomErrorMessageLabel, "Position profile ID parameter missing");

                    return;
                }

                // Get position profile ID.
                int.TryParse(Request.QueryString["positionprofileid"], out positionProfileID);

                if (positionProfileID <= 0)
                {
                    base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                        PositionProfileWorkflowSelection_bottomErrorMessageLabel, "Position profile is invalid");

                    return;
                }

                // Check if user is having permission for this page.
                if (positionProfileID != 0)
                {
                    if (!base.HasPositionProfileOwnerRights(positionProfileID, base.userID, new List<string> 
                    { Constants.PositionProfileOwners.OWNER, Constants.PositionProfileOwners.CO_OWNER }))
                    {
                        // Redirect to review page.
                        Response.Redirect(@"~\PositionProfile\PositionProfileReview.aspx?m=1&s=0&positionprofileid=" +
                            positionProfileID, false);
                        return;                        
                    }
                }

                if (!IsPostBack)
                {
                    //Reset viewstate/session values to null
                    Session["SELECTED_USER_LIST"] = null;
                    ViewState["TASK_OWNER"] = null;

                    // Bind position profile detail.
                    BindPositionProfileDetail(positionProfileID);

                    //Insert default owners
                    InsertDefaultOwners();

                    //Bind task owners
                    BindTaskOwners();
                    
                    //Open talent scout search in new window
                    PositionProfileWorkflowSelection_talentSearchHyperLink.NavigateUrl =
                        "~/TalentScoutHome.aspx?parentpage=PP_WORKFLOW&positionprofileid=" + positionProfileID;

                    //Create Interview Link
                    PositionProfileWorkflowSelection_createInterviewHyperLink.NavigateUrl =
                        "~/InterviewTestMaker/CreateAutomaticInterviewTest.aspx?m=1&s=0&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";

                    //Create Test Link
                    PositionProfileWorkflowSelection_createTestHyperLink.NavigateUrl =
                        "~/TestMaker/CreateAutomaticTest.aspx?m=1&s=0&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";

                    //Open candidate association in new window
                    PositionProfileWorkflowSelection_associateCandidateHyperLink.NavigateUrl =
                        "~/ResumeRepository/SearchCandidateRepository.aspx?m=0&s=1&positionprofileid=" + positionProfileID + "&parentpage=MENU";

                    //Recruiter assignment
                    PositionProfileWorkflowSelection_recruiterAssignmentAddTaskOwnerImageButton.Attributes.Add("onclick",
                        "return SearchOwners('Y','"
                        + PositionProfileWorkflowSelection_recruiterAssignmentAddTaskOwnerImageButton.ClientID + "','TO','"
                        + positionProfileID + "','" + Constants.PositionProfileOwners.RECRUITER_ASSIGNMENT_CREATOR + "')");

                    //Candiate association
                    PositionProfileWorkflowSelection_candidateAssociationAddTaskOwnerImageButton.Attributes.Add("onclick",
                        "return SearchOwners('Y','"
                    + PositionProfileWorkflowSelection_candidateAssociationAddTaskOwnerImageButton.ClientID + "','TO','"
                        + positionProfileID + "','" + Constants.PositionProfileOwners.CANDIDATE_ASSOCIATION_CREATOR + "')");

                    //Interview creation
                    PositionProfileWorkflowSelection_interviewCreationAddTaskOwnerImageButton.Attributes.Add("onclick",
                        "return SearchOwners('Y','"
                        + PositionProfileWorkflowSelection_interviewCreationAddTaskOwnerImageButton.ClientID + "','TO','"
                        + positionProfileID + "','" + Constants.PositionProfileOwners.INTERVIEW_CREATOR + "')");

                    //Interview session creation
                    PositionProfileWorkflowSelection_interviewSessionCreationAddTaskOwnerImageButton.Attributes.Add("onclick",
                        "return SearchOwners('Y','"
                        + PositionProfileWorkflowSelection_interviewSessionCreationAddTaskOwnerImageButton.ClientID + "','TO','"
                        + positionProfileID + "','" + Constants.PositionProfileOwners.INTERVIEW_SESSION_CREATOR + "')");

                    //Interview schedule creation
                    PositionProfileWorkflowSelection_interviewScheduleCreationAddTaskOwnerImageButton.Attributes.Add("onclick",
                        "return SearchOwners('Y','"
                        + PositionProfileWorkflowSelection_interviewScheduleCreationAddTaskOwnerImageButton.ClientID + "','TO','"
                        + positionProfileID + "','" + Constants.PositionProfileOwners.INTERVIEW_SCHEDULER + "')");

                    //Interview assessment
                    PositionProfileWorkflowSelection_interviewAssessorCreationAddTaskOwnerImageButton.Attributes.Add("onclick",
                        "return SearchOwners('Y','"
                        + PositionProfileWorkflowSelection_interviewAssessorCreationAddTaskOwnerImageButton.ClientID + "','TO','"
                        + positionProfileID + "','" + Constants.PositionProfileOwners.ASSESSOR_ASSIGNMENT + "')");

                    //Test Creation
                    PositionProfileWorkflowSelection_testCreationAddTaskOwnerImageButton.Attributes.Add("onclick",
                        "return SearchOwners('Y','"
                        + PositionProfileWorkflowSelection_testCreationAddTaskOwnerImageButton.ClientID + "','TO','"
                        + positionProfileID + "','" + Constants.PositionProfileOwners.TEST_CREATOR + "')");

                    //Test Sesssion Creation
                    PositionProfileWorkflowSelection_testSessionCreationAddTaskOwnerImageButton.Attributes.Add("onclick",
                        "return SearchOwners('Y','"
                        + PositionProfileWorkflowSelection_testSessionCreationAddTaskOwnerImageButton.ClientID + "','TO','"
                        + positionProfileID + "','" + Constants.PositionProfileOwners.TEST_SESSION_CREATOR + "')");

                    //Test Scheduler creation
                    PositionProfileWorkflowSelection_testScheduleCreationAddTaskOwnerImageButton.Attributes.Add("onclick",
                        "return SearchOwners('Y','"
                        + PositionProfileWorkflowSelection_testScheduleCreationAddTaskOwnerImageButton.ClientID + "','TO','"
                        + positionProfileID + "','" + Constants.PositionProfileOwners.TEST_SCHEDULER + "')");

                    //Submittal to client
                    PositionProfileWorkflowSelection_submittalClientAddTaskOwnerImageButton.Attributes.Add("onclick",
                        "return SearchOwners('Y','"
                        + PositionProfileWorkflowSelection_submittalClientAddTaskOwnerImageButton.ClientID + "','TO','"
                        + positionProfileID + "','" + Constants.PositionProfileOwners.SUBMITTAL_TO_CLIENT + "')");

                    //Bind position profile recruiters
                    BindPositionProfileRecruiters(positionProfileID);

                    //Bind associated candidates
                    BindAssociatedCandidates();

                    //Method to bind interview/test workflow status details
                    BindInterviewTestWorkflow();

                    //Hide recruiters list
                    PositionProfileWorkflowSelection_recruitersDiv.Style["display"] = "none";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                    PositionProfileWorkflowSelection_bottomErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when add recruiter image button button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_addRecruiterImageButton_Click(
            object sender, ImageClickEventArgs e)
        {
            try
            {
                string positionID = Request.QueryString["positionprofileid"];

                Response.Redirect("~/PositionProfile/PositionProfileRecruiterAssignment.aspx" +
                    "?m=1&s=0&positionprofileid=" +
                    positionID, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                    PositionProfileWorkflowSelection_bottomErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when talent search button button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_talentSearchImageButton_Click(
            object sender, ImageClickEventArgs e)
        {
            try
            {
                string positionID = Request.QueryString["positionprofileid"];

                Response.Redirect("~/PositionProfile/PositionProfileRecruiterAssignment.aspx" +
                    "?m=1&s=0&positionprofileid=" +
                    positionID, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                    PositionProfileWorkflowSelection_bottomErrorMessageLabel,
                    exp.Message);
            }
        }


        /// <summary>
        /// Handler method that is called when associate button button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_associateCandidateImageButton_Click(
            object sender, ImageClickEventArgs e)
        {
            try
            {
                string positionID = Request.QueryString["positionprofileid"];

                Response.Redirect("~/PositionProfile/PositionProfileRecruiterAssignment.aspx" +
                    "?m=1&s=0&positionprofileid=" +
                    positionID, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                    PositionProfileWorkflowSelection_bottomErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when interview creation add task
        /// owner button button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_interviewCreationAddTaskOwnerImageButton_Click(
            object sender, ImageClickEventArgs e)
        {
            try
            {
                AddTaskOwnerToList(Constants.PositionProfileOwners.INTERVIEW_CREATOR);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when interview session creation add task
        /// owner button button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_interviewSessionCreationAddTaskOwnerImageButton_Click(
            object sender, ImageClickEventArgs e)
        {
            try
            {
                AddTaskOwnerToList(Constants.PositionProfileOwners.INTERVIEW_SESSION_CREATOR);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when interview schedule creation add task
        /// owner button button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_interviewScheduleCreationAddTaskOwnerImageButton_Click(
            object sender, ImageClickEventArgs e)
        {
            try
            {
                AddTaskOwnerToList(Constants.PositionProfileOwners.INTERVIEW_SCHEDULER);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when interview assessment creation add task
        /// owner button button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_interviewAssessorCreationAddTaskOwnerImageButton_Click(
            object sender, ImageClickEventArgs e)
        {
            try
            {
                AddTaskOwnerToList(Constants.PositionProfileOwners.ASSESSOR_ASSIGNMENT);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when submittal to client add task
        /// owner button button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_submittalClientAddTaskOwnerImageButton_Click(
            object sender, ImageClickEventArgs e)
        {
            try
            {
                AddTaskOwnerToList(Constants.PositionProfileOwners.SUBMITTAL_TO_CLIENT);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        
        /// <summary>
        /// Handler method that is called when row command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_recruiterAssignmentAddTaskOwnerImageButton_Click(
           object sender, ImageClickEventArgs e)
        {
            try
            {
                AddTaskOwnerToList(Constants.PositionProfileOwners.RECRUITER_ASSIGNMENT_CREATOR);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when candidate associate task owner button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_candidateAssociationAddTaskOwnerImageButton_Click(
           object sender, EventArgs e)
        {
            try
            {
                AddTaskOwnerToList(Constants.PositionProfileOwners.CANDIDATE_ASSOCIATION_CREATOR);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Handler method that is called when test creation add task
        /// owner button button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_testCreationAddTaskOwnerImageButton_Click(
            object sender, ImageClickEventArgs e)
        {
            try
            {
                AddTaskOwnerToList(Constants.PositionProfileOwners.TEST_CREATOR);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when test session creation add task
        /// owner button button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_testSessionCreationAddTaskOwnerImageButton_Click(
            object sender, ImageClickEventArgs e)
        {
            try
            {
                AddTaskOwnerToList(Constants.PositionProfileOwners.TEST_SESSION_CREATOR);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Handler method that is called when test schedule creation add task
        /// owner button button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_testScheduleCreationAddTaskOwnerImageButton_Click(
            object sender, ImageClickEventArgs e)
        {
            try
            {
                AddTaskOwnerToList(Constants.PositionProfileOwners.TEST_SCHEDULER);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }


        /// <summary>
        /// Handler method that is called when save button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_saveContinueButton_Click(object sender, EventArgs e)
        {
            try
            {
                List<PositionProfileDetail> postionProfileActions = null;
                postionProfileActions = ConstructPositionProfileActions();

                int positionProfileID = 0;

                if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                    positionProfileID = Convert.ToInt32(Request.QueryString["positionprofileid"]);
                
                if (postionProfileActions != null && postionProfileActions.Count > 0)
                {
                    new PositionProfileBLManager().InsertPositionProfileActions(postionProfileActions,
                        positionProfileID, base.userID);

                    Response.Redirect("~/PositionProfile/PositionProfileRecruiterAssignment.aspx" 
                        + "?m=1&s=0&positionprofileid="+ positionProfileID, false);
                }
                else
                {
                    Response.Redirect("~/PositionProfile/PositionProfileRecruiterAssignment.aspx"
                        + "?m=1&s=0&positionprofileid="+ positionProfileID, false);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when item command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_candidateDataList_ItemCommand(object source,
           DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ViewCandidateActivityLog")
                {
                    HyperLink PositionProfileWorkflowSelection_candidateNameHyperLink =
                        e.Item.FindControl("PositionProfileWorkflowSelection_candidateNameHyperLink") as HyperLink;
                    PositionProfileWorkflowSelection_candidateNameHyperLink.NavigateUrl = 
                        "~/ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&candidateid="
                        + e.CommandArgument.ToString();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when item data bound.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="DataListItemEventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_candidateDataList_ItemDataBound(object sender, 
            DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || 
                    e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HyperLink PositionProfileWorkflowSelection_candidateNameHyperLink = 
                        (HyperLink)e.Item.FindControl("PositionProfileWorkflowSelection_candidateNameHyperLink");

                    HiddenField PositionProfileWorkflowSelection_candidateIDHiddenField =
                        (HiddenField)e.Item.FindControl("PositionProfileWorkflowSelection_candidateIDHiddenField");

                    PositionProfileWorkflowSelection_candidateNameHyperLink.NavigateUrl =
                        "~/ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&candidateid="
                        + PositionProfileWorkflowSelection_candidateIDHiddenField.Value.ToString();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when item data bound.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="DataListItemEventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_candidateSubmittedDataList_ItemDataBound(object sender,
            DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item ||
                    e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HyperLink PositionProfileWorkflowSelection_candidateSubmittedNameHyperLink =
                        (HyperLink)e.Item.FindControl("PositionProfileWorkflowSelection_candidateSubmittedNameHyperLink");

                    HiddenField PositionProfileWorkflowSelection_candidateSubmittedIDHiddenField =
                        (HiddenField)e.Item.FindControl("PositionProfileWorkflowSelection_candidateSubmittedIDHiddenField");

                    PositionProfileWorkflowSelection_candidateSubmittedNameHyperLink.NavigateUrl =
                        "~/ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&candidateid="
                        + PositionProfileWorkflowSelection_candidateSubmittedIDHiddenField.Value.ToString();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when interview creation check is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_interviewActiveImageButton_Click(
            object sender, ImageClickEventArgs e)
        {
            try
            {
                SetRequiredSection(PositionProfileWorkflowSelection_interviewActiveImageButton.
                    ImageUrl!="~/App_Themes/DefaultTheme/Images/pp_active.png",
                    PositionProfileWorkflowSelection_testActiveImageButton.
                    ImageUrl=="~/App_Themes/DefaultTheme/Images/pp_active.png");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when test creation required check is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_testActiveImageButton_Click(
            object sender, ImageClickEventArgs e)
        {
            try
            {
                SetRequiredSection(PositionProfileWorkflowSelection_interviewActiveImageButton.
                    ImageUrl=="~/App_Themes/DefaultTheme/Images/pp_active.png",
                    PositionProfileWorkflowSelection_testActiveImageButton.
                    ImageUrl!="~/App_Themes/DefaultTheme/Images/pp_active.png");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called interview creation data list item data bound
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_interviewCreationDataList_ItemDataBound(
            object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item ||
                   e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HyperLink PositionProfileWorkflowSelection_interviewNameHyperLink =
                        (HyperLink)e.Item.FindControl(
                        "PositionProfileWorkflowSelection_interviewNameHyperLink");

                    HiddenField PositionProfileStatus_interviewKeyHiddenField =
                        (HiddenField)e.Item.FindControl(
                        "PositionProfileStatus_interviewKeyHiddenField");

                    string interviewKey=PositionProfileStatus_interviewKeyHiddenField.Value;

                    PositionProfileWorkflowSelection_interviewNameHyperLink.NavigateUrl = 
                        "~/InterviewTestMaker/ViewInterviewTest.aspx?m=1&s=2&parentpage=S_INT&testkey="
                        + interviewKey;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when interview session creation data list
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_interviewSessionDataList_ItemDataBound(
            object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item ||
                    e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HyperLink PositionProfileWorkflowSelection_interviewSessionHyperLink =
                        (HyperLink)e.Item.FindControl(
                        "PositionProfileWorkflowSelection_interviewSessionHyperLink");

                    HiddenField PositionProfileWorkflowSelection_interviewSessionInterviewKeyHiddenField =
                        (HiddenField)e.Item.FindControl(
                        "PositionProfileWorkflowSelection_interviewSessionInterviewKeyHiddenField");

                    string interviewKey = 
                        PositionProfileWorkflowSelection_interviewSessionInterviewKeyHiddenField.Value;

                    PositionProfileWorkflowSelection_interviewSessionHyperLink.NavigateUrl =
                        "~/InterviewScheduler/InterviewScheduleCandidate.aspx?"+
                        "m=2&s=1&parentpage=S_POS_PRO&interviewsessionid=" + interviewKey;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when interview scheduling item data bound 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_interviewScheduledDataList_ItemDataBound(
            object source, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item ||
                    e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField PositionProfileWorkflowSelection_interviewSessionKeyHiddenField =
                           (HiddenField)e.Item.FindControl(
                           "PositionProfileWorkflowSelection_interviewSessionKeyHiddenField");

                    string interviewSessionKey = PositionProfileWorkflowSelection_interviewSessionKeyHiddenField.Value;
                    string interviewScheduleURL="~/InterviewScheduler/InterviewScheduleCandidate.aspx?m=2&s=1&index=3&interviewsessionid=" + 
                            interviewSessionKey + "&positionprofileid="+positionProfileID+"&parentpage=PP_REVIEW";

                    HyperLink PositionProfileWorkflowSelection_interviewScheduledHyperLink =
                    (HyperLink)e.Item.FindControl("PositionProfileWorkflowSelection_interviewScheduledHyperLink");

                    HyperLink PositionProfileWorkflowSelection_interviewCompletedHyperLink =
                    (HyperLink)e.Item.FindControl("PositionProfileWorkflowSelection_interviewCompletedHyperLink");

                    PositionProfileWorkflowSelection_interviewScheduledHyperLink.NavigateUrl = interviewScheduleURL;
                    PositionProfileWorkflowSelection_interviewCompletedHyperLink.NavigateUrl = interviewScheduleURL;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when interview assessed item data bound 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_interviewAssessedDataList_ItemDataBound(
            object source, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item ||
                    e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField PositionProfileWorkflowSelection_assessedSessionKeyHiddenField =
                           (HiddenField)e.Item.FindControl(
                           "PositionProfileWorkflowSelection_assessedSessionKeyHiddenField");

                    string interviewSessionKey = PositionProfileWorkflowSelection_assessedSessionKeyHiddenField.Value;
                    
                    LinkButton PositionProfileWorkflowSelection_assessedLinkButton =
                    (LinkButton)e.Item.FindControl("PositionProfileWorkflowSelection_assessedLinkButton");

                    PositionProfileWorkflowSelection_assessedLinkButton.Attributes.Add("onclick", "javascript:return ShowViewAssessedCandidates('"
                           + positionProfileID + "','" + interviewSessionKey + "');");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when test creation data list item  bound
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_testCreationDataList_ItemDataBound(
            object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item ||
                    e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    HyperLink PositionProfileWorkflowSelection_testNameHyperLink =
                        (HyperLink)e.Item.FindControl("PositionProfileWorkflowSelection_testNameHyperLink");

                    HiddenField PositionProfileStatus_testKeyHiddenField =
                        (HiddenField)e.Item.FindControl("PositionProfileStatus_testKeyHiddenField");

                    string testKey = PositionProfileStatus_testKeyHiddenField.Value;

                    PositionProfileWorkflowSelection_testNameHyperLink.NavigateUrl =
                        "~/TestMaker/ViewTest.aspx?m=1&s=2&type=copy&parentpage=S_TST&testkey=" + testKey;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when test session creation data list item  bound
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_testSessionDataList_ItemDataBound(
            object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item ||
                    e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    HyperLink PositionProfileWorkflowSelection_testSessionHyperLink =
                        (HyperLink)e.Item.FindControl("PositionProfileWorkflowSelection_testSessionHyperLink");

                    HiddenField PositionProfileWorkflowSelection_testSessionTestKeyHiddenField =
                        (HiddenField)e.Item.FindControl("PositionProfileWorkflowSelection_testSessionTestKeyHiddenField");

                    string testSessionKey = PositionProfileWorkflowSelection_testSessionTestKeyHiddenField.Value;

                    PositionProfileWorkflowSelection_testSessionHyperLink.NavigateUrl=
                        "~/Scheduler/ScheduleCandidate.aspx?m=2&s=1&parentpage=S_POS_PRO&testsessionid=" + testSessionKey;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when test scheduling data list clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_testScheduledDataList_ItemDataBound(
            object source, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item ||
                    e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HyperLink PositionProfileWorkflowSelection_testScheduledHyperLink =
                        (HyperLink)e.Item.FindControl("PositionProfileWorkflowSelection_testScheduledHyperLink");

                    HyperLink PositionProfileWorkflowSelection_testCompletedHyperLink =
                        (HyperLink)e.Item.FindControl("PositionProfileWorkflowSelection_testCompletedHyperLink");

                    HiddenField PositionProfileWorkflowSelection_testSessionKeyHiddenField =
                           (HiddenField)e.Item.FindControl("PositionProfileWorkflowSelection_testSessionKeyHiddenField");

                    string testSessionKey = 
                        PositionProfileWorkflowSelection_testSessionKeyHiddenField.Value;
                    string testScheduleURL = 
                        "~/Scheduler/ScheduleCandidate.aspx?m=2&s=1&parentpage=S_POS_PRO&testsessionid=" 
                        + testSessionKey;

                    PositionProfileWorkflowSelection_testScheduledHyperLink.NavigateUrl = testScheduleURL;
                    PositionProfileWorkflowSelection_testCompletedHyperLink.NavigateUrl = testScheduleURL;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when go to workflow selection link is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_goToWorkFlowLinkButton_Click(
            object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/PositionProfile/PositionProfileDetailInfo.aspx?m=1&s=0&positionprofileid="
                    + Request.QueryString.Get("positionprofileid"), false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when assigned recruiters link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_assignedRecruiterLinkButton_Click(
            object sender, EventArgs e)
        {
            try
            {
                if (PositionProfileWorkflowSelection_assignedRecruiterLinkButton.Text != "0")
                    PositionProfileWorkflowSelection_recruitersDiv.Style["display"] = "block";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when number of candidates link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_noOfCandidatesLinkButton_Click(
            object sender, EventArgs e)
        {
            try
            {
                if (PositionProfileWorkflowSelection_noOfCandidatesLinkButton.Text != "0")
                    PositionProfileWorkflowSelection_candidateDiv.Style["display"] = "block";
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when recruiter assignment task owner item command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataList_ItemCommand(
            object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteRecruiterAssignmentTaskOwner")
                {
                    HiddenField PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataList_firstNameHiddenField = (HiddenField)
                        e.Item.FindControl("PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataList_firstNameHiddenField");

                    HiddenField PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataList_emailHiddenField = (HiddenField)
                        e.Item.FindControl("PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataList_emailHiddenField");

                    int recruiterTaskOwnerUserID = Convert.ToInt32(e.CommandArgument.ToString());
                    DeletePositionProfileOwner(Constants.PositionProfileOwners.RECRUITER_ASSIGNMENT_CREATOR, recruiterTaskOwnerUserID, 
                        PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataList_firstNameHiddenField.Value, 
                        PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataList_emailHiddenField.Value);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when item data bound.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_recruitersDataList_ItemDataBound(object sender,
            DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item ||
                    e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    LinkButton PositionProfileWorkflowSelection_recruiterNameLinkButton = (LinkButton)
                        e.Item.FindControl("PositionProfileWorkflowSelection_recruiterNameLinkButton");

                    int recruiterID = 0;
                    HiddenField PositionProfileWorkflowSelection_recruiterIDHiddenField = (HiddenField)
                        e.Item.FindControl("PositionProfileWorkflowSelection_recruiterIDHiddenField");

                    if (!Utility.IsNullOrEmpty(PositionProfileWorkflowSelection_recruiterIDHiddenField.Value))
                        recruiterID = Convert.ToInt32(PositionProfileWorkflowSelection_recruiterIDHiddenField.Value);

                    // Add click handler.
                    PositionProfileWorkflowSelection_recruiterNameLinkButton.Attributes.Add("onclick",
                        "javascript:return ShowRecruiterProfile('" + recruiterID + "','RP');");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when candidate association task owner item command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataList_ItemCommand(
            object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteCandidateAssociationTaskOwner")
                {
                    HiddenField PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataList_firstNameHiddenField = (HiddenField)
                       e.Item.FindControl("PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataList_firstNameHiddenField");

                    HiddenField PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataList_emailHiddenField = (HiddenField)
                        e.Item.FindControl("PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataList_emailHiddenField");

                    int recruiterTaskOwnerUserID = Convert.ToInt32(e.CommandArgument.ToString());
                    DeletePositionProfileOwner(Constants.PositionProfileOwners.CANDIDATE_ASSOCIATION_CREATOR, recruiterTaskOwnerUserID, 
                        PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataList_firstNameHiddenField.Value, 
                        PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataList_emailHiddenField.Value);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when interview creation task owner item command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_interviewCreationTaskOwnerDataList_ItemCommand(
            object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteInterviewCreationTaskOwner")
                {
                    HiddenField PositionProfileWorkflowSelection_interviewCreationTaskOwnerDataList_firstNameHiddenField = (HiddenField)
                        e.Item.FindControl("PositionProfileWorkflowSelection_interviewCreationTaskOwnerDataList_firstNameHiddenField");

                    HiddenField PositionProfileWorkflowSelection_interviewCreationTaskOwnerDataList_emailHiddenField = (HiddenField)
                        e.Item.FindControl("PositionProfileWorkflowSelection_interviewCreationTaskOwnerDataList_emailHiddenField");

                    int recruiterTaskOwnerUserID = Convert.ToInt32(e.CommandArgument.ToString());
                    DeletePositionProfileOwner(Constants.PositionProfileOwners.INTERVIEW_CREATOR, recruiterTaskOwnerUserID, 
                        PositionProfileWorkflowSelection_interviewCreationTaskOwnerDataList_firstNameHiddenField.Value, 
                        PositionProfileWorkflowSelection_interviewCreationTaskOwnerDataList_emailHiddenField.Value);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when interview session creation task owner item command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerDataList_ItemCommand(
            object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteInterviewSessionCreationTaskOwner")
                {
                    HiddenField PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerDataList_firstNameHiddenField = (HiddenField)
                       e.Item.FindControl("PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerDataList_firstNameHiddenField");

                    HiddenField PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerDataList_emailHiddenField = (HiddenField)
                        e.Item.FindControl("PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerDataList_emailHiddenField");

                    int recruiterTaskOwnerUserID = Convert.ToInt32(e.CommandArgument.ToString());
                    DeletePositionProfileOwner(Constants.PositionProfileOwners.INTERVIEW_SESSION_CREATOR, recruiterTaskOwnerUserID,
                        PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerDataList_firstNameHiddenField.Value,
                        PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerDataList_emailHiddenField.Value);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when interview schedule creation task owner item command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerDataList_ItemCommand(
            object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteInterviewScheduleCreationTaskOwner")
                {
                    HiddenField PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerDataList_firstNameHiddenField = (HiddenField)
                       e.Item.FindControl("PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerDataList_firstNameHiddenField");

                    HiddenField PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerDataList_emailHiddenField = (HiddenField)
                        e.Item.FindControl("PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerDataList_emailHiddenField");

                    int recruiterTaskOwnerUserID = Convert.ToInt32(e.CommandArgument.ToString());
                    DeletePositionProfileOwner(Constants.PositionProfileOwners.INTERVIEW_SCHEDULER, recruiterTaskOwnerUserID,
                        PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerDataList_firstNameHiddenField.Value, 
                        PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerDataList_emailHiddenField.Value);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when interview assessor creation task owner item command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_interviewAssessorTaskOwnerDataList_ItemCommand(
            object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteInterviewAssessorCreationTaskOwner")
                {
                    HiddenField PositionProfileWorkflowSelection_interviewAssessorTaskOwnerDataList_firstNameHiddenField = (HiddenField)
                        e.Item.FindControl("PositionProfileWorkflowSelection_interviewAssessorTaskOwnerDataList_firstNameHiddenField");

                    HiddenField PositionProfileWorkflowSelection_interviewAssessorTaskOwnerDataList_emailHiddenField = (HiddenField)
                        e.Item.FindControl("PositionProfileWorkflowSelection_interviewAssessorTaskOwnerDataList_emailHiddenField");

                    int recruiterTaskOwnerUserID = Convert.ToInt32(e.CommandArgument.ToString());
                    DeletePositionProfileOwner(Constants.PositionProfileOwners.ASSESSOR_ASSIGNMENT, recruiterTaskOwnerUserID,
                        PositionProfileWorkflowSelection_interviewAssessorTaskOwnerDataList_firstNameHiddenField.Value,
                        PositionProfileWorkflowSelection_interviewAssessorTaskOwnerDataList_emailHiddenField.Value);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when test creation task owner item command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_testCreationTaskOwnerDataList_ItemCommand(
            object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteTestCreationTaskOwner")
                {
                    HiddenField PositionProfileWorkflowSelection_testCreationTaskOwnerDataList_firstNameHiddenField = (HiddenField)
                       e.Item.FindControl("PositionProfileWorkflowSelection_testCreationTaskOwnerDataList_firstNameHiddenField");

                    HiddenField PositionProfileWorkflowSelection_testCreationTaskOwnerDataList_emailHiddenField = (HiddenField)
                        e.Item.FindControl("PositionProfileWorkflowSelection_testCreationTaskOwnerDataList_emailHiddenField");

                    int recruiterTaskOwnerUserID = Convert.ToInt32(e.CommandArgument.ToString());
                    DeletePositionProfileOwner(Constants.PositionProfileOwners.TEST_CREATOR, recruiterTaskOwnerUserID,
                        PositionProfileWorkflowSelection_testCreationTaskOwnerDataList_firstNameHiddenField.Value,
                        PositionProfileWorkflowSelection_testCreationTaskOwnerDataList_emailHiddenField.Value);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when test session creation task owner item command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_testSessionCreationTaskOwnerDataList_ItemCommand(
            object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteTestSessionCreationTaskOwner")
                {
                    HiddenField PositionProfileWorkflowSelection_testSessionCreationTaskOwnerDataList_firstNameHiddenField = (HiddenField)
                        e.Item.FindControl("PositionProfileWorkflowSelection_testSessionCreationTaskOwnerDataList_firstNameHiddenField");

                    HiddenField PositionProfileWorkflowSelection_testSessionCreationTaskOwnerDataList_emailHiddenField = (HiddenField)
                        e.Item.FindControl("PositionProfileWorkflowSelection_testSessionCreationTaskOwnerDataList_emailHiddenField");

                    int recruiterTaskOwnerUserID = Convert.ToInt32(e.CommandArgument.ToString());
                    DeletePositionProfileOwner(Constants.PositionProfileOwners.TEST_SESSION_CREATOR, recruiterTaskOwnerUserID,
                        PositionProfileWorkflowSelection_testSessionCreationTaskOwnerDataList_firstNameHiddenField.Value,
                        PositionProfileWorkflowSelection_testSessionCreationTaskOwnerDataList_emailHiddenField.Value);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when test schedule creation task owner item command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerDataList_ItemCommand(
            object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteTestScheduleCreationTaskOwner")
                {
                    HiddenField PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerDataList_firstNameHiddenField = (HiddenField)
                       e.Item.FindControl("PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerDataList_firstNameHiddenField");

                    HiddenField PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerDataList_emailHiddenField = (HiddenField)
                        e.Item.FindControl("PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerDataList_emailHiddenField");

                    int recruiterTaskOwnerUserID = Convert.ToInt32(e.CommandArgument.ToString());
                    DeletePositionProfileOwner(Constants.PositionProfileOwners.TEST_SCHEDULER, recruiterTaskOwnerUserID,
                        PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerDataList_firstNameHiddenField.Value,
                        PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerDataList_emailHiddenField.Value);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when submittal to client task owner item command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileWorkflowSelection_submittalToClientTaskOwnerDataList_ItemCommand(
            object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteSubmittalToClientTaskOwner")
                {
                    int recruiterTaskOwnerUserID = Convert.ToInt32(e.CommandArgument.ToString());
                    DeletePositionProfileOwner(Constants.PositionProfileOwners.SUBMITTAL_TO_CLIENT, 
                        recruiterTaskOwnerUserID);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileWorkflowSelection_topErrorMessageLabel,
                  PositionProfileWorkflowSelection_bottomErrorMessageLabel, exp.Message);
            }
        }

        
        #endregion Event Handler

        #region Protected Overridden Methods

        protected override bool IsValidData()
        {
            bool isValidData = true;

            return isValidData;
        }

        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods

        /// <summary>
        /// Method to delete the position profile owner
        /// </summary>
        /// <param name="ownerType">The owner type</param>
        /// <param name="userID">The user ID</param>
        private void DeletePositionProfileOwner(string ownerType,
            int userID)
        {
            new PositionProfileBLManager().DeletePositionProfileOwner(positionProfileID,
                ownerType, userID);

            List<UserDetail> taskOwner = ViewState["TASK_OWNER"] as List<UserDetail>;
            taskOwner.RemoveAll(x => x.UserID == userID && x.OwnerType.Trim() == ownerType.Trim());
            ViewState["TASK_OWNER"] = taskOwner;
            AssignOwners(ownerType);
        }

        /// <summary>
        /// Method to delete the position profile owner.
        /// </summary>
        /// <param name="ownerType">
        /// A <see cref="string"/> that holds the owner type.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the owner user ID.
        /// </param>
        /// <param name="taskOwnerName">
        /// A <see cref="string"/> that holds the task owner name.
        /// </param>
        /// <param name="taskOwnerEmail">
        /// A <see cref="string"/> that holds the task owner email.
        /// </param>
        private void DeletePositionProfileOwner(string ownerType,
            int userID, string taskOwnerName, string taskOwnerEmail)
        {
            new PositionProfileBLManager().DeletePositionProfileOwner(positionProfileID,
                ownerType, userID);

            try
            {
                // Compose position profile detail.
                PositionProfileDetail positionProfileDetail = new PositionProfileDetail();
                positionProfileDetail.PositionProfileID = positionProfileID;
                positionProfileDetail.TaskOwnerName = taskOwnerName;
                positionProfileDetail.TaskOwnerEMail = taskOwnerEmail;
                positionProfileDetail.TaskOwnerType = ownerType.Trim();;
                positionProfileDetail.PositionProfileName = PositionProfileWorkflowSelection_positionProfileNameHiddenField.Value;
                positionProfileDetail.ClientName = PositionProfileWorkflowSelection_clientNameHiddenField.Value;

                // Sent alert mail to the associated user asynchronously.
                AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendStatusChangeAlertEmail);
                IAsyncResult result = taskDelegate.BeginInvoke(positionProfileDetail,
                    EntityType.DissociatedFromPositionProfile, new AsyncCallback(SendAlertEmailCallBack), taskDelegate);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }

            List<UserDetail> taskOwner = ViewState["TASK_OWNER"] as List<UserDetail>;
            taskOwner.RemoveAll(x => x.UserID == userID && x.OwnerType.Trim() == ownerType.Trim());
            ViewState["TASK_OWNER"] = taskOwner;
            AssignOwners(ownerType);
        }

        /// <summary>
        /// Method to get the position profile recruiters by profile id
        /// </summary>
        private void BindPositionProfileRecruiters(int positionProfileID)
        {
            List<UserDetail> userDetails = new List<UserDetail>();
            userDetails = new PositionProfileBLManager().
                GetPositionProfileOwnersByIDAndOwnerType(positionProfileID,
                Constants.PositionProfileOwners.CANDIDATE_RECRUITER);

            if (userDetails != null && userDetails.Count > 0)
            {
                PositionProfileWorkflowSelection_assignedRecruiterLinkButton.Text 
                    = userDetails.Count.ToString();

                PositionProfileWorkflowSelection_recruitersDataList.DataSource=userDetails;
                PositionProfileWorkflowSelection_recruitersDataList.DataBind();
            }
            else
            {
                PositionProfileWorkflowSelection_assignedRecruiterLinkButton.Text = "0";
            }
        }

        /// <summary>
        /// Method to include/add all selected task owners to a list
        /// </summary>
        /// <param name="OwnerType">The owner type</param>
        private void AddTaskOwnerToList(string ownerType)
        {
            List<UserDetail> taskOwners = null;
            if (ViewState["TASK_OWNER"] == null)
                taskOwners = new List<UserDetail>();
            else
                taskOwners = (List<UserDetail>)ViewState["TASK_OWNER"];

            List<UserDetail> selectedUsers = null;
            if (Session["SELECTED_USER_LIST"] == null)
                selectedUsers = new List<UserDetail>();
            else
                selectedUsers = (List<UserDetail>)Session["SELECTED_USER_LIST"];

            if (selectedUsers != null && selectedUsers.Count != 0)
            {
                foreach (UserDetail selectedUser in selectedUsers)
                {
                    if (!Utility.IsNullOrEmpty(taskOwners) && taskOwners.Count != 0 
                        && (taskOwners.Any(res => res.UserID == 
                            Convert.ToInt32(selectedUser.UserID) && 
                            res.OwnerType.Trim() == ownerType.Trim())
                        || taskOwners.Any(res => res.UserID == Convert.ToInt32(selectedUser.UserID) &&
                            res.OwnerType.Trim() == Constants.PositionProfileOwners.OWNER.Trim())
                        || taskOwners.Any(res => res.UserID == Convert.ToInt32(selectedUser.UserID) &&
                            res.OwnerType.Trim() == Constants.PositionProfileOwners.CO_OWNER.Trim())))
                        continue;

                    UserDetail taskOwner = new UserDetail();
                    taskOwner.UserID = Convert.ToInt32(selectedUser.UserID);
                    taskOwner.FirstName = selectedUser.FirstName.Trim();
                    taskOwner.LastName = selectedUser.LastName.Trim();
                    taskOwner.Email = selectedUser.Email;
                    taskOwner.OwnerType = ownerType.Trim();
                    taskOwners.Add(taskOwner);

                    new PositionProfileBLManager().InsertPositionProfileOwner(positionProfileID,ownerType,
                        Convert.ToInt32(selectedUser.UserID),base.userID);

                    try
                    {
                        // Compose position profile detail.
                        PositionProfileDetail positionProfileDetail = new PositionProfileDetail();
                        positionProfileDetail.PositionProfileID = positionProfileID;
                        positionProfileDetail.TaskOwnerEMail = selectedUser.Email;
                        positionProfileDetail.TaskOwnerName = selectedUser.FirstName;
                        positionProfileDetail.TaskOwnerType = ownerType.Trim();
                        positionProfileDetail.PositionProfileName = PositionProfileWorkflowSelection_positionProfileNameHiddenField.Value;
                        positionProfileDetail.ClientName = PositionProfileWorkflowSelection_clientNameHiddenField.Value;

                        // Sent alert mail to the associated user asynchronously.
                        AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendStatusChangeAlertEmail);
                        IAsyncResult result = taskDelegate.BeginInvoke(positionProfileDetail,
                            EntityType.AssociatedWithPositionProfile, new AsyncCallback(SendAlertEmailCallBack), taskDelegate);
                    }
                    catch (Exception exp)
                    {
                        Logger.ExceptionLog(exp);
                    }
                }
            }

            ViewState["TASK_OWNER"] = taskOwners;
            AssignOwners(ownerType);
        }

        /// <summary>
        /// Method to assign all the selected task owners to grid list
        /// </summary>
        /// <param name="OwnerType">The owner type</param>
        private void AssignOwners(string ownerType)
        {
            List<UserDetail> taskOwners = null;
            if (ViewState["TASK_OWNER"] == null)
                taskOwners = new List<UserDetail>();
            else
                taskOwners = (List<UserDetail>)ViewState["TASK_OWNER"];

            if (taskOwners == null || taskOwners.Count == 0)
                return;

            //Filter by particular task owner + position profile owner + co-owner
            var ownersList = Enumerable.Where(taskOwners,
                t => t.OwnerType.Trim() == ownerType.Trim() ||
                    t.OwnerType.Trim() == Constants.PositionProfileOwners.OWNER.Trim() ||
                    t.OwnerType.Trim() == Constants.PositionProfileOwners.CO_OWNER.Trim());

            if (ownersList != null)
            {
                switch (ownerType)
                {
                    case Constants.PositionProfileOwners.RECRUITER_ASSIGNMENT_CREATOR:
                        PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataList.DataSource = ownersList;
                        PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataList.DataBind();
                        break;
                    case Constants.PositionProfileOwners.CANDIDATE_ASSOCIATION_CREATOR:
                        var candAssociationOwnersList = Enumerable.Where(taskOwners,
                            t => t.OwnerType.Trim() == ownerType.Trim() ||
                                t.OwnerType.Trim() == Constants.PositionProfileOwners.OWNER.Trim() ||
                                t.OwnerType.Trim() == Constants.PositionProfileOwners.CO_OWNER.Trim() ||
                                t.OwnerType.Trim() == Constants.PositionProfileOwners.CANDIDATE_RECRUITER.Trim());
                        if (candAssociationOwnersList != null)
                        {
                            PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataList.DataSource = candAssociationOwnersList;
                            PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataList.DataBind();
                        }
                        break;
                    case Constants.PositionProfileOwners.INTERVIEW_CREATOR:
                        PositionProfileWorkflowSelection_interviewCreationTaskOwnerDataList.DataSource = ownersList;
                        PositionProfileWorkflowSelection_interviewCreationTaskOwnerDataList.DataBind();
                        break;
                    case Constants.PositionProfileOwners.INTERVIEW_SESSION_CREATOR:
                        PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerDataList.DataSource = ownersList;
                        PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerDataList.DataBind();
                        break;
                    case Constants.PositionProfileOwners.INTERVIEW_SCHEDULER:
                        PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerDataList.DataSource = ownersList;
                        PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerDataList.DataBind();
                        break;
                    case Constants.PositionProfileOwners.ASSESSOR_ASSIGNMENT:
                        PositionProfileWorkflowSelection_interviewAssessorTaskOwnerDataList.DataSource = ownersList;
                        PositionProfileWorkflowSelection_interviewAssessorTaskOwnerDataList.DataBind();
                        break;
                    case Constants.PositionProfileOwners.TEST_CREATOR:
                        PositionProfileWorkflowSelection_testCreationTaskOwnerDataList.DataSource = ownersList;
                        PositionProfileWorkflowSelection_testCreationTaskOwnerDataList.DataBind();
                        break;
                    case Constants.PositionProfileOwners.TEST_SESSION_CREATOR:
                        PositionProfileWorkflowSelection_testSessionCreationTaskOwnerDataList.DataSource = ownersList;
                        PositionProfileWorkflowSelection_testSessionCreationTaskOwnerDataList.DataBind();
                        break;
                    case Constants.PositionProfileOwners.TEST_SCHEDULER:
                        PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerDataList.DataSource = ownersList;
                        PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerDataList.DataBind();
                        break;
                    case Constants.PositionProfileOwners.SUBMITTAL_TO_CLIENT:
                        PositionProfileWorkflowSelection_submittalToClientTaskOwnerDataList.DataSource = ownersList;
                        PositionProfileWorkflowSelection_submittalToClientTaskOwnerDataList.DataBind();
                        break;
                }
            }
        }

        /// <summary>
        /// Method to bind the interview/test workflow status details
        /// </summary>
        private void BindInterviewTestWorkflow()
        {
            PositionProfieWorkflowDetail positionProfieWorkflowDetail =
                new PositionProfieWorkflowDetail();

            positionProfieWorkflowDetail = new PositionProfileBLManager().
               GetPositionProfileWorkflowSelection(positionProfileID);

            if (positionProfieWorkflowDetail != null)
            {
                //Show or hide required section
                SetRequiredSection(positionProfieWorkflowDetail.IsInterviewRequired,
                    positionProfieWorkflowDetail.IsTestRequired);

                BindInterviewDetails(positionProfieWorkflowDetail);
                BindInterviewSessionDetails(positionProfieWorkflowDetail);
                BindInterviewScheduledCompletedDetails(positionProfieWorkflowDetail);
                BindInterviewAssessedDetails(positionProfieWorkflowDetail);
                BindTestDetails(positionProfieWorkflowDetail);
                BindTestSessionDetails(positionProfieWorkflowDetail);
                BindTestScheduledCompletedDetails(positionProfieWorkflowDetail);
                BindSubmittalCandidatesDetails(positionProfieWorkflowDetail);
            }
        }

        /// <summary>
        /// Method to bind the interview details
        /// </summary>
        private void BindInterviewDetails(PositionProfieWorkflowDetail 
            positionProfieWorkflowDetail)
        {
            if (positionProfieWorkflowDetail != null &&
                positionProfieWorkflowDetail.InterviewDetail != null &&
                positionProfieWorkflowDetail.InterviewDetail.Count > 0)
            {
                PositionProfileWorkflowSelection_interviewCreationDataList.DataSource =
                    positionProfieWorkflowDetail.InterviewDetail;
                PositionProfileWorkflowSelection_interviewCreationDataList.DataBind();

                PositionProfileWorkflowSelection_createInterviewHyperLink.Visible=false;
                PositionProfileWorkflowSelection_interviewCreationErrorLabel.Visible = false;
                PositionProfileWorkflowSelection_interviewCreationErrorLabel.Text = string.Empty;
            }
            else
            {
                PositionProfileWorkflowSelection_createInterviewHyperLink.Visible=true;
                PositionProfileWorkflowSelection_interviewCreationErrorLabel.Visible = true;
                PositionProfileWorkflowSelection_interviewCreationErrorLabel.Text = 
                    "Interview not created.";
            }
        }

        /// <summary>
        /// Method to bind the interview session details
        /// </summary>
        private void BindInterviewSessionDetails(PositionProfieWorkflowDetail 
            positionProfieWorkflowDetail)
        {
            if (positionProfieWorkflowDetail != null &&
                positionProfieWorkflowDetail.InterviewSessionDetail != null &&
                positionProfieWorkflowDetail.InterviewSessionDetail.Count > 0 &&
                positionProfieWorkflowDetail.InterviewSessionDetail[0].InterviewSessionCount > 0)
            {
                PositionProfileWorkflowSelection_interviewSessionDataList.DataSource =
                    positionProfieWorkflowDetail.InterviewSessionDetail;
                PositionProfileWorkflowSelection_interviewSessionDataList.DataBind();
                PositionProfileWorkflowSelection_noOfInterviewSessionLabel.Visible = true;
                PositionProfileWorkflowSelection_interviewSessionErrorLabel.Text=string.Empty;
                PositionProfileWorkflowSelection_interviewSessionErrorLabel.Visible = false;
                PositionProfileWorkflowSelection_createInterviewSessionHyperLink.Visible = false;
            }
            else
            {
                PositionProfileWorkflowSelection_noOfInterviewSessionLabel.Visible = false;
                PositionProfileWorkflowSelection_interviewSessionErrorLabel.Visible = true;
                if (PositionProfileWorkflowSelection_createInterviewHyperLink.Visible)
                {
                    PositionProfileWorkflowSelection_interviewSessionErrorLabel.Text =
                        "Interview session not created.<br>Interview needs to be associated before creating interview session.<br>";
                    PositionProfileWorkflowSelection_createInterviewSessionHyperLink.Visible = false;
                }
                else
                {
                    PositionProfileWorkflowSelection_createInterviewSessionHyperLink.Visible = true;
                    PositionProfileWorkflowSelection_createInterviewSessionHyperLink.NavigateUrl =
                        "~/InterviewTestMaker/CreateInterviewTestSession.aspx?m=1&s=2&parentpage=S_POS_PRO&interviewtestkey="
                        + positionProfieWorkflowDetail.InterviewDetail[0].InterviewTestKey.ToString();

                    PositionProfileWorkflowSelection_interviewSessionErrorLabel.Text =
                        "Interview session not created.";
                }
            }
        }

        /// <summary>
        /// Method to bind the interview scheduled details
        /// </summary>
        private void BindInterviewScheduledCompletedDetails(PositionProfieWorkflowDetail 
            positionProfieWorkflowDetail)
        {

            if (positionProfieWorkflowDetail != null &&
                positionProfieWorkflowDetail.InterviewSessionDetail != null &&
                positionProfieWorkflowDetail.InterviewSessionDetail.Count > 0)
            {
                PositionProfileWorkflowSelection_interviewScheduledDataList.DataSource =
                    positionProfieWorkflowDetail.InterviewSessionDetail;
                PositionProfileWorkflowSelection_interviewScheduledDataList.DataBind();
                PositionProfileWorkflowSelection_interviewScheduleTitleDiv.Style["display"] = "block";
                PositionProfileWorkflowSelection_interviewScheduleErrorLabel.Text = string.Empty;
                PositionProfileWorkflowSelection_interviewScheduleErrorLabel.Visible = false;
                PositionProfileWorkflowSelection_createIterviewScheduleHyperLink.Visible = false;
            }
            else
            {
                PositionProfileWorkflowSelection_interviewScheduleTitleDiv.Style["display"] = "none";
                PositionProfileWorkflowSelection_interviewScheduleErrorLabel.Visible = true;
                if (positionProfieWorkflowDetail != null && 
                    positionProfieWorkflowDetail.InterviewSessionDetail == null)
                {
                    PositionProfileWorkflowSelection_interviewScheduleErrorLabel.Text =
                        "Interview not scheduled.<br>Interview session needs to be created before scheduling.";
                    PositionProfileWorkflowSelection_createIterviewScheduleHyperLink.Visible = false;
                }
            }
        }

        /// <summary>
        /// Method to bind the interview assessed details
        /// </summary>
        private void BindInterviewAssessedDetails(PositionProfieWorkflowDetail 
            positionProfieWorkflowDetail)
        {
            if (positionProfieWorkflowDetail != null &&
                positionProfieWorkflowDetail.InterviewSessionDetail != null &&
                positionProfieWorkflowDetail.InterviewSessionDetail.Count > 0)
            {
                PositionProfileWorkflowSelection_interviewAssessedDataList.DataSource =
                    positionProfieWorkflowDetail.InterviewSessionDetail;
                PositionProfileWorkflowSelection_interviewAssessedDataList.DataBind();
                PositionProfileWorkflowSelection_assessedCandidateLabel.Visible = true;
                PositionProfileWorkflowSelection_assessedCandidateErrorLabel.Text = string.Empty;
                PositionProfileWorkflowSelection_assessedCandidateErrorLabel.Visible = false;
            }
            else
            {
                PositionProfileWorkflowSelection_assessedCandidateLabel.Visible = false;
                PositionProfileWorkflowSelection_assessedCandidateErrorLabel.Visible = true;
                PositionProfileWorkflowSelection_assessedCandidateErrorLabel.Text="No candidates assessed.";
            }
        }
        
        /// <summary>
        /// Method to bind the test details
        /// </summary>
        private void BindTestDetails(PositionProfieWorkflowDetail 
            positionProfieWorkflowDetail)
        {
            if (positionProfieWorkflowDetail != null &&
                positionProfieWorkflowDetail.TestDetail != null &&
                positionProfieWorkflowDetail.TestDetail.Count > 0)
            {
                PositionProfileWorkflowSelection_testCreationDataList.DataSource =
                    positionProfieWorkflowDetail.TestDetail;
                PositionProfileWorkflowSelection_testCreationDataList.DataBind();
                PositionProfileWorkflowSelection_createTestHyperLink.Visible = false;
                PositionProfileWorkflowSelection_testCreationErrorLabel.Visible = false;
            }
            else
            {
                PositionProfileWorkflowSelection_createTestHyperLink.Visible = true;
                PositionProfileWorkflowSelection_testCreationErrorLabel.Visible = true;
            }
        }

        /// <summary>
        /// Method to bind the test session details
        /// </summary>
        private void BindTestSessionDetails(PositionProfieWorkflowDetail 
            positionProfieWorkflowDetail)
        {
            if (positionProfieWorkflowDetail != null &&
                positionProfieWorkflowDetail.TestSessionDetail != null &&
                positionProfieWorkflowDetail.TestSessionDetail.Count > 0 &&
                positionProfieWorkflowDetail.TestSessionDetail[0].TestSessionCount > 0)
            {
                PositionProfileWorkflowSelection_testSessionDataList.DataSource =
                    positionProfieWorkflowDetail.TestSessionDetail;
                PositionProfileWorkflowSelection_testSessionDataList.DataBind();
                PositionProfileWorkflowSelection_noOfTestSessionLabel.Visible = true;
                PositionProfileWorkflowSelection_testSessionErrorLabel.Visible = false;
                PositionProfileWorkflowSelection_testSessionErrorLabel.Text=string.Empty;
                PositionProfileWorkflowSelection_createTestSessionHyperLink.Visible = false;
            }
            else
            {
                PositionProfileWorkflowSelection_noOfTestSessionLabel.Visible = false;
                PositionProfileWorkflowSelection_testSessionErrorLabel.Visible = true;
                if (PositionProfileWorkflowSelection_createTestHyperLink.Visible)
                {
                    PositionProfileWorkflowSelection_testSessionErrorLabel.Text =
                        "Test session not created.<br>Test needs to be associated before creating test session.<br>";
                    PositionProfileWorkflowSelection_createTestSessionHyperLink.Visible = false;
                }
                else
                {
                    PositionProfileWorkflowSelection_createTestSessionHyperLink.Visible = true;
                    PositionProfileWorkflowSelection_createTestSessionHyperLink.NavigateUrl =
                        "~/TestMaker/CreateTestSession.aspx?m=1&s=3&parentpage=S_POS_PRO&testkey="
                        + positionProfieWorkflowDetail.TestDetail[0].TestKey.ToString();

                    PositionProfileWorkflowSelection_testSessionErrorLabel.Text =
                        "Test session not created.";
                }
            }
        }

        /// <summary>
        /// Method to bind the test scheduled details
        /// </summary>
        private void BindTestScheduledCompletedDetails(PositionProfieWorkflowDetail
            positionProfieWorkflowDetail)
        {
            if (positionProfieWorkflowDetail != null &&
                positionProfieWorkflowDetail.TestSessionDetail != null &&
                positionProfieWorkflowDetail.TestSessionDetail.Count > 0)
            {
                PositionProfileWorkflowSelection_testScheduledDataList.DataSource =
                    positionProfieWorkflowDetail.TestSessionDetail;
                PositionProfileWorkflowSelection_testScheduledDataList.DataBind();
                PositionProfileWorkflowSelection_testScheduleTitleDiv.Style["display"] = "block";
                PositionProfileWorkflowSelection_testScheduleErrorLabel.Visible = false;
                PositionProfileWorkflowSelection_createTestScheduleHyperLink.Visible = false;
            }
            else
            {
                PositionProfileWorkflowSelection_testScheduleTitleDiv.Style["display"] = "none";
                PositionProfileWorkflowSelection_testScheduleErrorLabel.Visible=true;
                PositionProfileWorkflowSelection_createTestScheduleHyperLink.Visible = false;

                if (positionProfieWorkflowDetail != null &&
                   positionProfieWorkflowDetail.TestSessionDetail == null)
                {
                    PositionProfileWorkflowSelection_testScheduleErrorLabel.Text = "Test not scheduled.<br> Test session needs to be created before scheduling.";
                }
            }
        }

        /// <summary>
        /// Method to bind the submittal candidate details
        /// </summary>
        private void BindSubmittalCandidatesDetails(PositionProfieWorkflowDetail 
            positionProfieWorkflowDetail)
        {
            if (!Utility.IsNullOrEmpty(positionProfieWorkflowDetail) &&
                !Utility.IsNullOrEmpty(positionProfieWorkflowDetail.SubmittedCandidateInformation) &&
                positionProfieWorkflowDetail.SubmittedCandidateInformation.Count > 0)
            {
                PositionProfileWorkflowSelection_submittedCandidatesHyperLink.Text =
                    positionProfieWorkflowDetail.SubmittedCandidateInformation.Count.ToString();

                PositionProfileWorkflowSelection_submittedCandidatesHyperLink.NavigateUrl =
                   "~/PositionProfile/PositionProfileStatus.aspx?m=1&s=1&positionprofileid=" + positionProfileID + "&type=s&parentpage=S_POS_PRO";
            }
            else
            {
                PositionProfileWorkflowSelection_submittedCandidatesHyperLink.Text = "0";
                PositionProfileWorkflowSelection_submittedCandidatesHyperLink.ToolTip = string.Empty;
                PositionProfileWorkflowSelection_submittedCandidatesHyperLink.NavigateUrl = "";
            }
        }

        /// <summary>
        /// Method to print all the associated candidate names
        /// </summary>
        private void BindAssociatedCandidates()
        {
            List<CandidateInformation> candidateInformation = 
                new List<CandidateInformation>();

            candidateInformation = new PositionProfileBLManager().
               GetPositionProfileCandidatesByPositionProfileID(positionProfileID);

            if (candidateInformation != null)
            {
                PositionProfileWorkflowSelection_noOfCandidatesLinkButton.Text
                    = candidateInformation.Count.ToString();
                PositionProfileWorkflowSelection_candidateDataList.DataSource
                    = candidateInformation;
                PositionProfileWorkflowSelection_candidateDataList.DataBind();
            }
            else
            {
                PositionProfileWorkflowSelection_noOfCandidatesLinkButton.Text = "0";
            }
        }

        /// <summary>
        /// Method to construct the position profile actions
        /// </summary>
        /// <returns></returns>
        private List<PositionProfileDetail> ConstructPositionProfileActions()
        {
            List<PositionProfileDetail> positionProfileActions = null;

            if (PositionProfileWorkflowSelection_interviewActiveImageButton.ImageUrl ==
                    "~/App_Themes/DefaultTheme/Images/pp_active.png")
            {
                if (positionProfileActions == null)
                    positionProfileActions = new List<PositionProfileDetail>();

                PositionProfileDetail profileOnwerAction = new PositionProfileDetail();
                profileOnwerAction.PositionProfileOwnerAction = "PAT_INT_RQ";
                positionProfileActions.Add(profileOnwerAction);

                profileOnwerAction = null;
                profileOnwerAction = new PositionProfileDetail();
                profileOnwerAction.PositionProfileOwnerAction = "PAT_INT_SS";
                positionProfileActions.Add(profileOnwerAction);

                profileOnwerAction = null;
                profileOnwerAction = new PositionProfileDetail();
                profileOnwerAction.PositionProfileOwnerAction = "PAT_INT_SD";
                positionProfileActions.Add(profileOnwerAction);
            }

            if (PositionProfileWorkflowSelection_testActiveImageButton.ImageUrl ==
                "~/App_Themes/DefaultTheme/Images/pp_active.png")
            {
                if (positionProfileActions == null)
                    positionProfileActions = new List<PositionProfileDetail>();

                PositionProfileDetail profileOnwerAction = new PositionProfileDetail();

                profileOnwerAction = new PositionProfileDetail();
                profileOnwerAction.PositionProfileOwnerAction = "PAT_TST_RQ";
                positionProfileActions.Add(profileOnwerAction);

                profileOnwerAction = null;
                profileOnwerAction = new PositionProfileDetail();
                profileOnwerAction.PositionProfileOwnerAction = "PAT_TST_SD";
                positionProfileActions.Add(profileOnwerAction);

                profileOnwerAction = null;
                profileOnwerAction = new PositionProfileDetail();
                profileOnwerAction.PositionProfileOwnerAction = "PAT_TST_SS";
                positionProfileActions.Add(profileOnwerAction);
            }

            return positionProfileActions;
        }

        /// <summary>
        /// Method that retrieves the position profile detail and assign it the 
        /// hidden fields for later reference.
        /// </summary>
        private void BindPositionProfileDetail(int positionProfileID)
        {
            PositionProfileDetail positionProfileDetail = new PositionProfileBLManager().GetPositionProfile(positionProfileID);

            if (positionProfileDetail == null)
                return;

            // Assign name & client to hidden fields.
            PositionProfileWorkflowSelection_positionProfileNameHiddenField.Value = positionProfileDetail.PositionName;
            PositionProfileWorkflowSelection_clientNameHiddenField.Value = positionProfileDetail.ClientName;
        }

        /// <summary>
        /// Method to bind the task owners by position profile id
        /// </summary>
        private void BindTaskOwners()
        {
            List<UserDetail> taskOwners = new List<UserDetail>();
            taskOwners = new PositionProfileBLManager().
               GetPositionProfileTaskOwners(positionProfileID,base.userID);

            if (taskOwners != null && taskOwners.Count > 0)
            {
                ViewState["TASK_OWNER"] = taskOwners;

                AssignOwners(Constants.PositionProfileOwners.RECRUITER_ASSIGNMENT_CREATOR);
                AssignOwners(Constants.PositionProfileOwners.CANDIDATE_ASSOCIATION_CREATOR);
                AssignOwners(Constants.PositionProfileOwners.INTERVIEW_CREATOR);
                AssignOwners(Constants.PositionProfileOwners.INTERVIEW_SESSION_CREATOR);
                AssignOwners(Constants.PositionProfileOwners.INTERVIEW_SCHEDULER);
                AssignOwners(Constants.PositionProfileOwners.ASSESSOR_ASSIGNMENT);
                AssignOwners(Constants.PositionProfileOwners.TEST_CREATOR);
                AssignOwners(Constants.PositionProfileOwners.TEST_SESSION_CREATOR);
                AssignOwners(Constants.PositionProfileOwners.TEST_SCHEDULER);
                AssignOwners(Constants.PositionProfileOwners.SUBMITTAL_TO_CLIENT);
            }
        }

        /// <summary>
        /// Method to insert the default owners
        /// </summary>
        private void InsertDefaultOwners()
        {
            new PositionProfileBLManager().InsertPositionProfileDefaultOwner(
                positionProfileID, base.userID);
        }

        /// <summary>
        /// Method to set the required section visible
        /// </summary>
        private void SetRequiredSection(bool interviewSection,bool testSection)
        {
            if (interviewSection)
            {
                PositionProfileWorkflowSelection_interviewActiveImageButton.ImageUrl =
                    "~/App_Themes/DefaultTheme/Images/pp_active.png";
                PositionProfileWorkflowSelection_showInterviewRequiredSectionDiv.Style["display"] = "block";
                PositionProfileWorkflowSelection_showInterviewRequiredSectionDownArrowDiv.Style["display"] = "block";
            }
            else
            {
                PositionProfileWorkflowSelection_interviewActiveImageButton.ImageUrl =
                    "~/App_Themes/DefaultTheme/Images/pp_deactive.png";
                PositionProfileWorkflowSelection_showInterviewRequiredSectionDiv.Style["display"] = "none";
                PositionProfileWorkflowSelection_showInterviewRequiredSectionDownArrowDiv.Style["display"] = "none";
            }
            if (testSection)
            {
                PositionProfileWorkflowSelection_testActiveImageButton.ImageUrl =
                   "~/App_Themes/DefaultTheme/Images/pp_active.png";
                PositionProfileWorkflowSelection_showTestRequiredSectionDiv.Style["display"] = "block";
                PositionProfileWorkflowSelection_showTestRequiredSectionDownArrowDiv.Style["display"] = "block";
            }
            else
            {
                PositionProfileWorkflowSelection_testActiveImageButton.ImageUrl =
                   "~/App_Themes/DefaultTheme/Images/pp_deactive.png";
                PositionProfileWorkflowSelection_showTestRequiredSectionDiv.Style["display"] = "none";
                PositionProfileWorkflowSelection_showTestRequiredSectionDownArrowDiv.Style["display"] = "none";
            }

            if (interviewSection == true || testSection == true)
                PositionProfileWorkflowSelection_showSubmittalRequiredDiv.Style["display"] = "block";
            else
                PositionProfileWorkflowSelection_showSubmittalRequiredDiv.Style["display"] = "none";
        }

        /// <summary>
        /// Method that send email to position profile associates indicating
        /// the status change of position profile.
        /// </summary>
        /// <param name="positionProfileDetail">
        /// A <see cref="PositionProfileDetail"/> that holds the position profile 
        /// detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type. In this case 
        /// AssociatedWithPositionProfile & DissociatedFromPositionProfile are
        /// the possible values.
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendStatusChangeAlertEmail(PositionProfileDetail positionProfileDetail, EntityType entityType)
        {
            try
            {
                // Construct position profile detail.
                // Send email.
                new EmailHandler().SendMail(entityType, positionProfileDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Private Methods

        #region Asynchronous Method Handlers

        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendAlertEmailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Asynchronous Method Handlers
    }
}