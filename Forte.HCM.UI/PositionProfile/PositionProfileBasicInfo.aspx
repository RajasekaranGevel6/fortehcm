﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PositionProfileMaster.Master"
    CodeBehind="PositionProfileBasicInfo.aspx.cs" Inherits="Forte.HCM.UI.PositionProfile.PositionProfileBasicInfo" %>

<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="pageNavigator" TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/AddClientControl.ascx" TagName="AddClientControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/SegmentListControl.ascx" TagName="SegmentListControl"
    TagPrefix="uc3" %>
<%@ Register Src="../Segments/VerticalBackgroundRequirementControl.ascx" TagName="VerticalBackgroundRequirementControl"
    TagPrefix="uc4" %>
<%@ Register Src="../Segments/TechnicalSkillRequirementControl.ascx" TagName="TechnicalSkillRequirementControl"
    TagPrefix="uc5" %>
<%@ Register Src="../Segments/RoleRequirementControl.ascx" TagName="RoleRequirementControl"
    TagPrefix="uc6" %>
<%@ Register Src="../Segments/EducationRequirementControl.ascx" TagName="EducationRequirementControl"
    TagPrefix="uc7" %>
<%@ Register Src="../Segments/ClientPositionDetailsControl.ascx" TagName="ClientPositionDetailsControl"
    TagPrefix="uc8" %>
<%@ Register Src="~/CommonControls/PositionProfileWorkflowControl.ascx" TagName="PositionProfileWorkflowControl"
    TagPrefix="uc9" %>
<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content ID="PositionProfileBasicInfo_bodyContent" runat="server" ContentPlaceHolderID="PositionProfileMaster_body">
    <script language="javascript" type="text/javascript">
        function ShowLoadingImage() {
            document.getElementById("<%=PositionProfileBasicInfo_clientNameTextBox.ClientID%>").className = "position_profile_client_bg";
        }
        function HideLoadingImage() {
            document.getElementById("<%=PositionProfileBasicInfo_clientNameTextBox.ClientID%>").className = "";
        }
        //date change event handler for grid's calendarExtender
       
        
        function dateSelectionChanged(sender, args) {
            debugger;
           
            let visibleDate = sender._visibleDate.getDateOnly().format("dd-MM-yyyy");
              
                // set the date back to the current date
            sender._textbox.set_Value(visibleDate);
            
        }
    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="padding-bottom:5px">
                <uc9:positionprofileworkflowcontrol id="PositionProfileBasicInfo_workflowControl"
                    runat="server" visible="true" pagetype="BI" />
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="PositionProfileBasicInfo_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="PositionProfileBasicInfo_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="PositionProfileBasicInfo_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <asp:HiddenField ID="PositionProfileBasicInfo_editPositionProfileIdHiddenField" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <!-- Part 1-->
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="PositionProfileBasicInfo_profileNameLabel" runat="server" SkinID="sknLabelText"
                                                        Text="Job description Name"></asp:Label><span class="mandatory">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="PositionProfileBasicInfo_positionProfileNameTextBox" runat="server"
                                                        MaxLength="200" Width="400px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td align="right">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="PositionProfileBasicInfo_profileIDLabel" runat="server" SkinID="sknLabelText"
                                                                    Text="Job description ID"></asp:Label>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="PositionProfileBasicInfo_profileIDLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="PositionProfileBasicInfo_dateOfRegistrationLabel" runat="server" SkinID="sknLabelText"
                                                        Text="Date Of Registration"></asp:Label><span class="mandatory">*</span>
                                                </td>
                                                <td colspan="5">
                                                    <table cellpadding="0" cellspacing="0" width="57%">
                                                        <tr>
                                                            <td style="width: 100px">
                                                                <div style="float: left; padding-right: 2px;">
                                                                    <asp:TextBox ID="PositionProfileBasicInfo_dateOfRegistrationTextBox" runat="server"
                                                                        Width="80px"></asp:TextBox>
                                                                </div>
                                                                <div style="float: left;">
                                                                    <asp:ImageButton ID="PositionProfileBasicInfo_dateofRegistrationCalenderImageButton"
                                                                        SkinID="sknCalendarImageButton" runat="server" ImageAlign="Middle" ToolTip="Select Date Of Registration" />
                                                                </div>
                                                                <ajaxtoolkit:maskededitextender id="PositionProfileBasicInfo_dateofRegistrationMaskedEditExtender"
                                                                    runat="server" targetcontrolid="PositionProfileBasicInfo_dateofRegistrationTextBox"
                                                                    mask="99/99/9999" messagevalidatortip="true" onfocuscssclass="MaskedEditFocus"
                                                                    oninvalidcssclass="MaskedEditError" masktype="Date" displaymoney="Left" acceptnegative="Left"
                                                                    errortooltipenabled="True" />
                                                                <ajaxtoolkit:maskededitvalidator id="PositionProfileBasicInfo_maskedEditValidator"
                                                                    runat="server" controlextender="PositionProfileBasicInfo_dateofRegistrationMaskedEditExtender"
                                                                    controltovalidate="PositionProfileBasicInfo_dateofRegistrationTextBox" emptyvaluemessage="Date is required"
                                                                    invalidvaluemessage="Date is invalid" display="None" tooltipmessage="Input a date"
                                                                    emptyvalueblurredtext="*" invalidvalueblurredmessage="*" validationgroup="MKE" />
                                                                <ajaxtoolkit:calendarextender id="PositionProfileBasicInfo_processedDateToCalendarExtender"
                                                                    runat="server" targetcontrolid="PositionProfileBasicInfo_dateofRegistrationTextBox" OnClientDateSelectionChanged="dateSelectionChanged"
                                                                    cssclass="MyCalendar" format="MM/dd/yyyy" popupposition="BottomLeft" popupbuttonid="PositionProfileBasicInfo_dateofRegistrationCalenderImageButton" />
                                                            </td>
                                                            <td style="width: 100px">
                                                                <asp:Label ID="PositionProfileBasicInfo_submittalDeadLineLabel" runat="server" SkinID="sknLabelText"
                                                                    Text="Submittal Deadline"></asp:Label><span class="mandatory">*</span>
                                                            </td>
                                                            <td style="width: 100px">
                                                                <div style="float: left; padding-right: 2px;">
                                                                    <asp:TextBox ID="PositionProfileBasicInfo_submittalDeadLineTextBox" runat="server"
                                                                        Width="80px"></asp:TextBox>
                                                                </div>
                                                                <div style="float: left;">
                                                                    <asp:ImageButton ID="PositionProfileBasicInfo_submittalDeadLineCallenderImageButton"
                                                                        SkinID="sknCalendarImageButton" runat="server" ImageAlign="Middle" ToolTip="Select Submittal Deadline" />
                                                                </div>
                                                                <ajaxtoolkit:maskededitextender id="PositionProfileBasicInfo_submittalDeadLineMaskedEditExtender"
                                                                    runat="server" targetcontrolid="PositionProfileBasicInfo_submittalDeadLineTextBox"
                                                                    mask="99/99/9999" messagevalidatortip="true" onfocuscssclass="MaskedEditFocus" 
                                                                    oninvalidcssclass="MaskedEditError" masktype="Date" displaymoney="Left" acceptnegative="Left"
                                                                    errortooltipenabled="True" />
                                                                <ajaxtoolkit:maskededitvalidator id="MaskedEditValidator1" runat="server" controlextender="PositionProfileBasicInfo_submittalDeadLineMaskedEditExtender"
                                                                    controltovalidate="PositionProfileBasicInfo_submittalDeadLineTextBox" emptyvaluemessage="Date is required"
                                                                    invalidvaluemessage="Date is invalid" display="None" tooltipmessage="Input a date"
                                                                    emptyvalueblurredtext="*" invalidvalueblurredmessage="*" validationgroup="MKE" />
                                                                <ajaxtoolkit:calendarextender id="CalendarExtender1" runat="server" targetcontrolid="PositionProfileBasicInfo_submittalDeadLineTextBox" OnClientDateSelectionChanged ="dateSelectionChanged"
                                                                    cssclass="MyCalendar" format="MM/dd/yyyy" popupposition="BottomLeft" popupbuttonid="PositionProfileBasicInfo_submittalDeadLineCallenderImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="PositionProfileBasicInfo_additionalInformationLabel" runat="server"
                                                        SkinID="sknLabelText" Text="Additional Information"></asp:Label>
                                                </td>
                                                <td align="right">
                                                    <asp:Label ID="PositionProfileBasicInfo_createdByLabel" runat="server" SkinID="sknLabelText"
                                                        Text="Created By : "></asp:Label>
                                                    <asp:Label ID="PositionProfileBasicInfo_createdByLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8">
                                                    <asp:TextBox ID="PositionProfileBasicInfo_additionInformationTextBox" runat="server"
                                                        MaxLength="500" TextMode="MultiLine" onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"
                                                        Rows="4" Columns="160"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- -->
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <!-- Part 2-->
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table class="panel_bg" cellpadding="0" cellspacing="0" style="width: 470px;">
                                            <tr>
                                                <td>
                                                    <table width="100%" cellpadding="4" cellspacing="4">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Label ID="PositionProfileBasicInfo_clientLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Client"></asp:Label>
                                                            </td>
                                                            <td align="right">
                                                                <asp:UpdatePanel ID="PositionProfileBasicInfo_addClientUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:ImageButton ID="PositionProfileBasicInfo_addClientImageButton" runat="server"
                                                                            OnCommand="PositionProfileBasicInfo_addClientImageButton_Command" SkinID="sknPPAddImageButton"
                                                                            ToolTip="Add Client" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Image runat="server" SkinID="sknPPLineImage" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="PositionProfileBasicInfo_clientNameUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:TextBox ID="PositionProfileBasicInfo_clientNameTextBox" runat="server" MaxLength="50"
                                                                            Width="400px" Height="25px" OnTextChanged="PositionProfileBasicInfo_clientNameTextBox_TextChanged"
                                                                            AutoPostBack="true" Style="font-size: 100%; font-family: Tahoma, Arial, sans-serif;
                                                                            color: gray; font-style: normal; text-align: left; vertical-align: middle;"></asp:TextBox>
                                                                        <ajaxtoolkit:textboxwatermarkextender id="TextBoxWatermarkExtender" runat="server"
                                                                            targetcontrolid="PositionProfileBasicInfo_clientNameTextBox" watermarktext="Enter client name"
                                                                            watermarkcssclass="position_profile_client_water">
                                                                        </ajaxtoolkit:textboxwatermarkextender>
                                                                        <div id="PositionProfileBasicInfo_showClientList" style="height: 100px; width: 250px;">
                                                                        </div>
                                                                        <ajaxtoolkit:autocompleteextender id="PositionProfileBasicInfo_clientNameAutoCompleteExtender"
                                                                            behaviorid="PositionProfileBasicInfo_clientNameBehaviorID" runat="server" servicepath="~/AutoComplete.asmx"
                                                                            servicemethod="GetClientList" targetcontrolid="PositionProfileBasicInfo_clientNameTextBox"
                                                                            minimumprefixlength="1" completionlistelementid="PositionProfileBasicInfo_showClientList"
                                                                            onclientpopulating="ShowLoadingImage" completionlistcssclass="position_profile_client_completion_list"
                                                                            completionlistitemcssclass="position_profile_client_completion_list_item" onclientpopulated="HideLoadingImage"
                                                                            onclientshowing="ShowLoadingImage" onclienthiding="HideLoadingImage" completionlisthighlighteditemcssclass="position_profile_client_completion_list_highlight"
                                                                            enablecaching="true" completionsetcount="12">
                                                                        </ajaxtoolkit:autocompleteextender>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                            <td valign="top">
                                                                <asp:UpdatePanel ID="PositionProfileBasicInfo_searchClientUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:ImageButton ID="PositionProfileBasicInfo_searchClientImageButton" runat="server"
                                                                            SkinID="sknPPSearchImageButton" ToolTip="Select Client" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileBasicInfo_clientNameTextBox" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                                <div style="display: none">
                                                                    <asp:Button ID="PositionProfileBasicInfo_refreshDepartmentButton" runat="server"
                                                                        Text="Refresh" SkinID="sknButtonId" OnClick="PositionProfileBasicInfo_refreshDepartmentButton_Click" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Label ID="PositionProfileBasicInfo_departmentLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Department"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:UpdatePanel ID="PositionProfileBasicInfo_departmentUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:TextBox ID="PositionProfileBasicInfo_departmentTextBox" ReadOnly="true" runat="server"
                                                                            Width="400px" Height="20px"></asp:TextBox>
                                                                        <asp:Panel ID="PositionProfileBasicInfo_departmentPanel" runat="server" CssClass="position_profile_client_chkbox_div">
                                                                            <asp:CheckBoxList ID="PositionProfileBasicInfo_departmentCheckBoxList" RepeatColumns="1"
                                                                                runat="server" CellPadding="0" OnSelectedIndexChanged="PositionProfileBasicInfo_departmentCheckBoxList_SelectedIndexChanged"
                                                                                AutoPostBack="true">
                                                                            </asp:CheckBoxList>
                                                                        </asp:Panel>
                                                                        <ajaxtoolkit:popupcontrolextender id="PositionProfileBasicInfo_departmentListPopupControlExtender"
                                                                            runat="server" targetcontrolid="PositionProfileBasicInfo_departmentTextBox" popupcontrolid="PositionProfileBasicInfo_departmentPanel"
                                                                            offsetx="2" offsety="2" position="Bottom">
                                                                        </ajaxtoolkit:popupcontrolextender>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileBasicInfo_clientNameTextBox" />
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileBasicInfo_refreshDepartmentButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Label ID="PositionProfileBasicInfo_contactLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Contact"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:UpdatePanel ID="PositionProfileBasicInfo_contactUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:TextBox ID="PositionProfileBasicInfo_contactTextbox" runat="server" ReadOnly="true"
                                                                            Width="400px" Height="20px"></asp:TextBox>
                                                                        <asp:Panel ID="PositionProfileBasicInfo_contactPanel" runat="server" CssClass="position_profile_client_chkbox_div">
                                                                            <asp:CheckBoxList ID="PositionProfileBasicInfo_contactCheckBoxList" RepeatColumns="1"
                                                                                runat="server" CellPadding="0" OnSelectedIndexChanged="PositionProfileBasicInfo_contactCheckBoxList_SelectedIndexChanged"
                                                                                AutoPostBack="true">
                                                                            </asp:CheckBoxList>
                                                                        </asp:Panel>
                                                                        <ajaxtoolkit:popupcontrolextender id="PositionProfileBasicInfo_contactListPopupControlExtender"
                                                                            runat="server" targetcontrolid="PositionProfileBasicInfo_contactTextbox" popupcontrolid="PositionProfileBasicInfo_contactPanel"
                                                                            offsetx="2" offsety="2" position="Bottom">
                                                                        </ajaxtoolkit:popupcontrolextender>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileBasicInfo_clientNameTextBox" />
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileBasicInfo_refreshDepartmentButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td valign="top">
                                        <table class="panel_bg" cellpadding="0" cellspacing="0" style="width: 470px; height: 232px;">
                                            <tr>
                                                <td valign="top">
                                                    <table width="100%" cellpadding="4" cellspacing="4">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Label ID="PositionProfileBasicInfo_coownerLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Job description Co-Owners"></asp:Label>
                                                            </td>
                                                            <td align="right">
                                                                <asp:UpdatePanel ID="PositionProfileBasicInfo_coownerUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:ImageButton ID="PositionProfileBasicInfo_coOwnerImageButton" runat="server"
                                                                            SkinID="sknPPAddImageButton" OnClick="PositionProfileBasicInfo_coOwnerImageButton_Click"
                                                                            ToolTip="Select Co-Owners" />
                                                                        <asp:HiddenField ID="PositionProfileBasicInfo_coOwnerUserIDHiddenField" runat="server" />
                                                                        <asp:HiddenField ID="PositionProfileBasicInfo_coOwnerFirstNameHiddenField" runat="server" />
                                                                        <asp:HiddenField ID="PositionProfileBasicInfo_coOwnerLastNameHiddenField" runat="server" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Image runat="server" SkinID="sknPPLineImage" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" colspan="2">
                                                                <div style="height: 120px; overflow: auto; display: block;" runat="server">
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <div id="PositionProfileBasicInfo_coOwnerDiv" style="height: 120px; overflow: auto;"
                                                                                    runat="server">
                                                                                    <asp:UpdatePanel ID="PositionProfileBasicInfo_coOwnerGridViewUpdatePanel" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <asp:GridView ID="PositionProfileBasicInfo_coOwnerGridView" runat="server" ShowHeader="false"
                                                                                                SkinID="sknPPOwnersGridView" OnRowCommand="PositionProfileBasicInfo_coOwnerGridView_RowCommand"
                                                                                                OnRowDataBound="PositionProfileBasicInfo_coOwnerGridView_OnRowDataBound">
                                                                                                <EmptyDataRowStyle CssClass="error_message_text" />
                                                                                                <EmptyDataTemplate>
                                                                                                    <table style="width: 100%">
                                                                                                        <tr>
                                                                                                            <td style="height: 60px">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                No co-owners to display
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </EmptyDataTemplate>
                                                                                                <Columns>
                                                                                                    <asp:TemplateField ItemStyle-Width="8%">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="PositionProfileBasicInfo_deleteCoOwnerImageButton" runat="server"
                                                                                                                SkinID="sknPPDeleteImageButton" CommandArgument='<%# Eval("UserID") %>' CommandName="DeleteCoOwner"
                                                                                                                ToolTip="Delete Co-Owner" />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="PositionProfileBasicInfo_coOwnerGridView_nameLabel" runat="server"
                                                                                                                Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:LinkButton ID="PositionProfileBasicInfo_coOwnerGridView_workLoadLinkButton"
                                                                                                                runat="server" CommandArgument='<%# Eval("UserID") %>' ToolTip="View Workload"
                                                                                                                Text="View Workload"></asp:LinkButton>
                                                                                                            <asp:HiddenField ID="PositionProfileBasicInfo_coOwnerGridView_userIDHiddenField"
                                                                                                                runat="server" Value='<%# Eval("UserID") %>' />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:AsyncPostBackTrigger ControlID="PositionProfileBasicInfo_coOwnerImageButton" />
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="PositionProfileBasicInfo_clientModalPopUpUpdatePanel" UpdateMode="Always"
                                runat="server">
                                <ContentTemplate>
                                    <asp:HiddenField ID="PositionProfileBasicInfo_clientIDHiddenField" runat="server" />
                                    <asp:HiddenField ID="PositionProfileBasicInfo_clientNameHiddenField" runat="server" />
                                    <span style="display: none">
                                        <asp:Button ID="PositionProfileBasicInfo_clientDummybutton" runat="server" />
                                    </span>
                                    <asp:Panel ID="PositionProfileBasicInfo_clientPaenl" runat="server" Style="display: none"
                                        CssClass="client_form_details">
                                        <uc2:addclientcontrol id="PositionProfileBasicInfo_clientControl" runat="server" />
                                    </asp:Panel>
                                    <ajaxtoolkit:modalpopupextender id="PositionProfileBasicInfo_clientModalpPopupExtender"
                                        runat="server" targetcontrolid="PositionProfileBasicInfo_clientDummybutton" popupcontrolid="PositionProfileBasicInfo_clientPaenl"
                                        backgroundcssclass="modalBackground">
                                    </ajaxtoolkit:modalpopupextender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <!-- -->
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <!-- Part 3 -->
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table class="panel_bg" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="4">
                                                        <tr>
                                                            <td align="right" colspan="2">
                                                                <asp:UpdatePanel ID="PositionProfileBasicInfo_customizedSegmentsUpadtePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="PositionProfileBasicInfo_addSegmentLinkButton" runat="server"
                                                                            SkinID="sknAddLinkButton" Text="Add" ToolTip="Click here to add Segment(s)" OnClick="PositionProfileBasicInfo_addSegmentLinkButton_Click" />
                                                                        &nbsp;&nbsp;
                                                                        <asp:Image ID="PositionProfileBasicInfo_customizedSegmentsImageButtion" runat="server"
                                                                            SkinID="sknPPFormEditImageButton" />&nbsp;&nbsp;
                                                                        <asp:LinkButton ID="PositionProfileBasicInfo_segmentsLinkButton" Text="Load Standard Segment"
                                                                            ToolTip="Click here to load standard segment" runat="server" OnClick="PositionProfileBasicInfo_segmentsLinkButton_Click"
                                                                            SkinID="sknActionLinkButton"></asp:LinkButton>
                                                                        <asp:HiddenField ID="PositionProfileBasicInfo_isCustomizedSegmentHiddenField" runat="server" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Image runat="server" SkinID="sknPPLineImage" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:UpdatePanel ID="PositionProfileBasicInfo_customizedSegmentsUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <div style="overflow: auto; display: block;" runat="server" id="PositionProfileBasicInfo_customizedSegmentsDiv">
                                                                            <table cellpadding="2" cellspacing="2" width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <div style="overflow: auto; display: block;" runat="server" id="PositionProfileBasicInfo_customizedSegmentsFormDiv">
                                                                                            <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="PositionProfileBasicInfo_customizedSegmentsLabel" runat="server" CssClass="position_profile_header_title"
                                                                                                            Text="Select from an Existing Form"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:UpdatePanel ID="PositionProfileBasicInfo_customizedSegmentsDropDownUpdatePanel"
                                                                                                            runat="server">
                                                                                                            <ContentTemplate>
                                                                                                                <asp:DropDownList ID="PositionProfileBasicInfo_customizedSegmentsDropDownList" runat="server"
                                                                                                                    OnSelectedIndexChanged="PositionProfileBasicInfo_customizedSegmentsDropDownList_SelectedIndexChanged"
                                                                                                                    AutoPostBack="true" />
                                                                                                            </ContentTemplate>
                                                                                                        </asp:UpdatePanel>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div style="height: 200px; overflow: auto; display: none;" runat="server" id="PositionProfileBasicInfo_customizedSegmentsGridViewDiv">
                                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="grid_body_form_bg">
                                                                                                        <div style="height: 170px; overflow: auto;" runat="server">
                                                                                                            <asp:UpdatePanel ID="PositionProfileBasicInfo_customizedSegmentsGridViewUpdatePanel"
                                                                                                                runat="server">
                                                                                                                <ContentTemplate>
                                                                                                                    <asp:GridView ID="PositionProfileBasicInfo_customizedSegmentsGridView" runat="server"
                                                                                                                        OnRowDataBound="PositionProfileBasicInfo_customizedSegmentsGridView_RowDataBound"
                                                                                                                        ShowHeader="false" OnRowCommand="PositionProfileBasicInfo_customizedSegmentsGridView_RowCommand">
                                                                                                                        <Columns>
                                                                                                                            <asp:TemplateField ItemStyle-Width="20%">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:ImageButton ID="PositionProfileBasicInfo_previewSegmentsGridViewImageButton"
                                                                                                                                        runat="server" SkinID="sknPreviewImageButton" CommandName="PreviewControl" CommandArgument='<%# Eval("SegmentID") %>'
                                                                                                                                        ToolTip="Preview" />
                                                                                                                                    <asp:ImageButton ID="PositionProfileBasicInfo_customizedSegmentsGridView_deleteImageButton"
                                                                                                                                        CommandArgument='<%# Eval("SegmentID") %>' runat="server" SkinID="sknDeleteImageButton"
                                                                                                                                        CommandName="DeleteSegment" ToolTip="Delete Segment" />
                                                                                                                                    <asp:ImageButton ID="PositionProfileBasicInfo_customizedSegmentsGridView_moveDownImageButton"
                                                                                                                                        runat="server" SkinID="sknMoveDown_ArrowImageButton" CommandName="MoveDown" CommandArgument='<%# Eval("DisplayOrder") %>'
                                                                                                                                        ToolTip="Move Segment Down" />
                                                                                                                                    <asp:ImageButton ID="PositionProfileBasicInfo_customizedSegmentsGridView_moveUpImageButton"
                                                                                                                                        runat="server" SkinID="sknMoveUp_ArrowImageButton" CommandName="MoveUp" CommandArgument='<%# Eval("DisplayOrder") %>'
                                                                                                                                        ToolTip="Move Segment Up" />
                                                                                                                                    <asp:HiddenField ID="PositionProfileBasicInfo_customizedSegmentsGridView_displayOrderHiddenField"
                                                                                                                                        runat="server" Value='<%# Eval("DisplayOrder") %>' />
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:HiddenField ID="PositionProfileBasicInfo_customizedSegmentsGridView_segmentIDHiddenField"
                                                                                                                                        runat="server" Value='<%# Eval("SegmentID") %>' />
                                                                                                                                    <asp:Label ID="PositionProfileBasicInfo_customizedSegmentsGridView_segmentNameLabel"
                                                                                                                                        runat="server" Text='<%# Eval("SegmentName") %>'></asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                        </Columns>
                                                                                                                    </asp:GridView>
                                                                                                                </ContentTemplate>
                                                                                                                <Triggers>
                                                                                                                    <asp:AsyncPostBackTrigger ControlID="PositionProfileBasicInfo_customizedSegmentsDropDownList" />
                                                                                                                </Triggers>
                                                                                                            </asp:UpdatePanel>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileBasicInfo_segmentsLinkButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="PositionProfileBasicInfo_customizedSegmentsDeleteUpdatePanel"
                                                                    runat="server">
                                                                    <ContentTemplate>
                                                                        <div style="display: none">
                                                                            <asp:Button ID="PositionProfileBasicInfo_customizedSegmentsDeleteHiddenButton" runat="server" />
                                                                        </div>
                                                                        <asp:Panel ID="PositionProfileBasicInfo_customizedSegmentsDeletePopupPanel" runat="server"
                                                                            Style="display: none" CssClass="popupcontrol_confirm_remove">
                                                                            <uc1:confirmmsgcontrol id="PositionProfileBasicInfo_customizedSegmentsDeleteConfirmMsgControl"
                                                                                runat="server" onokclick="PositionProfileBasicInfo_customizedSegmentsDeleteConfirmMsgControl_okClick"
                                                                                oncancelclick="PositionProfileBasicInfo_customizedSegmentsConfirmMsgControl_cancelClick" />
                                                                            <asp:HiddenField ID="PositionProfileBasicInfo_customizedSegmentsDeleteHiddenFiled"
                                                                                runat="server" />
                                                                        </asp:Panel>
                                                                        <ajaxtoolkit:modalpopupextender id="PositionProfileBasicInfo_customizedSegmentsModalPopupExtender"
                                                                            runat="server" popupcontrolid="PositionProfileBasicInfo_customizedSegmentsDeletePopupPanel"
                                                                            targetcontrolid="PositionProfileBasicInfo_customizedSegmentsDeleteHiddenButton"
                                                                            backgroundcssclass="modalBackground">
                                                                        </ajaxtoolkit:modalpopupextender>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <!-- Add new segment on edit form -->
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:UpdatePanel ID="PositionProfileBasicInfo_addSegmentUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:Button ID="PositionProfileBasicInfo_hiddenPopupModalButton" runat="server" Style="display: none" />
                                                                        <asp:Panel ID="PositionProfileBasicInfo_segmentListPanel" runat="server" Style="display: none;"
                                                                            CssClass="popupcontrol_segmentControl">
                                                                            <uc3:segmentlistcontrol id="PositionProfileBasicInfo_segmentListControl" onokclick="PositionProfileBasicInfo_segmentListControl_Add_segment"
                                                                                runat="server" />
                                                                        </asp:Panel>
                                                                        <ajaxtoolkit:modalpopupextender id="PositionProfileBasicInfo_addSegmentPopupExtender"
                                                                            runat="server" popupcontrolid="PositionProfileBasicInfo_segmentListPanel" targetcontrolid="PositionProfileBasicInfo_hiddenPopupModalButton"
                                                                            backgroundcssclass="modalBackground">
                                                                        </ajaxtoolkit:modalpopupextender>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileBasicInfo_addSegmentLinkButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <!-- Placed here -->
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:UpdatePanel ID="PositionProfileBasicInfo_placeHolderUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <div style="display: none">
                                                                            <asp:Button ID="PositionProfileBasicInfo_hiddenPlaceHolderButton" runat="server" />
                                                                        </div>
                                                                        <asp:Panel ID="PositionProfileBasicInfo_placeHolderPanel" runat="server" Style="display: none"
                                                                            CssClass="popupcontrol_segment_preview">
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="td_height_20">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" colspan="2">
                                                                                        <table width="90%" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                                                                                    <asp:Label ID="PositionProfileBasicInfo_placeHoldercancelLiteral" runat="server"
                                                                                                        Text="Segment Preview"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 25%" align="right">
                                                                                                    <asp:ImageButton ID="PositionProfileBasicInfo_placeHolderCloseImageButton" runat="server"
                                                                                                        SkinID="sknCloseImageButton" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="td_height_20" colspan="2">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2">
                                                                                                    <asp:PlaceHolder ID="PositionProfileBasicInfo_controlsPlaceHolder" runat="server">
                                                                                                    </asp:PlaceHolder>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="td_height_5">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" colspan="2">
                                                                                                    <asp:LinkButton ID="PositionProfileBasicInfo_controlsPlaceHolderCancelButton" runat="server"
                                                                                                        Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                        <ajaxtoolkit:modalpopupextender id="PositionProfileBasicInfo_placeHolderModalPopupExtender"
                                                                            runat="server" popupcontrolid="PositionProfileBasicInfo_placeHolderPanel" targetcontrolid="PositionProfileBasicInfo_hiddenPlaceHolderButton"
                                                                            backgroundcssclass="modalBackground">
                                                                        </ajaxtoolkit:modalpopupextender>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <!-- End place -->
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:UpdatePanel ID="PositionProfileBasicInfo_standardFormSegmentUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <div style="overflow: auto; display: none;" runat="server" id="PositionProfileBasicInfo_standardFormSegmentDiv">
                                                                            <table cellpadding="2" cellspacing="2" width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="PositionProfileBasicInfo_standardFormSegmentSelectLabel" runat="server"
                                                                                            CssClass="position_profile_header_title" Text="Select segment"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="grid_body_form_bg">
                                                                                                    <div style="height: 200px; overflow: auto">
                                                                                                        <asp:UpdatePanel ID="PositionProfileBasicInfo_standardFormSegmentsUpdatePanel" runat="server">
                                                                                                            <ContentTemplate>
                                                                                                                <asp:GridView ID="PositionProfileBasicInfo_standardFormSegmentsGridView" runat="server"
                                                                                                                    ShowHeader="false" OnRowCommand="PositionProfileBasicInfo_standardFormSegmentsGridView_RowCommand"
                                                                                                                    OnRowDataBound="PositionProfileBasicInfo_standardFormSegmentsGridView_RowDataBound">
                                                                                                                    <Columns>
                                                                                                                        <asp:TemplateField ItemStyle-Width="1%">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:ImageButton ID="PositionProfileBasicInfo_standardFormSegmentsGridView_previewSegmentImageButton"
                                                                                                                                    runat="server" SkinID="sknPreviewImageButton" CommandName="PreviewControl" CommandArgument='<%# Eval("SegmentID") %>'
                                                                                                                                    ToolTip="Preview" />
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateField>
                                                                                                                        <asp:TemplateField ItemStyle-Width="20%">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:ImageButton ID="PositionProfileBasicInfo_standardFormSegmentsGridView_deleteImageButton"
                                                                                                                                    CommandArgument='<%# Eval("SegmentID") %>' runat="server" SkinID="sknDeleteImageButton"
                                                                                                                                    CommandName="DeleteSegment" ToolTip="Delete Segment" />
                                                                                                                                <asp:ImageButton ID="PositionProfileBasicInfo_standardFormSegmentsGridView_moveDownImageButton"
                                                                                                                                    runat="server" SkinID="sknMoveDown_ArrowImageButton" CommandName="MoveDown" CommandArgument='<%# Eval("DisplayOrder") %>'
                                                                                                                                    ToolTip="Move Segment Down" />
                                                                                                                                <asp:ImageButton ID="PositionProfileBasicInfo_standardFormSegmentsGridView_moveUpImageButton"
                                                                                                                                    runat="server" SkinID="sknMoveUp_ArrowImageButton" CommandName="MoveUp" CommandArgument='<%# Eval("DisplayOrder") %>'
                                                                                                                                    ToolTip="Move Segment Up" />
                                                                                                                                <asp:HiddenField ID="PositionProfileBasicInfo_standardFormSegmentsGridView_displayOrderHiddenField"
                                                                                                                                    runat="server" Value='<%# Eval("DisplayOrder") %>' />
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateField>
                                                                                                                        <asp:TemplateField>
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:HiddenField ID="PositionProfileBasicInfo_standardFormSegmentsGridView_segmentIDHiddenField"
                                                                                                                                    runat="server" Value='<%# Eval("SegmentID") %>' />
                                                                                                                                <asp:Label ID="PositionProfileBasicInfo_standardFormSegmentsGridView_segmentNameLabel"
                                                                                                                                    runat="server" Text='<%# Eval("SegmentName") %>'>
                                                                                                                                </asp:Label>
                                                                                                                                <asp:HiddenField ID="PositionProfileBasicInfo_standardFormSegmentsGridView_activeHiddenField"
                                                                                                                                    runat="server" Value='<%# Eval("IsActive") %>' />
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateField>
                                                                                                                    </Columns>
                                                                                                                </asp:GridView>
                                                                                                                <asp:HiddenField ID="PositionProfileBasicInfo_standardFormSegmentsSelectedIdHiddenField"
                                                                                                                    runat="server" />
                                                                                                            </ContentTemplate>
                                                                                                        </asp:UpdatePanel>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileBasicInfo_segmentsLinkButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- -->
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc4:verticalbackgroundrequirementcontrol id="PositionProfileBasicInfo_verticalBackgroundRequirementControl"
                                runat="server" visible="false" />
                            <uc5:technicalskillrequirementcontrol id="PositionProfileBasicInfo_technicalSkillRequirementControl"
                                runat="server" visible="false" />
                            <uc6:rolerequirementcontrol id="PositionProfileBasicInfo_roleRequirementControl"
                                runat="server" visible="false" />
                            <uc7:educationrequirementcontrol id="PositionProfileBasicInfo_educationRequirementControl"
                                runat="server" visible="false" />
                            <uc8:clientpositiondetailscontrol id="PositionProfileBasicInfo_clientPositionDetailsControl"
                                runat="server" visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 130px">
                                        <asp:UpdatePanel ID="PositionProfileBasicInfo_saveContinueUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Button ID="PositionProfileBasicInfo_saveContinueButton" runat="server" Text="Save and Continue"
                                                    OnClick="PositionProfileBasicInfo_saveContinueButton_Click" SkinID="sknButtonId"
                                                    Height="26px" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td align="left">
                                        <asp:LinkButton ID="PositionProfileBasicInfo_cancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="PositionProfileBasicInfo_displayPositionSavedUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <div style="display: none">
                                        <asp:Button ID="PositionProfileBasicInfo_displayPositionSavedHiddenButton" runat="server" />
                                    </div>
                                    <asp:Panel ID="PositionProfileBasicInfo_displayPositionSavedPopupPanel" runat="server"
                                        Style="display: none" CssClass="popupcontrol_confirm_remove">
                                        <uc1:confirmmsgcontrol id="PositionProfileBasicInfo_displayPositionSavedConfirmMsgControl"
                                            runat="server" onokclick="PositionProfileBasicInfo_displayPositionSavedConfirmMsgControl_okClick"/>
                                    </asp:Panel>
                                    <ajaxtoolkit:modalpopupextender id="PositionProfileBasicInfo_displayPositionSavedModalPopupExtender"
                                        runat="server" popupcontrolid="PositionProfileBasicInfo_displayPositionSavedPopupPanel"
                                        targetcontrolid="PositionProfileBasicInfo_displayPositionSavedHiddenButton"
                                        backgroundcssclass="modalBackground">
                                   </ajaxtoolkit:modalpopupextender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="PositionProfileBasicInfo_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="PositionProfileBasicInfo_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label><asp:Label ID="PositionProfileBasicInfo_bottomErrorMessageLabel"
                                runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
