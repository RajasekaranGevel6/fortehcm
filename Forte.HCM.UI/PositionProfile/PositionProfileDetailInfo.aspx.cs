﻿
using System;
using System.Data; 
using System.Collections.Generic;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

using Resources;



namespace Forte.HCM.UI.PositionProfile
{
    public partial class PositionProfileDetailInfo :PageBase
    {
        //string positionProfile_id = Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING;
        List<int> segmentIdList = new List<int>();
        List<SegmentDetail> segmentDetailList;
        string SEGMENT_DETAILS = "SEGMENT_DETAILS";
        string POSITION_PROFILE_DETAILS = "POSITION_PROFILE_DETAILS";
        string SEGMENTS_KEYWORD = "SEGMENTS_KEYWORD";
        string EDIT_POSITION_PROFILE_KEYWORD = "EDIT_POSITION_PROFILE_KEYWORD";
        //string REQUIREMENT_PANEL_TEXTBOX = "REQUIREMENT_PANEL_TEXTBOX";
        int positionProfileId = 0;
        int featureID = 1;
        decimal totalWeightage, totalTechnicalWeightage, totalVerticalWeightage;
        decimal totalSegmentsVerticalWeightage, totalSegmentsTechnicalWeightage;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Check if user is having permission for this page.
                if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                {
                    if (!base.HasPositionProfileOwnerRights(
                        Convert.ToInt32(Request.QueryString["positionprofileid"]), base.userID, new List<string> 
                        { Constants.PositionProfileOwners.OWNER, Constants.PositionProfileOwners.CO_OWNER }))
                    {
                        // Redirect to review page.
                        Response.Redirect(@"~\PositionProfile\PositionProfileReview.aspx?m=1&s=0&positionprofileid=" + 
                            Request.QueryString["positionprofileid"], false);
                    }
                }
                ClearControls();

                Master.SetPageCaption(HCMResource.PositionProfile_EditTitle);

                // Subscribe to keyword populated event of requirements control.
                PositionProfileDetailInfo_requirementControl.KeywordPopupated += new
                    RequirementViewerControl.KeywordPopupatedDelegate(PositionProfileDetailInfo_RequirementControl_KeywordPopupated);

                // Subscribe to message thrown event of requirements control.
                PositionProfileDetailInfo_requirementControl.ControlMessageThrown += new 
                    RequirementViewerControl.ControlMessageThrownDelegate(PositionProfileDetailInfo_requirementControl_ControlMessageThrown);

                // Subscribe to file uploaded event of requirements control.
                PositionProfileDetailInfo_requirementControl.FileUploaded += new
                    RequirementViewerControl.FileUploadedDelegate(PositionProfileDetailInfo_RequirementControl_UploadClick);

                Session["USER_ID"] = base.userID;
                Session["TENANT_ID"] = base.tenantID;

                PositionProfileDetailInfo_editPositionProfileIdHiddenField.Value
                    = Request.QueryString.Get("positionprofileid");

                PositionProfileWorkflowControl.PageType = "DI";

                if (!IsPostBack)
                { 
                    if (!Utility.IsNullOrEmpty(PositionProfileDetailInfo_editPositionProfileIdHiddenField.Value))
                    SetControlPlace(DisplaySegmentControls(Convert.ToInt32
                        (PositionProfileDetailInfo_editPositionProfileIdHiddenField.Value)), "Normal");

                    BindPositionProfile();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString.Get("pp_session_key")))
                    {
                        string sessionkey = Request.QueryString.Get("pp_session_key");
                        string candidateIds = string.Empty;
                        string keywords = string.Empty;

                        new PositionProfileBLManager().GetPositionProfileCandidateTempIds(sessionkey, out candidateIds, out keywords);
                    }
                }

                if (CanCreatePositionProfileUsage(true))
                {
                    base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                             PositionProfileDetailInfo_bottomErrorMessageLabel,
                         "You have reached the maximum limit based your subscription plan. Cannot create more position profiles");
                }
            }
            catch (Exception exp) 
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                             PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
            }
        }

        private void ClearControls()
        { 
            PositionProfileDetailInfo_topErrorMessageLabel.Text =string.Empty;
            PositionProfileDetailInfo_bottomErrorMessageLabel.Text = string.Empty;

            PositionProfileDetailInfo_topSuccessMessageLabel.Text = string.Empty;
            PositionProfileDetailInfo_bottomSuccessMessageLabel.Text = string.Empty;

        }

        private DataTable DisplaySegmentControls(int positionProfileID)
        {
            try
            {
                DataTable dtSegments = new DataTable();

                dtSegments.Columns.Add("ControlName");
                dtSegments.Columns.Add("Top");
                dtSegments.Columns.Add("Dislay");
                dtSegments.Columns.Add("ControlID");
                dtSegments.AcceptChanges();

                DataRow drSegment = null;

                List<SegmentDetail> segmentList =
                    new PositionProfileBLManager().GetPositionProfileSegementList(positionProfileID);

                foreach (SegmentDetail sd in segmentList)
                {

                    if (sd.SegmentName == "Education" || 
                        sd.SegmentName == "Communication Skills" ||
                        sd.SegmentName == "Interpersonal Skills" ||
                        sd.SegmentName == "Decision Making" ||
                        sd.SegmentName == "Screening" ||
                        sd.SegmentName == "Interpersonal Skills")
                        continue;

                    drSegment = dtSegments.NewRow();
                    drSegment[0] = sd.SegmentName.Trim();
                    drSegment[1] = "0";
                    drSegment[2] = "block";
                    drSegment[3] = sd.SegmentID.ToString();
                    dtSegments.Rows.Add(drSegment);
                    
                }

                return dtSegments;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                             PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
                return null;
            }
        }

        private void SetControlPlace(DataTable dtDisplayControlNames,string displayType)
        {
            try 
            {
                if (dtDisplayControlNames == null) return;

                PositionProfileDetailInfo_positionDetail_headerDiv.Attributes.Add("style", "display:none");
                PositionProfileDetailInfo_technicalSkill_headerDiv.Attributes.Add("style", "display:none");
                PositionProfileDetailInfo_verticalBackground_headerDiv.Attributes.Add("style", "display:none");
                PositionProfileDetailInfo_rolesRequirment_headerDiv.Attributes.Add("style", "display:none");
                PositionProfileDetailInfo_positionRequirement_headerDiv.Attributes.Add("style", "display:none");

                int definedPosition = 255; 
                string divStyle = ""; 
                string tagVal = ""; 

                foreach (DataRow dr in dtDisplayControlNames.Rows)
                {
                    if (displayType == "Minimize" || displayType == "Maximize")
                        definedPosition = Convert.ToInt32(dr[1].ToString());

                    //divStyle = "TOP: " + definedPosition + "px;width:950px;border:solid 0px;display:block;position:absolute;direction:ltr;" + "Z-INDEX:" + definedPosition;
                    divStyle = "TOP: " + definedPosition + "px;width:950px;border:solid 0px;display:block;position:absolute;";
                     
                    switch (dr[0].ToString())
                    {
                        case "Verticals":
                            PositionProfileDetailInfo_verticalBackground_headerDiv.Attributes.Add("style", divStyle);
                            tagVal = "Verticals"; 

                            if (displayType == "Minimize") 
                                PositionProfileDetailInfo_verticalBackgroundRequirementDiv.Attributes.Add("style", "display:" + dr[2].ToString());                             
                            else
                                PositionProfileDetailInfo_verticalBackgroundRequirementDiv.Attributes.Add("style", "display:" + dr[2].ToString());

                            break;

                        case "Position":
                            PositionProfileDetailInfo_positionDetail_headerDiv.Attributes.Add("style", divStyle);
                            tagVal = "Position"; 

                            if (displayType == "Minimize")
                                PositionProfileDetailInfo_positionDetailDiv.Attributes.Add("style", "display:" + dr[2].ToString());
                            else
                                PositionProfileDetailInfo_positionDetailDiv.Attributes.Add("style", "display:" + dr[2].ToString());

                            break;
                        case "Technical Skills":
                            PositionProfileDetailInfo_technicalSkill_headerDiv.Attributes.Add("style", divStyle);
                            tagVal = "Technical Skills";

                            if (displayType == "Minimize")
                                PositionProfileDetailInfo_technicalSkillsDiv.Attributes.Add("style", "display:" + dr[2].ToString());
                            else
                                PositionProfileDetailInfo_technicalSkillsDiv.Attributes.Add("style", "display:" + dr[2].ToString());
                            
                            break;

                        case "Roles":
                            PositionProfileDetailInfo_rolesRequirment_headerDiv.Attributes.Add("style", divStyle);
                            tagVal = "Roles";

                            if (displayType == "Minimize")
                                PositionProfileDetailInfo_roleRequirementDiv.Attributes.Add("style", "display:" + dr[2].ToString());
                            else
                                PositionProfileDetailInfo_roleRequirementDiv.Attributes.Add("style", "display:" + dr[2].ToString());

                            break;

                        case "PositionRequirement":                            
                            tagVal = "PositionRequirement";

                            PositionProfileDetailInfo_positionRequirement_headerDiv.Attributes.Add("style", divStyle);
                            if (displayType == "Minimize")
                                PositionProfileDetailInfo_positionRequirementDiv.Attributes.Add("style", "display:" + dr[2].ToString());
                            else
                                PositionProfileDetailInfo_positionRequirementDiv.Attributes.Add("style", "display:" + dr[2].ToString());
                            break;
                    }

                    if (displayType == "Normal")
                    {
                        dr[1] = definedPosition;

                        switch (tagVal)
                        {
                            case "Verticals":
                            case "Technical Skills":
                                definedPosition += 290;
                                break;
                            case "Position":
                                definedPosition += 150;
                                break;
                            case "Roles":
                                definedPosition += 205;
                                break;
                            case "PositionRequirement":
                                definedPosition += 240;
                                break;
                        } 
                    }
                } 

                dtDisplayControlNames.AcceptChanges();

                int mainTblHeight = 0;
                int saveButtonPosition=0;
                if (dtDisplayControlNames.Rows.Count == 5)
                {
                    mainTblHeight = 1290;
                    saveButtonPosition = 1450;
                }
                else if (dtDisplayControlNames.Rows.Count == 4)
                {
                    mainTblHeight = 1090;
                    saveButtonPosition = 1280;
                }
                else if (dtDisplayControlNames.Rows.Count == 3)
                {
                    mainTblHeight = 920;
                    saveButtonPosition = 1080;
                    /*mainTblHeight =890;
                    saveButtonPosition = 1050;*/
                }
                else if (dtDisplayControlNames.Rows.Count == 2)
                {
                    mainTblHeight = 690;
                    saveButtonPosition = 850;
                }
                else if (dtDisplayControlNames.Rows.Count == 1)
                {
                    mainTblHeight = 450;
                    saveButtonPosition = 610;
                }

                SetSaveButtonPosition(GetBlockedHeight(dtDisplayControlNames),mainTblHeight,saveButtonPosition);

                ViewState["DISPLAY_CONTROLS"] = dtDisplayControlNames;
                ViewState["RESIZE_POSITION_TABLE"] = dtDisplayControlNames;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                             PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
            }
        }

        private int GetBlockedHeight(DataTable dt)
        {
            try
            {
                int blockedHeight = 0;

                foreach (DataRow dr in dt.Rows)
                {
                    if (dr[2].ToString() == "none")
                        switch (dr[0].ToString())
                        {
                            case "Position":
                                blockedHeight += 120;
                                break;
                            case "Verticals":
                            case "Technical Skills":
                                blockedHeight += 260;
                                break;
                            case "Roles":
                                blockedHeight += 175;
                                break;
                            case "PositionRequirement":
                                blockedHeight += 210;
                                break;
                        }
                }
                return blockedHeight;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                             PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
                return 0;
            }
        }

        private void SetSaveButtonPosition(int blockedHeight,int mainTableHeight,int saveBtnPosition)
        {
            string saveButtonPosition = "top:" + (saveBtnPosition - blockedHeight) + "px;width:75%;position:absolute;" + "Z-INDEX:" + (saveBtnPosition - blockedHeight);  
            PositionProfileDetailInfo_mainTable_saveTd.Attributes.Add("style", saveButtonPosition);
            PositionProfileDetailInfo_mainTableTd.Attributes.Add("height", (mainTableHeight - blockedHeight) + "px");              
        }
        
        private DataTable DeletePosition(DataTable dtPositionControls, string deleteText)
        {
            try
            {
                if (dtPositionControls  == null) return null;
                if (deleteText  == null) return null;

                DataTable tempPositionControls = dtPositionControls;

                int index = 0;
                int reduceSize = 0; 

                foreach (DataRow dr in tempPositionControls.Rows)
                {
                    if (dr[0].ToString() == deleteText)
                    {
                        int positionId = 0;
                        int segmentId = 0;

                        if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                        {
                            positionId = Convert.ToInt32(Request.QueryString["positionprofileid"]);
                        }
                        segmentId = Convert.ToInt32(dr[3]);

                        new PositionProfileBLManager().DeletePositionProfileSegment(positionId, segmentId);

                        tempPositionControls.Rows.Remove(dr);

                        break;
                    }
                    index += 1;
                }
                tempPositionControls.AcceptChanges();

                switch(deleteText)
                {
                    case "Position":
                        reduceSize = 150;
                        break;
                    case "Verticals":
                    case "Technical Skills":
                        reduceSize = 290;
                        break;
                    case "Roles":
                        reduceSize = 205;
                        break; 
                    case "PositionRequirement":
                        reduceSize = 240;
                        break;
                }
                for (int k = index + 1; k < tempPositionControls.Rows.Count; k++)
                {
                    tempPositionControls.Rows[k][1] =
                        Convert.ToInt32(tempPositionControls.Rows[k][1]) - reduceSize;
                }

                ViewState["DISPLAY_CONTROLS"] = tempPositionControls;
                ViewState["RESIZE_POSITION_TABLE"] = tempPositionControls;

                return tempPositionControls;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                             PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
                return null;
            }
        }

        private DataTable ResizePositionTable(DataTable dtPositionControls, string resizeText,string resizeType)
        {
            try
            {
                if (dtPositionControls == null) return null;

                DataTable tempResizePositionTable = dtPositionControls;

                int i = 0;
                int reSize = 0;

                foreach (DataRow dr in tempResizePositionTable.Rows)
                {
                    if (dr[0].ToString() == resizeText)
                    {
                        if (resizeType == "Minimize")
                            dr[2] = "none";
                        else
                            dr[2] = "block";

                        tempResizePositionTable.AcceptChanges();
                        break;
                    }

                    i = i + 1;
                }

                switch (resizeText)
                {
                    case "Position":
                        reSize = 120;
                        break;
                    case "Verticals":
                    case "Technical Skills":
                        reSize = 260;
                        break;
                    case "Roles":
                        reSize = 175;
                        break;
                    case "PositionRequirement":
                        reSize = 210;
                        break;
                }

                for (int k = i + 1; k < tempResizePositionTable.Rows.Count; k++)
                {  
                    if (resizeType == "Minimize")
                        tempResizePositionTable.Rows[k][1] = Convert.ToInt32(tempResizePositionTable.Rows[k][1]) - reSize;
                    else
                        tempResizePositionTable.Rows[k][1] = Convert.ToInt32(tempResizePositionTable.Rows[k][1]) + reSize;
                }

                tempResizePositionTable.AcceptChanges();

                ViewState["RESIZE_POSITION_TABLE"] = tempResizePositionTable;
                ViewState["DISPLAY_CONTROLS"] = tempResizePositionTable;

                return tempResizePositionTable;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                             PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
                return null;
            }
        }

        protected void PositionProfileDetailInfo_ImageButton_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
        {
            try
            { 
                if(e.CommandName==null ) return;

                if (ViewState["DISPLAY_CONTROLS"] == null) return;

                string deleteTagName = null;
                switch(e.CommandName)
                {
                    case "Verticals":
                        deleteTagName = "Vertical Background Requirement";
                        break;
                    case "Position":
                        deleteTagName = "Position Details ";
                        break;
                    case "Technical Skills":
                        deleteTagName = "Technical Skills";
                        break;
                    case "Roles":
                        deleteTagName = "Roles Requirement";
                        break;

                }
                PositionProfileDetailInfo_customizedSegmentsDeleteConfirmMsgControl.Message = string.Format
                   ("Are you sure you want to delete the segment '{0}' ?", deleteTagName);

                PositionProfileDetailInfo_customizedSegmentsDeleteConfirmMsgControl.Attributes.Add("height", "200px");
                PositionProfileDetailInfo_customizedSegmentsDeleteConfirmMsgControl.Type = MessageBoxType.YesNo;
                PositionProfileDetailInfo_customizedSegmentsDeleteConfirmMsgControl.Title = "Warning";

                switch(e.CommandName)
                {
                    case "Verticals":

                        if (DeleteSegmentsCondition(ViewState["DISPLAY_CONTROLS"] as DataTable))
                        {
                            ViewState["DELETE_SEGMENT"] = "Verticals";
                            PositionProfileDetailInfo_customizedSegmentsModalPopupExtender.Show();
                        }

                        /*SetControlPlace(DeletePosition(ViewState["DISPLAY_CONTROLS"]
                            as DataTable, "Verticals"), "Normal");*/
                        break;

                    case "Position":
                        if (DeleteSegmentsCondition(ViewState["DISPLAY_CONTROLS"] as DataTable))
                        {
                            ViewState["DELETE_SEGMENT"] = "Position";
                            PositionProfileDetailInfo_customizedSegmentsModalPopupExtender.Show();
                        }
                        /*SetControlPlace(DeletePosition(ViewState["DISPLAY_CONTROLS"]
                            as DataTable, "Position"), "Normal"); */
                        break;
                    case "Technical Skills":
                        if (DeleteSegmentsCondition(ViewState["DISPLAY_CONTROLS"] as DataTable))
                        {
                            ViewState["DELETE_SEGMENT"] = "Technical Skills";
                            PositionProfileDetailInfo_customizedSegmentsModalPopupExtender.Show();
                        }

                        /*SetControlPlace(DeletePosition(ViewState["DISPLAY_CONTROLS"]
                           as DataTable, "Technical Skills"), "Normal"); */
                        break;
                    case "Roles":
                        if (DeleteSegmentsCondition(ViewState["DISPLAY_CONTROLS"] as DataTable))
                        {
                            ViewState["DELETE_SEGMENT"] = "Roles";
                            PositionProfileDetailInfo_customizedSegmentsModalPopupExtender.Show();
                        }
                        /*SetControlPlace(DeletePosition(ViewState["DISPLAY_CONTROLS"]
                           as DataTable, "Roles"), "Normal");  */
                        break;
                    case "PositionRequirement":
                        SetControlPlace(DeletePosition(ViewState["DISPLAY_CONTROLS"]
                           as DataTable, "PositionRequirement"), "Normal");  
                        break;

                    case "TechnicalUpArrow":
                        PositionProfileDetailInfo_technicalSkill_downArrowImageButton.Visible = true;
                        PositionProfileDetailInfo_technicalSkill_upArrowImageButton.Visible=false ;
                        
                        SetControlPlace(ResizePositionTable(ViewState["RESIZE_POSITION_TABLE"]
                           as DataTable, "Technical Skills", "Minimize"), "Minimize");  
                        break;

                   case "TechnicalDownArrow":
                        PositionProfileDetailInfo_technicalSkill_downArrowImageButton.Visible = false;
                        PositionProfileDetailInfo_technicalSkill_upArrowImageButton.Visible = true;

                        SetControlPlace(ResizePositionTable(ViewState["RESIZE_POSITION_TABLE"]
                           as DataTable, "Technical Skills", "Maximize"), "Maximize");  
                        break; 

                    case "PositionDetailUpArrow":
                        PositionProfileDetailInfo_downArrow_PositionDetailImageButton.Visible = true;
                        PositionProfileDetailInfo_upArrow_PositionDetailImageButton.Visible = false;

                        SetControlPlace(ResizePositionTable(ViewState["RESIZE_POSITION_TABLE"]
                           as DataTable, "Position", "Minimize"), "Minimize");  
                        break;

                    case "PositionDetailDownArrow":
                        PositionProfileDetailInfo_downArrow_PositionDetailImageButton.Visible = false;
                        PositionProfileDetailInfo_upArrow_PositionDetailImageButton.Visible = true;

                        SetControlPlace(ResizePositionTable(ViewState["RESIZE_POSITION_TABLE"]
                           as DataTable, "Position", "Maximize"), "Maximize");  
                        break;

                    case "VerticalBackgroundUpArrow":
                        PositionProfileDetailInfo_verticalBackground_downArrowImageButton.Visible = true;
                        PositionProfileDetailInfo_verticalBackground_upArrowImageButton.Visible = false;

                        SetControlPlace(ResizePositionTable(ViewState["RESIZE_POSITION_TABLE"]
                           as DataTable, "Verticals", "Minimize"), "Minimize");  
                        break;

                    case "VerticalBackgroundDownArrow":
                        PositionProfileDetailInfo_verticalBackground_downArrowImageButton.Visible = false ;
                        PositionProfileDetailInfo_verticalBackground_upArrowImageButton.Visible = true;

                        SetControlPlace(ResizePositionTable(ViewState["RESIZE_POSITION_TABLE"]
                           as DataTable, "Verticals", "Maximize"), "Maximize");  
                        break;
                   
                    case "RolesUpArrow":
                        PositionProfileDetailInfo_rolesRequirment_downArrowImageButton.Visible = true;
                        PositionProfileDetailInfo_rolesRequirment_upArrowImageButton.Visible = false;
                       
                        SetControlPlace(ResizePositionTable(ViewState["RESIZE_POSITION_TABLE"]
                           as DataTable, "Roles", "Minimize"), "Minimize");  

                        break;
                    case "RolesDownArrow":
                        PositionProfileDetailInfo_rolesRequirment_downArrowImageButton.Visible = false;
                        PositionProfileDetailInfo_rolesRequirment_upArrowImageButton.Visible = true;

                        SetControlPlace(ResizePositionTable(ViewState["RESIZE_POSITION_TABLE"]
                       as DataTable, "Roles", "Maximize"), "Maximize");  

                        break;

                    case "PositionRequirementUpArrow":
                        PositionProfileDetailInfo_positionRequirment_upArrowImageButton.Visible =false ;
                        PositionProfileDetailInfo_positionRequirment_downArrowImageButton.Visible = true;

                        SetControlPlace(ResizePositionTable(ViewState["RESIZE_POSITION_TABLE"]
                           as DataTable, "PositionRequirement", "Minimize"), "Minimize");  
                        break;

                    case "PositionRequirementDownArrow":
                        PositionProfileDetailInfo_positionRequirment_upArrowImageButton.Visible =true ;
                        PositionProfileDetailInfo_positionRequirment_downArrowImageButton.Visible = false;

                        SetControlPlace(ResizePositionTable(ViewState["RESIZE_POSITION_TABLE"]
                           as DataTable, "PositionRequirement", "Maximize"), "Maximize");  
                        break;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                             PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
            }
        }
        
        protected void PositionProfileDetailInfo_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;
                //ControlVisibility();
                PositionProfileDetail positionProfileDetails = new PositionProfileDetail();

                positionProfileDetails.PositionName = Convert.ToString(PositionProfileDetailInfo_positionProfileNameHiddenField.Value).Trim();
                positionProfileDetails.PositionProfileAdditionalInformation = Convert.ToString(PositionProfileDetailInfo_additionalInformationHiddenField.Value ).Trim();
                positionProfileDetails.RegisteredBy = int.Parse(PositionProfileDetailInfo_registeredByHiddenField.Value);
                positionProfileDetails.RegistrationDate =
                    Convert.ToDateTime(PositionProfileDetailInfo_dateOfRegistrationHiddenField.Value);
                positionProfileDetails.SubmittalDate =
                    Convert.ToDateTime(PositionProfileDetailInfo_submittalDeadLineHiddenField.Value);
                positionProfileDetails.PositionID = Convert.ToInt32(PositionProfileDetailInfo_editPositionProfileIdHiddenField.Value);

                segmentDetailList = new List<SegmentDetail>();
                List<SegmentDetail> addedSegmentDetailList = new List<SegmentDetail>();
                segmentDetailList = (List<SegmentDetail>)ViewState[SEGMENT_DETAILS];

                if (segmentDetailList.Count < 1)
                    return;
                List<SkillWeightage> verticalSkillWeightage = null;
                List<SkillWeightage> technicalSkillWeightage = null;
                for (int i = 0; i < segmentDetailList.Count; i++)
                {
                    switch (segmentDetailList[i].SegmentID)
                    {
                        case (int)SegmentType.ClientPositionDetail:
                            addedSegmentDetailList.Add(PositionProfileDetailInfo_clientPositionDetailsControl.DataSource);
                            break;
                        case (int)SegmentType.CommunicationSkills:
                            break;
                        case (int)SegmentType.CulturalFit:
                            break;
                        case (int)SegmentType.DecisionMaking:
                            break;
                        case (int)SegmentType.Education:
                            //addedSegmentDetailList.Add(PositionProfileEntry_educationRequirementControl.DataSource);
                            break;
                        case (int)SegmentType.InterpersonalSkills:
                            break;
                        case (int)SegmentType.Roles:
                            addedSegmentDetailList.Add(PositionProfileDetailInfo_roleRequirementControl.DataSource);
                            break;
                        case (int)SegmentType.Screening:
                            break;
                        case (int)SegmentType.TechnicalSkills:
                            totalTechnicalWeightage = PositionProfileDetailInfo_technicalSkillRequirementControl.TotalWeightage;
                            addedSegmentDetailList.Add(PositionProfileDetailInfo_technicalSkillRequirementControl.DataSource);
                            totalSegmentsTechnicalWeightage =
                                PositionProfileDetailInfo_technicalSkillRequirementControl.TotalSegmentsWeightage;
                            technicalSkillWeightage = new List<SkillWeightage>();
                            technicalSkillWeightage =
                                PositionProfileDetailInfo_technicalSkillRequirementControl.SkillWeightageDataSource;
                            break;
                        case (int)SegmentType.Verticals:
                            totalVerticalWeightage = PositionProfileDetailInfo_verticalBackgroundRequirementControl.TotalWeightage;
                            totalSegmentsVerticalWeightage =
                                PositionProfileDetailInfo_verticalBackgroundRequirementControl.TotalSegmentsWeightage;
                            addedSegmentDetailList.Add(PositionProfileDetailInfo_verticalBackgroundRequirementControl.DataSource);
                            verticalSkillWeightage = new List<SkillWeightage>();
                            verticalSkillWeightage =
                                PositionProfileDetailInfo_verticalBackgroundRequirementControl.SkillWeightageDataSource;
                            break;
                    }
                }
                positionProfileDetails.Segments = addedSegmentDetailList;
                ViewState[POSITION_PROFILE_DETAILS] = positionProfileDetails;

                if (Support.Utility.IsNullOrEmpty(PositionProfileDetailInfo_editPositionProfileIdHiddenField.Value))
                {
                    PositionProfileSummaryValidation(verticalSkillWeightage, technicalSkillWeightage, positionProfileDetails);
                }
                else
                {
                    int positionID = int.Parse(PositionProfileDetailInfo_editPositionProfileIdHiddenField.Value);
                    Button button = (Button)sender;
                    PositionProfileDetailInfo_buttonCommandName.Value = button.CommandName.ToString();
                    switch (button.CommandName)
                    {
                        case "saveas":
                            PositionProfileSummaryValidation(verticalSkillWeightage,
                                technicalSkillWeightage, positionProfileDetails);
                            // InsertPositionProfile(true);
                            break; 
                        default:
                            positionProfileDetails.PositionID = positionID;
                            PositionProfileSummaryValidation(verticalSkillWeightage,
                                technicalSkillWeightage, positionProfileDetails);
                            //UpdatePositionProfile(positionProfileDetails);
                            break;
                    }
                } 
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                             PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will clear success/error label messages.
        /// </summary>
        private void ClearLabelMessage()
        {
            PositionProfileDetailInfo_topSuccessMessageLabel.Text = string.Empty;
            PositionProfileDetailInfo_topErrorMessageLabel.Text = string.Empty;
            PositionProfileDetailInfo_bottomSuccessMessageLabel.Text = string.Empty;
            PositionProfileDetailInfo_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            ClearLabelMessage();
            bool isValidData = true;
             
            DataList ClientInfoControl_clientDataList = (DataList)PositionProfileDetailInfo_ClientInfoControl.FindControl("ClientInfoControl_clientDataList");
             
          if (CanCreatePositionProfileUsage(true))
            {
                isValidData = false;
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                         PositionProfileDetailInfo_bottomErrorMessageLabel,
                     "You have reached the maximum limit based your subscription plan. Cannot create more position profiles");

                return isValidData;
            }
           

              if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState[SEGMENT_DETAILS]))
              {
                  segmentDetailList = new List<SegmentDetail>();
                  segmentDetailList = (List<SegmentDetail>)ViewState[SEGMENT_DETAILS];
                  if (segmentDetailList.Count < 1)
                  {
                      isValidData = false;
                      base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                           PositionProfileDetailInfo_bottomErrorMessageLabel,
                        HCMResource.PositionProfile_Segment_Empty);
                  }
              }
          return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override  void LoadValues()
        {
            ClearLabelMessage(); 
        }

        public void ShowPositionProfileSummary()
        {
            PositionProfileDetailInfo_viewPositionProfileSave_modalpPopupExtender.Show();
        }

        /// <summary>
        /// Method that is called when the keyword popuplate event is fired from 
        /// the requirements control.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="KeywordEventArgs"/> that holds the event data.
        /// </param>
        private void PositionProfileDetailInfo_RequirementControl_KeywordPopupated
            (object sender, KeywordEventArgs e)
        {
            try
            {
                // Populate technical skills.
                if (PositionProfileDetailInfo_technicalSkillRequirementControl != null &&
                    e.TechnicalSkills != null && e.TechnicalSkills.Count > 0)
                {
                    PositionProfileDetailInfo_technicalSkillRequirementControl.PopulateData(e.TechnicalSkills);
                }

                // Populate verticals.
                if (PositionProfileDetailInfo_verticalBackgroundRequirementControl != null &&
                    e.Verticals != null && e.Verticals.Count > 0)
                {
                    PositionProfileDetailInfo_verticalBackgroundRequirementControl.PopulateData(e.Verticals);
                }

                // Populate roles.
                if (PositionProfileDetailInfo_roleRequirementControl != null)
                {
                    PositionProfileDetailInfo_roleRequirementControl.PopulateData(e.PrimaryRole, e.SecondaryRole);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                             PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the message thrown event is fired from 
        /// the requirements control.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/> that holds the event data.
        /// </param>
        private void PositionProfileDetailInfo_requirementControl_ControlMessageThrown
            (object sender, ControlMessageEventArgs c)
        {
            try
            {
                if (c.MessageType == MessageType.Error)
                {
                    // Show error message.
                    base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                        PositionProfileDetailInfo_bottomErrorMessageLabel,c.Message);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                    PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the file uploaded event is fired from 
        /// the requirements control.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        private void PositionProfileDetailInfo_RequirementControl_UploadClick(object sender, EventArgs e)
        {
            try
            {
                switch (PositionProfileDetailInfo_requirementControl.Message)
                {
                    case MessageType.Success:
                        base.ShowMessage(PositionProfileDetailInfo_topSuccessMessageLabel,
                            PositionProfileDetailInfo_bottomSuccessMessageLabel,
                         HCMResource.PositionProfile_FileUpload_Successfully);
                        break;
                    case MessageType.Warning:
                        base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                         PositionProfileDetailInfo_bottomErrorMessageLabel,
                         HCMResource.PositionProfile_FileUpload_Empty);
                        break;
                    case MessageType.Error:
                        base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                         PositionProfileDetailInfo_bottomErrorMessageLabel,
                         HCMResource.PositionProfile_FileUpload_InvlaidFileType);
                        break;
                    case MessageType.Unexpected:
                        base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                         PositionProfileDetailInfo_bottomErrorMessageLabel,
                         HCMResource.PositionProfile_FileUpload_UnExpectedError);
                        break;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                         PositionProfileDetailInfo_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        private bool CanCreatePositionProfileUsage(bool veryFy)
        {
            if (!Support.Utility.IsNullOrEmpty(PositionProfileDetailInfo_editPositionProfileIdHiddenField.Value) && veryFy)
                return false; 
             
            return new AdminBLManager().IsFeatureUsageLimitExceeds(base.tenantID, featureID);
        }

        private void PositionProfileSummaryValidation(List<SkillWeightage> verticalSkillWeightage,
           List<SkillWeightage> technicalSkillWeightage, PositionProfileDetail positionProfileDetails)
        {
            try
            {
                string contactIDs = string.Empty;
                string clientContactIDs = string.Empty;
                string clientID = string.Empty;
                positionProfileDetails.RegisteredBy = base.userID;
                bool weightageFlag = true;
                totalWeightage = totalTechnicalWeightage + totalVerticalWeightage;
                if (totalWeightage > 100)
                {
                    base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                                      PositionProfileDetailInfo_bottomErrorMessageLabel,
                                      HCMResource.PositionProfile_Summary_Total_Weightage_Lessthen100);
                    weightageFlag = false;
                }
                if (totalSegmentsVerticalWeightage > 100)
                {
                    base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                                      PositionProfileDetailInfo_bottomErrorMessageLabel,
                                      HCMResource.PositionProfile_Summary_Vertical_Weightage_Lessthen100);
                    weightageFlag = false;
                }
                if (totalSegmentsTechnicalWeightage > 100)
                {
                    base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                                      PositionProfileDetailInfo_bottomErrorMessageLabel,
                                      HCMResource.PositionProfile_Summary_Technical_Weightage_Lessthen100);
                    weightageFlag = false;
                }

                if (weightageFlag)
                {
                    PositionProfileDetailInfo_summaryHiddenField.Value = "1";
                    PositionProfileDetailInfo_positionProfileSummary.SetErrorMessage = "";
                    ShowPositionProfileSummary();
                    SkillWeightage skillWeightage = null;

                    if (Support.Utility.IsNullOrEmpty(verticalSkillWeightage))
                    {
                        verticalSkillWeightage = new List<SkillWeightage>();
                        skillWeightage = new SkillWeightage();
                        skillWeightage.UserSegments = true;
                        verticalSkillWeightage.Add(skillWeightage);
                    }

                    if (Support.Utility.IsNullOrEmpty(technicalSkillWeightage))
                    {
                        technicalSkillWeightage = new List<SkillWeightage>();
                        skillWeightage = new SkillWeightage();
                        skillWeightage.UserSegments = true;
                        technicalSkillWeightage.Add(skillWeightage);
                    }

                    foreach (var ch in technicalSkillWeightage)
                    {
                        if (!verticalSkillWeightage.Exists(item => item.SkillName == ch.SkillName &&
                            item.Weightage == ch.Weightage) && !Support.Utility.IsNullOrEmpty(ch.SkillName))
                            verticalSkillWeightage.AddRange(technicalSkillWeightage);
                    }
                    ViewState[SEGMENTS_KEYWORD] = verticalSkillWeightage;

                    if (Support.Utility.IsNullOrEmpty(PositionProfileDetailInfo_editPositionProfileIdHiddenField.Value))
                    {
                        PositionProfileDetailInfo_positionProfileSummary.SkillWeightageDataSource = verticalSkillWeightage;


                    }
                    else
                    {
                        PositionProfileDetailInfo_positionProfileSummary.SkillWeightageDataSource =
                            ViewState[EDIT_POSITION_PROFILE_KEYWORD] as List<SkillWeightage>;
                    }

                    ViewState[POSITION_PROFILE_DETAILS] = positionProfileDetails;
                }

                if (ViewState[SEGMENTS_KEYWORD] != null)
                {
                    PositionProfileDetailInfo_positionProfileSummary.SkillWeightageDataSource =
                        ViewState[SEGMENTS_KEYWORD] as List<SkillWeightage>;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                    PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileDetailInfo_positionProfileSummary_AddSkillEvent(object sender, EventArgs e)
        {
            try
            {
                ShowPositionProfileSummary();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                    PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileDetailInfo_positionProfileSummary_CreateButton_Click(object sender, EventArgs e)
        {
            try
            {
                InsertPositionProfile(false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                    PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileDetailInfo_positionProfileSummary_RegenerateLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                PositionProfileDetailInfo_positionProfileSummary.SkillWeightageDataSource =
                    ViewState[SEGMENTS_KEYWORD] as List<SkillWeightage>;
                ShowPositionProfileSummary();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                    PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Inserts the position profile.
        /// </summary>
        /// <param name="saveAsFlag">if set to <c>true</c> [save as flag].</param>
        private void InsertPositionProfile(bool saveAsFlag)
        {
            try
            {
                decimal PositionProfileSum = 0;
                PositionProfileDetail positionProfileDetails = new PositionProfileDetail();
                positionProfileDetails = ViewState[POSITION_PROFILE_DETAILS] as PositionProfileDetail;
                List<SkillWeightage> userKeywordSkillWeightage = new List<SkillWeightage>();
                userKeywordSkillWeightage = PositionProfileDetailInfo_positionProfileSummary.SkillWeightageDataSource;
                string segmentskeyword = "";
                string userkeyword = "";
                bool skillFlag = false;
                bool duplicateFlag = false;
                List<PositionProfileKeywordDictionary> positionProfileKeywordDictionaryList =
                    new List<PositionProfileKeywordDictionary>();

                if (!Support.Utility.IsNullOrEmpty(userKeywordSkillWeightage))
                {
                    foreach (var ch in userKeywordSkillWeightage)
                    {
                        if (ch.UserSegments)
                        {
                            if (Support.Utility.IsNullOrEmpty(ch.SkillName))
                                continue;
                            userkeyword = userkeyword + ch.SkillName +
                                  "[" + Convert.ToInt32(ch.Weightage) + "],";
                            PositionProfileSum = PositionProfileSum + Convert.ToInt32(ch.Weightage);
                            GetPositionProfileDictinaryKeyword(ref skillFlag, ref duplicateFlag,
                                positionProfileKeywordDictionaryList, ch);

                        }
                        else
                        {
                            if (Support.Utility.IsNullOrEmpty(ch.SkillName))
                                continue;
                            segmentskeyword = segmentskeyword + ch.SkillName +
                                "[" + Convert.ToInt32(ch.Weightage) + "],";
                            PositionProfileSum = PositionProfileSum + Convert.ToInt32(ch.Weightage);
                            GetPositionProfileDictinaryKeyword(ref skillFlag, ref duplicateFlag,
                                positionProfileKeywordDictionaryList, ch);
                        }
                    }
                }

                if (duplicateFlag)
                {
                    PositionProfileDetailInfo_positionProfileSummary.SetErrorMessage =
                     HCMResource.PositionProfile_summary_SkillName_Duplicate;
                    ShowPositionProfileSummary();
                    return;
                }
                if (PositionProfileSum > 100)
                {
                    PositionProfileDetailInfo_positionProfileSummary.SetErrorMessage =
                       HCMResource.PositionProfile_Summary_Total_Weightage_Lessthen100;
                    ShowPositionProfileSummary();
                    return;
                }
                string keyword = "";
                positionProfileDetails.PositionProfileDictinaryKeywords = positionProfileKeywordDictionaryList;
                PositionProfileKeyword positionProfileKeyword = new PositionProfileKeyword();
                positionProfileKeyword.SegmentKeyword = segmentskeyword.TrimEnd(',');
                positionProfileKeyword.UserKeyword = userkeyword.TrimEnd(',');
                if (segmentskeyword.TrimEnd(',').Length > 0)
                    keyword = segmentskeyword;
                if (userkeyword.TrimEnd(',').Length > 0)
                    keyword = keyword + userkeyword.TrimEnd(',');
                positionProfileKeyword.Keyword = keyword.TrimEnd(',');
                positionProfileDetails.PositionProfileKeywords = positionProfileKeyword;
                ViewState[POSITION_PROFILE_DETAILS] = positionProfileDetails;
                if (!skillFlag)
                {
                    PositionProfileDetailInfo_positionProfileSummary.SetErrorMessage =
                        HCMResource.PositionProfile_summary_SkillName_Empty;
                    ShowPositionProfileSummary();
                    return;
                } 

                UpdatePositionProfile();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                             PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
            }
        }

        private static void GetPositionProfileDictinaryKeyword(ref bool skillFlag, ref bool duplicateFlag,
           List<PositionProfileKeywordDictionary> positionProfileKeywordDictionaryList, SkillWeightage ch)
        { 
            if (!positionProfileKeywordDictionaryList.Exists(item => item.Keyword.ToLower() ==
                ch.SkillName.ToLower() && item.skillTypes == ch.SkillTypes))
            {
                PositionProfileKeywordDictionary positionProfileKeywordDictionary =
                    new PositionProfileKeywordDictionary();
                positionProfileKeywordDictionary.Keyword = ch.SkillName;
                positionProfileKeywordDictionary.skillTypes = ch.SkillTypes;
                positionProfileKeywordDictionary.SkillCategory = ch.SkillCategory;
                positionProfileKeywordDictionaryList.Add(positionProfileKeywordDictionary);
            }
            else
                duplicateFlag = true;

            if (!Support.Utility.IsNullOrEmpty(ch.SkillName) &&
                !Support.Utility.IsNullOrEmpty(ch.Weightage))
                skillFlag = true;  
        }

        /// <summary>
        /// Updates the position profile.
        /// </summary>
        private void UpdatePositionProfile()
        {
            try
            {    
                PositionProfileDetail positionProfileDetails = new PositionProfileDetail();
                positionProfileDetails = ViewState[POSITION_PROFILE_DETAILS] as PositionProfileDetail;

                if (!Utility.IsNullOrEmpty(PositionProfileDetailInfo_requirementControl.AdditionalInformation))
                    positionProfileDetails.PositionProfileAdditionalInformation =
                        PositionProfileDetailInfo_requirementControl.AdditionalInformation;

                positionProfileId = new PositionProfileBLManager().UpdatePositionProfileAndSegmentDatas(positionProfileDetails,
                    base.userID, base.tenantID, PositionProfileDetailInfo_ClientInfoControl.ClientContactInformations);
                if (positionProfileId > 0)
                {
                    BindPositionProfile();
                    base.ShowMessage(PositionProfileDetailInfo_topSuccessMessageLabel,
                        PositionProfileDetailInfo_bottomSuccessMessageLabel,
                    HCMResource.PositionProfile_UpdatedSuccessfully);

                    Response.Redirect("../PositionProfile/PositionProfileWorkflowSelection.aspx?m=1&s=0&positionprofileid="
                        +positionProfileDetails.PositionID.ToString(), false);
                }
                else
                {
                    base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                        PositionProfileDetailInfo_bottomErrorMessageLabel,
                        HCMResource.PositionProfile_Already_Exists);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                             PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
            }
        }
          
        /// <summary>
        /// Binds the position profile.
        /// </summary>        
        private void BindPositionProfile()
        {
            try
            {
                if (Support.Utility.IsNullOrEmpty(PositionProfileDetailInfo_editPositionProfileIdHiddenField.Value)) return;

                PositionProfileDetail positionProfileDetail = new PositionProfileDetail();
                positionProfileDetail = new PositionProfileBLManager().GetPositionProfile
                    (Convert.ToInt32(PositionProfileDetailInfo_editPositionProfileIdHiddenField.Value));

                if (Support.Utility.IsNullOrEmpty(positionProfileDetail))
                    return;

                PositionProfileDetailInfo_positionProfileNameHiddenField.Value = positionProfileDetail.PositionName;
                PositionProfileDetailInfo_additionalInformationHiddenField.Value = positionProfileDetail.PositionProfileAdditionalInformation;
                PositionProfileDetailInfo_registeredByHiddenField.Value = positionProfileDetail.RegisteredBy.ToString();
                PositionProfileDetailInfo_dateOfRegistrationHiddenField.Value = positionProfileDetail.RegistrationDate.ToShortDateString();
                PositionProfileDetailInfo_submittalDeadLineHiddenField.Value = positionProfileDetail.SubmittalDate.ToShortDateString();
                PositionProfileDetailInfo_clientIDHiddenField.Value = positionProfileDetail.ClientID.ToString();
                PositionProfileDetailInfo_clientNameHiddenField.Value = positionProfileDetail.ClientName;

                if (positionProfileDetail.Segments == null) return;

                ViewState[SEGMENT_DETAILS] = positionProfileDetail.Segments;

                PositionProfileDetailInfo_requirementControl.AdditionalInformation = positionProfileDetail.PositionProfileAdditionalInformation;

                foreach (SegmentDetail segmentDetail in positionProfileDetail.Segments)
                {
                    switch (segmentDetail.SegmentID)
                    {
                        case (int)SegmentType.ClientPositionDetail:
                            if (!Utility.IsNullOrEmpty(segmentDetail.DataSource))
                                PositionProfileDetailInfo_clientPositionDetailsControl.DataSource = segmentDetail;
                            break;
                        case (int)SegmentType.Roles:
                            if (!Utility.IsNullOrEmpty(segmentDetail.DataSource))
                                PositionProfileDetailInfo_roleRequirementControl.DataSource = segmentDetail;
                            break;
                        case (int)SegmentType.TechnicalSkills:
                            if (!Utility.IsNullOrEmpty(segmentDetail.DataSource))
                                PositionProfileDetailInfo_technicalSkillRequirementControl.DataSource = segmentDetail;
                            break;
                        case (int)SegmentType.Verticals:
                            if (!Utility.IsNullOrEmpty(segmentDetail.DataSource))
                                PositionProfileDetailInfo_verticalBackgroundRequirementControl.DataSource = segmentDetail;
                            break;
                    }
                }

                if (!Support.Utility.IsNullOrEmpty(positionProfileDetail.PositionProfileKeywords))
                {
                    List<SkillWeightage> skillWeightageList = new List<SkillWeightage>();
                    PositionProfileKeyword positionProfileKeyword = new PositionProfileKeyword();
                    positionProfileKeyword = positionProfileDetail.PositionProfileKeywords;
                    string[] segmentKeywords = positionProfileKeyword.SegmentKeyword.Split(',');
                    foreach (string item in segmentKeywords)
                    {
                        if (Support.Utility.IsNullOrEmpty(item))
                            continue;
                        string skillName = item.Substring(0, item.IndexOf('['));
                        int weightage = 0;
                        int.TryParse(item.Substring(item.IndexOf('[') + 1, item.Length - item.IndexOf('[') - 2), out weightage);
                        // if (skillWeightageList.Exists(item1 => item1.SkillName == skillName && item1.Weightage == weightage))
                        //   continue;
                        SkillWeightage skillWeightage = new SkillWeightage();
                        skillWeightage.SkillName = skillName;
                        skillWeightage.Weightage = weightage;
                        skillWeightage.UserSegments = false;
                        skillWeightageList.Add(skillWeightage);
                    }

                    string[] userKeywords = positionProfileKeyword.UserKeyword.Split(',');
                    foreach (string item in userKeywords)
                    {
                        if (Support.Utility.IsNullOrEmpty(item))
                            continue;
                        string skillName = item.Substring(0, item.IndexOf('['));
                        int weightage = 0;
                        int.TryParse(item.Substring(item.IndexOf('[') + 1, item.Length - item.IndexOf('[') - 2), out weightage);
                        SkillWeightage skillWeightage = new SkillWeightage();
                        skillWeightage.SkillName = skillName;
                        skillWeightage.Weightage = weightage;
                        skillWeightage.UserSegments = true;
                        skillWeightageList.Add(skillWeightage);
                    }
                    if (!Support.Utility.IsNullOrEmpty(skillWeightageList))
                        ViewState[EDIT_POSITION_PROFILE_KEYWORD] = skillWeightageList;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                             PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
            }
        }
        protected void PositionProfileDetailInfo_gotoFirstStep_LinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PositionProfile/PositionProfileBasicInfo.aspx?m=1&s=0&positionprofileid="
                        + Request.QueryString.Get("positionprofileid"), false);
             
        }

        protected void PositionProfileDetailInfo_customizedSegmentsDeleteConfirmMsgControl_okClick(object sender,EventArgs e)
        {
            try 
            {
                if (!Utility.IsNullOrEmpty(ViewState["DELETE_SEGMENT"]))
                    SetControlPlace(DeletePosition(ViewState["DISPLAY_CONTROLS"]
                         as DataTable, ViewState["DELETE_SEGMENT"].ToString()), "Normal");

                if (!Utility.IsNullOrEmpty(PositionProfileDetailInfo_editPositionProfileIdHiddenField.Value))
                {
                    SetControlPlace(DisplaySegmentControls(Convert.ToInt32
                        (PositionProfileDetailInfo_editPositionProfileIdHiddenField.Value)), "Normal");


                    if (ViewState["DISPLAY_CONTROLS"] == null) return;

                    DataTable dttemp = ViewState["DISPLAY_CONTROLS"]
                         as DataTable;

                    foreach (DataRow dr in dttemp.Rows)
                    { 
                        switch(dr[0].ToString()) 
                        {
                            case "Position":
                                 PositionProfileDetailInfo_downArrow_PositionDetailImageButton.Visible = false;
                                 PositionProfileDetailInfo_upArrow_PositionDetailImageButton.Visible = true;
                                break;
                            case "Verticals":
                                PositionProfileDetailInfo_verticalBackground_downArrowImageButton.Visible = false ;
                                PositionProfileDetailInfo_verticalBackground_upArrowImageButton.Visible = true;
                                break;
                            case "Technical Skills":
                                PositionProfileDetailInfo_technicalSkill_downArrowImageButton.Visible = false;
                                PositionProfileDetailInfo_technicalSkill_upArrowImageButton.Visible = true;
                                break;
                            case "Roles":
                                 PositionProfileDetailInfo_rolesRequirment_downArrowImageButton.Visible = false;
                                 PositionProfileDetailInfo_rolesRequirment_upArrowImageButton.Visible = true;
                                break;
                            case "PositionRequirement":
                                PositionProfileDetailInfo_positionRequirment_upArrowImageButton.Visible =true ;
                                PositionProfileDetailInfo_positionRequirment_downArrowImageButton.Visible = false;
                                break;
                        }
                    }
                } 
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                             PositionProfileDetailInfo_bottomErrorMessageLabel, exp.Message);
            }
        }

        private bool DeleteSegmentsCondition(DataTable dtTemp)
        {     
            //Checking the segments atleast one need except position requirment
            if (dtTemp.Rows.Count < 3)
            {
                base.ShowMessage(PositionProfileDetailInfo_topErrorMessageLabel,
                       PositionProfileDetailInfo_bottomErrorMessageLabel, "Minimum one segment need to be save");
                return false;
            } 
            else
                return true;
        }
}
}