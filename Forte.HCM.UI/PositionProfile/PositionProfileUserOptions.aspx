﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PositionProfileUserOptions.aspx.cs"
    MasterPageFile="~/MasterPages/PositionProfileMaster.Master" Inherits="Forte.HCM.UI.PositionProfile.PositionProfileUserOptions" %>

<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content ID="PositionProfileUserOptions_content" ContentPlaceHolderID="PositionProfileMaster_body"
    runat="server">
    <script type="text/javascript" language="javascript">
        function LoadGetForm(dropDownLisID) {
            var dropdownlist = document.getElementById(dropDownLisID);
            var value = dropdownlist.options[dropdownlist.selectedIndex].value;
            if (value == 0) {               
                return;
            }
            else {
                LoadPreviewForm('', value);
            }

        }
    </script>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="PositionProfileUserOptions_headerLiteral" runat="server" Text="User Options"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="PositionProfileUserOptions_topSaveButton" runat="server" Text="Save"
                                            SkinID="sknButtonId" OnClick="PositionProfileUserOptions_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="PositionProfileUserOptions_topResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="PositionProfileUserOptions_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="PositionProfileUserOptions_topCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="PositionProfileUserOptions_messageUpdatePanel" runat="server"
                    UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="PositionProfileUserOptions_topSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="PositionProfileUserOptions_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="PositionProfileUserOptions_formSettingsLiteral" runat="server" Text="Form Settings"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="PositionProfileUserOptions_formSettingsUpdatePanel" runat="server"
                                            UpdateMode="Always">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="width: 12%;">
                                                            <asp:Label ID="PositionProfileUserOptions_formLayoutLabel" runat="server" Text="Default Form Layout"
                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="width: 60%;">
                                                            <div style="float: left; padding-right: 5px; width: 90%">
                                                                <asp:DropDownList ID="PositionProfileUserOptions_formLayoutDropDownList" runat="server"
                                                                    SkinID="sknSubjectDropDown" DataTextField="FormName" DataValueField="FormID">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:ImageButton ID="PositionProfileUserOptions_formLayoutHelpImageButton" SkinID="sknHelpImageButton"
                                                                    runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                    ToolTip="Enter the default form " />
                                                            </div>
                                                        </td>
                                                        <td style="width: 28%;" align="left">
                                                            <asp:LinkButton ID="PositionProfileUserOptions_previewLinkButton" runat="server"
                                                                SkinID="sknActionLinkButton" Text="Preview" 
                                                                ToolTip="Click here to have the preview of the form " 
                                                                onclick="PositionProfileUserOptions_previewLinkButton_Click"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="PositionProfileUserOptions_bottomMessageUpdatePanel" runat="server"
                    UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="PositionProfileUserOptions_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="PositionProfileUserOptions_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="4" align="right">
                    <tr>
                        <td>
                            <asp:Button ID="PositionProfileUserOptions_bottomSaveButton" runat="server" Text="Save"
                                SkinID="sknButtonId" OnClick="PositionProfileUserOptions_saveButton_Click" />
                        </td>
                        <td>
                            <asp:LinkButton ID="PositionProfileUserOptions_bottomResetLinkButton" runat="server"
                                Text="Reset" SkinID="sknActionLinkButton" OnClick="PositionProfileUserOptions_resetLinkButton_Click"></asp:LinkButton>
                        </td>
                        <td align="center" class="link_button">
                            |
                        </td>
                        <td>
                            <asp:LinkButton ID="PositionProfileUserOptions_bottomCancelLinkButton" runat="server"
                                Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
