﻿<%@ Page Title="Edit Position Profile" Language="C#" MasterPageFile="~/MasterPages/PositionProfileMaster.Master"
    AutoEventWireup="true" CodeBehind="PositionProfileDetailInfo.aspx.cs" Inherits="Forte.HCM.UI.PositionProfile.PositionProfileDetailInfo" %>

<%@ Register Src="../Segments/ClientPositionDetailsControl.ascx" TagName="ClientPositionDetailsControl"
    TagPrefix="uc1" %>
<%@ Register Src="../Segments/TechnicalSkillRequirementControl.ascx" TagName="TechnicalSkillRequirementControl"
    TagPrefix="uc2" %>
<%@ Register Src="../Segments/VerticalBackgroundRequirementControl.ascx" TagName="VerticalBackgroundRequirementControl"
    TagPrefix="uc3" %>
<%@ Register Src="../Segments/RoleRequirementControl.ascx" TagName="RoleRequirementControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/RequirementViewerControl.ascx" TagName="RequirementViewerControl"
    TagPrefix="uc5" %>
<%@ Register Src="~/CommonControls/PositionProfileSummary.ascx" TagName="PositionProfileSummary"
    TagPrefix="uc6" %>
<%@ Register Src="~/CommonControls/ClientInfoControl.ascx" TagName="ClientInfoControl"
    TagPrefix="uc7" %>
<%@ Register Src="~/CommonControls/PositionProfileWorkflowControl.ascx" TagName="PositionProfileWorkflowControl"
    TagPrefix="uc8" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc9" %>

<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PositionProfileMaster_body" runat="server">
    <asp:UpdatePanel ID="PositionProfileDetailInfo_updatePanel" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="PositionProfileDetailInfo_topMessageUpdatePanel" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="PositionProfileDetailInfo_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                <asp:Label ID="PositionProfileDetailInfo_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="PositionProfileDetailInfo_saveButton" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr> 
                <tr>
                    <td width="100%">
                     <%--   <asp:Image ID="PositionProfileDetailInfo_selectionImage" runat="server" SkinID="sknPPStepDetailInfo"
                            Width="100%" />--%>
                             <uc8:PositionProfileWorkflowControl ID="PositionProfileWorkflowControl" runat="server"
                                        Visible="true" />
                    </td>
                </tr>

                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="PositionProfileDetailInfo_customizedSegmentsDeleteUpdatePanel"
                            runat="server">
                            <ContentTemplate>
                                <div style="display: none">
                                    <asp:Button ID="PositionProfileDetailInfo_customizedSegmentsDeleteHiddenButton" runat="server" />
                                </div>
                                <asp:Panel ID="PositionProfileDetailInfo_customizedSegmentsDeletePopupPanel" runat="server"
                                    Style="display: none" CssClass="popupcontrol_confirm_remove">
                                    <uc9:ConfirmMsgControl ID="PositionProfileDetailInfo_customizedSegmentsDeleteConfirmMsgControl"
                                        runat="server" OnOkClick="PositionProfileDetailInfo_customizedSegmentsDeleteConfirmMsgControl_okClick" />
                                    <asp:HiddenField ID="PositionProfileDetailInfo_customizedSegmentsDeleteHiddenFiled" runat="server" />
                                </asp:Panel>
                                <ajaxToolKit:ModalPopupExtender ID="PositionProfileDetailInfo_customizedSegmentsModalPopupExtender" runat="server" PopupControlID="PositionProfileDetailInfo_customizedSegmentsDeletePopupPanel"
                                    TargetControlID="PositionProfileDetailInfo_customizedSegmentsDeleteHiddenButton"
                                    BackgroundCssClass="modalBackground">
                                </ajaxToolKit:ModalPopupExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>

                <tr>
                    <td class="tab_body_bg" runat="server" id="PositionProfileDetailInfo_mainTableTd"
                        valign="top" height="1090px">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td>
                                    <div id="PositionProfileDetailInfo_positionRequirement_headerDiv" runat="server"
                                        style="height: 200px;">
                                        <asp:HiddenField ID="PositionProfileDetailInfo_editPositionProfileIdHiddenField"
                                            runat="server" Value="50" />
                                        <asp:HiddenField ID="PositionProfileDetailInfo_positionProfileNameHiddenField" runat="server" />
                                        <asp:HiddenField ID="PositionProfileDetailInfo_additionalInformationHiddenField"
                                            runat="server" />
                                        <asp:HiddenField ID="PositionProfileDetailInfo_registeredByHiddenField" runat="server" />
                                        <asp:HiddenField ID="PositionProfileDetailInfo_dateOfRegistrationHiddenField" runat="server" />
                                        <asp:HiddenField ID="PositionProfileDetailInfo_submittalDeadLineHiddenField" runat="server" />
                                        <asp:HiddenField ID="PositionProfileDetailInfo_clientIDHiddenField" runat="server" />
                                        <asp:HiddenField ID="PositionProfileDetailInfo_clientNameHiddenField" runat="server" />
                                        <table width="950px" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="username_text" width="900px">
                                                    <asp:Literal ID="PositionProfileDetailInfo_positionRequirment_Literal" runat="server"
                                                        Text="Position Requirement"></asp:Literal>
                                                </td>
                                                <td width="25px" align="center"> 
                                                </td>
                                                <td width="25px" align="center">
                                                    <asp:ImageButton ID="PositionProfileDetailInfo_positionRequirment_upArrowImageButton"
                                                        runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/dashboard_widg_up_icon.gif"
                                                        CommandName="PositionRequirementUpArrow" OnCommand="PositionProfileDetailInfo_ImageButton_Command"
                                                        ToolTip="Minimize" />
                                                    <asp:ImageButton ID="PositionProfileDetailInfo_positionRequirment_downArrowImageButton"
                                                        runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/dashboard_widg_down_icon.gif"
                                                        Visible="false" CommandName="PositionRequirementDownArrow" OnCommand="PositionProfileDetailInfo_ImageButton_Command"
                                                        ToolTip="Maximize" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5" colspan="3">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="candidate_td_h_line">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <div id="PositionProfileDetailInfo_positionRequirementDiv" runat="server">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="td_height_5">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 950px">
                                                                    <uc5:RequirementViewerControl ID="PositionProfileDetailInfo_requirementControl" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="PositionProfileDetailInfo_positionDetail_headerDiv" runat="server" style="height: 150px;">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="username_text" width="900px">
                                                    <asp:Literal ID="PositionProfileDetailInfo_headerLiteral" runat="server" Text="Position Details"></asp:Literal>
                                                </td>
                                                <td width="25px" align="center">
                                                    <asp:ImageButton ID="PositionProfileDetailInfo_closePositionDetailImageButton" runat="server"
                                                        ImageUrl="~/App_Themes/DefaultTheme/Images/dashboard_widg_close_icon.gif" CommandName="Position"
                                                        OnCommand="PositionProfileDetailInfo_ImageButton_Command" ToolTip="Delete" />
                                                </td>
                                                <td width="25px" align="center">
                                                    <asp:ImageButton ID="PositionProfileDetailInfo_upArrow_PositionDetailImageButton"
                                                        runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/dashboard_widg_up_icon.gif"
                                                        CommandName="PositionDetailUpArrow" OnCommand="PositionProfileDetailInfo_ImageButton_Command"
                                                        ToolTip="Minimize" />
                                                    <asp:ImageButton ID="PositionProfileDetailInfo_downArrow_PositionDetailImageButton"
                                                        runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/dashboard_widg_down_icon.gif"
                                                        Visible="false" CommandName="PositionDetailDownArrow" OnCommand="PositionProfileDetailInfo_ImageButton_Command"
                                                        ToolTip="Maximize" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5" colspan="3">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="candidate_td_h_line">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <div id="PositionProfileDetailInfo_positionDetailDiv" runat="server"  clientidmode="AutoID">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="td_height_5">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <uc1:ClientPositionDetailsControl ID="PositionProfileDetailInfo_clientPositionDetailsControl"
                                                                        runat="server" Visible="true" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="PositionProfileDetailInfo_technicalSkill_headerDiv" runat="server">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="username_text" width="900px">
                                                    <asp:Literal ID="PositionProfileDetailInfo_technicalSkillLiteral" runat="server"
                                                        Text="Technical Skills"></asp:Literal>
                                                </td>
                                                <td width="25px" align="center">
                                                    <asp:ImageButton ID="PositionProfileDetailInfo_technicalSkill_CloseImageButton" runat="server"
                                                        ImageUrl="~/App_Themes/DefaultTheme/Images/dashboard_widg_close_icon.gif" CommandName="Technical Skills"
                                                        OnCommand="PositionProfileDetailInfo_ImageButton_Command" ToolTip="Delete" />
                                                </td>
                                                <td width="25px" align="center">
                                                    <asp:ImageButton ID="PositionProfileDetailInfo_technicalSkill_upArrowImageButton"
                                                        runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/dashboard_widg_up_icon.gif"
                                                        CommandName="TechnicalUpArrow" OnCommand="PositionProfileDetailInfo_ImageButton_Command"
                                                        ToolTip="Minimize" />
                                                    <asp:ImageButton ID="PositionProfileDetailInfo_technicalSkill_downArrowImageButton"
                                                        runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/dashboard_widg_down_icon.gif"
                                                        Visible="false" CommandName="TechnicalDownArrow" OnCommand="PositionProfileDetailInfo_ImageButton_Command"
                                                        ToolTip="Maximize" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5" colspan="3">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="candidate_td_h_line">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <div id="PositionProfileDetailInfo_technicalSkillsDiv" runat="server">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="td_height_5">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <uc2:TechnicalSkillRequirementControl ID="PositionProfileDetailInfo_technicalSkillRequirementControl"
                                                                        runat="server" Visible="true" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div runat="server" id="PositionProfileDetailInfo_verticalBackground_headerDiv">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="username_text" width="900px">
                                                    <asp:Literal ID="PositionProfileDetailInfo_verticalBackground_titleLiteral" runat="server"
                                                        Text="Vertical Background Requirement"></asp:Literal>
                                                </td>
                                                <td width="25px" align="center">
                                                    <asp:ImageButton ID="PositionProfileDetailInfo_verticalBackground_closeImageButton"
                                                        runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/dashboard_widg_close_icon.gif"
                                                        CommandName="Verticals" OnCommand="PositionProfileDetailInfo_ImageButton_Command"
                                                        ToolTip="Delete" />
                                                </td>
                                                <td width="25px" align="center">
                                                    <asp:ImageButton ID="PositionProfileDetailInfo_verticalBackground_upArrowImageButton"
                                                        runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/dashboard_widg_up_icon.gif"
                                                        CommandName="VerticalBackgroundUpArrow" OnCommand="PositionProfileDetailInfo_ImageButton_Command"
                                                        ToolTip="Minimize" />
                                                    <asp:ImageButton ID="PositionProfileDetailInfo_verticalBackground_downArrowImageButton"
                                                        runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/dashboard_widg_down_icon.gif"
                                                        Visible="false" CommandName="VerticalBackgroundDownArrow" OnCommand="PositionProfileDetailInfo_ImageButton_Command"
                                                        ToolTip="Maximize" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5" colspan="3">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="candidate_td_h_line">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <div id="PositionProfileDetailInfo_verticalBackgroundRequirementDiv" runat="server">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="td_height_5">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <uc3:VerticalBackgroundRequirementControl ID="PositionProfileDetailInfo_verticalBackgroundRequirementControl"
                                                                        runat="server" Visible="true" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="PositionProfileDetailInfo_rolesRequirment_headerDiv" runat="server">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="username_text" width="900px">
                                                    <asp:Literal ID="PositionProfileDetailInfo_rolesRequirment_Literal" runat="server"
                                                        Text="Roles Requirement"></asp:Literal>
                                                </td>
                                                <td width="25px" align="center">
                                                    <asp:ImageButton ID="PositionProfileDetailInfo_rolesRequirment_closeImageButton"
                                                        runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/dashboard_widg_close_icon.gif"
                                                        CommandName="Roles" OnCommand="PositionProfileDetailInfo_ImageButton_Command"
                                                        ToolTip="Delete" />
                                                </td>
                                                <td width="25px" align="center">
                                                    <asp:ImageButton ID="PositionProfileDetailInfo_rolesRequirment_upArrowImageButton"
                                                        runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/dashboard_widg_up_icon.gif"
                                                        CommandName="RolesUpArrow" OnCommand="PositionProfileDetailInfo_ImageButton_Command"
                                                        ToolTip="Minimize" />
                                                    <asp:ImageButton ID="PositionProfileDetailInfo_rolesRequirment_downArrowImageButton"
                                                        runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/dashboard_widg_down_icon.gif"
                                                        Visible="false" CommandName="RolesDownArrow" OnCommand="PositionProfileDetailInfo_ImageButton_Command"
                                                        ToolTip="Maximize" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5" colspan="3">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="candidate_td_h_line">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <div id="PositionProfileDetailInfo_roleRequirementDiv" runat="server">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="td_height_5">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <uc4:RoleRequirementControl ID="PositionProfileDetailInfo_roleRequirementControl"
                                                                        runat="server" Visible="true" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td id="PositionProfileDetailInfo_mainTable_saveTd" runat="server">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td width="15%">
                                                <asp:Button ID="PositionProfileDetailInfo_saveButton" SkinID="sknButtonOrange" runat="server"
                                                    Text="Save and Continue" CommandName="saveas" OnClick="PositionProfileDetailInfo_saveButton_Click"
                                                    ToolTip="Click here to save" />
                                            </td>
                                            <td width="15%">
                                                <asp:LinkButton ID="PositionProfileDetailInfo_gotoFirstStep_LinkButton" SkinID="sknDashboardLinkButton"
                                                    runat="server" ToolTip="Click here to go to previous page" OnClick="PositionProfileDetailInfo_gotoFirstStep_LinkButton_Click">Return to Basic Info</asp:LinkButton>
                                            </td>
                                            <td width="8%">
                                                <asp:LinkButton ID="PositionProfileDetailInfo_cancelLinkButton" SkinID="sknDashboardLinkButton"
                                                    Visible="false" runat="server">Cancel</asp:LinkButton>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="PositionProfileDetailInfo_positionProfileSavePanel" runat="server"
                                        Style="display: none" CssClass="popupcontrol_position_profile_summary" DefaultButton="PositionProfileDetailInfo_positionProfileSummary_CreateButton">
                                        <div style="display: none">
                                            <asp:Button ID="PositionProfileDetailInfo_viewPositionProfileSaveButton" runat="server" />
                                        </div>
                                        <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <uc6:PositionProfileSummary ID="PositionProfileDetailInfo_positionProfileSummary"
                                                        runat="server" OnAddSkillEvent="PositionProfileDetailInfo_positionProfileSummary_AddSkillEvent" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <table border="0" cellspacing="3" cellpadding="3" align="left">
                                                        <tr>
                                                            <td class="td_padding_top_5" style="padding-left: 10px">
                                                                <asp:Button ID="PositionProfileDetailInfo_positionProfileSummary_CreateButton" runat="server"
                                                                    Text="Create" SkinID="sknButtonId" OnClick="PositionProfileDetailInfo_positionProfileSummary_CreateButton_Click" />
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="PositionProfileDetailInfo_positionProfileSummary_RegenerateLinkButton"
                                                                    runat="server" Text="Regenerate" ToolTip="Click here to regenerate the keywords from the position profile segments"
                                                                    SkinID="sknPopupLinkButton" OnClick="PositionProfileDetailInfo_positionProfileSummary_RegenerateLinkButton_Click"
                                                                    Visible="true">
                                                                </asp:LinkButton>
                                                            </td>
                                                            <td class="td_padding_top_5">
                                                                <asp:LinkButton ID="PositionProfileDetailInfo_positionProfileSummary_cancelButton"
                                                                    runat="server" Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="PositionProfileDetailInfo_viewPositionProfileSave_modalpPopupExtender"
                                        runat="server" TargetControlID="PositionProfileDetailInfo_viewPositionProfileSaveButton"
                                        PopupControlID="PositionProfileDetailInfo_positionProfileSavePanel" BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                    <asp:HiddenField ID="PositionProfileDetailInfo_summaryHiddenField" runat="server"
                                        Value="-1" />
                                    <asp:HiddenField ID="PositionProfileDetailInfo_buttonCommandName" runat="server"
                                        Value="-1" />
                                </td>
                            </tr>
                            <tr>
                                <td style="display: none">
                                    <uc7:ClientInfoControl ID="PositionProfileDetailInfo_ClientInfoControl" runat="server"
                                        Visible="false" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="PositionProfileDetailInfo_bottomMessageUpdatePanel" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="PositionProfileDetailInfo_bottomSuccessMessageLabel" runat="server"
                                    SkinID="sknSuccessMessage"></asp:Label>
                                <asp:Label ID="PositionProfileDetailInfo_bottomErrorMessageLabel" runat="server"
                                    SkinID="sknErrorMessage"></asp:Label>
                            </ContentTemplate>
                            <Triggers>
                                 <asp:AsyncPostBackTrigger ControlID="PositionProfileDetailInfo_saveButton" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
             <asp:AsyncPostBackTrigger ControlID="PositionProfileDetailInfo_saveButton" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
