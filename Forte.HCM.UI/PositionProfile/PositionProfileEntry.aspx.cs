﻿#region Header                                                                 

#endregion

#region Directives                                                             

using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Web.UI;
using System.Reflection;
using System.Configuration;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

using Resources;
using AjaxControlToolkit;

#endregion
namespace Forte.HCM.UI.PositionProfile
{
    /// <summary>
    /// This page contains Position Profile CREATE, UPDATE,PREVIEW,SUMMARY, SAVE AS NEW functinalities.
    /// </summary>
    public partial class PositionProfileEntry : PageBase
    {

        #region Declaration                                                    
        string positionProfile_id = Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING;
        List<int> segmentIdList = new List<int>();
        List<SegmentDetail> segmentDetailList;
        string SEGMENT_DETAILS = "SEGMENT_DETAILS";
        string POSITION_PROFILE_DETAILS = "POSITION_PROFILE_DETAILS";
        string SEGMENTS_KEYWORD = "SEGMENTS_KEYWORD";
        string EDIT_POSITION_PROFILE_KEYWORD = "EDIT_POSITION_PROFILE_KEYWORD";
        string REQUIREMENT_PANEL_TEXTBOX = "REQUIREMENT_PANEL_TEXTBOX";
        int positionProfileId = 0;
        int featureID = 1;
        decimal totalWeightage, totalTechnicalWeightage, totalVerticalWeightage;
        decimal totalSegmentsVerticalWeightage, totalSegmentsTechnicalWeightage;
        string previousPage = string.Empty;
        //int clientID = 0;
        //int departmentID = 0;
        //int contactID = 0;


        #endregion

        #region Event Handler                                                  
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                PositionProfileEntry_eMailContentConfirmPopupExtenderControl.CommandEventAdd_Click += new CommonControls.ConfirmMsgControl.Button_CommandClick(PositionProfileEntry_eMailContentConfirmPopupExtenderControl_CommandEventAdd_Click);
                // Assign parent page navigation links.

                // Subscribe to keyword populated event.
                PositionProfileEntry_RequirementControl.KeywordPopupated += new 
                    RequirementViewerControl.KeywordPopupatedDelegate(PositionProfileEntry_RequirementControl_KeywordPopupated);

                // PositionProfileEntry_ClientInfoControl.ControlMessageThrown += new CommonControls.ClientInfoControl.ControlMessageThrownDelegate(PositionProfileEntry_ClientInfoControl_ControlMessageThrown);
                Page.Form.DefaultButton = PositionProfileEntry_topSaveButton.UniqueID;
                PositionProfileEntry_RequirementControl.FileUploaded += new
                    CommonControls.RequirementViewerControl.FileUploadedDelegate(PositionProfileEntry_RequirementControl_UploadClick);
                PositionProfileEntry_viewClientNameHiddenField.Value = base.NomenclatureAllAttributes.CommonNomenclatureAttributes.ClientName;

                Session["CLIENT_NAME"] = PositionProfileEntry_viewClientNameHiddenField.Value;
                LoadValues();

                Session["USER_ID"] = base.userID;
                Session["TENANT_ID"] = base.tenantID;
                if (!IsPostBack)
                {
                    AssignParentPageNavigationLink();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString.Get("pp_session_key")))
                    {
                        string sessionkey = Request.QueryString.Get("pp_session_key");
                        string candidateIds = string.Empty;
                        string keywords = string.Empty;

                        new PositionProfileBLManager().GetPositionProfileCandidateTempIds(sessionkey, out candidateIds, out keywords);
                        PositionProfileEntry_positionProfileNameTextBox.Text = keywords;
                        PositionProfileEntry_candidateIdsHiddenField.Value = candidateIds;
                    }
                }

                if (!Support.Utility.IsNullOrEmpty(Request.QueryString[positionProfile_id]))
                {


                    if (!IsPostBack)
                    {

                        ShowEditButton();
                        PositionProfileEntry_editPositionProfileIdHiddenField.Value = Request.QueryString[positionProfile_id];
                        BindPositionProfile();

                        Session["PP_CONTACT_LIST"] = "";
                    }
                    if (!Support.Utility.IsNullOrEmpty(Request.QueryString["type"]))
                    {
                        PositionProfileEntry_topSaveButton.Visible = false;
                        PositionProfileEntry_topSaveASButton.Text = "Copy Position Profile";
                        PositionProfileEntry_bottomSaveButton.Visible = false;
                        PositionProfileEntry_bottomSaveASButton.Text = "Copy Position Profile";
                        PositionProfileEntry_registeredByImageButton.Visible = false;
                    }
                    CreateTabs(false);
                    PositionProfileEntry_positionProfileSummary_RegenerateLinkButton.Visible = true;
                    PositionProfileEntry_headerLiteral.Text = HCMResource.PositionProfile_EditTitle;
                    Master.SetPageCaption(HCMResource.PositionProfile_EditTitle);
                }
                else
                {
                    Master.SetPageCaption(HCMResource.PositionProfile_Title);
                    PositionProfileEntry_headerLiteral.Text = HCMResource.PositionProfile_Title;
                    if (!Support.Utility.IsNullOrEmpty(ViewState[SEGMENT_DETAILS]))
                        CreateTabs(false);
                }

                if (CanCreatePositionProfileUsage(true))
                {
                    base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                         PositionProfileEntry_bottomErrorMessageLabel,
                         "You have reached the maximum limit based your subscription plan. Cannot create more position profiles");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the keyword popuplate event is fired from 
        /// the requirements control.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        private void PositionProfileEntry_RequirementControl_KeywordPopupated(object sender, KeywordEventArgs e)
        {
            try
            {
                // Check if data populated. 
                if ((e.TechnicalSkills == null || e.TechnicalSkills.Count == 0) &&
                    (e.Verticals == null || e.Verticals.Count == 0) &&
                    (e.PrimaryRole == null || e.PrimaryRole.Trim().Length == 0) &&
                    (e.SecondaryRole == null || e.SecondaryRole.Length == 0))
                {
                    // No data popuplated.
                    base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                        PositionProfileEntry_bottomErrorMessageLabel, "No data found to populate");

                    return;
                }
                else
                {
                    string message = "Data populated for ";
                    if (e.TechnicalSkills != null && e.TechnicalSkills.Count > 0)
                        message += "Technical Skills, ";

                    if (e.Verticals != null && e.Verticals.Count > 0)
                        message += "Verticals, ";

                    if (e.PrimaryRole != null && e.PrimaryRole.Trim().Length > 0)
                    {
                        message += "Roles";
                    }

                    message = message.Trim();
                    message = message.TrimEnd(',');
                    base.ShowMessage(PositionProfileEntry_topSuccessMessageLabel,
                       PositionProfileEntry_bottomSuccessMessageLabel, message);
                }

                // Populate technical skills.
                if (PositionProfileEntry_technicalSkillRequirementControl != null &&
                    e.TechnicalSkills != null && e.TechnicalSkills.Count > 0)
                {
                    PositionProfileEntry_technicalSkillRequirementControl.PopulateData(e.TechnicalSkills);
                }

                // Populate verticals.
                if (PositionProfileEntry_verticalBackgroundRequirementControl != null &&
                    e.Verticals != null && e.Verticals.Count > 0)
                {
                    PositionProfileEntry_verticalBackgroundRequirementControl.PopulateData(e.Verticals);
                }

                // Populate roles.
                if (PositionProfileEntry_roleRequirementControl != null)
                {
                    PositionProfileEntry_roleRequirementControl.PopulateData(e.PrimaryRole, e.SecondaryRole);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel, exp.Message);
            }
        }
        
        private void PositionProfileEntry_ClientInfoControl_ControlMessageThrown(object sender, ControlMessageEventArgs c)
        {
            try
            {
                // Show message on the label.
                if (c.MessageType == MessageType.Error)
                {
                    base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                    PositionProfileEntry_bottomErrorMessageLabel, c.Message);
                }
                else if (c.MessageType == MessageType.Success)
                {
                    base.ShowMessage(PositionProfileEntry_topSuccessMessageLabel,
                   PositionProfileEntry_bottomSuccessMessageLabel, c.Message);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel, exp.Message);
            }
        }

        void SingleQuestionEntry_categorySubjectControl_ControlMessageThrown(object sender, EventSupport.ControlMessageEventArgs c)
        {
            try
            {
                // Show message on the label.
                if (c.MessageType == MessageType.Error)
                {
                    base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                    PositionProfileEntry_bottomErrorMessageLabel, c.Message);
                }
                else if (c.MessageType == MessageType.Success)
                {
                    base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel, c.Message);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileEntry_defaultFormImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (int.Parse(PositionProfileEntry_formTypeDropWownList.SelectedValue) > 0)
                {
                    new AdminBLManager().UpdateDefaultForm(int.Parse(PositionProfileEntry_formTypeDropWownList.SelectedValue),
                        base.userID);

                    base.ShowMessage(PositionProfileEntry_topSuccessMessageLabel,
                      PositionProfileEntry_bottomSuccessMessageLabel,
                     HCMResource.PositionProfile_Update_DefaultForm);
                }
                else
                {
                    base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                     PositionProfileEntry_bottomErrorMessageLabel,
                     HCMResource.PositionProfile_Update_DefaultForm_Empty);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        void PositionProfileEntry_RequirementControl_UploadClick(object sender, EventArgs e)
        {
            try
            {
                switch (PositionProfileEntry_RequirementControl.Message)
                {
                    case MessageType.Success:
                        base.ShowMessage(PositionProfileEntry_topSuccessMessageLabel,
                            PositionProfileEntry_bottomSuccessMessageLabel,
                         HCMResource.PositionProfile_FileUpload_Successfully);
                        break;
                    case MessageType.Warning:
                        base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                         PositionProfileEntry_bottomErrorMessageLabel,
                         HCMResource.PositionProfile_FileUpload_Empty);
                        break;
                    case MessageType.Error:
                        base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                         PositionProfileEntry_bottomErrorMessageLabel,
                         HCMResource.PositionProfile_FileUpload_InvlaidFileType);
                        break;
                    case MessageType.Unexpected:
                        base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                         PositionProfileEntry_bottomErrorMessageLabel,
                         HCMResource.PositionProfile_FileUpload_UnExpectedError);
                        break;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        protected void PositionProfileEntry_previewButton_Click(object sender, EventArgs e)
        {
            try
            {
                List<Dictionary<object, object>> dictinaryList = null;
                PreviewPositionProfile(out dictinaryList);
                PositionProfileEntry_previewPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        private void PreviewPositionProfile(out  List<Dictionary<object, object>> dictionaryList)
        {
            PositionProfileEntry_previewPositionProfileNameLabel.Text =
                PositionProfileEntry_positionProfileNameTextBox.Text.Trim();
            PositionProfileEntry_previewPositionProfileRegisterLabel.Text =
                PositionProfileEntry_registeredByTextBox.Text.Trim();
            PositionProfileEntry_previewPositionProfileRegisterDateLabel.Text =
                PositionProfileEntry_dateOfRegistrationTextBox.Text.Trim();
            PositionProfileEntry_previewPositionProfileSubmittalDateLabel.Text =
                PositionProfileEntry_submittalDeadLineTextBox.Text.Trim();
            segmentDetailList = new List<SegmentDetail>();
            segmentDetailList = (List<SegmentDetail>)ViewState[SEGMENT_DETAILS];
            testPanle.Controls.Clear();

            dictionaryList = new List<Dictionary<object, object>>();
            Dictionary<object, object> dictionary = new Dictionary<object, object>();
            dictionary.Add("Position Profile Details", "");
            dictionary.Add(PositionProfileEntry_previewPositionProfileRegisterHeaderLabel.Text, PositionProfileEntry_registeredByTextBox.Text);
            dictionary.Add(PositionProfileEntry_previewPositionProfileNameHeaderLabel.Text, PositionProfileEntry_previewPositionProfileNameLabel.Text);
            dictionary.Add(PositionProfileEntry_previewPositionProfileRegisterDateHeaderLabel.Text, PositionProfileEntry_previewPositionProfileRegisterDateLabel.Text);
            dictionary.Add(PositionProfileEntry_previewPositionProfileSubmittalDateHeaderLabel.Text, PositionProfileEntry_previewPositionProfileSubmittalDateLabel.Text);
            dictionaryList.Add(dictionary);
            if (PositionProfileEntry_ClientInfoControl.ClientContactInformations != null)
            {
                PositionProfileEntry_previewClientDetailsTable.Visible = true;
                dictionary = new Dictionary<object, object>();
                dictionary.Add("Client Details", "");
                dictionary.Add("ClientDetails", PositionProfileEntry_ClientInfoControl.ClientContactInformations);
                dictionaryList.Add(dictionary);
                PositionProfileEntry_previewClientDetailsGridView.DataSource = PositionProfileEntry_ClientInfoControl.ClientContactInformations;
                PositionProfileEntry_previewClientDetailsGridView.DataBind();
            }
            else
            {
                PositionProfileEntry_previewClientDetailsTable.Visible = false;
            }

            //  List<DropDownItem> dropDownItem=null;
            Control segmentControl;
            for (int i = 0; i < segmentDetailList.Count; i++)
            {
                switch (segmentDetailList[i].SegmentID)
                {
                    case (int)SegmentType.ClientPositionDetail:
                        segmentControl = null;
                        segmentControl = LoadControl("../" + segmentDetailList[i].segmentPath);
                        ((Segments.ClientPositionDetailsControl)segmentControl).PreviewDataSource = PositionProfileEntry_clientPositionDetailsControl.DataSource;
                        PositionProfileEntry_previewPanel.Controls.Add(segmentControl);
                        ProcessXML(PositionProfileEntry_clientPositionDetailsControl.DataSource.DataSource, segmentDetailList[i].SegmentID, out dictionary);
                        dictionaryList.Add(dictionary);
                        break;
                    case (int)SegmentType.Education:
                        segmentControl = null;
                        segmentControl = LoadControl("../" + segmentDetailList[i].segmentPath);
                        ((Segments.EducationRequirementControl)segmentControl).PreviewDataSource =
                            PositionProfileEntry_educationRequirementControl.DataSource;
                        PositionProfileEntry_previewPanel.Controls.Add(segmentControl);
                        ProcessXML(PositionProfileEntry_educationRequirementControl.DataSource.DataSource, segmentDetailList[i].SegmentID, out dictionary);
                        dictionaryList.Add(dictionary);
                        break;
                    case (int)SegmentType.Roles:
                        segmentControl = null;
                        segmentControl = LoadControl("../" + segmentDetailList[i].segmentPath);
                        ((Segments.RoleRequirement)segmentControl).PreviewDataSource =
                            PositionProfileEntry_roleRequirementControl.DataSource;
                        PositionProfileEntry_previewPanel.Controls.Add(segmentControl);
                        ProcessXML(PositionProfileEntry_roleRequirementControl.DataSource.DataSource, segmentDetailList[i].SegmentID, out dictionary);
                        dictionaryList.Add(dictionary);
                        break;
                    case (int)SegmentType.TechnicalSkills:
                        segmentControl = null;
                        segmentControl = LoadControl("../" + segmentDetailList[i].segmentPath);
                        ((Segments.TechnicalSkillRequirement)segmentControl).PreviewDataSource =
                            PositionProfileEntry_technicalSkillRequirementControl.DataSource;
                        PositionProfileEntry_previewPanel.Controls.Add(segmentControl);
                        ProcessXML(PositionProfileEntry_technicalSkillRequirementControl.DataSource.DataSource, segmentDetailList[i].SegmentID, out dictionary);
                        dictionaryList.Add(dictionary);
                        break;
                    case (int)SegmentType.Verticals:
                        segmentControl = null;
                        segmentControl = LoadControl("../" + segmentDetailList[i].segmentPath);
                        ((Segments.VerticalBackgroundRequirement)segmentControl).PreviewDataSource =
                            PositionProfileEntry_verticalBackgroundRequirementControl.DataSource;
                        ((Segments.VerticalBackgroundRequirement)segmentControl).DisableControls();
                        PositionProfileEntry_previewPanel.Controls.Add(segmentControl);
                        ProcessXML(PositionProfileEntry_verticalBackgroundRequirementControl.DataSource.DataSource, segmentDetailList[i].SegmentID, out dictionary);
                        dictionaryList.Add(dictionary);
                        break;
                }


            }
            ViewState[REQUIREMENT_PANEL_TEXTBOX] = PositionProfileEntry_RequirementControl.RequirementPanelValue;
            //if (dropDownItem == null)
            //{
            //    Session["test"] = null;
            //    return;
            //}
            //for (int j = 0; j < dropDownItem.Count; j++)
            {
                //dictionary.Add(dropDownItem[j].DisplayText, dropDownItem[j].ValueText);

            }
            // Session["test"] = dictionaryList;
            // TestAreaChartControl_widgetMultiSelectControl.WidgetTypePropertyDataSource = testAreaList.ToList();


        }

        private void ProcessXML(string xmlText, int segmentId, out Dictionary<object, object> dictionary)
        {
            dictionary = new Dictionary<object, object>();
            Type idType = null;
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlText);
            switch (segmentId)
            {
                case (int)SegmentType.ClientPositionDetail:

                    dictionary.Add("Position Details", "");
                    ClientPositionDetails clientPositionDetails = new ClientPositionDetails();
                    idType = clientPositionDetails.GetType();
                    foreach (PropertyInfo pInfo in idType.GetProperties())
                    {
                        dictionary.Add(pInfo.Name, xmlDocument.GetElementsByTagName(pInfo.Name)[0].InnerText);
                    }
                    break;
                case (int)SegmentType.Education:

                    dictionary.Add("Education Details", "");
                    EducationRequirementDetails educationRequirementDetails = new EducationRequirementDetails();
                    idType = educationRequirementDetails.GetType();
                    foreach (PropertyInfo pInfo in idType.GetProperties())
                    {
                        dictionary.Add(pInfo.Name, xmlDocument.GetElementsByTagName(pInfo.Name)[0].InnerText);
                    }
                    break;
                case (int)SegmentType.Roles:

                    dictionary.Add("Role Details", "");
                    RoleRequirementDetails roleRequirementDetails = new RoleRequirementDetails();
                    idType = roleRequirementDetails.GetType();
                    foreach (PropertyInfo pInfo in idType.GetProperties())
                    {
                        dictionary.Add(pInfo.Name, xmlDocument.GetElementsByTagName(pInfo.Name)[0].InnerText);
                    }
                    break;
                case (int)SegmentType.TechnicalSkills:
                    TechnicalSkillDetails technicalSkillDetails = new TechnicalSkillDetails();
                    idType = technicalSkillDetails.GetType();
                    List<TechnicalSkillDetails> technicalSkillDetailsList = new List<TechnicalSkillDetails>();
                    dictionary.Add("Technical Details", "");
                    dictionary.Add("Total IT Experience", xmlDocument.GetElementsByTagName("TotalITExperience")[0].InnerText);
                    dictionary.Add("Total Weightage", xmlDocument.GetElementsByTagName("TotalWeightage")[0].InnerText);
                    if (Convert.ToBoolean(xmlDocument.GetElementsByTagName("IsAdditionalSkill")[0].InnerText))
                    {
                        dictionary.Add("Additional Skill", xmlDocument.GetElementsByTagName("AdditionalSkill")[0].InnerText);
                    }
                    for (int i = 0; i < xmlDocument.GetElementsByTagName("TechnicalSkillDetail").Count; i++)
                    {
                        technicalSkillDetails = new TechnicalSkillDetails();
                        foreach (PropertyInfo pInfo in idType.GetProperties())
                        {
                            pInfo.SetValue(technicalSkillDetails, xmlDocument.GetElementsByTagName(pInfo.Name)[i].InnerText, null);
                        }
                        technicalSkillDetailsList.Add(technicalSkillDetails);
                    }
                    dictionary.Add("TechDetails", technicalSkillDetailsList);
                    // dictionary.Add("Technical Details", "");
                    // TechnicalSkillDetails technicalSkillsDetails = new TechnicalSkillDetails();
                    //idType = technicalSkillsDetails.GetType();
                    break;
                case (int)SegmentType.Verticals:
                    dictionary.Add("Vertical Details", "");
                    VerticalDetails verticalDetail = new VerticalDetails();
                    idType = verticalDetail.GetType();
                    List<VerticalDetails> verticalDetailsList = new List<VerticalDetails>();
                    dictionary.Add("Total Weightage", xmlDocument.GetElementsByTagName("TotalWeightage")[0].InnerText);
                    if (Convert.ToBoolean(xmlDocument.GetElementsByTagName("IsAdditionalSkill")[0].InnerText))
                    {
                        if (xmlDocument.GetElementsByTagName("AdditionalSkill")[0].InnerText.Length > 0)
                        {
                            dictionary.Add("Additional Skill", xmlDocument.GetElementsByTagName("AdditionalSkill")[0].InnerText);
                        }
                    }
                    for (int i = 0; i < xmlDocument.GetElementsByTagName("VerticalDetail").Count; i++)
                    {
                        verticalDetail = new VerticalDetails();
                        foreach (PropertyInfo pInfo in idType.GetProperties())
                        {
                            pInfo.SetValue(verticalDetail, xmlDocument.GetElementsByTagName(pInfo.Name)[i].InnerText, null);
                        }
                        verticalDetailsList.Add(verticalDetail);
                    }
                    dictionary.Add("VerticleDetails", verticalDetailsList);
                    //VerticalDetails verticalDetails = new VerticalDetails();
                    //idType = verticalDetails.GetType();
                    //foreach (PropertyInfo pInfo in idType.GetProperties())
                    //{
                    //    dictionary.Add(pInfo.Name, xmlDocument.GetElementsByTagName(pInfo.Name)[0].InnerText);
                    //}
                    break;
            }




            //foreach (PropertyInfo pInfo in idType.GetProperties())
            //{
            //    // DropDownItem dropDownItems = new DropDownItem();
            //    //pInfo.SetValue(clientPositionDetails, xmlDocument.GetElementsByTagName(pInfo.Name)[0].InnerText, null);
            //    dictionary.Add(pInfo.Name, xmlDocument.GetElementsByTagName(pInfo.Name)[0].InnerText);
            //    //pInfo.SetValue(clientPositionDetails, xmlDocument.GetElementsByTagName(pInfo.Name)[0].InnerText, null);
            //}
            //if (Convert.ToBoolean(clientPositionDetails.ContractToHire))
            //{
            //    ClientPositionDetailsControl_contractToHireCheckBox.Checked = true;
            //    ClientPositionDetailsControl_durationTextBox.Text = clientPositionDetails.Duration;
            //}

            //string[] natureOfPosition = clientPositionDetails.NatureOfPosition.Split(',');
            //foreach (string item in natureOfPosition)
            //{
            //    for (int i = 0; i < ClientPositionDetailsControl_natureOfPositionsCheckBoxList.Items.Count; i++)
            //    {
            //        if (ClientPositionDetailsControl_natureOfPositionsCheckBoxList.Items[i].Value == item)
            //            ClientPositionDetailsControl_natureOfPositionsCheckBoxList.Items[i].Selected = true;
            //    }
            //}
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileEntry_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileEntry_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        /// <summary>
        /// Handles the Command event of the imageButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> 
        /// instance containing the event data.</param>
        void imageButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                IsDeleteClicked.Value = "";
                PositionProfileEntry_ConfirmPopupExtenderControl.Message =
                    HCMResource.PositionProfile_SegmentDelete_PopUp_Message;
                PositionProfileEntry_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                PositionProfileEntry_ConfirmPopupExtenderControl.Title =
                    HCMResource.PositionProfile_SegmentDelete_PopUp_Title;
                PositionProfileEntry_ConfirmPopupExtender.Show();
                PositionProfileEntry_toDeleteSegmentId.Value = e.CommandName;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        protected void PositionProfileEntry_addSegmentLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                segmentDetailList = new List<SegmentDetail>();
                segmentDetailList = ViewState[SEGMENT_DETAILS] as List<SegmentDetail>;
                PositionProfileEntry_segmentListControl.SelectedSegmentDetail = segmentDetailList;
                PositionProfileEntry_segmentListControl.SegmentList =
                    new AdminBLManager().GetAvailableActiveSegments();
                PositionProfileEntry_addSegmentPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        protected void PositionProfileEntry_segmentListControl_Add_segment(object sender, EventArgs e)
        {
            try
            {
                ControlVisibility();
                segmentDetailList = new List<SegmentDetail>();
                SegmentDetail segmentDetail;
                segmentDetailList = (List<SegmentDetail>)ViewState[SEGMENT_DETAILS];
                List<SegmentDetail> selectedSegmentList = new List<SegmentDetail>();
                selectedSegmentList = PositionProfileEntry_segmentListControl.SelectedSegmentDetail;
                int endDisplayOrder = 0;
                if (segmentDetailList.Count > 0)
                    endDisplayOrder = segmentDetailList[segmentDetailList.Count - 1].DisplayOrder;

                for (int i = 0; i < selectedSegmentList.Count; i++)
                {
                    segmentDetail = new SegmentDetail();
                    segmentDetail = selectedSegmentList[i];
                    if (!segmentDetailList.Exists(item => item.SegmentID == selectedSegmentList[i].SegmentID))
                    {
                        segmentDetail.DisplayOrder = endDisplayOrder + 1;
                        segmentDetailList.Add(segmentDetail);
                        endDisplayOrder = endDisplayOrder + 1;
                    }
                }
                ViewState[SEGMENT_DETAILS] = segmentDetailList;
                CreateTabs(true);
                base.ShowMessage(PositionProfileEntry_topSuccessMessageLabel,
               PositionProfileEntry_bottomSuccessMessageLabel,
              HCMResource.PositionProfile_SegmentsAddedRemovedSuccessfully);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        /// <summary>
        /// Handles the ActiveTabChanged event of the PositionProfileEntry_tabContainer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileEntry_tabContainer_ActiveTabChanged(object sender, EventArgs e)
        {
            try
            {
                ControlVisibility();
                if (PositionProfileEntry_tabContainer.ActiveTab != null)
                {
                    ControlVisibility(int.Parse(PositionProfileEntry_tabContainer.ActiveTab.ID.Remove(0, 3)));
                }
                //CreateTabs(PositionProfileEntry_tabContainer.);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileEntry_topSaveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileEntry_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;
                ControlVisibility();
                PositionProfileDetail positionProfileDetails = new PositionProfileDetail();
                positionProfileDetails.PositionName = PositionProfileEntry_positionProfileNameTextBox.Text.Trim();
                positionProfileDetails.PositionProfileAdditionalInformation = PositionProfileEntry_additionalInformationTextBox.Text.Trim();
                positionProfileDetails.RegisteredBy = int.Parse(PositionProfileEntry_registeredByHiddenField.Value);
                positionProfileDetails.RegistrationDate =
                    Convert.ToDateTime(PositionProfileEntry_dateOfRegistrationTextBox.Text.Trim());
                positionProfileDetails.SubmittalDate =
                    Convert.ToDateTime(PositionProfileEntry_submittalDeadLineTextBox.Text.Trim());
                segmentDetailList = new List<SegmentDetail>();
                List<SegmentDetail> addedSegmentDetailList = new List<SegmentDetail>();
                segmentDetailList = (List<SegmentDetail>)ViewState[SEGMENT_DETAILS];

                if (segmentDetailList.Count < 1)
                    return;
                List<SkillWeightage> verticalSkillWeightage = null;
                List<SkillWeightage> technicalSkillWeightage = null;
                for (int i = 0; i < segmentDetailList.Count; i++)
                {
                    switch (segmentDetailList[i].SegmentID)
                    {
                        case (int)SegmentType.ClientPositionDetail:
                            addedSegmentDetailList.Add(PositionProfileEntry_clientPositionDetailsControl.DataSource);
                            break;
                        case (int)SegmentType.CommunicationSkills:
                            break;
                        case (int)SegmentType.CulturalFit:
                            break;
                        case (int)SegmentType.DecisionMaking:
                            break;
                        case (int)SegmentType.Education:
                            addedSegmentDetailList.Add(PositionProfileEntry_educationRequirementControl.DataSource);
                            break;
                        case (int)SegmentType.InterpersonalSkills:
                            break;
                        case (int)SegmentType.Roles:
                            addedSegmentDetailList.Add(PositionProfileEntry_roleRequirementControl.DataSource);
                            break;
                        case (int)SegmentType.Screening:
                            break;
                        case (int)SegmentType.TechnicalSkills:
                            totalTechnicalWeightage = PositionProfileEntry_technicalSkillRequirementControl.TotalWeightage;
                            addedSegmentDetailList.Add(PositionProfileEntry_technicalSkillRequirementControl.DataSource);
                            totalSegmentsTechnicalWeightage =
                                PositionProfileEntry_technicalSkillRequirementControl.TotalSegmentsWeightage;
                            technicalSkillWeightage = new List<SkillWeightage>();
                            technicalSkillWeightage =
                                PositionProfileEntry_technicalSkillRequirementControl.SkillWeightageDataSource;
                            break;
                        case (int)SegmentType.Verticals:
                            totalVerticalWeightage = PositionProfileEntry_verticalBackgroundRequirementControl.TotalWeightage;
                            totalSegmentsVerticalWeightage =
                                PositionProfileEntry_verticalBackgroundRequirementControl.TotalSegmentsWeightage;
                            addedSegmentDetailList.Add(PositionProfileEntry_verticalBackgroundRequirementControl.DataSource);
                            verticalSkillWeightage = new List<SkillWeightage>();
                            verticalSkillWeightage =
                                PositionProfileEntry_verticalBackgroundRequirementControl.SkillWeightageDataSource;
                            break;
                    }
                }
                positionProfileDetails.Segments = addedSegmentDetailList;
                ViewState[POSITION_PROFILE_DETAILS] = positionProfileDetails;
                if (Support.Utility.IsNullOrEmpty(PositionProfileEntry_editPositionProfileIdHiddenField.Value))
                {
                    PositionProfileSummaryValidation(verticalSkillWeightage, technicalSkillWeightage, positionProfileDetails);
                }
                else
                {
                    int positionID = int.Parse(PositionProfileEntry_editPositionProfileIdHiddenField.Value);
                    Button button = (Button)sender;
                    PositionProfileEntry_buttonCommandName.Value = button.CommandName.ToString();
                    switch (button.CommandName)
                    {
                        case "saveas":
                            PositionProfileSummaryValidation(verticalSkillWeightage,
                                technicalSkillWeightage, positionProfileDetails);
                            // InsertPositionProfile(true);
                            break;
                        case "delete":
                            {
                                PositionProfileEntry_ConfirmPopupExtenderControl.Title =
                                    "Delete Position Profile";
                                PositionProfileEntry_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                                PositionProfileEntry_ConfirmPopupExtenderControl.Message =
                                    "Do you want delete the position profile";
                                PositionProfileEntry_ConfirmPopupExtender.Show();
                                // deletePositionProfile();
                                break;
                            }
                        default:
                            positionProfileDetails.PositionID = positionID;
                            PositionProfileSummaryValidation(verticalSkillWeightage,
                                technicalSkillWeightage, positionProfileDetails);
                            //UpdatePositionProfile(positionProfileDetails);
                            break;
                    }
                }
                CreateTabs(false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        protected void PositionProfileEntry_actionButton_Click(object sender, ImageClickEventArgs e)
        {
            int positionID = int.Parse(PositionProfileEntry_editPositionProfileIdHiddenField.Value);
            ImageButton button = (ImageButton)sender;
            switch (button.CommandName)
            {
                case "talentsearch":
                    Response.Redirect("~/TalentScoutHome.aspx?" +
                        Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING + "=" + positionID +
                        "&parentpage=" + Constants.ParentPage.POSITION_PROFILE, false);
                    break;
                case "authortest":
                    Response.Redirect("~/TestMaker/CreateAutomaticTest.aspx?m=1&s=0&" +
                        Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING + "=" + positionID +
                        "&parentpage=" + Constants.ParentPage.POSITION_PROFILE, false);
                    break;
                case "searchtest":
                    Response.Redirect("~/TestMaker/SearchTest.aspx?m=1&s=2&" +
                        Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING + "=" + positionID +
                        "&parentpage=" + Constants.ParentPage.POSITION_PROFILE, false);
                    break;

                case "InterviewTestMaker":
                    Response.Redirect("~/InterviewTestMaker/CreateAutomaticInterviewTest.aspx?m=1&s=0&" +
                         Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING + "=" + positionID +
                        "&parentpage=" + Constants.ParentPage.POSITION_PROFILE, false);
                    break;
                case "SearchInterviewTest":
                    Response.Redirect("~/InterviewTestMaker/SearchInterviewTest.aspx?m=1&s=2&" +
                         Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING + "=" + positionID +
                        "&parentpage=" + Constants.ParentPage.POSITION_PROFILE, false);
                    break;

                case "Status":
                    Response.Redirect("~/PositionProfile/PositionProfileStatus.aspx?m=1&s=1&" +
                        Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING + "=" + positionID +
                            "&parentpage=" + Constants.ParentPage.POSITION_PROFILE, false);
                    break;
                case "Activity":
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(),
             " ", "javascript:ShowPositionProfileActivityLog(" + positionID + ")", true);
                    break;
            }
        }
        protected void PositionProfileEntry_positionProfileSummary_CreateButton_Click(object sender, EventArgs e)
        {
            InsertPositionProfile(false);
        }
        protected void PositionProfileEntry_positionProfileSummary_AddSkillEvent(object sender, EventArgs e)
        {
            ShowPositionProfileSummary();
        }
        /// <summary>
        /// Handles the SelectedIndexChanged event of the PositionProfileEntry_formTypeDropWownList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileEntry_formTypeDropWownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PositionProfileEntry_toDeleteSegmentId.Value = null;
                PositionProfileEntry_ConfirmPopupExtender.Show();
                PositionProfileEntry_ConfirmPopupExtenderControl.Message =
                    HCMResource.PositionProfile_SegmentConrol_PopUp_Message;
                PositionProfileEntry_ConfirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                PositionProfileEntry_ConfirmPopupExtenderControl.Title =
                    HCMResource.PositionProfile_SegmentControl_PopUp_Title;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        /// <summary>
        /// Handles the CancelClick event of the PositionProfileEntry_ConfirmPopupExtenderControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileEntry_ConfirmPopupExtenderControl_CancelClick(object sender, EventArgs e)
        {
            try
            {
                ControlVisibility();
                if (Support.Utility.IsNullOrEmpty(PositionProfileEntry_toDeleteSegmentId.Value))
                {
                    segmentDetailList = ViewState[SEGMENT_DETAILS] as List<SegmentDetail>;
                    BindTabControl(true, false);
                    segmentDetailList = ViewState[SEGMENT_DETAILS] as List<SegmentDetail>;
                }
                else
                    CreateTabs(false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileEntry_previewCloseImageButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/> instance containing the event data.</param>
        protected void PositionProfileEntry_previewCloseImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (!Support.Utility.IsNullOrEmpty(ViewState[REQUIREMENT_PANEL_TEXTBOX]))
                    PositionProfileEntry_RequirementControl.RequirementPanelValue = ViewState[REQUIREMENT_PANEL_TEXTBOX].ToString();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileEntry_previewCancelButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileEntry_previewCancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Support.Utility.IsNullOrEmpty(ViewState[REQUIREMENT_PANEL_TEXTBOX]))
                    PositionProfileEntry_RequirementControl.RequirementPanelValue = ViewState[REQUIREMENT_PANEL_TEXTBOX].ToString();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        /// <summary>
        /// Handles the OkClick event of the PositionProfileEntry_ConfirmPopupExtenderControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileEntry_ConfirmPopupExtenderControl_OkClick(object sender, EventArgs e)
        {
            try
            {
                ControlVisibility();
                if (!Support.Utility.IsNullOrEmpty(PositionProfileEntry_toDeleteSegmentId.Value))
                {
                    int form_id = int.Parse(PositionProfileEntry_formIDHiddenField.Value);
                    if (!Support.Utility.IsNullOrEmpty(PositionProfileEntry_editPositionProfileIdHiddenField.Value))
                    {
                        form_id = 0;
                    }
                    segmentDetailList = new List<SegmentDetail>();
                    segmentDetailList = (List<SegmentDetail>)ViewState[SEGMENT_DETAILS];
                    SegmentDetail segmentDetail = new SegmentDetail();
                    segmentDetail = segmentDetailList.Find(item => item.SegmentID ==
                        int.Parse(PositionProfileEntry_toDeleteSegmentId.Value));
                    segmentDetailList.Remove(segmentDetail);
                    ViewState[SEGMENT_DETAILS] = segmentDetailList;
                    CreateTabs(true);
                    base.ShowMessage(PositionProfileEntry_topSuccessMessageLabel,
                       PositionProfileEntry_bottomSuccessMessageLabel,
                      HCMResource.PositionProfile_SegmentRemoved);
                }
                else
                {
                    BindTabControl(true, true);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        protected void PositionProfileEntry_positionProfileSummary_RegenerateLinkButton_Click(object sender, EventArgs e)
        {
            PositionProfileEntry_positionProfileSummary.SkillWeightageDataSource =
                ViewState[SEGMENTS_KEYWORD] as List<SkillWeightage>;
            ShowPositionProfileSummary();
        }
        #endregion

        #region Tab header image related methods                               
        internal class TabHeaderTemplate : ITemplate
        {
            ImageButton _colHeadersLit;
            bool _IsFilled;
            internal TabHeaderTemplate(ImageButton colHeaderLits, bool IsFilled)
            {
                _colHeadersLit = colHeaderLits;
                _IsFilled = IsFilled;
            }
            public void InstantiateIn(Control container)
            {
                try
                {
                    Panel panel = new Panel();
                    Label label = new Label();
                    label.Text = _colHeadersLit.CommandArgument;
                    label.Style.Add("padding-right", "5px");
                    panel.Controls.Add(label);
                    panel.Controls.Add(_colHeadersLit);
                    container.Controls.Add(panel);
                }
                catch (Exception exp)
                {
                    throw exp;
                }
            }
        }

        #endregion

        #region Private Methods                                                

        private void PositionProfileSummaryValidation(List<SkillWeightage> verticalSkillWeightage,
           List<SkillWeightage> technicalSkillWeightage, PositionProfileDetail positionProfileDetails)
        {
            string contactIDs = string.Empty;
            string clientContactIDs = string.Empty;
            string clientID = string.Empty;
            positionProfileDetails.RegisteredBy = base.userID;
            bool weightageFlag = true;
            totalWeightage = totalTechnicalWeightage + totalVerticalWeightage;
            if (totalWeightage > 100)
            {
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                                  PositionProfileEntry_bottomErrorMessageLabel,
                                  HCMResource.PositionProfile_Summary_Total_Weightage_Lessthen100);
                weightageFlag = false;
            }
            if (totalSegmentsVerticalWeightage > 100)
            {
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                                  PositionProfileEntry_bottomErrorMessageLabel,
                                  HCMResource.PositionProfile_Summary_Vertical_Weightage_Lessthen100);
                weightageFlag = false;
            }
            if (totalSegmentsTechnicalWeightage > 100)
            {
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                                  PositionProfileEntry_bottomErrorMessageLabel,
                                  HCMResource.PositionProfile_Summary_Technical_Weightage_Lessthen100);
                weightageFlag = false;
            }

            if (weightageFlag)
            {
                PositionProfileEntry_summaryHiddenField.Value = "1";
                PositionProfileEntry_positionProfileSummary.SetErrorMessage = "";
                ShowPositionProfileSummary();
                SkillWeightage skillWeightage = null;

                if (Support.Utility.IsNullOrEmpty(verticalSkillWeightage))
                {
                    verticalSkillWeightage = new List<SkillWeightage>();
                    skillWeightage = new SkillWeightage();
                    skillWeightage.UserSegments = true;
                    verticalSkillWeightage.Add(skillWeightage);
                }

                if (Support.Utility.IsNullOrEmpty(technicalSkillWeightage))
                {
                    technicalSkillWeightage = new List<SkillWeightage>();
                    skillWeightage = new SkillWeightage();
                    skillWeightage.UserSegments = true;
                    technicalSkillWeightage.Add(skillWeightage);
                }

                foreach (var ch in technicalSkillWeightage)
                {
                    if (!verticalSkillWeightage.Exists(item => item.SkillName == ch.SkillName &&
                        item.Weightage == ch.Weightage) && !Support.Utility.IsNullOrEmpty(ch.SkillName))
                        verticalSkillWeightage.AddRange(technicalSkillWeightage);
                }
                ViewState[SEGMENTS_KEYWORD] = verticalSkillWeightage;
                if (Support.Utility.IsNullOrEmpty(PositionProfileEntry_editPositionProfileIdHiddenField.Value))
                {
                    PositionProfileEntry_positionProfileSummary.SkillWeightageDataSource = verticalSkillWeightage;


                }
                else
                {
                    PositionProfileEntry_positionProfileSummary.SkillWeightageDataSource =
                        ViewState[EDIT_POSITION_PROFILE_KEYWORD] as List<SkillWeightage>;
                }

                ViewState[POSITION_PROFILE_DETAILS] = positionProfileDetails;


                //InsertContactdetails(ref contactIDs, ref clientContactIDs);


            }
        }

        /// <summary>
        /// Binds the position profile.
        /// </summary>
        private void BindPositionProfile()
        {
            PositionProfileDetail positionProfileDetail = new PositionProfileDetail();
            positionProfileDetail = new PositionProfileBLManager().GetPositionProfile
                (Convert.ToInt32(PositionProfileEntry_editPositionProfileIdHiddenField.Value));
            if (Support.Utility.IsNullOrEmpty(positionProfileDetail))
                return;

            if (positionProfileDetail.ClientID != 0)
            {
                List<Department> departmet = new List<Department>();
                PositionProfileEntry_clientIDHiddenField.Value = positionProfileDetail.ClientID.ToString();
                departmet.Add(new Department(positionProfileDetail.ClientID, positionProfileDetail.ClientName));
                PositionProfileEntry_ClientInfoControl.BindClientName = departmet;
                PositionProfileEntry_ClientInfoControl.BindDepartmentList = new ClientBLManager().GetDepartments(positionProfileDetail.ClientID);
                PositionProfileEntry_ClientInfoControl.BindContactList = new ClientBLManager().GetClientDerpartmentContacts(positionProfileDetail.ClientID);
                PositionProfileEntry_ClientInfoControl.BindSelectedDepartment = positionProfileDetail.DepartmentIds;
                PositionProfileEntry_ClientInfoControl.BindSelectedContact = positionProfileDetail.ContactIds;
            }
            PositionProfileEntry_positionProfileNameTextBox.Text = positionProfileDetail.PositionName;
            PositionProfileEntry_additionalInformationTextBox.Text = positionProfileDetail.PositionProfileAdditionalInformation;
            PositionProfileEntry_registeredByHiddenField.Value = positionProfileDetail.RegisteredBy.ToString();
            PositionProfileEntry_dateOfRegistrationTextBox.Text = positionProfileDetail.RegistrationDate.ToShortDateString();
            PositionProfileEntry_submittalDeadLineTextBox.Text = positionProfileDetail.SubmittalDate.ToShortDateString();
            ViewState[SEGMENT_DETAILS] = positionProfileDetail.Segments;
            CreateTabs(false);
            foreach (SegmentDetail segmentDetail in positionProfileDetail.Segments)
            {
                switch (segmentDetail.SegmentID)
                {
                    case (int)SegmentType.ClientPositionDetail:
                        PositionProfileEntry_clientPositionDetailsControl.DataSource = segmentDetail;
                        break;
                    //case (int)SegmentType.CommunicationSkills:
                    //    break;
                    //case (int)SegmentType.CulturalFit:
                    //    break;
                    //case (int)SegmentType.DecisionMaking:
                    //    break;
                    case (int)SegmentType.Education:
                        PositionProfileEntry_educationRequirementControl.DataSource = segmentDetail;
                        break;
                    //case (int)SegmentType.InterpersonalSkills:
                    //    break;
                    case (int)SegmentType.Roles:
                        PositionProfileEntry_roleRequirementControl.DataSource = segmentDetail;
                        break;
                    //case (int)SegmentType.Screening:
                    //    break;
                    case (int)SegmentType.TechnicalSkills:
                        PositionProfileEntry_technicalSkillRequirementControl.DataSource = segmentDetail;
                        break;
                    case (int)SegmentType.Verticals:
                        PositionProfileEntry_verticalBackgroundRequirementControl.DataSource = segmentDetail;
                        break;
                }
            }
            if (!Support.Utility.IsNullOrEmpty(positionProfileDetail.PositionProfileKeywords))
            {
                List<SkillWeightage> skillWeightageList = new List<SkillWeightage>();
                PositionProfileKeyword positionProfileKeyword = new PositionProfileKeyword();
                positionProfileKeyword = positionProfileDetail.PositionProfileKeywords;
                string[] segmentKeywords = positionProfileKeyword.SegmentKeyword.Split(',');
                foreach (string item in segmentKeywords)
                {
                    if (Support.Utility.IsNullOrEmpty(item))
                        continue;
                    string skillName = item.Substring(0, item.IndexOf('['));
                    int weightage = 0;
                    int.TryParse(item.Substring(item.IndexOf('[') + 1, item.Length - item.IndexOf('[') - 2), out weightage);
                    // if (skillWeightageList.Exists(item1 => item1.SkillName == skillName && item1.Weightage == weightage))
                    //   continue;
                    SkillWeightage skillWeightage = new SkillWeightage();
                    skillWeightage.SkillName = skillName;
                    skillWeightage.Weightage = weightage;
                    skillWeightage.UserSegments = false;
                    skillWeightageList.Add(skillWeightage);
                }

                string[] userKeywords = positionProfileKeyword.UserKeyword.Split(',');
                foreach (string item in userKeywords)
                {
                    if (Support.Utility.IsNullOrEmpty(item))
                        continue;
                    string skillName = item.Substring(0, item.IndexOf('['));
                    int weightage = 0;
                    int.TryParse(item.Substring(item.IndexOf('[') + 1, item.Length - item.IndexOf('[') - 2), out weightage);
                    SkillWeightage skillWeightage = new SkillWeightage();
                    skillWeightage.SkillName = skillName;
                    skillWeightage.Weightage = weightage;
                    skillWeightage.UserSegments = true;
                    skillWeightageList.Add(skillWeightage);
                }
                if (!Support.Utility.IsNullOrEmpty(skillWeightageList))
                    ViewState[EDIT_POSITION_PROFILE_KEYWORD] = skillWeightageList;
            }
        }


        /// <summary>
        /// Method that will clear success/error label messages.
        /// </summary>
        private void ClearLabelMessage()
        {
            PositionProfileEntry_topSuccessMessageLabel.Text = string.Empty;
            PositionProfileEntry_bottomSuccessMessageLabel.Text = string.Empty;
            PositionProfileEntry_topErrorMessageLabel.Text = string.Empty;
            PositionProfileEntry_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Binds the tab control.
        /// </summary>
        private void BindTabControl(bool activeTabFlag, bool mergeSegments)
        {
            PositionProfileEntry_tabContainer.Controls.Clear();
            List<SegmentDetail> segmentDetailList =
               new List<SegmentDetail>();
            segmentDetailList = new PositionProfileBLManager().
                GetPositionProfileSegment(int.Parse(PositionProfileEntry_formTypeDropWownList.SelectedValue));
            if (!mergeSegments)
            {
                ViewState[SEGMENT_DETAILS] = segmentDetailList;
                CreateTabs(activeTabFlag);
                return;
            }
            if (!Support.Utility.IsNullOrEmpty(ViewState[SEGMENT_DETAILS]))
            {
                var existsSegmentDetailList = ViewState[SEGMENT_DETAILS] as List<SegmentDetail>;
                for (int i = 0; i < segmentDetailList.Count; i++)
                {
                    if (!existsSegmentDetailList.Exists(item => item.SegmentID == segmentDetailList[i].SegmentID))
                    {
                        existsSegmentDetailList.Add(segmentDetailList[i]);
                    }
                }
                ViewState[SEGMENT_DETAILS] = existsSegmentDetailList;
            }
            else
            {
                ViewState[SEGMENT_DETAILS] = segmentDetailList;
            }
            CreateTabs(activeTabFlag);
        }

        /// <summary>
        /// Creates the tabs.
        /// </summary>
        /// <param name="activeTabFlag">if set to <c>true</c> [active tab flag].</param>
        private void CreateTabs(bool activeTabFlag)
        {
            PositionProfileEntry_tabContainer.Controls.Clear();
            if (Support.Utility.IsNullOrEmpty(ViewState[SEGMENT_DETAILS]))
                return;
            segmentDetailList = (List<SegmentDetail>)ViewState[SEGMENT_DETAILS];
            for (int i = 0; i < segmentDetailList.Count; i++)
            {
                segmentIdList.Add(segmentDetailList[i].SegmentID);
                TabPanel tabPanel = new TabPanel();
                tabPanel.ID = "tab" + segmentDetailList[i].SegmentID.ToString();
                tabPanel.HeaderText = segmentDetailList[i].SegmentName;
                // adding close ImageButton
                ImageButton imageButton = new ImageButton();
                imageButton.ID = "Img" + i;
                imageButton.ImageUrl = HCMResource.PositionProfile_TabCloseImagePath;
                //imageButton.CssClass = "tab_cont_close_button";
                imageButton.CommandName = segmentDetailList[i].SegmentID.ToString();
                //  imageButton.Attributes.Add("onclick", "DeleteClicked('" + IsDeleteClicked.ClientID + "');");
                imageButton.Command += new CommandEventHandler(imageButton_Command);
                imageButton.CommandArgument = segmentDetailList[i].SegmentName;
                tabPanel.HeaderTemplate = new TabHeaderTemplate(imageButton, false);
                PositionProfileEntry_tabContainer.Tabs.Add(tabPanel);
                if (i == 0 && activeTabFlag)
                    PositionProfileEntry_tabContainer.ActiveTab = tabPanel;
            }
            ViewState[SEGMENT_DETAILS] = segmentDetailList;
            if (PositionProfileEntry_tabContainer.ActiveTab != null)
            {
                ControlVisibility(int.Parse(PositionProfileEntry_tabContainer.ActiveTab.ID.Remove(0, 3)));
            }
        }

        /// <summary>
        /// Controls the visibility.
        /// </summary>
        private void ControlVisibility()
        {
            PositionProfileEntry_verticalBackgroundRequirementControl.Visible = false;
            PositionProfileEntry_technicalSkillRequirementControl.Visible = false;
            PositionProfileEntry_roleRequirementControl.Visible = false;
            PositionProfileEntry_educationRequirementControl.Visible = false;
            PositionProfileEntry_clientPositionDetailsControl.Visible = false;
        }

        /// <summary>
        /// Controls the visibility.
        /// </summary>
        /// <param name="segmentId">The segment id.</param>
        private void ControlVisibility(int segmentId)
        {
            switch (segmentId)
            {
                case (int)SegmentType.ClientPositionDetail:
                    PositionProfileEntry_clientPositionDetailsControl.Visible = true;
                    break;
                case (int)SegmentType.CommunicationSkills:
                    break;
                case (int)SegmentType.CulturalFit:
                    break;
                case (int)SegmentType.DecisionMaking:
                    break;
                case (int)SegmentType.Education:
                    PositionProfileEntry_educationRequirementControl.Visible = true;
                    break;
                case (int)SegmentType.InterpersonalSkills:
                    break;
                case (int)SegmentType.Roles:
                    PositionProfileEntry_roleRequirementControl.Visible = true;
                    break;
                case (int)SegmentType.Screening:
                    break;
                case (int)SegmentType.TechnicalSkills:
                    PositionProfileEntry_technicalSkillRequirementControl.Visible = true;
                    break;
                case (int)SegmentType.Verticals:
                    PositionProfileEntry_verticalBackgroundRequirementControl.Visible = true;
                    break;
            }
        }

        /// <summary>
        /// Resets the controls.
        /// </summary>
        /// <param name="segmentId">The segment id.</param>
        private void ResetControls(int segmentId)
        {
            switch (segmentId)
            {
                case (int)SegmentType.ClientPositionDetail:
                    PositionProfileEntry_clientPositionDetailsControl.ResetControls();
                    break;
                case (int)SegmentType.CommunicationSkills:
                    break;
                case (int)SegmentType.CulturalFit:
                    break;
                case (int)SegmentType.DecisionMaking:
                    break;
                case (int)SegmentType.Education:
                    PositionProfileEntry_educationRequirementControl.ResetControls();
                    break;
                case (int)SegmentType.InterpersonalSkills:
                    break;
                case (int)SegmentType.Roles:
                    PositionProfileEntry_roleRequirementControl.ResetControls();
                    break;
                case (int)SegmentType.Screening:
                    break;
                case (int)SegmentType.TechnicalSkills:
                    PositionProfileEntry_technicalSkillRequirementControl.ResetControls();
                    break;
                case (int)SegmentType.Verticals:
                    PositionProfileEntry_verticalBackgroundRequirementControl.ResetControls();
                    break;
            }
        }

        /// <summary>
        /// Binds the forms.
        /// </summary>
        private void BindForms()
        {
            PositionProfileEntry_formTypeDropWownList.DataSource = null;
            string form_id = Constants.PositionProfileConstants.FORM_ID_QUERYSTRING;
            PositionProfileEntry_formTypeDropWownList.DataSource = new PositionProfileBLManager().GetFormWithUsers(base.userID);
            PositionProfileEntry_formTypeDropWownList.DataTextField = Constants.PositionProfileConstants.FORM_NAME;
            PositionProfileEntry_formTypeDropWownList.DataValueField = Constants.PositionProfileConstants.FORM_ID;
            PositionProfileEntry_formTypeDropWownList.DataBind();
            if (!Support.Utility.IsNullOrEmpty(Request.QueryString[positionProfile_id]))
                return;

            PositionProfileEntry_formIDHiddenField.Value = new AdminBLManager().GetDefaultForm(base.userID).ToString();
            if (!Support.Utility.IsNullOrEmpty(Request.QueryString.Get(form_id)))
            {
                PositionProfileEntry_formIDHiddenField.Value = Request.QueryString.Get(form_id);

            }

            for (int i = 1; i < PositionProfileEntry_formTypeDropWownList.Items.Count; i++)
            {
                if (PositionProfileEntry_formTypeDropWownList.Items[i].Value == PositionProfileEntry_formIDHiddenField.Value)
                {
                    // PositionProfileEntry_formTypeTextBox.Text = PositionProfileEntry_formTypeDropWownList.Items[i].Text;
                    PositionProfileEntry_formTypeDropWownList.SelectedIndex = i;
                    break;
                }
            }
            BindTabControl(true, false);
        }

        /// <summary>
        /// Updates the position profile.
        /// </summary>
        private void UpdatePositionProfile()
        {
            PositionProfileDetail positionProfileDetails = new PositionProfileDetail();
            positionProfileDetails = ViewState[POSITION_PROFILE_DETAILS] as PositionProfileDetail;
            
            
            positionProfileId = new PositionProfileBLManager().UpdatePositionProfileAndSegmentDatas(positionProfileDetails, base.userID, 
                base.tenantID, PositionProfileEntry_ClientInfoControl.ClientContactInformations);
            if (positionProfileId > 0)
            {
                BindPositionProfile();
                base.ShowMessage(PositionProfileEntry_topSuccessMessageLabel,
                    PositionProfileEntry_bottomSuccessMessageLabel,
                HCMResource.PositionProfile_UpdatedSuccessfully);
                // Response.Redirect(Request.RawUrl, false);
            }
            else
            {
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                    PositionProfileEntry_bottomErrorMessageLabel,
                    HCMResource.PositionProfile_Already_Exists);
            }
        }

        /// <summary>
        /// Inserts the position profile.
        /// </summary>
        /// <param name="saveAsFlag">if set to <c>true</c> [save as flag].</param>
        private void InsertPositionProfile(bool saveAsFlag)
        {
            decimal PositionProfileSum = 0;
            PositionProfileDetail positionProfileDetails = new PositionProfileDetail();
            positionProfileDetails = ViewState[POSITION_PROFILE_DETAILS] as PositionProfileDetail;
            List<SkillWeightage> userKeywordSkillWeightage = new List<SkillWeightage>();
            userKeywordSkillWeightage = PositionProfileEntry_positionProfileSummary.SkillWeightageDataSource;
            string segmentskeyword = "";
            string userkeyword = "";
            bool skillFlag = false;
            bool duplicateFlag = false;
            List<PositionProfileKeywordDictionary> positionProfileKeywordDictionaryList =
                new List<PositionProfileKeywordDictionary>();

            if (!Support.Utility.IsNullOrEmpty(userKeywordSkillWeightage))
            {
                foreach (var ch in userKeywordSkillWeightage)
                {
                    if (ch.UserSegments)
                    {
                        if (Support.Utility.IsNullOrEmpty(ch.SkillName))
                            continue;
                        userkeyword = userkeyword + ch.SkillName +
                              "[" + Convert.ToInt32(ch.Weightage) + "],";
                        PositionProfileSum = PositionProfileSum + Convert.ToInt32(ch.Weightage);
                        GetPositionProfileDictinaryKeyword(ref skillFlag, ref duplicateFlag,
                            positionProfileKeywordDictionaryList, ch);

                    }
                    else
                    {
                        if (Support.Utility.IsNullOrEmpty(ch.SkillName))
                            continue;
                        segmentskeyword = segmentskeyword + ch.SkillName +
                            "[" + Convert.ToInt32(ch.Weightage) + "],";
                        PositionProfileSum = PositionProfileSum + Convert.ToInt32(ch.Weightage);
                        GetPositionProfileDictinaryKeyword(ref skillFlag, ref duplicateFlag,
                            positionProfileKeywordDictionaryList, ch);
                    }
                }
            }

            if (duplicateFlag)
            {
                PositionProfileEntry_positionProfileSummary.SetErrorMessage =
                 HCMResource.PositionProfile_summary_SkillName_Duplicate;
                ShowPositionProfileSummary();
                return;
            }
            if (PositionProfileSum > 100)
            {
                PositionProfileEntry_positionProfileSummary.SetErrorMessage =
                   HCMResource.PositionProfile_Summary_Total_Weightage_Lessthen100;
                ShowPositionProfileSummary();
                return;
            }
            string keyword = "";
            positionProfileDetails.PositionProfileDictinaryKeywords = positionProfileKeywordDictionaryList;
            PositionProfileKeyword positionProfileKeyword = new PositionProfileKeyword();
            positionProfileKeyword.SegmentKeyword = segmentskeyword.TrimEnd(',');
            positionProfileKeyword.UserKeyword = userkeyword.TrimEnd(',');
            if (segmentskeyword.TrimEnd(',').Length > 0)
                keyword = segmentskeyword;
            if (userkeyword.TrimEnd(',').Length > 0)
                keyword = keyword + userkeyword.TrimEnd(',');
            positionProfileKeyword.Keyword = keyword.TrimEnd(',');
            positionProfileDetails.PositionProfileKeywords = positionProfileKeyword;
            ViewState[POSITION_PROFILE_DETAILS] = positionProfileDetails;
            if (!skillFlag)
            {
                PositionProfileEntry_positionProfileSummary.SetErrorMessage =
                    HCMResource.PositionProfile_summary_SkillName_Empty;
                ShowPositionProfileSummary();
                return;
            }

            if (Support.Utility.IsNullOrEmpty(PositionProfileEntry_editPositionProfileIdHiddenField.Value)
                || PositionProfileEntry_buttonCommandName.Value == "saveas")
            {
                if (CanCreatePositionProfileUsage(false))
                {
                    base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                         PositionProfileEntry_bottomErrorMessageLabel,
                         "You have reached the maximum limit based your subscription plan. Cannot create more position profiles");
                    return;
                }

                List<UserDetail> userDetails = new List<UserDetail>();
                userDetails = null;

                positionProfileId = new PositionProfileBLManager().
                    InsertPositionProfileAndSegmentDatas(positionProfileDetails, PositionProfileEntry_ClientInfoControl.ClientContactInformations,userDetails, base.userID, base.tenantID);

                if (positionProfileId > 0)
                {
                    //if (!Forte.HCM.Support.Utility.IsNullOrEmpty(PositionProfileEntry_candidateIdsHiddenField) && !Forte.HCM.Support.Utility.IsNullOrEmpty(PositionProfileEntry_candidateIdsHiddenField.Value))
                    {
                        if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString.Get("pp_session_key")))
                        {
                            string sessionkey = Request.QueryString.Get("pp_session_key");
                            new PositionProfileBLManager().InsertPositionProfileTSCandidate(sessionkey, positionProfileId);
                        }
                    }
                    ShowEditButton();
                    PositionProfileEntry_editPositionProfileIdHiddenField.Value = positionProfileId.ToString();

                    // InsertContactdetails(ref contactIDs, ref clientContactIDs);

                    BindPositionProfile();
                    PositionProfileEntry_headerLiteral.Text = HCMResource.PositionProfile_EditTitle;
                    Master.SetPageCaption(HCMResource.PositionProfile_EditTitle);
                    base.ShowMessage(PositionProfileEntry_topSuccessMessageLabel,
                        PositionProfileEntry_bottomSuccessMessageLabel,
                        HCMResource.PositionProfile_AddedSuccessfully);
                }
                else
                {
                    if (!saveAsFlag)
                        PositionProfileEntry_editPositionProfileIdHiddenField.Value = null;


                    base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                        PositionProfileEntry_bottomErrorMessageLabel,
                        HCMResource.PositionProfile_Already_Exists);
                }
            }
            else
            {
                UpdatePositionProfile();
                // InsertContactdetails(ref contactIDs, ref clientContactIDs);
            }
        }



        private static void GetPositionProfileDictinaryKeyword(ref bool skillFlag, ref bool duplicateFlag,
            List<PositionProfileKeywordDictionary> positionProfileKeywordDictionaryList, SkillWeightage ch)
        {
            if (!positionProfileKeywordDictionaryList.Exists(item => item.Keyword.ToLower() ==
                ch.SkillName.ToLower() && item.skillTypes == ch.SkillTypes))
            {
                PositionProfileKeywordDictionary positionProfileKeywordDictionary =
                    new PositionProfileKeywordDictionary();
                positionProfileKeywordDictionary.Keyword = ch.SkillName;
                positionProfileKeywordDictionary.skillTypes = ch.SkillTypes;
                positionProfileKeywordDictionary.SkillCategory = ch.SkillCategory;
                positionProfileKeywordDictionaryList.Add(positionProfileKeywordDictionary);
            }
            else
                duplicateFlag = true;

            if (!Support.Utility.IsNullOrEmpty(ch.SkillName) &&
                !Support.Utility.IsNullOrEmpty(ch.Weightage))
                skillFlag = true;
        }

        /// <summary>
        /// Shows the edit button.
        /// </summary>
        private void ShowEditButton()
        {
            if (!Support.Utility.IsNullOrEmpty(Request.QueryString["type"]))
            {
                PositionProfileEntry_topSaveButton.Visible = false;
                PositionProfileEntry_topSaveASButton.Text = "Copy Position Profile";
            }
            PositionProfileEntry_topSaveASButton.Visible = true;
            PositionProfileEntry_bottomSaveASButton.Visible = true;
            PositionProfileEntry_authorTestsearchExistingTestImageButton.Visible = true;
            PositionProfileEntry_authorTestImageButton.Visible = true;
            PositionProfileEntry_talentSearchPositionProfileImageButton.Visible = true;
            PositionProfileEntry_bottomTalentSearchPositionProfileImageButton.Visible = true;
            PositionProfileEntry_bottomAuthorTestImageButton.Visible = true;
            PositionProfileEntry_bottomAuthorTestsearchExistingTestImageButton.Visible = true;
            PositionProfileEntry_statusImageButton.Visible = true;
            PositionProfileEntry_bottomStatusImageButton.Visible = true;
            PositionProfileEntry_activityImageButton.Visible = true;
            PositionProfileEntry_bottomActivityImageButton.Visible = true;
            PositionprofileEntry_InterviewTestMakerImageButton.Visible = true;
            PositionprofileEntry_SearchInterviewTestImageButton.Visible=true;
            PositionProfileEntry_bottomOfflineInterviewTestImageButton.Visible = true;
                PositionProfileEntry_bottomSearchExistingOfflineInterviewTestImageButton.Visible=true;
            //PositionProfileEntry_bottomDeleteButton.Visible = true;
            //PositionProfileEntry_topDeleteButton.Visible = true;
        }
        //public void ClientManagement_department_ModalpPopupExtender1()
        //{
        //    PositionProfileEntry_ClientInfoControl.ClientInfoControl_department_ModalpPopupExtender1();
        //}
        //public void ClientManagement_contact_ModalpPopupExtender1()
        //{
        //    PositionProfileEntry_ClientInfoControl.ClientInfoControl_contact_ModalpPopupExtender1();
        //}

        #endregion

        #region Protected Overriden Methods                                    

        public void ShowPositionProfileSummary()
        {
            PositionProfileEntry_viewPositionProfileSave_modalpPopupExtender.Show();
        }

        private bool CanCreatePositionProfileUsage(bool veryFy)
        {
            if (!Support.Utility.IsNullOrEmpty(PositionProfileEntry_editPositionProfileIdHiddenField.Value) && veryFy)
                return false;

            return new AdminBLManager().IsFeatureUsageLimitExceeds(base.tenantID, featureID);
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            ClearLabelMessage();
            bool isValidData = true;
            bool registrationDate = true;
            bool submittalDate = true;

            DataList ClientInfoControl_clientDataList = (DataList)PositionProfileEntry_ClientInfoControl.FindControl("ClientInfoControl_clientDataList");
            TextBox ClientInfoControl_departmentComboTextBox = (TextBox)PositionProfileEntry_ClientInfoControl.FindControl("ClientInfoControl_departmentComboTextBox");
            TextBox ClientInfoControl_departmentContactComboTextbox = (TextBox)PositionProfileEntry_ClientInfoControl.FindControl("ClientInfoControl_departmentContactComboTextbox");
            TextBox ClientInfoControl_clientNameTextBox = (TextBox)PositionProfileEntry_ClientInfoControl.FindControl("ClientInfoControl_clientNameTextBox");

            //if (ClientInfoControl_clientDataList.Items.Count != 0)
            //{
            //    if (ClientInfoControl_departmentContactComboTextbox.Text.Trim().Length == 0 &&
            //        ClientInfoControl_departmentComboTextBox.Text.Trim().Length == 0)
            //    {
            //        isValidData = false;
            //        base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
            //             PositionProfileEntry_bottomErrorMessageLabel,
            //             HCMResource.PositionProfileEntry_PositionProfileContactDetailsInsufficient);
            //        return isValidData;
            //    }
            //}

            if (CanCreatePositionProfileUsage(true))
            {
                isValidData = false;
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                     PositionProfileEntry_bottomErrorMessageLabel,
                     "You have reached the maximum limit based your subscription plan. Cannot create more position profiles");

                return isValidData;
            }
            if (PositionProfileEntry_positionProfileNameTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                  HCMResource.PositionProfile_EnterPositionProfileName);
            }

            DateTime registratationDateValue = DateTime.Now;
            DateTime submittalDeadLineDateValue = DateTime.Now;
            if (PositionProfileEntry_dateOfRegistrationTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   HCMResource.PositionProfile_EnterRegistrationDate);
            }
            else
            {
                if (!DateTime.TryParse(PositionProfileEntry_dateOfRegistrationTextBox.Text.Trim(),
                    out registratationDateValue))
                {
                    isValidData = false;
                    registrationDate = false;
                    base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                       PositionProfileEntry_bottomErrorMessageLabel,
                       HCMResource.PositionProfile_EnterValidRegistrationDate);
                }
            }
            if (PositionProfileEntry_submittalDeadLineTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   HCMResource.PositionProfile_EnterSubmittalDate);
            }
            else
            {
                if (!DateTime.TryParse(PositionProfileEntry_submittalDeadLineTextBox.Text.Trim(),
                    out submittalDeadLineDateValue))
                {
                    submittalDate = false;
                    isValidData = false;
                    base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                       PositionProfileEntry_bottomErrorMessageLabel,
                      HCMResource.PositionProfile_EnterValidSubmittalDate);
                }

            }
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState[SEGMENT_DETAILS]))
            {
                segmentDetailList = new List<SegmentDetail>();
                segmentDetailList = (List<SegmentDetail>)ViewState[SEGMENT_DETAILS];
                if (segmentDetailList.Count < 1)
                {
                    isValidData = false;
                    base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                       PositionProfileEntry_bottomErrorMessageLabel,
                      HCMResource.PositionProfile_Segment_Empty);
                }
            }

            if (registrationDate && submittalDate)
            {
                if (DateTime.Compare(submittalDeadLineDateValue, registratationDateValue) < 0)
                {
                    isValidData = false;
                    base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                       PositionProfileEntry_bottomErrorMessageLabel,
                       HCMResource.PositionProfile_CompareRegistrationDateSubmittalDate);
                }
            }


            return isValidData;
        }
        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            ClearLabelMessage();
            ClientManagementEventInitialize();
            if (!IsPostBack)
            {
                Page.Form.DefaultFocus = PositionProfileEntry_positionProfileNameTextBox.UniqueID;
                PositionProfileEntry_positionProfileNameTextBox.Focus();
                PositionProfileEntry_dateOfRegistrationTextBox.Text = DateTime.Today.ToShortDateString();
                if (!Support.Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                    PositionProfileEntry_registeredByTextBox.Text = ((UserDetail)Session["USER_DETAIL"]).FirstName;

                PositionProfileEntry_registeredByHiddenField.Value = base.userID.ToString();
                BindForms();
                PositionProfileEntry_registeredByImageButton.Attributes.Add("onclick", "return LoadUserUnderTenantID('"
                 + PositionProfileEntry_registeredBydummyAuthorIdHidenField.ClientID + "','"
                 + PositionProfileEntry_registeredByHiddenField.ClientID + "','"
                 + PositionProfileEntry_registeredByTextBox.ClientID + "','PO_PRO_FORM_CREAT')");
            }

        }


        #endregion Protected Overriden Methods

        #region Client Management                                              

        private void AssignParentPageNavigationLink()
        {
            bool backLinkFlag = false;
            string backLinkCaption = "";
            int clientId = 0;
            int departementId = 0;
            int contactId = 0;
            List<int> selectedDepartment = null;

            if (Request.QueryString[Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING] != null && Request.QueryString[Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING] != null)
            {
                contactId = int.Parse(Request.QueryString[Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING]);

                departementId = int.Parse(Request.QueryString[Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING]);
                selectedDepartment = new List<int>();
                selectedDepartment.Add(departementId);

                backLinkCaption = "Back to Contact";
                backLinkFlag = true;
            }
            else if (Request.QueryString[Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING] != null)
            {
                departementId = int.Parse(Request.QueryString[Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING]);
                selectedDepartment = new List<int>();
                selectedDepartment.Add(departementId);
                backLinkCaption = "Back to Department";
                backLinkFlag = true;
            }
            else if (Request.QueryString[Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING] != null)
            {
                backLinkFlag = true;
                clientId = int.Parse(Request.QueryString[Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING]);
                backLinkCaption = "Back to Client";
            }
            else
            {
                return;
            }
            int newClientId = 0;
            string clientName = "";
            new ClientBLManager().GetClientName(clientId == 0 ? (int?)null : clientId, departementId == 0 ? (int?)null : departementId, contactId == 0 ? (int?)null : contactId, out newClientId, out clientName);
            if (newClientId == 0)
                return;


            // DSK Changed newClientId + "-" + "0-" + contactId.ToString() 0 Insted of departementId

            List<string> selectedContacts = new List<string>();
            selectedContacts.Add(newClientId + "-" + departementId + "-" + contactId.ToString());
                      
            List<Department> departmet = new List<Department>();
           
            PositionProfileEntry_clientIDHiddenField.Value = newClientId.ToString();           
           
            departmet.Add(new Department(newClientId, clientName));
            PositionProfileEntry_ClientInfoControl.BindClientName = departmet;

            PositionProfileEntry_ClientInfoControl.BindDepartmentList = new ClientBLManager().GetDepartments(newClientId);
            PositionProfileEntry_ClientInfoControl.BindSelectedDepartment = selectedDepartment;   

            PositionProfileEntry_ClientInfoControl.BindContactList = new ClientBLManager().GetClientDerpartmentContacts(newClientId);
         
           PositionProfileEntry_ClientInfoControl.BindSelectedContact = selectedContacts;

            PositionProfileEntry_topPreviousPageLinkButtonSeparatorLabel.Visible = backLinkFlag;
            PositionProfileEntry_bottomPreviousPageLinkButtonSeparatorLabel.Visible = backLinkFlag;
            PositionProfileEntry_topPreviousPageLinkButton.Visible = backLinkFlag;
            PositionProfileEntry_bottomPreviousPageLinkButton.Visible = backLinkFlag;
            PositionProfileEntry_topPreviousPageLinkButton.Text = backLinkCaption;
            PositionProfileEntry_bottomPreviousPageLinkButton.Text = backLinkCaption;
        }

        protected void PositionProfileEntry_previousPageLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                string clientMgmt_client = "";
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString[Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING]))
                {
                    clientMgmt_client = Request.QueryString[Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING];
                    Response.Redirect(@"~\ClientCenter\ClientManagement.aspx?" + Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING + "=" + clientMgmt_client + "&m=0&s=0", false);
                }
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString[Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING]))
                {
                    clientMgmt_client = Request.QueryString[Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING];
                    Response.Redirect(@"~\ClientCenter\ClientManagement.aspx?" + Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING + "=" + clientMgmt_client + "&m=0&s=0", false);
                }
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(Request.QueryString[Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING]))
                {
                    clientMgmt_client = Request.QueryString[Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING];
                    Response.Redirect(@"~\ClientCenter\ClientManagement.aspx?" + Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING + "=" + clientMgmt_client + "&m=0&s=0", false);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        private void ClientManagementEventInitialize()
        {
            try
            {
                PositionProfileEntry_ClientInfoControl.DataListEvent_Command += new ClientInfoControl.DataListCommand(PositionProfileEntry_ClientInfoControl_DataListEvent_Command);
                PositionProfileEntry_ClientInfoControl.CommandEventAdd_Click += new CommonControls.ClientInfoControl.Button_CommandClick(PositionProfileEntry_ClientInfoControl_CommandEventAdd_Click);
                PositionProfileEntry_ClientInfoControl.AddClientEvent_Command+=new ClientInfoControl.AddClientEvent(PositionProfileEntry_ClientInfoControl_AddClientEvent_Command);
                PositionProfileEntry_clientControl.OkCommandClick += new ClientControl.Button_CommandClick(PositionProfileEntry_clientManagemant_OkCommandClick);
                PositionProfileEntry_departmentControl.OkCommandClick += new DepartmentControl.Button_CommandClick(PositionProfileEntry_clientManagemant_OkCommandClick);
                PositionProfileEntry_contactControl.OkCommandClick += new ContactControl.Button_CommandClick(PositionProfileEntry_clientManagemant_OkCommandClick);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }

        }

        void PositionProfileEntry_ClientInfoControl_DataListEvent_Command(object sender, DataListCommandEventArgs e)
        {
            try
            {
                PositionProfileEntry_clientIDHiddenField.Value = null;
                PositionProfileEntry_ClientInfoControl.BindClientName = null;
                PositionProfileEntry_ClientInfoControl.BindDepartmentList = null;
                PositionProfileEntry_ClientInfoControl.BindContactList = null;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        void PositionProfileEntry_ClientInfoControl_AddClientEvent_Command(object sender, EventArgs e)
        {
            try
            {
                TextBox ClientInfoControl_clientNameTextBox1 = (TextBox)sender;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(ClientInfoControl_clientNameTextBox1.Text))
                {
                    base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                     PositionProfileEntry_bottomErrorMessageLabel,
                     Resources.HCMResource.SearchClientControl_ClientCannotBeEmpty);
                }
                else
                {
                    string[] clientDetail = ClientInfoControl_clientNameTextBox1.Text.ToString().Split(new char[] { '|' });
                    if (clientDetail.Length == 2)
                    {
                        int result = 0;
                        if (!int.TryParse(clientDetail[1].ToString(), out result))
                        {
                            // If so, raise the event handler.
                            base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                                PositionProfileEntry_bottomErrorMessageLabel,
                                Resources.HCMResource.SearchClientControl_ClientCannotBeEmpty);
                            return;
                        }
                        // Validate whether both client name and id match with each other
                        if (clientDetail.Length == 2)
                        {
                            if (!new CommonBLManager().IsValidClient
                                (Convert.ToInt32(clientDetail[1]), clientDetail[0].Trim()))
                            {
                                // If fails
                                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                                       PositionProfileEntry_bottomErrorMessageLabel,
                                       "'" + clientDetail[0].Trim() + "' and '"
                                       + clientDetail[1].Trim() + "' are mismatched");
                            }
                            else
                            {
                                List<Department> departmet = new List<Department>();
                                PositionProfileEntry_clientIDHiddenField.Value = result.ToString();
                                departmet.Add(new Department(result, clientDetail[0]));
                                PositionProfileEntry_ClientInfoControl.BindClientName = departmet;
                                PositionProfileEntry_ClientInfoControl.BindDepartmentList = new ClientBLManager().GetDepartments(result);
                                PositionProfileEntry_ClientInfoControl.BindContactList = new ClientBLManager().GetClientDerpartmentContacts(result);
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        void PositionProfileEntry_clientManagemant_OkCommandClick(object sender, CommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case Constants.ClientManagementConstants.ADD_CLIENT:
                        int clientID = 0;
                        ClientInformation clientInformation = new ClientInformation();
                        clientInformation = PositionProfileEntry_clientControl.ClientDataSource;
                        clientInformation.CreatedBy = base.userID;
                        clientInformation.TenantID = base.tenantID;
                        new ClientBLManager().InsertClient(clientInformation, out clientID);
                        if (clientID > 0)
                        {
                            PositionProfileEntry_clientIDHiddenField.Value = clientID.ToString();
                            base.ShowMessage(PositionProfileEntry_topSuccessMessageLabel,
                                 PositionProfileEntry_bottomSuccessMessageLabel,
                                "Client created successfully");
                            List<Department> departmet = new List<Department>();
                            departmet.Add(new Department(clientID, clientInformation.ClientName));
                            PositionProfileEntry_ClientInfoControl.BindClientName = departmet;
                            PositionProfileEntry_ClientInfoControl.BindDepartmentList = null;
                        }
                        else
                        {
                            PositionProfileEntry_clientIDHiddenField.Value = null;
                            PositionProfileEntry_ClientInfoControl.BindClientName = null;
                            PositionProfileEntry_clientControl.ShowMessage = "Client name already exist for this tenant";
                            ClientManagement_client_ModalpPopupExtender();
                        }
                        break;
                    case Constants.ClientManagementConstants.ADD_CLIENT_DEPARTMENT:
                        int departmentID = 0;
                        Department department = new Department();
                        department = PositionProfileEntry_departmentControl.DepartmentDataSource;
                        department.CreatedBy = base.userID;
                        new ClientBLManager().InsertDepartment(department, out departmentID);
                        if (departmentID > 0)
                        {
                            PositionProfileEntry_departIDHiddenField.Value = departmentID.ToString();
                            PositionProfileEntry_ClientInfoControl.BindDepartmentList = new CommonBLManager().GetDepartments
                                 (Convert.ToInt16(PositionProfileEntry_clientIDHiddenField.Value));
                            base.ShowMessage(PositionProfileEntry_topSuccessMessageLabel,
                                  PositionProfileEntry_bottomSuccessMessageLabel,
                                 "Department created successfully");
                        }
                        else
                        {
                            PositionProfileEntry_departIDHiddenField.Value = null;
                            PositionProfileEntry_departmentControl.ShowMessage = "Department name already exist for this client";
                            ClientManagement_department_ModalpPopupExtender();
                            PositionProfileEntry_departmentControl.ClientDataSource = new ClientBLManager().
                                GetClientInformationByClientId(int.Parse(PositionProfileEntry_clientIDHiddenField.Value.ToString()));
                        }
                        break;
                    case Constants.ClientManagementConstants.ADD_CONTACT:
                        int contactID = 0;
                        ClientContactInformation clientContactInformation = new ClientContactInformation();
                        clientContactInformation = PositionProfileEntry_contactControl.clientContactInformationDataSource;
                        clientContactInformation.CreatedBy = base.userID;
                        clientContactInformation.TenantID = base.tenantID;
                        new ClientBLManager().InsertContact(clientContactInformation, out contactID);
                        if (contactID > 0)
                        {
                            PositionProfileEntry_ClientInfoControl.BindContactList = new ClientBLManager().
                                GetClientDerpartmentContacts(int.Parse(PositionProfileEntry_clientIDHiddenField.Value.ToString()));
                        }
                        else
                        {
                            PositionProfileEntry_contactControl.ShowMessage = "Contact email id already exist for this tenant";
                            ClientManagement_contact_ModalpPopupExtender1();
                        }
                        break;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }

        }


        void PositionProfileEntry_ClientInfoControl_CommandEventAdd_Click(object sender, CommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case Constants.ClientManagementConstants.ADD_CLIENT:
                        PositionProfileEntry_clientControl.ClientDataSource = null;
                        ClientManagement_client_ModalpPopupExtender();
                        break;
                    case Constants.ClientManagementConstants.ADD_DEPARTMENT:
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(PositionProfileEntry_clientIDHiddenField.Value))
                        {
                            base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                              PositionProfileEntry_bottomErrorMessageLabel,
                             "Please select client");
                        }
                        else
                        {
                            ClientManagement_department_ModalpPopupExtender();
                            PositionProfileEntry_departmentControl.DepartmentDataSource = null;
                            PositionProfileEntry_departmentControl.ClientDataSource = new ClientBLManager().
                                GetClientInformationByClientId(int.Parse(PositionProfileEntry_clientIDHiddenField.Value.ToString()));
                        }
                        break;
                    case Constants.ClientManagementConstants.ADD_CONTACT:
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(PositionProfileEntry_clientIDHiddenField.Value))
                        {
                            base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                              PositionProfileEntry_bottomErrorMessageLabel,
                             "Please select client");
                        }
                        else
                        {
                            // PositionProfileEntry_contactModalpPopupExtender();
                            PositionProfileEntry_contactControl.clientContactInformationDataSource = null;
                            ClientManagement_contact_ModalpPopupExtender();
                        }
                        break;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        public void ClientManagement_client_ModalpPopupExtender()
        {
            try
            {
                PositionProfileEntry_clientControl.ClientAttributesDataSouce = new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENT);
                //ClientManagement_clientControl.ClientAttributesDataSouce = new ControlUtility().LoadTest();
                PositionProfileEntry_clientControl.BindSaveButtonCommand = Constants.ClientManagementConstants.ADD_CLIENT;
                PositionProfileEntry_clientModalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }
        public void ClientManagement_department_ModalpPopupExtender()
        {
            try
            {
                NomenclatureCustomize nomenclatureCustomize = new NomenclatureCustomize();
                nomenclatureCustomize = new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTDEPARTMENT);
                nomenclatureCustomize.ClientName = base.NomenclatureAllAttributes.CommonNomenclatureAttributes.ClientName;
                PositionProfileEntry_departmentControl.DepartmenAttributesDataSource = nomenclatureCustomize;
                PositionProfileEntry_departmentControl.ClientInformationListDatasource = new ClientBLManager().GetClientName(base.tenantID);
                //PositionProfileEntry_departmentModalPopUpUpdatePanel.Update();
                PositionProfileEntry_departmentControl.BindSaveButtonCommand = Constants.ClientManagementConstants.ADD_DEPARTMENT;
                PositionProfileEntry_departmentModalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        public void ClientManagement_department_ModalpPopupExtender1()
        {
            try
            {
                NomenclatureCustomize nomenclatureCustomize = new NomenclatureCustomize();
                nomenclatureCustomize = new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTDEPARTMENT);
                nomenclatureCustomize.ClientName = base.NomenclatureAllAttributes.CommonNomenclatureAttributes.ClientName;
                PositionProfileEntry_departmentControl.DepartmenAttributesDataSource = nomenclatureCustomize;
                PositionProfileEntry_departmentControl.BindSaveButtonCommand = Constants.ClientManagementConstants.ADD_DEPARTMENT;
                PositionProfileEntry_departmentModalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }
        public void ClientManagement_contact_ModalpPopupExtender()
        {
            try
            {
                NomenclatureCustomize nomenclatureCustomize = new NomenclatureCustomize();
                nomenclatureCustomize = new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTCONTACTS);
                nomenclatureCustomize.ClientName = base.NomenclatureAllAttributes.CommonNomenclatureAttributes.ClientName;
                PositionProfileEntry_contactControl.ContactAttributesDataSource = nomenclatureCustomize;
                PositionProfileEntry_contactControl.ClientInformationListDatasource = new ClientBLManager().GetClientName(base.tenantID);
                PositionProfileEntry_contactControl.ClientDataSource =
                            new ClientBLManager().GetClientInformationByClientId(int.Parse(PositionProfileEntry_clientIDHiddenField.Value.ToString()));
                PositionProfileEntry_contactControl.BindSaveButtonCommand = Constants.ClientManagementConstants.ADD_CONTACT;
                PositionProfileEntry_contactModalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        public void ClientManagement_contact_ModalpPopupExtender1()
        {
            try
            {
                NomenclatureCustomize nomenclatureCustomize = new NomenclatureCustomize();
                nomenclatureCustomize = new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTCONTACTS);
                nomenclatureCustomize.ClientName = base.NomenclatureAllAttributes.CommonNomenclatureAttributes.ClientName;
                PositionProfileEntry_contactControl.ContactAttributesDataSource = nomenclatureCustomize;
                PositionProfileEntry_contactControl.BindSaveButtonCommand = Constants.ClientManagementConstants.ADD_CONTACT;
                PositionProfileEntry_contactModalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        #endregion
        
        #region Print and Email                                                
        protected void PositionProfileEntry_emailButton_Click(object sender, EventArgs e)
        {
            AssignEmailContent();

        }

        private void AssignEmailContent()
        {
            CandidateReportDetail candidateReportDetail;
            string fileName;
            PDFHeader(out candidateReportDetail, out fileName);
            List<Dictionary<object, object>> dictinaryList = null;
            PreviewPositionProfile(out dictinaryList);
            StringBuilder stringBuilder = new StringBuilder();
            StringWriter SW = new StringWriter(stringBuilder);
            HtmlTextWriter htmlTW = new HtmlTextWriter(SW);
            PositionProfileEntry_PreviewPopupPanel.RenderControl(htmlTW);
            stringBuilder = new StringBuilder();
            stringBuilder.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
            stringBuilder.Append("<head>");
            stringBuilder.Append("<LINK href=\"" + ConfigurationManager.AppSettings["DEFAULT_URL"] + "/App_Themes/DefaultTheme/DefaultStyle.css\" rel=\"stylesheet\" type=\"text/css\">");
            stringBuilder.Append("<title>");
            stringBuilder.Append("Position Profile");
            stringBuilder.Append("</title>");
            stringBuilder.Append("</head>");
            stringBuilder.Append("<body>");
            stringBuilder.Append("<table style=\"width:100%; height:150px; background-color:white;overflow:auto\"><tr><td valign=\"top\">&nbsp;</td></tr></table>");
            stringBuilder.Append(htmlTW.InnerWriter);
            stringBuilder.Append("</body>");
            stringBuilder.Append("</html>");
            Session[Constants.SessionConstants.PP_PDF_HEADER] = candidateReportDetail;
            Session[Constants.SessionConstants.PP_EMAIL_CONTENT] = stringBuilder.ToString();
            Session[Constants.SessionConstants.PP_PDF_CONTENT] = dictinaryList;
            PositionProfileEntry_eMailContentConfirmPopupExtenderControl.Title = "Email Confirmation";
            PositionProfileEntry_eMailContentConfirmPopupExtenderControl.Message = "How do you want to email the position profile?";
            PositionProfileEntry_eMailContentConfirmPopupExtenderControl.Type = MessageBoxType.EmailConfirmType;
            PositionProfileEntry_emailContentConfirmModalPopupExtender.Show();
        }

        protected void PositionProfileEntry_downloadButton_Click(object sender, EventArgs e)
        {
            try
            {
                CandidateReportDetail candidateReportDetail;
                string fileName;
                PDFHeader(out candidateReportDetail, out fileName);
                List<Dictionary<object, object>> dictinaryList = null;
                PreviewPositionProfile(out dictinaryList);

                Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName.Replace(" ", "_") + ".pdf");
                Response.ContentType = "application/pdf";

                Response.BinaryWrite(new WidgetReport().GetPDFPPByteArray("POSITION PROFILE", dictinaryList, candidateReportDetail));
                Response.End();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileEntry_topErrorMessageLabel,
                   PositionProfileEntry_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        private void PDFHeader(out CandidateReportDetail candidateReportDetail, out string fileName)
        {
            candidateReportDetail = new CandidateReportDetail();
            // candidateReportDetail = GetSelectedItems(candidateReportDetail);
            fileName = PositionProfileEntry_positionProfileNameTextBox.Text.Trim().Length == 0 ? "PositionProfile" : PositionProfileEntry_positionProfileNameTextBox.Text.Trim();
            UserDetail userDetail = new UserDetail();
            userDetail = (UserDetail)Session["USER_DETAIL"];
            candidateReportDetail.TestName = fileName;
            candidateReportDetail.LoginUser = userDetail.FirstName + "," + userDetail.LastName;
        }


        public override void VerifyRenderingInServerForm(Control control)
        { /* Do nothing */ }

        void PositionProfileEntry_eMailContentConfirmPopupExtenderControl_CommandEventAdd_Click(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "attachment")
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(),
                    " ", "javascript:PositionProfileEmail('','" + PositionProfileEntry_editPositionProfileIdHiddenField.Value + "')", true);
            }
            else if (e.CommandName == "bodycontent")
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(),
                  " ", "javascript:PositionProfileEmail('pdf','" + PositionProfileEntry_editPositionProfileIdHiddenField.Value + "')", true);
            }
        }
        #endregion
    }
}