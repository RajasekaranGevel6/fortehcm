﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PositionProfileReview.aspx.cs"
    MasterPageFile="~/MasterPages/PositionProfileMaster.Master" Inherits="Forte.HCM.UI.PositionProfile.PositionProfileReview" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc8" %>
<%@ Register Src="~/CommonControls/PositionProfileWorkflowControl.ascx" TagName="PositionProfileWorkflowControl"
    TagPrefix="uc9" %>
<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content ID="PositionProfileReview_bodyContent" runat="server" ContentPlaceHolderID="PositionProfileMaster_body">
    <script language="javascript" type="text/javascript">

        $(document).ready(function ()
        {
            // Handler method for 'Change Status' link button click.
            $("#<%= PositionProfileReview_changeStatusLinkButton.ClientID %>").click(function ()
            {
                $("#PositionProfileReview_statusLabelDiv").hide();
                $("#PositionProfileReview_statusDropDownDiv").show();

                // Keep the selected status value in hidden fied.
                $("#<%= PositionProfileReview_currentPositionStatusHiddenField.ClientID %>").val($("#<%= PositionProfileReview_statusDropDownList.ClientID %>").val());

                return false;
            });

            // Handler method for 'Cancel' link button click (save status cancellation).
            $("#<%= PositionProfileReview_statusCancelLinkButton.ClientID %>").click(function ()
            {
                $("#PositionProfileReview_statusDropDownDiv").hide();
                $("#PositionProfileReview_statusLabelDiv").show();

                // Reset the selected drop down value with the actual value, if user
                // cancels the save process.
                if ($("#<%= PositionProfileReview_currentPositionStatusHiddenField.ClientID %>").val() != null &&
                    $("#<%= PositionProfileReview_currentPositionStatusHiddenField.ClientID %>").val() != '')
                {
                    $("#<%= PositionProfileReview_statusDropDownList.ClientID %>").val($("#<%= PositionProfileReview_currentPositionStatusHiddenField.ClientID %>").val());
                }

                return false;
            });

        });

    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="msg_align">
                <asp:Label ID="PositionProfileReview_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="PositionProfileReview_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                <asp:HiddenField ID="PositionProfileReview_positionProfileNameHiddenField" runat="server" />
                <asp:HiddenField ID="PositionProfileReview_clientNameHiddenField" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <uc9:PositionProfileWorkflowControl ID="PositionProfileReview_workflowControl" runat="server"
                    Visible="true" PageType="RW" />
            </td>
        </tr>
        <tr>
            <td style="height:5px">
            </td>
        </tr>
        <tr>
            <td class="position_profile_summary_section">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="left" style="padding-left: 2px; padding-top: 2px">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="PositionProfileReview_topRefreshButton" runat="server" Text="Refresh"
                                            SkinID="sknButtonId" OnClick="PositionProfileReview_refreshButton_Click" ToolTip="Refresh" />
                                    </td>
                                    <td>
                                        <asp:Button ID="PositionProfileReview_topSendMailButton" runat="server" Text="Send Mail"
                                            SkinID="sknButtonId" OnClick="PositionProfileReview_sendMailButton_Click" ToolTip="Send Mail" />
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="PositionProfileReview_topPDFDownloadImageButton" runat="server"
                                            SkinID="sknPdfImageButton" ToolTip="Download Position Profile" CommandName="download"
                                            OnClick="PositionProfileEntry_actionButton_Click" Visible="true" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right" style="padding-left: 2px; padding-top: 2px">
                            <table>
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="PositionProfileReview_topActivityImageButton" runat="server"
                                            SkinID="sknSkillViewPPActivityImageButton" ToolTip="Position Profile Activity"
                                            Visible="true" />
                                    </td>
                                    <td>
                                        <asp:HyperLink ID="PositionProfileReview_topDashboardHyperLink" runat="server" Target="_blank"
                                            ImageUrl="~/App_Themes/DefaultTheme/Images/icon_position_profile_dashboard.jpg"
                                            ToolTip="Position Profile Dashboard" />
                                    </td>
                                    <td style="padding-left: 4px">
                                        <asp:LinkButton ID="PositionProfileReview_topCancelLinkButton" runat="server" SkinID="sknActionBoldLinkButton"
                                            Text="Cancel" OnClick="ParentPageRedirect" ToolTip="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 8px" colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr class="position_profile_summary_status_background" style="height: 26px">
                                                <td style="width: 13%; padding-left: 4px">
                                                    <asp:Label ID="PositionProfileReview_statusLabel" runat="server" Text="Position Status"
                                                        CssClass="position_profile_summary_status_caption_cell"></asp:Label>
                                                </td>
                                                <td style="width: 85%">
                                                    <div id="PositionProfileReview_statusLabelDiv" style="width: auto; float: left; padding-right: 4px;
                                                        padding-left: 5px; display: block;">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="PositionProfileReview_statusValueLabel" runat="server" Text="" CssClass="position_profile_summary_status_value_cell"></asp:Label>
                                                                </td>
                                                                <td style="padding-left: 6px">
                                                                    <asp:LinkButton ID="PositionProfileReview_changeStatusLinkButton" runat="server"
                                                                        Visible="false" Text="Change Status" ToolTip="Click here to change position status"
                                                                        CssClass="position_profile_summary_profile_edit_link"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div id="PositionProfileReview_statusDropDownDiv" style="width: auto; float: left;
                                                        padding-right: 4px; padding-left: 5px; display: none;">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="PositionProfileReview_statusDropDownList" runat="server" SkinID="sknPositionProfileStatusDropDownList"
                                                                        Width="80px">
                                                                    </asp:DropDownList>
                                                                    <asp:HiddenField ID="PositionProfileReview_currentPositionStatusHiddenField" runat="server" />
                                                                </td>
                                                                <td style="padding-left: 6px">
                                                                    <asp:Button ID="PositionProfileReview_statusSaveButton" runat="server" Text="Save"
                                                                        SkinID="sknButtonId" OnClick="PositionProfileReview_statusSaveButton_Click" ToolTip="Click here to save position status" />
                                                                </td>
                                                                <td style="padding-left: 6px">
                                                                    <asp:LinkButton ID="PositionProfileReview_statusCancelLinkButton" runat="server"
                                                                        Text="Cancel" ToolTip="Click here to cancel saving position status" CssClass="position_profile_summary_profile_cancel_link"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:PlaceHolder ID="PositionProfileReview_summaryPlaceHolder" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 8px" colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="padding-left: 2px; padding-bottom: 2px">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="PositionProfileReview_bottomRefreshButton" runat="server" Text="Refresh"
                                            SkinID="sknButtonId" OnClick="PositionProfileReview_refreshButton_Click" ToolTip="Refresh" />
                                    </td>
                                    <td>
                                        <asp:Button ID="PositionProfileReview_bottomSendMailButton" runat="server" Text="Send Mail"
                                            SkinID="sknButtonId" OnClick="PositionProfileReview_sendMailButton_Click" ToolTip="Send Mail" />
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="PositionProfileReview_bottomPDFDownloadImageButton" runat="server"
                                            SkinID="sknPdfImageButton" ToolTip="Download Position Profile" CommandName="download"
                                            OnClick="PositionProfileEntry_actionButton_Click" Visible="true" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right" style="padding-left: 2px; padding-bottom: 2px">
                            <table>
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="PositionProfileReview_bottomActivityImageButton" runat="server"
                                            SkinID="sknSkillViewPPActivityImageButton" ToolTip="Position Profile Activity"
                                            Visible="true" />
                                    </td>
                                    <td>
                                        <asp:HyperLink ID="PositionProfileReview_bottomDashboardHyperLink" runat="server"
                                            Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_position_profile_dashboard.jpg"
                                            ToolTip="Position Profile Dashboard" />
                                    </td>
                                    <td style="padding-left: 4px">
                                        <asp:LinkButton ID="PositionProfileReview_bottomCancelLinkButton" runat="server"
                                            SkinID="sknActionBoldLinkButton" Text="Cancel" OnClick="ParentPageRedirect" ToolTip="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="PositionProfileReview_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="PositionProfileReview_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PositionProfileReview_emailTypePopupPanel" runat="server" Style="display: none"
                    CssClass="position_profile_summary_email_confirm_popup">
                    <uc8:ConfirmMsgControl ID="PositionProfileReview_emailTypeConfirmMsgControl" runat="server"
                        Type="EmailConfirmType" Title="" Message="" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="PositionProfileReview_emailTypeModalPopupExtender"
                    runat="server" PopupControlID="PositionProfileReview_emailTypePopupPanel" TargetControlID="PositionProfileReview_emailContentButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
            <td>
                <asp:Button ID="PositionProfileReview_emailContentButton" runat="server" Style="display: none" />
            </td>
        </tr>
    </table>
</asp:Content>
