﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PositionProfileMaster.Master"
    CodeBehind="PositionProfileWorkflowSelection.aspx.cs" Inherits="Forte.HCM.UI.PositionProfile.PositionProfileWorkflowSelection" %>

<%@ Register Src="~/CommonControls/PositionProfileWorkflowControl.ascx" TagName="PositionProfileWorkflowControl"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content ID="PositionProfileWorkflowSelection_bodyContent" runat="server" ContentPlaceHolderID="PositionProfileMaster_body">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="padding-bottom: 5px">
                <uc1:PositionProfileWorkflowControl ID="PositionProfileReview_workflowControl" runat="server"
                    Visible="true" PageType="WS" />
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="PositionProfileWorkflowSelection_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="PositionProfileWorkflowSelection_topSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="PositionProfileWorkflowSelection_topErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                        <asp:HiddenField ID="PositionProfileWorkflowSelection_positionProfileNameHiddenField" runat="server" />
                        <asp:HiddenField ID="PositionProfileWorkflowSelection_clientNameHiddenField" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" align="center" width="100%">
                                <tr>
                                    <td>
                                        <table class="pp_workflow_panel_bg" cellpadding="0" cellspacing="0" align="center"
                                            style="height: 60px;">
                                            <tr>
                                                <td valign="top">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td style="width: 56px">
                                                                            <asp:Image ID="PositionProfileWorkflowSelection_recruiterAssigmentImage" SkinID="sknPPRecruiterAssignmentImage"
                                                                                runat="server" />
                                                                        </td>
                                                                        <td style="width: 15px">
                                                                        </td>
                                                                        <td style="width: 500px">
                                                                            <asp:Label ID="PositionProfileWorkflowSelection_titleLabel" runat="server" SkinID="sknPPHeaderLabel"
                                                                                Text="Recruiter Assignment"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="PositionProfileWorkflowSelection_assignedRecruiterLabel" runat="server"
                                                                                SkinID="sknLabelText" Text="Assigned Recruiters"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 25px">
                                                                            <asp:UpdatePanel ID="PositionProfileWorkflowSelection_assignedRecruitersUpdatePanel"
                                                                                runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton ID="PositionProfileWorkflowSelection_assignedRecruiterLinkButton"
                                                                                        runat="server" ToolTip="View Recruiters" SkinID="sknActionLinkButton" OnClick="PositionProfileWorkflowSelection_assignedRecruiterLinkButton_Click"></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="PositionProfileWorkflowSelection_coownerUpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:ImageButton ID="PositionProfileWorkflowSelection_addRecruiterImageButton" runat="server"
                                                                                        SkinID="sknPPAddImageButton" ToolTip="Change/Assign Recruiters" OnClick="PositionProfileWorkflowSelection_addRecruiterImageButton_Click" />
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="6">
                                                                            <asp:Image ID="PositionProfileWorkflowSelection_recruiterAssignmentTopLineImage"
                                                                                runat="server" Height="1px" Width="100%" SkinID="sknPPLineImage" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_10">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <div style="height: 40px; overflow: auto; display: block;" runat="server">
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="PositionProfileWorkflowSelection_recruiterAssignmentUpdatePanel"
                                                                                    runat="server">
                                                                                    <ContentTemplate>
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td style="width: 100px" valign="top">
                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_taskOwnerLabel" runat="server" SkinID="sknLabelText"
                                                                                                        Text="Task Owner :"></asp:Label>
                                                                                                    <asp:ImageButton ID="PositionProfileWorkflowSelection_recruiterAssignmentAddTaskOwnerImageButton"
                                                                                                        OnClick="PositionProfileWorkflowSelection_recruiterAssignmentAddTaskOwnerImageButton_Click"
                                                                                                        SkinID="sknPPAddRecruiterImageButton" ToolTip="Select Task Owners" runat="server" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <div style="width: 100%; overflow: auto; height: 40px;" runat="server" id="PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataListDiv">
                                                                                                        <asp:DataList ID="PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataList"
                                                                                                            runat="server" RepeatColumns="3" RepeatDirection="Horizontal" 
                                                                                                            OnItemCommand="PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataList_ItemCommand">
                                                                                                            <ItemTemplate>
                                                                                                                <table cellpadding="1" cellspacing="1" border="0" align="center" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td style="width: 10px; text-align: left">
                                                                                                                            <asp:ImageButton ID="PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataList_deleteImageButton"
                                                                                                                                runat="server" SkinID="sknPPDeleteIconImageButton" CommandArgument='<%# Eval("UserID") %>'
                                                                                                                                CommandName="DeleteRecruiterAssignmentTaskOwner" Visible='<%# Eval("OwnerType") != null && Eval("OwnerType").ToString()!="POT_OWR" && Eval("OwnerType").ToString()!="POT_CO_OWR" %>'
                                                                                                                                ToolTip="Delete Task Owner" />
                                                                                                                        </td>
                                                                                                                        <td style="width: 150px; text-align: left">
                                                                                                                            <asp:Label ID="PositionProfileWorkflowSelection_recruiterTaskOnwerNameLabel" runat="server"
                                                                                                                                SkinID="sknLabelFieldText" Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'>
                                                                                                                            </asp:Label>
                                                                                                                            <asp:HiddenField ID="PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataList_firstNameHiddenField" runat="server" Value='<%# Eval("FirstName") %>'/>
                                                                                                                            <asp:HiddenField ID="PositionProfileWorkflowSelection_recruiterAssignmentTaskOwnerDataList_emailHiddenField" runat="server" Value='<%# Eval("Email") %>'/>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:DataList>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="PositionProfileWorkflowSelection_recruitersUpdatePanelDiv" runat="server">
                                                                    <ContentTemplate>
                                                                        <div id="PositionProfileWorkflowSelection_recruitersDiv" style="height: 60px; overflow: auto;
                                                                            display: none;" runat="server">
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td class="tab_body_bg">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div style="width: 100%; overflow: auto; height: 20px;" runat="server" id="PositionProfileWorkflowSelection_recruitersDataListDiv">
                                                                                                        <asp:DataList ID="PositionProfileWorkflowSelection_recruitersDataList" runat="server"
                                                                                                            RepeatColumns="4" OnItemDataBound="PositionProfileWorkflowSelection_recruitersDataList_ItemDataBound"
                                                                                                            RepeatDirection="Horizontal">
                                                                                                            <ItemTemplate>
                                                                                                                <table cellpadding="2" cellspacing="2" border="0" align="center" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td style="width: 150px; text-align: left">
                                                                                                                            <asp:LinkButton ID="PositionProfileWorkflowSelection_recruiterNameLinkButton" runat="server"
                                                                                                                                ToolTip="View Recruiter Profile" Target="_blank" SkinID="sknActionLinkButton"
                                                                                                                                Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'>
                                                                                                                            </asp:LinkButton>
                                                                                                                            <asp:HiddenField ID="PositionProfileWorkflowSelection_recruiterIDHiddenField" Value='<%#  Eval("UserID") %>'
                                                                                                                                runat="server" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:DataList>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_assignedRecruiterLinkButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Image ID="PositionProfileWorkflowSelection_recruiterAssignmentBottomLineImage"
                                                                    runat="server" Height="1px" Width="100%" SkinID="sknPPLineImage" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <asp:Image ID="PositionProfileWorkflowSelection_recruiterAssingedImage" SkinID="sknPPDownArrowImage"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <!-- Candidate Association  -->
                                <tr>
                                    <td>
                                        <table class="pp_workflow_panel_bg" cellpadding="0" cellspacing="0" align="center"
                                            style="height: 60px;">
                                            <tr>
                                                <td valign="top">
                                                    <table width="100%" cellpadding="4" cellspacing="4">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td style="width: 54px">
                                                                            <asp:Image ID="PositionProfileWorkflowSelection_candidateAssociationImage" SkinID="sknPPCandidateAssociationImage"
                                                                                runat="server" />
                                                                        </td>
                                                                        <td style="width: 15px">
                                                                        </td>
                                                                        <td align="left" style="width: 450px">
                                                                            <asp:Label ID="PositionProfileWorkflowSelection_candidateAssociationLabel" runat="server"
                                                                                SkinID="sknPPHeaderLabel" Text="Candidate Association"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 120px">
                                                                            <asp:Label ID="PositionProfileWorkflowSelection_noOfCandidatesLabel" runat="server"
                                                                                SkinID="sknLabelText" Text="Number Of Candidates"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 20px">
                                                                            <asp:UpdatePanel ID="PositionProfileWorkflowSelection_noOfCandidatesUpdatePanel"
                                                                                runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton ID="PositionProfileWorkflowSelection_noOfCandidatesLinkButton" runat="server"
                                                                                        SkinID="sknActionLinkButton" ToolTip="View Associated Candidates" OnClick="PositionProfileWorkflowSelection_noOfCandidatesLinkButton_Click"></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                        <td style="width: 75px" align="right">
                                                                            <asp:UpdatePanel ID="PositionProfileWorkflowSelection_candidateSearchUpdatePanel"
                                                                                runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:HyperLink ID="PositionProfileWorkflowSelection_talentSearchHyperLink" runat="server"
                                                                                        Target="_blank" ToolTip="Associate Candidates Through intelliSPOT" ImageUrl="~/App_Themes/DefaultTheme/Images/talent_search_icon.gif">
                                                                                    </asp:HyperLink>&nbsp;&nbsp;
                                                                                    <asp:HyperLink ID="PositionProfileWorkflowSelection_associateCandidateHyperLink"
                                                                                        runat="server" Target="_blank" ToolTip="Associate Candidates" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_pp_associate_candidate.gif">
                                                                                    </asp:HyperLink>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="6">
                                                                            <asp:Image ID="PositionProfileWorkflowSelection_associateCandidateTopLineImage" runat="server"
                                                                                Height="1px" Width="100%" SkinID="sknPPLineImage" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="PositionProfileWorkflowSelection_candidateAssociationTaskOwnerUpdaetPanel"
                                                                    runat="server">
                                                                    <ContentTemplate>
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td style="width: 100px" valign="top">
                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_candidateAssociationTaskOwnerLabel"
                                                                                        runat="server" Text="Task Owner :" SkinID="sknLabelText"></asp:Label>
                                                                                    <asp:ImageButton ID="PositionProfileWorkflowSelection_candidateAssociationAddTaskOwnerImageButton"
                                                                                        OnClick="PositionProfileWorkflowSelection_candidateAssociationAddTaskOwnerImageButton_Click"
                                                                                        SkinID="sknPPAddRecruiterImageButton" ToolTip="Select Task Owners" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <div style="width: 100%; overflow: auto; height: 40px;" runat="server" id="PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataListDiv">
                                                                                        <asp:DataList ID="PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataList"
                                                                                            runat="server" RepeatColumns="3" RepeatDirection="Horizontal" OnItemCommand="PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataList_ItemCommand">
                                                                                            <ItemTemplate>
                                                                                                <table cellpadding="1" cellspacing="1" border="0" align="center" width="100%">
                                                                                                    <tr>
                                                                                                        <td style="width: 10px; text-align: left">
                                                                                                            <asp:ImageButton ID="PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataList_deleteImageButton"
                                                                                                                runat="server" SkinID="sknPPDeleteIconImageButton" CommandArgument='<%# Eval("UserID") %>'
                                                                                                                CommandName="DeleteCandidateAssociationTaskOwner" Visible='<%# Eval("OwnerType") != null && Eval("OwnerType").ToString()!="POT_OWR" && Eval("OwnerType").ToString()!="POT_CO_OWR" && Eval("OwnerType").ToString()!="POT_CAN_RE" %>'
                                                                                                                ToolTip="Delete Task Owner" />
                                                                                                        </td>
                                                                                                        <td style="width: 150px; text-align: left">
                                                                                                            <asp:Label ID="PositionProfileWorkflowSelection_recruiterTaskOnwerNameLabel" runat="server"
                                                                                                                SkinID="sknLabelFieldText" Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'>
                                                                                                            </asp:Label>
                                                                                                            <asp:HiddenField ID="PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataList_firstNameHiddenField" runat="server" Value='<%# Eval("FirstName") %>'/>
                                                                                                            <asp:HiddenField ID="PositionProfileWorkflowSelection_candidateAssociationTaskOwnerDataList_emailHiddenField" runat="server" Value='<%# Eval("Email") %>'/>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </ItemTemplate>
                                                                                        </asp:DataList>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <asp:UpdatePanel ID="PositionProfileWorkflowSelection_candidateDivUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <div id="PositionProfileWorkflowSelection_candidateDiv" style="height: 60px; overflow: auto;
                                                                            display: none;" runat="server">
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td class="tab_body_bg">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div style="width: 100%; overflow: auto; height: 40px;" runat="server" id="PositionProfileWorkflowSelection_candidateDataListDiv">
                                                                                                        <asp:DataList ID="PositionProfileWorkflowSelection_candidateDataList" runat="server"
                                                                                                            RepeatColumns="4" RepeatDirection="Horizontal" OnItemDataBound="PositionProfileWorkflowSelection_candidateDataList_ItemDataBound">
                                                                                                            <ItemTemplate>
                                                                                                                <table cellpadding="2" cellspacing="2" border="0" align="center" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td style="width: 200px; text-align: left">
                                                                                                                            <asp:HyperLink ID="PositionProfileWorkflowSelection_candidateNameHyperLink" runat="server"
                                                                                                                                CommandName="ViewCandidateActivityLog" ToolTip="Candidate Activity Dashboard"
                                                                                                                                Target="_blank" SkinID="sknLabelFieldTextHyperLink" Text='<%# Eval("caiLastName")==null ? Eval("caiFirstName") : String.Format("{0} {1}", Eval("caiFirstName"), Eval("caiLastName")) %>'>
                                                                                                                            </asp:HyperLink>
                                                                                                                            <asp:HiddenField ID="PositionProfileWorkflowSelection_candidateIDHiddenField" Value='<%#  Eval("caiID") %>'
                                                                                                                                runat="server" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:DataList>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_noOfCandidatesLinkButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Image ID="PositionProfileWorkflowSelection_associateCandidateBottomLineImage"
                                                                    runat="server" Height="1px" Width="100%" SkinID="sknPPLineImage" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <asp:Image ID="PositionProfileWorkflowSelection_associateCandidateDownArrowImage"
                                                        SkinID="sknPPDownArrowImage" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_10">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <!-- Right Side [Interview Section -->
                                                <td align="right" valign="top">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%" class="pp_workflow_panel_bg_small">
                                                                    <tr>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                                                                                <tr>
                                                                                    <td class="tab_body_bg">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td align="left">
                                                                                                                <asp:Image ID="PositionProfileWorkflowSelection_interviewCreationImage" SkinID="sknPPInterviewCreationImage"
                                                                                                                    runat="server" />
                                                                                                            </td>
                                                                                                            <td style="width: 15px">
                                                                                                            </td>
                                                                                                            <td align="left" style="width: 400px">
                                                                                                                <asp:Label ID="PositionProfileWorkflowSelection_interviewCreationTitleLabel" runat="server"
                                                                                                                    SkinID="sknPPHeaderLabel" Text="Interview Creation"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:UpdatePanel ID="PositionProfileWorkflowSelection_interviewUpdatePanel" runat="server">
                                                                                                                    <ContentTemplate>
                                                                                                                        <asp:ImageButton ID="PositionProfileWorkflowSelection_interviewActiveImageButton"
                                                                                                                            runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/pp_deactive.png" OnClick="PositionProfileWorkflowSelection_interviewActiveImageButton_Click" />
                                                                                                                    </ContentTemplate>
                                                                                                                </asp:UpdatePanel>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="4" align="right">
                                                                                                                <asp:ImageButton ID="PositionProfileWorkflowSelection_interviewCreationAddTaskOwnerImageButton"
                                                                                                                    OnClick="PositionProfileWorkflowSelection_interviewCreationAddTaskOwnerImageButton_Click"
                                                                                                                    SkinID="sknPPAddRecruiterImageButton" ToolTip="Select Task Owners" runat="server" />
                                                                                                                <asp:Label ID="PositionProfileWorkflowSelection_interviewCreationTaskOwnerLabel"
                                                                                                                    runat="server" SkinID="sknLabelText" Text="Task Owner"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="4">
                                                                                                                <asp:Image ID="PositionProfileWorkflowSelection_creatInterviewLineImage" runat="server"
                                                                                                                    Height="1px" Width="100%" SkinID="sknPPLineImage" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left">
                                                                                                    <asp:UpdatePanel ID="PositionProfileWorkflowSelection_interviewCreationUpdatePanel"
                                                                                                        runat="server">
                                                                                                        <ContentTemplate>
                                                                                                            <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                <tr>
                                                                                                                    <td align="left">
                                                                                                                        <asp:DataList ID="PositionProfileWorkflowSelection_interviewCreationTaskOwnerDataList"
                                                                                                                            runat="server" RepeatColumns="2" RepeatDirection="Horizontal" OnItemCommand="PositionProfileWorkflowSelection_interviewCreationTaskOwnerDataList_ItemCommand">
                                                                                                                            <ItemTemplate>
                                                                                                                                <table cellpadding="1" cellspacing="1" border="0" align="center" width="100%">
                                                                                                                                    <tr>
                                                                                                                                        <td style="width: 10px; text-align: left">
                                                                                                                                            <asp:ImageButton ID="PositionProfileWorkflowSelection_interviewCreationTaskOwnerDataList_deleteImageButton"
                                                                                                                                                runat="server" SkinID="sknPPDeleteIconImageButton" CommandArgument='<%# Eval("UserID") %>'
                                                                                                                                                CommandName="DeleteInterviewCreationTaskOwner" Visible='<%# Eval("OwnerType") != null && Eval("OwnerType").ToString()!="POT_OWR" && Eval("OwnerType").ToString()!="POT_CO_OWR" %>'
                                                                                                                                                ToolTip="Delete Task Owner" />
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 150px; text-align: left">
                                                                                                                                            <asp:Label ID="PositionProfileWorkflowSelection_interviewCreationTaskOnwerNameLabel"
                                                                                                                                                runat="server" SkinID="sknLabelFieldText" Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'>
                                                                                                                                            </asp:Label>
                                                                                                                                            <asp:HiddenField ID="PositionProfileWorkflowSelection_interviewCreationTaskOwnerDataList_firstNameHiddenField" runat="server" Value='<%# Eval("FirstName") %>'/>
                                                                                                                                            <asp:HiddenField ID="PositionProfileWorkflowSelection_interviewCreationTaskOwnerDataList_emailHiddenField" runat="server" Value='<%# Eval("Email") %>'/>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:DataList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ContentTemplate>
                                                                                                        <Triggers>
                                                                                                            <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_interviewCreationAddTaskOwnerImageButton" />
                                                                                                        </Triggers>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" style="padding-left: 2px">
                                                                                                    <asp:DataList ID="PositionProfileWorkflowSelection_interviewCreationDataList" runat="server"
                                                                                                        CellSpacing="5" CellPadding="0" RepeatColumns="1" RepeatDirection="Vertical"
                                                                                                        Width="100px" OnItemDataBound="PositionProfileWorkflowSelection_interviewCreationDataList_ItemDataBound">
                                                                                                        <ItemTemplate>
                                                                                                            <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
                                                                                                                <tr>
                                                                                                                    <td align="left">
                                                                                                                        <asp:HyperLink ID="PositionProfileWorkflowSelection_interviewNameHyperLink" runat="server"
                                                                                                                            ToolTip="View Interview" Target="_blank" SkinID="sknLabelFieldTextHyperLink"
                                                                                                                            Text='<%# Eval("InterviewName") %>'>
                                                                                                                        </asp:HyperLink>
                                                                                                                        <asp:HiddenField ID="PositionProfileStatus_interviewKeyHiddenField" runat="server"
                                                                                                                            Value='<%# Eval("InterviewTestKey") %>' />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle Wrap="true" />
                                                                                                    </asp:DataList>
                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_interviewCreationErrorLabel" CssClass="pp_error_message_text"
                                                                                                        runat="server"></asp:Label>&nbsp;
                                                                                                    <asp:HyperLink ID="PositionProfileWorkflowSelection_createInterviewHyperLink" runat="server"
                                                                                                        ToolTip="Create Interview" Target="_blank" SkinID="sknLabelFieldTextHyperLink"
                                                                                                        Text="Create Interview">
                                                                                                    </asp:HyperLink>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left">
                                                                                        <asp:UpdatePanel ID="PositionProfileWorkflowSelection_interviewRequiredSectionUpdatePanel"
                                                                                            runat="server">
                                                                                            <ContentTemplate>
                                                                                                <div style="display: block;" runat="server" id="PositionProfileWorkflowSelection_showInterviewRequiredSectionDiv">
                                                                                                    <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                        <tr>
                                                                                                            <td align="center">
                                                                                                                <asp:Image ID="PositionProfileWorkflowSelection_interviewCreationDownArrowImage"
                                                                                                                    SkinID="sknPPDownArrowImage" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="tab_body_bg">
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <asp:Image ID="PositionProfileWorkflowSelection_interviewSessionCreationImage" SkinID="sknPPInterviewSessionCreationImage"
                                                                                                                                                        runat="server" />
                                                                                                                                                </td>
                                                                                                                                                <td style="width: 15px">
                                                                                                                                                </td>
                                                                                                                                                <td align="left" style="width: 400px">
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_interviewSessionCreationTitleLabel"
                                                                                                                                                        runat="server" SkinID="sknPPHeaderLabel" Text="Interview Session Creation"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td colspan="3" align="right">
                                                                                                                                                    <asp:ImageButton ID="PositionProfileWorkflowSelection_interviewSessionCreationAddTaskOwnerImageButton"
                                                                                                                                                        OnClick="PositionProfileWorkflowSelection_interviewSessionCreationAddTaskOwnerImageButton_Click"
                                                                                                                                                        SkinID="sknPPAddRecruiterImageButton" ToolTip="Select Task Owners" runat="server" />
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerLabel"
                                                                                                                                                        runat="server" SkinID="sknLabelText" Text="Task Owner"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td colspan="3">
                                                                                                                                                    <asp:Image ID="PositionProfileWorkflowSelection_interviewSessionCreationDownArrowImage"
                                                                                                                                                        runat="server" Height="1px" Width="100%" SkinID="sknPPLineImage" />
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <asp:UpdatePanel ID="PositionProfileWorkflowSelection_interviewSessionCreationUpdatePanel"
                                                                                                                                            runat="server">
                                                                                                                                            <ContentTemplate>
                                                                                                                                                <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                                                    <tr>
                                                                                                                                                        <td align="left">
                                                                                                                                                            <asp:DataList ID="PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerDataList"
                                                                                                                                                                runat="server" RepeatColumns="2" RepeatDirection="Horizontal" OnItemCommand="PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerDataList_ItemCommand">
                                                                                                                                                                <ItemTemplate>
                                                                                                                                                                    <table cellpadding="1" cellspacing="1" border="0" align="center" width="100%">
                                                                                                                                                                        <tr>
                                                                                                                                                                            <td style="width: 10px; text-align: left">
                                                                                                                                                                                <asp:ImageButton ID="PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerDataList_deleteImageButton"
                                                                                                                                                                                    runat="server" SkinID="sknPPDeleteIconImageButton" CommandArgument='<%# Eval("UserID") %>'
                                                                                                                                                                                    CommandName="DeleteInterviewSessionCreationTaskOwner" Visible='<%# Eval("OwnerType") != null && Eval("OwnerType").ToString()!="POT_OWR" && Eval("OwnerType").ToString()!="POT_CO_OWR" %>'
                                                                                                                                                                                    ToolTip="Delete Task Owner" />
                                                                                                                                                                            </td>
                                                                                                                                                                            <td style="width: 150px; text-align: left">
                                                                                                                                                                                <asp:Label ID="PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerNameLabel"
                                                                                                                                                                                    runat="server" SkinID="sknLabelFieldText" Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'>
                                                                                                                                                                                </asp:Label>
                                                                                                                                                                                <asp:HiddenField ID="PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerDataList_firstNameHiddenField" runat="server" Value='<%# Eval("FirstName") %>'/>
                                                                                                                                                                                <asp:HiddenField ID="PositionProfileWorkflowSelection_interviewSessionCreationTaskOwnerDataList_emailHiddenField" runat="server" Value='<%# Eval("Email") %>'/>
                                                                                                                                                                            </td>
                                                                                                                                                                        </tr>
                                                                                                                                                                    </table>
                                                                                                                                                                </ItemTemplate>
                                                                                                                                                            </asp:DataList>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </table>
                                                                                                                                            </ContentTemplate>
                                                                                                                                            <Triggers>
                                                                                                                                                <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_interviewSessionCreationAddTaskOwnerImageButton" />
                                                                                                                                            </Triggers>
                                                                                                                                        </asp:UpdatePanel>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td align="left">
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_noOfInterviewSessionLabel" SkinID="sknLabelText"
                                                                                                                                                        Text="No.of Sessions" runat="server"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td align="left">
                                                                                                                                                    <asp:DataList ID="PositionProfileWorkflowSelection_interviewSessionDataList" runat="server"
                                                                                                                                                        Width="100%" CellSpacing="0" CellPadding="0" RepeatColumns="1" RepeatDirection="Vertical"
                                                                                                                                                        OnItemDataBound="PositionProfileWorkflowSelection_interviewSessionDataList_ItemDataBound">
                                                                                                                                                        <ItemTemplate>
                                                                                                                                                            <table cellpadding="0" cellspacing="0" border="0" align="left">
                                                                                                                                                                <tr>
                                                                                                                                                                    <td>
                                                                                                                                                                        <asp:HyperLink ID="PositionProfileWorkflowSelection_interviewSessionHyperLink" runat="server"
                                                                                                                                                                            CommandName="Search Interviw" ToolTip="View Interview Session" Target="_blank"
                                                                                                                                                                            SkinID="sknLabelFieldTextHyperLink" Text='<%# String.Format("{0}({1})", Eval("InterviewTestSessionID"), Eval("InterviewSessionCount")) %>'>
                                                                                                                                                                        </asp:HyperLink>
                                                                                                                                                                        <asp:HiddenField ID="PositionProfileWorkflowSelection_interviewSessionInterviewKeyHiddenField"
                                                                                                                                                                            runat="server" Value='<%# Eval("InterviewTestSessionID") %>' />
                                                                                                                                                                    </td>
                                                                                                                                                                </tr>
                                                                                                                                                            </table>
                                                                                                                                                        </ItemTemplate>
                                                                                                                                                        <ItemStyle Width="20px" BorderStyle="None" Wrap="true" />
                                                                                                                                                    </asp:DataList>
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_interviewSessionErrorLabel" CssClass="pp_error_message_text"
                                                                                                                                                        runat="server"></asp:Label>
                                                                                                                                                    <asp:HyperLink ID="PositionProfileWorkflowSelection_createInterviewSessionHyperLink"
                                                                                                                                                        runat="server" Target="_blank" SkinID="sknLabelFieldTextHyperLink" Text="Create Interview Session"
                                                                                                                                                        ToolTip="Create Interview Session">
                                                                                                                                                    </asp:HyperLink>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center">
                                                                                                                <asp:Image ID="PositionProfileWorkflowSelection_interviewSessionDownArrowImage" SkinID="sknPPDownArrowImage"
                                                                                                                    runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="tab_body_bg">
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <asp:Image ID="PositionProfileWorkflowSelection_interviewScheduleCreationImage" SkinID="sknPPInterviewScheduleImage"
                                                                                                                                                        runat="server" />
                                                                                                                                                </td>
                                                                                                                                                <td style="width: 15px">
                                                                                                                                                </td>
                                                                                                                                                <td align="left" style="width: 400px">
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_interviewScheduleCreationTitleLabel"
                                                                                                                                                        runat="server" SkinID="sknPPHeaderLabel" Text="Interview Scheduling"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td colspan="3" align="right">
                                                                                                                                                    <asp:ImageButton ID="PositionProfileWorkflowSelection_interviewScheduleCreationAddTaskOwnerImageButton"
                                                                                                                                                        OnClick="PositionProfileWorkflowSelection_interviewScheduleCreationAddTaskOwnerImageButton_Click"
                                                                                                                                                        SkinID="sknPPAddRecruiterImageButton" ToolTip="Select Task Owners" runat="server" />
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerLabel"
                                                                                                                                                        runat="server" SkinID="sknLabelText" Text="Task Owner"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td colspan="3">
                                                                                                                                                    <asp:Image ID="PositionProfileWorkflowSelection_interviewScheduleLineImage" runat="server"
                                                                                                                                                        Height="1px" Width="100%" SkinID="sknPPLineImage" />
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <asp:UpdatePanel ID="PositionProfileWorkflowSelection_interviewScheduleCreationUpdatePanel"
                                                                                                                                            runat="server">
                                                                                                                                            <ContentTemplate>
                                                                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                                    <tr>
                                                                                                                                                        <td align="left">
                                                                                                                                                            <asp:DataList ID="PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerDataList"
                                                                                                                                                                runat="server" RepeatColumns="2" RepeatDirection="Horizontal" OnItemCommand="PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerDataList_ItemCommand">
                                                                                                                                                                <ItemTemplate>
                                                                                                                                                                    <table cellpadding="1" cellspacing="1" border="0" align="center" width="100%">
                                                                                                                                                                        <tr>
                                                                                                                                                                            <td style="width: 10px; text-align: left">
                                                                                                                                                                                <asp:ImageButton ID="PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerDataList_deleteImageButton"
                                                                                                                                                                                    runat="server" SkinID="sknPPDeleteIconImageButton" CommandArgument='<%# Eval("UserID") %>'
                                                                                                                                                                                    CommandName="DeleteInterviewScheduleCreationTaskOwner" Visible='<%# Eval("OwnerType") != null && Eval("OwnerType").ToString()!="POT_OWR" && Eval("OwnerType").ToString()!="POT_CO_OWR" %>'
                                                                                                                                                                                    ToolTip="Delete Task Owner" />
                                                                                                                                                                            </td>
                                                                                                                                                                            <td style="width: 150px; text-align: left">
                                                                                                                                                                                <asp:Label ID="PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerNameLabel"
                                                                                                                                                                                    runat="server" SkinID="sknLabelFieldText" Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'>
                                                                                                                                                                                </asp:Label>
                                                                                                                                                                                <asp:HiddenField ID="PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerDataList_firstNameHiddenField" runat="server" Value='<%# Eval("FirstName") %>'/>
                                                                                                                                                                                <asp:HiddenField ID="PositionProfileWorkflowSelection_interviewScheduleCreationTaskOwnerDataList_emailHiddenField" runat="server" Value='<%# Eval("Email") %>'/>
                                                                                                                                                                            </td>
                                                                                                                                                                        </tr>
                                                                                                                                                                    </table>
                                                                                                                                                                </ItemTemplate>
                                                                                                                                                            </asp:DataList>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                    <tr>
                                                                                                                                                        <td colspan="2">
                                                                                                                                                            <asp:Label ID="PositionProfileWorkflowSelection_interviewScheduleErrorLabel" CssClass="pp_error_message_text"
                                                                                                                                                                runat="server"></asp:Label>
                                                                                                                                                            <asp:HyperLink ID="PositionProfileWorkflowSelection_createIterviewScheduleHyperLink"
                                                                                                                                                                runat="server" ToolTip="Schedule Candidate" Target="_blank" SkinID="sknLabelFieldTextHyperLink"
                                                                                                                                                                Text="Schedule Candidate">
                                                                                                                                                            </asp:HyperLink>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </table>
                                                                                                                                            </ContentTemplate>
                                                                                                                                            <Triggers>
                                                                                                                                                <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_interviewScheduleCreationAddTaskOwnerImageButton" />
                                                                                                                                            </Triggers>
                                                                                                                                        </asp:UpdatePanel>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <div id="PositionProfileWorkflowSelection_interviewScheduleTitleDiv" runat="server">
                                                                                                                                                        <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="width: 100px">
                                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_interviewScheduledTitleLabel" runat="server"
                                                                                                                                                                        Text="Scheduled" SkinID="sknHomePageLabel"></asp:Label>
                                                                                                                                                                </td>
                                                                                                                                                                <td valign="top" align="left">
                                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_interviewCompletedTitleLabel" runat="server"
                                                                                                                                                                        Text="Completed" SkinID="sknHomePageLabel"></asp:Label>
                                                                                                                                                                </td>
                                                                                                                                                            </tr>
                                                                                                                                                        </table>
                                                                                                                                                    </div>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <asp:DataList ID="PositionProfileWorkflowSelection_interviewScheduledDataList" runat="server"
                                                                                                                                                        Width="100%" CellSpacing="0" CellPadding="0" RepeatColumns="1" RepeatDirection="Vertical"
                                                                                                                                                        OnItemDataBound="PositionProfileWorkflowSelection_interviewScheduledDataList_ItemDataBound">
                                                                                                                                                        <ItemTemplate>
                                                                                                                                                            <table cellpadding="2" cellspacing="2" border="0" align="left">
                                                                                                                                                                <tr>
                                                                                                                                                                    <td style="width: 102px">
                                                                                                                                                                        <asp:HyperLink ID="PositionProfileWorkflowSelection_interviewScheduledHyperLink"
                                                                                                                                                                            runat="server" CommandName="InterviewScheduled" ToolTip="View Interview Scheduled/Schedule Candidate"
                                                                                                                                                                            Target="_blank" SkinID="sknLabelFieldTextHyperLink" Text='<%# String.Format("{0}({1})", Eval("ScheduledInterviews"), Eval("InterviewSessionCount")) %>'>
                                                                                                                                                                        </asp:HyperLink>
                                                                                                                                                                        <asp:HiddenField ID="PositionProfileWorkflowSelection_interviewSessionKeyHiddenField"
                                                                                                                                                                            runat="server" Value='<%# Eval("InterviewTestSessionID") %>' />
                                                                                                                                                                    </td>
                                                                                                                                                                    <td>
                                                                                                                                                                        <asp:HyperLink ID="PositionProfileWorkflowSelection_interviewCompletedHyperLink"
                                                                                                                                                                            runat="server" CommandName="InterviewCompleted" ToolTip="View Interview Completed"
                                                                                                                                                                            Target="_blank" SkinID="sknLabelFieldTextHyperLink" Text='<%# String.Format("{0}({1})", Eval("CompletedInterviews"), Eval("InterviewSessionCount")) %>'>
                                                                                                                                                                        </asp:HyperLink>
                                                                                                                                                                    </td>
                                                                                                                                                                </tr>
                                                                                                                                                            </table>
                                                                                                                                                        </ItemTemplate>
                                                                                                                                                        <ItemStyle Width="40px" BorderStyle="None" Wrap="true" />
                                                                                                                                                    </asp:DataList>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center">
                                                                                                                <asp:Image ID="PositionProfileWorkflowSelection_interviewCompletedDownArrowImage"
                                                                                                                    SkinID="sknPPDownArrowImage" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="tab_body_bg">
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <asp:Image ID="PositionProfileWorkflowSelection_interviewAssessorCreationImage" SkinID="sknPPInterviewAssessmentImage"
                                                                                                                                                        runat="server" />
                                                                                                                                                </td>
                                                                                                                                                <td style="width: 15px">
                                                                                                                                                </td>
                                                                                                                                                <td align="left" style="width: 400px">
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_interviewAssessorCreationTitleLabel"
                                                                                                                                                        runat="server" SkinID="sknPPHeaderLabel" Text="Interview Assessment"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td colspan="3" align="right">
                                                                                                                                                    <asp:ImageButton ID="PositionProfileWorkflowSelection_interviewAssessorCreationAddTaskOwnerImageButton"
                                                                                                                                                        OnClick="PositionProfileWorkflowSelection_interviewAssessorCreationAddTaskOwnerImageButton_Click"
                                                                                                                                                        SkinID="sknPPAddRecruiterImageButton" ToolTip="Select Task Owners" runat="server" />
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_interviewAssessorAddTaskOwnerLabel"
                                                                                                                                                        runat="server" SkinID="sknLabelText" Text="Task Owner"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td colspan="3">
                                                                                                                                                    <asp:Image ID="PositionProfileWorkflowSelection_interviewAssessorLineImage" runat="server"
                                                                                                                                                        Height="1px" Width="100%" SkinID="sknPPLineImage" />
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <asp:UpdatePanel ID="PositionProfileWorkflowSelection_interviewAssessorCreationUpdatePanel"
                                                                                                                                            runat="server">
                                                                                                                                            <ContentTemplate>
                                                                                                                                                <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                                                    <tr>
                                                                                                                                                        <td align="left">
                                                                                                                                                            <asp:DataList ID="PositionProfileWorkflowSelection_interviewAssessorTaskOwnerDataList"
                                                                                                                                                                runat="server" RepeatColumns="2" RepeatDirection="Horizontal" OnItemCommand="PositionProfileWorkflowSelection_interviewAssessorTaskOwnerDataList_ItemCommand">
                                                                                                                                                                <ItemTemplate>
                                                                                                                                                                    <table cellpadding="1" cellspacing="1" border="0" align="center" width="100%">
                                                                                                                                                                        <tr>
                                                                                                                                                                            <td style="width: 10px; text-align: left">
                                                                                                                                                                                <asp:ImageButton ID="PositionProfileWorkflowSelection_interviewAssessorTaskOwnerDataList_deleteImageButton"
                                                                                                                                                                                    runat="server" SkinID="sknPPDeleteIconImageButton" CommandArgument='<%# Eval("UserID") %>'
                                                                                                                                                                                    CommandName="DeleteInterviewAssessorCreationTaskOwner" Visible='<%# Eval("OwnerType") != null && Eval("OwnerType").ToString()!="POT_OWR" && Eval("OwnerType").ToString()!="POT_CO_OWR" %>'
                                                                                                                                                                                    ToolTip="Delete Task Owner" />
                                                                                                                                                                            </td>
                                                                                                                                                                            <td style="width: 150px; text-align: left">
                                                                                                                                                                                <asp:Label ID="PositionProfileWorkflowSelection_interviewAssessorTaskOwnerNameLabel"
                                                                                                                                                                                    runat="server" SkinID="sknLabelFieldText" Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'>
                                                                                                                                                                                </asp:Label>
                                                                                                                                                                                <asp:HiddenField ID="PositionProfileWorkflowSelection_interviewAssessorTaskOwnerDataList_firstNameHiddenField" runat="server" Value='<%# Eval("FirstName") %>'/>
                                                                                                                                                                                <asp:HiddenField ID="PositionProfileWorkflowSelection_interviewAssessorTaskOwnerDataList_emailHiddenField" runat="server" Value='<%# Eval("Email") %>'/>
                                                                                                                                                                            </td>
                                                                                                                                                                        </tr>
                                                                                                                                                                    </table>
                                                                                                                                                                </ItemTemplate>
                                                                                                                                                            </asp:DataList>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </table>
                                                                                                                                            </ContentTemplate>
                                                                                                                                            <Triggers>
                                                                                                                                                <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_interviewAssessorCreationAddTaskOwnerImageButton" />
                                                                                                                                            </Triggers>
                                                                                                                                        </asp:UpdatePanel>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td align="left">
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_assessedCandidateLabel" SkinID="sknLabelText"
                                                                                                                                                        Text="Assessed Candidates" runat="server"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                <asp:DataList ID="PositionProfileWorkflowSelection_interviewAssessedDataList" runat="server"
                                                                                                                                                        Width="100%" CellSpacing="0" CellPadding="0" RepeatColumns="1" RepeatDirection="Vertical"
                                                                                                                                                        OnItemDataBound="PositionProfileWorkflowSelection_interviewAssessedDataList_ItemDataBound">
                                                                                                                                                        <ItemTemplate>
                                                                                                                                                            <table cellpadding="2" cellspacing="2" border="0" align="left">
                                                                                                                                                                <tr>
                                                                                                                                                                    <td style="width: 102px">
                                                                                                                                                                        <asp:LinkButton ID="PositionProfileWorkflowSelection_assessedLinkButton"
                                                                                                                                                                            runat="server" CommandName="InterviewAssessed" ToolTip="View Assessed Candidates"
                                                                                                                                                                            SkinID="sknActionLinkButton"  Text='<%# String.Format("{0}({1})", Eval("InterviewTestSessionID"), Eval("AssessedCandidates")) %>'>
                                                                                                                                                                        </asp:LinkButton>
                                                                                                                                                                        <asp:HiddenField ID="PositionProfileWorkflowSelection_assessedSessionKeyHiddenField"
                                                                                                                                                                            runat="server" Value='<%# Eval("InterviewTestSessionID") %>' />
                                                                                                                                                                    </td>
                                                                                                                                                                </tr>
                                                                                                                                                            </table>
                                                                                                                                                        </ItemTemplate>
                                                                                                                                                        <ItemStyle Width="40px" BorderStyle="None" Wrap="true" />
                                                                                                                                                    </asp:DataList>
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_assessedCandidateErrorLabel" CssClass="pp_error_message_text" runat="server"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_interviewActiveImageButton" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:UpdatePanel ID="PositionProfileWorkflowSelection_showInterviewRequiredSectionDownArrowUpdatePanel"
                                                                    runat="server">
                                                                    <ContentTemplate>
                                                                        <div style="display: block" runat="server" id="PositionProfileWorkflowSelection_showInterviewRequiredSectionDownArrowDiv">
                                                                            <asp:Image ID="PositionProfileWorkflowSelection_interviewDownArrowImage" SkinID="sknPPDownArrowImage"
                                                                                runat="server" />
                                                                        </div>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_interviewActiveImageButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width: 10px">
                                                </td>
                                                <!-- Left Side [Test Section] -->
                                                <td valign="top">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%" class="pp_workflow_panel_bg_small">
                                                                    <tr>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                                                                                <tr>
                                                                                    <td class="tab_body_bg">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td style="width: 56px">
                                                                                                                <asp:Image ID="PositionProfileWorkflowSelection_testCreationImage" SkinID="sknPPTestCreationImage"
                                                                                                                    runat="server" />
                                                                                                            </td>
                                                                                                            <td style="width: 15px">
                                                                                                            </td>
                                                                                                            <td align="left" style="width: 400px">
                                                                                                                <asp:Label ID="PositionProfileWorkflowSelection_testCreationTitleLabel" runat="server"
                                                                                                                    SkinID="sknPPHeaderLabel" Text="Test Creation"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:UpdatePanel ID="PositionProfileWorkflowSelection_testUpdatePanel" runat="server">
                                                                                                                    <ContentTemplate>
                                                                                                                        <asp:ImageButton ID="PositionProfileWorkflowSelection_testActiveImageButton" runat="server"
                                                                                                                            ImageUrl="~/App_Themes/DefaultTheme/Images/pp_deactive.png" OnClick="PositionProfileWorkflowSelection_testActiveImageButton_Click" />
                                                                                                                    </ContentTemplate>
                                                                                                                </asp:UpdatePanel>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right" colspan="4">
                                                                                                                <asp:ImageButton ID="PositionProfileWorkflowSelection_testCreationAddTaskOwnerImageButton"
                                                                                                                    OnClick="PositionProfileWorkflowSelection_testCreationAddTaskOwnerImageButton_Click"
                                                                                                                    SkinID="sknPPAddRecruiterImageButton" ToolTip="Select Task Owners" runat="server" />
                                                                                                                <asp:Label ID="PositionProfileWorkflowSelection_testCreationTaskOwnerLabel" runat="server"
                                                                                                                    SkinID="sknLabelText" Text="Task Owner"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="4">
                                                                                                                <asp:Image ID="PositionProfileWorkflowSelection_createTestLineImage" runat="server"
                                                                                                                    Height="1px" Width="100%" SkinID="sknPPLineImage" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left">
                                                                                                    <asp:UpdatePanel ID="PositionProfileWorkflowSelection_testCreationUpdatePanel" runat="server">
                                                                                                        <ContentTemplate>
                                                                                                            <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                <tr>
                                                                                                                    <td align="left">
                                                                                                                        <asp:DataList ID="PositionProfileWorkflowSelection_testCreationTaskOwnerDataList"
                                                                                                                            runat="server" RepeatColumns="2" RepeatDirection="Horizontal" OnItemCommand="PositionProfileWorkflowSelection_testCreationTaskOwnerDataList_ItemCommand">
                                                                                                                            <ItemTemplate>
                                                                                                                                <table cellpadding="1" cellspacing="1" border="0" align="center" width="100%">
                                                                                                                                    <tr>
                                                                                                                                        <td style="width: 10px; text-align: left">
                                                                                                                                            <asp:ImageButton ID="PositionProfileWorkflowSelection_testCreationTaskOwnerDataList_deleteImageButton"
                                                                                                                                                runat="server" SkinID="sknPPDeleteIconImageButton" CommandArgument='<%# Eval("UserID") %>'
                                                                                                                                                CommandName="DeleteTestCreationTaskOwner" Visible='<%# Eval("OwnerType") != null && Eval("OwnerType").ToString()!="POT_OWR" && Eval("OwnerType").ToString()!="POT_CO_OWR" %>'
                                                                                                                                                ToolTip="Delete Task Owner" />
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 150px; text-align: left">
                                                                                                                                            <asp:Label ID="PositionProfileWorkflowSelection_testCreationTaskOwnerNameLabel" runat="server"
                                                                                                                                                SkinID="sknLabelFieldText" Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'>
                                                                                                                                            </asp:Label>
                                                                                                                                            <asp:HiddenField ID="PositionProfileWorkflowSelection_testCreationTaskOwnerDataList_firstNameHiddenField" runat="server" Value='<%# Eval("FirstName") %>'/>
                                                                                                                                            <asp:HiddenField ID="PositionProfileWorkflowSelection_testCreationTaskOwnerDataList_emailHiddenField" runat="server" Value='<%# Eval("Email") %>'/>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:DataList>
                                                                                                                        <asp:Label ID="PositionProfileWorkflowSelection_testCreationTaskOwnerLabelValue"
                                                                                                                            runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ContentTemplate>
                                                                                                        <Triggers>
                                                                                                            <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_testCreationAddTaskOwnerImageButton" />
                                                                                                        </Triggers>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" style="padding-left: 2px">
                                                                                                    <asp:DataList ID="PositionProfileWorkflowSelection_testCreationDataList" runat="server"
                                                                                                        CellSpacing="5" CellPadding="0" RepeatColumns="1" RepeatDirection="Vertical"
                                                                                                        Width="100px" OnItemDataBound="PositionProfileWorkflowSelection_testCreationDataList_ItemDataBound">
                                                                                                        <ItemTemplate>
                                                                                                            <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
                                                                                                                <tr>
                                                                                                                    <td align="left">
                                                                                                                        <asp:HyperLink ID="PositionProfileWorkflowSelection_testNameHyperLink" runat="server"
                                                                                                                            ToolTip="View Test" Target="_blank" SkinID="sknLabelFieldTextHyperLink" Text='<%# Eval("Name") %>'>
                                                                                                                        </asp:HyperLink>
                                                                                                                        <asp:HiddenField ID="PositionProfileStatus_testKeyHiddenField" runat="server" Value='<%# Eval("TestKey") %>' />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle Wrap="true" />
                                                                                                    </asp:DataList>
                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_testCreationErrorLabel" Text="Test not created."
                                                                                                        CssClass="pp_error_message_text" runat="server"></asp:Label>
                                                                                                    <asp:HyperLink ID="PositionProfileWorkflowSelection_createTestHyperLink" runat="server"
                                                                                                        ToolTip="Create Test" Target="_blank" SkinID="sknLabelFieldTextHyperLink" Text="Create Test">
                                                                                                    </asp:HyperLink>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left">
                                                                                        <asp:UpdatePanel ID="PositionProfileWorkflowSelection_testRequiredSectionUpdatePanel"
                                                                                            runat="server">
                                                                                            <ContentTemplate>
                                                                                                <div style="display: block" runat="server" id="PositionProfileWorkflowSelection_showTestRequiredSectionDiv">
                                                                                                    <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                        <tr>
                                                                                                            <td align="center">
                                                                                                                <asp:Image ID="PositionProfileWorkflowSelection_testRequiredDownArrowImage" SkinID="sknPPDownArrowImage"
                                                                                                                    runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="tab_body_bg">
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <asp:Image ID="PositionProfileWorkflowSelection_testSessionCreationImage" SkinID="sknPPTestCreationImage"
                                                                                                                                                        runat="server" />
                                                                                                                                                </td>
                                                                                                                                                <td style="width: 15px">
                                                                                                                                                </td>
                                                                                                                                                <td align="left" style="width: 400px">
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_testSessionCreationTitleLabel" runat="server"
                                                                                                                                                        SkinID="sknPPHeaderLabel" Text="Test Session Creation"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td align="right" colspan="3">
                                                                                                                                                    <asp:ImageButton ID="PositionProfileWorkflowSelection_testSessionCreationAddTaskOwnerImageButton"
                                                                                                                                                        OnClick="PositionProfileWorkflowSelection_testSessionCreationAddTaskOwnerImageButton_Click"
                                                                                                                                                        SkinID="sknPPAddRecruiterImageButton" ToolTip="Select Task Owners" runat="server" />
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_testSessionCreationTaskOwnerLabel"
                                                                                                                                                        runat="server" SkinID="sknLabelText" Text="Task Owner"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td colspan="3">
                                                                                                                                                    <asp:Image ID="PositionProfileWorkflowSelection_testSessionLineImage" runat="server"
                                                                                                                                                        Height="1px" Width="100%" SkinID="sknPPLineImage" />
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <asp:UpdatePanel ID="PositionProfileWorkflowSelection_testSessionCreationUpdatePanel"
                                                                                                                                            runat="server">
                                                                                                                                            <ContentTemplate>
                                                                                                                                                <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                                                    <tr>
                                                                                                                                                        <td align="left">
                                                                                                                                                            <asp:DataList ID="PositionProfileWorkflowSelection_testSessionCreationTaskOwnerDataList"
                                                                                                                                                                runat="server" RepeatColumns="2" RepeatDirection="Horizontal" OnItemCommand="PositionProfileWorkflowSelection_testSessionCreationTaskOwnerDataList_ItemCommand">
                                                                                                                                                                <ItemTemplate>
                                                                                                                                                                    <table cellpadding="1" cellspacing="1" border="0" align="center" width="100%">
                                                                                                                                                                        <tr>
                                                                                                                                                                            <td style="width: 10px; text-align: left">
                                                                                                                                                                                <asp:ImageButton ID="PositionProfileWorkflowSelection_testSessionCreationTaskOwnerDataList_deleteImageButton"
                                                                                                                                                                                    runat="server" SkinID="sknPPDeleteIconImageButton" CommandArgument='<%# Eval("UserID") %>'
                                                                                                                                                                                    CommandName="DeleteTestSessionCreationTaskOwner" Visible='<%# Eval("OwnerType") != null && Eval("OwnerType").ToString()!="POT_OWR" && Eval("OwnerType").ToString()!="POT_CO_OWR" %>'
                                                                                                                                                                                    ToolTip="Delete Task Owner" />
                                                                                                                                                                            </td>
                                                                                                                                                                            <td style="width: 150px; text-align: left">
                                                                                                                                                                                <asp:Label ID="PositionProfileWorkflowSelection_testSessionCreationTaskOwnerNameLabel"
                                                                                                                                                                                    runat="server" SkinID="sknLabelFieldText" Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'>
                                                                                                                                                                                </asp:Label>
                                                                                                                                                                                <asp:HiddenField ID="PositionProfileWorkflowSelection_testSessionCreationTaskOwnerDataList_firstNameHiddenField" runat="server" Value='<%# Eval("FirstName") %>'/>
                                                                                                                                                                                <asp:HiddenField ID="PositionProfileWorkflowSelection_testSessionCreationTaskOwnerDataList_emailHiddenField" runat="server" Value='<%# Eval("Email") %>'/>
                                                                                                                                                                            </td>
                                                                                                                                                                        </tr>
                                                                                                                                                                    </table>
                                                                                                                                                                </ItemTemplate>
                                                                                                                                                            </asp:DataList>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </table>
                                                                                                                                            </ContentTemplate>
                                                                                                                                            <Triggers>
                                                                                                                                                <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_testSessionCreationAddTaskOwnerImageButton" />
                                                                                                                                            </Triggers>
                                                                                                                                        </asp:UpdatePanel>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td align="left">
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_noOfTestSessionLabel" SkinID="sknLabelText"
                                                                                                                                                        Text="No.of Sessions" runat="server"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td align="left">
                                                                                                                                                    <asp:DataList ID="PositionProfileWorkflowSelection_testSessionDataList" runat="server"
                                                                                                                                                        Width="100%" CellSpacing="0" CellPadding="0" RepeatColumns="1" RepeatDirection="Vertical"
                                                                                                                                                        OnItemDataBound="PositionProfileWorkflowSelection_testSessionDataList_ItemDataBound">
                                                                                                                                                        <ItemTemplate>
                                                                                                                                                            <table cellpadding="2" cellspacing="2" border="0" align="left">
                                                                                                                                                                <tr>
                                                                                                                                                                    <td>
                                                                                                                                                                        <asp:HyperLink ID="PositionProfileWorkflowSelection_testSessionHyperLink" runat="server"
                                                                                                                                                                            CommandName="Test Interviw" ToolTip="View Test Session" Target="_blank" SkinID="sknLabelFieldTextHyperLink"
                                                                                                                                                                            Text='<%# String.Format("{0}({1})", Eval("TestSessionID"), Eval("TestSessionCount")) %>'>
                                                                                                                                                                        </asp:HyperLink>
                                                                                                                                                                        <asp:HiddenField ID="PositionProfileWorkflowSelection_testSessionTestKeyHiddenField"
                                                                                                                                                                            runat="server" Value='<%# Eval("TestSessionID") %>' />
                                                                                                                                                                    </td>
                                                                                                                                                                </tr>
                                                                                                                                                            </table>
                                                                                                                                                        </ItemTemplate>
                                                                                                                                                        <ItemStyle Width="20px" BorderStyle="None" Wrap="true" />
                                                                                                                                                    </asp:DataList>
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_testSessionErrorLabel" CssClass="pp_error_message_text"
                                                                                                                                                        runat="server"></asp:Label>
                                                                                                                                                    <asp:HyperLink ID="PositionProfileWorkflowSelection_createTestSessionHyperLink" runat="server"
                                                                                                                                                        Target="_blank" SkinID="sknLabelFieldTextHyperLink" Text="Create Test Session"
                                                                                                                                                        ToolTip="Create Test Session">
                                                                                                                                                    </asp:HyperLink>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center">
                                                                                                                <asp:Image ID="PositionProfileWorkflowSelection_testSessionDownArrowImage" SkinID="sknPPDownArrowImage"
                                                                                                                    runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="tab_body_bg">
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <asp:Image ID="PositionProfileWorkflowSelection_testScheduleCreationImage" SkinID="sknPPTestScheduleImage"
                                                                                                                                                        runat="server" />
                                                                                                                                                </td>
                                                                                                                                                <td style="width: 15px">
                                                                                                                                                </td>
                                                                                                                                                <td align="left" style="width: 400px">
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_testScheduleCreationTitleLabel" runat="server"
                                                                                                                                                        SkinID="sknPPHeaderLabel" Text="Test Scheduling"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td align="right" colspan="3">
                                                                                                                                                    <asp:ImageButton ID="PositionProfileWorkflowSelection_testScheduleCreationAddTaskOwnerImageButton"
                                                                                                                                                        OnClick="PositionProfileWorkflowSelection_testScheduleCreationAddTaskOwnerImageButton_Click"
                                                                                                                                                        SkinID="sknPPAddRecruiterImageButton" ToolTip="Select Task Owners" runat="server" />
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerLabel"
                                                                                                                                                        runat="server" SkinID="sknLabelText" Text="Task Owner"></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td colspan="3">
                                                                                                                                                    <asp:Image ID="PositionProfileWorkflowSelection_testScheduleCreationLineImage" runat="server"
                                                                                                                                                        Height="1px" Width="100%" SkinID="sknPPLineImage" />
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <asp:UpdatePanel ID="PositionProfileWorkflowSelection_testScheduleCreationUpdatePanel"
                                                                                                                                            runat="server">
                                                                                                                                            <ContentTemplate>
                                                                                                                                                <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                                                    <tr>
                                                                                                                                                        <td align="left">
                                                                                                                                                            <asp:DataList ID="PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerDataList"
                                                                                                                                                                runat="server" RepeatColumns="2" RepeatDirection="Horizontal" OnItemCommand="PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerDataList_ItemCommand">
                                                                                                                                                                <ItemTemplate>
                                                                                                                                                                    <table cellpadding="1" cellspacing="1" border="0" align="center" width="100%">
                                                                                                                                                                        <tr>
                                                                                                                                                                            <td style="width: 10px; text-align: left">
                                                                                                                                                                                <asp:ImageButton ID="PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerDataList_deleteImageButton"
                                                                                                                                                                                    runat="server" SkinID="sknPPDeleteIconImageButton" CommandArgument='<%# Eval("UserID") %>'
                                                                                                                                                                                    CommandName="DeleteTestScheduleCreationTaskOwner" Visible='<%# Eval("OwnerType") != null && Eval("OwnerType").ToString()!="POT_OWR" && Eval("OwnerType").ToString()!="POT_CO_OWR" %>'
                                                                                                                                                                                    ToolTip="Delete Task Owner" />
                                                                                                                                                                            </td>
                                                                                                                                                                            <td style="width: 150px; text-align: left">
                                                                                                                                                                                <asp:Label ID="PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerNameLabel"
                                                                                                                                                                                    runat="server" SkinID="sknLabelFieldText" Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'>
                                                                                                                                                                                </asp:Label>
                                                                                                                                                                                <asp:HiddenField ID="PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerDataList_firstNameHiddenField" runat="server" Value='<%# Eval("FirstName") %>'/>
                                                                                                                                                                                <asp:HiddenField ID="PositionProfileWorkflowSelection_testScheduleCreationTaskOwnerDataList_emailHiddenField" runat="server" Value='<%# Eval("Email") %>'/>
                                                                                                                                                                            </td>
                                                                                                                                                                        </tr>
                                                                                                                                                                    </table>
                                                                                                                                                                </ItemTemplate>
                                                                                                                                                            </asp:DataList>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </table>
                                                                                                                                            </ContentTemplate>
                                                                                                                                            <Triggers>
                                                                                                                                                <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_testScheduleCreationAddTaskOwnerImageButton" />
                                                                                                                                            </Triggers>
                                                                                                                                        </asp:UpdatePanel>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td align="left">
                                                                                                                                        <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <div id="PositionProfileWorkflowSelection_testScheduleTitleDiv" runat="server">
                                                                                                                                                        <table cellpadding="2" cellspacing="2" width="100%">
                                                                                                                                                            <tr>
                                                                                                                                                                <td style="width: 100px">
                                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_testScheduledTitleLabel" runat="server"
                                                                                                                                                                        Text="Scheduled" SkinID="sknHomePageLabel"></asp:Label>
                                                                                                                                                                </td>
                                                                                                                                                                <td valign="top" align="left">
                                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_testCompletedTitleLabel" runat="server"
                                                                                                                                                                        Text="Completed" SkinID="sknHomePageLabel"></asp:Label>
                                                                                                                                                                </td>
                                                                                                                                                            </tr>
                                                                                                                                                        </table>
                                                                                                                                                    </div>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <asp:DataList ID="PositionProfileWorkflowSelection_testScheduledDataList" runat="server"
                                                                                                                                                        Width="100%" CellSpacing="0" CellPadding="0" RepeatColumns="1" RepeatDirection="Vertical"
                                                                                                                                                        OnItemDataBound="PositionProfileWorkflowSelection_testScheduledDataList_ItemDataBound">
                                                                                                                                                        <ItemTemplate>
                                                                                                                                                            <table cellpadding="2" cellspacing="2" border="0" align="left">
                                                                                                                                                                <tr>
                                                                                                                                                                    <td style="width: 102px">
                                                                                                                                                                        <asp:HyperLink ID="PositionProfileWorkflowSelection_testScheduledHyperLink" runat="server"
                                                                                                                                                                            CommandName="TestScheduled" ToolTip="View Test Scheduled/Schedule Candidate"
                                                                                                                                                                            Target="_blank" SkinID="sknLabelFieldTextHyperLink" Text='<%# String.Format("{0}({1})", Eval("ScheduledTests"), Eval("TestSessionCount")) %>'>
                                                                                                                                                                        </asp:HyperLink>
                                                                                                                                                                        <asp:HiddenField ID="PositionProfileWorkflowSelection_testSessionKeyHiddenField"
                                                                                                                                                                            runat="server" Value='<%# Eval("TestSessionID") %>' />
                                                                                                                                                                    </td>
                                                                                                                                                                    <td>
                                                                                                                                                                        <asp:HyperLink ID="PositionProfileWorkflowSelection_testCompletedHyperLink" runat="server"
                                                                                                                                                                            CommandName="TestCompleted" ToolTip="View Test Completed" Target="_blank" SkinID="sknLabelFieldTextHyperLink"
                                                                                                                                                                            Text='<%# String.Format("{0}({1})", Eval("CompletedTests"), Eval("TestSessionCount")) %>'>
                                                                                                                                                                        </asp:HyperLink>
                                                                                                                                                                    </td>
                                                                                                                                                                </tr>
                                                                                                                                                            </table>
                                                                                                                                                        </ItemTemplate>
                                                                                                                                                        <ItemStyle Width="40px" BorderStyle="None" Wrap="true" />
                                                                                                                                                    </asp:DataList>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_testScheduleErrorLabel" CssClass="pp_error_message_text"
                                                                                                                                                        runat="server"></asp:Label>
                                                                                                                                                    <asp:HyperLink ID="PositionProfileWorkflowSelection_createTestScheduleHyperLink"
                                                                                                                                                        runat="server" ToolTip="Schedule Candidate" Target="_blank" SkinID="sknLabelFieldTextHyperLink"
                                                                                                                                                        Text="Schedule Candidate">
                                                                                                                                                    </asp:HyperLink>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_testActiveImageButton" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:UpdatePanel ID="PositionProfileWorkflowSelection_showTestRequiredSectionDownArrowUpadtePanel"
                                                                    runat="server">
                                                                    <ContentTemplate>
                                                                        <div style="display: block" runat="server" id="PositionProfileWorkflowSelection_showTestRequiredSectionDownArrowDiv">
                                                                            <asp:Image ID="PositionProfileWorkflowSelection_testDownArrowImage" SkinID="sknPPDownArrowImage"
                                                                                runat="server" />
                                                                        </div>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_testActiveImageButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_10">
                                    </td>
                                </tr>
                                <!-- Submittal to client -->
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="PositionProfileWorkflowSelection_showSubmittalRequiredUpdatePanel"
                                            runat="server">
                                            <ContentTemplate>
                                                <div style="display: block" runat="server" id="PositionProfileWorkflowSelection_showSubmittalRequiredDiv">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td>
                                                                <table class="pp_workflow_panel_bg" cellpadding="0" cellspacing="0" align="center"
                                                                    style="height: 60px;" border="0">
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <table width="80%" cellpadding="2" cellspacing="2" style="margin-left: 200px">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellpadding="0" cellspacing="0" width="80%">
                                                                                            <tr>
                                                                                                <td style="width: 49px">
                                                                                                    <asp:Image ID="PositionProfileWorkflowSelection_submittalClientImage" SkinID="sknPPSubmittalCreationImage"
                                                                                                        runat="server" />
                                                                                                </td>
                                                                                                <td style="width: 15px">
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_submittalClientLabel" runat="server"
                                                                                                        SkinID="sknPPHeaderLabel" Text="Submittal to Client"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="3" style="width: 65px" align="right">
                                                                                                    <asp:ImageButton ID="PositionProfileWorkflowSelection_submittalClientAddTaskOwnerImageButton"
                                                                                                        OnClick="PositionProfileWorkflowSelection_submittalClientAddTaskOwnerImageButton_Click"
                                                                                                        SkinID="sknPPAddRecruiterImageButton" ToolTip="Select Task Owners" runat="server" />
                                                                                                    <asp:Label ID="PositionProfileWorkflowSelection_submittalClientTaskOnwerLabel" runat="server"
                                                                                                        SkinID="sknLabelText" Text="Task Owner"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="3">
                                                                                                    <asp:Image ID="PositionProfileWorkflowSelection_submittalClientTopLineImage" runat="server"
                                                                                                        Height="1px" Width="100%" SkinID="sknPPLineImage" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <div style="height: 50px; width: 470px; overflow: auto; display: block;" runat="server">
                                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:UpdatePanel ID="PositionProfileWorkflowSelection_submittalClientUpdatePanel"
                                                                                                            runat="server">
                                                                                                            <ContentTemplate>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <asp:DataList ID="PositionProfileWorkflowSelection_submittalToClientTaskOwnerDataList"
                                                                                                                                runat="server" RepeatColumns="3" RepeatDirection="Horizontal" OnItemCommand="PositionProfileWorkflowSelection_submittalToClientTaskOwnerDataList_ItemCommand">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <table cellpadding="1" cellspacing="1" border="0" align="center" width="100%">
                                                                                                                                        <tr>
                                                                                                                                            <td style="width: 10px; text-align: left">
                                                                                                                                                <asp:ImageButton ID="PositionProfileWorkflowSelection_submittalToClientTaskOwnerDataList_deleteImageButton"
                                                                                                                                                    runat="server" SkinID="sknPPDeleteIconImageButton" CommandArgument='<%# Eval("UserID") %>'
                                                                                                                                                    CommandName="DeleteSubmittalToClientTaskOwner" Visible='<%# Eval("OwnerType") != null && Eval("OwnerType").ToString()!="POT_OWR" && Eval("OwnerType").ToString()!="POT_CO_OWR" %>'
                                                                                                                                                    ToolTip="Delete Task Owner" />
                                                                                                                                            </td>
                                                                                                                                            <td style="width: 150px; text-align: left">
                                                                                                                                                <asp:Label ID="PositionProfileWorkflowSelection_submittalToClientTaskOwnerNameLabel"
                                                                                                                                                    runat="server" SkinID="sknLabelFieldText" Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>'>
                                                                                                                                                </asp:Label>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:DataList>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </ContentTemplate>
                                                                                                            <Triggers>
                                                                                                                <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_submittalClientAddTaskOwnerImageButton" />
                                                                                                            </Triggers>
                                                                                                        </asp:UpdatePanel>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="PositionProfileWorkflowSelection_submittedCandidatesLabel" runat="server"
                                                                                            SkinID="sknLabelText" Text="Submitted Candidates :"></asp:Label>&nbsp;
                                                                                            <asp:HyperLink ID="PositionProfileWorkflowSelection_submittedCandidatesHyperLink" runat="server" 
                                                                                                ToolTip="View Submitted Candidates" Target="_blank"  SkinID="sknLabelFieldTextHyperLink" >
                                                                                            </asp:HyperLink>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="PositionProfileWorkflowSelection_interviewActiveImageButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <!-- End Submittal to client -->
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_10">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 130px">
                                        <asp:Button ID="PositionProfileWorkflowSelection_saveContinueButton" runat="server"
                                            Text="Save and Continue" SkinID="sknButtonId" Height="26px" OnClick="PositionProfileWorkflowSelection_saveContinueButton_Click" />
                                    </td>
                                    <td style="width: 150px">
                                        <asp:LinkButton ID="PositionProfileWorkflowSelection_goToWorkFlowLinkButton" SkinID="sknDashboardLinkButton"
                                            runat="server" ToolTip="Click here to return to workflow selection" OnClick="PositionProfileWorkflowSelection_goToWorkFlowLinkButton_Click"
                                            Text="Return to Detailed Info"> </asp:LinkButton>
                                    </td>
                                    <td align="left">
                                        <asp:LinkButton ID="PositionProfileWorkflowSelection_cancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="PositionProfileWorkflowSelection_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="PositionProfileWorkflowSelection_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label><asp:Label ID="PositionProfileWorkflowSelection_bottomErrorMessageLabel"
                                runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
