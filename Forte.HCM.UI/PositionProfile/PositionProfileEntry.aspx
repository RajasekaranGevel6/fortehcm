﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/PositionProfileMaster.Master"
    AutoEventWireup="true" EnableEventValidation="false" CodeBehind="PositionProfileEntry.aspx.cs"
    Inherits="Forte.HCM.UI.PositionProfile.PositionProfileEntry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Segments/VerticalBackgroundRequirementControl.ascx" TagName="VerticalBackgroundRequirementControl"
    TagPrefix="uc1" %>
<%@ Register Src="../Segments/TechnicalSkillRequirementControl.ascx" TagName="TechnicalSkillRequirementControl"
    TagPrefix="uc2" %>
<%@ Register Src="../Segments/RoleRequirementControl.ascx" TagName="RoleRequirementControl"
    TagPrefix="uc3" %>
<%@ Register Src="../Segments/EducationRequirementControl.ascx" TagName="EducationRequirementControl"
    TagPrefix="uc5" %>
<%@ Register Src="../Segments/ClientPositionDetailsControl.ascx" TagName="ClientPositionDetailsControl"
    TagPrefix="uc6" %>
<%@ Register Src="~/CommonControls/RequirementViewerControl.ascx" TagName="RequirementViewerControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/SegmentListControl.ascx" TagName="SegmentListControl"
    TagPrefix="uc7" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc8" %>
<%@ Register Src="~/CommonControls/PositionProfileSummary.ascx" TagName="PositionProfileSummary"
    TagPrefix="uc9" %>
<%@ Register Src="~/CommonControls/ClientInfoControl.ascx" TagName="ClientInfoControl"
    TagPrefix="uc10" %>
    
<%@ Register Src="~/CommonControls/DepartmentControl.ascx" TagName="Department" TagPrefix="uc11" %>
<%@ Register Src="~/CommonControls/ClientControl.ascx" TagName="Client" TagPrefix="uc12" %>
<%@ Register Src="~/CommonControls/ContactControl.ascx" TagName="Contact" TagPrefix="uc13" %>

<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content ID="PositionProfileEntry_bodyContent" runat="server" ContentPlaceHolderID="PositionProfileMaster_body">

    <script type="text/javascript">
        function ShowEmailConfirm() {
            //displays email confirmation pop up extender
            // $find("<%= PositionProfileEntry_emailContentConfirmModalPopupExtender.ID %>").show();
            $find("ctl00_PositionProfileMaster_body_PositionProfileEntry_emailContentConfirmModalPopupExtender").show();
            return false;
        }        
    </script>
    &nbsp;<asp:UpdatePanel ID="PositionProfileEntry_updatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PositionProfileEntry_mainPanel" runat="server" DefaultButton="PositionProfileEntry_bottomSaveButton">
                <table border="0" cellspacing="3" cellpadding="0" width="100%">
                    <tr>
                        <td class="header_bg">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="80%" class="header_text_bold">
                                        <asp:Literal ID="PositionProfileEntry_headerLiteral" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%" align="right">
                                        <table cellpadding="3" cellspacing="4" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="PositionProfileEntry_activityImageButton"
                                                        runat="server" SkinID="sknSkillViewPPActivityImageButton" ToolTip="Position Profile Activity"
                                                        CommandName="Activity" OnClick="PositionProfileEntry_actionButton_Click" Visible="false" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="PositionProfileEntry_statusImageButton" runat="server" SkinID="sknSkillViewPPDashboardImageButton"
                                                        ToolTip="Position Profile Dashboard" CommandName="Status" OnClick="PositionProfileEntry_actionButton_Click"
                                                        Visible="false" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="PositionProfileEntry_talentSearchPositionProfileImageButton"
                                                        runat="server" SkinID="sknTalentSearchImageButton" ToolTip="intelliSPOT Search" CommandName="talentsearch"
                                                        OnClick="PositionProfileEntry_actionButton_Click" Visible="false" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="PositionProfileEntry_authorTestImageButton" runat="server" SkinID="sknAuthorTestImageButton"
                                                        ToolTip="Author Test" CommandName="authortest" OnClick="PositionProfileEntry_actionButton_Click"
                                                        Visible="false" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="PositionProfileEntry_authorTestsearchExistingTestImageButton"
                                                        runat="server" SkinID="sknSearchExistingTestImageButton" ToolTip="Search Existing Test"
                                                        CommandName="searchtest" OnClick="PositionProfileEntry_actionButton_Click" Visible="false" />
                                                </td>
                                                 
                                                  <td>
                                                    <asp:ImageButton ID="PositionprofileEntry_InterviewTestMakerImageButton" runat="server" SkinID="sknOfflineInterviewTestmakerImageButton"
                                                        ToolTip="Interview Maker" CommandName="InterviewTestMaker" OnClick="PositionProfileEntry_actionButton_Click"
                                                        Visible="false" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="PositionprofileEntry_SearchInterviewTestImageButton"
                                                        runat="server" SkinID="sknOfflineSearchExistingTestImageButton" ToolTip="Search Existing Interview"
                                                        CommandName="SearchInterviewTest" OnClick="PositionProfileEntry_actionButton_Click" Visible="false" />
                                                </td>

                                                <td>
                                                    <asp:Button ID="PositionProfileEntry_topDeleteButton" runat="server" Text="Delete"
                                                        SkinID="sknButtonId" OnClick="PositionProfileEntry_saveButton_Click" CommandName="delete"
                                                        Visible="false" />
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:Button ID="PositionProfileEntry_topSaveASButton" runat="server" Text="Save as New"
                                                        SkinID="sknButtonId" OnClick="PositionProfileEntry_saveButton_Click" CommandName="saveas"
                                                        Visible="false" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="PositionProfileEntry_topSaveButton" runat="server" Text="Save" SkinID="sknButtonId"
                                                        OnClick="PositionProfileEntry_saveButton_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="PositionProfileEntry_topPreviewButton" runat="server" Text="Preview"
                                                        SkinID="sknButtonId" OnClick="PositionProfileEntry_previewButton_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="PositionProfileEntry_topDownloadButton" runat="server" Text="Download"
                                                        SkinID="sknButtonId" OnClick="PositionProfileEntry_downloadButton_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="PositionProfileEntry_topEmailButton" runat="server" Text="Email"
                                                        SkinID="sknButtonId" OnClick="PositionProfileEntry_emailButton_Click" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="PositionProfileEntry_topPreviousPageLinkButton" runat="server"
                                                        OnClick="PositionProfileEntry_previousPageLinkButton_Click" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                </td>
                                                <td width="4%" align="center" class="link_button">
                                                    <asp:Label ID="PositionProfileEntry_topPreviousPageLinkButtonSeparatorLabel" runat="server"
                                                        Text="|" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="PositionProfileEntry_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                        Text="Reset" OnClick="PositionProfileEntry_resetLinkButton_Click" />
                                                </td>
                                                <td width="4%" align="center" class="link_button">
                                                    |
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="PositionProfileEntry_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                        Text="Cancel" OnClick="ParentPageRedirect" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="msg_align">
                            <%--  <asp:UpdatePanel ID="CreateTestSession_topSuccessErrorMsgUpdatePanel" runat="server">
                    <ContentTemplate>--%>
                            <asp:Label ID="PositionProfileEntry_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="PositionProfileEntry_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            <%--  </ContentTemplate>
                </asp:UpdatePanel>--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg_pp" style="width: 995px; overflow: auto; table-layout: fixed">
                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td id="PositionProfileEntry_mainTd" style="height: 590px; width: 100%; overflow: auto;"
                                        valign="top" align="left">
                                        <div style="width: 100%; overflow: auto; table-layout: fixed">
                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; overflow: auto;
                                                table-layout: fixed">
                                                <tr>
                                                    <td>
                                                        <div style="height: 590px; vertical-align: top; overflow: auto; table-layout: fixed">
                                                            <table width="900px" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="header_bg">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td class="td_height_8">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellpadding="1" cellspacing="1">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="PositionProfileEntry_formTypeLabel" runat="server" Text="Form Layout"
                                                                                                    SkinID="sknLabelText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div style="float: left; padding-right: 2px;">
                                                                                                    <%--<asp:DropDownList ID="PositionProfileEntry_formTypeDropWownList1" runat="server" AutoPostBack="true"
                                                                                    OnSelectedIndexChanged="PositionProfileEntry_formTypeDropWownList_SelectedIndexChanged"
                                                                                    AppendDataBoundItems="true" Width="250px" Visible="false">
                                                                                    <asp:ListItem Text="--Select--" Value="-1" Selected="True"></asp:ListItem>
                                                                                </asp:DropDownList>--%>
                                                                                                    <ajaxToolkit:ComboBox ID="PositionProfileEntry_formTypeDropWownList" runat="server"
                                                                                                        AutoPostBack="true" AppendDataBoundItems="true" DropDownStyle="DropDownList"
                                                                                                        AutoCompleteMode="SuggestAppend" CaseSensitive="False" Width="250px" CssClass="WindowsStyle"
                                                                                                        OnSelectedIndexChanged="PositionProfileEntry_formTypeDropWownList_SelectedIndexChanged"
                                                                                                        ItemInsertLocation="Append">
                                                                                                        <asp:ListItem Text="--Select--" Value="-1" Selected="True"></asp:ListItem>
                                                                                                    </ajaxToolkit:ComboBox>
                                                                                                    <asp:HiddenField ID="PositionProfileEntry_formIDHiddenField" runat="server" Value="1" />
                                                                                                </div>
                                                                                                <div style="float: left;">
                                                                                                    <asp:ImageButton ID="PositionProfileEntry_formTypeHelpImageButton" SkinID="sknHelpImageButton"
                                                                                                        runat="server" ToolTip="Choose the various form here" ImageAlign="Middle" OnClientClick="javascript:return false;" />
                                                                                                    <asp:ImageButton ID="PositionProfileEntry_defaultFormImageButton" SkinID="sknDefaultFormImageButton"
                                                                                                        runat="server" ToolTip="Set as favourite position profile form" ImageAlign="Middle"
                                                                                                        OnClick="PositionProfileEntry_defaultFormImageButton_Click" />
                                                                                                </div>
                                                                                            </td>
                                                                                            <td align="right">
                                                                                                <asp:Label ID="PositionProfileEntry_registeredByLabel" runat="server" Text="Registered By"
                                                                                                    SkinID="sknLabelText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div style="float: left; padding-right: 2px;">
                                                                                                    <asp:TextBox ID="PositionProfileEntry_registeredByTextBox" runat="server" MaxLength="50"
                                                                                                        Width="80px"></asp:TextBox>
                                                                                                </div>
                                                                                                <div style="float: left;">
                                                                                                    <asp:ImageButton ID="PositionProfileEntry_registeredByImageButton" SkinID="sknbtnSearchicon"
                                                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to select the position profile register" />
                                                                                                    <asp:HiddenField ID="PositionProfileEntry_registeredBydummyAuthorIdHidenField" runat="server" />
                                                                                                    <asp:HiddenField ID="PositionProfileEntry_registeredByHiddenField" runat="server" />
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_8">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 18%">
                                                                                                <asp:Label ID="PositionProfileEntry_positionProfileNameLabel" runat="server" Text="Position Profile Name"
                                                                                                    SkinID="sknLabelText"></asp:Label>
                                                                                                <span class="mandatory">*</span>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="PositionProfileEntry_positionProfileNameTextBox" runat="server"
                                                                                                    Width="100px"></asp:TextBox>
                                                                                            </td>
                                                                                            <td align="right">
                                                                                                <asp:Label ID="PositionProfileEntry_dateOfRegistrationLabel" runat="server" Text="Date Of Registration"
                                                                                                    SkinID="sknLabelText"></asp:Label>
                                                                                                <span class="mandatory">*</span>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div style="float: left; padding-right: 2px;">
                                                                                                    <asp:TextBox ID="PositionProfileEntry_dateOfRegistrationTextBox" runat="server" Width="80px"></asp:TextBox>
                                                                                                </div>
                                                                                                <div style="float: left;">
                                                                                                    <asp:ImageButton ID="PositionProfileEntry_dateOfRegistrationCalenderImageButton"
                                                                                                        SkinID="sknCalendarImageButton" runat="server" ImageAlign="Middle" />
                                                                                                </div>
                                                                                                <ajaxToolkit:MaskedEditExtender ID="PositionProfileEntry_dateOfRegistrationMaskedEditExtender"
                                                                                                    runat="server" TargetControlID="PositionProfileEntry_dateOfRegistrationTextBox"
                                                                                                    Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                                    OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                                    ErrorTooltipEnabled="True" />
                                                                                                <ajaxToolkit:MaskedEditValidator ID="PositionProfileEntry_maskedEditValidator" runat="server"
                                                                                                    ControlExtender="PositionProfileEntry_dateOfRegistrationMaskedEditExtender" ControlToValidate="PositionProfileEntry_dateOfRegistrationTextBox"
                                                                                                    EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                                                                    TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                                                    ValidationGroup="MKE" />
                                                                                                <ajaxToolkit:CalendarExtender ID="SearchCreditRequest_processedDateToCalendarExtender"
                                                                                                    runat="server" TargetControlID="PositionProfileEntry_dateOfRegistrationTextBox"
                                                                                                    CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="PositionProfileEntry_dateOfRegistrationCalenderImageButton" />
                                                                                            </td>
                                                                                            <td align="right">
                                                                                                <asp:Label ID="PositionProfileEntry_submittalDeadLineLabel" runat="server" Text="Submittal Deadline"
                                                                                                    SkinID="sknLabelText"></asp:Label>
                                                                                                <span class="mandatory">*</span>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div style="float: left; padding-right: 2px;">
                                                                                                    <asp:TextBox ID="PositionProfileEntry_submittalDeadLineTextBox" runat="server" Width="80px"></asp:TextBox>
                                                                                                </div>
                                                                                                <div style="float: left;">
                                                                                                    <asp:ImageButton ID="PositionProfileEntry_submittalDeadLineCallenderImageButton"
                                                                                                        SkinID="sknCalendarImageButton" runat="server" ImageAlign="Middle" />
                                                                                                </div>
                                                                                                <ajaxToolkit:MaskedEditExtender ID="PositionProfileEntry_submittalDeadLineMaskedEditExtender"
                                                                                                    runat="server" TargetControlID="PositionProfileEntry_submittalDeadLineTextBox"
                                                                                                    Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                                                    OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                                                                    ErrorTooltipEnabled="True" />
                                                                                                <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="PositionProfileEntry_submittalDeadLineMaskedEditExtender"
                                                                                                    ControlToValidate="PositionProfileEntry_submittalDeadLineTextBox" EmptyValueMessage="Date is required"
                                                                                                    InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                                                                                    EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="PositionProfileEntry_submittalDeadLineTextBox"
                                                                                                    CssClass="MyCalendar" Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="PositionProfileEntry_submittalDeadLineCallenderImageButton" />
                                                                                            </td>
                                                                                            <td>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_8">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 18%">
                                                                                                <asp:Label ID="PositionProfileEntry_additionalInformationLabel" runat="server" Text="Additional Information"
                                                                                                    SkinID="sknLabelText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="PositionProfileEntry_additionalInformationTextBox" runat="server"
                                                                                                   Rows="4" Columns="140" TextMode="MultiLine"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_height_8">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_8">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <uc10:ClientInfoControl ID="PositionProfileEntry_ClientInfoControl" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_8">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                            <tr>
                                                                                <td>
                                                                                    <input type="hidden" runat="server" id="IsDeleteClicked" />
                                                                                    <ajaxToolkit:TabContainer ID="PositionProfileEntry_tabContainer" runat="server" ActiveTabIndex="0"
                                                                                        AutoPostBack="true" SkinID="sknPositionProfileTabContainer" OnActiveTabChanged="PositionProfileEntry_tabContainer_ActiveTabChanged">
                                                                                        <ajaxToolKit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                                                                                        </ajaxToolKit:TabPanel>
                                                                                    </ajaxToolkit:TabContainer>
                                                                                </td>
                                                                                <td align="right" style="border-bottom: solid 1px #CCCCCC">
                                                                                    <asp:LinkButton ID="PositionProfileEntry_addSegmentLinkButton" runat="server" SkinID="sknAddLinkButton"
                                                                                        Text="Add" ToolTip="Click here to add Segments" OnClick="PositionProfileEntry_addSegmentLinkButton_Click" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100%">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <uc1:VerticalBackgroundRequirementControl ID="PositionProfileEntry_verticalBackgroundRequirementControl"
                                                                                        runat="server" Visible="false" />
                                                                                    <uc2:TechnicalSkillRequirementControl ID="PositionProfileEntry_technicalSkillRequirementControl"
                                                                                        runat="server" Visible="false" />
                                                                                    <uc3:RoleRequirementControl ID="PositionProfileEntry_roleRequirementControl" runat="server"
                                                                                        Visible="false" />
                                                                                    <uc5:EducationRequirementControl ID="PositionProfileEntry_educationRequirementControl"
                                                                                        runat="server" Visible="false" />
                                                                                    <uc6:ClientPositionDetailsControl ID="PositionProfileEntry_clientPositionDetailsControl"
                                                                                        runat="server" Visible="false" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td align="center" id="PositionProfileEntry_leftTd" style="padding-left: 3px; width: 1%">
                                        <asp:Panel ID="testPanle" runat="server">
                                            <div style="width: 390px; height: 400px; vertical-align: top; table-layout: fixed;
                                                overflow: auto">
                                                <uc4:RequirementViewerControl ID="PositionProfileEntry_RequirementControl" runat="server" />
                                            </div>
                                        </asp:Panel>
                                    </td>
                                    <td style="width: 32px; padding-left: 3px; table-layout: fixed; float: right; height: 400px;
                                        vertical-align: top">
                                        <img alt="" id="PositionProfileEntry_expandableImg" runat="server" src="~/App_Themes/DefaultTheme/Images/pp_btn_out.png"
                                            style="cursor: pointer" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="msg_align">
                            <%--   <asp:UpdatePanel ID="CreateTestSession_bottomSuccessErrorMsgUpdatePanel" runat="server">
                    <ContentTemplate>--%>
                            <asp:Label ID="PositionProfileEntry_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="PositionProfileEntry_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            <%-- </ContentTemplate>
                </asp:UpdatePanel>--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="80%" class="header_text_bold">
                                    </td>
                                    <td width="20%" align="right">
                                        <table cellpadding="3" cellspacing="4" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="PositionProfileEntry_bottomActivityImageButton"
                                                        runat="server" SkinID="sknSkillViewPPActivityImageButton" ToolTip="Position Profile Activity"
                                                        CommandName="Activity" OnClick="PositionProfileEntry_actionButton_Click" Visible="false" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="PositionProfileEntry_bottomStatusImageButton" runat="server"
                                                        SkinID="sknSkillViewPPDashboardImageButton" ToolTip="Position Profile Dashboard"
                                                        CommandName="Status" OnClick="PositionProfileEntry_actionButton_Click" Visible="false" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="PositionProfileEntry_bottomTalentSearchPositionProfileImageButton"
                                                        runat="server" SkinID="sknTalentSearchImageButton" ToolTip="intelliSPOT Search" CommandName="talentsearch"
                                                        OnClick="PositionProfileEntry_actionButton_Click" Visible="false" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="PositionProfileEntry_bottomAuthorTestImageButton" runat="server"
                                                        SkinID="sknAuthorTestImageButton" ToolTip="Author Test" CommandName="authortest"
                                                        OnClick="PositionProfileEntry_actionButton_Click" Visible="false" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="PositionProfileEntry_bottomAuthorTestsearchExistingTestImageButton"
                                                        runat="server" SkinID="sknSearchExistingTestImageButton" ToolTip="Search Existing Test"
                                                        CommandName="searchtest" OnClick="PositionProfileEntry_actionButton_Click" Visible="false" />
                                                </td>
                                               
                                                <td>
                                                    <asp:ImageButton ID="PositionProfileEntry_bottomOfflineInterviewTestImageButton" runat="server" SkinID="sknOfflineInterviewTestmakerImageButton"
                                                        ToolTip="Interview Maker" CommandName="InterviewTestMaker" OnClick="PositionProfileEntry_actionButton_Click"
                                                        Visible="false" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="PositionProfileEntry_bottomSearchExistingOfflineInterviewTestImageButton"
                                                        runat="server" SkinID="sknOfflineSearchExistingTestImageButton" ToolTip="Search Existing Interview"
                                                        CommandName="SearchInterviewTest" OnClick="PositionProfileEntry_actionButton_Click" Visible="false" />
                                                </td>

                                                <td>
                                                    <asp:Button ID="PositionProfileEntry_bottomDeleteButton" runat="server" Text="Delete"
                                                        SkinID="sknButtonId" OnClick="PositionProfileEntry_saveButton_Click" CommandName="delete"
                                                        Visible="false" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="PositionProfileEntry_bottomSaveASButton" runat="server" Text="Save as New"
                                                        SkinID="sknButtonId" OnClick="PositionProfileEntry_saveButton_Click" CommandName="saveas"
                                                        Visible="false" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="PositionProfileEntry_bottomSaveButton" runat="server" Text="Save"
                                                        SkinID="sknButtonId" OnClick="PositionProfileEntry_saveButton_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="PositionProfileEntry_bottomPreviewButton" runat="server" Text="Preview"
                                                        SkinID="sknButtonId" OnClick="PositionProfileEntry_previewButton_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="PositionProfileEntry_bottomDownloadButton" runat="server" Text="Download"
                                                        SkinID="sknButtonId" OnClick="PositionProfileEntry_downloadButton_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="PositionProfileEntry_bottomEmailButton" runat="server" Text="Email"
                                                        SkinID="sknButtonId" OnClick="PositionProfileEntry_emailButton_Click" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="PositionProfileEntry_bottomPreviousPageLinkButton" runat="server"
                                                        OnClick="PositionProfileEntry_previousPageLinkButton_Click" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                </td>
                                                <td width="4%" align="center" class="link_button">
                                                    <asp:Label ID="PositionProfileEntry_bottomPreviousPageLinkButtonSeparatorLabel" runat="server"
                                                        Text="|" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="PositionProfileEntry_bottomResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                        Text="Reset" OnClick="PositionProfileEntry_resetLinkButton_Click" />
                                                </td>
                                                <td width="4%" align="center" class="link_button">
                                                    |
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="PositionProfileEntry_bottomCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                        Text="Cancel" OnClick="ParentPageRedirect" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="PositionProfileEntry_segmentListPanel" runat="server" Style="display: none;"
                CssClass="popupcontrol_segmentControl">
                <uc7:SegmentListControl ID="PositionProfileEntry_segmentListControl" OnOkClick="PositionProfileEntry_segmentListControl_Add_segment"
                    runat="server" />
            </asp:Panel>
            <asp:Button ID="PositionProfileEntry_hiddenPopupModalButton" runat="server" Style="display: none" />
            <asp:Button ID="PositionProfileEntry_hiddenConfirmPopupModalButton" runat="server"
                Style="display: none" />
            <asp:Button ID="Button1" runat="server" Style="display: none" />
            <asp:Button ID="PositionProfileEntry_hiddenPreviewModalButton" runat="server" Style="display: none" />
            <asp:Panel ID="PositionProfileEntry_ConfirmPopupPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_confirm_remove">
                <uc8:ConfirmMsgControl ID="PositionProfileEntry_ConfirmPopupExtenderControl" runat="server"
                    OnOkClick="PositionProfileEntry_ConfirmPopupExtenderControl_OkClick" OnCancelClick="PositionProfileEntry_ConfirmPopupExtenderControl_CancelClick" />
            </asp:Panel>
            <asp:Panel ID="PositionProfileEntry_PreviewPopupPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_segment_Controls_preview">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align: center">
                    <tr>
                        <td class="td_height_20">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <table width="95%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 85%" class="popup_header_text" valign="middle" align="left">
                                        <asp:Label ID="PositionProfileEntry_placeHoldercancelLiteral" runat="server" Text="Position Profile Preview"></asp:Label>
                                    </td>
                                    <td style="width: 15%" align="right">
                                        <asp:ImageButton ID="PositionProfileEntry_placeHolderCloseImageButton" runat="server"
                                            SkinID="sknCloseImageButton" OnClick="PositionProfileEntry_previewCloseImageButton_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_20" colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popup_panel_inner_bg">
                                        <div id="content" style="height: 450px; overflow: auto;">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="PositionProfileEntry_previewPositionProfileNameHeaderLabel" runat="server"
                                                            Text="Position Profile Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="PositionProfileEntry_previewPositionProfileNameLabel" runat="server"
                                                            Text="test" SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="PositionProfileEntry_previewPositionProfileRegisterHeaderLabel" runat="server"
                                                            Text="Registered By" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="PositionProfileEntry_previewPositionProfileRegisterLabel" runat="server"
                                                            Text="test" SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="PositionProfileEntry_previewPositionProfileRegisterDateHeaderLabel"
                                                            runat="server" Text="Date Of Registration" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="PositionProfileEntry_previewPositionProfileRegisterDateLabel" runat="server"
                                                            Text="test" SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="PositionProfileEntry_previewPositionProfileSubmittalDateHeaderLabel"
                                                            runat="server" Text="Submittal Deadline" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="PositionProfileEntry_previewPositionProfileSubmittalDateLabel" runat="server"
                                                            Text="test" SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_5" colspan="9">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="9">
                                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" id="PositionProfileEntry_previewClientDetailsTable"
                                                            runat="server">
                                                            <tr>
                                                                <td class="header_bg" colspan="2">
                                                                    <asp:Literal ID="PositionProfileEntry_previewClientDetailsHeaderLiteral" runat="server"
                                                                        Text="Client Details"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 100%">
                                                                    <asp:GridView ID="PositionProfileEntry_previewClientDetailsGridView" runat="server">
                                                                        <Columns>
                                                                            <asp:BoundField HeaderText="Client Name" DataField="ClientName" />
                                                                            <asp:BoundField HeaderText="Department Name" DataField="DepartmentName" />
                                                                            <asp:BoundField HeaderText="Contact Name" DataField="ContactName" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_height_5" colspan="9">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="8" align="left" style="width: 100%">
                                                        <asp:Panel ID="PositionProfileEntry_previewPanel" runat="server">
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">
                                        <asp:LinkButton ID="PositionProfileEntry_controlsPlaceHolderCancelButton" runat="server"
                                            Text="Cancel" SkinID="sknPopupLinkButton" OnClick="PositionProfileEntry_previewCancelButton_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="PositionProfileEntry_positionProfileSavePanel" runat="server" Style="display: none"
                CssClass="popupcontrol_position_profile_summary" DefaultButton="PositionProfileEntry_positionProfileSummary_CreateButton">
                <div style="display: none">
                    <asp:Button ID="PositionProfileEntry_viewPositionProfileSaveButton" runat="server" />
                </div>
                <table width="100%" border="0" cellspacing="3" cellpadding="0">
                    <tr>
                        <td>
                            <uc9:PositionProfileSummary ID="PositionProfileEntry_positionProfileSummary" runat="server"
                                OnAddSkillEvent="PositionProfileEntry_positionProfileSummary_AddSkillEvent" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table border="0" cellspacing="3" cellpadding="3" align="left">
                                <tr>
                                    <td class="td_padding_top_5" style="padding-left: 10px">
                                        <asp:Button ID="PositionProfileEntry_positionProfileSummary_CreateButton" runat="server"
                                            Text="Create" SkinID="sknButtonId" OnClick="PositionProfileEntry_positionProfileSummary_CreateButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="PositionProfileEntry_positionProfileSummary_RegenerateLinkButton"
                                            runat="server" Text="Regenerate" ToolTip="Click here to regenerate the keywords from the position profile segments"
                                            SkinID="sknPopupLinkButton" OnClick="PositionProfileEntry_positionProfileSummary_RegenerateLinkButton_Click"
                                            Visible="true">
                                        </asp:LinkButton>
                                    </td>
                                    <td class="td_padding_top_5">
                                        <asp:LinkButton ID="PositionProfileEntry_positionProfileSummary_cancelButton" runat="server"
                                            Text="Cancel" SkinID="sknPopupLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="PositionProfileEntry_emailContentConfirmPopupPanel" runat="server"
                Style="display: none" CssClass="popupcontrol_confirm_remove">
                <uc8:ConfirmMsgControl ID="PositionProfileEntry_eMailContentConfirmPopupExtenderControl"
                    runat="server" Type="EmailConfirmType" Title="te" Message="dfgdfg" />
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="PositionProfileEntry_emailContentConfirmModalPopupExtender"
                runat="server" PopupControlID="PositionProfileEntry_emailContentConfirmPopupPanel"
                TargetControlID="Button1" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender>
            <ajaxToolkit:ModalPopupExtender ID="PositionProfileEntry_viewPositionProfileSave_modalpPopupExtender"
                runat="server" TargetControlID="PositionProfileEntry_viewPositionProfileSaveButton"
                PopupControlID="PositionProfileEntry_positionProfileSavePanel" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender>
            <ajaxToolkit:ModalPopupExtender ID="PositionProfileEntry_previewPopupExtender" runat="server"
                PopupControlID="PositionProfileEntry_PreviewPopupPanel" TargetControlID="PositionProfileEntry_hiddenPreviewModalButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender>
            <ajaxToolkit:ModalPopupExtender ID="PositionProfileEntry_ConfirmPopupExtender" runat="server"
                PopupControlID="PositionProfileEntry_ConfirmPopupPanel" TargetControlID="PositionProfileEntry_hiddenConfirmPopupModalButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender>
            <ajaxToolkit:ModalPopupExtender ID="PositionProfileEntry_addSegmentPopupExtender"
                runat="server" PopupControlID="PositionProfileEntry_segmentListPanel" TargetControlID="PositionProfileEntry_hiddenPopupModalButton"
                BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender>
            <ajaxToolkit:CollapsiblePanelExtender ID="PositionProfileEntry_collapsiblePanelExtender"
                runat="Server" TargetControlID="testPanle" Collapsed="True" ExpandControlID="PositionProfileEntry_expandableImg"
                CollapseControlID="PositionProfileEntry_expandableImg" AutoCollapse="False" AutoExpand="False"
                ImageControlID="PositionProfileEntry_expandableImg" ExpandedImage="~/App_Themes/DefaultTheme/Images/pp_btn_out.png"
                CollapsedImage="~/App_Themes/DefaultTheme/Images/pp_btn_in.png" ExpandDirection="Horizontal"
                CollapsedSize="0" ScrollContents="true" />
            <asp:HiddenField ID="PositionProfileEntry_toDeleteSegmentId" runat="server" />
            <asp:HiddenField ID="PositionProfileEntry_editPositionProfileIdHiddenField" runat="server" />
            <asp:HiddenField ID="PositionProfileEntry_summaryHiddenField" runat="server" Value="-1" />
            <asp:HiddenField ID="PositionProfileEntry_buttonCommandName" runat="server" Value="-1" />
            <asp:HiddenField ID="PositionProfileEntry_viewClientNameHiddenField" runat="server" />
            <asp:HiddenField ID="PositionProfileEntry_candidateIdsHiddenField" runat="server" />

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="PositionProfileEntry_bottomSaveButton" />
            <asp:PostBackTrigger ControlID="PositionProfileEntry_bottomCancelLinkButton" />
            <asp:PostBackTrigger ControlID="PositionProfileEntry_topSaveButton" />
            <asp:PostBackTrigger ControlID="PositionProfileEntry_topCancelLinkButton" />
            <%--<asp:PostBackTrigger ControlID="PositionProfileEntry_topEmailButton" />
            <asp:PostBackTrigger ControlID="PositionProfileEntry_bottomEmailButton" />--%>
            <asp:PostBackTrigger ControlID="PositionProfileEntry_topDownloadButton" />
            <asp:PostBackTrigger ControlID="PositionProfileEntry_bottomDownloadButton" />
        </Triggers>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="PositionProfileEntry_clientModalPopUpUpdatePanel" runat="server"
    UpdateMode="Always">
    <ContentTemplate>
        <asp:HiddenField ID="PositionProfileEntry_clientIDHiddenField" runat="server" />
        <span style="display: none">
            <asp:Button ID="PositionProfileEntry_clientDummybutton" runat="server" />
        </span>
        <asp:Panel ID="PositionProfileEntry_clientPaenl" runat="server" CssClass="client_details">
            <uc12:Client ID="PositionProfileEntry_clientControl" runat="server" />
        </asp:Panel>
        <ajaxToolKit:ModalPopupExtender ID="PositionProfileEntry_clientModalpPopupExtender"
            runat="server" TargetControlID="PositionProfileEntry_clientDummybutton" PopupControlID="PositionProfileEntry_clientPaenl"
            BackgroundCssClass="modalBackground">
        </ajaxToolKit:ModalPopupExtender>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="PositionProfileEntry_departmentModalPopUpUpdatePanel" runat="server"
    UpdateMode="Always">
    <ContentTemplate>
        <asp:HiddenField ID="PositionProfileEntry_departIDHiddenField" runat="server" />
        <span style="display: none">
            <asp:Button ID="PositionProfileEntry_departmentDummybutton" runat="server" />
        </span>
        <asp:Panel ID="PositionProfileEntry_departmentPaenl" runat="server" CssClass="client_details">
            <uc11:Department ID="PositionProfileEntry_departmentControl" runat="server" />
        </asp:Panel>
        <ajaxToolKit:ModalPopupExtender ID="PositionProfileEntry_departmentModalpPopupExtender"
            runat="server" TargetControlID="PositionProfileEntry_departmentDummybutton" PopupControlID="PositionProfileEntry_departmentPaenl"
            BackgroundCssClass="modalBackground">
        </ajaxToolKit:ModalPopupExtender>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="PositionProfileEntry_contactModalPopUpUpdatePanel" runat="server"
    UpdateMode="Always">
    <ContentTemplate>
        <span style="display: none">
            <asp:Button ID="PositionProfileEntry_contactDummybutton" runat="server" />
        </span>
        <asp:Panel ID="PositionProfileEntry_contactPaenl" runat="server" CssClass="contact_details">
            <uc13:Contact ID="PositionProfileEntry_contactControl" runat="server" />
        </asp:Panel>
        <ajaxToolKit:ModalPopupExtender ID="PositionProfileEntry_contactModalpPopupExtender"
            runat="server" TargetControlID="PositionProfileEntry_contactDummybutton" PopupControlID="PositionProfileEntry_contactPaenl"
            BackgroundCssClass="modalBackground">
        </ajaxToolKit:ModalPopupExtender>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
