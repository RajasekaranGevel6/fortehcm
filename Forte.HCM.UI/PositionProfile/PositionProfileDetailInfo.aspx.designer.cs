﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.PositionProfile {
    
    
    public partial class PositionProfileDetailInfo {
        
        /// <summary>
        /// PositionProfileDetailInfo_updatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel PositionProfileDetailInfo_updatePanel;
        
        /// <summary>
        /// PositionProfileDetailInfo_topMessageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel PositionProfileDetailInfo_topMessageUpdatePanel;
        
        /// <summary>
        /// PositionProfileDetailInfo_topSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label PositionProfileDetailInfo_topSuccessMessageLabel;
        
        /// <summary>
        /// PositionProfileDetailInfo_topErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label PositionProfileDetailInfo_topErrorMessageLabel;
        
        /// <summary>
        /// PositionProfileWorkflowControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.PositionProfileWorkflowControl PositionProfileWorkflowControl;
        
        /// <summary>
        /// PositionProfileDetailInfo_customizedSegmentsDeleteUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel PositionProfileDetailInfo_customizedSegmentsDeleteUpdatePanel;
        
        /// <summary>
        /// PositionProfileDetailInfo_customizedSegmentsDeleteHiddenButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button PositionProfileDetailInfo_customizedSegmentsDeleteHiddenButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_customizedSegmentsDeletePopupPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PositionProfileDetailInfo_customizedSegmentsDeletePopupPanel;
        
        /// <summary>
        /// PositionProfileDetailInfo_customizedSegmentsDeleteConfirmMsgControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.ConfirmMsgControl PositionProfileDetailInfo_customizedSegmentsDeleteConfirmMsgControl;
        
        /// <summary>
        /// PositionProfileDetailInfo_customizedSegmentsDeleteHiddenFiled control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField PositionProfileDetailInfo_customizedSegmentsDeleteHiddenFiled;
        
        /// <summary>
        /// PositionProfileDetailInfo_customizedSegmentsModalPopupExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender PositionProfileDetailInfo_customizedSegmentsModalPopupExtender;
        
        /// <summary>
        /// PositionProfileDetailInfo_mainTableTd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableCell PositionProfileDetailInfo_mainTableTd;
        
        /// <summary>
        /// PositionProfileDetailInfo_positionRequirement_headerDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl PositionProfileDetailInfo_positionRequirement_headerDiv;
        
        /// <summary>
        /// PositionProfileDetailInfo_editPositionProfileIdHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField PositionProfileDetailInfo_editPositionProfileIdHiddenField;
        
        /// <summary>
        /// PositionProfileDetailInfo_positionProfileNameHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField PositionProfileDetailInfo_positionProfileNameHiddenField;
        
        /// <summary>
        /// PositionProfileDetailInfo_additionalInformationHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField PositionProfileDetailInfo_additionalInformationHiddenField;
        
        /// <summary>
        /// PositionProfileDetailInfo_registeredByHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField PositionProfileDetailInfo_registeredByHiddenField;
        
        /// <summary>
        /// PositionProfileDetailInfo_dateOfRegistrationHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField PositionProfileDetailInfo_dateOfRegistrationHiddenField;
        
        /// <summary>
        /// PositionProfileDetailInfo_submittalDeadLineHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField PositionProfileDetailInfo_submittalDeadLineHiddenField;
        
        /// <summary>
        /// PositionProfileDetailInfo_clientIDHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField PositionProfileDetailInfo_clientIDHiddenField;
        
        /// <summary>
        /// PositionProfileDetailInfo_clientNameHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField PositionProfileDetailInfo_clientNameHiddenField;
        
        /// <summary>
        /// PositionProfileDetailInfo_positionRequirment_Literal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal PositionProfileDetailInfo_positionRequirment_Literal;
        
        /// <summary>
        /// PositionProfileDetailInfo_positionRequirment_upArrowImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileDetailInfo_positionRequirment_upArrowImageButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_positionRequirment_downArrowImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileDetailInfo_positionRequirment_downArrowImageButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_positionRequirementDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl PositionProfileDetailInfo_positionRequirementDiv;
        
        /// <summary>
        /// PositionProfileDetailInfo_requirementControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.RequirementViewerControl PositionProfileDetailInfo_requirementControl;
        
        /// <summary>
        /// PositionProfileDetailInfo_positionDetail_headerDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl PositionProfileDetailInfo_positionDetail_headerDiv;
        
        /// <summary>
        /// PositionProfileDetailInfo_headerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal PositionProfileDetailInfo_headerLiteral;
        
        /// <summary>
        /// PositionProfileDetailInfo_closePositionDetailImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileDetailInfo_closePositionDetailImageButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_upArrow_PositionDetailImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileDetailInfo_upArrow_PositionDetailImageButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_downArrow_PositionDetailImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileDetailInfo_downArrow_PositionDetailImageButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_positionDetailDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl PositionProfileDetailInfo_positionDetailDiv;
        
        /// <summary>
        /// PositionProfileDetailInfo_clientPositionDetailsControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.Segments.ClientPositionDetailsControl PositionProfileDetailInfo_clientPositionDetailsControl;
        
        /// <summary>
        /// PositionProfileDetailInfo_technicalSkill_headerDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl PositionProfileDetailInfo_technicalSkill_headerDiv;
        
        /// <summary>
        /// PositionProfileDetailInfo_technicalSkillLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal PositionProfileDetailInfo_technicalSkillLiteral;
        
        /// <summary>
        /// PositionProfileDetailInfo_technicalSkill_CloseImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileDetailInfo_technicalSkill_CloseImageButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_technicalSkill_upArrowImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileDetailInfo_technicalSkill_upArrowImageButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_technicalSkill_downArrowImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileDetailInfo_technicalSkill_downArrowImageButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_technicalSkillsDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl PositionProfileDetailInfo_technicalSkillsDiv;
        
        /// <summary>
        /// PositionProfileDetailInfo_technicalSkillRequirementControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.Segments.TechnicalSkillRequirement PositionProfileDetailInfo_technicalSkillRequirementControl;
        
        /// <summary>
        /// PositionProfileDetailInfo_verticalBackground_headerDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl PositionProfileDetailInfo_verticalBackground_headerDiv;
        
        /// <summary>
        /// PositionProfileDetailInfo_verticalBackground_titleLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal PositionProfileDetailInfo_verticalBackground_titleLiteral;
        
        /// <summary>
        /// PositionProfileDetailInfo_verticalBackground_closeImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileDetailInfo_verticalBackground_closeImageButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_verticalBackground_upArrowImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileDetailInfo_verticalBackground_upArrowImageButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_verticalBackground_downArrowImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileDetailInfo_verticalBackground_downArrowImageButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_verticalBackgroundRequirementDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl PositionProfileDetailInfo_verticalBackgroundRequirementDiv;
        
        /// <summary>
        /// PositionProfileDetailInfo_verticalBackgroundRequirementControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.Segments.VerticalBackgroundRequirement PositionProfileDetailInfo_verticalBackgroundRequirementControl;
        
        /// <summary>
        /// PositionProfileDetailInfo_rolesRequirment_headerDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl PositionProfileDetailInfo_rolesRequirment_headerDiv;
        
        /// <summary>
        /// PositionProfileDetailInfo_rolesRequirment_Literal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal PositionProfileDetailInfo_rolesRequirment_Literal;
        
        /// <summary>
        /// PositionProfileDetailInfo_rolesRequirment_closeImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileDetailInfo_rolesRequirment_closeImageButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_rolesRequirment_upArrowImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileDetailInfo_rolesRequirment_upArrowImageButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_rolesRequirment_downArrowImageButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton PositionProfileDetailInfo_rolesRequirment_downArrowImageButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_roleRequirementDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl PositionProfileDetailInfo_roleRequirementDiv;
        
        /// <summary>
        /// PositionProfileDetailInfo_roleRequirementControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.Segments.RoleRequirement PositionProfileDetailInfo_roleRequirementControl;
        
        /// <summary>
        /// PositionProfileDetailInfo_mainTable_saveTd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableCell PositionProfileDetailInfo_mainTable_saveTd;
        
        /// <summary>
        /// PositionProfileDetailInfo_saveButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button PositionProfileDetailInfo_saveButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_gotoFirstStep_LinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton PositionProfileDetailInfo_gotoFirstStep_LinkButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_cancelLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton PositionProfileDetailInfo_cancelLinkButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_positionProfileSavePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PositionProfileDetailInfo_positionProfileSavePanel;
        
        /// <summary>
        /// PositionProfileDetailInfo_viewPositionProfileSaveButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button PositionProfileDetailInfo_viewPositionProfileSaveButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_positionProfileSummary control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.PositionProfileSummary PositionProfileDetailInfo_positionProfileSummary;
        
        /// <summary>
        /// PositionProfileDetailInfo_positionProfileSummary_CreateButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button PositionProfileDetailInfo_positionProfileSummary_CreateButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_positionProfileSummary_RegenerateLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton PositionProfileDetailInfo_positionProfileSummary_RegenerateLinkButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_positionProfileSummary_cancelButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton PositionProfileDetailInfo_positionProfileSummary_cancelButton;
        
        /// <summary>
        /// PositionProfileDetailInfo_viewPositionProfileSave_modalpPopupExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender PositionProfileDetailInfo_viewPositionProfileSave_modalpPopupExtender;
        
        /// <summary>
        /// PositionProfileDetailInfo_summaryHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField PositionProfileDetailInfo_summaryHiddenField;
        
        /// <summary>
        /// PositionProfileDetailInfo_buttonCommandName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField PositionProfileDetailInfo_buttonCommandName;
        
        /// <summary>
        /// PositionProfileDetailInfo_ClientInfoControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Forte.HCM.UI.CommonControls.ClientInfoControl PositionProfileDetailInfo_ClientInfoControl;
        
        /// <summary>
        /// PositionProfileDetailInfo_bottomMessageUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel PositionProfileDetailInfo_bottomMessageUpdatePanel;
        
        /// <summary>
        /// PositionProfileDetailInfo_bottomSuccessMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label PositionProfileDetailInfo_bottomSuccessMessageLabel;
        
        /// <summary>
        /// PositionProfileDetailInfo_bottomErrorMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label PositionProfileDetailInfo_bottomErrorMessageLabel;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Forte.HCM.UI.MasterPages.PositionProfileMaster Master {
            get {
                return ((Forte.HCM.UI.MasterPages.PositionProfileMaster)(base.Master));
            }
        }
    }
}
