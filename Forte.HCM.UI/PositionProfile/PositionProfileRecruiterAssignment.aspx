﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PositionProfileMaster.Master"
    CodeBehind="PositionProfileRecruiterAssignment.aspx.cs" Inherits="Forte.HCM.UI.PositionProfile.PositionProfileRecruiterAssignment" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/PositionProfileWorkflowControl.ascx" TagName="PositionProfileWorkflowControl"
    TagPrefix="uc9" %>
<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content ID="PositionProfileRecruiterAssignment_bodyContent" runat="server" ContentPlaceHolderID="PositionProfileMaster_body">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="padding-bottom:5px">
                <uc9:PositionProfileWorkflowControl ID="PositionProfileRecruiterAssignment_workflowControl" runat="server" Visible="true" PageType="RA"/>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="PositionProfileRecruiterAssignment_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="PositionProfileRecruiterAssignment_topSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="PositionProfileRecruiterAssignment_topErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                        <asp:HiddenField ID="PositionProfileRecruiterAssignment_tsSessionKeyHiddenField"
                            runat="server" />
                        <asp:HiddenField ID="PositionProfileRecruiterAssignment_editPositionProfileIdHiddenField"
                            runat="server" />
                        <asp:HiddenField ID="PositionProfileRecruiterAssignment_positionProfileNameHiddenField" runat="server" />
                        <asp:HiddenField ID="PositionProfileRecruiterAssignment_clientNameHiddenField" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <!-- Part 1-->
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="PositionProfileRecruiterAssignment_showRecruiterUpdatePanel"
                                            runat="server">
                                            <ContentTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td style="width:450px">
                                                                        <asp:RadioButton ID="PositionProfileRecruiterAssignment_currentRecruitersRadioButton"
                                                                            Text="Show only recruiters that currently/recently worked on positions requiring similar skills"
                                                                            runat="server" GroupName="RecruiterAssignment" AutoPostBack="true" 
                                                                         OnCheckedChanged="PositionProfileRecruiterAssignment_currentRecruitersRadioButton_CheckedChanged" />
                                                                    </td>
                                                                    <td>
                                                                        <div style="display: block;" runat="server" id="PositionProfileRecruiterAssignment_recentMonthDiv">
                                                                            <asp:UpdatePanel ID="PositionProfileRecruiterAssignment_recentMonthUpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:DropDownList ID="PositionProfileRecruiterAssignment_recentMonthsDropDownList"
                                                                                        runat="server" AutoPostBack="true"
                                                                                        onselectedindexchanged="PositionProfileRecruiterAssignment_recentMonthsDropDownList_SelectedIndexChanged">
                                                                                    </asp:DropDownList>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_10">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButton ID="PositionProfileRecruiterAssignment_earlierRecruitersRadioButton"
                                                                Text="Show only recruiters that have been assigned requirements from this client earlier"
                                                                runat="server" GroupName="RecruiterAssignment" AutoPostBack="true" OnCheckedChanged="PositionProfileRecruiterAssignment_earlierRecruitersRadioButton_CheckedChanged" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_10">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButton ID="PositionProfileRecruiterAssignment_allRecruitersRadioButton"
                                                                Text="Show All" runat="server" GroupName="RecruiterAssignment" AutoPostBack="true"
                                                                OnCheckedChanged="PositionProfileRecruiterAssignment_allRecruitersRadioButton_CheckedChanged" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td>
                                        <table class="panel_bg" cellpadding="0" cellspacing="0" style="height: 80px;">
                                            <tr>
                                                <td valign="top">
                                                    <table width="100%" cellpadding="2" cellspacing="2">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Label ID="PositionProfileRecruiterAssignment_recruitersLabel" runat="server"
                                                                    CssClass="position_profile_recruiter_title" Text="Assigned Recruiters"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Image SkinID="sknPPLineImage" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <div style="height: 90px; overflow: auto; display: block;" runat="server">
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <div id="PositionProfileRecruiterAssignment_profileRecruitersDiv" style="height: 85px;
                                                                                    overflow: auto;" runat="server">
                                                                                    <asp:UpdatePanel ID="PositionProfileRecruiterAssignment_profileRecruitersGridViewUpdatePanel"
                                                                                        runat="server">
                                                                                        <ContentTemplate>
                                                                                            <asp:GridView ID="PositionProfileRecruiterAssignment_profileRecruitersGridView" runat="server"
                                                                                                ShowHeader="false" OnRowCommand="PositionProfileRecruiterAssignment_profileRecruitersGridView_RowCommand"
                                                                                                OnRowDataBound="PositionProfileRecruiterAssignment_profileRecruitersGridView_OnRowDataBound"
                                                                                                SkinID="sknPPOwnersGridView">
                                                                                                <EmptyDataRowStyle CssClass="error_message_text" />
                                                                                                <EmptyDataTemplate>
                                                                                                    <table style="width: 100%">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                No recruiters to display
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </EmptyDataTemplate>
                                                                                                <Columns>
                                                                                                    <asp:TemplateField ItemStyle-Width="8%">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="PositionProfileRecruiterAssignment_profileRecruitersGridView_deleteImageButton"
                                                                                                                runat="server" SkinID="sknPPDeleteIconImageButton" CommandArgument='<%# Eval("UserID") %>'
                                                                                                                CommandName="DeleteRecruiter" Visible='<%# Eval("OwnerType") != null && Eval("OwnerType").ToString()!="POT_OWR" && Eval("OwnerType").ToString()!="POT_CO_OWR" %>' ToolTip="Delete" />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField>
                                                                                                        <ItemTemplate>
                                                                                                                <asp:HiddenField ID="PositionProfileRecruiterAssignment_profileRecruitersGridView_recruiterIDHiddenField" runat="server" Value='<%# Eval("UserID") %>'/>
                                                                                                                <asp:HiddenField ID="PositionProfileRecruiterAssignment_profileRecruitersGridView_recruiterNameHiddenField" runat="server" Value='<%# Eval("FirstName") %>'/>
                                                                                                                <asp:HiddenField ID="PositionProfileRecruiterAssignment_profileRecruitersGridView_recruiterEMailHiddenField" runat="server" Value='<%# Eval("EMail") %>'/>
                                                                                                                <asp:LinkButton ID="PositionProfileRecruiterAssignment_profileRecruitersGridView_nameLinkButton" runat="server" Text='<%# Eval("LastName")==null ? Eval("FirstName") : String.Format("{0} {1}", Eval("FirstName"), Eval("LastName")) %>' ToolTip="Click here to view recruiter profile"></asp:LinkButton>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:AsyncPostBackTrigger ControlID="PositionProfileRecruiterAssignment_recruitersGridView" />
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- -->
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <!-- Part 2-->
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table class="panel_bg" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <table width="100%" cellpadding="4" cellspacing="4">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:Label ID="PositionProfileRecruiterAssignment_recruiterTitleLabel" runat="server"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileRecruiterAssignment_currentRecruitersRadioButton" />
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileRecruiterAssignment_earlierRecruitersRadioButton" />
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileRecruiterAssignment_allRecruitersRadioButton" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Image runat="server" SkinID="sknPPLineImage" Width="100%" Height="2px" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="grid_body_bg">
                                                                <asp:UpdatePanel ID="PositionProfileRecruiterAssignment_recruitersUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <div style="overflow: auto; display: block;" runat="server">
                                                                            <table cellpadding="2" cellspacing="2" width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <div id="PositionProfileRecruiterAssignment_recruitersDiv" style="height: 120px;
                                                                                            overflow: auto;" runat="server">
                                                                                            <asp:UpdatePanel ID="PositionProfileRecruiterAssignment_recruitersGridViewUpdatePanel"
                                                                                                runat="server">
                                                                                                <ContentTemplate>
                                                                                                    <asp:GridView ID="PositionProfileRecruiterAssignment_recruitersGridView" 
                                                                                                        OnRowCreated="PositionProfileRecruiterAssignment_RowCreated"
                                                                                                        OnSorting="PositionProfileRecruiterAssignment_recruitersGridView_Sorting"
                                                                                                        OnRowCommand="PositionProfileRecruiterAssignment_recruitersGridView_RowCommand"
                                                                                                        OnRowDataBound="PositionProfileRecruiterAssignment_recruitersGridView_OnRowDataBound"
                                                                                                        runat="server" AllowSorting="true">
                                                                                                        <EmptyDataRowStyle CssClass="error_message_text" />
                                                                                                        <EmptyDataTemplate>
                                                                                                            <table style="width: 100%">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        No recruiters to display
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </EmptyDataTemplate>
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField ItemStyle-Width="8%">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:RadioButton ID="PositionProfileRecruiterAssignment_recruitersGridView_viewPositionRadioButton"
                                                                                                                        runat="server" CommandArgument='<%# Eval("UserID") %>' ToolTip="Click here to view position assignments" AutoPostBack="true"
                                                                                                                        OnCheckedChanged="PositionProfileRecruiterAssignment_recruitersGridView_viewPositionRadioButton_CheckedChanged" />
                                                                                                                    <asp:ImageButton ID="PositionProfileRecruiterAssignment_recruitersGridView_addRecruiterImageButton"
                                                                                                                        runat="server" SkinID="sknPPAddRecruiterImageButton" CommandArgument='<%# Eval("UserID") %>'
                                                                                                                        CommandName="AssignRecruiter" ToolTip="Assign Recruiter" />
                                                                                                                    <asp:HiddenField ID="PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField"
                                                                                                                        Value='<%# Eval("UserID") %>' runat="server" />
                                                                                                                         <asp:HiddenField ID="PositionProfileRecruiterAssignment_recruitersGridView_recruiterNameHiddenField"
                                                                                                                        Value='<%# Eval("RecruiterFirstName") %>' runat="server" />
                                                                                                                        <asp:HiddenField ID="PositionProfileRecruiterAssignment_recruitersGridView_recruiterEMailHiddenField"
                                                                                                                        Value='<%# Eval("RecruiterEMail") %>' runat="server" />
                                                                                                                </ItemTemplate>
                                                                                                                <ItemStyle Width="8%" />
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="Recruiter Name" SortExpression="RECRUITER_NAME">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:LinkButton ID="PositionProfileRecruiterAssignment_recruitersGridView_nameLinkButton" runat="server" Text='<%# Eval("RecruiterName")  %>' ToolTip="Click here to view recruiter profile"></asp:LinkButton>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="No. of current active Position Assignments" SortExpression="ACTIVE_POSITION_ASSIGNMENTS">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:LinkButton ID="PositionProfileRecruiterAssignment_recruitersGridView_activePositionAssignmentLinkButton" 
                                                                                                                    Text='<%# Eval("ActivePositionAssignments") %>' runat="server" ToolTip="Click here to view position assignments" CommandName="ViewPositionsProfile">
                                                                                                                    </asp:LinkButton>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="Date of most recent Position Assignment" SortExpression="RECENT_POSITION_ASSIGNMENT">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="PositionProfileRecruiterAssignment_recruitersGridView_recentPositionAssignmentLabel"
                                                                                                                        runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("RecentPositionAssignment"))) %>'></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                    </asp:GridView>
                                                                                                </ContentTemplate>
                                                                                            </asp:UpdatePanel>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <uc2:PageNavigator ID="PositionProfileRecruiterAssignment_recruitersPageNavigator" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileRecruiterAssignment_currentRecruitersRadioButton" />
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileRecruiterAssignment_earlierRecruitersRadioButton" />
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileRecruiterAssignment_allRecruitersRadioButton" />
                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileRecruiterAssignment_recentMonthsDropDownList" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- -->
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <!-- Part 3 -->
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="PositionProfileRecruiterAssignment_showRecruitersDetailedUpdatePanel" runat="server">
                            <ContentTemplate>
                            <div style="display: none" runat="server" id="PositionProfileRecruiterAssignment_showRecruitersDetailedViewDiv">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <table class="panel_bg" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <table width="100%" cellpadding="4" cellspacing="4">
                                                            <tr>
                                                                <td align="left">
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td align="left" style="width:300px">
                                                                                <asp:UpdatePanel ID="PositionProfileRecruiterAssignment_recruitersNameUpdatePanel"
                                                                                    runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:Label ID="PositionProfileRecruiterAssignment_recruiterNameLabel" runat="server"
                                                                                            SkinID="sknRecruiterNameLabel"></asp:Label>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileRecruiterAssignment_recruitersGridView" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                            <td align="right">
                                                                                <table cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="PositionProfileRecruiterAssignment_positionProfileStatusLabel" SkinID="sknLabelText"
                                                                                                Text="Position Status" runat="server"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width:10px"> </td>
                                                                                        <td>
                                                                                            <asp:UpdatePanel ID="PositionProfileRecruiterAssignment_positionProfileStatusUpdatePanel"
                                                                                                runat="server">
                                                                                                <ContentTemplate>
                                                                                                    <asp:DropDownList ID="PositionProfileRecruiterAssignment_positionProfileStatusDropDown"
                                                                                                        runat="server" AutoPostBack="true" OnSelectedIndexChanged="PositionProfileRecruiterAssignment_positionProfileStatusDropDown_SelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                </ContentTemplate>
                                                                                            </asp:UpdatePanel>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Image runat="server" SkinID="sknPPLineImage" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="grid_body_bg">
                                                                    <asp:UpdatePanel ID="PositionProfileRecruiterAssignment_recruitersDetailedViewUpdatePanel"
                                                                        runat="server">
                                                                        <ContentTemplate>
                                                                            <div style="overflow: auto; display: block;" runat="server">
                                                                                <table cellpadding="2" cellspacing="2" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div id="PositionProfileRecruiterAssignment_recruitersDetailedViewDiv" style="height: 180px;
                                                                                                overflow: auto;" runat="server">
                                                                                                <asp:UpdatePanel ID="PositionProfileRecruiterAssignment_recruitersDetailedViewGridViewUpdatePanel"
                                                                                                    runat="server">
                                                                                                    <ContentTemplate>
                                                                                                        <asp:GridView ID="PositionProfileRecruiterAssignment_recruitersDetailedViewGridView"
                                                                                                            runat="server"
                                                                                                            OnRowDataBound="PositionProfileRecruiterAssignment_recruitersDetailedView_OnRowDataBound">
                                                                                                            <EmptyDataRowStyle CssClass="error_message_text" />
                                                                                                            <EmptyDataTemplate>
                                                                                                                <table style="width: 100%">
                                                                                                                    <tr style="height: 80px">
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            No position to display
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </EmptyDataTemplate>
                                                                                                            <Columns>
                                                                                                                <asp:TemplateField ItemStyle-Width="5%">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:ImageButton ID="PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_previewImageButton"
                                                                                                                            runat="server" SkinID="sknPreviewImageButton" CommandArgument='<%# Eval("PositionProfileID") %>'
                                                                                                                            CommandName="PreviewProfile" ToolTip="Click here to preview position profile detail" />
                                                                                                                        <asp:HiddenField ID="PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_positionProfileIDHiddenField"
                                                                                                                            runat="server" Value='<%# Eval("PositionProfileID") %>' />
                                                                                                                            <asp:HiddenField ID="PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_clientIDHiddenField" 
                                                                                                                            runat="server" Value='<%# Eval("ClientID") %>'/>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                                <asp:TemplateField HeaderText="Position Profile" ItemStyle-Width="25%">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:HyperLink ID="PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_positionNameHyperLink"
                                                                                                                            runat="server" Text='<%# Eval("PositionProfileName") %>' CommandArgument='<%# Eval("PositionProfileID") %>'
                                                                                                                            CommandName="ReviewProfile" ToolTip="Click here to view position profile review" SkinID="sknLabelFieldTextHyperLink" Target="_blank"></asp:HyperLink>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                                <asp:TemplateField HeaderText="Client Name" ItemStyle-Width="20%">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:LinkButton ID="PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_clientNameLinkButton"  runat="server" 
                                                                                                                            Text='<%# Eval("ClientName") %>' ToolTip="Click here to view client details"></asp:LinkButton>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                                <asp:TemplateField HeaderText="Assigned<br>Date" ItemStyle-Width="10%">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:Label ID="PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_assignedDateLabel"
                                                                                                                            runat="server" Text='<%# Eval("AssignedDateString") %>'></asp:Label>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                                <asp:TemplateField HeaderText="Position<br>Status" ItemStyle-Width="10%">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:Label ID="PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_statusLabel"
                                                                                                                            runat="server" Text='<%# Eval("PositionProfileStatusName") %>'></asp:Label>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                                <asp:TemplateField HeaderStyle-Wrap="true" ItemStyle-Width="10%" HeaderText="Candidates<br>Associated">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:Label ID="PositionProfileRecruiterAssignment_noOfCandidatesAssociatedByRecruiter" Text='<%# Eval("CandidatesAssociatedByRecruiter") %>'  runat="server"></asp:Label>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                                <asp:TemplateField HeaderStyle-Wrap="true" ItemStyle-Width="10%" HeaderText="Candidates<br>Submitted">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:Label ID="PositionProfileRecruiterAssignment_noOfCandidatesSubmittedToClientLabel" Text='<%# Eval("CandidatesSubmittedToClientByRecruiter") %>' runat="server">
                                                                                                                        </asp:Label>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                                <asp:TemplateField HeaderText="Position Profile Owner" ItemStyle-Width="10%">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:Label ID="PositionProfileRecruiterAssignemnt_profileOwnerLabel" Text='<%# Eval("PositionProfileOwner") %>' runat="server"></asp:Label>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                            </Columns>
                                                                                                        </asp:GridView>
                                                                                                    </ContentTemplate>
                                                                                                    <Triggers>
                                                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileRecruiterAssignment_recruitersGridView" />
                                                                                                        <asp:AsyncPostBackTrigger ControlID="PositionProfileRecruiterAssignment_positionProfileStatusDropDown" />
                                                                                                    </Triggers>
                                                                                                </asp:UpdatePanel>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:HiddenField ID="PositionProfileRecruiterAssignment_recruiterIDHiddenField" runat="server" />
                                                                                            <uc2:PageNavigator ID="PositionProfileRecruiterAssignment_recruitersDetailedViewPageNavigator"
                                                                                                runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="PositionProfileRecruiterAssignment_recruitersGridView" />
                            </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width:130px">
                                        <asp:Button ID="PositionProfileRecruiterAssignment_saveContinueButton" runat="server"
                                            Text="Save and Continue" SkinID="sknButtonId" Height="26px" OnClick="PositionProfileRecruiterAssignment_saveContinueButton_Click" />
                                    </td>
                                    <td  style="width:180px">
                                        <asp:LinkButton ID="PositionProfileRecruiterAssignment_goToWorkFlowLinkButton" 
                                            SkinID="sknDashboardLinkButton" runat="server" 
                                            ToolTip="Click here to return to workflow selection" 
                                            onclick="PositionProfileRecruiterAssignment_goToWorkFlowLinkButton_Click" Text="Return to Workflow Selection"> </asp:LinkButton>
                                    </td>
                                    <td align="left">
                                        <asp:LinkButton ID="PositionProfileRecruiterAssignment_cancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="PositionProfileRecruiterAssignment_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="PositionProfileRecruiterAssignment_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label><asp:Label ID="PositionProfileRecruiterAssignment_bottomErrorMessageLabel"
                                runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
