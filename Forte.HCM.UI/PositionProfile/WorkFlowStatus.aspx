﻿<%@ Page Title="Work Flow Status" Language="C#" MasterPageFile="~/MasterPages/PositionProfileMaster.Master"
 AutoEventWireup="true" CodeBehind="WorkFlowStatus.aspx.cs" Inherits="Forte.HCM.UI.PositionProfile.WorkFlowStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PositionProfileMaster_body" runat="server">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td class="msg_align">                
                    <asp:UpdatePanel ID="WorkFlowStatus_topMessageUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="WorkFlowStatus_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="WorkFlowStatus_topErrorMessageLabel" runat="server"  SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate> 
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="tab_body_bg" width="100%">
                     <table cellpadding="0" cellspacing="0" border="0" width="100%" > 
                          <tr>
                            <td rowspan="4" align="center" width="100%">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td style="padding-top:10px" width="50%">
                                            <div style="border:solid 1px #ccc;width:100%" >
                                                <table cellpadding="1" cellspacing="1" border="0"  align="center" width="100%">
                                                    <tr>
                                                        <td colspan="3" align="center">
                                                            <asp:Label ID="WorkFlowStatus_recruiterAssigment_TitleLabel" runat="server" SkinID="sknLabelAssessorFieldHeaderText" Text="Recruiter <br />Assignment"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                           <asp:Label ID="WorkFlowStatus_taskOwner_recruiterAssigment_TitleLabel" runat="server" Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label> 
                                                         </td>
                                                        <td colspan="2">
                                                            <div style="word-break:break-all;word-wrap:break-word;color:#0066D6;font-size:11px;width:60px" 
                                                                    id="WorkFlowStatus_taskOwner_recruiterAssigment_Div" runat="server" >  
                                                                <span>Paaul Selvakumar</span>
                                                            </div>
                                                             
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="WorkFlowStatus_assignedRecruiters_recruiterAssigment_TitleLabel" runat="server" Text="Assigned Recruiters : " SkinID="sknHomePageLabel"></asp:Label> 
                                                        </td>
                                                        <td>
                                                            <asp:HyperLink ID="WorkFlowStatus_assignedRecruiters_recruiterAssigment_HyperLink" runat="server" Text="3" SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                        </td>
                                                        <td>
                                                            <asp:HyperLink ID="WorkFlowStatus_assignedRecruiters_edit_recruiterAssigment_HyperLink"  SkinID="sknLabelFieldTextHyperLink" runat="server">edit</asp:HyperLink>
                                                        </td>
                                                    </tr> 
                                                </table>
                                            </div>
                                        </td>
                                        <td style="padding-top:10px" width="50%">
                                             <div style="border:solid 1px #ccc;width:100%" >
                                                <table cellpadding="1" cellspacing="1" border="0" align="center" width="100%">
                                                    <tr>
                                                        <td colspan="2" align="center">
                                                            <asp:Label ID="WorkFlowStatus_candidateAssociate_TitleLabel" runat="server" SkinID="sknLabelAssessorFieldHeaderText" Text="Candidate <br /> Association"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                           <asp:Label ID="WorkFlowStatus_candidateAssociate_TaskOwner_TitleLabel" runat="server" Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label> 
                                                         </td>
                                                        <td>
                                                             <div style="word-break:break-all;word-wrap:break-word;color:#0066D6;font-size:11px;width:60px" 
                                                                    id="WorkFlowStatus_candidateAssociate_TaskOwner_Div" runat="server" >  
                                                                <span>Paaul Selvakumar</span>
                                                            </div> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                            <asp:Label ID="WorkFlowStatus_candidateAssociate_TaskOwner_CandidatesCountLabel" runat="server" Text="Number of candidates : " SkinID="sknHomePageLabel"></asp:Label> 
                                                        </td>
                                                        <td>
                                                            <asp:HyperLink ID="WorkFlowStatus_candidateAssociate_TaskOwner_CandidatesCountHyperLink" runat="server" Text="10" SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                        </td>
                                                    </tr> 
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td> 
                          </tr> 

                          <tr>
                            <td colspan="2" style="padding:15px">
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td>
                                            <div style="padding:10px;border:solid 1px #ccc;" >
                                                <table cellpadding="2" cellspacing="2" border="0" align="center" width="100%">
                                                    <tr>
                                                        <td colspan="3" align="center">
                                                            <asp:Label ID="Label5" runat="server" SkinID="sknLabelAssessorFieldHeaderText" Text="Interview Creation"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                           <asp:Label ID="Label6" runat="server" Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label> 
                                                         </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="Label7" runat="server" 
                                                            Text="Paaul" style="word-break:break-all;word-wrap:break-word;white-space:pre"  SkinID="sknLabelAssessorFieldText" ></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                            <asp:Label ID="Label8" runat="server" Text="Interview : " SkinID="sknHomePageLabel"></asp:Label> 
                                                        </td>
                                                        <td>
                                                            <asp:HyperLink ID="HyperLink2" runat="server" Text="Flex Testing" SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                        </td>
                                                        <td>
                                                             
                                                        </td>
                                                    </tr> 
                                                </table>
                                            </div>
                                        </td>

                                        <td>
                                            <div style="padding:10px;border:solid 1px #ccc;" >
                                                <table cellpadding="2" cellspacing="2" border="0" align="center" width="100%">
                                                    <tr>
                                                        <td colspan="3" align="center">
                                                            <asp:Label ID="Label13" runat="server" SkinID="sknLabelAssessorFieldHeaderText" Text="Interview Session Creation"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                           <asp:Label ID="Label14" runat="server" Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label> 
                                                         </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="Label15" runat="server" 
                                                            Text="Paaul" style="word-break:break-all;word-wrap:break-word;white-space:pre"  SkinID="sknLabelAssessorFieldText" ></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                            <asp:Label ID="Label16" runat="server" Text="Number of Sessions : " SkinID="sknHomePageLabel"></asp:Label> 
                                                        </td>
                                                        <td>
                                                            <asp:HyperLink ID="HyperLink6" runat="server" Text="10" SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                        </td>
                                                        <td>
                                                             
                                                        </td>
                                                    </tr> 
                                                </table>
                                            </div>
                                        </td>

                                        <td>
                                            <div style="padding:10px;border:solid 1px #ccc;" >
                                                <table cellpadding="2" cellspacing="2" border="0" align="center" width="100%">
                                                    <tr>
                                                        <td colspan="3" align="center">
                                                            <asp:Label ID="Label17" runat="server" SkinID="sknLabelAssessorFieldHeaderText" Text="Interview Scheduling"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                           <asp:Label ID="Label18" runat="server" Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label> 
                                                         </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="Label19" runat="server" 
                                                            Text="Paaul" style="word-break:break-all;word-wrap:break-word;white-space:pre"  SkinID="sknLabelAssessorFieldText" ></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                            <asp:Label ID="Label20" runat="server" Text="Scheduled : " SkinID="sknHomePageLabel"></asp:Label> 
                                                        </td>
                                                        <td>
                                                            <asp:HyperLink ID="HyperLink8" runat="server" Text="3" SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                        </td>
                                                        <td>
                                                         <asp:Label ID="Label33" runat="server" Text="Completed : " SkinID="sknHomePageLabel"></asp:Label> 
                                                            <asp:HyperLink ID="HyperLink9" SkinID="sknLabelFieldTextHyperLink" runat="server">1</asp:HyperLink>
                                                        </td>
                                                    </tr> 
                                                </table>
                                            </div>
                                        </td>

                                        <td>
                                            <div style="padding:10px;border:solid 1px #ccc;" >
                                                <table cellpadding="2" cellspacing="2" border="0" align="center" width="100%">
                                                    <tr>
                                                        <td colspan="3" align="center">
                                                            <asp:Label ID="Label21" runat="server" SkinID="sknLabelAssessorFieldHeaderText" Text="Interview Assessment"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                           <asp:Label ID="Label22" runat="server" Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label> 
                                                         </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="Label23" runat="server" 
                                                            Text="Paaul" style="word-break:break-all;word-wrap:break-word;white-space:pre"  SkinID="sknLabelAssessorFieldText" ></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                            <asp:Label ID="Label24" runat="server" Text="Assigned Candidates : " SkinID="sknHomePageLabel"></asp:Label> 
                                                        </td>
                                                        <td>
                                                            <asp:HyperLink ID="HyperLink10" runat="server" Text="3" SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                        </td>
                                                        <td>  </td>
                                                    </tr> 
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                          </tr>
                          
                          <tr>
                            <td class="td_height_5">&nbsp;</td>
                          </tr>
                            
                          <tr>
                            <td colspan="2"  style="padding:15px">
                                  <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                           <td>
                                            <div style="padding:10px;border:solid 1px #ccc;" >
                                                <table cellpadding="2" cellspacing="2" border="0" align="center" width="100%">
                                                    <tr>
                                                        <td colspan="3" align="center">
                                                            <asp:Label ID="Label9" runat="server" SkinID="sknLabelAssessorFieldHeaderText" Text="Test Creation"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                           <asp:Label ID="Label10" runat="server" Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label> 
                                                         </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="Label11" runat="server" 
                                                            Text="Paaul" style="word-break:break-all;word-wrap:break-word;white-space:pre"  SkinID="sknLabelAssessorFieldText" ></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                            <asp:Label ID="Label12" runat="server" Text="Interview : " SkinID="sknHomePageLabel"></asp:Label> 
                                                        </td>
                                                        <td>
                                                            <asp:HyperLink ID="HyperLink3" runat="server" Text="Flex Testing" SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                        </td>
                                                        <td>
                                                             
                                                        </td>
                                                    </tr> 
                                                </table>
                                            </div>
                                        </td>

                                        <td>
                                            <div style="padding:10px;border:solid 1px #ccc;" >
                                                <table cellpadding="2" cellspacing="2" border="0" align="center" width="100%">
                                                    <tr>
                                                        <td colspan="3" align="center">
                                                            <asp:Label ID="Label25" runat="server" SkinID="sknLabelAssessorFieldHeaderText" Text="Test Session Creation"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                           <asp:Label ID="Label26" runat="server" Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label> 
                                                         </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="Label27" runat="server" 
                                                            Text="Paaul" style="word-break:break-all;word-wrap:break-word;white-space:pre"  SkinID="sknLabelAssessorFieldText" ></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                            <asp:Label ID="Label28" runat="server" Text="Number of Sessions : " SkinID="sknHomePageLabel"></asp:Label> 
                                                        </td>
                                                        <td>
                                                            <asp:HyperLink ID="HyperLink4" runat="server" Text="10" SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                        </td>
                                                        <td>
                                                             
                                                        </td>
                                                    </tr> 
                                                </table>
                                            </div>
                                        </td>

                                        <td>
                                            <div style="padding:10px;border:solid 1px #ccc;" >
                                                <table cellpadding="2" cellspacing="2" border="0" align="center" width="100%">
                                                    <tr>
                                                        <td colspan="3" align="center">
                                                            <asp:Label ID="Label29" runat="server" SkinID="sknLabelAssessorFieldHeaderText" Text="Test Scheduling"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                           <asp:Label ID="Label30" runat="server" Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label> 
                                                         </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="Label31" runat="server" 
                                                            Text="Paaul" style="word-break:break-all;word-wrap:break-word;white-space:pre"  SkinID="sknLabelAssessorFieldText" ></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="60%">
                                                            <asp:Label ID="Label32" runat="server" Text="Scheduled : " SkinID="sknHomePageLabel"></asp:Label> 
                                                        </td>
                                                        <td>
                                                            <asp:HyperLink ID="HyperLink5" runat="server" Text="3" SkinID="sknLabelFieldTextHyperLink"></asp:HyperLink>
                                                        </td>
                                                        <td>
                                                         <asp:Label ID="Label34" runat="server" Text="Completed : " SkinID="sknHomePageLabel"></asp:Label> 
                                                            <asp:HyperLink ID="HyperLink7" SkinID="sknLabelFieldTextHyperLink" runat="server">1</asp:HyperLink>
                                                        </td>
                                                    </tr> 
                                                </table>
                                            </div>
                                        </td>

                                            <td>&nbsp;</td>
                                        </tr>
                                   </table>
                            </td>
                          </tr>
                     </table> 
                </td>
            </tr>
            <tr>
                <td class="msg_align">                
                    <asp:UpdatePanel ID="WorkFlowStatus_bottomMessageUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="WorkFlowStatus_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="WorkFlowStatus_bottomErrorMessageLabel" runat="server"  SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate> 
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
</asp:Content>
