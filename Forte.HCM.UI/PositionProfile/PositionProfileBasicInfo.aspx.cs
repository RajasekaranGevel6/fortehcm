﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PositionProfileBasicInfo.aspx.cs
// File that represents the user interface layout and functionalities
// for the PositionProfileBasicInfo page. This page helps in searching for 
// clients by providing client name.
// This class inherits the Forte.HCM.UI.Common.PageBase class.

#endregion

#region Directives

using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Web.UI;
using System.Linq;
using System.Reflection;
using System.Configuration;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.Segments;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

using Resources;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.UI.PositionProfile
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the PositionProfileBasicInfo page. This page helps in searching for 
    /// clients by providing search client name.
    /// This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class PositionProfileBasicInfo : PageBase
    {
        #region Declaration                                                    
        
        List<SegmentDetail> segmentDetailList;

        /// <summary>
        /// A <see cref="int"/> that hold the position profile ID.
        /// </summary>
        private int positionProfileID = 0;

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="positionProfileDetail">
        /// A <see cref="PositionProfileDetail"/> that holds the position profile detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegate(PositionProfileDetail positionProfileDetail, EntityType entityType);

        #endregion Declaration

        #region Event Handler                                                  

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set the page title
                Master.SetPageCaption("Position Profile Basic Info");

                // Check if position profile ID is passed.
                if (!Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                {
                    // Get position profile ID.
                    int.TryParse(Request.QueryString["positionprofileid"], out positionProfileID);
                }
                
                // Check if user is having permission for this page.
                if (positionProfileID!=0)
                {
                    if (!base.HasPositionProfileOwnerRights(positionProfileID, base.userID,
                        new List<string> { Constants.PositionProfileOwners.OWNER,
                            Constants.PositionProfileOwners.CO_OWNER }))
                    {
                        // Redirect to review page.
                        Response.Redirect(@"~\PositionProfile\PositionProfileReview.aspx?m=1&s=0&positionprofileid=" +
                            positionProfileID, false);
                    }
                }

                LoadValues();

                if (!IsPostBack)
                {
                    PositionProfileBasicInfo_searchClientImageButton.Attributes.Add("onclick", "javascript:return LoadClient('"
                    + PositionProfileBasicInfo_clientIDHiddenField.ClientID + "','" 
                    + PositionProfileBasicInfo_clientNameHiddenField.ClientID + "','" 
                    + PositionProfileBasicInfo_refreshDepartmentButton.ClientID + "')");

                    PositionProfileBasicInfo_coOwnerImageButton.Attributes.Add("onclick", "return SearchOwners('Y','"
                        + PositionProfileBasicInfo_coOwnerImageButton.ClientID + "','CO','"
                        + positionProfileID + "','" + Constants.PositionProfileOwners.CO_OWNER + "')");

                    ViewState["STANDARD_FORM_SEGMENTS"] = null;
                    ViewState["CUSTOMIZED_FORM_SEGMENTS"] = null;
                    Session["SELECTED_USER_LIST"] = null;

                    if (positionProfileID!=0)
                    {
                        PositionProfileBasicInfo_editPositionProfileIdHiddenField.Value = positionProfileID.ToString();
                        BindPositionProfile();
                        LoadCustomizedSegmentsByPositionProfileID(positionProfileID);
                        PositionProfileBasicInfo_standardFormSegmentDiv.Style["display"] = "none";
                        PositionProfileBasicInfo_customizedSegmentsFormDiv.Style["display"] = "none";
                        PositionProfileBasicInfo_customizedSegmentsGridViewDiv.Style["display"] = "block";

                        PositionProfileBasicInfo_addSegmentLinkButton.Visible = true;
                        PositionProfileBasicInfo_segmentsLinkButton.Text = "Load Customized Segment";
                        PositionProfileBasicInfo_segmentsLinkButton.ToolTip = string.Empty;
                        PositionProfileBasicInfo_segmentsLinkButton.Visible = false;
                        PositionProfileBasicInfo_customizedSegmentsImageButtion.Visible = false;

                        PositionProfileBasicInfo_profileIDLabel.Visible = true;
                    }
                    else
                    {
                        if (!Utility.IsNullOrEmpty(Request.QueryString.Get("pp_session_key")))
                        {
                            string sessionkey = Request.QueryString.Get("pp_session_key");
                            string candidateIds = string.Empty;
                            string keywords = string.Empty;

                            new PositionProfileBLManager().GetPositionProfileCandidateTempIds(sessionkey, out candidateIds, out keywords);
                            PositionProfileBasicInfo_positionProfileNameTextBox.Text = keywords;
                        }

                        LoadCustomizedSegments();
                        LoadStandardFormSegments();
                        PositionProfileBasicInfo_customizedSegmentsDiv.Style["display"] = "block";
                        PositionProfileBasicInfo_standardFormSegmentDiv.Style["display"] = "none";
                        PositionProfileBasicInfo_addSegmentLinkButton.Visible = false;
                        PositionProfileBasicInfo_profileIDLabel.Visible = false;
                        PositionProfileBasicInfo_segmentsLinkButton.Visible = true;
                        PositionProfileBasicInfo_customizedSegmentsImageButtion.Visible = true;
                    }

                    if (CanCreatePositionProfileUsage(true))
                    {
                        base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                             PositionProfileBasicInfo_bottomErrorMessageLabel,
                             "You have reached the maximum limit based your subscription plan. Cannot create more Job descriptions");
                    }

                    //Bind Postion profile owners
                    BindPositionProfileCoOwners();

                    //Assing parent page navigation links inputs
                    AssignParentPageNavigationLink();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                    PositionProfileBasicInfo_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileBasicInfo_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileBasicInfo_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                    PositionProfileBasicInfo_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler to show the selected client name in a datalist
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileBasicInfo_addClientImageButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                //By default hide department and contact details
                PositionProfileBasicInfo_clientControl.CheckBoxDepartmentProperty = false;
                PositionProfileBasicInfo_clientControl.CheckBoxContactProperty = false;

                //Hide department and contact div
                PositionProfileBasicInfo_clientControl.HideDepartmentDiv = "none";
                PositionProfileBasicInfo_clientControl.HideContactDiv = "none";
                
                PositionProfileBasicInfo_clientControl.ClientDataSource = null;
                PositionProfileBasicInfo_clientControl.DepartmentDataSource = null;
                PositionProfileBasicInfo_clientControl.ContactDataSource = null;

                PositionProfileBasicInfo_clientControl.ClientAttributesDataSource = 
                    new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENT);

                PositionProfileBasicInfo_clientControl.DepartmenAttributesDataSource = 
                    new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTDEPARTMENT);

                PositionProfileBasicInfo_clientControl.ContactAttributesDataSource = 
                    new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTCONTACTS);
                

                PositionProfileBasicInfo_clientControl.TenantID = base.tenantID;
                PositionProfileBasicInfo_clientControl.UserID = base.userID;
                PositionProfileBasicInfo_clientModalpPopupExtender.Show();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                    PositionProfileBasicInfo_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        
    
        /// <summary>
        /// Handler to show the selected client name in a datalist
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        //protected void PositionProfileBasicInfo_addCoOwnersButton_Click(object sender, EventArgs e)
        protected void PositionProfileBasicInfo_coOwnerImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int loggedOwnerID = ((UserDetail)Session["USER_DETAIL"]).UserID;

                List<UserDetail> users = null;
                if (ViewState["USER_LIST"] == null)
                    users = new List<UserDetail>();
                else
                    users = (List<UserDetail>)ViewState["USER_LIST"];

                List<UserDetail> selectedUsers = null;

                if (Session["SELECTED_USER_LIST"] == null)
                    selectedUsers = new List<UserDetail>();
                else
                    selectedUsers = (List<UserDetail>)Session["SELECTED_USER_LIST"];

                 if (selectedUsers != null && selectedUsers.Count != 0)
                 {
                     foreach (UserDetail selectedUser in selectedUsers)
                     {
                         if(loggedOwnerID == Convert.ToInt32(selectedUser.UserID))
                             continue;

                         if (!Utility.IsNullOrEmpty(users) && users.Count != 0 && 
                             users.Any(res => res.UserID == Convert.ToInt32(selectedUser.UserID)))
                             continue;

                         UserDetail user = new UserDetail();
                         user.UserID = Convert.ToInt32(selectedUser.UserID);
                         user.FirstName = selectedUser.FirstName.Trim();
                         user.LastName = selectedUser.LastName.Trim();
                         user.Email = selectedUser.Email;
                         user.OwnerType = Constants.PositionProfileOwners.CO_OWNER;
                         users.Add(user);

                         if (positionProfileID != 0)
                         {
                             new PositionProfileBLManager().InsertPositionProfileOwner(positionProfileID,
                                 Constants.PositionProfileOwners.CO_OWNER, Convert.ToInt32(selectedUser.UserID), base.userID);
                             
                             try
                             {
                                 // Compose position profile detail.
                                 PositionProfileDetail positionProfileDetail= new PositionProfileDetail();
                                 positionProfileDetail.PositionProfileID = positionProfileID;
                                 positionProfileDetail.TaskOwnerEMail = selectedUser.Email;
                                 positionProfileDetail.TaskOwnerName = selectedUser.FirstName;
                                 positionProfileDetail.TaskOwnerType = Constants.PositionProfileOwners.CO_OWNER;
                                 positionProfileDetail.PositionProfileName = PositionProfileBasicInfo_positionProfileNameTextBox.Text;

                                 if (!Utility.IsNullOrEmpty(PositionProfileBasicInfo_clientNameTextBox.Text))
                                 {
                                     string []clientValues = PositionProfileBasicInfo_clientNameTextBox.Text.Trim().Split('|');
                                     if (clientValues != null && clientValues.Length > 0)
                                         positionProfileDetail.ClientName = clientValues[0].Trim();
                                 }

                                 // Sent alert mail to the associated user asynchronously.
                                 AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendStatusChangeAlertEmail);
                                 IAsyncResult result = taskDelegate.BeginInvoke(positionProfileDetail, 
                                     EntityType.AssociatedWithPositionProfile, new AsyncCallback(SendAlertEmailCallBack), taskDelegate);
                             }
                             catch (Exception exp)
                             {
                                 Logger.ExceptionLog(exp);
                             }
                         }
                     }
                }

                ViewState["USER_LIST"] = users;
                PositionProfileBasicInfo_coOwnerGridView.DataSource = users;
                PositionProfileBasicInfo_coOwnerGridView.DataBind();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                    PositionProfileBasicInfo_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handler to show the selected client name in a datalist
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileBasicInfo_clientNameTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox ClientInfoControl_clientNameTextBox1 = (TextBox)sender;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(ClientInfoControl_clientNameTextBox1.Text))
                {
                    base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                     PositionProfileBasicInfo_bottomErrorMessageLabel,
                     Resources.HCMResource.SearchClientControl_ClientCannotBeEmpty);
                }
                else
                {
                    LoadDeartmentContact(ClientInfoControl_clientNameTextBox1.Text);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                   PositionProfileBasicInfo_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the 
        /// PositionProfileBasicInfo_departmentCheckBoxList_SelectedIndexChanged control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void PositionProfileBasicInfo_departmentCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBoxList checkBoxList = (CheckBoxList)sender;
                StringBuilder selectedPropertyStringBuilder = new StringBuilder();
                for (int i = 0; i < checkBoxList.Items.Count; i++)
                {
                    if (checkBoxList.Items[i].Selected)
                    {
                        selectedPropertyStringBuilder.Append(checkBoxList.Items[i].Text);
                        selectedPropertyStringBuilder.Append(",");
                    }
                }
                PositionProfileBasicInfo_departmentTextBox.Text = selectedPropertyStringBuilder.ToString().TrimEnd(',');
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                   PositionProfileBasicInfo_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the PositionProfileBasicInfo_contactCheckBoxList_SelectedIndexChanged control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void PositionProfileBasicInfo_contactCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBoxList checkBoxList = (CheckBoxList)sender;
                StringBuilder selectedPropertyStringBuilder = new StringBuilder();
                for (int i = 0; i < checkBoxList.Items.Count; i++)
                {
                    if (checkBoxList.Items[i].Selected)
                    {
                        selectedPropertyStringBuilder.Append(checkBoxList.Items[i].Text);
                        selectedPropertyStringBuilder.Append(",");
                    }
                }
                PositionProfileBasicInfo_contactTextbox.Text = selectedPropertyStringBuilder.ToString().TrimEnd(',');
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                   PositionProfileBasicInfo_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        /// <summary>
        /// Handles the RowDataBound event of the PositionProfileBasicInfo_standardFormSegmentsGridView_RowDataBound control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/>
        /// instance containing the event data.</param>
        protected void PositionProfileBasicInfo_standardFormSegmentsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;
                
                HiddenField hiddenField = (HiddenField)e.Row.FindControl
                    ("PositionProfileBasicInfo_standardFormSegmentsGridView_segmentIDHiddenField");

                ImageButton deleteImageButton = (ImageButton)e.Row.FindControl
                    ("PositionProfileBasicInfo_standardFormSegmentsGridView_deleteImageButton");

                ImageButton dowmImageButton = (ImageButton)e.Row.FindControl
                    ("PositionProfileBasicInfo_standardFormSegmentsGridView_moveDownImageButton");

                ImageButton upImageButton = (ImageButton)e.Row.FindControl
                    ("PositionProfileBasicInfo_standardFormSegmentsGridView_moveUpImageButton");

                HiddenField displayOrderHiddenField = (HiddenField)e.Row.FindControl
                    ("PositionProfileBasicInfo_standardFormSegmentsGridView_displayOrderHiddenField");

                Label segmentNameLabel = (Label)e.Row.FindControl
                    ("PositionProfileBasicInfo_standardFormSegmentsGridView_segmentNameLabel");

                if (e.Row.RowIndex == 0)
                {
                    upImageButton.Visible = false;
                }

                if (Utility.IsNullOrEmpty(hiddenField.Value) || hiddenField.Value == "0")
                {
                    dowmImageButton.Visible = false;
                    upImageButton.Visible = false;
                    deleteImageButton.Visible = false;
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                  PositionProfileBasicInfo_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the RowDataBound event of the PositionProfileBasicInfo_customizedSegmentsGridView_RowDataBound control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/>
        /// instance containing the event data.</param>
        protected void PositionProfileBasicInfo_customizedSegmentsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                HiddenField hiddenField = (HiddenField)e.Row.FindControl
                       ("PositionProfileBasicInfo_customizedSegmentsGridView_segmentIDHiddenField");

                ImageButton deleteImageButton = (ImageButton)e.Row.FindControl
                    ("PositionProfileBasicInfo_customizedSegmentsGridView_deleteImageButton");

                ImageButton dowmImageButton = (ImageButton)e.Row.FindControl
                    ("PositionProfileBasicInfo_customizedSegmentsGridView_moveDownImageButton");

                ImageButton upImageButton = (ImageButton)e.Row.FindControl
                    ("PositionProfileBasicInfo_customizedSegmentsGridView_moveUpImageButton");

                if (positionProfileID!=0)
                {
                    if (e.Row.RowIndex == 0)
                    {
                        upImageButton.Visible = false;
                    }

                    if (Utility.IsNullOrEmpty(hiddenField.Value) || hiddenField.Value == "0")
                    {
                        dowmImageButton.Visible = false;
                        upImageButton.Visible = false;
                        deleteImageButton.Visible = false;
                    }
                }
                else
                {
                    dowmImageButton.Visible = false;
                    upImageButton.Visible = false;
                    deleteImageButton.Visible = false;
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                  PositionProfileBasicInfo_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the RowCommand event of the 
        /// PositionProfileBasicInfo_standardFormSegmentsGridView_RowCommand control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> 
        /// instance containing the event data.</param>
        protected void PositionProfileBasicInfo_standardFormSegmentsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "PreviewControl")
                {
                    LoadSegments(e.CommandArgument.ToString());
                    return;
                }

                //if the command name is delete segment, delete the segment
                if (e.CommandName == "DeleteSegment")
                {
                    ImageButton sourceButton = (ImageButton)e.CommandSource;
                    GridViewRow row = (GridViewRow)sourceButton.Parent.Parent;

                    Label nameLable = (Label)row.FindControl("PositionProfileBasicInfo_standardFormSegmentsGridView_segmentNameLabel");

                    if (Utility.IsNullOrEmpty(e.CommandArgument.ToString()))
                        return;

                    if (ViewState["STANDARD_FORM_SEGMENTS"] == null)
                        return;

                    int selectedID = int.Parse(e.CommandArgument.ToString());

                    List<SegmentDetail> segments = ViewState["STANDARD_FORM_SEGMENTS"] as List<SegmentDetail>;
                    segments.RemoveAll(delegate(SegmentDetail segment)
                    {
                        return segment.SegmentID.ToString() == selectedID.ToString();
                    });

                    ArrangeByDisplayOrder(segments);

                    PositionProfileBasicInfo_standardFormSegmentsGridView.DataSource = segments;
                    PositionProfileBasicInfo_standardFormSegmentsGridView.DataBind();

                    HideLastRecord();
                    ViewState["STANDARD_FORM_SEGMENTS"] = segments;

                    return;
                }

                List<SegmentDetail> Segments = ViewState["STANDARD_FORM_SEGMENTS"] as List<SegmentDetail>;
                int order = int.Parse(e.CommandArgument.ToString());

                //if the command name is move down , move the record one line down 
                if (e.CommandName == "MoveDown")
                {

                    foreach (SegmentDetail segment in Segments)
                    {
                        //Change the display order of the segment
                        if (segment.DisplayOrder == order)
                        {
                            segment.DisplayOrder = order + 1;
                            continue;
                        }
                        if (segment.DisplayOrder == order + 1)
                        {
                            segment.DisplayOrder = order;
                            continue;
                        }
                    }
                }

                //if the command name is moveup, move the record one line up
                if (e.CommandName == "MoveUp")
                {
                    foreach (SegmentDetail segment in Segments)
                    {
                        if (segment.DisplayOrder == order - 1)
                        {
                            segment.DisplayOrder = order;
                            continue;
                        }
                        if (segment.DisplayOrder == order)
                        {
                            segment.DisplayOrder = order - 1;
                            continue;
                        }
                    }
                }

                PositionProfileBasicInfo_standardFormSegmentsGridView.DataSource = Segments.OrderBy(p => p.DisplayOrder);
                PositionProfileBasicInfo_standardFormSegmentsGridView.DataBind();

                // Hide the last down arrow button in the grid
                HideLastRecord();
                ViewState["STANDARD_FORM_SEGMENTS"] = Segments;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                  PositionProfileBasicInfo_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void PositionProfileBasicInfo_customizedSegmentsDeleteConfirmMsgControl_okClick
            (object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CUSTOMIZED_FORM_SEGMENTS"] == null)
                    return;

                if (PositionProfileBasicInfo_customizedSegmentsDeleteHiddenFiled.Value.ToString() == string.Empty)
                    return;

                int selectedID = int.Parse(PositionProfileBasicInfo_customizedSegmentsDeleteHiddenFiled.Value);

                List<SegmentDetail> segments = ViewState["CUSTOMIZED_FORM_SEGMENTS"] as List<SegmentDetail>;
                segments.RemoveAll(delegate(SegmentDetail segment)
                {
                    return segment.SegmentID.ToString() == selectedID.ToString();
                });

                ArrangeByDisplayOrder(segments);

                if (positionProfileID!=0)
                {
                    new PositionProfileBLManager().DeletePositionProfileSegment(positionProfileID, selectedID);
                    UpdatePositionProfileDisplayOrder(segments, positionProfileID);
                }

                PositionProfileBasicInfo_customizedSegmentsGridView.DataSource = segments;
                PositionProfileBasicInfo_customizedSegmentsGridView.DataBind();

                HideCustomizedSegmentLastRecord();
                ViewState["CUSTOMIZED_FORM_SEGMENTS"] = segments;

                Session[Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS] = segments;
                if (segments != null && segments.Count == 0)
                {
                    LoadEmptyGrid();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                  PositionProfileBasicInfo_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void PositionProfileBasicInfo_customizedSegmentsConfirmMsgControl_cancelClick
            (object sender, EventArgs e)
        {
            try
            {
                PositionProfileBasicInfo_customizedSegmentsDeleteHiddenFiled.Value = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                  PositionProfileBasicInfo_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void PositionProfileBasicInfo_displayPositionSavedConfirmMsgControl_okClick
            (object sender, EventArgs e)
        {
            try
            {
                int positionID = 0;

                if (!Utility.IsNullOrEmpty(PositionProfileBasicInfo_editPositionProfileIdHiddenField.Value))
                    positionID = Convert.ToInt32(PositionProfileBasicInfo_editPositionProfileIdHiddenField.Value);

                Response.Redirect("~/PositionProfile/PositionProfileDetailInfo.aspx?m=1&s=0&positionprofileid="
                            + positionID, false);
                base.ShowMessage(PositionProfileBasicInfo_topSuccessMessageLabel,
                    PositionProfileBasicInfo_bottomSuccessMessageLabel,
                    HCMResource.PositionProfile_AddedSuccessfully);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                  PositionProfileBasicInfo_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the RowCommand event of the 
        /// PositionProfileBasicInfo_customizedSegmentsGridView_RowCommand control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> 
        /// instance containing the event data.</param>
        protected void PositionProfileBasicInfo_customizedSegmentsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "PreviewControl")
                {
                    LoadSegments(e.CommandArgument.ToString());
                    return;
                }
                //if the command name is delete segment, delete the segment
                if (e.CommandName == "DeleteSegment")
                {
                    if (ViewState["CUSTOMIZED_FORM_SEGMENTS"] != null &&
                      (ViewState["CUSTOMIZED_FORM_SEGMENTS"] as List<SegmentDetail>).Count == 1)
                        {
                            base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                                    PositionProfileBasicInfo_bottomErrorMessageLabel,
                                    "Atleast one segment is required.");
                            return;
                        }

                    ImageButton sourceButton = (ImageButton)e.CommandSource;
                    GridViewRow row = (GridViewRow)sourceButton.Parent.Parent;

                    Label nameLable = (Label)row.FindControl("PositionProfileBasicInfo_customizedSegmentsGridView_segmentNameLabel");
                    PositionProfileBasicInfo_customizedSegmentsDeleteHiddenFiled.Value = e.CommandArgument.ToString();

                    PositionProfileBasicInfo_customizedSegmentsDeleteConfirmMsgControl.Message = string.Format
                        (Resources.HCMResource.PositionProfileFormEntry_DeleteConfirmation, nameLable.Text);

                    PositionProfileBasicInfo_customizedSegmentsDeletePopupPanel.Style.Add("height", "200px");
                    PositionProfileBasicInfo_customizedSegmentsDeleteConfirmMsgControl.Type = MessageBoxType.YesNo;
                    PositionProfileBasicInfo_customizedSegmentsDeleteConfirmMsgControl.Title = "Warning";

                    PositionProfileBasicInfo_customizedSegmentsModalPopupExtender.Show();
                    return;
                }
                List<SegmentDetail> Segments = ViewState["CUSTOMIZED_FORM_SEGMENTS"] as List<SegmentDetail>;
                int order = int.Parse(e.CommandArgument.ToString());

                //if the command name is move down , move the record one line down 
                if (e.CommandName == "MoveDown")
                {

                    foreach (SegmentDetail segment in Segments)
                    {
                        //Change the display order of the segment
                        if (segment.DisplayOrder == order)
                        {
                            segment.DisplayOrder = order + 1;
                            continue;
                        }
                        if (segment.DisplayOrder == order + 1)
                        {
                            segment.DisplayOrder = order;
                            continue;
                        }
                    }
                }

                //if the command name is moveup, move the record one line up
                if (e.CommandName == "MoveUp")
                {
                    foreach (SegmentDetail segment in Segments)
                    {
                        if (segment.DisplayOrder == order - 1)
                        {
                            segment.DisplayOrder = order;
                            continue;
                        }
                        if (segment.DisplayOrder == order)
                        {
                            segment.DisplayOrder = order - 1;
                            continue;
                        }
                    }
                }

                
                if (positionProfileID != 0 && (e.CommandName == "MoveUp" || e.CommandName == "MoveDown"))
                {
                    UpdatePositionProfileDisplayOrder(Segments, positionProfileID);
                }

                PositionProfileBasicInfo_customizedSegmentsGridView.DataSource = Segments.OrderBy(p => p.DisplayOrder);
                PositionProfileBasicInfo_customizedSegmentsGridView.DataBind();

                // Hide the last down arrow button in the grid
                HideCustomizedSegmentLastRecord();
                ViewState["CUSTOMIZED_FORM_SEGMENTS"] = Segments;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                  PositionProfileBasicInfo_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the refresh button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileBasicInfo_refreshDepartmentButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Utility.IsNullOrEmpty(PositionProfileBasicInfo_clientNameHiddenField.Value))
                {
                    LoadDeartmentContact(PositionProfileBasicInfo_clientNameHiddenField.Value);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                  PositionProfileBasicInfo_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the drop down index is changed.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileBasicInfo_customizedSegmentsDropDownList_SelectedIndexChanged(
         object sender, EventArgs e)
        {
            if (PositionProfileBasicInfo_customizedSegmentsDropDownList.SelectedValue != "--Select--")
            {
                LoadExistingForm(Convert.ToInt32(
                    PositionProfileBasicInfo_customizedSegmentsDropDownList.SelectedValue));
                PositionProfileBasicInfo_customizedSegmentsGridViewDiv.Style["display"] = "block";
            }
            else
            {
                PositionProfileBasicInfo_customizedSegmentsGridView.DataSource = null;
                PositionProfileBasicInfo_customizedSegmentsGridView.DataBind();
                PositionProfileBasicInfo_customizedSegmentsGridViewDiv.Style["display"] = "none";
            }
        }
        /// <summary>
        /// Handler method that is called when the segment type is navigated.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileBasicInfo_segmentsLinkButton_Click(object sender, EventArgs e)
        {
            if (positionProfileID==0)
            {
                if (PositionProfileBasicInfo_segmentsLinkButton.Text == "Load Customized Segment")
                {
                    PositionProfileBasicInfo_segmentsLinkButton.Text = "Load Standard Segment";
                    PositionProfileBasicInfo_standardFormSegmentDiv.Style["display"] = "none";
                    PositionProfileBasicInfo_customizedSegmentsDiv.Style["display"] = "block";
                    PositionProfileBasicInfo_segmentsLinkButton.ToolTip = "Click here to load standard segment";
                }
                else
                {
                    PositionProfileBasicInfo_segmentsLinkButton.Text = "Load Customized Segment";
                    PositionProfileBasicInfo_standardFormSegmentDiv.Style["display"] = "block";
                    PositionProfileBasicInfo_customizedSegmentsDiv.Style["display"] = "none";
                    PositionProfileBasicInfo_segmentsLinkButton.ToolTip = "Click here to load customized segment";
                }
            }
        }

        /// <summary>
        /// Handler method that is called when the save and continue button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileBasicInfo_saveContinueButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValidData())
                    return;

                int positionProfileID = 0;
                
                PositionProfileDetail positionProfileDetails = new PositionProfileDetail();
                positionProfileDetails.PositionName = PositionProfileBasicInfo_positionProfileNameTextBox.Text.Trim();
                positionProfileDetails.PositionProfileAdditionalInformation = 
                    PositionProfileBasicInfo_additionInformationTextBox.Text.Trim();
                positionProfileDetails.RegisteredBy = base.userID;
                positionProfileDetails.RegistrationDate =
                    Convert.ToDateTime(PositionProfileBasicInfo_dateOfRegistrationTextBox.Text.Trim());
                positionProfileDetails.SubmittalDate =
                    Convert.ToDateTime(PositionProfileBasicInfo_submittalDeadLineTextBox.Text.Trim());
                positionProfileDetails.PositionProfileStatus = "PPS_OPEN";

                //Set position profile keywords
                PositionProfileKeyword positionProfileKeyword = new PositionProfileKeyword();
                positionProfileKeyword.SegmentKeyword = null;
                positionProfileKeyword.UserKeyword = null;
                positionProfileKeyword.Keyword = null;
                positionProfileDetails.PositionProfileKeywords = positionProfileKeyword;
                positionProfileDetails.Segments = SegmentDetail();

              
                //if (positionProfileDetails.Segments == null || positionProfileDetails.Segments.Count == 0)
                //{
                //    base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                //    PositionProfileBasicInfo_bottomErrorMessageLabel, "Choose the Job description form or load from standard segment");
                //    return;
                //}
                
                //Set client information
                List<ClientContactInformation> clientContactInfo = new List<ClientContactInformation>();
                clientContactInfo = ClientContactInformations;

                if (Utility.IsNullOrEmpty(PositionProfileBasicInfo_editPositionProfileIdHiddenField.Value))
                {
                    if (CanCreatePositionProfileUsage(false))
                    {
                        base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                             PositionProfileBasicInfo_bottomErrorMessageLabel,
                             "You have reached the maximum limit based your" +
                            "subscription plan. Cannot create more Job descriptions");
                        return;
                    }

                    //Set co-owner
                    List<UserDetail> userDetails = null;
                    if (ViewState["USER_LIST"] == null)
                        userDetails = new List<UserDetail>();
                    else
                        userDetails = (List<UserDetail>)ViewState["USER_LIST"];

                    //Add created user as owner
                    UserDetail ownerDetail = new UserDetail();
                    ownerDetail.UserID = ((UserDetail)Session["USER_DETAIL"]).UserID;
                    ownerDetail.OwnerType = Constants.PositionProfileOwners.OWNER;

                    userDetails.Add(ownerDetail);

                    positionProfileID = new PositionProfileBLManager().
                    InsertPositionProfileAndSegmentDatas(positionProfileDetails, clientContactInfo,userDetails,
                        base.userID, base.tenantID);
                    
                    PositionProfileBasicInfo_editPositionProfileIdHiddenField.Value = positionProfileID.ToString();

                    if (positionProfileID > 0)
                    {
                        if (!Utility.IsNullOrEmpty(Request.QueryString.Get("pp_session_key")))
                        {
                            string sessionkey = Request.QueryString.Get("pp_session_key");
                            new PositionProfileBLManager().InsertPositionProfileTSCandidate(sessionkey, positionProfileID);
                        }
                    }

                    PositionProfileBasicInfo_displayPositionSavedConfirmMsgControl.Message =
                        "Job description created successfully.<br><br>Do you want to continue to enter detailed information?";

                    PositionProfileBasicInfo_displayPositionSavedPopupPanel.Style.Add("height", "200px");
                    PositionProfileBasicInfo_displayPositionSavedConfirmMsgControl.Type = MessageBoxType.YesNo;
                    PositionProfileBasicInfo_displayPositionSavedConfirmMsgControl.Title = "Confirm Navigation";

                    PositionProfileBasicInfo_displayPositionSavedModalPopupExtender.Show();
                }
                else
                {
                    positionProfileDetails.PositionProfileID = 
                        Convert.ToInt32(PositionProfileBasicInfo_editPositionProfileIdHiddenField.Value);
                    positionProfileDetails.PositionID = positionProfileDetails.PositionProfileID;
                   
                    positionProfileID = new PositionProfileBLManager().
                        UpdatePositionProfileBasicInfo(positionProfileDetails,
                        base.userID, base.tenantID, clientContactInfo);

                    if (positionProfileDetails.PositionProfileID > 0)
                    {
                        Response.Redirect("~/PositionProfile/PositionProfileDetailInfo.aspx?m=1&s=0&positionprofileid="
                            + positionProfileDetails.PositionProfileID, false);
                        base.ShowMessage(PositionProfileBasicInfo_topSuccessMessageLabel,
                            PositionProfileBasicInfo_bottomSuccessMessageLabel,
                            HCMResource.PositionProfile_AddedSuccessfully);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                  PositionProfileBasicInfo_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when row command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileBasicInfo_coOwnerGridView_RowCommand(object sender, 
            GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteCoOwner")
                {
                    int userID = Convert.ToInt32(e.CommandArgument.ToString());
                    List<UserDetail> userDetails = ViewState["USER_LIST"] as List<UserDetail>;

                    // Get deleted co-owner detail.
                    UserDetail deletedUserDetail = (from userDetail in userDetails 
                                                    where userDetail.UserID == userID select userDetail).First() as UserDetail;

                    // Remove co-owner from the list.
                    userDetails.RemoveAll(delegate(UserDetail userDetail)
                    {
                        return userDetail.UserID == userID;
                    });

                    if (positionProfileID != 0)
                    {
                        new PositionProfileBLManager().DeletePositionProfileOwner(positionProfileID,
                            Constants.PositionProfileOwners.CO_OWNER, userID);

                        try
                        {
                            // Compose position profile detail.
                            PositionProfileDetail positionProfileDetail = new PositionProfileDetail();
                            positionProfileDetail.PositionProfileID = positionProfileID;
                            positionProfileDetail.TaskOwnerEMail = deletedUserDetail.Email;
                            positionProfileDetail.TaskOwnerName = deletedUserDetail.FirstName;
                            positionProfileDetail.TaskOwnerType = Constants.PositionProfileOwners.CO_OWNER;
                            positionProfileDetail.PositionProfileName = PositionProfileBasicInfo_positionProfileNameTextBox.Text;

                            if (!Utility.IsNullOrEmpty(PositionProfileBasicInfo_clientNameTextBox.Text))
                            {
                                string[] clientValues = PositionProfileBasicInfo_clientNameTextBox.Text.Trim().Split('|');
                                if (clientValues != null && clientValues.Length > 0)
                                    positionProfileDetail.ClientName = clientValues[0].Trim();
                            }

                            // Sent alert mail to the associated user asynchronously.
                            AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendStatusChangeAlertEmail);
                            IAsyncResult result = taskDelegate.BeginInvoke(positionProfileDetail,
                                EntityType.DissociatedFromPositionProfile, new AsyncCallback(SendAlertEmailCallBack), taskDelegate);
                        }
                        catch (Exception exp)
                        {
                            Logger.ExceptionLog(exp);
                        }
                    }

                    ViewState["USER_LIST"] = userDetails;
                    PositionProfileBasicInfo_coOwnerGridView.DataSource = userDetails;
                    PositionProfileBasicInfo_coOwnerGridView.DataBind();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                  PositionProfileBasicInfo_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the data is bound to the grid row.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileBasicInfo_coOwnerGridView_OnRowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton PositionProfileBasicInfo_coOwnerGridView_workLoadLinkButton = (LinkButton)
                        e.Row.FindControl("PositionProfileBasicInfo_coOwnerGridView_workLoadLinkButton");

                    int userID = 0;
                    HiddenField PositionProfileBasicInfo_coOwnerGridView_userIDHiddenField = (HiddenField)
                        e.Row.FindControl("PositionProfileBasicInfo_coOwnerGridView_userIDHiddenField");

                    if (!Utility.IsNullOrEmpty(PositionProfileBasicInfo_coOwnerGridView_userIDHiddenField.Value))
                        userID = Convert.ToInt32(PositionProfileBasicInfo_coOwnerGridView_userIDHiddenField.Value);

                    // Add click handler.
                    PositionProfileBasicInfo_coOwnerGridView_workLoadLinkButton.Attributes.Add("onclick",
                        "javascript:return ShowRecruiterProfile('" + userID + "','WL');");
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                  PositionProfileBasicInfo_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when add segment link is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileBasicInfo_segmentListControl_Add_segment(object sender, EventArgs e)
        {
            try
            {
                segmentDetailList = new List<SegmentDetail>();
                SegmentDetail segmentDetail;
                segmentDetailList = (List<SegmentDetail>)(ViewState["CUSTOMIZED_FORM_SEGMENTS"]);
                List<SegmentDetail> selectedSegmentList = new List<SegmentDetail>();
                selectedSegmentList = PositionProfileBasicInfo_segmentListControl.SelectedSegmentDetail;
                int endDisplayOrder = 0;
                if (segmentDetailList.Count > 0)
                    endDisplayOrder = segmentDetailList[segmentDetailList.Count - 1].DisplayOrder;

                for (int i = 0; i < selectedSegmentList.Count; i++)
                {
                    segmentDetail = new SegmentDetail();
                    segmentDetail = selectedSegmentList[i];
                    if (!segmentDetailList.Exists(item => item.SegmentID == selectedSegmentList[i].SegmentID))
                    {
                        segmentDetail.DisplayOrder = endDisplayOrder + 1;
                        segmentDetailList.Add(segmentDetail);
                        endDisplayOrder = endDisplayOrder + 1;

                        if(positionProfileID!=0)
                        {
                            switch (segmentDetail.SegmentID)
                            {
                                case (int)SegmentType.ClientPositionDetail:
                                    segmentDetail.DataSource = PositionProfileBasicInfo_clientPositionDetailsControl.DataSource.DataSource;
                                    break;
                                case (int)SegmentType.Education:
                                    segmentDetail.DataSource = PositionProfileBasicInfo_educationRequirementControl.DataSource.DataSource;
                                    break;
                                case (int)SegmentType.Roles:
                                    segmentDetail.DataSource = PositionProfileBasicInfo_roleRequirementControl.DataSource.DataSource;
                                    break;
                                case (int)SegmentType.TechnicalSkills:
                                    segmentDetail.DataSource = PositionProfileBasicInfo_technicalSkillRequirementControl.DataSource.DataSource;
                                    break;
                                case (int)SegmentType.Verticals:
                                    segmentDetail.DataSource = PositionProfileBasicInfo_verticalBackgroundRequirementControl.DataSource.DataSource;
                                    break;
                            }

                            new PositionProfileBLManager().InsertPositionProfileSegment(segmentDetail,
                                positionProfileID, base.userID);
                        }
                    }
                }

                if (positionProfileID != 0)
                {
                    ArrangeByDisplayOrder(segmentDetailList);
                    UpdatePositionProfileDisplayOrder(segmentDetailList, positionProfileID);
                }
               
                ViewState["CUSTOMIZED_FORM_SEGMENTS"] = segmentDetailList;
                PositionProfileBasicInfo_customizedSegmentsGridView.DataSource = segmentDetailList;
                PositionProfileBasicInfo_customizedSegmentsGridView.DataBind();

                HideCustomizedSegmentLastRecord();

                base.ShowMessage(PositionProfileBasicInfo_topSuccessMessageLabel,
                PositionProfileBasicInfo_bottomSuccessMessageLabel,
               HCMResource.PositionProfile_SegmentsAddedRemovedSuccessfully);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                   PositionProfileBasicInfo_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when add segment link is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileBasicInfo_addSegmentLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                segmentDetailList = new List<SegmentDetail>();
                segmentDetailList = ViewState["CUSTOMIZED_FORM_SEGMENTS"] as List<SegmentDetail>;
                PositionProfileBasicInfo_segmentListControl.SelectedSegmentDetail = segmentDetailList;
                PositionProfileBasicInfo_segmentListControl.SegmentList = new AdminBLManager().GetAvailableActiveSegments();
                PositionProfileBasicInfo_addSegmentPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                   PositionProfileBasicInfo_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when add client button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="CommandEventArgs"/> that holds the event data.
        /// </param>
        void PositionProfileBasicInfo_clientControl_OkCommandClick(object sender, 
            CommandEventArgs e)
        {
            try
            {
                ClientInformation clientInformation = new ClientInformation();
                clientInformation = PositionProfileBasicInfo_clientControl.ClientDataSource;

                int clientID = 0;
                clientInformation.CreatedBy = base.userID;
                clientInformation.TenantID = base.tenantID;
                new ClientBLManager().InsertClient(clientInformation, out clientID);

                //Add Departments
                int departmentID = 0;
                Department department = new Department();
                department = PositionProfileBasicInfo_clientControl.DepartmentDataSource;

                if (department.IsDepartmentRequired)
                {
                    department.CreatedBy = base.userID;
                    clientInformation.TenantID = base.tenantID;
                    department.ClientID = clientID;
                    new ClientBLManager().InsertDepartment(department, out departmentID);
                }

                //Add Contact
                int contactID = 0;
                ClientContactInformation clientContactInformation = new ClientContactInformation();
                clientContactInformation = PositionProfileBasicInfo_clientControl.ContactDataSource;

                if (clientContactInformation.IsContactRequired)
                {
                    clientContactInformation.CreatedBy = base.userID;
                    clientContactInformation.TenantID = base.tenantID;
                    clientContactInformation.ClientID = clientID;
                    clientContactInformation.DepartmentID = departmentID;
                    clientContactInformation.SellectedDeparments = departmentID == 0 ? null : departmentID.ToString();
                    new ClientBLManager().InsertContact(clientContactInformation, out contactID);
                }
                //Load the new added client name,department and contacts
                LoadDeartmentContact(string.Format("{0} | {1}",
                    clientInformation.ClientName, clientID));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                   PositionProfileBasicInfo_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        #endregion Event Handler

        #region Private Method                                                 

        /// <summary>
        /// Method that send email to position profile associates indicating
        /// the status change of position profile.
        /// </summary>
        /// <param name="positionProfileDetail">
        /// A <see cref="PositionProfileDetail"/> that holds the position profile 
        /// detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type. In this case 
        /// AssociatedWithPositionProfile & DissociatedFromPositionProfile are
        /// the possible values.
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendStatusChangeAlertEmail(PositionProfileDetail positionProfileDetail, EntityType entityType)
        {
            try
            {
                // Construct position profile detail.
                // Send email.
                new EmailHandler().SendMail(entityType, positionProfileDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method to get the position profile owners by profile id
        /// </summary>
        private void BindPositionProfileCoOwners()
        {
            PositionProfileBasicInfo_coOwnerGridView.DataSource = null;
            PositionProfileBasicInfo_coOwnerGridView.DataBind();

            if (!Utility.IsNullOrEmpty(PositionProfileBasicInfo_editPositionProfileIdHiddenField.Value))
            {
                List<UserDetail> userDetails = new List<UserDetail>();
                userDetails = new PositionProfileBLManager().GetPositionProfileOwnersByIDAndOwnerType(
                    Convert.ToInt32(PositionProfileBasicInfo_editPositionProfileIdHiddenField.Value),
                    Constants.PositionProfileOwners.CO_OWNER);
                if (userDetails != null && userDetails.Count > 0)
                {
                    PositionProfileBasicInfo_coOwnerGridView.DataSource = userDetails;
                    PositionProfileBasicInfo_coOwnerGridView.DataBind();
                    ViewState["USER_LIST"] = userDetails;
                }
            }
        }

        /// <summary>
        /// Binds the position profile.
        /// </summary>
        private void BindPositionProfile()
        {
            PositionProfileDetail positionProfileDetail = new PositionProfileDetail();
            positionProfileDetail = new PositionProfileBLManager().GetPositionProfile
                (Convert.ToInt32(PositionProfileBasicInfo_editPositionProfileIdHiddenField.Value));
            if (Support.Utility.IsNullOrEmpty(positionProfileDetail))
                return;

            if (positionProfileDetail.ClientID != 0)
            {
                PositionProfileBasicInfo_clientIDHiddenField.Value = positionProfileDetail.ClientID.ToString();
                
                LoadDeartmentContact(string.Format("{0} | {1}",
                    positionProfileDetail.ClientName,positionProfileDetail.ClientID));
                
                BindSelectedDepartment(positionProfileDetail.DepartmentIds);
                BindSelectedContact(positionProfileDetail.ContactIds);
            }
            PositionProfileBasicInfo_profileIDLabelValue.Text = positionProfileDetail.PositionProfileKey;
            PositionProfileBasicInfo_positionProfileNameTextBox.Text = positionProfileDetail.PositionName;
            PositionProfileBasicInfo_additionInformationTextBox.Text = positionProfileDetail.PositionProfileAdditionalInformation;
            PositionProfileBasicInfo_dateOfRegistrationTextBox.Text = positionProfileDetail.RegistrationDate.ToShortDateString();
            PositionProfileBasicInfo_submittalDeadLineTextBox.Text = positionProfileDetail.SubmittalDate.ToShortDateString();
        }

        /// <summary>
        /// Method bind the segment details
        /// </summary>
        /// <returns>List<SegmentDetail></returns>
        private List<SegmentDetail> SegmentDetail()
        {
            List<SegmentDetail> SegmentDetails = new List<SegmentDetail>();

            if (positionProfileID==0)
            {
                if (PositionProfileBasicInfo_segmentsLinkButton.Text.Trim() == "Load Standard Segment")
                {
                    SegmentDetails = ViewState["CUSTOMIZED_FORM_SEGMENTS"] as List<SegmentDetail>;
                }
                else
                {
                    SegmentDetails = ViewState["STANDARD_FORM_SEGMENTS"] as List<SegmentDetail>;
                }
            }
            else
            {
                SegmentDetails = ViewState["CUSTOMIZED_FORM_SEGMENTS"] as List<SegmentDetail>;
            }

            if (SegmentDetails != null && SegmentDetails.Count > 0)
            {
                for (int i = 0; i < SegmentDetails.Count; i++)
                {

                    switch (SegmentDetails[i].SegmentID)
                    {
                        case (int)SegmentType.ClientPositionDetail:
                            SegmentDetails[i].DataSource = PositionProfileBasicInfo_clientPositionDetailsControl.DataSource.DataSource;
                            break;
                        case (int)SegmentType.CommunicationSkills:
                            break;
                        case (int)SegmentType.CulturalFit:
                            break;
                        case (int)SegmentType.DecisionMaking:
                            break;
                        case (int)SegmentType.Education:
                            SegmentDetails[i].DataSource = PositionProfileBasicInfo_educationRequirementControl.DataSource.DataSource;
                            break;
                        case (int)SegmentType.InterpersonalSkills:
                            break;
                        case (int)SegmentType.Roles:
                            SegmentDetails[i].DataSource = PositionProfileBasicInfo_roleRequirementControl.DataSource.DataSource;
                            break;
                        case (int)SegmentType.Screening:
                            break;
                        case (int)SegmentType.TechnicalSkills:
                            SegmentDetails[i].DataSource = PositionProfileBasicInfo_technicalSkillRequirementControl.DataSource.DataSource;
                            break;
                        case (int)SegmentType.Verticals:
                            SegmentDetails[i].DataSource = PositionProfileBasicInfo_verticalBackgroundRequirementControl.DataSource.DataSource;
                            break;
                    }
                }
            }
            return SegmentDetails;
        }

        /// <summary>
        /// Method that will clear success/error label messages.
        /// </summary>
        private void ClearLabelMessage()
        {
            PositionProfileBasicInfo_topSuccessMessageLabel.Text = string.Empty;
            PositionProfileBasicInfo_bottomSuccessMessageLabel.Text = string.Empty;
            PositionProfileBasicInfo_topErrorMessageLabel.Text = string.Empty;
            PositionProfileBasicInfo_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Load the department and contact against the client id
        /// </summary>
        /// <param name="clientInfo"></param>
        private void LoadDeartmentContact(string clientInfo)
        {
            string[] clientDetail = clientInfo.ToString().Split(new char[] { '|' });
            if (clientDetail.Length == 2)
            {
                int result = 0;
                if (!int.TryParse(clientDetail[1].ToString(), out result))
                {
                    // If so, raise the event handler.
                    base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                        PositionProfileBasicInfo_bottomErrorMessageLabel,
                        Resources.HCMResource.SearchClientControl_ClientCannotBeEmpty);
                    return;
                }

                // Validate whether both client name and id match with each other
                if (clientDetail.Length == 2)
                {
                    if (!new CommonBLManager().IsValidClient
                        (Convert.ToInt32(clientDetail[1]), clientDetail[0].Trim()))
                    {
                        base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                                PositionProfileBasicInfo_bottomErrorMessageLabel,
                                "'" + clientDetail[0].Trim() + "' and '"
                                + clientDetail[1].Trim() + "' are mismatched");
                    }
                    else
                    {
                        PositionProfileBasicInfo_clientNameTextBox.Text = clientInfo;
                        PositionProfileBasicInfo_clientIDHiddenField.Value = clientDetail[0].Trim();
                        PositionProfileBasicInfo_clientNameHiddenField.Value = clientDetail[0].Trim();
                        PositionProfileBasicInfo_clientIDHiddenField.Value = result.ToString();
                        BindContactList(new ClientBLManager().GetClientDerpartmentContacts(result));
                        BindDepartmentList(new ClientBLManager().GetDepartments(result));
                    }
                }
            }
        }

        /// <summary>
        /// Method to initialize the component
        /// </summary>
        private void ClientManagementEventInitialize()
        {
            try
            {
                PositionProfileBasicInfo_clientControl.OkCommandClick+=
                    new AddClientControl.Button_CommandClick(PositionProfileBasicInfo_clientControl_OkCommandClick);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                   PositionProfileBasicInfo_bottomErrorMessageLabel,
                   exp.Message);
            }

        }

        /// <summary>
        /// Loads the empty grid.
        /// </summary>
        private void LoadEmptyGrid()
        {
            List<SegmentDetail> nullSegments = new List<SegmentDetail>();

            SegmentDetail nullSegment = new SegmentDetail();

            nullSegment.SegmentName = "";

            nullSegments.Add(nullSegment);

            PositionProfileBasicInfo_customizedSegmentsGridView.DataSource = nullSegments;
            PositionProfileBasicInfo_customizedSegmentsGridView.DataBind();
            HideCustomizedSegmentLastRecord();
        }

        /// <summary>
        /// Hides the last record.
        /// </summary>
        private void HideLastRecord()
        {
            foreach (GridViewRow row in PositionProfileBasicInfo_standardFormSegmentsGridView.Rows)
            {
                if (row.RowIndex == PositionProfileBasicInfo_standardFormSegmentsGridView.Rows.Count - 1)
                {
                    ImageButton dowmImageButton = (ImageButton)row.FindControl
                        ("PositionProfileBasicInfo_standardFormSegmentsGridView_moveDownImageButton");
                    dowmImageButton.Visible = false;
                }
            }
        }

        /// <summary>
        /// Hides the customizedlast record.
        /// </summary>
        private void HideCustomizedSegmentLastRecord()
        {
            foreach (GridViewRow row in PositionProfileBasicInfo_customizedSegmentsGridView.Rows)
            {
                if (row.RowIndex == PositionProfileBasicInfo_customizedSegmentsGridView.Rows.Count - 1)
                {
                    ImageButton dowmImageButton = (ImageButton)row.FindControl
                        ("PositionProfileBasicInfo_customizedSegmentsGridView_moveDownImageButton");
                    dowmImageButton.Visible = false;
                }
            }
        }

        /// <summary>
        /// Arranges the by display order.
        /// </summary>
        /// <param name="segments">The segments.</param>
        private void ArrangeByDisplayOrder(List<SegmentDetail> segments)
        {
            int displayOrder = 1;
            foreach (SegmentDetail seg in segments)
            {
                seg.DisplayOrder = displayOrder;

                displayOrder = displayOrder + 1;
            }
        }
        /// <summary>
        /// Loads the existing segments.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        private void LoadExistingForm(int formID)
        {
            FormDetail form = new FormDetail();
            List<SegmentDetail> SegmentDetails = new List<SegmentDetail>();
            
            form = new AdminBLManager().GetExistingSegments(formID);
            form.segmentList = form.segmentList.OrderBy(p => p.DisplayOrder).ToList();

            PositionProfileBasicInfo_customizedSegmentsGridView.DataSource = form.segmentList;
            PositionProfileBasicInfo_customizedSegmentsGridView.DataBind();

            HideCustomizedSegmentLastRecord();
            foreach (Segment seg in form.segmentList)
            {
                SegmentDetail segmentDetail = new SegmentDetail();
                segmentDetail.SegmentID = seg.SegmentID;
                segmentDetail.SegmentName = seg.SegmentName;
                segmentDetail.DisplayOrder = seg.DisplayOrder;
                SegmentDetails.Add(segmentDetail);
            }

            ViewState["CUSTOMIZED_FORM_SEGMENTS"] = SegmentDetails;
            Session[Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS] = form.segmentList;
        }

        /// <summary>
        /// Loads the cutomized segments by position profile id.
        /// </summary>
        /// <param name="positionProfileID">The position Profile ID.</param>
        private void LoadCustomizedSegmentsByPositionProfileID(int positionProfileID)
        {
            List<SegmentDetail> segmentList = 
              new PositionProfileBLManager().GetPositionProfileSegementListByPositionProfileID(positionProfileID);

            if (segmentList != null)
            {
                PositionProfileBasicInfo_customizedSegmentsGridView.DataSource = segmentList.OrderBy(p => p.DisplayOrder).ToList();
                PositionProfileBasicInfo_customizedSegmentsGridView.DataBind();

                HideCustomizedSegmentLastRecord();

                ViewState["CUSTOMIZED_FORM_SEGMENTS"] = segmentList;
                Session[Support.Constants.SessionConstants.PREVIEW_FORM_SEGMENTS] = segmentList;
            }
        }

        /// <summary>
        /// Binds the forms.
        /// </summary>
        private void LoadCustomizedSegments()
        {
            PositionProfileBasicInfo_customizedSegmentsDropDownList.DataSource = null;
            List<FormDetail> dataSource = new PositionProfileBLManager().GetFormWithUsers(base.userID);
            //FormDetail formDetail = new FormDetail();
            dataSource.Insert(0, new FormDetail { FormID = 0, FormName = "", Tags = "" });
            PositionProfileBasicInfo_customizedSegmentsDropDownList.DataSource = dataSource;
            PositionProfileBasicInfo_customizedSegmentsDropDownList.DataTextField = Constants.PositionProfileConstants.FORM_NAME;
            PositionProfileBasicInfo_customizedSegmentsDropDownList.DataValueField = Constants.PositionProfileConstants.FORM_ID;
            PositionProfileBasicInfo_customizedSegmentsDropDownList.DataBind();

            PositionProfileBasicInfo_customizedSegmentsGridViewDiv.Style["display"] = "block";

            int formID = 0; 
            if (!Utility.IsNullOrEmpty(Request.QueryString["form_id"]))
            {
                formID = Convert.ToInt32(Request.QueryString["form_id"]);
            }
            else
            {
                formID = new AdminBLManager().GetDefaultForm(base.userID);
            }
            //PositionProfileBasicInfo_customizedSegmentsDropDownList.SelectedValue = formID.ToString();
            //LoadExistingForm(formID);
        }

        /// <summary>
        /// Loads the standard form segments.
        /// </summary>
        private void LoadStandardFormSegments()
        {
            List<Segment> segments = new AdminBLManager().GetAvailableSegments();
            List<SegmentDetail> segmentDetails = new List<SegmentDetail>();

            int i = 1;
            foreach (Segment seg in segments)
            {
                SegmentDetail segmentDetail = new SegmentDetail();
                segmentDetail.SegmentID = seg.SegmentID;
                segmentDetail.SegmentName = seg.SegmentName;
                segmentDetail.DisplayOrder = i;
                segmentDetails.Add(segmentDetail);
                i++;
            }

            PositionProfileBasicInfo_standardFormSegmentsGridView.DataSource = segmentDetails;
            PositionProfileBasicInfo_standardFormSegmentsGridView.DataBind();
            HideLastRecord();
            ViewState["STANDARD_FORM_SEGMENTS"] = segmentDetails;
        }

        /// <summary>
        /// Method to bind the contact list 
        /// </summary>
        /// <param name="department"></param>
        private void BindContactList(List<ClientContactInformation> contactInformation)
        {
            PositionProfileBasicInfo_contactTextbox.Text = string.Empty;
            PositionProfileBasicInfo_contactCheckBoxList.Items.Clear();
            PositionProfileBasicInfo_contactCheckBoxList.DataSource = contactInformation;
            PositionProfileBasicInfo_contactCheckBoxList.DataTextField = "ContactName";
            PositionProfileBasicInfo_contactCheckBoxList.DataValueField = "ClientDepartmentContactID";
            PositionProfileBasicInfo_contactCheckBoxList.DataBind();
        }

        /// <summary>
        /// Method to bind the department list 
        /// </summary>
        /// <param name="department"></param>
        private void BindDepartmentList(List<Department> department)
        {
            PositionProfileBasicInfo_departmentTextBox.Text = string.Empty;
            PositionProfileBasicInfo_departmentCheckBoxList.Items.Clear();
            PositionProfileBasicInfo_departmentCheckBoxList.DataSource = department;
            PositionProfileBasicInfo_departmentCheckBoxList.DataTextField = "DepartmentName";
            PositionProfileBasicInfo_departmentCheckBoxList.DataValueField = "DepartmentID";
            PositionProfileBasicInfo_departmentCheckBoxList.DataBind();
        }

        /// <summary>
        /// Method that makes the selected department item checked
        /// </summary>
        /// <param name="ContactIds">List<int> DepartmentIds</param>
        private void BindSelectedDepartment(List<int> departmentIDs)
        {
            if (departmentIDs == null || departmentIDs.Count == 0)
                return;

            StringBuilder departmentName = new StringBuilder();
            for (int val = 0; val < PositionProfileBasicInfo_departmentCheckBoxList.Items.Count; val++)
            {
                if (departmentIDs.Exists(item => item.ToString() == 
                    PositionProfileBasicInfo_departmentCheckBoxList.Items[val].Value))
                {
                    PositionProfileBasicInfo_departmentCheckBoxList.Items[val].Selected = true;
                    departmentName.Append(PositionProfileBasicInfo_departmentCheckBoxList.Items[val].Text);
                    departmentName.Append(",");

                }
            }
            PositionProfileBasicInfo_departmentTextBox.Text = departmentName.ToString().TrimEnd(',');
        }

        /// <summary>
        /// Method that makes the selected contact item checked
        /// </summary>
        /// <param name="ContactIds">List<string> ContactIds</param>
        private void BindSelectedContact(List<string> ContactIds)
        {
            StringBuilder contactNames = new StringBuilder();
            for (int val = 0; val < PositionProfileBasicInfo_contactCheckBoxList.Items.Count; val++)
            {
                if (ContactIds.Exists(item => item.ToString() == 
                    PositionProfileBasicInfo_contactCheckBoxList.Items[val].Value))
                {
                    PositionProfileBasicInfo_contactCheckBoxList.Items[val].Selected = true;
                    contactNames.Append(PositionProfileBasicInfo_contactCheckBoxList.Items[val].Text);
                    contactNames.Append(",");
                }
            }
            PositionProfileBasicInfo_contactTextbox.Text = contactNames.ToString().TrimEnd(',');
        }


        /// <summary>
        /// Loads the segments.
        /// </summary>
        /// <param name="segmentID">The segment ID.</param>
        private void LoadSegments(string segmentID)
        {
            string segmentPath = new AdminBLManager().GetSegmentPath(segmentID);

            Control segmentControl = this.LoadControl("../" + segmentPath);

            if ((segmentControl.TemplateControl).AppRelativeVirtualPath.Contains("TechnicalSkillRequirementControl"))
            {
                TechnicalSkillRequirement tech = (TechnicalSkillRequirement)segmentControl;

                tech.DefaultTechnicalSkill();
            }

            if ((segmentControl.TemplateControl).AppRelativeVirtualPath.Contains("VerticalBackgroundRequirementControl"))
            {
                Segments.VerticalBackgroundRequirement vertical = (Segments.VerticalBackgroundRequirement)segmentControl;

                vertical.DefaultGridValue();
            }

            PositionProfileBasicInfo_controlsPlaceHolder.Controls.Add(segmentControl);

            PositionProfileBasicInfo_placeHolderModalPopupExtender.Show();

        }

        /// <summary>
        /// Method to update segment order
        /// </summary>
        /// <param name="SegmentDetail">The segment details</param>
        private void UpdatePositionProfileDisplayOrder(List<SegmentDetail>
            SegmentDetails, int positionProfileID)
        {
            foreach (SegmentDetail segmentDetail in SegmentDetails)
            {
                new PositionProfileBLManager().UpdatePositionProfileSegmentDisplayOrder(positionProfileID,
                    segmentDetail.SegmentID, segmentDetail.DisplayOrder);
            }
        }

        /// <summary>
        /// Method to load the selected client/contact/departement
        /// </summary>
        private void AssignParentPageNavigationLink()
        {
            int clientID = 0;
            int departementID = 0;
            int contactID = 0;
            List<int> selectedDepartment = null;

            if (!Utility.IsNullOrEmpty(Request.QueryString[Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING]) &&
                !Utility.IsNullOrEmpty(Request.QueryString[Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING]))
            {
                contactID = int.Parse(Request.QueryString[Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING]);

                departementID = int.Parse(Request.QueryString[Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING]);
                selectedDepartment = new List<int>();
                selectedDepartment.Add(departementID);
            }
            else if (!Utility.IsNullOrEmpty(Request.QueryString[Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING]))
            {
                departementID = int.Parse(Request.QueryString[Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING]);
                selectedDepartment = new List<int>();
                selectedDepartment.Add(departementID);
            }
            else if (!Utility.IsNullOrEmpty(Request.QueryString[Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING]))
            {
                clientID = int.Parse(Request.QueryString[Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING]);
            }
            else
            {
                return;
            }

            int newClientID = 0;
            string clientName = "";
            new ClientBLManager().GetClientName(clientID == 0 ? (int?)null : 
                clientID, departementID == 0 ? (int?)null : departementID, contactID == 0 ?
                (int?)null : contactID, out newClientID, out clientName);
            if (newClientID == 0)
                return;

            List<string> selectedContacts = new List<string>();
            selectedContacts.Add(newClientID + "-" + departementID + "-" + contactID.ToString());

            List<Department> departmet = new List<Department>();

            PositionProfileBasicInfo_clientIDHiddenField.Value = newClientID.ToString();

            LoadDeartmentContact(string.Format("{0} | {1}",
                   clientName, newClientID));

            if(selectedDepartment!=null)
                BindSelectedDepartment(selectedDepartment);

            if (selectedContacts != null)
                BindSelectedContact(selectedContacts);
        }

        /// <summary>
        /// Method to verify whether position profile can be created or not
        /// </summary>
        /// <param name="veryFy"></param>
        /// <returns></returns>
        private bool CanCreatePositionProfileUsage(bool veryFy)
        {

            if (!Support.Utility.IsNullOrEmpty(PositionProfileBasicInfo_editPositionProfileIdHiddenField.Value) && veryFy)
                return false;

            int featureID = 1;
            return new AdminBLManager().IsFeatureUsageLimitExceeds(base.tenantID, featureID);
        }

        #endregion Private Method

        #region Asynchronous Method Handlers                                   

        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendAlertEmailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Asynchronous Method Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            ClearLabelMessage();
            bool isValidData = true;
            bool registrationDate = true;
            bool submittalDate = true;

            if (CanCreatePositionProfileUsage(true))
            {
                isValidData = false;
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                     PositionProfileBasicInfo_bottomErrorMessageLabel,
                     "You have reached the maximum limit based your subscription plan. Cannot create more Job descriptions");

                return isValidData;
            }
            if (PositionProfileBasicInfo_positionProfileNameTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                   PositionProfileBasicInfo_bottomErrorMessageLabel,
                  HCMResource.PositionProfile_EnterPositionProfileName);
            }

            DateTime registratationDateValue = DateTime.Now;
            DateTime submittalDeadLineDateValue = DateTime.Now;
            if (PositionProfileBasicInfo_dateOfRegistrationTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                   PositionProfileBasicInfo_bottomErrorMessageLabel,
                   HCMResource.PositionProfile_EnterRegistrationDate);
            }
            else
            {
                if (!DateTime.TryParse(PositionProfileBasicInfo_dateOfRegistrationTextBox.Text.Trim(),
                    out registratationDateValue))
                {
                    isValidData = false;
                    registrationDate = false;
                    base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                       PositionProfileBasicInfo_bottomErrorMessageLabel,
                       HCMResource.PositionProfile_EnterValidRegistrationDate);
                }
            }
            if (PositionProfileBasicInfo_submittalDeadLineTextBox.Text.Trim().Length == 0)
            {
                isValidData = false;
                base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                   PositionProfileBasicInfo_bottomErrorMessageLabel,
                   HCMResource.PositionProfile_EnterSubmittalDate);
            }
            else
            {
                if (!DateTime.TryParse(PositionProfileBasicInfo_submittalDeadLineTextBox.Text.Trim(),
                    out submittalDeadLineDateValue))
                {
                    submittalDate = false;
                    isValidData = false;
                    base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                       PositionProfileBasicInfo_bottomErrorMessageLabel,
                      HCMResource.PositionProfile_EnterValidSubmittalDate);
                }

            }
           
            if (registrationDate && submittalDate)
            {
                if (DateTime.Compare(submittalDeadLineDateValue, registratationDateValue) < 0)
                {
                    isValidData = false;
                    base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                       PositionProfileBasicInfo_bottomErrorMessageLabel,
                       HCMResource.PositionProfile_CompareRegistrationDateSubmittalDate);
                }
            }

            if (positionProfileID==0)
            {
                if (PositionProfileBasicInfo_segmentsLinkButton.Text == "Load Standard Segment" &&
                    PositionProfileBasicInfo_customizedSegmentsDropDownList.SelectedValue == "--Select--" &&
                    (PositionProfileBasicInfo_customizedSegmentsGridView == null ||
                    PositionProfileBasicInfo_customizedSegmentsGridView.Rows.Count == 0))
                {
                    isValidData = false;
                    base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                         PositionProfileBasicInfo_bottomErrorMessageLabel,
                         HCMResource.PositionProfile_Segment_Empty);
                }
                if (PositionProfileBasicInfo_segmentsLinkButton.Text == "Load Customized Segment")
                {
                    if (PositionProfileBasicInfo_standardFormSegmentsGridView == null ||
                        PositionProfileBasicInfo_standardFormSegmentsGridView.Rows.Count == 0)
                    {
                        isValidData = false;
                        base.ShowMessage(PositionProfileBasicInfo_topErrorMessageLabel,
                             PositionProfileBasicInfo_bottomErrorMessageLabel,
                             HCMResource.PositionProfile_Segment_Empty);
                    }
                }
            }
            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads default values into controls.
        /// </summary>
        protected override void LoadValues()
        {
            ClearLabelMessage();
            ClientManagementEventInitialize();
            if (!IsPostBack)
            {
                Page.Form.DefaultFocus = PositionProfileBasicInfo_positionProfileNameTextBox.UniqueID;
                PositionProfileBasicInfo_positionProfileNameTextBox.Focus();
                PositionProfileBasicInfo_dateOfRegistrationTextBox.Text = DateTime.Today.ToShortDateString();

                string firstName = ((UserDetail)Session["USER_DETAIL"]).FirstName;
                string lastName = ((UserDetail)Session["USER_DETAIL"]).LastName;
                //Set created by name
                PositionProfileBasicInfo_createdByLabelValue.Text = 
                    string.Format("{0} {1}", firstName,lastName);
            }
        }

        #endregion Protected Overridden Methods

        #region Public Method                                                  

        /// <summary>
        /// Method that constructs the client information
        /// </summary>
        public List<ClientContactInformation> ClientContactInformations
        {
            get
            {
                try
                {
                    List<ClientContactInformation> clientContactInformationList = null;
                    if (Utility.IsNullOrEmpty(PositionProfileBasicInfo_clientNameHiddenField.Value))
                        return clientContactInformationList;
                    for (int val = 0; val < PositionProfileBasicInfo_contactCheckBoxList.Items.Count; val++)
                    {
                        if (PositionProfileBasicInfo_contactCheckBoxList.Items[val].Selected)
                        {
                            if (clientContactInformationList == null)
                                clientContactInformationList = new List<ClientContactInformation>();
                            ClientContactInformation clientContactInformation = new ClientContactInformation();
                            clientContactInformation.ClientName = PositionProfileBasicInfo_clientNameHiddenField.Value;
                            string[] contacts = PositionProfileBasicInfo_contactCheckBoxList.Items[val].Text.Trim().Split('-');
                            string[] departmentDetails = PositionProfileBasicInfo_contactCheckBoxList.Items[val].Value.Split('-');
                            clientContactInformation.ClientID = int.Parse(departmentDetails[0]);
                            clientContactInformation.DepartmentID = departmentDetails[1] == "0" ? (int?)null : int.Parse(departmentDetails[1]);
                            clientContactInformation.ContactID = int.Parse(departmentDetails[2]);
                            clientContactInformation.DepartmentName = contacts[0];
                            clientContactInformation.ContactName = contacts[1];
                            clientContactInformationList.Add(clientContactInformation);
                        }
                    }

                    for (int val = 0; val < PositionProfileBasicInfo_departmentCheckBoxList.Items.Count; val++)
                    {
                        if (PositionProfileBasicInfo_departmentCheckBoxList.Items[val].Selected)
                        {
                            if (clientContactInformationList == null)
                                clientContactInformationList = new List<ClientContactInformation>();

                            if (clientContactInformationList.Exists(item => item.DepartmentName == PositionProfileBasicInfo_departmentCheckBoxList.Items[val].Text))
                            {
                                continue;
                            }
                            {
                                ClientContactInformation clientContactInformation = new ClientContactInformation();
                                clientContactInformation.ClientName = PositionProfileBasicInfo_clientNameHiddenField.Value;
                                clientContactInformation.DepartmentName = PositionProfileBasicInfo_departmentCheckBoxList.Items[val].Text.Trim();
                                clientContactInformation.ClientID = int.Parse(PositionProfileBasicInfo_clientIDHiddenField.Value);
                                clientContactInformation.DepartmentID = int.Parse(PositionProfileBasicInfo_departmentCheckBoxList.Items[val].Value);
                                clientContactInformationList.Add(clientContactInformation);
                            }

                        }
                    }
                    if (Utility.IsNullOrEmpty(clientContactInformationList))
                    {
                        clientContactInformationList = new List<ClientContactInformation>();
                        ClientContactInformation clientContactInformation = new ClientContactInformation();
                        clientContactInformation.ClientID = int.Parse(PositionProfileBasicInfo_clientIDHiddenField.Value);
                        clientContactInformation.ClientName = PositionProfileBasicInfo_clientNameHiddenField.Value;
                        clientContactInformationList.Add(clientContactInformation);
                    }

                    return clientContactInformationList;
                }
                catch (Exception exp)
                {
                    throw exp;
                }
            }
        }

        /// <summary>
        /// Method that will show feature pop up extender
        /// </summary>
        public void ShowClientControlModalPopUp()
        {
            PositionProfileBasicInfo_clientControl.ClientAttributesDataSource =
                new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENT);

            PositionProfileBasicInfo_clientControl.DepartmenAttributesDataSource =
                new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTDEPARTMENT);

            PositionProfileBasicInfo_clientControl.ContactAttributesDataSource =
                new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTCONTACTS);

            PositionProfileBasicInfo_clientModalpPopupExtender.Show();
        }
        #endregion Public Method
    }
}