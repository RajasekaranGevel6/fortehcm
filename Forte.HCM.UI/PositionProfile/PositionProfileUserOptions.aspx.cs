﻿#region Directives                                                   
using System;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.UI.Common;

using Microsoft.Practices.EnterpriseLibrary.Logging;
#endregion Directives

namespace Forte.HCM.UI.PositionProfile
{
    /// <summary>
    /// Represents the class to change the user options
    /// </summary>
    public partial class PositionProfileUserOptions : PageBase
    {
        #region Event Handlers                                       
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("User Options");

                Page.Form.DefaultButton = PositionProfileUserOptions_topSaveButton.UniqueID;
                Page.Form.DefaultFocus = PositionProfileUserOptions_topSaveButton.UniqueID;
                Page.SetFocus(PositionProfileUserOptions_topSaveButton.ClientID);
                //PositionProfileUserOptions_topSaveButton.Focus();
                if (!IsPostBack)
                {
                    LoadValues();

                    PositionProfileUserOptions_previewLinkButton.Attributes.Add("onclick", "return LoadGetForm('" +
                        PositionProfileUserOptions_formLayoutDropDownList.ClientID + "');");
                }
                PositionProfileUserOptions_topErrorMessageLabel.Text = string.Empty;
                PositionProfileUserOptions_bottomErrorMessageLabel.Text = string.Empty;

                PositionProfileUserOptions_topSuccessMessageLabel.Text = string.Empty;
                PositionProfileUserOptions_bottomSuccessMessageLabel.Text = string.Empty;
            }
            catch (Exception exception)
            {
                base.ShowMessage(PositionProfileUserOptions_topErrorMessageLabel,
                    PositionProfileUserOptions_bottomErrorMessageLabel,
                    exception.Message);
                Logger.Write(exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileUserOptions_saveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void PositionProfileUserOptions_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (PositionProfileUserOptions_formLayoutDropDownList.SelectedValue == "0")
                {
                    base.ShowMessage(PositionProfileUserOptions_topErrorMessageLabel,
                    PositionProfileUserOptions_bottomErrorMessageLabel,
                    Resources.HCMResource.PositionProfileUserOptions_PleaseSelectAFormType);
                    return;
                }

                //Update the user options for the user ID
                new PositionProfileBLManager().UpdatePositionProfileUserOptions
                    (int.Parse(PositionProfileUserOptions_formLayoutDropDownList.SelectedValue), base.userID);

                base.ShowMessage(PositionProfileUserOptions_bottomSuccessMessageLabel,
                    PositionProfileUserOptions_topSuccessMessageLabel,
                    Resources.HCMResource.PositionProfileUserOptions_DefaultFormUpdatedSuccessfully);
            }
            catch (Exception exception)
            {
                base.ShowMessage(PositionProfileUserOptions_topErrorMessageLabel,
                    PositionProfileUserOptions_bottomErrorMessageLabel, exception.Message);
                Logger.Write(exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileUserOptions_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/>
        /// instance containing the event data.</param>
        protected void PositionProfileUserOptions_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                base.ShowMessage(PositionProfileUserOptions_topErrorMessageLabel,
                    PositionProfileUserOptions_bottomErrorMessageLabel, exception.Message);
                Logger.Write(exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the PositionProfileUserOptions_previewLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PositionProfileUserOptions_previewLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (PositionProfileUserOptions_formLayoutDropDownList.SelectedValue == "0")
                {
                    base.ShowMessage(PositionProfileUserOptions_topErrorMessageLabel,
                  PositionProfileUserOptions_bottomErrorMessageLabel,
                  Resources.HCMResource.PositionProfileUserOptions_PleaseSelectAFormType);

                    return;
                }
            }
            catch (Exception exception)
            {
                base.ShowMessage(PositionProfileUserOptions_topErrorMessageLabel,
                  PositionProfileUserOptions_bottomErrorMessageLabel, exception.Message);
                Logger.Write(exception.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                         

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that loads values into user input controls such
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected override void LoadValues()
        {

            //Get the existing form for the corresponding tenant ID
            PositionProfileUserOptions_formLayoutDropDownList.DataSource =
                new PositionProfileBLManager().GetFormWithUsers(base.userID);
            PositionProfileUserOptions_formLayoutDropDownList.DataBind();

            PositionProfileUserOptions_formLayoutDropDownList.Items.Insert(0,
                new ListItem("--Select--", "0"));

            //Get the default form for the corresponding userID
            int defaultFormId = new AdminBLManager().GetDefaultForm(base.userID);

            //Select the corresponding default form 
            foreach (ListItem item in PositionProfileUserOptions_formLayoutDropDownList.Items)
            {
                if (item.Value == defaultFormId.ToString())
                {
                    item.Selected = true;
                }
            }
        }
        #endregion Protected Overridden Methods
    }
}