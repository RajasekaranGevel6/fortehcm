﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PositionProfileRecruiterAssignment.aspx.cs
// File that represents the user interface layout and functionalities
// for the PositionProfileRecruiterAssignment page. 
// This page helps in assigning recruiters for the this position profile.
// This class inherits the Forte.HCM.UI.Common.PageBase class.

#endregion

#region Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.UI.PositionProfile
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the PositionProfileRecruiterAssignment page. 
    /// This page helps in assigning recruiters for the this position profile
    /// This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>

    public partial class PositionProfileRecruiterAssignment : PageBase
    {
        #region Private Variables

        /// <summary>
        /// A <see cref="int"/> that hold the position profile ID.
        /// </summary>
        private int positionProfileID = 0;

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="positionProfileDetail">
        /// A <see cref="PositionProfileDetail"/> that holds the position profile detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegate(PositionProfileDetail positionProfileDetail, EntityType entityType);

        #endregion Private Variables

        #region Event Handler

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set the page title
                Master.SetPageCaption("Position Profile Recruiter Assignment");

                // Check if position profile ID is passed.
                if (Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                {
                    base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                        PositionProfileRecruiterAssignment_bottomErrorMessageLabel, "Position profile ID parameter missing");

                    return;
                }

                // Get position profile ID.
                int.TryParse(Request.QueryString["positionprofileid"], out positionProfileID);

                if (positionProfileID <= 0)
                {
                    base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                        PositionProfileRecruiterAssignment_bottomErrorMessageLabel, "Position profile is invalid");

                    return;
                }

                // Check if user is having permission for this page.
                if (positionProfileID!=0)
                {
                    if (!base.HasPositionProfileOwnerRights(positionProfileID, base.userID, new List<string> 
                    { Constants.PositionProfileOwners.OWNER, Constants.PositionProfileOwners.CO_OWNER, Constants.PositionProfileOwners.RECRUITER_ASSIGNMENT_CREATOR }))
                    {
                        // Redirect to review page.
                        Response.Redirect(@"~\PositionProfile\PositionProfileReview.aspx?m=1&s=0&positionprofileid=" +
                            positionProfileID, false);
                    }
                }

                if (!IsPostBack)
                {
                    //Reset view state values
                    ViewState["POSITION_PROFILE_OWNERS"] = null;

                    // Assign default sort field and order keys.
                    if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "ACTIVE_POSITION_ASSIGNMENTS";

                    // Bind position profile detail.
                    BindPositionProfileDetail(positionProfileID);

                    // Bind recruiter details
                    BindPositionProfileRecruiters(positionProfileID);

                    // Bind position profile status
                    BindPositionProfileStatus();

                    // Load all recruiters
                    LoadRecruiterDetail("RA",1);

                    PositionProfileRecruiterAssignment_allRecruitersRadioButton.Checked = true;
                    
                    // Bind recent months
                    BindRecentMonths();
                }

                // Subscribe to recruiter list paging event.
                PositionProfileRecruiterAssignment_recruitersPageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler(
                    PositionProfileRecruiterAssignment_recruitersPageNavigator_PageNumberClick);

                // Subscribe to recruiter detailed view paging event.
                PositionProfileRecruiterAssignment_recruitersDetailedViewPageNavigator.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler(
                    PositionProfileRecruiterAssignment_recruitersDetailedViewPageNavigator_PageNumberClick);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                    PositionProfileRecruiterAssignment_bottomErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when row command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileRecruiterAssignment_profileRecruitersGridView_RowCommand(object sender,
            GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteRecruiter")
                {
                    // Retrieve grid view row.
                    GridViewRow gridRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);

                    HiddenField PositionProfileRecruiterAssignment_profileRecruitersGridView_recruiterNameHiddenField =
                        (HiddenField)gridRow.FindControl("PositionProfileRecruiterAssignment_profileRecruitersGridView_recruiterNameHiddenField");

                    HiddenField PositionProfileRecruiterAssignment_profileRecruitersGridView_recruiterEMailHiddenField =
                        (HiddenField)gridRow.FindControl("PositionProfileRecruiterAssignment_profileRecruitersGridView_recruiterEMailHiddenField");

                    int userID = Convert.ToInt32(e.CommandArgument.ToString());

                    new PositionProfileBLManager().DeletePositionProfileOwner(positionProfileID,
                        Constants.PositionProfileOwners.CANDIDATE_RECRUITER, userID);

                    try
                    {
                        // Compose position profile detail.
                        PositionProfileDetail positionProfileDetail = new PositionProfileDetail();
                        positionProfileDetail.PositionProfileID = positionProfileID;
                        positionProfileDetail.TaskOwnerEMail = PositionProfileRecruiterAssignment_profileRecruitersGridView_recruiterEMailHiddenField.Value;
                        positionProfileDetail.TaskOwnerName = PositionProfileRecruiterAssignment_profileRecruitersGridView_recruiterNameHiddenField.Value;
                        positionProfileDetail.TaskOwnerType = Constants.PositionProfileOwners.CANDIDATE_RECRUITER;
                        positionProfileDetail.PositionProfileName = PositionProfileRecruiterAssignment_positionProfileNameHiddenField.Value;
                        positionProfileDetail.ClientName = PositionProfileRecruiterAssignment_clientNameHiddenField.Value;

                        // Sent alert mail to the associated user asynchronously.
                        AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendStatusChangeAlertEmail);
                        IAsyncResult result = taskDelegate.BeginInvoke(positionProfileDetail,
                            EntityType.DissociatedFromPositionProfile, new AsyncCallback(SendAlertEmailCallBack), taskDelegate);
                    }
                    catch (Exception exp)
                    {
                        Logger.ExceptionLog(exp);
                    }

                    BindPositionProfileRecruiters(positionProfileID);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                  PositionProfileRecruiterAssignment_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the data is bound to the grid row.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileRecruiterAssignment_recruitersGridView_OnRowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    #region Assign Recruiter Profile

                    LinkButton PositionProfileRecruiterAssignment_recruitersGridView_nameLinkButton = (LinkButton)
                        e.Row.FindControl("PositionProfileRecruiterAssignment_recruitersGridView_nameLinkButton");

                    ImageButton PositionProfileRecruiterAssignment_recruitersGridView_addRecruiterImageButton = (ImageButton)
                       e.Row.FindControl("PositionProfileRecruiterAssignment_recruitersGridView_addRecruiterImageButton");

                    int recruiterID = 0;
                    HiddenField PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField = (HiddenField)
                        e.Row.FindControl("PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField");

                    if (!Utility.IsNullOrEmpty(PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField.Value))
                        recruiterID = Convert.ToInt32(PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField.Value);


                    List<UserDetail> positionOwners = null;
                    if (ViewState["POSITION_PROFILE_OWNERS"] == null)
                        positionOwners = new List<UserDetail>();
                    else
                        positionOwners = (List<UserDetail>)ViewState["POSITION_PROFILE_OWNERS"];

                    if (!Utility.IsNullOrEmpty(positionOwners) && positionOwners.Count != 0 &&
                        (positionOwners.Any(res => res.UserID == recruiterID &&
                            (res.OwnerType.Trim() == Constants.PositionProfileOwners.OWNER.Trim() ||
                            res.OwnerType.Trim() == Constants.PositionProfileOwners.CO_OWNER.Trim()))))
                    {
                        PositionProfileRecruiterAssignment_recruitersGridView_addRecruiterImageButton.Visible = false;
                    }
                    else
                    {
                        PositionProfileRecruiterAssignment_recruitersGridView_addRecruiterImageButton.Visible = true;
                    }


                    // Add click handler.
                    PositionProfileRecruiterAssignment_recruitersGridView_nameLinkButton.Attributes.Add("onclick",
                        "javascript:return ShowRecruiterProfile('" + recruiterID + "','RP');");

                    #endregion Assign Recruiter Profile
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                  PositionProfileRecruiterAssignment_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the data is bound to the grid row.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileRecruiterAssignment_profileRecruitersGridView_OnRowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    #region Assign Recruiter Profile

                    LinkButton PositionProfileRecruiterAssignment_profileRecruitersGridView_nameLinkButton = (LinkButton)
                        e.Row.FindControl("PositionProfileRecruiterAssignment_profileRecruitersGridView_nameLinkButton");

                    int recruiterID = 0;
                    HiddenField PositionProfileRecruiterAssignment_profileRecruitersGridView_recruiterIDHiddenField = (HiddenField)
                        e.Row.FindControl("PositionProfileRecruiterAssignment_profileRecruitersGridView_recruiterIDHiddenField");

                    if (!Utility.IsNullOrEmpty(PositionProfileRecruiterAssignment_profileRecruitersGridView_recruiterIDHiddenField.Value))
                        recruiterID = Convert.ToInt32(PositionProfileRecruiterAssignment_profileRecruitersGridView_recruiterIDHiddenField.Value);

                    // Add click handler.
                    PositionProfileRecruiterAssignment_profileRecruitersGridView_nameLinkButton.Attributes.Add("onclick",
                        "javascript:return ShowRecruiterProfile('" + recruiterID + "','RP');");

                    #endregion Assign Recruiter Profile
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                  PositionProfileRecruiterAssignment_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the data is bound to the grid row.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileRecruiterAssignment_recruitersDetailedView_OnRowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    #region Assign View Client

                    ImageButton PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_previewImageButton = (ImageButton)
                        e.Row.FindControl("PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_previewImageButton");


                    LinkButton PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_clientNameLinkButton = (LinkButton)
                        e.Row.FindControl("PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_clientNameLinkButton");

                    HyperLink PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_positionNameHyperLink = (HyperLink)
                        e.Row.FindControl("PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_positionNameHyperLink");

                    int positionID = 0;
                    HiddenField PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_positionProfileIDHiddenField = (HiddenField)
                        e.Row.FindControl("PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_positionProfileIDHiddenField");

                    if (!Utility.IsNullOrEmpty(PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_positionProfileIDHiddenField.Value))
                        positionID = Convert.ToInt32(PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_positionProfileIDHiddenField.Value);

                    int clientID = 0;
                    HiddenField PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_clientIDHiddenField = (HiddenField)
                        e.Row.FindControl("PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_clientIDHiddenField");

                    if (!Utility.IsNullOrEmpty(PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_clientIDHiddenField.Value))
                        clientID = Convert.ToInt32(PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_clientIDHiddenField.Value);

                    // Add click handler.
                    PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_clientNameLinkButton.Attributes.Add("onclick",
                        "javascript:return ShowViewClient('" + clientID + "');");

                    PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_positionNameHyperLink.NavigateUrl =
                           "~/PositionProfile/PositionProfileReview.aspx?m=1&s=0&positionprofileid=" + positionID + "&parentpage=S_POS_PRO";

                    PositionProfileRecruiterAssignment_recruitersDetailedViewGridView_previewImageButton.Attributes.Add("onclick",
                        "javascript:return ViewPositionProfile('" + positionID + "');");

                    #endregion Assign View Client
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                  PositionProfileRecruiterAssignment_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when row radion button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileRecruiterAssignment_recruitersGridView_viewPositionRadioButton_CheckedChanged(
            object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow row in PositionProfileRecruiterAssignment_recruitersGridView.Rows)
                {
                    RadioButton PositionProfileRecruiterAssignment_recruitersGridView_viewPositionUnCheckedRadioButton =
                   (RadioButton)row.FindControl("PositionProfileRecruiterAssignment_recruitersGridView_viewPositionRadioButton");

                    PositionProfileRecruiterAssignment_recruitersGridView_viewPositionUnCheckedRadioButton.Checked = false;
                }

                //Set the clicked row
                RadioButton radioButton = (RadioButton)sender;
                GridViewRow currentRow = (GridViewRow)radioButton.NamingContainer;

                RadioButton PositionProfileRecruiterAssignment_recruitersGridView_viewPositionRadioButton =
                    (RadioButton)currentRow.FindControl("PositionProfileRecruiterAssignment_recruitersGridView_viewPositionRadioButton");
                PositionProfileRecruiterAssignment_recruitersGridView_viewPositionRadioButton.Checked = true;


                LinkButton PositionProfileRecruiterAssignment_recruitersGridView_nameLinkButton =
                    (LinkButton)currentRow.FindControl("PositionProfileRecruiterAssignment_recruitersGridView_nameLinkButton");

                HiddenField PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField =
                    (HiddenField)currentRow.FindControl("PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField");

                PositionProfileRecruiterAssignment_recruiterNameLabel.Text=
                    PositionProfileRecruiterAssignment_recruitersGridView_nameLinkButton.Text.ToString();
                // Load assignment details.
                if (!Utility.IsNullOrEmpty(PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField.Value))
                {
                    PositionProfileRecruiterAssignment_recruiterIDHiddenField.Value = 
                        PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField.Value;
                    LoadAssignmentDetails(Convert.ToInt32(PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField.Value), "PPS_OPEN", 1);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                  PositionProfileRecruiterAssignment_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the recruiters grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        private void PositionProfileRecruiterAssignment_recruitersPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                // Load the recruiter list
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "ACTIVE_POSITION_ASSIGNMENTS";

                // Reset the paging control.
                PositionProfileRecruiterAssignment_recruitersPageNavigator.Reset();

                string filterBy = string.Empty;

                if (PositionProfileRecruiterAssignment_currentRecruitersRadioButton.Checked)
                    filterBy = "RS";
                if (PositionProfileRecruiterAssignment_earlierRecruitersRadioButton.Checked)
                    filterBy = "RC";
                if (PositionProfileRecruiterAssignment_allRecruitersRadioButton.Checked)
                    filterBy = "RA";

                PositionProfileRecruiterAssignment_showRecruitersDetailedViewDiv.Style["display"] = "none";

                LoadRecruiterDetail(filterBy, e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the recruiters details grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        private void PositionProfileRecruiterAssignment_recruitersDetailedViewPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                // Load assignment details.
                if(Utility.IsNullOrEmpty(PositionProfileRecruiterAssignment_recruiterIDHiddenField.Value))
                {
                    LoadAssignmentDetails(Convert.ToInt32(
                        PositionProfileRecruiterAssignment_recruiterIDHiddenField.Value),
                        PositionProfileRecruiterAssignment_positionProfileStatusDropDown.
                        SelectedItem.Value.ToString().Trim(),e.PageNumber);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void PositionProfileRecruiterAssignment_recruitersGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                string filterBy=string.Empty;

                if(PositionProfileRecruiterAssignment_currentRecruitersRadioButton.Checked)
                    filterBy="RS";
                if (PositionProfileRecruiterAssignment_earlierRecruitersRadioButton.Checked)
                    filterBy="RC";
                if(PositionProfileRecruiterAssignment_allRecruitersRadioButton.Checked)
                    filterBy="RA";

                PositionProfileRecruiterAssignment_showRecruitersDetailedViewDiv.Style["display"] = "none";

                // Reset and show records for first page.
                PositionProfileRecruiterAssignment_recruitersPageNavigator.Reset();

                LoadRecruiterDetail(filterBy, 1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void PositionProfileRecruiterAssignment_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (PositionProfileRecruiterAssignment_recruitersGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when row command is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileRecruiterAssignment_recruitersGridView_RowCommand(object sender,
            GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "AssignRecruiter")
                {
                    int userID = Convert.ToInt32(e.CommandArgument.ToString());

                    if (!Utility.IsNullOrEmpty(userID))
                    {
                        // Retrieve grid view row.
                        GridViewRow gridRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);

                        HiddenField PositionProfileRecruiterAssignment_recruitersGridView_recruiterNameHiddenField =
                            (HiddenField)gridRow.FindControl("PositionProfileRecruiterAssignment_recruitersGridView_recruiterNameHiddenField");

                        HiddenField PositionProfileRecruiterAssignment_recruitersGridView_recruiterEMailHiddenField =
                            (HiddenField)gridRow.FindControl("PositionProfileRecruiterAssignment_recruitersGridView_recruiterEMailHiddenField");

                        List<UserDetail> positionOwners = null;
                        if (ViewState["POSITION_PROFILE_OWNERS"] == null)
                            positionOwners = new List<UserDetail>();
                        else
                            positionOwners = (List<UserDetail>)ViewState["POSITION_PROFILE_OWNERS"];

                        bool sendMail = false;
                        if (Utility.IsNullOrEmpty(positionOwners) || positionOwners.Count == 0)
                        {
                            new PositionProfileBLManager().InsertPositionProfileOwner(positionProfileID,
                                Constants.PositionProfileOwners.CANDIDATE_RECRUITER, userID, base.userID);

                            sendMail = true;
                        }
                        else if (!(!Utility.IsNullOrEmpty(positionOwners) && positionOwners.Count != 0 &&
                            (positionOwners.Any(res => res.UserID == userID &&
                                (res.OwnerType.Trim() == Constants.PositionProfileOwners.OWNER.Trim() ||
                                res.OwnerType.Trim() == Constants.PositionProfileOwners.CO_OWNER.Trim())))))
                        {
                            new PositionProfileBLManager().InsertPositionProfileOwner(positionProfileID,
                               Constants.PositionProfileOwners.CANDIDATE_RECRUITER, userID, base.userID);

                            sendMail = true;
                        }

                        if (sendMail)
                        {
                            try
                            {
                                // Compose position profile detail.
                                PositionProfileDetail positionProfileDetail = new PositionProfileDetail();
                                positionProfileDetail.PositionProfileID = positionProfileID;
                                positionProfileDetail.TaskOwnerEMail = PositionProfileRecruiterAssignment_recruitersGridView_recruiterEMailHiddenField.Value;
                                positionProfileDetail.TaskOwnerName = PositionProfileRecruiterAssignment_recruitersGridView_recruiterNameHiddenField.Value;
                                positionProfileDetail.TaskOwnerType = Constants.PositionProfileOwners.CANDIDATE_RECRUITER;
                                positionProfileDetail.PositionProfileName = PositionProfileRecruiterAssignment_positionProfileNameHiddenField.Value;
                                positionProfileDetail.ClientName = PositionProfileRecruiterAssignment_clientNameHiddenField.Value;

                                // Sent alert mail to the associated user asynchronously.
                                AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendStatusChangeAlertEmail);
                                IAsyncResult result = taskDelegate.BeginInvoke(positionProfileDetail,
                                    EntityType.AssociatedWithPositionProfile, new AsyncCallback(SendAlertEmailCallBack), taskDelegate);
                            }
                            catch (Exception exp)
                            {
                                Logger.ExceptionLog(exp);
                            }
                        }
                        BindPositionProfileRecruiters(positionProfileID);
                    }
                }
                if (e.CommandName == "ViewPositionsProfile")
                {
                   LinkButton linkButton = (LinkButton)e.CommandSource;
                   GridViewRow row = (GridViewRow)linkButton.Parent.Parent;

                   LinkButton PositionProfileRecruiterAssignment_recruitersGridView_nameLinkButton =
                   (LinkButton)row.FindControl("PositionProfileRecruiterAssignment_recruitersGridView_nameLinkButton");

                    HiddenField PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField =
                        (HiddenField)row.FindControl("PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField");

                    PositionProfileRecruiterAssignment_recruiterNameLabel.Text =
                        PositionProfileRecruiterAssignment_recruitersGridView_nameLinkButton.Text.ToString();

                    // Load assignment details.
                    if (!Utility.IsNullOrEmpty(PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField.Value))
                    {
                        PositionProfileRecruiterAssignment_recruiterIDHiddenField.Value =
                            PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField.Value;
                        LoadAssignmentDetails(Convert.ToInt32(
                            PositionProfileRecruiterAssignment_recruitersGridView_userIDHiddenField.Value), "PPS_OPEN", 1);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                  PositionProfileRecruiterAssignment_bottomErrorMessageLabel, exp.Message);
            }
        }
        
        /// <summary>
        /// Handler method that is called when the save and continue button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileRecruiterAssignment_saveContinueButton_Click(
            object sender, EventArgs e)
        {
            try
            {
                int positionProfileID=0;
                if(!Utility.IsNullOrEmpty(Request.QueryString["positionProfileID"]))
                    positionProfileID = 
                        Convert.ToInt32(Request.QueryString["positionProfileID"].ToString());
                
                Response.Redirect("~/PositionProfile/PositionProfileReview.aspx?m=1&s=0&positionprofileid="
                    + positionProfileID, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                  PositionProfileRecruiterAssignment_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when current recruiter radion button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileRecruiterAssignment_currentRecruitersRadioButton_CheckedChanged(
            object sender, EventArgs e)
        {
            try
            {
                // Load the recruiter list
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "ACTIVE_POSITION_ASSIGNMENTS";

                // Reset the paging control.
                PositionProfileRecruiterAssignment_recruitersPageNavigator.Reset();

                //Hide position details
                PositionProfileRecruiterAssignment_showRecruitersDetailedViewDiv.Style["display"] = "none";

                //Show only recruiters that currently/recently worked on positions requiring similar skills 
                LoadRecruiterDetail("RS",1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                  PositionProfileRecruiterAssignment_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when earlier recruiter radion button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileRecruiterAssignment_earlierRecruitersRadioButton_CheckedChanged(
            object sender, EventArgs e)
        {
            try
            {
                // Load the recruiter list
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "ACTIVE_POSITION_ASSIGNMENTS";

                // Reset the paging control.
                PositionProfileRecruiterAssignment_recruitersPageNavigator.Reset();

                //Hide position details
                PositionProfileRecruiterAssignment_showRecruitersDetailedViewDiv.Style["display"] = "none";

                //Show only recruiters that have been assigned requirements from this client earlier 
                LoadRecruiterDetail("RC",1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                  PositionProfileRecruiterAssignment_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when all recruiter radion button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileRecruiterAssignment_allRecruitersRadioButton_CheckedChanged(
            object sender, EventArgs e)
        {
            try
            {
                // Load the recruiter list
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "ACTIVE_POSITION_ASSIGNMENTS";

                // Reset the paging control.
                PositionProfileRecruiterAssignment_recruitersPageNavigator.Reset();

                //Hide position details
                PositionProfileRecruiterAssignment_showRecruitersDetailedViewDiv.Style["display"] = "none";

                //Show all recruiter
                LoadRecruiterDetail("RA",1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                  PositionProfileRecruiterAssignment_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when go to workflow selection link is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void PositionProfileRecruiterAssignment_goToWorkFlowLinkButton_Click(
            object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/PositionProfile/PositionProfileWorkflowSelection.aspx?m=1&s=0&positionprofileid="
                    + Request.QueryString.Get("positionprofileid"), false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                  PositionProfileRecruiterAssignment_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Hanlder to show or hide the values/reason
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PositionProfileRecruiterAssignment_positionProfileStatusDropDown_SelectedIndexChanged(
            object sender, EventArgs e)
        {
            try
            {
                string positionProfileStatus=
                    PositionProfileRecruiterAssignment_positionProfileStatusDropDown.SelectedItem.Value.ToString().Trim();
                int recruiterID = 0;
                if (!Utility.IsNullOrEmpty(PositionProfileRecruiterAssignment_recruiterIDHiddenField.Value))
                    recruiterID = Convert.ToInt32(PositionProfileRecruiterAssignment_recruiterIDHiddenField.Value);

                LoadAssignmentDetails(recruiterID, positionProfileStatus, 1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                  PositionProfileRecruiterAssignment_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when drop down item is selected.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void PositionProfileRecruiterAssignment_recentMonthsDropDownList_SelectedIndexChanged(object sender, 
            EventArgs e)
        {
            try
            {
                PositionProfileRecruiterAssignment_showRecruitersDetailedViewDiv.Style["display"] = "none";
                LoadRecruiterDetail("RS", 1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileRecruiterAssignment_topErrorMessageLabel,
                  PositionProfileRecruiterAssignment_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handler

        #region Private Method

        /// <summary>
        /// Bind position profile status
        /// </summary>
        private void BindPositionProfileStatus()
        {
            PositionProfileRecruiterAssignment_positionProfileStatusDropDown.DataSource =
                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.POSITION_PROFILE_STATUS, "A");
            PositionProfileRecruiterAssignment_positionProfileStatusDropDown.DataTextField = "AttributeName";
            PositionProfileRecruiterAssignment_positionProfileStatusDropDown.DataValueField = "AttributeID";
            PositionProfileRecruiterAssignment_positionProfileStatusDropDown.DataBind();

            PositionProfileRecruiterAssignment_positionProfileStatusDropDown.Items.Insert(0, "All");
            PositionProfileRecruiterAssignment_positionProfileStatusDropDown.SelectedIndex = 0;
        }

        /// <summary>
        /// Method to bind the recent months drop down
        /// </summary>
        private void BindRecentMonths()
        {
            for (int i = 1; i <= 6; i++)
            {
                // Construct list items.
                PositionProfileRecruiterAssignment_recentMonthsDropDownList.Items.Add
                    (new ListItem(i == 1 ? string.Format("Past {0} Month", i) : string.Format("Past {0} Months", i), i.ToString()));
            }

            PositionProfileRecruiterAssignment_recentMonthsDropDownList.SelectedIndex = 1;
        }

        /// <summary>
        /// Method that loads the assignment details.
        /// </summary>
        /// <param name="recruiterID">The recruiter id</param>
        /// <param name="PositionProfileStatus">The position profile status</param>
        /// <param name="pageNumber">The page number</param>
        private void LoadAssignmentDetails(int recruiterID, 
            string PositionProfileStatus, int pageNumber)
        {
            int totalRecords = 0;
            
            PositionProfileRecruiterAssignment_showRecruitersDetailedViewDiv.Style["display"] = "block";

            if (PositionProfileStatus == "All")
                PositionProfileStatus = null;

            // Get recruiter assignment details.
            string filterBy = "RA";
            if (PositionProfileRecruiterAssignment_currentRecruitersRadioButton.Checked)
                filterBy = "RS";
            if (PositionProfileRecruiterAssignment_earlierRecruitersRadioButton.Checked)
                filterBy = "RC";

            List<RecruiterAssignmentDetail> assignementDetails = null;

            // Get recruiter assignment details.
            if (filterBy == "RA")
            {
                assignementDetails = new PositionProfileBLManager().
                    GetRecruiterAssignments(recruiterID, PositionProfileStatus, pageNumber, 
                    base.GridPageSize, out totalRecords);
            }
            else
            {
                assignementDetails = new PositionProfileBLManager().GetRecruiterAssignmentDetail(base.tenantID,
                    positionProfileID, PositionProfileStatus, "RD", recruiterID, -2, filterBy, pageNumber,
                    base.GridPageSize, ViewState["SORT_FIELD"].ToString(),
                    (SortType)ViewState["SORT_ORDER"], out totalRecords);
            }
            if (assignementDetails != null && assignementDetails.Count > 0)
            {
                PositionProfileRecruiterAssignment_recruitersDetailedViewPageNavigator.Visible = true;
                PositionProfileRecruiterAssignment_recruitersDetailedViewPageNavigator.TotalRecords = totalRecords;
                PositionProfileRecruiterAssignment_recruitersDetailedViewPageNavigator.PageSize = base.GridPageSize;

                PositionProfileRecruiterAssignment_recruitersDetailedViewGridView.DataSource = assignementDetails;
                PositionProfileRecruiterAssignment_recruitersDetailedViewGridView.DataBind();
            }
            else
            {
                PositionProfileRecruiterAssignment_recruitersDetailedViewPageNavigator.Visible = false;
                PositionProfileRecruiterAssignment_recruitersDetailedViewGridView.DataSource = null;
                PositionProfileRecruiterAssignment_recruitersDetailedViewGridView.DataBind();
            }

            if(Utility.IsNullOrEmpty(PositionProfileStatus))
                PositionProfileRecruiterAssignment_positionProfileStatusDropDown.SelectedIndex = 0;
            else
                PositionProfileRecruiterAssignment_positionProfileStatusDropDown.SelectedValue =
                    PositionProfileStatus;
        }

        /// <summary>
        /// Method that retrieves the position profile detail and assign it the 
        /// hidden fields for later reference.
        /// </summary>
        private void BindPositionProfileDetail(int positionProfileID)
        {
            PositionProfileDetail positionProfileDetail = new PositionProfileBLManager().GetPositionProfile(positionProfileID);

            if (positionProfileDetail == null)
                return;

            // Assign name & client to hidden fields.
            PositionProfileRecruiterAssignment_positionProfileNameHiddenField.Value = positionProfileDetail.PositionName;
            PositionProfileRecruiterAssignment_clientNameHiddenField.Value = positionProfileDetail.ClientName;
        }

        /// <summary>
        /// Method to get the position profile recruiters by profile id
        /// </summary>
        private void BindPositionProfileRecruiters(int positionProfileID)
        {
            List<UserDetail> ownerDetails = new List<UserDetail>();
            ownerDetails = new PositionProfileBLManager().
               GetPositionProfileTaskOwners(positionProfileID, base.userID);

            var recruiterList = Enumerable.Where(ownerDetails,
                t => t.OwnerType.Trim() == Constants.PositionProfileOwners.CANDIDATE_RECRUITER.Trim() ||
                    t.OwnerType.Trim() == Constants.PositionProfileOwners.OWNER.Trim() ||
                    t.OwnerType.Trim() == Constants.PositionProfileOwners.CO_OWNER.Trim());

            var ownerList = Enumerable.Where(ownerDetails,
                o => o.OwnerType.Trim() == Constants.PositionProfileOwners.OWNER.Trim() ||
                    o.OwnerType.Trim() == Constants.PositionProfileOwners.CO_OWNER.Trim());

            if(ownerList!=null)
                ViewState["POSITION_PROFILE_OWNERS"] = ownerList.ToList();

            if (recruiterList != null)
            {
                PositionProfileRecruiterAssignment_profileRecruitersGridView.DataSource = recruiterList;
                PositionProfileRecruiterAssignment_profileRecruitersGridView.DataBind();
            }
            else
            {
                PositionProfileRecruiterAssignment_profileRecruitersGridView.DataSource = null;
                PositionProfileRecruiterAssignment_profileRecruitersGridView.DataBind();
            }
        }

        /// <summary>
        /// Method to get the recruiter list against tenant id
        /// </summary>
        private void LoadRecruiterDetail(string filterBy, int pageNumber)
        {
            int totalRecords = 0;
            int recentMonth = -2;

            if (filterBy == "RS")
            {
                PositionProfileRecruiterAssignment_recruiterTitleLabel.Text =
                    PositionProfileRecruiterAssignment_currentRecruitersRadioButton.Text.ToString();
                PositionProfileRecruiterAssignment_recentMonthDiv.Style["display"] = "block";
                recentMonth = -(Convert.ToInt32(PositionProfileRecruiterAssignment_recentMonthsDropDownList.SelectedItem.Value));
            }
            if (filterBy == "RC")
            {
                PositionProfileRecruiterAssignment_recruiterTitleLabel.Text =
                    PositionProfileRecruiterAssignment_earlierRecruitersRadioButton.Text.ToString();
                PositionProfileRecruiterAssignment_recentMonthDiv.Style["display"] = "none";
            }
            if (filterBy == "RA")
            {
                PositionProfileRecruiterAssignment_recruiterTitleLabel.Text =
                    PositionProfileRecruiterAssignment_allRecruitersRadioButton.Text.ToString();
                PositionProfileRecruiterAssignment_recentMonthDiv.Style["display"] = "none";
            }

            List<RecruiterDetail> recruiterDetails = null;

            if (filterBy == "RA")
            {
                recruiterDetails = new List<RecruiterDetail>();
                recruiterDetails = new PositionProfileBLManager().GetAllRecruiter(base.tenantID,
                    pageNumber,base.GridPageSize, ViewState["SORT_FIELD"].ToString(),
                    (SortType)ViewState["SORT_ORDER"], out totalRecords);
            }
            else
            {
                recruiterDetails = new List<RecruiterDetail>();
                recruiterDetails = new PositionProfileBLManager().GetRecruiter(base.tenantID,
                    positionProfileID, "PPS_OPEN", "SSD", 0, recentMonth, filterBy, pageNumber,
                    base.GridPageSize, ViewState["SORT_FIELD"].ToString(),
                    (SortType)ViewState["SORT_ORDER"], out totalRecords);
            }

            if (recruiterDetails != null && recruiterDetails.Count > 0)
            {
                PositionProfileRecruiterAssignment_recruitersPageNavigator.Visible = true;
                PositionProfileRecruiterAssignment_recruitersPageNavigator.TotalRecords = totalRecords;
                PositionProfileRecruiterAssignment_recruitersPageNavigator.PageSize = base.GridPageSize;

                PositionProfileRecruiterAssignment_recruitersGridView.DataSource = recruiterDetails;
                PositionProfileRecruiterAssignment_recruitersGridView.DataBind();
            }
            else
            {
                PositionProfileRecruiterAssignment_recruitersPageNavigator.Visible = false;
                PositionProfileRecruiterAssignment_recruitersGridView.DataSource = null;
                PositionProfileRecruiterAssignment_recruitersGridView.DataBind();
            }
        }

        /// <summary>
        /// Method that will clear success/error label messages.
        /// </summary>
        private void ClearLabelMessage()
        {
            PositionProfileRecruiterAssignment_topSuccessMessageLabel.Text = string.Empty;
            PositionProfileRecruiterAssignment_bottomSuccessMessageLabel.Text = string.Empty;
            PositionProfileRecruiterAssignment_topErrorMessageLabel.Text = string.Empty;
            PositionProfileRecruiterAssignment_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Method that send email to position profile associates indicating
        /// the status change of position profile.
        /// </summary>
        /// <param name="positionProfileDetail">
        /// A <see cref="PositionProfileDetail"/> that holds the position profile 
        /// detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type. In this case 
        /// AssociatedWithPositionProfile & DissociatedFromPositionProfile are
        /// the possible values.
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendStatusChangeAlertEmail(PositionProfileDetail positionProfileDetail, EntityType entityType)
        {
            try
            {
                // Construct position profile detail.
                // Send email.
                new EmailHandler().SendMail(entityType, positionProfileDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion  Private Method

        #region Asynchronous Method Handlers

        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendAlertEmailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Asynchronous Method Handlers

        #region Protected Overridden Methods

        protected override bool IsValidData()
        {
            ClearLabelMessage();
            bool isValidData = true;
            

            return isValidData;
        }

        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}