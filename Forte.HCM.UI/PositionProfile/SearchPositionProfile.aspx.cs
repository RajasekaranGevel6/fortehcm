﻿#region Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.ExternalService;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.PositionProfile
{
    /// <summary>
    /// Represents the class that used to search the position profile. 
    /// Position profiles are searched based on the client details if provided,
    /// else all the position profiles are listed
    /// </summary>
    public partial class SearchPositionProfile : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.220
        /// </summary>
        private const string RESTORED_HEIGHT = "340px";

        /// <summary>
        /// 
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.300
        /// </summary>
        private const string EXPANDED_HEIGHT = "420px";

        #endregion Private Constants

        #region Private Variables                                              

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        private delegate void AsyncTaskDelegate();

        #endregion Private Variables

        #region Event Handlers                                                 

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Assign parent page navigation links.
                AssignParentPageNavigationLink();

                SearchPositionProfile_topErrorMessageLabel.Text = string.Empty;
                SearchPositionProfile_bottomErrorMessageLabel.Text = string.Empty;
                SearchPositionProfile_topSuccessMessageLabel.Text = string.Empty;
                SearchPositionProfile_bottomSuccessMessageLabel.Text = string.Empty;

                SearchPositionProfile_pageNavigator.PageNumberClick +=
                   new PageNavigator.PageNumberClickEventHandler
                       (SearchPositionProfile_pageNavigator_PageNumberClick);

                Page.Form.DefaultButton = SearchPostionProfile_topSearchButton.UniqueID;

                SearchPositionProfile_createdByTextBox.Attributes.Add("readonly", "readonly");

                if (!IsPostBack)
                {
                    if (!Utility.IsNullOrEmpty(Request.QueryString.Get("pp_session_key")))
                    {
                        SearchPositionProfile_tsSessionKeyHiddenField.Value = Request.QueryString.Get("pp_session_key");

                        // Show a message to user.
                        if (!Utility.IsNullOrEmpty(Request.QueryString.Get("type"))
                            && Request.QueryString.Get("type").ToUpper() == "TS_PICK")
                        {
                            base.ShowMessage(SearchPositionProfile_topSuccessMessageLabel,
                                SearchPositionProfile_bottomSuccessMessageLabel, 
                                "You can click on the " + "<img src='../App_Themes/DefaultTheme/Images/icon_pp_associate_candidate.gif' />"  + " icon on the desired row to associate the picked candidates with the position profile");
                        }
                    }

                    SearchPostionProfile_searchProfileResultsTR.Attributes.Add("onclick",
                        "ExpandOrRestore('" +
                        SearchPostionProfile_profileGridViewUpdatePanelDiv.ClientID + "','" +
                        SearchPostionProfile_simpleSearchDiv.ClientID + "','" +
                        SearchPostionProfile_searchResultsUpSpan.ClientID + "','" +
                        SearchPostionProfile_searchResultsDownSpan.ClientID + "','" +
                        SearchPostionProfile_restoreHiddenField.ClientID + "','" +
                        RESTORED_HEIGHT + "','" +
                        EXPANDED_HEIGHT + "')");

                    // Set default focus field.
                    Page.Form.DefaultFocus = SearchPositionProfile_positionIDTextBox.UniqueID;
                    SearchPositionProfile_positionProfileNameTextBox.Focus();

                    SearchPositionProfile_pageNavigator.Visible = false;

                    Master.SetPageCaption("Search Position Profile");

                    // Add handler to 'created by' image button.
                    SearchPositionProfile_createdByImageButton.Attributes.Add("onclick",
                         "javascript:return LoadUserForPositionProfileModule('"
                        + SearchPositionProfile_createdBydummyAuthorIdHidenField.ClientID + "','"
                        + SearchPositionProfile_createdByHiddenField.ClientID + "','"
                        + SearchPositionProfile_createdByTextBox.ClientID + "')");

                    // Check if the page is launched from menu. If so, clear the existing
                    // session variables.
                    if ((!Utility.IsNullOrEmpty(Request.QueryString["parentpage"])) &&
                        (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU
                        || Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_LANDING || 
                        (!Utility.IsNullOrEmpty(Request.QueryString["type"]))))
                    {
                        base.ClearSearchCriteriaSession();
                    }
                    
                    ViewState["SORT_ORDER"] = SortType.Descending;
                    ViewState["SORT_FIELD"] = "DATE";

                    //Set enable/disable option
                    if (SearchPositionProfile_showMyPositionProfileCheckBox.Checked)
                        SearchPositionProfile_createdByDiv.Style.Add("display", "none");
                    else
                        SearchPositionProfile_createdByDiv.Style.Add("display", "block");
                    
                    SearchPositionProfile_showMyPositionProfileCheckBox.Attributes.Add("onclick",
                    "javascript:ShowCreatedByDiv('" + SearchPositionProfile_showMyPositionProfileCheckBox.ClientID + "', " +
                    "'" + SearchPositionProfile_createdByDiv.ClientID + "','" + SearchPositionProfile_createdByTextBox.ClientID + "', " +
                    "'" + SearchPositionProfile_createdByHiddenField.ClientID + "');");

                    // Check if page is requested from the client management tabs. If yes,
                    // then apply the specific search and show the results. Also keep the
                    // required details in the session for paging click event.
                    if (!Utility.IsNullOrEmpty(Request.QueryString["parentpage"]) &&
                        Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.CLIENT_MANAGEMENT)
                    {
                        // Clear search criteria session.
                        Session[Constants.SearchCriteriaSessionKey.SEARCH_POSITION_PROFILE] = null;

                        // Set the client management ID search status.
                        Session["FROM_CLIENT_MANAGEMENT"] = true;

                        SearchPositionProfile_pageNavigator.Reset();
                    }
                    else
                    {
                        // Check if page is redirected from any child page. If the page
                        // if redirected from any child page, fill the search criteria
                        // and apply the search.
                        if (Session[Constants.SearchCriteriaSessionKey.SEARCH_POSITION_PROFILE] != null)
                            FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_POSITION_PROFILE]
                                as PositionProfileSearchCriteria);
                    }
                    // Search the records.
                    SearchPositionProfileDetails(1);
                }

                CheckAndSetExpandorRestore();

                // Maintain div status.
                if (SearchPositionProfile_simpleHiddenField.Value.Trim().ToUpper() == "A")
                {
                    SearchPositionProfile_advancedDiv.Style["display"] = "block";
                    SearchPositionProfile_simpleLinkButton.Text = "Simple";
                    SearchPositionProfile_simpleLinkButton.ToolTip = "Click here to do simple search";  
                }
                else if (SearchPositionProfile_simpleHiddenField.Value.Trim().ToUpper() == "S")
                { 
                    SearchPositionProfile_advancedDiv.Style["display"] = "none";
                    SearchPositionProfile_simpleLinkButton.Text = "Advanced";
                    SearchPositionProfile_simpleLinkButton.ToolTip = "Click here to do advanced search"; 
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfile_topErrorMessageLabel,
                    SearchPositionProfile_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        void SearchPositionProfile_pageNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            try
            {
                SearchPostionProfile_profileGridViewUpdatePanelDiv.Visible = true;
                SearchPositionProfile_pageNavigator.Visible = true;
                ViewState["PAGENUMBER"] = e.PageNumber;
                SearchPositionProfileDetails(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfile_topErrorMessageLabel,
                    SearchPositionProfile_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the SearchPositionProfile_simpleLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> 
        /// instance containing the event data.</param>
        protected void SearchPositionProfile_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (SearchPositionProfile_simpleLinkButton.Text.ToLower() == "simple")
                {
                    SearchPositionProfile_simpleLinkButton.Text = "Advanced";
                    SearchPositionProfile_advancedDiv.Style["display"] = "none";
                }
                else
                {
                    SearchPositionProfile_simpleLinkButton.Text = "Simple";
                    SearchPositionProfile_advancedDiv.Style["display"] = "block";
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfile_topErrorMessageLabel,
                    SearchPositionProfile_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the SearchPostionProfile_topSearchButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void SearchPostionProfile_topSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Reset the client management ID search status.
                Session["FROM_CLIENT_MANAGEMENT"] = false;
                ViewState["SORT_ORDER"] = SortType.Descending;
                ViewState["SORT_FIELD"] = "DATE";
                SearchPositionProfile_pageNavigator.Reset();
                ViewState["PAGENUMBER"] = 1;
                SearchPositionProfileDetails(int.Parse(ViewState["PAGENUMBER"].ToString()));
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfile_topErrorMessageLabel,
                    SearchPositionProfile_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handles the Sorting event of the SearchPostionProfile_postionProfileGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/>
        /// instance containing the event data.</param>
        protected void SearchPostionProfile_postionProfileGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Assign the sorting and sort order
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }

                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] = ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;


                ViewState["SORT_FIELD"] = e.SortExpression;
                SearchPositionProfile_pageNavigator.Reset();
                SearchPositionProfileDetails(1);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfile_topErrorMessageLabel,
                    SearchPositionProfile_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        protected void SearchPostionProfile_postionProfileGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                ImageButton SearchPostionProfile_postionProfileGridView_deletePositionProfileImageButton =
                    (ImageButton)e.Row.FindControl("SearchPostionProfile_postionProfileGridView_deletePositionProfileImageButton");

                ImageButton SearchPostionProfile_postionProfileGridView_changeStatusImageButton =
                    (ImageButton)e.Row.FindControl("SearchPostionProfile_postionProfileGridView_changeStatusImageButton");

                ImageButton SearchPostionProfile_postionProfileGridView_associateCandidatesImageButton =
                 (ImageButton)e.Row.FindControl("SearchPostionProfile_postionProfileGridView_associateCandidatesImageButton");

                bool hasOwnerRightsHiddenField = false;
                HiddenField SearchPositionProfile_hasOwnerRightsHiddenField = (HiddenField)e.Row.FindControl("SearchPositionProfile_hasOwnerRightsHiddenField");
                if (SearchPositionProfile_hasOwnerRightsHiddenField.Value != null)
                    hasOwnerRightsHiddenField = Convert.ToBoolean(SearchPositionProfile_hasOwnerRightsHiddenField.Value);

                if (hasOwnerRightsHiddenField)
                {
                    SearchPostionProfile_postionProfileGridView_changeStatusImageButton.Visible = true;
                    SearchPostionProfile_postionProfileGridView_deletePositionProfileImageButton.Visible = true;
                }
                else
                {
                    SearchPostionProfile_postionProfileGridView_changeStatusImageButton.Visible = false;
                    SearchPostionProfile_postionProfileGridView_deletePositionProfileImageButton.Visible = false;
                }
               
                Label SearchPostionProfile_postionProfileGridView_positionProfileNameLabel =
                    (Label)e.Row.FindControl("SearchPostionProfile_postionProfileGridView_positionProfileNameLabel");

                HtmlContainerControl SearchPostionProfile_postionProfileGridView_positionProfileNameDiv =
                   (HtmlContainerControl)e.Row.FindControl("SearchPostionProfile_postionProfileGridView_positionProfileNameDiv");

                if (SearchPostionProfile_postionProfileGridView_positionProfileNameLabel!=null)
                    SearchPostionProfile_postionProfileGridView_positionProfileNameDiv.InnerText = 
                        SearchPostionProfile_postionProfileGridView_positionProfileNameLabel.Text.Trim();

                if (!Utility.IsNullOrEmpty(SearchPositionProfile_tsSessionKeyHiddenField.Value))
                {
                    SearchPostionProfile_postionProfileGridView_associateCandidatesImageButton.Visible = true;
                    SearchPostionProfile_postionProfileGridView_associateCandidatesImageButton.CommandArgument =
                        SearchPostionProfile_postionProfileGridView_associateCandidatesImageButton.CommandArgument + "-" +
                        SearchPositionProfile_tsSessionKeyHiddenField.Value;
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfile_topErrorMessageLabel,
                    SearchPositionProfile_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handles the RowCommand event of the SearchPostionProfile_postionProfileGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> 
        /// instance containing the event data.</param>
        protected void SearchPostionProfile_postionProfileGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "EditPositionProfile" && e.CommandName != "PositionProfileDashboard" &&
                    e.CommandName != "Activity" && e.CommandName != "ChangePositionStatus" && e.CommandName != "DeletePositionProfile"
                    && e.CommandName  != "AssociateCandidate")
                    return;

                // Retrieve grid view row.
                GridViewRow gridRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);

                switch (e.CommandName)
                {
                    case "EditPositionProfile":
                        {
                            Response.Redirect("~/PositionProfile/PositionProfileReview.aspx?m=1&s=0" +
                                "&positionprofileid" + "=" + e.CommandArgument +
                                "&parentpage=" + Constants.ParentPage.SEARCH_POSITION_PROFILE, false);
                        }
                        break;

                    case "PositionProfileDashboard":
                        {
                            HiddenField SearchPositionProfile_editableHiddenField =
                                (HiddenField)gridRow.FindControl("SearchPositionProfile_editableHiddenField1");

                            Response.Redirect("~/PositionProfile/PositionProfileStatus.aspx?m=1&s=1&" + Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING + "=" + e.CommandArgument +
                                "&parentpage=" + Constants.ParentPage.SEARCH_POSITION_PROFILE, false);
                        }
                        break;

                    case "Activity":
                        {
                            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(),
                                " ", "javascript:ShowPositionProfileActivityLog(" + e.CommandArgument + ")", true);
                        }
                        break;

                    case "ChangePositionStatus":
                        {
                            // Get status.
                            HiddenField SearchPostionProfile_postionProfileGridView_statusHiddenField =
                               (HiddenField)gridRow.FindControl("SearchPostionProfile_postionProfileGridView_statusHiddenField");

                            Label positionProfileLabel = (Label)gridRow.FindControl("SearchPostionProfile_postionProfileGridView_positionProfileNameLabel");
                            Label clientLabel = (Label)gridRow.FindControl("SearchPostionProfile_postionProfileGridView_clientNameLabel");

                            // Assign ID & name to hidden field.
                            SearchPositionProfile_changePositionStatusPositionProfileIDHiddenField.Value = e.CommandArgument.ToString();
                            SearchPositionProfile_changePositionStatusPositionProfileNameHiddenField.Value = positionProfileLabel.Text;
                            SearchPositionProfile_changePositionStatusClientNameHiddenField.Value = clientLabel.Text;

                            // Assign data source.
                            SearchPositionProfile_changePositionStatusControl.StatusDataSource = new AttributeBLManager().GetAttributesByType("PP_STATUS");
                            SearchPositionProfile_changePositionStatusControl.SelectedStatusValue = SearchPostionProfile_postionProfileGridView_statusHiddenField.Value.Trim();
                            SearchPositionProfile_changePositionStatusModalPopupExtender.Show();
                        }
                        break;

                    case "DeletePositionProfile":
                        {
                            Label positionProfileLabel = (Label)gridRow.FindControl("SearchPostionProfile_postionProfileGridView_positionProfileNameLabel");

                            // Assign ID & name to hidden field.
                            SearchPositionProfile_deletePositionProfileIDHiddenField.Value = e.CommandArgument.ToString();
                            SearchPositionProfile_deletePositionProfileNameHiddenField.Value = positionProfileLabel.Text;

                            SearchPositionProfile_deletePositionProfileConfirmMsgControl.Message = "Are you sure want to delete the position profile '" + positionProfileLabel.Text.Trim() + "' ?";
                            SearchPositionProfile_deletePositionProfileConfirmMsgControl.Title = "Delete Position Profile";
                            SearchPositionProfile_deletePositionProfileConfirmMsgControl.Type = DataObjects.MessageBoxType.YesNo;

                            SearchPositionProfile_deletePositionProfileModalPopupExtender.Show();
                        }
                        break;

                    case "AssociateCandidate":
                        AssociateCandidatesWithPositionProfile(e.CommandArgument.ToString());
                        break;
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfile_topErrorMessageLabel,
                    SearchPositionProfile_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Method that is called when the save button is clicked in the change 
        /// position status window.
        /// </summary>
        /// <param name="sender"> 
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void SearchPositionProfile_changePositionStatusControl_saveButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(SearchPositionProfile_changePositionStatusPositionProfileIDHiddenField.Value))
                    return;

                // Update the status.
                new PositionProfileBLManager().UpdatePositionProfileStatus(
                   SearchPositionProfile_changePositionStatusControl.SelectedStatusValue.Trim(), null,
                   base.userID, Convert.ToInt32 (SearchPositionProfile_changePositionStatusPositionProfileIDHiddenField.Value), 0, "POSITION");

                base.ShowMessage(SearchPositionProfile_bottomSuccessMessageLabel,
                    SearchPositionProfile_topSuccessMessageLabel, "Position status updated successfully");

                // Send the status change alert email asynchronously.
                AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendStatusChangeAlertEmail);
                IAsyncResult result = taskDelegate.BeginInvoke(new AsyncCallback(SendStatusChangeAlertEmailCallBack), taskDelegate);

                // Reload data for the current page.
                SearchPositionProfileDetails(int.Parse(ViewState["PAGENUMBER"].ToString()));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchPositionProfile_topErrorMessageLabel,
                    SearchPositionProfile_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the cancel link button is clicked in the change 
        /// position status window.
        /// </summary>
        /// <param name="sender"> 
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void SearchPositionProfile_changePositionStatusControl_cancelButtonClick(object sender, EventArgs e)
        {
            try
            {
                // Clear hidden field data.
                SearchPositionProfile_changePositionStatusPositionProfileIDHiddenField.Value = null;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SearchPositionProfile_topErrorMessageLabel,
                    SearchPositionProfile_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that associate the candidates with the selected position
        /// profile.
        /// </summary>
        /// <param name="data">
        /// A <see cref="string"/> that holds the data. This contains position 
        /// profile ID & session key separated by '-' symbol.
        /// </param>
        private void AssociateCandidatesWithPositionProfile(string data)
        {
            string[] dataArray = data.Split('-');
            new PositionProfileBLManager().InsertPositionProfileTSCandidate(dataArray[1], int.Parse(dataArray[0]));
            base.ShowMessage(SearchPositionProfile_topSuccessMessageLabel,
                SearchPositionProfile_bottomSuccessMessageLabel,
                "Candidates associated with the position profile");
        }

        /// <summary>
        /// Handles the Click event of the SearchPositionProfile_resetLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void SearchPositionProfile_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfile_topErrorMessageLabel,
                    SearchPositionProfile_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Handles the RowCreated event of the SearchPostionProfile_postionProfileGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/>
        /// instance containing the event data.</param>
        protected void SearchPostionProfile_postionProfileGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                if(Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                    return;

                int sortColumnIndex = GetSortColumnIndex
                    (SearchPostionProfile_postionProfileGridView,
                    (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfile_topErrorMessageLabel,
                    SearchPositionProfile_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Method that is called when the ok button is clicked in the deleted 
        /// position status window.
        /// </summary>
        /// <param name="sender"> 
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void SearchPositionProfile_deletePositionProfile_okButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(SearchPositionProfile_deletePositionProfileIDHiddenField))
                    return;

                // Delete position profile.
                new PositionProfileBLManager().DeletePositionProfile
                    (int.Parse(SearchPositionProfile_deletePositionProfileIDHiddenField.Value));

                base.ShowMessage(SearchPositionProfile_bottomSuccessMessageLabel,
                    SearchPositionProfile_topSuccessMessageLabel,
                    "Position profile deleted successfully");

                // Send the delete alert email asynchronously.
                AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendDeleteAlertEmail);
                IAsyncResult result = taskDelegate.BeginInvoke(new AsyncCallback(SendDeleteAlertEmailCallBack), taskDelegate);

                // Reload data for the current page.
                SearchPositionProfileDetails(int.Parse(ViewState["PAGENUMBER"].ToString()));
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfile_topErrorMessageLabel,
                    SearchPositionProfile_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        /// <summary>
        /// Method that is called when the cancel link button is clicked in the delete 
        /// position status window.
        /// </summary>
        /// <param name="sender"> 
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void SearchPositionProfile_deletePositionProfile_cancelButtonClick(object sender, EventArgs e)
        {
            try
            {
                // Clear hidden field data.
                SearchPositionProfile_deletePositionProfileIDHiddenField.Value = string.Empty;
                SearchPositionProfile_deletePositionProfileNameHiddenField.Value = string.Empty;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(SearchPositionProfile_topErrorMessageLabel,
                    SearchPositionProfile_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        protected void SearchPositionProfile_previousPageLinkButton_Click(object sender, EventArgs e)
        {
            string clientMgmt_client = "";
            if (!Utility.IsNullOrEmpty(Request.QueryString[Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING]))
            {
                clientMgmt_client = Request.QueryString[Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING];
                Response.Redirect(@"~\ClientCenter\ClientManagement.aspx?" + Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING + "=" + clientMgmt_client + "&m=0&s=0", false);
            }
            if (!Utility.IsNullOrEmpty(Request.QueryString[Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING]))
            {
                clientMgmt_client = Request.QueryString[Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING];
                Response.Redirect(@"~\ClientCenter\ClientManagement.aspx?" + Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING + "=" + clientMgmt_client + "&m=0&s=0", false);
            }
            if (!Utility.IsNullOrEmpty(Request.QueryString[Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING]))
            {
                clientMgmt_client = Request.QueryString[Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING];
                Response.Redirect(@"~\ClientCenter\ClientManagement.aspx?" + Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING + "=" + clientMgmt_client + "&m=0&s=0", false);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        private void AssignParentPageNavigationLink()
        {
            if (Request.QueryString[Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING] != null)
            {
                SearchPositionProfile_topPreviousPageLinkButtonSeparatorLabel.Visible = true;
                SearchPositionProfile_bottomPreviousPageLinkButtonSeparatorLabel.Visible = true;
                SearchPositionProfile_topPreviousPageLinkButton.Visible = true;
                SearchPositionProfile_bottomPreviousPageLinkButton.Visible = true;
                SearchPositionProfile_topPreviousPageLinkButton.Text = "Back to Contact";
                SearchPositionProfile_bottomPreviousPageLinkButton.Text = "Back to Contact";
                Session["CONT_ID"] = Convert.ToInt16(Request.QueryString[Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING]);
            }
            if (Request.QueryString[Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING] != null)
            {
                SearchPositionProfile_topPreviousPageLinkButtonSeparatorLabel.Visible = true;
                SearchPositionProfile_bottomPreviousPageLinkButtonSeparatorLabel.Visible = true;
                SearchPositionProfile_topPreviousPageLinkButton.Visible = true;
                SearchPositionProfile_bottomPreviousPageLinkButton.Visible = true;
                SearchPositionProfile_topPreviousPageLinkButton.Text = "Back to Department";
                SearchPositionProfile_bottomPreviousPageLinkButton.Text = "Back to Department";
                Session["DEP_ID"] = Convert.ToInt16(Request.QueryString[Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING]);
            }
            if (Request.QueryString[Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING] != null)
            {
                SearchPositionProfile_topPreviousPageLinkButtonSeparatorLabel.Visible = true;
                SearchPositionProfile_bottomPreviousPageLinkButtonSeparatorLabel.Visible = true;
                SearchPositionProfile_topPreviousPageLinkButton.Visible = true;
                SearchPositionProfile_bottomPreviousPageLinkButton.Visible = true;
                SearchPositionProfile_topPreviousPageLinkButton.Text = "Back to Client";
                SearchPositionProfile_bottomPreviousPageLinkButton.Text = "Back to Client";
                Session["CLT_ID"] = Convert.ToInt16(Request.QueryString[Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING]);
            }
        }

        /// <summary>
        /// Fills the search criteria.
        /// </summary>
        /// <param name="positionProfileSearchCriteria">The position profile search criteria.</param>
        private void FillSearchCriteria(PositionProfileSearchCriteria positionProfileSearchCriteria)
        {
            if (positionProfileSearchCriteria.SearchType == SearchType.Simple)
            {
                SearchPositionProfile_simpleLinkButton.Text = "Advanced";
                SearchPositionProfile_advancedDiv.Style["display"] = "none";
            }
            else
            {
                if (!Utility.IsNullOrEmpty(positionProfileSearchCriteria.clientPositionDetails.NatureOfPosition))
                {
                    string[] positionIDs = positionProfileSearchCriteria.clientPositionDetails.NatureOfPosition.Split(',', '%', ' ');

                    foreach (string id in positionIDs)
                    {
                        if (id != string.Empty && id != "")
                        {
                            if (SearchPositionProfile_positionNatureCheckBoxList.Items.FindByValue(id) != null)
                            {
                                SearchPositionProfile_positionNatureCheckBoxList.Items.FindByValue(id).Selected = true;
                            }
                        }
                    }
                }
                SearchPositionProfile_simpleLinkButton.Text = "Simple";

                SearchPositionProfile_advancedDiv.Style["display"] = "block";
            }

            if (!Utility.IsNullOrEmpty(positionProfileSearchCriteria.clientPositionDetails.ClientName))
            {
                SearchPositionProfile_clientNameTextBox.Text = positionProfileSearchCriteria.clientPositionDetails.ClientName;
            }
            if (!Utility.IsNullOrEmpty(positionProfileSearchCriteria.clientPositionDetails.Location))
            {
                SearchPositionProfile_locationTextBox.Text = positionProfileSearchCriteria.clientPositionDetails.Location;
            }
            if (!Utility.IsNullOrEmpty(positionProfileSearchCriteria.clientPositionDetails.PositionName))
            {
                SearchPositionProfile_positionNameTextBox.Text = positionProfileSearchCriteria.clientPositionDetails.PositionName;
            }
            if (!Utility.IsNullOrEmpty(positionProfileSearchCriteria.clientPositionDetails.PositionID))
            {
                SearchPositionProfile_positionIDTextBox.Text = positionProfileSearchCriteria.clientPositionDetails.PositionID;
            }

            if (!Utility.IsNullOrEmpty(positionProfileSearchCriteria.clientPositionDetails.PositionProfileName))
            {
                SearchPositionProfile_positionProfileNameTextBox.Text = positionProfileSearchCriteria.clientPositionDetails.PositionProfileName;
            }

            if (!Utility.IsNullOrEmpty(positionProfileSearchCriteria.clientPositionDetails.ShowType))
            {
                SearchPositionProfile_showPositionDropDownList.SelectedValue = positionProfileSearchCriteria.clientPositionDetails.ShowType;
            }

            if (!Utility.IsNullOrEmpty(positionProfileSearchCriteria.clientPositionDetails.searchType))
            {
                SearchPositionProfile_simpleHiddenField.Value = positionProfileSearchCriteria.clientPositionDetails.searchType; 
            }

            if (positionProfileSearchCriteria.clientPositionDetails.MyProfile) 
                SearchPositionProfile_showMyPositionProfileCheckBox.Checked = true; 
            else
                SearchPositionProfile_showMyPositionProfileCheckBox.Checked = false;


            // Assign 'created by'.
            SearchPositionProfile_createdByHiddenField.Value = positionProfileSearchCriteria.
                clientPositionDetails.CreatedBy.ToString();

            SearchPositionProfile_createdByTextBox.Text = positionProfileSearchCriteria.
                clientPositionDetails.CreatedByName;

            SearchPostionProfile_restoreHiddenField.Value = positionProfileSearchCriteria.IsMaximized == true ? "Y" : "N";
            ViewState["SORT_FIELD"] = positionProfileSearchCriteria.SortExpression;
            ViewState["SORT_ORDER"] = positionProfileSearchCriteria.SortDirection;

            // Apply search
            SearchPositionProfileDetails(positionProfileSearchCriteria.CurrentPage);

            // Highlight the last page number which is stored in session
            // when the page is launched from somewhere else.
            SearchPositionProfile_pageNavigator.MoveToPage(positionProfileSearchCriteria.CurrentPage);
        }

        /// <summary>
        /// Searches the position profile details.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        private void SearchPositionProfileDetails(int pageNumber)
        {
            ViewState["PAGENUMBER"] = pageNumber;

            PositionProfileDetailSearchCriteria searchCriteria = null;

            // Check if page is requested from the client management tabs.
            if (!Utility.IsNullOrEmpty(Session["FROM_CLIENT_MANAGEMENT"]) &&
                Convert.ToBoolean(Session["FROM_CLIENT_MANAGEMENT"]) == true)
            {
                searchCriteria = new PositionProfileDetailSearchCriteria();

                if (!Utility.IsNullOrEmpty(Request.QueryString["CLT_ID"]))
                    searchCriteria.ClientID = Convert.ToInt32(Request.QueryString["CLT_ID"]);
                else if (!Utility.IsNullOrEmpty(Request.QueryString["DEP_ID"]))
                    searchCriteria.DepartmentID = Convert.ToInt32(Request.QueryString["DEP_ID"]);
                else if (!Utility.IsNullOrEmpty(Request.QueryString["CONT_ID"]))
                    searchCriteria.ContactID = Convert.ToInt32(Request.QueryString["CONT_ID"]);

                if (SearchPositionProfile_showPositionDropDownList.SelectedIndex != -1)
                {
                    searchCriteria.ShowType = SearchPositionProfile_showPositionDropDownList.SelectedValue;
                }

                if (SearchPositionProfile_showMyPositionProfileCheckBox.Checked)
                    searchCriteria.CreatedBy = base.userID;
                else if (!Utility.IsNullOrEmpty(SearchPositionProfile_createdByHiddenField.Value))
                    searchCriteria.CreatedBy = Convert.ToInt32(SearchPositionProfile_createdByHiddenField.Value);
            }
            else
            {
                // Get the user entered search terms.
                searchCriteria = GetSearchTerms();
            }

            searchCriteria.searchType = SearchPositionProfile_simpleHiddenField.Value;
                        
            if (!SearchPositionProfile_showMyPositionProfileCheckBox.Checked)
                SearchPositionProfile_createdByDiv.Style.Add("display", "block");
            else
                SearchPositionProfile_createdByDiv.Style.Add("display", "none");
            
            int totalRecords = 0;
            SearchPostionProfile_profileGridViewUpdatePanelDiv.Visible = true;
            SearchPositionProfile_pageNavigator.Visible = true;
            PositionProfileBLManager positionProfileBLManager = new PositionProfileBLManager();

            List<PositionProfileDetail> positionProfileDetails = positionProfileBLManager.
                GetPositionProfile(ViewState["SORT_ORDER"] == null ? "" : ViewState["SORT_ORDER"].ToString(),
                ViewState["SORT_FIELD"] == null ? "" : ViewState["SORT_FIELD"].ToString(),
                pageNumber, base.GridPageSize, out totalRecords, searchCriteria, base.userID);

            if (positionProfileDetails != null && positionProfileDetails.Count == 0)
            {
                SearchPostionProfile_profileGridViewUpdatePanelDiv.Visible = false;
                SearchPositionProfile_pageNavigator.Visible = false;
                base.ShowMessage(SearchPositionProfile_topErrorMessageLabel, SearchPositionProfile_bottomErrorMessageLabel,
                    Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                SearchPostionProfile_postionProfileGridView.DataSource = positionProfileDetails;
                SearchPostionProfile_postionProfileGridView.DataBind();

                SearchPositionProfile_pageNavigator.TotalRecords = totalRecords;
                SearchPositionProfile_pageNavigator.PageSize = base.GridPageSize;
            }

            //Save the details in the session 
            PositionProfileSearchCriteria positionProfileSearchCriteria = new PositionProfileSearchCriteria();

            positionProfileSearchCriteria.CurrentPage = pageNumber;

            if (SearchPositionProfile_advancedDiv.Style["display"] == "block")
            {
                positionProfileSearchCriteria.SearchType = SearchType.Advanced;
            }
            else
                positionProfileSearchCriteria.SearchType = SearchType.Simple;

            positionProfileSearchCriteria.clientPositionDetails = searchCriteria;

            if (!Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
            {
                positionProfileSearchCriteria.SortDirection = (SortType)ViewState["SORT_ORDER"];
            }

            if (!Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
            {
                positionProfileSearchCriteria.SortExpression = ViewState["SORT_FIELD"].ToString();
            }
            else
            {
                positionProfileSearchCriteria.SortExpression = "";
            }

            positionProfileSearchCriteria.CurrentPage = pageNumber;

            positionProfileSearchCriteria.IsMaximized = SearchPostionProfile_restoreHiddenField.Value.Trim().
                ToUpper() == "Y" ? true : false;

            Session[Constants.SearchCriteriaSessionKey.SEARCH_POSITION_PROFILE] = positionProfileSearchCriteria;
        }

        /// <summary>
        /// Gets the search terms.
        /// </summary>
        /// <returns></returns>
        private PositionProfileDetailSearchCriteria GetSearchTerms()
        {
            PositionProfileDetailSearchCriteria clientPositionDetail = new PositionProfileDetailSearchCriteria();

            clientPositionDetail.ClientName = SearchPositionProfile_clientNameTextBox.Text.Trim();

            clientPositionDetail.Location = SearchPositionProfile_locationTextBox.Text.Trim();

            clientPositionDetail.PositionName = SearchPositionProfile_positionNameTextBox.Text.Trim();

            clientPositionDetail.PositionID = SearchPositionProfile_positionIDTextBox.Text.Trim();

            //clientPositionDetail.NatureOfPosition = SearchPositionProfile_positionNatureDropDownList.SelectedValue;

            clientPositionDetail.PositionProfileName = SearchPositionProfile_positionProfileNameTextBox.Text.Trim();

            clientPositionDetail.ContactName = SearchPositionProfile_contactNameTextBox.Text.Trim();

            clientPositionDetail.DepartmentName = SearchPositionProfile_departmentNameTextBox.Text.Trim();

            // Assign 'created by'.
            
            if (SearchPositionProfile_showMyPositionProfileCheckBox.Checked)
                clientPositionDetail.CreatedBy = base.userID;
            else if (!Utility.IsNullOrEmpty(SearchPositionProfile_createdByHiddenField.Value))
                clientPositionDetail.CreatedBy = Convert.ToInt32(SearchPositionProfile_createdByHiddenField.Value);
            
            clientPositionDetail.CreatedByName = SearchPositionProfile_createdByTextBox.Text.Trim();

            if (SearchPositionProfile_simpleLinkButton.Text.ToLower() == "simple")
            {
                foreach (ListItem item in SearchPositionProfile_positionNatureCheckBoxList.Items)
                {
                    if (item.Selected)
                    {
                        clientPositionDetail.NatureOfPosition = clientPositionDetail.NatureOfPosition + item.Value + "%,%";
                    }
                }

                if (clientPositionDetail.NatureOfPosition != null && clientPositionDetail.NatureOfPosition.Length > 3)
                {
                    clientPositionDetail.NatureOfPosition = "%" + clientPositionDetail.NatureOfPosition;

                    clientPositionDetail.NatureOfPosition = clientPositionDetail.NatureOfPosition.Remove(clientPositionDetail.NatureOfPosition.Length - 3, 2);
                }
            }
            if (SearchPositionProfile_showPositionDropDownList.SelectedIndex != -1)
            {
                clientPositionDetail.ShowType = SearchPositionProfile_showPositionDropDownList.SelectedValue;
            }

            if (SearchPositionProfile_showMyPositionProfileCheckBox.Checked)
                clientPositionDetail.MyProfile = true;
            else
                clientPositionDetail.MyProfile = false;

            return clientPositionDetail;
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Utility.IsNullOrEmpty(SearchPostionProfile_restoreHiddenField.Value) &&
                SearchPostionProfile_restoreHiddenField.Value == "Y")
            {
                SearchPostionProfile_simpleSearchDiv.Style["display"] = "none";
                SearchPostionProfile_searchResultsUpSpan.Style["display"] = "block";
                SearchPostionProfile_searchResultsDownSpan.Style["display"] = "none";
                SearchPostionProfile_profileGridViewUpdatePanelDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                SearchPostionProfile_simpleSearchDiv.Style["display"] = "block";
                SearchPostionProfile_searchResultsUpSpan.Style["display"] = "none";
                SearchPostionProfile_searchResultsDownSpan.Style["display"] = "block";
                SearchPostionProfile_profileGridViewUpdatePanelDiv.Style["height"] = RESTORED_HEIGHT;
            }

            if (!Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_POSITION_PROFILE]))
                if (!Utility.IsNullOrEmpty(SearchPostionProfile_restoreHiddenField.Value))
                    ((PositionProfileSearchCriteria)Session[Constants.SearchCriteriaSessionKey.SEARCH_POSITION_PROFILE]).IsMaximized =
                        SearchPostionProfile_restoreHiddenField.Value == "Y" ? true : false;
        }

        /// <summary>
        /// Method that send email to position profile associates indicating
        /// the deletion of position profile.
        /// </summary>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendDeleteAlertEmail()
        {
            try
            {
                // Construct position profile detail.
                PositionProfileDetail positionProfileDetail = new PositionProfileDetail();
                positionProfileDetail.PositionProfileID = Convert.ToInt32(SearchPositionProfile_deletePositionProfileIDHiddenField.Value);
                positionProfileDetail.PositionProfileName = SearchPositionProfile_deletePositionProfileNameHiddenField.Value;

                // Send email.
                new EmailHandler().SendMail(EntityType.PositionProfileDeleted, positionProfileDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }

            // Clear hidden field data.
            SearchPositionProfile_deletePositionProfileIDHiddenField.Value = string.Empty;
            SearchPositionProfile_deletePositionProfileNameHiddenField.Value = string.Empty;
        }

        /// <summary>
        /// Method that send email to position profile associates indicating
        /// the status change of position profile.
        /// </summary>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendStatusChangeAlertEmail()
        {
            try
            {
                // Construct position profile detail.
                PositionProfileDetail positionProfileDetail = new PositionProfileDetail();
                positionProfileDetail.PositionProfileID = Convert.ToInt32(SearchPositionProfile_changePositionStatusPositionProfileIDHiddenField.Value);
                positionProfileDetail.PositionProfileName = SearchPositionProfile_changePositionStatusPositionProfileNameHiddenField.Value;
                positionProfileDetail.PositionProfileStatusName = SearchPositionProfile_changePositionStatusControl.SelectedStatusText;
                positionProfileDetail.ClientName = SearchPositionProfile_changePositionStatusClientNameHiddenField.Value;
                positionProfileDetail.ModifiedByName = base.userFullName;

                // Send email.
                new EmailHandler().SendMail(EntityType.PositionProfileStatusChanged, positionProfileDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }

            // Clear hidden field data.
            SearchPositionProfile_changePositionStatusPositionProfileIDHiddenField.Value = string.Empty;
            SearchPositionProfile_changePositionStatusPositionProfileNameHiddenField.Value = string.Empty;
        }

        #endregion Private Methods

        #region Asynchronous Method Handlers                                   

        /// <summary>
        /// Handler method that acts as the callback method for delete position 
        /// profile email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendDeleteAlertEmailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that acts as the callback method for position profile 
        /// status change email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendStatusChangeAlertEmailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }
       
        #endregion Asynchronous Method Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Overridden method that loads default values into controls.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Overridden Methods
    }
}