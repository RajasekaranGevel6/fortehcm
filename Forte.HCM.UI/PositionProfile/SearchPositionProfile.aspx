﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PositionProfileMaster.Master"
    CodeBehind="SearchPositionProfile.aspx.cs" Inherits="Forte.HCM.UI.PositionProfile.SearchPositionProfile" %>

<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="pageNavigator" TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/ChangePositionStatusControl.ascx" TagName="ChangePositionStatusControl"
    TagPrefix="ucs" %>
<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content ID="SearchPositionProfile_bodyContent" runat="server" ContentPlaceHolderID="PositionProfileMaster_body">
    <script type="text/javascript" language="javascript">
        function ShowCreatedByDiv(checkBox, div, textBoxID, hiddenID)
        {
            var checkBox = document.getElementById(checkBox);
            var divID = document.getElementById(div);
            if (checkBox.checked == true)
            {
                document.getElementById(textBoxID).value = "";
                document.getElementById(hiddenID).value = "";
                divID.style["display"] = "none";
            }
            else
            {
                divID.style["display"] = "block";
            }
        }

        function ShowDiv(div)
        {

            var divID = document.getElementById(div.ClientID);
            if (divID.style["display"] == "block")
                divID.style["display"] = "none";
            else
                divID.style["display"] = "block";

            return;
        }

        function CollapseAllRows(targetControl)
        {
            var gridCtrl = document.getElementById("<%= SearchPostionProfile_profileGridViewUpdatePanelDiv.ClientID %>");
            if (gridCtrl != null)
            {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++)
                {
                    if (rowItems[indexRow].id.indexOf("SearchPostionProfile_commandDiv") != "-1")
                    {
                        rowItems[indexRow].style.display = "none";
                    }
                }
            }
            return false;
        }

        function ShowCommandDetails(ctrlId)
        {
            if (document.getElementById(ctrlId).style.display == "none")
            {
                CollapseAllRows(ctrlId);
                document.getElementById(ctrlId).style.display = "block";
            }
            else
            {
                CollapseAllRows(ctrlId);
                document.getElementById(ctrlId).style.display = "none";
            }

            return false;
        }

        // JQuery methods.
        function ShowAdvanced()
        {
            if ($("#<%= SearchPositionProfile_simpleLinkButton.ClientID %>").text() == 'Advanced')
            {
                $("#<%= SearchPositionProfile_simpleLinkButton.ClientID %>").text('Simple');
                $("#<%= SearchPositionProfile_simpleLinkButton.ClientID %>").attr('title', 'Click here to do simple search');
                $("#<%= SearchPositionProfile_advancedDiv.ClientID %>").show();
                $("#<%= SearchPositionProfile_simpleHiddenField.ClientID %>").val('A');
            }
            else
            {
                // Set link button text & tooltip                     
                $("#<%= SearchPositionProfile_simpleLinkButton.ClientID %>").text('Advanced');
                $("#<%= SearchPositionProfile_simpleLinkButton.ClientID %>").attr('title', 'Click here to do advanced  search');
                $("#<%= SearchPositionProfile_simpleHiddenField.ClientID %>").val('S');
                // Hide the simple search div.
                $("#<%= SearchPositionProfile_advancedDiv.ClientID %>").hide();

                var chks = $("#<%= SearchPositionProfile_positionNatureCheckBoxList.ClientID %>" + " input:checkbox");
                chks.attr("checked", false);
            }

            return false;
        }  
         


    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 72%" class="header_text_bold">
                            <asp:Literal ID="SearchPositionProfile_headerLiteral" runat="server" Text="Search Position Profile"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="SearchPositionProfile_topPreviousPageLinkButton" runat="server"
                                            Visible="false" OnClick="SearchPositionProfile_previousPageLinkButton_Click"
                                            SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        <asp:Label ID="SearchPositionProfile_topPreviousPageLinkButtonSeparatorLabel" runat="server"
                                            Text="|" Visible="false"></asp:Label>
                                    </td>
                                    <td style="width: 16%" align="right">
                                        <asp:LinkButton ID="SearchPositionProfile_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="SearchPositionProfile_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="SearchPositionProfile_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="SearchPositionProfile_topMessageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="SearchPositionProfile_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SearchPositionProfile_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <asp:HiddenField ID="SearchPositionProfile_tsSessionKeyHiddenField" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:UpdatePanel runat="server" ID="SearchPostionProfile_searchDivUpdatePanel">
                                <ContentTemplate>
                                    <div id="SearchPostionProfile_simpleSearchDiv" runat="server">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="right">
                                                    <asp:HiddenField ID="SearchPositionProfile_simpleHiddenField" runat="server" />
                                                    <asp:LinkButton ID="SearchPositionProfile_simpleLinkButton" runat="server" Text="Advanced"
                                                        SkinID="sknActionLinkButton" OnClientClick="javascript:return ShowAdvanced();"></asp:LinkButton>
                                                    <%-- OnClick="SearchPositionProfile_simpleLinkButton_Click"--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="panel_bg" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0" class="panel_inner_body_bg">
                                                        <tr>
                                                            <td style="width: 13%">
                                                                <asp:Label ID="SearchPositionProfile_positionProfileNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Position Profile Name"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="SearchPositionProfile_positionProfileNameTextBox" runat="server"
                                                                    MaxLength="200" Width="175px"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 12%">
                                                                <asp:Label ID="SearchPositionProfile_positionIDLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Position Profile ID"></asp:Label>
                                                            </td>
                                                            <td style="width: 24%">
                                                                <div style="float: left; padding-right: 5px; width: 90%">
                                                                    <asp:TextBox ID="SearchPositionProfile_positionIDTextBox" runat="server" MaxLength="100"
                                                                        Width="175px"></asp:TextBox>
                                                                </div>
                                                                <div style="float: left;">
                                                                    <asp:ImageButton ID="SearchPositionProfile_positionIDImageButton" SkinID="sknbtnSearchicon"
                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to select the position ID"
                                                                        Visible="false" />
                                                                </div>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="SearchPositionProfile_positionNameLabel" runat="server" Text="Position Name"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:TextBox ID="SearchPositionProfile_positionNameTextBox" runat="server" MaxLength="100"
                                                                    Width="175px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5" colspan="6">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="SearchPositionProfile_clientNameLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Client Name"></asp:Label>
                                                            </td>
                                                            <td style="width: 24%">
                                                                <asp:TextBox ID="SearchPositionProfile_clientNameTextBox" runat="server" MaxLength="100"
                                                                    Width="175px"></asp:TextBox>
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="SearchPositionProfile_locationLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                    Text="Location"></asp:Label>
                                                            </td>
                                                            <td align="left">
                                                                <div style="float: left; padding-right: 5px; width: 90%">
                                                                    <asp:TextBox ID="SearchPositionProfile_locationTextBox" runat="server" MaxLength="100"
                                                                        Width="175px"></asp:TextBox>
                                                                </div>
                                                                <div style="float: left;">
                                                                    <asp:ImageButton ID="SearchPositionProfile_locationImageButton" SkinID="sknbtnSearchicon"
                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to select the location"
                                                                        Visible="false" />
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="SearchPositionProfile_departmentNameLabel" runat="server" Text="Department Name"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="SearchPositionProfile_departmentNameTextBox" runat="server" MaxLength="100"
                                                                    Width="175px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5" colspan="6">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:CheckBox ID="SearchPositionProfile_showMyPositionProfileCheckBox" runat="server"
                                                                    Font-Bold="true" Text="Show only my profiles" Checked="true" />
                                                                &nbsp;
                                                                <asp:ImageButton ID="SearchPositionProfile_showMyPositionProfileHelpImageButton"
                                                                    SkinID="sknHelpImageButton" runat="server" ToolTip="Check this to show only the profile created by me"
                                                                    ImageAlign="Top" OnClientClick="javascript:return false;" />
                                                            </td>
                                                            <td colspan="2">
                                                                <div id="SearchPositionProfile_createdByDiv" runat="server">
                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="SearchPositionProfile_createdByLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                    Text="Created By"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <div style="float: left; padding-left: 41px; width: 176px">
                                                                                    <asp:TextBox ID="SearchPositionProfile_createdByTextBox" runat="server" MaxLength="101"
                                                                                        Width="100%" ReadOnly="true"></asp:TextBox>
                                                                                </div>
                                                                                <div style="float: left; padding-left: 10px;">
                                                                                    <asp:ImageButton ID="SearchPositionProfile_createdByImageButton" SkinID="sknbtnSearchicon"
                                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to select the user" Visible="true" />
                                                                                    <asp:HiddenField ID="SearchPositionProfile_createdBydummyAuthorIdHidenField" runat="server" />
                                                                                    <asp:HiddenField ID="SearchPositionProfile_createdByHiddenField" runat="server" />
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="SearchPositionProfile_contactNameLabel" runat="server" Text="Contact Name"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="SearchPositionProfile_contactNameTextBox" runat="server" MaxLength="100"
                                                                    Width="175px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="6">
                                                                <div id="SearchPositionProfile_advancedDiv" runat="server" style="display: none">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="width: 17%">
                                                                                <asp:Label ID="SearchPositionProfile_positionNatureLabel" runat="server" Text="Nature Of Position "
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <%--  <asp:DropDownList ID="SearchPositionProfile_positionNatureDropDownList" runat="server">
                                                                                    <asp:ListItem Text="Contract" Value="1">                                                           
                                                                                    </asp:ListItem>
                                                                                    <asp:ListItem Text="Direct hire" Value="2">
                                                                                    </asp:ListItem>
                                                                                    <asp:ListItem Text="Contract-to-hire" Value="3"></asp:ListItem>
                                                                                </asp:DropDownList>--%>
                                                                            <td class="checkbox_list_bg" align="left" style="width: 40%">
                                                                                <div style="float: left; padding-right: 5px;">
                                                                                    <asp:CheckBoxList ID="SearchPositionProfile_positionNatureCheckBoxList" runat="server"
                                                                                        RepeatDirection="Horizontal" RepeatColumns="3" CellSpacing="2" TextAlign="Right"
                                                                                        TabIndex="5">
                                                                                        <asp:ListItem Text="Contract" Value="1">                                                           
                                                                                        </asp:ListItem>
                                                                                        <asp:ListItem Text="Direct Hire" Value="2">
                                                                                        </asp:ListItem>
                                                                                        <asp:ListItem Text="Contract To Hire" Value="3"></asp:ListItem>
                                                                                    </asp:CheckBoxList>
                                                                                </div>
                                                                            </td>
                                                                            <td style="width: 48%">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8" colspan="6">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="SearchPositionProfile_showLabel" runat="server" Text="Show Position"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:DropDownList ID="SearchPositionProfile_showPositionDropDownList" runat="server"
                                                                    SkinID="sknAssessorDropDownList" Width="270px">
                                                                    <asp:ListItem Text="Show positions owned and/or assigned to me" Selected="True" Value="BOTH"></asp:ListItem>
                                                                    <asp:ListItem Text="Show positions owned by me" Value="POT_OWR"></asp:ListItem>
                                                                    <asp:ListItem Text="Show positions assigned to me" Value="POT_CO_OWR"></asp:ListItem>
                                                                    <asp:ListItem Text="Show all positions" Value="ALL"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="td_padding_top_5">
                                                    <asp:Button ID="SearchPostionProfile_topSearchButton" runat="server" Text="Search"
                                                        SkinID="sknButtonId" OnClick="SearchPostionProfile_topSearchButton_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="SearchPostionProfile_topSearchButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr id="SearchPostionProfile_searchProfileResultsTR" runat="server">
                                    <td class="header_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="width: 98%" align="left" class="header_text_bold">
                                                    <asp:Literal ID="SearchPostionProfile_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                                        ID="SearchPostionProfile_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                        Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                </td>
                                                <td style="width: 2%" align="right">
                                                    <span id="SearchPostionProfile_searchResultsUpSpan" runat="server" style="display: none;">
                                                        <asp:Image ID="SearchPostionProfile_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                                    </span><span id="SearchPostionProfile_searchResultsDownSpan" runat="server" style="display: block;">
                                                        <asp:Image ID="SearchPostionProfile_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                    </span>
                                                    <asp:HiddenField ID="SearchPostionProfile_restoreHiddenField" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <asp:UpdatePanel runat="server" ID="SearchPostionProfile_testGridViewUpdatePanel">
                                            <ContentTemplate>
                                                <div runat="server" id="SearchPostionProfile_profilesDiv" visible="true">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="left">
                                                                <div id="SearchPostionProfile_profileGridViewUpdatePanelDiv" runat="server" visible="false"
                                                                    style="height: 220px; overflow: auto;">
                                                                    <asp:GridView ID="SearchPostionProfile_postionProfileGridView" runat="server" AllowSorting="True"
                                                                        AutoGenerateColumns="False" Width="100%" SkinID="sknWrapHeaderGrid" OnSorting="SearchPostionProfile_postionProfileGridView_Sorting"
                                                                        OnRowDataBound="SearchPostionProfile_postionProfileGridView_RowDataBound" OnRowCommand="SearchPostionProfile_postionProfileGridView_RowCommand"
                                                                        OnRowCreated="SearchPostionProfile_postionProfileGridView_RowCreated">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-Width="1px" HeaderText="" Visible="false">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchPostionProfile_postionProfileGridView_keysHiddenLabel" runat="server"
                                                                                        Text='<%# Eval("Keys") %>' Visible="false"></asp:Label>
                                                                                    <asp:HiddenField runat="server" ID="SearchPositionProfile_closedStatusHiddenField"
                                                                                        Value='<% # Eval("Closed") %>' />
                                                                                    <asp:HiddenField ID="SearchPositionProfile_dataAccessRightsHiddenField" runat="server"
                                                                                        Value='<%# Eval("DataAccessRights") %>' />
                                                                                    <asp:HiddenField ID="SearchPositionProfile_hasOwnerRightsHiddenField" runat="server"
                                                                                        Value='<%# Eval("HasOwnerRights") %>' />
                                                                                    <asp:HiddenField ID="SearchPositionProfile_editableHiddenField" runat="server" Value='<%# Eval("IsEditable") %>' />
                                                                                </ItemTemplate>
                                                                                <ItemStyle Wrap="false" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField ItemStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <div align="left" id="SearchPostionProfile_commandDiv" style="display: block" runat="server">
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="50%" align="left">
                                                                                            <tr align="left">
                                                                                                <td>
                                                                                                    <asp:ImageButton ID="SearchPostionProfile_postionProfileGridView_editPositionProfileImageButton"
                                                                                                        runat="server" SkinID="sknEditPositionProfile" ToolTip="View/Edit Position Profile"
                                                                                                        CommandName="EditPositionProfile" CommandArgument='<% # Eval("PositionProfileID") %>' />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:HiddenField ID="SearchPositionProfile_editableHiddenField1" runat="server" Value='<%# Eval("IsEditable") %>' />
                                                                                                    <asp:ImageButton ID="SearchPostionProfile_postionProfileGridView_statusImageButton"
                                                                                                        runat="server" SkinID="sknSkillViewPPDashboardImageButton" ToolTip="Position Profile Dashboard"
                                                                                                        CommandName="PositionProfileDashboard" CommandArgument='<% # Eval("PositionProfileID") %>' />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:ImageButton ID="SearchPostionProfile_postionProfileGridView_activityImageButton"
                                                                                                        runat="server" SkinID="sknSkillViewPPActivityImageButton" ToolTip="Position Profile Activity"
                                                                                                        CommandName="Activity" CommandArgument='<% # Eval("PositionProfileID") %>' />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:ImageButton ID="SearchPostionProfile_postionProfileGridView_changeStatusImageButton"
                                                                                                        runat="server" SkinID="sknCopyPositionProfile" ToolTip="Change Position Status"
                                                                                                        CommandName="ChangePositionStatus" CommandArgument='<% # Eval("PositionProfileID") %>' />
                                                                                                    <asp:HiddenField ID="SearchPostionProfile_postionProfileGridView_statusHiddenField"
                                                                                                        runat="server" Value='<%# Eval("PositionProfileStatus") %>' />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:ImageButton ID="SearchPostionProfile_postionProfileGridView_deletePositionProfileImageButton"
                                                                                                        runat="server" SkinID="sknDeletePositionProfile" ToolTip="Delete Position Profile"
                                                                                                        CommandName="DeletePositionProfile" CommandArgument='<% # Eval("PositionProfileID") %>' />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:ImageButton ID="SearchPostionProfile_postionProfileGridView_associateCandidatesImageButton"
                                                                                                        runat="server" SkinID="sknPPassociateCandidateImageButton" ToolTip="Associate Candidate With Position Profile"
                                                                                                        CommandName="AssociateCandidate" CommandArgument='<% # Eval("PositionProfileID") %>'
                                                                                                        Visible="false" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Position Profile Name" SortExpression="POSITION_PROFILE_NAME"
                                                                                ItemStyle-Width="30%" ItemStyle-Wrap="true">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchPostionProfile_postionProfileGridView_positionProfileNameLabel"
                                                                                        runat="server" Text='<% # Eval("PositionProfileName") %>' Visible="false"></asp:Label>
                                                                                    <div id="SearchPostionProfile_postionProfileGridView_positionProfileNameDiv" runat="server"
                                                                                        style="word-break: break-all; word-wrap: break-word; width: 100px">
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="25%" />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="Position Profile ID" SortExpression="POSITION_ID" DataField="ClientPositionName"
                                                                                ItemStyle-Width="20%" />
                                                                            <asp:TemplateField HeaderText="Client Name" HeaderStyle-Width="20%" ItemStyle-Width="12%"
                                                                                SortExpression="CLIENT_NAME" Visible="true">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchPostionProfile_postionProfileGridView_clientNameLabel" runat="server"
                                                                                        Text='<% # Eval("ClientName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Position Name" HeaderStyle-Width="5%" ItemStyle-Width="15%"
                                                                                SortExpression="POSITION_NAME" Visible="false">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchPostionProfile_postionProfileGridView_positionNameLabel" runat="server"
                                                                                        Text='<% # Eval("PositionName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Department Name" HeaderStyle-Width="5%" ItemStyle-Width="10%"
                                                                                SortExpression="DEPARTMENT_NAME">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchPostionProfile_postionProfileGridView_DepartmentNameLabel" runat="server"
                                                                                        Text='<% # Eval("DepartmentName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Contact Name" HeaderStyle-Width="5%" ItemStyle-Width="10%"
                                                                                SortExpression="CONTACT_NAME">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchPostionProfile_postionProfileGridView_contactNameLabel" runat="server"
                                                                                        Text='<% # Eval("ContactName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Created Date" HeaderStyle-Width="5%" ItemStyle-Width="8%"
                                                                                SortExpression="DATE DESC">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchPostionProfile_postionProfileGridView_dateLabel" runat="server"
                                                                                        Text='<%# GetDateFormat(Convert.ToDateTime(Eval("Date"))) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Created By" HeaderStyle-Width="5%" ItemStyle-Width="8%"
                                                                                SortExpression="CREATED_BY_NAME">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchPostionProfile_postionProfileGridView_createdByLabel" runat="server"
                                                                                        Text='<% # Eval("CreatedByName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField SortExpression="NO_OF_POSITION DESC" ItemStyle-Width="5%" HeaderStyle-Width="5%"
                                                                                ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="td_padding_right_20" HeaderStyle-HorizontalAlign="Left"
                                                                                HeaderText="No Of Positions" HeaderStyle-CssClass="td_padding_right_20">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchPostionProfile_postionProfileGridView_noOfPositionsLabel" runat="server"
                                                                                        Text='<%#Eval("NoOfPositions") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField SortExpression="NO_OF_CANDIDATE DESC" HeaderText="No Of Assessed Candidates"
                                                                                HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="td_padding_right_20"
                                                                                HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="10%" HeaderStyle-CssClass="td_padding_right_20">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchPostionProfile_postionProfileGridView_assessedCandidateLabel"
                                                                                        runat="server" Text='<%#Eval("NoOfAssessedCandidates") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="Status" DataField="IsOpened" ItemStyle-Width="8%" SortExpression="STATUS"
                                                                                Visible="false" />
                                                                            <asp:TemplateField SortExpression="POSITION_PROFILE_STATUS ASC" HeaderText="Position Status"
                                                                                HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="td_padding_right_20"
                                                                                HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="10%" HeaderStyle-CssClass="td_padding_right_20">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="SearchPostionProfile_posttionProfileGridView_positionProfileStatusLabel"
                                                                                        runat="server" Text='<%#Eval("PositionProfileStatusName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <uc1:pageNavigator ID="SearchPositionProfile_pageNavigator" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="SearchPositionProfile_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="SearchPositionProfile_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label><asp:Label
                            ID="SearchPositionProfile_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%">
                            &nbsp;
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="SearchPositionProfile_bottomPreviousPageLinkButton" runat="server"
                                            Visible="false" OnClick="SearchPositionProfile_previousPageLinkButton_Click"
                                            SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        <asp:Label ID="SearchPositionProfile_bottomPreviousPageLinkButtonSeparatorLabel"
                                            runat="server" Text="|" Visible="false"></asp:Label>
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="SearchPositionProfile_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="SearchPositionProfile_resetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="SearchPositionProfile_bottomCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="SearchPositionProfile_deletePositionProfileUpdatePanel" runat="server">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="SearchPositionProfile_deletePositionProfileHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="SearchPositionProfile_deletePositionProfilePopupPanel" runat="server"
                            Style="display: none" CssClass="delete_position_profile_popup_control">
                            <asp:HiddenField ID="SearchPositionProfile_deletePositionProfileIDHiddenField" runat="server" />
                            <asp:HiddenField ID="SearchPositionProfile_deletePositionProfileNameHiddenField"
                                runat="server" />
                            <uc1:ConfirmMsgControl ID="SearchPositionProfile_deletePositionProfileConfirmMsgControl"
                                runat="server" OnOkClick="SearchPositionProfile_deletePositionProfile_okButtonClick"
                                OnCancelClick="SearchPositionProfile_deletePositionProfile_cancelButtonClick" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="SearchPositionProfile_deletePositionProfileModalPopupExtender"
                            runat="server" PopupControlID="SearchPositionProfile_deletePositionProfilePopupPanel"
                            TargetControlID="SearchPositionProfile_deletePositionProfileHiddenButton" BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="SearchPositionProfile_changePositionStatusUpdatePanel" runat="server">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="SearchPositionProfile_changePositionStatusHiddenButton" runat="server" />
                        </div>
                        <asp:HiddenField ID="SearchPositionProfile_changePositionStatusPositionProfileIDHiddenField"
                            runat="server" />
                        <asp:HiddenField ID="SearchPositionProfile_changePositionStatusPositionProfileNameHiddenField"
                            runat="server" />
                        <asp:HiddenField ID="SearchPositionProfile_changePositionStatusClientNameHiddenField"
                            runat="server" />
                        <asp:Panel ID="SearchPositionProfile_changePositionStatusPanel" runat="server" Style="display: none"
                            CssClass="change_position_status_popup_control">
                            <asp:HiddenField ID="SearchPositionProfile_changePositionStatusPanel_positionProfileIDHiddenField"
                                runat="server" />
                            <ucs:ChangePositionStatusControl ID="SearchPositionProfile_changePositionStatusControl"
                                runat="server" OnSaveButtonClicked="SearchPositionProfile_changePositionStatusControl_saveButtonClick"
                                OnCancelButtonClicked="SearchPositionProfile_changePositionStatusControl_cancelButtonClick" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="SearchPositionProfile_changePositionStatusModalPopupExtender"
                            runat="server" PopupControlID="SearchPositionProfile_changePositionStatusPanel"
                            TargetControlID="SearchPositionProfile_changePositionStatusHiddenButton" BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
