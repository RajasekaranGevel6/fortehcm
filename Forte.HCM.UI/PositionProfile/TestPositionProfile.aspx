﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestPositionProfile.aspx.cs" Inherits="Forte.HCM.UI.PositionProfile.TestPositionProfile" %>
<%@ Register src="../Segments/VerticalBackgroundRequirementControl.ascx" tagname="VerticalBackgroundRequirementControl" tagprefix="uc1" %>
<%@ Register src="../Segments/TechnicalSkillRequirementControl.ascx" tagname="TechnicalSkillRequirementControl" tagprefix="uc2" %>
<%@ Register src="../Segments/RoleRequirementControl.ascx" tagname="RoleRequirementControl" tagprefix="uc3" %>
<%@ Register src="../Segments/EducationRequirementControl.ascx" tagname="EducationRequirementControl" tagprefix="uc5" %>
<%@ Register src="../Segments/ClientPositionDetailsControl.ascx" tagname="ClientPositionDetailsControl" tagprefix="uc6" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
        function Show(id) {
            alert(id);
            //document.getElementById(id).visible = true;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <ajaxToolKit:ToolkitScriptManager ID="TestPositionProfile_scriptManager" runat="server"></ajaxToolKit:ToolkitScriptManager>
  
    <div>
    <ajaxToolKit:TabContainer ID="testTabContainer" runat="server" AutoPostBack="true" OnActiveTabChanged="testTabContainer_ActiveTabChanged">
    <ajaxToolKit:TabPanel ID="test1" runat="server" HeaderText="tab1">
    <ContentTemplate>
    </ContentTemplate>
     </ajaxToolKit:TabPanel>
     <ajaxToolKit:TabPanel ID="test1sdf" runat="server" HeaderText="tabsdf1">
    <ContentTemplate>
    </ContentTemplate>
    </ajaxToolKit:TabPanel>
    </ajaxToolKit:TabContainer>
    <table style="width: 100%">
        <tr>
            <td>
                <uc1:VerticalBackgroundRequirementControl ID="VerticalBackgroundRequirementControl1" 
                    runat="server" Visible="false" />
            </td>
        </tr>
        <tr>
            <td>
                <uc2:TechnicalSkillRequirementControl ID="TechnicalSkillRequirementControl1" 
                    runat="server" Visible="false" />
            </td>
        </tr>
        <tr>
            <td>
                <uc3:RoleRequirementControl ID="RoleRequirementControl1" runat="server"  Visible="false" />
            </td>
        </tr>
        <tr>
            <td>
                <uc5:EducationRequirementControl ID="EducationRequirementControl1" 
                    runat="server"  Visible="false" />
            </td>
        </tr>
        <tr>
            <td>
                <uc6:ClientPositionDetailsControl ID="ClientPositionDetailsControl1" 
                    runat="server" Visible="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>

    </table>
    </div>
    </form>
</body>
</html>
