﻿using System;
using System.IO;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;
using Forte.HCM.ExternalDataService;

//using Forte.HCM.UI.CPXService;

using OfficeOpenXml;
using Forte.HCM.Utilities;

namespace Forte.HCM.UI.PositionProfile
{
    public partial class PositionProfileStatus : PageBase
    {
        #region Private Veriables

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing of
        /// position profile status change email alert.
        /// </summary>
        private delegate void PositionProfileStatusDelegate();

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing of
        /// candidate status change email alert.
        /// </summary>
        private delegate void CandidateStatusDelegate(int ID);

        private List<AttributeDetail> CandidateStatusList = null;
        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LoadValues();
                WorkflowStatusExpandorRestore();
                //AssociatedCandidatesExpandorRestore();

                PositionProfileStatus_workflowStatusTR.Attributes.Add("onclick",
                 "ExpandOrMinimize('" +
                 PositionProfileStatus_workflowStatusDiv.ClientID + "','" +
                 PositionProfileStatus_workflowStatusUpSpan.ClientID + "','" +
                 PositionProfileStatus_workflowStatusDownSpan.ClientID + "','" +
                 PositionProfileStatus_workflowStatus_restoreHiddenField.ClientID + "')");

                PositionProfileStatus_associatedCandidatesTR.Attributes.Add("onclick",
                 "ExpandOrMinimize('" +
                 PositionProfileStatus_associatedCandidates_Div.ClientID + "','" +
                 PositionProfileStatus_associatedCandidates_upSpan.ClientID + "','" +
                 PositionProfileStatus_associatedCandidates_downSpan.ClientID + "','" +
                 PositionProfileStatus_associatedCandidates_restoreHiddenField.ClientID + "')");

                SearchControlEnable(true);

                if (!IsPostBack)
                {
                    PositionProfileStatus_selectedcandidates.Value = null;
                    if (Utility.IsNullOrEmpty(Request.QueryString["kill"]))
                    {
                        Session[Constants.SearchCriteriaSessionKey.POSITION_PROFILE_STATUS] = null;
                    }

                    if (!Utility.IsNullOrEmpty(Request.QueryString["type"]))
                    {
                        switch (Request.QueryString["type"].ToString().ToLower())
                        {
                            case "s":
                                PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue = "PCS_SUB";
                                break;
                            case "a":
                                PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue = "PCS_ASS";
                                break;
                            case "r":
                                PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue = "PCS_REJ";
                                break;
                            case "h":
                                PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue = "PCS_HIR";
                                break;
                            case "all":
                                PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue = "All";
                                break;
                        }
                    }

                    if (base.HasPositionProfileOwnerRights(
                       Convert.ToInt32(Request.QueryString["positionprofileid"]),
                       base.userID, new List<string> { Constants.PositionProfileOwners.OWNER, 
                           Constants.PositionProfileOwners.CO_OWNER}))
                    {
                        PositionProfileEntry_topEmailButton.Visible = true;
                        PositionProfileEntry_bottomEmailButton.Visible = true;

                        PositionProfileStatus_bottomEmail_InternalButton.Visible = true;
                        PositionProfileStatus_topEmail_InternalButton.Visible = true;

                        PositionProfileStatus_positionProfileStatusLinkButton.Visible = true;
                        PositionProfileStatus_editWorkflowStatus_HyperLink.Visible = true;
                    }


                    if (base.HasPositionProfileOwnerRights(
                      Convert.ToInt32(Request.QueryString["positionprofileid"]),
                      base.userID, new List<string> {  
                           Constants.PositionProfileOwners.CANDIDATE_RECRUITER, 
                           Constants.PositionProfileOwners.CANDIDATE_ASSOCIATION_CREATOR, 
                           Constants.PositionProfileOwners. CANDIDATE_RECRUITER, 
                           Constants.PositionProfileOwners. INTERVIEW_CREATOR, 
                           Constants.PositionProfileOwners. INTERVIEW_SESSION_CREATOR,
                           Constants.PositionProfileOwners.INTERVIEW_SCHEDULER}))
                    {

                        PositionProfileStatus_bottomEmail_InternalButton.Visible = true;
                        PositionProfileStatus_topEmail_InternalButton.Visible = true;
                    }

                    ///GetPositionProfileID();
                    if (Session[Constants.SearchCriteriaSessionKey.POSITION_PROFILE_STATUS] != null)
                    {
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.POSITION_PROFILE_STATUS]
                            as PositionProfileStatusSearchCriteria);
                    }

                    string candidateStatus = null;
                    if (PositionProfileStatus_showCandidatesRadioButtonList.SelectedIndex != -1)
                        candidateStatus = PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue;

                    BindPositionProfile();

                    if (candidateStatus ==null || candidateStatus.ToLower() == "all") candidateStatus = null;

                    BindPositionProfileCandidates(candidateStatus);

                    GetRecruiterAssignment(GetPositionProfileID());
                    GetCandidateAssociationCount(GetPositionProfileID());
                    GetPositionProfileInterviews(GetPositionProfileID());

                    GetPositionProfileTests(GetPositionProfileID());

                    PositionProfileStatus_ws_candidateAssociation_HyperLink.NavigateUrl =
                        "../ResumeRepository/SearchCandidateRepository.aspx?m=0&s=1&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&positionprofileid=" + GetPositionProfileID();

                    PositionProfileStatus_talenScount_candidateAssociation_HyperLink.NavigateUrl = "../TalentScoutHome.aspx?parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&positionprofileid=" + GetPositionProfileID();
                    PositionProfileStatus_editWorkflowStatus_HyperLink.NavigateUrl = "../PositionProfile/PositionProfileWorkflowSelection.aspx?m=1&s=0&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&positionprofileid=" + GetPositionProfileID().ToString();
                    PositionProfileStatusDiv.Style["display"] = "none";
                    
                    // Assign client information
                    AssignClientDetails();
                }
                WorkflowStatusExpandorRestore();
                //AssociatedCandidatesExpandorRestore();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                   PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_showCandidates_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                StringBuilder item = new StringBuilder();
                for (int i = 0; i < PositionProfileStatus_showCandidatesRadioButtonList.Items.Count; i++)
                {
                    if (PositionProfileStatus_showCandidatesRadioButtonList.Items[i].Selected)
                    {
                        item.Append(PositionProfileStatus_showCandidatesRadioButtonList.Items[i].Value);
                        item.Append(",");
                    }
                }

                //ViewState["FILTER_BY"] = item.ToString().TrimEnd(',');
                ViewState["SORT_FIELD"] = PositionProfileStatus_orderbyDropdownList.SelectedValue;
                ViewState["PAGENUMBER"] = "1";
                if (PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue == "All")
                    BindPositionProfileCandidates(null);
                else
                    BindPositionProfileCandidates(PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        protected void PositionProfileStatus_orderbyDropdownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ViewState["SORT_FIELD"] = PositionProfileStatus_orderbyDropdownList.SelectedValue;
                BindPositionProfileCandidates(PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        void PositionProfileStatus_candidatesPageNavigator_PageNumberClick(object sender, EventSupport.PageNumberEventArgs e)
        {
            try
            {
                PositionProfileStatus_candidatesPageNavigator.Visible = true;
                ViewState["PAGENUMBER"] = e.PageNumber;
                BindPositionProfileCandidates(PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel,
                    exception.Message);
            }
        }

        protected void PositionProfileStatus_candidatesGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string candidateID = e.CommandArgument.ToString();
                string positionProfileID = Request.QueryString
                   [Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING];
                int index = 0;
                string interviewCandidateSessionKey = string.Empty;
                int interviewAttemptID = 0;

                switch (e.CommandName)
                {
                    case "skilldetails":
                        ScriptManager.RegisterStartupScript(this, this.GetType(),
                            "SkillDetails", "<script language='javascript'>ShowPositionProfileSkillScorePopup(" +
                            positionProfileID + "," + candidateID + ");</script>", false);
                        break;
                    case "noteslog":
                        ScriptManager.RegisterStartupScript(this, this.GetType(),
                            "CandidateActivityLog", "<script language='javascript'>ShowCandidateActivityLog(" +
                            positionProfileID + "," + candidateID + ");</script>", false);
                        break;
                    case "resumedownload":
                        string mimeType;
                        string fileName;
                        Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT] = null;

                        byte[] resumeContent = new CandidateBLManager().GetResumeContentsByCandidiateID
                            (int.Parse(candidateID), out mimeType, out fileName);

                        if ((resumeContent == null) || (resumeContent.Length == 0))
                        {
                            PositionProfileStatus_topErrorMessageLabel.Text = "No resume found ";
                            return;
                        }
                        Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT] = resumeContent;
                        Response.Redirect("../Common/Download.aspx?fname=" + fileName + "&mtype=" +
                            mimeType + " &type=DownloadResume", false);
                        break;
                    case "email":

                        break;
                    case "download":
                        break;
                    case "testscore":
                        string[] candidateTestDetail = candidateID.Split(',');

                        Response.Redirect("~/TestMaker/TestResult.aspx?m=1&s=3&candidatesession="
                            + candidateTestDetail[0] + "&attemptid=" + candidateTestDetail[2] +
                            "&testkey=" + candidateTestDetail[1] + "&" + Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING + "="
                            + GetPositionProfileID().ToString() + "&parentpage="
                            + Constants.ParentPage.POSITION_PROFILE_STATUS, false);
                        break;
                    case "CandidateSummary":
                        index = Convert.ToInt32(e.CommandArgument);
                        interviewCandidateSessionKey = (PositionProfileStatus_candidatesGridView.Rows
                            [index].FindControl("PositionProfileStatus_candidatesGridViewInterviewCandidateSessionIDHiddenField") as HiddenField).Value;
                        interviewAttemptID = Convert.ToInt32((PositionProfileStatus_candidatesGridView.Rows
                            [index].FindControl("PositionProfileStatus_candidatesGridViewInterviewAttemptIDHiddenField") as HiddenField).Value);

                        Response.Redirect("~/Assessments/CandidateAssessorSummary.aspx" +
                            "?m=4&s=0&candidatesessionid=" + interviewCandidateSessionKey +
                            "&attemptid=" + interviewAttemptID +
                            "&positionprofileid=" + Request.QueryString["positionprofileid"] +
                            "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS, false);

                        break;
                    case "showpublish":
                        index = Convert.ToInt32(e.CommandArgument);
                        interviewCandidateSessionKey = (PositionProfileStatus_candidatesGridView.Rows
                            [index].FindControl("PositionProfileStatus_candidatesGridViewInterviewCandidateSessionIDHiddenField") as HiddenField).Value;
                        string candidateName = (PositionProfileStatus_candidatesGridView.Rows
                                [index].FindControl("PositionProfileStatus_candidatesGridViewCandidateNameHiddenField") as HiddenField).Value;
                        interviewAttemptID = Convert.ToInt32((PositionProfileStatus_candidatesGridView.Rows
                            [index].FindControl("PositionProfileStatus_candidatesGridViewInterviewAttemptIDHiddenField") as HiddenField).Value);

                        InterviewScoreParamDetail interviewScoreParamDetail = null;
                        if (interviewScoreParamDetail == null)
                            interviewScoreParamDetail = new InterviewScoreParamDetail();

                        interviewScoreParamDetail.CandidateInterviewSessionKey = interviewCandidateSessionKey;
                        interviewScoreParamDetail.AttemptID = interviewAttemptID;
                        interviewScoreParamDetail.CandidateName = candidateName;
                        PositionProfileStatus_emailConfirmation_ConfirmMsgControl.InterviewParams = interviewScoreParamDetail;
                        PositionProfileStatus_emailConfirmation_ModalPopupExtender.Show();
                        PositionProfileStatus_emailConfirmation_ConfirmMsgControl.ShowScoreUrl();

                        break;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                   PositionProfileStatus_bottomErrorMessageLabel,
                   exp.Message);
            }

        }

        protected void PositionProfileStatus_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Session[Constants.SearchCriteriaSessionKey.POSITION_PROFILE_STATUS] = null;
                PositionProfileStatus_selectedcandidates.Value = null;
                Response.Redirect(Request.RawUrl);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                   PositionProfileStatus_bottomErrorMessageLabel,
                   exp.Message);
            }
        }

        protected void PositionProfileEntry_emailButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(PositionProfileStatus_selectedcandidates.Value))
                {
                    ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, "No candidate(s) was not selected");
                    return;
                }

                PositionProfileStatus_printCandidatesGridView.Visible = true;
                int positionProfileID = GetPositionProfileID();
                int totalRecords = 0;

                string candidateStatus = null;
                if (PositionProfileStatus_showCandidatesRadioButtonList.SelectedIndex != -1)
                    candidateStatus = PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue;

                if (candidateStatus == null || candidateStatus.ToLower() == "all") candidateStatus = null;

                Session["SELECTED_CANDIDATES"] = PositionProfileStatus_selectedcandidates.Value;

                PositionProfileStatus_printCandidatesGridView.DataSource = new PositionProfileBLManager().GetPositionProfileCandidates(
                    positionProfileID, ViewState["SORT_FIELD"].ToString(), PositionProfileStatus_selectedcandidates.Value, 1,
                    1000, ViewState["FILTER_BY"].ToString(), 0, candidateStatus, out totalRecords);
                PositionProfileStatus_printCandidatesGridView.DataBind();

                AssignEmailContent();

                PositionProfileStatus_printCandidatesGridView.Visible = false;

                string popupScript = "<script language='javascript'>PositionProfileSubmitEmail('PP_STAT'," +
                    GetPositionProfileID() + ",'C','" + PositionProfileStatus_topResetLinkButton.ClientID + "');</script>";

                //Page.ClientScript.RegisterStartupScript(GetType(), "PopupScript", popupScript);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "p", popupScript, false);

                /*ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "EmailPositionProfileStatus",  "<script language='javascript'>PositionProfileSubmitEmail('PP_STAT'," +
                    GetPositionProfileID() + ",'C');</script>", false);*/
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_emailInternalButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(PositionProfileStatus_selectedcandidates.Value))
                {
                    ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, "No candidate(s) was not selected");
                    return;
                }

                PositionProfileStatus_printCandidatesGridView.Visible = true;
                int positionProfileID = GetPositionProfileID();
                int totalRecords = 0;

                string candidateStatus = null;
                if (PositionProfileStatus_showCandidatesRadioButtonList.SelectedIndex != -1)
                    candidateStatus = PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue;

                if (candidateStatus == null || candidateStatus.ToLower() == "all") candidateStatus = null;

                Session["SELECTED_CANDIDATES"] = PositionProfileStatus_selectedcandidates.Value;

                PositionProfileStatus_printCandidatesGridView.DataSource = new PositionProfileBLManager().GetPositionProfileCandidates(
                    positionProfileID, ViewState["SORT_FIELD"].ToString(), PositionProfileStatus_selectedcandidates.Value, 1,
                    1000, ViewState["FILTER_BY"].ToString(), 0, candidateStatus, out totalRecords);
                PositionProfileStatus_printCandidatesGridView.DataBind();
                AssignEmailContent();

                PositionProfileStatus_printCandidatesGridView.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "EmailPositionProfileStatus", "<script language='javascript'>PositionProfileSubmitEmail('PP_STAT'," +
                    GetPositionProfileID() + ",'I','" + PositionProfileStatus_topResetLinkButton.ClientID + "');</script>", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_candidatesGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                {
                    CandidateStatusList = new AttributeBLManager().GetAttributesByType("PP_CAN_STA", "V");
                    return;
                }

                //Bind Candidate status
                DropDownList PositionProfileStatus_positionStatusGridView_DropDownList =
                    (DropDownList)e.Row.FindControl("PositionProfileStatus_positionStatusGridView_DropDownList");
                if (CandidateStatusList != null)
                {
                    PositionProfileStatus_positionStatusGridView_DropDownList.DataSource = CandidateStatusList;
                    PositionProfileStatus_positionStatusGridView_DropDownList.DataValueField = "AttributeID";
                    PositionProfileStatus_positionStatusGridView_DropDownList.DataTextField = "AttributeName";
                    PositionProfileStatus_positionStatusGridView_DropDownList.DataBind();
                }

                var PositionProfileStatus_candidatesGridViewSelectCandidateCheckbox =
                    (CheckBox)e.Row.FindControl("PositionProfileStatus_candidatesGridViewSelectCandidateCheckbox");

                HiddenField hiddenField = (HiddenField)e.Row.FindControl("PositionProfileStatus_candidatesGridViewPPStatusHiddenField");

                // Add click handler to 'select candidates' option.
                PositionProfileStatus_candidatesGridViewSelectCandidateCheckbox.Attributes.Add
                    ("onclick", "selectedCanidates(" + PositionProfileStatus_candidatesGridViewSelectCandidateCheckbox.ClientID + ",'" + hiddenField.Value + "')");
                // Get CaiID.
                HiddenField caiIDHiddenField = (HiddenField)e.Row.FindControl
                    ("PositionProfileStatus_candidatesGridViewCaiIDHiddenField");

                //Get Candidate name control
                HyperLink PositionProfileStatus_candidatesGridViewCandidateNameLabel =
                    (HyperLink)e.Row.FindControl("PositionProfileStatus_candidatesGridViewCandidateNameLabel");

                PositionProfileStatus_candidatesGridViewCandidateNameLabel.NavigateUrl = "../ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage="+
                    Constants.ParentPage.POSITION_PROFILE_STATUS + "&candidateid=" + caiIDHiddenField.Value+ "&positionprofileid="+ GetPositionProfileID();

                // Get candidate session id
                HiddenField PositionProfileStatus_candidatesGridViewInterviewCandidateSessionIDHiddenField = (HiddenField)e.Row.FindControl
                    ("PositionProfileStatus_candidatesGridViewInterviewCandidateSessionIDHiddenField");

                //Get popup window for listing the documents
                LinkButton PositionProfileStatus_candidatesGridView_addDocumentLinkButton =
                    (LinkButton)e.Row.FindControl("PositionProfileStatus_candidatesGridView_addDocumentLinkButton");

                LinkButton PositionProfileStatus_candidateStatus_editLinkButton =
                    (LinkButton)e.Row.FindControl("PositionProfileStatus_candidateStatus_editLinkButton");

                if (!base.HasPositionProfileOwnerRights(
                   Convert.ToInt32(Request.QueryString["positionprofileid"]),
                   base.userID, new List<string> { Constants.PositionProfileOwners.OWNER, 
                           Constants.PositionProfileOwners.CO_OWNER,Constants.PositionProfileOwners.CANDIDATE_RECRUITER}))
                {
                    PositionProfileStatus_candidateStatus_editLinkButton.Visible = false;
                    PositionProfileStatus_candidatesGridView_addDocumentLinkButton.Visible = false;
                }
                else
                {
                    PositionProfileStatus_candidateStatus_editLinkButton.Visible = true;
                    PositionProfileStatus_candidatesGridView_addDocumentLinkButton.Visible = true;
                    PositionProfileStatus_candidatesGridView_addDocumentLinkButton.Attributes.Add("onclick", "return DocumentList('" + GetPositionProfileID() + "','" + caiIDHiddenField.Value + "');");
                }

                // Get attempt id
                HiddenField PositionProfileStatus_candidatesGridViewInterviewAttemptIDHiddenField = (HiddenField)e.Row.FindControl
                    ("PositionProfileStatus_candidatesGridViewInterviewAttemptIDHiddenField");

                int attemptID = 0;
                if (!Utility.IsNullOrEmpty(PositionProfileStatus_candidatesGridViewInterviewAttemptIDHiddenField.Value))
                    attemptID = Convert.ToInt32(PositionProfileStatus_candidatesGridViewInterviewAttemptIDHiddenField.Value);

                //Open Candidate Rating Summary in new window
                HyperLink PositionProfileStatus_candidatesGridViewCandidateRatingSummaryHyperLink =
                        (HyperLink)e.Row.FindControl("PositionProfileStatus_candidatesGridViewCandidateRatingSummaryHyperLink");

                PositionProfileStatus_candidatesGridViewCandidateRatingSummaryHyperLink.NavigateUrl =
                    "~/Assessments/CandidateAssessorSummary.aspx" +
                    "?m=4&s=0&candidatesessionid=" + PositionProfileStatus_candidatesGridViewInterviewCandidateSessionIDHiddenField.Value +
                    "&attemptid=" + attemptID +
                    "&positionprofileid=" + Request.QueryString["positionprofileid"] +
                    "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS;

                //Open Candidate Interview Response in new window
                HiddenField PositionProfileStatus_candidatesGridViewOnlineInterviewKeyHiddenField =
                    (HiddenField)e.Row.FindControl("PositionProfileStatus_candidatesGridViewOnlineInterviewKeyHiddenField");

                HiddenField PositionProfileStatus_candidatesGridViewChatRoomkeyHiddenField =
                    (HiddenField)e.Row.FindControl("PositionProfileStatus_candidatesGridViewChatRoomkeyHiddenField");
    
                HyperLink PositionProfileStatus_candidatesGridViewCandidateInterviewResponseSummaryHyperLink =
                        (HyperLink)e.Row.FindControl("PositionProfileStatus_candidatesGridViewCandidateInterviewResponseSummaryHyperLink");

                HiddenField PositionProfileStatus_candidatesGridViewCandidateInterviewIDHiddenField =
                    (HiddenField)e.Row.FindControl("PositionProfileStatus_candidatesGridViewCandidateInterviewIDHiddenField");

                if (!Utility.IsNullOrEmpty(PositionProfileStatus_candidatesGridViewChatRoomkeyHiddenField.Value))
                {
                    PositionProfileStatus_candidatesGridViewCandidateInterviewResponseSummaryHyperLink.NavigateUrl =
                        "~/OnlineInterview/OnlineInterviewAssessorSummary.aspx" +
                        "?m=3&s=0&interviewkey=" + PositionProfileStatus_candidatesGridViewOnlineInterviewKeyHiddenField.Value +
                        "&interviewid=" + PositionProfileStatus_candidatesGridViewCandidateInterviewIDHiddenField.Value +
                        "&chatroomid=" + PositionProfileStatus_candidatesGridViewChatRoomkeyHiddenField.Value +
                        "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS;
                }

                // Assign image URL to candidate photo.
                Image candidateImage = (Image)e.Row.FindControl
                    ("PositionProfileStatus_candidatesGridViewCandidateImage");
                candidateImage.ImageUrl = "~/Common/CandidateImageHandler.ashx?source=PP_STAT_PAGE&candidateid=" +
                    caiIDHiddenField.Value;

                Label PositionProfileStatus_positionStatusGridView_CommentsLabel =
                    (Label)e.Row.FindControl("PositionProfileStatus_positionStatusGridView_CommentsLabel");

                if (PositionProfileStatus_positionStatusGridView_CommentsLabel.Text != "")
                {
                    PositionProfileStatus_positionStatusGridView_CommentsLabel.Visible = true;

                    Label PositionProfileStatus_positionStatusGridView_statusComments_Label =
                            (Label)e.Row.FindControl("PositionProfileStatus_positionStatusGridView_statusComments_Label");

                    PositionProfileStatus_positionStatusGridView_statusComments_Label.Visible = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_printCandidatesGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                // Get CaiID.
                HiddenField caiIDHiddenField = (HiddenField)e.Row.FindControl
                    ("PositionProfileStatus_printCandidatesGridViewCaiIDHiddenField");

                // Assign image URL to candidate photo.
                Image candidateImage = (Image)e.Row.FindControl
                    ("PositionProfileStatus_printCandidatesGridViewCandidateImage");

                string path = ConfigurationManager.AppSettings["DEFAULT_URL"] +
                    ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] + "/" +
                    caiIDHiddenField.Value + "." + ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];


                if (File.Exists(Server.MapPath("~/" +
                    ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] + "/" +
                    caiIDHiddenField.Value + "." + ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"])))
                {
                    //if (File.Exists(path))
                    candidateImage.ImageUrl = path;
                }
                else
                    candidateImage.ImageUrl = base.CandidateNoPhotoAvailablePath;
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void PositionProfileEntry_activityImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "PositionProfileActivity", "<script language='javascript'>ShowPositionProfileActivityLog(" +
                    GetPositionProfileID().ToString() + ");</script>", false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void PositionProfileEntry_exportToExcelImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int totalRecords = 0;
                int position_profileID = GetPositionProfileID();
                // Get the table that contains the rows for download.

                List<CandidateInformation> candidateList = new List<CandidateInformation>();

                string candidateStatus = null;
                if (PositionProfileStatus_showCandidatesRadioButtonList.SelectedIndex != -1)
                    candidateStatus = PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue;

                if (candidateStatus == null || candidateStatus.ToLower() == "all") candidateStatus = null;

                candidateList = new PositionProfileBLManager().GetPositionProfileCandidates(
                    position_profileID, ViewState["SORT_FIELD"].ToString(), null, int.Parse(ViewState["PAGENUMBER"].ToString()),
                    int.MaxValue, ViewState["FILTER_BY"].ToString(), 0, candidateStatus, out totalRecords);

                // DataTable table = Util.ToDataSet(candidateList);

                //  DataTable table = Util.ConvertTo(candidateList);
                int positionProfileID = GetPositionProfileID();
                PositionProfileDashboard positionProfileDashboard = new PositionProfileDashboard();
                positionProfileDashboard = new PositionProfileBLManager().GetPositionProfileStatus(positionProfileID);
                ExportToExcel(candidateList, positionProfileDashboard);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_candidateAssociate_TaskOwner_CandidatesCountLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(PositionProfileStatus_candidateAssociate_TaskOwner_CandidatesIdsHiddenField.Value)) 
                    return;

                GetCandidateAssociationList(GetPositionProfileID(), 
                    PositionProfileStatus_candidateAssociate_TaskOwner_CandidatesIdsHiddenField.Value);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_interviewScheduling_scheduleDataList_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                HiddenField PositionProfileStatus_interviewSessionCount_PositionIds_HiddenField = null;

                if (e.CommandName == "SCHEDULEDINT")
                {
                    PositionProfileStatus_interviewSessionCount_PositionIds_HiddenField =
                        (HiddenField)e.Item.FindControl("PositionProfileStatus_interviewSessionCount_scheduledPositionIds_HiddenField");
                }
                else if (e.CommandName == "COMPLETEDINT")
                {
                    PositionProfileStatus_interviewSessionCount_PositionIds_HiddenField =
                       (HiddenField)e.Item.FindControl("PositionProfileStatus_interviewSessionCount_completedPositionIds_HiddenField");
                }

                if (PositionProfileStatus_interviewSessionCount_PositionIds_HiddenField == null)
                {
                    PositionProfileStatus_candidatesGridView.DataSource = null;
                    PositionProfileStatus_candidatesGridView.DataBind();
                    base.ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                        PositionProfileStatus_bottomErrorMessageLabel,
                        "No candidates found to display"); 
                    return;
                }

                //if (Utility.IsNullOrEmpty(PositionProfileStatus_interviewSessionCount_PositionIds_HiddenField.Value)) return;
                //SearchControlEnable(false);
                GetCandidateAssociationList(GetPositionProfileID(),
                    PositionProfileStatus_interviewSessionCount_PositionIds_HiddenField.Value);

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_testSchedule_scheduledDataList_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                HiddenField PositionProfileStatus_testSessionCount_PositionIds_HiddenField = null;

                if (e.CommandName == "SCHEDULEDTST")
                {
                    PositionProfileStatus_testSessionCount_PositionIds_HiddenField =
                        (HiddenField)e.Item.FindControl("PositionProfileStatus_testSchedule_scheduledPositionIds_HiddenField");
                }
                else if (e.CommandName == "COMPLETEDTST")
                {
                    PositionProfileStatus_testSessionCount_PositionIds_HiddenField =
                       (HiddenField)e.Item.FindControl("PositionProfileStatus_testSchedule_completedPositionIds_HiddenField");
                }

                if (PositionProfileStatus_testSessionCount_PositionIds_HiddenField == null) return;

                if (Utility.IsNullOrEmpty(PositionProfileStatus_testSessionCount_PositionIds_HiddenField.Value))
                {
                    PositionProfileStatus_candidatesGridView.DataSource = null;
                    PositionProfileStatus_candidatesGridView.DataBind();

                    base.ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel,
                    "No candidates found to display");
                    
                    return;
                }
                GetCandidateAssociationList(GetPositionProfileID(),
                    PositionProfileStatus_testSessionCount_PositionIds_HiddenField.Value);

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_interviewCreation_interviewsDataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                DataListItem listItem = e.Item;

                InterviewDetail interviewDetail = null;

                interviewDetail = (InterviewDetail)listItem.DataItem;

                if (interviewDetail == null) return;

                HtmlAnchor interviewName_Href = (HtmlAnchor)e.Item.FindControl("PositionProfileStatus_interviewCreation_interviewName_Href");
                if (interviewName_Href != null)
                {
                    if (interviewDetail.SessionCount > 0)
                        interviewName_Href.HRef = "../InterviewTestMaker/ViewInterviewTest.aspx?m=1&s=2&positionprofileid=" + GetPositionProfileID().ToString() + "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&testkey=" + interviewDetail.ParentInterviewKey;
                    else
                        interviewName_Href.HRef = "../InterviewTestMaker/CreateAutomaticInterviewTest.aspx?m=1&s=0&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&testkey=" + interviewDetail.ParentInterviewKey + "&positionprofileid=" + GetPositionProfileID().ToString();
                    //interviewName_Href.HRef = "../InterviewTestMaker/CreateInterviewTestSession.aspx?m=1&s=2&parentpage=S_POS_PRO&interviewtestkey=" + interviewDetail.ParentInterviewKey;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_interviewSession_DataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                DataListItem listItem = e.Item;

                InterviewDetail interviewDetail = null;

                interviewDetail = (InterviewDetail)listItem.DataItem;

                if (interviewDetail == null) return;


                HtmlAnchor interviewSessionCount_Href = (HtmlAnchor)e.Item.FindControl("PositionProfileStatus_interviewSessionCount_Href");
                interviewSessionCount_Href.InnerText = interviewDetail.InterviewTestKey + "(" + interviewDetail.SessionCount.ToString() + ")";

                if (interviewSessionCount_Href != null)
                {
                    if (interviewDetail.SessionCount > 0)
                        interviewSessionCount_Href.HRef = "../InterviewScheduler/InterviewScheduleCandidate.aspx?m=2&s=1&positionprofileid=" + GetPositionProfileID().ToString() + "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&interviewsessionid=" + interviewDetail.InterviewTestKey;
                    else
                        interviewSessionCount_Href.HRef = "../InterviewTestMaker/CreateInterviewTestSession.aspx?m=1&s=2&positionprofileid=" + GetPositionProfileID().ToString() + "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&interviewtestkey=" + interviewDetail.ParentInterviewKey;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_editWorkflowStatus_LinkButton_Click(object sender, EventArgs e)
        {
            if (!Utility.IsNullOrEmpty(GetPositionProfileID()))
                Response.Redirect("../PositionProfile/PositionProfileWorkflowSelection.aspx?m=0&s=1&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&positionprofileid="
                   + GetPositionProfileID().ToString(), false);
        }

        protected void PositionProfileStatus_testSession_DataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                DataListItem listItem = e.Item;

                TestDetail testdetail = null;

                testdetail = (TestDetail)listItem.DataItem;

                if (testdetail == null) return;

                HtmlAnchor testSessionCount_Href = (HtmlAnchor)e.Item.FindControl("PositionProfileStatus_testSession_testCount_Href");
                testSessionCount_Href.InnerText = testdetail.SessionKey + " (" + testdetail.SessionCount + ")";
                if (testSessionCount_Href != null)
                {
                    if (testdetail.SessionCount > 0)
                        testSessionCount_Href.HRef = "../Scheduler/ScheduleCandidate.aspx?m=2&s=1&positionprofileid=" + GetPositionProfileID().ToString() + "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&testsessionid=" + testdetail.SessionKey;
                    else
                        testSessionCount_Href.HRef = "../TestMaker/CreateTestSession.aspx?m=1&s=3&positionprofileid=" + GetPositionProfileID().ToString() + "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&testkey=" + testdetail.TestKey;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_positionStatus_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PositionProfileStatus_statusSaveButton.Visible = false;
                PositionProfileStatus_statusCancelLinkButton.Visible = false;

                if (PositionProfileStatus_positionStatus_DropDownList.SelectedIndex == -1) return;

                if (ViewState["POSITION_STATUS"].ToString() !=
                    PositionProfileStatus_positionStatus_DropDownList.SelectedItem.Value.Trim())
                {
                    PositionProfileStatus_statusSaveButton.Visible = true;
                    PositionProfileStatus_statusCancelLinkButton.Visible = true;

                    PositionProfileStatusDiv.Style["display"] = "block";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_positionStatusGridView_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                DropDownList ddl = (DropDownList)sender;
                GridViewRow gRow = (GridViewRow)ddl.NamingContainer;

                if (gRow == null) return;

                Button PositionProfileStatus_printCandidatesGridViewStatus_saveButton =
                    (Button)gRow.FindControl("PositionProfileStatus_printCandidatesGridViewStatus_saveButton");

                LinkButton PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton =
                    (LinkButton)gRow.FindControl("PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton");

                HiddenField PositionProfileStatus_positionStatusGridView_statusHiddenField =
                    (HiddenField)gRow.FindControl("PositionProfileStatus_positionStatusGridView_statusHiddenField");

                Label PositionProfileStatus_positionStatusGridView_statusComments_Label =
                    (Label)gRow.FindControl("PositionProfileStatus_positionStatusGridView_statusComments_Label");

                Label PositionProfileStatus_positionStatusGridView_CommentsLabel =
                    (Label)gRow.FindControl("PositionProfileStatus_positionStatusGridView_CommentsLabel");

                TextBox PositionProfileStatus_positionStatusGridView_statusComments_TextBox =
                    (TextBox)gRow.FindControl("PositionProfileStatus_positionStatusGridView_statusComments_TextBox");


                string selectedValue = ((DropDownList)(gRow.FindControl("PositionProfileStatus_positionStatusGridView_DropDownList"))).SelectedItem.Text.Trim();

                if (PositionProfileStatus_positionStatusGridView_statusHiddenField == null) return;

                if (selectedValue != PositionProfileStatus_positionStatusGridView_statusHiddenField.Value.Trim())
                {
                    PositionProfileStatus_printCandidatesGridViewStatus_saveButton.Visible = true;
                    PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton.Visible = true;
                    PositionProfileStatus_positionStatusGridView_statusComments_Label.Visible = true;
                    PositionProfileStatus_positionStatusGridView_statusComments_TextBox.Visible = true;
                    PositionProfileStatus_positionStatusGridView_statusComments_TextBox.MaxLength = 100;

                    PositionProfileStatus_positionStatusGridView_CommentsLabel.Visible = false;
                }
                else
                {
                    PositionProfileStatus_printCandidatesGridViewStatus_saveButton.Visible = false;
                    PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton.Visible = false;
                    PositionProfileStatus_positionStatusGridView_statusComments_Label.Visible = false;
                    PositionProfileStatus_positionStatusGridView_statusComments_TextBox.Visible = false;

                    PositionProfileStatus_positionStatusGridView_CommentsLabel.Visible = true;
                    if (PositionProfileStatus_positionStatusGridView_CommentsLabel.Text != "")
                        PositionProfileStatus_positionStatusGridView_statusComments_Label.Visible = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_statusCancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                LinkButton lnkButton = (LinkButton)sender;
                GridViewRow gRow = (GridViewRow)lnkButton.NamingContainer;

                if (gRow == null) return;

                Button PositionProfileStatus_printCandidatesGridViewStatus_saveButton =
                    (Button)gRow.FindControl("PositionProfileStatus_printCandidatesGridViewStatus_saveButton");

                LinkButton PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton =
                    (LinkButton)gRow.FindControl("PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton");

                HiddenField PositionProfileStatus_positionStatusGridView_statusHiddenField =
                    (HiddenField)gRow.FindControl("PositionProfileStatus_positionStatusGridView_statusHiddenField");

                Label PositionProfileStatus_positionStatusGridView_statusComments_Label =
                    (Label)gRow.FindControl("PositionProfileStatus_positionStatusGridView_statusComments_Label");

                Label PositionProfileStatus_positionStatusGridView_CommentsLabel =
                    (Label)gRow.FindControl("PositionProfileStatus_positionStatusGridView_CommentsLabel");

                TextBox PositionProfileStatus_positionStatusGridView_statusComments_TextBox =
                    (TextBox)gRow.FindControl("PositionProfileStatus_positionStatusGridView_statusComments_TextBox");

                Label PositionProfileStatus_positionStatusGridView_statusLabel =
                    (Label)gRow.FindControl("PositionProfileStatus_positionStatusGridView_statusLabel");

                DropDownList PositionProfileStatus_positionStatusGridView_DropDownList =
                        (DropDownList)gRow.FindControl("PositionProfileStatus_positionStatusGridView_DropDownList");

                PositionProfileStatus_positionStatusGridView_statusLabel.Visible = true;
                PositionProfileStatus_positionStatusGridView_CommentsLabel.Visible = true;
                PositionProfileStatus_printCandidatesGridViewStatus_saveButton.Visible = false;
                PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton.Visible = false;
                PositionProfileStatus_positionStatusGridView_statusComments_Label.Visible = false;
                PositionProfileStatus_positionStatusGridView_statusComments_TextBox.Visible = false;
                PositionProfileStatus_positionStatusGridView_DropDownList.Visible = false;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_statusSaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (PositionProfileStatus_positionStatus_DropDownList.SelectedIndex == -1) 
                    return;

                ClearControls();

                new PositionProfileBLManager().UpdatePositionProfileStatus(
                   PositionProfileStatus_positionStatus_DropDownList.SelectedValue.Trim(), null,
                   base.userID, GetPositionProfileID(), 0, "POSITION");

                ViewState["POSITION_STATUS"] = PositionProfileStatus_positionStatus_DropDownList.SelectedValue.Trim();    
                // Send alert email to position profile associates, indicating 
                // the status change.
                try
                {
                    // Send the status change alert email asynchronously.
                    PositionProfileStatusDelegate taskDelegate = new PositionProfileStatusDelegate(SendStatusChangeAlertEmail);
                    IAsyncResult result = taskDelegate.BeginInvoke(new AsyncCallback(SendStatusChangeAlertEmailCallBack), taskDelegate);
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }

                ShowMessage(PositionProfileStatus_topSuccessMessageLabel,
                   PositionProfileStatus_bottomSuccessMessageLabel, "Position profile status updated successfully");

                PositionProfileStatus_positionProfileStatusLabel.Text = PositionProfileStatus_positionStatus_DropDownList.SelectedItem.Text;
                PositionProfileStatus_positionProfileStatusHiddenField.Value = PositionProfileStatus_positionStatus_DropDownList.SelectedItem.Text;
                ///Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_candidateStatusLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                LinkButton lnkButton = (LinkButton)sender;
                GridViewRow gRow = (GridViewRow)lnkButton.NamingContainer;

                if (gRow == null) return;

                Label PositionProfileStatus_positionStatusGridView_statusLabel =
                    (Label)gRow.FindControl("PositionProfileStatus_positionStatusGridView_statusLabel");

                DropDownList PositionProfileStatus_positionStatusGridView_DropDownList =
                    (DropDownList)gRow.FindControl("PositionProfileStatus_positionStatusGridView_DropDownList");

                Button PositionProfileStatus_printCandidatesGridViewStatus_saveButton =
                    (Button)gRow.FindControl("PositionProfileStatus_printCandidatesGridViewStatus_saveButton");

                LinkButton PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton =
                    (LinkButton)gRow.FindControl("PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton");

                if (PositionProfileStatus_printCandidatesGridViewStatus_saveButton != null)
                    PositionProfileStatus_printCandidatesGridViewStatus_saveButton.Visible = true;

                if (PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton != null)
                    PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton.Visible = true;

                if (PositionProfileStatus_positionStatusGridView_DropDownList != null)
                    PositionProfileStatus_positionStatusGridView_DropDownList.Visible = true;

                if (PositionProfileStatus_positionStatusGridView_statusLabel != null)
                    PositionProfileStatus_positionStatusGridView_statusLabel.Visible = false;

                HiddenField PositionProfileStatus_positionStatusGridView_statusHiddenField =
                   (HiddenField)gRow.FindControl("PositionProfileStatus_positionStatusGridView_statusHiddenField");

                if (PositionProfileStatus_positionStatusGridView_statusHiddenField.Value != "")
                {
                    switch (PositionProfileStatus_positionStatusGridView_statusHiddenField.Value)
                    {
                        case "Associated":
                            PositionProfileStatus_positionStatusGridView_DropDownList.SelectedIndex = 0;
                            break;
                        case "Hired":
                            PositionProfileStatus_positionStatusGridView_DropDownList.SelectedIndex = 1;
                            break;
                        case "Submitted":
                            PositionProfileStatus_positionStatusGridView_DropDownList.SelectedIndex = 2;
                            break;
                        case "Rejected":
                            PositionProfileStatus_positionStatusGridView_DropDownList.SelectedIndex = 3;
                            break;
                        case "Deleted":
                            PositionProfileStatus_positionStatusGridView_DropDownList.SelectedIndex = 4;
                            break;
                    }
                }

                string selectedValue = ((DropDownList)(gRow.FindControl("PositionProfileStatus_positionStatusGridView_DropDownList"))).SelectedItem.Text.Trim();

                if (PositionProfileStatus_positionStatusGridView_statusHiddenField == null) return;

                if (PositionProfileStatus_positionStatusGridView_statusHiddenField.Value == selectedValue)
                {
                    PositionProfileStatus_printCandidatesGridViewStatus_saveButton.Visible = false;
                    PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton.Visible = false;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_candidateStatusSaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                int candidateId = 0;
                string status = string.Empty;

                Button button = (Button)sender;
                GridViewRow gRow = (GridViewRow)button.NamingContainer;

                if (gRow == null) return;

                HiddenField PositionProfileStatus_candidatesGridViewCaiIDHiddenField =
                    (HiddenField)gRow.FindControl("PositionProfileStatus_candidatesGridViewCaiIDHiddenField");

                TextBox PositionProfileStatus_positionStatusGridView_statusComments_TextBox =
                  (TextBox)gRow.FindControl("PositionProfileStatus_positionStatusGridView_statusComments_TextBox");

                HiddenField PositionProfileStatus_candidatesGridViewPPStatusHiddenField =
                    (HiddenField)gRow.FindControl("PositionProfileStatus_candidatesGridViewPPStatusHiddenField");

                if (PositionProfileStatus_candidatesGridViewCaiIDHiddenField != null)
                    candidateId = Convert.ToInt32(PositionProfileStatus_candidatesGridViewCaiIDHiddenField.Value);

                string selectedValue = ((DropDownList)(gRow.FindControl
                    ("PositionProfileStatus_positionStatusGridView_DropDownList"))).SelectedItem.Text.Trim();

                switch (selectedValue)
                {
                    case "Associated":
                        status = "PCS_ASS";
                        break;
                    case "Submitted":
                        status = "PCS_SUB";
                        break;
                    case "Rejected":
                        status = "PCS_REJ";
                        break;
                    case "Hired":
                        status = "PCS_HIR";
                        break;
                    case "Deleted":
                        status = "PCS_DEL";
                        break;
                }

                new PositionProfileBLManager().UpdatePositionProfileStatus(status, PositionProfileStatus_positionStatusGridView_statusComments_TextBox.Text.Trim(),
                  base.userID, GetPositionProfileID(), candidateId, "POSITION_CANDIDATE");

                // Send the status change alert email asynchronously.
                if (!Utility.IsNullOrEmpty(PositionProfileStatus_candidatesGridViewPPStatusHiddenField.Value))
                {
                    CandidateStatusDelegate taskDelegate = new CandidateStatusDelegate(SendCandidateStatusChangeAlertEmail);
                    IAsyncResult result = taskDelegate.BeginInvoke(Convert.ToInt32(PositionProfileStatus_candidatesGridViewPPStatusHiddenField.Value),
                        new AsyncCallback(SendCandidateStatusChangeAlertEmailCallBack), taskDelegate);
                }

                string candidateStatus = null;
                if (PositionProfileStatus_showCandidatesRadioButtonList.SelectedIndex != -1)
                    candidateStatus = PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue;
                
                if (candidateStatus == null || candidateStatus.ToLower() == "all") candidateStatus = null;

                ShowMessage(PositionProfileStatus_topSuccessMessageLabel,
                    PositionProfileStatus_bottomSuccessMessageLabel, "Position profile candidate status saved successfully");

                BindPositionProfileCandidates(candidateStatus);
                PositionProfileStatus_resultsUpdatePanel.Update();
                //Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the submit to CPX button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void PositionProfileStatus_candidatesGridView_submitToCPXButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear message.
                PositionProfileStatus_topSuccessMessageLabel.Text = string.Empty;
                PositionProfileStatus_bottomSuccessMessageLabel.Text = string.Empty;
                PositionProfileStatus_topErrorMessageLabel.Text = string.Empty;
                PositionProfileStatus_bottomErrorMessageLabel.Text = string.Empty;

                Button button = (Button)sender;
                GridViewRow gridRow = (GridViewRow)button.NamingContainer;

                if (gridRow == null) 
                    return;

                // Get candidate login ID.
                HiddenField PositionProfileStatus_candidatesGridViewCandidateLoginIDHiddenField =
                    (HiddenField)gridRow.FindControl("PositionProfileStatus_candidatesGridViewCandidateLoginIDHiddenField");

                int candidateLoginID = 0;
                int.TryParse(PositionProfileStatus_candidatesGridViewCandidateLoginIDHiddenField.Value, out candidateLoginID);

                if (candidateLoginID == 0)
                {
                    ShowMessage(PositionProfileStatus_topErrorMessageLabel, 
                        PositionProfileStatus_bottomErrorMessageLabel, "Not a valid candidate to submit");
                    return;
                }

                // Get position profile ID.
                int positionProfileID = GetPositionProfileID();

                 if (Session["USER_DETAIL"] == null)
                    return;

                // Get login user detail.
                UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                // Get XML for CPX candidate submission.
                string xml = new ExternalDataManager().GetPositionProfileCandidateData
                    ("EDS_CPX_SC", positionProfileID, candidateLoginID, userDetail);

                /*
                // Submit to CPX.
                BridgeHandlerService bridgeHandlerService = new BridgeHandlerService();
                bridgeHandlerService.Url = ConfigurationManager.AppSettings["CPX_SERVICE_URL"];

                // Submit candidate.
                bridgeHandlerService.SubmitCandidate(xml);
                */

                // Show a success message.
                ShowMessage(PositionProfileStatus_topSuccessMessageLabel,
                    PositionProfileStatus_bottomSuccessMessageLabel, "Candidate submitted to CPX successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_showOnlyMy_Candidates_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                int totalRecords = 0;

                string candidateStatus = null;
                if (PositionProfileStatus_showCandidatesRadioButtonList.SelectedIndex != -1)
                    candidateStatus = PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue;

                if (candidateStatus.ToLower() == "all") candidateStatus = null;

                if (PositionProfileStatus_showOnlyMy_Candidates_CheckBox.Checked)
                {
                    PositionProfileStatus_candidatesGridView.DataSource =
                        new PositionProfileBLManager().GetPositionProfileCandidates(
                        GetPositionProfileID(), ViewState["SORT_FIELD"].ToString(), null, 1,
                        base.GridPageSize, ViewState["FILTER_BY"].ToString(), base.userID, candidateStatus, out totalRecords);

                    PositionProfileStatus_candidatesGridView.DataBind();
                    PositionProfileStatus_candidatesPageNavigator.TotalRecords = totalRecords;
                    PositionProfileStatus_candidatesPageNavigator.PageSize = base.GridPageSize;
                    if (totalRecords == 0)
                    { 
                        ClearControls();
                        base.ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                           PositionProfileStatus_bottomErrorMessageLabel,
                           "No candidates found to display");
                    }
                }
                else
                    BindPositionProfileCandidates(candidateStatus);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_submittalClient_candidateCount_LinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(PositionProfileStatus_submittalClient_candidateCount_LinkButton.Text) &&
                    Convert.ToInt32(PositionProfileStatus_submittalClient_candidateCount_LinkButton.Text) == 0) return;
                ClearControls();
                PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue = "PCS_SUB";

                BindPositionProfileCandidates("PCS_SUB");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void PositionProfileStatus_testCreation_testDataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                DataListItem listItem = e.Item;

                TestDetail testdetail = null;

                testdetail = (TestDetail)listItem.DataItem;

                if (testdetail == null) return;

                HtmlAnchor PositionProfileStatus_testCreation_testName_Href = (HtmlAnchor)e.Item.FindControl("PositionProfileStatus_testCreation_testName_Href");

                if (PositionProfileStatus_testCreation_testName_Href != null)
                {
                    if (testdetail.SessionCount > 0)
                        PositionProfileStatus_testCreation_testName_Href.HRef = "../TestMaker/ViewTest.aspx?m=1&s=2&positionprofileid=" + GetPositionProfileID() + "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&testkey=" + testdetail.TestKey;
                    else
                        PositionProfileStatus_testCreation_testName_Href.HRef = "../TestMaker/CreateAutomaticTest.aspx?m=1&s=0&positionprofileid=" + GetPositionProfileID() + "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&index=3";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }
        
        protected void PositionProfileStatus_InterviewAssessmentDataList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                DataListItem listItem = e.Item;

                InterviewDetail interviewDetail = null;

                interviewDetail = (InterviewDetail)listItem.DataItem;

                if (interviewDetail == null) return;

                HyperLink PositionProfileStatus_interviewAssessment_CandidateCountHyperLink = (HyperLink)e.Item.FindControl("PositionProfileStatus_interviewAssessment_CandidateCountHyperLink");
                PositionProfileStatus_interviewAssessment_CandidateCountHyperLink.Text = interviewDetail.InterviewTestKey + "(" + interviewDetail.AssessedCount.ToString() + ")";

                if (PositionProfileStatus_interviewAssessment_CandidateCountHyperLink == null) return;

                if (interviewDetail.AssessedCount > 0)
                {
                    PositionProfileStatus_interviewAssessment_CandidateCountHyperLink.Attributes.Add("onclick", "javascript:return ShowViewAssessedCandidates('" + GetPositionProfileID() + "','" + interviewDetail.InterviewTestKey + "')");
                    PositionProfileStatus_interviewAssessment_CandidateCountHyperLink.NavigateUrl = "#";
                }
                //PositionProfileStatus_interviewAssessment_CandidateCountHyperLink.HRef = "../InterviewScheduler/InterviewScheduleCandidate.aspx?m=2&s=1&positionprofileid=" + GetPositionProfileID().ToString() + "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&interviewsessionid=" + interviewDetail.InterviewTestKey;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion

        #region Protected Overridden Methods
     
        public override void VerifyRenderingInServerForm(Control control)
        { /* Do nothing */ }
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            Master.SetPageCaption("Position Profile Dashboard");
            PositionProfileStatus_candidatesPageNavigator.PageNumberClick +=
                    new CommonControls.PageNavigator.PageNumberClickEventHandler(PositionProfileStatus_candidatesPageNavigator_PageNumberClick);
            if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                ViewState["SORT_FIELD"] = PositionProfileStatus_orderbyDropdownList.SelectedValue;
            if (Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                ViewState["PAGENUMBER"] = "1";
            if (Utility.IsNullOrEmpty(ViewState["FILTER_BY"]))
                ViewState["FILTER_BY"] = "A";

            PositionProfileStatus_topErrorMessageLabel.Text = "";
            PositionProfileStatus_bottomErrorMessageLabel.Text = "";
        }

        /// <summary>
        /// Gets the visibility.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        protected bool GetVisibility(string obj)
        {
            return Utility.IsNullOrEmpty(obj) ? false : true; 
            //return obj==null || obj.Length == 0 ? true : false;
        }

        #endregion

        #region Private Methods

        private void BindPositionProfile()
        {
            int positionProfileID = GetPositionProfileID();
            PositionProfileDashboard positionProfileDashboard = new PositionProfileDashboard();
            positionProfileDashboard = new PositionProfileBLManager().GetPositionProfileStatus(positionProfileID);
            if (Utility.IsNullOrEmpty(positionProfileDashboard))
            {
                base.ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                 PositionProfileStatus_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                return;
            }

            PositionProfileStatus_positionProfileNameHyperLink.Text = positionProfileDashboard.PositionProfileName;
            PositionProfileStatus_positionProfileNameHyperLink.NavigateUrl = "../PositionProfile/PositionProfileReview.aspx?m=1&s=0&positionprofileid=" + positionProfileID.ToString() + "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS;
            PositionProfileStatus_clientNameHiddenField.Value = positionProfileDashboard.ClientName;

            ViewState["POSITION_STATUS"] = "";

            if (!Utility.IsNullOrEmpty(positionProfileDashboard.PositionStatus))
            {
                switch (positionProfileDashboard.PositionStatus)
                {
                    case "PPS_OPEN":
                        PositionProfileStatus_positionProfileStatusLabel.Text = "Open";
                        PositionProfileStatus_positionProfileStatusHiddenField.Value = "Open";
                        break;
                    case "PPS_HOLD":
                        PositionProfileStatus_positionProfileStatusLabel.Text = "Hold";
                        PositionProfileStatus_positionProfileStatusHiddenField.Value = "Hold";
                        break;
                    case "PPS_CLOSED":
                        PositionProfileStatus_positionProfileStatusLabel.Text = "Closed";
                        PositionProfileStatus_positionProfileStatusHiddenField.Value = "Closed";
                        break;
                }

                ViewState["POSITION_STATUS"] = positionProfileDashboard.PositionStatus;
                PositionProfileStatus_positionStatus_DropDownList.SelectedValue = positionProfileDashboard.PositionStatus;
            }
        }

        private void BindPositionProfileCandidates(string candidateStatus)
        {
            int positionProfileID = GetPositionProfileID();
            int totalRecords = 0;

            if (candidateStatus != null && candidateStatus.ToLower() == "all")
                candidateStatus = null;

            PositionProfileStatusSearchCriteria positionProfileStatusSearchCriteria = new PositionProfileStatusSearchCriteria();

            positionProfileStatusSearchCriteria.PositionProfileId = positionProfileID.ToString();
            positionProfileStatusSearchCriteria.SortField = ViewState["SORT_FIELD"].ToString();
            positionProfileStatusSearchCriteria.PageNo = ViewState["PAGENUMBER"].ToString();

            positionProfileStatusSearchCriteria.SelectedCandiateIDs = PositionProfileStatus_selectedcandidates.Value;
            Session[Constants.SearchCriteriaSessionKey.POSITION_PROFILE_STATUS] = positionProfileStatusSearchCriteria;
            PositionProfileStatus_candidatesGridView.DataSource = new PositionProfileBLManager().GetPositionProfileCandidates(
                positionProfileID, ViewState["SORT_FIELD"].ToString(), null, int.Parse(ViewState["PAGENUMBER"].ToString()),
                base.GridPageSize, ViewState["FILTER_BY"].ToString(), 0, candidateStatus, out totalRecords);
            PositionProfileStatus_candidatesGridView.DataBind();
            PositionProfileStatus_candidatesPageNavigator.TotalRecords = totalRecords;
            PositionProfileStatus_candidatesPageNavigator.PageSize = base.GridPageSize;
            PositionProfileStatus_candidatesPageNavigator.MoveToPage(int.Parse(ViewState["PAGENUMBER"].ToString()));
            RePopulateCheckBoxes();
            if (totalRecords == 0)
            {
                //  PositionProfileStatus_showCandidates.Visible = false;
                //  PositionProfileStatus_orderbyDropdownList.Visible = false;
                ClearControls();
                base.ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                   PositionProfileStatus_bottomErrorMessageLabel,
                   "No candidates found to display");
            }
        }

        private int GetPositionProfileID()
        {
            int positionProfileID = 0;
            if (!int.TryParse(Request.QueryString
               [Constants.PositionProfileConstants.POSITION_PROFILE_ID_QUERYSTRING], out positionProfileID))
            {
                base.ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                //return;
            }
            return positionProfileID;
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void WorkflowStatusExpandorRestore()
        {
            PositionProfileStatus_workflowStatusDiv.Style["display"] = "none";
            PositionProfileStatus_workflowStatusUpSpan.Style["display"] = "none";
            PositionProfileStatus_workflowStatusDownSpan.Style["display"] = "block";

            if (!Utility.IsNullOrEmpty(PositionProfileStatus_workflowStatus_restoreHiddenField.Value) &&
                PositionProfileStatus_workflowStatus_restoreHiddenField.Value == "Y")
            {
                PositionProfileStatus_workflowStatusDiv.Style["display"] = "none";
                PositionProfileStatus_workflowStatusUpSpan.Style["display"] = "none";
                PositionProfileStatus_workflowStatusDownSpan.Style["display"] = "block";
            }
            else if (!Utility.IsNullOrEmpty(PositionProfileStatus_workflowStatus_restoreHiddenField.Value) &&
                PositionProfileStatus_workflowStatus_restoreHiddenField.Value == "N")
            {
                PositionProfileStatus_workflowStatusDiv.Style["display"] = "block";
                PositionProfileStatus_workflowStatusUpSpan.Style["display"] = "block";
                PositionProfileStatus_workflowStatusDownSpan.Style["display"] = "none";
            }
        }

        private void GetRecruiterAssignment(int positionProfileId)
        {
            int cnt = new PositionProfileBLManager().
                GetPositionProfileRecruiterAssignment(positionProfileId, "POT_CAN_RE");

            PositionProfileStatus_assignedRecruiters_new_recruiterAssigment_HyperLink.NavigateUrl
                = "../PositionProfile/PositionProfileRecruiterAssignment.aspx?m=1&s=0&positionprofileid=" + positionProfileId + "&parentpage = " + Constants.ParentPage.POSITION_PROFILE_STATUS;

            if (cnt > 0)
            {
                PositionProfileStatus_assignedRecruiters_recruiterAssigment_HyperLink.Text = cnt.ToString();
                PositionProfileStatus_assignedRecruiters_recruiterAssigment_HyperLink.NavigateUrl =
                    "../PositionProfile/PositionProfileRecruiterAssignment.aspx?m=1&s=0&positionprofileid=" + positionProfileId + "&parentpage = " + Constants.ParentPage.POSITION_PROFILE_STATUS;
                PositionProfileStatus_assignedRecruiters_edit_recruiterAssigment_HyperLink.NavigateUrl
                    = "../PositionProfile/PositionProfileRecruiterAssignment.aspx?m=1&s=0&positionprofileid=" + positionProfileId + "&parentpage = " + Constants.ParentPage.POSITION_PROFILE_STATUS;

                PositionProfileStatus_ws_recruiterAssigment.Attributes.Add("class", "pp_workflowstatus_panel_completed");
            }
        }

        private void FillSearchCriteria(PositionProfileStatusSearchCriteria positionProfileStatusSearchCriteria)
        {
            //PositionProfileStatus_positionProfileIDHiddenField.Value = positionProfileStatusSearchCriteria.PositionProfileId;
            ViewState["SORT_FIELD"] = positionProfileStatusSearchCriteria.SortField;
            ViewState["PAGENUMBER"] = positionProfileStatusSearchCriteria.PageNo;

            PositionProfileStatus_orderbyDropdownList.SelectedValue = positionProfileStatusSearchCriteria.SortField;
            PositionProfileStatus_selectedcandidates.Value = positionProfileStatusSearchCriteria.SelectedCandiateIDs;
        }

        private void ClearControls()
        {
            PositionProfileStatus_topSuccessMessageLabel.Text = string.Empty;
            PositionProfileStatus_bottomSuccessMessageLabel.Text = string.Empty;
            PositionProfileStatus_topErrorMessageLabel.Text = string.Empty;
            PositionProfileStatus_bottomErrorMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Method that send email to position profile associates indicating
        /// the status change of position profile.
        /// </summary>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendStatusChangeAlertEmail()
        {
            try
            {
                // Construct position profile detail.
                PositionProfileDetail positionProfileDetail = new PositionProfileDetail();
                positionProfileDetail.PositionProfileID = GetPositionProfileID();
                positionProfileDetail.PositionProfileName = PositionProfileStatus_positionProfileNameHyperLink.Text;
                positionProfileDetail.PositionProfileStatusName = PositionProfileStatus_positionStatus_DropDownList.SelectedItem.Text;
                positionProfileDetail.ClientName = PositionProfileStatus_clientNameHiddenField.Value;// string.Empty;
                positionProfileDetail.ModifiedByName = base.userFullName;

                // Send email.
                new EmailHandler().SendMail(EntityType.PositionProfileStatusChanged, positionProfileDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        private void AssociatedCandidatesExpandorRestore()
        {
            PositionProfileStatus_associatedCandidates_Div.Style["display"] = "none";
            PositionProfileStatus_associatedCandidates_upSpan.Style["display"] = "none";
            PositionProfileStatus_associatedCandidates_downSpan.Style["display"] = "block";

            if (!Utility.IsNullOrEmpty(PositionProfileStatus_associatedCandidates_restoreHiddenField.Value) &&
                PositionProfileStatus_associatedCandidates_restoreHiddenField.Value == "Y")
            {
                PositionProfileStatus_associatedCandidates_Div.Style["display"] = "none";
                PositionProfileStatus_associatedCandidates_upSpan.Style["display"] = "none";
                PositionProfileStatus_associatedCandidates_downSpan.Style["display"] = "block";
            }
            else if (!Utility.IsNullOrEmpty(PositionProfileStatus_associatedCandidates_restoreHiddenField.Value) &&
                PositionProfileStatus_associatedCandidates_restoreHiddenField.Value == "N")
            {
                PositionProfileStatus_associatedCandidates_Div.Style["display"] = "block";
                PositionProfileStatus_associatedCandidates_upSpan.Style["display"] = "block";
                PositionProfileStatus_associatedCandidates_downSpan.Style["display"] = "none";
            }
        }

        private void SearchControlEnable(bool flag)
        {
            PositionProfileStatus_showOnlyMy_Candidates_CheckBox.Enabled = flag;
            PositionProfileStatus_showCandidatesRadioButtonList.Enabled = flag;
            PositionProfileStatus_orderbyDropdownList.Enabled = flag;
        }

        private string RemoveLastSeparator(string objVal)
        {
            if (objVal == null) return string.Empty;

            if (objVal.LastIndexOf(',') != -1)
                objVal = objVal.Remove(objVal.LastIndexOf(','));

            return objVal;
        }

        private void GetPositionProfileTests(int positionProfileId)
        {
            try
            {
                int testCount = 0;
                string createdBy = null;

                PositionProfileStatus_ws_testCreation_HyperLink.Visible = true;
                PositionProfileStatus_ws_testSession_HyperLink.Visible = false;

                TestScheduleSearchCriteria testDetailList = new TestSessionBLManager().
                    GetPositionProfileTests(positionProfileId, out testCount, out createdBy);

                string owner = RemoveLastSeparator(createdBy);
                PositionProfileStatus_testCreation_ownerDiv.InnerText = owner;

                if (testDetailList != null && testDetailList.ListTestdetail.Count > 0)
                {
                    PositionProfileStatus_ws_testCreation_HyperLink.Visible = false;
                    PositionProfileStatus_ws_testCreation.Attributes.Add("class", "pp_workflowstatus_panel_completed");
                }
                else
                {
                    PositionProfileStatus_ws_testCreation_HyperLink.NavigateUrl = "../TestMaker/CreateAutomaticTest.aspx?m=1&s=0&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&index=3&&positionprofileid=" + positionProfileId;
                    return;
                }

                PositionProfileStatus_testCreation_testDataList.DataSource = testDetailList.ListTestdetail;
                PositionProfileStatus_testCreation_testDataList.DataBind();

                PositionProfileStatus_testSession_DataList.DataSource = testDetailList.ListTestSessiondetail;
                PositionProfileStatus_testSession_DataList.DataBind();

                int sessionCount = 0;

                if (testDetailList.ListTestSessiondetail == null)
                {
                    if (testDetailList.ListTestdetail.Count > 0)
                    {
                        PositionProfileStatus_ws_testSession_HyperLink.Visible = true;
                        TestDetail testdetail = testDetailList.ListTestdetail[0];
                        PositionProfileStatus_ws_testSession_HyperLink.NavigateUrl = "../TestMaker/CreateTestSession.aspx?m=1&s=2&positionprofileid=" + GetPositionProfileID() + "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&testkey=" + testdetail.TestKey;
                    }
                }
                else
                {
                    foreach (TestDetail id in testDetailList.ListTestSessiondetail)
                    {
                        sessionCount += id.SessionCount;
                    }

                    if (sessionCount > 0)
                    {
                        PositionProfileStatus_ws_testSession.Attributes.Add("class", "pp_workflowstatus_panel_completed");
                        PositionProfileStatus_testSession_ownerDiv.InnerText = owner;
                    }
                }

                List<TestSessionDetail> testSessionDetail = new TestSessionBLManager().
                    GetPositionProfileTestSessionCount(positionProfileId);

                PositionProfileStatus_testSchedule_scheduledDataList.DataSource = testSessionDetail;
                PositionProfileStatus_testSchedule_scheduledDataList.DataBind();

                if (testSessionDetail != null && testSessionDetail.Count > 0)
                    PositionProfileStatus_testSchedule_taskOwnerDiv.InnerText = owner;

                if (testSessionDetail != null && testSessionDetail.Count > 0)
                    PositionProfileStatus_ws_testSchedule.Attributes.Add("class", "pp_workflowstatus_panel_inprogress");

                List<string> candidateIds = new TestSessionBLManager().
                    GetPositionProfileSubmittalCandidates(positionProfileId, out testCount, out createdBy);

                PositionProfileStatus_submittalClient_ownerDiv.InnerText = createdBy;
                PositionProfileStatus_submittalClient_candidateCount_LinkButton.Text = testCount.ToString();

                if (candidateIds.Count > 0)
                    PositionProfileStatus_ws_submitClient.Attributes.Add("class", "pp_workflowstatus_panel_inprogress");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        private void GetCandidateAssociationCount(int positionProfileId)
        {
            try
            {
                string owner = null;

                List<string> candidateAssociationList = new PositionProfileBLManager().
                    GetPositionProfileCandidateAssociationCount(positionProfileId, out owner);

                if (Utility.IsNullOrEmpty(candidateAssociationList)) return;

                if (candidateAssociationList.Count < 2) return;

                PositionProfileStatus_candidateAssociate_TaskOwner_CandidatesCountLinkButton.Text = candidateAssociationList[0].ToString();

                PositionProfileStatus_candidateAssociate_TaskOwner_CandidatesIdsHiddenField.Value = candidateAssociationList[1].ToString();

                PositionProfileStatus_taskOwner_recruiterAssigment_Div.InnerText = RemoveLastSeparator(owner);
                PositionProfileStatus_candidateAssociate_TaskOwner_Div.InnerText = RemoveLastSeparator(owner);
                PositionProfileStatus_ws_candidateAssociation.Attributes.Add("class", "pp_workflowstatus_panel_completed");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel, exp.Message);
            }
        }

        private void GetCandidateAssociationList(int positionProfileId, string candidateIds)
        {
            int totalRecords = 0;

            string candidateStatus = null;
            if (PositionProfileStatus_showCandidatesRadioButtonList.SelectedIndex != -1)
                candidateStatus = PositionProfileStatus_showCandidatesRadioButtonList.SelectedValue;

            if (candidateStatus == null || candidateStatus.ToLower() == "all") candidateStatus = null;

            PositionProfileStatus_candidatesGridView.DataSource = new PositionProfileBLManager().GetPositionProfileCandidates(
              positionProfileId, ViewState["SORT_FIELD"].ToString(), candidateIds,
              int.Parse(ViewState["PAGENUMBER"].ToString()),
              base.GridPageSize, ViewState["FILTER_BY"].ToString(), 0, candidateStatus, out totalRecords);
            PositionProfileStatus_candidatesGridView.DataBind();
            PositionProfileStatus_candidatesPageNavigator.TotalRecords = totalRecords;
            PositionProfileStatus_candidatesPageNavigator.PageSize = base.GridPageSize;
            PositionProfileStatus_candidatesPageNavigator.MoveToPage(int.Parse(ViewState["PAGENUMBER"].ToString()));
            RePopulateCheckBoxes();
        }

        private void GetPositionProfileInterviews(int positionProfileId)
        {
            string interviewOwner = null;

            PositionProfileStatus_ws_interviewSession_HyperLink.Visible = false;

            PositionProfileStatus_interviewCreation_interviewsDataList.DataSource = null;
            PositionProfileStatus_interviewCreation_interviewsDataList.DataBind();

            PositionProfileStatus_interviewSession_DataList.DataSource = null;
            PositionProfileStatus_interviewSession_DataList.DataBind();

            InterviewSessionSearchCriteria interviewDetailList = new PositionProfileBLManager().
                GetPositionProfileInterviews(positionProfileId, out interviewOwner);

            if (interviewDetailList == null || interviewDetailList.ListInterviewDetail.Count == 0)
            {
                PositionProfileStatus_ws_interviewCreation_HyperLink.Visible = true;
                PositionProfileStatus_ws_interviewCreation_HyperLink.NavigateUrl = 
                    "../InterviewTestMaker/CreateAutomaticInterviewTest.aspx?m=1&s=0&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS +
                    "&positionprofileid=" + GetPositionProfileID(); ;
                return;
            }

            interviewOwner = RemoveLastSeparator(interviewOwner);

            if (interviewDetailList != null && interviewDetailList.ListInterviewDetail.Count > 0)
                PositionProfileStatus_ws_interviewCreation.Attributes.Add("class", "pp_workflowstatus_panel_completed");

            PositionProfileStatus_ws_interviewCreation_HyperLink.Visible = false;

            PositionProfileStatus_interviewCreation_ownerDiv.InnerText = interviewOwner;

            PositionProfileStatus_interviewCreation_interviewsDataList.DataSource = interviewDetailList.ListInterviewDetail;
            PositionProfileStatus_interviewCreation_interviewsDataList.DataBind();

            int sessionCount = 0; 

            if (interviewDetailList.ListInterviewSessionDetail == null)
            {
                if (sessionCount == 0)
                {
                    PositionProfileStatus_ws_interviewSession_HyperLink.Visible = true;

                    if (interviewDetailList.ListInterviewDetail.Count > 0)
                    {
                        InterviewDetail intdetail = interviewDetailList.ListInterviewDetail[0];

                        PositionProfileStatus_ws_interviewSession_HyperLink.NavigateUrl = 
                            "../InterviewTestMaker/CreateInterviewTestSession.aspx?m=1&s=2&positionprofileid=" + GetPositionProfileID() +
                            "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&interviewtestkey=" + intdetail.ParentInterviewKey;
                    }
                }
            }
            else
            {
                foreach (InterviewDetail id in interviewDetailList.ListInterviewSessionDetail)
                {
                    sessionCount += id.SessionCount; 
                }

                if (sessionCount > 0)
                {
                    PositionProfileStatus_interviewSession_taskOwner_Div.InnerText = interviewOwner;
                    PositionProfileStatus_ws_interviewSession.Attributes.Add("class", "pp_workflowstatus_panel_completed");
                }

                PositionProfileStatus_interviewSession_DataList.DataSource = interviewDetailList.ListInterviewSessionDetail;
                PositionProfileStatus_interviewSession_DataList.DataBind();
            }

            List<InterviewSessionDetail> interviewSessionDetail = new PositionProfileBLManager().
                GetPositionProfileInterviewSessionCount(positionProfileId);

            PositionProfileStatus_interviewScheduling_scheduleDataList.DataSource = interviewSessionDetail;
            PositionProfileStatus_interviewScheduling_scheduleDataList.DataBind();

            if (interviewSessionDetail != null && interviewSessionDetail.Count > 0)
                PositionProfileStatus_interviewScheduling_taskOwnerDiv.InnerText = interviewOwner;

            if (sessionCount > 0)
                PositionProfileStatus_ws_interviewSchedule.Attributes.Add("class", "pp_workflowstatus_panel_inprogress"); 
             
            PositionProfileStatus_InterviewAssessmentDataList.DataSource = interviewDetailList.ListInterviewSessionDetail;
            PositionProfileStatus_InterviewAssessmentDataList.DataBind();

            GetPositionProfileInterviewAssessment(positionProfileId);
        }

        private void GetPositionProfileInterviewAssessment(int positionProfileId)
        {
            string createdBy = null;
            List<string> interviewAssessmentList = new PositionProfileBLManager().
                   GetPositionProfileInterviewAssessment(positionProfileId, base.userID, out createdBy);

            if (Utility.IsNullOrEmpty(interviewAssessmentList)) return;
            if (interviewAssessmentList.Count < 1) return;

            if (Convert.ToInt32(interviewAssessmentList[0]) == 0) return;

            createdBy = RemoveLastSeparator(createdBy);
            PositionProfileStatus_interviewAssessment_OwnerDiv.InnerText = createdBy;
            PositionProfileStatus_ws_interviewAssessment.Attributes.Add("class", "pp_workflowstatus_panel_completed");

            //PositionProfileStatus_interviewAssessment_CandidateCountHyperLink.Text = interviewAssessmentList[0].ToString();
            /*PositionProfileStatus_interviewAssessment_CandidateCountHyperLink.NavigateUrl =
                "../Assessments/MyAssessments.aspx?m=5&s=0&parentpage=" + Constants.ParentPage.POSITION_PROFILE_STATUS + "&positionprofileid=" + positionProfileId;*/
        }

        private List<string> SelectedCandidateDetails
        {
            get
            {
                List<string> candidateReportDetailList = new List<string>();
                if (!Utility.IsNullOrEmpty(PositionProfileStatus_selectedcandidates.Value))
                {
                    string[] candidates = PositionProfileStatus_selectedcandidates.Value.Split(',');
                    foreach (string candidate in candidates)
                    {
                        if (candidate.Length > 0)
                        {
                            candidateReportDetailList.Add(candidate);
                        }
                    }
                }
                return candidateReportDetailList;
            }
        }

        private void RePopulateCheckBoxes()
        {
            foreach (GridViewRow row in PositionProfileStatus_candidatesGridView.Rows)
            {
                var chkBox = row.FindControl("PositionProfileStatus_candidatesGridViewSelectCandidateCheckbox") as CheckBox;

                var hiddenField =
                   (HiddenField)row.FindControl("PositionProfileStatus_candidatesGridViewPPStatusHiddenField");

                chkBox.Checked = SelectedCandidateDetails.Exists(X => X == hiddenField.Value);
            }
        }

        private void PDFHeader(out CandidateReportDetail candidateReportDetail, out string fileName)
        {
            candidateReportDetail = new CandidateReportDetail();
            // candidateReportDetail = GetSelectedItems(candidateReportDetail);
            fileName = PositionProfileStatus_positionProfileNameHyperLink.Text.Trim();
            UserDetail userDetail = new UserDetail();
            userDetail = (UserDetail)Session["USER_DETAIL"];
            candidateReportDetail.TestName = fileName;
            candidateReportDetail.LoginUser = userDetail.FirstName + "," + userDetail.LastName;
        }

        private void AssignEmailContent()
        {
            StringBuilder stringBuilder = new StringBuilder();
            StringWriter SW = new StringWriter(stringBuilder);
            HtmlTextWriter htmlTW = new HtmlTextWriter(SW);
            //PositionProfileStatus_informationPanel.RenderControl(htmlTW);
            PositionProfileStatus_printCandidatesGridViewPanel.RenderControl(htmlTW);
            stringBuilder = new StringBuilder();
            stringBuilder.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
            stringBuilder.Append("<head>");
            stringBuilder.Append("<LINK href=\"" + ConfigurationManager.AppSettings["DEFAULT_URL"] + "/App_Themes/DefaultTheme/DefaultStyle.css\" rel=\"stylesheet\" type=\"text/css\">");
            stringBuilder.Append("<title>");
            stringBuilder.Append("Position Profile");
            stringBuilder.Append("</title>");
            stringBuilder.Append("</head>");
            stringBuilder.Append("<body>");
            stringBuilder.Append("<table style=\"width:100%; height:50px; background-color:white;overflow:auto\"><tr><td valign=\"top\">&nbsp;</td></tr></table>");

            stringBuilder.Append(htmlTW.InnerWriter);
            stringBuilder.Append("</body>");
            stringBuilder.Append("</html>");

            Session[Constants.SessionConstants.PP_STATUS_EMAIL_CONTENT] = stringBuilder.ToString();
            Session[Constants.SessionConstants.PP_SELECTED_CANDIATE_ID] = SelectedCandidateDetails;
        }

        /// <summary>
        /// Method that send email to recruiter indicating the status change
        /// of candidate against position profile.
        /// </summary>
        /// <param name="ID">
        /// A <see cref="int"/> that holds the gen ID.
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendCandidateStatusChangeAlertEmail(int genID)
        {
            try
            {
                // Send email.
                new EmailHandler().SendMail(EntityType.PositionProfileCandidateStatusChanged, genID.ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        private void AssignClientDetails()
        {
            // Get client details
            if (GetPositionProfileID() != 0)
            {
                ClientInformation clientInfo =
                    new ClientBLManager().GetClientInfoPositionProfileID(GetPositionProfileID());

                if (!Utility.IsNullOrEmpty(clientInfo))
                {
                    //Set client name
                    if (!Utility.IsNullOrEmpty(clientInfo.ClientName))
                    {
                        PositionProfileStatus_clientNameLinkButton.Text = clientInfo.ClientName;
                    }
                    else
                    {
                        PositionProfileStatus_clientNameLinkButton.Text = "Client not associated";
                    }

                    // Add handler for client name link button.
                    if (clientInfo.ClientID != 0)
                    {
                        PositionProfileStatus_clientNameLinkButton.Attributes.Add("onclick",
                            "javascript:return ShowViewClient('" + clientInfo.ClientID + "');");
                    }

                    // Get client departments
                    new ClientInfoDataManager().GetClientDepartmentContactInfo(PositionProfileStatus_showClientDepartmentsLabel,
                        clientInfo.ClientDepartments, "CD", true);
                    // Get client contacts
                    new ClientInfoDataManager().GetClientDepartmentContactInfo(PositionProfileStatus_showClientContactsLabel,
                        clientInfo.ClientContacts, "CC", true);
                }
                else
                {
                    PositionProfileStatus_clientNameLinkButton.Text = "Client not associated";
                }
            }
        }

        #endregion Private Methods

        #region Public Methods
        public void ExportToExcel(List<CandidateInformation> candidateList, PositionProfileDashboard positionProfileDashboard)
        {
            try
            {
                if (positionProfileDashboard == null && candidateList == null)
                {
                    base.ShowMessage(PositionProfileStatus_topErrorMessageLabel,
                    PositionProfileStatus_bottomErrorMessageLabel,
                    "No candidates found to export");
                    return;
                }

                string fileName = positionProfileDashboard.PositionProfileName.Replace(' ', '_');
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=" + fileName + ".xlsx");

                DataTable positionProfileDetailTable = new DataTable();
                positionProfileDetailTable.Columns.Add("Position Profile Name", typeof(string));
                positionProfileDetailTable.Columns.Add("Client Name", typeof(string));
                positionProfileDetailTable.Columns.Add("Date Of Registration", typeof(DateTime));
                positionProfileDetailTable.Columns.Add("Submittal Deadline", typeof(DateTime));
                positionProfileDetailTable.Columns.Add("Registered By", typeof(string));
                DataRow dr1;
                dr1 = positionProfileDetailTable.NewRow();
                dr1[0] = positionProfileDashboard.PositionProfileName;
                dr1[1] = positionProfileDashboard.ClientName;
                dr1[2] = positionProfileDashboard.DateOfRegistration == null ? "" :
                     GetDateFormat(Convert.ToDateTime(positionProfileDashboard.DateOfRegistration)); ;
                dr1[3] = positionProfileDashboard.SubmittalDeadline == null ? "" :
                     GetDateFormat(Convert.ToDateTime(positionProfileDashboard.SubmittalDeadline));
                dr1[4] = positionProfileDashboard.RegisteredBy;

                positionProfileDetailTable.Rows.Add(dr1);

                DataTable skillTable = new DataTable();
                skillTable.Columns.Add("Skill Name", typeof(string));
                skillTable.Columns.Add("Weightage", typeof(decimal));

                DataRow skillDataRow;
                if (positionProfileDashboard.Keywords != null)
                {
                    string[] segmentKeywords = positionProfileDashboard.Keywords.Split(',');
                    List<SkillWeightage> skillWeightageList = new List<SkillWeightage>();
                    foreach (string item in segmentKeywords)
                    {
                        if (Support.Utility.IsNullOrEmpty(item))
                            continue;
                        string skillName = item.Substring(0, item.IndexOf('['));
                        int weightage = 0;
                        int.TryParse(item.Substring(item.IndexOf('[') + 1, item.Length - item.IndexOf('[') - 2), out weightage);
                        skillDataRow = skillTable.NewRow();
                        skillDataRow[0] = skillName;
                        skillDataRow[1] = weightage;
                        skillTable.Rows.Add(skillDataRow);
                    }
                }
                using (ExcelPackage pck = new ExcelPackage())
                {
                    // Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add
                        (fileName);

                    // Load the datatable into the sheet, starting from the 
                    // cell A1 and print the column names on row 1.
                    ws.Cells[Constants.ExcelExport.STARTING_CELL].LoadFromDataTable(positionProfileDetailTable, true);
                    ws.Row(1).Style.Font.Bold = true;

                    string decimalFormat = Constants.ExcelExportFormats.DECIMAL_COMMA;
                    foreach (DataColumn column in positionProfileDetailTable.Columns)
                    {
                        if (column.DataType == typeof(System.DateTime))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + positionProfileDetailTable.Rows.Count, column.Ordinal + 1])
                            {
                                col.Style.Numberformat.Format = Constants.ExcelExportFormats.DATE;

                            }
                        }
                        else if (column.DataType == typeof(decimal))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + positionProfileDetailTable.Rows.Count, column.Ordinal + 1])
                            {
                                if (decimalFormat == "C")
                                {
                                    for (int row = 2; row <= ws.Dimension.End.Row; row++)
                                    {
                                        ws.Cells[row, column.Ordinal + 1].Value =
                                            ws.Cells[row, column.Ordinal + 1].Value != null ? ws.Cells[row, column.Ordinal + 1].Value.ToString().Replace(".", ",") : string.Empty;
                                    }
                                    //col.Style.Numberformat.Format = "0\",\"0000";
                                }
                                else
                                    col.Style.Numberformat.Format = "###0.0000";
                            }
                        }
                    }

                    ws.Cells["A" + (positionProfileDetailTable.Rows.Count + 3)].LoadFromDataTable(skillTable, true);
                    ws.Row(positionProfileDetailTable.Rows.Count + 3).Style.Font.Bold = true;
                    foreach (DataColumn column in skillTable.Columns)
                    {
                        if (column.DataType == typeof(System.DateTime))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + skillTable.Rows.Count, column.Ordinal + 1])
                            {
                                col.Style.Numberformat.Format = Constants.ExcelExportFormats.DATE;
                            }
                        }
                        else if (column.DataType == typeof(decimal))
                        {
                            using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + skillTable.Rows.Count, column.Ordinal + 1])
                            {
                                if (decimalFormat == "C")
                                {
                                    for (int row = 2; row <= ws.Dimension.End.Row; row++)
                                    {
                                        ws.Cells[row, column.Ordinal + 1].Value =
                                            ws.Cells[row, column.Ordinal + 1].Value != null ? ws.Cells[row, column.Ordinal + 1].Value.ToString() : string.Empty;
                                    }
                                    // col.Style.Numberformat.Format = "0\",\"0000";
                                }
                                else
                                    col.Style.Numberformat.Format = "###0.0000";
                            }
                        }
                    }

                    if (candidateList != null)
                    {
                        DataTable candidateTable = new DataTable();
                        candidateTable.Columns.Add("Candidate Name", typeof(string));
                        candidateTable.Columns.Add("Associated By", typeof(string));
                        candidateTable.Columns.Add("Scheduler Name", typeof(string));
                        candidateTable.Columns.Add("Test Score", typeof(decimal));
                        candidateTable.Columns.Add("Interview Total Weighted Score", typeof(string));
                        // candidateTable.Columns.Add("Resume Score", typeof(decimal));
                        candidateTable.Columns.Add("Test Status", typeof(string));
                        candidateTable.Columns.Add("Interview Status", typeof(string));
                        // candidateTable.Columns.Add("Source From", typeof(string));

                        DataRow candidateDataRow;
                        foreach (CandidateInformation item in candidateList)
                        {
                            if (Support.Utility.IsNullOrEmpty(item))
                                continue;
                            candidateDataRow = candidateTable.NewRow();
                            candidateDataRow[0] = item.caiFirstName;
                            candidateDataRow[1] = item.RecruiterName;
                            candidateDataRow[2] = item.SchedulerName;
                            candidateDataRow[3] = item.TestScore;
                            candidateDataRow[4] = item.InterviewTotalWeightedScore;
                            // candidateDataRow[5] = item.ResumeScore;
                            candidateDataRow[5] = item.Status;
                            candidateDataRow[6] = item.InterviewStatus;
                            //candidateDataRow[8] = item.SourceFrom;
                            candidateTable.Rows.Add(candidateDataRow);
                        }
                        ws.Cells["A" + (positionProfileDetailTable.Rows.Count + 3 + skillTable.Rows.Count + 2)].LoadFromDataTable(candidateTable, true);
                        ws.Row(positionProfileDetailTable.Rows.Count + 3 + skillTable.Rows.Count + 2).Style.Font.Bold = true;

                        foreach (DataColumn column in candidateTable.Columns)
                        {
                            if (column.DataType == typeof(System.DateTime))
                            {
                                using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + candidateTable.Rows.Count, column.Ordinal + 1])
                                {
                                    col.Style.Numberformat.Format = Constants.ExcelExportFormats.DATE;
                                }
                            }
                            else if (column.DataType == typeof(decimal))
                            {
                                using (ExcelRange col = ws.Cells[2, column.Ordinal + 1, 2 + candidateTable.Rows.Count, column.Ordinal + 1])
                                {
                                    if (decimalFormat == "C")
                                    {
                                        for (int row = 2; row <= ws.Dimension.End.Row; row++)
                                        {
                                            ws.Cells[row, column.Ordinal + 1].Value =
                                                ws.Cells[row, column.Ordinal + 1].Value != null ? ws.Cells[row, column.Ordinal + 1].Value.ToString() : string.Empty;
                                        }
                                        //col.Style.Numberformat.Format = "0\",\"0000";
                                    }
                                    else
                                        col.Style.Numberformat.Format = "###0.0000";
                                }
                            }
                        }
                    }
                    Response.BinaryWrite(pck.GetAsByteArray());
                    Response.End();
                }
                //  HttpContext.Current.Response.End();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        #endregion

        #region Asynchronous Method Handlers

        /// <summary>
        /// Handler method that acts as the callback method for position profile 
        /// status change email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendStatusChangeAlertEmailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                PositionProfileStatusDelegate caller = (PositionProfileStatusDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that acts as the callback method for candidate status
        /// change email alert against the position profile.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendCandidateStatusChangeAlertEmailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                CandidateStatusDelegate caller = (CandidateStatusDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Asynchronous Method Handlers
    }
}