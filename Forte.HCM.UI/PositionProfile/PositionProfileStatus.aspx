﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/PositionProfileMaster.Master"
    AutoEventWireup="true" CodeBehind="PositionProfileStatus.aspx.cs" Inherits="Forte.HCM.UI.PositionProfile.PositionProfileStatus" %>

<%@ Register Src="~/CommonControls/PageNavigator.ascx" TagName="pageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmInterviewScoreMsgControl.ascx" TagName="ConfirmInterviewScoreMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PositionProfileMaster_body" runat="server">
    <script language="javascript" type="text/javascript">

        function selectedCanidates(a, b) {
            try {
                var candidates = document.getElementById('<%=PositionProfileStatus_selectedcandidates.ClientID %>').value;
                if (a.checked) {
                    if (candidates.length == 0)
                        candidates = b;
                    else
                        candidates = candidates + "," + b;
                }
                else {
                    candidates = candidates.replace(b, "");
                }
                document.getElementById('<%=PositionProfileStatus_selectedcandidates.ClientID %>').value = candidates;
            }
            catch (exp) {
                // alert(exp);
            }
        }

        function SetPositionStatusDivToggle() {
            $("#<%= PositionProfileStatusDiv.ClientID %>").slideToggle();

            //.toggle(400);  
            /*var div = document.getElementById('<%= PositionProfileStatusDiv.ClientID %>');
            if (div.style.display == 'block')
            div.style.display = 'none';
            else
            div.style.display = 'block';*/

            return false;
        }

        function WorkflowStatusDivSlideDown() {
            $("#<%= PositionProfileStatus_workflowStatusDiv.ClientID %>").slideDown(1500);
            $("#<%= PositionProfileStatus_workflowStatusUpSpan.ClientID %>").show();
            $("#<%= PositionProfileStatus_workflowStatusDownSpan.ClientID %>").hide();
            return false;
        }

        function WorkflowStatusDivSlideUp() {
            $("#<%= PositionProfileStatus_workflowStatusDiv.ClientID %>").slideUp(1500);
            $("#<%=PositionProfileStatus_workflowStatusUpSpan.ClientID %>").hide();
            $("#<%=PositionProfileStatus_workflowStatusDownSpan.ClientID %>").show();
            return false;
        }

        function HideDiv() {

            var hdnObj = document.getElementById('<%= PositionProfileStatus_positionProfileStatusHiddenField.ClientID %>');
            var index = -1;

            if (hdnObj.value == 'Open') {
                index = 0;
            }
            else if (hdnObj.value == 'Hold') {
                index = 1;
            }
            else if (hdnObj.value == 'Closed') {
                index = 2;
            }

            document.getElementById('<%= PositionProfileStatus_positionStatus_DropDownList.ClientID %>').selectedIndex = index;
            document.getElementById('<%= PositionProfileStatus_statusCancelLinkButton.ClientID  %>').style.display = 'none';
            document.getElementById('<%= PositionProfileStatus_statusSaveButton.ClientID  %>').style.display = 'none';

            var div = document.getElementById('<%= PositionProfileStatusDiv.ClientID %>');

            if (div.style.display == 'block')
                div.style.display = 'none';


            return false;
        }

        function checkTextAreaMaxLength(textBox, e, length) {

            var mLen = textBox["MaxLength"];
            if (null == mLen)
                mLen = length;

            var maxLength = parseInt(mLen);
            if (!checkSpecialKeys(e)) {
                if (textBox.value.length > maxLength - 1) {
                    if (window.event)//IE
                        e.returnValue = false;
                    else//Firefox
                        e.preventDefault();
                }
            }
        }
        function checkSpecialKeys(e) {
            if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40)
                return false;
            else
                return true;
        }

        function AdditionalDocumentList(positionprofileid, candidateid) {
            var height = 460;
            var width = 500;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;

            window.open("../Popup/AdditionalDocument.aspx?positionprofileid=" + positionprofileid + "&candidateid=" + candidateid, "_blank", "height=500, width=575, left=150,top=150, " +
            "location=no, menubar=no, resizable=no, " +
            "scrollbars=no, titlebar=no, toolbar=no", true);

            /*var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:"
            + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../Popup/AdditionalDocument.aspx" +
            "?positionprofileid=" + pp_id +
            "&candidateid=" + candidateid;

            showModalDialog(queryStringValue, window.self, sModalFeature); */
            return false;
        }
        function ParentWindowFunction() {
            alert('Hi');
            return false;
        }
    </script>
    <asp:UpdatePanel RenderMode="Inline" runat="server" ID="UpdatePanel2">
        <Triggers>
            <asp:PostBackTrigger ControlID="PositionProfileStatus_candidatesGridView" />
            <asp:PostBackTrigger ControlID="PositionProfileEntry_topActivityImageButton" />
            <asp:PostBackTrigger ControlID="PositionProfileEntry_bottomActivityImageButton" />
            <asp:PostBackTrigger ControlID="PositionProfileEntry_topEmailButton" />
            <asp:PostBackTrigger ControlID="PositionProfileEntry_bottomEmailButton" />
            <asp:PostBackTrigger ControlID="PositionProfileEntry_topDownloadImageButton" />
            <asp:PostBackTrigger ControlID="PositionProfileEntry_bottomDownloadImageButton" />
            <%--<asp:AsyncPostBackTrigger ControlID="PositionProfileStatus_showCandidates" />--%>
        </Triggers>
        <ContentTemplate>
            <asp:HiddenField ID="PositionProfileStatus_selectedcandidates" runat="server" />
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td class="header_bg">
                        <table width="100%" border="0" cellspacing="2" cellpadding="0">
                            <tr>
                                <td width="78%" class="header_text_bold">
                                    <asp:UpdatePanel ID="PositionProfileStatus_positionProfileNameUpdatePanel" runat="server">
                                        <ContentTemplate>
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td width="80%">
                                                        <asp:HiddenField ID="PositionProfileStatus_clientNameHiddenField" runat="server" />
                                                        <asp:HyperLink ID="PositionProfileStatus_positionProfileNameHyperLink" runat="server"
                                                            SkinID="sknOrangeBoldHyperLink" Target="_blank" Font-Overline="false" ToolTip="Click here to review the position profile" />
                                                    </td>
                                                    <td width="7%">
                                                        <asp:Label ID="PositionProfileStatus_positionProfileStatusHeadLabel" runat="server"
                                                            Text="Position Status -" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td width="6%">
                                                        <asp:Label ID="PositionProfileStatus_positionProfileStatusLabel" runat="server" SkinID="sknLabelAssessorFieldscoreText"></asp:Label>
                                                        <asp:HiddenField ID="PositionProfileStatus_positionProfileStatusHiddenField" runat="server" />
                                                    </td>
                                                    <td width="7%" align="left">
                                                        <asp:LinkButton ID="PositionProfileStatus_positionProfileStatusLinkButton" OnClientClick="javascript:return SetPositionStatusDivToggle();"
                                                            runat="server" Visible="false" SkinID="sknActionLinkButton" ToolTip="Click Here To Change The Position Status">(Change)</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="PositionProfileStatus_statusSaveButton" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td width="22%" align="right">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="PositionProfileEntry_topDownloadImageButton" runat="server"
                                                    ToolTip="Export To Excel" SkinID="sknExportExcelImageButton" CommandName="Export"
                                                    OnClick="PositionProfileEntry_exportToExcelImageButton_Click" />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="PositionProfileEntry_topActivityImageButton" runat="server"
                                                    SkinID="sknSkillViewPPActivityImageButton" ToolTip="Position Profile Activity"
                                                    CommandName="Activity" Visible="true" OnClick="PositionProfileEntry_activityImageButton_Click" />
                                            </td>
                                            <td>
                                                <asp:Button ID="PositionProfileEntry_topEmailButton" runat="server" Text="Submit To Client"
                                                    Visible="false" ToolTip="Submit To Client" SkinID="sknButtonId" OnClick="PositionProfileEntry_emailButton_Click" />
                                            </td>
                                            <td>
                                                <asp:Button ID="PositionProfileStatus_topEmail_InternalButton" runat="server" Text="Internal Submittal"
                                                    Visible="false" ToolTip="Internal Submittal" SkinID="sknButtonId" OnClick="PositionProfileStatus_emailInternalButton_Click" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="PositionProfileStatus_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="PositionProfileStatus_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td class="link_button">
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="PositionProfileStatus_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                    Text="Cancel" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <%--                        <asp:UpdatePanel runat="server" ID="PositionProfileStatus_topMessageUpdatePanel">
                            <ContentTemplate>--%>
                        <asp:Label ID="PositionProfileStatus_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="PositionProfileStatus_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        <%--   </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </td>
                </tr>
                <tr>
                    <td class="tab_body_bg">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <asp:Panel ID="PositionProfileStatus_informationPanel" runat="server">
                                        <table cellpadding="0" cellspacing="3" width="100%">
                                            <tr>
                                                <td>
                                                    <div id="PositionProfileStatusDiv" runat="server" style="display: none">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td class="td_height_4">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="11%">
                                                                    <asp:Label ID="PositionProfileStatus_positionStatusLabel" runat="server" SkinID="sknCanReumeLabelFieldText"
                                                                        Text="Position Status"></asp:Label>
                                                                </td>
                                                                <td width="10%">
                                                                    <asp:DropDownList ID="PositionProfileStatus_positionStatus_DropDownList" runat="server"
                                                                        SkinID="sknAssessorDropDownList" AutoPostBack="true" OnSelectedIndexChanged="PositionProfileStatus_positionStatus_DropDownList_SelectedIndexChanged">
                                                                        <asp:ListItem Text="Open" Value="PPS_OPEN" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Text="Hold" Value="PPS_HOLD"></asp:ListItem>
                                                                        <asp:ListItem Text="Closed" Value="PPS_CLOSED"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td class="td_height_10" width="1%">
                                                                    &nbsp;
                                                                </td>
                                                                <td width="7%">
                                                                    <asp:Button ID="PositionProfileStatus_statusSaveButton" runat="server" SkinID="sknButtonId"
                                                                        Text="Save" Visible="false" ToolTip="Click here to save the position status"
                                                                        OnClick="PositionProfileStatus_statusSaveButton_Click" />
                                                                </td>
                                                                <td class="td_height_10" width="1%">
                                                                    &nbsp;
                                                                </td>
                                                                <td align="left" width="10%">
                                                                    <asp:LinkButton ID="PositionProfileStatus_statusCancelLinkButton" Visible="false"
                                                                        runat="server" SkinID="sknActionLinkButton" ToolTip="Click here to cancel saving position status"
                                                                        OnClientClick="javascript:return HideDiv();">Cancel</asp:LinkButton>
                                                                </td>
                                                                <td width="60%">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_4">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <!-- Modified -->
                            <tr>
                                <td style="padding: 3px">
                                    <asp:UpdatePanel ID="PositionProfileStatus_clientInfoUpdatePanel" runat="server"
                                        UpdateMode="Always">
                                        <ContentTemplate>
                                            <table cellpadding="0" cellspacing="0" border="0" width="940px">
                                                <tr runat="server">
                                                    <td class="client_tab_body_bg">
                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                            <tr>
                                                                <td style="width: 200px">
                                                                    <asp:LinkButton ID="PositionProfileStatus_clientNameLinkButton" SkinID="sknLabelFieldTextLinkButton"
                                                                        ToolTip="Click here to view client details" runat="server"></asp:LinkButton>
                                                                </td>
                                                                <td style="width: 300px">
                                                                    <asp:Label ID="PositionProfileStatus_showClientDepartmentsLabel" runat="server"></asp:Label>
                                                                </td>
                                                                <td style="width: 300px">
                                                                    <asp:Label ID="PositionProfileStatus_showClientContactsLabel" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <!-- End Modified -->
                            <tr>
                                <td width="100%" style="padding-left: 3px">
                                    <table cellpadding="0" cellspacing="0" border="0" width="940px">
                                        <tr>
                                            <td class="header_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 55%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="PositionProfileStatus_workflowStatusLiteral" runat="server" Text="Workflow Status"></asp:Literal>&nbsp;
                                                        </td>
                                                        <td width="10%">
                                                            <asp:HyperLink ID="PositionProfileStatus_editWorkflowStatus_HyperLink" SkinID="sknLabelFieldTextHyperLink"
                                                                runat="server" Target="_blank" Visible="false" ToolTip="Edit Workflow" Text="Edit Workflow"></asp:HyperLink>
                                                        </td>
                                                        <td width="2%" align="center">
                                                            <div style="border: 1px solid #A5A5A5; height: 10px; width: 10px; background-color: #edecec;">
                                                            </div>
                                                        </td>
                                                        <td width="7%">
                                                            <asp:Label ID="PositionProfileStatus_pendingLegendLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="Pending"></asp:Label>
                                                        </td>
                                                        <td width="2%" align="center">
                                                            <div style="border: 1px solid #A5A5A5; height: 10px; width: 10px; background-color: #fffad0">
                                                            </div>
                                                        </td>
                                                        <td width="8%">
                                                            <asp:Label ID="PositionProfileStatus_inProgressLegendLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="In Progress"></asp:Label>
                                                        </td>
                                                        <td width="2%" align="center">
                                                            <div style="border: 1px solid #A5A5A5; height: 10px; width: 10px; background-color: #DCFCE2">
                                                            </div>
                                                        </td>
                                                        <td width="8%">
                                                            <asp:Label ID="PositionProfileStatus_completedLegendLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="Completed"></asp:Label>
                                                        </td>
                                                        <td style="width: 2%" align="right" valign="middle">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr id="PositionProfileStatus_workflowStatusTR" runat="server">
                                                                    <td>
                                                                        <span id="PositionProfileStatus_workflowStatusUpSpan" runat="server" style="display: none;">
                                                                            <asp:Image ID="PositionProfileStatus_workflowStatusImage" runat="server" SkinID="sknMinimizeImage" />
                                                                        </span><span id="PositionProfileStatus_workflowStatusDownSpan" runat="server" style="display: block;">
                                                                            <asp:Image ID="PositionProfileStatus_workflowStatusDownImage" runat="server" SkinID="sknMaximizeImage" />
                                                                        </span>
                                                                        <asp:HiddenField ID="PositionProfileStatus_workflowStatus_restoreHiddenField" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_border" style="width: 100%;">
                                                <div runat="server" id="PositionProfileStatus_workflowStatusDiv" visible="true">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="80%" align="center">
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="pp_workflowstatus_panel_bg" id="PositionProfileStatus_ws_recruiterAssigment"
                                                                runat="server" colspan="3" valign="top">
                                                                <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                                                    <tr>
                                                                        <td colspan="3" align="left" class="pp_workflowstatus_panel_headertext">
                                                                            <asp:Label ID="PositionProfileStatus_recruiterAssigment_TitleLabel" runat="server"
                                                                                Text="Recruiter Assignment"></asp:Label>
                                                                        </td>
                                                                        <td align="right" style="padding-right: 15px">
                                                                            <asp:HyperLink ID="PositionProfileStatus_assignedRecruiters_edit_recruiterAssigment_HyperLink"
                                                                                SkinID="sknLabelFieldTextHyperLink" Visible="false" runat="server" ToolTip="Click here to edit the assigned recruiters"
                                                                                Target="_blank">edit</asp:HyperLink>
                                                                        </td>
                                                                        <td width="1%">
                                                                            <asp:HyperLink ID="PositionProfileStatus_assignedRecruiters_new_recruiterAssigment_HyperLink"
                                                                                runat="server" Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/pp_add_btn.png"
                                                                                ToolTip="Change/Assign Recruiters" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="5" class="position_profile_dashboard_table_border">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="10%">
                                                                            <asp:Label ID="PositionProfileStatus_taskOwner_recruiterAssigment_TitleLabel" runat="server"
                                                                                Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td width="60%">
                                                                            <div class="pp_dashboard_divtext" id="PositionProfileStatus_taskOwner_recruiterAssigment_Div"
                                                                                runat="server">
                                                                            </div>
                                                                        </td>
                                                                        <td width="15%">
                                                                            <asp:Label ID="PositionProfileStatus_assignedRecruiters_recruiterAssigment_TitleLabel"
                                                                                runat="server" Text="Assigned Recruiters : " SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td width="15%" colspan="2" align="left">
                                                                            <asp:HyperLink ID="PositionProfileStatus_assignedRecruiters_recruiterAssigment_HyperLink"
                                                                                runat="server" SkinID="sknLabelFieldTextHyperLink" Target="_blank" ToolTip="View Recruiters"></asp:HyperLink>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="pp_workflowstatus_panel_bg" colspan="3" valign="top" id="PositionProfileStatus_ws_candidateAssociation"
                                                                runat="server">
                                                                <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                                                    <tr>
                                                                        <td colspan="4" align="left" class="pp_workflowstatus_panel_headertext">
                                                                            <asp:Label ID="PositionProfileStatus_candidateAssociate_TitleLabel" runat="server"
                                                                                Text="Candidate Association"></asp:Label>
                                                                        </td>
                                                                        <td width="3%" align="left">
                                                                            <asp:HyperLink ID="PositionProfileStatus_talenScount_candidateAssociation_HyperLink"
                                                                                runat="server" Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/talent_search_icon.gif"
                                                                                ToolTip="Associate Candidates Through intelliSPOT" />
                                                                        </td>
                                                                        <td width="1%" align="right">
                                                                            <asp:HyperLink ID="PositionProfileStatus_ws_candidateAssociation_HyperLink" runat="server"
                                                                                Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_pp_associate_candidate.gif"
                                                                                ToolTip="Associate Candidates" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="6" class="position_profile_dashboard_table_border">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="10%">
                                                                            <asp:Label ID="PositionProfileStatus_candidateAssociate_TaskOwner_TitleLabel" runat="server"
                                                                                Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td width="58%">
                                                                            <div class="pp_dashboard_divtext" id="PositionProfileStatus_candidateAssociate_TaskOwner_Div"
                                                                                runat="server">
                                                                            </div>
                                                                        </td>
                                                                        <td align="left" width="17%">
                                                                            <asp:Label ID="PositionProfileStatus_candidateAssociate_TaskOwner_CandidatesCountLabel"
                                                                                runat="server" Text="Number Of Candidates : " SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td align="left" colspan="3">
                                                                            <asp:HiddenField ID="PositionProfileStatus_candidateAssociate_TaskOwner_CandidatesIdsHiddenField"
                                                                                runat="server" />
                                                                            <asp:LinkButton ID="PositionProfileStatus_candidateAssociate_TaskOwner_CandidatesCountLinkButton"
                                                                                runat="server" Text="" SkinID="sknActionLinkButton" ToolTip="View Associated Candidates"
                                                                                OnClick="PositionProfileStatus_candidateAssociate_TaskOwner_CandidatesCountLinkButton_Click"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="pp_workflowstatus_panel_bg" width="39%" valign="top" id="PositionProfileStatus_ws_interviewCreation"
                                                                runat="server">
                                                                <table cellpadding="1" cellspacing="1" border="0" align="left" width="100%">
                                                                    <tr>
                                                                        <td colspan="2" align="left" class="pp_workflowstatus_panel_headertext">
                                                                            <asp:Label ID="PositionProfileStatus_interviewCreation_titleLabel" runat="server"
                                                                                Text="Interview Creation"></asp:Label>
                                                                        </td>
                                                                        <td width="1%" align="right">
                                                                            <asp:HyperLink ID="PositionProfileStatus_ws_interviewCreation_HyperLink" runat="server"
                                                                                Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/pp_deactive.png" ToolTip="Click here to create interview" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" class="position_profile_dashboard_table_border">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="21%">
                                                                            <asp:Label ID="PositionProfileStatus_interviewCreation_taskTitleLabel" runat="server"
                                                                                Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" colspan="2">
                                                                            <div id="PositionProfileStatus_interviewCreation_ownerDiv" runat="server" class="pp_dashboard_divtext" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="PositionProfileStatus_interviewCreation_interviewTitle_Label" runat="server"
                                                                                Text="Interview :" SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td align="left" colspan="2">
                                                                            <div style="overflow: auto; height: 60px; width: 200px">
                                                                                <asp:DataList ID="PositionProfileStatus_interviewCreation_interviewsDataList" runat="server"
                                                                                    CellSpacing="0" BorderStyle="None" CellPadding="1" RepeatColumns="1" RepeatDirection="Vertical"
                                                                                    Width="100%" OnItemDataBound="PositionProfileStatus_interviewCreation_interviewsDataList_ItemDataBound">
                                                                                    <ItemTemplate>
                                                                                        <table cellpadding="0" cellspacing="0" border="0" align="left">
                                                                                            <tr>
                                                                                                <td align="left">
                                                                                                    <a id="PositionProfileStatus_interviewCreation_interviewName_Href" target="_blank"
                                                                                                        class="position_profile_dashboard" runat="server" title="View Interview">
                                                                                                        <%# Eval("InterviewName") %></a>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_interviewCreation_interviewKey_HiddenField"
                                                                                                        runat="server" Value='<%# Eval("ParentInterviewKey") %>' />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Wrap="true" />
                                                                                </asp:DataList>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="2%">
                                                                &nbsp;
                                                            </td>
                                                            <td class="pp_workflowstatus_panel_bg" width="39%" valign="top" id="PositionProfileStatus_ws_testCreation"
                                                                runat="server">
                                                                <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                                                    <tr>
                                                                        <td colspan="2" align="left" class="pp_workflowstatus_panel_headertext">
                                                                            <asp:Label ID="PositionProfileStatus_testCreation_titleLabel" runat="server" Text="Test Creation"></asp:Label>
                                                                        </td>
                                                                        <td width="1%" align="right">
                                                                            <asp:HyperLink ID="PositionProfileStatus_ws_testCreation_HyperLink" Target="_blank"
                                                                                runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/pp_deactive.png" ToolTip="Click here to create test" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" class="position_profile_dashboard_table_border">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="25%" align="left">
                                                                            <asp:Label ID="PositionProfileStatus_testCreation_ownerTitle_Label" runat="server"
                                                                                Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td align="left" colspan="2">
                                                                            <div id="PositionProfileStatus_testCreation_ownerDiv" runat="server" class="pp_dashboard_divtext" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top">
                                                                            <asp:Label ID="PositionProfileStatus_testCreation_testTitleLabel" runat="server"
                                                                                Text="Test : " SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td align="left" colspan="2" valign="top">
                                                                            <div style="overflow: auto; height: 60px; width: 200px">
                                                                                <asp:DataList ID="PositionProfileStatus_testCreation_testDataList" runat="server"
                                                                                    CellSpacing="0" CellPadding="1" RepeatColumns="1" RepeatDirection="Vertical"
                                                                                    Width="100%" OnItemDataBound="PositionProfileStatus_testCreation_testDataList_ItemDataBound">
                                                                                    <ItemTemplate>
                                                                                        <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                                                                            <tr>
                                                                                                <td width="100%">
                                                                                                    <a id="PositionProfileStatus_testCreation_testName_Href" runat="server" target="_blank"
                                                                                                        class="position_profile_dashboard" title="View Test">
                                                                                                        <%# Eval("Name")%></a>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_testCreation_testKey_HiddenField" runat="server"
                                                                                                        Value='<%# Eval("TestKey") %>' />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Width="60px" Wrap="true" />
                                                                                </asp:DataList>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="pp_workflowstatus_panel_bg" width="39%" valign="top" id="PositionProfileStatus_ws_interviewSession"
                                                                runat="server">
                                                                <table cellpadding="1" cellspacing="1" border="0" align="left" width="100%">
                                                                    <tr>
                                                                        <td colspan="3" align="left" class="pp_workflowstatus_panel_headertext">
                                                                            <asp:Label ID="PositionProfileStatus_interviewSession_TitleLabel1" runat="server"
                                                                                Text="Interview Session Creation"></asp:Label>
                                                                        </td>
                                                                        <td width="1%" align="right">
                                                                            <asp:HyperLink ID="PositionProfileStatus_ws_interviewSession_HyperLink" Target="_blank"
                                                                                runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/pp_deactive.png" ToolTip="Click here to create interview session" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4" class="position_profile_dashboard_table_border">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" width="32%">
                                                                            <asp:Label ID="PositionProfileStatus_interviewSession_taskOwnerTitle_Label1" runat="server"
                                                                                Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td align="left" colspan="2">
                                                                            <div id="PositionProfileStatus_interviewSession_taskOwner_Div" runat="server" class="pp_dashboard_divtext" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top">
                                                                            <asp:Label ID="PositionProfileStatus_interviewSession_countLabel1" runat="server"
                                                                                Text="Number of Sessions :" SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" colspan="2">
                                                                            <div style="overflow: auto; height: 60px; width: 200px">
                                                                                <asp:DataList ID="PositionProfileStatus_interviewSession_DataList" runat="server"
                                                                                    Width="100%" BorderStyle="None" CellSpacing="0" CellPadding="1" RepeatColumns="1"
                                                                                    RepeatDirection="Vertical" OnItemDataBound="PositionProfileStatus_interviewSession_DataList_ItemDataBound">
                                                                                    <ItemTemplate>
                                                                                        <table cellpadding="0" cellspacing="0" border="0" align="left">
                                                                                            <tr>
                                                                                                <td align="left">
                                                                                                    <a id="PositionProfileStatus_interviewSessionCount_Href" runat="server" target="_blank"
                                                                                                        class="position_profile_dashboard" title="View Interview Session"></a>
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_interviewSessionCount_interviewKey_HiddenField"
                                                                                                        runat="server" Value='<%# Eval("InterviewTestKey") %>' />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle BorderStyle="None" Wrap="true" />
                                                                                </asp:DataList>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="2%">
                                                                &nbsp;
                                                            </td>
                                                            <td class="pp_workflowstatus_panel_bg" width="39%" valign="top" id="PositionProfileStatus_ws_testSession"
                                                                runat="server">
                                                                <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                                                    <tr>
                                                                        <td colspan="2" align="left" class="pp_workflowstatus_panel_headertext">
                                                                            <asp:Label ID="PositionProfileStatus_testSession_titleLabel" runat="server" Text="Test Session Creation"></asp:Label>
                                                                        </td>
                                                                        <td width="1%" align="right">
                                                                            <asp:HyperLink ID="PositionProfileStatus_ws_testSession_HyperLink" Target="_blank"
                                                                                runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/pp_deactive.png" ToolTip="Click here to create test session" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" class="position_profile_dashboard_table_border">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="30%">
                                                                            <asp:Label ID="PositionProfileStatus_testSession_taskOwner_titleLabel" runat="server"
                                                                                Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td align="left" colspan="2">
                                                                            <div id="PositionProfileStatus_testSession_ownerDiv" runat="server" class="pp_dashboard_divtext" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_5">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top">
                                                                            <asp:Label ID="PositionProfileStatus_testSession_noofSessionLabel" runat="server"
                                                                                Text="Number of Sessions : " SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td align="left" colspan="2" valign="top">
                                                                            <div style="overflow: auto; width: 180px; height: 60px">
                                                                                <asp:DataList ID="PositionProfileStatus_testSession_DataList" runat="server" CellSpacing="0"
                                                                                    CellPadding="0" RepeatColumns="1" RepeatDirection="Vertical" Width="100%" OnItemDataBound="PositionProfileStatus_testSession_DataList_ItemDataBound">
                                                                                    <ItemTemplate>
                                                                                        <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <a id="PositionProfileStatus_testSession_testCount_Href" runat="server" target="_blank"
                                                                                                        class="position_profile_dashboard" title="View Test Session"></a>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_testSession_sessionKey_HiddenField" runat="server"
                                                                                                        Value='<%# Eval("SessionKey") %>' />
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_testSession_testKey_HiddenField" runat="server"
                                                                                                        Value='<%# Eval("TestKey") %>' />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Wrap="true" />
                                                                                </asp:DataList>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="pp_workflowstatus_panel_bg" width="39%" valign="top" id="PositionProfileStatus_ws_interviewSchedule"
                                                                runat="server">
                                                                <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                                                    <tr>
                                                                        <td colspan="2" align="left" class="pp_workflowstatus_panel_headertext">
                                                                            <asp:Label ID="PositionProfileStatus_interviewScheduling_TitleLabel" runat="server"
                                                                                Text="Interview Scheduling"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="position_profile_dashboard_table_border">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="20%">
                                                                            <asp:Label ID="PositionProfileStatus_interviewScheduling_taskOwner_titleLabel" runat="server"
                                                                                Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <div id="PositionProfileStatus_interviewScheduling_taskOwnerDiv" runat="server" class="pp_dashboard_divtext" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="70%" align="center">
                                                                                <tr>
                                                                                    <td valign="top" align="center">
                                                                                        <asp:Label ID="PositionProfileStatus_interviewScheduling_scheduleTitle_Label" runat="server"
                                                                                            Text="Scheduled" SkinID="sknHomePageLabel"></asp:Label>
                                                                                    </td>
                                                                                    <td width="20%">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td valign="top" align="center">
                                                                                        <asp:Label ID="PositionProfileStatus_interviewScheduling_completedTitleLabel" runat="server"
                                                                                            Text="Completed" SkinID="sknHomePageLabel"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_height_5">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top" align="center" colspan="3" width="100%">
                                                                                        <div style="overflow: auto; height: 60px; width: 221px">
                                                                                            <asp:DataList ID="PositionProfileStatus_interviewScheduling_scheduleDataList" runat="server"
                                                                                                Width="100%" CellSpacing="0" CellPadding="0" RepeatColumns="1" RepeatDirection="Vertical"
                                                                                                OnItemCommand="PositionProfileStatus_interviewScheduling_scheduleDataList_ItemCommand">
                                                                                                <ItemTemplate>
                                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                                                                                                        <tr>
                                                                                                            <td align="center" width="40%">
                                                                                                                <asp:LinkButton ID="PositionProfileStatus_interviewSessionCount_scheduledLinkButton"
                                                                                                                    runat="server" Text='<%# Eval("ScheduledInterviews")%>' SkinID="sknActionLinkButton"
                                                                                                                    CssClass="position_profile_dashboard" CommandName="SCHEDULEDINT" ToolTip="View Interview Scheduled/Schedule Candidate"></asp:LinkButton>
                                                                                                                <asp:HiddenField ID="PositionProfileStatus_interviewSessionCount_scheduledInterviewKey_HiddenField"
                                                                                                                    runat="server" Value='<%# Eval("InterviewTestID") %>' />
                                                                                                                <asp:HiddenField ID="PositionProfileStatus_interviewSessionCount_scheduledPositionIds_HiddenField"
                                                                                                                    runat="server" Value='<%# Eval("ClientRequestID") %>' />
                                                                                                            </td>
                                                                                                            <td width="20%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td align="center" width="40%">
                                                                                                                <asp:LinkButton ID="PositionProfileStatus_interviewSessionCount_completedLinkButton"
                                                                                                                    runat="server" Text='<%# Eval("CompletedInterviews")%>' SkinID="sknActionLinkButton"
                                                                                                                    CssClass="position_profile_dashboard" CommandName="COMPLETEDINT" ToolTip="View Interview Completed"></asp:LinkButton>
                                                                                                                <asp:HiddenField ID="PositionProfileStatus_interviewSessionCount_completedInterviewKey_HiddenField"
                                                                                                                    runat="server" Value='<%# Eval("InterviewTestID") %>' />
                                                                                                                <asp:HiddenField ID="PositionProfileStatus_interviewSessionCount_completedPositionIds_HiddenField"
                                                                                                                    runat="server" Value='<%# Eval("ClientRequestID") %>' />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle BorderStyle="None" Wrap="true" />
                                                                                            </asp:DataList>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="2%">
                                                                &nbsp;
                                                            </td>
                                                            <td class="pp_workflowstatus_panel_bg" width="39%" valign="top" id="PositionProfileStatus_ws_testSchedule"
                                                                runat="server">
                                                                <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                                                    <tr>
                                                                        <td colspan="2" align="left" class="pp_workflowstatus_panel_headertext">
                                                                            <asp:Label ID="PositionProfileStatus_testSchedule_titleLabel" runat="server" Text="Test Scheduling"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="position_profile_dashboard_table_border">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="20%" align="left">
                                                                            <asp:Label ID="PositionProfileStatus_testSchedule_taskOwner_titleLabel" runat="server"
                                                                                Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <div id="PositionProfileStatus_testSchedule_taskOwnerDiv" runat="server" class="pp_dashboard_divtext" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_5">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" align="center" width="100%">
                                                                            <table cellpadding="0" cellspacing="0" border="0" align="center" width="70%">
                                                                                <tr>
                                                                                    <td valign="top" align="center">
                                                                                        <asp:Label ID="PositionProfileStatus_testSchedule_scheduledLabel" runat="server"
                                                                                            Text="Scheduled" SkinID="sknHomePageLabel"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td valign="top" align="center">
                                                                                        <asp:Label ID="PositionProfileStatus_testSchedule_completedLabel" runat="server"
                                                                                            Text="Completed" SkinID="sknHomePageLabel"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_height_2">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top" align="center" colspan="3">
                                                                                        <div style="overflow: auto; width: 230px; height: 60px">
                                                                                            <asp:DataList ID="PositionProfileStatus_testSchedule_scheduledDataList" runat="server"
                                                                                                Width="100%" CellSpacing="0" CellPadding="0" RepeatColumns="1" RepeatDirection="Vertical"
                                                                                                OnItemCommand="PositionProfileStatus_testSchedule_scheduledDataList_ItemCommand">
                                                                                                <ItemTemplate>
                                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                                                                                                        <tr>
                                                                                                            <td align="center">
                                                                                                                <asp:LinkButton ID="PositionProfileStatus_testSchedule_scheduledLinkButton" runat="server"
                                                                                                                    Text='<%# Eval("ScheduledTests")%>' SkinID="sknActionLinkButton" CssClass="position_profile_dashboard"
                                                                                                                    CommandName="SCHEDULEDTST" ToolTip="View Test Schedule/Schedule Candidate"></asp:LinkButton>
                                                                                                                <asp:HiddenField ID="PositionProfileStatus_testSchedule_scheduledPositionIds_HiddenField"
                                                                                                                    runat="server" Value='<%# Eval("ClientRequestID") %>' />
                                                                                                            </td>
                                                                                                            <td width="15px">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:LinkButton ID="PositionProfileStatus_testSchedule_completedLinkButton" runat="server"
                                                                                                                    Text='<%# Eval("CompletedTests")%>' SkinID="sknActionLinkButton" CssClass="position_profile_dashboard"
                                                                                                                    CommandName="COMPLETEDTST" ToolTip="View Test Completed"></asp:LinkButton>
                                                                                                                <asp:HiddenField ID="PositionProfileStatus_testSchedule_completedPositionIds_HiddenField"
                                                                                                                    runat="server" Value='<%# Eval("ClientRequestID") %>' />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle BorderStyle="None" Wrap="true" />
                                                                                            </asp:DataList>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="pp_workflowstatus_panel_bg" width="39%" valign="top" id="PositionProfileStatus_ws_interviewAssessment"
                                                                runat="server">
                                                                <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                                                    <tr>
                                                                        <td colspan="2" align="left" class="pp_workflowstatus_panel_headertext">
                                                                            <asp:Label ID="PositionProfileStatus_interviewAssessment_TitleLabel" runat="server"
                                                                                Text="Interview Assessment"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="position_profile_dashboard_table_border">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="33%">
                                                                            <asp:Label ID="PositionProfileStatus_interviewAssessment_TaskOwner_TitleLabel" runat="server"
                                                                                Text="Task Owner : " SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <div id="PositionProfileStatus_interviewAssessment_OwnerDiv" runat="server" class="pp_dashboard_divtext" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_2">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top">
                                                                            <asp:Label ID="PositionProfileStatus_interviewAssessment_AssignedCandidate_TitleLabel"
                                                                                runat="server" Text="Assessed Candidates : " SkinID="sknHomePageLabel"></asp:Label>
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <div style="overflow: auto; height: 60px; width: 200px">
                                                                                <asp:DataList ID="PositionProfileStatus_InterviewAssessmentDataList" runat="server"
                                                                                    Width="100%" BorderStyle="None" CellSpacing="0" CellPadding="1" RepeatColumns="1"
                                                                                    RepeatDirection="Vertical" OnItemDataBound="PositionProfileStatus_InterviewAssessmentDataList_ItemDataBound">
                                                                                    <ItemTemplate>
                                                                                        <table cellpadding="0" cellspacing="0" border="0" align="left">
                                                                                            <tr>
                                                                                                <td align="left">
                                                                                                    <asp:HyperLink ID="PositionProfileStatus_interviewAssessment_CandidateCountHyperLink"
                                                                                                        ToolTip="View Assessed Candidates" runat="server" SkinID="sknLabelFieldTextHyperLink">
                                                                                                    </asp:HyperLink>
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_InterviewAssessment_SessionKey_HiddenField"
                                                                                                        runat="server" Value='<%# Eval("InterviewTestKey") %>' />
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_InterviewAssessment_InterviewKey_HiddenField"
                                                                                                        runat="server" Value='<%# Eval("ParentInterviewKey") %>' />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle BorderStyle="None" Wrap="true" />
                                                                                </asp:DataList>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="2%">
                                                                &nbsp;
                                                            </td>
                                                            <td width="39%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="pp_workflowstatus_panel_bg" colspan="3" valign="top" id="PositionProfileStatus_ws_submitClient"
                                                                runat="server">
                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                    <tr>
                                                                        <td colspan="4" class="pp_workflowstatus_panel_headertext" align="center">
                                                                            <asp:Label ID="PositionProfileStatus_submittalClient_titleLabel" runat="server" Text="Submittal to Client"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4" class="position_profile_dashboard_table_border">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="10%">
                                                                            <asp:Label ID="PositionProfileStatus_submittalClient_taskOwner_titleLabel" runat="server"
                                                                                SkinID="sknHomePageLabel" Text="Task Owner : "></asp:Label>
                                                                        </td>
                                                                        <td width="55%">
                                                                            <div id="PositionProfileStatus_submittalClient_ownerDiv" runat="server" class="pp_dashboard_divtext" />
                                                                        </td>
                                                                        <td width="20%">
                                                                            <asp:Label ID="PositionProfileStatus_submittalClient_candidate_titleLabel" runat="server"
                                                                                SkinID="sknHomePageLabel" Text="Submitted Candidates :"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:LinkButton ID="PositionProfileStatus_submittalClient_candidateCount_LinkButton"
                                                                                runat="server" SkinID="sknActionLinkButton" OnClick="PositionProfileStatus_submittalClient_candidateCount_LinkButton_Click"
                                                                                ToolTip="View Submittal Candidates"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_8">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" style="padding: 3px">
                                    <table cellpadding="0" cellspacing="0" border="0" width="940px">
                                        <tr id="PositionProfileStatus_associatedCandidatesTR" runat="server">
                                            <td class="header_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 98%" align="left" class="header_text_bold">
                                                            <asp:Literal ID="PositionProfileStatus_associatedCandidates_Literal" runat="server"
                                                                Text="Associated Candidates"></asp:Literal>
                                                        </td>
                                                        <td style="width: 2%" align="right">
                                                            <span id="PositionProfileStatus_associatedCandidates_upSpan" runat="server" style="display: block;">
                                                                <asp:Image ID="PositionProfileStatus_associatedCandidates_upImage" runat="server"
                                                                    SkinID="sknMinimizeImage" />
                                                            </span><span id="PositionProfileStatus_associatedCandidates_downSpan" runat="server"
                                                                style="display: none;">
                                                                <asp:Image ID="PositionProfileStatus_associatedCandidates_downImage" runat="server"
                                                                    SkinID="sknMaximizeImage" />
                                                            </span>
                                                            <asp:HiddenField ID="PositionProfileStatus_associatedCandidates_restoreHiddenField"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 50%" valign="top" class="grid_border">
                                                <div runat="server" id="PositionProfileStatus_associatedCandidates_Div" visible="true">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                <ContentTemplate>--%>
                                                            <td class="panel_bg">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                    <tr>
                                                                        <td width="80%" style="padding-left: 7px">
                                                                            <asp:CheckBox ID="PositionProfileStatus_showOnlyMy_Candidates_CheckBox" SkinID="sknMyRecordCheckBox"
                                                                                Text="Show Only My Candidates" TextAlign="Right" runat="server" AutoPostBack="true"
                                                                                OnCheckedChanged="PositionProfileStatus_showOnlyMy_Candidates_CheckBox_CheckedChanged" />
                                                                            <asp:ImageButton ID="PositionProfileStatus_showOnlyMy_Candidates_HelpImageButton"
                                                                                SkinID="sknHelpImageButton" runat="server" ToolTip="Check this to show only the user candidates"
                                                                                ImageAlign="Top" OnClientClick="javascript:return false;" />
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_5">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 80%">
                                                                            <asp:RadioButtonList ID="PositionProfileStatus_showCandidatesRadioButtonList" runat="server"
                                                                                RepeatDirection="Horizontal" AutoPostBack="true" CssClass="radio_button_text"
                                                                                OnSelectedIndexChanged="PositionProfileStatus_showCandidates_SelectedIndexChanged">
                                                                                <asp:ListItem Text="Associated" Value="PCS_ASS" Selected="True"></asp:ListItem>
                                                                                <asp:ListItem Text="Submitted" Value="PCS_SUB"></asp:ListItem>
                                                                                <asp:ListItem Text="Rejected" Value="PCS_REJ"></asp:ListItem>
                                                                                <asp:ListItem Text="Hired" Value="PCS_HIR"></asp:ListItem>
                                                                                <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                        <td style="width: 20%" align="right">
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        Sort By
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <asp:DropDownList ID="PositionProfileStatus_orderbyDropdownList" runat="server" AutoPostBack="true"
                                                                                            OnSelectedIndexChanged="PositionProfileStatus_orderbyDropdownList_SelectedIndexChanged">
                                                                                            <asp:ListItem Text="--Select--" Value="NONE"></asp:ListItem>
                                                                                            <asp:ListItem Text="Test Status" Value="TEST_STATUS"></asp:ListItem>
                                                                                            <asp:ListItem Text="Offline Interview Status" Value="INTERVIEW_STATUS"></asp:ListItem>
                                                                                            <asp:ListItem Text="Online Interview Status" Value="ONLINE_INTERVIEW_STATUS"></asp:ListItem>
                                                                                            <asp:ListItem Text="Associated By" Value="ASSOCIATED_BY"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <%--  </ContentTemplate>
                                                                </asp:UpdatePanel>--%>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <uc1:pageNavigator ID="PositionProfileStatus_candidatesPageNavigator" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="PositionProfileStatus_resultsUpdatePanel" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:GridView ID="PositionProfileStatus_candidatesGridView" runat="server" SkinID="sknNewGridView"
                                                                            OnRowCommand="PositionProfileStatus_candidatesGridView_RowCommand" OnRowDataBound="PositionProfileStatus_candidatesGridView_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                                                            <tr>
                                                                                                <td class="td_height_2">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td rowspan="2" valign="top" width="7%">
                                                                                                    <asp:Image ID="PositionProfileStatus_candidatesGridViewCandidateImage" runat="server"
                                                                                                        AlternateText="Photo not available" />
                                                                                                </td>
                                                                                                <td valign="top" style="width: 1%" align="left">
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_candidatesGridViewCaiIDHiddenField" runat="server"
                                                                                                        Value='<%# Eval("caiID") %>' />
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_candidatesGridViewCandidateLoginIDHiddenField"
                                                                                                        runat="server" Value='<%# Eval("CandidateLoginID") %>' />
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_candidatesGridViewPPStatusHiddenField"
                                                                                                        runat="server" Value='<%# Eval("PositionProfileStatusID") %>' />
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_candidatesGridViewInterviewCandidateSessionIDHiddenField"
                                                                                                        runat="server" Value='<%# Eval("InterviewCandidateSessionID") %>' />
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_candidatesGridViewInterviewAttemptIDHiddenField"
                                                                                                        runat="server" Value='<%# Eval("InterviewAttemptID") %>' />
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_candidatesGridViewCandidateNameHiddenField"
                                                                                                        runat="server" Value='<%# Eval("caiFirstName") %>' />
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_candidatesGridViewOnlineInterviewKeyHiddenField"
                                                                                                        runat="server" Value='<%# Eval("OnlineInterviewKey") %>' />
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_candidatesGridViewChatRoomkeyHiddenField"
                                                                                                        runat="server" Value='<%# Eval("ChatRoomKey") %>' />
                                                                                                    <asp:HiddenField ID="PositionProfileStatus_candidatesGridViewCandidateInterviewIDHiddenField"
                                                                                                        runat="server" Value='<%# Eval("CandidateOnlineInterviewID") %>' />
                                                                                                </td>
                                                                                                <td valign="top" style="width: 25%">
                                                                                                    <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:HyperLink ID="PositionProfileStatus_candidatesGridViewCandidateNameLabel" SkinID="sknOrangeBoldHyperLink"
                                                                                                                    runat="server" Text='<%# Eval("caiFirstName") %>' ToolTip="Candidate Name" Target="_blank"></asp:HyperLink>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="PositionProfileStatus_candidatesGridViewCandidateAddressLabel" runat="server"
                                                                                                                    SkinID="sknLabelFieldMultiText" Text='<%# Eval("caiAddress") %>' ToolTip="Address"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="PositionProfileStatus_candidatesGridViewCandidateZipcodeLabel" runat="server"
                                                                                                                    SkinID="sknLabelFieldMultiText" Text='<%# Eval("caiZip") %>' ToolTip="Zipcode"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="PositionProfileStatus_candidatesGridViewCandidatePhoneNoLabel" runat="server"
                                                                                                                    SkinID="sknLabelFieldText" Text='<%# Eval("caiCell") %>' ToolTip="Phone No"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <asp:Label ID="PositionProfileStatus_candidatesGridViewRecruitedByHeadLabel" runat="server"
                                                                                                                                SkinID="sknLabelFieldHeaderText" Text="Associated By"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <asp:Label ID="PositionProfileStatus_candidatesGridViewRecruitedByLabel" runat="server"
                                                                                                                                SkinID="sknLabelFieldText" Text='<%# Eval("RecruiterName") %>'></asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td valign="top" style="width: 67%">
                                                                                                    <table width="100%" cellpadding="2" cellspacing="2" border="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table width="100%" cellpadding="2" cellspacing="2" border="0">
                                                                                                                    <tr>
                                                                                                                        <td style="width: 16%">
                                                                                                                            <asp:Label ID="PositionProfileStatus_candidatesGridViewTestScoreHeaderLabel" runat="server"
                                                                                                                                Text="Test Score" SkinID="sknLabelText"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="width: 40%">
                                                                                                                            <asp:Label ID="PositionProfileStatus_candidatesGridViewTestScoreLabel" runat="server"
                                                                                                                                Text='<%# Eval("TestScore") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                            &nbsp;
                                                                                                                            <asp:ImageButton ID="PositionProfileStatus_candidatesGridViewTestScoreDetailImageButton"
                                                                                                                                runat="server" SkinID="sknCandidateDetailImageButton" ToolTip="Test Score" CommandName="testscore"
                                                                                                                                Visible='<%# Eval("IsTestScore") %>' CommandArgument='<%# Eval("candidateSessionID")+ "," + Eval("TestKey")+ "," + Eval("AttemptID")%>' />
                                                                                                                        </td>
                                                                                                                        <td style="width: 1%">
                                                                                                                        </td>
                                                                                                                        <td style="width: 43%">
                                                                                                                            <asp:UpdatePanel ID="PositionProfileStatus_candidatesGridView_statusUpdatePanel"
                                                                                                                                runat="server" UpdateMode="Conditional">
                                                                                                                                <ContentTemplate>
                                                                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                                        <tr>
                                                                                                                                            <td align="left" width="19%">
                                                                                                                                                <asp:Label ID="PositionProfileStatus_printCandidatesGridViewStatusHeaderLabel" runat="server"
                                                                                                                                                    Text="Status" SkinID="sknLabelText"></asp:Label>
                                                                                                                                            </td>
                                                                                                                                            <td style="padding-left: 1px" width="38%">
                                                                                                                                                <asp:DropDownList ID="PositionProfileStatus_positionStatusGridView_DropDownList"
                                                                                                                                                    Visible="false" runat="server" SkinID="sknAssessorDropDownList" AutoPostBack="true"
                                                                                                                                                    Width="80px" OnSelectedIndexChanged="PositionProfileStatus_positionStatusGridView_DropDownList_SelectedIndexChanged">
                                                                                                                                                </asp:DropDownList>
                                                                                                                                                <asp:HiddenField ID="PositionProfileStatus_positionStatusGridView_statusHiddenField"
                                                                                                                                                    runat="server" Value='<%# Eval("CandidateStatus") %>' />
                                                                                                                                                <asp:Label ID="PositionProfileStatus_positionStatusGridView_statusLabel" SkinID="sknLabelAssessorFieldscoreText"
                                                                                                                                                    runat="server" Text='<%# Eval("CandidateStatus") %>'></asp:Label>
                                                                                                                                            </td>
                                                                                                                                            <td width="25%">
                                                                                                                                                <asp:Button ID="PositionProfileStatus_printCandidatesGridViewStatus_saveButton" SkinID="sknButtonId"
                                                                                                                                                    runat="server" Text="Save" Visible="false" OnClick="PositionProfileStatus_candidateStatusSaveButton_Click"
                                                                                                                                                    ToolTip="Click here to save the candidate status" />
                                                                                                                                            </td>
                                                                                                                                            <td width="18%">
                                                                                                                                                <asp:LinkButton ID="PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton"
                                                                                                                                                    runat="server" Visible="false" OnClick="PositionProfileStatus_statusCancelLinkButton_Click"
                                                                                                                                                    ToolTip="Click here to cancel the candidate status">Cancel</asp:LinkButton>
                                                                                                                                            </td>
                                                                                                                                            <div style="display: none">
                                                                                                                                                <asp:Button Visible="false" ID="PositionProfileStatus_candidatesGridView_submitToCPXButton" Text="Submit To CPX"
                                                                                                                                                    runat="server" SkinID="sknButtonId" OnClick="PositionProfileStatus_candidatesGridView_submitToCPXButton_Click" />
                                                                                                                                            </div>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                </ContentTemplate>
                                                                                                                                <Triggers>
                                                                                                                                    <asp:AsyncPostBackTrigger ControlID="PositionProfileStatus_candidateStatus_editLinkButton" />
                                                                                                                                    <asp:AsyncPostBackTrigger ControlID="PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton" />
                                                                                                                                </Triggers>
                                                                                                                            </asp:UpdatePanel>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <asp:Label ID="PositionProfileStatus_candidatesGridViewInterviewScoreHeaderLabel"
                                                                                                                                runat="server" Text="Offline Interview Score" SkinID="sknLabelText"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td colspan="2">
                                                                                                                            <asp:ImageButton ID="PositionProfileStatus_candidatesGridViewCandidateRatingSummaryImageButton"
                                                                                                                                runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif"
                                                                                                                                ToolTip="Candidate Rating Summary" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                                                CommandName="CandidateSummary" Visible="false" />
                                                                                                                            <asp:HyperLink ID="PositionProfileStatus_candidatesGridViewCandidateRatingSummaryHyperLink"
                                                                                                                                ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif"
                                                                                                                                runat="server" Target="_blank" ToolTip="Candidate Rating Summary" Visible='<%# Eval("InterviewCandidateSessionID") != null &&  Eval("InterviewCandidateSessionID") != string.Empty %>'>
                                                                                                                            </asp:HyperLink>
                                                                                                                            &nbsp;<asp:Label ID="PositionProfileStatus_candidatesGridViewInterviewTotalWeightedScoreLabel"
                                                                                                                                runat="server" Text="Total Weighted Score" SkinID="sknLabelText"></asp:Label>
                                                                                                                            &nbsp;<asp:Label ID="PositionProfileStatus_candidatesGridViewInterviewTotalWeightedScoreValueLabel"
                                                                                                                                SkinID="sknLabelFieldTextBold" runat="server" Text='<%# Eval("InterviewTotalWeightedScore") %>'
                                                                                                                                ToolTip="Total Weighted Score"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td rowspan="3" valign="top" align="left">
                                                                                                                            <asp:UpdatePanel ID="PositionProfileStatus_candidatesGridView_statusCommentsUpdatePanel"
                                                                                                                                runat="server" UpdateMode="Conditional">
                                                                                                                                <ContentTemplate>
                                                                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                                                                        <tr>
                                                                                                                                            <td valign="top">
                                                                                                                                                <asp:Label ID="PositionProfileStatus_positionStatusGridView_statusComments_Label"
                                                                                                                                                    runat="server" Text="Comments" SkinID="sknLabelText" Visible="false"></asp:Label>
                                                                                                                                            </td>
                                                                                                                                            <td>
                                                                                                                                                &nbsp;
                                                                                                                                            </td>
                                                                                                                                            <td valign="top">
                                                                                                                                                <asp:Label ID="PositionProfileStatus_positionStatusGridView_CommentsLabel" Visible="false"
                                                                                                                                                    runat="server" SkinID="sknLabelAnswerText" Text='<%# Eval("CandidateStatusComments") %>'></asp:Label>
                                                                                                                                                <asp:TextBox ID="PositionProfileStatus_positionStatusGridView_statusComments_TextBox"
                                                                                                                                                    Height="45px" Width="160px" Visible="false" runat="server" MaxLength="100" TextMode="MultiLine"
                                                                                                                                                    Text='<%# Eval("CandidateStatusComments") %>' ToolTip="Enter comments here about the selected candidate status option"
                                                                                                                                                    onkeyup="CommentsCount(100,this)" onchange="CommentsCount(100,this)"> </asp:TextBox>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                </ContentTemplate>
                                                                                                                                <Triggers>
                                                                                                                                    <asp:AsyncPostBackTrigger ControlID="PositionProfileStatus_positionStatusGridView_DropDownList" />
                                                                                                                                    <asp:AsyncPostBackTrigger ControlID="PositionProfileStatus_printCandidatesGridViewStatus_saveButton" />
                                                                                                                                    <%--<asp:AsyncPostBackTrigger ControlID ="PositionProfileStatus_printCandidatesGridViewStatus_cancelLinkButton" />--%>
                                                                                                                                </Triggers>
                                                                                                                            </asp:UpdatePanel>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <asp:Label ID="PositionProfileStatus_candidatesGridViewOnlineInterviewScoreHeaderLabel"
                                                                                                                                runat="server" Text="Online Interview Score" SkinID="sknLabelText"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td colspan="2">
                                                                                                                            <asp:HyperLink ID="PositionProfileStatus_candidatesGridViewCandidateInterviewResponseSummaryHyperLink"
                                                                                                                                ImageUrl="~/App_Themes/DefaultTheme/Images/candidate_test_details_icons.gif"
                                                                                                                                runat="server" Target="_blank" ToolTip="Assessor Rating Summary" Visible='<%# Eval("OnlineInterviewStatus") != null && Eval("OnlineInterviewStatus") == "Completed" %>'>
                                                                                                                            </asp:HyperLink>
                                                                                                                            &nbsp;<asp:Label ID="PositionProfileStatus_candidatesGridViewOnlineInterviewTotalWeightedScoreLabel"
                                                                                                                                runat="server" Text="Total Weighted Score" SkinID="sknLabelText"></asp:Label>
                                                                                                                            &nbsp;<asp:Label ID="PositionProfileStatus_candidatesGridViewOnlineInterviewTotalWeightedScoreValueLabel"
                                                                                                                                SkinID="sknLabelFieldTextBold" runat="server" Text='<%# Eval("OnlineInterviewTotalScore") %>'
                                                                                                                                ToolTip="Total Weighted Score"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td rowspan="3" valign="top" align="left">&nbsp;</td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <asp:Label ID="PositionProfileStatus_candidatesGridViewTestStatusHeaderLabel" runat="server"
                                                                                                                                Text="Test Status" SkinID="sknLabelText"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td colspan="2">
                                                                                                                            <asp:Label ID="PositionProfileStatus_candidatesGridViewTestStatusLabel" runat="server"
                                                                                                                                Text='<%# Eval("Status") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <asp:Label ID="PositionProfileStatus_candidatesGridViewInterviewStatusHeaderLabel"
                                                                                                                                runat="server" Text="Offline Interview Status" SkinID="sknLabelText"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td colspan="2">
                                                                                                                            <asp:Label ID="PositionProfileStatus_candidatesGridViewInterviewStatusLabel" runat="server"
                                                                                                                                Text='<%# Eval("InterviewStatus") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                     <tr>
                                                                                                                        <td>
                                                                                                                            <asp:Label ID="PositionProfileStatus_candidatesGridViewOnlineInterviewStatusHeaderLabel"
                                                                                                                                runat="server" Text="Online Interview Status" SkinID="sknLabelText"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td colspan="2">
                                                                                                                            <asp:Label ID="PositionProfileStatus_candidatesGridViewOnlineInterviewStatusLabel" runat="server"
                                                                                                                                Text='<%# Eval("OnlineInterviewStatus") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4" align="right">
                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                        <tr>
                                                                                                            <td align="left">
                                                                                                                <asp:CheckBox ID="PositionProfileStatus_candidatesGridViewSelectCandidateCheckbox"
                                                                                                                    runat="server" />
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:LinkButton ID="PositionProfileStatus_candidatesGridView_addDocumentLinkButton"
                                                                                                                    SkinID="sknAddLinkButton" runat="server" ToolTip="Click here to add/view the additional documents">Add/View Document</asp:LinkButton>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                            <td align="right">
                                                                                                                <asp:LinkButton ID="PositionProfileStatus_candidateStatus_editLinkButton" runat="server"
                                                                                                                    SkinID="sknLabelFieldTextHyperLink" ToolTip="Click here to edit the candidate status"
                                                                                                                    OnClick="PositionProfileStatus_candidateStatusLinkButton_Click">Edit Candidate Status</asp:LinkButton>&nbsp;
                                                                                                                <asp:ImageButton ID="PositionProfileStatus_publishInterviewImageButton" runat="server"
                                                                                                                    ImageUrl="~/App_Themes/DefaultTheme/Images/publish.png" ToolTip="Publish Candidate Interview Response"
                                                                                                                    Width="18px" Height="18px" CommandArgument='<%# Container.DataItemIndex %>' CommandName="showpublish"
                                                                                                                    Visible='<%# Eval("InterviewStatus") == "Completed" %>' />
                                                                                                                <asp:ImageButton ID="PositionProfileStatus_candidatesGridViewSkillDetailsImageButton"
                                                                                                                    runat="server" SkinID="sknSkillDetailsImageButton" ToolTip="Skill Details" CommandName="skilldetails"
                                                                                                                    CommandArgument='<%# Eval("caiID") %>' />
                                                                                                                <asp:ImageButton ID="PositionProfileStatus_candidatesGridViewActivityLogImageButton"
                                                                                                                    runat="server" SkinID="sknSkillActivityLogImageButton" ToolTip="View Candidate Activity Log"
                                                                                                                    CommandName="noteslog" CommandArgument='<%# Eval("caiID") %>' />
                                                                                                                <asp:ImageButton ID="PositionProfileStatus_candidatesGridViewViewResumeImageButton"
                                                                                                                    runat="server" SkinID="sknSkillViewResumeImageButton" ToolTip="Download Resume"
                                                                                                                    CommandName="resumedownload" CommandArgument='<%# Eval("caiID") %>' />
                                                                                                                <%--  <asp:ImageButton ID="PositionProfileStatus_candidatesGridViewEmailImageButton" runat="server"
                                                                                                    SkinID="sknMailImageButton" ToolTip="Email" CommandName="email" CommandArgument='<%# Eval("caiID") %>' />
                                                                                                <asp:ImageButton ID="PositionProfileStatus_candidatesGridViewDownloadPDFImageButton"
                                                                                                    runat="server" SkinID="sknPdfImageButton" ToolTip="Download PDF" CommandName="download"
                                                                                                    CommandArgument='<%# Eval("caiID") %>' />--%>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="PositionProfileStatus_showPublishUrlUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <div style="float: left;">
                                                                            <asp:Panel ID="PositionProfileStatus_emailConfirmationPanel" runat="server" Style="display: none;
                                                                                height: 254px;" CssClass="popupcontrol_confirm_publish_candidate_interview_response">
                                                                                <div id="PositionProfileStatus_emailConfirmationDiv" style="display: none">
                                                                                    <asp:Button ID="PositionProfileStatus_emailConfirmation_hiddenButton" runat="server" />
                                                                                </div>
                                                                                <uc2:ConfirmInterviewScoreMsgControl ID="PositionProfileStatus_emailConfirmation_ConfirmMsgControl"
                                                                                    runat="server" Title="Publish Candidate Interview Response" Type="InterviewScoreConfirmType" />
                                                                            </asp:Panel>
                                                                            <ajaxToolKit:ModalPopupExtender ID="PositionProfileStatus_emailConfirmation_ModalPopupExtender"
                                                                                BehaviorID="PositionProfileStatus_emailConfirmation_ModalPopupExtender" runat="server"
                                                                                PopupControlID="PositionProfileStatus_emailConfirmationPanel" TargetControlID="PositionProfileStatus_emailConfirmation_hiddenButton"
                                                                                BackgroundCssClass="modalBackground">
                                                                            </ajaxToolKit:ModalPopupExtender>
                                                                        </div>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel runat="server" ID="PositionProfileStatus_bottomMessageUpdatePanel">
                            <ContentTemplate>
                                <asp:Label ID="PositionProfileStatus_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                <asp:Label ID="PositionProfileStatus_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="header_bg">
                        <table width="100%" border="0" cellspacing="2" cellpadding="0">
                            <tr>
                                <td width="72%">
                                </td>
                                <td width="28%" align="right">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="PositionProfileEntry_bottomDownloadImageButton" runat="server"
                                                    ToolTip="Export To Excel" SkinID="sknExportExcelImageButton" CommandName="Export"
                                                    OnClick="PositionProfileEntry_exportToExcelImageButton_Click" />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="PositionProfileEntry_bottomActivityImageButton" runat="server"
                                                    SkinID="sknSkillViewPPActivityImageButton" ToolTip="Position Profile Activity"
                                                    CommandName="Activity" Visible="true" OnClick="PositionProfileEntry_activityImageButton_Click" />
                                            </td>
                                            <td>
                                                <asp:Button ID="PositionProfileEntry_bottomEmailButton" runat="server" Text="Submit To Client"
                                                    Visible="false" ToolTip="Submit To Client" SkinID="sknButtonId" OnClick="PositionProfileEntry_emailButton_Click" />
                                            </td>
                                            <td>
                                                <asp:Button ID="PositionProfileStatus_bottomEmail_InternalButton" runat="server"
                                                    Text="Internal Submittal" Visible="false" ToolTip="Internal Submittal" SkinID="sknButtonId"
                                                    OnClick="PositionProfileStatus_emailInternalButton_Click" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="PositionProfileStatus_bottomReset" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="PositionProfileStatus_resetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td>
                                                |
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="PositionProfileStatus_bottomCancel" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="PositionProfileStatus_printCandidatesGridViewPanel" runat="server">
                            <asp:GridView ID="PositionProfileStatus_printCandidatesGridView" runat="server" SkinID="sknNewGridView"
                                Visible="false" OnRowDataBound="PositionProfileStatus_printCandidatesGridView_RowDataBound">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                <tr>
                                                    <td valign="top" style="width: 8%" align="center">
                                                        <asp:Image ID="PositionProfileStatus_printCandidatesGridViewCandidateImage" runat="server"
                                                            AlternateText="Photo not available" />
                                                        <asp:HiddenField ID="PositionProfileStatus_printCandidatesGridViewCaiIDHiddenField"
                                                            runat="server" Value='<%# Eval("caiID") %>' Visible="false" />
                                                    </td>
                                                    <td valign="top" style="width: 20%">
                                                        <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="PositionProfileStatus_printCandidatesGridViewCandidateNameLabel" SkinID="sknLabelFieldTextBold"
                                                                        runat="server" Text='<%# Eval("caiFirstName") %>' ToolTip="Candidate Name"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="PositionProfileStatus_printCandidatesGridViewCandidateAddressLabel"
                                                                        runat="server" SkinID="sknLabelFieldMultiText" Text='<%# Eval("caiAddress") %>'
                                                                        ToolTip="Address"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="PositionProfileStatus_printCandidatesGridViewCandidateZipcodeLabel"
                                                                        runat="server" SkinID="sknLabelFieldMultiText" Text='<%# Eval("caiZip") %>' ToolTip="Zipcode"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="PositionProfileStatus_printCandidatesGridViewCandidatePhoneNoLabel"
                                                                        runat="server" SkinID="sknLabelFieldText" Text='<%# Eval("caiCell") %>' ToolTip="Phone No"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_20">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="PositionProfileStatus_printCandidatesGridViewRecruitedByHeadLabel"
                                                                                    runat="server" SkinID="sknLabelFieldHeaderText" Text="Associated By"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="PositionProfileStatus_printCandidatesGridViewRecruitedByLabel" runat="server"
                                                                                    SkinID="sknLabelFieldText" Text='<%# Eval("RecruiterName") %>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td valign="top" style="width: 25%">
                                                        <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="PositionProfileStatus_printCandidatesGridViewTestScoreHeaderLabel"
                                                                                    runat="server" Text="Test Score" SkinID="sknLabelText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="PositionProfileStatus_printCandidatesGridViewTestScoreLabel" runat="server"
                                                                                    Text='<%# Eval("TestScore") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="PositionProfileStatus_printCandidatesGridViewInterviewScoreHeaderLabel"
                                                                                    runat="server" Text="Interview Score" SkinID="sknLabelText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;<asp:Label ID="PositionProfileStatus_printCandidatesGridViewInterviewTotalWeightedScoreLabel"
                                                                                    runat="server" Text="Total Weighted Score" SkinID="sknLabelText"></asp:Label>
                                                                                &nbsp;<asp:Label ID="PositionProfileStatus_printCandidatesGridViewInterviewTotalWeightedScoreValueLabel"
                                                                                    SkinID="sknLabelFieldTextBold" runat="server" Text='<%# Eval("InterviewTotalWeightedScore") %>'
                                                                                    ToolTip="Total Weighted Score"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="PositionProfileStatus_printCandidatesGridViewTestStatusHeaderLabel"
                                                                                    runat="server" Text="Test Status" SkinID="sknLabelText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="PositionProfileStatus_printCandidatesGridViewTestStatusLabel" runat="server"
                                                                                    Text='<%# Eval("Status") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="PositionProfileStatus_printCandidatesGridViewInterviewStatusHeaderLabel"
                                                                                    runat="server" Text="Offline Interview Status" SkinID="sknLabelText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="PositionProfileStatus_printCandidatesGridViewInterviewStatusLabel"
                                                                                    runat="server" Text='<%# Eval("InterviewStatus") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="PositionProfileStatus_printCandidatesGridViewOnlineInterviewStatusHeaderLabel"
                                                                                    runat="server" Text="Online Interview Status" SkinID="sknLabelText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="PositionProfileStatus_printCandidatesGridViewOnlineInterviewStatusLabel"
                                                                                    runat="server" Text='<%# Eval("OnlineInterviewStatus") %>' SkinID="sknLabelFieldText"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td valign="top" align="left">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="left">
                                                            <tr>
                                                                <td align="left" width="10%">
                                                                    <asp:Label ID="PositionProfileStatus_printCandidatesGridView_candidateStatusLabel"
                                                                        runat="server" SkinID="sknLabelText" Text="Status "></asp:Label>
                                                                </td>
                                                                <td align="left" width="90%">
                                                                    <asp:Label ID="PositionProfileStatus_printCandidatesGridView_candidateStatusValueLabel"
                                                                        runat="server" SkinID="sknLabelFieldText" Text='<%# Eval("CandidateStatus") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_height_10">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="PositionProfileStatus_printCandidatesGridView_commentsTitleLabel"
                                                                        runat="server" SkinID="sknLabelText" Text="Comments " Visible='<%# Convert.ToBoolean(GetVisibility(Convert.ToString(Eval("CandidateStatusComments")))) %>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="PositionProfileStatus_printCandidatesGridView_commentsLabel" runat="server"
                                                                        SkinID="sknLabelFieldText" Text='<%# Eval("CandidateStatusComments") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <%-- <asp:HiddenField ID="PositionProfileStatus_positionProfileIDHiddenField" runat="server"
                Value="0" />--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
