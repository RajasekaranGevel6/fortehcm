﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PositionProfileReview.aspx.cs
// File that represents the user interface layout and functionalities for 
// the PositionProfileReview page. This page helps in viewing the position 
// profile summary. 

#endregion Header 

#region Directives                                                             

using System;
using System.IO;
using System.Text;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;
using Forte.HCM.UI.CommonControls;

using iTextSharp.text.pdf;

#endregion Directives

namespace Forte.HCM.UI.PositionProfile
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for 
    /// the PositionProfileReview page. This page helps in viewing the position 
    /// profile summary. This class inherits the Forte.HCM.UI.Common.PageBase 
    /// class.
    /// </summary>
    public partial class PositionProfileReview : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="int"/> that holds the tables count.
        /// </summary>
        private const int TABLES_COUNT = 10;

        /// <summary>
        /// A <see cref="int"/> that holds the header detail table index.
        /// </summary>
        private const int HEADER_DETAIL_TABLE_INDEX = 0;

        #endregion Private Constants
        
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="int"/> that hold the position profile ID.
        /// </summary>
        private int positionProfileID = 0;

        /// <summary>
        /// A <see cref="string"/> that holds the position profile status.
        /// </summary>
        private string positionProfileStatus = string.Empty;

        /// <summary>
        /// A <see cref="string"/> that holds the position profile status name
        /// </summary>
        private string positionProfileStatusName = string.Empty;

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        private delegate void AsyncTaskDelegate();

        #endregion Private Variables

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Position Profile Review");

                // Clear messages.
                ClearMesages();

                // Check if position profile ID is passed.
                if (Utility.IsNullOrEmpty(Request.QueryString["positionprofileid"]))
                {
                    base.ShowMessage(PositionProfileReview_topErrorMessageLabel, 
                        PositionProfileReview_bottomErrorMessageLabel, "Position profile ID parameter missing");

                    return;
                }

                // Get position profile ID.
                int.TryParse(Request.QueryString["positionprofileid"], out positionProfileID);

                if (positionProfileID <= 0)
                {
                    base.ShowMessage(PositionProfileReview_topErrorMessageLabel,
                        PositionProfileReview_bottomErrorMessageLabel, "Position profile is invalid");

                    return;
                }

                // Check if user is having permission for this page.
                if (positionProfileID != 0)
                {
                    if (!base.HasPositionProfileReviewRights(positionProfileID, base.tenantID))
                    {
                        // Redirect to search position profile page.
                        Response.Redirect(@"~\PositionProfile\SearchPositionProfile.aspx?m=1&s=1&parentpage=WF_LAND&index=1", false);
                        return;
                    }
                }

                // Subscribe to email type confirmation popup.
                PositionProfileReview_emailTypeConfirmMsgControl.CommandEventAdd_Click += new 
                    ConfirmMsgControl.Button_CommandClick
                    (PositionProfileReview_emailTypeConfirmMsgControl_CommandEventAdd_Click);

                // Load summary.
                LoadSummary();

                // Assign handlers.
                AssignHandlers();

                // Show status changed message.
                if (!IsPostBack && !Utility.IsNullOrEmpty(Session["POSITION_PROFILE_REVIEW_STATUS_CHANGED"]) &&  
                    Convert.ToBoolean(Session["POSITION_PROFILE_REVIEW_STATUS_CHANGED"]) == true)
                {
                    // Reset status changed flag from session.
                    Session["POSITION_PROFILE_REVIEW_STATUS_CHANGED"] = null;

                    ShowMessage(PositionProfileReview_topSuccessMessageLabel,
                        PositionProfileReview_bottomSuccessMessageLabel, "Position status updated successfully");
                }
            }
            catch (Exception exp)
            {
                ShowMessage(PositionProfileReview_topErrorMessageLabel, 
                    PositionProfileReview_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the action button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/>that holds the event data.
        /// </param>
        protected void PositionProfileEntry_actionButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                string positionID = Request.QueryString["positionprofileid"];

                ImageButton button = (ImageButton)sender;

                switch (button.CommandName)
                {
                    case "download":
                            DataSet rawData = new PositionProfileBLManager().GetPositionProfileSummary(positionProfileID, base.userID);
                            if (rawData != null && rawData.Tables.Count > 0)
                            {
                                CandidateReportDetail candidateReportDetail;
                                PDFDocumentWriter PdfWriter = new PDFDocumentWriter();
                                string logoPath = Server.MapPath("~/");
                                PDFHeader(out candidateReportDetail);                               
                                DataTable TableHeader = rawData.Tables[HEADER_DETAIL_TABLE_INDEX];
                                DataRow dataRow = TableHeader.Rows[0];
                                string fileName = dataRow["POSITION_PROFILE_NAME"].ToString().Trim();
                                Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName.Replace(" ", "_") + ".pdf");
                                Response.ContentType = "application/pdf";
                                Response.BinaryWrite(PdfWriter.GetPostionprofileReview(rawData, logoPath, candidateReportDetail));
                                Response.End(); 
                            }                          
                        break;

                }
            }
            catch (Exception exp)
            {
                ShowMessage(PositionProfileReview_topErrorMessageLabel,
                    PositionProfileReview_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the send mail is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void PositionProfileReview_sendMailButton_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                StringWriter stringWriter = new StringWriter(stringBuilder);
                HtmlTextWriter htmlTextWriter = new HtmlTextWriter(stringWriter);

                // Get raw data.
                DataSet rawData = new PositionProfileBLManager().GetPositionProfileSummary(positionProfileID, base.userID);

                // Keep recruiter emails in session.
                if (rawData != null && rawData.Tables.Count > 0 && rawData.Tables[0].Rows != null && rawData.Tables[0].Rows.Count > 0)
                {
                    // Assign recruiters email list in session.
                    string recruitersSessionKey = "POSITION_PROFILE_REVIEW_RECRUITER_EMAILS_" + positionProfileID;
                    if (rawData.Tables[0].Rows[0]["RECRUITER_EMAILS"] != null)
                        Session[recruitersSessionKey] = rawData.Tables[0].Rows[0]["RECRUITER_EMAILS"].ToString();
                    else
                        Session[recruitersSessionKey] = string.Empty;

                    // Assign position profile name in session.
                    string positionProfileNameSessionKey = "POSITION_PROFILE_REVIEW_PROFILE_NAME_" + positionProfileID;
                    if (rawData.Tables[0].Rows[0]["POSITION_PROFILE_NAME"] != null)
                        Session[positionProfileNameSessionKey] = rawData.Tables[0].Rows[0]["POSITION_PROFILE_NAME"].ToString();
                    else
                        Session[positionProfileNameSessionKey] = string.Empty;
                }

                // Construct html table.
                Table summaryTable = new PositionProfileReviewDataManager().GetPositionProfileSummary
                    (rawData, false, true);

                summaryTable.RenderControl(htmlTextWriter);

                // Construct the session key.
                string sessionKey = "POSITION_PROFILE_REVIEW_" + positionProfileID;

                // Keep the table in session.
                Session[sessionKey] = stringBuilder.ToString();

                PositionProfileReview_emailTypeConfirmMsgControl.Title = "Email Confirmation";
                PositionProfileReview_emailTypeConfirmMsgControl.Message = "How do you want to email the position profile?";
                PositionProfileReview_emailTypeConfirmMsgControl.Type = MessageBoxType.EmailConfirmType;
                PositionProfileReview_emailTypeModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                ShowMessage(PositionProfileReview_topErrorMessageLabel,
                    PositionProfileReview_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the status save button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void PositionProfileReview_statusSaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(PositionProfileReview_statusDropDownList.SelectedValue))
                    return;

                // Update the status.
                new PositionProfileBLManager().UpdatePositionProfileStatus(
                   PositionProfileReview_statusDropDownList.SelectedValue.Trim(), null, 
                   base.userID, positionProfileID, 0, "POSITION");

                // Set status changed flag to session.
                Session["POSITION_PROFILE_REVIEW_STATUS_CHANGED"] = true;

                // Send alert email to position profile associates, indicating 
                // the status change.
                try
                {
                    // Send the status change alert email asynchronously.
                    AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendStatusChangeAlertEmail);
                    IAsyncResult result = taskDelegate.BeginInvoke(new AsyncCallback(SendStatusChangeAlertEmailCallBack), taskDelegate);
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }

                // Redirect to the same page by setting session flag to 'status changed'
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                ShowMessage(PositionProfileReview_topErrorMessageLabel,
                    PositionProfileReview_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the refresh button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void PositionProfileReview_refreshButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Redirect to the same page.
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                ShowMessage(PositionProfileReview_topErrorMessageLabel,
                    PositionProfileReview_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the email type option is selected.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        private void PositionProfileReview_emailTypeConfirmMsgControl_CommandEventAdd_Click(object sender, CommandEventArgs e)
        {
            try
            {
                // Construct user type ('O' for owner/co-owner & 'I' for internal)
                string userType = PositionProfileReview_changeStatusLinkButton.Visible ? "O" : "I";

                if (e.CommandName == "attachment")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(),
                        "ReviewPDF", "<script language='javascript'>PositionProfileReviewEmail('PP_REVIEW_PDF','" + positionProfileID + "','" + userType + "');</script>", false);
                }
                else if (e.CommandName == "bodycontent")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(),
                      "Review", "<script language='javascript'>PositionProfileReviewEmail('PP_REVIEW','" + positionProfileID + "','" + userType + "');</script>", false);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(PositionProfileReview_topErrorMessageLabel,
                    PositionProfileReview_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Asynchronous Method Handlers                                   

        /// <summary>
        /// Handler method that acts as the callback method for position profile 
        /// status change email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendStatusChangeAlertEmailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Asynchronous Method Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that send email to position profile associates indicating
        /// the status change of position profile.
        /// </summary>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendStatusChangeAlertEmail()
        {
            try
            {
                // Construct position profile detail.
                PositionProfileDetail positionProfileDetail = new PositionProfileDetail();
                positionProfileDetail.PositionProfileID = positionProfileID;
                positionProfileDetail.PositionProfileName = PositionProfileReview_positionProfileNameHiddenField.Value;
                positionProfileDetail.PositionProfileStatusName = PositionProfileReview_statusDropDownList.SelectedItem.Text;
                positionProfileDetail.ClientName = PositionProfileReview_clientNameHiddenField.Value;
                positionProfileDetail.ModifiedByName = base.userFullName;

                // Send email.
                new EmailHandler().SendMail(EntityType.PositionProfileStatusChanged, positionProfileDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that clears the messages.
        /// </summary>
        private void ClearMesages()
        {
            PositionProfileReview_topErrorMessageLabel.Text = string.Empty;
            PositionProfileReview_bottomErrorMessageLabel.Text = string.Empty;
            PositionProfileReview_topSuccessMessageLabel.Text = string.Empty;
            PositionProfileReview_bottomSuccessMessageLabel.Text = string.Empty;
        }

        /// <summary>
        /// Method that loads the summary data as a dynamic html table.
        /// </summary>
        private void LoadSummary()
        {
            // Get raw data.
            DataSet rawData = new PositionProfileBLManager().GetPositionProfileSummary(positionProfileID, base.userID);

            // Construct html table.
            Table summaryTable = new PositionProfileReviewDataManager().GetPositionProfileSummary
                (rawData, true, false);

            if (summaryTable != null)
                PositionProfileReview_summaryPlaceHolder.Controls.Add(summaryTable);

            // Assign position profile name to hidden field.
            if (rawData != null && rawData.Tables.Count > 0 && rawData.Tables[0].Rows != null &&
                rawData.Tables[0].Rows.Count > 0)
            {
                if (rawData.Tables[0].Rows[0]["POSITION_PROFILE_NAME"] != null)
                    PositionProfileReview_positionProfileNameHiddenField .Value = rawData.Tables[0].Rows[0]["POSITION_PROFILE_NAME"].ToString();

                if (rawData.Tables[0].Rows[0]["CLIENT"] != null)
                {
                    string client = rawData.Tables[0].Rows[0]["CLIENT"].ToString();

                    string clientID = string.Empty;
                    string clientName = string.Empty;

                    if (!Utility.IsNullOrEmpty(client))
                    {
                        Regex regex = new Regex(@"(^\d*)");
                        Match match = regex.Match(client);

                        if (match.Success)
                        {
                            clientID = match.Value;
                            clientName = regex.Replace(client, string.Empty);
                            PositionProfileReview_clientNameHiddenField.Value = clientName.Trim(new char[] { '~' });
                        }
                    }
                }
            }

            if (!IsPostBack)
            {
                // Load position profile status.
                LoadPositionProfileStatus(rawData);
            }
        }

        /// <summary>
        /// Method that loads the position profile status.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        private void LoadPositionProfileStatus(DataSet rawData)
        {
            // Check for validity of raw data.
            if (rawData == null || rawData.Tables.Count == 0)
                throw new Exception("Raw data cannot be null");

            if (rawData.Tables.Count != TABLES_COUNT)
                throw new Exception("Raw data is invalid");

            // Retrieve has owner rights status.
            if (rawData.Tables[HEADER_DETAIL_TABLE_INDEX].Rows == null || rawData.Tables[HEADER_DETAIL_TABLE_INDEX].Rows.Count == 0)
                throw new Exception("Raw data is invalid");

            // Retreive 0th row.
            DataRow dataRow = rawData.Tables[HEADER_DETAIL_TABLE_INDEX].Rows[0];

            if (dataRow["POSITION_PROFILE_STATUS"] != null && dataRow["POSITION_PROFILE_STATUS"] != DBNull.Value)
                positionProfileStatus = dataRow["POSITION_PROFILE_STATUS"].ToString().Trim();

            if (dataRow["POSITION_PROFILE_STATUS_NAME"] != null && dataRow["POSITION_PROFILE_STATUS_NAME"] != DBNull.Value)
                positionProfileStatusName = dataRow["POSITION_PROFILE_STATUS_NAME"].ToString().Trim();

            // Assign position profile status change rights.
            if (dataRow["HAS_OWNER_RIGHTS"] != null && dataRow["HAS_OWNER_RIGHTS"] != DBNull.Value)
                PositionProfileReview_changeStatusLinkButton.Visible = dataRow["HAS_OWNER_RIGHTS"].ToString().Trim().ToUpper() == "Y" ? true : false;

            // Assign position profile status to display label.
            PositionProfileReview_statusValueLabel.Text = positionProfileStatusName;

            // Load position profile status drop down items.
            PositionProfileReview_statusDropDownList.DataSource =
               new AttributeBLManager().GetAttributesByType("PP_STATUS");
            PositionProfileReview_statusDropDownList.DataTextField = "AttributeName";
            PositionProfileReview_statusDropDownList.DataValueField = "AttributeID";
            PositionProfileReview_statusDropDownList.DataBind();

            if (!Utility.IsNullOrEmpty(positionProfileStatus) && PositionProfileReview_statusDropDownList.Items.
                FindByValue(positionProfileStatus) != null)
            {
                PositionProfileReview_statusDropDownList.SelectedValue = positionProfileStatus;
            }
        }

        /// <summary>
        /// Method that assign hanldlers to button actions.
        /// </summary>
        private void AssignHandlers()
        {
            PositionProfileReview_topActivityImageButton.Attributes.Add("onclick",
                "javascript:return ShowPositionProfileActivityLog(" + positionProfileID + ")");

            PositionProfileReview_bottomActivityImageButton.Attributes.Add("onclick",
                "javascript:return ShowPositionProfileActivityLog(" + positionProfileID + ")");

            PositionProfileReview_topDashboardHyperLink.NavigateUrl = "../PositionProfile/PositionProfileStatus.aspx?m=1&s=1" +
                "&positionprofileid=" + positionProfileID +
                "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_REVIEW;

            PositionProfileReview_bottomDashboardHyperLink.NavigateUrl = "../PositionProfile/PositionProfileStatus.aspx?m=1&s=1" +
                "&positionprofileid=" + positionProfileID +
                "&parentpage=" + Constants.ParentPage.POSITION_PROFILE_REVIEW;
        }

        /// <summary>
        /// Method that retrieves the header for pdf.
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A <see cref="CandidateReportDetail"/> that holds the user detail.
        /// </param>
        private void PDFHeader(out CandidateReportDetail candidateReportDetail)
        {
            candidateReportDetail = new CandidateReportDetail();
            UserDetail userDetail = new UserDetail();
            userDetail = (UserDetail)Session["USER_DETAIL"];
            candidateReportDetail.LoginUser = userDetail.FirstName + " " + userDetail.LastName;
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Overridden method that loads default values into controls.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Overridden Methods
    }
}