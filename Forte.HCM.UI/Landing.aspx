﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WorkflowMaster.Master"
    AutoEventWireup="true" CodeBehind="Landing.aspx.cs" Inherits="Forte.HCM.UI.Landing" %>

<%@ MasterType VirtualPath="~/MasterPages/WorkflowMaster.Master" %>

<asp:Content ID="Landing_Content" ContentPlaceHolderID="WorkflowMaster_body" runat="server">
    <br />
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:Label ID="Landing_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="Landing_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <div id="Landing_mainDIV" runat="server">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="77">
                                            <img src="App_Themes/DefaultTheme/Images/wf_top_left_strip.jpg" alt="" width="9"
                                                height="77" />
                                        </td>
                                        <td valign="middle" class="wf_top_center_strip">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="center" class="wf_title">What would you like to do today?
                                                    </td>
                                                    
                                                    <td align="center" class="wf_title">
                                                        <asp:Button Visible="false"  ID="btnCorUserMng" runat="server" Text="Corporate User Management" SkinID="sknCorUserMng" PostBackUrl="~/Admin/CorporateUserManagement.aspx?m=0&s=0" />
                                                         
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <img src="App_Themes/DefaultTheme/Images/wf_top_right_strip.jpg" alt="" width="9"
                                                height="77" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="wf_body">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td align="center">
                                            <table width="75%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td align="center">
                                                        <asp:UpdatePanel ID="Landing_accordionUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <ajaxToolKit:Accordion ID="Landing_accordion" runat="server" RequireOpenedPane="false"
                                                                    SelectedIndex="-1" AutoSize="None">
                                                                    <Panes>
                                                                        <ajaxToolKit:AccordionPane ID="Landing_tenantManagementAccordionPane" runat="server"
                                                                            ContentCssClass="wf_padding_left10" Visible="false">
                                                                            <Header>
                                                                                <asp:Image ID="Landing_tenantManagementHeaderImageButton" runat="server" SkinID="sknAccordionTenantManagementHeaderImage"
                                                                                    BorderStyle="None" BorderWidth="0px" />
                                                                            </Header>
                                                                            <Content>
                                                                                <asp:ImageButton ID="Landing_superAdminImageButton" Visible="false" runat="server" SkinID="sknSuperAdminContent"
                                                                                    PostBackUrl="~/SiteAdminHome.aspx" />
                                                                                <br />
                                                                                <asp:ImageButton ID="Landing_addTenantImageButton" runat="server" SkinID="sknAddTenantContent"
                                                                                    PostBackUrl="~/CustomerAdminHome.aspx" />
                                                                            </Content>
                                                                        </ajaxToolKit:AccordionPane>
                                                                        <ajaxToolKit:AccordionPane ID="Landing_clientManagementAccordionPane" runat="server"
                                                                            ContentCssClass="wf_padding_left10">
                                                                            <Header>
                                                                                <asp:Image ID="Landing_clientManagementHeaderImageButton" runat="server" SkinID="sknAccordionClientManagementHeaderImage"
                                                                                    BorderStyle="None" BorderWidth="0px" />
                                                                            </Header>
                                                                            <Content>
                                                                                <asp:ImageButton ID="Landing_addClientImageButton" runat="server" SkinID="sknAddClientContent"
                                                                                    PostBackUrl="~/ClientCenter/ClientManagement.aspx?m=0&s=0&parentpage=WF_LAND&addclient=y&index=0" />
                                                                                <br />
                                                                                <asp:ImageButton ID="Landing_viewClientImageButton" runat="server" SkinID="sknViewClientContent"
                                                                                    PostBackUrl="~/ClientCenter/ClientManagement.aspx?m=0&s=0&parentpage=WF_LAND&index=0" />
                                                                            </Content>
                                                                        </ajaxToolKit:AccordionPane>
                                                                        
                                                                        <ajaxToolKit:AccordionPane ID="Landing_positionProfileAccordionPane" runat="server"
                                                                            ContentCssClass="wf_padding_left10">
                                                                            <Header>
                                                                                <asp:Image ID="Landing_positionProfileHeaderImageButton" runat="server" SkinID="sknAccordionPositionProfileHeaderImage"
                                                                                    BorderStyle="None" BorderWidth="0px" />
                                                                            </Header>
                                                                            <Content>
                                                                                <asp:ImageButton ID="Landing_createPositionProfileImageButton" runat="server" SkinID="sknCreatePositionProfileContent"
                                                                                    PostBackUrl="~/PositionProfile/PositionProfileBasicInfo.aspx?m=1&s=0&parentpage=WF_LAND&index=1" />
                                                                                <br />
                                                                                <asp:ImageButton ID="Landing_viewPositionProfileImageButton" runat="server" SkinID="sknViewPositionProfileContent"
                                                                                    PostBackUrl="~/PositionProfile/SearchPositionProfile.aspx?m=1&s=1&parentpage=WF_LAND&index=1" />
                                                                            </Content>
                                                                        </ajaxToolKit:AccordionPane>
                                                                        <ajaxToolKit:AccordionPane Visible="false" ID="Landing_weightedSearchAccordionPane" runat="server">
                                                                            <Header>
                                                                                <asp:ImageButton ID="Landing_weightedSearchHeaderImageButton" runat="server" SkinID="sknAccordionWeightedSearchHeader"
                                                                                    PostBackUrl="~/TalentScoutHome.aspx?parentpage=WF_LAND" />
                                                                            </Header>
                                                                        </ajaxToolKit:AccordionPane>
                                                                        <ajaxToolKit:AccordionPane ID="Landing_assessmentAccordionPane" runat="server" ContentCssClass="wf_padding_left10">
                                                                            <Header>
                                                                                <asp:Image ID="Landing_assessmentHeaderImageButton" runat="server" SkinID="sknAccordionAssessmentHeaderImage" />
                                                                            </Header>
                                                                            <Content>
                                                                                <asp:ImageButton ID="Landing_uploadContentImageButton" SkinID="sknuploadContent"
                                                                                    runat="server" PostBackUrl="~/Questions/BatchQuestionEntry.aspx?m=0&s=0&parentpage=WF_LAND&index=3" />
                                                                                <br />
                                                                                <asp:ImageButton ID="Landing_createTestImageButton" runat="server" SkinID="sknCreateTestContent"
                                                                                    PostBackUrl="~/TestMaker/CreateAutomaticTest.aspx?m=1&s=0&parentpage=WF_LAND&index=3" />
                                                                                <br />
                                                                                <asp:ImageButton ID="Landing_searchTestImageButton" runat="server" SkinID="sknSearchTestContent"
                                                                                    PostBackUrl="~/TestMaker/TestSession.aspx?m=1&s=3&parentpage=WF_LAND&index=3" />
                                                                                <br />
                                                                                <asp:ImageButton ID="Landing_scheduleTestImageButton" SkinID="sknScheduleTestContent"
                                                                                    runat="server" PostBackUrl="~/Scheduler/ScheduleCandidate.aspx?m=2&s=1&parentpage=WF_LAND&index=3" />
                                                                            </Content>
                                                                        </ajaxToolKit:AccordionPane>
                                                                        <ajaxToolKit:AccordionPane Visible="false" ID="Landing_interviewAccordionPane" runat="server" ContentCssClass="wf_padding_left10">
                                                                            <Header>
                                                                                <asp:Image ID="Landing_interviewHeaderImageButton" runat="server" SkinID="sknAccordionInterviewHeaderImage" />
                                                                            </Header>
                                                                            <Content>
                                                                                <asp:ImageButton ID="Landing_interviewUploadContentImageButton" SkinID="sknInterviewUploadContent"
                                                                                    runat="server" PostBackUrl="~/InterviewQuestions/InterviewBatchQuestionEntry.aspx?m=0&s=0&parentpage=WF_LAND&index=4" />
                                                                                <br />
                                                                                <asp:ImageButton ID="Landing_interviewCreateTestImageButton" runat="server" SkinID="sknInterviewCreateTestContent"
                                                                                    PostBackUrl="~/InterviewTestMaker/CreateAutomaticInterviewTest.aspx?m=1&s=0&parentpage=WF_LAND&index=4" />
                                                                                <br />
                                                                                <asp:ImageButton ID="Landing_interviewSearchTestImageButton" runat="server" SkinID="sknInterviewSearchTestContent"
                                                                                    PostBackUrl="~/InterviewTestMaker/InterviewTestSession.aspx?m=1&s=3&parentpage=WF_LAND&index=4" />
                                                                                <br />
                                                                                <asp:ImageButton ID="Landing_interviewScheduleTestImageButton" SkinID="sknInterviewScheduleTestContent"
                                                                                    runat="server" PostBackUrl="~/InterviewScheduler/InterviewScheduleCandidate.aspx?m=2&s=1&parentpage=WF_LAND&index=4" />
                                                                                <br />
                                                                                <asp:ImageButton ID="Landing_interviewAssessInterviewImageButton" SkinID="sknInterviewAssessInterviewContent"
                                                                                    runat="server" PostBackUrl="~/Assessments/MyAssessments.aspx?m=5&s=0&parentpage=WF_LAND&index=4" />
                                                                            </Content>
                                                                        </ajaxToolKit:AccordionPane>
                                                                        <ajaxToolKit:AccordionPane  Visible="false" ID="Landing_onlineInterviewAccordionPane" runat="server"
                                                                            ContentCssClass="wf_padding_left10">
                                                                            <Header>
                                                                                <asp:Image ID="Landing_onlineInterviewHeaderImageButton" runat="server" SkinID="sknAccordionOnlineInterviewHeaderImage" />
                                                                            </Header>
                                                                            <Content>
                                                                                <asp:ImageButton ID="Landing_onlineInterviewCreateImageButton" runat="server" SkinID="sknOnlineInterviewCreateContent"
                                                                                    PostBackUrl="~/OnlineInterview/OnlineInterviewCreation.aspx?m=3&s=0&parentpage=WF_LAND&index=5" />
                                                                                <br />
                                                                                <asp:ImageButton ID="Landing_onlineInterviewSearchImageButton" runat="server" SkinID="sknOnlineInterviewSearchContent"
                                                                                    PostBackUrl="~/OnlineInterview/OnlineInterviewSession.aspx?m=3&s=1&parentpage=WF_LAND&index=5" />
                                                                            </Content>
                                                                        </ajaxToolKit:AccordionPane>
                                                                        <ajaxToolKit:AccordionPane ID="Landing_viewReportsAccordionPane" runat="server" ContentCssClass="wf_padding_left10">
                                                                            <Header>
                                                                                <asp:Image ID="Landing_viewReportsHeaderImageButton" runat="server" SkinID="SknAccordionViewReportsHeader" />
                                                                            </Header>
                                                                            <Content>
                                                                                <asp:ImageButton ID="Landing_viewReportsTestReportsImageButton" runat="server" SkinID="sknReportsTestReports"
                                                                                    PostBackUrl="~/ReportCenter/TestReport.aspx?m=3&s=0&parentpage=WF_LAND&index=6" />
                                                                                <br />
                                                                                <asp:ImageButton Visible="false" ID="Landing_viewReportsInterviewReportsImageButton" runat="server"
                                                                                    SkinID="sknReportsInterviewReports" PostBackUrl="~/InterviewReportCenter/InterviewTestReport.aspx?m=4&s=0&parentpage=WF_LAND&index=6" />
                                                                                <br />
                                                                            </Content>
                                                                        </ajaxToolKit:AccordionPane>
                                                                        <ajaxToolKit:AccordionPane Visible="false" ID="Landing_CandidateAccordionPane" runat="server" ContentCssClass="wf_padding_left10">
                                                                            <Header>
                                                                                <asp:Image ID="Landing_CandidateHeaderImageButton" runat="server" SkinID="sknPPCandidateAssociationImage" />
                                                                            </Header>
                                                                            <Content>
                                                                                <asp:ImageButton ID="ImageButton1" runat="server" SkinID="sknReportsTestReports"
                                                                                    PostBackUrl="~/ReportCenter/TestReport.aspx?m=3&s=0&parentpage=WF_LAND&index=6" />
                                                                                <br />
                                                                                <asp:ImageButton Visible="false" ID="ImageButton2" runat="server"
                                                                                    SkinID="sknReportsInterviewReports" PostBackUrl="~/InterviewReportCenter/InterviewTestReport.aspx?m=4&s=0&parentpage=WF_LAND&index=6" />
                                                                                <br />
                                                                            </Content>
                                                                        </ajaxToolKit:AccordionPane>

                                                                    </Panes>
                                                                </ajaxToolKit:Accordion>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td valign="middle">
                                                        <div>
                                                            <asp:ImageButton ID="Landing_loadLandingImageButton" runat="server" SkinID="sknLoadWorflowHeader"
                                                                PostBackUrl="~/Workflow.aspx?parentpage=WF_LAND" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
