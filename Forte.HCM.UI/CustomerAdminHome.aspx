﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/CustomerAdminMaster.Master"
    CodeBehind="CustomerAdminHome.aspx.cs" Inherits="Forte.HCM.UI.CustomerAdminHome" %>

<%@ MasterType VirtualPath="~/MasterPages/CustomerAdminMaster.Master" %>
<asp:Content ID="CustomerAdminHome_bodyContent" runat="server" ContentPlaceHolderID="CustomerAdminMaster_body">
    <table width="972px" border="0" cellspacing="0" cellpadding="0" class="table_bg"
        align="center">
        <tr>
            <td align="center" class="title_home_icon">
                <h2>
                    Tenant Admin Home
                </h2>
            </td>
        </tr>
        <tr>
            <td align="center">
                <img src="App_Themes/DefaultTheme/Images/Customer_Admin.png" alt="Tenant Admin"
                    height="226px" width="234px" />
            </td>
        </tr>
    </table>
</asp:Content>
