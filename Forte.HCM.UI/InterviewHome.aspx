﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/InterviewMaster.Master"
    CodeBehind="InterviewHome.aspx.cs" Inherits="Forte.HCM.UI.InterviewHome" %>

<%@ MasterType VirtualPath="~/MasterPages/InterviewMaster.Master" %>
<asp:Content ID="InterviewHome_bodyContent" runat="server" ContentPlaceHolderID="InterviewMaster_body">
    <table width="972px" border="0" cellspacing="0" cellpadding="0" class="table_bg"
        align="center">
        <tr>
            <td align="center" class="title_home_icon">
                <h2>
                    Interview Module Home
                </h2>
            </td>
        </tr>
        <tr>
            <td align="center">
                <img src="App_Themes/DefaultTheme/Images/Interview_Tool.png" alt="Interview Module"
                    height="226px" width="234px" />
            </td>
        </tr>
    </table>
</asp:Content>
