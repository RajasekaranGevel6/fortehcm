﻿using System;
using System.Web.UI;

namespace Forte.HCM.UI
{
    public partial class ResumeRepositoryHome : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.SetPageCaption("Resume Repository Home");
        }
    }
}