﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LicenseAgreement.aspx.cs"
    Inherits="Forte.HCM.UI.Subscription.LicenseAgreement" MasterPageFile="~/MasterPages/LicenseAgreementMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/LicenseAgreementMaster.Master" %>
<asp:Content ID="LicenseAgreement_body" ContentPlaceHolderID="LicenseAgreementMaster_body"
    runat="server">
    <script language="javascript" type="text/javascript">

        var remainingLabel = '<%= LicenseAgreement_remainingtimeLabel.ClientID %>';
        var showTimer = '<%= LicenseAgreement_showTimerHiddenField.ClientID %>';
        var timerCount = 10;
        var t;

        function onLoadPage() {
            if (document.getElementById(showTimer).value == 'Y') {
                ShowRemainingTime();
            }
        }

        function ShowRemainingTime() {
            timerCount -= 1;
            document.getElementById(remainingLabel).innerHTML = timerCount;
            if (timerCount == 0) {
                document.getElementById(showTimer).value = "G";
                clearTimeout(t);
                __doPostBack(null, null);
            }
            else
                t = setTimeout(ShowRemainingTime, 1000);
        }

        onload = onLoadPage;

    </script>
    <div id="LicenseAgreement_div" runat="server" style="margin: auto; width: 100%; height: 100%;
        background-position: center;">
        <table style="width: 100%" cellpadding="0" cellspacing="0" class="td_pading_logo_10">
            <tr>
                <td class="header_bg">
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td width="100%" class="header_text_bold">
                                <asp:Literal ID="LicenseAgreement_headerLiteral" runat="server" Text="License Agreement"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="td_height_5">
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:Label ID="LicenseAgreement_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                    <asp:Label ID="LicenseAgreement_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tab_body_bg" style="width: 100%; color: #81868A; font-size: small" align="left">
                    <div style="height: 420px; width: 100%; overflow: auto">
                        <p onkeydown="return DisableCtrlKey(event)">
                            <span class="license_agreement_title">Forte HCM Inc.</span>
                            <br />
                            <span class="license_agreement_bold">PLEASE READ THIS SOFTWARE LICENSE AGREEMENT ("LICENSE")
                                CAREFULLY BEFORE USING THE ForteHCM SOFTWARE(ForteHCM™) . BY USING OUR SOFTWARE,
                                YOU ARE AGREEING TO BE BOUND BY THE TERMS OF THIS LICENSE. IF YOU DO NOT AGREE TO
                                THE TERMS OF THIS LICENSE, DO NOT USE THE SOFTWARE. IF YOU DO NOT AGREE TO THE TERMS
                                OF THE LICENSE, YOU MAY RETURN THE Forte HCM SOFTWARE TO THE PLACE WHERE YOU OBTAINED
                                OR TO Forte HCM Inc. at 2500 W. HIGGINS RD, #870, HOFFMAN ESTATES, IL- 60169. IF
                                THE ForteHCM SOFTWARE WAS ACCESSED ELECTRONICALLY, CLICK "DISAGREE".
                                <br />
                                <br />
                                IMPORTANT NOTE: This software is licensed to you only & remote access is only provided
                                for lawful personal use or as otherwise legally permitted. If you are uncertain
                                about your permit access to any material you should contact Forte HCM Inc or your
                                sponsoring firm. </span>
                            <br />
                            <br />
                            <span class="license_agreement_title">General</span>
                            <br />
                            <span class="license_agreement_bold">ForteHCM™</span> <span class="license_agreement_regular">( also referred to as FHCM) and any
                                third party software, documentation and any fonts accompanying this License whether
                                on disk, in read only memory, on any other media or in any other form (collectively
                                the "ForteHCM Software") are licensed, not sold, to you by Forte HCM Inc. The terms
                                of this License will govern any software upgrades provided by FHCM that replace
                                and/or supplement the original FHCM Software product, unless such upgrade is accompanied
                                by a separate license in which case the terms of that license will govern. Title
                                and intellectual property rights in and to any content displayed by or accessed
                                through the FHCM Software belongs to the respective content owner. Such content
                                may be protected by copyright or other intellectual property laws and treaties,
                                and may be subject to terms of use of the third party providing such content. This
                                License does not grant you any rights to use such content. </span>
                            <br />
                            <br />
                            <span class="license_agreement_title">Permitted License Uses and Restrictions</span>
                            <br />
                            <span class="license_agreement_regular">Subject to the terms and conditions of this
                                License, you are granted a limited non-exclusive license to install and use FHCM
                                Software. </span><span class="license_agreement_bold">You may not make FHCM Software
                                    available over a network where it could be used by multiple computers at the same
                                    time.</span><br />
                            <br />
                            <span class="license_agreement_regular">This License Agreement is between the Customer
                                whose name appears on the Subscription Agreement (by virtue of logging in or by
                                executing an agreement) and Forte HCM Inc. The products are licensed to specific
                                customers for their use in their recruiting, assessment and hiring efforts. This
                                License Agreement allows you to use the copyrighted software programs and documentation
                                that comprise the </span><span class="license_agreement_bold">ForteHCM™</span><span
                                    class="license_agreement_regular"> intelliSPOT, intelliTEST & intelliVIEW products.
                                    The uses of these products are subject to limitations explained below. This right
                                    is granted to you: (1) upon condition that you accept the terms of this License
                                    Agreement, (2) in consideration of payment of the license fee, and (3) limited use
                                    based on the plan subscribed to and/ or any trial offer.</span>
                            <br />
                            <br />
                            <span class="license_agreement_title">LICENSE AGREEMENT TERMS AND CONDITIONS </span>
                            <br />
                            <br />
                            <span class="license_agreement_title">GRANT OF LICENSE </span>
                            <br />
                            <span class="license_agreement_regular">This </span><span class="license_agreement_bold">ForteHCM™</span>
                            <span class="license_agreement_regular">product application consists of intelliSPOT,
                                intelliTEST, intelliVIEW, all related executable, configuration files, DLL Components,
                                Filters and SearchBots. <span class="license_agreement_bold">ForteHCM™</span> <span
                                    class="license_agreement_regular">grants you a non-exclusive, nontransferable license
                                    to use the above stated products & its application, the accompanying storage media
                                    and associated documentation, and any additional SearchBots or Filters, provided
                                    to you in the future, on disk, USB, online downloads, web services or otherwise
                                    (collectively, the "Product") in accordance with the terms of this agreement.</span>
                                <br />
                                <br />
                                <span class="license_agreement_title">YOUR USE OF THE PRODUCT</span> </span>
                            <br />
                            <span class="license_agreement_regular">The intelliSPOT product offering provides links
                                to resume data banks. Resume sources can be expanded to include fee-based resume
                                banks for which the Customer is accountable for Customer's subscription fees. Resume
                                banks/firms may and in all likelihood impose daily download limits. It is the duty
                                of the customer and not the responsibility of ForteHCM, Inc. to make sure that they
                                comply with those limits. Customer is accountable for compliance with applicable
                                copyright and privacy laws including not copying or otherwise using any personally
                                identifiable information about individuals from their resumes. You may use the Product
                                only for your individual operational purposes in your home, business or institution.
                                You may not use the Product for the purpose of constructing, renting, or selling
                                information services that infringe upon the Terms of Use. You may not sublicense,
                                assign, or transfer any of your rights or obligations under this agreement. You
                                may not distribute, rent, sublicense, or lease the Product in whole or in part to
                                any third party. </span>
                            <br />
                            <br />
                            <span class="license_agreement_regular">Forte HCM, Inc. is not responsible for the accuracy
                                or veracity of the content contained in any resume bank or in its assessment platform.
                                In addition, Forte HCM, Inc. is, in no way, responsible for the Customer’s selection
                                or rejection of candidates based on content provided by its assessment platform
                                (intelliTEST & intelliVIEW) or for the performance of any person hired as a result
                                of the use of the Product. It is expressly understood by the Customer that the Product
                                is a facilitating tool that the Customer may use in helping sort resumes based upon
                                Customer determined employment criteria. Customer’s decision or actions with regard
                                to any employment situation is strictly the responsibility and determination of
                                the Customer, and Forte HCM, Inc. bears no responsibility whatsoever with regard
                                to said determination.</span>
                            <br />
                            <br />
                            <span class="license_agreement_regular">You have NO right to modify the Product in any
                                way, incorporate it into a compilation, reproduce it, or create any derivative work.
                                You may not reverse assemble, reverse engineer or decompile the Product, or otherwise
                                obtain access to the source code of the Dynamic-Link Libraries (DLL) or to any of
                                the program Piles which make up the Product. You agree to use the Product, in accordance
                                with local, state, and federal laws.</span>
                            <br />
                            <br />
                            <span class="license_agreement_title">OWNERSHIP </span>
                            <br />
                            <span class="license_agreement_regular">The Product is a proprietary product of Forte
                                HCM Inc., and is protected under U.S. copyright law and international treaty provisions.
                                Title and related rights in the content accessed through the Product is the property
                                of the applicable content owner and may be protected by applicable law. All intellectual
                                property rights in and to the product are retained by Forte HCM Inc.</span>
                            <br />
                            <br />
                            <span class="license_agreement_title">WARRANTIES DISCLAIMER </span>
                            <br />
                            <span class="license_agreement_regular">Limited Warranty on Media: Forte HCM Inc., warrants
                                that, when provided on disks, the disks on which the Product is initially furnished,
                                shall, for a period of 60 (sixty) days from delivery (the "Warranty Period") be
                                free, in normal use, from defects in material and workmanship. If, during the Warranty
                                Period, defects in the disks appear, you may return the disks and associated documentation
                                to your point of purchase for replacement.</span>
                            <br />
                            <br />
                            <span class="license_agreement_regular">Disclaimer of Warranty on Software: EXCEPT FOR
                                THE WARRANTIES SET FORTH ABOVE, THE PRODUCT IS LICENSED "AS IS," AND ForteHCM. Inc.,
                                DISCLAIMS ANY AND ALL OTHER WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
                                ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE,
                                AND NONINFRINGEMENT IN ALL JURISDICTIONS WHERE THESE WARRANTIES MAY BE DISCLAIMED
                                IN THE LICENSING OF INTELLECTUAL PROPERTY. In instances where the Product includes
                                access to fee-based resume banks, users are responsible for complying with all applicable
                                copyright and privacy laws concerning access and use of the resume content, including,
                                but not limited to, payment of subscription fees. </span>
                            <br />
                            <br />
                            <span class="license_agreement_regular">Disclaimer of Warranty on Customer data/content:
                                ForteHCM has controls in place to avoid loss of data but ultimately Forte HCM Inc
                                cannot be responsible for any loss of customer data. ForteHCM encourages its customers
                                to have a backup copy of its data.</span>
                            <br />
                            <br />
                            <span class="license_agreement_regular">No Consequential Damages: In no event shall
                                FHCM, or its dealers, have any liability to you or to any other person or entity
                                for any indirect, incidental, special, or consequential damages whatsoever, including,
                                but not limited to, loss of revenue or profit, loss or damaged data, or other commercial
                                or economic loss, even if ForteHCM Inc., has been advised of the possibility of
                                such damages or they are foreseeable; or for claims by a third party. The limitations
                                in this section shall apply whether or not the alleged breach or default is a breach
                                of a fundamental condition or term, or a fundamental breach. SOME STATES/COUNTRIES
                                DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL
                                DAMAGES SO THE ABOVE LIMITATIONS OR EXCLUSIONS MAY NOT APPLY TO YOU. </span>
                            <br />
                            <br />
                            <span class="license_agreement_title">EXPORT CONTROLS </span>
                            <br />
                            <span class="license_agreement_regular">No part of the Product or its underlying information
                                or technology may be downloaded or otherwise exported (1) into (or to a national
                                or resident of) Cuba, Libya, North Korea, Iran, Syria or any other country to which
                                the U.S. has embargoed goods; or (2) to anyone on the U.S. Treasury Department's
                                list of Specially Designated Nationals or the U S Commerce Department's Table of
                                Denial Orders. By downloading or using the Product, you are agreeing to the foregoing
                                and you are representing and warranting that you are not located in, under the control
                                of, or a national or resident of any such country or on any such list. </span>
                            <br />
                            <br />
                            <span class="license_agreement_title">TERM AND TERMINATION </span>
                            <br />
                            <span class="license_agreement_regular">Forte HCM Inc., reserves the right to terminate
                                this License Agreement if you have exceeded or attempted to exceed the licensed
                                uses in any way, or you have otherwise failed to comply with any of the terms and
                                conditions contained in this License Agreement. Termination notice may be effected
                                either my email provided at the time of login or simply by denial to FHCM product
                                features as set in “features & pricing” at our website (www.forte-hcm.com). Otherwise,
                                the License Agreement will terminate without any notice, if not renewed, at a fixed
                                date in accordance with the License Agreement for ForteHCM Products. UPON TERMINATION
                                OF THIS LICENSE AGREEMENT, YOU AGREE: (1) TO REMOVE OR UNINSTALL ALL COPIES OF THE
                                PRODUCT, AND ANY FILTERS, DIRECTORS, AND AGENTS THAT MAY HAVE BEEN PROVIDED TO YOU;
                                and (2) TO RETURN, IF REQUESTED, ALL COPIES OF THE PRODUCT, ANY FILTERS, DIRECTORS,
                                AND AGENTS THAT MAY HAVE BEEN PROVIDED TO YOU, THE MEDIA, AND ALL RELATED MATERIALS
                                AND DOCUMENTATION OR TO DESTROY THE SAME AND CERTIFY, IF REQUESTED, TO Forte HCM,
                                Inc. , IN WRITING THAT YOU HAVE DONE SO.
                                <br />
                                This License Agreement constitutes the entire agreement between the parties with
                                respect to the use of the Product, and supersedes all prior or contemporaneous understandings
                                or agreements, written or oral, regarding such subject matter. No amendment to or
                                modification of this License Agreement will be binding unless documented in writing
                                and signed by a duly authorized representative of Forte HCM Inc. This License Agreement
                                shall be governed by the laws of the State of Illinois. If any provision of this
                                License Agreement is held to be unenforceable, such provision shall be reformed
                                only to the extent necessary to make it enforceable and the other provisions shall
                                remain in full force and effect </span>
                            <br />
                            <br />
                            <span class="license_agreement_title">PRIVACY STATEMENT </span>
                            <br />
                            <span class="license_agreement_regular">Your privacy is very important. We recognize
                                that when you choose to provide us with information about yourself, you trust us
                                to act in a responsible manner. We believe the information you provide should only
                                be- used to help us provide you with better service. This Privacy Statement addresses
                                the Site's practices regarding information collected directly through or from the
                                Site. It does not address or govern any information gathering, use, or dissemination
                                practices related to information collected through other means or media. Candidate
                                test scores may be collated along with your resume to provide meaningful info about
                                you to potential hirers. Customer acknowledges that Forte HCM Inc may have right
                                to reuse customer assessment content in case of free subscription.
                                <br />
                                Non-identifiable visitor information and data which visitors </span><span class="license_agreement_regular_underline">voluntarily</span><span class="license_agreement_regular"> provide
                                to Forte HCM Inc. is the property of FHCM & may be provided to third parties for
                                business purposes. In addition, identifiable or non-identifiable data also may be
                                disclosed or distributed to another party pursuant to a subpoena, court order, or
                                other form of legal process, or if determined by Forte HCM, Inc. in its sole judgment
                                that such disclosure or distribution is appropriate to protect the life, health,
                                or property of ForteHCM, Inc. or any other person or entity. </span>
                            <br />
                            <br />
                            <span class="license_agreement_bold">What personal information do we collect on our
                                Site?</span>
                            <br />
                            <span class="license_agreement_regular">We gather three types of information on the
                                Site: (1) email and any other information you provide when you register (2) credit
                                card information you use to pay for assessment and (3) post-test data. 
                               <br/>Your credit
                                card Information will not be sold, rented or given away to any third party firm
                                without your written permission. </span>
                            <br />
                            <br />
                            <span class="license_agreement_bold">Optional personal information for registration
                                purposes:</span>
                            <br />
                            <span class="license_agreement_regular">To use certain features on the Site, we ask
                                you to register. When you do this, we collect basic information about you such as
                                your email address and profession. This information will only be used to correspond
                                & demo products to you. </span>
                            <br />
                            <span class="license_agreement_regular">If you are a candidate registering to take our
                                free tests, your candidate information (test data, credentials, skills domain…etc.)
                                may be used for hiring purposes or we may pass on your information to potential
                                hirers who may reach out to you. </span>
                            <br />
                            <br />
                            <span class="license_agreement_bold">Links to other Web sites </span>
                            <br />
                            <span class="license_agreement_regular">The Site may provide links to other sites, and
                                advertisements for or placed by third parties. Please be aware that in such a scenario
                                we are not responsible for the privacy practices (or other practices, goods, services,
                                or content) of other sites, advertisers, or third parties. It is possible that these
                                links or advertisements, themselves, may be used by third parties or others to collect
                                personal or other information about site visitors. We encourage you to review the
                                privacy policies of these other sites and of these advertisers and third parties.
                            </span>
                        </p>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="2" cellspacing="2">
                        <tr>
                            <td>
                                <asp:Button ID="LicenseAgreement_agreeButton" runat="server" Text="Agree" SkinID="sknButtonId"
                                    OnClick="LicenseAgreement_agreeButton_Click" ToolTip="Click here to agree the license agreement" />
                            </td>
                            <td>
                                <asp:Button ID="LicenseAgreement_disagreeButton" runat="server" Text="Disagree" SkinID="sknButtonId"
                                    OnClick="LicenseAgreement_disagreeButton_Click" ToolTip="Click here to disagree the license agreement. You will not be allowed to proceed further" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:Label ID="LicenseAgreement_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                    <asp:Label ID="LicenseAgreement_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="LicenseAgreement_showTimerHiddenField" runat="server" />
                    <div id="LicenseAgreement_timerDiv" style="display: none; height: 100px; width: 320px;
                        left: 350px; top: 240px; z-index: 1; position: absolute" class="popupcontrol_confirm"
                        runat="server">
                        <table width="100%" cellpadding="10" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <table cellpadding="10" cellspacing="0" border="0">
                                        <tr>
                                            <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                <table width="100%" cellspacing="0" border="0">
                                                    <tr>
                                                        <td align="center" class="label_field_text">
                                                            <asp:Label ID="LicenseAgreement_remainingtimeLabel" runat="server" SkinID="sknAgreementTimerValueLabelFieldText"
                                                                Text="10"></asp:Label>
                                                            <asp:Label ID="LicenseAgreement_remainingTimeTextHeadLabel" runat="server" Text=" sec to redirect to XXX page."
                                                                SkinID="sknAgreementTimerLabelFieldText"></asp:Label>
                                                            <asp:Label ID="LicenseAgreement_clickMessageLabel" runat="server" Text="Click " SkinID="sknAgreementTimerLabelFieldText"></asp:Label>
                                                            <asp:LinkButton ID="LicenseAgreement_navigateLinkButton" runat="server" SkinID="sknAgreementTimerLinkButton"
                                                                Text="here" OnClick="LicenseAgreement_navigateLinkButton_Click">
                                                            </asp:LinkButton>
                                                            <asp:Label ID="LicenseAgreement_clickRemainingTextHeadLabel" runat="server" Text=" to redirect to XXX page."
                                                                SkinID="sknAgreementTimerLabelFieldText"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_20">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
