﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ActivateSubscription.cs
// File that represents the user interface for activating the 
// user account.

#endregion Header

#region Namespace                                                              

using System;
using System.Web.Configuration;

using Resources;
using Forte.HCM.BL;
using Forte.HCM.Utilities;

#endregion Namespace

namespace Forte.HCM.UI.Subscription
{
    public partial class ActivateSubscription : System.Web.UI.Page
    {

        #region Events                                                         

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString.Keys.Count == 0)
                {
                    RedirectToLoginPage();
                    return;
                }
                if (string.IsNullOrEmpty(Request.QueryString[WebConfigurationManager.AppSettings["SIGNUP_USERNAME"]])
                    || string.IsNullOrEmpty(Request.QueryString[WebConfigurationManager.AppSettings["SIGNUP_CONFIRMATION"]]))
                {
                    RedirectToLoginPage();
                    return;
                }
                else
                {
                    if (Forte_ActivateSubscription_statusHidden.Value == "do")
                    {
                        RedirectToLoginPage();
                        return;
                    }
                    if (IsPostBack)
                        return;
                    CheckAndActivateAccount(
                        Request.QueryString[WebConfigurationManager.AppSettings["SIGNUP_USERNAME"]],
                        Request.QueryString[WebConfigurationManager.AppSettings["SIGNUP_CONFIRMATION"]]);
                }
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void Forte_ActivateSubscription_homeLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("../Default.aspx", false);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        #endregion Events

        #region Private Methods                                                

        private void CheckAndActivateAccount(string EncryptedUserName, string EncryptedConfirmationCode)
        {
            try
            {
                ShowSuccessMessage(
                    string.Format(HCMResource.ActivateSubscription_AccountActivated,
                    CheckAndActivateUserAccount(DecryptString(ref EncryptedUserName), ref EncryptedConfirmationCode)));
            }
            catch (FormatException)
            {
                ShowErrorMessage(new Exception("Invalid user name"));
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
            finally
            {
                Forte_ActivateSubscription_statusHidden.Value = "start";
                Forte_ActivateSubscription_timingDiv.Style.Add("display", "block");
            }
        }

        private string CheckAndActivateUserAccount(string User_Email, ref string EncryptedConfirmationCode)
        {
            return new UserRegistrationBLManager().CheckForActivationCode(User_Email, EncryptedConfirmationCode.Replace(' ', '+'));
        }

        private string DecryptString(ref string EncryptedString)
        {
            return new EncryptAndDecrypt().DecryptString(EncryptedString.Replace(' ', '+'));
        }

        private void RedirectToLoginPage()
        {
            Response.Redirect("../Default.aspx", false);
        }

        private void ShowErrorMessage(Exception exp)
        {
            if (string.IsNullOrEmpty(Forte_ActivateSubscription_topErrorMessageLabel.Text))
                Forte_ActivateSubscription_topErrorMessageLabel.Text = exp.Message;
            else
                Forte_ActivateSubscription_topErrorMessageLabel.Text += exp.Message;
        }

        private void ShowSuccessMessage(string Message)
        {
            if (string.IsNullOrEmpty(Forte_ActivateSubscription_topSuccessMessageLabel.Text))
                Forte_ActivateSubscription_topSuccessMessageLabel.Text = Message;
            else
                Forte_ActivateSubscription_topSuccessMessageLabel.Text += Message;
        }

        #endregion Private Methods
    }
}