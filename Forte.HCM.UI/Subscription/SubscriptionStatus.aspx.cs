﻿#region Namespace                                                              

using System;
using System.Web;

using Resources;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;

#endregion Namespace

namespace Forte.HCM.UI.Subscription
{
    public partial class SubscriptionStatus : PageBase
    {
        #region Events                                                         

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Master.SetPageCaption(HCMResource.SubscriptionStatus_PageTitle);
                if (IsPostBack)
                    return;
                if (Utility.IsNullOrEmpty(Session["Subscription_Status"]))
                {
                    RedirectToLoginPage();
                    return;
                }
                ShowSuccessMessage();
                Session["Subscription_Status"] = null; 
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        #endregion Events

        #region Private Methods                                                

        /// <summary>
        /// A Method that redirects to home page
        /// </summary>
        private void RedirectToLoginPage()
        {
            Response.Redirect("~/Default.aspx", false);
        }

        /// <summary>
        /// A Method that shows the success message
        /// </summary>
        private void ShowSuccessMessage()
        {
            // Check if the subscription is corporate.
            if (Request.QueryString["corporate"] != null &&
                Request.QueryString["corporate"].ToUpper().Trim() == "Y")
            {
                base.ShowMessage(SubscriptionStatus_topSuccessMessageLabel, @"Your account created successfully <br/ >
                    An activation request has been sent to the administrator <br /> Your request for account will be 
                    processed soon and you will get an indication mail on this");
            }
            else
            {
                base.ShowMessage(SubscriptionStatus_topSuccessMessageLabel, @"Your account created successfully <br/ >
                    An activation URL has been sent to your email  <br /> Please open your inbox and click
                    on the desired link on the corresponding email to activate your account");
            }
        }

        /// <summary>
        /// A Method that shows error message to the user
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the error message
        /// that should shown to the user
        /// </param>
        private void ShowErrorMessage(string Message)
        {
            base.ShowMessage(SubscriptionStatus_topErrorMessageLabel, Message);
        }

        /// <summary>
        /// A Method that loggs the exception object and shows the 
        /// error message to the user
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception object
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            Logger.ExceptionLog(exp);
            ShowErrorMessage(exp.Message);
        }

        #endregion Private Methods

        #region Override Methods                                               
        
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Override Methods
    }
}