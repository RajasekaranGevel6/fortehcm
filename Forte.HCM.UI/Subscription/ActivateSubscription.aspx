﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ActivateSubscription.aspx.cs"
    Inherits="Forte.HCM.UI.Subscription.ActivateSubscription" MasterPageFile="~/MasterPages/SubscriptionMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var Label = '<%= Forte_ActivateSubscription_remainingtimeLabel.ClientID %>';
        var Hidden = '<%= Forte_ActivateSubscription_statusHidden.ClientID %>';
        var Count = 10;
        var t;
        function onLoadPage() {
            if (document.getElementById(Hidden).value = 'start') {
                ShowRemainingTime();
            }
        }
        function ShowRemainingTime() {
            Count -= 1;
            document.getElementById(Label).innerHTML = Count;
            if (Count == 0) {
                document.getElementById(Hidden).value = "do";
                clearTimeout(t);
                __doPostBack(null, null);
            }
            else
                t = setTimeout(ShowRemainingTime, 1000);
        }
        onload = onLoadPage;
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubscriptionMaster_body" runat="server">
    <center>
        <div>
            <asp:Label ID="Forte_ActivateSubscription_topErrorMessageLabel" runat="server" EnableViewState="false"
                Width="100%" ForeColor="Red"></asp:Label>
            <asp:Label ID="Forte_ActivateSubscription_topSuccessMessageLabel" runat="server"
                EnableViewState="false" Width="100%" SkinID="sknSuccessMessage"></asp:Label>
        </div>
        <input type="hidden" runat="server" id="Forte_ActivateSubscription_statusHidden" />
        <div id="Forte_ActivateSubscription_timingDiv" runat="server" style="display: none;">
            <br />
            <asp:Label ID="Forte_ActivateSubscription_remainingtimeLabel" runat="server" SkinID="sknLabelFieldText"
                Text="10"></asp:Label>
            <asp:Label ID="Forte_ActivateSubscription_remainingTimeTextHeadLabel" runat="server"
                Text=" sec to redirect to login page" SkinID="sknLabelFieldText"></asp:Label>
            <p>
                <asp:Label ID="Forte_ActivateSubscription_clickMessageLabel" runat="server"
                    Text="Click "></asp:Label>
                <asp:LinkButton ID="Forte_ActivateSubscription_homeLinkButton" runat="server" SkinID="sknActionLinkButton"
                    Text="here" OnClick="Forte_ActivateSubscription_homeLinkButton_Click">
                </asp:LinkButton>
                <asp:Label ID="Forte_ActivateSubscription_clickRemainingTextHeadLabel" runat="server"
                    Text=" to redirect to login page."></asp:Label>
            </p>
        </div>
    </center>
</asp:Content>
