﻿
#region Namespace                                                              

using System;
using System.Web;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

using Resources;
using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Namespace

namespace Forte.HCM.UI.Subscription
{
    public partial class SubscriptionType : PageBase
    {

        #region Variables                                                      

        /// <summary>
        /// A <see cref="System.String"/> that holds the string value
        /// used to store the selected subscription type
        /// </summary>
        private const string REGISTRATIONTYPE = "SELECTEDREGISTRATIONTYPE";

        /// <summary>
        /// A <see cref="System.String"/> that holds the user detail
        /// session variable
        /// </summary>
        private const string USER_DETAIL_SESSION_VARIABLE = "USER_DETAIL";

        #endregion Variables

        #region Events                                                         

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LoadValues();
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void ConfirmMessageControl_OkButtonClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/CustomerAdminHome.aspx", false);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void UpgradeAccount_CancelClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Admin/UpgradeAccount.aspx?m=0&s=2", false);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void lnkFooter_Click(object sender, EventArgs e)
        {
            try
            {
                string commandName = ((LinkButton)sender).CommandName;

                string []commandValues = commandName.Split(new char[]{','});

                if (commandValues.Length <= 2)
                    return;

                ShowOrRedirectSubscriptionStatus(commandValues[0],
                    Convert.ToInt32(commandValues[1]), true,
                    Convert.ToInt32(commandValues[2]));
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SubscriptionType_RegisterUser(object sender, EventArgs e)
        {
            try
            {
                string CommandName = ((Button)sender).CommandName;
                ShowOrRedirectSubscriptionStatus(CommandName.Substring(0, CommandName.IndexOf(",")),
                    Convert.ToInt32(CommandName.Substring(CommandName.IndexOf(',') + 1)), false, null);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        #endregion Events

        #region Private Methods                                                

        private void ShowOrRedirectSubscriptionStatus(string SubscriptionName, int SubscriptionId, Boolean isTrial, int? trialDays)
        {
            if (Session[USER_DETAIL_SESSION_VARIABLE] != null)
            {
                UserRegistrationInfo userRegistrationInfo = new UserRegistrationInfo();
                userRegistrationInfo.SubscriptionName = SubscriptionName;
                userRegistrationInfo.SubscriptionId = SubscriptionId;
                userRegistrationInfo.UserID = base.userID;
                userRegistrationInfo.IsTrial = Convert.ToInt16(isTrial ? 1 : 0);
                if (isTrial)
                    userRegistrationInfo.TrialDays = trialDays;
                SubscriptionType_UpgradeAccountConfirmationControl.SubscriptionDetailsDataSource = userRegistrationInfo;
                SubscriptionType_upgradeAccountConfirmationPopupExtender.Show();
            }
            else
            {
                UserRegistrationInfo userInfo = new UserRegistrationInfo();
                userInfo.SubscriptionId = SubscriptionId;
                userInfo.SubscriptionName = SubscriptionName;
                userInfo.IsTrial = Convert.ToInt16(isTrial ? 1 : 0);
                userInfo.TrialDays = trialDays;
                Session[REGISTRATIONTYPE] = userInfo;
                Response.Redirect("~/Subscription/Signup.aspx", false);
            }
        }

        private void ShowErrorMessage(Exception exp)
        {
            Logger.ExceptionLog(exp);
            base.ShowMessage(SubscriptionType_topErrorMessageLabel,
                SubscriptionType_bottomErrorMessageLabel, exp.Message);
        }
       
        private void LoadSusbcriptionTypes()
        {
            List<SubscriptionFeatures> subscriptionFeatures = null;
            List<SubscriptionFeatureTypes> subscriptionFeatureTypes = new List<SubscriptionFeatureTypes>();
            List<SubscriptionTypes> subscriptionTypes = new List<SubscriptionTypes>();
            SubscriptionTypes subscriptionType = null;
            SubscriptionFeatureTypes subscriptionFeatureType = null;
            try
            {
                int freeSubscriptionTrialDays, freeStandardSubscriptionTrialDays, freeCorporateSubscriptonTrialDays;
                subscriptionFeatures = GetSubscriptionWithFeaturesList(out freeSubscriptionTrialDays, out freeStandardSubscriptionTrialDays, out freeCorporateSubscriptonTrialDays);
                var FeatureList = (from FIDS in subscriptionFeatures
                                   select new { FIDS.FeatureId, FIDS.FeatureName }).Distinct().ToList();
                foreach (var FL in FeatureList)
                {
                    subscriptionFeatureType = new SubscriptionFeatureTypes();
                    subscriptionFeatureType.FeatureId = FL.FeatureId;
                    subscriptionFeatureType.FeatureName = FL.FeatureName;
                    subscriptionFeatureTypes.Add(subscriptionFeatureType);
                    subscriptionFeatureType = null;
                }
                int UserSubscriptionId = 0;
                if (Session[USER_DETAIL_SESSION_VARIABLE] != null)
                    UserSubscriptionId = (Session[USER_DETAIL_SESSION_VARIABLE] as UserDetail).SubscriptionId;
                SubscriptionType_subscriptionGridView.DataSource = subscriptionFeatureTypes;
                SubscriptionType_subscriptionGridView.DataBind();
                HtmlTableCell Headertc = null;
                HtmlTableRow Headertr = new HtmlTableRow();
                Label HeaderLabel = null;
                HtmlTableCell Footertc = null;
                HtmlTableRow Footertr = new HtmlTableRow();
                HtmlTableRow FooterLinktr = new HtmlTableRow();
                Button btnFooter = null;
                LinkButton lnkFooter = null;
                var SubscriptionIds = (from SIDS in subscriptionFeatures
                                       select new { SIDS.SubscriptionId, SIDS.SubscriptionName }).Distinct().ToList();
                foreach (var SL in SubscriptionIds)
                {
                    subscriptionType = new SubscriptionTypes();
                    subscriptionType.SubscriptionId = SL.SubscriptionId;
                    subscriptionType.SubscriptionName = SL.SubscriptionName;
                    subscriptionTypes.Add(subscriptionType);
                    subscriptionType = null;
                    HeaderLabel = new Label();
                    HeaderLabel.ID = "SubscriptionType_" + SL.SubscriptionName.Trim().Replace(' ', '_') + "HeaderLabel";
                    HeaderLabel.Text = SL.SubscriptionName;
                    Headertc = new HtmlTableCell();
                    Headertc.Controls.Add(HeaderLabel);
                    Headertc.Width = "20%";
                    Headertc.Attributes.Add("align", "left");
                    Headertr.Cells.Add(Headertc);

                    lnkFooter = new LinkButton();
                    lnkFooter.ID = "SubscriptionType_" + SL.SubscriptionName.Trim().Replace(' ', '_') + "_LinkButton";
                    lnkFooter.Text = "Try It Free ! ";

                    lnkFooter.CommandName = SL.SubscriptionName + "," + SL.SubscriptionId;


                    lnkFooter.Click += new EventHandler(lnkFooter_Click);
                    Footertc = new HtmlTableCell();
                    Footertc.Width = "20%";
                    Footertc.Attributes.Add("align", "left");
                    if (UserSubscriptionId == SL.SubscriptionId)
                        lnkFooter.Enabled = false;
                    else
                    {
                        lnkFooter.Enabled = true;
                        lnkFooter.SkinID = "sknActionLinkButton";
                    }

                    if (SL.SubscriptionName == "Free")
                        lnkFooter.Visible = false;

                        /*
                         <asp:ImageButton ID="SingleQuestionEntry_complexityImageButton" SkinID="sknHelpImageButton"
                                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="Please select the complexity of the question here" />
                         */

                    Footertc.Controls.Add(lnkFooter);

                    ImageButton helpIcon = null;

                    switch (SL.SubscriptionName)
                    {
                        case "Standard":
                            lnkFooter.CommandName += "," + freeStandardSubscriptionTrialDays;
                            helpIcon = new ImageButton();
                            helpIcon.SkinID = "sknHelpImageButton";
                            helpIcon.ToolTip = string.Format("Trial period available for {0} days", freeStandardSubscriptionTrialDays);
                            helpIcon.OnClientClick = "javascript:return false;";
                            Footertc.Controls.Add(helpIcon);
                            break;
                        case "Corporate":
                            lnkFooter.CommandName += "," + freeCorporateSubscriptonTrialDays;
                            helpIcon = new ImageButton();
                            helpIcon.SkinID = "sknHelpImageButton";
                            helpIcon.ToolTip = string.Format("Trial period available for {0} days", freeCorporateSubscriptonTrialDays);
                            helpIcon.OnClientClick = "javascript:return false;";
                            Footertc.Controls.Add(helpIcon);
                            break;
                        default:
                            break;
                    }

                    FooterLinktr.Cells.Add(Footertc);
                    Footertc = null;
                    //
                    btnFooter = new Button();
                    btnFooter.ID = "SubscriptionType_" + SL.SubscriptionName.Trim().Replace(' ', '_') + "_Button";
                    btnFooter.Click += new EventHandler(SubscriptionType_RegisterUser);
                    switch (SL.SubscriptionName)
                    {
                        case "Free":
                            btnFooter.SkinID = "sknSignUpfreedButton";
                            btnFooter.BackColor = System.Drawing.Color.Transparent;
                            btnFooter.CommandName = SL.SubscriptionName + "," + SL.SubscriptionId;
                            btnFooter.BorderStyle = BorderStyle.None;
                            break;
                        case "Standard":
                            btnFooter.SkinID = "sknSignUpstandardButton";
                            btnFooter.BackColor = System.Drawing.Color.Transparent;
                            btnFooter.CommandName = SL.SubscriptionName + "," + SL.SubscriptionId;
                            btnFooter.BorderStyle = BorderStyle.None;
                            break;
                        case "Corporate":
                            btnFooter.SkinID = "sknSignUpcorporateButton";
                            btnFooter.BackColor = System.Drawing.Color.Transparent;
                            btnFooter.CommandName = SL.SubscriptionName + "," + SL.SubscriptionId;
                            btnFooter.BorderStyle = BorderStyle.None;
                            break;
                        default:
                            btnFooter.SkinID = "sknButtonId";
                            btnFooter.Text = SL.SubscriptionName.Trim();
                            btnFooter.CommandName = btnFooter.Text + "," + SL.SubscriptionId;
                            break;
                    }
                    //btnFooter.SkinID = "sknButtonId";
                    Footertc = new HtmlTableCell();
                    Footertc.Width = "20%";
                    Footertc.Attributes.Add("align", "left");
                    if (UserSubscriptionId == SL.SubscriptionId)
                        btnFooter.Enabled = false;
                    else
                        btnFooter.Enabled = true;
                    Footertc.Controls.Add(btnFooter);
                    Footertr.Cells.Add(Footertc);
                }
                ((HtmlTable)SubscriptionType_subscriptionGridView.HeaderRow.FindControl("SubscriptionType_subscriptionNameHeaderTable")).Rows.Add(Headertr);
                ((HtmlTable)SubscriptionType_subscriptionGridView.FooterRow.FindControl("SubscriptionType_SubscriptionFooterTable")).Rows.Add(FooterLinktr);
                //((HtmlTable)SubscriptionType_subscriptionGridView.FooterRow.FindControl("SubscriptionType_SubscriptionFooterTable")).Rows.Add(FooterEmptytr);
                ((HtmlTable)SubscriptionType_subscriptionGridView.FooterRow.FindControl("SubscriptionType_SubscriptionFooterTable")).Rows.Add(Footertr);
                HtmlTableRow tr = null;
                HtmlTableCell tc = null;
                Label lbl = null;
                int FeatureId;
                Label lblFeatureValue = null;
                for (int i = 0; i < SubscriptionType_subscriptionGridView.Rows.Count; i++)
                {
                    FeatureId = Convert.ToInt32(((Label)SubscriptionType_subscriptionGridView.Rows[i].FindControl("SubscriptionType_featureIDLabel")).Text);
                    lblFeatureValue = (Label)SubscriptionType_subscriptionGridView.Rows[i].FindControl("SubscriptionType_featureNameLabel");
                    tr = new HtmlTableRow();
                    for (int j = 0; j < subscriptionTypes.Count; j++)
                    {
                        tc = new HtmlTableCell();
                        lbl = new Label();
                        lbl.ID = "SubscriptionTypes_" + lblFeatureValue.Text.Trim().Replace(' ', '_') + "_Value";
                        lbl.Text = Enumerable.Where(subscriptionFeatures, p => p.SubscriptionId == subscriptionTypes[j].SubscriptionId && p.FeatureId == FeatureId).ToList()[0].FeatureValue;
                        tc.Controls.Add(lbl);
                        tc.Width = "20%";
                        tr.Cells.Add(tc);
                        tc = null;
                    }
                    ((HtmlTable)SubscriptionType_subscriptionGridView.Rows[i].FindControl("SubscriptionType_featureValueTable")).Rows.Add(tr);
                }
            }
            finally
            {
                if (subscriptionFeatures != null) subscriptionFeatures = null;
                if (subscriptionFeatureTypes != null) subscriptionFeatureTypes = null;
                if (subscriptionTypes != null) subscriptionTypes = null;
                if (subscriptionType != null) subscriptionType = null;
                if (subscriptionFeatureType != null) subscriptionFeatureType = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        private List<SubscriptionFeatures> GetSubscriptionWithFeaturesList(out int freeSubscriptionTrialDays, out int freeStandardSubscriptionTrialDays, out int freeCorporateSubscriptonTrialDays)
        {
            return new SubscriptionBLManager().GetSubscriptionWithFeatures(out freeSubscriptionTrialDays, out freeStandardSubscriptionTrialDays, out freeCorporateSubscriptonTrialDays);
        }

        #endregion Private Methods

        #region Public Methods                                                 

        /// <summary>
        /// A Method that invokes the upgrade subscription modal pop up
        /// </summary>
        public void ShowUpgradeModalPopUp()
        {
            SubscriptionType_upgradeAccountConfirmationPopupExtender.Show();
        }

        /// <summary>
        /// A Method that invokes the subscription updated 
        /// confirmation modal pop up extender
        /// </summary>
        public void InvokeSubscriptionUpdateModalPopUp()
        {
            SubscriptionType_confirmSubscriptionChangedModalPopupExtender.Show();
        }

        #endregion Public Methods

        #region Override Methods                                               

        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            Master.SetPageCaption(HCMResource.SubscriptionType_SubscriptionPageTitle);
            LoadSusbcriptionTypes();
            if (Session[USER_DETAIL_SESSION_VARIABLE] == null)
                SubscriptionType_backToLoginButton.Visible = true;
            else
                SubscriptionType_backToLoginButton.Visible = false;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (Session[REGISTRATIONTYPE] == null)
                return;
            //string CommandName = Session[REGISTRATIONTYPE].ToString();
            //ShowOrRedirectSubscriptionStatus(CommandName.Substring(0, CommandName.IndexOf(",")),
                    //Convert.ToInt32(CommandName.Substring(CommandName.IndexOf(',') + 1)));
        }

        #endregion Override Methods

    }
}