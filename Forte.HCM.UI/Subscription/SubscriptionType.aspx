﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubscriptionType.aspx.cs"
    Inherits="Forte.HCM.UI.Subscription.SubscriptionType" MasterPageFile="~/MasterPages/SubscriptionMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/SubscriptionMaster.Master" %>
<%@ Register Src="../CommonControls/UpgradeAccountConfirmationControl.ascx" TagName="UpgradeAccountConfirmationControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubscriptionMaster_body" runat="server">
    <div id="Div2" runat="server" style="margin: auto; width: 100%; height: 100%; background-position: center;">
        <table cellpadding="0" cellspacing="0" border="0" width="995px" style="height: 100%">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="height: 530px">
                        <tr>
                            <td align="center" class="td_padding_10_hcm">
                                <table cellpadding="0" cellspacing="0" border="0" width="98%">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="SubscriptionType_backToLoginButton" runat="server" PostBackUrl="~/Default.aspx"
                                                CommandName="Back to Login" SkinID="sknSignUpbackButton" BackColor="Transparent"
                                                BorderStyle="None" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <asp:Label ID="SubscriptionType_topErrorMessageLabel" runat="server" Width="100%"
                                                    EnableViewState="false" SkinID="sknErrorMessage"></asp:Label>
                                                <asp:Label ID="ForteHcm_RegistartionType_topSuccessMessageLabel" runat="server" Width="100%"
                                                    EnableViewState="false" SkinID="sknSuccessMessage"></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_bottomborder">
                                            <div style="overflow: auto; width: 99%;">
                                                <asp:GridView ID="SubscriptionType_subscriptionGridView" runat="server" AutoGenerateColumns="false"
                                                    AllowPaging="false" AllowSorting="false" ShowFooter="true" ShowHeader="true"
                                                    SkinID="sknSubscriptionTypeGridView">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Feature">
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="SubscriptionType_featureIDLabel" runat="server" Text='<%# Eval("FeatureId") %>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:Label ID="SubscriptionType_featureNameLabel" runat="server" Text='<%# Eval("FeatureName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterStyle HorizontalAlign="Left" />
                                                            <FooterTemplate>
                                                                <asp:Label ID="SubscriptionType_registerUsingLabel" runat="server" Text="Register Using"></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <table width="100%" id="SubscriptionType_subscriptionNameHeaderTable" runat="server"
                                                                    cellpadding="0" border="0" cellspacing="0">
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <table width="100%" id="SubscriptionType_featureValueTable" cellpadding="0" border="0"
                                                                    cellspacing="0" runat="server">
                                                                </table>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <table width="100%" id="SubscriptionType_SubscriptionFooterTable" runat="server"
                                                                    cellpadding="0" cellspacing="0" border="0">
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div style="width: 100%; height: 100%">
        <asp:Label ID="SubscriptionType_bottomErrorMessageLabel" runat="server" Width="100%"
            EnableViewState="false" SkinID="sknErrorMessage"></asp:Label>
        <asp:Label ID="SubscriptionType_bottomSuccessMessageLabel" runat="server" Width="100%"
            EnableViewState="false" SkinID="sknSuccessMessage"></asp:Label>
    </div>
    <div>
        <table width="90%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:Panel ID="SubscriptionType_upgradeAccountPopupPanel" runat="server" Style="display: none"
                        CssClass="popupcontrol_upgrade_preview">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div style="display: none">
                                        <asp:Button ID="SubscriptionType_hiddenPopupModalButton" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_20" colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <table width="90%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="width: 85%" class="popup_header_text" valign="middle" align="left">
                                                <asp:Label ID="SubscriptionType_literal" runat="server" Text="Upgrade Account"></asp:Label>
                                            </td>
                                            <td style="width: 15%" align="right">
                                                <asp:ImageButton ID="SubscriptionType_closeImageButton" runat="server" SkinID="sknCloseImageButton"
                                                    OnClick="UpgradeAccount_CancelClick" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_20" colspan="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <uc1:UpgradeAccountConfirmationControl ID="SubscriptionType_UpgradeAccountConfirmationControl"
                                                    runat="server" />
                                                <ajaxToolKit:ModalPopupExtender ID="SubscriptionType_upgradeAccountConfirmationPopupExtender"
                                                    runat="server" PopupControlID="SubscriptionType_upgradeAccountPopupPanel" TargetControlID="SubscriptionType_hiddenPopupModalButton"
                                                    BackgroundCssClass="modalBackground">
                                                </ajaxToolKit:ModalPopupExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_5">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="2">
                                                <asp:LinkButton ID="SubscriptionType_cancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknPopupLinkButton" OnClick="UpgradeAccount_CancelClick"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="SubscriptionType_confirmSubscriptionChangedPanel" runat="server" Style="display: none;
                        height: 220px;" CssClass="popupcontrol_confirm">
                        <div id="SubscriptionType_confirmSubscriptionChangedDiv" style="display: none">
                            <asp:Button ID="SubscriptionType_confirmSubscriptionChanged_hiddenButton" runat="server" />
                        </div>
                        <uc2:ConfirmMsgControl ID="SubscriptionType_confirmSubscriptionChangedConfirmMsgControl"
                            runat="server" Title="Subscription Status" Type="Ok" Message="Subscription Details Updated"
                            OnOkClick="ConfirmMessageControl_OkButtonClick" />
                    </asp:Panel>
                    <ajaxToolKit:ModalPopupExtender ID="SubscriptionType_confirmSubscriptionChangedModalPopupExtender"
                        runat="server" PopupControlID="SubscriptionType_confirmSubscriptionChangedPanel"
                        TargetControlID="SubscriptionType_confirmSubscriptionChanged_hiddenButton" BackgroundCssClass="modalBackground">
                    </ajaxToolKit:ModalPopupExtender>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
