﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SubscriptionMaster.Master" AutoEventWireup="true" CodeBehind="UserSignup.aspx.cs" Inherits="Forte.HCM.UI.Subscription.UserSignup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubscriptionMaster_body" runat="server">
    <div id="Resource_Selection_cnt_outer">
        <div id="sign_outer">
            <div id="sign_up_band_bg">
                <div id="title_Resource_Selection">
                    Sign UP Today !
                </div>
                <div id="title_sign_up">
                    Talent Acquisition Infrastructure</div>
            </div>
            <div id="sign_inner">
                <div id="select_plan">
                    <ul>
                        <li>
                            <img src="../App_Themes/DefaultTheme/Images/icon_sign_up.png" /></li>
                        <li>Select a Forte HCM plan:</li>
                    </ul>
                </div>
                <div>
                    <asp:UpdatePanel ID="UserRegistration_topMessageLabelUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="UserRegistration_topErrorMessageLabel" runat="server" EnableViewState="false"
                                ForeColor="Red" Width="100%"></asp:Label>
                            <asp:Label ID="UserRegistration_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                Width="100%" ForeColor="Green"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="sing_left">
                    <div id="sing_left_title">
                        Business</div>
                    Business Edition Pricing<br>
                    <ul>
                        <li>sample_1</li>
                        <li>sample_2</li>
                        <li>sample_3</li>
                    </ul>
                    <div id="sing_left_btn">
                        <a href="FeaturesList.aspx">See all features</a></div>
                </div>
                <div id="sign_middle">
                    <div id="sing_middle_title">
                        Enterprise</div>
                    <span>Get in touch for Customized Quote</span>
                    <ul>
                        <li>sample_1</li>
                        <li>sample_2</li>
                        <li>sample_3</li>
                    </ul>
                    <div id="sign_middle_btn">
                        <a href="FeaturesList.aspx">See all features</a></div>
                </div>
                <div id="sign_form_outer">
                    <div id="sing_form_title">
                        Set up your account</div>
                    <div id="form_outer">
                        <div id="form_container">
                            <div id="form_container_left">
                                <asp:Label runat="server" ID="UserRegistration_firstNameHeaderLabel" Text="First Name :"></asp:Label>
                            </div>
                            <div id="form_container_right">
                                <asp:TextBox ID="UserRegistration_firstNameTextBox" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        
                    </div>
                    <div id="form_outer">
                        <div id="form_container">
                            <div id="form_container_left">
                                <asp:Label runat="server" ID="UserRegistration_lastNameHeaderLabel" Text="Last Name :"> </asp:Label>
                            </div>
                            <div id="form_container_right">
                                <asp:TextBox ID="UserRegistration_lastNameTextBox" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        
                    </div>
                    <div id="form_outer">
                        <div id="form_container">
                            <div id="form_container_left">
                                <asp:Label runat="server" ID="UserRegistration_userNameHeaderLabel">Email :</asp:Label>
                            </div>
                            <div id="form_container_right">
                                <asp:UpdatePanel ID="UserRegistration_userNameUpdatePanel" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="UserRegistration_userNameTextBox" runat="server" MaxLength="100"
                                            AutoCompleteType="None"></asp:TextBox>
                                        &nbsp;
                                        <asp:LinkButton ID="UserRegistration_checkUserEmailIdAvailableButton" CssClass="link_button_hcm"
                                            OnClick="UserRegistration_checkUserEmailIdAvailableButton_Click" runat="server"
                                            Text="Check"></asp:LinkButton>
                                        <div id="UserRegistration_userEmailAvailableStatusDiv" runat="server" style="display: none;">
                                            <asp:Label ID="UserRegistration_inValidEmailAvailableStatus" runat="server" Width="100%"
                                                EnableViewState="false" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="UserRegistration_validEmailAvailableStatus" runat="server" Width="100%"
                                                EnableViewState="false" ForeColor="Green"></asp:Label>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                       
                    </div>
                    <div id="form_outer">
                        <div id="form_container">
                            <div id="form_container_left">
                                <asp:Label runat="server" ID="UserRegistration_passwordHeaderLabel" Text="Password :"></asp:Label>
                            </div>
                            <div id="form_container_right">
                                <asp:TextBox ID="UserRegistration_passwordTextBox" TextMode="Password" runat="server"></asp:TextBox>
                                <ajaxToolKit:PasswordStrength ID="UserRegistration_passwordTextPasswordStrength"
                                    runat="server" DisplayPosition="BelowRight" HelpHandlePosition="LeftSide" RequiresUpperAndLowerCaseCharacters="true"
                                    PreferredPasswordLength="6" HelpStatusLabelID="UserRegistration_passwordHelpLabel"
                                    StrengthIndicatorType="Text" TargetControlID="UserRegistration_passwordTextBox"
                                    TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent">
                                </ajaxToolKit:PasswordStrength>
                                <br />
                                <asp:Label ID="UserRegistration_passwordHelpLabel" runat="server"></asp:Label>
                            </div>
                        </div>
                        
                    </div>
                    <div id="form_outer">
                        <div id="form_container">
                            <div id="form_container_left">
                                <asp:Label runat="server" ID="UserRegistration_reEnterPasswordHeaderLabel" Text="Conform password:"></asp:Label>
                            </div>
                            <div id="form_container_right">
                                <asp:TextBox ID="UserRegistration_confrimPasswordTextBox" TextMode="Password" runat="server"></asp:TextBox>
                            </div>
                        </div>
                     
                    </div>
                    <div id="form_outer">
                        <div id="form_container">
                            <div id="form_container_left">
                                <asp:Label runat="server" ID="UserRegistration_phoneHeaderLabel" Text="Your Phone:"></asp:Label>
                            </div>
                            <div id="form_container_right">
                                <asp:TextBox ID="UserRegistration_phoneTextBox" CssClass="text_box" runat="server"></asp:TextBox>
                            </div>
                        </div>
                       
                    </div>
                    <div id="form_outer">
                        <div id="form_container">
                            <div id="form_container_left">
                                <input type="hidden" runat="server" id="UserRegistration_MaximumCorporateUsersHidden"
                                    value="0" />
                                <input type="hidden" runat="server" id="UserRegistration_subscriptionIdHidden" value="0" />
                                <input type="hidden" runat="server" id="UserRegistration_isTrialHidden" value="0" />
                                <input type="hidden" runat="server" id="UserRegistration_trialDaysHidden" />
                                <asp:UpdatePanel ID="UserRegistration_bottonMessageLabelUpdatePanel" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="UserRegistration_errorMessageLabel" runat="server" EnableViewState="false"
                                            ForeColor="Red" Width="100%" Visible="false"></asp:Label>
                                        <asp:Label ID="UserRegistration_bottomSuccessMessageLabel" runat="server" EnableViewState="False"
                                            Width="100%" ForeColor="Green" Visible="false"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div id="form_container_right">
                                <asp:Button ID="UserRegistration_registerButton" CssClass="form_btn" BackColor="Transparent"
                                    BorderStyle="None" runat="server" OnCommand="UserRegistration_registerButton_CommandClick" />
                            </div>
                        </div>
                      
                    </div>
                    <div id="form_txt">
                        By registering your agree to Forte HCM <a href="#">Terms of Services</a></div>
                </div>
                <div id="help_cnt">
                    Need Help deciding? Call 877-377-9681</div>
            </div>
        </div>
    </div>
</asp:Content>
