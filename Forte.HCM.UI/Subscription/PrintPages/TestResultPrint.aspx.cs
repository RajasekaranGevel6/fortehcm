﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestResultPrint.cs
// File that Represents the layout to print Test 
// result page. This page helps to view the  various statistics and count
// of the questions in the test

#endregion Header

#region Directives

using System;
using System.Web;
using System.Web.UI;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Configuration;
using System.Data;
using System.IO;
using Forte.HCM.Utilities;

#endregion

namespace Forte.HCM.UI.PrintPages
{
    /// <summary>
    /// Represents the class that holds the layout to print test result page.
    /// This page helps to view the  various statistics and count of the 
    /// questions in the test
    /// </summary>
    public partial class TestResultPrint : Page
    {
        #region Variables

        static BaseColor itemColor = new BaseColor(20, 142, 192);
        static BaseColor headerColor = new BaseColor(40, 48, 51);
        /// <summary>
        /// A <see cref="BaseColor"/> that holds the item black color.
        /// </summary>
        static BaseColor itemBlackColor = new BaseColor(0, 0, 0); 
        string chartImagePath = "";
 
        #endregion Variables

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                string baseURL = ConfigurationManager.AppSettings["DEFAULT_URL"];

                //Load Style Sheet and Skin
                if (!Utility.IsNullOrEmpty(baseURL))
                {
                    chartImagePath = baseURL + "chart/";
                    lnkStyleSheet.Href = baseURL +
                        "App_Themes/DefaultTheme/DefaultStyle.css";
                    lnkSkin.Href = baseURL +
                        "App_Themes/DefaultTheme/DefaultSkin.skin";
                }
                //Load Style Sheets and Skin
                if (!Utility.IsNullOrEmpty(baseURL))
                {
                    chartImagePath = baseURL + "chart/";
                    lnkStyleSheet.Href = baseURL +
                        "App_Themes/DefaultTheme/DefaultStyle.css";
                    lnkSkin.Href = baseURL +
                        "App_Themes/DefaultTheme/DefaultSkin.skin";
                }

                if (!IsPostBack)
                {
                    Response.Clear();

                    //Method to load the values in test statistics
                    LoadValues();

                    //Method to load the values in candidate statistics 
                    LoadCandidateStatistics();

                    // Print pdf
                    if (!Utility.IsNullOrEmpty(Request.QueryString["downloadoremail"]))
                    {
                        // Download PDF
                        if (Request.QueryString["downloadoremail"].ToUpper() == "D")
                         PrintPdfUsingiTextSharpTables(); 
                        if (Request.QueryString["downloadoremail"].ToUpper() == "INT")
                        { 
                            GeneretePDF_AssessorSummaryRating(Request.QueryString["candidatesession"].ToString(),
                                Convert.ToInt32(Request.QueryString["attemptid"]));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
            }
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Represents the method to load the candidate statistics details
        /// </summary>
        private void LoadCandidateStatistics()
        {
            string sessionKey = string.Empty;
            string testKey = string.Empty;
            string histogramSessionKey = string.Empty;
            int attemptID = 0;

            if ((Request.QueryString["candidatesession"] != null) &&
                (Request.QueryString["candidatesession"].ToString().Length != 0))
                sessionKey = Request.QueryString["candidatesession"].ToString();

            if ((Request.QueryString["attemptid"] != null) &&
                (Request.QueryString["attemptid"].ToString().Length != 0))
                attemptID = int.Parse(Request.QueryString["attemptid"].ToString());

            if ((Request.QueryString["testkey"] != null) &&
               (Request.QueryString["testkey"].ToString().Length != 0))
                testKey = Request.QueryString["testkey"].ToString();

            CandidateStatisticsDetail CandidateStatisticsDataSource =
                new ReportBLManager().GetCandidateTestResultStatisticsDetails(sessionKey, testKey, attemptID);

            LoadCandidateStatisticsDetails(CandidateStatisticsDataSource);

            TestResultPrint_testScoreImage.ImageUrl =
                        chartImagePath + Constants.ChartConstants.CHART_HISTOGRAM +
                        "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png";

            TestResultPrint_testScoreCategoryImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_CATEGORY +
                "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png";

            TestResultPrint_testScoreSubjectImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_SUBJECT +
                "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png";

            TestResultPrint_testAreaScoreImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_TESTAREA +
                "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png";
        }

        /// <summary>
        /// Method used to load the values
        /// </summary>
        private void LoadValues()
        {
            //Get the test id
            string testID = Request.QueryString["testkey"];
            //Get the test name   
            string testName;
            testName = new ReportBLManager().
                GetTestName(Request.QueryString["testkey"]);
            string candidateSessionId = null;
            string candidateName = null;

            if (Request.QueryString["candidatesession"] != null &&
                Request.QueryString["candidatesession"].ToString().Length != 0)
            {
                candidateSessionId = Request.QueryString["candidatesession"].ToString().Trim();
                //Get the CandidateName
                candidateName = new CandidateBLManager().GetCandidateName(candidateSessionId);
                LoadCandidateTestDetails
                    (testID, testName, candidateName, "0.00");
            }

            TestResultPrint_categoryStatisticsImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_CATEGORY + "-" + testID + ".png";
            TestResultPrint_subjectStatisticsImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_SUBJECT + "-" + testID + ".png";
            TestResultPrint_testAreaStatisticsImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_TESTAREA + "-" + testID + ".png";
            TestResultPrint_complexityStatisticsImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_COMPLEXITY + "-" + testID + ".png";
            LoadTestSummaryDetails(testID);
        }

        /// <summary>
        /// Represents the method to load candidate details
        /// </summary>
        /// <param name="testId">
        /// A<see cref="string"/>that holds the test id 
        /// </param>
        /// <param name="testName">
        /// A<see cref="string"/>that holds the test name
        /// </param>
        /// <param name="candidateName">
        /// A<see cref="string"/>that holds the candidate name
        /// </param>
        /// <param name="creditsEarned">
        /// A<see cref="string"/>that holds the credits earned
        /// </param>
        private void LoadCandidateTestDetails(string testId, string testName,
            string candidateName, string creditsEarned)
        {
            TestResultPrint_testIDValueLabel.Text = testId;
            TestResultPrint_testNameValueTextBox.Text = testName;
            TestResultPrint_candidateNameValueTextBox.Text = candidateName;
            TestResultPrint_creditsEarnedValueTextBox.Text = creditsEarned;
        }

        /// <summary>
        /// Represents the method to load the candidate test summary details
        /// </summary>
        /// <param name="testId">
        /// A<see cref="string"/>that holds the test id 
        /// </param>
        private void LoadTestSummaryDetails(string testID)
        {
            try
            {
                //Get the test details from database
                TestStatistics TestStatisticsDataSource =
                    new ReportBLManager().GetTestSummaryDetails(testID);
                //Get the general test statistics and load the 
                //test summary details
                TestResultPrint_testAuthorLabel.Text =
                    TestStatisticsDataSource.TestAuthor;
                TestResultPrint_highScoreLabel.Text =
                    TestStatisticsDataSource.HighestScore.ToString().Trim();
                TestResultPrint_noOfQuestionLabel.Text =
                    TestStatisticsDataSource.NoOfQuestions.ToString().Trim();
                TestResultPrint_lowScoreLabel.Text =
                    TestStatisticsDataSource.LowestScore.ToString().Trim();
                TestResultPrint_noOfCandidateLabel.Text =
                    TestStatisticsDataSource.NoOfCandidates.ToString().Trim();
                TestResultPrint_meanScoreLabel.Text
                    = TestStatisticsDataSource.MeanScore.ToString();
                TestResultPrint_avgTimeLabel.Text =
                    Utility.ConvertSecondsToHoursMinutesSeconds(
                    Convert.ToInt32(TestStatisticsDataSource.
                    AverageTimeTakenByCandidates));
                TestResultPrint_scoreSDLabel.Text =
                    TestStatisticsDataSource.StandardDeviation.ToString();
                TestResultPrint_scoreRangeLabel.Text = TestStatisticsDataSource.ScoreRange.ToString();
                TestResultPrint_categoryGridview.DataSource =
                   TestStatisticsDataSource.SubjectStatistics;
                TestResultPrint_categoryGridview.DataBind();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                throw exception;
            }
        }

        /// <summary>
        /// Represents the method to load the candidate statistics details
        /// </summary>
        /// <param name="CandidateStatisticsDataSource">
        /// A<see cref="CandidateStatisticsDetail"/>that holds the candidate statistics details 
        /// </param>
        private void LoadCandidateStatisticsDetails(CandidateStatisticsDetail CandidateStatisticsDataSource)
        {
            TestResultPrint_noOfQuestionLabelValue.Text =
                CandidateStatisticsDataSource.TotalQuestion.ToString();
            TestResultPrint_noOfQuestionattenedLabelValue.Text =
                CandidateStatisticsDataSource.TotalQuestionAttended.ToString();
            TestResultPrint_absoluteScoreLabelValue.Text =
                CandidateStatisticsDataSource.AbsoluteScore.ToString();
            TestResultPrint_noOfQuestionSkippedLabelValue.Text =
                CandidateStatisticsDataSource.TotalQuestionSkipped.ToString();
            TestResultPrint_relativeScoreLabelValue.Text = CandidateStatisticsDataSource.RelativeScore.ToString();
            TestResultPrint_noOfQuestionAnsweredCorrectlyLabelValue.Text =
                CandidateStatisticsDataSource.QuestionsAnsweredCorrectly.ToString();
            TestResultPrint_percentileLabelValue.Text
                = CandidateStatisticsDataSource.Percentile.ToString();
            TestResultPrint_noOfQuestionAnsweredWronglyLabelValue.Text
                = CandidateStatisticsDataSource.QuestionsAnsweredWrongly.ToString();
            TestResultPrint_noOfQuestionTimeTakenLabelValue.Text
                = CandidateStatisticsDataSource.TimeTaken;
            TestResultPrint_percentageQuestionsAnsweredCorrectlyLabelValue.Text
                = CandidateStatisticsDataSource.AnsweredCorrectly.ToString();
            TestResultPrint_myScoreLabelValue.Text =
                CandidateStatisticsDataSource.MyScore.ToString();
            decimal averageTimeTaken = 0;
            if (!Utility.IsNullOrEmpty(CandidateStatisticsDataSource.AverageTimeTaken)
                && CandidateStatisticsDataSource.AverageTimeTaken.Length != 0)
            {
                averageTimeTaken = decimal.Parse(CandidateStatisticsDataSource.AverageTimeTaken);
            }
            TestResultPrint_testStatusLabelValue.Text = CandidateStatisticsDataSource.TestStatus;
            TestResultPrint_avgTimeTakenLabelValue.Text =
                Utility.ConvertSecondsToHoursMinutesSeconds
                (decimal.ToInt32(averageTimeTaken));
        }

        /// <summary>
        /// Represents the method to download the page layout as pdf using Itext libraries
        /// </summary>       
        private void PrintPdfUsingiTextSharpTables()
        {
            string candSessionID = string.Empty;
            string candAttemptID = string.Empty;
            string fileName = string.Empty;
            string imagePath = string.Empty;
            string histogramSessionKey = string.Empty;

            if ((Request.QueryString["candidatesession"] != null) &&
                (Request.QueryString["candidatesession"].ToString().Length != 0))
                candSessionID = Request.QueryString["candidatesession"].ToString();
            if ((Request.QueryString["attemptid"] != null) &&
                (Request.QueryString["attemptid"].ToString().Length != 0))
                candAttemptID = Request.QueryString["attemptid"].ToString();

            fileName = candSessionID + "_" + candAttemptID + ".pdf";

            Response.ContentType = "application/pdf";
            if (fileName != string.Empty)
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            else
                Response.AddHeader("content-disposition", "attachment;filename=TestResult.pdf");

            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            // Set Styles
            //iTextSharp.text.Font fontCaptionStyle = new iTextSharp.text.Font(
            //iTextSharp.text.Font.FontFamily.HELVETICA, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font fontCaptionStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 8, iTextSharp.text.Font.NORMAL, headerColor);
            //iTextSharp.text.Font fontHeaderStyle = new iTextSharp.text.Font(
            //iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font fontHeaderStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 7, iTextSharp.text.Font.NORMAL, headerColor);
            //iTextSharp.text.Font fontItemStyle = new iTextSharp.text.Font(
            //iTextSharp.text.Font.FontFamily.HELVETICA, 6, iTextSharp.text.Font.BOLD, itemColor);
            iTextSharp.text.Font fontItemStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 7, iTextSharp.text.Font.NORMAL, itemColor);

            //Set Document Size as A4.
            Document doc = new Document(iTextSharp.text.PageSize.A4);

            //Set document path
            //PdfWriter.GetInstance(doc, new FileStream(Server.MapPath(@"../Chart/Test.pdf"), FileMode.Create));

            //Construct Table Structures            
            PdfPTable tableCandidateInfo = new PdfPTable(8);

            //table.TotalWidth = 210f;
            //table.LockedWidth = true;            
            tableCandidateInfo.SpacingBefore = 5f;
            tableCandidateInfo.SpacingAfter = 8f;

            //Candidate Info table Structure
            PdfPCell cellTestIDHeader = new PdfPCell(
                new Phrase("Test ID", fontHeaderStyle));
            tableCandidateInfo.AddCell(cellTestIDHeader);
            PdfPCell cellTestID = new PdfPCell(
                new Phrase(TestResultPrint_testIDValueLabel.Text, fontItemStyle));
            tableCandidateInfo.AddCell(cellTestID);
            PdfPCell cellTestNameHeader = new PdfPCell(
                new Phrase("Test Name", fontHeaderStyle));
            tableCandidateInfo.AddCell(cellTestNameHeader);
            PdfPCell cellTestName = new PdfPCell(
                new Phrase(TestResultPrint_testNameValueTextBox.Text, fontItemStyle));
            tableCandidateInfo.AddCell(cellTestName);
            PdfPCell cellCandidateNameHeader = new PdfPCell(
                new Phrase("Candidate Name", fontHeaderStyle));
            tableCandidateInfo.AddCell(cellCandidateNameHeader);
            PdfPCell cellCandidateName = new PdfPCell(
                new Phrase(TestResultPrint_candidateNameValueTextBox.Text, fontItemStyle));
            tableCandidateInfo.AddCell(cellCandidateName);
            PdfPCell cellCrerditsEarnedHeader = new PdfPCell(
                new Phrase("Credits Earned in ($)", fontHeaderStyle));
            tableCandidateInfo.AddCell(cellCrerditsEarnedHeader);
            PdfPCell cellCrerditsEarned = new PdfPCell(
                new Phrase(TestResultPrint_creditsEarnedValueTextBox.Text, fontItemStyle));
            tableCandidateInfo.AddCell(cellCrerditsEarned);

            PdfPTable tableTestStatisticsHeader = new PdfPTable(1);
            tableTestStatisticsHeader.SpacingAfter = 8f;
            PdfPCell tableTestStatisticsHeaderCell = new PdfPCell(
                new Phrase("Test Statistics",
                fontCaptionStyle));
            tableTestStatisticsHeaderCell.BackgroundColor = new BaseColor(235, 239, 241);
            PdfPCell tableTestStatisticsItems = new PdfPCell(
                new Phrase("Items", fontItemStyle));
            tableTestStatisticsHeader.AddCell(tableTestStatisticsHeaderCell);

            //Summary table structure
            PdfPTable tableTestStatistics = new PdfPTable(8);
            tableTestStatistics.SpacingAfter = 8f;
            //tableTestStatistics.SpacingBefore= 8f;
            tableTestStatistics.WidthPercentage = 100;
            PdfPCell cellTestSummaryHeader = new PdfPCell(
                new Phrase("Test Summary", fontCaptionStyle));
            cellTestSummaryHeader.Colspan = 4;
            cellTestSummaryHeader.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            tableTestStatistics.AddCell(cellTestSummaryHeader);
            PdfPCell cellTestCategoryHeader = new PdfPCell(
                new Phrase("Categories / Subjects", fontCaptionStyle));
            cellTestCategoryHeader.Colspan = 4;
            cellTestCategoryHeader.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            tableTestStatistics.AddCell(cellTestCategoryHeader);
            PdfPCell cellTestAuthorHeader = new PdfPCell(
                new Phrase("Test Author", fontHeaderStyle));
            tableTestStatistics.AddCell(cellTestAuthorHeader);
            PdfPCell cellTestAuthor = new PdfPCell(
                new Phrase(TestResultPrint_testAuthorLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellTestAuthor);
            PdfPCell cellTestScoreHeader = new PdfPCell(
                new Phrase("Highest Score", fontHeaderStyle));
            tableTestStatistics.AddCell(cellTestScoreHeader);
            PdfPCell cellTestScore = new PdfPCell(
                new Phrase(TestResultPrint_highScoreLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellTestScore);

            string testID = string.Empty;
            if (!Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
            {
                testID = Request.QueryString["testkey"];
            }

            PdfPCell cellCategoryOrSubjects = new PdfPCell(
                new Phrase("Category", fontHeaderStyle));
            cellCategoryOrSubjects.Colspan = 4;
            cellCategoryOrSubjects.Rowspan = 6;
            cellCategoryOrSubjects.Table = new PdfPTable(2);

            cellCategoryOrSubjects.Table.AddCell(new Phrase("Category", fontItemStyle));
            cellCategoryOrSubjects.Table.AddCell(new Phrase("Subject", fontItemStyle));

            for (int i = 0; i < TestResultPrint_categoryGridview.Rows.Count; i++)
            {
                PdfPCell cellInnerCategory = new PdfPCell(
                    new Phrase(TestResultPrint_categoryGridview.Rows[i].Cells[0].Text, fontHeaderStyle));
                PdfPCell cellInnerSubject = new PdfPCell(
                    new Phrase(TestResultPrint_categoryGridview.Rows[i].Cells[1].Text, fontHeaderStyle));
                cellCategoryOrSubjects.Table.AddCell(cellInnerCategory);
                cellCategoryOrSubjects.Table.AddCell(cellInnerSubject);
            }
            tableTestStatistics.AddCell(cellCategoryOrSubjects);
            PdfPCell cellNOofQuestionsHeader = new PdfPCell(
                new Phrase("Number Of Questions", fontHeaderStyle));
            tableTestStatistics.AddCell(cellNOofQuestionsHeader);
            PdfPCell cellNOofQuestions = new PdfPCell(
                new Phrase(TestResultPrint_noOfQuestionLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellNOofQuestions);
            PdfPCell cellLowestScoreHeader = new PdfPCell(
                new Phrase("Lowest Score", fontHeaderStyle));
            tableTestStatistics.AddCell(cellLowestScoreHeader);
            PdfPCell cellLowestScore = new PdfPCell(
                new Phrase(TestResultPrint_lowScoreLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellLowestScore);
            PdfPCell cellNOofCandidatesHeader = new PdfPCell(
                new Phrase("Number Of Candidates", fontHeaderStyle));
            tableTestStatistics.AddCell(cellNOofCandidatesHeader);
            PdfPCell cellNOofCandidates = new PdfPCell(
                new Phrase(TestResultPrint_noOfCandidateLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellNOofCandidates);
            PdfPCell cellTestMeanScoreHeader = new PdfPCell(
                new Phrase("Mean Score", fontHeaderStyle));
            tableTestStatistics.AddCell(cellTestMeanScoreHeader);
            PdfPCell cellTestMeanScore = new PdfPCell(
                new Phrase(TestResultPrint_meanScoreLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellTestMeanScore);
            PdfPCell cellAverageTimeHeader = new PdfPCell(
                new Phrase("Average Time Taken By Candidates", fontHeaderStyle));
            tableTestStatistics.AddCell(cellAverageTimeHeader);
            PdfPCell cellAverageTime = new PdfPCell(
                new Phrase(TestResultPrint_avgTimeLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellAverageTime);
            PdfPCell cellStandardDeviationHeader = new PdfPCell(
                new Phrase("Standard Deviation", fontHeaderStyle));
            tableTestStatistics.AddCell(cellStandardDeviationHeader);
            PdfPCell cellStandardDeviation = new PdfPCell(
                new Phrase(TestResultPrint_scoreSDLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellStandardDeviation);
            PdfPCell cellScoreRangeHeader = new PdfPCell(
                new Phrase("Score Range", fontHeaderStyle));
            tableTestStatistics.AddCell(cellScoreRangeHeader);
            PdfPCell cellScoreRange = new PdfPCell(
                new Phrase(TestResultPrint_scoreRangeLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellScoreRange);
            PdfPCell cellEmptyHeader = new PdfPCell(
                new Phrase("", fontHeaderStyle));
            tableTestStatistics.AddCell(cellEmptyHeader);
            PdfPCell cellEmptyItem = new PdfPCell(
                new Phrase("", fontItemStyle));
            tableTestStatistics.AddCell(cellEmptyItem);

            int rowCount = 0;
            if (TestResultPrint_categoryGridview.Rows.Count > 6)
                rowCount = TestResultPrint_categoryGridview.Rows.Count - 6;
            for (int j = 0; j < rowCount; j++)
            {
                PdfPCell cellEmptyItemforRowSpan = new PdfPCell(new Phrase("", fontItemStyle));
                cellEmptyItemforRowSpan.Colspan = 4;
                tableTestStatistics.AddCell(cellEmptyItemforRowSpan);
            }
            tableTestStatisticsItems.AddElement(tableTestStatistics);

            //Category and Subject Statistics table structure
            PdfPTable tableCategoryStatistics = new PdfPTable(2);
            tableCategoryStatistics.SpacingAfter = 8f;
            tableCategoryStatistics.WidthPercentage = 100;
            PdfPCell cellCategoryStatistics = new PdfPCell(
                new Phrase("Category Statistics", fontCaptionStyle));
            PdfPCell cellSubjectStatistics = new PdfPCell(
                new Phrase("Subject Statistics", fontCaptionStyle));
            cellCategoryStatistics.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            cellSubjectStatistics.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            tableCategoryStatistics.AddCell(cellCategoryStatistics);
            tableCategoryStatistics.AddCell(cellSubjectStatistics);
            iTextSharp.text.Image imageCategory = iTextSharp.text.Image.GetInstance(
                chartImagePath + Constants.ChartConstants.CHART_CATEGORY + "-" + testID + ".png");
            imageCategory.ScaleToFit(205f, 300f);
            imageCategory.Border = Rectangle.BOX;
            imageCategory.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellCategoryImage = new PdfPCell(imageCategory);
            cellCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableCategoryStatistics.AddCell(cellCategoryImage);
            iTextSharp.text.Image imageSubject = iTextSharp.text.Image.GetInstance(
                chartImagePath + Constants.ChartConstants.CHART_SUBJECT + "-" + testID + ".png");
            imageSubject.ScaleToFit(205f, 300f);
            imageSubject.Border = Rectangle.BOX;
            imageSubject.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellSubjectImage = new PdfPCell(imageSubject);
            cellSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableCategoryStatistics.AddCell(cellSubjectImage);

            tableTestStatisticsItems.AddElement(tableCategoryStatistics);

            //Test Area and complexity table structure
            PdfPTable tableTestAreaStatistics = new PdfPTable(2);
            //tableTestAreaStatistics.SpacingAfter = 3f;
            tableTestAreaStatistics.WidthPercentage = 100;
            PdfPCell cellAreStatisticsHeader = new PdfPCell(
                new Phrase("Test Area Statistics", fontCaptionStyle));
            PdfPCell cellComplexityStatisticsHeader = new PdfPCell(
                new Phrase("Complexity Statistics", fontCaptionStyle));
            tableTestAreaStatistics.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            tableTestAreaStatistics.AddCell(cellAreStatisticsHeader);
            tableTestAreaStatistics.AddCell(cellComplexityStatisticsHeader);
            iTextSharp.text.Image imageArea = iTextSharp.text.Image.GetInstance(
                chartImagePath + Constants.ChartConstants.CHART_TESTAREA + "-" + testID + ".png");
            imageArea.ScaleToFit(205f, 300f);
            imageArea.Border = Rectangle.BOX;
            imageArea.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellAreaImage = new PdfPCell(imageArea);
            cellAreaImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableTestAreaStatistics.AddCell(cellAreaImage);
            iTextSharp.text.Image imageComplexity = iTextSharp.text.Image.GetInstance(
                chartImagePath + Constants.ChartConstants.CHART_COMPLEXITY + "-" + testID + ".png");
            imageComplexity.ScaleToFit(205f, 300f);
            imageComplexity.Border = Rectangle.BOX;
            imageComplexity.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellComplexityImage = new PdfPCell(imageComplexity);
            cellComplexityImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableTestAreaStatistics.AddCell(cellComplexityImage);

            tableTestStatisticsItems.AddElement(tableTestAreaStatistics);
            tableTestStatisticsHeader.AddCell(tableTestStatisticsItems);

            PdfPTable tableCandidateStatisticsHeader = new PdfPTable(1);
            tableCandidateStatisticsHeader.SpacingAfter = 8f;
            PdfPCell tableCandidateStatisticsHeaderCell = new PdfPCell(
                new Phrase("Candidate Statistics", fontCaptionStyle));
            tableCandidateStatisticsHeaderCell.BackgroundColor = new BaseColor(235, 239, 241);
            PdfPCell tableCandidateStatisticsItemCell = new PdfPCell(
                new Phrase("", fontItemStyle));
            tableCandidateStatisticsHeader.AddCell(tableCandidateStatisticsHeaderCell);

            //Candidate Statistics
            PdfPTable tableCandidateStatistics = new PdfPTable(4);
            tableCandidateStatistics.SpacingAfter = 8f;
            //tableCandidateStatistics.SpacingBefore = 8f;
            tableCandidateStatistics.WidthPercentage = 100;
            PdfPCell cellCandidateNoOfQuestionsHeader = new PdfPCell(
                new Phrase("Number Of Questions", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateNoOfQuestionsHeader);
            PdfPCell cellCandidateNoOfQuestion = new PdfPCell(
                new Phrase(TestResultPrint_noOfQuestionLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateNoOfQuestion);
            PdfPCell cellCandidateMyScoreHeader = new PdfPCell(
                new Phrase("My Score", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateMyScoreHeader);
            PdfPCell cellCandidateMyScore = new PdfPCell(
                new Phrase(TestResultPrint_myScoreLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateMyScore);
            PdfPCell cellCandidateTotalQuestionsAttendedHeader = new PdfPCell(
                new Phrase("Total Questions Attended", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateTotalQuestionsAttendedHeader);
            PdfPCell cellCandidateTotalQuestionsAttended = new PdfPCell(
                new Phrase(TestResultPrint_noOfQuestionattenedLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateTotalQuestionsAttended);
            PdfPCell cellCCandidateAbsoluteScoreHeader = new PdfPCell(
                new Phrase("Absolute Score", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCCandidateAbsoluteScoreHeader);
            PdfPCell cellCandidateAbsolute = new PdfPCell(
                new Phrase(TestResultPrint_absoluteScoreLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAbsolute);
            PdfPCell cellCandidateTotalQuestionsSkippedHeader = new PdfPCell(
                new Phrase("Total Questions Skipped", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateTotalQuestionsSkippedHeader);
            PdfPCell cellCandidateTotalQuestionsSkipped = new PdfPCell(
                new Phrase(TestResultPrint_noOfQuestionSkippedLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateTotalQuestionsSkipped);
            PdfPCell cellCandidateRelativeScoreHeader = new PdfPCell(
                new Phrase("Relative Score", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateRelativeScoreHeader);
            PdfPCell cellCandidateRelativeScore = new PdfPCell(
                new Phrase(TestResultPrint_relativeScoreLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateRelativeScore);
            PdfPCell cellCandidateAnsweredCorrectlyHeader = new PdfPCell(
                new Phrase("Questions Answered Correctly", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateAnsweredCorrectlyHeader);
            PdfPCell cellCandidateAnsweredCorrectly = new PdfPCell(
                new Phrase(TestResultPrint_noOfQuestionAnsweredCorrectlyLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAnsweredCorrectly);
            PdfPCell cellCandidatePercentileHeader = new PdfPCell(
                new Phrase("Percentile", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidatePercentileHeader);
            PdfPCell cellCandidateercentile = new PdfPCell(
                new Phrase(TestResultPrint_percentileLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateercentile);
            PdfPCell cellCandidateAnsweredWronglyHeader = new PdfPCell(
                new Phrase("Questions Answered Wrongly", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateAnsweredWronglyHeader);
            PdfPCell cellCandidateAnsweredWrongly = new PdfPCell(
                new Phrase(TestResultPrint_noOfQuestionAnsweredWronglyLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAnsweredWrongly);
            PdfPCell cellCandidateTotalTimeTakenHeader = new PdfPCell(
                new Phrase("Total Time Taken", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateTotalTimeTakenHeader);
            PdfPCell cellCandidateTotalTimeTaken = new PdfPCell(
                new Phrase(TestResultPrint_noOfQuestionTimeTakenLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateTotalTimeTaken);
            PdfPCell cellCandidateAnsweredCorrectlyPercentageHeader = new PdfPCell(
                new Phrase("Questions Answered Correctly (in %)", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateAnsweredCorrectlyPercentageHeader);
            PdfPCell cellCandidateAnsweredCorrectlyPercentage = new PdfPCell(
                new Phrase(TestResultPrint_percentageQuestionsAnsweredCorrectlyLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAnsweredCorrectlyPercentage);
            PdfPCell cellCandidateAverageTimeTakenToAnswerEachQuestionHeader = new PdfPCell(
                new Phrase("Average Time Taken To Answer Each Question", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateAverageTimeTakenToAnswerEachQuestionHeader);
            PdfPCell cellCandidateAverageTimeTakenToAnswerEachQuestion = new PdfPCell(
                new Phrase(TestResultPrint_avgTimeTakenLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAverageTimeTakenToAnswerEachQuestion);


            PdfPCell cellCandidateTestStatusHeader = new PdfPCell(
               new Phrase("Test Status", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateTestStatusHeader);
            PdfPCell cellCandidateTestStatus = new PdfPCell(
                new Phrase(TestResultPrint_testStatusLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateTestStatus);
            PdfPCell cellCandidateAverageTimeTakenToAnswerEachQuestionHeader1 = new PdfPCell(
                new Phrase("", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateAverageTimeTakenToAnswerEachQuestionHeader1);
            PdfPCell cellCandidateAverageTimeTakenToAnswerEachQuestion1 = new PdfPCell(
                new Phrase("", fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAverageTimeTakenToAnswerEachQuestion1);
   

            tableCandidateStatisticsItemCell.AddElement(tableCandidateStatistics);

            string sessionKey = string.Empty;
            string testKey = string.Empty;
            int attemptID = 0;
            if ((Request.QueryString["candidatesession"] != null) &&
                (Request.QueryString["candidatesession"].ToString().Length != 0))
                sessionKey = Request.QueryString["candidatesession"].ToString();
            if ((Request.QueryString["attemptid"] != null) &&
                (Request.QueryString["attemptid"].ToString().Length != 0))
                attemptID = int.Parse(Request.QueryString["attemptid"].ToString());
            if ((Request.QueryString["testkey"] != null) &&
               (Request.QueryString["testkey"].ToString().Length != 0))
                testKey = Request.QueryString["testkey"].ToString();

            //Test Score and category table structure
            PdfPTable tableTestScore = new PdfPTable(2);
            tableTestScore.SpacingAfter = 8f;
            tableTestScore.WidthPercentage = 100;
            PdfPCell cellScoreHeader = new PdfPCell(new Phrase("Test Scores", fontCaptionStyle));
            PdfPCell cellScoreCategoryHeader = new PdfPCell(
                new Phrase("Score Distribution Amongst Categories", fontCaptionStyle));
            tableTestScore.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            tableTestScore.AddCell(cellScoreHeader);
            tableTestScore.AddCell(cellScoreCategoryHeader);

            bool flag = false;
            imagePath = Server.MapPath("~/chart/") +
                Constants.ChartConstants.CHART_HISTOGRAM + "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png";

            iTextSharp.text.Image imageTestScore =null; 
            if (!File.Exists(imagePath))
            {    
                imageTestScore = iTextSharp.text.Image.GetInstance(Server.MapPath("~/chart/empty.png")); 
            }
            else
            {
                flag = true;
                imageTestScore = iTextSharp.text.Image.GetInstance(chartImagePath +
                   Constants.ChartConstants.CHART_HISTOGRAM + "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png");
                imageTestScore.ScaleToFit(205f, 300f);
                imageTestScore.Border = Rectangle.BOX;
            } 
            
            imageTestScore.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellTestScoreImage = new PdfPCell(imageTestScore);
            cellCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            if (!flag)
            {
                imageTestScore.Alignment = iTextSharp.text.Image.ALIGN_JUSTIFIED;
                cellTestScoreImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                cellTestScoreImage.FixedHeight = 50f; 
            }

            tableTestScore.AddCell(cellTestScoreImage);

            flag = false;

            imagePath = Server.MapPath("~/chart/") +
                Constants.ChartConstants.CHART_CATEGORY + "-" +
                testKey + "-" + sessionKey + "-" + attemptID + ".png";

            iTextSharp.text.Image imageScoreCategory = null;
            if (!File.Exists(imagePath))
            {
                imageScoreCategory = iTextSharp.text.Image.GetInstance(Server.MapPath("~/chart/empty.png")); 
            }
            else
            {
                flag = true;
                imageScoreCategory = iTextSharp.text.Image.GetInstance(
                  chartImagePath + Constants.ChartConstants.CHART_CATEGORY + "-" +
                  testKey + "-" + sessionKey + "-" + attemptID + ".png");
                imageScoreCategory.ScaleToFit(205f, 300f);
                imageScoreCategory.Border = Rectangle.BOX;
            } 
            
            imageScoreCategory.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellScoreCategoryImage = new PdfPCell(imageScoreCategory); 
            cellSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            if (!flag)
            {
                imageScoreCategory.Alignment = iTextSharp.text.Image.ALIGN_JUSTIFIED;
                cellScoreCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                cellScoreCategoryImage.FixedHeight = 50f;
            }
            tableTestScore.AddCell(cellScoreCategoryImage);
            flag = false;
            tableCandidateStatisticsItemCell.AddElement(tableTestScore);

            //Score subject and score distribution area table structure
            PdfPTable tableScoreSubject = new PdfPTable(2);
            //tableScoreSubject.SpacingAfter = 3f;
            tableScoreSubject.WidthPercentage = 100;
            PdfPCell cellScoreSubjectHeader = new PdfPCell(
                new Phrase("Score Distribution Amongst Subjects", fontCaptionStyle));
            PdfPCell cellScoreTestAreaHeader = new PdfPCell(
                new Phrase("Score Distribution Amongst Test Areas", fontCaptionStyle));
            tableScoreSubject.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            tableScoreSubject.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            tableScoreSubject.AddCell(cellScoreSubjectHeader);
            tableScoreSubject.AddCell(cellScoreTestAreaHeader);

              imagePath = Server.MapPath("~/chart/") +
                 Constants.ChartConstants.CHART_SUBJECT + "-" +
                testKey + "-" + sessionKey + "-" + attemptID + ".png";

            iTextSharp.text.Image imageScoreSubject = null;
            if (!File.Exists(imagePath))
            {
                imageScoreSubject = iTextSharp.text.Image.GetInstance(Server.MapPath("~/chart/empty.png")); 
            }
            else
            {
                flag = true ;
                imageScoreSubject = iTextSharp.text.Image.GetInstance(
                   chartImagePath + Constants.ChartConstants.CHART_SUBJECT + "-" +
                   testKey + "-" + sessionKey + "-" + attemptID + ".png");
                imageScoreSubject.ScaleToFit(205f, 300f);
                imageScoreSubject.Border = Rectangle.BOX;
            }
            imageScoreSubject.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellScoreSubjectImage = new PdfPCell(imageScoreSubject);
            cellAreaImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            if (!flag)
            {
                imageScoreSubject.Alignment = iTextSharp.text.Image.ALIGN_JUSTIFIED;
                cellScoreSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                cellScoreSubjectImage.FixedHeight = 50f;
            }
            tableScoreSubject.AddCell(cellScoreSubjectImage);
            flag = false;

            imagePath = Server.MapPath("~/chart/") +
               Constants.ChartConstants.CHART_TESTAREA + "-" +
                testKey + "-" + sessionKey + "-" + attemptID + ".png";
            iTextSharp.text.Image imageScoreArea = null;
            if (!File.Exists(imagePath))
            {
                imageScoreArea = iTextSharp.text.Image.GetInstance(Server.MapPath("~/chart/empty.png")); 
            }
            else
            {
                flag = true;
                imageScoreArea = iTextSharp.text.Image.GetInstance(
                   chartImagePath + Constants.ChartConstants.CHART_TESTAREA + "-" +
                   testKey + "-" + sessionKey + "-" + attemptID + ".png");
                imageScoreArea.ScaleToFit(205f, 300f);
                imageScoreArea.Border = Rectangle.BOX;
            } 
            
            imageScoreArea.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellScoreAreaImage = new PdfPCell(imageScoreArea);
            cellComplexityImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            if (!flag)
            {
                imageScoreArea.Alignment = iTextSharp.text.Image.ALIGN_JUSTIFIED;
                cellScoreAreaImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                cellScoreAreaImage.FixedHeight = 50f;
            }
            tableScoreSubject.AddCell(cellScoreAreaImage);

            tableCandidateStatisticsItemCell.AddElement(tableScoreSubject);
            tableCandidateStatisticsHeader.AddCell(tableCandidateStatisticsItemCell);

            //Response Output
            PdfWriter.GetInstance(doc, Response.OutputStream);
            doc.Open();
            doc.Add(tableCandidateInfo);
            doc.Add(tableTestStatisticsHeader);
            doc.Add(tableCandidateStatisticsHeader);
            doc.Close();
        }

        #endregion Private Methods


        #region Candidate Interview Assessment SummaryPDF
         
        private string GetCandidateImageFile(int candidateID)
        {
            string path = null;

            bool isExists = false;
            if (candidateID > 0)
            {
                path = Server.MapPath("~/") +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                            "\\" + candidateID + "." +
                            ConfigurationManager.AppSettings["CANDIDATE_PHOTO_FILE_EXTENSION"];
                if (File.Exists(path))
                    isExists = true;
                else
                    isExists = false;
            }

            if (isExists)
                return path;
            else
            {
                return Server.MapPath("~/") +
                      ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"] +
                      "\\" + "photo_not_available.png";
            }
        }

        private void GeneretePDF_AssessorSummaryRating(string candidateSessionID, int attemptID)
        {
             
            DataTable assessmentTable = new DataTable();

            if (Request.QueryString["candidatesessionid"] == null 
                && Request.QueryString["attemptid"] == null) 
                return; 
             
            CandidateInterviewSessionDetail sessionDetail = new InterviewSchedulerBLManager().
                GetCandidateInterviewSessionDetail(candidateSessionID, attemptID);

            PDFDocumentWriter pdfWriter = new PDFDocumentWriter();

            string fileName = sessionDetail.CandidateName + "_" + "InterviewDetail.pdf";

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            Document doc = new Document(PDFDocumentWriter.paper,
                PDFDocumentWriter.leftMargin, PDFDocumentWriter.rightMargin,
                PDFDocumentWriter.topMargin, PDFDocumentWriter.bottomMargin);

            string logoPath = Server.MapPath("~/");

            iTextSharp.text.Image imgBackground = iTextSharp.text.Image.GetInstance(logoPath + "Images/AssessmentBackground.jpg");

            imgBackground.ScaleToFit(1000, 132);
            imgBackground.Alignment = iTextSharp.text.Image.UNDERLYING;
            imgBackground.SetAbsolutePosition(50, 360);

            PdfPTable pageHeader = new PdfPTable(pdfWriter.PageHeader(logoPath, "Candidate Interview Assessment Report"));
            pageHeader.SpacingAfter = 10f;

            //Construct Table Structures    
            PdfPTable tableHeaderDetail = new PdfPTable(1);
            PdfPCell cellBorder = new PdfPCell(new Phrase(""));
            cellBorder.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            cellBorder.Border = 0;
            tableHeaderDetail.WidthPercentage = 90;
            cellBorder.PaddingTop = 20f;
            cellBorder.AddElement(pdfWriter.TableHeader(sessionDetail, candidateSessionID,
                attemptID, GetCandidateImageFile(sessionDetail.CandidateInformationID), out assessmentTable));
            tableHeaderDetail.AddCell(cellBorder);
            tableHeaderDetail.SpacingAfter = 30f;
            //Response Output
            PdfWriter.GetInstance(doc, Response.OutputStream);

            doc.Open();
            doc.Add(pageHeader);
            doc.Add(imgBackground); 
            doc.Add(tableHeaderDetail); 
            doc.Add(pdfWriter.TableDetail(assessmentTable));
            doc.Close(); 
        }

        #endregion
    }
}