﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SignUP.cs
// File that represents the user interface for creating
// a new user in to the forte repository.

#endregion Header

#region Namespace

using System;
using System.Web;
using System.Text;
using System.Configuration;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Resources;
using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;
using Forte.HCM.UI.Common;
using System.Data;
using Forte.HCM.Common.DL;
using System.Data.Common;
using System.Drawing;

#endregion Namespace

namespace Forte.HCM.UI.Subscription
{
    public partial class Signup : PageBase
    {

        #region Enum

        private enum SubscriptionRolesEnum
        {
            SR_COR_AMN = 3,
            SR_FRE = 1,
            SR_STA = 2,
        }

        #endregion

        #region Variables

        /// <summary>
        /// A <see cref="System.String"/> that holds the string value
        /// used to store the selected subscription type.
        /// </summary>
        private const string SELECTEDREGISTRATIONTYPE = "SELECTEDREGISTRATIONTYPE";

        /// <summary>
        /// A <see cref="System.String"/> that holds the string value
        /// used to store the capcha value in viewstate.
        /// </summary>
        private const string CAPTCHAVALUEVIEWSTATE = "CAPTCHA_TEXT";

        #endregion Variables

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption(HCMResource.UserRegistration_SignUpPageTitle);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                if (Session[SELECTEDREGISTRATIONTYPE] == null)
                {
                    Response.Redirect("~/Subscription/SubscriptionType.aspx", false);
                    return;
                }
                UserRegistration_passwordTextBox.Attributes.Add("value", UserRegistration_passwordTextBox.Text);
                UserRegistration_confrimPasswordTextBox.Attributes.Add("value", UserRegistration_confrimPasswordTextBox.Text);

                // Load subscription features
                LoadSubscriptionFeatures();

                if (IsPostBack)
                    return;
                UserRegistration_passwordTextBox.Text = "";
                UserRegistration_confrimPasswordTextBox.Text = "";
                GenerateCaptchaImage();
                LoadValues();
                try
                {
                    //ListItem OtherListItem = UserRegistration_typeOfBussinessCheckBoxList.Items.FindByText("Other");
                    UserRegistration_otherTypeOfBussinessCheckBox.Attributes.Add("onclick", "ShowValue(this.checked);");
                }
                catch { }
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void UserRegistration_regenerateCaptchaImageLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                GenerateCaptchaImage();
                UserRegistration_captchaImageTextBox.Text = "";
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void UserRegistration_cancelRegistrationButton_CancelClick(object sender, CommandEventArgs e)
        {
            try
            {
                Session[SELECTEDREGISTRATIONTYPE] = null;
                Response.Redirect("~/Subscription/SubscriptionType.aspx", false);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void UserRegistration_checkUserEmailIdAvailableButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(UserRegistration_userNameTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistraion_EmptyUserEmail);
                    return;
                }
                if (!IsValidEmailAddress(UserRegistration_userNameTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_InvalidEmailAddress);
                    return;
                }
                if (CheckForEmailAddressAvailability(UserRegistration_userNameTextBox.Text.Trim()))
                    UserRegistration_validEmailAvailableStatus.Text = HCMResource.UserRegistration_UserEmailAvailable;
                else
                    UserRegistration_inValidEmailAvailableStatus.Text = HCMResource.UserRegistration_UserEmailNotAvailable;
                UserRegistration_userEmailAvailableStatusDiv.Style.Add("display", "block");
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void UserRegistration_registerButton_CommandClick(object sender, CommandEventArgs e)
        {
            try
            {
                MakeDefaultCSS();

                bool basicVerification = true;

                if (!IsValidData(out basicVerification))
                {
                    if (!basicVerification)
                    {
                        ShowErrorMessage(HCMResource.UserRegistration_EnterMandatoryFields);
                    }

                    return;
                }
                InsertUserAndSendConfirmationCodeEmail();
                Session[SELECTEDREGISTRATIONTYPE] = null;
                ShowSuccessMessage(HCMResource.UserRegistration_UserInserted);
                UserRegistration_captchaImageTextBox.Text = "";
                GenerateCaptchaImage();
                Session["Subscription_Status"] = UserRegistration_userNameTextBox.Text.Trim();

                // Construct subscription status page URL.
                string statusURL = "~/Subscription/SubscriptionStatus.aspx";

                // Check if the subscription type is corporate. If corporate 
                // add additional query string parameters.
                if (Convert.ToInt32(UserRegistration_subscriptionIdHidden.Value) == 
                    (int)SubscriptionRolesEnum.SR_COR_AMN)
                {
                    statusURL += "?corporate=y";
                }
                Response.Redirect(statusURL, false);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        private void MakeDefaultCSS()
        {

            UserRegistration_userNameTextBox.CssClass = "text_box";
            UserRegistration_passwordTextBox.CssClass = "text_box";
            UserRegistration_confrimPasswordTextBox.CssClass = "text_box";
            UserRegistration_firstNameTextBox.CssClass = "text_box";
            UserRegistration_lastNameTextBox.CssClass = "text_box";
            UserRegistration_phoneTextBox.CssClass = "text_box";
            UserRegistration_companyNameTextBox.CssClass = "text_box";
            UserRegistration_numberOfUsersTextBox.CssClass = "text_box";
            UserRegistration_captchaImageTextBox.CssClass = "text_box";
            UserRegistration_otherTypeOfBussinessDIV.Style["border-color"] = "#D8D8D8";

        }

        #endregion Events

        #region Private Methods

        /// <summary>
        /// Method that will load the subscription features.
        /// </summary>
        private void LoadSubscriptionFeatures()
        {
            UserRegistrationInfo UserInfo = Session[SELECTEDREGISTRATIONTYPE] as UserRegistrationInfo;
            UserRegistration_subscriptionIdHidden.Value = UserInfo.SubscriptionId.ToString();

            // Retrieve the list of subscription features.
            if (UserRegistration_subscriptionIdHidden.Value == null ||
                UserRegistration_subscriptionIdHidden.Value.Trim().Length == 0)
            {
                return;
            }

            int subscriptionID = Convert.ToInt32(UserRegistration_subscriptionIdHidden.Value);

            List<SubscriptionFeatures> features = new SubscriptionBLManager().
                GetSubscriptionFeatures(subscriptionID);

            if (features == null || features.Count == 0)
            {
                return;
            }

            Table table = new Table();
            table.Width = new Unit(100, UnitType.Percentage);

            TableRow tableRow = null;

            foreach (SubscriptionFeatures feature in features)
            {
                tableRow = new TableRow();
                tableRow.Cells.AddRange(GetCells(feature));

                // Add row to the table.
                table.Rows.Add(tableRow);
            }

            Signup_featuresPlaceHolder.Controls.Add(table);
        }

        private TableCell[] GetCells(SubscriptionFeatures feature)
        {
            TableCell[] cells = new TableCell[2];

            ImageButton imageButton = new ImageButton();
            imageButton.SkinID = "sknFeatureBulletin";

            // Bulletin image.
            cells[0] = new TableCell();
            cells[0].Width = new Unit(5, UnitType.Percentage);
            cells[0].Controls.Add(imageButton);

            // Feature.
            cells[1] = new TableCell();
            cells[1].Width = new Unit(95, UnitType.Percentage);
            cells[1].Text = string.Format("<span class='signup_feature_label'>{0}(</span><span class='signup_feature_value_label'>{1}</span><span class='signup_feature_label'>)</span>", feature.FeatureName, feature.FeatureValue);

            return cells;
        }

        /// <summary>
        /// A Method that shows the success message to the user
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the message
        /// </param>
        private void ShowSuccessMessage(string Message)
        {
            base.ShowMessage(UserRegistration_topSuccessMessageLabel,
                UserRegistration_bottomSuccessMessageLabel, Message);
        }

        /// <summary>
        /// A Method that shows the error message to the user
        /// </summary>
        /// <param name="Message">
        /// A <see cref="System.String"/> that holds the message
        /// </param>
        private void ShowErrorMessage(string Message)
        {
            base.ShowMessage(UserRegistration_topErrorMessageLabel,
                UserRegistration_errorMessageLabel, Message);
        }

        /// <summary>
        /// A method that logs the exception message and shows
        /// the exception to the user
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception object.
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            if (exp == null)
                return;
            Logger.ExceptionLog(exp);
            ShowErrorMessage(exp.Message);
        }

        /// <summary>
        /// A Method that sets the visible status to the 
        /// number of users textbox as per the selected
        /// subscription type
        /// </summary>
        /// <param name="VisibilityStatus">
        /// A <see cref="System.Boolean"/> that holds the visibility status
        /// of the textbox
        /// </param>
        private void SetVisibileToUsers(bool VisibilityStatus)
        {
            UserRegistration_numberOfUsersTr.Visible = VisibilityStatus;
        }

        /// <summary>
        /// A Method that communicates to the BL manager
        /// to check whether the email already exists in repository
        /// or not
        /// </summary>
        /// <param name="User_Email">
        /// A <see cref="System.String"/> that holds the user email 
        /// to be validated in the repository
        /// </param>
        /// <returns>
        /// A <see cref="System.Boolean"/> that holds the availability status.
        /// </returns>
        private bool CheckForEmailAddressAvailability(string User_Email)
        {
            return new UserRegistrationBLManager().CheckUserEmailExists(User_Email, base.tenantID);
        }

        /// <summary>
        /// A Method that gets the selected bussiness types from 
        /// the check box list.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that holds the multiple selected
        /// bussiness types id's with comma seperated operator
        /// </returns>
        private string GetSelectedBussinessTypesFromList()
        {
            StringBuilder SelectedItems = new StringBuilder();
            for (int ItemsCount = 0; ItemsCount < UserRegistration_typeOfBussinessCheckBoxList.Items.Count; ItemsCount++)
                if (UserRegistration_typeOfBussinessCheckBoxList.Items[ItemsCount].Selected)
                    SelectedItems.Append(UserRegistration_typeOfBussinessCheckBoxList.Items[ItemsCount].Value + ",");
            return SelectedItems.ToString().TrimEnd(',');
        }

        /// <summary>
        /// A method that communicates with the BL manager to insert
        /// a new user in the repository and sends the activation link
        /// to the user as e-mail.
        /// </summary>
        /// <param name="Confirmation_Code">
        /// A <see cref="System.String"/> that holds the confirmation 
        /// code for the activation of account
        /// </param>
        private void InsertUserAndSendConfirmationCodeEmail()
        {
            if (Session[SELECTEDREGISTRATIONTYPE] == null)
            {
                Response.Redirect("~/Subscription/SubscriptionType.aspx", false);
                return;
            }
            UserRegistrationInfo userRegistrationInfo = null;
            RegistrationConfirmation registrationConfirmation = null;
            string Confirmation_Code = string.Empty;
            IDbTransaction transaction = null;
            try
            {
                userRegistrationInfo = new UserRegistrationInfo();
                userRegistrationInfo.BussinessTypesIds = GetSelectedBussinessTypesFromList();
                userRegistrationInfo.BussinessTypeOther = UserRegistration_otherBussinessTextBox.Text.Trim();
                userRegistrationInfo.UserEmail = UserRegistration_userNameTextBox.Text.Trim();
                userRegistrationInfo.Password = new EncryptAndDecrypt().EncryptString(UserRegistration_passwordTextBox.Text.Trim());
                userRegistrationInfo.FirstName = UserRegistration_firstNameTextBox.Text.Trim();
                userRegistrationInfo.LastName = UserRegistration_lastNameTextBox.Text.Trim();
                userRegistrationInfo.Phone = UserRegistration_phoneTextBox.Text.Trim();
                userRegistrationInfo.Company = UserRegistration_companyNameTextBox.Text.Trim();
                userRegistrationInfo.Title = UserRegistration_titleTextBox.Text.Trim();
                userRegistrationInfo.NumberOfUsers = UserRegistration_numberOfUsersTextBox.Text.Trim() == "" ? Convert.ToInt16(0) :
                    Convert.ToInt16(UserRegistration_numberOfUsersTextBox.Text.Trim());
                userRegistrationInfo.SubscriptionId = Convert.ToInt32(UserRegistration_subscriptionIdHidden.Value);
                userRegistrationInfo.SubscriptionRole = Enum.GetName(typeof(SubscriptionRolesEnum), userRegistrationInfo.SubscriptionId);
                if (Utility.IsNullOrEmpty(userRegistrationInfo.SubscriptionRole))
                    userRegistrationInfo.SubscriptionRole = "SR_OTHER";
                userRegistrationInfo.IsTrial = Convert.ToInt16(UserRegistration_isTrialHidden.Value);
                if (!Utility.IsNullOrEmpty(UserRegistration_trialDaysHidden.Value))
                    userRegistrationInfo.TrialDays = Convert.ToInt32(UserRegistration_trialDaysHidden.Value);
                registrationConfirmation = new RegistrationConfirmation();
                Confirmation_Code = GetConfirmationCode();
                registrationConfirmation.ConfirmationCode = Confirmation_Code;
                transaction = new TransactionManager().Transaction;
                new UserRegistrationBLManager().InsertUser(null, userRegistrationInfo, registrationConfirmation, transaction as DbTransaction);

                // Check if the user is non-corporate. For non-corporate 
                // subscription types, send the confirmation mail. For 
                // corporate send a mail to the admin, indicating that a
                // corporate user has signed up.
                if (userRegistrationInfo.SubscriptionId != (int)SubscriptionRolesEnum.SR_COR_AMN)
                    SendConfirmationCodeEmail(UserRegistration_userNameTextBox.Text.Trim(), Confirmation_Code);
                else
                    SendAdminIndicationMail(userRegistrationInfo);

                transaction.Commit();
            }
            catch
            {
                if (!Utility.IsNullOrEmpty(transaction))
                    transaction.Rollback();
                throw;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(transaction)) transaction = null;
                if (userRegistrationInfo != null) userRegistrationInfo = null;
                if (registrationConfirmation != null) registrationConfirmation = null;
                try
                {
                    GC.SuppressFinalize(userRegistrationInfo);
                    GC.SuppressFinalize(registrationConfirmation);
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }

        }

        /// <summary>
        /// A Method that generates a random string and used 
        /// the encrypted string as confirmation code for a user
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that holds the encrypted string
        /// </returns>
        private string GetConfirmationCode()
        {
            return new EncryptAndDecrypt().EncryptString(RandomString(15));
        }

        /// <summary>
        /// A Method that sends the activation code as email
        /// </summary>
        /// <param name="userEmail">
        /// A <see cref="String"/> that holds the user email
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="String"/> that holds the encrypted 
        /// confirmation code to activate his/her account
        /// </param>
        private void SendConfirmationCodeEmail(string userEmail, string confirmationCode)
        {
            // Send mail.
            new EmailHandler().SendMail
                (userEmail, ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"],
                HCMResource.UserRegistration_MailActivationCodeSubject,
                GetMailBody(new EncryptAndDecrypt().EncryptString(userEmail), confirmationCode));
        }

        /// <summary>
        /// Method that sends the indication mail to the admin, when a 
        /// corporate user signed up.
        /// </summary>
        /// <param name="userRegistrationInfo">
        /// A <see cref="UserRegistrationInfo"/> that holds the user 
        /// registration info.
        /// </param>
        private void SendAdminIndicationMail(UserRegistrationInfo userRegistrationInfo)
        {
            new EmailHandler().SendMail(
                ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"],
                HCMResource.UserRegistration_CorporateSubscriptionAccountRequestedSubject,
                GetMailBody(userRegistrationInfo));
        }

        /// <summary>
        /// Method that constructs the body content of the email.
        /// </summary>
        /// A <see cref="String"/> that holds the user email.
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="String"/> that holds the encrypted 
        /// confirmation code to activate his/her account.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that contains the mail body.
        /// </returns>
        private string GetMailBody(string userEmail, string confirmationCode)
        {
            StringBuilder sbBody = new StringBuilder();

            try
            {
                sbBody.Append("Dear ");
                sbBody.Append(UserRegistration_firstNameTextBox.Text.Trim());
                sbBody.Append(" ");
                sbBody.Append(UserRegistration_lastNameTextBox.Text.Trim());
                sbBody.Append(",");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Welcome to ForteHCM !");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This is to acknowledge that your request to create an account was processed successfully.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("To Activate your account, please copy and paste the ");
                sbBody.Append("below URL in the browser or click on the link");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                string strUri =
                    string.Format(WebConfigurationManager.AppSettings["SIGNUP_ACTIVATIONURL"] + "?" +
                    WebConfigurationManager.AppSettings["SIGNUP_USERNAME"] + "={0}&" +
                    WebConfigurationManager.AppSettings["SIGNUP_CONFIRMATION"] + "={1}",
                    userEmail, confirmationCode);
                sbBody.Append("<a href='" + strUri + "'");
                sbBody.Append(" >" + strUri + "</a>");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Thank you for your interest in ForteHCM.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("============================== Disclaimer ==============================");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("=========================== End Of Disclaimer ============================");
                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        /// <summary>
        /// Method that constructs the body content of the email.
        /// </summary>
        /// <param name="userRegistrationInfo">
        /// A <see cref="UserRegistrationInfo"/> that holds the user 
        /// registration info.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that contains mail body.
        /// </returns>
        private string GetMailBody(UserRegistrationInfo userRegistrationInfo)
        {
            StringBuilder sbBody = new StringBuilder();

            try
            {
                sbBody.Append("Dear Admin,");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Sub: " + HCMResource.UserRegistration_CorporateSubscriptionAccountRequestedSubject);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("A new corporate subscription account was created");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("The user details are given below:");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Company : " + userRegistrationInfo.Company);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Title : " + userRegistrationInfo.Title);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("First Name : " + userRegistrationInfo.FirstName);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Last Name : " + userRegistrationInfo.LastName);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Number Of Users : " + userRegistrationInfo.NumberOfUsers);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Review the details and process the request.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("============================== Disclaimer ==============================");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("=========================== End Of Disclaimer ============================");
                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        /// <summary>
        /// A Method that binds the bussiness type that are available in 
        /// repository to the checkboxlist
        /// </summary>
        private void BindBussinessTypes()
        {
            UserRegistration_typeOfBussinessCheckBoxList.DataSource = GetBussinessTypes();
            UserRegistration_typeOfBussinessCheckBoxList.DataValueField = "BusinessTypeID";
            UserRegistration_typeOfBussinessCheckBoxList.DataTextField = "BusinessTypeName";
            UserRegistration_typeOfBussinessCheckBoxList.DataBind();
        }

        /// <summary>
        /// A Method that gets the list of business types
        /// available in repository
        /// </summary>
        /// <returns>
        /// A <see cref="Forte.HCM.DataObjects.BusinessTypeDetail"/> that holds the 
        /// available business types in the repository
        /// </returns>
        private List<BusinessTypeDetail> GetBussinessTypes()
        {
            int TotalRecords = 0;
            return new AdminBLManager().GetBusinessTypes(string.Empty, "NAME", "A", 1, null, out TotalRecords);
        }

        /// <summary>
        /// A Method that validates whether the user inputed username
        /// is valid email or not using regular expression
        /// </summary>
        /// <param name="strUserEmailId">
        /// A <see cref="System.String"/> that holds the email to validate
        /// </param>
        /// <returns>
        /// A <see cref="System.Boolean"/> that holds the validity status
        /// </returns>
        private bool IsValidEmailAddress(string strUserEmailId)
        {
            //Regex regex = new Regex(".+@.+\\.[a-zA-Z]+", RegexOptions.Compiled);
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$", RegexOptions.Compiled);
            return regex.IsMatch(strUserEmailId);
        }

        #region Captcha Methods

        /// <summary>
        /// 
        /// </summary>
        private void GenerateCaptchaImage()
        {
            CaptchaImage ci = null;
            try
            {
                UserRegistration_capchaImage.Src = "";
                ci = new CaptchaImage(
                   RandomString(10), 200, 80, "Century Schoolbook");
                Session["CAPTCHA_IMAGE"] = ci.Image;
                ViewState[CAPTCHAVALUEVIEWSTATE] = ci.Text;
                CountHidden.Value = (Convert.ToInt32(CountHidden.Value) + 1).ToString();
                UserRegistration_capchaImage.Src = "~/Common/CaptchaImageHandler.ashx?id=" +
                     CountHidden.Value;
            }
            finally
            {
                if (ci != null) ci = null;
            }
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }

        #endregion Captcha Methods

        #endregion Private Methods

        #region Override Methods

        protected override bool IsValidData()
        {
            return false;
        }

        private bool IsValidData(out bool basicVerification)
        {
            bool isValid = true;

            basicVerification = true;
            StringBuilder sbErrorMessage = new StringBuilder();
            if (string.IsNullOrEmpty(UserRegistration_userNameTextBox.Text.Trim()))
            {
                //ShowErrorMessage(HCMResource.UserRegistraion_EmptyUserEmail);
                UserRegistration_userNameTextBox.CssClass = "mandatory_text_box";
                basicVerification = false;
                isValid = false;
            }
            else if (!IsValidEmailAddress(UserRegistration_userNameTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.UserRegistration_InvalidEmailAddress);


                isValid = false;
            }
            else if (!CheckForEmailAddressAvailability(UserRegistration_userNameTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.UserRegistration_UserEmailNotAvailable);
                isValid = false;
            }
            if (string.IsNullOrEmpty(UserRegistration_passwordTextBox.Text.Trim()))
            {
                //ShowErrorMessage(HCMResource.UserRegistration_EmptyPassword);
                UserRegistration_passwordTextBox.CssClass = "mandatory_text_box";
                basicVerification = false;
                isValid = false;
            }
            if (string.IsNullOrEmpty(UserRegistration_confrimPasswordTextBox.Text.Trim()))
            {
                //ShowErrorMessage(HCMResource.UserRegistration_ConfirmPasswordEmpty);
                UserRegistration_confrimPasswordTextBox.CssClass = "mandatory_text_box";
                basicVerification = false;
                isValid = false;
            }
            if (!string.IsNullOrEmpty(UserRegistration_passwordTextBox.Text.Trim()) &&
                !string.IsNullOrEmpty(UserRegistration_confrimPasswordTextBox.Text.Trim()) &&
                UserRegistration_confrimPasswordTextBox.Text.Trim() != UserRegistration_passwordTextBox.Text.Trim())
            {
                ShowErrorMessage(HCMResource.UserRegistration_PasswordMisMatch);
                isValid = false;
            }
            else if (!string.IsNullOrEmpty(UserRegistration_passwordTextBox.Text.Trim()) &&
                !string.IsNullOrEmpty(UserRegistration_confrimPasswordTextBox.Text.Trim()) &&
                UserRegistration_passwordTextBox.Text.Trim().Length < 6)
            {
                ShowErrorMessage(HCMResource.UserRegistration_MinimumPasswordFailed);
                isValid = false;
            }
            if (string.IsNullOrEmpty(UserRegistration_firstNameTextBox.Text.Trim()))
            {
                //ShowErrorMessage(HCMResource.UserRegistration_EmptyFirstName);
                UserRegistration_firstNameTextBox.CssClass = "mandatory_text_box";
                basicVerification = false;
                isValid = false;
            }
            if (string.IsNullOrEmpty(UserRegistration_lastNameTextBox.Text.Trim()))
            {
                //ShowErrorMessage(HCMResource.UserRegistration_EmptyLastName);
                UserRegistration_lastNameTextBox.CssClass = "mandatory_text_box";
                basicVerification = false;
                isValid = false;
            }
            if (string.IsNullOrEmpty(UserRegistration_phoneTextBox.Text.Trim()))
            {
                //ShowErrorMessage(HCMResource.UserRegistration_EmptyPhoneNo);
                UserRegistration_phoneTextBox.CssClass = "mandatory_text_box";
                basicVerification = false;
                isValid = false;
            }
            if (string.IsNullOrEmpty(UserRegistration_companyNameTextBox.Text.Trim()))
            {
                //ShowErrorMessage(HCMResource.UserRegistration_EmptyCompanyName);
                UserRegistration_companyNameTextBox.CssClass = "mandatory_text_box";
                basicVerification = false;
                isValid = false;
            }
            if (!UserRegistration_otherTypeOfBussinessCheckBox.Checked && UserRegistration_typeOfBussinessCheckBoxList.SelectedIndex < 0)
            {
                //ShowErrorMessage(HCMResource.UserRegistration_EmptyTypeOfBusiness);
                UserRegistration_otherTypeOfBussinessDIV.Style["border-color"] = "red";
                basicVerification = false;
                isValid = false;
            }
            //if (UserRegistration_typeOfBussinessCheckBoxList.SelectedIndex == UserRegistration_typeOfBussinessCheckBoxList.Items.Count - 1)
            if (UserRegistration_otherTypeOfBussinessCheckBox.Checked)
            {
                if (string.IsNullOrEmpty(UserRegistration_otherBussinessTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_EmptyOtherTypeOfBussiness);
                    isValid = false;
                }
                UserRegistration_otherBussinessDiv.Style.Add("display", "block");
            }
            else
                UserRegistration_otherBussinessDiv.Style.Add("display", "none");
            if (Session[SELECTEDREGISTRATIONTYPE] == null)
            {
                Response.Redirect("/Register/RegistrationType.aspx", false);
                return false;
            }
            else if (UserRegistration_registrationTypeLabel.Text.ToLower() == "corporate")
            {
                if (string.IsNullOrEmpty(UserRegistration_numberOfUsersTextBox.Text.Trim()))
                {
                    ShowErrorMessage(HCMResource.UserRegistration_EmptyNumberOfUsers);
                    isValid = false;
                }
                else
                {
                    int Temp = 0;
                    int.TryParse(UserRegistration_numberOfUsersTextBox.Text.Trim(), out Temp);
                    if (Temp == 0)
                    {
                        ShowErrorMessage(HCMResource.UserRegistration_InvalidNumberOfUsers);
                        isValid = false;
                    }
                    if (Temp > Convert.ToInt32(UserRegistration_MaximumCorporateUsersHidden.Value))
                    {
                        ShowErrorMessage(
                            string.Format(HCMResource.UserRegistration_MaximumSystemUserExceed, UserRegistration_MaximumCorporateUsersHidden.Value));
                        isValid = false;
                    }
                }
            }
            if (string.IsNullOrEmpty(UserRegistration_captchaImageTextBox.Text.Trim()))
            {
                // ShowErrorMessage(HCMResource.UserRegistration_EmptyCaptchaValue);
                UserRegistration_captchaImageTextBox.CssClass = "mandatory_text_box";
                basicVerification = false;
                isValid = false;
            }
            else if (UserRegistration_captchaImageTextBox.Text.Trim() != ViewState[CAPTCHAVALUEVIEWSTATE].ToString())
            {
                ShowErrorMessage(HCMResource.UserRegistration_CaptchaValueMisMatch);
                isValid = false;
            }
            return isValid;
        }

        protected override void LoadValues()
        {
            UserRegistrationInfo UserInfo = Session[SELECTEDREGISTRATIONTYPE] as UserRegistrationInfo;
            UserRegistration_subscriptionIdHidden.Value = UserInfo.SubscriptionId.ToString();
            UserRegistration_registrationTypeLabel.Text = UserInfo.SubscriptionName;
            UserRegistration_isTrialHidden.Value = UserInfo.IsTrial.ToString();
            if (!Utility.IsNullOrEmpty(UserInfo.TrialDays))
                UserRegistration_trialDaysHidden.Value = UserInfo.TrialDays.ToString();
            SetVisibileToUsers(
                UserInfo.SubscriptionName.ToLower() == "corporate" ? true : false);
            BindBussinessTypes();
            UserRegistration_MaximumCorporateUsersHidden.Value = new UserRegistrationBLManager().GetMaximumCorporateUsers().ToString();
            UserRegistration_numberOfUsersHelpImageButton.ToolTip = "Maximum number of users allowed is " +
                UserRegistration_MaximumCorporateUsersHidden.Value;
        }

        #endregion Override Methods
    }
}