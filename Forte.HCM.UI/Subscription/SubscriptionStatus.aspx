﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubscriptionStatus.aspx.cs"
    Inherits="Forte.HCM.UI.Subscription.SubscriptionStatus" MasterPageFile="~/MasterPages/SubscriptionMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/SubscriptionMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubscriptionMaster_body" runat="server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="height: 100%">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" style="height: 530px">
                    <tr>
                        <td align="center" class="td_padding_10_hcm" valign="top">
                            <table cellpadding="0" cellspacing="0" border="0" width="98%">
                                <tr>
                                    <td align="right" valign="top">
                                        <asp:Button ID="SubscriptionStatus_backToLoginButton" runat="server" PostBackUrl="~/Default.aspx"
                                            CommandName="Back to Login" SkinID="sknSignUpbackButton" BackColor="Transparent"
                                            BorderStyle="None" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="msg_align">
                                        <div>
                                            <asp:Label ID="SubscriptionStatus_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                            <asp:Label ID="SubscriptionStatus_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"
                                                EnableViewState="false"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_bottomborder">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
