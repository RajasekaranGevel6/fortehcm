﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SubscriptionMaster.Master"
    AutoEventWireup="true" CodeBehind="FeaturesList.aspx.cs" Inherits="Forte.HCM.UI.Subscription.FeaturesList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubscriptionMaster_body" runat="server">
    <div id="Resource_Selection_cnt_outer">
        <div id="sign_outer">
            <div id="sign_up_band_bg">
                <div id="title_Resource_Selection">
                    Pricing
                </div>
            </div>
        </div>
        <div id="price_outer">
            <div id="price_outer_left">
                <div id="price_outer_left_title">
                    Professional<span>Free</span></div>
                <div id="price_outer_left_cnt">
                    Powerful talent search and reports for targeted results</div>
            </div>
            <div id="price_outer_center">
                <div id="price_outer_center_inner">
                    <div id="price_outer_center_inner_left">
                        <div id="price_outer_center_title">
                            Business<span>$15/User/Month</span></div>
                    </div>
                    <div id="price_outer_center_inner_right">
                        <a href="#" onclick="Popup=window.open('pricing_freetrial.html','Popup','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no, width=420,height=150,left=300,top=203'); return false;">
                            <img src="../App_Themes/DefaultTheme/Images/price_icon.jpg" border="0" title="Business"></a></div>
                </div>
                <div id="price_outer_center_cnt">
                    Strong combination of talent screening
                    <br>
                    and assessment</div>
                <div id="price_outer_center_btn">
                    <a href="sign_up.html">
                        <img src="../App_Themes/DefaultTheme/Images/start_free_trail.jpg" title="Start Free Trail"
                            border="0"></a></div>
            </div>
            <div id="price_outer_right">
                <div id="price_outer_left_title">
                    Enterprise<!--span>Call: 1-847-867-9382</span--></div>
                <div id="price_outer_left_cnt">
                    Scalable and customizable talent
                    <br>
                    management</div>
                <div id="enterprise_btn_outer">
                    <a href="#" onclick="Popup=window.open('get_in_touch_popup.html','Popup','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no, width=480,height=450,left=300,top=203'); return false;">
                        <img src="../App_Themes/DefaultTheme/Images/get_in_touch.jpg" width="251" height="40"
                            title="Get in Touch" border="0"></a></div>
            </div>
        </div>
    </div>
    <div id="pricing_bottom">
        <div id="pricing_bottom_outer">
            <div id="pricing_headign">
            </div>
            <div id="pricing_title_Feature">
                Feature</div>
            <div id="pricing_title_Professional">
                Professional</div>
            <div id="pricing_title_Business">
                Business(per user per month)</div>
            <div id="pricing_title_enterprise">
                Enterprise( per account per month)</div>
            <div id="pricing_title_comments">
                Comments</div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_Feature_cnt">
                    No of users/account</div>
                <div id="pricing_title_sub_cnt">
                </div>
                <div id="pricing_title_Standard_cnt">
                    1</div>
                <div id="pricing_title_Enterprise_cnt">
                </div>
                <div id="pricing_title_Enterprise_cnt">
                    1</div>
                <div id="pricing_title_comments_cnt">
                    Unlimited</div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_intelliSPOT">
                    intelliSPOT</div>
                <div id="pricing_title_sub_cnt">
                </div>
                <div id="pricing_title_Standard_cnt">
                </div>
                <div id="pricing_title_Enterprise_cnt">
                </div>
                <div id="pricing_title_Enterprise_cnt">
                </div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_empty">
                </div>
                <div id="pricing_title_sub_cnt">
                    Position profiles created</div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" alt="Delete" width="16" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check">(
                    Limited to 5)</div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check">(Unlimited)</div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_empty">
                </div>
                <div id="pricing_title_sub_cnt">
                    Weighted profile search</div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_empty">
                </div>
                <div id="pricing_title_sub_cnt">
                    Candidate search</div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_empty">
                </div>
                <div id="pricing_title_sub_cnt">
                    Links to external job board</div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check">(
                    Limited to 1)</div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check">(
                    Limited to 1)
                </div>
                <div id="pricing_title_Enterprise_cnt">
                    Multiple</div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_empty">
                </div>
                <div id="pricing_title_sub_cnt">
                    Position profile search</div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_empty">
                </div>
                <div id="pricing_title_sub_cnt">
                    Parsing owned resumes</div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_empty">
                </div>
                <div id="pricing_title_sub_cnt">
                    No of resumes uploaded</div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_empty">
                </div>
                <div id="pricing_title_sub_cnt">
                    Multiple views(Pan &amp; List)</div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_Feature_cnt">
                    Desktop utility</div>
                <div id="pricing_title_sub_cnt">
                </div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" alt="Delete" width="16" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_intelliSPOT">
                    Cloning</div>
                <div id="pricing_title_sub_cnt">
                </div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" alt="Delete" width="16" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check">(
                    Limited to 10)</div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check">(Limited
                    to 50)</div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_Feature_cnt">
                    Product customization</div>
                <div id="pricing_title_sub_cnt">
                </div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" alt="Delete" width="16" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" alt="Delete" width="16" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_intelliSPOT">
                    Assessment</div>
                <div id="pricing_title_sub_cnt">
                </div>
                <div id="pricing_title_Standard_cnt">
                </div>
                <div id="pricing_title_Enterprise_cnt">
                </div>
                <div id="pricing_title_Enterprise_cnt">
                </div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_empty">
                </div>
                <div id="pricing_title_sub_cnt">
                    Online test administered</div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" alt="Delete" width="16" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check">(
                    Limited to 3)</div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check">(Limited
                    to 30)&nbsp;<a href="#" onclick="Popup=window.open('trial expires.html','Popup','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no, width=420,height=150,left=300,top=203'); return false;">i</a></div>
                <div id="pricing_title_comments_cnt">
                    i= call for assessments
                    <br>
                    over 30</div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_empty">
                </div>
                <div id="pricing_title_sub_cnt">
                    Creat test</div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" alt="Delete" width="16" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check">(
                    Limited to 5)</div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check">(Limited
                    to 50)
                </div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_empty">
                </div>
                <div id="pricing_title_sub_cnt">
                    Client content services</div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" alt="Delete" width="16" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif"><sup>1</sup></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_empty">
                </div>
                <div id="pricing_title_sub_cnt">
                    Infuse own content to administer test</div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" alt="Delete" width="16" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_empty">
                </div>
                <div id="pricing_title_sub_cnt">
                    Collaborative interview</div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" alt="Delete" width="16" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" alt="Delete" width="16" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif">(Limited to 15)&nbsp;<a
                        href="#" onclick="Popup=window.open('trial expires.html','Popup','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no, width=420,height=150,left=300,top=203'); return false;">i</a></div>
                <div id="pricing_title_comments_cnt">
                    i= call for interviews over 15</div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_Feature_cnt">
                    Admin</div>
                <div id="pricing_title_sub_cnt">
                </div>
                <div id="pricing_title_Standard_cnt">
                </div>
                <div id="pricing_title_Enterprise_cnt">
                </div>
                <div id="pricing_title_Enterprise_cnt">
                </div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_empty">
                </div>
                <div id="pricing_title_sub_cnt">
                    API for 3rd party integration</div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" alt="Delete" width="16" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif"><sup>1</sup></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_intelliSPOT">
                    Support</div>
                <div id="pricing_title_sub_cnt">
                </div>
                <div id="pricing_title_Standard_cnt">
                    Email</div>
                <div id="pricing_title_Enterprise_cnt">
                    Email</div>
                <div id="pricing_title_Enterprise_cnt">
                    Email/Phone</div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_Feature_cnt">
                    Branding</div>
                <div id="pricing_title_sub_cnt">
                </div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" width="16" alt="Delete" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" width="16" alt="Delete" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_intelliSPOT">
                    Access to central<br>
                    repository</div>
                <div id="pricing_title_sub_cnt">
                </div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" width="16" alt="Delete" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" width="16" alt="Delete" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_Feature_cnt">
                    Client Management</div>
                <div id="pricing_title_sub_cnt">
                </div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" width="16" alt="Delete" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check">(
                    Limited to 5)</div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_intelliSPOT">
                    Cyber Proctor</div>
                <div id="pricing_title_sub_cnt">
                </div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/delete.png" width="16" alt="Delete" height="16"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
            <div id="pricing_inner_cnt">
                <div id="pricing_title_Feature_cnt">
                    Reports</div>
                <div id="pricing_title_sub_cnt">
                </div>
                <div id="pricing_title_Standard_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_Enterprise_cnt">
                    <img src="../App_Themes/DefaultTheme/Images/upsell_check.gif" title="upsell_check"></div>
                <div id="pricing_title_comments_cnt">
                </div>
            </div>
        </div>
        <div id="pricing_bottom_outermain">&nbsp;&nbsp;</div>
    </div>
    
</asp:Content>
