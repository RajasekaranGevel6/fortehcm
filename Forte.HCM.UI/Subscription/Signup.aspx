﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Signup.aspx.cs" Inherits="Forte.HCM.UI.Subscription.Signup"
    MasterPageFile="~/MasterPages/SubscriptionMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/SubscriptionMaster.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var Text = '<%= UserRegistration_otherBussinessTextBox.ClientID %>';
        var Div = '<%= UserRegistration_otherBussinessDiv.ClientID %>';

        function ShowValue(CheckBoxch) {
            if (CheckBoxch) {
                document.getElementById(Text).value = "";
                document.getElementById(Div).style.display = "block";
            }
            else {
                document.getElementById(Text).value = "";
                document.getElementById(Div).style.display = "none";
            }
        }
        // Method that will show/hide the captcha image help panel.
        function ShowWhatsThis(show) {
            if (show == 'Y') {
                $find('signUp_captchaHelpTextModalPopUpExtender').show();
                //document.getElementById('SignUp_whatsThisDiv').style.display = "block";
            }

            else {
                //document.getElementById('SignUp_whatsThisDiv').style.display = "none";
                $find('signUp_captchaHelpTextModalPopUpExtender').hide();
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SubscriptionMaster_body" runat="server">
    <center>
        <div style="margin: auto; width: 100%; height: 100%; background-position: center;">
            <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UserRegistration_userNameUpdatePanel">
                <ProgressTemplate>
                    <div id='loading' style="width: 100%; left: 0; top: 0; background-color: Red">
                        <div id="fade" class="black_overlay_display" style="filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=30);">
                            <div class="white_content_display" align="center">
                                <div class="centerimage">
                                    <div id="divProgress2" style="vertical-align: middle; width: 100%; position: fixed;
                                        background-position: center; height: 100%;">
                                        <table border="2" width="100%" align="center" cellpadding="0" cellspacing="0" style="height: 100%;">
                                            <tr>
                                                <td>
                                                    <asp:Image ID="Image1" ImageUrl="~/App_Themes/DefaultTheme/Images/loader.gif" runat="server"
                                                        ToolTip="Loading" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <div style="width: 100%; height: 100%">
            <table cellpadding="0" cellspacing="0" border="0" width="995" style="height: 100%">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="height: 530px">
                            <tr>
                                <td class="signup_main_bg_fortehcm">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td valign="top" align="left" style="padding: 25px; width: 40%; table-layout: fixed">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td class="td_padding_bottom_top_hcm" align="left">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="signup_title">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="UserRegistration_registrationTypeHeaderLabel" runat="server" Text="Registration Type : "
                                                                                        CssClass="signup_registration_type_label"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="UserRegistration_registrationTypeLabel" runat="server" CssClass="signup_registration_type_value_label"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                                            <tr>
                                                                                <td class="signup_feature_label_header">
                                                                                    Features
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:PlaceHolder ID="Signup_featuresPlaceHolder" runat="server"></asp:PlaceHolder>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="signup_link_message">
                                                            Click here to go to
                                                            <asp:LinkButton ID="Signup_subscriptionTypesLinkButton" runat="server" Text="Subscription Types"
                                                                CssClass="signup_link_btn" PostBackUrl="~/Subscription/SubscriptionType.aspx"
                                                                ToolTip="Click here to go to subscription types page">
                                                            </asp:LinkButton>
                                                            page
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="right" valign="top" style="width: 60%; height: 530px">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" style="height: 100%">
                                                    <tr>
                                                        <td class="signup_bg_shade_fortehcm" style="width: 14px">
                                                        </td>
                                                        <td class="signup_sub_bg_fortehcm" align="left" style="padding: 25px; width: 560px;
                                                            table-layout: fixed">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td valign="top">
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td class="td_padding_bottom_top_hcm">
                                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                                        <tr>
                                                                                            <td class="signup_title">
                                                                                                Sign Up
                                                                                            </td>
                                                                                            <td class="signup_subtitle">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="msg_align">
                                                                                    <div>
                                                                                        <asp:UpdatePanel ID="UserRegistration_topMessageLabelUpdatePanel" runat="server">
                                                                                            <ContentTemplate>
                                                                                                <asp:Label ID="UserRegistration_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                                                                    ForeColor="Red" Width="100%"></asp:Label>
                                                                                                <asp:Label ID="UserRegistration_topSuccessMessageLabel" runat="server" EnableViewState="false"
                                                                                                    Width="100%" ForeColor="Green"></asp:Label>
                                                                                            </ContentTemplate>
                                                                                        </asp:UpdatePanel>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellpadding="1" cellspacing="2" border="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="UserRegistration_userNameHeaderLabel" runat="server" Text="User ID"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 25px;" align="right">
                                                                                                <span class="mandatory">*</span>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div>
                                                                                                    <asp:UpdatePanel ID="UserRegistration_userNameUpdatePanel" runat="server">
                                                                                                        <ContentTemplate>
                                                                                                            <asp:TextBox ID="UserRegistration_userNameTextBox" runat="server" MaxLength="100"
                                                                                                                Width="200px" AutoCompleteType="None"></asp:TextBox>
                                                                                                            &nbsp;&nbsp;
                                                                                                            <asp:LinkButton ID="UserRegistration_checkUserEmailIdAvailableButton" CssClass="link_button_hcm"
                                                                                                                OnClick="UserRegistration_checkUserEmailIdAvailableButton_Click" runat="server"
                                                                                                                Text="Check"></asp:LinkButton>
                                                                                                            <div id="UserRegistration_userEmailAvailableStatusDiv" runat="server" style="display: none;">
                                                                                                                <asp:Label ID="UserRegistration_inValidEmailAvailableStatus" runat="server" Width="100%"
                                                                                                                    EnableViewState="false" ForeColor="Red"></asp:Label>
                                                                                                                <asp:Label ID="UserRegistration_validEmailAvailableStatus" runat="server" Width="100%"
                                                                                                                    EnableViewState="false" ForeColor="Green"></asp:Label>
                                                                                                            </div>
                                                                                                        </ContentTemplate>
                                                                                                    </asp:UpdatePanel>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="UserRegistration_passwordHeaderLabel" runat="server" Text="Password"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 25px;" align="right">
                                                                                                <span class="mandatory">*</span>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="UserRegistration_passwordTextBox" CssClass="textbox_hcm" runat="server"
                                                                                                    TextMode="Password" MaxLength="20"></asp:TextBox>
                                                                                                <ajaxToolKit:PasswordStrength ID="UserRegistration_passwordTextPasswordStrength"
                                                                                                    runat="server" DisplayPosition="BelowRight" HelpHandlePosition="LeftSide" RequiresUpperAndLowerCaseCharacters="true"
                                                                                                    PreferredPasswordLength="6" HelpStatusLabelID="UserRegistration_passwordHelpLabel"
                                                                                                    StrengthIndicatorType="Text" TargetControlID="UserRegistration_passwordTextBox"
                                                                                                    TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent">
                                                                                                </ajaxToolKit:PasswordStrength>
                                                                                                <br />
                                                                                                <asp:Label ID="UserRegistration_passwordHelpLabel" runat="server"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="UserRegistration_reEnterPasswordHeaderLabel" runat="server" Text="Re-type Password"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 25px;" align="right">
                                                                                                <span class="mandatory">*</span>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="UserRegistration_confrimPasswordTextBox" CssClass="textbox_hcm"
                                                                                                    runat="server" TextMode="Password" MaxLength="20"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="UserRegistration_firstNameHeaderLabel" runat="server" Text="First Name"></asp:Label>
                                                                                            </td>
                                                                                            <td align="right">
                                                                                                <span class="mandatory">*</span>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="UserRegistration_firstNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="UserRegistration_lastNameHeaderLabel" runat="server" Text="Last Name"></asp:Label>
                                                                                            </td>
                                                                                            <td align="right">
                                                                                                <span class="mandatory">*</span>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="UserRegistration_lastNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="UserRegistration_phoneHeaderLabel" runat="server" Text="Phone"></asp:Label>
                                                                                            </td>
                                                                                            <td align="right">
                                                                                                <span class="mandatory">*</span>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="UserRegistration_phoneTextBox" runat="server" MaxLength="15"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="UserRegistration_companyNameHeaderLabel" runat="server" Text="Company Name"></asp:Label>
                                                                                            </td>
                                                                                            <td align="right">
                                                                                                <span class="mandatory">*</span>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="UserRegistration_companyNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="UserRegistration_tileHeaderLable" runat="server" Text="Title"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="UserRegistration_titleTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="UserRegistration_typeOfBussinessHeaderLabel" runat="server" Text="Type of Business"></asp:Label>
                                                                                            </td>
                                                                                            <td align="right">
                                                                                                <span class="mandatory">*</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <div id="UserRegistration_otherTypeOfBussinessDIV" runat="server" class="checkbox_list_bg">
                                                                                                    <asp:CheckBoxList ID="UserRegistration_typeOfBussinessCheckBoxList" runat="server"
                                                                                                        RepeatDirection="Horizontal" RepeatColumns="3" TextAlign="Right" CellSpacing="5">
                                                                                                    </asp:CheckBoxList>
                                                                                                    &nbsp;
                                                                                                    <asp:CheckBox ID="UserRegistration_otherTypeOfBussinessCheckBox" runat="server" Text="Other" />
                                                                                                    <div id="UserRegistration_otherBussinessDiv" runat="server" style="display: none;">
                                                                                                        <asp:TextBox ID="UserRegistration_otherBussinessTextBox" runat="server" MaxLength="30"></asp:TextBox>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr id="UserRegistration_numberOfUsersTr" runat="server" visible="false">
                                                                                            <td>
                                                                                                <asp:Label ID="UserRegistration_numberOfUsersHeaderLabel" runat="server" Text="Number of Users"></asp:Label>
                                                                                            </td>
                                                                                            <td align="right">
                                                                                                <span class="mandatory">*</span>
                                                                                            </td>
                                                                                            <td>
                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td style="width: 35%;">
                                                                                                            <asp:TextBox ID="UserRegistration_numberOfUsersTextBox" runat="server" MaxLength="2"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:ImageButton ID="UserRegistration_numberOfUsersHelpImageButton" runat="server"
                                                                                                                SkinID="sknHelpImageButton" OnClientClick="return false"/>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                <input type="hidden" runat="server" id="UserRegistration_MaximumCorporateUsersHidden"
                                                                                                    value="0" />
                                                                                                <input type="hidden" runat="server" id="UserRegistration_subscriptionIdHidden" value="0" />
                                                                                                <input type="hidden" runat="server" id="UserRegistration_isTrialHidden" value="0" />
                                                                                                <input type="hidden" runat="server" id="UserRegistration_trialDaysHidden" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                            </td>
                                                                                            <td>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:UpdatePanel ID="Temp" runat="server">
                                                                                                    <ContentTemplate>
                                                                                                        <input type="hidden" runat="server" id="CountHidden" value="0" />
                                                                                                        <img id="UserRegistration_capchaImage" src="" runat="server" alt="Captcha Image" />
                                                                                                        &nbsp;
                                                                                                        <asp:LinkButton ID="UserRegistration_regenerateCaptchaImageLinkButton" runat="server"
                                                                                                            Text="Regenerate Key" CssClass="link_button_hcm" OnClick="UserRegistration_regenerateCaptchaImageLinkButton_Click"></asp:LinkButton>
                                                                                                        &nbsp;&nbsp;<asp:LinkButton ID="TestInstructions_whatsThisLinkButton" runat="server"
                                                                                                            Text="What's This" SkinID="sknActionLinkButton" OnClientClick="javascript:return ShowWhatsThis('Y')" />
                                                                                                        <ajaxToolKit:ModalPopupExtender ID="signUp_captchaHelpTextModalPopUpExtender" runat="server"
                                                                                                            BehaviorID="signUp_captchaHelpTextModalPopUpExtender" TargetControlID="TestInstructions_whatsThisLinkButton"
                                                                                                            PopupControlID="SignUp_whatsThisDiv" BackgroundCssClass="modalBackground">
                                                                                                        </ajaxToolKit:ModalPopupExtender>
                                                                                                        <div id="SignUp_whatsThisDiv" style="display: none; height: 220px; width: 270px;
                                                                                                            left: 390px; top: 340px; z-index: 0; position: absolute" class="popupcontrol_confirm">
                                                                                                            <table width="100%" cellpadding="10" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td style="width: 95%" class="popup_header_text" valign="middle" align="left">
                                                                                                                        <asp:Literal ID="TestInstructions_whatsThisDiv_titleLiteral" runat="server" Text="What's This"></asp:Literal>
                                                                                                                    </td>
                                                                                                                    <td style="width: 5%" valign="top" align="right">
                                                                                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="SignUp_whatsThisDiv_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                                                                                                                        OnClientClick="javascript:return ShowWhatsThis('N')" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2">
                                                                                                                        <table cellpadding="10" cellspacing="0" border="0">
                                                                                                                            <tr>
                                                                                                                                <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                                                                                                    <table width="100%" cellspacing="0" border="0">
                                                                                                                                        <tr>
                                                                                                                                            <td align="center" class="label_field_text">
                                                                                                                                                <asp:Literal ID="SignUp_whatsThisDiv_messageLiteral" runat="server" Text="The image you are looking at is known as a captcha. This is a security test to determine whether or not the user is human. In order to submit the form, you must enter the text seen in the image into the text box. The letters are case sensitive. If you can't make out the text, click the Regenerate Key link seen below the image">
                                                                                                                                                </asp:Literal>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                        <tr>
                                                                                                                                            <td class="td_height_20">
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </ContentTemplate>
                                                                                                </asp:UpdatePanel>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="UserRegistration_captchaImageHeaderLabel" runat="server" Text="Enter text as shown"></asp:Label>
                                                                                            </td>
                                                                                            <td align="right">
                                                                                                <span class="mandatory">*</span>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:UpdatePanel ID="UserRegistration_captchaImageTextBoxUpdatePanel" runat="server">
                                                                                                    <ContentTemplate>
                                                                                                        <asp:TextBox ID="UserRegistration_captchaImageTextBox" runat="server" AutoCompleteType="None"></asp:TextBox>
                                                                                                    </ContentTemplate>
                                                                                                </asp:UpdatePanel>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="msg_align">
                                                                                    <asp:UpdatePanel ID="UserRegistration_bottonMessageLabelUpdatePanel" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <asp:Label ID="UserRegistration_errorMessageLabel" runat="server" EnableViewState="false"
                                                                                                ForeColor="Red" Width="100%"></asp:Label>
                                                                                            <asp:Label ID="UserRegistration_bottomSuccessMessageLabel" runat="server" EnableViewState="False"
                                                                                                Width="100%" ForeColor="Green"></asp:Label>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="msg_align">
                                                                                    <asp:Button ID="UserRegistration_registerButton" SkinID="sknSignUpregisterButton"
                                                                                        BackColor="Transparent" BorderStyle="None" runat="server" OnCommand="UserRegistration_registerButton_CommandClick" />&nbsp;
                                                                                    <asp:Button ID="UserRegistration_cancelRegistrationButton" runat="server" SkinID="sknSignUpcancelButton"
                                                                                        BackColor="Transparent" BorderStyle="None" OnCommand="UserRegistration_cancelRegistrationButton_CancelClick" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </center>
</asp:Content>
