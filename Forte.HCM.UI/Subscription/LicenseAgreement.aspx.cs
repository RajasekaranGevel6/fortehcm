﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// LicenseAgreement.aspx.cs
// File that represents the user interface layout and functionalities
// for the LicenseAgreement page. This page shows the license agreement
// to the user and get the acceptance. This class inherits the 
// Forte.HCM.UI.Common.PageBase class

#endregion Header 

#region Directives

using System;
using System.Configuration;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

using Resources;

#endregion Directives

namespace Forte.HCM.UI.Subscription
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the LicenseAgreement page. This page shows the license agreement
    /// to the user and get the acceptance. This class inherits the 
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class LicenseAgreement : PageBase
    {
        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("License Agreement");

                // Check if query string does not contains the activation parameters, for 
                // non logged users.
                if  (base.userID == 0 && 
                    (Utility.IsNullOrEmpty(Request.QueryString[ConfigurationManager.AppSettings["SIGNUP_USERNAME"]])
                   || Utility.IsNullOrEmpty(Request.QueryString[ConfigurationManager.AppSettings["SIGNUP_CONFIRMATION"]])))
                {
                    // Show error message.
                    base.ShowMessage(LicenseAgreement_topErrorMessageLabel, 
                        LicenseAgreement_bottomErrorMessageLabel, 
                        "Activation parameters are missing. Please enter the correct URL");

                    // Hide the action buttons.
                    LicenseAgreement_agreeButton.Visible = false;
                    LicenseAgreement_disagreeButton.Visible = false;

                    return;
                }

                if (LicenseAgreement_showTimerHiddenField.Value != null &&
                    LicenseAgreement_showTimerHiddenField.Value == "G")
                {
                    if (base.userID == 0)
                        Response.Redirect("../Default.aspx", false);
                    else
                        Response.Redirect("../Landing.aspx", false);
                    
                    return;
                }

                if (!IsPostBack)
                {
                    // Set navigate page captions.
                    if (base.userID == 0)
                    {
                        LicenseAgreement_remainingTimeTextHeadLabel.Text = " sec to redirect to home page.";
                        LicenseAgreement_clickRemainingTextHeadLabel.Text = " to redirect to home page.";
                    }
                    else
                    {
                        LicenseAgreement_remainingTimeTextHeadLabel.Text = " sec to redirect to landing page.";
                        LicenseAgreement_clickRemainingTextHeadLabel.Text = " to redirect to landing page.";
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the agree button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will accept the agreement and proceed the user to the landing
        /// page.
        /// </remarks>
        protected void LicenseAgreement_agreeButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ClearMessages();

                bool proceed = true;

                // Check if user already logged in or not.
                if (base.userID == 0)
                {
                    // User not yet logged in and opted to activate the account.
                    // Activate the user and update the legal accepted status.
                    string encryptedUserName = Request.QueryString
                        [ConfigurationManager.AppSettings["SIGNUP_USERNAME"]].Replace(' ', '+');

                    // Get decrypted user name.
                    string decryptedUserName = new EncryptAndDecrypt().DecryptString(encryptedUserName);

                    // Get encrypted confirmation code.
                    string encryptedConfirationCode = Request.QueryString
                        [ConfigurationManager.AppSettings["SIGNUP_CONFIRMATION"]].Replace(' ', '+');

                    try
                    {
                        // Check and update with the database.
                        string displayName = new UserRegistrationBLManager().CheckForActivationCodeWithLegalAcceptance
                            (decryptedUserName, encryptedConfirationCode);

                        base.ShowMessage(LicenseAgreement_topSuccessMessageLabel,
                            LicenseAgreement_bottomSuccessMessageLabel,
                            "Your account has been activated successfully");
                    }
                    catch (Exception exp)
                    {
                        proceed = false;
                        base.ShowMessage(LicenseAgreement_topErrorMessageLabel,
                            LicenseAgreement_bottomErrorMessageLabel, exp.Message);
                    }
                }
                else
                {
                    // User already logged in and opted to accept the legal agreement.
                    // Update the legal accepted status.
                    new UserRegistrationBLManager().UpdateLegalAcceptedStatus(base.userID, true);

                    base.ShowMessage(LicenseAgreement_topSuccessMessageLabel,
                        LicenseAgreement_bottomSuccessMessageLabel,
                        "Your license agreement has been accepted successfully");

                    // Get the user detail from session and.
                    if (Session["USER_DETAIL"] == null)
                    {
                        base.ShowMessage(LicenseAgreement_topErrorMessageLabel,
                            LicenseAgreement_bottomErrorMessageLabel, "Your session has been expired");

                        proceed = false;
                    }
                    else
                    {
                        // Get the user detail from session.
                        UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                        // Update the 'isLegalAccepted' status.
                        userDetail.LegalAccepted = true;

                        // Keep the user detail in session.
                        Session["USER_DETAIL"] = userDetail;
                    }
                }

                if (proceed == false)
                    return;

                // Hide the action buttons.
                LicenseAgreement_agreeButton.Visible = false;
                LicenseAgreement_disagreeButton.Visible = false;

                // Show the timer.
                LicenseAgreement_showTimerHiddenField.Value = "Y";
                LicenseAgreement_timerDiv.Style.Add("display", "block");
            }
            catch (Exception exp)
            {
                if (exp.Message.ToUpper().Contains("INVALID LENGTH FOR A"))
                {
                     base.ShowMessage(LicenseAgreement_topErrorMessageLabel,
                        LicenseAgreement_bottomErrorMessageLabel, 
                        "Invalid confirmation code");
                }
                else if (exp.Message.ToUpper().Contains("INVALID CHARACTER IN A"))
                {
                     base.ShowMessage(LicenseAgreement_topErrorMessageLabel,
                        LicenseAgreement_bottomErrorMessageLabel, 
                        "Invalid confirmation code");
                }
                else if (exp.Message.ToUpper().Contains("LENGTH OF THE DATA TO DECRYPT"))
                {
                    base.ShowMessage(LicenseAgreement_topErrorMessageLabel,
                       LicenseAgreement_bottomErrorMessageLabel,
                       "Invalid confirmation code");
                }
                else if (exp.Message.ToUpper().Contains("PADDING IS INVALID AND CANNOT BE"))
                {
                    base.ShowMessage(LicenseAgreement_topErrorMessageLabel,
                        LicenseAgreement_bottomErrorMessageLabel,
                        "Invalid confirmation code");
                }
                else
                {
                    base.ShowMessage(LicenseAgreement_topErrorMessageLabel,
                        LicenseAgreement_bottomErrorMessageLabel, exp.Message);
                }

                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the diaagree button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will not allow the user to proceed further.
        /// </remarks>
        protected void LicenseAgreement_disagreeButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ClearMessages();

                // Check if user already logged in or not.
                if (base.userID == 0)
                {
                    base.ShowMessage(LicenseAgreement_topErrorMessageLabel,
                       LicenseAgreement_bottomErrorMessageLabel,
                       "Click on agree button to activate your account. Clicking on disagree will not allow you to proceed further");
                }
                else
                {
                    base.ShowMessage(LicenseAgreement_topErrorMessageLabel,
                       LicenseAgreement_bottomErrorMessageLabel,
                       "Click on agree button to accept the license agreement. Clicking on disagree will not allow you to proceed further");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the navigate link button is 
        /// clicked in the timer panel.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void LicenseAgreement_navigateLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (base.userID == 0)
                    Response.Redirect("../Default.aspx", false);
                else
                    Response.Redirect("../Landing.aspx", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Overridden method that loads default values into controls.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Overridden Methods

        #region Private Methods

        /// <summary>
        /// Method that clear the messages.
        /// </summary>
        private void ClearMessages()
        {
            LicenseAgreement_topSuccessMessageLabel.Text = string.Empty;
            LicenseAgreement_bottomSuccessMessageLabel.Text = string.Empty;
            LicenseAgreement_topErrorMessageLabel.Text = string.Empty;
            LicenseAgreement_bottomErrorMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods
    }
}