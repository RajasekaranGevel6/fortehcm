﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SingleQuestionEntry.cs
// File that represents the user interface for question entry page.
// This will helps to add a single question to the question repository.

#endregion Header

#region Directives

using System;
using System.Web;
using System.Text;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using Resources;
using System.IO;


#endregion Directives

namespace Forte.HCM.UI.InterviewQuestions
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities for 
    /// the question entry page. This page helps add a single question to the
    /// question repository.
    /// </summary>
    /// <remarks>
    /// This class subclasses the <see cref="PageBase"/> object.
    /// </remarks>
    public partial class InterviewSingleQuestionEntry : PageBase
    {
        #region Events Handlers

        /// <summary>
        /// This method checks to see if the querystring is found or not. If the 
        /// querystring contains the questionkey, the page will be considered as 
        /// edit question entry page. Otherwise, it will be InterviewSingleQuestionEntry page.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.DefaultButton = InterviewSingleQuestionEntry_topSaveButton.UniqueID;
                Page.SetFocus(InterviewSingleQuestionEntry_topSaveButton.ClientID);
                ClearAllLabelMessage();

                if (!IsPostBack)
                {

                    Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;

                    LoadValues();

                    Page.Form.DefaultFocus = InterviewSingleQuestionEntry_questionTextBox.UniqueID;
                    InterviewSingleQuestionEntry_questionTextBox.Focus();

                    // Check if the querystring is empty (i.e questionkey). If the condition meets
                    // TRUE, the page will show 4 answer hoices by default. if the page is opened 
                    // in EDIT MODE. Note: The query string will hold questionkey only.
                    if (Utility.IsNullOrEmpty(Request.QueryString["questionkey"]))
                    {
                        List<AnswerChoice> choices = new List<AnswerChoice>();

                        // Generate a loop up to NumberOfAnswerChoices which is a constant and 
                        // it is defined in the PageBase. If its value is changed in the PageBase,
                        // the page will show the options accordingly.
                        for (int choicesId = 0; choicesId < 1; choicesId++)
                        {
                            choices.Add(new AnswerChoice(string.Empty, choicesId, false));
                        }

                        InterviewSingleQuestionEntry_answerChoicesGridView.DataSource = choices;
                        InterviewSingleQuestionEntry_answerChoicesGridView.DataBind();

                        ReLoadViewState();

                        // Set visibility false to the CREDITS EARNED field. No need to show these
                        // for adding new question.
                        InterviewSingleQuestionEntry_creditTextBox.Visible = false;
                        InterviewSingleQuestionEntry_creditHeadLabel.Visible = false;

                        // Set page title
                        InterviewSingleQuestionEntry_headerLiteral.Text =
                            Resources.HCMResource.SingleQuestionEntry_Title;
                        Master.SetPageCaption(Resources.HCMResource.SingleQuestionEntry_Title);

                        // Show currently logged in user name in the Author textbox
                        InterviewSingleQuestionEntry_authorTextBox.Text
                            = new CommonBLManager().GetUserDetail(base.userID).FirstName;
                        InterviewSingleQuestionEntry_authorIdHiddenField.Value = base.userID.ToString();
                    }
                    else
                    {
                        // Show the edit mode table row that displays the 
                        // question ID and last modified date.
                        InterviewSingleQuestionEntry_editModeTR.Visible = true;

                        ViewState["IS_EDIT_MODE"] = true;

                        // Change the page title and header label
                        InterviewSingleQuestionEntry_headerLiteral.Text =
                            Resources.HCMResource.SingleQuestionEntry_Edit_Title;
                        Master.SetPageCaption(Resources.HCMResource.SingleQuestionEntry_Edit_Title);

                        // Set the visibility status for CREDITS EARNED field
                        InterviewSingleQuestionEntry_creditHeadLabel.Visible = true;
                        InterviewSingleQuestionEntry_creditTextBox.Visible = true;

                        LoadQuestionDetails(Request.QueryString["questionkey"].ToString());

                        // Check if the page is redirected from single question after saving a
                        // question. If so, show the success message accordingly.
                        if (!Utility.IsNullOrEmpty(Request.QueryString["fromnew"]) &&
                            Request.QueryString["fromnew"].ToUpper() == "Y")
                        {
                            base.ShowMessage(InterviewSingleQuestionEntry_topSuccessMessageLabel,
                                InterviewSingleQuestionEntry_bottomSuccessMessageLabel,
                                string.Format(Resources.HCMResource.SingleQuestionEntry_AddedSuccessfully,
                                Request.QueryString["questionkey"]));
                        }
                        else if (!Utility.IsNullOrEmpty(Request.QueryString["fromedit"]) &&
                            Request.QueryString["fromedit"].ToUpper() == "Y")
                        {
                            base.ShowMessage(InterviewSingleQuestionEntry_topSuccessMessageLabel,
                                InterviewSingleQuestionEntry_bottomSuccessMessageLabel,
                                string.Format(Resources.HCMResource.SingleQuestionEntry_UpdatedSuccessfully,
                                Request.QueryString["questionkey"]));
                        }
                    }
                }

                // Change the EDIT page title
                if (Convert.ToBoolean(ViewState["IS_EDIT_MODE"]))
                {
                    InterviewSingleQuestionEntry_headerLiteral.Text =
                        Resources.HCMResource.SingleQuestionEntry_Edit_Title;
                    Master.SetPageCaption(Resources.HCMResource.SingleQuestionEntry_Edit_Title);
                }
                else
                {
                    // Set SINGLE question entry page title
                    InterviewSingleQuestionEntry_headerLiteral.Text =
                        Resources.HCMResource.SingleQuestionEntry_Title;
                    Master.SetPageCaption(Resources.HCMResource.SingleQuestionEntry_Title);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                    InterviewSingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }

            // Subscribing event to prevent existing category.
            InterviewSingleQuestionEntry_categorySubjectControl.ControlMessageThrown +=
                new CategorySubjectControl.ControlMessageThrownDelegate
                    (InterviewSingleQuestionEntry_categorySubjectControl_ControlMessageThrown);

            // Open Search User popup by calling javascript funciton
            InterviewSingleQuestionEntry_authorImageButton.Attributes.Add("onclick", "return LoadAdminName('"
                + InterviewSingleQuestionEntry_dummyAuthorId.ClientID + "','"
                + InterviewSingleQuestionEntry_authorIdHiddenField.ClientID + "','"
                + InterviewSingleQuestionEntry_authorTextBox.ClientID + "','QA')");

            InterviewSingleQuestionEntry_addImageLinkButton.Attributes.Add("onclick", "return ShowAddImagePanel('" + this.InterviewSingleQuestionEntry_AddQuestionImageTR.ClientID + "','" + InterviewSingleQuestionEntry_addImageLinkButton.ClientID + "')");
        }

        /// <summary>
        /// Handler that shows error/success messages.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="c">
        /// A <see cref="ControlMessageEventArgs"/> that contains the event data.
        /// </param>
        private void InterviewSingleQuestionEntry_categorySubjectControl_ControlMessageThrown
            (object sender, ControlMessageEventArgs c)
        {
            if (c.MessageType == MessageType.Error)
            {
                InterviewSingleQuestionEntry_topErrorMessageLabel.Text = c.Message.ToString();
                InterviewSingleQuestionEntry_bottomErrorMessageLabel.Text = c.Message.ToString();
            }
            else if (c.MessageType == MessageType.Success)
            {
                InterviewSingleQuestionEntry_topSuccessMessageLabel.Text = c.Message.ToString();
                InterviewSingleQuestionEntry_bottomSuccessMessageLabel.Text = c.Message.ToString();
            }
        }

        /// <summary>
        /// This method will be called when the user clicks the Accept button in 
        /// the disclaimer popup. It performs a task based on the page mode. For 
        /// example, If the page mode is EDIT and the question is not included in
        /// any test, the question will be saved as a new question. Otherwise, the
        /// question will get updated. Also, in this event handler, concurrent 
        /// modification is handled by using the MODIFIED_DATE.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void DisclaimerControl_acceptButton_Click(object sender, EventArgs e)
        {
            ClearAllLabelMessage();

            bool redirectToEditFromNew = false;
            bool redirectToEditFromEdit = false;
            QuestionDetail questionDetail = null;
            string questionKey = null;

            try
            {
                questionDetail = ConstructQuestionDetails();

                if (Convert.ToBoolean(ViewState["IS_EDIT_MODE"]) == true)
                {
                    questionKey = Request.QueryString["questionkey"].ToString();

                    if (Convert.ToBoolean(ViewState["CAN_EDIT"]) == false)
                    {
                        new QuestionBLManager().SaveInterviewQuestion(questionDetail);
                        redirectToEditFromNew = true;
                    }
                    else
                    {
                        // Check if the question is modified by someone else. 
                        // This will help to prevent concurrent updation
                        if (new CommonBLManager().IsInterviewRecordModified("QUESTION", questionKey,
                            (DateTime)ViewState["MODIFIED_DATE"]))
                        {
                            ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                                InterviewSingleQuestionEntry_bottomErrorMessageLabel,
                                    Resources.HCMResource.SingleQuestionEntry_ConcurrentEntryIsNotPossible);
                            return;
                        }

                        new QuestionBLManager().UpdateInterviewQuestion(questionDetail, questionKey,
                            InterviewSingleQuestionEntry_deletedChoicesHiddenField.Value, base.userID);

                        redirectToEditFromEdit = true;
                    }
                }
                else
                {
                    new QuestionBLManager().SaveInterviewQuestion(questionDetail);
                    redirectToEditFromNew = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                    InterviewSingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }

            string redirectURL = "InterviewSingleQuestionEntry.aspx?m=0";

            if (redirectToEditFromNew == true)
            {
                // This will check whether the navigation came from Search Question page.
                // If the question is associated with the any tests, user tries to edit,
                // This will show the appropriate success message after its been saved.
                if (Request.QueryString["parentpage"] == Constants.ParentPage.SEARCH_INTERVIEW_QUESTION)
                {
                    redirectURL = redirectURL + "&s=2&questionKey=" + questionDetail.QuestionKey
                        + "&fromnew=y&parentpage=S_IQS&reseturl=s";
                    Response.Redirect(redirectURL, false);
                }
                else
                {
                    // Open newly saved question in edit mode.
                    Response.Redirect("InterviewSingleQuestionEntry.aspx?m=0&s=1&questionKey="
                        + questionDetail.QuestionKey
                        + "&fromnew=y&reseturl=s", false);
                }
            }
            else if (redirectToEditFromEdit == true)
            {
                // If the control comes from the search question page, highlight the parent page
                // menu (i.e. search question). Otherwise, highlight the InterviewSingleQuestionEntry menu.
                if (Request.QueryString["parentpage"] == Constants.ParentPage.SEARCH_INTERVIEW_QUESTION)
                    redirectURL = redirectURL + "&s=2&questionKey=" + questionKey
                        + "&fromedit=y&parentpage=S_IQS";
                else
                    redirectURL = redirectURL + "&s=1&questionKey=" + questionKey
                        + "&fromedit=y&parentpage=SNG_QUS";

                Response.Redirect(redirectURL, false);
            }
        }

        /// <summary>
        /// Handler that will check all the mandatory fields are filled. Then it allows the user
        /// to save a question.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void InterviewSingleQuestionEntry_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                bool isEmptySubject = false;

                ClearAllLabelMessage();

                InterviewSingleQuestionEntry_authorTextBox.Text =
                    Request[InterviewSingleQuestionEntry_authorTextBox.UniqueID].Trim();

                if (!IsValidData())
                    return;

                QuestionDetail questionDetail = new QuestionDetail();

                // Get selected subject list from the category subject control
                questionDetail.Subjects = InterviewSingleQuestionEntry_categorySubjectControl.SubjectDataSource;

                // Get added category list
                List<Subject> categories = InterviewSingleQuestionEntry_categorySubjectControl.CategoryDataSource;

                foreach (Subject subject in categories)
                {
                    // Get selected subject count
                    int SubjectCount = (from sub in questionDetail.Subjects
                                        where sub.IsSelected == true &&
                                        subject.CategoryID == sub.CategoryID
                                        select sub).Count();
                    if (SubjectCount != 0)
                        continue;

                    base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                            InterviewSingleQuestionEntry_bottomErrorMessageLabel,
                            string.Format("No subjects selected for '" + subject.CategoryName + "'"));
                    isEmptySubject = true;
                }

                // Terminate the further execution if no subject is selected against the category.
                if (isEmptySubject)
                    return;

                InterviewSingleQuestionEntry_disclaimerPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                    InterviewSingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will add a new choice by using viewstate.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InterviewSingleQuestionEntry_addAnswerLinkButton_Click(object sender, EventArgs e)
        {
            ClearAllLabelMessage();

            try
            {
                List<AnswerChoice> choices = new List<AnswerChoice>();

                if (Utility.IsNullOrEmpty(ViewState["ANSWER_CHOICES"]))
                {
                    choices.Add(new AnswerChoice(string.Empty, 0, false));
                    ViewState["ANSWER_CHOICES"] = choices;
                }
                else
                {
                    ReLoadViewState();
                    choices = ViewState["ANSWER_CHOICES"] as List<AnswerChoice>;
                    choices.Add(new AnswerChoice(string.Empty, choices.Count + 1, false));
                    ViewState["ANSWER_CHOICES"] = choices;
                }

                // Do not allow to add more than 10 choices for a question
                if (choices.Count > 10)
                {
                    base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                        InterviewSingleQuestionEntry_bottomErrorMessageLabel,
                        Resources.HCMResource.SingleQuestionEntry_Maximum_10_Choices_Can_Be_Added);
                }
                else
                {
                    InterviewSingleQuestionEntry_answerChoicesGridView.DataSource = choices;
                    InterviewSingleQuestionEntry_answerChoicesGridView.DataBind();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                    InterviewSingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will delete a row from the gridview(choice)
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that contains the event data.
        /// </param>
        protected void InterviewSingleQuestionEntry_answerChoicesGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                List<AnswerChoice> choices = new List<AnswerChoice>();
                AnswerChoice choice = new AnswerChoice();
                int optionId;

                // If a row needs to delete
                if (e.CommandName == "DeleteRow")
                {
                    int rowIndex = Convert.ToInt32(e.CommandArgument);
                    GridViewRow currentRow = InterviewSingleQuestionEntry_answerChoicesGridView.Rows[rowIndex];

                    HiddenField InterviewSingleQuestionEntry_questionOptionIdHiddenField = (HiddenField)
                        currentRow.FindControl("InterviewSingleQuestionEntry_questionOptionIdHiddenField");

                    optionId = Convert.ToInt32(InterviewSingleQuestionEntry_questionOptionIdHiddenField.Value);

                    // Get deleted OptionIds then, assigned to a hidden control
                    if (Utility.IsNullOrEmpty(InterviewSingleQuestionEntry_deletedChoicesHiddenField.Value))
                    {
                        InterviewSingleQuestionEntry_deletedChoicesHiddenField.Value = optionId.ToString();
                    }
                    else
                    {
                        InterviewSingleQuestionEntry_deletedChoicesHiddenField.Value
                            = InterviewSingleQuestionEntry_deletedChoicesHiddenField.Value + "," + optionId.ToString();
                    }

                    // Get latest choices list from viewstate and assign to choices
                    ReLoadViewState();
                    choices = (List<AnswerChoice>)ViewState["ANSWER_CHOICES"];

                    // Remove the current row if the choice count is greater than 2
                    if (choices.Count > 1)
                    {
                        choices.RemoveAt(Convert.ToInt32(rowIndex));
                    }
                    else
                    {
                        base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                            InterviewSingleQuestionEntry_bottomErrorMessageLabel,
                            "A question must contain atleast one answer choice");
                    }

                    // Then rebind the gridview with the remaining choice list
                    InterviewSingleQuestionEntry_answerChoicesGridView.DataSource = choices;
                    InterviewSingleQuestionEntry_answerChoicesGridView.DataBind();

                    // Update the viewstate
                    ViewState["ANSWER_CHOICES"] = choices;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                    InterviewSingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will make the selection of correct answer radio button.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewRowEventArgs"/> that contains the event data.
        /// </param>
        protected void InterviewSingleQuestionEntry_answerChoicesGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton InterviewSingleQuestionEntry_deleteImageButton = (ImageButton)
                    e.Row.FindControl("InterviewSingleQuestionEntry_deleteImageButton");

                InterviewSingleQuestionEntry_deleteImageButton.CommandArgument = e.Row.RowIndex.ToString();

                // RadioButton InterviewSingleQuestionEntry_correctAnswerRadioButton = (RadioButton)
                //     e.Row.FindControl("InterviewSingleQuestionEntry_correctAnswerRadioButton");

                HiddenField InterviewSingleQuestionEntry_isCorrectAnswerHiddenField = (HiddenField)
                    e.Row.FindControl("InterviewSingleQuestionEntry_isCorrectAnswerHiddenField");

                // Make a selection to the correct answer
                // if (Convert.ToBoolean(InterviewSingleQuestionEntry_isCorrectAnswerHiddenField.Value) == true)
                //     InterviewSingleQuestionEntry_correctAnswerRadioButton.Checked = true;
            }
        }

        /// <summary>
        /// Handler that helps to preview the question before it is saved.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void InterviewSingleQuestionEntry_previewButton_Click(object sender, EventArgs e)
        {
            try
            {
                InterviewSingleQuestionEntry_authorTextBox.Text
                    = Request[InterviewSingleQuestionEntry_authorTextBox.UniqueID].Trim();

                QuestionDetail questionDetails = ConstructQuestionDetails();

                InterviewSingleQuestionEntry_questionPreviewControl.QuestionDatasource = questionDetails;
                InterviewSingleQuestionEntry_questionPreviewControl.Title = "Preview";
                InterviewSingleQuestionEntry_questionPreviewModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                    InterviewSingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This will clear if the page mode is new question entry. Otherwise, it will reload
        /// the data against given question key.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void InterviewSingleQuestionEntry_resetLinkButton_Click(object sender, EventArgs e)
        {
            // Redirect to edit question entry page when the mode is edit
            // Otherwise, redirect to single question entry page
            if (Convert.ToBoolean(ViewState["IS_EDIT_MODE"]) == true)
            {
                if (Request.QueryString["reseturl"] != null)
                {
                    Response.Redirect("~/InterviewQuestions/InterviewSingleQuestionEntry.aspx?m=0&s=1", false);
                }
                else
                {
                    Response.Redirect("~/InterviewQuestions/InterviewSingleQuestionEntry.aspx?m=0&s=2&parentpage=S_IQS&questionkey="
                        + Request.QueryString["questionkey"], false);
                }
            }
            else
            {
                Response.Redirect("~/InterviewQuestions/InterviewSingleQuestionEntry.aspx?m=0&s=1", false);
            }
        }


        /// <summary>
        /// Handler that helps to save the posted file in session
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>   
        protected void InterviewSingleQuestion_questionImageButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (InterviewSingleQuestionEntry_fileUpload.HasFile)
                {
                    if (InterviewSingleQuestionEntry_fileUpload.PostedFile.ContentLength > 102400) // check if image size exceeds 100kb
                    {
                        base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                    InterviewSingleQuestionEntry_bottomErrorMessageLabel, HCMResource.BatchQuestionEntry_imageSizeExceeded);
                        return;
                    }

                    // Check if the photo is of the desired extension.
                    string extension = Path.GetExtension(InterviewSingleQuestionEntry_fileUpload.FileName);
                    extension = extension.ToLower();

                    if (extension != ".gif" && extension != ".png" && extension != ".jpg" && extension != ".jpeg")
                    {
                        base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                            InterviewSingleQuestionEntry_bottomErrorMessageLabel, 
                            "Only gif/png/jpg/jpeg files are allowed");
                        return;
                    }

                    // Read the bytes from the posted file.
                    HttpPostedFile postedQstImage = InterviewSingleQuestionEntry_fileUpload.PostedFile;
                    int nFileLen = postedQstImage.ContentLength;
                    byte[] imgData = new byte[nFileLen];

                    postedQstImage.InputStream.Read(imgData, 0, nFileLen);

                    Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = imgData;
                    InterviewSingleQuestionEntry_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=INTERVIEW_QUESTION_IMAGE";
                    InterviewSingleQuestionEntry_AddQuestionImageTR.Style.Add("display", "none");
                    InterviewSingleQuestionEntry_DisplayQuestionImageTR.Style.Add("display", "");
                    InterviewSingleQuestionEntry_addImageLinkButton.Style.Add("display", "none");
                }

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                    InterviewSingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler that helps to delete the posted file.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void SingleQuestion_deleteQstLnkButtonClick(object sender, EventArgs e)
        {
            try
            {
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
                InterviewSingleQuestionEntry_questionImage.ImageUrl = string.Empty;
                InterviewSingleQuestionEntry_AddQuestionImageTR.Style.Add("display", "none");
                InterviewSingleQuestionEntry_DisplayQuestionImageTR.Style.Add("display", "none");
                InterviewSingleQuestionEntry_addImageLinkButton.Style.Add("display", "");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                    InterviewSingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that helps to close the popup.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CloseQuestionImagePopUpClick(object sender, EventArgs e)
        {
            try
            {
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                    InterviewSingleQuestionEntry_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Method that will construct the question detail.
        /// </summary>
        /// <remarks>This method can be used by Save and Preview buttons. </remarks>
        /// <returns>
        /// A <see cref="QuestionDetail"/> that contains the question detail object.
        /// </returns>
        private QuestionDetail ConstructQuestionDetails()
        {
            // Construct question detail object.
            QuestionDetail questionDetail = new QuestionDetail();

            questionDetail.Question = InterviewSingleQuestionEntry_questionTextBox.Text.Trim();

            if (InterviewSingleQuestionEntry_complexityDropDownList.SelectedItem.Value != null &&
                InterviewSingleQuestionEntry_complexityDropDownList.SelectedItem.Value.Trim().ToUpper() != "--SELECT--")
            {
                questionDetail.Complexity = InterviewSingleQuestionEntry_complexityDropDownList.SelectedItem.Value;
                questionDetail.ComplexityName = InterviewSingleQuestionEntry_complexityDropDownList.SelectedItem.Text;
            }

            questionDetail.Tag = InterviewSingleQuestionEntry_tagsTextBox.Text.Trim();

            if (Convert.ToBoolean(ViewState["IS_EDIT_MODE"]) == true)
            {
                // Check if the CreditsEarned textbox is not empty
                if (InterviewSingleQuestionEntry_creditTextBox.Text.Length > 0)
                {
                    // Set modified credits to object field.
                    questionDetail.CreditsEarned
                        = Convert.ToDecimal(InterviewSingleQuestionEntry_creditTextBox.Text);
                    questionDetail.DeletedCategories
                        = InterviewSingleQuestionEntry_categorySubjectControl.DeletedCategories;
                }
            }
            else
            {
                // Set 0.00 if the page is single question entry
                questionDetail.CreditsEarned = 0.00M;
            }

            questionDetail.NoOfChoices =
                Convert.ToInt16(InterviewSingleQuestionEntry_answerChoicesGridView.Rows.Count);

            // Construct the answer choice.
            List<AnswerChoice> answerChoices = new List<AnswerChoice>();
            AnswerChoice answerChoice = null;

            foreach (GridViewRow gvr in InterviewSingleQuestionEntry_answerChoicesGridView.Rows)
            {
                TextBox InterviewSingleQuestionEntry_answerTextBox =
                    (TextBox)gvr.FindControl("InterviewSingleQuestionEntry_answerTextBox");

                // RadioButton InterviewSingleQuestionEntry_correctAnswerRadioButton =
                //    (RadioButton)gvr.FindControl("InterviewSingleQuestionEntry_correctAnswerRadioButton");

                HiddenField InterviewSingleQuestionEntry_questionOptionIdHiddenField = (HiddenField)
                    gvr.FindControl("InterviewSingleQuestionEntry_questionOptionIdHiddenField");

                // if (InterviewSingleQuestionEntry_correctAnswerRadioButton.Checked)
                //     questionDetail.Answer = Convert.ToInt16(gvr.RowIndex + 1);

                answerChoice = new AnswerChoice();
                answerChoice.ChoiceID = gvr.RowIndex + 1;
                answerChoice.Choice = InterviewSingleQuestionEntry_answerTextBox.Text.Trim();
                answerChoice.IsCorrect = true;
                answerChoice.QuestionGenId =
                            int.Parse(InterviewSingleQuestionEntry_questionOptionIdHiddenField.Value);

                if (Convert.ToBoolean(ViewState["IS_EDIT_MODE"]) == true)
                {
                    // If the question option id is empty, then it will b considered as a new choice.
                    if (Utility.IsNullOrEmpty(answerChoice.QuestionGenId) || answerChoice.QuestionGenId == 0)
                        answerChoice.RecordStatus = RecordStatus.New;
                    else
                        answerChoice.RecordStatus = RecordStatus.Modified;
                }
                answerChoices.Add(answerChoice);
            }

            questionDetail.AnswerChoices = answerChoices;

            if (!Utility.IsNullOrEmpty(InterviewSingleQuestionEntry_authorIdHiddenField.Value.ToString()))
                questionDetail.Author = Convert.ToInt32(InterviewSingleQuestionEntry_authorIdHiddenField.Value);

            // Additional settings (Test AreaId).
            if (!Utility.IsNullOrEmpty(InterviewSingleQuestionEntry_testAreaRadioButtonList.SelectedValue))
            {
                questionDetail.TestAreaID = InterviewSingleQuestionEntry_testAreaRadioButtonList.SelectedValue;
                questionDetail.TestAreaName = InterviewSingleQuestionEntry_testAreaRadioButtonList.SelectedItem.Text;
            }

            // Check if the page is edit mode. If so, retain the existing Created by
            // and CreateDate value as it is in the database.
            if (Convert.ToBoolean(ViewState["IS_EDIT_MODE"]) == true)
            {
                questionDetail.CreatedBy =
                    Convert.ToInt32(InterviewSingleQuestionEntry_createdByHiddenField.Value);
                questionDetail.CreatedDate =
                    Convert.ToDateTime(InterviewSingleQuestionEntry_createdDateHiddenField.Value);
            }
            else
            {
                questionDetail.CreatedBy = Convert.ToInt32(base.userID);
                questionDetail.CreatedDate = DateTime.Now;
            }

            questionDetail.ModifiedBy = Convert.ToInt32(base.userID);
            questionDetail.Subjects = InterviewSingleQuestionEntry_categorySubjectControl.SubjectDataSource;

            // Get selected subject ids only. This will be used in QuestionBLManager:InsertQuestionRelation
            // method to insert multiple subjects simultanesouly.
            questionDetail.SelectedSubjectIDs = InterviewSingleQuestionEntry_categorySubjectControl.SelectedSubjectIDList;
            if (Session["POSTED_INTERVIEW_QUESTION_IMAGE"] != null)
            {
                questionDetail.QuestionImage = Session["POSTED_INTERVIEW_QUESTION_IMAGE"] as byte[];
                questionDetail.HasImage = true;
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = null;
            }
            else
                questionDetail.HasImage = false;

            return questionDetail;
        }

        /// <summary>
        /// Method that will load the question details for the given question key.
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that contains the question key.
        /// </param>
        private void LoadQuestionDetails(string questionKey)
        {
            // Construct the question details object
            QuestionDetail questionDetail = new QuestionBLManager().GetInterviewQuestion(questionKey);

            // Keep modified date in view state.
            ViewState["MODIFIED_DATE"] = questionDetail.ModifiedDate;

            // Call the method which returns true if the question key is already exists.
            bool canEdit = new QuestionBLManager().IsInterviewQuestionExistsInTest(questionKey);

            ViewState["CAN_EDIT"] = !canEdit;

            if (Convert.ToBoolean(ViewState["CAN_EDIT"]) == false)
            {
                base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                InterviewSingleQuestionEntry_bottomErrorMessageLabel,
                string.Format(Resources.HCMResource.InterviewSingleQuestionEntry_QuestionKeyAlreadyExists,
                questionKey.ToString().Trim()));
            }

            InterviewSingleQuestionEntry_questionIDValueLabel.Text = questionDetail.QuestionKey.Trim();
            InterviewSingleQuestionEntry_lastModifiedDateValueLabel.Text =
                questionDetail.ModifiedDate.ToString("MMM-dd-yyyy");

            InterviewSingleQuestionEntry_questionTextBox.Text = questionDetail.Question.ToString().Trim();
            InterviewSingleQuestionEntry_tagsTextBox.Text = questionDetail.Tag.ToString().Trim();
            InterviewSingleQuestionEntry_creditTextBox.Text = questionDetail.CreditsEarned.ToString();

            // Get user name/email by passing user id
            InterviewSingleQuestionEntry_authorTextBox.Text =
                new CommonBLManager().GetUserDetail(questionDetail.Author).FirstName;
            InterviewSingleQuestionEntry_authorIdHiddenField.Value = questionDetail.Author.ToString();
            InterviewSingleQuestionEntry_createdByHiddenField.Value = questionDetail.CreatedBy.ToString().Trim();
            InterviewSingleQuestionEntry_createdDateHiddenField.Value = questionDetail.CreatedDate.ToString();

            InterviewSingleQuestionEntry_answerChoicesGridView.DataSource = questionDetail.AnswerChoices;
            InterviewSingleQuestionEntry_answerChoicesGridView.DataBind();

            // Fetch unique categories from the list.
            var distinctCategories = questionDetail.Subjects.GroupBy(x => x.CategoryID)
                .Select(x => x.First());

            // Get selected subjects
            InterviewSingleQuestionEntry_categorySubjectControl.SubjectsToBeSelected = questionDetail.Subjects;

            InterviewSingleQuestionEntry_categorySubjectControl.QuestionKey = questionKey.ToString().Trim();
            InterviewSingleQuestionEntry_categorySubjectControl.CategoryDataSource = distinctCategories.ToList<Subject>();
            InterviewSingleQuestionEntry_categorySubjectControl.SubjectDataSource = distinctCategories.ToList<Subject>();

            InterviewSingleQuestionEntry_testAreaRadioButtonList.SelectedValue = questionDetail.TestAreaID;
            InterviewSingleQuestionEntry_complexityDropDownList.SelectedValue = questionDetail.Complexity;

            // Assign answer choices to viewstate. This will be used for adding new choice or delete choice.
            ViewState["ANSWER_CHOICES"] = questionDetail.AnswerChoices;

            if (questionDetail.HasImage)
            {
                Session["POSTED_INTERVIEW_QUESTION_IMAGE"] = questionDetail.QuestionImage;
                InterviewSingleQuestionEntry_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=INTERVIEW_QUESTION_IMAGE&questionKey=" + questionDetail.QuestionKey;
                InterviewSingleQuestionEntry_AddQuestionImageTR.Style.Add("display", "none");
                InterviewSingleQuestionEntry_DisplayQuestionImageTR.Style.Add("display", "");
                InterviewSingleQuestionEntry_addImageLinkButton.Style.Add("display", "none");
            }
        }

        /// <summary>
        /// Method that helps to maintain the viewstate which contains the updated answer 
        /// choices. For example, if you add or remove an answer choice, viewstate takes 
        /// the current list of answer choices.
        /// </summary>
        private void ReLoadViewState()
        {
            List<AnswerChoice> choices = new List<AnswerChoice>();

            foreach (GridViewRow row in InterviewSingleQuestionEntry_answerChoicesGridView.Rows)
            {
                ImageButton InterviewSingleQuestionEntry_deleteImageButton = (ImageButton)
                    row.FindControl("InterviewSingleQuestionEntry_deleteImageButton");

                // RadioButton InterviewSingleQuestionEntry_correctAnswerRadioButton = (RadioButton)
                //     row.FindControl("InterviewSingleQuestionEntry_correctAnswerRadioButton");

                TextBox InterviewSingleQuestionEntry_answerTextBox = (TextBox)
                    row.FindControl("InterviewSingleQuestionEntry_answerTextBox");

                HiddenField InterviewSingleQuestionEntry_isCorrectAnswerHiddenField = (HiddenField)
                    row.FindControl("InterviewSingleQuestionEntry_isCorrectAnswerHiddenField");

                HiddenField InterviewSingleQuestionEntry_questionOptionIdHiddenField = (HiddenField)
                    row.FindControl("InterviewSingleQuestionEntry_questionOptionIdHiddenField");

                AnswerChoice choice = new AnswerChoice();
                choice.ChoiceID = Convert.ToInt32(InterviewSingleQuestionEntry_deleteImageButton.CommandArgument) + 1;
                choice.Choice = InterviewSingleQuestionEntry_answerTextBox.Text.Trim();

                // Find the correct answer radio button
                // if (InterviewSingleQuestionEntry_correctAnswerRadioButton.Checked)
                //    choice.IsCorrect = true;

                choice.QuestionGenId = Convert.ToInt32(InterviewSingleQuestionEntry_questionOptionIdHiddenField.Value);
                choices.Add(choice);
            }
            ViewState["ANSWER_CHOICES"] = choices;
        }

        /// <summary>
        /// Method taht helps to clear success/error label text.
        /// </summary>
        private void ClearAllLabelMessage()
        {
            InterviewSingleQuestionEntry_topErrorMessageLabel.Text = string.Empty;
            InterviewSingleQuestionEntry_bottomErrorMessageLabel.Text = string.Empty;
            InterviewSingleQuestionEntry_topSuccessMessageLabel.Text = string.Empty;
            InterviewSingleQuestionEntry_bottomSuccessMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// It checks the all the mandatory fields are filled without fail.
        /// </summary>
        /// <returns>
        /// It returns either TRUE or FALSE based on the validation.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;

            // bool isChecked = false;

            StringBuilder errorMessage = new StringBuilder();

            InterviewSingleQuestionEntry_topErrorMessageLabel.Text = string.Empty;
            InterviewSingleQuestionEntry_bottomErrorMessageLabel.Text = string.Empty;

            // Validate question description
            if (InterviewSingleQuestionEntry_questionTextBox.Text.Trim().Length == 0)
            {
                errorMessage.Append("Question, ");
                //base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                //    InterviewSingleQuestionEntry_bottomErrorMessageLabel,
                //    Resources.HCMResource.SingleQuestionEntry_QuestionCannotBeEmpty);
                //isValidData = false;
            }

            // Validate answer choices
            foreach (GridViewRow gvr in InterviewSingleQuestionEntry_answerChoicesGridView.Rows)
            {
                // RadioButton InterviewSingleQuestionEntry_correctAnswerRadioButton = (RadioButton)
                //    gvr.FindControl("InterviewSingleQuestionEntry_correctAnswerRadioButton");

                // if (InterviewSingleQuestionEntry_correctAnswerRadioButton.Checked)
                // {
                //     isChecked = true;
                // }

                TextBox InterviewSingleQuestionEntry_answerTextBox = (TextBox)
                   gvr.FindControl("InterviewSingleQuestionEntry_answerTextBox");

                // Validate choice textbox
                if (InterviewSingleQuestionEntry_answerTextBox.Text.Trim().Length == 0)
                {
                    errorMessage.Append("Answer choice of row " + (gvr.RowIndex + 1) + ", ");
                    //base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                    //    InterviewSingleQuestionEntry_bottomErrorMessageLabel,
                    //    string.Format(Resources.HCMResource.SingleQuestionEntry_AnswerChoiceCannotBeEmpty,
                    //    gvr.RowIndex + 1));
                    //  isValidData = false;
                }
            }

            // If no option is checked


            //if (!isChecked)
            //{
            //    errorMessage.Append("Correct answer, ");
            //    //base.ShowMessage(SingleQuestionEntry_topErrorMessageLabel,
            //    //    SingleQuestionEntry_bottomErrorMessageLabel,
            //    //    Resources.HCMResource.SingleQuestionEntry_CorrectAnswerNotSelected);
            //    //isValidData = false;
            //}

            // Validate category
            List<Subject> subjects = new List<Subject>();
            subjects = InterviewSingleQuestionEntry_categorySubjectControl.CategoryDataSource;
            bool subjectFlag = true;
            if (subjects == null || subjects.Count == 0)
            {
                errorMessage.Append("Category, ");
                //base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                //    InterviewSingleQuestionEntry_bottomErrorMessageLabel,
                //    Resources.HCMResource.SingleQuestionEntry_CategoryNotSelected);
                subjectFlag = false;
                // isValidData = false;
            }

            // Validate subjects
            string selectedSubjects = InterviewSingleQuestionEntry_categorySubjectControl.SelectedSubjectIDList;

            List<Subject> SubjectCategory = new List<Subject>();
            SubjectCategory = InterviewSingleQuestionEntry_categorySubjectControl.SubjectDataSource;
            if (!Utility.IsNullOrEmpty(SubjectCategory))
            {
                if (SubjectCategory.Exists(p => p.IsSelected == true))
                {
                    subjectFlag = true;
                }
                else
                {
                    subjectFlag = false;
                }
            }
            else
            {
                subjectFlag = false;
            }

            if (!subjectFlag)
            {
                errorMessage.Append("Subject, ");
                //base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                //       InterviewSingleQuestionEntry_bottomErrorMessageLabel,
                //       string.Format(Resources.HCMResource.SingleQuestionEntry_SubjectsNotSelected));
                //isValidData = false;
            }

            // Validate Testarea
            ListItem item = InterviewSingleQuestionEntry_testAreaRadioButtonList.SelectedItem;
            if (Utility.IsNullOrEmpty(item))
            {
                errorMessage.Append("Interview area, ");
                //base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                //    InterviewSingleQuestionEntry_bottomErrorMessageLabel,
                //    Resources.HCMResource.SingleQuestionEntry_TestAreaNotSelected);
                //isValidData = false;
            }

            // Validate Complexity
            if (InterviewSingleQuestionEntry_complexityDropDownList.SelectedIndex == 0)
            {
                errorMessage.Append("Complexity, ");
                //base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                //    InterviewSingleQuestionEntry_bottomErrorMessageLabel,
                //    Resources.HCMResource.SingleQuestionEntry_ComplexityNotSelected);
                //isValidData = false;
            }
            if (errorMessage.Length > 0)
            {
                base.ShowMessage(InterviewSingleQuestionEntry_topErrorMessageLabel,
                    InterviewSingleQuestionEntry_bottomErrorMessageLabel,
                    Resources.HCMResource.Common_Mandatory_Error_Message + " " + errorMessage.ToString().Trim().TrimEnd(','));
                isValidData = false;
            }

            return isValidData;
        }

        /// <summary>
        /// It loads the default data to the respective controls.
        /// </summary>
        protected override void LoadValues()
        {
            Master.SetPageCaption(Resources.HCMResource.SingleQuestionEntry_Title);

            //Bind test area based on TEST_AREA attribute type
            InterviewSingleQuestionEntry_testAreaRadioButtonList.DataSource =
                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.TEST_AREA, "A");
            InterviewSingleQuestionEntry_testAreaRadioButtonList.DataTextField = "AttributeName";
            InterviewSingleQuestionEntry_testAreaRadioButtonList.DataValueField = "AttributeID";

            InterviewSingleQuestionEntry_testAreaRadioButtonList.DataBind();

            //Bind complexities/attributes based on the attribute type
            InterviewSingleQuestionEntry_complexityDropDownList.DataSource =
                new AttributeBLManager().GetAttributesByType(Constants.AttributeTypes.COMPLEXITY, "A");
            InterviewSingleQuestionEntry_complexityDropDownList.DataTextField = "AttributeName";
            InterviewSingleQuestionEntry_complexityDropDownList.DataValueField = "AttributeID";
            InterviewSingleQuestionEntry_complexityDropDownList.DataBind();

            InterviewSingleQuestionEntry_complexityDropDownList.Items.Insert(0, "--Select--");
            InterviewSingleQuestionEntry_complexityDropDownList.SelectedIndex = 0;

            // Get disclaimer message from the database
            InterviewSingleQuestionEntry_disclaimerControl.Message =
                new CommonBLManager().GetDisclaimerMessage
                (Constants.DisclaimerMessageConstants.SAVE_QUESTION).ToString();
        }

        #endregion Protected Overridden Methods
    }
}